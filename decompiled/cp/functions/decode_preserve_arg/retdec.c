int64_t decode_preserve_arg(int64_t a1, int64_t a2, int32_t a3) {
    char * v1 = xstrdup((char *)a1); // 0x45e4
    int64_t v2 = function_3870(); // 0x461b
    if (v2 != 0) {
        // 0x4625
        *(char *)v2 = 0;
    }
    char * v3 = (char)a3 != 0 ? "--preserve" : "--no-preserve"; // 0x4605
    int64_t v4 = __xargmatch_internal(v3, v1, g24, (int32_t *)&g5, 4, g29, true); // 0x4651
    uint32_t v5 = *(int32_t *)(4 * v4 + (int64_t)&g5); // 0x4658
    int32_t v6 = v5; // 0x465c
    if (v5 >= 7) {
        int64_t v7 = decode_preserve_arg_cold(); // 0x465c
        v6 = *(int32_t *)(4 * v7 + (int64_t)&g5);
    }
    int32_t v8 = *(int32_t *)(4 * (int64_t)v6 + (int64_t)&g3); // 0x4665
    return (int64_t)v8 + (int64_t)&g3;
}