bool do_copy(int32_t n_files, char ** file, char * target_directory, bool no_target_directory, int32_t * x) {
    int64_t v1 = n_files;
    int64_t v2 = __readfsqword(40); // 0x56fb
    int32_t v3 = 0; // bp-234, 0x570f
    int64_t v4 = v1; // 0x571b
    int64_t v5; // 0x56d0
    int64_t v6; // 0x56d0
    int64_t v7; // 0x56d0
    int64_t v8; // 0x56d0
    int64_t v9; // 0x56d0
    int32_t v10; // 0x56d0
    int32_t v11; // 0x56d0
    int32_t v12; // 0x56d0
    int32_t v13; // 0x56d0
    char * v14; // 0x56d0
    char * v15; // 0x56d0
    int64_t v16; // 0x56d0
    if ((int64_t)(target_directory == NULL) < v1) {
        // 0x5721
        v16 = (int64_t)file;
        if (no_target_directory) {
            if (target_directory != NULL) {
                // 0x5c5d
                function_3800();
            } else {
                // 0x58f6
                v5 = v16;
                if (n_files > 2) {
                    goto lab_0x5c81;
                } else {
                    goto lab_0x5903;
                }
            }
        } else {
            int64_t v17; // bp-216, 0x56d0
            if (target_directory == NULL) {
                int64_t v18 = *(int64_t *)(v16 - 8 + 8 * v1); // 0x5b8a
                char * v19 = (char *)v18; // 0x5b9c
                int32_t v20 = target_directory_operand(v19, (struct stat *)&v17); // 0x5ba3
                if (v20 == -1) {
                    int32_t v21 = *(int32_t *)function_36d0(); // 0x5be6
                    v6 = v16;
                    v7 = v16;
                    if (v21 == 2) {
                        goto lab_0x5c38;
                    } else {
                        goto lab_0x5bef;
                    }
                } else {
                    // 0x5bb3
                    v5 = v16;
                    if (v18 == 0) {
                        goto lab_0x5903;
                    } else {
                        int32_t v22 = n_files - 1; // 0x5bb3
                        v11 = v22;
                        v15 = v19;
                        v13 = v20;
                        if (v22 > 1) {
                            goto lab_0x5b67;
                        } else {
                            // 0x5bd0
                            v10 = 1;
                            v14 = v19;
                            v12 = v20;
                            v9 = v18;
                            v8 = 1;
                            if (v22 == 1) {
                                goto lab_0x576b;
                            } else {
                                goto lab_0x58c3_2;
                            }
                        }
                    }
                }
            } else {
                int32_t v23 = target_directory_operand(target_directory, (struct stat *)&v17); // 0x574a
                if (v23 == -1) {
                    goto lab_0x5cc0;
                } else {
                    // 0x575e
                    v10 = n_files;
                    v14 = target_directory;
                    v12 = v23;
                    v11 = n_files;
                    v15 = target_directory;
                    v13 = v23;
                    if (n_files != 1) {
                        goto lab_0x5b67;
                    } else {
                        goto lab_0x576b;
                    }
                }
            }
        }
    } else {
        goto lab_0x5d08;
    }
  lab_0x57b5:;
    // 0x57b5
    char * v24; // 0x56d0
    last_component(v24);
    int64_t v25 = function_3820() + 24; // 0x57cc
    int64_t v26 = v25 & -0x1000; // 0x57d3
    int64_t v27; // 0x56d0
    int64_t v28 = v27 - v26; // 0x57d9
    int64_t v29 = v27; // 0x57e3
    int64_t v30 = v27; // 0x57e3
    if (v26 != 0) {
        int64_t v31 = v29 - (int64_t)"_ctype_get_mb_cur_max"; // 0x57e5
        v29 = v31;
        v30 = v31;
        while (v31 != v28) {
            // 0x57e5
            v31 = v29 - (int64_t)"_ctype_get_mb_cur_max";
            v29 = v31;
            v30 = v31;
        }
    }
    int64_t v32 = function_3a80(); // 0x581d
    char * v33 = (char *)v32; // 0x5828
    strip_trailing_slashes(v33);
    int32_t v34 = (int32_t)*v33 - 46; // 0x5832
    int32_t v35 = v34; // 0x5835
    if (v34 == 0) {
        int32_t v36 = (int32_t)*(char *)(v32 + 1) - 46; // 0x583d
        v35 = v36;
        if (v36 == 0) {
            // 0x5842
            v35 = (int32_t)*(char *)(v32 + 2);
        }
    }
    int64_t v37 = v30 - (v25 & (int64_t)(int32_t)"__ctype_get_mb_cur_max"); // 0x5800
    char * v38 = v24; // 0x5865
    int64_t * v39 = (int64_t *)(v37 - 16); // 0x5865
    int64_t * v40 = (int64_t *)(v37 - 8); // 0x5865
    int64_t v41 = v37; // 0x5865
    char ** v42; // bp-280, 0x56d0
    int64_t v43 = (int64_t)file_name_concat(v14, (char *)(v32 + (int64_t)(v35 == 0)), v42); // 0x5865
    goto lab_0x5868;
  lab_0x59f8:;
    int64_t v75 = function_3820() + 24; // 0x5a04
    int64_t v76 = v75 & -0x1000; // 0x5a0b
    int64_t v77 = v27 - v76; // 0x5a11
    int64_t v78 = v27; // 0x5a1b
    int64_t v79 = v27; // 0x5a1b
    if (v76 != 0) {
        int64_t v80 = v78 - (int64_t)"_ctype_get_mb_cur_max"; // 0x5a1d
        v78 = v80;
        v79 = v80;
        while (v80 != v77) {
            // 0x5a1d
            v80 = v78 - (int64_t)"_ctype_get_mb_cur_max";
            v78 = v80;
            v79 = v80;
        }
    }
    int64_t v81 = v79 - (v75 & (int64_t)(int32_t)"__ctype_get_mb_cur_max"); // 0x5a38
    char * v82 = (char *)function_3a80(); // 0x5a60
    strip_trailing_slashes(v82);
    char * v83 = file_name_concat(v14, v82, v42); // 0x5a76
    int64_t v84 = (int64_t)v83; // 0x5a76
    int64_t * v85 = (int64_t *)(v81 - 8);
    *v85 = (int64_t)v14;
    int64_t * v86 = (int64_t *)(v81 - 16);
    int64_t v69; // 0x56d0
    *v86 = v69;
    char * v87 = *(char *)(v69 + 60) != 0 ? "%s -> %s\n" : NULL; // 0x5ab9
    int64_t sb; // bp-224, 0x56d0
    int64_t sb2; // bp-232, 0x56d0
    bool v88 = make_dir_parents_private(v83, sb - v84, v12, v87, (int32_t **)&sb2, (bool *)&v3, &g59); // 0x5ab9
    int64_t v89 = sb; // 0x5acc
    if (*(char *)sb == 47) {
        int64_t v90 = v89 + 1;
        sb = v90;
        v89 = v90;
        while (*(char *)v90 == 47) {
            // 0x5ad8
            v90 = v89 + 1;
            sb = v90;
            v89 = v90;
        }
    }
    int64_t v55 = v81; // 0x5aeb
    int64_t v56 = v84; // 0x5aeb
    int64_t v57 = 0; // 0x5aeb
    int64_t v67; // 0x578f
    if (v88) {
        // 0x5ae9
        v38 = (char *)v67;
        v39 = v86;
        v40 = v85;
        v41 = v81;
        v43 = v84;
        goto lab_0x5868;
    } else {
        goto lab_0x5afb;
    }
  lab_0x5868:;
    int64_t v44 = v43;
    int64_t v45 = v41;
    *v40 = 0;
    int64_t v46; // bp-233, 0x56d0
    *v39 = (int64_t)&v46;
    char * v47 = (char *)v44; // 0x5894
    bool v48 = copy(v38, v47, v12, (char *)sb, v3 % 256, x, (bool *)&g59, (bool *)&g59); // 0x5894
    int64_t v49; // 0x56d0
    int64_t v50 = v48 ? v49 : 0; // 0x589b
    int64_t v51 = v45; // 0x58a5
    int64_t v52 = v44; // 0x58a5
    int64_t v53 = v50; // 0x58a5
    if (*(char *)&parents_option != 0) {
        int64_t v54 = re_protect(v47, v12, (char *)sb, (int32_t *)sb2, x) ? v50 : 0; // 0x5b5b
        v55 = v45;
        v56 = v44;
        v57 = v54;
        goto lab_0x5afb;
    } else {
        goto lab_0x58ab;
    }
  lab_0x5afb:;
    int64_t v58 = v57;
    int64_t v59 = v56;
    int64_t v60 = v55;
    v51 = v60;
    v52 = v59;
    v53 = v58;
    if (*(char *)&parents_option != 0) {
        // 0x5b03
        v51 = v60;
        v52 = v59;
        v53 = v58;
        if (sb2 != 0) {
            int64_t v61 = *(int64_t *)(sb2 + 160); // 0x5b1b
            sb2 = v61;
            function_3670();
            v51 = v60;
            v52 = v59;
            v53 = v58;
            int64_t v62 = v61; // 0x5b31
            while (v61 != 0) {
                // 0x5b18
                v61 = *(int64_t *)(v62 + 160);
                sb2 = v61;
                function_3670();
                v51 = v60;
                v52 = v59;
                v53 = v58;
                v62 = v61;
            }
        }
    }
    goto lab_0x58ab;
  lab_0x58ab:;
    // 0x58ab
    int64_t v63; // 0x56d0
    int64_t v64 = v63 + 1; // 0x58ae
    function_3670();
    v63 = v64;
    int64_t v65 = v51; // 0x58bd
    int64_t v66 = v53; // 0x58bd
    v9 = v52;
    v8 = v53;
    if (v64 >= (int64_t)v10) {
        // break -> 0x58c3
        goto lab_0x58c3_2;
    }
    goto lab_0x5788;
  lab_0x5d08:
    // 0x5d08
    if ((int32_t)v4 == 1) {
        // 0x5d3e
        quotearg_style();
        // 0x5c9e
        function_3800();
        function_3b80();
        // 0x5cb6
        usage(1);
        goto lab_0x5cc0;
    } else {
        // 0x5d19
        function_3800();
        function_3b80();
        // 0x5cb6
        usage(1);
        goto lab_0x5cc0;
    }
  lab_0x5c81:
    // 0x5c81
    quotearg_style();
    // 0x5c9e
    function_3800();
    function_3b80();
    // 0x5cb6
    usage(1);
    goto lab_0x5cc0;
  lab_0x5903:
    // 0x5903
    if (*(char *)&parents_option != 0) {
        // 0x5d19
        function_3800();
        function_3b80();
        // 0x5cb6
        usage(1);
        goto lab_0x5cc0;
    } else {
        int64_t v71 = *(int64_t *)v5; // 0x590a
        int64_t v72; // 0x56d0
        if (!(((int32_t)v72 == 0 | *(char *)((int64_t)x + 22) == 0))) {
            // 0x5931
            if ((int32_t)function_39e0() == 0) {
                if ((char)v3 == 0) {
                    // 0x5c41
                    function_3a70();
                }
            }
        }
        char * v73 = (char *)*(int64_t *)(v5 + 8); // 0x59cc
        bool v74 = copy((char *)v71, v73, -100, v73, -((v3 % 256)), x, (bool *)&sb, NULL); // 0x59cc
        v9 = v71;
        v8 = v74 ? 0xffffffff : 0;
        goto lab_0x58c3_2;
    }
  lab_0x5cc0:
    // 0x5cc0
    quotearg_style();
    function_3800();
    function_36d0();
    function_3b80();
    v4 = 1;
    goto lab_0x5d03;
  lab_0x5c38:
    // 0x5c38
    v3 = 1;
    v6 = v7;
    goto lab_0x5bef;
  lab_0x5bef:
    // 0x5bef
    v5 = v6;
    if (n_files < 3) {
        goto lab_0x5903;
    } else {
        int64_t v70 = quotearg_style(); // 0x5c08
        function_3800();
        function_3b80();
        v7 = v70;
        goto lab_0x5c38;
    }
  lab_0x5d03:
    // 0x5d03
    function_3840();
    goto lab_0x5d08;
  lab_0x5b67:
    // 0x5b67
    dest_info_init(x);
    src_info_init(x);
    v10 = v11;
    v14 = v15;
    v12 = v13;
    goto lab_0x576b;
  lab_0x576b:
    // 0x576b
    v42 = (char **)&sb;
    v69 = (int64_t)x;
    v63 = 0;
    v65 = (int64_t)&v42;
    v66 = 1;
    while (true) {
      lab_0x5788:
        // 0x5788
        v49 = v66;
        v27 = v65;
        v67 = *(int64_t *)(8 * v63 + v16);
        sb = 0;
        if (*(char *)&remove_trailing_slashes != 0) {
            char * v68 = (char *)v67;
            strip_trailing_slashes(v68);
            v24 = v68;
            if (*(char *)&parents_option == 0) {
                goto lab_0x57b5;
            } else {
                goto lab_0x59f8;
            }
        } else {
            // 0x57a5
            if (*(char *)&parents_option != 0) {
                goto lab_0x59f8;
            } else {
                // 0x57a5
                v24 = (char *)v67;
                goto lab_0x57b5;
            }
        }
    }
    goto lab_0x58c3_2;
  lab_0x58c3_2:
    // 0x58c3
    v4 = v9;
    if (v2 == __readfsqword(40)) {
        // 0x58d6
        return v8 % 2 != 0;
    }
    goto lab_0x5d03;
}