uint do_copy(int param_1,EVP_PKEY_CTX **param_2,EVP_PKEY_CTX *param_3,char param_4,int *param_5)

{
  char cVar1;
  EVP_PKEY_CTX *pEVar2;
  void *pvVar3;
  long lVar4;
  char cVar5;
  uint uVar6;
  int iVar7;
  char *pcVar8;
  size_t sVar9;
  byte *pbVar10;
  EVP_PKEY_CTX *pEVar11;
  void *pvVar12;
  undefined8 uVar13;
  undefined8 uVar14;
  int *piVar15;
  ulong uVar16;
  long lVar17;
  undefined *puVar18;
  char ***pppcVar19;
  char ***pppcVar20;
  char ***pppcVar21;
  int *piVar22;
  EVP_PKEY_CTX *pEVar23;
  uint uVar24;
  long in_FS_OFFSET;
  byte bVar25;
  char **local_118;
  EVP_PKEY_CTX *local_110;
  undefined1 *local_108;
  int local_100;
  int local_fc;
  char local_ea;
  undefined local_e9;
  void *local_e8;
  char *local_e0;
  stat local_d8;
  long local_40;
  
  bVar25 = 0;
  pppcVar20 = &local_118;
  pppcVar21 = &local_118;
  pppcVar19 = &local_118;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_ea = '\0';
  local_110 = param_3;
  local_108 = (undefined1 *)param_5;
  local_fc = param_1;
  if ((int)(uint)(param_3 == (EVP_PKEY_CTX *)0x0) < param_1) {
    local_d8.st_mode = 0;
    if (param_4 != '\0') {
      if (param_3 == (EVP_PKEY_CTX *)0x0) {
        if (param_1 < 3) goto LAB_00105903;
      }
      else {
        uVar13 = dcgettext(0,"cannot combine --target-directory (-t) and --no-target-directory (-T)"
                           ,5);
        param_2 = (EVP_PKEY_CTX **)0x0;
        error(1,0,uVar13);
      }
      uVar13 = quotearg_style(4,param_2[2]);
      pcVar8 = "extra operand %s";
LAB_00105c9e:
      uVar14 = dcgettext(0,pcVar8,5);
      error(0,0,uVar14,uVar13);
      goto LAB_00105cb6;
    }
    if (param_3 == (EVP_PKEY_CTX *)0x0) {
      pEVar2 = param_2[(long)param_1 + -1];
      local_110 = pEVar2;
      local_100 = target_directory_operand(pEVar2,&local_d8);
      if (local_100 == -1) {
        piVar15 = __errno_location();
        iVar7 = *piVar15;
        if (iVar7 == 2) goto LAB_00105c38;
        while (2 < local_fc) {
          param_2 = (EVP_PKEY_CTX **)quotearg_style(4,local_110);
          uVar13 = dcgettext(0,"target %s",5);
          error(1,iVar7,uVar13,param_2);
LAB_00105c38:
          local_ea = '\x01';
        }
      }
      else {
        local_fc = param_1 + -1;
        if (pEVar2 != (EVP_PKEY_CTX *)0x0) {
          if (1 < local_fc) goto LAB_00105b67;
          uVar24 = 1;
          if (local_fc == 1) goto LAB_0010576b;
          goto LAB_001058c3;
        }
      }
LAB_00105903:
      cVar1 = local_ea;
      pEVar2 = *param_2;
      pEVar11 = param_2[1];
      if (parents_option != '\0') {
        pcVar8 = "with --parents, the destination must be a directory";
        goto LAB_00105d19;
      }
      if (((((*(char *)((long)local_108 + 0x16) != '\0') && (*(int *)local_108 != 0)) &&
           (iVar7 = strcmp((char *)pEVar2,(char *)pEVar11), iVar7 == 0)) && (cVar1 == '\0')) &&
         (((local_d8.st_mode != 0 || (iVar7 = stat((char *)pEVar11,&local_d8), iVar7 == 0)) &&
          (piVar15 = (int *)local_108, (local_d8.st_mode & 0xf000) == 0x8000)))) {
        pEVar11 = (EVP_PKEY_CTX *)find_backup_file_name(0xffffff9c,pEVar11,*(int *)local_108);
        piVar22 = (int *)x_tmp_0;
        for (lVar17 = 0x16; lVar17 != 0; lVar17 = lVar17 + -1) {
          *piVar22 = *piVar15;
          piVar15 = piVar15 + (ulong)bVar25 * -2 + 1;
          piVar22 = piVar22 + (ulong)bVar25 * -2 + 1;
        }
        x_tmp_0._0_4_ = 0;
        local_108 = x_tmp_0;
      }
      uVar24 = copy(pEVar2,pEVar11);
      pppcVar20 = &local_118;
LAB_001058c3:
      pppcVar21 = pppcVar20;
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        return uVar24;
      }
      goto LAB_00105d03;
    }
    local_100 = target_directory_operand(param_3,&local_d8);
    if (local_100 != -1) {
      if (local_fc != 1) {
LAB_00105b67:
        piVar15 = (int *)local_108;
        dest_info_init(local_108);
        src_info_init(piVar15);
      }
LAB_0010576b:
      lVar17 = 0;
      uVar24 = 1;
      local_118 = &local_e0;
      do {
        pEVar2 = param_2[lVar17];
        local_e0 = (char *)0x0;
        if (remove_trailing_slashes == '\0') {
          if (parents_option != '\0') goto LAB_001059f8;
LAB_001057b5:
          *(undefined8 *)((long)pppcVar19 + -8) = 0x1057ba;
          pcVar8 = (char *)last_component();
          *(undefined8 *)((long)pppcVar19 + -8) = 0x1057c5;
          sVar9 = strlen(pcVar8);
          for (puVar18 = (undefined *)pppcVar19;
              puVar18 != (undefined *)((long)pppcVar19 + -(sVar9 + 0x18 & 0xfffffffffffff000));
              puVar18 = puVar18 + -0x1000) {
            *(undefined8 *)(puVar18 + -8) = *(undefined8 *)(puVar18 + -8);
          }
          uVar16 = (ulong)((uint)(sVar9 + 0x18) & 0xff0);
          lVar4 = -uVar16;
          pppcVar19 = (char ***)(puVar18 + lVar4);
          if (uVar16 != 0) {
            *(undefined8 *)(puVar18 + -8) = *(undefined8 *)(puVar18 + -8);
          }
          *(undefined8 *)(puVar18 + lVar4 + -8) = 0x105822;
          pbVar10 = (byte *)memcpy((void *)((ulong)(puVar18 + lVar4 + 0xf) & 0xfffffffffffffff0),
                                   pcVar8,sVar9 + 1);
          *(undefined8 *)(puVar18 + lVar4 + -8) = 0x10582d;
          strip_trailing_slashes(pbVar10);
          uVar6 = *pbVar10 - 0x2e;
          if ((uVar6 == 0) && (uVar6 = pbVar10[1] - 0x2e, uVar6 == 0)) {
            uVar6 = (uint)pbVar10[2];
          }
          *(undefined8 *)(puVar18 + lVar4 + -8) = 0x105865;
          pEVar11 = (EVP_PKEY_CTX *)file_name_concat(local_110,pbVar10 + (uVar6 == 0),local_118);
LAB_00105868:
          *(undefined8 *)((long)pppcVar19 + -8) = 0;
          *(undefined **)((long)pppcVar19 + -0x10) = &local_e9;
          *(undefined8 *)((long)pppcVar19 + -0x18) = 0x105899;
          uVar6 = copy(pEVar2,pEVar11);
          uVar24 = uVar24 & uVar6;
          if (parents_option != '\0') {
            *(undefined8 *)((long)pppcVar19 + -8) = 0x105b5b;
            uVar6 = re_protect(pEVar11,local_100,local_e0,local_e8,local_108);
            uVar24 = uVar24 & uVar6;
            goto LAB_00105afb;
          }
        }
        else {
          *(undefined8 *)((long)pppcVar19 + -8) = 0x1059e8;
          strip_trailing_slashes(pEVar2);
          if (parents_option == '\0') goto LAB_001057b5;
LAB_001059f8:
          *(undefined8 *)((long)pppcVar19 + -8) = 0x1059fd;
          sVar9 = strlen((char *)pEVar2);
          for (puVar18 = (undefined *)pppcVar19;
              puVar18 != (undefined *)((long)pppcVar19 + -(sVar9 + 0x18 & 0xfffffffffffff000));
              puVar18 = puVar18 + -0x1000) {
            *(undefined8 *)(puVar18 + -8) = *(undefined8 *)(puVar18 + -8);
          }
          uVar16 = (ulong)((uint)(sVar9 + 0x18) & 0xff0);
          lVar4 = -uVar16;
          pppcVar19 = (char ***)(puVar18 + lVar4);
          if (uVar16 != 0) {
            *(undefined8 *)(puVar18 + -8) = *(undefined8 *)(puVar18 + -8);
          }
          *(undefined8 *)(puVar18 + lVar4 + -8) = 0x105a5a;
          pvVar12 = memcpy((void *)((ulong)(puVar18 + lVar4 + 0xf) & 0xfffffffffffffff0),pEVar2,
                           sVar9 + 1);
          *(undefined8 *)(puVar18 + lVar4 + -8) = 0x105a65;
          strip_trailing_slashes(pvVar12);
          *(undefined8 *)(puVar18 + lVar4 + -8) = 0x105a7b;
          pEVar23 = local_110;
          pEVar11 = (EVP_PKEY_CTX *)file_name_concat(local_110,pvVar12,local_118);
          pcVar8 = (char *)0x0;
          cVar1 = *(char *)((long)local_108 + 0x3c);
          *(EVP_PKEY_CTX **)(puVar18 + lVar4 + -8) = pEVar23;
          if (cVar1 != '\0') {
            pcVar8 = "%s -> %s\n";
          }
          *(undefined1 **)(puVar18 + lVar4 + -0x10) = local_108;
          *(undefined8 *)(puVar18 + lVar4 + -0x18) = 0x105abe;
          cVar5 = make_dir_parents_private
                            (pEVar11,(long)local_e0 - (long)pEVar11,local_100,pcVar8,&local_e8,
                             &local_ea);
          cVar1 = *local_e0;
          while (cVar1 == '/') {
            local_e0 = local_e0 + 1;
            cVar1 = *local_e0;
          }
          if (cVar5 != '\0') goto LAB_00105868;
          uVar24 = 0;
          pppcVar19 = (char ***)(puVar18 + lVar4);
LAB_00105afb:
          pvVar12 = local_e8;
          if (parents_option != '\0') {
            while (pvVar12 != (void *)0x0) {
              pvVar3 = *(void **)((long)pvVar12 + 0xa0);
              local_e8 = pvVar3;
              *(undefined8 *)((long)pppcVar19 + -8) = 0x105b2e;
              free(pvVar12);
              pvVar12 = pvVar3;
            }
          }
        }
        lVar17 = lVar17 + 1;
        *(undefined8 *)((long)pppcVar19 + -8) = 0x1058b7;
        free(pEVar11);
        pppcVar20 = pppcVar19;
      } while ((int)lVar17 < local_fc);
      goto LAB_001058c3;
    }
  }
  else {
    if (param_1 == 1) {
      uVar13 = quotearg_style(4,*param_2);
      pcVar8 = "missing destination file operand after %s";
      goto LAB_00105c9e;
    }
    pcVar8 = "missing file operand";
LAB_00105d19:
    uVar13 = dcgettext(0,pcVar8,5);
    error(0,0,uVar13);
LAB_00105cb6:
    usage(1);
  }
  uVar13 = quotearg_style(4,local_110);
  uVar14 = dcgettext(0,"target directory %s",5);
  piVar15 = __errno_location();
  error(1,*piVar15,uVar14,uVar13);
LAB_00105d03:
                    /* WARNING: Subroutine does not return */
  *(undefined8 *)((long)pppcVar21 + -8) = 0x105d08;
  __stack_chk_fail();
}