uint32_t do_copy(int32_t edi, void** rsi, void** rdx, signed char cl, void** r8, void** r9) {
    void** rdi1;
    void** rcx4;
    void* rsp7;
    void* rbp8;
    void** r13_9;
    void** rsp10;
    int32_t v11;
    void** v12;
    void** v13;
    void* rax14;
    void* v15;
    int32_t eax16;
    unsigned char v17;
    void** rsi18;
    struct s0* rsp19;
    void** rsp20;
    struct s0* rsp21;
    struct s0* rsp22;
    void** rsp23;
    struct s0* rsp24;
    struct s0* rsp25;
    struct s0* rsp26;
    struct s0* rsp27;
    struct s0* rsp28;
    struct s0* rsp29;
    void** rax30;
    struct s0* rsp31;
    struct s0* rsp32;
    void** rsi33;
    int1_t zf34;
    void** r14_35;
    void** r12_36;
    uint32_t ebx37;
    int64_t rax38;
    void** rsi39;
    int32_t eax40;
    int64_t rdx41;
    void** rax42;
    void*** rdi43;
    int32_t ecx44;
    void** rsi45;
    uint32_t eax46;
    uint32_t r14d47;
    int64_t rax48;
    void** rbx49;
    int64_t r14_50;
    void** eax51;
    void** v52;
    void** rax53;
    void** rax54;
    void** rax55;
    void** eax56;
    void* rax57;
    int64_t rbx58;
    int1_t zf59;
    void** r15_60;
    void** v61;
    int1_t zf62;
    void** rdi63;
    struct s0* rsp64;
    int1_t zf65;
    struct s0* rsp66;
    void** rax67;
    void** r12_68;
    struct s0* rsp69;
    void** rax70;
    void** rsp71;
    void** r8_72;
    void*** rax73;
    void** rcx74;
    uint64_t rdx75;
    struct s0* rsp76;
    void** rax77;
    void** rsp78;
    void** r8_79;
    void*** rax80;
    void** rcx81;
    uint64_t rdx82;
    void* rdx83;
    void* rsp84;
    int64_t* rsp85;
    void** rax86;
    int64_t* rsp87;
    uint32_t eax88;
    void** rsi89;
    struct s1* rsp90;
    void** rax91;
    void** rsp92;
    void** r12_93;
    void* rdx94;
    void* rsp95;
    int64_t* rsp96;
    void** rax97;
    int64_t* rsp98;
    int64_t* rsp99;
    void** rax100;
    int1_t zf101;
    void*** rsp102;
    void*** rsp103;
    struct s2* rsp104;
    signed char al105;
    struct s3* rsp106;
    struct s4* rsp107;
    int1_t zf108;
    struct s0* rsp109;
    void** rsp110;
    struct s5* rsp111;
    uint32_t eax112;
    struct s6* rsp113;
    struct s7* rsp114;
    int1_t zf115;
    void** v116;
    struct s0* rsp117;
    uint32_t eax118;
    uint32_t eax119;
    struct s0* rsp120;
    void** r15_121;
    void** rdi122;
    struct s0* rsp123;

    *reinterpret_cast<int32_t*>(&rdi1) = edi;
    *reinterpret_cast<signed char*>(&rcx4) = cl;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp8 = rsp7;
    r13_9 = rsi;
    rsp10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 - 8 - 8 - 0xe8);
    v11 = *reinterpret_cast<int32_t*>(&rdi1);
    v12 = rdx;
    v13 = r8;
    rax14 = g28;
    v15 = rax14;
    eax16 = 0;
    v17 = 0;
    *reinterpret_cast<unsigned char*>(&eax16) = reinterpret_cast<uint1_t>(rdx == 0);
    if (eax16 >= *reinterpret_cast<int32_t*>(&rdi1)) {
        while (1) {
            if (!(*reinterpret_cast<int32_t*>(&rdi1) - 1)) {
                rsi18 = *reinterpret_cast<void***>(rsi);
                rsp19 = reinterpret_cast<struct s0*>(rsp10 - 8);
                rsp19->f0 = 0x5d4b;
                quotearg_style(4, rsi18, 4, rsi18);
                rsp20 = reinterpret_cast<void**>(&rsp19->f8);
                addr_5c9e_4:
                rsp21 = reinterpret_cast<struct s0*>(rsp20 - 8);
                rsp21->f0 = 0x5ca5;
                fun_3800();
                rsp22 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp21->f8) - 8);
                rsp22->f0 = 0x5cb6;
                fun_3b80();
                rsp23 = reinterpret_cast<void**>(&rsp22->f8);
            } else {
                addr_5d19_6:
                rsp24 = reinterpret_cast<struct s0*>(rsp10 - 8);
                rsp24->f0 = 0x5d20;
                fun_3800();
                rsp25 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp24->f8) - 8);
                rsp25->f0 = 0x5d2e;
                fun_3b80();
                rsp23 = reinterpret_cast<void**>(&rsp25->f8);
            }
            rsp26 = reinterpret_cast<struct s0*>(rsp23 - 8);
            rsp26->f0 = 0x5cc0;
            usage();
            rsp10 = reinterpret_cast<void**>(&rsp26->f8);
            addr_5cc0_8:
            rsp27 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp27->f0 = 0x5cd1;
            quotearg_style(4, v12, 4, v12);
            rsp28 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp27->f8) - 8);
            rsp28->f0 = 0x5ce7;
            fun_3800();
            rsp29 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp28->f8) - 8);
            rsp29->f0 = 0x5cef;
            rax30 = fun_36d0();
            *reinterpret_cast<int32_t*>(&rdi1) = 1;
            rsi = *reinterpret_cast<void***>(rax30);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rsp31 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp29->f8) - 8);
            rsp31->f0 = 0x5d03;
            fun_3b80();
            rsp10 = reinterpret_cast<void**>(&rsp31->f8);
            addr_5d03_9:
            rsp32 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp32->f0 = 0x5d08;
            fun_3840();
            rsp10 = reinterpret_cast<void**>(&rsp32->f8);
        }
    }
    if (*reinterpret_cast<signed char*>(&rcx4)) {
        if (v12) {
            fun_3800();
            rsi = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            fun_3b80();
            rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8 - 8 + 8);
            goto addr_5c81_13;
        } else {
            if (v11 > 2) {
                addr_5c81_13:
                rsi33 = *reinterpret_cast<void***>(rsi + 16);
                quotearg_style(4, rsi33, 4, rsi33);
                rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
                goto addr_5c9e_4;
            } else {
                addr_5903_15:
                zf34 = parents_option == 0;
                r14_35 = *reinterpret_cast<void***>(r13_9);
                r12_36 = *reinterpret_cast<void***>(r13_9 + 8);
                if (!zf34) {
                    goto addr_5d19_6;
                } else {
                    ebx37 = v17;
                    if (*reinterpret_cast<unsigned char*>(v13 + 22) && (*reinterpret_cast<void***>(v13) && ((rax38 = fun_39e0(r14_35, r12_36, rdx, rcx4, r8), rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8), !*reinterpret_cast<int32_t*>(&rax38)) && !*reinterpret_cast<signed char*>(&ebx37)))) {
                        if (!1 || (rsi39 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff30), eax40 = fun_3a70(r12_36, rsi39, r12_36, rsi39), rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8), eax40 == 0)) {
                            if (!1) {
                                *reinterpret_cast<void***>(&rdx41) = *reinterpret_cast<void***>(v13);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx41) + 4) = 0;
                                rax42 = find_backup_file_name(0xffffff9c, r12_36, rdx41, rcx4);
                                rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
                                rdi43 = reinterpret_cast<void***>(0x1c100);
                                ecx44 = 22;
                                rsi45 = v13;
                                while (ecx44) {
                                    --ecx44;
                                    *rdi43 = *reinterpret_cast<void***>(rsi45);
                                    rdi43 = rdi43 + 4;
                                    rsi45 = rsi45 + 4;
                                }
                                r12_36 = rax42;
                                x_tmp_0 = 0;
                            }
                        }
                    }
                    rdi1 = r14_35;
                    rsi = r12_36;
                    eax46 = copy(rdi1, rsi, 0xffffff9c, r12_36);
                    r14d47 = eax46;
                    rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) - 8 - 8 + 8 + 8 + 8);
                }
            }
        }
    } else {
        if (!v12) {
            rax48 = v11;
            rbx49 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsi + rax48 * 8) - 8);
            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff30);
            r14_50 = rax48;
            rdi1 = rbx49;
            v12 = rbx49;
            eax51 = target_directory_operand();
            rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
            v52 = eax51;
            if (!(eax51 + 1)) {
                rax53 = fun_36d0();
                rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
                if (*reinterpret_cast<void***>(rax53) != 2) 
                    goto addr_5bef_29;
                while (1) {
                    v17 = 1;
                    addr_5bef_29:
                    if (v11 <= 2) 
                        goto addr_5903_15;
                    rax54 = quotearg_style(4, v12, 4, v12);
                    r13_9 = rax54;
                    rax55 = fun_3800();
                    rcx4 = r13_9;
                    rdx = rax55;
                    fun_3b80();
                    rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8 - 8 + 8 - 8 + 8);
                }
            }
            v11 = *reinterpret_cast<int32_t*>(&r14_50) - 1;
            if (!rbx49) 
                goto addr_5903_15;
            if (v11 > 1) 
                goto addr_5b67_34; else 
                goto addr_5bd0_35;
        } else {
            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff30);
            eax56 = target_directory_operand();
            rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
            v52 = eax56;
            if (!(eax56 + 1)) 
                goto addr_5cc0_8;
            if (v11 != 1) 
                goto addr_5b67_34; else 
                goto addr_576b_38;
        }
    }
    addr_58c3_39:
    rax57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v15) - reinterpret_cast<int64_t>(g28));
    if (rax57) 
        goto addr_5d03_9;
    return r14d47;
    addr_5b67_34:
    dest_info_init(v13, rsi);
    src_info_init(v13, rsi);
    rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8 - 8 + 8);
    goto addr_576b_38;
    addr_5bd0_35:
    r14d47 = 1;
    if (v11 == 1) {
        addr_576b_38:
        *reinterpret_cast<int32_t*>(&rbx58) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx58) + 4) = 0;
        r14d47 = 1;
    } else {
        goto addr_58c3_39;
    }
    while (1) {
        zf59 = remove_trailing_slashes == 0;
        r15_60 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<uint64_t>(rbx58 * 8));
        v61 = reinterpret_cast<void**>(0);
        if (zf59) {
            zf62 = parents_option == 0;
            rdi63 = r15_60;
            if (!zf62) 
                goto addr_59f8_44; else 
                goto addr_57b5_45;
        }
        rsp64 = reinterpret_cast<struct s0*>(rsp10 - 8);
        rsp64->f0 = 0x59e8;
        strip_trailing_slashes(r15_60, rsi, rdx, rcx4, r8, r9);
        rsp10 = reinterpret_cast<void**>(&rsp64->f8);
        zf65 = parents_option == 0;
        rdi63 = r15_60;
        if (zf65) {
            addr_57b5_45:
            rsp66 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp66->f0 = 0x57ba;
            rax67 = last_component(rdi63, rsi, rdx, rcx4, rdi63, rsi, rdx, rcx4);
            r12_68 = rax67;
            rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp66->f8) - 8);
            rsp69->f0 = 0x57c5;
            rax70 = fun_3820(rax67, rax67);
            rsp71 = reinterpret_cast<void**>(&rsp69->f8);
            r8_72 = rax70 + 1;
            rax73 = reinterpret_cast<void***>(rax70 + 24);
            rcx74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp71) - (reinterpret_cast<uint64_t>(rax73) & 0xfffffffffffff000));
            rdx75 = reinterpret_cast<uint64_t>(rax73) & 0xfffffffffffffff0;
            if (rsp71 != rcx74) {
                do {
                    rsp71 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp71) - reinterpret_cast<uint64_t>("_ctype_get_mb_cur_max"));
                    *reinterpret_cast<uint64_t*>(reinterpret_cast<unsigned char>(rsp71) + reinterpret_cast<uint64_t>("strcmp")) = 0x57c5;
                } while (rsp71 != rcx74);
            }
        } else {
            addr_59f8_44:
            rsp76 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp76->f0 = 0x59fd;
            rax77 = fun_3820(rdi63, rdi63);
            rsp78 = reinterpret_cast<void**>(&rsp76->f8);
            r8_79 = rax77 + 1;
            rax80 = reinterpret_cast<void***>(rax77 + 24);
            rcx81 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp78) - (reinterpret_cast<uint64_t>(rax80) & 0xfffffffffffff000));
            rdx82 = reinterpret_cast<uint64_t>(rax80) & 0xfffffffffffffff0;
            if (rsp78 != rcx81) {
                do {
                    rsp78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp78) - reinterpret_cast<uint64_t>("_ctype_get_mb_cur_max"));
                    *reinterpret_cast<uint64_t*>(reinterpret_cast<unsigned char>(rsp78) + reinterpret_cast<uint64_t>("strcmp")) = 0x59fd;
                } while (rsp78 != rcx81);
                goto addr_5a32_49;
            }
        }
        *reinterpret_cast<uint32_t*>(&rdx83) = *reinterpret_cast<uint32_t*>(&rdx75) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx83) + 4) = 0;
        rsp84 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp71) - reinterpret_cast<uint64_t>(rdx83));
        if (rdx83) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp84) + reinterpret_cast<uint64_t>(rdx83) + 0xfffffffffffffff8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp84) + reinterpret_cast<uint64_t>(rdx83) + 0xfffffffffffffff8);
        }
        rsp85 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp84) - 8);
        *rsp85 = 0x5822;
        rax86 = fun_3a80(reinterpret_cast<uint64_t>(rsp84) + 15 & 0xfffffffffffffff0, r12_68, r8_72);
        rsp87 = rsp85 + 1 - 1;
        *rsp87 = 0x582d;
        strip_trailing_slashes(rax86, r12_68, r8_72, rcx74, r8_72, r9);
        eax88 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax86) - 46);
        if (!eax88 && (eax88 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax86 + 1) - 46), !eax88)) {
            eax88 = *reinterpret_cast<unsigned char*>(rax86 + 2);
        }
        rsi89 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax86) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(eax88 < 1)));
        rsp90 = reinterpret_cast<struct s1*>(rsp87 + 1 - 1);
        rsp90->f0 = 0x5865;
        rax91 = file_name_concat(v12, rsi89, v12, rsi89);
        rsp92 = reinterpret_cast<void**>(&rsp90->f8);
        r12_93 = rax91;
        goto addr_5868_55;
        addr_5a32_49:
        *reinterpret_cast<uint32_t*>(&rdx94) = *reinterpret_cast<uint32_t*>(&rdx82) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx94) + 4) = 0;
        rsp95 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp78) - reinterpret_cast<uint64_t>(rdx94));
        if (rdx94) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp95) + reinterpret_cast<uint64_t>(rdx94) + 0xfffffffffffffff8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp95) + reinterpret_cast<uint64_t>(rdx94) + 0xfffffffffffffff8);
        }
        rsp96 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp95) - 8);
        *rsp96 = 0x5a5a;
        rax97 = fun_3a80(reinterpret_cast<uint64_t>(rsp95) + 15 & 0xfffffffffffffff0, r15_60, r8_79);
        rsp98 = rsp96 + 1 - 1;
        *rsp98 = 0x5a65;
        strip_trailing_slashes(rax97, r15_60, r8_79, rcx81, r8_79, r9);
        rsp99 = rsp98 + 1 - 1;
        *rsp99 = 0x5a7b;
        rax100 = file_name_concat(v12, rax97, v12, rax97);
        *reinterpret_cast<int32_t*>(&rcx4) = 0;
        *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
        r12_93 = rax100;
        zf101 = *reinterpret_cast<unsigned char*>(v13 + 60) == 0;
        rsp102 = reinterpret_cast<void***>(rsp99 + 1 - 1);
        *rsp102 = v12;
        if (!zf101) {
            rcx4 = reinterpret_cast<void**>("%s -> %s\n");
        }
        rsp103 = rsp102 - 8;
        *rsp103 = v13;
        rsi = reinterpret_cast<void**>(-static_cast<int64_t>(reinterpret_cast<unsigned char>(r12_93)));
        rsp104 = reinterpret_cast<struct s2*>(rsp103 - 8);
        rsp104->f0 = 0x5abe;
        al105 = make_dir_parents_private(r12_93, rsi, v52, rcx4, reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff20, reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff1e, rsp104->f8);
        rsp106 = reinterpret_cast<struct s3*>(&rsp104->f8);
        rdx = reinterpret_cast<void**>(0);
        r8 = rsp106->f0;
        rsp107 = reinterpret_cast<struct s4*>(&rsp106->f8);
        r9 = rsp107->f0;
        rsp92 = reinterpret_cast<void**>(&rsp107->f8);
        zf108 = reinterpret_cast<int1_t>(free == 47);
        if (zf108) {
            rdx = reinterpret_cast<void**>(1);
            do {
                v61 = rdx;
                ++rdx;
            } while (*reinterpret_cast<void***>(rdx + 0xffffffffffffffff) == 47);
        }
        if (al105) {
            addr_5868_55:
            rsp109 = reinterpret_cast<struct s0*>(rsp92 - 8);
            rsp109->f0 = 0;
            r9 = v13;
            rsp110 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp109) - 8);
            *rsp110 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp8) - 0xe1);
            *reinterpret_cast<uint32_t*>(&r8) = 0;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            rdx = v52;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rsp111 = reinterpret_cast<struct s5*>(rsp110 - 1);
            rsp111->f0 = 0x5899;
            eax112 = copy(r15_60, r12_93, rdx, v61, r15_60, r12_93, rdx, v61);
            rsp113 = reinterpret_cast<struct s6*>(&rsp111->f8);
            rcx4 = rsp113->f0;
            rsp114 = reinterpret_cast<struct s7*>(&rsp113->f8);
            rsi = rsp114->f0;
            rsp92 = reinterpret_cast<void**>(&rsp114->f8);
            r14d47 = r14d47 & eax112;
            zf115 = parents_option == 0;
            if (!zf115) {
                r8 = v13;
                rcx4 = v116;
                rdx = v61;
                rsi = v52;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                rsp117 = reinterpret_cast<struct s0*>(rsp92 - 8);
                rsp117->f0 = 0x5b5b;
                eax118 = re_protect(r12_93, rsi, rdx, rcx4, r8, r9);
                rsp92 = reinterpret_cast<void**>(&rsp117->f8);
                r14d47 = r14d47 & eax118;
                eax119 = parents_option;
            } else {
                addr_58ab_64:
                rdi1 = r12_93;
                ++rbx58;
                rsp120 = reinterpret_cast<struct s0*>(rsp92 - 8);
                rsp120->f0 = 0x58b7;
                fun_3670(rdi1, rdi1);
                rsp10 = reinterpret_cast<void**>(&rsp120->f8);
                if (v11 > *reinterpret_cast<int32_t*>(&rbx58)) 
                    continue; else 
                    goto addr_58c3_39;
            }
        } else {
            eax119 = parents_option;
            r14d47 = 0;
        }
        if (*reinterpret_cast<signed char*>(&eax119) && (r15_121 = v116, !!r15_121)) {
            do {
                rdi122 = r15_121;
                r15_121 = *reinterpret_cast<void***>(r15_121 + 0xa0);
                v116 = r15_121;
                rsp123 = reinterpret_cast<struct s0*>(rsp92 - 8);
                rsp123->f0 = 0x5b2e;
                fun_3670(rdi122, rdi122);
                rsp92 = reinterpret_cast<void**>(&rsp123->f8);
            } while (r15_121);
            goto addr_58ab_64;
        }
    }
}