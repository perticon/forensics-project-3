bool re_protect(char * const_dst_name, int32_t dst_dirfd, char * dst_relname, int32_t * attr_list, int32_t * x) {
    // 0x4e20
    int64_t v1; // bp-120, 0x4e20
    int64_t v2 = &v1; // 0x4e39
    int64_t v3 = __readfsqword(40); // 0x4e41
    int64_t v4 = function_3820() + 24 & -0x1000; // 0x4e63
    int64_t v5 = v2; // 0x4e73
    if (v4 != 0) {
        v5 -= (int64_t)"_ctype_get_mb_cur_max";
        while (v5 != v2 - v4) {
            // 0x4e75
            v5 -= (int64_t)"_ctype_get_mb_cur_max";
        }
    }
    int64_t v6 = function_3a80(); // 0x4eab
    int64_t v7 = 1; // 0x4ec1
    char v8; // 0x4e20
    int64_t v9; // 0x4e20
    int64_t v10; // 0x4e20
    int64_t v11; // 0x4e20
    char * v12; // 0x4e20
    char * v13; // 0x4e20
    int64_t * v14; // 0x4f1a
    if (attr_list != NULL) {
        int64_t v15 = (int64_t)x;
        int64_t v16; // bp-104, 0x4e20
        v1 = &v16;
        v12 = (char *)(v15 + 30);
        v13 = (char *)((int64_t)dst_relname - (int64_t)const_dst_name + v6);
        v9 = (int64_t)attr_list;
        while (true) {
          lab_0x4f1a:
            // 0x4f1a
            v10 = v9;
            v14 = (int64_t *)(v10 + 152);
            *(char *)(*v14 + v6) = 0;
            if (*(char *)(v15 + 31) != 0) {
                // 0x4f2d
                v16 = *(int64_t *)(v10 + 72);
                if ((int32_t)function_36b0() != 0) {
                    // break -> 0x4f66
                    break;
                }
            }
            // 0x4ed8
            if (*(char *)(v15 + 29) != 0) {
                // 0x4fb0
                if ((int32_t)function_3bf0() == 0) {
                    goto lab_0x4ee3;
                } else {
                    int64_t v17 = chown_failure_ok(); // 0x4fd2
                    if ((char)v17 == 0) {
                        // 0x50da
                        quotearg_style();
                        v11 = 0x100000000000000 * v17 >> 56;
                        goto lab_0x507b;
                    }
                    // 0x4fdf
                    function_3bf0();
                    v8 = 0;
                    if (*v12 == 0) {
                        goto lab_0x4ef1;
                    } else {
                        goto lab_0x5010;
                    }
                }
            } else {
                goto lab_0x4ee3;
            }
        }
        // 0x4f66
        quotearg_style();
        function_3800();
        function_36d0();
        function_3b80();
        v7 = 0;
    }
    goto lab_0x50ae_2;
  lab_0x50ae_2:
    // 0x50ae
    if (v3 != __readfsqword(40)) {
        // 0x50fb
        return function_3840() % 2 != 0;
    }
    // 0x50bd
    return v7 != 0;
  lab_0x4ee3:;
    char v18 = *v12; // 0x4ee3
    v8 = v18;
    if (v18 != 0) {
        goto lab_0x5010;
    } else {
        goto lab_0x4ef1;
    }
  lab_0x5010:
    // 0x5010
    v7 = 0;
    if (copy_acl(v13, -1, (char *)v6, -1, *(int32_t *)(v10 + 24)) != 0) {
        goto lab_0x50ae_2;
    }
    goto lab_0x4efe;
  lab_0x4ef1:
    // 0x4ef1
    if (*(char *)(v10 + 144) != 0) {
        // 0x5040
        if ((int32_t)function_3ad0() != 0) {
            // 0x505f
            quotearg_style();
            v11 = v8;
            goto lab_0x507b;
        }
    }
    goto lab_0x4efe;
  lab_0x4efe:
    // 0x4efe
    *(char *)(*v14 + v6) = 47;
    int64_t v19 = *(int64_t *)(v10 + 160); // 0x4f0a
    v9 = v19;
    if (v19 == 0) {
        goto lab_0x50ae_2;
    }
    goto lab_0x4f1a;
  lab_0x507b:
    // 0x507b
    function_3800();
    function_36d0();
    function_3b80();
    v7 = v11 % 2;
    goto lab_0x50ae_2;
}