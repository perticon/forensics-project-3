ulong re_protect(char *param_1,int param_2,ulong param_3,long param_4,long param_5)

{
  __gid_t _Var1;
  __uid_t __owner;
  __mode_t __mode;
  long lVar2;
  timespec **pptVar3;
  char cVar4;
  int iVar5;
  size_t sVar6;
  void *pvVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  int *piVar10;
  timespec **pptVar11;
  char *pcVar13;
  ulong uVar14;
  long in_FS_OFFSET;
  timespec *local_78;
  ulong local_70;
  timespec local_68;
  undefined8 local_58;
  undefined8 local_50;
  long local_40;
  timespec **pptVar12;
  
  pptVar11 = &local_78;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_70 = param_3;
  sVar6 = strlen(param_1);
  pptVar12 = &local_78;
  pptVar3 = &local_78;
  while (pptVar12 != (timespec **)((long)&local_78 - (sVar6 + 0x18 & 0xfffffffffffff000))) {
    pptVar11 = (timespec **)((long)pptVar3 + -0x1000);
    *(undefined8 *)((long)pptVar3 + -8) = *(undefined8 *)((long)pptVar3 + -8);
    pptVar12 = (timespec **)((long)pptVar3 + -0x1000);
    pptVar3 = (timespec **)((long)pptVar3 + -0x1000);
  }
  uVar14 = (ulong)((uint)(sVar6 + 0x18) & 0xff0);
  lVar2 = -uVar14;
  if (uVar14 != 0) {
    *(undefined8 *)((long)pptVar11 + -8) = *(undefined8 *)((long)pptVar11 + -8);
  }
  *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104eb0;
  pvVar7 = memcpy((void *)((ulong)((long)pptVar11 + lVar2 + 0xf) & 0xfffffffffffffff0),param_1,
                  sVar6 + 1);
  pcVar13 = (char *)((long)pvVar7 + (local_70 - (long)param_1));
  if (param_4 != 0) {
    local_78 = &local_68;
    do {
      cVar4 = *(char *)(param_5 + 0x1f);
      *(undefined *)((long)pvVar7 + *(long *)(param_4 + 0x98)) = 0;
      if (cVar4 != '\0') {
        local_68.tv_sec = *(__time_t *)(param_4 + 0x48);
        local_68.tv_nsec = *(long *)(param_4 + 0x50);
        local_58 = *(undefined8 *)(param_4 + 0x58);
        local_50 = *(undefined8 *)(param_4 + 0x60);
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104f5e;
        iVar5 = utimensat(param_2,pcVar13,local_78,0);
        if (iVar5 != 0) {
          *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104f73;
          uVar8 = quotearg_style(4,pvVar7);
          *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104f89;
          uVar9 = dcgettext(0,"failed to preserve times for %s",5);
          *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104f91;
          piVar10 = __errno_location();
          iVar5 = *piVar10;
          *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104fa2;
          error(0,iVar5,uVar9,uVar8);
          uVar14 = 0;
          goto LAB_001050ae;
        }
      }
      if (*(char *)(param_5 + 0x1d) != '\0') {
        _Var1 = *(__gid_t *)(param_4 + 0x20);
        __owner = *(__uid_t *)(param_4 + 0x1c);
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104fc7;
        iVar5 = fchownat(param_2,pcVar13,__owner,_Var1,0x100);
        if (iVar5 == 0) goto LAB_00104ee3;
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104fd7;
        cVar4 = chown_failure_ok();
        if (cVar4 != '\0') {
          _Var1 = *(__gid_t *)(param_4 + 0x20);
          *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x104ff8;
          fchownat(param_2,pcVar13,0xffffffff,_Var1,0x100);
          cVar4 = *(char *)(param_5 + 0x1e);
          goto joined_r0x00105000;
        }
        local_70 = local_70 & 0xffffffffffffff00;
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x1050ea;
        uVar8 = quotearg_style(4,pvVar7);
        pcVar13 = "failed to preserve ownership for %s";
LAB_0010507b:
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x105082;
        uVar9 = dcgettext(0,pcVar13,5);
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x10508a;
        piVar10 = __errno_location();
        iVar5 = *piVar10;
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x10509b;
        error(0,iVar5,uVar9,uVar8);
        uVar14 = local_70 & 0xff;
        goto LAB_001050ae;
      }
LAB_00104ee3:
      cVar4 = *(char *)(param_5 + 0x1e);
joined_r0x00105000:
      if (cVar4 == '\0') {
        if (*(char *)(param_4 + 0x90) != '\0') {
          __mode = *(__mode_t *)(param_4 + 0x18);
          local_70 = local_70 & 0xffffffffffffff00;
          *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x105057;
          iVar5 = fchmodat(param_2,pcVar13,__mode,0x100);
          if (iVar5 != 0) {
            *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x10506c;
            uVar8 = quotearg_style(4,pvVar7);
            pcVar13 = "failed to preserve permissions for %s";
            goto LAB_0010507b;
          }
        }
      }
      else {
        *(undefined8 *)((long)pptVar11 + lVar2 + -8) = 0x105029;
        iVar5 = copy_acl(pcVar13,0xffffffff,pvVar7);
        if (iVar5 != 0) {
          uVar14 = 0;
          goto LAB_001050ae;
        }
      }
      *(undefined *)((long)pvVar7 + *(long *)(param_4 + 0x98)) = 0x2f;
      param_4 = *(long *)(param_4 + 0xa0);
    } while (param_4 != 0);
  }
  uVar14 = 1;
LAB_001050ae:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    *(code **)((long)pptVar11 + lVar2 + -8) = usage;
    __stack_chk_fail();
  }
  return uVar14;
}