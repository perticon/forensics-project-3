uint32_t re_protect(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void* rbp8;
    void** r15_9;
    void** r14d10;
    void** r13_11;
    void** rbx12;
    void** v13;
    void* rax14;
    void* v15;
    void** rax16;
    void* rsp17;
    void** r8_18;
    void*** rax19;
    void* rcx20;
    uint64_t rdx21;
    void* rdx22;
    void* rsp23;
    int64_t* rsp24;
    void** rax25;
    void* rsp26;
    void** r12_27;
    void** r13_28;
    uint32_t r8d29;
    void*** v30;
    int1_t zf31;
    int64_t rdi32;
    int64_t* rsp33;
    int64_t rax34;
    void** rcx35;
    int64_t rdx36;
    int64_t rdi37;
    int64_t* rsp38;
    int32_t eax39;
    uint32_t r8d40;
    void** r8_41;
    int64_t* rsp42;
    int32_t eax43;
    int64_t rdi44;
    unsigned char v45;
    int64_t* rsp46;
    int32_t eax47;
    int64_t* rsp48;
    void** eax49;
    void* rsp50;
    void** rcx51;
    int64_t rdi52;
    int64_t* rsp53;
    void* rax54;
    int64_t* rsp55;
    int64_t* rsp56;
    int64_t* rsp57;
    int64_t* rsp58;
    int64_t* rsp59;
    void* rsp60;
    int64_t* rsp61;
    int64_t* rsp62;
    int64_t* rsp63;
    int64_t* rsp64;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp8 = rsp7;
    r15_9 = r8;
    r14d10 = esi;
    r13_11 = rdi;
    rbx12 = rcx;
    v13 = rdx;
    rax14 = g28;
    v15 = rax14;
    rax16 = fun_3820(rdi);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 - 8 - 8 - 72 - 8 + 8);
    r8_18 = rax16 + 1;
    rax19 = reinterpret_cast<void***>(rax16 + 24);
    rcx20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - (reinterpret_cast<uint64_t>(rax19) & 0xfffffffffffff000));
    rdx21 = reinterpret_cast<uint64_t>(rax19) & 0xfffffffffffffff0;
    if (rsp17 != rcx20) {
        do {
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
        } while (rsp17 != rcx20);
    }
    *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx21) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - reinterpret_cast<int64_t>(rdx22));
    if (rdx22) {
        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp23) + reinterpret_cast<int64_t>(rdx22) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp23) + reinterpret_cast<int64_t>(rdx22) - 8);
    }
    rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp23) - 8);
    *rsp24 = 0x4eb0;
    rax25 = fun_3a80(reinterpret_cast<uint64_t>(rsp23) + 15 & 0xfffffffffffffff0, r13_11, r8_18);
    rsp26 = reinterpret_cast<void*>(rsp24 + 1);
    r12_27 = rax25;
    r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_27) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(r13_11)));
    if (!rbx12) {
        addr_50a8_6:
        r8d29 = 1;
    } else {
        v30 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbp8) - 96);
        while ((zf31 = *reinterpret_cast<signed char*>(r15_9 + 31) == 0, *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_27) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx12 + 0x98))) = 0, zf31) || (*reinterpret_cast<void***>(&rdi32) = r14d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0, rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8), *rsp33 = 0x4f5e, rax34 = fun_36b0(rdi32, r13_28, v30, rdi32, r13_28, v30), rsp26 = reinterpret_cast<void*>(rsp33 + 1), *reinterpret_cast<int32_t*>(&rax34) == 0)) {
            if (!*reinterpret_cast<unsigned char*>(r15_9 + 29) || (rcx35 = *reinterpret_cast<void***>(rbx12 + 32), *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx36) = *reinterpret_cast<unsigned char*>(rbx12 + 28), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx36) + 4) = 0, *reinterpret_cast<void***>(&rdi37) = r14d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0, rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8), *rsp38 = 0x4fc7, eax39 = fun_3bf0(rdi37, r13_28, rdx36, rcx35, 0x100), rsp26 = reinterpret_cast<void*>(rsp38 + 1), eax39 == 0)) {
                r8d40 = *reinterpret_cast<unsigned char*>(r15_9 + 30);
                if (*reinterpret_cast<unsigned char*>(&r8d40)) {
                    addr_5010_11:
                    r8_41 = *reinterpret_cast<void***>(rbx12 + 24);
                    *reinterpret_cast<int32_t*>(&r8_41 + 4) = 0;
                    rsp42 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
                    *rsp42 = 0x5029;
                    eax43 = copy_acl(r13_28, 0xffffffff, r12_27, 0xffffffff, r8_41, r9);
                    rsp26 = reinterpret_cast<void*>(rsp42 + 1);
                    if (eax43) 
                        goto addr_5031_12;
                } else {
                    addr_4ef1_13:
                    if (!*reinterpret_cast<void***>(rbx12 + 0x90)) 
                        goto addr_4efe_14;
                    *reinterpret_cast<void***>(&rdi44) = r14d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi44) + 4) = 0;
                    v45 = *reinterpret_cast<unsigned char*>(&r8d40);
                    rsp46 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
                    *rsp46 = 0x5057;
                    eax47 = fun_3ad0(rdi44, r13_28, rdi44, r13_28);
                    rsp26 = reinterpret_cast<void*>(rsp46 + 1);
                    if (eax47) 
                        goto addr_505f_16;
                }
                addr_4efe_14:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_27) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx12 + 0x98))) = 47;
                rbx12 = *reinterpret_cast<void***>(rbx12 + 0xa0);
                if (!rbx12) 
                    goto addr_50a8_6;
            } else {
                rsp48 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
                *rsp48 = 0x4fd7;
                eax49 = chown_failure_ok(r15_9, r13_28, rdx36, rcx35, 0x100);
                rsp50 = reinterpret_cast<void*>(rsp48 + 1);
                if (!*reinterpret_cast<unsigned char*>(&eax49)) 
                    goto addr_50da_18;
                rcx51 = *reinterpret_cast<void***>(rbx12 + 32);
                *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
                *reinterpret_cast<void***>(&rdi52) = r14d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi52) + 4) = 0;
                rsp53 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp50) - 8);
                *rsp53 = 0x4ff8;
                fun_3bf0(rdi52, r13_28, 0xffffffff, rcx51, 0x100);
                rsp26 = reinterpret_cast<void*>(rsp53 + 1);
                r8d40 = *reinterpret_cast<unsigned char*>(r15_9 + 30);
                if (!*reinterpret_cast<unsigned char*>(&r8d40)) 
                    goto addr_4ef1_13;
                goto addr_5010_11;
            }
        }
        goto addr_4f66_21;
    }
    addr_50ae_22:
    rax54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v15) - reinterpret_cast<int64_t>(g28));
    if (rax54) {
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8) = reinterpret_cast<int64_t>(usage);
        fun_3840();
    } else {
        return r8d29;
    }
    addr_4f66_21:
    rsp55 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
    *rsp55 = 0x4f73;
    quotearg_style(4, r12_27, 4, r12_27);
    rsp56 = rsp55 + 1 - 1;
    *rsp56 = 0x4f89;
    fun_3800();
    rsp57 = rsp56 + 1 - 1;
    *rsp57 = 0x4f91;
    fun_36d0();
    rsp58 = rsp57 + 1 - 1;
    *rsp58 = 0x4fa2;
    fun_3b80();
    rsp26 = reinterpret_cast<void*>(rsp58 + 1);
    r8d29 = 0;
    goto addr_50ae_22;
    addr_5031_12:
    r8d29 = 0;
    goto addr_50ae_22;
    addr_505f_16:
    rsp59 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
    *rsp59 = 0x506c;
    quotearg_style(4, r12_27, 4, r12_27);
    rsp60 = reinterpret_cast<void*>(rsp59 + 1);
    addr_507b_26:
    rsp61 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp60) - 8);
    *rsp61 = 0x5082;
    fun_3800();
    rsp62 = rsp61 + 1 - 1;
    *rsp62 = 0x508a;
    fun_36d0();
    rsp63 = rsp62 + 1 - 1;
    *rsp63 = 0x509b;
    fun_3b80();
    rsp26 = reinterpret_cast<void*>(rsp63 + 1);
    r8d29 = v45;
    goto addr_50ae_22;
    addr_50da_18:
    v45 = *reinterpret_cast<unsigned char*>(&eax49);
    rsp64 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp50) - 8);
    *rsp64 = 0x50ea;
    quotearg_style(4, r12_27, 4, r12_27);
    rsp60 = reinterpret_cast<void*>(rsp64 + 1);
    goto addr_507b_26;
}