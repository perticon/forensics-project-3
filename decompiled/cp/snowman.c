
void* g28;

struct s0 {
    int64_t f0;
    void** f8;
};

void** quotearg_style(int64_t rdi, void** rsi, ...);

void** fun_3800();

void fun_3b80();

void usage();

void** fun_36d0();

void fun_3840();

unsigned char parents_option = 0;

int64_t fun_39e0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

int32_t fun_3a70(void** rdi, void** rsi, ...);

void** find_backup_file_name(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

/* x_tmp.0 */
int32_t x_tmp_0 = 0;

uint32_t copy(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** target_directory_operand();

void dest_info_init(void** rdi, void** rsi);

void src_info_init(void** rdi, void** rsi);

signed char remove_trailing_slashes = 0;

void strip_trailing_slashes(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** last_component(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** fun_3820(void** rdi, ...);

void** fun_3a80(void** rdi, void** rsi, void** rdx, ...);

struct s1 {
    int64_t f0;
    void** f8;
};

void** file_name_concat(void** rdi, void** rsi, ...);

struct s2 {
    int64_t f0;
    void** f8;
};

signed char make_dir_parents_private(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9, void** a7);

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s4 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** free;

struct s5 {
    int64_t f0;
    void** f8;
};

struct s6 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s7 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

uint32_t re_protect(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9);

void fun_3670(void** rdi, ...);

uint32_t do_copy(int32_t edi, void** rsi, void** rdx, signed char cl, void** r8, void** r9) {
    void** rdi1;
    void** rcx4;
    void* rsp7;
    void* rbp8;
    void** r13_9;
    void** rsp10;
    int32_t v11;
    void** v12;
    void** v13;
    void* rax14;
    void* v15;
    int32_t eax16;
    unsigned char v17;
    void** rsi18;
    struct s0* rsp19;
    void** rsp20;
    struct s0* rsp21;
    struct s0* rsp22;
    void** rsp23;
    struct s0* rsp24;
    struct s0* rsp25;
    struct s0* rsp26;
    struct s0* rsp27;
    struct s0* rsp28;
    struct s0* rsp29;
    void** rax30;
    struct s0* rsp31;
    struct s0* rsp32;
    void** rsi33;
    int1_t zf34;
    void** r14_35;
    void** r12_36;
    uint32_t ebx37;
    int64_t rax38;
    void** rsi39;
    int32_t eax40;
    int64_t rdx41;
    void** rax42;
    void*** rdi43;
    int32_t ecx44;
    void** rsi45;
    uint32_t eax46;
    uint32_t r14d47;
    int64_t rax48;
    void** rbx49;
    int64_t r14_50;
    void** eax51;
    void** v52;
    void** rax53;
    void** rax54;
    void** rax55;
    void** eax56;
    void* rax57;
    int64_t rbx58;
    int1_t zf59;
    void** r15_60;
    void** v61;
    int1_t zf62;
    void** rdi63;
    struct s0* rsp64;
    int1_t zf65;
    struct s0* rsp66;
    void** rax67;
    void** r12_68;
    struct s0* rsp69;
    void** rax70;
    void** rsp71;
    void** r8_72;
    void*** rax73;
    void** rcx74;
    uint64_t rdx75;
    struct s0* rsp76;
    void** rax77;
    void** rsp78;
    void** r8_79;
    void*** rax80;
    void** rcx81;
    uint64_t rdx82;
    void* rdx83;
    void* rsp84;
    int64_t* rsp85;
    void** rax86;
    int64_t* rsp87;
    uint32_t eax88;
    void** rsi89;
    struct s1* rsp90;
    void** rax91;
    void** rsp92;
    void** r12_93;
    void* rdx94;
    void* rsp95;
    int64_t* rsp96;
    void** rax97;
    int64_t* rsp98;
    int64_t* rsp99;
    void** rax100;
    int1_t zf101;
    void*** rsp102;
    void*** rsp103;
    struct s2* rsp104;
    signed char al105;
    struct s3* rsp106;
    struct s4* rsp107;
    int1_t zf108;
    struct s0* rsp109;
    void** rsp110;
    struct s5* rsp111;
    uint32_t eax112;
    struct s6* rsp113;
    struct s7* rsp114;
    int1_t zf115;
    void** v116;
    struct s0* rsp117;
    uint32_t eax118;
    uint32_t eax119;
    struct s0* rsp120;
    void** r15_121;
    void** rdi122;
    struct s0* rsp123;

    *reinterpret_cast<int32_t*>(&rdi1) = edi;
    *reinterpret_cast<signed char*>(&rcx4) = cl;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp8 = rsp7;
    r13_9 = rsi;
    rsp10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 - 8 - 8 - 0xe8);
    v11 = *reinterpret_cast<int32_t*>(&rdi1);
    v12 = rdx;
    v13 = r8;
    rax14 = g28;
    v15 = rax14;
    eax16 = 0;
    v17 = 0;
    *reinterpret_cast<unsigned char*>(&eax16) = reinterpret_cast<uint1_t>(rdx == 0);
    if (eax16 >= *reinterpret_cast<int32_t*>(&rdi1)) {
        while (1) {
            if (!(*reinterpret_cast<int32_t*>(&rdi1) - 1)) {
                rsi18 = *reinterpret_cast<void***>(rsi);
                rsp19 = reinterpret_cast<struct s0*>(rsp10 - 8);
                rsp19->f0 = 0x5d4b;
                quotearg_style(4, rsi18, 4, rsi18);
                rsp20 = reinterpret_cast<void**>(&rsp19->f8);
                addr_5c9e_4:
                rsp21 = reinterpret_cast<struct s0*>(rsp20 - 8);
                rsp21->f0 = 0x5ca5;
                fun_3800();
                rsp22 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp21->f8) - 8);
                rsp22->f0 = 0x5cb6;
                fun_3b80();
                rsp23 = reinterpret_cast<void**>(&rsp22->f8);
            } else {
                addr_5d19_6:
                rsp24 = reinterpret_cast<struct s0*>(rsp10 - 8);
                rsp24->f0 = 0x5d20;
                fun_3800();
                rsp25 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp24->f8) - 8);
                rsp25->f0 = 0x5d2e;
                fun_3b80();
                rsp23 = reinterpret_cast<void**>(&rsp25->f8);
            }
            rsp26 = reinterpret_cast<struct s0*>(rsp23 - 8);
            rsp26->f0 = 0x5cc0;
            usage();
            rsp10 = reinterpret_cast<void**>(&rsp26->f8);
            addr_5cc0_8:
            rsp27 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp27->f0 = 0x5cd1;
            quotearg_style(4, v12, 4, v12);
            rsp28 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp27->f8) - 8);
            rsp28->f0 = 0x5ce7;
            fun_3800();
            rsp29 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp28->f8) - 8);
            rsp29->f0 = 0x5cef;
            rax30 = fun_36d0();
            *reinterpret_cast<int32_t*>(&rdi1) = 1;
            rsi = *reinterpret_cast<void***>(rax30);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rsp31 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp29->f8) - 8);
            rsp31->f0 = 0x5d03;
            fun_3b80();
            rsp10 = reinterpret_cast<void**>(&rsp31->f8);
            addr_5d03_9:
            rsp32 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp32->f0 = 0x5d08;
            fun_3840();
            rsp10 = reinterpret_cast<void**>(&rsp32->f8);
        }
    }
    if (*reinterpret_cast<signed char*>(&rcx4)) {
        if (v12) {
            fun_3800();
            rsi = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            fun_3b80();
            rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8 - 8 + 8);
            goto addr_5c81_13;
        } else {
            if (v11 > 2) {
                addr_5c81_13:
                rsi33 = *reinterpret_cast<void***>(rsi + 16);
                quotearg_style(4, rsi33, 4, rsi33);
                rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
                goto addr_5c9e_4;
            } else {
                addr_5903_15:
                zf34 = parents_option == 0;
                r14_35 = *reinterpret_cast<void***>(r13_9);
                r12_36 = *reinterpret_cast<void***>(r13_9 + 8);
                if (!zf34) {
                    goto addr_5d19_6;
                } else {
                    ebx37 = v17;
                    if (*reinterpret_cast<unsigned char*>(v13 + 22) && (*reinterpret_cast<void***>(v13) && ((rax38 = fun_39e0(r14_35, r12_36, rdx, rcx4, r8), rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8), !*reinterpret_cast<int32_t*>(&rax38)) && !*reinterpret_cast<signed char*>(&ebx37)))) {
                        if (!1 || (rsi39 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff30), eax40 = fun_3a70(r12_36, rsi39, r12_36, rsi39), rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8), eax40 == 0)) {
                            if (!1) {
                                *reinterpret_cast<void***>(&rdx41) = *reinterpret_cast<void***>(v13);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx41) + 4) = 0;
                                rax42 = find_backup_file_name(0xffffff9c, r12_36, rdx41, rcx4);
                                rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
                                rdi43 = reinterpret_cast<void***>(0x1c100);
                                ecx44 = 22;
                                rsi45 = v13;
                                while (ecx44) {
                                    --ecx44;
                                    *rdi43 = *reinterpret_cast<void***>(rsi45);
                                    rdi43 = rdi43 + 4;
                                    rsi45 = rsi45 + 4;
                                }
                                r12_36 = rax42;
                                x_tmp_0 = 0;
                            }
                        }
                    }
                    rdi1 = r14_35;
                    rsi = r12_36;
                    eax46 = copy(rdi1, rsi, 0xffffff9c, r12_36);
                    r14d47 = eax46;
                    rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) - 8 - 8 + 8 + 8 + 8);
                }
            }
        }
    } else {
        if (!v12) {
            rax48 = v11;
            rbx49 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsi + rax48 * 8) - 8);
            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff30);
            r14_50 = rax48;
            rdi1 = rbx49;
            v12 = rbx49;
            eax51 = target_directory_operand();
            rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
            v52 = eax51;
            if (!(eax51 + 1)) {
                rax53 = fun_36d0();
                rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
                if (*reinterpret_cast<void***>(rax53) != 2) 
                    goto addr_5bef_29;
                while (1) {
                    v17 = 1;
                    addr_5bef_29:
                    if (v11 <= 2) 
                        goto addr_5903_15;
                    rax54 = quotearg_style(4, v12, 4, v12);
                    r13_9 = rax54;
                    rax55 = fun_3800();
                    rcx4 = r13_9;
                    rdx = rax55;
                    fun_3b80();
                    rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8 - 8 + 8 - 8 + 8);
                }
            }
            v11 = *reinterpret_cast<int32_t*>(&r14_50) - 1;
            if (!rbx49) 
                goto addr_5903_15;
            if (v11 > 1) 
                goto addr_5b67_34; else 
                goto addr_5bd0_35;
        } else {
            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff30);
            eax56 = target_directory_operand();
            rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8);
            v52 = eax56;
            if (!(eax56 + 1)) 
                goto addr_5cc0_8;
            if (v11 != 1) 
                goto addr_5b67_34; else 
                goto addr_576b_38;
        }
    }
    addr_58c3_39:
    rax57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v15) - reinterpret_cast<int64_t>(g28));
    if (rax57) 
        goto addr_5d03_9;
    return r14d47;
    addr_5b67_34:
    dest_info_init(v13, rsi);
    src_info_init(v13, rsi);
    rsp10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10 - 8) + 8 - 8 + 8);
    goto addr_576b_38;
    addr_5bd0_35:
    r14d47 = 1;
    if (v11 == 1) {
        addr_576b_38:
        *reinterpret_cast<int32_t*>(&rbx58) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx58) + 4) = 0;
        r14d47 = 1;
    } else {
        goto addr_58c3_39;
    }
    while (1) {
        zf59 = remove_trailing_slashes == 0;
        r15_60 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<uint64_t>(rbx58 * 8));
        v61 = reinterpret_cast<void**>(0);
        if (zf59) {
            zf62 = parents_option == 0;
            rdi63 = r15_60;
            if (!zf62) 
                goto addr_59f8_44; else 
                goto addr_57b5_45;
        }
        rsp64 = reinterpret_cast<struct s0*>(rsp10 - 8);
        rsp64->f0 = 0x59e8;
        strip_trailing_slashes(r15_60, rsi, rdx, rcx4, r8, r9);
        rsp10 = reinterpret_cast<void**>(&rsp64->f8);
        zf65 = parents_option == 0;
        rdi63 = r15_60;
        if (zf65) {
            addr_57b5_45:
            rsp66 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp66->f0 = 0x57ba;
            rax67 = last_component(rdi63, rsi, rdx, rcx4, rdi63, rsi, rdx, rcx4);
            r12_68 = rax67;
            rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(&rsp66->f8) - 8);
            rsp69->f0 = 0x57c5;
            rax70 = fun_3820(rax67, rax67);
            rsp71 = reinterpret_cast<void**>(&rsp69->f8);
            r8_72 = rax70 + 1;
            rax73 = reinterpret_cast<void***>(rax70 + 24);
            rcx74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp71) - (reinterpret_cast<uint64_t>(rax73) & 0xfffffffffffff000));
            rdx75 = reinterpret_cast<uint64_t>(rax73) & 0xfffffffffffffff0;
            if (rsp71 != rcx74) {
                do {
                    rsp71 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp71) - reinterpret_cast<uint64_t>("_ctype_get_mb_cur_max"));
                    *reinterpret_cast<uint64_t*>(reinterpret_cast<unsigned char>(rsp71) + reinterpret_cast<uint64_t>("strcmp")) = 0x57c5;
                } while (rsp71 != rcx74);
            }
        } else {
            addr_59f8_44:
            rsp76 = reinterpret_cast<struct s0*>(rsp10 - 8);
            rsp76->f0 = 0x59fd;
            rax77 = fun_3820(rdi63, rdi63);
            rsp78 = reinterpret_cast<void**>(&rsp76->f8);
            r8_79 = rax77 + 1;
            rax80 = reinterpret_cast<void***>(rax77 + 24);
            rcx81 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp78) - (reinterpret_cast<uint64_t>(rax80) & 0xfffffffffffff000));
            rdx82 = reinterpret_cast<uint64_t>(rax80) & 0xfffffffffffffff0;
            if (rsp78 != rcx81) {
                do {
                    rsp78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp78) - reinterpret_cast<uint64_t>("_ctype_get_mb_cur_max"));
                    *reinterpret_cast<uint64_t*>(reinterpret_cast<unsigned char>(rsp78) + reinterpret_cast<uint64_t>("strcmp")) = 0x59fd;
                } while (rsp78 != rcx81);
                goto addr_5a32_49;
            }
        }
        *reinterpret_cast<uint32_t*>(&rdx83) = *reinterpret_cast<uint32_t*>(&rdx75) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx83) + 4) = 0;
        rsp84 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp71) - reinterpret_cast<uint64_t>(rdx83));
        if (rdx83) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp84) + reinterpret_cast<uint64_t>(rdx83) + 0xfffffffffffffff8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp84) + reinterpret_cast<uint64_t>(rdx83) + 0xfffffffffffffff8);
        }
        rsp85 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp84) - 8);
        *rsp85 = 0x5822;
        rax86 = fun_3a80(reinterpret_cast<uint64_t>(rsp84) + 15 & 0xfffffffffffffff0, r12_68, r8_72);
        rsp87 = rsp85 + 1 - 1;
        *rsp87 = 0x582d;
        strip_trailing_slashes(rax86, r12_68, r8_72, rcx74, r8_72, r9);
        eax88 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax86) - 46);
        if (!eax88 && (eax88 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax86 + 1) - 46), !eax88)) {
            eax88 = *reinterpret_cast<unsigned char*>(rax86 + 2);
        }
        rsi89 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax86) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(eax88 < 1)));
        rsp90 = reinterpret_cast<struct s1*>(rsp87 + 1 - 1);
        rsp90->f0 = 0x5865;
        rax91 = file_name_concat(v12, rsi89, v12, rsi89);
        rsp92 = reinterpret_cast<void**>(&rsp90->f8);
        r12_93 = rax91;
        goto addr_5868_55;
        addr_5a32_49:
        *reinterpret_cast<uint32_t*>(&rdx94) = *reinterpret_cast<uint32_t*>(&rdx82) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx94) + 4) = 0;
        rsp95 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp78) - reinterpret_cast<uint64_t>(rdx94));
        if (rdx94) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp95) + reinterpret_cast<uint64_t>(rdx94) + 0xfffffffffffffff8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp95) + reinterpret_cast<uint64_t>(rdx94) + 0xfffffffffffffff8);
        }
        rsp96 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp95) - 8);
        *rsp96 = 0x5a5a;
        rax97 = fun_3a80(reinterpret_cast<uint64_t>(rsp95) + 15 & 0xfffffffffffffff0, r15_60, r8_79);
        rsp98 = rsp96 + 1 - 1;
        *rsp98 = 0x5a65;
        strip_trailing_slashes(rax97, r15_60, r8_79, rcx81, r8_79, r9);
        rsp99 = rsp98 + 1 - 1;
        *rsp99 = 0x5a7b;
        rax100 = file_name_concat(v12, rax97, v12, rax97);
        *reinterpret_cast<int32_t*>(&rcx4) = 0;
        *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
        r12_93 = rax100;
        zf101 = *reinterpret_cast<unsigned char*>(v13 + 60) == 0;
        rsp102 = reinterpret_cast<void***>(rsp99 + 1 - 1);
        *rsp102 = v12;
        if (!zf101) {
            rcx4 = reinterpret_cast<void**>("%s -> %s\n");
        }
        rsp103 = rsp102 - 8;
        *rsp103 = v13;
        rsi = reinterpret_cast<void**>(-static_cast<int64_t>(reinterpret_cast<unsigned char>(r12_93)));
        rsp104 = reinterpret_cast<struct s2*>(rsp103 - 8);
        rsp104->f0 = 0x5abe;
        al105 = make_dir_parents_private(r12_93, rsi, v52, rcx4, reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff20, reinterpret_cast<int64_t>(rbp8) + 0xffffffffffffff1e, rsp104->f8);
        rsp106 = reinterpret_cast<struct s3*>(&rsp104->f8);
        rdx = reinterpret_cast<void**>(0);
        r8 = rsp106->f0;
        rsp107 = reinterpret_cast<struct s4*>(&rsp106->f8);
        r9 = rsp107->f0;
        rsp92 = reinterpret_cast<void**>(&rsp107->f8);
        zf108 = reinterpret_cast<int1_t>(free == 47);
        if (zf108) {
            rdx = reinterpret_cast<void**>(1);
            do {
                v61 = rdx;
                ++rdx;
            } while (*reinterpret_cast<void***>(rdx + 0xffffffffffffffff) == 47);
        }
        if (al105) {
            addr_5868_55:
            rsp109 = reinterpret_cast<struct s0*>(rsp92 - 8);
            rsp109->f0 = 0;
            r9 = v13;
            rsp110 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp109) - 8);
            *rsp110 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp8) - 0xe1);
            *reinterpret_cast<uint32_t*>(&r8) = 0;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            rdx = v52;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rsp111 = reinterpret_cast<struct s5*>(rsp110 - 1);
            rsp111->f0 = 0x5899;
            eax112 = copy(r15_60, r12_93, rdx, v61, r15_60, r12_93, rdx, v61);
            rsp113 = reinterpret_cast<struct s6*>(&rsp111->f8);
            rcx4 = rsp113->f0;
            rsp114 = reinterpret_cast<struct s7*>(&rsp113->f8);
            rsi = rsp114->f0;
            rsp92 = reinterpret_cast<void**>(&rsp114->f8);
            r14d47 = r14d47 & eax112;
            zf115 = parents_option == 0;
            if (!zf115) {
                r8 = v13;
                rcx4 = v116;
                rdx = v61;
                rsi = v52;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                rsp117 = reinterpret_cast<struct s0*>(rsp92 - 8);
                rsp117->f0 = 0x5b5b;
                eax118 = re_protect(r12_93, rsi, rdx, rcx4, r8, r9);
                rsp92 = reinterpret_cast<void**>(&rsp117->f8);
                r14d47 = r14d47 & eax118;
                eax119 = parents_option;
            } else {
                addr_58ab_64:
                rdi1 = r12_93;
                ++rbx58;
                rsp120 = reinterpret_cast<struct s0*>(rsp92 - 8);
                rsp120->f0 = 0x58b7;
                fun_3670(rdi1, rdi1);
                rsp10 = reinterpret_cast<void**>(&rsp120->f8);
                if (v11 > *reinterpret_cast<int32_t*>(&rbx58)) 
                    continue; else 
                    goto addr_58c3_39;
            }
        } else {
            eax119 = parents_option;
            r14d47 = 0;
        }
        if (*reinterpret_cast<signed char*>(&eax119) && (r15_121 = v116, !!r15_121)) {
            do {
                rdi122 = r15_121;
                r15_121 = *reinterpret_cast<void***>(r15_121 + 0xa0);
                v116 = r15_121;
                rsp123 = reinterpret_cast<struct s0*>(rsp92 - 8);
                rsp123->f0 = 0x5b2e;
                fun_3670(rdi122, rdi122);
                rsp92 = reinterpret_cast<void**>(&rsp123->f8);
            } while (r15_121);
            goto addr_58ab_64;
        }
    }
}

void** dir_len();

struct s8 {
    signed char[4088] pad4088;
    uint64_t fff8;
};

void** fun_3cb0(void** rdi, void** rsi, void** rdx, ...);

void fun_f000();

struct s9 {
    signed char[4088] pad4088;
    uint64_t fff8;
};

int64_t fun_36b0(int64_t rdi, void** rsi, void*** rdx, ...);

int32_t fun_3bf0(int64_t rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

int32_t copy_acl(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_3ad0(int64_t rdi, void** rsi, ...);

void** chown_failure_ok(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

void** fun_3870(void** rdi, ...);

void** xmalloc(int64_t rdi, void** rsi, void** rdx);

void** set_process_security_ctx(void** rdi, void** rsi, ...);

int32_t fun_3a50(int64_t rdi, void** rsi);

void fun_3b40(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

int32_t cached_umask(void** rdi, void** rsi);

void** set_file_security_ctx(void** rdi, ...);

signed char make_dir_parents_private(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9, void** a7) {
    void* rsp8;
    void* rbp9;
    int64_t r15_10;
    void** r13_11;
    void** rbx12;
    void** v13;
    void** r12_14;
    void** v15;
    void** v16;
    void* rax17;
    void* v18;
    void** rax19;
    void* rsp20;
    void** r14_21;
    void** rax22;
    void* rsp23;
    void*** rax24;
    void* rcx25;
    uint64_t rdx26;
    void* rdx27;
    void* rsp28;
    int64_t* rsp29;
    void** rax30;
    struct s8* rsp31;
    void** v32;
    void*** rax33;
    struct s8* rdx34;
    uint64_t rcx35;
    void* rcx36;
    void* rsp37;
    void** rax38;
    int64_t* rsp39;
    void* rsp40;
    void** rax41;
    void** v42;
    int64_t* rsp43;
    void** eax44;
    int32_t eax45;
    uint32_t v46;
    void** rsi47;
    void** rsp48;
    void** rax49;
    void** rsp50;
    void** rsp51;
    void* rdx52;
    void** rsp53;
    void** rsp54;
    void** rbp55;
    int64_t* rsp56;
    void** r15_57;
    void*** rsp58;
    void** r14d59;
    void*** rsp60;
    void** r13_61;
    void*** rsp62;
    void*** rsp63;
    void** rbx64;
    void* rax65;
    int64_t* rsp66;
    void** rax67;
    struct s9* rsp68;
    void** r8_69;
    void*** rax70;
    struct s9* rcx71;
    uint64_t rdx72;
    void* rdx73;
    void* rsp74;
    int64_t* rsp75;
    void** rax76;
    void* rsp77;
    void** r12_78;
    void** r13_79;
    int1_t zf80;
    void** rax81;
    int64_t rdi82;
    uint64_t rdx83;
    int64_t rax84;
    void*** rdx85;
    int64_t* rsp86;
    int64_t rax87;
    void** rcx88;
    int64_t rdx89;
    int64_t rdi90;
    int64_t* rsp91;
    int32_t eax92;
    uint32_t r8d93;
    void** r8_94;
    int64_t* rsp95;
    int32_t eax96;
    int64_t rdi97;
    int64_t* rsp98;
    int32_t eax99;
    int64_t* rsp100;
    void** eax101;
    void* rsp102;
    void** rcx103;
    int64_t rdi104;
    int64_t* rsp105;
    void* rax106;
    int64_t* rsp107;
    int64_t* rsp108;
    int64_t* rsp109;
    int64_t* rsp110;
    int64_t* rsp111;
    void* rsp112;
    int64_t* rsp113;
    int64_t* rsp114;
    int64_t* rsp115;
    int64_t* rsp116;
    void** rbx117;
    int1_t zf118;
    void** v119;
    void** v120;
    void** rsp121;
    void** rax122;
    void** rdi123;
    void** rsp124;
    void** eax125;
    void* rsp126;
    void** rsp127;
    int32_t eax128;
    void* rsp129;
    uint32_t v130;
    void** rsp131;
    void** rax132;
    void** rsp133;
    void** rax134;
    void** rax135;
    void** rsp136;
    void** eax137;
    void** rsp138;
    void** eax139;
    int64_t rdi140;
    void** rsp141;
    int32_t eax142;
    void* rsp143;
    void** rsp144;
    void** rdi145;
    void** rsp146;
    void** eax147;
    uint32_t eax148;
    uint32_t v149;
    void** rsp150;
    int32_t eax151;
    uint32_t v152;
    int64_t rdi153;
    void** rsp154;
    int32_t eax155;
    int1_t zf156;
    void** rsp157;
    void** eax158;
    uint32_t v159;
    void** rsp160;
    void** rax161;
    void** rsp162;
    void** rsp163;
    void** rsp164;
    void** rax165;
    void* rsp166;
    void** rsp167;
    void** rax168;
    void** rsp169;
    void** rax170;
    void** rsp171;
    void** rsp172;
    void** rax173;
    void** rsp174;
    void** rax175;

    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp9 = rsp8;
    *reinterpret_cast<void***>(&r15_10) = edx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_10) + 4) = 0;
    r13_11 = rdi;
    rbx12 = rsi;
    v13 = rcx;
    r12_14 = a7;
    v15 = r8;
    v16 = r9;
    rax17 = g28;
    v18 = rax17;
    rax19 = dir_len();
    rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 - 8 - 8 - 8 - 8 - 0x178 - 8 + 8);
    r14_21 = rax19;
    *reinterpret_cast<void***>(v15) = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(r14_21) > reinterpret_cast<unsigned char>(rbx12)) {
        rax22 = fun_3820(r13_11);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp20) - 8 + 8);
        r8 = rax22 + 1;
        rax24 = reinterpret_cast<void***>(rax22 + 24);
        rcx25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp23) - (reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffff000));
        rdx26 = reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffffff0;
        if (rsp23 != rcx25) {
            do {
                rsp23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp23) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
            } while (rsp23 != rcx25);
        }
        *reinterpret_cast<uint32_t*>(&rdx27) = *reinterpret_cast<uint32_t*>(&rdx26) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp23) - reinterpret_cast<int64_t>(rdx27));
        if (rdx27) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<int64_t>(rdx27) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<int64_t>(rdx27) - 8);
        }
        rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp28) - 8);
        *rsp29 = 0x4815;
        rax30 = fun_3a80(reinterpret_cast<uint64_t>(rsp28) + 15 & 0xfffffffffffffff0, r13_11, r8);
        rsp31 = reinterpret_cast<struct s8*>(rsp29 + 1);
        v32 = rax30;
        rax33 = reinterpret_cast<void***>(r14_21 + 24);
        rdx34 = reinterpret_cast<struct s8*>(reinterpret_cast<uint64_t>(rsp31) - (reinterpret_cast<uint64_t>(rax33) & 0xfffffffffffff000));
        rcx35 = reinterpret_cast<uint64_t>(rax33) & 0xfffffffffffffff0;
        if (rsp31 != rdx34) {
            do {
                rsp31 = reinterpret_cast<struct s8*>(reinterpret_cast<uint64_t>(rsp31) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
                *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp31) + reinterpret_cast<int64_t>("strcmp")) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp31) + reinterpret_cast<int64_t>("strcmp"));
            } while (rsp31 != rdx34);
        }
        *reinterpret_cast<uint32_t*>(&rcx36) = *reinterpret_cast<uint32_t*>(&rcx35) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx36) + 4) = 0;
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp31) - reinterpret_cast<int64_t>(rcx36));
        if (rcx36) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp37) + reinterpret_cast<int64_t>(rcx36) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp37) + reinterpret_cast<int64_t>(rcx36) - 8);
        }
        rax38 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp37) + 15 & 0xfffffffffffffff0);
        r13_11 = rax38;
        rsp39 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp37) - 8);
        *rsp39 = 0x487f;
        fun_3a80(rax38, v32, r14_21);
        rsp40 = reinterpret_cast<void*>(rsp39 + 1);
        rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) + reinterpret_cast<unsigned char>(rbx12));
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_11) + reinterpret_cast<unsigned char>(r14_21)) = 0;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 47)) {
            do {
                ++rsi;
            } while (*reinterpret_cast<void***>(rsi) == 47);
        }
        rax41 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp9) + 0xfffffffffffffea0);
        *reinterpret_cast<int32_t*>(&rcx) = 0;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rdi = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        v42 = rax41;
        rsp43 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp40) - 8);
        *rsp43 = 0x48b4;
        eax44 = fun_3cb0(rdi, rsi, rax41);
        rsp20 = reinterpret_cast<void*>(rsp43 + 1);
        if (eax44) 
            goto addr_48bc_14;
    } else {
        addr_4780_15:
        eax45 = 1;
        goto addr_4785_16;
    }
    if ((v46 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) {
        rsi47 = r13_11;
    } else {
        *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(0);
        goto addr_4780_15;
    }
    addr_4d3c_20:
    rsp48 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp48 = reinterpret_cast<void*>(0x4d46);
    rax49 = quotearg_style(4, rsi47, 4, rsi47);
    r12_14 = rax49;
    rsp50 = rsp48 + 1 - 1;
    *rsp50 = reinterpret_cast<void*>(0x4d5c);
    fun_3800();
    rcx = r12_14;
    rsi = reinterpret_cast<void**>(0);
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsp51 = rsp50 + 1 - 1;
    *rsp51 = reinterpret_cast<void*>(0x4d6d);
    fun_3b80();
    rsp20 = reinterpret_cast<void*>(rsp51 + 1);
    addr_4d6d_21:
    eax45 = 0;
    addr_4785_16:
    rdx52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v18) - reinterpret_cast<int64_t>(g28));
    if (!rdx52) {
        return *reinterpret_cast<signed char*>(&eax45);
    }
    rsp53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp53 = reinterpret_cast<void*>(0x4e1c);
    fun_3840();
    rsp54 = rsp53 + 1 - 1;
    *rsp54 = rbp9;
    rbp55 = rsp54;
    rsp56 = reinterpret_cast<int64_t*>(rsp54 - 1);
    *rsp56 = r15_10;
    r15_57 = r8;
    rsp58 = reinterpret_cast<void***>(rsp56 - 1);
    *rsp58 = r14_21;
    r14d59 = rsi;
    rsp60 = rsp58 - 8;
    *rsp60 = r13_11;
    r13_61 = rdi;
    rsp62 = rsp60 - 8;
    *rsp62 = r12_14;
    rsp63 = rsp62 - 8;
    *rsp63 = rbx12;
    rbx64 = rcx;
    *reinterpret_cast<void**>(rbp55 - 13) = rdx52;
    rax65 = g28;
    *(rbp55 - 7) = rax65;
    rsp66 = reinterpret_cast<int64_t*>(rsp63 - 72 - 8);
    *rsp66 = 0x4e55;
    rax67 = fun_3820(rdi);
    rsp68 = reinterpret_cast<struct s9*>(rsp66 + 1);
    r8_69 = rax67 + 1;
    rax70 = reinterpret_cast<void***>(rax67 + 24);
    rcx71 = reinterpret_cast<struct s9*>(reinterpret_cast<uint64_t>(rsp68) - (reinterpret_cast<uint64_t>(rax70) & 0xfffffffffffff000));
    rdx72 = reinterpret_cast<uint64_t>(rax70) & 0xfffffffffffffff0;
    if (rsp68 != rcx71) 
        goto addr_4e75_25;
    addr_4e8a_26:
    *reinterpret_cast<uint32_t*>(&rdx73) = *reinterpret_cast<uint32_t*>(&rdx72) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx73) + 4) = 0;
    rsp74 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp68) - reinterpret_cast<int64_t>(rdx73));
    if (rdx73) {
        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp74) + reinterpret_cast<int64_t>(rdx73) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp74) + reinterpret_cast<int64_t>(rdx73) - 8);
    }
    rsp75 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp74) - 8);
    *rsp75 = 0x4eb0;
    rax76 = fun_3a80(reinterpret_cast<uint64_t>(rsp74) + 15 & 0xfffffffffffffff0, r13_61, r8_69);
    rsp77 = reinterpret_cast<void*>(rsp75 + 1);
    r12_78 = rax76;
    r13_79 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_78) + reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(*reinterpret_cast<void**>(rbp55 - 13)) - reinterpret_cast<unsigned char>(r13_61)));
    if (!rbx64) {
        addr_50a8_29:
    } else {
        *reinterpret_cast<void****>(rbp55 - 14) = reinterpret_cast<void***>(rbp55 - 12);
        while ((zf80 = *reinterpret_cast<signed char*>(r15_57 + 31) == 0, *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_78) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx64 + 0x98))) = 0, zf80) || (rax81 = *reinterpret_cast<void***>(rbx64 + 80), *reinterpret_cast<void***>(&rdi82) = r14d59, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0, *reinterpret_cast<void***>(rbp55 - 12) = *reinterpret_cast<void***>(rbx64 + 72), rdx83 = *reinterpret_cast<uint64_t*>(rbx64 + 88), *reinterpret_cast<void***>(rbp55 - 11) = rax81, rax84 = *reinterpret_cast<int64_t*>(rbx64 + 96), *reinterpret_cast<uint64_t*>(rbp55 - 10) = rdx83, rdx85 = *reinterpret_cast<void****>(rbp55 - 14), *reinterpret_cast<int64_t*>(rbp55 - 9) = rax84, rsp86 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8), *rsp86 = 0x4f5e, rax87 = fun_36b0(rdi82, r13_79, rdx85, rdi82, r13_79, rdx85), rsp77 = reinterpret_cast<void*>(rsp86 + 1), *reinterpret_cast<int32_t*>(&rax87) == 0)) {
            if (!*reinterpret_cast<unsigned char*>(r15_57 + 29) || (rcx88 = *reinterpret_cast<void***>(rbx64 + 32), *reinterpret_cast<int32_t*>(&rcx88 + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx89) = *reinterpret_cast<unsigned char*>(rbx64 + 28), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx89) + 4) = 0, *reinterpret_cast<void***>(&rdi90) = r14d59, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi90) + 4) = 0, rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8), *rsp91 = 0x4fc7, eax92 = fun_3bf0(rdi90, r13_79, rdx89, rcx88, 0x100), rsp77 = reinterpret_cast<void*>(rsp91 + 1), eax92 == 0)) {
                r8d93 = *reinterpret_cast<unsigned char*>(r15_57 + 30);
                if (*reinterpret_cast<void**>(&r8d93)) {
                    addr_5010_34:
                    r8_94 = *reinterpret_cast<void***>(rbx64 + 24);
                    *reinterpret_cast<int32_t*>(&r8_94 + 4) = 0;
                    rsp95 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
                    *rsp95 = 0x5029;
                    eax96 = copy_acl(r13_79, 0xffffffff, r12_78, 0xffffffff, r8_94, r9);
                    rsp77 = reinterpret_cast<void*>(rsp95 + 1);
                    if (eax96) 
                        goto addr_5031_35;
                } else {
                    addr_4ef1_36:
                    if (!*reinterpret_cast<void***>(rbx64 + 0x90)) 
                        goto addr_4efe_37;
                    *reinterpret_cast<void***>(&rdi97) = r14d59;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi97) + 4) = 0;
                    *reinterpret_cast<void**>(rbp55 - 13) = *reinterpret_cast<void**>(&r8d93);
                    rsp98 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
                    *rsp98 = 0x5057;
                    eax99 = fun_3ad0(rdi97, r13_79, rdi97, r13_79);
                    rsp77 = reinterpret_cast<void*>(rsp98 + 1);
                    if (eax99) 
                        goto addr_505f_39;
                }
                addr_4efe_37:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_78) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx64 + 0x98))) = 47;
                rbx64 = *reinterpret_cast<void***>(rbx64 + 0xa0);
                if (!rbx64) 
                    goto addr_50a8_29;
            } else {
                rsp100 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
                *rsp100 = 0x4fd7;
                eax101 = chown_failure_ok(r15_57, r13_79, rdx89, rcx88, 0x100);
                rsp102 = reinterpret_cast<void*>(rsp100 + 1);
                if (!*reinterpret_cast<void**>(&eax101)) 
                    goto addr_50da_41;
                rcx103 = *reinterpret_cast<void***>(rbx64 + 32);
                *reinterpret_cast<int32_t*>(&rcx103 + 4) = 0;
                *reinterpret_cast<void***>(&rdi104) = r14d59;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi104) + 4) = 0;
                rsp105 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp102) - 8);
                *rsp105 = 0x4ff8;
                fun_3bf0(rdi104, r13_79, 0xffffffff, rcx103, 0x100);
                rsp77 = reinterpret_cast<void*>(rsp105 + 1);
                r8d93 = *reinterpret_cast<unsigned char*>(r15_57 + 30);
                if (!*reinterpret_cast<void**>(&r8d93)) 
                    goto addr_4ef1_36;
                goto addr_5010_34;
            }
        }
        goto addr_4f66_44;
    }
    addr_50ae_45:
    rax106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(*(rbp55 - 7)) - reinterpret_cast<int64_t>(g28));
    if (rax106) {
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8) = reinterpret_cast<int64_t>(usage);
        fun_3840();
    } else {
        goto *reinterpret_cast<int64_t*>(rbp55 - 5 + 1 + 1 + 1 + 1 + 1 + 1);
    }
    addr_4f66_44:
    rsp107 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
    *rsp107 = 0x4f73;
    quotearg_style(4, r12_78, 4, r12_78);
    rsp108 = rsp107 + 1 - 1;
    *rsp108 = 0x4f89;
    fun_3800();
    rsp109 = rsp108 + 1 - 1;
    *rsp109 = 0x4f91;
    fun_36d0();
    rsp110 = rsp109 + 1 - 1;
    *rsp110 = 0x4fa2;
    fun_3b80();
    rsp77 = reinterpret_cast<void*>(rsp110 + 1);
    goto addr_50ae_45;
    addr_5031_35:
    goto addr_50ae_45;
    addr_505f_39:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
    *rsp111 = 0x506c;
    quotearg_style(4, r12_78, 4, r12_78);
    rsp112 = reinterpret_cast<void*>(rsp111 + 1);
    addr_507b_49:
    rsp113 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp112) - 8);
    *rsp113 = 0x5082;
    fun_3800();
    rsp114 = rsp113 + 1 - 1;
    *rsp114 = 0x508a;
    fun_36d0();
    rsp115 = rsp114 + 1 - 1;
    *rsp115 = 0x509b;
    fun_3b80();
    rsp77 = reinterpret_cast<void*>(rsp115 + 1);
    goto addr_50ae_45;
    addr_50da_41:
    *reinterpret_cast<void**>(rbp55 - 13) = *reinterpret_cast<void**>(&eax101);
    rsp116 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp102) - 8);
    *rsp116 = 0x50ea;
    quotearg_style(4, r12_78, 4, r12_78);
    rsp112 = reinterpret_cast<void*>(rsp116 + 1);
    goto addr_507b_49;
    do {
        addr_4e75_25:
        rsp68 = reinterpret_cast<struct s9*>(reinterpret_cast<uint64_t>(rsp68) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp68) + reinterpret_cast<int64_t>("strcmp")) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp68) + reinterpret_cast<int64_t>("strcmp"));
    } while (rsp68 != rcx71);
    goto addr_4e8a_26;
    addr_48bc_14:
    rbx117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx12) + reinterpret_cast<unsigned char>(v32));
    zf118 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx117) == 47);
    v119 = rbx117;
    r14_21 = rbx117;
    if (!zf118) {
        r14_21 = v119;
    } else {
        do {
            ++r14_21;
        } while (*reinterpret_cast<void***>(r14_21) == 47);
    }
    rdi = r14_21;
    v120 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp9) + 0xffffffffffffff30);
    while (rsi = reinterpret_cast<void**>(47), rsp121 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8), *rsp121 = reinterpret_cast<void*>(0x490a), rax122 = fun_3870(rdi), rsp20 = reinterpret_cast<void*>(rsp121 + 1), rbx12 = rax122, !!rax122) {
        *reinterpret_cast<void***>(rbx12) = reinterpret_cast<void**>(0);
        rdi123 = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(&rdi123 + 4) = 0;
        rsp124 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
        *rsp124 = reinterpret_cast<void*>(0x492d);
        eax125 = fun_3cb0(rdi123, r14_21, v42);
        rsp126 = reinterpret_cast<void*>(rsp124 + 1);
        r13_11 = eax125;
        *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        if (eax125 || *reinterpret_cast<unsigned char*>(r12_14 + 28) & 0xffffff00) {
            rsp127 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp126) - 8);
            *rsp127 = reinterpret_cast<void*>(0x4a03);
            eax128 = fun_3a70(v119, v120, v119, v120);
            rsp129 = reinterpret_cast<void*>(rsp127 + 1);
            if (!eax128) {
                if ((v130 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) 
                    goto addr_4c46_58;
            } else {
                rsp131 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp129) - 8);
                *rsp131 = reinterpret_cast<void*>(0x4a10);
                rax132 = fun_36d0();
                rsp129 = reinterpret_cast<void*>(rsp131 + 1);
                r8 = *reinterpret_cast<void***>(rax132);
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                if (r8) 
                    goto addr_4c4c_60;
            }
            rsp133 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp129) - 8);
            *rsp133 = reinterpret_cast<void*>(0x4a26);
            rax134 = xmalloc(0xa8, v120, v42);
            rsp126 = reinterpret_cast<void*>(rsp133 + 1);
            __asm__("movdqa xmm0, [rbp-0xd0]");
            __asm__("movdqa xmm1, [rbp-0xc0]");
            __asm__("movdqa xmm2, [rbp-0xb0]");
            __asm__("movdqa xmm3, [rbp-0xa0]");
            r9 = rax134;
            __asm__("movups [rax], xmm0");
            __asm__("movdqa xmm5, [rbp-0x80]");
            __asm__("movdqa xmm6, [rbp-0x70]");
            __asm__("movdqa xmm7, [rbp-0x60]");
            __asm__("movdqa xmm0, [rbp-0x50]");
            __asm__("movups [rax+0x10], xmm1");
            __asm__("movdqa xmm4, [rbp-0x90]");
            __asm__("movups [rax+0x20], xmm2");
            __asm__("movups [rax+0x30], xmm3");
            __asm__("movups [rax+0x40], xmm4");
            __asm__("movups [rax+0x50], xmm5");
            __asm__("movups [rax+0x60], xmm6");
            __asm__("movups [rax+0x70], xmm7");
            __asm__("movups [rax+0x80], xmm0");
            *reinterpret_cast<void**>(r9 + 0x98) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx12) - reinterpret_cast<unsigned char>(v32));
            rax135 = *reinterpret_cast<void***>(v15);
            *reinterpret_cast<void***>(r9 + 0x90) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r9 + 0xa0) = rax135;
            *reinterpret_cast<void***>(v15) = r9;
            if (!r13_11) 
                goto addr_4947_62;
        } else {
            addr_4947_62:
            rsi = v32;
            *reinterpret_cast<int32_t*>(&rcx) = 0;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            r8 = r12_14;
            rdi = v119;
            rsp136 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp126) - 8);
            *rsp136 = reinterpret_cast<void*>(0x4961);
            eax137 = set_process_security_ctx(rdi, rsi);
            rsp20 = reinterpret_cast<void*>(rsp136 + 1);
            if (!*reinterpret_cast<signed char*>(&eax137)) 
                goto addr_4d6d_21; else 
                goto addr_4969_63;
        }
        rsi = v32;
        r8 = r12_14;
        *reinterpret_cast<int32_t*>(&rcx) = 1;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rdi = v119;
        rsp138 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp126) - 8);
        *rsp138 = reinterpret_cast<void*>(0x4ae7);
        eax139 = set_process_security_ctx(rdi, rsi, rdi, rsi);
        rsp20 = reinterpret_cast<void*>(rsp138 + 1);
        if (!*reinterpret_cast<signed char*>(&eax139)) 
            goto addr_4d6d_21;
        *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(1);
        if (*reinterpret_cast<unsigned char*>(r12_14 + 29)) 
            goto addr_4b10_66;
        if (!*reinterpret_cast<unsigned char*>(r12_14 + 30)) {
            r13_11 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        } else {
            r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 24)) & 18);
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        }
        addr_4b1c_70:
        *reinterpret_cast<void***>(&rdi140) = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi140) + 4) = 0;
        if (*reinterpret_cast<void***>(r12_14 + 32)) {
        }
        rsp141 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
        *rsp141 = reinterpret_cast<void*>(0x4b44);
        eax142 = fun_3a50(rdi140, r14_21);
        rsp143 = reinterpret_cast<void*>(rsp141 + 1);
        r9 = r9;
        if (eax142) 
            goto addr_4dd3_73;
        if (v13) {
            rsp144 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp143) - 8);
            *rsp144 = reinterpret_cast<void*>(0x4b80);
            fun_3b40(1, v13, v119, v32);
            rsp143 = reinterpret_cast<void*>(rsp144 + 1);
            r9 = r9;
        }
        *reinterpret_cast<int32_t*>(&rcx) = 0x100;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rdi145 = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(&rdi145 + 4) = 0;
        rsp146 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp143) - 8);
        *rsp146 = reinterpret_cast<void*>(0x4ba5);
        eax147 = fun_3cb0(rdi145, r14_21, v42);
        rsp20 = reinterpret_cast<void*>(rsp146 + 1);
        r9 = r9;
        if (eax147) 
            goto addr_4d80_77;
        eax148 = v149;
        if (*reinterpret_cast<unsigned char*>(r12_14 + 30)) 
            goto addr_4bee_79;
        if (~eax148 & reinterpret_cast<unsigned char>(r13_11)) {
            rsp150 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
            *rsp150 = reinterpret_cast<void*>(0x4cf1);
            eax151 = cached_umask(rdi145, r14_21);
            rsp20 = reinterpret_cast<void*>(rsp150 + 1);
            r9 = r9;
            r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) & reinterpret_cast<uint32_t>(~eax151));
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
            eax148 = v152;
            if (~eax148 & reinterpret_cast<unsigned char>(r13_11)) 
                goto addr_4bdf_82;
        }
        if ((eax148 & 0x1c0) == 0x1c0) {
            addr_4bee_79:
            if ((eax148 | 0x1c0) == eax148) 
                goto addr_4c12_85;
            *reinterpret_cast<int32_t*>(&rcx) = 0x100;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            *reinterpret_cast<void***>(&rdi153) = *reinterpret_cast<void***>(&r15_10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi153) + 4) = 0;
            rsp154 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
            *rsp154 = reinterpret_cast<void*>(0x4c0a);
            eax155 = fun_3ad0(rdi153, r14_21);
            rsp20 = reinterpret_cast<void*>(rsp154 + 1);
            if (eax155) 
                goto addr_4df5_87;
        } else {
            addr_4bdf_82:
            r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) | eax148);
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
            *reinterpret_cast<void***>(r9 + 0x90) = reinterpret_cast<void**>(1);
            *reinterpret_cast<void***>(r9 + 24) = r13_11;
            goto addr_4bee_79;
        }
        addr_4c12_85:
        if (*reinterpret_cast<void***>(v16)) 
            goto addr_49c0_88;
        addr_4989_90:
        if (!*reinterpret_cast<void***>(r12_14 + 40)) {
            if (!*reinterpret_cast<signed char*>(r12_14 + 51)) {
                addr_49c0_88:
                zf156 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx12 + 1) == 47);
                *reinterpret_cast<void***>(rbx12) = reinterpret_cast<void**>(47);
                rdi = rbx12 + 1;
                if (!zf156) 
                    continue;
            } else {
                goto addr_4995_93;
            }
        } else {
            addr_4995_93:
            rdi = v32;
            rsi = reinterpret_cast<void**>(0);
            rsp157 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
            *rsp157 = reinterpret_cast<void*>(0x49a6);
            eax158 = set_file_security_ctx(rdi);
            rsp20 = reinterpret_cast<void*>(rsp157 + 1);
            if (!*reinterpret_cast<signed char*>(&eax158)) {
                if (*reinterpret_cast<unsigned char*>(r12_14 + 52)) 
                    goto addr_4d6d_21;
                goto addr_49c0_88;
            }
        }
        do {
            ++rdi;
        } while (*reinterpret_cast<void***>(rdi) == 47);
        continue;
        addr_4b10_66:
        r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 24)) & 63);
        *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        goto addr_4b1c_70;
        addr_4969_63:
        if ((v159 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) 
            goto addr_4dc7_99;
        *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(0);
        goto addr_4989_90;
    }
    goto addr_4780_15;
    addr_4c46_58:
    r8 = reinterpret_cast<void**>(20);
    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
    addr_4c4c_60:
    rsp160 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp129) - 8);
    *rsp160 = reinterpret_cast<void*>(0x4c64);
    rax161 = quotearg_style(4, v119, 4, v119);
    r12_14 = rax161;
    rsp162 = rsp160 + 1 - 1;
    *rsp162 = reinterpret_cast<void*>(0x4c7a);
    fun_3800();
    rsi = r8;
    rcx = r12_14;
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsp163 = rsp162 + 1 - 1;
    *rsp163 = reinterpret_cast<void*>(0x4c8f);
    fun_3b80();
    rsp20 = reinterpret_cast<void*>(rsp163 + 1);
    eax45 = 0;
    goto addr_4785_16;
    addr_4dc7_99:
    rsi47 = v32;
    goto addr_4d3c_20;
    addr_4dd3_73:
    rsp164 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp143) - 8);
    *rsp164 = reinterpret_cast<void*>(0x4de4);
    rax165 = quotearg_style(4, v32);
    rsp166 = reinterpret_cast<void*>(rsp164 + 1);
    r13_11 = rax165;
    addr_4da0_101:
    rsp167 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp166) - 8);
    *rsp167 = reinterpret_cast<void*>(0x4da7);
    rax168 = fun_3800();
    r12_14 = rax168;
    rsp169 = rsp167 + 1 - 1;
    *rsp169 = reinterpret_cast<void*>(0x4daf);
    rax170 = fun_36d0();
    rcx = r13_11;
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsi = *reinterpret_cast<void***>(rax170);
    rsp171 = rsp169 + 1 - 1;
    *rsp171 = reinterpret_cast<void*>(0x4dc0);
    fun_3b80();
    rsp20 = reinterpret_cast<void*>(rsp171 + 1);
    eax45 = 0;
    goto addr_4785_16;
    addr_4d80_77:
    rsp172 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp172 = reinterpret_cast<void*>(0x4d91);
    rax173 = quotearg_style(4, v32, 4, v32);
    rsp166 = reinterpret_cast<void*>(rsp172 + 1);
    r13_11 = rax173;
    goto addr_4da0_101;
    addr_4df5_87:
    rsp174 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp174 = reinterpret_cast<void*>(0x4e06);
    rax175 = quotearg_style(4, v32);
    rsp166 = reinterpret_cast<void*>(rsp174 + 1);
    r13_11 = rax175;
    goto addr_4da0_101;
}

void** fun_38b0();

int32_t fun_39a0(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

void** create_hole(void** edi, void** rsi, void** edx, void** rcx, void** r8, ...) {
    void** r12_6;
    void** ebx7;
    void** rax8;
    int64_t rdi9;
    int32_t eax10;
    void** rax11;

    r12_6 = rcx;
    ebx7 = edx;
    rax8 = fun_38b0();
    if (reinterpret_cast<signed char>(rax8) < reinterpret_cast<signed char>(0)) {
        quotearg_style(4, rsi, 4, rsi);
        fun_3800();
        fun_36d0();
        fun_3b80();
        return 0;
    } else {
        if (!*reinterpret_cast<signed char*>(&ebx7) || ((*reinterpret_cast<void***>(&rdi9) = edi, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0, eax10 = fun_39a0(rdi9, 3, reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(r12_6), r12_6), eax10 >= 0) || (rax11 = fun_36d0(), *reinterpret_cast<unsigned char*>(&r12_6) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax11) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax11) == 38))), !!*reinterpret_cast<unsigned char*>(&r12_6)))) {
            r12_6 = reinterpret_cast<void**>(1);
        } else {
            quotearg_style(4, rsi, 4, rsi);
            fun_3800();
            fun_3b80();
        }
        return r12_6;
    }
}

struct s10 {
    signed char[24] pad24;
    uint32_t f18;
};

int32_t fun_3bb0(int64_t rdi, int64_t rsi, int64_t rdx);

int32_t qset_acl(void** rdi, void** rsi);

/* set_owner.isra.0 */
int32_t set_owner_isra_0(void** rdi, void** rsi, void** edx, void** rcx, void** r8d, void** r9, int32_t a7, int32_t a8, unsigned char a9, struct s10* a10) {
    uint32_t edx11;
    uint32_t eax12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;
    int32_t eax16;
    void** rax17;
    void** ebp18;
    void** rcx19;
    int64_t rdi20;
    int64_t rdx21;
    int64_t rsi22;
    int64_t rdi23;
    int32_t eax24;
    void** rax25;
    void** r13d26;
    int64_t rdx27;
    int64_t rdi28;
    void** rsi29;
    int32_t eax30;
    void** rax31;

    if (a9) 
        goto addr_6a10_2;
    if (!(0xff0000000000ff & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 24)))) {
        if (!*reinterpret_cast<signed char*>(rdi + 57)) 
            goto addr_6a10_2;
        r9 = *reinterpret_cast<void***>(rdi + 16);
        edx11 = a10->f18;
    } else {
        edx11 = a10->f18;
    }
    eax12 = ~reinterpret_cast<unsigned char>(r9);
    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax12) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax12) + 1) | 14);
    if (!(eax12 & edx11 & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max"))) {
        addr_6a10_2:
        if (r8d == 0xffffffff) {
            *reinterpret_cast<int32_t*>(&rcx13) = a8;
            *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx14) = a7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
            *reinterpret_cast<void***>(&rdi15) = edx;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
            eax16 = fun_3bf0(rdi15, rcx, rdx14, rcx13, 0x100);
            if (!eax16) {
                addr_6ac0_9:
                return 1;
            } else {
                rax17 = fun_36d0();
                ebp18 = *reinterpret_cast<void***>(rax17);
                if (ebp18 == 1 || reinterpret_cast<int1_t>(ebp18 == 22)) {
                    *reinterpret_cast<int32_t*>(&rcx19) = a8;
                    *reinterpret_cast<int32_t*>(&rcx19 + 4) = 0;
                    *reinterpret_cast<void***>(&rdi20) = edx;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                    fun_3bf0(rdi20, rcx, 0xffffffff, rcx19, 0x100);
                    *reinterpret_cast<void***>(rax17) = ebp18;
                } else {
                    addr_6a4f_12:
                    quotearg_style(4, rsi, 4, rsi);
                    fun_3800();
                    fun_3b80();
                    goto addr_6a84_13;
                }
            }
        } else {
            *reinterpret_cast<int32_t*>(&rdx21) = a8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx21) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi22) = a7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi22) + 4) = 0;
            *reinterpret_cast<void***>(&rdi23) = r8d;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
            eax24 = fun_3bb0(rdi23, rsi22, rdx21);
            if (!eax24) 
                goto addr_6ac0_9;
            rax25 = fun_36d0();
            r13d26 = *reinterpret_cast<void***>(rax25);
            if (r13d26 == 1) 
                goto addr_6b48_16;
            if (r13d26 != 22) 
                goto addr_6a4f_12;
            addr_6b48_16:
            *reinterpret_cast<int32_t*>(&rdx27) = a8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            *reinterpret_cast<void***>(&rdi28) = r8d;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
            fun_3bb0(rdi28, 0xffffffff, rdx27);
            *reinterpret_cast<void***>(rax25) = r13d26;
        }
    } else {
        rsi29 = r8d;
        *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
        eax30 = qset_acl(rsi, rsi29);
        if (eax30) {
            rax31 = fun_36d0();
            if (*reinterpret_cast<void***>(rax31) != 1 && *reinterpret_cast<void***>(rax31) != 22 || *reinterpret_cast<signed char*>(rdi + 27)) {
                quotearg_style(4, rsi);
                fun_3800();
                fun_3b80();
                goto addr_6a84_13;
            } else {
                goto addr_6a84_13;
            }
        } else {
            goto addr_6a10_2;
        }
    }
    if (!*reinterpret_cast<signed char*>(rdi + 26)) {
        return 0;
    }
    addr_6a84_13:
    return -static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rdi + 50));
}

void** quotearg_n_style();

void** stdout = reinterpret_cast<void**>(0);

void emit_verbose(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rax8;
    void** rax9;
    void** rax10;
    void** rdi11;
    void** rax12;

    rax7 = quotearg_n_style();
    rax8 = quotearg_n_style();
    fun_3b40(1, "%s -> %s", rax8, rax7);
    if (rdx) {
        rax9 = quotearg_style(4, rdx, 4, rdx);
        rax10 = fun_3800();
        fun_3b40(1, rax10, rax9, rax7);
    }
    rdi11 = stdout;
    rax12 = *reinterpret_cast<void***>(rdi11 + 40);
    if (reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi11 + 48))) {
        *reinterpret_cast<void***>(rdi11 + 40) = rax12 + 1;
        *reinterpret_cast<void***>(rax12) = reinterpret_cast<void**>(10);
        return;
    }
}

struct s11 {
    signed char[24] pad24;
    uint64_t f18;
    signed char[4056] pad4088;
    uint64_t fff8;
};

int32_t renameatu();

void record_file(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** seen_file(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** same_nameat();

void** fun_3910(void** rdi, void** rsi, int64_t rdx);

int32_t fun_3980(void** rdi, void** rsi, void** rdx, void** rcx);

void restore_default_fscreatecon_or_die(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t utimecmpat(void** rdi, void** rsi, void* rdx, void** rcx, void** r8);

signed char can_write_any_file(void** rdi, void** rsi);

int32_t fun_3750(int64_t rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9);

signed char overwrite_ok(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9);

void** simple_backup_suffix = reinterpret_cast<void**>(0);

uint32_t fun_3990(void** rdi, void** rsi, void** rdx, ...);

void** subst_suffix(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...);

void** backup_file_rename(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_3b70(void** rdi, void** rsi, void** rdx);

void** quotearg_n_style_colon();

void** remember_copied(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void** src_to_dest_lookup(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s12 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

void** savedir(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s13 {
    void** f0;
    signed char[7] pad8;
    int32_t f8;
    signed char[4] pad16;
    int32_t f10;
    signed char[4] pad24;
    unsigned char f18;
    signed char[7] pad32;
    struct s10* f20;
};

struct s14 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    void** f28;
    signed char[7] pad48;
    void** f30;
};

/* mask.0 */
void** mask_0 = reinterpret_cast<void**>(0xff);

void** fun_3a60();

int32_t set_acl(void** rdi, void** rsi, ...);

void forget_created(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_3c00(int64_t rdi, void** rsi, ...);

signed char create_hard_link(void** rdi, void** esi, void** rdx, void** rcx, void** r8d, void** r9, signed char a7, int32_t a8, unsigned char a9);

void** force_symlinkat(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void** dir_name(void** rdi, void** rsi, ...);

void** open_safer(void** rdi);

void** fun_3ca0(int64_t rdi, void* rsi, ...);

void** openat_safer(int64_t rdi, void** rsi, ...);

int32_t fun_3920(int64_t rdi, void** rsi, ...);

int32_t fun_3720(int64_t rdi, void** rsi, ...);

void** fun_39f0(int64_t rdi, void** rsi, void** rdx, void** rcx);

struct s15 {
    signed char[20] pad20;
    void** f14;
};

int32_t fun_38f0(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fdutimensat(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_3b50(int64_t rdi, ...);

void fdadvise(int64_t rdi);

void** buffer_lcm(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

signed char write_zeros(void** edi, void** rsi);

void** sparse_copy(void** edi, void** esi, void** rdx, void** rcx, void** r8, void** r9d, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12);

int32_t fun_38a0(int64_t rdi, void** rsi, ...);

int32_t fun_3a30(int64_t rdi, void** rsi, ...);

int32_t fun_36a0(int64_t rdi, void** rsi);

void** areadlink_with_size(void** rdi, int64_t rsi);

void** areadlinkat_with_size(int64_t rdi, void** rsi);

void** top_level_src_name = reinterpret_cast<void**>(0);

unsigned char copy_internal(void** rdi, void** rsi, void** edx, void** rcx, void** r8d, void** r9, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12) {
    void** rdx3;
    void** r8_5;
    void* rsp13;
    void* rbp14;
    void** r15_15;
    struct s11* rsp16;
    void** rdi17;
    void** v18;
    void** rsi19;
    void** v20;
    void** v21;
    void** rcx22;
    void** eax23;
    void** v24;
    void** v25;
    void** rbx26;
    void** v27;
    void** v28;
    void** r13_29;
    void** v30;
    void** v31;
    signed char v32;
    void* rax33;
    void* v34;
    uint32_t eax35;
    void** v36;
    int32_t eax37;
    void** rax38;
    void** r12_39;
    void** rsi40;
    void** v41;
    uint32_t ecx42;
    uint1_t zf43;
    void** eax44;
    void** v45;
    void** r12_46;
    void** v47;
    void** v48;
    void** r14_49;
    void** eax50;
    void* rsp51;
    void* rsp52;
    uint32_t eax53;
    uint32_t r14d54;
    uint32_t eax55;
    void** eax56;
    void** rax57;
    struct s11* rsp58;
    int64_t v59;
    int64_t v60;
    int64_t v61;
    int64_t v62;
    int64_t rdx63;
    uint32_t v64;
    uint32_t v65;
    uint32_t v66;
    uint32_t v67;
    uint64_t v68;
    void** eax69;
    uint32_t r12d70;
    uint32_t v71;
    uint32_t v72;
    void** eax73;
    uint32_t v74;
    uint32_t v75;
    uint64_t v76;
    void** rax77;
    uint32_t v78;
    void** eax79;
    int64_t v80;
    int64_t v81;
    void** v82;
    void** v83;
    uint32_t v84;
    int32_t eax85;
    uint32_t v86;
    void** eax87;
    uint32_t v88;
    uint32_t v89;
    void** eax90;
    int32_t eax91;
    int64_t v92;
    int64_t v93;
    uint32_t v94;
    uint32_t v95;
    int64_t v96;
    int64_t v97;
    void** eax98;
    void*** rsp99;
    void** rdx100;
    void* rax101;
    void*** rsp102;
    void** rdx103;
    void*** rsp104;
    void** eax105;
    int64_t v106;
    int64_t v107;
    int64_t v108;
    int64_t v109;
    void*** rsp110;
    void** rax111;
    void*** rsp112;
    struct s11* rsp113;
    int64_t v114;
    int64_t v115;
    void*** rsp116;
    int32_t eax117;
    uint32_t eax118;
    uint32_t v119;
    void*** rsp120;
    signed char al121;
    int64_t rdi122;
    void*** rsp123;
    int32_t eax124;
    void*** rsp125;
    signed char al126;
    uint32_t v127;
    void*** rsp128;
    void*** rsp129;
    void*** rsp130;
    uint32_t r13d131;
    void*** rsp132;
    void** rax133;
    void*** rsp134;
    uint32_t eax135;
    uint32_t v136;
    uint32_t v137;
    uint32_t v138;
    void*** rsp139;
    void** rax140;
    void** r13_141;
    uint32_t v142;
    void*** rsp143;
    void** rax144;
    void*** rsp145;
    void** rax146;
    void*** rsp147;
    void** rax148;
    void** r12_149;
    void*** rsp150;
    void** rax151;
    void*** rsp152;
    uint32_t eax153;
    void** rdi154;
    void*** rsp155;
    int64_t rax156;
    void*** rsp157;
    void** rax158;
    void** rsi159;
    void*** rsp160;
    void** rax161;
    void** rdi162;
    void** rdx163;
    void*** rsp164;
    void** eax165;
    void*** rsp166;
    int64_t v167;
    int64_t v168;
    int64_t rdi169;
    void*** rsp170;
    void** rax171;
    struct s11* rsp172;
    void** r12_173;
    void*** rsp174;
    void** rax175;
    void*** rsp176;
    void*** rsp177;
    void*** rsp178;
    void** r13_179;
    void*** rsp180;
    void** rax181;
    struct s11* rsp182;
    void* rax183;
    struct s11* rsi184;
    uint64_t rcx185;
    void* rsp186;
    void** rax187;
    int64_t* rsp188;
    void** rax189;
    int64_t* rsp190;
    int64_t* rsp191;
    void*** rsp192;
    struct s11* rsp193;
    void*** rsp194;
    void*** rsp195;
    void** rax196;
    void*** rsp197;
    struct s11* rsp198;
    void** rdi199;
    void*** rsp200;
    void** eax201;
    void*** rsp202;
    void** rax203;
    void*** rsp204;
    uint32_t v205;
    void*** rsp206;
    void*** rsp207;
    void*** rsp208;
    void** rax209;
    void*** rsp210;
    void** rax211;
    void*** rsp212;
    void* rdx213;
    uint32_t v214;
    void** rdi215;
    void*** rsp216;
    void** eax217;
    void*** rsp218;
    void** rax219;
    void*** rsp220;
    void*** rsp221;
    void*** rsp222;
    int64_t v223;
    void** v224;
    void** rax225;
    void*** rsp226;
    void** rax227;
    void*** rsp228;
    void** edx229;
    void** rsi230;
    void*** rsp231;
    void** eax232;
    uint32_t v233;
    uint32_t eax234;
    uint32_t eax235;
    void*** rsp236;
    void** eax237;
    void*** rsp238;
    void** eax239;
    void** rax240;
    void** v241;
    void** v242;
    void*** rsp243;
    void** rax244;
    struct s11* rsp245;
    void** r12_246;
    void*** rsp247;
    void** rax248;
    void*** rsp249;
    struct s11* rax250;
    struct s12* rax251;
    struct s12* v252;
    uint32_t v253;
    int64_t rdi254;
    void*** rsp255;
    int32_t eax256;
    struct s11* rsp257;
    void*** rsp258;
    void** rax259;
    struct s11* rsp260;
    void** rdi261;
    void*** rsp262;
    void** eax263;
    void*** rsp264;
    void** rax265;
    uint32_t v266;
    unsigned char v267;
    int64_t rdi268;
    void*** rsp269;
    int32_t eax270;
    void*** rsp271;
    void** rax272;
    void** v273;
    void*** rsp274;
    int64_t v275;
    void*** rsp276;
    void** eax277;
    void*** rsp278;
    void** rax279;
    void*** rsp280;
    void** rax281;
    void*** rsp282;
    void*** rsp283;
    void*** rsp284;
    void** rax285;
    void*** rsp286;
    void** rax287;
    void*** rsp288;
    void** v289;
    uint32_t eax290;
    uint32_t eax291;
    void*** rsp292;
    void** rax293;
    struct s11* rsp294;
    void** v295;
    void*** rsp296;
    void*** rsp297;
    void*** rsp298;
    void*** rsp299;
    void** v300;
    void* v301;
    void** v302;
    void** v303;
    void** r13_304;
    void** v305;
    void** rbx306;
    void** v307;
    void** v308;
    void*** rsp309;
    void** rax310;
    void*** rsp311;
    void** rax312;
    uint32_t eax313;
    void*** rsp314;
    void*** rsp315;
    unsigned char v316;
    void*** rsp317;
    void*** rsp318;
    struct s13* rsp319;
    struct s12** rsp320;
    struct s14* rsp321;
    unsigned char al322;
    void*** rsp323;
    void*** rsp324;
    uint32_t esi325;
    void*** rsp326;
    void** rax327;
    void*** rsp328;
    uint32_t edi329;
    void** rdi330;
    void*** rsp331;
    void** eax332;
    void** rdi333;
    void*** rsp334;
    void*** rdx335;
    int64_t rdi336;
    int64_t v337;
    int64_t v338;
    int64_t v339;
    int64_t v340;
    void*** rsp341;
    int64_t rax342;
    void*** rsp343;
    void** rax344;
    void*** rsp345;
    void** rax346;
    void*** rsp347;
    void*** rsp348;
    void*** rsp349;
    int32_t eax350;
    uint32_t eax351;
    void*** rsp352;
    void** eax353;
    void*** rsp354;
    void** r13d355;
    void*** rsp356;
    void** eax357;
    void*** rsp358;
    int64_t rdi359;
    void*** rsp360;
    int32_t eax361;
    void** rdi362;
    void** rdx363;
    void*** rsp364;
    void** eax365;
    int32_t v366;
    void*** rsp367;
    struct s11* rsp368;
    void*** rsp369;
    int32_t eax370;
    void*** rsp371;
    void** rax372;
    void*** rsp373;
    void*** rsp374;
    void*** rsp375;
    void*** rsp376;
    void*** rsp377;
    void*** rsp378;
    void** rax379;
    int32_t v380;
    void** rdx381;
    int32_t v382;
    int32_t v383;
    int32_t v384;
    void** v385;
    void*** rsp386;
    void** rcx387;
    void*** rsp388;
    void*** rsp389;
    void*** rsp390;
    struct s13* rsp391;
    int32_t eax392;
    void** edx393;
    void*** rsp394;
    int64_t v395;
    int64_t v396;
    int64_t rdi397;
    void** rsi398;
    void*** rsp399;
    int32_t eax400;
    void*** rsp401;
    void*** rsp402;
    void** rax403;
    void*** rsp404;
    void** rax405;
    void*** rsp406;
    void** rax407;
    void*** rsp408;
    void** rax409;
    void** rdi410;
    void*** rsp411;
    void*** rsp412;
    void*** rsp413;
    struct s13* rsp414;
    signed char al415;
    void** rsi416;
    void*** rsp417;
    void** eax418;
    void*** rsp419;
    void** rax420;
    void*** rsp421;
    void** rax422;
    void*** rsp423;
    void** rax424;
    void*** rsp425;
    void*** rsp426;
    void** rax427;
    struct s11* rsp428;
    void** rsi429;
    void*** rsp430;
    int32_t eax431;
    void** rdi432;
    void** rdx433;
    void*** rsp434;
    void** eax435;
    void*** rsp436;
    int64_t v437;
    int64_t v438;
    void*** rsp439;
    void*** rsp440;
    void** rax441;
    void** v442;
    uint32_t eax443;
    void*** rsp444;
    void** eax445;
    struct s11* rsp446;
    void** r13d447;
    void*** rsp448;
    void** rax449;
    void* rsi450;
    int64_t rdi451;
    void*** rsp452;
    void** eax453;
    struct s11* rsp454;
    void*** rsp455;
    void** rax456;
    void*** rsp457;
    void** rax458;
    void*** rsp459;
    void** rax460;
    struct s11* rsp461;
    int64_t v462;
    int64_t v463;
    void*** rsp464;
    void** rax465;
    void*** rsp466;
    void** rax467;
    void** v468;
    int64_t rdi469;
    void*** rsp470;
    void** eax471;
    void** r12d472;
    void*** rsp473;
    void** rax474;
    void*** rsp475;
    int64_t rdi476;
    void*** rsp477;
    int32_t eax478;
    struct s11* rsp479;
    void*** rsp480;
    void** rax481;
    void*** rsp482;
    void** rax483;
    void*** rsp484;
    void** rax485;
    void*** rsp486;
    void*** rsp487;
    void*** rsp488;
    uint32_t eax489;
    int64_t rdi490;
    void*** rsp491;
    int32_t eax492;
    void*** rsp493;
    void** eax494;
    int64_t rdi495;
    void** eax496;
    void*** rsp497;
    void** eax498;
    void*** rsp499;
    void** rax500;
    uint32_t eax501;
    unsigned char dl502;
    void*** rsp503;
    void** rax504;
    void*** rsp505;
    void** rax506;
    int64_t rdi507;
    void*** rsp508;
    void** rax509;
    uint32_t eax510;
    void** v511;
    uint32_t v512;
    void*** rsp513;
    void** rax514;
    struct s15* r8d515;
    void*** rsp516;
    void** rax517;
    void*** rsp518;
    void** rax519;
    void*** rsp520;
    int64_t rdi521;
    void*** rsp522;
    void** eax523;
    void*** rsp524;
    void** rax525;
    void*** rsp526;
    void** rax527;
    int64_t rdi528;
    void*** rsp529;
    int32_t eax530;
    void** eax531;
    void*** rsp532;
    int32_t eax533;
    int64_t rdi534;
    void*** rsp535;
    int32_t eax536;
    void*** rsp537;
    void** rax538;
    void*** rsp539;
    void** rax540;
    void*** rsp541;
    void** rax542;
    void*** rsp543;
    void*** rsp544;
    int32_t eax545;
    uint32_t eax546;
    unsigned char al547;
    int1_t zf548;
    void*** rsp549;
    void** eax550;
    void*** rsp551;
    void** edx552;
    void*** rsp553;
    int32_t eax554;
    uint32_t eax555;
    void** eax556;
    int64_t rdi557;
    void*** rsp558;
    int32_t eax559;
    void*** rsp560;
    void** rax561;
    void*** rsp562;
    void** rax563;
    void*** rsp564;
    void** rax565;
    void*** rsp566;
    void*** rsp567;
    void** rax568;
    void*** rsp569;
    void** rax570;
    void*** rsp571;
    void** rax572;
    void*** rsp573;
    void** rax574;
    void*** rsp575;
    void* rsi576;
    int64_t rdi577;
    void*** rsp578;
    void** eax579;
    void*** rsp580;
    void** rax581;
    struct s11* rsp582;
    int64_t rdi583;
    void*** rsp584;
    int32_t eax585;
    void** v586;
    void** v587;
    void** rax588;
    void*** rsp589;
    void** eax590;
    void*** rsp591;
    void*** rsp592;
    void** eax593;
    void*** rsp594;
    int64_t rdi595;
    void*** rsp596;
    int32_t eax597;
    void*** rsp598;
    void** rax599;
    void*** rsp600;
    void** rax601;
    void*** rsp602;
    void** rax603;
    void*** rsp604;
    void*** rsp605;
    void** rax606;
    void*** rsp607;
    void** rax608;
    void*** rsp609;
    void** r8d610;
    int64_t rdi611;
    void*** rsp612;
    int64_t rdi613;
    int64_t v614;
    void*** rsp615;
    void** rax616;
    void** r8_617;
    void** v618;
    void* rsi619;
    void** rax620;
    int32_t eax621;
    int32_t v622;
    int64_t rdi623;
    void*** rsp624;
    int32_t eax625;
    signed char v626;
    int32_t eax627;
    uint1_t zf628;
    void** v629;
    void** rax630;
    void** v631;
    uint32_t edi632;
    void** v633;
    signed char v634;
    void** r14_635;
    void** v636;
    void** rbx637;
    void*** rsp638;
    void** rax639;
    void** r15_640;
    void** rax641;
    void*** rsp642;
    void** rax643;
    void*** rsp644;
    void** rax645;
    unsigned char v646;
    void*** rsp647;
    signed char al648;
    void*** rsp649;
    void** eax650;
    void*** rsp651;
    void*** rsp652;
    struct s12* rax653;
    void*** rsp654;
    void*** rsp655;
    struct s13* rsp656;
    struct s12** rsp657;
    struct s14* rsp658;
    void** eax659;
    void** edi660;
    void*** rsp661;
    void** rax662;
    void*** rsp663;
    void** rax664;
    void*** rsp665;
    signed char al666;
    uint32_t r8d667;
    void*** rsp668;
    void** rax669;
    struct s11* rsp670;
    int64_t rdi671;
    void*** rsp672;
    int32_t eax673;
    int64_t rdi674;
    void*** rsp675;
    int32_t eax676;
    void*** rsp677;
    void** rax678;
    void** r8b679;
    void*** rsp680;
    void** rax681;
    void*** rsp682;
    void** rax683;
    void*** rsp684;
    int64_t rdi685;
    void*** rsp686;
    int32_t eax687;
    int1_t zf688;
    void** rax689;
    int32_t v690;
    void** v691;
    int32_t v692;
    void** v693;
    void** v694;
    void*** rsp695;
    void** rcx696;
    void*** rsp697;
    void*** rsp698;
    void*** rsp699;
    struct s13* rsp700;
    int32_t eax701;
    void*** rsp702;
    void** rax703;
    void*** rsp704;
    void** rax705;
    void*** rsp706;
    void** rax707;
    void*** rsp708;
    void** r8_709;
    void*** rsp710;
    void** rax711;
    void*** rsp712;
    void** rax713;
    int1_t less_or_equal714;
    void*** rsp715;
    void** rax716;
    void*** rsp717;
    void*** rsp718;
    struct s12* rax719;
    void*** rsp720;
    void*** rsp721;
    struct s13* rsp722;
    struct s12** rsp723;
    struct s14* rsp724;
    void** eax725;
    int64_t v726;
    int64_t v727;
    void*** rsp728;
    void** rax729;
    void*** rsp730;
    void** rax731;
    void** eax732;
    void*** rsp733;
    void** rax734;
    void*** rsp735;
    void** rax736;
    void*** rsp737;
    void*** rsp738;
    void** rax739;
    void*** rsp740;
    void** rax741;
    void*** rsp742;
    void*** rsp743;
    void** eax744;
    int64_t rdi745;
    void*** rsp746;
    int32_t eax747;
    int64_t rdi748;
    void*** rsp749;
    int32_t eax750;
    void** eax751;
    void*** rsp752;
    void** rax753;
    uint32_t eax754;
    void** v755;
    int64_t rdi756;
    void*** rsp757;
    int32_t eax758;
    void*** rsp759;
    void** rax760;
    void*** rsp761;
    void** rax762;
    void*** rsp763;
    int64_t v764;
    void** rax765;
    struct s11* rsp766;
    void*** rsp767;
    void** rax768;
    void*** rsp769;
    void** eax770;
    struct s11* rsp771;
    void*** rsp772;
    uint32_t v773;
    void** v774;
    void*** rsp775;
    void** rax776;
    int64_t rdi777;
    void*** rsp778;
    void** rax779;
    void*** rsp780;
    void*** rsp781;
    void** rax782;
    void*** rsp783;
    void** rax784;
    void*** rsp785;
    void*** rsp786;
    int64_t rax787;
    struct s11* rsp788;
    void*** rsp789;
    void*** rsp790;
    void*** rsp791;
    void** v792;
    int64_t rdx793;
    int32_t v794;
    int64_t rdi795;
    void*** rsp796;
    int32_t eax797;
    void*** rsp798;
    void** eax799;
    void*** rsp800;
    void** rax801;
    void*** rsp802;
    void** rax803;
    void*** rsp804;
    void*** rsp805;
    void** eax806;
    void** eax807;
    uint32_t eax808;
    void*** rsp809;
    void** rdi810;
    void** rdx811;
    void*** rsp812;
    uint32_t eax813;
    int64_t v814;
    void*** rsp815;
    int64_t v816;
    void** v817;
    void** rax818;
    uint64_t v819;
    void*** rsp820;
    int64_t v821;
    void** v822;
    void** rax823;
    void*** rsp824;
    int32_t eax825;
    void*** rsp826;
    void** rax827;
    void*** rsp828;
    void** rax829;
    void*** rsp830;
    void*** rsp831;
    void*** rsp832;
    void*** rsp833;
    void** eax834;
    struct s11* rsp835;
    void*** rsp836;
    void** rax837;
    void*** rsp838;
    void** rax839;
    void*** rsp840;
    void** rax841;
    void*** rsp842;
    void*** rsp843;
    void** eax844;
    void** rax845;
    void*** rsp846;
    void** rax847;
    void*** rsp848;
    void*** rsp849;
    struct s13* rsp850;
    signed char al851;
    void*** rsp852;
    void** rax853;
    void*** rsp854;
    void** rax855;
    void*** rsp856;
    void** rax857;
    void*** rsp858;
    void** rax859;
    void*** rsp860;
    void*** rsp861;
    void*** rsp862;
    void** rax863;
    void*** rsp864;
    void** rax865;
    void*** rsp866;
    void** rax867;
    struct s11* rsp868;
    void** rcx869;
    void** rdx870;
    int64_t rdi871;
    void*** rsp872;
    int32_t eax873;
    void*** rsp874;
    void** rax875;
    unsigned char al876;
    void*** rsp877;
    void** rax878;
    void*** rsp879;
    void*** rsp880;
    void*** rsp881;
    void** rax882;
    void*** rsp883;
    void** rax884;
    void*** rsp885;
    void** rax886;
    void*** rsp887;
    void*** rsp888;
    int64_t v889;
    int64_t v890;
    void** rdi891;
    void*** rsp892;
    void** eax893;
    void*** rsp894;
    void** rax895;
    void*** rsp896;
    void*** rsp897;
    void*** rsp898;
    uint64_t v899;
    int1_t zf900;
    uint32_t v901;
    int64_t rdi902;
    void*** rsp903;
    int32_t eax904;
    void*** rsp905;
    void** rax906;
    void*** rsp907;
    void** rax908;
    void*** rsp909;
    void*** rsp910;
    uint32_t eax911;
    void*** rsp912;
    void** rax913;
    void*** rsp914;
    void** rax915;
    void*** rsp916;
    void*** rsp917;
    signed char al918;
    void*** rsp919;
    int64_t v920;
    void** v921;
    void** rax922;
    void** rax923;
    void*** rsp924;
    void** rax925;
    void*** rsp926;
    void*** rsp927;
    struct s13* rsp928;
    signed char al929;
    void** rsi930;
    void*** rsp931;
    void*** rsp932;
    void*** rsp933;

    rdx3 = edx;
    r8_5 = r8d;
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp14 = rsp13;
    r15_15 = rdi;
    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(rsp13) - 8 - 8 - 8 - 8 - 8 - 0x3a8);
    rdi17 = a12;
    v18 = rsi;
    rsi19 = a10;
    v20 = rcx;
    v21 = a7;
    rcx22 = a11;
    eax23 = a9;
    v24 = rdx3;
    v25 = r9;
    rbx26 = a8;
    v27 = eax23;
    v28 = rsi19;
    r13_29 = *reinterpret_cast<void***>(rbx26 + 64);
    v30 = rcx22;
    v31 = rdi17;
    v32 = *reinterpret_cast<signed char*>(&eax23);
    rax33 = g28;
    v34 = rax33;
    *reinterpret_cast<void***>(rcx22) = reinterpret_cast<void**>(0);
    eax35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
    *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax35);
    if (!*reinterpret_cast<unsigned char*>(&eax35)) {
        *reinterpret_cast<unsigned char*>(&v36) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r8_5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r8_5 == 0)));
        if (!r13_29) 
            goto addr_70ce_3; else 
            goto addr_6ecc_4;
    }
    if (reinterpret_cast<signed char>(r13_29) < reinterpret_cast<signed char>(0)) {
        rcx22 = v20;
        rdx3 = v24;
        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
        rsi19 = r15_15;
        rdi17 = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
        eax37 = renameatu();
        rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (eax37) {
            rax38 = fun_36d0();
            rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
            r13_29 = *reinterpret_cast<void***>(rax38);
        } else {
            r8_5 = reinterpret_cast<void**>(1);
            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
            *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
            goto addr_70ce_3;
        }
    }
    rdx3 = v31;
    *reinterpret_cast<unsigned char*>(&v36) = reinterpret_cast<uint1_t>(r13_29 == 0);
    r8_5 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v36)));
    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
    *reinterpret_cast<void***>(rdx3) = r8_5;
    if (r13_29) {
        addr_6ecc_4:
        if (r13_29 != 17 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 2)) {
            r12_39 = r15_15;
            rsi40 = r15_15;
            rdi17 = reinterpret_cast<void**>(0xffffff9c);
            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
        } else {
            v41 = reinterpret_cast<void**>(0);
            goto addr_6f31_12;
        }
    } else {
        addr_70ce_3:
        if (*reinterpret_cast<signed char*>(rbx26 + 63)) {
            v41 = reinterpret_cast<void**>(0);
            r13_29 = reinterpret_cast<void**>(0);
            goto addr_6f31_12;
        } else {
            rdi17 = v24;
            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
            r12_39 = v18;
            r13_29 = reinterpret_cast<void**>(0);
            rsi40 = v20;
        }
    }
    ecx42 = 0;
    zf43 = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(rbx26 + 4) == 2);
    r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
    v41 = r8_5;
    *reinterpret_cast<unsigned char*>(&ecx42) = zf43;
    rdx3 = r9;
    rcx22 = reinterpret_cast<void**>(ecx42 << 8);
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    eax44 = fun_3cb0(rdi17, rsi40, rdx3);
    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    r8_5 = v41;
    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
    rsi19 = r12_39;
    if (eax44) 
        goto addr_7c30_16;
    v41 = v45;
    if ((reinterpret_cast<unsigned char>(v45) & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000 || (r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 56))), *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&r12_46))) {
        addr_6f31_12:
        if (!v27 || (rdi17 = *reinterpret_cast<void***>(rbx26 + 80), rdi17 == 0)) {
            addr_6f81_18:
            if (*reinterpret_cast<signed char*>(rbx26 + 4) == 4) {
                v47 = reinterpret_cast<void**>(1);
            } else {
                if (*reinterpret_cast<signed char*>(rbx26 + 4) != 3 || !v27) {
                    v47 = reinterpret_cast<void**>(0);
                } else {
                    v47 = reinterpret_cast<void**>(1);
                    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r8_5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r8_5 == 0))) {
                        v48 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                        goto addr_7210_24;
                    }
                }
            }
        } else {
            r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
            if ((reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000 || *reinterpret_cast<void***>(rbx26)) {
                addr_6f68_26:
                rdx3 = r9;
                rsi19 = r15_15;
                record_file(rdi17, rsi19, rdx3, rcx22);
                rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                r8_5 = r8_5;
                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                goto addr_6f81_18;
            } else {
                v47 = r8_5;
                eax50 = seen_file(rdi17, r15_15, r9, rcx22);
                rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                r12_46 = eax50;
                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                if (*reinterpret_cast<signed char*>(&eax50)) {
                    quotearg_style(4, r15_15, 4, r15_15);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                    goto addr_922c_29;
                } else {
                    rdi17 = *reinterpret_cast<void***>(rbx26 + 80);
                    r9 = r9;
                    r8_5 = v47;
                    goto addr_6f68_26;
                }
            }
        }
    } else {
        quotearg_style(4, r15_15, 4, r15_15);
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (*reinterpret_cast<signed char*>(rbx26 + 25)) {
            goto addr_922c_29;
        } else {
            fun_3800();
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
            goto addr_7bce_34;
        }
    }
    v48 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r8_5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r8_5 == 0))) 
        goto addr_6fc0_36;
    if (r13_29 == 17 && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 2)) {
        *reinterpret_cast<uint32_t*>(&r14_49) = 0;
        r13_29 = reinterpret_cast<void**>(0);
        goto addr_8213_39;
    }
    eax53 = reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000);
    if (eax53 == 0x8000) 
        goto addr_7b20_41;
    *reinterpret_cast<unsigned char*>(&r14_49) = reinterpret_cast<uint1_t>(eax53 == 0xa000);
    *reinterpret_cast<unsigned char*>(&eax53) = reinterpret_cast<uint1_t>(eax53 == 0x4000);
    r14d54 = *reinterpret_cast<uint32_t*>(&r14_49) | eax53;
    eax55 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 20)) ^ 1;
    *reinterpret_cast<unsigned char*>(&r14_49) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r14d54) | *reinterpret_cast<unsigned char*>(&eax55));
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_730c_43;
    addr_7b20_41:
    *reinterpret_cast<uint32_t*>(&r14_49) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_730c_43;
    *reinterpret_cast<uint32_t*>(&r14_49) = *reinterpret_cast<unsigned char*>(rbx26 + 58);
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_730c_43;
    *reinterpret_cast<uint32_t*>(&r14_49) = *reinterpret_cast<unsigned char*>(rbx26 + 23);
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_730c_43;
    rdi17 = *reinterpret_cast<void***>(rbx26);
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    if (rdi17) {
        *reinterpret_cast<uint32_t*>(&r14_49) = 1;
        goto addr_730c_43;
    }
    *reinterpret_cast<uint32_t*>(&r14_49) = *reinterpret_cast<unsigned char*>(rbx26 + 21);
    if (*reinterpret_cast<unsigned char*>(&r14_49)) {
        addr_730c_43:
        r8_5 = reinterpret_cast<void**>(0x100);
        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
    } else {
        if (r8_5) 
            goto addr_7b6b_50;
    }
    rsi19 = v20;
    rdi17 = v24;
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    rdx3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
    rcx22 = r8_5;
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    eax56 = fun_3cb0(rdi17, rsi19, rdx3, rdi17, rsi19, rdx3);
    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax56) {
        rax57 = fun_36d0();
        rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (*reinterpret_cast<void***>(rax57) == 40) {
            if (*reinterpret_cast<unsigned char*>(rbx26 + 22)) {
                addr_7b72_54:
                v48 = reinterpret_cast<void**>(0);
                *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                if (!reinterpret_cast<int1_t>(r13_29 == 17)) 
                    goto addr_6fc0_36;
            } else {
                goto addr_7adc_56;
            }
        } else {
            if (*reinterpret_cast<void***>(rax57) == 2) {
                addr_7b6b_50:
                *reinterpret_cast<unsigned char*>(&v36) = 1;
                goto addr_7b72_54;
            } else {
                addr_7adc_56:
                quotearg_style(4, v18, 4, v18);
                rsp58 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                goto addr_7afc_59;
            }
        }
    }
    if (*reinterpret_cast<void***>(rbx26 + 8) == 2) 
        goto addr_8210_61;
    if (v59 != v60 || v61 != v62) {
        if (*reinterpret_cast<signed char*>(rbx26 + 4) != 2) 
            goto addr_8210_61;
        *reinterpret_cast<void***>(&rdx63) = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
        r9 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
    } else {
        r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 23)));
        if (*reinterpret_cast<unsigned char*>(&r13_29)) 
            goto addr_8213_39;
        if (*reinterpret_cast<signed char*>(rbx26 + 4) == 2) 
            goto addr_9a50_67; else 
            goto addr_87ac_68;
    }
    addr_7363_69:
    r12_46 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
    r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
    if ((v64 & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000 || (v65 & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000) {
        rsi19 = *reinterpret_cast<void***>(rbx26);
        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
        if (!rsi19) {
            rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))));
            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
            if (!*reinterpret_cast<signed char*>(&rcx22) && !*reinterpret_cast<unsigned char*>(rbx26 + 21)) {
                if ((v66 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000) 
                    goto addr_92f2_73; else 
                    goto addr_92bf_74;
            }
            if ((v67 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000) 
                goto addr_8210_61;
            if (!*reinterpret_cast<unsigned char*>(&rdx63)) 
                goto addr_960d_77;
            if (v68 > 1) 
                goto addr_9ee8_79;
        } else {
            if (*reinterpret_cast<unsigned char*>(&rdx63)) {
                rcx22 = v20;
                rsi19 = r15_15;
                rdi17 = reinterpret_cast<void**>(0xffffff9c);
                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                r13_29 = reinterpret_cast<void**>(0);
                eax69 = same_nameat();
                rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                r12d70 = reinterpret_cast<unsigned char>(eax69) ^ 1;
                goto addr_97e9_82;
            } else {
                if (*reinterpret_cast<void***>(rbx26 + 24)) 
                    goto addr_8210_61;
                if (*reinterpret_cast<signed char*>(rbx26 + 4) == 2) 
                    goto addr_8210_61;
                if ((v71 & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000) 
                    goto addr_8210_61;
                if ((v72 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000) 
                    goto addr_8210_61; else 
                    goto addr_73d6_87;
            }
        }
    } else {
        rcx22 = v20;
        rsi19 = r15_15;
        rdi17 = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
        eax73 = same_nameat();
        rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        r13_29 = eax73;
        if (*reinterpret_cast<signed char*>(&eax73)) 
            goto addr_73d6_87;
        if (*reinterpret_cast<void***>(rbx26)) 
            goto addr_8213_39;
        r9 = r9;
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        if (!r9) 
            goto addr_8213_39;
        r13_29 = reinterpret_cast<void**>(1);
        r12d70 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) ^ 1;
        goto addr_97e9_82;
    }
    addr_960d_77:
    if ((v74 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000) {
        addr_92d8_92:
        if (!*reinterpret_cast<signed char*>(&rcx22) || ((v75 & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000 || (v76 <= 1 || (rdi17 = r15_15, rax77 = fun_3910(rdi17, rsi19, rdx63), rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8), r8_5 = r8_5, rax77 == 0)))) {
            addr_92f2_73:
            if (!*reinterpret_cast<unsigned char*>(rbx26 + 58)) 
                goto addr_930d_93;
            if ((v78 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000) 
                goto addr_8210_61;
        } else {
            rcx22 = v20;
            rsi19 = rax77;
            eax79 = same_nameat();
            rdi17 = rax77;
            r13_29 = reinterpret_cast<void**>(0);
            r12d70 = reinterpret_cast<unsigned char>(eax79) ^ 1;
            fun_3670(rdi17, rdi17);
            rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8 - 8 + 8);
            goto addr_97e9_82;
        }
    } else {
        addr_9621_96:
        if (v80 != v81) 
            goto addr_8210_61;
        if (v82 != v83) 
            goto addr_8210_61; else 
            goto addr_963d_98;
    }
    addr_930d_93:
    if (*reinterpret_cast<signed char*>(rbx26 + 4) != 2) 
        goto addr_73d6_87;
    if ((v84 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000) {
        rsi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30);
        rdi17 = r15_15;
        eax85 = fun_3a70(rdi17, rsi19, rdi17, rsi19);
        rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (eax85) {
            goto addr_8210_61;
        }
    } else {
        __asm__("movdqa xmm5, [r8]");
        __asm__("movaps [rbp-0xd0], xmm5");
        __asm__("movdqa xmm6, [r8+0x10]");
        __asm__("movaps [rbp-0xc0], xmm6");
        __asm__("movdqa xmm7, [r8+0x20]");
        __asm__("movaps [rbp-0xb0], xmm7");
        __asm__("movdqa xmm5, [r8+0x30]");
        __asm__("movaps [rbp-0xa0], xmm5");
        __asm__("movdqa xmm6, [r8+0x40]");
        __asm__("movaps [rbp-0x90], xmm6");
        __asm__("movdqa xmm7, [r8+0x50]");
        __asm__("movaps [rbp-0x80], xmm7");
        __asm__("movdqa xmm5, [r8+0x60]");
        __asm__("movaps [rbp-0x70], xmm5");
        __asm__("movdqa xmm6, [r8+0x70]");
        __asm__("movaps [rbp-0x60], xmm6");
        __asm__("movdqa xmm7, [r8+0x80]");
        __asm__("movaps [rbp-0x50], xmm7");
    }
    if ((v86 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000) 
        goto addr_a607_104;
    __asm__("movdqa xmm5, [r12]");
    __asm__("movaps [rbp-0x160], xmm5");
    __asm__("movdqa xmm6, [r12+0x10]");
    __asm__("movaps [rbp-0x150], xmm6");
    __asm__("movdqa xmm7, [r12+0x20]");
    __asm__("movaps [rbp-0x140], xmm7");
    __asm__("movdqa xmm5, [r12+0x30]");
    __asm__("movaps [rbp-0x130], xmm5");
    __asm__("movdqa xmm6, [r12+0x40]");
    __asm__("movaps [rbp-0x120], xmm6");
    __asm__("movdqa xmm7, [r12+0x50]");
    __asm__("movaps [rbp-0x110], xmm7");
    __asm__("movdqa xmm5, [r12+0x60]");
    __asm__("movaps [rbp-0x100], xmm5");
    __asm__("movdqa xmm6, [r12+0x70]");
    __asm__("movaps [rbp-0xf0], xmm6");
    __asm__("movdqa xmm7, [r12+0x80]");
    __asm__("movaps [rbp-0xe0], xmm7");
    goto addr_942b_106;
    addr_97e9_82:
    if (!*reinterpret_cast<signed char*>(&r12d70)) 
        goto addr_73d6_87;
    goto addr_8213_39;
    addr_963d_98:
    r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 23)));
    if (*reinterpret_cast<unsigned char*>(&r13_29)) {
        goto addr_8213_39;
    }
    addr_92d4_109:
    rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))));
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    goto addr_92d8_92;
    addr_9ee8_79:
    rcx22 = v20;
    *reinterpret_cast<void***>(&rdx63) = v24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
    rsi19 = r15_15;
    rdi17 = reinterpret_cast<void**>(0xffffff9c);
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    eax87 = same_nameat();
    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    r8_5 = r8_5;
    r13_29 = eax87;
    if (!*reinterpret_cast<signed char*>(&eax87)) {
        r12d70 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) ^ 1;
        goto addr_97e9_82;
    } else {
        if ((v88 & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000) {
            addr_92bf_74:
            if ((v89 & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000) 
                goto addr_9621_96; else 
                goto addr_92d4_109;
        } else {
            goto addr_92d4_109;
        }
    }
    addr_9a50_67:
    *reinterpret_cast<void***>(&rdx63) = reinterpret_cast<void**>(1);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
    r9 = reinterpret_cast<void**>(1);
    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
    goto addr_7363_69;
    addr_87ac_68:
    r12_46 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0);
    rdi17 = v24;
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    rcx22 = reinterpret_cast<void**>(0x100);
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    rsi19 = v20;
    eax90 = fun_3cb0(rdi17, rsi19, r12_46, rdi17, rsi19, r12_46);
    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax90) 
        goto addr_8213_39;
    r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30);
    rdi17 = r15_15;
    rsi19 = r8_5;
    eax91 = fun_3980(rdi17, rsi19, r12_46, 0x100);
    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax91) 
        goto addr_8213_39;
    *reinterpret_cast<void***>(&rdx63) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
    r8_5 = r8_5;
    if (v92 == v93) 
        goto addr_880f_115;
    addr_8820_116:
    if ((v94 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000 && ((v95 & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000 && *reinterpret_cast<unsigned char*>(rbx26 + 21))) {
        goto addr_8213_39;
    }
    addr_880f_115:
    *reinterpret_cast<unsigned char*>(&rdx63) = reinterpret_cast<uint1_t>(v96 == v97);
    goto addr_8820_116;
    addr_922c_29:
    fun_3800();
    rsp52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
    addr_7bce_34:
    fun_3b80();
    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp52) - 8 + 8);
    goto addr_7433_118;
    addr_7446_119:
    eax98 = r12_46;
    return *reinterpret_cast<unsigned char*>(&eax98);
    while (1) {
        addr_8310_120:
        if (*reinterpret_cast<void***>(rbx26 + 24)) 
            goto addr_886a_121;
        if ((reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) 
            goto addr_8341_123; else 
            goto addr_832c_124;
        addr_8303_125:
        rsp99 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp99 = reinterpret_cast<void**>(0x8308);
        restore_default_fscreatecon_or_die(rdi17, rsi19, rdx100, rcx22, r8_5, r9);
        rsp16 = reinterpret_cast<struct s11*>(rsp99 + 8);
        continue;
        addr_871d_126:
        goto addr_8303_125;
        while (1) {
            addr_8908_127:
            if (v31) {
                *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
            }
            while (1) {
                addr_74b0_129:
                r12_46 = reinterpret_cast<void**>(1);
                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                while (1) {
                    addr_7433_118:
                    rax101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v34) - reinterpret_cast<int64_t>(g28));
                    if (!rax101) 
                        goto addr_7446_119;
                    rsp102 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp102 = reinterpret_cast<void**>(0xa607);
                    fun_3840();
                    rsp16 = reinterpret_cast<struct s11*>(rsp102 + 8);
                    addr_a607_104:
                    rsi19 = v20;
                    rdi17 = v24;
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rcx22 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                    rdx103 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0);
                    rsp104 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp104 = reinterpret_cast<void**>(0xa622);
                    eax105 = fun_3cb0(rdi17, rsi19, rdx103, rdi17, rsi19, rdx103);
                    rsp16 = reinterpret_cast<struct s11*>(rsp104 + 8);
                    if (eax105) {
                        goto addr_8210_61;
                    }
                    addr_942b_106:
                    if (v106 != v107 || v108 != v109) {
                        addr_8210_61:
                        r13_29 = reinterpret_cast<void**>(0);
                    } else {
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 23)) {
                            addr_73d6_87:
                            rsp110 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp110 = reinterpret_cast<void**>(0x73ec);
                            rax111 = quotearg_n_style();
                            rbx26 = rax111;
                            rsp112 = rsp110 + 8 - 8;
                            *rsp112 = reinterpret_cast<void**>(0x73fe);
                            quotearg_n_style();
                            rsp113 = reinterpret_cast<struct s11*>(rsp112 + 8);
                            goto addr_740d_133;
                        } else {
                            *reinterpret_cast<unsigned char*>(&r13_29) = reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_46 + 24)) & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000);
                        }
                    }
                    addr_8213_39:
                    if (!*reinterpret_cast<signed char*>(rbx26 + 59)) 
                        goto addr_8310_120;
                    if ((reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                        if (!*reinterpret_cast<void***>(rbx26 + 24)) 
                            goto addr_8341_123;
                    } else {
                        r8_5 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        if (*reinterpret_cast<signed char*>(rbx26 + 31) && (r8_5 = reinterpret_cast<void**>(1), *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0, !!*reinterpret_cast<void***>(rbx26 + 24))) {
                            r8_5 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            r8_5 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v114 != v115)));
                        }
                        r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
                        rsi19 = v20;
                        rdi17 = v24;
                        *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                        rcx22 = r9;
                        rsp116 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp116 = reinterpret_cast<void**>(0x8280);
                        eax117 = utimecmpat(rdi17, rsi19, reinterpret_cast<int64_t>(rbp14) - 0x1f0, rcx22, r8_5);
                        rsp16 = reinterpret_cast<struct s11*>(rsp116 + 8);
                        if (eax117 < 0) 
                            goto addr_91b0_140; else 
                            goto addr_8288_141;
                    }
                    addr_886a_121:
                    if (*reinterpret_cast<void***>(rbx26 + 8) == 2) 
                        goto addr_8908_127;
                    if (*reinterpret_cast<void***>(rbx26 + 8) == 3) 
                        goto addr_88d9_143;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 4)) 
                        goto addr_8341_123;
                    if (!*reinterpret_cast<signed char*>(rbx26 + 61)) 
                        goto addr_8341_123;
                    eax118 = v119;
                    rdx3 = reinterpret_cast<void**>(eax118 & reinterpret_cast<uint32_t>(fun_f000));
                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                    if (rdx3 == 0xa000) {
                        if (*reinterpret_cast<unsigned char*>(&r13_29)) 
                            goto addr_a1d4_148;
                    } else {
                        rsp120 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp120 = reinterpret_cast<void**>(0x88ad);
                        al121 = can_write_any_file(rdi17, rsi19);
                        rsp16 = reinterpret_cast<struct s11*>(rsp120 + 8);
                        if (al121 || (*reinterpret_cast<void***>(&rdi122) = v24, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi122) + 4) = 0, rcx22 = reinterpret_cast<void**>(0x200), *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0, rsi19 = v20, rsp123 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp123 = reinterpret_cast<void**>(0x88d1), eax124 = fun_3750(rdi122, rsi19, 2, 0x200, r8_5, r9), rsp16 = reinterpret_cast<struct s11*>(rsp123 + 8), eax124 == 0)) {
                            addr_8341_123:
                            if (*reinterpret_cast<unsigned char*>(&r13_29)) 
                                goto addr_74b0_129;
                        } else {
                            addr_88d9_143:
                            rcx22 = v20;
                            r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                            rsi19 = v18;
                            rsp125 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp125 = reinterpret_cast<void**>(0x88fc);
                            al126 = overwrite_ok(rbx26, rsi19, v24, rcx22, r8_5, r9);
                            rsp16 = reinterpret_cast<struct s11*>(rsp125 + 8);
                            if (al126) 
                                goto addr_8341_123; else 
                                goto addr_8904_150;
                        }
                        eax118 = v127;
                        rdx3 = reinterpret_cast<void**>(eax118 & reinterpret_cast<uint32_t>(fun_f000));
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        if (rdx3 == 0x4000) {
                            rdx3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000));
                            *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                            if (rdx3 == 0x4000) {
                                addr_83d7_153:
                                r12_46 = *reinterpret_cast<void***>(rbx26);
                                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                goto addr_83da_154;
                            } else {
                                addr_8f68_155:
                                if (!*reinterpret_cast<void***>(rbx26 + 24) || (r12_46 = *reinterpret_cast<void***>(rbx26), *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0, r12_46 == 0)) {
                                    rsp128 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp128 = reinterpret_cast<void**>(0x98fb);
                                    quotearg_style(4, v18, 4, v18);
                                    rsp129 = rsp128 + 8 - 8;
                                    *rsp129 = reinterpret_cast<void**>(0x9911);
                                    fun_3800();
                                    r12_46 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                    rsp130 = rsp129 + 8 - 8;
                                    *rsp130 = reinterpret_cast<void**>(0x9925);
                                    fun_3b80();
                                    rsp16 = reinterpret_cast<struct s11*>(rsp130 + 8);
                                    continue;
                                }
                            }
                        }
                    }
                    r13d131 = reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000);
                    if (r13d131 == 0x4000) {
                        if (!*reinterpret_cast<void***>(rbx26 + 24) || (r12_46 = *reinterpret_cast<void***>(rbx26), *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0, r12_46 == 0)) {
                            rsp132 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp132 = reinterpret_cast<void**>(0x989f);
                            rax133 = quotearg_n_style();
                            rbx26 = rax133;
                            rsp134 = rsp132 + 8 - 8;
                            *rsp134 = reinterpret_cast<void**>(0x98b5);
                            quotearg_n_style();
                            rsp113 = reinterpret_cast<struct s11*>(rsp134 + 8);
                        } else {
                            if (!v27) 
                                goto addr_8f7e_161;
                            goto addr_838b_163;
                        }
                    } else {
                        r12_46 = *reinterpret_cast<void***>(rbx26);
                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                        if (!v27) {
                            addr_83da_154:
                            eax135 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                            if (*reinterpret_cast<unsigned char*>(&eax135)) {
                                if ((v136 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) 
                                    goto addr_95d6_166;
                                eax118 = v137;
                                goto addr_972d_168;
                            } else {
                                if (r12_46) 
                                    goto addr_95a5_170; else 
                                    goto addr_83ef_171;
                            }
                        } else {
                            addr_838b_163:
                            if (r12_46 == 3) {
                                if (*reinterpret_cast<void***>(rbx26 + 24)) {
                                    addr_8f7e_161:
                                    rdx3 = reinterpret_cast<void**>(v138 & reinterpret_cast<uint32_t>(fun_f000));
                                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                                    if (rdx3 == 0x4000) 
                                        goto addr_972d_168; else 
                                        goto addr_8f96_173;
                                } else {
                                    addr_95a5_170:
                                    rsp139 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp139 = reinterpret_cast<void**>(0x95ad);
                                    rax140 = last_component(r15_15, rsi19, rdx3, rcx22, r15_15, rsi19, rdx3, rcx22);
                                    rsp16 = reinterpret_cast<struct s11*>(rsp139 + 8);
                                    r13_141 = rax140;
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax140) == 46)) {
                                        addr_8fd2_174:
                                        if ((v142 & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                                            addr_8489_175:
                                            v48 = reinterpret_cast<void**>(0);
                                            r13_29 = reinterpret_cast<void**>(17);
                                            goto addr_6fc0_36;
                                        } else {
                                            addr_8fe8_176:
                                            if (r12_46 == 3 || ((rsp143 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp143 = reinterpret_cast<void**>(0x8ffa), rax144 = fun_3820(r13_141, r13_141), v36 = rax144, rsp145 = rsp143 + 8 - 8, *rsp145 = reinterpret_cast<void**>(0x900d), rax146 = last_component(v20, rsi19, rdx3, rcx22, v20, rsi19, rdx3, rcx22), rsp147 = rsp145 + 8 - 8, *rsp147 = reinterpret_cast<void**>(0x901c), rax148 = fun_3820(rax146, rax146), r12_149 = simple_backup_suffix, rsp150 = rsp147 + 8 - 8, *rsp150 = reinterpret_cast<void**>(0x9032), rax151 = fun_3820(r12_149, r12_149), rsp16 = reinterpret_cast<struct s11*>(rsp150 + 8), !reinterpret_cast<int1_t>(v36 == reinterpret_cast<unsigned char>(rax151) + reinterpret_cast<unsigned char>(rax148))) || ((v36 = rax148, rsp152 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp152 = reinterpret_cast<void**>(0x905f), eax153 = fun_3990(r13_141, rax146, rax148, r13_141, rax146, rax148), rsp16 = reinterpret_cast<struct s11*>(rsp152 + 8), !!eax153) || ((rdi154 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_141) + reinterpret_cast<unsigned char>(v36)), rsp155 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp155 = reinterpret_cast<void**>(0x9077), rax156 = fun_39e0(rdi154, r12_149, v36, rcx22, r8_5, rdi154, r12_149, v36, rcx22, r8_5), rsp16 = reinterpret_cast<struct s11*>(rsp155 + 8), !!*reinterpret_cast<int32_t*>(&rax156)) || ((rsp157 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp157 = reinterpret_cast<void**>(0x908a), rax158 = fun_3820(v20, v20), rsi159 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(rax158)), rsp160 = rsp157 + 8 - 8, *rsp160 = reinterpret_cast<void**>(0x909a), rax161 = subst_suffix(v20, rsi159, r12_149, rcx22, r8_5, r9, v20, rsi159, r12_149, rcx22), rdi162 = v24, *reinterpret_cast<int32_t*>(&rdi162 + 4) = 0, rcx22 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0, rdx163 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30), rsp164 = rsp160 + 8 - 8, *rsp164 = reinterpret_cast<void**>(0x90b4), eax165 = fun_3cb0(rdi162, rax161, rdx163, rdi162, rax161, rdx163), rsp166 = rsp164 + 8 - 8, *rsp166 = reinterpret_cast<void**>(0x90bf), fun_3670(rax161, rax161), rsp16 = reinterpret_cast<struct s11*>(rsp166 + 8), !!eax165) || (v167 != v106 || v168 != v108)))))) {
                                                rdx3 = *reinterpret_cast<void***>(rbx26);
                                                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                                                *reinterpret_cast<void***>(&rdi169) = v24;
                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi169) + 4) = 0;
                                                rsp170 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                *rsp170 = reinterpret_cast<void**>(0x90ef);
                                                rax171 = backup_file_rename(rdi169, v20, rdx3, rcx22, r8_5, r9);
                                                rsp172 = reinterpret_cast<struct s11*>(rsp170 + 8);
                                                r12_173 = rax171;
                                                if (!rax171) {
                                                    rsp174 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp172) - 8);
                                                    *rsp174 = reinterpret_cast<void**>(0xaf73);
                                                    rax175 = fun_36d0();
                                                    rsp16 = reinterpret_cast<struct s11*>(rsp174 + 8);
                                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax175) == 2)) {
                                                        rsp176 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                        *rsp176 = reinterpret_cast<void**>(0xafbf);
                                                        quotearg_style(4, v18, 4, v18);
                                                        rsp58 = reinterpret_cast<struct s11*>(rsp176 + 8);
                                                        addr_7afc_59:
                                                        rsp177 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp58) - 8);
                                                        *rsp177 = reinterpret_cast<void**>(0x7b03);
                                                        fun_3800();
                                                        r12_46 = reinterpret_cast<void**>(0);
                                                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                                        rsp178 = rsp177 + 8 - 8;
                                                        *rsp178 = reinterpret_cast<void**>(0x7b19);
                                                        fun_3b80();
                                                        rsp16 = reinterpret_cast<struct s11*>(rsp178 + 8);
                                                        continue;
                                                    } else {
                                                        *reinterpret_cast<unsigned char*>(&v36) = 1;
                                                        r13_29 = reinterpret_cast<void**>(17);
                                                        v48 = reinterpret_cast<void**>(0);
                                                        goto addr_6fc0_36;
                                                    }
                                                } else {
                                                    r13_179 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(v18));
                                                    rsp180 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp172) - 8);
                                                    *rsp180 = reinterpret_cast<void**>(0x910a);
                                                    rax181 = fun_3820(rax171, rax171);
                                                    rsp182 = reinterpret_cast<struct s11*>(rsp180 + 8);
                                                    r8_5 = rax181 + 1;
                                                    rax183 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_179) + reinterpret_cast<unsigned char>(rax181) + 24);
                                                    rsi184 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp182) - (reinterpret_cast<uint64_t>(rax183) & 0xfffffffffffff000));
                                                    rcx185 = reinterpret_cast<uint64_t>(rax183) & 0xfffffffffffffff0;
                                                    if (rsp182 != rsi184) {
                                                        do {
                                                            rsp182 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp182) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
                                                            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp182) + reinterpret_cast<int64_t>("strcmp")) = 0x910a;
                                                        } while (rsp182 != rsi184);
                                                    }
                                                    rcx22 = reinterpret_cast<void**>(*reinterpret_cast<uint32_t*>(&rcx185) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max"));
                                                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                                    rsp186 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp182) - reinterpret_cast<unsigned char>(rcx22));
                                                    if (rcx22) {
                                                        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp186) + reinterpret_cast<unsigned char>(rcx22) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp186) + reinterpret_cast<unsigned char>(rcx22) - 8);
                                                    }
                                                    rax187 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp186) + 15 & 0xfffffffffffffff0);
                                                    r13_29 = reinterpret_cast<void**>(17);
                                                    v48 = rax187;
                                                    rsp188 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp186) - 8);
                                                    *rsp188 = 0x9183;
                                                    rax189 = fun_3b70(rax187, v18, r13_179);
                                                    rdx3 = r8_5;
                                                    rsp190 = rsp188 + 1 - 1;
                                                    *rsp190 = 0x9195;
                                                    fun_3a80(rax189, r12_173, rdx3);
                                                    rsp191 = rsp190 + 1 - 1;
                                                    *rsp191 = 0x919d;
                                                    fun_3670(r12_173, r12_173);
                                                    rsp16 = reinterpret_cast<struct s11*>(rsp191 + 1);
                                                    *reinterpret_cast<unsigned char*>(&v36) = 1;
                                                    goto addr_6fc0_36;
                                                }
                                            } else {
                                                if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                                                    rsp192 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                    *rsp192 = reinterpret_cast<void**>(0xab11);
                                                    fun_3800();
                                                    rsp193 = reinterpret_cast<struct s11*>(rsp192 + 8);
                                                } else {
                                                    rsp194 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                    *rsp194 = reinterpret_cast<void**>(0xaac5);
                                                    fun_3800();
                                                    rsp193 = reinterpret_cast<struct s11*>(rsp194 + 8);
                                                }
                                                rsp195 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp193) - 8);
                                                *rsp195 = reinterpret_cast<void**>(0xaada);
                                                rax196 = quotearg_n_style();
                                                rbx26 = rax196;
                                                rsp197 = rsp195 + 8 - 8;
                                                *rsp197 = reinterpret_cast<void**>(0xaaf0);
                                                quotearg_n_style();
                                                rsp198 = reinterpret_cast<struct s11*>(rsp197 + 8);
                                                r8_5 = rbx26;
                                                goto addr_741d_190;
                                            }
                                        }
                                    } else {
                                        eax135 = 0;
                                        goto addr_8fad_192;
                                    }
                                }
                            } else {
                                rdi199 = *reinterpret_cast<void***>(rbx26 + 72);
                                rsi19 = v20;
                                rdx3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                                rsp200 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp200 = reinterpret_cast<void**>(0x83ac);
                                eax201 = seen_file(rdi199, rsi19, rdx3, rcx22, rdi199, rsi19, rdx3, rcx22);
                                rsp16 = reinterpret_cast<struct s11*>(rsp200 + 8);
                                if (*reinterpret_cast<signed char*>(&eax201)) {
                                    rsp202 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp202 = reinterpret_cast<void**>(0x9ebe);
                                    rax203 = quotearg_n_style();
                                    rbx26 = rax203;
                                    rsp204 = rsp202 + 8 - 8;
                                    *rsp204 = reinterpret_cast<void**>(0x9ed4);
                                    quotearg_n_style();
                                    rsp113 = reinterpret_cast<struct s11*>(rsp204 + 8);
                                } else {
                                    if (r13d131 == 0x4000) 
                                        goto addr_83d7_153;
                                    eax118 = v205;
                                    rdx3 = reinterpret_cast<void**>(eax118 & reinterpret_cast<uint32_t>(fun_f000));
                                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                                    if (rdx3 == 0x4000) 
                                        goto addr_8f68_155; else 
                                        goto addr_83d7_153;
                                }
                            }
                        }
                    }
                    addr_740d_133:
                    rsp206 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp113) - 8);
                    *rsp206 = reinterpret_cast<void**>(0x7414);
                    fun_3800();
                    rsp198 = reinterpret_cast<struct s11*>(rsp206 + 8);
                    r8_5 = rbx26;
                    addr_741d_190:
                    rsp207 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp198) - 8);
                    *rsp207 = reinterpret_cast<void**>(0x7428);
                    fun_3b80();
                    rsp16 = reinterpret_cast<struct s11*>(rsp207 + 8);
                    goto addr_7430_197;
                    addr_972d_168:
                    if ((eax118 & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                        addr_95d6_166:
                        if (!r12_46) 
                            goto addr_8489_175;
                    } else {
                        if (r12_46) {
                            addr_8f96_173:
                            rsp208 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp208 = reinterpret_cast<void**>(0x8f9e);
                            rax209 = last_component(r15_15, rsi19, rdx3, rcx22, r15_15, rsi19, rdx3, rcx22);
                            rsp16 = reinterpret_cast<struct s11*>(rsp208 + 8);
                            r13_141 = rax209;
                            eax135 = 1;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_141) == 46)) 
                                goto addr_8fe8_176; else 
                                goto addr_8fad_192;
                        } else {
                            rsp210 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp210 = reinterpret_cast<void**>(0x9759);
                            rax211 = quotearg_n_style_colon();
                            rbx26 = rax211;
                            rsp212 = rsp210 + 8 - 8;
                            *rsp212 = reinterpret_cast<void**>(0x976b);
                            quotearg_n_style_colon();
                            rsp113 = reinterpret_cast<struct s11*>(rsp212 + 8);
                            goto addr_740d_133;
                        }
                    }
                    goto addr_8f96_173;
                    addr_8fad_192:
                    *reinterpret_cast<int32_t*>(&rdx213) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx213) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rdx213) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r13_141 + 1) == 46);
                    rdx3 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_141) + reinterpret_cast<uint64_t>(rdx213) + 1)));
                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                    if (!*reinterpret_cast<signed char*>(&rdx3) || *reinterpret_cast<signed char*>(&rdx3) == 47) {
                        addr_83ef_171:
                        r13_29 = reinterpret_cast<void**>(17);
                        v48 = reinterpret_cast<void**>(0);
                        rdx3 = reinterpret_cast<void**>(v214 & reinterpret_cast<uint32_t>(fun_f000));
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r12_46) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdx3 == 0x4000)) | *reinterpret_cast<unsigned char*>(&eax135));
                        if (*reinterpret_cast<unsigned char*>(&r12_46)) {
                            addr_6fc0_36:
                            if (!v27) 
                                goto addr_6fcd_201;
                            addr_7210_24:
                            if (!*reinterpret_cast<void***>(rbx26 + 72)) 
                                goto addr_6fcd_201;
                            r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))));
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&r12_46)) 
                                goto addr_6fcd_201;
                        } else {
                            if (*reinterpret_cast<unsigned char*>(rbx26 + 21)) 
                                goto addr_8447_204;
                            if (!*reinterpret_cast<unsigned char*>(rbx26 + 49)) 
                                goto addr_6fc0_36; else 
                                goto addr_842f_206;
                        }
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&eax135)) 
                            goto addr_8fe8_176; else 
                            goto addr_8fd2_174;
                    }
                    rcx22 = *reinterpret_cast<void***>(rbx26);
                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                    if (rcx22) {
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                            if (!r13_29) {
                                goto addr_74d8_212;
                            }
                        }
                    }
                    rdx3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
                        goto addr_7243_214;
                    r14_49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30);
                    rdi215 = v24;
                    *reinterpret_cast<int32_t*>(&rdi215 + 4) = 0;
                    rcx22 = reinterpret_cast<void**>(0x100);
                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                    rdx3 = r14_49;
                    rsp216 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp216 = reinterpret_cast<void**>(0x8949);
                    eax217 = fun_3cb0(rdi215, v20, rdx3, rdi215, v20, rdx3);
                    rsp16 = reinterpret_cast<struct s11*>(rsp216 + 8);
                    if (eax217) {
                        addr_6fcd_201:
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 60) || *reinterpret_cast<void***>(rbx26 + 24)) {
                            addr_6fdd_216:
                            if (!r13_29) {
                                if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                                    addr_74d8_212:
                                    r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v36)));
                                    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                                    *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0;
                                } else {
                                    addr_7466_218:
                                    if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                                        rsp218 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp218 = reinterpret_cast<void**>(0x8a03);
                                        rax219 = fun_3800();
                                        rsp220 = rsp218 + 8 - 8;
                                        *rsp220 = reinterpret_cast<void**>(0x8a12);
                                        fun_3b40(1, rax219, 5, rcx22, 1, rax219, 5, rcx22);
                                        rsp221 = rsp220 + 8 - 8;
                                        *rsp221 = reinterpret_cast<void**>(0x8a28);
                                        emit_verbose(r15_15, v18, v48, rcx22, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s11*>(rsp221 + 8);
                                        goto addr_7470_220;
                                    }
                                }
                            } else {
                                if (*reinterpret_cast<unsigned char*>(rbx26 + 56) && (reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                                    if (v27) {
                                        rsp222 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp222 = reinterpret_cast<void**>(0x85ef);
                                        rax225 = remember_copied(v20, v223, v224, rcx22, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s11*>(rsp222 + 8);
                                        r14_49 = rax225;
                                    } else {
                                        rsp226 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp226 = reinterpret_cast<void**>(0x7029);
                                        rax227 = src_to_dest_lookup(v223, v224, v224, rcx22, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s11*>(rsp226 + 8);
                                        r14_49 = rax227;
                                    }
                                    if (!r14_49) 
                                        goto addr_7090_226; else 
                                        goto addr_7031_227;
                                }
                            }
                        } else {
                            if ((reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) {
                                rdx3 = v48;
                                rsp228 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp228 = reinterpret_cast<void**>(0x799c);
                                emit_verbose(r15_15, v18, rdx3, rcx22, r8_5, r9);
                                rsp16 = reinterpret_cast<struct s11*>(rsp228 + 8);
                                goto addr_6fdd_216;
                            }
                        }
                    } else {
                        rdx3 = r14_49;
                        goto addr_7243_214;
                    }
                    addr_74e3_231:
                    r12_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max"));
                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                    if (*reinterpret_cast<signed char*>(rbx26 + 57)) {
                        r12_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 16)) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max"));
                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                    }
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 29)) {
                        edx229 = v41;
                        r8_5 = rbx26;
                        rcx22 = r13_29;
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        rsi230 = v18;
                        rsp231 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp231 = reinterpret_cast<void**>(0x758b);
                        eax232 = set_process_security_ctx(r15_15, rsi230);
                        rsp16 = reinterpret_cast<struct s11*>(rsp231 + 8);
                        if (!*reinterpret_cast<signed char*>(&eax232)) 
                            goto addr_7430_197;
                        v233 = reinterpret_cast<unsigned char>(r12_46) & 63;
                        eax234 = reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000);
                        *reinterpret_cast<uint32_t*>(&v31) = eax234;
                        if (eax234 != 0x4000) 
                            goto addr_75bb_236;
                    } else {
                        eax235 = reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000);
                        *reinterpret_cast<uint32_t*>(&v31) = eax235;
                        if (eax235 == 0x4000) {
                            r8_5 = rbx26;
                            rsp236 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp236 = reinterpret_cast<void**>(0x7a43);
                            eax237 = set_process_security_ctx(r15_15, v18, r15_15, v18);
                            rsp16 = reinterpret_cast<struct s11*>(rsp236 + 8);
                            if (!*reinterpret_cast<signed char*>(&eax237)) 
                                goto addr_7430_197;
                            v233 = reinterpret_cast<unsigned char>(r12_46) & 18;
                        } else {
                            edx229 = v41;
                            r8_5 = rbx26;
                            rcx22 = r13_29;
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            rsi230 = v18;
                            rsp238 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp238 = reinterpret_cast<void**>(0x753f);
                            eax239 = set_process_security_ctx(r15_15, rsi230);
                            rsp16 = reinterpret_cast<struct s11*>(rsp238 + 8);
                            v233 = 0;
                            if (!*reinterpret_cast<signed char*>(&eax239)) {
                                goto addr_7430_197;
                            }
                        }
                    }
                    rax240 = v21;
                    rdx100 = v241;
                    rcx22 = v242;
                    if (rax240) {
                        do {
                            if (*reinterpret_cast<void***>(rax240 + 8) != rdx100) 
                                continue;
                            if (*reinterpret_cast<void***>(rax240 + 16) == rcx22) 
                                break;
                            rax240 = *reinterpret_cast<void***>(rax240);
                        } while (rax240);
                        goto addr_7e38_246;
                    } else {
                        goto addr_7e38_246;
                    }
                    rsp243 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp243 = reinterpret_cast<void**>(0x7aa5);
                    rax244 = quotearg_style(4, r15_15, 4, r15_15);
                    rsp245 = reinterpret_cast<struct s11*>(rsp243 + 8);
                    r12_246 = rax244;
                    addr_8678_249:
                    rsp247 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp245) - 8);
                    *rsp247 = reinterpret_cast<void**>(0x867f);
                    rax248 = fun_3800();
                    rcx22 = r12_246;
                    rsi19 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rdi17 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rdx100 = rax248;
                    rsp249 = rsp247 + 8 - 8;
                    *rsp249 = reinterpret_cast<void**>(0x8690);
                    fun_3b80();
                    rsp16 = reinterpret_cast<struct s11*>(rsp249 + 8);
                    goto addr_7d80_250;
                    addr_7e38_246:
                    rax250 = rsp16;
                    if (rsp16 != rax250) {
                        do {
                            rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
                            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp16) + reinterpret_cast<int64_t>("strcmp")) = 0x758b;
                        } while (rsp16 != rax250);
                    }
                    rsp16 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(rsp16) - 32);
                    rsp16->f18 = 0x758b;
                    rax251 = reinterpret_cast<struct s12*>(reinterpret_cast<uint64_t>(rsp16) + 15 & 0xfffffffffffffff0);
                    v252 = rax251;
                    rax251->f0 = v21;
                    rax251->f8 = rdx100;
                    rax251->f10 = rcx22;
                    if (*reinterpret_cast<unsigned char*>(&v36) || (v253 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) {
                        *reinterpret_cast<void***>(&rdi254) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi254) + 4) = 0;
                        rsp255 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp255 = reinterpret_cast<void**>(0x7ebd);
                        eax256 = fun_3a50(rdi254, v20);
                        rsp257 = reinterpret_cast<struct s11*>(rsp255 + 8);
                        if (eax256) {
                            rsp258 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp257) - 8);
                            *rsp258 = reinterpret_cast<void**>(0x8f19);
                            rax259 = quotearg_style(4, v18, 4, v18);
                            rsp260 = reinterpret_cast<struct s11*>(rsp258 + 8);
                            r13_29 = rax259;
                        } else {
                            rdi261 = v24;
                            *reinterpret_cast<int32_t*>(&rdi261 + 4) = 0;
                            rcx22 = reinterpret_cast<void**>(0x100);
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                            rsp262 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp257) - 8);
                            *rsp262 = reinterpret_cast<void**>(0x7ee3);
                            eax263 = fun_3cb0(rdi261, v20, rdx100, rdi261, v20, rdx100);
                            rsp16 = reinterpret_cast<struct s11*>(rsp262 + 8);
                            if (eax263) {
                                rsp264 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp264 = reinterpret_cast<void**>(0x96e3);
                                rax265 = quotearg_style(4, v18, 4, v18);
                                rsp260 = reinterpret_cast<struct s11*>(rsp264 + 8);
                                r13_29 = rax265;
                            } else {
                                if ((v266 & 0x1c0) == 0x1c0) {
                                    v267 = 0;
                                    goto addr_7f37_259;
                                }
                                rcx22 = reinterpret_cast<void**>(0x100);
                                *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                rdx100 = reinterpret_cast<void**>(v266 | 0x1c0);
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                *reinterpret_cast<void***>(&rdi268) = v24;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi268) + 4) = 0;
                                rsp269 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp269 = reinterpret_cast<void**>(0x7f28);
                                eax270 = fun_3ad0(rdi268, v20, rdi268, v20);
                                rsp16 = reinterpret_cast<struct s11*>(rsp269 + 8);
                                v267 = 1;
                                if (eax270) {
                                    rsp271 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp271 = reinterpret_cast<void**>(0x9b73);
                                    rax272 = quotearg_style(4, v18, 4, v18);
                                    rsp260 = reinterpret_cast<struct s11*>(rsp271 + 8);
                                    r13_29 = rax272;
                                } else {
                                    addr_7f37_259:
                                    if (!*reinterpret_cast<void***>(v28)) {
                                        rdx100 = v273;
                                        rsp274 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp274 = reinterpret_cast<void**>(0x9675);
                                        remember_copied(v20, v275, rdx100, 0x100, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s11*>(rsp274 + 8);
                                        *reinterpret_cast<void***>(v28) = reinterpret_cast<void**>(1);
                                        goto addr_7f47_263;
                                    }
                                }
                            }
                        }
                    } else {
                        if (!*reinterpret_cast<void***>(rbx26 + 40) && !*reinterpret_cast<signed char*>(rbx26 + 51)) {
                            goto addr_91ea_266;
                        }
                        rdi17 = v18;
                        rsi19 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rdx100 = rbx26;
                        rsp276 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp276 = reinterpret_cast<void**>(0x91dc);
                        eax277 = set_file_security_ctx(rdi17, rdi17);
                        rsp16 = reinterpret_cast<struct s11*>(rsp276 + 8);
                        if (*reinterpret_cast<signed char*>(&eax277) || !*reinterpret_cast<unsigned char*>(rbx26 + 52)) {
                            addr_91ea_266:
                            v267 = 0;
                            v233 = 0;
                            goto addr_7f90_268;
                        } else {
                            addr_7d80_250:
                            if (*reinterpret_cast<signed char*>(rbx26 + 51)) 
                                goto addr_8303_125; else 
                                goto addr_7d8a_269;
                        }
                    }
                    addr_8f28_270:
                    rsp278 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp260) - 8);
                    *rsp278 = reinterpret_cast<void**>(0x8f2f);
                    rax279 = fun_3800();
                    rsp280 = rsp278 + 8 - 8;
                    *rsp280 = reinterpret_cast<void**>(0x8f37);
                    rax281 = fun_36d0();
                    rcx22 = r13_29;
                    rdx100 = rax279;
                    rdi17 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rsi19 = *reinterpret_cast<void***>(rax281);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp282 = rsp280 + 8 - 8;
                    *rsp282 = reinterpret_cast<void**>(0x8f48);
                    fun_3b80();
                    rsp16 = reinterpret_cast<struct s11*>(rsp282 + 8);
                    goto addr_7d80_250;
                    addr_7f47_263:
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                        if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                            rdx100 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsp283 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp283 = reinterpret_cast<void**>(0x9811);
                            emit_verbose(r15_15, v18, 0, 0x100, r8_5, r9);
                            rsp16 = reinterpret_cast<struct s11*>(rsp283 + 8);
                        } else {
                            rsp284 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp284 = reinterpret_cast<void**>(0x7f68);
                            rax285 = quotearg_style(4, v18, 4, v18);
                            r12_46 = rax285;
                            rsp286 = rsp284 + 8 - 8;
                            *rsp286 = reinterpret_cast<void**>(0x7f7e);
                            rax287 = fun_3800();
                            rdx100 = r12_46;
                            rsp288 = rsp286 + 8 - 8;
                            *rsp288 = reinterpret_cast<void**>(0x7f90);
                            fun_3b40(1, rax287, rdx100, 0x100, 1, rax287, rdx100, 0x100);
                            rsp16 = reinterpret_cast<struct s11*>(rsp288 + 8);
                        }
                    }
                    addr_7f90_268:
                    *reinterpret_cast<unsigned char*>(&r12_46) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v25)) & *reinterpret_cast<unsigned char*>(rbx26 + 28));
                    if (*reinterpret_cast<unsigned char*>(&r12_46)) {
                        if (*reinterpret_cast<void***>(v25) != v289) {
                            addr_81d1_275:
                            if (v27) {
                                eax290 = v267;
                                r9 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                r13_29 = reinterpret_cast<void**>(0);
                                r8_5 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                v32 = *reinterpret_cast<signed char*>(&eax290);
                            } else {
                                eax291 = v267;
                                r13_29 = reinterpret_cast<void**>(0);
                                v32 = *reinterpret_cast<signed char*>(&eax291);
                                goto addr_7708_278;
                            }
                        } else {
                            goto addr_7fa8_280;
                        }
                    } else {
                        addr_7fa8_280:
                        __asm__("movdqu xmm0, [rbx]");
                        __asm__("movdqu xmm1, [rbx+0x10]");
                        __asm__("movdqu xmm2, [rbx+0x20]");
                        __asm__("movdqu xmm3, [rbx+0x30]");
                        __asm__("movdqu xmm4, [rbx+0x40]");
                        __asm__("movaps [rbp-0x300], xmm0");
                        __asm__("movaps [rbp-0x2f0], xmm1");
                        __asm__("movaps [rbp-0x2e0], xmm2");
                        __asm__("movaps [rbp-0x2d0], xmm3");
                        __asm__("movaps [rbp-0x2c0], xmm4");
                        rsp292 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp292 = reinterpret_cast<void**>(0x7ffb);
                        rax293 = savedir(r15_15, 2, rdx100, rcx22, r8_5, r9);
                        rsp294 = reinterpret_cast<struct s11*>(rsp292 + 8);
                        v295 = rax293;
                        if (!rax293) {
                            rsp296 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                            *rsp296 = reinterpret_cast<void**>(0xaedc);
                            quotearg_style(4, r15_15, 4, r15_15);
                            rsp297 = rsp296 + 8 - 8;
                            *rsp297 = reinterpret_cast<void**>(0xaef2);
                            fun_3800();
                            rsp298 = rsp297 + 8 - 8;
                            *rsp298 = reinterpret_cast<void**>(0xaefa);
                            fun_36d0();
                            r12_46 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            rsp299 = rsp298 + 8 - 8;
                            *rsp299 = reinterpret_cast<void**>(0xaf0e);
                            fun_3b80();
                            rsp16 = reinterpret_cast<struct s11*>(rsp299 + 8);
                            goto addr_81d1_275;
                        } else {
                            if (*reinterpret_cast<signed char*>(rbx26 + 4) == 3) {
                                *reinterpret_cast<int32_t*>(&v300 + 4) = 2;
                            }
                            if (!*reinterpret_cast<void***>(v295)) {
                                *reinterpret_cast<unsigned char*>(&v21) = 0;
                                r12_46 = reinterpret_cast<void**>(1);
                                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            } else {
                                v301 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(v18));
                                v302 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf8);
                                v25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf6);
                                v47 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf7);
                                v303 = r13_29;
                                r13_304 = v295;
                                v305 = rbx26;
                                rbx306 = v30;
                                *reinterpret_cast<unsigned char*>(&v21) = 0;
                                v307 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd00);
                                v31 = reinterpret_cast<void**>(1);
                                v308 = r15_15;
                                v30 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
                                do {
                                    rsp309 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                                    *rsp309 = reinterpret_cast<void**>(0x80ff);
                                    rax310 = file_name_concat(v308, r13_304);
                                    rsp311 = rsp309 + 8 - 8;
                                    *rsp311 = reinterpret_cast<void**>(0x8113);
                                    rax312 = file_name_concat(v18, r13_304);
                                    r9 = v30;
                                    r8_5 = v303;
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                    eax313 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v28));
                                    rsp314 = rsp311 + 8 - 8;
                                    *rsp314 = v302;
                                    rsp315 = rsp314 - 8;
                                    *rsp315 = v25;
                                    v316 = *reinterpret_cast<unsigned char*>(&eax313);
                                    rsp317 = rsp315 - 8;
                                    *rsp317 = v47;
                                    rsp318 = rsp317 - 8;
                                    *rsp318 = reinterpret_cast<void**>(0);
                                    rsp319 = reinterpret_cast<struct s13*>(rsp318 - 8);
                                    rsp319->f0 = v307;
                                    rsp320 = reinterpret_cast<struct s12**>(reinterpret_cast<uint64_t>(rsp319) - 8);
                                    *rsp320 = v252;
                                    rsp321 = reinterpret_cast<struct s14*>(rsp320 - 1);
                                    rsp321->f0 = 0x8170;
                                    al322 = copy_internal(rax310, rax312, v24, reinterpret_cast<unsigned char>(rax312) + reinterpret_cast<uint64_t>(v301), r8_5, r9, rsp321->f8, rsp321->f10, 0, rsp321->f20, rsp321->f28, rsp321->f30);
                                    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                                    *reinterpret_cast<void***>(rbx306) = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx306))));
                                    v31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) & al322);
                                    rsp323 = &rsp321->f8 + 48 - 8;
                                    *rsp323 = reinterpret_cast<void**>(0x818d);
                                    fun_3670(rax312, rax312);
                                    rsp324 = rsp323 + 8 - 8;
                                    *rsp324 = reinterpret_cast<void**>(0x8195);
                                    fun_3670(rax310, rax310);
                                    rsp294 = reinterpret_cast<struct s11*>(rsp324 + 8);
                                    if (!1) 
                                        break;
                                    esi325 = v316;
                                    *reinterpret_cast<unsigned char*>(&v21) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&v21) | *reinterpret_cast<unsigned char*>(&esi325));
                                    rsp326 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                                    *rsp326 = reinterpret_cast<void**>(0x80de);
                                    rax327 = fun_3820(r13_304, r13_304);
                                    rsp294 = reinterpret_cast<struct s11*>(rsp326 + 8);
                                    r13_304 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_304) + reinterpret_cast<unsigned char>(rax327) + 1);
                                } while (*reinterpret_cast<void***>(r13_304));
                                r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31)));
                                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                r15_15 = v308;
                                rbx26 = v305;
                            }
                            rsp328 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                            *rsp328 = reinterpret_cast<void**>(0x81c0);
                            fun_3670(v295, v295);
                            rsp16 = reinterpret_cast<struct s11*>(rsp328 + 8);
                            edi329 = *reinterpret_cast<unsigned char*>(&v21);
                            *reinterpret_cast<void***>(v28) = *reinterpret_cast<void***>(&edi329);
                            goto addr_81d1_275;
                        }
                    }
                    addr_767f_291:
                    if (*reinterpret_cast<void***>(rbx26 + 72) && (rdi330 = v24, *reinterpret_cast<int32_t*>(&rdi330 + 4) = 0, r14_49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30), v31 = r8_5, v27 = r9, rsp331 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp331 = reinterpret_cast<void**>(0x76b5), eax332 = fun_3cb0(rdi330, v20, r14_49, rdi330, v20, r14_49), rsp16 = reinterpret_cast<struct s11*>(rsp331 + 8), r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v27))), *reinterpret_cast<int32_t*>(&r9 + 4) = 0, r8_5 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31))), *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0, !eax332)) {
                        rdi333 = *reinterpret_cast<void***>(rbx26 + 72);
                        rsp334 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp334 = reinterpret_cast<void**>(0x76dc);
                        record_file(rdi333, v20, r14_49, 0x100);
                        rsp16 = reinterpret_cast<struct s11*>(rsp334 + 8);
                        r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v27)));
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        r8_5 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31)));
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    }
                    addr_76f0_293:
                    if (!*reinterpret_cast<unsigned char*>(rbx26 + 23)) 
                        goto addr_76ff_294;
                    if (r8_5) 
                        continue;
                    addr_76ff_294:
                    if (r9) 
                        continue;
                    addr_7708_278:
                    if (!*reinterpret_cast<signed char*>(rbx26 + 31)) 
                        goto addr_77c0_296;
                    rdx335 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30);
                    *reinterpret_cast<void***>(&rdi336) = v24;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi336) + 4) = 0;
                    v108 = v337;
                    v106 = v338;
                    v339 = v340;
                    rsp341 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp341 = reinterpret_cast<void**>(0x776a);
                    rax342 = fun_36b0(rdi336, v20, rdx335, rdi336, v20, rdx335);
                    rsp16 = reinterpret_cast<struct s11*>(rsp341 + 8);
                    if (!*reinterpret_cast<int32_t*>(&rax342)) 
                        goto addr_77c0_296;
                    rsp343 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp343 = reinterpret_cast<void**>(0x777f);
                    rax344 = quotearg_style(4, v18, 4, v18);
                    v27 = rax344;
                    rsp345 = rsp343 + 8 - 8;
                    *rsp345 = reinterpret_cast<void**>(0x7799);
                    rax346 = fun_3800();
                    r14_49 = rax346;
                    rsp347 = rsp345 + 8 - 8;
                    *rsp347 = reinterpret_cast<void**>(0x77a1);
                    fun_36d0();
                    rsp348 = rsp347 + 8 - 8;
                    *rsp348 = reinterpret_cast<void**>(0x77b6);
                    fun_3b80();
                    rsp16 = reinterpret_cast<struct s11*>(rsp348 + 8);
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 50)) 
                        goto addr_7430_197;
                    addr_77c0_296:
                    if (*reinterpret_cast<unsigned char*>(&r13_29)) 
                        continue;
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 29)) 
                        goto addr_77cf_300;
                    addr_784e_301:
                    if (0xff0000000000ff & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) {
                        r8_5 = v41;
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        rsp349 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp349 = reinterpret_cast<void**>(0x9280);
                        eax350 = copy_acl(r15_15, 0xffffffff, v18, 0xffffffff, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s11*>(rsp349 + 8);
                        if (!eax350) 
                            continue;
                    } else {
                        if (*reinterpret_cast<signed char*>(rbx26 + 57)) {
                            goto addr_957d_306;
                        } else {
                            eax351 = *reinterpret_cast<unsigned char*>(&v36);
                            if (*reinterpret_cast<unsigned char*>(&eax351) & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 32))) {
                                rbx26 = mask_0;
                                *reinterpret_cast<int32_t*>(&rbx26 + 4) = 0;
                                if ((reinterpret_cast<unsigned char>(v41) & 0x7000) != 0x4000) {
                                }
                                if (rbx26 == 0xffffffff) {
                                    rsp352 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp352 = reinterpret_cast<void**>(0xa1b7);
                                    eax353 = fun_3a60();
                                    rbx26 = eax353;
                                    *reinterpret_cast<int32_t*>(&rbx26 + 4) = 0;
                                    mask_0 = eax353;
                                    rsp354 = rsp352 + 8 - 8;
                                    *rsp354 = reinterpret_cast<void**>(0xa1c6);
                                    fun_3a60();
                                    rsp16 = reinterpret_cast<struct s11*>(rsp354 + 8);
                                }
                                goto addr_957d_306;
                            } else {
                                if (v233) {
                                    r13d355 = mask_0;
                                    if (r13d355 == 0xffffffff) {
                                        rsp356 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp356 = reinterpret_cast<void**>(0xa23e);
                                        eax357 = fun_3a60();
                                        r13d355 = eax357;
                                        mask_0 = eax357;
                                        rsp358 = rsp356 + 8 - 8;
                                        *rsp358 = reinterpret_cast<void**>(0xa24e);
                                        fun_3a60();
                                        rsp16 = reinterpret_cast<struct s11*>(rsp358 + 8);
                                    }
                                    v233 = v233 & ~reinterpret_cast<unsigned char>(r13d355);
                                    if (!v233) 
                                        goto addr_788a_317;
                                } else {
                                    addr_788a_317:
                                    if (!v32) 
                                        continue; else 
                                        goto addr_7897_318;
                                }
                                if (v32 == 1) {
                                    addr_7897_318:
                                    *reinterpret_cast<void***>(&rdi359) = v24;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi359) + 4) = 0;
                                    rsp360 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp360 = reinterpret_cast<void**>(0x78ba);
                                    eax361 = fun_3ad0(rdi359, v20, rdi359, v20);
                                    rsp16 = reinterpret_cast<struct s11*>(rsp360 + 8);
                                    if (!eax361) 
                                        continue; else 
                                        goto addr_78c2_320;
                                } else {
                                    if (!*reinterpret_cast<unsigned char*>(&v36) || (rdi362 = v24, *reinterpret_cast<int32_t*>(&rdi362 + 4) = 0, rdx363 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10), rsp364 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp364 = reinterpret_cast<void**>(0x7c1b), eax365 = fun_3cb0(rdi362, v20, rdx363, rdi362, v20, rdx363), rsp16 = reinterpret_cast<struct s11*>(rsp364 + 8), eax365 == 0)) {
                                        if (!(v233 & reinterpret_cast<uint32_t>(~v366))) {
                                            continue;
                                        }
                                    } else {
                                        rsi19 = v18;
                                        addr_7c30_16:
                                        rsp367 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp367 = reinterpret_cast<void**>(0x7c3a);
                                        quotearg_style(4, rsi19, 4, rsi19);
                                        rsp368 = reinterpret_cast<struct s11*>(rsp367 + 8);
                                        goto addr_7c49_325;
                                    }
                                }
                            }
                        }
                    }
                    addr_7902_326:
                    if (!*reinterpret_cast<unsigned char*>(rbx26 + 50)) 
                        continue;
                    goto addr_7430_197;
                    addr_957d_306:
                    rsp369 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp369 = reinterpret_cast<void**>(0x958e);
                    eax370 = set_acl(v18, 0xffffffff, v18, 0xffffffff);
                    rsp16 = reinterpret_cast<struct s11*>(rsp369 + 8);
                    if (!eax370) 
                        continue;
                    goto addr_7430_197;
                    addr_78c2_320:
                    rsp371 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp371 = reinterpret_cast<void**>(0x78d3);
                    rax372 = quotearg_style(4, v18, 4, v18);
                    r14_49 = rax372;
                    rsp373 = rsp371 + 8 - 8;
                    *rsp373 = reinterpret_cast<void**>(0x78e9);
                    fun_3800();
                    rsp374 = rsp373 + 8 - 8;
                    *rsp374 = reinterpret_cast<void**>(0x78f1);
                    fun_36d0();
                    rsp375 = rsp374 + 8 - 8;
                    *rsp375 = reinterpret_cast<void**>(0x7902);
                    fun_3b80();
                    rsp16 = reinterpret_cast<struct s11*>(rsp375 + 8);
                    goto addr_7902_326;
                    addr_7c49_325:
                    rsp376 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp368) - 8);
                    *rsp376 = reinterpret_cast<void**>(0x7c50);
                    fun_3800();
                    rsp377 = rsp376 + 8 - 8;
                    *rsp377 = reinterpret_cast<void**>(0x7c58);
                    fun_36d0();
                    rsp378 = rsp377 + 8 - 8;
                    *rsp378 = reinterpret_cast<void**>(0x7c69);
                    fun_3b80();
                    rsp16 = reinterpret_cast<struct s11*>(rsp378 + 8);
                    goto addr_7430_197;
                    addr_77cf_300:
                    *reinterpret_cast<int32_t*>(&rax379) = v380;
                    *reinterpret_cast<int32_t*>(&rax379 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx381) = v382;
                    *reinterpret_cast<int32_t*>(&rdx381 + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&v36) || (v383 != *reinterpret_cast<int32_t*>(&rax379) || v384 != *reinterpret_cast<int32_t*>(&rdx381))) {
                        r9 = v385;
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        rsp386 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp386 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                        *reinterpret_cast<uint32_t*>(&rcx387) = *reinterpret_cast<unsigned char*>(&v36);
                        *reinterpret_cast<int32_t*>(&rcx387 + 4) = 0;
                        r8_5 = reinterpret_cast<void**>(0xffffffff);
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        rsp388 = rsp386 - 8;
                        *rsp388 = rcx387;
                        rsp389 = rsp388 - 8;
                        *rsp389 = rdx381;
                        rsp390 = rsp389 - 8;
                        *rsp390 = rax379;
                        rsp391 = reinterpret_cast<struct s13*>(rsp390 - 8);
                        rsp391->f0 = reinterpret_cast<void**>(0x782b);
                        eax392 = set_owner_isra_0(rbx26, v18, v24, v20, 0xffffffff, r9, rsp391->f8, rsp391->f10, rsp391->f18, rsp391->f20);
                        rsp16 = reinterpret_cast<struct s11*>(&rsp391->f8 + 8);
                        if (eax392 == -1) 
                            goto addr_7430_197;
                        edx393 = v41;
                        *reinterpret_cast<unsigned char*>(&edx393 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx393 + 1) & 0xf1);
                        if (eax392) {
                            edx393 = v41;
                        }
                        v41 = edx393;
                        goto addr_784e_301;
                    } else {
                        goto addr_784e_301;
                    }
                    addr_7d8a_269:
                    if (!r14_49) {
                        rsp394 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp394 = reinterpret_cast<void**>(0x8a6b);
                        forget_created(v395, v396, rdx100, rcx22, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s11*>(rsp394 + 8);
                    }
                    addr_7d93_335:
                    if (!v48) {
                        addr_7430_197:
                        r12_46 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                        continue;
                    } else {
                        *reinterpret_cast<void***>(&rdi397) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi397) + 4) = 0;
                        rsi398 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(v18)) + reinterpret_cast<unsigned char>(v48));
                        rsp399 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp399 = reinterpret_cast<void**>(0x7dc4);
                        eax400 = fun_3c00(rdi397, rsi398, rdi397, rsi398);
                        rsp16 = reinterpret_cast<struct s11*>(rsp399 + 8);
                        if (eax400) {
                            rsp401 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp401 = reinterpret_cast<void**>(0x8a41);
                            quotearg_style(4, v18, 4, v18);
                            rsp368 = reinterpret_cast<struct s11*>(rsp401 + 8);
                            goto addr_7c49_325;
                        } else {
                            if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                                rsp402 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp402 = reinterpret_cast<void**>(0x7dec);
                                rax403 = quotearg_n_style();
                                rsp404 = rsp402 + 8 - 8;
                                *rsp404 = reinterpret_cast<void**>(0x7e02);
                                rax405 = quotearg_n_style();
                                rsp406 = rsp404 + 8 - 8;
                                *rsp406 = reinterpret_cast<void**>(0x7e18);
                                rax407 = fun_3800();
                                rsp408 = rsp406 + 8 - 8;
                                *rsp408 = reinterpret_cast<void**>(0x7e2d);
                                fun_3b40(1, rax407, rax405, rax403, 1, rax407, rax405, rax403);
                                rsp16 = reinterpret_cast<struct s11*>(rsp408 + 8);
                                goto addr_7430_197;
                            }
                        }
                    }
                    addr_75bb_236:
                    r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 58)));
                    if (!*reinterpret_cast<unsigned char*>(&r13_29)) {
                        if (*reinterpret_cast<unsigned char*>(rbx26 + 23)) {
                            *reinterpret_cast<int32_t*>(&rax409) = 1;
                            *reinterpret_cast<int32_t*>(&rax409 + 4) = 0;
                            if (!*reinterpret_cast<unsigned char*>(rbx26 + 22)) {
                                *reinterpret_cast<int32_t*>(&rax409) = 0;
                                *reinterpret_cast<int32_t*>(&rax409 + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&rax409) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 3);
                            }
                            rdi410 = v47;
                            *reinterpret_cast<int32_t*>(&rdi410 + 4) = 0;
                            rdx100 = r15_15;
                            r8_5 = v24;
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            rcx22 = v18;
                            rsi19 = reinterpret_cast<void**>(0xffffff9c);
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            rsp411 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8 - 8);
                            *rsp411 = rdi410;
                            rdi17 = r15_15;
                            rsp412 = rsp411 - 8;
                            *rsp412 = reinterpret_cast<void**>(0);
                            rsp413 = rsp412 - 8;
                            *rsp413 = rax409;
                            rsp414 = reinterpret_cast<struct s13*>(rsp413 - 8);
                            rsp414->f0 = reinterpret_cast<void**>(0x85bf);
                            al415 = create_hard_link(rdi17, 0xffffff9c, rdx100, rcx22, r8_5, v20, 1, 0, rsp414->f18);
                            rsp16 = reinterpret_cast<struct s11*>(&rsp414->f8 + 8);
                            r9 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (al415) 
                                goto addr_7633_344;
                            goto addr_7d80_250;
                        }
                        if (*reinterpret_cast<uint32_t*>(&v31) == 0x8000) 
                            goto addr_8b68_347;
                        r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&v31) != 0xa000)) & *reinterpret_cast<unsigned char*>(rbx26 + 20));
                        if (!r9) 
                            goto addr_7c9e_349;
                    } else {
                        if (*reinterpret_cast<void***>(r15_15) == 47) {
                            addr_7606_351:
                            rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 22)));
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            r8_5 = reinterpret_cast<void**>(0xffffffff);
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            rsi416 = v24;
                            *reinterpret_cast<int32_t*>(&rsi416 + 4) = 0;
                            rsp417 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp417 = reinterpret_cast<void**>(0x7625);
                            eax418 = force_symlinkat(r15_15, rsi416, v20, rcx22, 0xffffffff, r15_15, rsi416, v20, rcx22, 0xffffffff);
                            rsp16 = reinterpret_cast<struct s11*>(rsp417 + 8);
                            r9 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax418) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(eax418 == 0))) {
                                rsp419 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp419 = reinterpret_cast<void**>(0x8ea2);
                                rax420 = quotearg_n_style();
                                r15_15 = rax420;
                                rsp421 = rsp419 + 8 - 8;
                                *rsp421 = reinterpret_cast<void**>(0x8eb8);
                                rax422 = quotearg_n_style();
                                r13_29 = rax422;
                                rsp423 = rsp421 + 8 - 8;
                                *rsp423 = reinterpret_cast<void**>(0x8ece);
                                rax424 = fun_3800();
                                r8_5 = r15_15;
                                rcx22 = r13_29;
                                rsi19 = eax418;
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax424;
                                rdi17 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                rsp425 = rsp423 + 8 - 8;
                                *rsp425 = reinterpret_cast<void**>(0x8ee3);
                                fun_3b80();
                                rsp16 = reinterpret_cast<struct s11*>(rsp425 + 8);
                                goto addr_7d80_250;
                            }
                        } else {
                            rsp426 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp426 = reinterpret_cast<void**>(0x75db);
                            rax427 = dir_name(v20, rsi230, v20, rsi230);
                            rsp428 = reinterpret_cast<struct s11*>(rsp426 + 8);
                            if (reinterpret_cast<int1_t>(v24 == 0xffffff9c) && (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax427) == 46) && !*reinterpret_cast<void***>(rax427 + 1)) || ((rsi429 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0), rsp430 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8), *rsp430 = reinterpret_cast<void**>(0x8613), eax431 = fun_3a70(".", rsi429, ".", rsi429), rsp428 = reinterpret_cast<struct s11*>(rsp430 + 8), !!eax431) || (rdi432 = v24, *reinterpret_cast<int32_t*>(&rdi432 + 4) = 0, rdx433 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30), rsp434 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8), *rsp434 = reinterpret_cast<void**>(0x8632), eax435 = fun_3cb0(rdi432, rax427, rdx433, rdi432, rax427, rdx433), rsp428 = reinterpret_cast<struct s11*>(rsp434 + 8), !!eax435))) {
                                addr_75fe_354:
                                rsp436 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8);
                                *rsp436 = reinterpret_cast<void**>(0x7606);
                                fun_3670(rax427, rax427);
                                rsp16 = reinterpret_cast<struct s11*>(rsp436 + 8);
                                goto addr_7606_351;
                            } else {
                                if (v437 != v106 || v438 != v108) {
                                    rsp439 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8);
                                    *rsp439 = reinterpret_cast<void**>(0x8656);
                                    fun_3670(rax427, rax427);
                                    rsp440 = rsp439 + 8 - 8;
                                    *rsp440 = reinterpret_cast<void**>(0x8669);
                                    rax441 = quotearg_n_style_colon();
                                    rsp245 = reinterpret_cast<struct s11*>(rsp440 + 8);
                                    r12_246 = rax441;
                                    goto addr_8678_249;
                                } else {
                                    goto addr_75fe_354;
                                }
                            }
                        }
                    }
                    addr_8b68_347:
                    v21 = v442;
                    eax443 = *reinterpret_cast<unsigned char*>(rbx26 + 49);
                    v28 = *reinterpret_cast<void***>(&eax443);
                    rsp444 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp444 = reinterpret_cast<void**>(0x8ba0);
                    eax445 = open_safer(r15_15);
                    rsp446 = reinterpret_cast<struct s11*>(rsp444 + 8);
                    r13d447 = eax445;
                    if (reinterpret_cast<signed char>(eax445) < reinterpret_cast<signed char>(0)) {
                        rsp448 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp446) - 8);
                        *rsp448 = reinterpret_cast<void**>(0x98d6);
                        rax449 = quotearg_style(4, r15_15, 4, r15_15);
                        rsp260 = reinterpret_cast<struct s11*>(rsp448 + 8);
                        r13_29 = rax449;
                        goto addr_8f28_270;
                    } else {
                        rsi450 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - 0xd0);
                        *reinterpret_cast<void***>(&rdi451) = eax445;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi451) + 4) = 0;
                        rsp452 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp446) - 8);
                        *rsp452 = reinterpret_cast<void**>(0x8bb9);
                        eax453 = fun_3ca0(rdi451, rsi450, rdi451, rsi450);
                        rsp454 = reinterpret_cast<struct s11*>(rsp452 + 8);
                        if (eax453) {
                            rsp455 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp455 = reinterpret_cast<void**>(0x9b2f);
                            rax456 = quotearg_style(4, r15_15, 4, r15_15);
                            v28 = rax456;
                            rsp457 = rsp455 + 8 - 8;
                            *rsp457 = reinterpret_cast<void**>(0x9b49);
                            rax458 = fun_3800();
                            rsp459 = rsp457 + 8 - 8;
                            *rsp459 = reinterpret_cast<void**>(0x9b51);
                            rax460 = fun_36d0();
                            rsp461 = reinterpret_cast<struct s11*>(rsp459 + 8);
                            rcx22 = v28;
                            rdx100 = rax458;
                            rsi19 = *reinterpret_cast<void***>(rax460);
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        } else {
                            if (v462 != v106 || v463 != v108) {
                                rsp464 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp464 = reinterpret_cast<void**>(0x9691);
                                rax465 = quotearg_style(4, r15_15, 4, r15_15);
                                rsp466 = rsp464 + 8 - 8;
                                *rsp466 = reinterpret_cast<void**>(0x96a7);
                                rax467 = fun_3800();
                                rsp461 = reinterpret_cast<struct s11*>(rsp466 + 8);
                                rcx22 = rax465;
                                rsi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax467;
                            } else {
                                v468 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_46) & 0x1ff);
                                if (*reinterpret_cast<unsigned char*>(&v36)) 
                                    goto addr_9b87_364;
                                rsi19 = v20;
                                *reinterpret_cast<void***>(&rdi469) = v24;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi469) + 4) = 0;
                                rdx100 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(edx229) - (reinterpret_cast<unsigned char>(edx229) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(edx229) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(edx229) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v28) < 1)))) & 0xfffffe00) + 0x201);
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                rsp470 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp470 = reinterpret_cast<void**>(0x8c2d);
                                eax471 = openat_safer(rdi469, rsi19, rdi469, rsi19);
                                r12d472 = eax471;
                                rsp473 = rsp470 + 8 - 8;
                                *rsp473 = reinterpret_cast<void**>(0x8c35);
                                rax474 = fun_36d0();
                                rsp454 = reinterpret_cast<struct s11*>(rsp473 + 8);
                                r8_5 = *reinterpret_cast<void***>(rax474);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                r9 = rax474;
                                if (reinterpret_cast<signed char>(r12d472) < reinterpret_cast<signed char>(0)) 
                                    goto addr_a253_366; else 
                                    goto addr_8c44_367;
                            }
                        }
                    }
                    addr_96af_368:
                    rsp475 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp461) - 8);
                    *rsp475 = reinterpret_cast<void**>(0x96bb);
                    fun_3b80();
                    rsp454 = reinterpret_cast<struct s11*>(rsp475 + 8);
                    r9 = reinterpret_cast<void**>(0);
                    addr_8e48_369:
                    *reinterpret_cast<void***>(&rdi476) = r13d447;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi476) + 4) = 0;
                    v28 = r9;
                    rsp477 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp477 = reinterpret_cast<void**>(0x8e57);
                    eax478 = fun_3920(rdi476, rsi19, rdi476, rsi19);
                    rsp479 = reinterpret_cast<struct s11*>(rsp477 + 8);
                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                    if (eax478 < 0) {
                        rsp480 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp479) - 8);
                        *rsp480 = reinterpret_cast<void**>(0x9ae6);
                        rax481 = quotearg_style(4, r15_15, 4, r15_15);
                        r15_15 = rax481;
                        rsp482 = rsp480 + 8 - 8;
                        *rsp482 = reinterpret_cast<void**>(0x9afc);
                        rax483 = fun_3800();
                        r13_29 = rax483;
                        rsp484 = rsp482 + 8 - 8;
                        *rsp484 = reinterpret_cast<void**>(0x9b04);
                        rax485 = fun_36d0();
                        rcx22 = r15_15;
                        rdx100 = r13_29;
                        rsi19 = *reinterpret_cast<void***>(rax485);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp486 = rsp484 + 8 - 8;
                        *rsp486 = reinterpret_cast<void**>(0x9b15);
                        fun_3b80();
                        rdi17 = reinterpret_cast<void**>(0);
                        rsp487 = rsp486 + 8 - 8;
                        *rsp487 = reinterpret_cast<void**>(0x9b1d);
                        fun_3670(0, 0);
                        rsp16 = reinterpret_cast<struct s11*>(rsp487 + 8);
                        goto addr_7d80_250;
                    } else {
                        rdi17 = reinterpret_cast<void**>(0);
                        v28 = r9;
                        r13_29 = reinterpret_cast<void**>(0);
                        rsp488 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp479) - 8);
                        *rsp488 = reinterpret_cast<void**>(0x8e79);
                        fun_3670(0, 0);
                        rsp16 = reinterpret_cast<struct s11*>(rsp488 + 8);
                        r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        if (!r9) {
                            goto addr_7d80_250;
                        }
                    }
                    addr_a253_366:
                    if (r8_5 == 2) 
                        goto addr_a299_373;
                    eax489 = *reinterpret_cast<unsigned char*>(rbx26 + 22);
                    if (!*reinterpret_cast<unsigned char*>(&eax489)) 
                        goto addr_9bf1_375;
                    *reinterpret_cast<void***>(&rdi490) = v24;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi490) + 4) = 0;
                    v30 = r9;
                    rsp491 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp491 = reinterpret_cast<void**>(0xa280);
                    eax492 = fun_3720(rdi490, v20);
                    rsp454 = reinterpret_cast<struct s11*>(rsp491 + 8);
                    r9 = v30;
                    if (!eax492) 
                        goto addr_a28f_377;
                    if (*reinterpret_cast<void***>(r9) == 2) {
                        addr_a299_373:
                        if (!*reinterpret_cast<void***>(rbx26 + 40) || (rdx100 = v468, *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0, r8_5 = rbx26, rcx22 = reinterpret_cast<void**>(1), *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0, rsi19 = v18, rsp493 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp493 = reinterpret_cast<void**>(0xa2c1), eax494 = set_process_security_ctx(r15_15, rsi19, r15_15, rsi19), rsp454 = reinterpret_cast<struct s11*>(rsp493 + 8), r9 = eax494, *reinterpret_cast<int32_t*>(&r9 + 4) = 0, !!*reinterpret_cast<signed char*>(&eax494))) {
                            addr_9b87_364:
                            *reinterpret_cast<void***>(&rdi495) = v24;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi495) + 4) = 0;
                            rdx100 = reinterpret_cast<void**>(0xc1);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsi19 = v20;
                            eax496 = reinterpret_cast<void**>(~v233 & reinterpret_cast<unsigned char>(v468));
                            rcx22 = eax496;
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            v30 = eax496;
                            rsp497 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp497 = reinterpret_cast<void**>(0x9bb6);
                            eax498 = openat_safer(rdi495, rsi19, rdi495, rsi19);
                            r12d472 = eax498;
                            rsp499 = rsp497 + 8 - 8;
                            *rsp499 = reinterpret_cast<void**>(0x9bbe);
                            rax500 = fun_36d0();
                            rsp454 = reinterpret_cast<struct s11*>(rsp499 + 8);
                            r8_5 = *reinterpret_cast<void***>(rax500);
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            eax501 = reinterpret_cast<unsigned char>(r12d472) >> 31;
                            dl502 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r8_5 == 17)) & *reinterpret_cast<unsigned char*>(&eax501));
                            *reinterpret_cast<unsigned char*>(&v36) = dl502;
                            if (!dl502) 
                                goto addr_a401_379;
                        } else {
                            *reinterpret_cast<unsigned char*>(&v36) = 0;
                            goto addr_8e48_369;
                        }
                    } else {
                        v28 = r9;
                        rsp503 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp503 = reinterpret_cast<void**>(0xab38);
                        rax504 = quotearg_style(4, v18, 4, v18);
                        rsp505 = rsp503 + 8 - 8;
                        *rsp505 = reinterpret_cast<void**>(0xab4e);
                        rax506 = fun_3800();
                        rsp461 = reinterpret_cast<struct s11*>(rsp505 + 8);
                        rcx22 = rax504;
                        rdx100 = rax506;
                        rsi19 = *reinterpret_cast<void***>(v28);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        goto addr_96af_368;
                    }
                    eax489 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                    r8_5 = reinterpret_cast<void**>(17);
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    if (!*reinterpret_cast<unsigned char*>(&eax489)) {
                        rsi19 = v20;
                        *reinterpret_cast<void***>(&rdi507) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi507) + 4) = 0;
                        rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffffc7);
                        rcx22 = reinterpret_cast<void**>(1);
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        v47 = reinterpret_cast<void**>(17);
                        v25 = rax500;
                        rsp508 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp508 = reinterpret_cast<void**>(0xa3bb);
                        rax509 = fun_39f0(rdi507, rsi19, rdx100, 1);
                        rsp454 = reinterpret_cast<struct s11*>(rsp508 + 8);
                        r8_5 = reinterpret_cast<void**>(17);
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        if (reinterpret_cast<signed char>(rax509) < reinterpret_cast<signed char>(0)) {
                            addr_a401_379:
                            eax510 = reinterpret_cast<unsigned char>(r12d472) >> 31;
                            *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r8_5 == 21)) & *reinterpret_cast<unsigned char*>(&eax510));
                            *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&rdx100);
                            if (!*reinterpret_cast<unsigned char*>(&rdx100)) {
                                v511 = reinterpret_cast<void**>(~reinterpret_cast<unsigned char>(v468) & reinterpret_cast<unsigned char>(v30));
                                if (reinterpret_cast<signed char>(r12d472) < reinterpret_cast<signed char>(0)) {
                                    *reinterpret_cast<unsigned char*>(&v36) = 1;
                                } else {
                                    *reinterpret_cast<unsigned char*>(&v36) = 1;
                                    v512 = v233;
                                    goto addr_8c85_387;
                                }
                            } else {
                                r8_5 = reinterpret_cast<void**>(21);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                if (*reinterpret_cast<void***>(v18)) {
                                    rsp513 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp513 = reinterpret_cast<void**>(0xa438);
                                    rax514 = fun_3820(v18, v18);
                                    rsp454 = reinterpret_cast<struct s11*>(rsp513 + 8);
                                    r8d515 = reinterpret_cast<struct s15*>(0);
                                    *reinterpret_cast<unsigned char*>(&r8d515) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(rax514) + 0xffffffffffffffff) != 47);
                                    r8_5 = reinterpret_cast<void**>(&r8d515->f14);
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                }
                            }
                        } else {
                            r9 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 62)));
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (!r9) {
                                rsp516 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp516 = reinterpret_cast<void**>(0xaf37);
                                rax517 = quotearg_style(4, v18, 4, v18);
                                rsp518 = rsp516 + 8 - 8;
                                *rsp518 = reinterpret_cast<void**>(0xaf4d);
                                rax519 = fun_3800();
                                rcx22 = rax517;
                                rsi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax519;
                                rsp520 = rsp518 + 8 - 8;
                                *rsp520 = reinterpret_cast<void**>(0xaf61);
                                fun_3b80();
                                rsp454 = reinterpret_cast<struct s11*>(rsp520 + 8);
                                r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(r9)));
                                goto addr_8e48_369;
                            } else {
                                rcx22 = v30;
                                *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                rsi19 = v20;
                                rdx100 = reinterpret_cast<void**>(65);
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                *reinterpret_cast<void***>(&rdi521) = v24;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi521) + 4) = 0;
                                rsp522 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp522 = reinterpret_cast<void**>(0xa3f4);
                                eax523 = openat_safer(rdi521, rsi19, rdi521, rsi19);
                                rsp454 = reinterpret_cast<struct s11*>(rsp522 + 8);
                                r12d472 = eax523;
                                r8_5 = *reinterpret_cast<void***>(v25);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                goto addr_a401_379;
                            }
                        }
                    } else {
                        addr_9bf1_375:
                        *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax489);
                    }
                    v28 = r8_5;
                    rsp524 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp524 = reinterpret_cast<void**>(0x9c0f);
                    rax525 = quotearg_style(4, v18, 4, v18);
                    rsp526 = rsp524 + 8 - 8;
                    *rsp526 = reinterpret_cast<void**>(0x9c25);
                    rax527 = fun_3800();
                    rsp461 = reinterpret_cast<struct s11*>(rsp526 + 8);
                    rsi19 = v28;
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rcx22 = rax525;
                    rdx100 = rax527;
                    goto addr_96af_368;
                    addr_8c85_387:
                    if (!v28) 
                        goto addr_9cad_394;
                    rdx100 = *reinterpret_cast<void***>(rbx26 + 68);
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    if (rdx100) {
                        rdx100 = r13d447;
                        *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                        rsi19 = reinterpret_cast<void**>(0x40049409);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        *reinterpret_cast<void***>(&rdi528) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi528) + 4) = 0;
                        rsp529 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp529 = reinterpret_cast<void**>(0xa55c);
                        eax530 = fun_38f0(rdi528, 0x40049409, rdx100, rcx22);
                        rsp454 = reinterpret_cast<struct s11*>(rsp529 + 8);
                        if (!eax530) {
                            addr_9cad_394:
                            eax531 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 29)) | reinterpret_cast<unsigned char>(v511));
                            v28 = eax531;
                            if (eax531) {
                                v28 = reinterpret_cast<void**>(0);
                                goto addr_8c9d_398;
                            } else {
                                if (!*reinterpret_cast<signed char*>(rbx26 + 31)) {
                                    addr_9dab_400:
                                    if (0xff0000000000ff & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) {
                                        r8_5 = v21;
                                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                        rcx22 = r12d472;
                                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                        rsi19 = r13d447;
                                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                        rdx100 = v18;
                                        rsp532 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                        *rsp532 = reinterpret_cast<void**>(0xa49f);
                                        eax533 = copy_acl(r15_15, rsi19, rdx100, rcx22, r8_5, r9);
                                        rsp454 = reinterpret_cast<struct s11*>(rsp532 + 8);
                                        r9 = reinterpret_cast<void**>(1);
                                        if (!eax533) {
                                            addr_8e1c_402:
                                            *reinterpret_cast<void***>(&rdi534) = r12d472;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi534) + 4) = 0;
                                            rsp535 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                            *rsp535 = reinterpret_cast<void**>(0x8e2b);
                                            eax536 = fun_3920(rdi534, rsi19, rdi534, rsi19);
                                            rsp454 = reinterpret_cast<struct s11*>(rsp535 + 8);
                                            if (eax536 < 0) {
                                                rsp537 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                                *rsp537 = reinterpret_cast<void**>(0xa32b);
                                                rax538 = quotearg_style(4, v18, 4, v18);
                                                v28 = rax538;
                                                rsp539 = rsp537 + 8 - 8;
                                                *rsp539 = reinterpret_cast<void**>(0xa345);
                                                rax540 = fun_3800();
                                                rsp541 = rsp539 + 8 - 8;
                                                *rsp541 = reinterpret_cast<void**>(0xa34d);
                                                rax542 = fun_36d0();
                                                rcx22 = v28;
                                                rdx100 = rax540;
                                                rsi19 = *reinterpret_cast<void***>(rax542);
                                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                                rsp543 = rsp541 + 8 - 8;
                                                *rsp543 = reinterpret_cast<void**>(0xa362);
                                                fun_3b80();
                                                rsp454 = reinterpret_cast<struct s11*>(rsp543 + 8);
                                                r9 = reinterpret_cast<void**>(0);
                                                goto addr_8e48_369;
                                            } else {
                                                r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(r9)));
                                                goto addr_8e48_369;
                                            }
                                        }
                                    } else {
                                        if (*reinterpret_cast<signed char*>(rbx26 + 57)) {
                                            rdx100 = *reinterpret_cast<void***>(rbx26 + 16);
                                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                            rsi19 = r12d472;
                                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                            rsp544 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                            *rsp544 = reinterpret_cast<void**>(0xa641);
                                            eax545 = set_acl(v18, rsi19, v18, rsi19);
                                            rsp454 = reinterpret_cast<struct s11*>(rsp544 + 8);
                                            r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax545 == 0)));
                                            goto addr_8e1c_402;
                                        }
                                        eax546 = *reinterpret_cast<unsigned char*>(&v36);
                                        al547 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax546) & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 32)));
                                        *reinterpret_cast<unsigned char*>(&v21) = al547;
                                        if (al547) {
                                            zf548 = reinterpret_cast<int1_t>(mask_0 == 0xffffffff);
                                            if (zf548) {
                                                rsp549 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                                *rsp549 = reinterpret_cast<void**>(0xadcf);
                                                eax550 = fun_3a60();
                                                mask_0 = eax550;
                                                rsp551 = rsp549 + 8 - 8;
                                                *rsp551 = reinterpret_cast<void**>(0xaddc);
                                                fun_3a60();
                                                rsp454 = reinterpret_cast<struct s11*>(rsp551 + 8);
                                            }
                                            edx552 = mask_0;
                                            rsi19 = r12d472;
                                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                            rdx100 = reinterpret_cast<void**>(~reinterpret_cast<unsigned char>(edx552) & 0x1b6);
                                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                            rsp553 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                            *rsp553 = reinterpret_cast<void**>(0xaba3);
                                            eax554 = set_acl(v18, rsi19);
                                            rsp454 = reinterpret_cast<struct s11*>(rsp553 + 8);
                                            eax555 = *reinterpret_cast<unsigned char*>(&v21);
                                            r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax554 == 0)));
                                            *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax555);
                                            goto addr_8e1c_402;
                                        }
                                        r9 = reinterpret_cast<void**>(1);
                                        if (!(v512 | reinterpret_cast<unsigned char>(v28))) 
                                            goto addr_8e1c_402;
                                        eax556 = mask_0;
                                        if (eax556 == 0xffffffff) 
                                            goto addr_ae66_414; else 
                                            goto addr_9e06_415;
                                    }
                                } else {
                                    addr_9cd7_416:
                                    rdx100 = v20;
                                    r8_5 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                    *reinterpret_cast<void***>(&rdi557) = r12d472;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi557) + 4) = 0;
                                    rsi19 = v24;
                                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                    rcx22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd60);
                                    rsp558 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp558 = reinterpret_cast<void**>(0x9d2e);
                                    eax559 = fdutimensat(rdi557, rsi19, rdx100, rcx22);
                                    rsp454 = reinterpret_cast<struct s11*>(rsp558 + 8);
                                    if (eax559 && (rsp560 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp560 = reinterpret_cast<void**>(0xaa15), rax561 = quotearg_style(4, v18, 4, v18), v25 = rax561, rsp562 = rsp560 + 8 - 8, *rsp562 = reinterpret_cast<void**>(0xaa2f), rax563 = fun_3800(), v30 = rax563, rsp564 = rsp562 + 8 - 8, *rsp564 = reinterpret_cast<void**>(0xaa3b), rax565 = fun_36d0(), rcx22 = v25, rdx100 = v30, rsi19 = *reinterpret_cast<void***>(rax565), *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0, rsp566 = rsp564 + 8 - 8, *rsp566 = reinterpret_cast<void**>(0xaa54), fun_3b80(), rsp454 = reinterpret_cast<struct s11*>(rsp566 + 8), !!*reinterpret_cast<unsigned char*>(rbx26 + 50))) {
                                        goto addr_8e19_418;
                                    }
                                }
                            }
                        } else {
                            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 68) == 2)) {
                                rsp567 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp567 = reinterpret_cast<void**>(0xa580);
                                rax568 = quotearg_n_style();
                                v30 = rax568;
                                rsp569 = rsp567 + 8 - 8;
                                *rsp569 = reinterpret_cast<void**>(0xa59a);
                                rax570 = quotearg_n_style();
                                v21 = rax570;
                                rsp571 = rsp569 + 8 - 8;
                                *rsp571 = reinterpret_cast<void**>(0xa5b4);
                                rax572 = fun_3800();
                                v28 = rax572;
                                rsp573 = rsp571 + 8 - 8;
                                *rsp573 = reinterpret_cast<void**>(0xa5c0);
                                rax574 = fun_36d0();
                                r8_5 = v30;
                                rcx22 = v21;
                                rsi19 = *reinterpret_cast<void***>(rax574);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = v28;
                                rsp575 = rsp573 + 8 - 8;
                                *rsp575 = reinterpret_cast<void**>(0xa5e0);
                                fun_3b80();
                                rsp454 = reinterpret_cast<struct s11*>(rsp575 + 8);
                                r9 = reinterpret_cast<void**>(0);
                                goto addr_8e1c_402;
                            }
                        }
                    } else {
                        addr_8c9d_398:
                        rsi576 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - 0x160);
                        *reinterpret_cast<void***>(&rdi577) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi577) + 4) = 0;
                        rsp578 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp578 = reinterpret_cast<void**>(0x8cac);
                        eax579 = fun_3ca0(rdi577, rsi576, rdi577, rsi576);
                        rsp454 = reinterpret_cast<struct s11*>(rsp578 + 8);
                        if (eax579) {
                            rsp580 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp580 = reinterpret_cast<void**>(0xaa74);
                            rax581 = quotearg_style(4, v18, 4, v18);
                            rsp582 = reinterpret_cast<struct s11*>(rsp580 + 8);
                            v21 = rax581;
                            goto addr_a6f1_422;
                        } else {
                            v30 = eax579;
                            rsi19 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v511)));
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            if (rsi19) {
                                *reinterpret_cast<void***>(&rdi583) = r12d472;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi583) + 4) = 0;
                                rsp584 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp584 = reinterpret_cast<void**>(0x8cd4);
                                eax585 = fun_3b50(rdi583, rdi583);
                                rsp454 = reinterpret_cast<struct s11*>(rsp584 + 8);
                                rdx100 = v30;
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                if (!eax585) {
                                    rdx100 = v511;
                                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                }
                                v511 = rdx100;
                            }
                            if (!v28) 
                                goto addr_a194_428;
                            v586 = v587;
                            v47 = v587;
                            if (reinterpret_cast<uint64_t>(v587 + 0xfffffffffffe0000) > 0x1ffffffffffe0000) {
                                v47 = reinterpret_cast<void**>(0x20000);
                                *reinterpret_cast<int32_t*>(&rax588) = 0x200;
                                *reinterpret_cast<int32_t*>(&rax588 + 4) = 0;
                                if (reinterpret_cast<unsigned char>(v586 + 0xffffffffffffffff) <= reinterpret_cast<unsigned char>(0x1fffffffffffffff)) {
                                    rax588 = v586;
                                }
                                v586 = rax588;
                            }
                            if ((*reinterpret_cast<uint32_t*>(&v339) & reinterpret_cast<uint32_t>(fun_f000)) != 0x8000) 
                                goto addr_a047_434; else 
                                goto addr_8d66_435;
                        }
                    }
                    addr_9e8c_436:
                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 50)) ^ 1);
                    goto addr_8e1c_402;
                    addr_ae66_414:
                    v30 = reinterpret_cast<void**>(1);
                    rsp589 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp589 = reinterpret_cast<void**>(0xae74);
                    eax590 = fun_3a60();
                    mask_0 = eax590;
                    v21 = eax590;
                    rsp591 = rsp589 + 8 - 8;
                    *rsp591 = reinterpret_cast<void**>(0xae87);
                    fun_3a60();
                    rsp454 = reinterpret_cast<struct s11*>(rsp591 + 8);
                    rdx100 = v21;
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    r9 = reinterpret_cast<void**>(1);
                    if (!(~reinterpret_cast<unsigned char>(rdx100) & v512 | reinterpret_cast<unsigned char>(v28))) 
                        goto addr_8e1c_402;
                    ++rdx100;
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    if (!rdx100) {
                        rsp592 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp592 = reinterpret_cast<void**>(0xaeb7);
                        eax593 = fun_3a60();
                        mask_0 = eax593;
                        rsp594 = rsp592 + 8 - 8;
                        *rsp594 = reinterpret_cast<void**>(0xaec4);
                        fun_3a60();
                        rsp454 = reinterpret_cast<struct s11*>(rsp594 + 8);
                    }
                    eax556 = mask_0;
                    addr_9e1c_440:
                    *reinterpret_cast<void***>(&rdi595) = r12d472;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi595) + 4) = 0;
                    rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v468) & ~reinterpret_cast<unsigned char>(eax556));
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp596 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp596 = reinterpret_cast<void**>(0x9e2e);
                    eax597 = fun_3b50(rdi595, rdi595);
                    rsp454 = reinterpret_cast<struct s11*>(rsp596 + 8);
                    r9 = reinterpret_cast<void**>(1);
                    if (!eax597) 
                        goto addr_8e1c_402;
                    rsp598 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp598 = reinterpret_cast<void**>(0x9e4d);
                    rax599 = quotearg_style(4, v18, 4, v18);
                    v21 = rax599;
                    rsp600 = rsp598 + 8 - 8;
                    *rsp600 = reinterpret_cast<void**>(0x9e67);
                    rax601 = fun_3800();
                    v28 = rax601;
                    rsp602 = rsp600 + 8 - 8;
                    *rsp602 = reinterpret_cast<void**>(0x9e73);
                    rax603 = fun_36d0();
                    rcx22 = v21;
                    rdx100 = v28;
                    rsi19 = *reinterpret_cast<void***>(rax603);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp604 = rsp602 + 8 - 8;
                    *rsp604 = reinterpret_cast<void**>(0x9e8c);
                    fun_3b80();
                    rsp454 = reinterpret_cast<struct s11*>(rsp604 + 8);
                    goto addr_9e8c_436;
                    addr_9e06_415:
                    rdx100 = reinterpret_cast<void**>(~reinterpret_cast<unsigned char>(eax556) & v512 | reinterpret_cast<unsigned char>(v28));
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    if (!rdx100) 
                        goto addr_8e1c_402; else 
                        goto addr_9e1c_440;
                    addr_a6f1_422:
                    rsp605 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp582) - 8);
                    *rsp605 = reinterpret_cast<void**>(0xa6f8);
                    rax606 = fun_3800();
                    v28 = rax606;
                    rsp607 = rsp605 + 8 - 8;
                    *rsp607 = reinterpret_cast<void**>(0xa704);
                    rax608 = fun_36d0();
                    rcx22 = v21;
                    rdx100 = v28;
                    rsi19 = *reinterpret_cast<void***>(rax608);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp609 = rsp607 + 8 - 8;
                    *rsp609 = reinterpret_cast<void**>(0xa71d);
                    fun_3b80();
                    rsp454 = reinterpret_cast<struct s11*>(rsp609 + 8);
                    goto addr_8e19_418;
                    addr_a047_434:
                    r8d610 = reinterpret_cast<void**>(1);
                    addr_a04d_442:
                    if (!0 || *reinterpret_cast<signed char*>(rbx26 + 12) != 3 && (r8d610 == 1 || *reinterpret_cast<signed char*>(rbx26 + 12) != 2)) {
                        *reinterpret_cast<void***>(&rdi611) = r13d447;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi611) + 4) = 0;
                        v30 = r8d610;
                        rsp612 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp612 = reinterpret_cast<void**>(0xa07b);
                        fdadvise(rdi611);
                        rdi613 = v614;
                        if (reinterpret_cast<uint64_t>(rdi613 - 0x20000) > 0x1ffffffffffe0000) {
                            rdi613 = 0x20000;
                        }
                        rsp615 = rsp612 + 8 - 8;
                        *rsp615 = reinterpret_cast<void**>(0xa0b5);
                        rax616 = buffer_lcm(rdi613, v47, 0x7fffffffffffffff, 2);
                        rsp454 = reinterpret_cast<struct s11*>(rsp615 + 8);
                        r8_617 = v30;
                        *reinterpret_cast<int32_t*>(&r8_617 + 4) = 0;
                        rcx22 = rax616;
                        if ((*reinterpret_cast<uint32_t*>(&v339) & reinterpret_cast<uint32_t>(fun_f000)) == 0x8000 && reinterpret_cast<unsigned char>(v618) < reinterpret_cast<unsigned char>(v47)) {
                            v47 = v618 + 1;
                        }
                        rsi619 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx22) + reinterpret_cast<unsigned char>(v47) + 0xffffffffffffffff);
                        rdx100 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsi619) % reinterpret_cast<unsigned char>(rcx22));
                        rax620 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsi619) - reinterpret_cast<unsigned char>(rdx100));
                        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax620) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax620 == 0))) {
                            rcx22 = rax620;
                        }
                        eax621 = 0;
                        *reinterpret_cast<unsigned char*>(&eax621) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbx26 + 68));
                        v47 = rcx22;
                        v622 = eax621;
                        if (r8_617 != 3) 
                            goto addr_a11d_450;
                    } else {
                        rcx22 = reinterpret_cast<void**>(2);
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        rdx100 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                        *reinterpret_cast<void***>(&rdi623) = r13d447;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi623) + 4) = 0;
                        v30 = r8d610;
                        rsp624 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp624 = reinterpret_cast<void**>(0xa67f);
                        fdadvise(rdi623);
                        rsp454 = reinterpret_cast<struct s11*>(rsp624 + 8);
                        r8_617 = v30;
                        *reinterpret_cast<int32_t*>(&r8_617 + 4) = 0;
                        if (r8_617 == 3) {
                            eax625 = 0;
                            *reinterpret_cast<unsigned char*>(&eax625) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbx26 + 68));
                            v622 = eax625;
                            v626 = *reinterpret_cast<signed char*>(rbx26 + 12);
                            goto addr_a73a_453;
                        } else {
                            eax627 = 0;
                            *reinterpret_cast<unsigned char*>(&eax627) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbx26 + 68));
                            r9 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            zf628 = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(rbx26 + 12) == 3);
                            v622 = eax627;
                            r9 = reinterpret_cast<void**>(static_cast<unsigned char>(zf628));
                            goto addr_a133_455;
                        }
                    }
                    v626 = reinterpret_cast<signed char>(1);
                    addr_a73a_453:
                    v30 = v629;
                    rax630 = v302;
                    if (reinterpret_cast<signed char>(rax630) < reinterpret_cast<signed char>(0)) {
                        v631 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v30) > reinterpret_cast<signed char>(0));
                    } else {
                        edi632 = reinterpret_cast<unsigned char>(v28);
                        v25 = r15_15;
                        v631 = reinterpret_cast<void**>(0);
                        v633 = reinterpret_cast<void**>(0);
                        v634 = *reinterpret_cast<signed char*>(&edi632);
                        v302 = r14_49;
                        r14_635 = reinterpret_cast<void**>(0);
                        v636 = rbx26;
                        rbx637 = rax630;
                        while (1) {
                            rsp638 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp638 = reinterpret_cast<void**>(0xa7a9);
                            rax639 = fun_38b0();
                            rsp454 = reinterpret_cast<struct s11*>(rsp638 + 8);
                            r15_640 = rax639;
                            if (reinterpret_cast<signed char>(rax639) >= reinterpret_cast<signed char>(0)) {
                                rax641 = v30;
                                if (reinterpret_cast<signed char>(rax641) < reinterpret_cast<signed char>(r15_640)) {
                                    rax641 = r15_640;
                                }
                                v30 = rax641;
                            } else {
                                rsp642 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp642 = reinterpret_cast<void**>(0xa7ba);
                                rax643 = fun_36d0();
                                rsp454 = reinterpret_cast<struct s11*>(rsp642 + 8);
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax643) == 6)) 
                                    goto addr_abbb_464;
                                r15_640 = v30;
                                if (reinterpret_cast<signed char>(rbx637) >= reinterpret_cast<signed char>(v30)) 
                                    goto addr_a979_466;
                            }
                            addr_a7d6_467:
                            rsp644 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp644 = reinterpret_cast<void**>(0xa7e3);
                            rax645 = fun_38b0();
                            rsp454 = reinterpret_cast<struct s11*>(rsp644 + 8);
                            if (reinterpret_cast<signed char>(rax645) < reinterpret_cast<signed char>(0)) 
                                goto addr_abbb_464;
                            rcx22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx637) - reinterpret_cast<unsigned char>(v633)) - reinterpret_cast<unsigned char>(r14_635));
                            if (!rcx22) {
                                if (v626 != 1) {
                                    rcx22 = v586;
                                }
                                v646 = 0;
                                r14_635 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_640) - reinterpret_cast<unsigned char>(rbx637));
                                r8_617 = rcx22;
                            } else {
                                if (v626 == 1) {
                                    rsp647 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp647 = reinterpret_cast<void**>(0xa9b4);
                                    al648 = write_zeros(r12d472, rcx22);
                                    rsp454 = reinterpret_cast<struct s11*>(rsp647 + 8);
                                    if (!al648) 
                                        goto addr_ad88_474;
                                    v646 = 0;
                                    r8_617 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&r8_617 + 4) = 0;
                                    r14_635 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_640) - reinterpret_cast<unsigned char>(rbx637));
                                } else {
                                    rdx100 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                    rsi19 = v18;
                                    *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<uint1_t>(v626 == 3);
                                    rsp649 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp649 = reinterpret_cast<void**>(0xa825);
                                    eax650 = create_hole(r12d472, rsi19, rdx100, rcx22, r8_617, r12d472, rsi19, rdx100, rcx22, r8_617);
                                    rsp454 = reinterpret_cast<struct s11*>(rsp649 + 8);
                                    v646 = *reinterpret_cast<unsigned char*>(&eax650);
                                    if (!*reinterpret_cast<unsigned char*>(&eax650)) 
                                        goto addr_ad51_477;
                                    r8_617 = v586;
                                    r14_635 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_640) - reinterpret_cast<unsigned char>(rbx637));
                                }
                            }
                            r9 = reinterpret_cast<void**>(1);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            rsi19 = r12d472;
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            r15_15 = v25;
                            rsp651 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp651 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf7);
                            rcx22 = v47;
                            rsp652 = rsp651 - 8;
                            *rsp652 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd00);
                            *reinterpret_cast<int32_t*>(&rax653) = v622;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax653) + 4) = 0;
                            rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf8);
                            rsp654 = rsp652 - 8;
                            *rsp654 = r14_635;
                            rsp655 = rsp654 - 8;
                            *rsp655 = v18;
                            rsp656 = reinterpret_cast<struct s13*>(rsp655 - 8);
                            rsp656->f0 = r15_15;
                            rsp657 = reinterpret_cast<struct s12**>(reinterpret_cast<uint64_t>(rsp656) - 8);
                            *rsp657 = rax653;
                            rsp658 = reinterpret_cast<struct s14*>(rsp657 - 1);
                            rsp658->f0 = 0xa887;
                            eax659 = sparse_copy(r13d447, rsi19, rdx100, rcx22, r8_617, 1, rsp658->f8, rsp658->f10, rsp658->f18, rsp658->f20, rsp658->f28, rsp658->f30);
                            rsp454 = reinterpret_cast<struct s11*>(&rsp658->f8 + 48);
                            if (!*reinterpret_cast<signed char*>(&eax659)) 
                                goto addr_ad51_477;
                            edi660 = reinterpret_cast<void**>(static_cast<uint32_t>(v646));
                            rdx100 = reinterpret_cast<void**>(static_cast<uint32_t>(v316));
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx637) + reinterpret_cast<unsigned char>(v300));
                            if (1) {
                                edi660 = rdx100;
                            }
                            v631 = rsi19;
                            v634 = *reinterpret_cast<signed char*>(&edi660);
                            if (reinterpret_cast<signed char>(v300) < reinterpret_cast<signed char>(r14_635)) 
                                goto addr_ac24_483;
                            rdx100 = reinterpret_cast<void**>(3);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsp661 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp661 = reinterpret_cast<void**>(0xa8d6);
                            rax662 = fun_38b0();
                            rsp454 = reinterpret_cast<struct s11*>(rsp661 + 8);
                            if (reinterpret_cast<signed char>(rax662) < reinterpret_cast<signed char>(0)) 
                                goto addr_a928_485;
                            v633 = rbx637;
                            rbx637 = rax662;
                            continue;
                            addr_a979_466:
                            rdx100 = reinterpret_cast<void**>(2);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsi19 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            rsp663 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp663 = reinterpret_cast<void**>(0xa988);
                            rax664 = fun_38b0();
                            rsp454 = reinterpret_cast<struct s11*>(rsp663 + 8);
                            r15_640 = rax664;
                            if (reinterpret_cast<signed char>(rax664) < reinterpret_cast<signed char>(0)) 
                                goto addr_abbb_464;
                            if (reinterpret_cast<signed char>(rbx637) >= reinterpret_cast<signed char>(rax664)) 
                                goto addr_ad6b_488;
                            v30 = rax664;
                            goto addr_a7d6_467;
                        }
                    }
                    addr_ac62_490:
                    if (v626 == 1) {
                        rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v30) - reinterpret_cast<unsigned char>(v631));
                        rsp665 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp665 = reinterpret_cast<void**>(0xadf7);
                        al666 = write_zeros(r12d472, rsi19);
                        rsp454 = reinterpret_cast<struct s11*>(rsp665 + 8);
                        if (al666) {
                            addr_a968_492:
                            r8d667 = reinterpret_cast<unsigned char>(v28);
                        } else {
                            addr_adff_493:
                            rsp668 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp668 = reinterpret_cast<void**>(0xae10);
                            rax669 = quotearg_style(4, v18, 4, v18);
                            rsp670 = reinterpret_cast<struct s11*>(rsp668 + 8);
                            v30 = rax669;
                            goto addr_abf0_494;
                        }
                    } else {
                        rsi19 = v30;
                        *reinterpret_cast<void***>(&rdi671) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi671) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&v25) = *reinterpret_cast<unsigned char*>(&rdx100);
                        rsp672 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp672 = reinterpret_cast<void**>(0xac84);
                        eax673 = fun_38a0(rdi671, rsi19, rdi671, rsi19);
                        rsp454 = reinterpret_cast<struct s11*>(rsp672 + 8);
                        if (eax673) 
                            goto addr_adff_493;
                        if (v626 != 3) 
                            goto addr_a968_492;
                        rdx100 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v25)));
                        *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                        if (!*reinterpret_cast<unsigned char*>(&rdx100)) 
                            goto addr_a968_492;
                        rdx100 = v631;
                        rsi19 = reinterpret_cast<void**>(3);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        *reinterpret_cast<void***>(&rdi674) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi674) + 4) = 0;
                        rcx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v30) - reinterpret_cast<unsigned char>(rdx100));
                        rsp675 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp675 = reinterpret_cast<void**>(0xacc6);
                        eax676 = fun_39a0(rdi674, 3, rdx100, rcx22, rdi674, 3, rdx100, rcx22);
                        rsp454 = reinterpret_cast<struct s11*>(rsp675 + 8);
                        if (eax676 >= 0) 
                            goto addr_a968_492;
                        rsp677 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp677 = reinterpret_cast<void**>(0xacd3);
                        rax678 = fun_36d0();
                        rsp454 = reinterpret_cast<struct s11*>(rsp677 + 8);
                        r9 = rax678;
                        r8b679 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax678) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax678) == 38)));
                        if (r8b679) 
                            goto addr_a968_492;
                        v30 = r8b679;
                        v25 = r9;
                        rsp680 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp680 = reinterpret_cast<void**>(0xad0d);
                        rax681 = quotearg_style(4, v18);
                        v28 = rax681;
                        rsp682 = rsp680 + 8 - 8;
                        *rsp682 = reinterpret_cast<void**>(0xad27);
                        rax683 = fun_3800();
                        r9 = v25;
                        rcx22 = v28;
                        rdx100 = rax683;
                        rsi19 = *reinterpret_cast<void***>(r9);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp684 = rsp682 + 8 - 8;
                        *rsp684 = reinterpret_cast<void**>(0xad44);
                        fun_3b80();
                        rsp454 = reinterpret_cast<struct s11*>(rsp684 + 8);
                        r8d667 = reinterpret_cast<unsigned char>(v30);
                    }
                    addr_a970_501:
                    r8_5 = reinterpret_cast<void**>(r8d667 ^ 1);
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    addr_a17e_502:
                    if (r8_5) 
                        goto addr_8e19_418;
                    if (!0 || (rsi19 = v300, *reinterpret_cast<void***>(&rdi685) = r12d472, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi685) + 4) = 0, rsp686 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp686 = reinterpret_cast<void**>(0xa6c5), eax687 = fun_38a0(rdi685, rsi19, rdi685, rsi19), rsp454 = reinterpret_cast<struct s11*>(rsp686 + 8), eax687 >= 0)) {
                        addr_a194_428:
                        zf688 = *reinterpret_cast<signed char*>(rbx26 + 31) == 0;
                        v28 = v511;
                        if (zf688) {
                            if (*reinterpret_cast<unsigned char*>(rbx26 + 29) && ((*reinterpret_cast<int32_t*>(&rax689) = v690, *reinterpret_cast<int32_t*>(&rax689 + 4) = 0, rdx100 = v691, *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0, *reinterpret_cast<int32_t*>(&rax689) != v692) || v693 != rdx100)) {
                                r9 = v694;
                                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                r8_5 = r12d472;
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                rsp695 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp695 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0);
                                *reinterpret_cast<uint32_t*>(&rcx696) = *reinterpret_cast<unsigned char*>(&v36);
                                *reinterpret_cast<int32_t*>(&rcx696 + 4) = 0;
                                rsi19 = v18;
                                rsp697 = rsp695 - 8;
                                *rsp697 = rcx696;
                                rcx22 = v20;
                                rsp698 = rsp697 - 8;
                                *rsp698 = rdx100;
                                rdx100 = v24;
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                rsp699 = rsp698 - 8;
                                *rsp699 = rax689;
                                rsp700 = reinterpret_cast<struct s13*>(rsp699 - 8);
                                rsp700->f0 = reinterpret_cast<void**>(0x9d90);
                                eax701 = set_owner_isra_0(rbx26, rsi19, rdx100, rcx22, r8_5, r9, rsp700->f8, rsp700->f10, 1, rsp700->f20);
                                rsp454 = reinterpret_cast<struct s11*>(&rsp700->f8 + 8);
                                if (eax701 == -1) {
                                    addr_8e19_418:
                                    r9 = reinterpret_cast<void**>(0);
                                    goto addr_8e1c_402;
                                } else {
                                    if (!eax701) {
                                        v21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v21) & 0xfffff1ff);
                                        goto addr_9dab_400;
                                    }
                                }
                            }
                        } else {
                            goto addr_9cd7_416;
                        }
                    } else {
                        rsp702 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp702 = reinterpret_cast<void**>(0xa6de);
                        rax703 = quotearg_style(4, v18, 4, v18);
                        rsp582 = reinterpret_cast<struct s11*>(rsp702 + 8);
                        v21 = rax703;
                        goto addr_a6f1_422;
                    }
                    addr_abf0_494:
                    rsp704 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp670) - 8);
                    *rsp704 = reinterpret_cast<void**>(0xabf7);
                    rax705 = fun_3800();
                    v28 = rax705;
                    rsp706 = rsp704 + 8 - 8;
                    *rsp706 = reinterpret_cast<void**>(0xac03);
                    rax707 = fun_36d0();
                    rcx22 = v30;
                    rdx100 = v28;
                    rsi19 = *reinterpret_cast<void***>(rax707);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp708 = rsp706 + 8 - 8;
                    *rsp708 = reinterpret_cast<void**>(0xac1c);
                    fun_3b80();
                    rsp454 = reinterpret_cast<struct s11*>(rsp708 + 8);
                    addr_ac1c_510:
                    r8d667 = 0;
                    goto addr_a970_501;
                    addr_abbb_464:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v636;
                    goto addr_abd0_511;
                    addr_ad51_477:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v636;
                    goto addr_ac1c_510;
                    addr_ac24_483:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v636;
                    r8_709 = v631;
                    addr_ac40_512:
                    v30 = r8_709;
                    goto addr_a944_513;
                    addr_a928_485:
                    rsp710 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp710 = reinterpret_cast<void**>(0xa92d);
                    rax711 = fun_36d0();
                    rsp454 = reinterpret_cast<struct s11*>(rsp710 + 8);
                    r14_49 = v302;
                    rbx26 = v636;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax711) == 6)) {
                        addr_abd0_511:
                        rsp712 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp712 = reinterpret_cast<void**>(0xabdd);
                        rax713 = quotearg_style(4, r15_15, 4, r15_15);
                        rsp670 = reinterpret_cast<struct s11*>(rsp712 + 8);
                        v30 = rax713;
                        goto addr_abf0_494;
                    } else {
                        addr_a944_513:
                        less_or_equal714 = reinterpret_cast<signed char>(v30) <= reinterpret_cast<signed char>(v631);
                        *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<uint1_t>(!less_or_equal714);
                        if (!less_or_equal714) 
                            goto addr_ac62_490;
                        if (v634) 
                            goto addr_ac62_490; else 
                            goto addr_a968_492;
                    }
                    addr_ad88_474:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v636;
                    rsp715 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp715 = reinterpret_cast<void**>(0xadb0);
                    rax716 = quotearg_n_style_colon();
                    rsp670 = reinterpret_cast<struct s11*>(rsp715 + 8);
                    v30 = rax716;
                    goto addr_abf0_494;
                    addr_ad6b_488:
                    r14_49 = v302;
                    r15_15 = v25;
                    r8_709 = rax664;
                    rbx26 = v636;
                    goto addr_ac40_512;
                    addr_a11d_450:
                    v586 = reinterpret_cast<void**>(0);
                    r9 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                    r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(rbx26 + 12) == 3)));
                    addr_a133_455:
                    rsi19 = r12d472;
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp717 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp717 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf6);
                    rcx22 = v47;
                    rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf8);
                    rsp718 = rsp717 - 8;
                    *rsp718 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd00);
                    *reinterpret_cast<int32_t*>(&rax719) = v622;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax719) + 4) = 0;
                    rsp720 = rsp718 - 8;
                    *rsp720 = reinterpret_cast<void**>(0xff);
                    rsp721 = rsp720 - 8;
                    *rsp721 = v18;
                    rsp722 = reinterpret_cast<struct s13*>(rsp721 - 8);
                    rsp722->f0 = r15_15;
                    rsp723 = reinterpret_cast<struct s12**>(reinterpret_cast<uint64_t>(rsp722) - 8);
                    *rsp723 = rax719;
                    rsp724 = reinterpret_cast<struct s14*>(rsp723 - 1);
                    rsp724->f0 = 0xa174;
                    eax725 = sparse_copy(r13d447, rsi19, rdx100, rcx22, v586, r9, rsp724->f8, rsp724->f10, rsp724->f18, 0xff, rsp724->f28, rsp724->f30);
                    rsp454 = reinterpret_cast<struct s11*>(&rsp724->f8 + 48);
                    r8_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax725) ^ 1);
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    goto addr_a17e_502;
                    addr_8d66_435:
                    r8d610 = reinterpret_cast<void**>(1);
                    if (v726 < v727 / 0x200) {
                        rsp728 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp728 = reinterpret_cast<void**>(0x8d99);
                        rax729 = fun_38b0();
                        rsp454 = reinterpret_cast<struct s11*>(rsp728 + 8);
                        v302 = rax729;
                        if (reinterpret_cast<signed char>(rax729) >= reinterpret_cast<signed char>(0) || (rsp730 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp730 = reinterpret_cast<void**>(0x8dae), rax731 = fun_36d0(), rsp454 = reinterpret_cast<struct s11*>(rsp730 + 8), eax732 = *reinterpret_cast<void***>(rax731), v30 = rax731, eax732 == 6)) {
                            r8d610 = reinterpret_cast<void**>(3);
                            goto addr_a04d_442;
                        } else {
                            if (eax732 == 22 || eax732 == 95) {
                                r8d610 = reinterpret_cast<void**>(2);
                                goto addr_a04d_442;
                            } else {
                                rsp733 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp733 = reinterpret_cast<void**>(0x8de2);
                                rax734 = quotearg_style(4, r15_15, 4, r15_15);
                                v28 = rax734;
                                rsp735 = rsp733 + 8 - 8;
                                *rsp735 = reinterpret_cast<void**>(0x8dfc);
                                rax736 = fun_3800();
                                r8_5 = v30;
                                rcx22 = v28;
                                rdx100 = rax736;
                                rsi19 = *reinterpret_cast<void***>(r8_5);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rsp737 = rsp735 + 8 - 8;
                                *rsp737 = reinterpret_cast<void**>(0x8e19);
                                fun_3b80();
                                rsp454 = reinterpret_cast<struct s11*>(rsp737 + 8);
                                goto addr_8e19_418;
                            }
                        }
                    }
                    addr_a28f_377:
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                        rsp738 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp738 = reinterpret_cast<void**>(0xae39);
                        rax739 = quotearg_style(4, v18, 4, v18);
                        rsp740 = rsp738 + 8 - 8;
                        *rsp740 = reinterpret_cast<void**>(0xae4f);
                        rax741 = fun_3800();
                        rsp742 = rsp740 + 8 - 8;
                        *rsp742 = reinterpret_cast<void**>(0xae61);
                        fun_3b40(1, rax741, rax739, rcx22);
                        rsp454 = reinterpret_cast<struct s11*>(rsp742 + 8);
                        goto addr_a299_373;
                    }
                    addr_8c44_367:
                    if (!*reinterpret_cast<void***>(rbx26 + 40)) {
                        if (!*reinterpret_cast<signed char*>(rbx26 + 51)) {
                            v512 = 0;
                            v511 = reinterpret_cast<void**>(0);
                            goto addr_8c85_387;
                        } else {
                            goto addr_8c4f_524;
                        }
                    } else {
                        addr_8c4f_524:
                        rdx100 = rbx26;
                        rsi19 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp743 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp743 = reinterpret_cast<void**>(0x8c60);
                        eax744 = set_file_security_ctx(v18);
                        rsp454 = reinterpret_cast<struct s11*>(rsp743 + 8);
                        r9 = eax744;
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        if (!*reinterpret_cast<signed char*>(&eax744) && *reinterpret_cast<unsigned char*>(rbx26 + 52)) {
                            *reinterpret_cast<unsigned char*>(&v36) = 0;
                            goto addr_8e1c_402;
                        }
                    }
                    addr_7c9e_349:
                    if (*reinterpret_cast<uint32_t*>(&v31) == "_ctype_get_mb_cur_max") {
                        rcx22 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        v28 = r9;
                        *reinterpret_cast<void***>(&rdi745) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi745) + 4) = 0;
                        rsp746 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp746 = reinterpret_cast<void**>(0x99ed);
                        eax747 = fun_3a30(rdi745, v20, rdi745, v20);
                        rsp16 = reinterpret_cast<struct s11*>(rsp746 + 8);
                        r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        if (!eax747 || (*reinterpret_cast<void***>(&rdi748) = v24, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi748) + 4) = 0, rsp749 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp749 = reinterpret_cast<void**>(0x9a15), eax750 = fun_36a0(rdi748, v20), rsp16 = reinterpret_cast<struct s11*>(rsp749 + 8), r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28))), *reinterpret_cast<int32_t*>(&r9 + 4) = 0, eax750 == 0)) {
                            addr_7633_344:
                            r8_5 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&v31) != 0x4000)));
                            if (*reinterpret_cast<unsigned char*>(&v36)) 
                                goto addr_765d_527;
                            eax751 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 20)) ^ 1);
                            *reinterpret_cast<unsigned char*>(&eax751) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax751) & reinterpret_cast<unsigned char>(r8_5));
                            if (!*reinterpret_cast<unsigned char*>(&eax751)) 
                                goto addr_765d_527;
                        } else {
                            rsp752 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp752 = reinterpret_cast<void**>(0x9a36);
                            rax753 = quotearg_style(4, v18, 4, v18);
                            rsp260 = reinterpret_cast<struct s11*>(rsp752 + 8);
                            r13_29 = rax753;
                            goto addr_8f28_270;
                        }
                    } else {
                        eax754 = *reinterpret_cast<uint32_t*>(&v31);
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax754) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax754) + 1) & 0xbf);
                        if (eax754 == 0x2000 || *reinterpret_cast<uint32_t*>(&v31) == 0xc000) {
                            rcx22 = v755;
                            r13_29 = reinterpret_cast<void**>(0);
                            v28 = r9;
                            *reinterpret_cast<void***>(&rdi756) = v24;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi756) + 4) = 0;
                            rsp757 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp757 = reinterpret_cast<void**>(0x9545);
                            eax758 = fun_3a30(rdi756, v20, rdi756, v20);
                            rsp16 = reinterpret_cast<struct s11*>(rsp757 + 8);
                            r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (!eax758) 
                                goto addr_7633_344;
                            rsp759 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp759 = reinterpret_cast<void**>(0x9566);
                            rax760 = quotearg_style(4, v18, 4, v18);
                            rsp260 = reinterpret_cast<struct s11*>(rsp759 + 8);
                            r13_29 = rax760;
                            goto addr_8f28_270;
                        } else {
                            if (*reinterpret_cast<uint32_t*>(&v31) != 0xa000) {
                                rsp761 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp761 = reinterpret_cast<void**>(0x9ab4);
                                rax762 = quotearg_style(4, r15_15, 4, r15_15);
                                rsp245 = reinterpret_cast<struct s11*>(rsp761 + 8);
                                r12_246 = rax762;
                                goto addr_8678_249;
                            } else {
                                rsp763 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp763 = reinterpret_cast<void**>(0x7cef);
                                rax765 = areadlink_with_size(r15_15, v764);
                                rsp766 = reinterpret_cast<struct s11*>(rsp763 + 8);
                                r13_29 = rax765;
                                if (!rax765) {
                                    rsp767 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp766) - 8);
                                    *rsp767 = reinterpret_cast<void**>(0xa37e);
                                    rax768 = quotearg_style(4, r15_15, 4, r15_15);
                                    rsp260 = reinterpret_cast<struct s11*>(rsp767 + 8);
                                    r13_29 = rax768;
                                    goto addr_8f28_270;
                                } else {
                                    rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 22)));
                                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                    rdx100 = v20;
                                    r8_5 = reinterpret_cast<void**>(0xffffffff);
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                    rsi19 = v24;
                                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                    rsp769 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp766) - 8);
                                    *rsp769 = reinterpret_cast<void**>(0x7d1a);
                                    eax770 = force_symlinkat(rax765, rsi19, rdx100, rcx22, 0xffffffff);
                                    rsp771 = reinterpret_cast<struct s11*>(rsp769 + 8);
                                    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax770) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(eax770 == 0)) {
                                        rdi17 = r13_29;
                                        rsp772 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp771) - 8);
                                        *rsp772 = reinterpret_cast<void**>(0x9f3c);
                                        fun_3670(rdi17, rdi17);
                                        rsp16 = reinterpret_cast<struct s11*>(rsp772 + 8);
                                    } else {
                                        if (*reinterpret_cast<signed char*>(rbx26 + 59) != 1 || (*reinterpret_cast<unsigned char*>(&v36) || ((v773 & reinterpret_cast<uint32_t>(fun_f000)) != 0xa000 || ((v28 = v774, rsp775 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp771) - 8), *rsp775 = reinterpret_cast<void**>(0xa4de), rax776 = fun_3820(r13_29, r13_29), rsp771 = reinterpret_cast<struct s11*>(rsp775 + 8), rdx100 = v28, rdx100 != rax776) || (*reinterpret_cast<void***>(&rdi777) = v24, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi777) + 4) = 0, rsp778 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp771) - 8), *rsp778 = reinterpret_cast<void**>(0xa500), rax779 = areadlinkat_with_size(rdi777, v20), rsp771 = reinterpret_cast<struct s11*>(rsp778 + 8), rax779 == 0))))) {
                                            addr_7d38_540:
                                            rsp780 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp771) - 8);
                                            *rsp780 = reinterpret_cast<void**>(0x7d40);
                                            fun_3670(r13_29, r13_29);
                                            rsp781 = rsp780 + 8 - 8;
                                            *rsp781 = reinterpret_cast<void**>(0x7d51);
                                            rax782 = quotearg_style(4, v18, 4, v18);
                                            r13_29 = rax782;
                                            rsp783 = rsp781 + 8 - 8;
                                            *rsp783 = reinterpret_cast<void**>(0x7d67);
                                            rax784 = fun_3800();
                                            rcx22 = r13_29;
                                            rsi19 = eax770;
                                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                            rdi17 = reinterpret_cast<void**>(0);
                                            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                            rdx100 = rax784;
                                            rsp785 = rsp783 + 8 - 8;
                                            *rsp785 = reinterpret_cast<void**>(0x7d79);
                                            fun_3b80();
                                            rsp16 = reinterpret_cast<struct s11*>(rsp785 + 8);
                                            goto addr_7d80_250;
                                        } else {
                                            rsi19 = r13_29;
                                            v28 = rax779;
                                            rsp786 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp771) - 8);
                                            *rsp786 = reinterpret_cast<void**>(0xa51b);
                                            rax787 = fun_39e0(rax779, rsi19, rdx100, rcx22, 0xffffffff);
                                            rsp788 = reinterpret_cast<struct s11*>(rsp786 + 8);
                                            if (*reinterpret_cast<int32_t*>(&rax787)) {
                                                rsp789 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp788) - 8);
                                                *rsp789 = reinterpret_cast<void**>(0xafa9);
                                                fun_3670(v28, v28);
                                                rsp771 = reinterpret_cast<struct s11*>(rsp789 + 8);
                                                goto addr_7d38_540;
                                            } else {
                                                rsp790 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp788) - 8);
                                                *rsp790 = reinterpret_cast<void**>(0xa52f);
                                                fun_3670(v28, v28);
                                                rdi17 = r13_29;
                                                rsp791 = rsp790 + 8 - 8;
                                                *rsp791 = reinterpret_cast<void**>(0xa537);
                                                fun_3670(rdi17, rdi17);
                                                rsp16 = reinterpret_cast<struct s11*>(rsp791 + 8);
                                            }
                                        }
                                    }
                                    if (*reinterpret_cast<signed char*>(rbx26 + 51)) 
                                        goto addr_8303_125;
                                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 29)));
                                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                    r13_29 = reinterpret_cast<void**>(1);
                                    if (r9) {
                                        rcx22 = v792;
                                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                        *reinterpret_cast<int32_t*>(&rdx793) = v794;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx793) + 4) = 0;
                                        r8_5 = reinterpret_cast<void**>(0x100);
                                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                        v28 = r9;
                                        *reinterpret_cast<void***>(&rdi795) = v24;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi795) + 4) = 0;
                                        rsp796 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp796 = reinterpret_cast<void**>(0x9f85);
                                        eax797 = fun_3bf0(rdi795, v20, rdx793, rcx22, 0x100);
                                        rsp16 = reinterpret_cast<struct s11*>(rsp796 + 8);
                                        if (!eax797) {
                                            r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                                            r9 = reinterpret_cast<void**>(0);
                                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                            goto addr_7633_344;
                                        } else {
                                            rsp798 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                            *rsp798 = reinterpret_cast<void**>(0x9f9d);
                                            eax799 = chown_failure_ok(rbx26, v20, rdx793, rcx22, 0x100);
                                            rsp16 = reinterpret_cast<struct s11*>(rsp798 + 8);
                                            r9 = reinterpret_cast<void**>(0);
                                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                            r13_29 = eax799;
                                            if (!*reinterpret_cast<signed char*>(&eax799)) {
                                                rsp800 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                *rsp800 = reinterpret_cast<void**>(0x9fbe);
                                                rax801 = fun_3800();
                                                rsp802 = rsp800 + 8 - 8;
                                                *rsp802 = reinterpret_cast<void**>(0x9fc6);
                                                rax803 = fun_36d0();
                                                rcx22 = v18;
                                                rdi17 = reinterpret_cast<void**>(0);
                                                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                                rdx100 = rax801;
                                                rsi19 = *reinterpret_cast<void***>(rax803);
                                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                                rsp804 = rsp802 + 8 - 8;
                                                *rsp804 = reinterpret_cast<void**>(0x9fdb);
                                                fun_3b80();
                                                rsp16 = reinterpret_cast<struct s11*>(rsp804 + 8);
                                                if (*reinterpret_cast<unsigned char*>(rbx26 + 50)) 
                                                    goto addr_7d80_250;
                                                r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                                                r9 = reinterpret_cast<void**>(0);
                                                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                                goto addr_7633_344;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!*reinterpret_cast<void***>(rbx26 + 40)) {
                        if (!*reinterpret_cast<signed char*>(rbx26 + 51)) {
                            addr_8770_553:
                            r8_5 = eax751;
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            goto addr_765d_527;
                        }
                    }
                    rdi17 = v18;
                    rsi19 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rdx100 = rbx26;
                    v31 = r9;
                    rsp805 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp805 = reinterpret_cast<void**>(0x874b);
                    eax806 = set_file_security_ctx(rdi17);
                    rsp16 = reinterpret_cast<struct s11*>(rsp805 + 8);
                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31)));
                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                    r8_5 = eax806;
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&eax806)) {
                        addr_765d_527:
                        eax807 = v27;
                        if (!*reinterpret_cast<signed char*>(&eax807)) {
                            r12_46 = reinterpret_cast<void**>(1);
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            goto addr_76f0_293;
                        } else {
                            r12_46 = eax807;
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            v32 = 0;
                            goto addr_767f_291;
                        }
                    } else {
                        eax808 = *reinterpret_cast<unsigned char*>(rbx26 + 52);
                        *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax808);
                        if (*reinterpret_cast<unsigned char*>(&eax808)) 
                            goto addr_7d80_250; else 
                            goto addr_8770_553;
                    }
                    addr_7470_220:
                    if (*reinterpret_cast<void***>(rbx26 + 40)) {
                        rsp809 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp809 = reinterpret_cast<void**>(0x748b);
                        set_file_security_ctx(v18, v18);
                        rsp16 = reinterpret_cast<struct s11*>(rsp809 + 8);
                    }
                    if (v31) {
                        *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
                    }
                    if (!v27) 
                        goto addr_74b0_129;
                    if (*reinterpret_cast<signed char*>(rbx26 + 63)) 
                        break;
                    rdi810 = *reinterpret_cast<void***>(rbx26 + 72);
                    rdx811 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
                    rsp812 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp812 = reinterpret_cast<void**>(0x8977);
                    record_file(rdi810, v20, rdx811, rcx22, rdi810, v20, rdx811, rcx22);
                    rsp16 = reinterpret_cast<struct s11*>(rsp812 + 8);
                    r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v27)));
                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                    continue;
                    eax813 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                    if (!*reinterpret_cast<signed char*>(&eax813)) {
                        if (!*reinterpret_cast<void***>(rbx26 + 48)) 
                            goto addr_74d8_212;
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 23)) 
                            goto addr_792c_568;
                    } else {
                        if (v814 == 1) {
                            rsp815 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp815 = reinterpret_cast<void**>(0x8a83);
                            rax818 = src_to_dest_lookup(v816, v817, rdx3, rcx22, r8_5, r9);
                            rsp16 = reinterpret_cast<struct s11*>(rsp815 + 8);
                            r14_49 = rax818;
                            goto addr_86b5_571;
                        } else {
                            if (!*reinterpret_cast<void***>(rbx26 + 48) || *reinterpret_cast<unsigned char*>(rbx26 + 23)) {
                                *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                                *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0;
                                goto addr_7155_574;
                            }
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                    *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0;
                    goto addr_7963_576;
                    addr_792c_568:
                    if (v819 > 1 || (v27 && *reinterpret_cast<signed char*>(rbx26 + 4) == 3 || (*reinterpret_cast<uint32_t*>(&r14_49) = 0, *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0, *reinterpret_cast<signed char*>(rbx26 + 4) == 4))) {
                        rsp820 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp820 = reinterpret_cast<void**>(0x86b2);
                        rax823 = remember_copied(v20, v821, v822, rcx22, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s11*>(rsp820 + 8);
                        r14_49 = rax823;
                    } else {
                        addr_795b_578:
                        if (*reinterpret_cast<signed char*>(&eax813)) {
                            addr_7155_574:
                            if (r13_29 == 17) {
                                rcx22 = v20;
                                rsp824 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp824 = reinterpret_cast<void**>(0x84ba);
                                eax825 = fun_3c00(0xffffff9c, r15_15, 0xffffff9c, r15_15);
                                rsp16 = reinterpret_cast<struct s11*>(rsp824 + 8);
                                if (!eax825) 
                                    goto addr_7466_218;
                                rsp826 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp826 = reinterpret_cast<void**>(0x84c7);
                                rax827 = fun_36d0();
                                rsp16 = reinterpret_cast<struct s11*>(rsp826 + 8);
                                r13_29 = *reinterpret_cast<void***>(rax827);
                                if (r13_29) 
                                    goto addr_715f_581; else 
                                    goto addr_84d3_582;
                            } else {
                                addr_715f_581:
                                if (r13_29 == 22) {
                                    rsp828 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp828 = reinterpret_cast<void**>(0x8aee);
                                    rax829 = quotearg_n_style();
                                    rbx26 = rax829;
                                    rsp830 = rsp828 + 8 - 8;
                                    *rsp830 = reinterpret_cast<void**>(0x8b04);
                                    quotearg_n_style();
                                    rsp831 = rsp830 + 8 - 8;
                                    *rsp831 = reinterpret_cast<void**>(0x8b1a);
                                    fun_3800();
                                    r8_5 = rbx26;
                                    r12_46 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                    rsp832 = rsp831 + 8 - 8;
                                    *rsp832 = reinterpret_cast<void**>(0x8b34);
                                    fun_3b80();
                                    rsp16 = reinterpret_cast<struct s11*>(rsp832 + 8);
                                    *reinterpret_cast<void***>(v30) = reinterpret_cast<void**>(1);
                                    continue;
                                }
                            }
                        } else {
                            addr_7963_576:
                            r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v36)));
                            goto addr_74e3_231;
                        }
                    }
                    addr_86b5_571:
                    if (!r14_49) {
                        addr_7090_226:
                        eax813 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                        goto addr_795b_578;
                    } else {
                        if ((reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                            addr_7031_227:
                            rsp833 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp833 = reinterpret_cast<void**>(0x7047);
                            eax834 = same_nameat();
                            rsp835 = reinterpret_cast<struct s11*>(rsp833 + 8);
                            if (*reinterpret_cast<signed char*>(&eax834)) {
                                rsp836 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp835) - 8);
                                *rsp836 = reinterpret_cast<void**>(0x94c5);
                                rax837 = quotearg_n_style();
                                r13_29 = rax837;
                                rsp838 = rsp836 + 8 - 8;
                                *rsp838 = reinterpret_cast<void**>(0x94db);
                                rax839 = quotearg_n_style();
                                rsp840 = rsp838 + 8 - 8;
                                *rsp840 = reinterpret_cast<void**>(0x94f1);
                                rax841 = fun_3800();
                                r8_5 = r13_29;
                                rcx22 = rax839;
                                rsi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax841;
                                rdi17 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                rsp842 = rsp840 + 8 - 8;
                                *rsp842 = reinterpret_cast<void**>(0x9505);
                                fun_3b80();
                                rsp16 = reinterpret_cast<struct s11*>(rsp842 + 8);
                                *reinterpret_cast<void***>(v30) = reinterpret_cast<void**>(1);
                            } else {
                                rcx22 = r14_49;
                                rsp843 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp835) - 8);
                                *rsp843 = reinterpret_cast<void**>(0x7066);
                                eax844 = same_nameat();
                                rsp16 = reinterpret_cast<struct s11*>(rsp843 + 8);
                                if (*reinterpret_cast<signed char*>(&eax844)) 
                                    goto addr_9a60_587;
                                if (*reinterpret_cast<signed char*>(rbx26 + 4) == 4) 
                                    goto addr_7090_226; else 
                                    goto addr_7076_589;
                            }
                        } else {
                            rax845 = v47;
                            *reinterpret_cast<int32_t*>(&rax845 + 4) = 0;
                            rdx100 = r14_49;
                            rdi17 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                            rsi19 = v24;
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            r9 = v20;
                            rsp846 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8 - 8);
                            *rsp846 = rax845;
                            *reinterpret_cast<uint32_t*>(&rax847) = *reinterpret_cast<unsigned char*>(rbx26 + 60);
                            *reinterpret_cast<int32_t*>(&rax847 + 4) = 0;
                            rcx22 = v18;
                            r8_5 = rsi19;
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            rsp848 = rsp846 - 8;
                            *rsp848 = rax847;
                            rsp849 = rsp848 - 8;
                            *rsp849 = reinterpret_cast<void**>(1);
                            rsp850 = reinterpret_cast<struct s13*>(rsp849 - 8);
                            rsp850->f0 = reinterpret_cast<void**>(0x8707);
                            al851 = create_hard_link(0, rsi19, rdx100, rcx22, r8_5, r9, 1, rsp850->f10, rsp850->f18);
                            rsp16 = reinterpret_cast<struct s11*>(&rsp850->f8 + 8);
                            if (al851) 
                                goto addr_74b0_129;
                        }
                    }
                    addr_8713_591:
                    if (!*reinterpret_cast<signed char*>(rbx26 + 51)) 
                        goto addr_7d93_335; else 
                        goto addr_871d_126;
                    addr_7076_589:
                    if (*reinterpret_cast<signed char*>(rbx26 + 4) != 3 || !v27) {
                        r15_15 = v18;
                        rsp852 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp852 = reinterpret_cast<void**>(0x982f);
                        rax853 = subst_suffix(r15_15, v20, r14_49, rcx22, r8_5, r9, r15_15, v20, r14_49, rcx22);
                        rsp854 = rsp852 + 8 - 8;
                        *rsp854 = reinterpret_cast<void**>(0x9844);
                        rax855 = quotearg_n_style();
                        r14_49 = rax855;
                        rsp856 = rsp854 + 8 - 8;
                        *rsp856 = reinterpret_cast<void**>(0x9856);
                        rax857 = quotearg_n_style();
                        r13_29 = rax857;
                        rsp858 = rsp856 + 8 - 8;
                        *rsp858 = reinterpret_cast<void**>(0x986c);
                        rax859 = fun_3800();
                        r8_5 = r14_49;
                        rcx22 = r13_29;
                        rdx100 = rax859;
                        rsi19 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp860 = rsp858 + 8 - 8;
                        *rsp860 = reinterpret_cast<void**>(0x9880);
                        fun_3b80();
                        rdi17 = rax853;
                        rsp861 = rsp860 + 8 - 8;
                        *rsp861 = reinterpret_cast<void**>(0x9888);
                        fun_3670(rdi17, rdi17);
                        rsp16 = reinterpret_cast<struct s11*>(rsp861 + 8);
                        goto addr_8713_591;
                    } else {
                        goto addr_7090_226;
                    }
                    addr_84d3_582:
                    goto addr_7466_218;
                    if (!reinterpret_cast<int1_t>(r13_29 == 18)) {
                        rsp862 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp862 = reinterpret_cast<void**>(0x8506);
                        rax863 = quotearg_n_style();
                        rbx26 = rax863;
                        rsp864 = rsp862 + 8 - 8;
                        *rsp864 = reinterpret_cast<void**>(0x8518);
                        rax865 = quotearg_n_style();
                        rsp866 = rsp864 + 8 - 8;
                        *rsp866 = reinterpret_cast<void**>(0x852e);
                        rax867 = fun_3800();
                        rsp868 = reinterpret_cast<struct s11*>(rsp866 + 8);
                        r8_5 = rbx26;
                        rcx869 = rax865;
                        rdx870 = rax867;
                    } else {
                        *reinterpret_cast<void***>(&rdi871) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi871) + 4) = 0;
                        rsp872 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp872 = reinterpret_cast<void**>(0x71a2);
                        eax873 = fun_3720(rdi871, v20, rdi871, v20);
                        rsp16 = reinterpret_cast<struct s11*>(rsp872 + 8);
                        if (!eax873 || (rsp874 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp874 = reinterpret_cast<void**>(0x71ab), rax875 = fun_36d0(), rsp16 = reinterpret_cast<struct s11*>(rsp874 + 8), reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax875) == 2))) {
                            al876 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(v41) & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000)) & *reinterpret_cast<unsigned char*>(rbx26 + 60));
                            *reinterpret_cast<unsigned char*>(&v36) = al876;
                            if (al876) {
                                r13_29 = reinterpret_cast<void**>(1);
                                rsp877 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp877 = reinterpret_cast<void**>(0x8aa9);
                                rax878 = fun_3800();
                                rsp879 = rsp877 + 8 - 8;
                                *rsp879 = reinterpret_cast<void**>(0x8ab8);
                                fun_3b40(1, rax878, 5, rcx22, 1, rax878, 5, rcx22);
                                rsp880 = rsp879 + 8 - 8;
                                *rsp880 = reinterpret_cast<void**>(0x8ace);
                                emit_verbose(r15_15, v18, v48, rcx22, r8_5, r9);
                                rsp16 = reinterpret_cast<struct s11*>(rsp880 + 8);
                                goto addr_74e3_231;
                            } else {
                                *reinterpret_cast<unsigned char*>(&v36) = 1;
                                r13_29 = reinterpret_cast<void**>(1);
                                goto addr_74e3_231;
                            }
                        } else {
                            rsp881 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp881 = reinterpret_cast<void**>(0x899e);
                            rax882 = quotearg_n_style();
                            rbx26 = rax882;
                            rsp883 = rsp881 + 8 - 8;
                            *rsp883 = reinterpret_cast<void**>(0x89b0);
                            rax884 = quotearg_n_style();
                            rsp885 = rsp883 + 8 - 8;
                            *rsp885 = reinterpret_cast<void**>(0x89c6);
                            rax886 = fun_3800();
                            rsp868 = reinterpret_cast<struct s11*>(rsp885 + 8);
                            r8_5 = rbx26;
                            rcx869 = rax884;
                            rdx870 = rax886;
                        }
                    }
                    rsp887 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp868) - 8);
                    *rsp887 = reinterpret_cast<void**>(0x8543);
                    fun_3b80();
                    rsp888 = rsp887 + 8 - 8;
                    *rsp888 = reinterpret_cast<void**>(0x8556);
                    forget_created(v889, v890, rdx870, rcx869, r8_5, r9);
                    rsp16 = reinterpret_cast<struct s11*>(rsp888 + 8);
                    goto addr_7430_197;
                    addr_7243_214:
                    if ((*reinterpret_cast<uint32_t*>(&v339) & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000 && (rdi891 = *reinterpret_cast<void***>(rbx26 + 72), rsp892 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp892 = reinterpret_cast<void**>(0x7266), eax893 = seen_file(rdi891, v20, rdx3, rcx22, rdi891, v20, rdx3, rcx22), rsp16 = reinterpret_cast<struct s11*>(rsp892 + 8), !!*reinterpret_cast<signed char*>(&eax893))) {
                        rsp894 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp894 = reinterpret_cast<void**>(0x7284);
                        rax895 = quotearg_n_style();
                        rbx26 = rax895;
                        rsp896 = rsp894 + 8 - 8;
                        *rsp896 = reinterpret_cast<void**>(0x7296);
                        quotearg_n_style();
                        rsp897 = rsp896 + 8 - 8;
                        *rsp897 = reinterpret_cast<void**>(0x72ac);
                        fun_3800();
                        r8_5 = rbx26;
                        rsp898 = rsp897 + 8 - 8;
                        *rsp898 = reinterpret_cast<void**>(0x72c0);
                        fun_3b80();
                        rsp16 = reinterpret_cast<struct s11*>(rsp898 + 8);
                        continue;
                    }
                    addr_842f_206:
                    if (!*reinterpret_cast<void***>(rbx26 + 48)) 
                        goto addr_9c5e_603;
                    if (v899 > 1) 
                        goto addr_8447_204;
                    addr_9c5e_603:
                    zf900 = *reinterpret_cast<signed char*>(rbx26 + 4) == 2;
                    r13_29 = reinterpret_cast<void**>(17);
                    v48 = reinterpret_cast<void**>(0);
                    if (!zf900) 
                        goto addr_6fc0_36;
                    if ((v901 & reinterpret_cast<uint32_t>(fun_f000)) != 0x8000) {
                        addr_8447_204:
                        *reinterpret_cast<void***>(&rdi902) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi902) + 4) = 0;
                        rdx3 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        rsp903 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp903 = reinterpret_cast<void**>(0x845b);
                        eax904 = fun_3720(rdi902, v20, rdi902, v20);
                        rsp16 = reinterpret_cast<struct s11*>(rsp903 + 8);
                        if (eax904 && (rsp905 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp905 = reinterpret_cast<void**>(0x8464), rax906 = fun_36d0(), rsp16 = reinterpret_cast<struct s11*>(rsp905 + 8), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax906) == 2))) {
                            rsp907 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp907 = reinterpret_cast<void**>(0xa2ec);
                            rax908 = quotearg_style(4, v18, 4, v18);
                            r14_49 = rax908;
                            rsp909 = rsp907 + 8 - 8;
                            *rsp909 = reinterpret_cast<void**>(0xa302);
                            fun_3800();
                            rsp910 = rsp909 + 8 - 8;
                            *rsp910 = reinterpret_cast<void**>(0xa315);
                            fun_3b80();
                            rsp16 = reinterpret_cast<struct s11*>(rsp910 + 8);
                            continue;
                        }
                    } else {
                        goto addr_6fc0_36;
                    }
                    eax911 = *reinterpret_cast<unsigned char*>(rbx26 + 60);
                    *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax911);
                    if (*reinterpret_cast<unsigned char*>(&eax911)) {
                        r13_29 = reinterpret_cast<void**>(17);
                        rsp912 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp912 = reinterpret_cast<void**>(0xa00f);
                        rax913 = quotearg_style(4, v18, 4, v18);
                        rsp914 = rsp912 + 8 - 8;
                        *rsp914 = reinterpret_cast<void**>(0xa025);
                        rax915 = fun_3800();
                        rdx3 = rax913;
                        rsp916 = rsp914 + 8 - 8;
                        *rsp916 = reinterpret_cast<void**>(0xa037);
                        fun_3b40(1, rax915, rdx3, rcx22, 1, rax915, rdx3, rcx22);
                        rsp16 = reinterpret_cast<struct s11*>(rsp916 + 8);
                        v48 = reinterpret_cast<void**>(0);
                        goto addr_6fc0_36;
                    } else {
                        *reinterpret_cast<unsigned char*>(&v36) = 1;
                        goto addr_8489_175;
                    }
                    addr_91b0_140:
                    if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                        addr_832c_124:
                        if (*reinterpret_cast<void***>(rbx26 + 8) == 2) 
                            goto addr_74b0_129;
                        if (*reinterpret_cast<void***>(rbx26 + 8) != 3) 
                            goto addr_8341_123;
                        rcx22 = v20;
                        r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                        rsi19 = v18;
                        rsp917 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp917 = reinterpret_cast<void**>(0x971a);
                        al918 = overwrite_ok(rbx26, rsi19, v24, rcx22, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s11*>(rsp917 + 8);
                        if (al918) 
                            goto addr_8341_123; else 
                            goto addr_9722_613;
                    } else {
                        goto addr_886a_121;
                    }
                    addr_8288_141:
                    if (v31) {
                        *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
                    }
                    rsp919 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp919 = reinterpret_cast<void**>(0x82b1);
                    rax922 = remember_copied(v20, v920, v921, rcx22, r8_5, r9);
                    rsp16 = reinterpret_cast<struct s11*>(rsp919 + 8);
                    rdx100 = rax922;
                    if (!rax922) 
                        goto addr_74b0_129;
                    rax923 = v47;
                    *reinterpret_cast<int32_t*>(&rax923 + 4) = 0;
                    rdi17 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rsi19 = v24;
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    r9 = v20;
                    rcx22 = v18;
                    rsp924 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8 - 8);
                    *rsp924 = rax923;
                    *reinterpret_cast<uint32_t*>(&rax925) = *reinterpret_cast<unsigned char*>(rbx26 + 60);
                    *reinterpret_cast<int32_t*>(&rax925 + 4) = 0;
                    r8_5 = rsi19;
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    rsp926 = rsp924 - 8;
                    *rsp926 = rax925;
                    rsp927 = rsp926 - 8;
                    *rsp927 = reinterpret_cast<void**>(1);
                    rsp928 = reinterpret_cast<struct s13*>(rsp927 - 8);
                    rsp928->f0 = reinterpret_cast<void**>(0x82ed);
                    al929 = create_hard_link(0, rsi19, rdx100, rcx22, r8_5, r9, 1, rsp928->f10, rsp928->f18);
                    rsp16 = reinterpret_cast<struct s11*>(&rsp928->f8 + 8);
                    if (al929) 
                        goto addr_74b0_129;
                    if (!*reinterpret_cast<signed char*>(rbx26 + 51)) 
                        goto addr_7430_197; else 
                        goto addr_8303_125;
                }
                continue;
                addr_9a60_587:
                rsi930 = top_level_src_name;
                rsp931 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp931 = reinterpret_cast<void**>(0x9a71);
                quotearg_style(4, rsi930, 4, rsi930);
                rsp932 = rsp931 + 8 - 8;
                *rsp932 = reinterpret_cast<void**>(0x9a87);
                fun_3800();
                rsp933 = rsp932 + 8 - 8;
                *rsp933 = reinterpret_cast<void**>(0x9a98);
                fun_3b80();
                rsp16 = reinterpret_cast<struct s11*>(rsp933 + 8);
                if (*reinterpret_cast<void***>(rbx26 + 24)) 
                    goto addr_8908_127;
                continue;
                addr_a1d4_148:
                continue;
                addr_9722_613:
            }
            addr_8904_150:
        }
    }
}

uint32_t force_linkat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

signed char create_hard_link(void** rdi, void** esi, void** rdx, void** rcx, void** r8d, void** r9, signed char a7, int32_t a8, unsigned char a9) {
    int64_t rdi10;
    int64_t rdx11;
    int32_t r12d12;
    void** r8_13;
    uint32_t eax14;
    void** r9_15;
    void** v16;
    void** rax17;
    uint32_t ebp18;
    void** rax19;
    void** rax20;
    int32_t eax21;

    *reinterpret_cast<void***>(&rdi10) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    *reinterpret_cast<void***>(&rdx11) = r8d;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
    r12d12 = a8;
    *reinterpret_cast<uint32_t*>(&r8_13) = static_cast<uint32_t>(a9) << 10;
    *reinterpret_cast<int32_t*>(&r8_13 + 4) = 0;
    eax14 = force_linkat(rdi10, rdx, rdx11, r9);
    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax14 == 0))) {
        *reinterpret_cast<int32_t*>(&r9_15) = 0;
        *reinterpret_cast<int32_t*>(&r9_15 + 4) = 0;
        if (!rdi) {
            rax17 = subst_suffix(rcx, r9, rdx, v16, r8_13, 0);
            r9_15 = rax17;
        }
        quotearg_n_style();
        quotearg_n_style();
        fun_3800();
        r12d12 = 0;
        fun_3b80();
        fun_3670(r9_15, r9_15);
    } else {
        ebp18 = eax14 >> 31;
        *reinterpret_cast<unsigned char*>(&r12d12) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d12) & *reinterpret_cast<unsigned char*>(&ebp18));
        if (*reinterpret_cast<unsigned char*>(&r12d12)) {
            rax19 = quotearg_style(4, rcx, 4, rcx);
            rax20 = fun_3800();
            fun_3b40(1, rax20, rax19, v16);
        } else {
            r12d12 = 1;
        }
    }
    eax21 = r12d12;
    return *reinterpret_cast<signed char*>(&eax21);
}

void restore_default_fscreatecon_or_die(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rax8;
    void** rax9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** rdi13;
    void** rax14;
    int64_t rbx15;

    rax7 = fun_36d0();
    *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(95);
    rax8 = fun_3800();
    fun_3b80();
    rax9 = quotearg_n_style();
    rax10 = quotearg_n_style();
    fun_3b40(1, "%s -> %s", rax10, rax9);
    if (rax8) {
        rax11 = quotearg_style(4, rax8, 4, rax8);
        rax12 = fun_3800();
        fun_3b40(1, rax12, rax11, rax9);
    }
    rdi13 = stdout;
    rax14 = *reinterpret_cast<void***>(rdi13 + 40);
    if (reinterpret_cast<unsigned char>(rax14) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi13 + 48))) {
        *reinterpret_cast<void***>(rdi13 + 40) = rax14 + 1;
        *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(10);
        goto rbx15;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void** stderr = reinterpret_cast<void**>(0);

void fun_3c40(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void* a7, void** a8, int64_t a9, void* a10, ...);

void strmode(int64_t rdi, void* rsi, int64_t rdx, void** rcx);

signed char overwrite_ok(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    void* rax7;
    signed char al8;
    int64_t rdi9;
    int32_t eax10;
    void* rsp11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** rcx15;
    void** rdi16;
    void** rdx17;
    void* v18;
    void** v19;
    int64_t v20;
    int64_t rdi21;
    void** r12d22;
    void** rax23;
    void* rsp24;
    void** r13_25;
    void** v26;
    void** rax27;
    void* rsp28;
    void** rdx29;
    void** rax30;
    int64_t v31;
    void* v32;
    void* rax33;
    int32_t ebx34;
    void** rax35;
    int64_t v36;
    int64_t rdi37;
    void** rdx38;
    int32_t eax39;
    void** rax40;
    int64_t v41;

    rax7 = g28;
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 24)) & reinterpret_cast<uint32_t>(fun_f000)) == 0xa000 || ((al8 = can_write_any_file(rdi, rsi), !!al8) || (*reinterpret_cast<void***>(&rdi9) = edx, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0, eax10 = fun_3750(rdi9, rcx, 2, 0x200, r8, r9), rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32 - 8 + 8 - 8 + 8), eax10 == 0))) {
        rax12 = quotearg_style(4, rsi, 4, rsi);
        r12_13 = program_name;
        rax14 = fun_3800();
        rcx15 = r12_13;
        rdi16 = stderr;
        rdx17 = rax14;
        fun_3c40(rdi16, 1, rdx17, rcx15, rax12, r9, v18, v19, v20, rax7);
    } else {
        *reinterpret_cast<void***>(&rdi21) = *reinterpret_cast<void***>(r8 + 24);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
        strmode(rdi21, reinterpret_cast<int64_t>(rsp11) + 12, 2, 0x200);
        r12d22 = *reinterpret_cast<void***>(r8 + 24);
        rax23 = quotearg_style(4, rsi, 4, rsi);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
        *reinterpret_cast<uint32_t*>(&r12_13) = reinterpret_cast<unsigned char>(r12d22) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
        r13_25 = program_name;
        if (*reinterpret_cast<void***>(rdi + 24) || *reinterpret_cast<unsigned char*>(rdi + 20) & 0xffff00) {
            v26 = reinterpret_cast<void**>(0x61ec);
            rax27 = fun_3800();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            rdx29 = rax27;
        } else {
            v26 = reinterpret_cast<void**>(0x6233);
            rax30 = fun_3800();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            rdx29 = rax30;
        }
        rcx15 = r13_25;
        rdi16 = stderr;
        fun_3c40(rdi16, 1, rdx29, rcx15, rax23, r12_13, reinterpret_cast<int64_t>(rsp28) - 8 + 21, v26, v31, v32);
        rdx17 = v26;
    }
    rax33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax7) - reinterpret_cast<int64_t>(g28));
    if (!rax33) {
    }
    fun_3840();
    ebx34 = *reinterpret_cast<int32_t*>(&rdx17);
    rax35 = fun_38b0();
    if (reinterpret_cast<signed char>(rax35) >= reinterpret_cast<signed char>(0)) 
        goto addr_626b_12;
    quotearg_style(4, 1, 4, 1);
    fun_3800();
    fun_36d0();
    fun_3b80();
    goto v36;
    addr_626b_12:
    if (!(!*reinterpret_cast<signed char*>(&ebx34) || ((*reinterpret_cast<int32_t*>(&rdi37) = *reinterpret_cast<int32_t*>(&rdi16), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0, rdx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax35) - reinterpret_cast<unsigned char>(rcx15)), eax39 = fun_39a0(rdi37, 3, rdx38, rcx15, rdi37, 3, rdx38, rcx15), eax39 >= 0) || (rax40 = fun_36d0(), !!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax40) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax40) == 38))))))) {
        quotearg_style(4, 1, 4, 1);
        fun_3800();
        fun_3b80();
    }
    goto v41;
}

void** ximalloc(void* rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** subst_suffix(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void** rsi7;
    void** rax8;
    void** r15_9;
    void** rax10;

    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) - reinterpret_cast<unsigned char>(rdi));
    rax8 = fun_3820(rdx);
    r15_9 = rax8 + 1;
    rax10 = ximalloc(reinterpret_cast<unsigned char>(r15_9) + reinterpret_cast<unsigned char>(rsi7), rsi7, rdx, rcx, r8);
    fun_3a80(reinterpret_cast<unsigned char>(rax10) + reinterpret_cast<unsigned char>(rsi7), rdx, r15_9);
}

void* fun_3900(int64_t rdi);

int32_t fun_3c10(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** xalignalloc(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fun_3970(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** full_write(int64_t rdi, void** rsi, void** rdx);

void** sparse_copy(void** edi, void** esi, void** rdx, void** rcx, void** r8, void** r9d, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12) {
    void** r14d13;
    void** v14;
    void** v15;
    void** rdx16;
    void** v17;
    void** rcx18;
    void** eax19;
    void** v20;
    void** v21;
    void** rsi22;
    void** v23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    unsigned char v28;
    void** r15d29;
    void** rdi30;
    void** v31;
    void** r13d32;
    void** r12_33;
    int64_t rdi34;
    void* rax35;
    void** rax36;
    int64_t rax37;
    void** r14d38;
    void** r15_39;
    void** v40;
    void** r12_41;
    void** r14d42;
    void** r8d43;
    void** r12_44;
    uint32_t ebx45;
    void** rax46;
    int64_t rdi47;
    int32_t eax48;
    void** rax49;
    void** eax50;
    int32_t eax51;
    void** rax52;
    void** r11_53;
    void** rax54;
    void** r11_55;
    void** v56;
    void** rax57;
    void** rbp58;
    void** r14_59;
    void** rbx60;
    unsigned char v61;
    void** r12_62;
    unsigned char v63;
    uint32_t v64;
    int64_t rdi65;
    void** rax66;
    void** eax67;
    uint32_t eax68;
    void** rax69;

    r14d13 = esi;
    v14 = rdx;
    v15 = rcx;
    rdx16 = a10;
    v17 = a8;
    rcx18 = a12;
    eax19 = a7;
    v20 = edi;
    v21 = a9;
    rsi22 = a11;
    *reinterpret_cast<void***>(rcx18) = reinterpret_cast<void**>(0);
    v23 = r8;
    v24 = r9d;
    v25 = rdx16;
    v26 = rsi22;
    v27 = rcx18;
    v28 = *reinterpret_cast<unsigned char*>(&r9d);
    *reinterpret_cast<void***>(rsi22) = reinterpret_cast<void**>(0);
    if (r8 || !*reinterpret_cast<signed char*>(&eax19)) {
        if (!v25) {
            addr_642f_3:
            r15d29 = reinterpret_cast<void**>(1);
        } else {
            addr_6448_4:
            rdi30 = v15;
            v31 = r14d13;
            if (v23) {
                rdi30 = v23;
                goto addr_645e_6;
            }
        }
    } else {
        if (!rdx16) 
            goto addr_642f_3;
        r13d32 = edi;
        r12_33 = rdx16;
        do {
            addr_63f0_9:
            r8 = reinterpret_cast<void**>(0x7fffffffc0000000);
            r9d = reinterpret_cast<void**>(0);
            rdx16 = r14d13;
            *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
            *reinterpret_cast<void***>(&rdi34) = r13d32;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            if (reinterpret_cast<unsigned char>(r12_33) <= reinterpret_cast<unsigned char>(0x7fffffffc0000000)) {
                r8 = r12_33;
            }
            rcx18 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi22) = 0;
            *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
            rax35 = fun_3900(rdi34);
            if (!rax35) 
                goto addr_6903_12;
            if (reinterpret_cast<int64_t>(rax35) >= reinterpret_cast<int64_t>(0)) {
                *reinterpret_cast<void***>(v26) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)) + reinterpret_cast<uint64_t>(rax35));
                r12_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_33) - reinterpret_cast<uint64_t>(rax35));
                if (r12_33) 
                    goto addr_63f0_9; else 
                    goto addr_642f_3;
            }
            rax36 = fun_36d0();
            rcx18 = *reinterpret_cast<void***>(rax36);
            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
            if (rcx18 == 38) 
                goto addr_6860_16;
            if (reinterpret_cast<signed char>(rcx18) > reinterpret_cast<signed char>(26)) 
                goto addr_67e0_18;
            if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rcx18) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rcx18 == 0)) 
                continue;
            rax37 = 1 << *reinterpret_cast<unsigned char*>(&rcx18);
            if (*reinterpret_cast<uint32_t*>(&rax37) & 0x4440200) 
                goto addr_6860_16;
            if (rcx18 == 1) 
                goto addr_691c_22;
        } while (rcx18 == 4);
        goto addr_67d7_24;
    }
    addr_64f7_25:
    return r15d29;
    addr_645e_6:
    r14d38 = reinterpret_cast<void**>(0);
    r15_39 = reinterpret_cast<void**>(0);
    v40 = rdi30;
    goto addr_646f_26;
    addr_6903_12:
    v25 = r12_33;
    if (!*reinterpret_cast<void***>(v26)) 
        goto addr_6448_4;
    goto addr_642f_3;
    addr_6860_16:
    v25 = r12_33;
    goto addr_6448_4;
    addr_67e0_18:
    v25 = r12_33;
    if (rcx18 == 95) 
        goto addr_6448_4;
    addr_67ee_28:
    r15d29 = reinterpret_cast<void**>(0);
    quotearg_n_style();
    quotearg_n_style();
    fun_3800();
    fun_3b80();
    goto addr_64f7_25;
    addr_67d7_24:
    goto addr_67ee_28;
    addr_691c_22:
    v25 = r12_33;
    if (!*reinterpret_cast<void***>(v26)) {
        goto addr_6448_4;
    }
    addr_6972_30:
    r12_41 = r15_39;
    r14d42 = v31;
    r8d43 = r14d38;
    addr_6943_31:
    if (!*reinterpret_cast<signed char*>(&r8d43)) 
        goto addr_642f_3;
    r12_44 = r12_41;
    ebx45 = *reinterpret_cast<unsigned char*>(&v24);
    rax46 = fun_38b0();
    if (reinterpret_cast<signed char>(rax46) >= reinterpret_cast<signed char>(0)) 
        goto addr_626b_34;
    quotearg_style(4, v21, 4, v21);
    fun_3800();
    fun_36d0();
    fun_3b80();
    return 0;
    addr_626b_34:
    if (!*reinterpret_cast<signed char*>(&ebx45) || ((*reinterpret_cast<void***>(&rdi47) = r14d42, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi47) + 4) = 0, eax48 = fun_39a0(rdi47, 3, reinterpret_cast<unsigned char>(rax46) - reinterpret_cast<unsigned char>(r12_44), r12_44), eax48 >= 0) || (rax49 = fun_36d0(), *reinterpret_cast<unsigned char*>(&r12_44) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax49) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax49) == 38))), !!*reinterpret_cast<unsigned char*>(&r12_44)))) {
        r12_44 = reinterpret_cast<void**>(1);
    } else {
        quotearg_style(4, v21, 4, v21);
        fun_3800();
        fun_3b80();
    }
    return r12_44;
    addr_68bd_39:
    r15d29 = rcx18;
    quotearg_style(4, v21, 4, v21);
    fun_3800();
    fun_36d0();
    fun_3b80();
    goto addr_64f7_25;
    addr_6938_40:
    r14d42 = v31;
    r8d43 = r13d32;
    r12_41 = r15_39;
    goto addr_6943_31;
    addr_6695_41:
    r15d29 = eax50;
    goto addr_64f7_25;
    addr_6870_42:
    r15d29 = reinterpret_cast<void**>(0);
    quotearg_style(4, v17, 4, v17);
    fun_3800();
    fun_3b80();
    goto addr_64f7_25;
    addr_64be_43:
    r15d29 = reinterpret_cast<void**>(0);
    quotearg_style(4, v17, 4, v17);
    fun_3800();
    fun_3b80();
    goto addr_64f7_25;
    while (1) {
        addr_6718_44:
        eax51 = fun_3c10(rdi30, rsi22, rdx16, rcx18, r8);
        rax52 = xalignalloc(static_cast<int64_t>(eax51), v15, rdx16, rcx18, r8);
        r11_53 = rax52;
        *reinterpret_cast<void***>(v14) = r11_53;
        while (1) {
            do {
                rdx16 = v15;
                rsi22 = r11_53;
                rdi30 = v20;
                *reinterpret_cast<int32_t*>(&rdi30 + 4) = 0;
                if (reinterpret_cast<unsigned char>(v25) <= reinterpret_cast<unsigned char>(rdx16)) {
                    rdx16 = v25;
                }
                rax54 = fun_3970(rdi30, rsi22, rdx16, rcx18, r8);
                r11_55 = r11_53;
                v56 = rax54;
                if (reinterpret_cast<signed char>(rax54) < reinterpret_cast<signed char>(0)) 
                    break;
                if (!rax54) 
                    goto addr_6972_30;
                rax57 = v26;
                rbp58 = v56;
                rcx18 = r14d38;
                r14_59 = r11_55;
                rbx60 = v40;
                *reinterpret_cast<void***>(rax57) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax57)) + reinterpret_cast<unsigned char>(rbp58));
                v61 = reinterpret_cast<uint1_t>(!!v23);
                do {
                    addr_6540_50:
                    r12_62 = rbp58;
                    if (reinterpret_cast<unsigned char>(rbx60) > reinterpret_cast<unsigned char>(rbp58)) {
                        rbx60 = rbp58;
                    }
                    r13d32 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rbx60)) & v61);
                    if (!r13d32) {
                        if (rbx60 == rbp58 && *reinterpret_cast<unsigned char*>(&rcx18) != 1 || (r13d32 = rcx18, rbx60 == 0)) {
                            addr_6652_54:
                            r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<unsigned char>(rbx60));
                            r9d = reinterpret_cast<void**>(0);
                            v63 = *reinterpret_cast<unsigned char*>(&rcx18);
                            v64 = 1;
                            if (!*reinterpret_cast<unsigned char*>(&rcx18)) {
                                addr_65e4_55:
                                *reinterpret_cast<void***>(&rdi65) = v31;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi65) + 4) = 0;
                                rdx16 = r15_39;
                                rsi22 = r11_55;
                                rax66 = full_write(rdi65, rsi22, rdx16);
                                r9d = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r9d)));
                                rcx18 = reinterpret_cast<void**>(static_cast<uint32_t>(v63));
                                *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                                if (r15_39 != rax66) 
                                    goto addr_68bd_39;
                            } else {
                                addr_666c_56:
                                rdx16 = reinterpret_cast<void**>(static_cast<uint32_t>(v28));
                                *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                                rsi22 = v21;
                                rcx18 = r15_39;
                                eax50 = create_hole(v31, rsi22, rdx16, rcx18, r8);
                                r9d = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r9d)));
                                if (!*reinterpret_cast<signed char*>(&eax50)) 
                                    goto addr_6695_41;
                            }
                        } else {
                            goto addr_66b9_58;
                        }
                        *reinterpret_cast<uint32_t*>(&rax57) = v64;
                        if (!*reinterpret_cast<uint32_t*>(&rax57)) {
                            r11_55 = r14_59;
                            rbp58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp58) - reinterpret_cast<unsigned char>(rbx60));
                            rcx18 = r13d32;
                            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                            r14_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_59) + reinterpret_cast<unsigned char>(rbx60));
                            r15_39 = rbx60;
                            continue;
                        } else {
                            if (!rbx60) {
                                if (*reinterpret_cast<unsigned char*>(&r9d)) 
                                    goto addr_6850_63;
                                *reinterpret_cast<int32_t*>(&r12_62) = 0;
                                *reinterpret_cast<int32_t*>(&r12_62 + 4) = 0;
                            } else {
                                if (*reinterpret_cast<unsigned char*>(&r9d)) {
                                    r15_39 = rbx60;
                                    rcx18 = r13d32;
                                    r11_55 = r14_59;
                                    *reinterpret_cast<int32_t*>(&rbx60) = 0;
                                    *reinterpret_cast<int32_t*>(&rbx60 + 4) = 0;
                                    goto addr_6540_50;
                                }
                            }
                            r11_55 = r14_59;
                            rcx18 = r13d32;
                            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                            r14_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_59) + reinterpret_cast<unsigned char>(rbx60));
                            rbp58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_62) - reinterpret_cast<unsigned char>(rbx60));
                            *reinterpret_cast<int32_t*>(&r15_39) = 0;
                            *reinterpret_cast<int32_t*>(&r15_39 + 4) = 0;
                            continue;
                        }
                    } else {
                        rsi22 = r14_59;
                        rdx16 = rbx60;
                        do {
                            if (*reinterpret_cast<void***>(rsi22)) 
                                break;
                            ++rsi22;
                            --rdx16;
                            if (!rdx16) 
                                goto addr_66a0_71;
                        } while (*reinterpret_cast<unsigned char*>(&rdx16) & 15);
                        goto addr_657e_73;
                    }
                    eax67 = r13d32;
                    rdx16 = rcx18;
                    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                    r13d32 = reinterpret_cast<void**>(0);
                    addr_65a9_75:
                    *reinterpret_cast<unsigned char*>(&r9d) = reinterpret_cast<uint1_t>(!!r15_39);
                    r9d = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9d) & reinterpret_cast<unsigned char>(rdx16));
                    if (rbx60 != rbp58 || !*reinterpret_cast<unsigned char*>(&eax67)) {
                        addr_66b0_76:
                        if (*reinterpret_cast<unsigned char*>(&r9d)) {
                            v64 = 0;
                        } else {
                            addr_66b9_58:
                            r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<unsigned char>(rbx60));
                            rax57 = r15_39 - 0x8000000000000000;
                            if (reinterpret_cast<unsigned char>(rax57) < reinterpret_cast<unsigned char>(rbx60)) 
                                goto addr_6870_42; else 
                                goto addr_66d2_78;
                        }
                    } else {
                        if (!*reinterpret_cast<unsigned char*>(&r9d)) {
                            r13d32 = reinterpret_cast<void**>(0);
                            goto addr_6652_54;
                        } else {
                            v64 = 1;
                            r13d32 = reinterpret_cast<void**>(0);
                        }
                    }
                    v63 = *reinterpret_cast<unsigned char*>(&rcx18);
                    if (*reinterpret_cast<unsigned char*>(&rcx18)) 
                        goto addr_666c_56; else 
                        goto addr_65e4_55;
                    addr_66d2_78:
                    rbp58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp58) - reinterpret_cast<unsigned char>(rbx60));
                    r14_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_59) + reinterpret_cast<unsigned char>(rbx60));
                    rcx18 = r13d32;
                    *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                    continue;
                    addr_66a0_71:
                    *reinterpret_cast<unsigned char*>(&rax57) = reinterpret_cast<uint1_t>(!!r15_39);
                    r9d = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rcx18) ^ 1) & *reinterpret_cast<uint32_t*>(&rax57));
                    goto addr_66b0_76;
                    addr_657e_73:
                    eax68 = fun_3990(r14_59, rsi22, rdx16, r14_59, rsi22, rdx16);
                    rcx18 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rcx18)));
                    r11_55 = r11_55;
                    r13d32 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax68 == 0)));
                    rdx16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13d32) ^ reinterpret_cast<unsigned char>(rcx18));
                    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&eax67) = reinterpret_cast<uint1_t>(!!eax68);
                    goto addr_65a9_75;
                } while (rbp58);
                addr_66e8_85:
                rdi30 = v56;
                v25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(rdi30));
                *reinterpret_cast<void***>(v27) = r13d32;
                if (!v25) 
                    goto addr_6938_40; else 
                    continue;
                addr_6850_63:
                *reinterpret_cast<int32_t*>(&r15_39) = 0;
                *reinterpret_cast<int32_t*>(&r15_39 + 4) = 0;
                goto addr_66e8_85;
                r14d38 = r13d32;
                r11_53 = *reinterpret_cast<void***>(v14);
            } while (r11_53);
            break;
            rax69 = fun_36d0();
            if (*reinterpret_cast<void***>(rax69) != 4) 
                goto addr_64be_43;
            addr_646f_26:
            r11_53 = *reinterpret_cast<void***>(v14);
            if (!r11_53) 
                goto addr_6718_44;
        }
    }
}

void** fun_3ab0(void** rdi, void** rsi, void** rdx, ...);

void** simple_pattern = reinterpret_cast<void**>(67);

unsigned char g16460 = 0;

void** samedir_template(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rax7;
    void** rdi8;
    void** rax9;
    void** rax10;
    void** rdx11;
    uint32_t edx12;
    void** rax13;

    rbx5 = rsi;
    rax6 = last_component(rdi, rsi, rdx, rcx);
    rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(rdi));
    rdi8 = rax7 + 9;
    if (reinterpret_cast<unsigned char>(rdi8) <= reinterpret_cast<unsigned char>(0x100) || (rax9 = fun_3ab0(rdi8, rsi, rdx), rbx5 = rax9, !!rax9)) {
        rax10 = fun_3b70(rbx5, rdi, rax7);
        rdx11 = simple_pattern;
        *reinterpret_cast<void***>(rax10) = rdx11;
        edx12 = g16460;
        *reinterpret_cast<void***>(rax10 + 8) = *reinterpret_cast<void***>(&edx12);
        rax13 = rbx5;
    } else {
        *reinterpret_cast<int32_t*>(&rax13) = 0;
        *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
    }
    return rax13;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0x165e0);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0x165e0);
    if (*reinterpret_cast<void***>(rdi + 40) != 0x165e0) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x92d8]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0x165e0);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x913f]");
        if (1) 
            goto addr_d5ac_6;
        __asm__("comiss xmm1, [rip+0x9136]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x9128]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_d58c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_d582_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_d58c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_d582_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_d582_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_d58c_13:
        return 0;
    } else {
        addr_d5ac_6:
        return r8_3;
    }
}

struct s17 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s16 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    signed char[8] pad40;
    struct s17* f28;
    int64_t f30;
    signed char[16] pad72;
    void** f48;
};

struct s18 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s19 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_36c0(void** rdi, ...);

int32_t transfer_entries(struct s16* rdi, struct s16* rsi, int32_t edx) {
    void** rdx3;
    struct s16* r14_4;
    int32_t r12d5;
    struct s16* rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s18* rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s19* r13_18;
    void** rax19;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = rsi->f0;
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rsi->f8)) {
        do {
            addr_d616_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_d608_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(rbp6->f8) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_d616_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = r14_4->f10;
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(r14_4->f30(rdi12, rsi10)), rsi10 = r14_4->f10, reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s18*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_d68e_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax15 = r14_4->f48;
                            r14_4->f18 = r14_4->f18 + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            r14_4->f48 = r13_9;
                            if (!rdx3) 
                                goto addr_d68e_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_3cda_12;
                    addr_d68e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_d608_3;
            }
            rsi16 = r14_4->f10;
            rdi12 = r15_8;
            rax17 = reinterpret_cast<void**>(r14_4->f30(rdi12, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(r14_4->f10)) 
                goto addr_3cda_12;
            r13_18 = reinterpret_cast<struct s19*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
            if (r13_18->f0) 
                goto addr_d6c8_16;
            r13_18->f0 = r15_8;
            r14_4->f18 = r14_4->f18 + 1;
            continue;
            addr_d6c8_16:
            rax19 = r14_4->f48;
            if (!rax19) {
                rax19 = fun_3ab0(16, rsi16, rdx3);
                if (!rax19) 
                    goto addr_d73a_19;
            } else {
                r14_4->f48 = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx3 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx3;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            rbp6->f18 = rbp6->f18 - 1;
        } while (reinterpret_cast<unsigned char>(rbp6->f8) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_3cda_12:
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    fun_36c0(rdi12, rdi12);
    addr_d73a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
        fun_36c0(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_d43f_22:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_d3e4_25;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<unsigned char*>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_d450_29;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<unsigned char*>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_d450_29;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_d43f_22;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_d3e4_25;
            }
        }
    }
    addr_d441_33:
    return rax11;
    addr_d3e4_25:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_d441_33;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_d450_29:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_3810();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_3810();
    if (r8d > 10) {
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x166c0 + rax11 * 4) + 0x166c0;
    }
}

struct s20 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_38d0();

struct s21 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s20* rcx, ...) {
    int64_t rbx5;
    void* rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s21* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0xfd1f;
    rax8 = fun_36d0();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
        fun_36c0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x1c090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xc1d1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0xfdab;
            fun_38d0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s21*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x1c5c0) {
                fun_3670(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0xfe3a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax6) - reinterpret_cast<int64_t>(g28));
        if (rax29) {
            fun_3840();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x1c0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

uint32_t re_protect(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void* rbp8;
    void** r15_9;
    void** r14d10;
    void** r13_11;
    void** rbx12;
    void** v13;
    void* rax14;
    void* v15;
    void** rax16;
    void* rsp17;
    void** r8_18;
    void*** rax19;
    void* rcx20;
    uint64_t rdx21;
    void* rdx22;
    void* rsp23;
    int64_t* rsp24;
    void** rax25;
    void* rsp26;
    void** r12_27;
    void** r13_28;
    uint32_t r8d29;
    void*** v30;
    int1_t zf31;
    int64_t rdi32;
    int64_t* rsp33;
    int64_t rax34;
    void** rcx35;
    int64_t rdx36;
    int64_t rdi37;
    int64_t* rsp38;
    int32_t eax39;
    uint32_t r8d40;
    void** r8_41;
    int64_t* rsp42;
    int32_t eax43;
    int64_t rdi44;
    unsigned char v45;
    int64_t* rsp46;
    int32_t eax47;
    int64_t* rsp48;
    void** eax49;
    void* rsp50;
    void** rcx51;
    int64_t rdi52;
    int64_t* rsp53;
    void* rax54;
    int64_t* rsp55;
    int64_t* rsp56;
    int64_t* rsp57;
    int64_t* rsp58;
    int64_t* rsp59;
    void* rsp60;
    int64_t* rsp61;
    int64_t* rsp62;
    int64_t* rsp63;
    int64_t* rsp64;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp8 = rsp7;
    r15_9 = r8;
    r14d10 = esi;
    r13_11 = rdi;
    rbx12 = rcx;
    v13 = rdx;
    rax14 = g28;
    v15 = rax14;
    rax16 = fun_3820(rdi);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 - 8 - 8 - 72 - 8 + 8);
    r8_18 = rax16 + 1;
    rax19 = reinterpret_cast<void***>(rax16 + 24);
    rcx20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - (reinterpret_cast<uint64_t>(rax19) & 0xfffffffffffff000));
    rdx21 = reinterpret_cast<uint64_t>(rax19) & 0xfffffffffffffff0;
    if (rsp17 != rcx20) {
        do {
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
        } while (rsp17 != rcx20);
    }
    *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx21) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - reinterpret_cast<int64_t>(rdx22));
    if (rdx22) {
        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp23) + reinterpret_cast<int64_t>(rdx22) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp23) + reinterpret_cast<int64_t>(rdx22) - 8);
    }
    rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp23) - 8);
    *rsp24 = 0x4eb0;
    rax25 = fun_3a80(reinterpret_cast<uint64_t>(rsp23) + 15 & 0xfffffffffffffff0, r13_11, r8_18);
    rsp26 = reinterpret_cast<void*>(rsp24 + 1);
    r12_27 = rax25;
    r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_27) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(r13_11)));
    if (!rbx12) {
        addr_50a8_6:
        r8d29 = 1;
    } else {
        v30 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbp8) - 96);
        while ((zf31 = *reinterpret_cast<signed char*>(r15_9 + 31) == 0, *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_27) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx12 + 0x98))) = 0, zf31) || (*reinterpret_cast<void***>(&rdi32) = r14d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0, rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8), *rsp33 = 0x4f5e, rax34 = fun_36b0(rdi32, r13_28, v30, rdi32, r13_28, v30), rsp26 = reinterpret_cast<void*>(rsp33 + 1), *reinterpret_cast<int32_t*>(&rax34) == 0)) {
            if (!*reinterpret_cast<unsigned char*>(r15_9 + 29) || (rcx35 = *reinterpret_cast<void***>(rbx12 + 32), *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx36) = *reinterpret_cast<unsigned char*>(rbx12 + 28), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx36) + 4) = 0, *reinterpret_cast<void***>(&rdi37) = r14d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0, rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8), *rsp38 = 0x4fc7, eax39 = fun_3bf0(rdi37, r13_28, rdx36, rcx35, 0x100), rsp26 = reinterpret_cast<void*>(rsp38 + 1), eax39 == 0)) {
                r8d40 = *reinterpret_cast<unsigned char*>(r15_9 + 30);
                if (*reinterpret_cast<unsigned char*>(&r8d40)) {
                    addr_5010_11:
                    r8_41 = *reinterpret_cast<void***>(rbx12 + 24);
                    *reinterpret_cast<int32_t*>(&r8_41 + 4) = 0;
                    rsp42 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
                    *rsp42 = 0x5029;
                    eax43 = copy_acl(r13_28, 0xffffffff, r12_27, 0xffffffff, r8_41, r9);
                    rsp26 = reinterpret_cast<void*>(rsp42 + 1);
                    if (eax43) 
                        goto addr_5031_12;
                } else {
                    addr_4ef1_13:
                    if (!*reinterpret_cast<void***>(rbx12 + 0x90)) 
                        goto addr_4efe_14;
                    *reinterpret_cast<void***>(&rdi44) = r14d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi44) + 4) = 0;
                    v45 = *reinterpret_cast<unsigned char*>(&r8d40);
                    rsp46 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
                    *rsp46 = 0x5057;
                    eax47 = fun_3ad0(rdi44, r13_28, rdi44, r13_28);
                    rsp26 = reinterpret_cast<void*>(rsp46 + 1);
                    if (eax47) 
                        goto addr_505f_16;
                }
                addr_4efe_14:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_27) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx12 + 0x98))) = 47;
                rbx12 = *reinterpret_cast<void***>(rbx12 + 0xa0);
                if (!rbx12) 
                    goto addr_50a8_6;
            } else {
                rsp48 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
                *rsp48 = 0x4fd7;
                eax49 = chown_failure_ok(r15_9, r13_28, rdx36, rcx35, 0x100);
                rsp50 = reinterpret_cast<void*>(rsp48 + 1);
                if (!*reinterpret_cast<unsigned char*>(&eax49)) 
                    goto addr_50da_18;
                rcx51 = *reinterpret_cast<void***>(rbx12 + 32);
                *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
                *reinterpret_cast<void***>(&rdi52) = r14d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi52) + 4) = 0;
                rsp53 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp50) - 8);
                *rsp53 = 0x4ff8;
                fun_3bf0(rdi52, r13_28, 0xffffffff, rcx51, 0x100);
                rsp26 = reinterpret_cast<void*>(rsp53 + 1);
                r8d40 = *reinterpret_cast<unsigned char*>(r15_9 + 30);
                if (!*reinterpret_cast<unsigned char*>(&r8d40)) 
                    goto addr_4ef1_13;
                goto addr_5010_11;
            }
        }
        goto addr_4f66_21;
    }
    addr_50ae_22:
    rax54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v15) - reinterpret_cast<int64_t>(g28));
    if (rax54) {
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8) = reinterpret_cast<int64_t>(usage);
        fun_3840();
    } else {
        return r8d29;
    }
    addr_4f66_21:
    rsp55 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
    *rsp55 = 0x4f73;
    quotearg_style(4, r12_27, 4, r12_27);
    rsp56 = rsp55 + 1 - 1;
    *rsp56 = 0x4f89;
    fun_3800();
    rsp57 = rsp56 + 1 - 1;
    *rsp57 = 0x4f91;
    fun_36d0();
    rsp58 = rsp57 + 1 - 1;
    *rsp58 = 0x4fa2;
    fun_3b80();
    rsp26 = reinterpret_cast<void*>(rsp58 + 1);
    r8d29 = 0;
    goto addr_50ae_22;
    addr_5031_12:
    r8d29 = 0;
    goto addr_50ae_22;
    addr_505f_16:
    rsp59 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp26) - 8);
    *rsp59 = 0x506c;
    quotearg_style(4, r12_27, 4, r12_27);
    rsp60 = reinterpret_cast<void*>(rsp59 + 1);
    addr_507b_26:
    rsp61 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp60) - 8);
    *rsp61 = 0x5082;
    fun_3800();
    rsp62 = rsp61 + 1 - 1;
    *rsp62 = 0x508a;
    fun_36d0();
    rsp63 = rsp62 + 1 - 1;
    *rsp63 = 0x509b;
    fun_3b80();
    rsp26 = reinterpret_cast<void*>(rsp63 + 1);
    r8d29 = v45;
    goto addr_50ae_22;
    addr_50da_18:
    v45 = *reinterpret_cast<unsigned char*>(&eax49);
    rsp64 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp50) - 8);
    *rsp64 = 0x50ea;
    quotearg_style(4, r12_27, 4, r12_27);
    rsp60 = reinterpret_cast<void*>(rsp64 + 1);
    goto addr_507b_26;
}

/* zeros.3 */
void** zeros_3 = reinterpret_cast<void**>(0);

/* nz.2 */
void** nz_2 = reinterpret_cast<void**>(0);

void** fun_39c0(void** rdi, int64_t rsi);

signed char write_zeros(void** edi, void** rsi) {
    int1_t zf3;
    void** r12d4;
    void** rbp5;
    void** rdi6;
    void** rax7;
    void** rbx8;
    void** rsi9;
    int64_t rdi10;
    void** rax11;

    zf3 = zeros_3 == 0;
    r12d4 = edi;
    rbp5 = rsi;
    if (zf3 && (rdi6 = nz_2, rax7 = fun_39c0(rdi6, 1), zeros_3 = rax7, !rax7)) {
        nz_2 = reinterpret_cast<void**>(0x400);
        zeros_3 = reinterpret_cast<void**>(0x1c160);
    }
    if (rbp5) {
        do {
            rbx8 = nz_2;
            rsi9 = zeros_3;
            *reinterpret_cast<void***>(&rdi10) = r12d4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            if (reinterpret_cast<unsigned char>(rbp5) <= reinterpret_cast<unsigned char>(rbx8)) {
                rbx8 = rbp5;
            }
            rax11 = full_write(rdi10, rsi9, rbx8);
            if (rax11 != rbx8) 
                break;
            rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) - reinterpret_cast<unsigned char>(rax11));
        } while (rbp5);
        goto addr_5e18_8;
    } else {
        goto addr_5e18_8;
    }
    return 0;
    addr_5e18_8:
    return 1;
}

struct s22 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s22* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s22* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x16663);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x1665c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x16667);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x16658);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

void** xstrdup(void** rdi, void** rsi);

int64_t argmatch_die = 0xbaf0;

int64_t __xargmatch_internal(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void decode_preserve_arg(void** rdi, void** rsi, int32_t edx, int64_t rcx, int64_t r8, void** r9, int64_t a7, int64_t a8) {
    int32_t ebx9;
    void** rax10;
    void** rax11;
    void** rax12;
    int64_t r9_13;
    int64_t rax14;
    int64_t rax15;

    ebx9 = edx;
    rax10 = xstrdup(rdi, rsi);
    rax11 = reinterpret_cast<void**>("--no-preserve");
    if (*reinterpret_cast<signed char*>(&ebx9)) {
        rax11 = reinterpret_cast<void**>("--preserve");
    }
    rax12 = fun_3870(rax10, rax10);
    if (rax12) {
        *reinterpret_cast<void***>(rax12) = reinterpret_cast<void**>(0);
    }
    r9_13 = argmatch_die;
    rax14 = __xargmatch_internal(rax11, rax10, 0x1b560, 0x14440, 4, r9_13);
    if (*reinterpret_cast<uint32_t*>(0x14440 + rax14 * 4) > 6) {
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
    } else {
        *reinterpret_cast<uint32_t*>(&rax15) = *reinterpret_cast<uint32_t*>(0x14440 + rax14 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x14320 + rax15 * 4) + 0x14320;
    }
}

int64_t __gmon_start__ = 0;

void fun_3003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g1bc98 = 0;

void fun_3033() {
    __asm__("cli ");
    goto g1bc98;
}

void fun_3043() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3053() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3063() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3073() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3083() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3093() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3103() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3113() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3123() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3133() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3143() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3153() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3163() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3173() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3183() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3193() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3203() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3213() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3223() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3233() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3243() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3253() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3263() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3273() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3283() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3293() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3303() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3313() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3323() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3333() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3343() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3353() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3363() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3373() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3383() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3393() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3403() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3413() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3423() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3433() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3443() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3453() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3463() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3473() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3483() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3493() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3503() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3513() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3523() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3533() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3543() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3553() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3563() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3573() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3583() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3593() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3603() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3613() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3623() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3633() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3643() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3653() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3663() {
    __asm__("cli ");
    goto 0x3020;
}

int64_t free = 0;

void fun_3673() {
    __asm__("cli ");
    goto free;
}

int64_t __cxa_finalize = 0;

void fun_3683() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x3030;

void fun_3693() {
    __asm__("cli ");
    goto getenv;
}

int64_t mkfifoat = 0x3040;

void fun_36a3() {
    __asm__("cli ");
    goto mkfifoat;
}

int64_t utimensat = 0x3050;

void fun_36b3() {
    __asm__("cli ");
    goto utimensat;
}

int64_t abort = 0x3060;

void fun_36c3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x3070;

void fun_36d3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x3080;

void fun_36e3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x3090;

void fun_36f3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x30a0;

void fun_3703() {
    __asm__("cli ");
    goto __fpending;
}

int64_t mkdir = 0x30b0;

void fun_3713() {
    __asm__("cli ");
    goto mkdir;
}

int64_t unlinkat = 0x30c0;

void fun_3723() {
    __asm__("cli ");
    goto unlinkat;
}

int64_t qsort = 0x30d0;

void fun_3733() {
    __asm__("cli ");
    goto qsort;
}

int64_t reallocarray = 0x30e0;

void fun_3743() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t faccessat = 0x30f0;

void fun_3753() {
    __asm__("cli ");
    goto faccessat;
}

int64_t readlink = 0x3100;

void fun_3763() {
    __asm__("cli ");
    goto readlink;
}

int64_t fcntl = 0x3110;

void fun_3773() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clock_gettime = 0x3120;

void fun_3783() {
    __asm__("cli ");
    goto clock_gettime;
}

int64_t write = 0x3130;

void fun_3793() {
    __asm__("cli ");
    goto write;
}

int64_t textdomain = 0x3140;

void fun_37a3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t pathconf = 0x3150;

void fun_37b3() {
    __asm__("cli ");
    goto pathconf;
}

int64_t fclose = 0x3160;

void fun_37c3() {
    __asm__("cli ");
    goto fclose;
}

int64_t opendir = 0x3170;

void fun_37d3() {
    __asm__("cli ");
    goto opendir;
}

int64_t bindtextdomain = 0x3180;

void fun_37e3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x3190;

void fun_37f3() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x31a0;

void fun_3803() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x31b0;

void fun_3813() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x31c0;

void fun_3823() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x31d0;

void fun_3833() {
    __asm__("cli ");
    goto openat;
}

int64_t __stack_chk_fail = 0x31e0;

void fun_3843() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x31f0;

void fun_3853() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x3200;

void fun_3863() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x3210;

void fun_3873() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x3220;

void fun_3883() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x3230;

void fun_3893() {
    __asm__("cli ");
    goto strrchr;
}

int64_t ftruncate = 0x3240;

void fun_38a3() {
    __asm__("cli ");
    goto ftruncate;
}

int64_t lseek = 0x3250;

void fun_38b3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x3260;

void fun_38c3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x3270;

void fun_38d3() {
    __asm__("cli ");
    goto memset;
}

int64_t geteuid = 0x3280;

void fun_38e3() {
    __asm__("cli ");
    goto geteuid;
}

int64_t ioctl = 0x3290;

void fun_38f3() {
    __asm__("cli ");
    goto ioctl;
}

int64_t copy_file_range = 0x32a0;

void fun_3903() {
    __asm__("cli ");
    goto copy_file_range;
}

int64_t canonicalize_file_name = 0x32b0;

void fun_3913() {
    __asm__("cli ");
    goto canonicalize_file_name;
}

int64_t close = 0x32c0;

void fun_3923() {
    __asm__("cli ");
    goto close;
}

int64_t rewinddir = 0x32d0;

void fun_3933() {
    __asm__("cli ");
    goto rewinddir;
}

int64_t strspn = 0x32e0;

void fun_3943() {
    __asm__("cli ");
    goto strspn;
}

int64_t closedir = 0x32f0;

void fun_3953() {
    __asm__("cli ");
    goto closedir;
}

int64_t posix_fadvise = 0x3300;

void fun_3963() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t read = 0x3310;

void fun_3973() {
    __asm__("cli ");
    goto read;
}

int64_t lstat = 0x3320;

void fun_3983() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x3330;

void fun_3993() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fallocate = 0x3340;

void fun_39a3() {
    __asm__("cli ");
    goto fallocate;
}

int64_t fputs_unlocked = 0x3350;

void fun_39b3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x3360;

void fun_39c3() {
    __asm__("cli ");
    goto calloc;
}

int64_t __getdelim = 0x3370;

void fun_39d3() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x3380;

void fun_39e3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t readlinkat = 0x3390;

void fun_39f3() {
    __asm__("cli ");
    goto readlinkat;
}

int64_t dirfd = 0x33a0;

void fun_3a03() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x33b0;

void fun_3a13() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t fpathconf = 0x33c0;

void fun_3a23() {
    __asm__("cli ");
    goto fpathconf;
}

int64_t mknodat = 0x33d0;

void fun_3a33() {
    __asm__("cli ");
    goto mknodat;
}

int64_t rpmatch = 0x33e0;

void fun_3a43() {
    __asm__("cli ");
    goto rpmatch;
}

int64_t mkdirat = 0x33f0;

void fun_3a53() {
    __asm__("cli ");
    goto mkdirat;
}

int64_t umask = 0x3400;

void fun_3a63() {
    __asm__("cli ");
    goto umask;
}

int64_t stat = 0x3410;

void fun_3a73() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x3420;

void fun_3a83() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x3430;

void fun_3a93() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x3440;

void fun_3aa3() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x3450;

void fun_3ab3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x3460;

void fun_3ac3() {
    __asm__("cli ");
    goto fflush;
}

int64_t fchmodat = 0x3470;

void fun_3ad3() {
    __asm__("cli ");
    goto fchmodat;
}

int64_t nl_langinfo = 0x3480;

void fun_3ae3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t renameat2 = 0x3490;

void fun_3af3() {
    __asm__("cli ");
    goto renameat2;
}

int64_t __freading = 0x34a0;

void fun_3b03() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x34b0;

void fun_3b13() {
    __asm__("cli ");
    goto realloc;
}

int64_t linkat = 0x34c0;

void fun_3b23() {
    __asm__("cli ");
    goto linkat;
}

int64_t setlocale = 0x34d0;

void fun_3b33() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x34e0;

void fun_3b43() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t fchmod = 0x34f0;

void fun_3b53() {
    __asm__("cli ");
    goto fchmod;
}

int64_t chmod = 0x3500;

void fun_3b63() {
    __asm__("cli ");
    goto chmod;
}

int64_t mempcpy = 0x3510;

void fun_3b73() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t error = 0x3520;

void fun_3b83() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x3530;

void fun_3b93() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x3540;

void fun_3ba3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fchown = 0x3550;

void fun_3bb3() {
    __asm__("cli ");
    goto fchown;
}

int64_t fdopendir = 0x3560;

void fun_3bc3() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t futimens = 0x3570;

void fun_3bd3() {
    __asm__("cli ");
    goto futimens;
}

int64_t __cxa_atexit = 0x3580;

void fun_3be3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t fchownat = 0x3590;

void fun_3bf3() {
    __asm__("cli ");
    goto fchownat;
}

int64_t renameat = 0x35a0;

void fun_3c03() {
    __asm__("cli ");
    goto renameat;
}

int64_t getpagesize = 0x35b0;

void fun_3c13() {
    __asm__("cli ");
    goto getpagesize;
}

int64_t exit = 0x35c0;

void fun_3c23() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x35d0;

void fun_3c33() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x35e0;

void fun_3c43() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getrandom = 0x35f0;

void fun_3c53() {
    __asm__("cli ");
    goto getrandom;
}

int64_t aligned_alloc = 0x3600;

void fun_3c63() {
    __asm__("cli ");
    goto aligned_alloc;
}

int64_t mbsinit = 0x3610;

void fun_3c73() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t symlinkat = 0x3620;

void fun_3c83() {
    __asm__("cli ");
    goto symlinkat;
}

int64_t iswprint = 0x3630;

void fun_3c93() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x3640;

void fun_3ca3() {
    __asm__("cli ");
    goto fstat;
}

int64_t fstatat = 0x3650;

void fun_3cb3() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x3660;

void fun_3cc3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_3b30(int64_t rdi, ...);

void fun_37e0(int64_t rdi, void** rsi);

void fun_37a0(int64_t rdi, void** rsi);

void atexit(int64_t rdi, void** rsi);

signed char selinux_enabled = 0;

void cp_options_default(void** rdi, void** rsi);

void** fun_3690(int64_t rdi, void** rsi);

int32_t fun_3850(int64_t rdi, void** rsi, void** rdx, void** rcx);

int64_t Version = 0x16461;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9, int64_t a7, int64_t a8);

signed char fun_3c20();

void** quote(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void hash_init(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t optind = 0;

int32_t xget_version(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void set_simple_backup_suffix(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_3d43(int32_t edi, void** rsi) {
    int32_t r12d3;
    void** rbp4;
    void** rdi5;
    void** r15_6;
    void** r9_7;
    int32_t v8;
    int32_t v9;
    void** r8_10;
    void** rcx11;
    void** rdx12;
    void** rsi13;
    int64_t rdi14;
    int32_t eax15;
    void** rdi16;
    int64_t rcx17;
    int64_t rax18;
    void** rax19;
    void** rax20;
    uint32_t eax21;
    int1_t zf22;
    int64_t rdi23;
    int64_t rdx24;
    uint32_t eax25;
    uint32_t eax26;
    int1_t zf27;
    void** rax28;
    void** rax29;

    __asm__("cli ");
    r12d3 = edi;
    rbp4 = rsi;
    rdi5 = *reinterpret_cast<void***>(rsi);
    r15_6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 + 48);
    set_program_name(rdi5);
    fun_3b30(6, 6);
    fun_37e0("coreutils", "/usr/local/share/locale");
    fun_37a0("coreutils", "/usr/local/share/locale");
    atexit(0xc920, "/usr/local/share/locale");
    selinux_enabled = 0;
    cp_options_default(r15_6, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&r9_7) = 0;
    *reinterpret_cast<int32_t*>(&r9_7 + 4) = 0;
    v8 = 1;
    v9 = 0;
    fun_3690("POSIXLY_CORRECT", "/usr/local/share/locale");
    while (1) {
        *reinterpret_cast<int32_t*>(&r8_10) = 0;
        *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
        rcx11 = reinterpret_cast<void**>(0x1b5a0);
        rdx12 = reinterpret_cast<void**>("abdfHilLnprst:uvxPRS:TZ");
        rsi13 = rbp4;
        *reinterpret_cast<int32_t*>(&rdi14) = r12d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
        eax15 = fun_3850(rdi14, rsi13, "abdfHilLnprst:uvxPRS:TZ", 0x1b5a0);
        if (eax15 != -1) {
            if (eax15 > 0x88) {
                addr_3f5a_4:
                usage();
                *reinterpret_cast<signed char*>(&v9) = 1;
                continue;
            } else {
                if (eax15 <= 71) {
                    if (eax15 == 0xffffff7d) {
                        rdi16 = stdout;
                        r9_7 = reinterpret_cast<void**>("David MacKenzie");
                        rcx17 = Version;
                        version_etc(rdi16, "cp", "GNU coreutils", rcx17, "Torbjorn Granlund", "David MacKenzie", "Jim Meyering", 0);
                        fun_3c20();
                        decode_preserve_arg(0, r15_6, 1, rcx17, "Torbjorn Granlund", "David MacKenzie", "Jim Meyering", 0);
                        continue;
                    }
                    if (eax15 != 0xffffff7e) 
                        goto addr_3f5a_4;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax18) = eax15 - 72;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax18) > 64) 
                        goto addr_3f5a_4; else 
                        break;
                }
            }
            usage();
        }
        if (1 || !*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&v9) + 2)) {
            if (0) {
                if (!0) {
                    addr_426b_16:
                    if (!0) 
                        goto addr_4278_17;
                    if (1) 
                        goto addr_4278_17; else 
                        goto addr_43fc_19;
                }
            } else {
                if (1) 
                    goto addr_426b_16;
                if (1) 
                    goto addr_438b_22;
                if (1) 
                    goto addr_438b_22; else 
                    goto addr_3f39_24;
            }
        }
        addr_3f45_26:
        fun_3800();
        fun_3b80();
        goto addr_3f5a_4;
        addr_43fc_19:
        addr_3f39_24:
        goto addr_3f45_26;
    }
    goto *reinterpret_cast<int32_t*>(0x1433c + rax18 * 4) + 0x1433c;
    addr_4469_28:
    rax19 = fun_3800();
    *reinterpret_cast<int32_t*>(&rsi13) = 0;
    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
    rdx12 = rax19;
    fun_3b80();
    addr_448d_29:
    rax20 = fun_36d0();
    *reinterpret_cast<void***>(rax20) = reinterpret_cast<void**>(95);
    quote(0, rsi13, rdx12, 0, r8_10, r9_7);
    fun_3800();
    fun_3b80();
    while (1) {
        addr_4303_30:
        if (!*reinterpret_cast<signed char*>(&eax21)) 
            goto addr_42c6_31;
        zf22 = selinux_enabled == 0;
        if (zf22) 
            goto addr_4310_33;
        addr_42c6_31:
        if (0) 
            goto addr_4469_28;
        hash_init(rdi23, rsi13, rdx12, 0, r8_10, r9_7);
        r8_10 = r15_6;
        rdx24 = optind;
        rsi13 = rbp4 + rdx24 * 8;
        rdx12 = reinterpret_cast<void**>(0);
        eax25 = do_copy(r12d3 - *reinterpret_cast<int32_t*>(&rdx24), rsi13, 0, 0, r8_10, r9_7);
        eax26 = eax25 ^ 1;
        *reinterpret_cast<uint32_t*>(&rdi23) = *reinterpret_cast<unsigned char*>(&eax26);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
        *reinterpret_cast<signed char*>(&eax21) = fun_3c20();
        continue;
        addr_42ba_35:
        if (0) 
            goto addr_448d_29; else 
            goto addr_42c6_31;
        addr_42b5_36:
        goto addr_42ba_35;
        while (1) {
            addr_43d2_37:
            v8 = 4;
            while (1) {
                if (!*reinterpret_cast<signed char*>(&v9)) 
                    goto addr_429c_39;
                while (1) {
                    addr_429c_39:
                    rcx11 = reinterpret_cast<void**>(0);
                    eax21 = 0;
                    if (1) 
                        goto addr_4303_30;
                    if (!0) 
                        goto addr_42b5_36;
                    if (1) {
                        zf27 = selinux_enabled == 0;
                        if (!zf27) 
                            goto addr_42ba_35;
                        addr_4310_33:
                        rax28 = fun_3800();
                        *reinterpret_cast<int32_t*>(&rsi13) = 0;
                        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rdi23) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
                        rdx12 = rax28;
                        fun_3b80();
                    } else {
                        fun_3800();
                        fun_3b80();
                        addr_438b_22:
                        *reinterpret_cast<int32_t*>(&rdx12) = 5;
                        *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
                        rax29 = fun_3800();
                        rsi13 = reinterpret_cast<void**>(0);
                        xget_version(rax29, 0, 5, rcx11, r8_10, r9_7);
                        addr_4278_17:
                        rdi23 = 0;
                        set_simple_backup_suffix(0, rsi13, rdx12, rcx11, r8_10, r9_7);
                        if (v8 != 1) 
                            break;
                    }
                    if (!*reinterpret_cast<signed char*>(&v9)) 
                        goto addr_43d2_37;
                    if (0) 
                        goto addr_43d2_37;
                    v8 = 2;
                }
            }
        }
    }
}

int64_t __libc_start_main = 0;

void fun_44d3() {
    __asm__("cli ");
    __libc_start_main(0x3d40, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x1c008;

void fun_3680(int64_t rdi);

int64_t fun_4573() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_3680(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_45b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_39b0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

unsigned char g1;

signed char g2;

int32_t fun_36e0(void** rdi, void** rsi, void** rdx, ...);

void fun_5103(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** r8_4;
    void** r12_5;
    void** rax6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    void** r12_21;
    void** rax22;
    void** r12_23;
    void** rax24;
    void** r12_25;
    void** rax26;
    void** r12_27;
    void** rax28;
    void** r12_29;
    void** rax30;
    void** r12_31;
    void** rax32;
    void** r12_33;
    void** rax34;
    void** r12_35;
    void** rax36;
    void** r12_37;
    void** rax38;
    void** r12_39;
    void** rax40;
    void** r12_41;
    void** rax42;
    void** r12_43;
    void** rax44;
    uint32_t ecx45;
    uint32_t ecx46;
    int1_t zf47;
    void** rax48;
    void** rax49;
    int32_t eax50;
    void** rax51;
    void** r13_52;
    void** rax53;
    void** rax54;
    int32_t eax55;
    void** rax56;
    void** r14_57;
    void** rax58;
    void** rax59;
    void** rax60;
    void** rdi61;
    void** r8_62;
    void** r9_63;
    void* v64;
    void** v65;
    int64_t v66;
    void* v67;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_3800();
            r8_4 = r12_2;
            fun_3b40(1, rax3, r12_2, r12_2, 1, rax3, r12_2, r12_2);
            r12_5 = stdout;
            rax6 = fun_3800();
            fun_39b0(rax6, r12_5, 5, r12_2, r8_4);
            r12_7 = stdout;
            rax8 = fun_3800();
            fun_39b0(rax8, r12_7, 5, r12_2, r8_4);
            r12_9 = stdout;
            rax10 = fun_3800();
            fun_39b0(rax10, r12_9, 5, r12_2, r8_4);
            r12_11 = stdout;
            rax12 = fun_3800();
            fun_39b0(rax12, r12_11, 5, r12_2, r8_4);
            r12_13 = stdout;
            rax14 = fun_3800();
            fun_39b0(rax14, r12_13, 5, r12_2, r8_4);
            r12_15 = stdout;
            rax16 = fun_3800();
            fun_39b0(rax16, r12_15, 5, r12_2, r8_4);
            r12_17 = stdout;
            rax18 = fun_3800();
            fun_39b0(rax18, r12_17, 5, r12_2, r8_4);
            r12_19 = stdout;
            rax20 = fun_3800();
            fun_39b0(rax20, r12_19, 5, r12_2, r8_4);
            r12_21 = stdout;
            rax22 = fun_3800();
            fun_39b0(rax22, r12_21, 5, r12_2, r8_4);
            r12_23 = stdout;
            rax24 = fun_3800();
            fun_39b0(rax24, r12_23, 5, r12_2, r8_4);
            r12_25 = stdout;
            rax26 = fun_3800();
            fun_39b0(rax26, r12_25, 5, r12_2, r8_4);
            r12_27 = stdout;
            rax28 = fun_3800();
            fun_39b0(rax28, r12_27, 5, r12_2, r8_4);
            r12_29 = stdout;
            rax30 = fun_3800();
            fun_39b0(rax30, r12_29, 5, r12_2, r8_4);
            r12_31 = stdout;
            rax32 = fun_3800();
            fun_39b0(rax32, r12_31, 5, r12_2, r8_4);
            r12_33 = stdout;
            rax34 = fun_3800();
            fun_39b0(rax34, r12_33, 5, r12_2, r8_4);
            r12_35 = stdout;
            rax36 = fun_3800();
            fun_39b0(rax36, r12_35, 5, r12_2, r8_4);
            r12_37 = stdout;
            rax38 = fun_3800();
            fun_39b0(rax38, r12_37, 5, r12_2, r8_4);
            r12_39 = stdout;
            rax40 = fun_3800();
            fun_39b0(rax40, r12_39, 5, r12_2, r8_4);
            r12_41 = stdout;
            rax42 = fun_3800();
            fun_39b0(rax42, r12_41, 5, r12_2, r8_4);
            r12_43 = stdout;
            rax44 = fun_3800();
            fun_39b0(rax44, r12_43, 5, r12_2, r8_4);
            do {
                if (1) 
                    break;
                ecx45 = reinterpret_cast<unsigned char>(free);
            } while (99 != ecx45 || ((ecx46 = g1, 0x70 != ecx46) || (zf47 = g2 == 0, !zf47)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax48 = fun_3800();
                fun_3b40(1, rax48, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax48, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax49 = fun_3b30(5);
                if (!rax49 || (eax50 = fun_36e0(rax49, "en_", 3, rax49, "en_", 3), !eax50)) {
                    rax51 = fun_3800();
                    r12_2 = reinterpret_cast<void**>("cp");
                    r13_52 = reinterpret_cast<void**>(" invocation");
                    fun_3b40(1, rax51, "https://www.gnu.org/software/coreutils/", "cp", 1, rax51, "https://www.gnu.org/software/coreutils/", "cp");
                } else {
                    r12_2 = reinterpret_cast<void**>("cp");
                    goto addr_569b_9;
                }
            } else {
                rax53 = fun_3800();
                fun_3b40(1, rax53, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax53, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax54 = fun_3b30(5);
                if (!rax54 || (eax55 = fun_36e0(rax54, "en_", 3, rax54, "en_", 3), !eax55)) {
                    addr_559e_11:
                    rax56 = fun_3800();
                    r13_52 = reinterpret_cast<void**>(" invocation");
                    fun_3b40(1, rax56, "https://www.gnu.org/software/coreutils/", "cp", 1, rax56, "https://www.gnu.org/software/coreutils/", "cp");
                    if (!reinterpret_cast<int1_t>(r12_2 == "cp")) {
                        r13_52 = reinterpret_cast<void**>(0x16437);
                    }
                } else {
                    addr_569b_9:
                    r14_57 = stdout;
                    rax58 = fun_3800();
                    fun_39b0(rax58, r14_57, 5, "https://www.gnu.org/software/coreutils/", r8_4);
                    goto addr_559e_11;
                }
            }
            rax59 = fun_3800();
            fun_3b40(1, rax59, r12_2, r13_52, 1, rax59, r12_2, r13_52);
            addr_5159_14:
            fun_3c20();
        }
    } else {
        rax60 = fun_3800();
        rdi61 = stderr;
        fun_3c40(rdi61, 1, rax60, r12_2, r8_62, r9_63, v64, v65, v66, v67);
        goto addr_5159_14;
    }
}

struct s23 {
    signed char[51] pad51;
    signed char f33;
};

struct s24 {
    signed char[40] pad40;
    int64_t f28;
};

struct s25 {
    signed char[49] pad49;
    signed char f31;
    signed char[2] pad52;
    unsigned char f34;
};

int64_t fun_6bc3(void** rdi) {
    struct s23* r8_2;
    int32_t r12d3;
    struct s24* r8_4;
    unsigned char cl5;
    void** rax6;
    int64_t rax7;
    void** rax8;
    struct s25* r8_9;
    uint32_t r12d10;
    int64_t rax11;

    __asm__("cli ");
    if (!r8_2->f33) {
        *reinterpret_cast<unsigned char*>(&r12d3) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!r8_4->f28)) & cl5);
        if (*reinterpret_cast<unsigned char*>(&r12d3)) {
            rax6 = fun_36d0();
            *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(95);
            *reinterpret_cast<int32_t*>(&rax7) = r12d3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
            return rax7;
        } else {
            return 1;
        }
    } else {
        rax8 = fun_36d0();
        if (!r8_9->f31 || (r12d10 = r8_9->f34, !!*reinterpret_cast<signed char*>(&r12d10))) {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(95);
            quotearg_style(4, rdi);
            fun_3800();
            fun_3b80();
            r12d10 = r8_9->f34;
        } else {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(95);
        }
        *reinterpret_cast<uint32_t*>(&rax11) = r12d10 ^ 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        return rax11;
    }
}

struct s26 {
    signed char[49] pad49;
    signed char f31;
    signed char[2] pad52;
    signed char f34;
};

int64_t fun_6ca3(int64_t rdi) {
    void** rax2;
    struct s26* rdx3;

    __asm__("cli ");
    rax2 = fun_36d0();
    if (!rdx3->f31 || rdx3->f34) {
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(95);
        quotearg_n_style();
        fun_3800();
        fun_3b80();
        return 0;
    } else {
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(95);
        return 0;
    }
}

struct s27 {
    signed char[72] pad72;
    int64_t f48;
};

int64_t hash_initialize(int64_t rdi);

void xalloc_die();

void fun_6d23(struct s27* rdi) {
    int64_t rax2;

    __asm__("cli ");
    rax2 = hash_initialize(61);
    rdi->f48 = rax2;
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

struct s28 {
    signed char[80] pad80;
    int64_t f50;
};

void fun_6d63(struct s28* rdi) {
    int64_t rax2;

    __asm__("cli ");
    rax2 = hash_initialize(61);
    rdi->f50 = rax2;
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

struct s29 {
    signed char[26] pad26;
    unsigned char f1a;
    unsigned char f1b;
    signed char[36] pad64;
    int32_t f40;
};

struct s30 {
    signed char[72] pad72;
    int64_t f48;
};

int64_t fun_38e0();

void fun_6da3(struct s29* rdi) {
    struct s29* rbx2;
    struct s30* rdi3;
    int64_t* rdi4;
    void* rcx5;
    int64_t rcx6;
    int64_t rax7;
    unsigned char al8;

    __asm__("cli ");
    rbx2 = rdi;
    rdi3 = reinterpret_cast<struct s30*>(reinterpret_cast<int64_t>(rdi) + 8);
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi3) - 8) = 0;
    rdi3->f48 = 0;
    rdi4 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi3) & 0xfffffffffffffff8);
    rcx5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbx2) - reinterpret_cast<uint64_t>(rdi4));
    *reinterpret_cast<uint32_t*>(&rcx6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rcx5) + 88) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    while (rcx6) {
        --rcx6;
        *rdi4 = 0;
        ++rdi4;
    }
    rax7 = fun_38e0();
    rbx2->f40 = -1;
    al8 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) == 0);
    rbx2->f1b = al8;
    rbx2->f1a = al8;
    return;
}

struct s31 {
    signed char[26] pad26;
    unsigned char f1a;
};

void** fun_6df3(struct s31* rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_36d0();
    *reinterpret_cast<unsigned char*>(&rax2) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax2) == 1)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax2) == 22)));
    if (*reinterpret_cast<unsigned char*>(&rax2)) {
        *reinterpret_cast<uint32_t*>(&rax2) = static_cast<uint32_t>(rdi->f1a) ^ 1;
        *reinterpret_cast<int32_t*>(&rax2 + 4) = 0;
    }
    return rax2;
}

void fun_38c0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void** top_level_dst_name = reinterpret_cast<void**>(0);

unsigned char fun_afe3(void** rdi, void** rsi, void** edx, void** rcx, void** r8d, void** r9, void** a7, void** a8) {
    void* rax9;
    int64_t rax10;
    unsigned char al11;
    void* rdx12;

    __asm__("cli ");
    rax9 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9)) <= reinterpret_cast<unsigned char>(3)) {
        *reinterpret_cast<signed char*>(&rax10) = *reinterpret_cast<signed char*>(r9 + 12);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        if (static_cast<uint32_t>(rax10 - 1) <= 2) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 68)) > reinterpret_cast<unsigned char>(2)) {
                addr_b0cd_4:
                fun_38c0("VALID_REFLINK_MODE (co->reflink_mode)", "src/copy.c", 0xc11, "valid_options");
            } else {
                if (*reinterpret_cast<unsigned char*>(r9 + 23) && *reinterpret_cast<unsigned char*>(r9 + 58)) {
                    fun_38c0("!(co->hard_link && co->symbolic_link)", "src/copy.c", 0xc12, "valid_options");
                    goto addr_b0ae_7;
                }
                if (*reinterpret_cast<signed char*>(&rax10) == 2 || *reinterpret_cast<void***>(r9 + 68) != 2) {
                    top_level_src_name = rdi;
                    top_level_dst_name = rsi;
                    al11 = copy_internal(rdi, rsi, edx, rcx, r8d, 0, 0, r9, 1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 - 8 + 23, a7, a8);
                    rdx12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax9) - reinterpret_cast<int64_t>(g28));
                    if (!rdx12) {
                        return al11;
                    }
                } else {
                    addr_b0ae_7:
                    fun_38c0("! (co->reflink_mode == REFLINK_ALWAYS && co->sparse_mode != SPARSE_AUTO)", "src/copy.c", 0xc13, "valid_options");
                    goto addr_b0cd_4;
                }
            }
            fun_3840();
        }
        fun_38c0("VALID_SPARSE_MODE (co->sparse_mode)", "src/copy.c", 0xc10, "valid_options");
    }
    fun_38c0("VALID_BACKUP_TYPE (co->backup_type)", "src/copy.c", 0xc0f, "valid_options");
}

int64_t fun_b133() {
    void** r12d1;
    void** eax2;
    int64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    r12d1 = mask_0;
    if (r12d1 == 0xffffffff) {
        eax2 = fun_3a60();
        mask_0 = eax2;
        fun_3a60();
        *reinterpret_cast<void***>(&rax3) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    } else {
        *reinterpret_cast<void***>(&rax4) = r12d1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    }
}

uint64_t fun_b173(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

struct s32 {
    int64_t f0;
    int64_t f8;
};

struct s33 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_b183(struct s32* rdi, struct s33* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f0 == rsi->f0) {
        rax3 = rsi->f8;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f8 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

struct s34 {
    signed char[16] pad16;
    void** f10;
};

void fun_b1a3(struct s34* rdi) {
    void** rdi2;

    __asm__("cli ");
    rdi2 = rdi->f10;
    fun_3670(rdi2, rdi2);
    goto fun_3670;
}

int64_t src_to_dest = 0;

void** hash_remove(int64_t rdi, void* rsi);

void fun_b1c3(int64_t rdi, int64_t rsi) {
    void* rax3;
    int64_t rdi4;
    void** rax5;
    void** rdi6;
    void* rax7;

    __asm__("cli ");
    rax3 = g28;
    rdi4 = src_to_dest;
    rax5 = hash_remove(rdi4, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32);
    if (rax5) {
        rdi6 = *reinterpret_cast<void***>(rax5 + 16);
        fun_3670(rdi6, rdi6);
        fun_3670(rax5, rax5);
    }
    rax7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax3) - reinterpret_cast<int64_t>(g28));
    if (rax7) {
        fun_3840();
    } else {
        return;
    }
}

void** hash_lookup(int64_t rdi, void** rsi);

void** fun_b233(int64_t rdi, int64_t rsi) {
    void* rax3;
    int64_t rdi4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    rdi4 = src_to_dest;
    rax5 = hash_lookup(rdi4, reinterpret_cast<int64_t>(__zero_stack_offset()) - 40);
    if (rax5) {
        rax5 = *reinterpret_cast<void***>(rax5 + 16);
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax3) - reinterpret_cast<int64_t>(g28));
    if (rdx6) {
        fun_3840();
    } else {
        return rax5;
    }
}

struct s35 {
    signed char[88] pad88;
    uint64_t f58;
    int64_t f60;
};

void** hash_insert(int64_t rdi, void** rsi, void** rdx, struct s35* rcx);

void** fun_b293(void** rdi, void** rsi, void** rdx, struct s35* rcx) {
    void** rax5;
    void** rax6;
    int64_t rdi7;
    void** rax8;
    void** rax9;
    void** rdi10;

    __asm__("cli ");
    rax5 = xmalloc(24, rsi, rdx);
    rax6 = xstrdup(rdi, rsi);
    rdi7 = src_to_dest;
    *reinterpret_cast<void***>(rax5) = rsi;
    *reinterpret_cast<void***>(rax5 + 16) = rax6;
    *reinterpret_cast<void***>(rax5 + 8) = rdx;
    rax8 = hash_insert(rdi7, rax5, rdx, rcx);
    if (!rax8) {
        xalloc_die();
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = 0;
        *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
        if (rax5 != rax8) {
            rdi10 = *reinterpret_cast<void***>(rax5 + 16);
            fun_3670(rdi10, rdi10);
            fun_3670(rax5, rax5);
            rax9 = *reinterpret_cast<void***>(rax8 + 16);
        }
        return rax9;
    }
}

void fun_b313() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = hash_initialize(0x67);
    src_to_dest = rax1;
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_3b20();

void fun_b353(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_3b20;
}

void** fun_3c80();

void fun_b3d3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_3c80;
}

int32_t try_tempname_len(void** rdi, ...);

int64_t fun_b3f3(int32_t edi, int64_t rsi, int32_t edx, void** rcx, int32_t r8d, int32_t r9d, void** a7) {
    int32_t r15d8;
    void* rsp9;
    void* rdx10;
    void** rdx11;
    void** eax12;
    void** eax13;
    void** r12d14;
    void* rax15;
    int64_t rax16;
    void** rax17;
    void** rax18;
    void** rax19;
    void** rax20;
    int32_t eax21;
    int64_t rdi22;
    int32_t eax23;
    void** rax24;
    int64_t rdi25;
    void** rax26;

    __asm__("cli ");
    r15d8 = r9d;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rdx10 = g28;
    *reinterpret_cast<int32_t*>(&rdx11) = 0;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    eax12 = a7;
    if (reinterpret_cast<signed char>(eax12) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&rdx11) = edx;
        *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
        eax13 = fun_3b20();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        r12d14 = eax13;
        if (!eax13) {
            addr_b4c3_3:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx10) - reinterpret_cast<int64_t>(g28));
            if (rax15) {
                fun_3840();
            } else {
                *reinterpret_cast<void***>(&rax16) = r12d14;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                return rax16;
            }
        } else {
            rax17 = fun_36d0();
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            eax12 = *reinterpret_cast<void***>(rax17);
        }
    }
    if (*reinterpret_cast<signed char*>(&r15d8) != 1 || !reinterpret_cast<int1_t>(eax12 == 17)) {
        r12d14 = eax12;
        goto addr_b4c3_3;
    } else {
        rax18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 48);
        rax19 = samedir_template(rcx, rax18, rdx11, rcx);
        if (!rax19) {
            rax20 = fun_36d0();
            r12d14 = *reinterpret_cast<void***>(rax20);
            goto addr_b4c3_3;
        } else {
            eax21 = try_tempname_len(rax19);
            if (!eax21) {
                *reinterpret_cast<int32_t*>(&rdi22) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
                eax23 = fun_3c00(rdi22, rax19, rdi22, rax19);
                r12d14 = reinterpret_cast<void**>(0xffffffff);
                if (eax23) {
                    rax24 = fun_36d0();
                    r12d14 = *reinterpret_cast<void***>(rax24);
                }
                *reinterpret_cast<int32_t*>(&rdi25) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
                fun_3720(rdi25, rax19, rdi25, rax19);
            } else {
                rax26 = fun_36d0();
                r12d14 = *reinterpret_cast<void***>(rax26);
            }
            if (rax19 != rax18) {
                fun_3670(rax19, rax19);
                goto addr_b4c3_3;
            }
        }
    }
}

int64_t fun_b563(int64_t rdi, int32_t esi, void** rdx, void** rcx, void** r8d) {
    int32_t ebx6;
    void* rsp7;
    void* rax8;
    void** eax9;
    void** r12d10;
    void* rax11;
    int64_t rax12;
    void** rax13;
    void** rbx14;
    void** rax15;
    void** rax16;
    int32_t eax17;
    int64_t rdi18;
    int32_t eax19;
    void** rax20;
    int64_t rdi21;
    void** rax22;

    __asm__("cli ");
    ebx6 = *reinterpret_cast<int32_t*>(&rcx);
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128);
    rax8 = g28;
    if (reinterpret_cast<signed char>(r8d) < reinterpret_cast<signed char>(0)) {
        eax9 = fun_3c80();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        r12d10 = eax9;
        if (!eax9) {
            addr_b603_3:
            rax11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax8) - reinterpret_cast<int64_t>(g28));
            if (rax11) {
                fun_3840();
            } else {
                *reinterpret_cast<void***>(&rax12) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                return rax12;
            }
        } else {
            rax13 = fun_36d0();
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            r8d = *reinterpret_cast<void***>(rax13);
        }
    }
    if (*reinterpret_cast<signed char*>(&ebx6) != 1 || !reinterpret_cast<int1_t>(r8d == 17)) {
        r12d10 = r8d;
        goto addr_b603_3;
    } else {
        rbx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
        rax15 = samedir_template(rdx, rbx14, rdx, rcx);
        if (!rax15) {
            rax16 = fun_36d0();
            r12d10 = *reinterpret_cast<void***>(rax16);
            goto addr_b603_3;
        } else {
            eax17 = try_tempname_len(rax15, rax15);
            if (!eax17) {
                *reinterpret_cast<int32_t*>(&rdi18) = esi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0;
                eax19 = fun_3c00(rdi18, rax15, rdi18, rax15);
                r12d10 = reinterpret_cast<void**>(0xffffffff);
                if (eax19) {
                    rax20 = fun_36d0();
                    *reinterpret_cast<int32_t*>(&rdi21) = esi;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
                    r12d10 = *reinterpret_cast<void***>(rax20);
                    fun_3720(rdi21, rax15, rdi21, rax15);
                }
            } else {
                rax22 = fun_36d0();
                r12d10 = *reinterpret_cast<void***>(rax22);
            }
            if (rax15 != rbx14) {
                fun_3670(rax15, rax15);
                goto addr_b603_3;
            }
        }
    }
}

int32_t qcopy_acl();

int64_t fun_b6a3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t eax7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax7 = qcopy_acl();
    if (eax7 == -2) {
        quote(rdi, rsi, rdx, rcx, r8, r9);
        fun_36d0();
        fun_3b80();
        *reinterpret_cast<int32_t*>(&rax8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        if (eax7 == -1) {
            quote(rdx, rsi, rdx, rcx, r8, r9);
            fun_3800();
            fun_36d0();
            fun_3b80();
        }
        *reinterpret_cast<int32_t*>(&rax9) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

int64_t fun_b753(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t eax7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax7 = qset_acl(rdi, rsi);
    if (eax7) {
        quote(rdi, rsi, rdx, rcx, r8, r9);
        fun_3800();
        fun_36d0();
        fun_3b80();
        *reinterpret_cast<int32_t*>(&rax8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

void** fun_3760(int64_t rdi, void** rsi, void** rdx);

void** fun_3b10(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_b7d3(int64_t rdi, void** rsi) {
    int64_t rbp3;
    void** rbx4;
    void* rax5;
    void* v6;
    int1_t zf7;
    unsigned char r14b8;
    void** v9;
    void** rax10;
    void** r15_11;
    void* rax12;
    void** rdi13;
    void** rdx14;
    void** rax15;
    void** r13_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** rcx20;
    void** rax21;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0x80;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    rax5 = g28;
    v6 = rax5;
    zf7 = rsi == 0;
    r14b8 = reinterpret_cast<uint1_t>(!zf7);
    if (!zf7 && (rbx4 = rsi + 1, reinterpret_cast<unsigned char>(rsi) >= reinterpret_cast<unsigned char>(0x401))) {
        rbx4 = reinterpret_cast<void**>(0x401);
    }
    v9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 + 16);
    goto addr_b838_4;
    addr_b920_5:
    rax10 = fun_36d0();
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(12);
    addr_b8cb_6:
    rax12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v6) - reinterpret_cast<int64_t>(g28));
    if (rax12) {
        fun_3840();
    } else {
        return r15_11;
    }
    addr_b8c0_9:
    rdi13 = r15_11;
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    fun_3670(rdi13, rdi13);
    goto addr_b8cb_6;
    while (rax15 = fun_3ab0(rbx4, rsi, rdx14), r13_16 = rax15, !!rax15) {
        r15_11 = rax15;
        do {
            rdx14 = rbx4;
            rsi = r13_16;
            rax17 = fun_3760(rbp3, rsi, rdx14);
            if (reinterpret_cast<signed char>(rax17) < reinterpret_cast<signed char>(0)) 
                goto addr_b8c0_9;
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(rax17)) 
                goto addr_b8f8_14;
            fun_3670(r15_11, r15_11);
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(0x3fffffffffffffff)) {
                if (rbx4 == 0x7fffffffffffffff) 
                    goto addr_b920_5;
                rbx4 = reinterpret_cast<void**>(0x7fffffffffffffff);
                addr_b838_4:
                if (!reinterpret_cast<int1_t>(rbx4 == 0x80)) 
                    break;
            } else {
                rbx4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<unsigned char>(rbx4));
                if (rbx4 != 0x80) 
                    break;
            }
            r13_16 = v9;
            *reinterpret_cast<int32_t*>(&r15_11) = 0;
            *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        } while (!r14b8);
    }
    goto addr_b920_5;
    addr_b8f8_14:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax17)) = 0;
    r12_18 = rax17 + 1;
    if (!r15_11) {
        rax19 = fun_3ab0(r12_18, rsi, rdx14);
        r15_11 = rax19;
        if (rax19) {
            fun_3a80(rax19, r13_16, r12_18);
            goto addr_b8cb_6;
        }
    } else {
        if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(r12_18)) {
            rax21 = fun_3b10(r15_11, r12_18, rdx14, rcx20);
            if (rax21) {
                r15_11 = rax21;
            }
            goto addr_b8cb_6;
        }
    }
}

void** fun_b963(int32_t edi, void** rsi, void** rdx) {
    void** r13_4;
    void** rbp5;
    int32_t ebx6;
    void* rax7;
    void* v8;
    int1_t zf9;
    unsigned char r12b10;
    void** rax11;
    void** v12;
    void** rdi13;
    void** r15_14;
    void* rax15;
    void** rax16;
    void** rax17;
    void** r14_18;
    int64_t rdi19;
    void** rax20;
    void** r12_21;
    void** rax22;
    void** rax23;
    void** rax24;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_4) = 0x80;
    *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
    rbp5 = rsi;
    ebx6 = edi;
    rax7 = g28;
    v8 = rax7;
    zf9 = rdx == 0;
    r12b10 = reinterpret_cast<uint1_t>(!zf9);
    if (!zf9) {
        *reinterpret_cast<int32_t*>(&rax11) = 0x401;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rdx) < reinterpret_cast<unsigned char>(0x401)) {
            rax11 = rdx + 1;
        }
        r13_4 = rax11;
    }
    v12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 + 16);
    goto addr_b9c0_6;
    addr_ba50_7:
    rdi13 = r15_14;
    *reinterpret_cast<int32_t*>(&r15_14) = 0;
    *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
    fun_3670(rdi13, rdi13);
    addr_ba5b_8:
    rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v8) - reinterpret_cast<int64_t>(g28));
    if (rax15) {
        fun_3840();
    } else {
        return r15_14;
    }
    addr_bab0_11:
    rax16 = fun_36d0();
    *reinterpret_cast<int32_t*>(&r15_14) = 0;
    *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
    *reinterpret_cast<void***>(rax16) = reinterpret_cast<void**>(12);
    goto addr_ba5b_8;
    while (rax17 = fun_3ab0(r13_4, rsi, rdx, r13_4, rsi, rdx), r14_18 = rax17, !!rax17) {
        r15_14 = rax17;
        do {
            rdx = r14_18;
            rsi = rbp5;
            *reinterpret_cast<int32_t*>(&rdi19) = ebx6;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
            rax20 = fun_39f0(rdi19, rsi, rdx, r13_4);
            if (reinterpret_cast<signed char>(rax20) < reinterpret_cast<signed char>(0)) 
                goto addr_ba50_7;
            if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(rax20)) 
                goto addr_ba88_16;
            fun_3670(r15_14, r15_14);
            if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(0x3fffffffffffffff)) {
                if (r13_4 == 0x7fffffffffffffff) 
                    goto addr_bab0_11;
                r13_4 = reinterpret_cast<void**>(0x7fffffffffffffff);
                addr_b9c0_6:
                if (!reinterpret_cast<int1_t>(r13_4 == 0x80)) 
                    break;
            } else {
                r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_4) + reinterpret_cast<unsigned char>(r13_4));
                if (r13_4 != 0x80) 
                    break;
            }
            r14_18 = v12;
            *reinterpret_cast<int32_t*>(&r15_14) = 0;
            *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
        } while (!r12b10);
    }
    goto addr_bae3_22;
    addr_ba88_16:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_18) + reinterpret_cast<unsigned char>(rax20)) = 0;
    r12_21 = rax20 + 1;
    if (!r15_14) {
        rax22 = fun_3ab0(r12_21, rsi, rdx, r12_21, rsi, rdx);
        if (!rax22) {
            addr_bae3_22:
            *reinterpret_cast<int32_t*>(&r15_14) = 0;
            *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
            goto addr_ba5b_8;
        } else {
            rax23 = fun_3a80(rax22, r14_18, r12_21, rax22, r14_18, r12_21);
            r15_14 = rax23;
            goto addr_ba5b_8;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(r12_21)) {
            rax24 = fun_3b10(r15_14, r12_21, rdx, r13_4);
            if (rax24) {
                r15_14 = rax24;
            }
            goto addr_ba5b_8;
        }
    }
}

void fun_baf3() {
    __asm__("cli ");
    goto usage;
}

int64_t fun_bb03(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_3820(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_36e0(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_bb83_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_bbd0_6; else 
                    continue;
            } else {
                rax18 = fun_3820(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_bc00_8;
                if (v16 == -1) 
                    goto addr_bbbe_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_bb83_5;
            } else {
                eax19 = fun_3990(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_bb83_5;
            }
            addr_bbbe_10:
            v16 = rbx15;
            goto addr_bb83_5;
        }
    }
    addr_bbe5_16:
    return v12;
    addr_bbd0_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_bbe5_16;
    addr_bc00_8:
    v12 = rbx15;
    goto addr_bbe5_16;
}

int64_t fun_bc13(void** rdi, void*** rsi, void** rdx, void** rcx, void** r8) {
    void** r12_6;
    void** rdi7;
    void*** rbp8;
    int64_t rbx9;
    int64_t rax10;

    __asm__("cli ");
    r12_6 = rdi;
    rdi7 = *rsi;
    if (!rdi7) {
        addr_bc58_2:
        return -1;
    } else {
        rbp8 = rsi;
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        do {
            rax10 = fun_39e0(rdi7, r12_6, rdx, rcx, r8);
            if (!*reinterpret_cast<int32_t*>(&rax10)) 
                break;
            ++rbx9;
            rdi7 = rbp8[rbx9 * 8];
        } while (rdi7);
        goto addr_bc58_2;
    }
    return rbx9;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_bc73(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_3800();
    } else {
        fun_3800();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_3b80;
}

void fun_bd03(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rdx9;
    void** rbp10;
    void* v11;
    void* rbx12;
    void** r14_13;
    void** v14;
    void** rax15;
    void** rsi16;
    void** r15_17;
    int64_t rbx18;
    uint32_t eax19;
    void** rax20;
    void** rdi21;
    void* v22;
    int64_t v23;
    void** rax24;
    void** rdi25;
    void* v26;
    int64_t v27;
    void** rdi28;
    void** rax29;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    r12_8 = rdx;
    *reinterpret_cast<int32_t*>(&rdx9) = 5;
    *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
    rbp10 = rsi;
    v11 = rbx12;
    r14_13 = stderr;
    v14 = rdi;
    rax15 = fun_3800();
    rsi16 = r14_13;
    fun_39b0(rax15, rsi16, 5, rcx, r8);
    r15_17 = *reinterpret_cast<void***>(rdi);
    *reinterpret_cast<int32_t*>(&rbx18) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx18) + 4) = 0;
    if (r15_17) {
        do {
            if (!rbx18 || (rdx9 = r12_8, rsi16 = rbp10, eax19 = fun_3990(r13_7, rsi16, rdx9, r13_7, rsi16, rdx9), !!eax19)) {
                r13_7 = rbp10;
                rax20 = quote(r15_17, rsi16, rdx9, rcx, r8, r9);
                rdi21 = stderr;
                rdx9 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                rcx = rax20;
                fun_3c40(rdi21, 1, "\n  - %s", rcx, r8, r9, v22, v14, v23, v11);
            } else {
                rax24 = quote(r15_17, rsi16, rdx9, rcx, r8, r9);
                rdi25 = stderr;
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                rdx9 = reinterpret_cast<void**>(", %s");
                rcx = rax24;
                fun_3c40(rdi25, 1, ", %s", rcx, r8, r9, v26, v14, v27, v11);
            }
            ++rbx18;
            rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r12_8));
            r15_17 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v14) + reinterpret_cast<uint64_t>(rbx18 * 8));
        } while (r15_17);
    }
    rdi28 = stderr;
    rax29 = *reinterpret_cast<void***>(rdi28 + 40);
    if (reinterpret_cast<unsigned char>(rax29) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi28 + 48))) {
        goto 0x3880;
    } else {
        *reinterpret_cast<void***>(rdi28 + 40) = rax29 + 1;
        *reinterpret_cast<void***>(rax29) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(void** rdi, void** rsi, void** rdx, void** rcx);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void argmatch_valid(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_be33(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    void** r13_10;
    void** r12_11;
    void** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int64_t rax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_be77_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_beee_4;
        } else {
            addr_beee_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            rax17 = fun_39e0(rdi15, r14_9, rdx, rcx, r8);
            if (!*reinterpret_cast<int32_t*>(&rax17)) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<void***>(rbp12 + rbx16 * 8);
        } while (rdi15);
        goto addr_be70_8;
    } else {
        goto addr_be70_8;
    }
    return rbx16;
    addr_be70_8:
    rax14 = -1;
    goto addr_be77_3;
}

struct s36 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_bf03(void** rdi, struct s36* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_3990(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

void fun_bf63(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rbx5 = rdi;
    if (!rdi) {
        rax6 = fun_3690("SIMPLE_BACKUP_SUFFIX", rsi);
        rbx5 = rax6;
        if (!rax6) {
            simple_backup_suffix = reinterpret_cast<void**>("~");
            return;
        }
    }
    if (*reinterpret_cast<void***>(rbx5) && (rax7 = last_component(rbx5, rsi, rdx, rcx), rbx5 == rax7)) {
        simple_backup_suffix = rbx5;
        return;
    }
}

void** base_len(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t opendirat(int64_t rdi, void** rsi);

void fun_3930(int64_t rdi, void** rsi, void** rdx, void** rcx);

void* fun_37b0(void** rdi, int64_t rsi);

void* fun_3a20();

struct s37 {
    void** f0;
    signed char[18] pad19;
    void** f13;
    unsigned char f14;
    unsigned char f15;
};

struct s37* fun_3aa0(int64_t rdi, void** rsi, ...);

struct s38 {
    int16_t f0;
    signed char f2;
};

int32_t fun_3950(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** fun_bfc3(uint32_t edi, void** rsi, void** rdx, void** rcx) {
    uint32_t v5;
    void** v6;
    int32_t v7;
    unsigned char v8;
    void* rax9;
    void* v10;
    void** rax11;
    void** v12;
    void** rax13;
    void* rsp14;
    void* r14_15;
    void** rdi16;
    void** v17;
    void** v18;
    void** rax19;
    void** rax20;
    void** rax21;
    void** rax22;
    void** v23;
    void** rax24;
    void** rax25;
    void** v26;
    void** rax27;
    void* rsp28;
    void** r12_29;
    void* rax30;
    void** v31;
    int64_t rbp32;
    uint32_t v33;
    void** rbx34;
    void** rdx35;
    void** rsi36;
    void* rsp37;
    void** rdi38;
    uint32_t r15d39;
    void** r8_40;
    uint16_t* r15_41;
    int64_t rdi42;
    uint32_t r13d43;
    int64_t rax44;
    void* rsp45;
    void** rax46;
    unsigned char al47;
    uint32_t eax48;
    uint32_t v49;
    void** rdi50;
    int32_t eax51;
    void** rax52;
    void** rax53;
    void** rax54;
    void** rax55;
    void* rsp56;
    uint32_t r8d57;
    void* rax58;
    uint32_t r8d59;
    void* rax60;
    void** rax61;
    void** v62;
    void** v63;
    struct s37* rax64;
    void** r13_65;
    void** rax66;
    void** rdi67;
    void** r15_68;
    uint32_t eax69;
    void** r13_70;
    int64_t rax71;
    uint32_t eax72;
    uint32_t eax73;
    void** r15_74;
    uint32_t eax75;
    unsigned char* r9_76;
    uint32_t eax77;
    unsigned char* r9_78;
    void** rdx79;
    void** rax80;
    void** rax81;
    void** r8_82;
    uint64_t rsi83;
    void** rsi84;
    void** rax85;
    struct s38* rax86;
    void** rax87;
    void* rsp88;
    void* rdi89;
    uint32_t edx90;
    unsigned char* rax91;
    void** rax92;
    void** r15d93;
    void** rdx94;
    void** rdi95;
    void** rdi96;
    void** rax97;

    __asm__("cli ");
    v5 = edi;
    v6 = rsi;
    v7 = *reinterpret_cast<int32_t*>(&rdx);
    v8 = *reinterpret_cast<unsigned char*>(&rcx);
    rax9 = g28;
    v10 = rax9;
    rax11 = last_component(rsi, rsi, rdx, rcx);
    v12 = rax11;
    rax13 = base_len(rax11, rsi, rdx, rcx);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8);
    r14_15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(rsi));
    rdi16 = simple_backup_suffix;
    v17 = rax13;
    v18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(r14_15));
    if (!rdi16) {
        rax19 = fun_3690("SIMPLE_BACKUP_SUFFIX", rsi);
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
        rdi16 = reinterpret_cast<void**>("~");
        if (rax19 && (*reinterpret_cast<void***>(rax19) && (rax20 = last_component(rax19, rsi, rdx, rcx), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8), rdi16 = rax20, rax19 != rax20))) {
            rdi16 = reinterpret_cast<void**>("~");
        }
        simple_backup_suffix = rdi16;
    }
    rax21 = fun_3820(rdi16);
    rax22 = rax21 + 1;
    v23 = rax22;
    *reinterpret_cast<int32_t*>(&rax24) = 9;
    *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
    if (reinterpret_cast<signed char>(rax22) >= reinterpret_cast<signed char>(9)) {
        rax24 = rax22;
    }
    rax25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(rax24) + 1);
    v26 = rax25;
    rax27 = fun_3ab0(rax25, v18, rdx);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8);
    r12_29 = rax27;
    if (!rax27) {
        addr_c330_8:
        rax30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v10) - reinterpret_cast<int64_t>(g28));
        if (rax30) {
            fun_3840();
        } else {
            return r12_29;
        }
    } else {
        v31 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rbp32) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp32) + 4) = 0;
        v33 = v5;
        rbx34 = v17 + 4;
        do {
            rdx35 = v18;
            rsi36 = v6;
            fun_3a80(r12_29, rsi36, rdx35, r12_29, rsi36, rdx35);
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            if (v7 == 1) {
                rdx35 = v23;
                rsi36 = simple_backup_suffix;
                rdi38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<unsigned char>(v18));
                fun_3a80(rdi38, rsi36, rdx35, rdi38, rsi36, rdx35);
                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                if (!v8) 
                    break;
                rsi36 = v6;
                r15d39 = v8;
                rcx = r12_29;
                *reinterpret_cast<uint32_t*>(&r8_40) = 0;
            } else {
                if (!rbp32) {
                    r15_41 = reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                    *reinterpret_cast<uint32_t*>(&rdi42) = v5;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi42) + 4) = 0;
                    r13d43 = *r15_41;
                    *r15_41 = 46;
                    rcx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x84);
                    rsi36 = r12_29;
                    rax44 = opendirat(rdi42, rsi36);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                    rbp32 = rax44;
                    rdx35 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r15_41) + reinterpret_cast<unsigned char>(v17));
                    if (!rbp32) {
                        rax46 = fun_36d0();
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rdx35 = rdx35;
                        *r15_41 = *reinterpret_cast<uint16_t*>(&r13d43);
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax46) == 12);
                        *reinterpret_cast<void***>(rdx35) = reinterpret_cast<void**>(0x7e317e2e);
                        *reinterpret_cast<signed char*>(rdx35 + 4) = 0;
                        eax48 = al47 + 2;
                        v49 = eax48;
                        if (eax48 == 2) {
                            addr_c3f0_18:
                            if (v7 == 2) {
                                rdx35 = v23;
                                rsi36 = simple_backup_suffix;
                                rdi50 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<unsigned char>(v18));
                                fun_3a80(rdi50, rsi36, rdx35, rdi50, rsi36, rdx35);
                                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                v7 = 1;
                                goto addr_c3fb_20;
                            }
                        } else {
                            if (v49 == 3) 
                                goto addr_c56c_22;
                            goto addr_c29b_24;
                        }
                    } else {
                        *r15_41 = *reinterpret_cast<uint16_t*>(&r13d43);
                        *reinterpret_cast<void***>(rdx35) = reinterpret_cast<void**>(0x7e317e2e);
                        *reinterpret_cast<signed char*>(rdx35 + 4) = 0;
                        goto addr_c0c1_26;
                    }
                } else {
                    fun_3930(rbp32, rsi36, rdx35, rcx);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                    goto addr_c0c1_26;
                }
            }
            addr_c2c6_28:
            *reinterpret_cast<uint32_t*>(&rdx35) = v33;
            *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
            eax51 = renameatu();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            if (!eax51) 
                break; else 
                continue;
            addr_c3fb_20:
            r15d39 = 1;
            rax52 = last_component(r12_29, rsi36, rdx35, rcx);
            rax53 = base_len(rax52, rsi36, rdx35, rcx);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8);
            rcx = rax53;
            if (reinterpret_cast<signed char>(rax53) > reinterpret_cast<signed char>(14)) {
                rdx35 = rax52;
                if (v31) {
                    addr_c525_30:
                    if (reinterpret_cast<signed char>(rcx) <= reinterpret_cast<signed char>(v31)) {
                        r15d39 = 1;
                        goto addr_c423_32;
                    } else {
                        addr_c530_33:
                        rsi36 = v31;
                        rcx = rsi36 + 0xffffffffffffffff;
                        rax54 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r12_29)) - reinterpret_cast<unsigned char>(rdx35));
                        if (reinterpret_cast<signed char>(rax54) >= reinterpret_cast<signed char>(rsi36)) {
                            rax54 = rcx;
                        }
                    }
                } else {
                    rax55 = fun_36d0();
                    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (reinterpret_cast<int32_t>(v33) < reinterpret_cast<int32_t>(0)) {
                        r8d57 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rax52));
                        *reinterpret_cast<void***>(rax52) = reinterpret_cast<void**>(46);
                        *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(0);
                        rax58 = fun_37b0(r12_29, 3);
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                        rdx35 = rax52;
                        r8d59 = r8d57;
                        rcx = rax53;
                        v31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax58) - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax58) < static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax55)) < reinterpret_cast<unsigned char>(1)))));
                        *reinterpret_cast<void***>(rdx35) = *reinterpret_cast<void***>(&r8d59);
                    } else {
                        *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(0);
                        rax60 = fun_3a20();
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                        rcx = rax53;
                        rdx35 = rax52;
                        v31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax60) - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax60) < static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax55)) < reinterpret_cast<unsigned char>(1)))));
                    }
                    rsi36 = v31;
                    if (reinterpret_cast<signed char>(rsi36) < reinterpret_cast<signed char>(0)) 
                        goto addr_c587_39; else 
                        goto addr_c525_30;
                }
            } else {
                addr_c423_32:
                if (!v8) 
                    break; else 
                    goto addr_c42e_40;
            }
            r15d39 = 0;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx35) + reinterpret_cast<unsigned char>(rax54)) = 0x7e;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx35) + reinterpret_cast<unsigned char>(rax54) + 1) = 0;
            goto addr_c423_32;
            addr_c587_39:
            *reinterpret_cast<int32_t*>(&rax61) = 14;
            *reinterpret_cast<int32_t*>(&rax61 + 4) = 0;
            if (rsi36 == 0xffffffffffffffff) {
                rax61 = rsi36;
            }
            v31 = rax61;
            goto addr_c530_33;
            addr_c42e_40:
            if (v7 != 1) {
                addr_c2b7_44:
                rsi36 = v12;
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                *reinterpret_cast<uint32_t*>(&r8_40) = 1;
                goto addr_c2c6_28;
            } else {
                rsi36 = v6;
                rcx = r12_29;
                *reinterpret_cast<uint32_t*>(&r8_40) = 0;
                goto addr_c2c6_28;
            }
            addr_c29b_24:
            if (v49 != 1) {
                if (!v8) 
                    break;
                r15d39 = v8;
                goto addr_c2b7_44;
            }
            addr_c0c1_26:
            v49 = 2;
            v62 = reinterpret_cast<void**>(1);
            v63 = v26;
            addr_c0e0_48:
            while (rax64 = fun_3aa0(rbp32, rsi36, rbp32, rsi36), rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), !!rax64) {
                do {
                    r13_65 = reinterpret_cast<void**>(&rax64->f13);
                    rax66 = fun_3820(r13_65, r13_65);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (reinterpret_cast<unsigned char>(rax66) < reinterpret_cast<unsigned char>(rbx34)) 
                        goto addr_c0e0_48;
                    rdi67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                    rsi36 = r13_65;
                    r15_68 = v17 + 2;
                    rdx35 = r15_68;
                    eax69 = fun_3990(rdi67, rsi36, rdx35, rdi67, rsi36, rdx35);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    if (eax69) 
                        goto addr_c0e0_48;
                    r13_70 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_65) + reinterpret_cast<unsigned char>(r15_68));
                    *reinterpret_cast<uint32_t*>(&rax71) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_70));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax71) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rdx35) = static_cast<uint32_t>(rax71 - 49);
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&rdx35) > 8) 
                        goto addr_c0e0_48;
                    eax72 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_70 + 1))));
                    *reinterpret_cast<unsigned char*>(&r8_40) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax71) == 57);
                    *reinterpret_cast<uint32_t*>(&rdx35) = eax72;
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    eax73 = eax72 - 48;
                    if (eax73 > 9) {
                        *reinterpret_cast<int32_t*>(&rcx) = 1;
                        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r15_74) = 1;
                        *reinterpret_cast<int32_t*>(&r15_74 + 4) = 0;
                    } else {
                        *reinterpret_cast<int32_t*>(&r15_74) = 1;
                        *reinterpret_cast<int32_t*>(&r15_74 + 4) = 0;
                        do {
                            *reinterpret_cast<unsigned char*>(&eax73) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rdx35) == 57);
                            ++r15_74;
                            *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<uint32_t*>(&r8_40) & eax73;
                            eax75 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_70) + reinterpret_cast<unsigned char>(r15_74))));
                            rcx = r15_74;
                            *reinterpret_cast<uint32_t*>(&rdx35) = eax75;
                            *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                            eax73 = eax75 - 48;
                        } while (eax73 <= 9);
                    }
                    if (*reinterpret_cast<unsigned char*>(&rdx35) != 0x7e) 
                        goto addr_c0e0_48;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_70) + reinterpret_cast<unsigned char>(rcx) + 1)) 
                        goto addr_c0e0_48;
                    if (reinterpret_cast<signed char>(v62) >= reinterpret_cast<signed char>(r15_74)) {
                        if (v62 != r15_74) 
                            goto addr_c0e0_48;
                        rdx35 = rcx;
                        rsi36 = r13_70;
                        r9_76 = reinterpret_cast<unsigned char*>(v18 + 2);
                        eax77 = fun_3990(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r9_76), rsi36, rdx35);
                        rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rcx = rcx;
                        r9_78 = r9_76;
                        *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<unsigned char*>(&r8_40);
                        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax77) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax77 == 0))) 
                            break;
                    } else {
                        r9_78 = reinterpret_cast<unsigned char*>(v18 + 2);
                    }
                    *reinterpret_cast<uint32_t*>(&rdx79) = *reinterpret_cast<unsigned char*>(&r8_40);
                    *reinterpret_cast<int32_t*>(&rdx79 + 4) = 0;
                    rax80 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx79) + reinterpret_cast<unsigned char>(r15_74));
                    v49 = *reinterpret_cast<unsigned char*>(&r8_40);
                    v62 = rax80;
                    rax81 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax80) + reinterpret_cast<uint64_t>(r9_78) + 2);
                    if (reinterpret_cast<signed char>(rax81) <= reinterpret_cast<signed char>(v63)) {
                        r8_82 = r12_29;
                    } else {
                        if (__intrinsic()) {
                            v63 = rax81;
                        } else {
                            v63 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rax81) >> 1) + reinterpret_cast<unsigned char>(rax81));
                        }
                        *reinterpret_cast<int32_t*>(&rsi83) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi83) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rsi83) = reinterpret_cast<uint1_t>(v63 == 0);
                        rsi84 = reinterpret_cast<void**>(rsi83 | reinterpret_cast<unsigned char>(v63));
                        rax85 = fun_3b10(r12_29, rsi84, rdx79, rcx);
                        rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rcx = rcx;
                        rdx79 = rdx79;
                        r8_82 = rax85;
                        if (!rax85) 
                            goto addr_c564_68;
                    }
                    rsi36 = r13_70;
                    rax86 = reinterpret_cast<struct s38*>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r8_82));
                    rax86->f0 = 0x7e2e;
                    rax86->f2 = 48;
                    rax87 = fun_3a80(reinterpret_cast<uint64_t>(rax86) + reinterpret_cast<unsigned char>(rdx79) + 2, rsi36, r15_74 + 2);
                    rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    rcx = rcx;
                    r8_40 = r8_82;
                    rdi89 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax87) + reinterpret_cast<unsigned char>(rcx));
                    edx90 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi89) + 0xffffffffffffffff);
                    rax91 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi89) + 0xffffffffffffffff);
                    if (*reinterpret_cast<signed char*>(&edx90) == 57) {
                        do {
                            *rax91 = 48;
                            edx90 = *(rax91 - 1);
                            --rax91;
                        } while (*reinterpret_cast<signed char*>(&edx90) == 57);
                    }
                    *reinterpret_cast<uint32_t*>(&rdx35) = edx90 + 1;
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    r12_29 = r8_40;
                    *rax91 = *reinterpret_cast<unsigned char*>(&rdx35);
                    rax64 = fun_3aa0(rbp32, rsi36);
                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp88) - 8 + 8);
                } while (rax64);
                goto addr_c289_73;
            }
            addr_c290_75:
            if (v49 == 2) 
                goto addr_c3f0_18; else 
                goto addr_c29b_24;
            addr_c289_73:
            goto addr_c290_75;
            rax92 = fun_36d0();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
        } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax92) == 17) && *reinterpret_cast<signed char*>(&r15d39) == 1);
        goto addr_c2f2_77;
    }
    if (rbp32) {
        fun_3950(rbp32, rsi36, rdx35, rcx);
        goto addr_c330_8;
    }
    addr_c2f2_77:
    r15d93 = *reinterpret_cast<void***>(rax92);
    rdx94 = rax92;
    if (rbp32) {
        fun_3950(rbp32, rsi36, rdx94, rcx);
        rdx94 = rax92;
    }
    rdi95 = r12_29;
    *reinterpret_cast<int32_t*>(&r12_29) = 0;
    *reinterpret_cast<int32_t*>(&r12_29 + 4) = 0;
    fun_3670(rdi95, rdi95);
    *reinterpret_cast<void***>(rdx94) = r15d93;
    goto addr_c330_8;
    addr_c56c_22:
    rdi96 = r12_29;
    *reinterpret_cast<int32_t*>(&r12_29) = 0;
    *reinterpret_cast<int32_t*>(&r12_29 + 4) = 0;
    fun_3670(rdi96, rdi96);
    rax97 = fun_36d0();
    *reinterpret_cast<void***>(rax97) = reinterpret_cast<void**>(12);
    goto addr_c330_8;
    addr_c564_68:
    fun_3950(rbp32, rsi84, rdx79, rcx);
    goto addr_c56c_22;
}

int64_t backupfile_internal();

void fun_c6d3() {
    __asm__("cli ");
    goto backupfile_internal;
}

void fun_c6e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = backupfile_internal();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_c703(void** rdi, void** rsi) {
    int64_t r9_3;
    int64_t rax4;
    int64_t rax5;

    __asm__("cli ");
    if (!rsi) {
        return 2;
    } else {
        if (*reinterpret_cast<void***>(rsi)) {
            r9_3 = argmatch_die;
            rax4 = __xargmatch_internal(rdi, rsi, 0x1b9a0, 0x16520, 4, r9_3);
            *reinterpret_cast<int32_t*>(&rax5) = *reinterpret_cast<int32_t*>(0x16520 + rax4 * 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
            return rax5;
        } else {
            return 2;
        }
    }
}

int64_t fun_c763(void** rdi, void** rsi) {
    void** rax3;
    int64_t r9_4;
    int64_t rax5;
    int64_t rax6;
    int64_t r9_7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rsi || !*reinterpret_cast<void***>(rsi)) {
        rax3 = fun_3690("VERSION_CONTROL", rsi);
        if (!rax3 || !*reinterpret_cast<void***>(rax3)) {
            return 2;
        } else {
            r9_4 = argmatch_die;
            rax5 = __xargmatch_internal("$VERSION_CONTROL", rax3, 0x1b9a0, 0x16520, 4, r9_4);
            *reinterpret_cast<int32_t*>(&rax6) = *reinterpret_cast<int32_t*>(0x16520 + rax5 * 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
            return rax6;
        }
    } else {
        r9_7 = argmatch_die;
        rax8 = __xargmatch_internal(rdi, rsi, 0x1b9a0, 0x16520, 4, r9_7);
        *reinterpret_cast<int32_t*>(&rax9) = *reinterpret_cast<int32_t*>(0x16520 + rax8 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

struct s39 {
    unsigned char f0;
    unsigned char f1;
};

struct s39* fun_c803(struct s39* rdi) {
    uint32_t edx2;
    struct s39* rax3;
    struct s39* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s39*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s39*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s39*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_c863(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_3820(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

uint64_t fun_c893(uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t rcx4;
    uint64_t rdi5;
    uint64_t rax6;
    uint64_t r8_7;
    uint64_t rax8;
    uint64_t rdx9;
    uint64_t rax10;
    int64_t rdx11;

    __asm__("cli ");
    rcx4 = rdi;
    rdi5 = rdx;
    if (!rcx4) {
        *reinterpret_cast<int32_t*>(&rcx4) = 0x2000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
        if (rsi) {
            rcx4 = rsi;
            goto addr_c8ab_4;
        }
    }
    if (!rsi) {
        addr_c8ab_4:
        rax6 = rdi5;
        if (rcx4 <= rdi5) {
            rax6 = rcx4;
        }
    } else {
        r8_7 = rsi;
        rax8 = rcx4;
        while (rdx9 = rax8 % r8_7, !!rdx9) {
            rax8 = r8_7;
            r8_7 = rdx9;
        }
        rax10 = rsi * (rcx4 / r8_7);
        *reinterpret_cast<uint32_t*>(&rdx11) = __intrinsic();
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        if (rax10 > rdi5) 
            goto addr_c8ab_4;
        if (rdx11) 
            goto addr_c8ab_4; else 
            goto addr_c8f6_12;
    }
    return rax6;
    addr_c8f6_12:
    return rax10;
}

int64_t file_name = 0;

void fun_c913(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

void** stdin = reinterpret_cast<void**>(0);

int64_t freadahead(void** rdi);

int32_t rpl_fseeko(void** rdi);

int32_t rpl_fflush(void** rdi);

int32_t close_stream(void** rdi);

void close_stdout();

int32_t exit_failure = 1;

void** fun_36f0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void** quotearg_colon(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_c923() {
    void** rbp1;
    int64_t rax2;
    int32_t eax3;
    void** rdi4;
    int32_t eax5;
    int32_t eax6;
    void** rax7;
    int64_t r13_8;
    void** r12_9;
    void** rax10;
    int64_t rsi11;
    void** rcx12;
    int64_t rdx13;
    int64_t rdi14;
    void** r8_15;
    void** rax16;
    int32_t eax17;

    __asm__("cli ");
    rbp1 = stdin;
    rax2 = freadahead(rbp1);
    if (rax2) {
        eax3 = rpl_fseeko(rbp1);
        rdi4 = stdin;
        if (eax3 || (eax5 = rpl_fflush(rdi4), rdi4 = stdin, eax5 == 0)) {
            eax6 = close_stream(rdi4);
            if (!eax6) {
                addr_c949_4:
                goto close_stdout;
            } else {
                addr_c97f_5:
                rax7 = fun_3800();
                r13_8 = file_name;
                r12_9 = rax7;
                rax10 = fun_36d0();
                if (!r13_8) {
                    while (1) {
                        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                        rcx12 = r12_9;
                        rdx13 = reinterpret_cast<int64_t>("%s");
                        fun_3b80();
                        close_stdout();
                        addr_c9cf_7:
                        *reinterpret_cast<int32_t*>(&rdi14) = exit_failure;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                        rax10 = fun_36f0(rdi14, rsi11, rdx13, rcx12, r8_15);
                    }
                } else {
                    rax16 = quotearg_colon(r13_8, "error closing file", 5);
                    *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                    r8_15 = r12_9;
                    rcx12 = rax16;
                    rdx13 = reinterpret_cast<int64_t>("%s: %s");
                    fun_3b80();
                    close_stdout();
                    goto addr_c9cf_7;
                }
            }
        } else {
            close_stream(rdi4);
            goto addr_c97f_5;
        }
    } else {
        eax17 = close_stream(rbp1);
        if (eax17) 
            goto addr_c97f_5; else 
            goto addr_c949_4;
    }
}

int64_t file_name = 0;

void fun_ca23(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_ca33(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

void fun_ca43() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_36d0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_3800();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_cad3_5;
        rax10 = quotearg_colon(rdi9, "write error", 5);
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_3b80();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_36f0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_cad3_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_3b80();
    }
}

int64_t mdir_name();

void fun_caf3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mdir_name();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_cb13(void** rdi, void** rsi, void** rdx, void** rcx) {
    void* rbp5;
    void** rbx6;
    void** rax7;
    void* rax8;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp5) + 4) = 0;
    rbx6 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp5) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax7 = last_component(rdi, rsi, rdx, rcx);
    rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(rbx6));
    while (reinterpret_cast<uint64_t>(rax8) > reinterpret_cast<uint64_t>(rbp5) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(rax8) + 0xffffffffffffffff) == 47) {
        rax8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax8) + 0xffffffffffffffff);
    }
    return;
}

void** fun_cb53(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    void** rbx6;
    void** rax7;
    void** r12_8;
    void* rax9;
    uint32_t ebx10;
    void** rax11;
    void** r8_12;
    void** rax13;
    void** rax14;
    void** rax15;

    __asm__("cli ");
    rbp5 = rdi;
    *reinterpret_cast<int32_t*>(&rbx6) = 0;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx6) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax7 = last_component(rdi, rsi, rdx, rcx);
    r12_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(rbp5));
    while (reinterpret_cast<unsigned char>(rbx6) < reinterpret_cast<unsigned char>(r12_8)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r12_8) + 0xffffffffffffffff) != 47) 
            goto addr_cbd0_4;
        --r12_8;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_8) ^ 1);
    ebx10 = *reinterpret_cast<uint32_t*>(&rax9) & 1;
    rax11 = fun_3ab0(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<uint64_t>(rax9) + 1, rsi, rdx);
    if (!rax11) {
        addr_cbfd_7:
        *reinterpret_cast<int32_t*>(&r8_12) = 0;
        *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
    } else {
        rax13 = fun_3a80(rax11, rbp5, r12_8);
        r8_12 = rax13;
        if (!*reinterpret_cast<signed char*>(&ebx10)) {
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_cbbe_10;
        } else {
            *reinterpret_cast<void***>(rax13) = reinterpret_cast<void**>(46);
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_cbbe_10;
        }
    }
    addr_cbc3_12:
    return r8_12;
    addr_cbbe_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_12) + reinterpret_cast<unsigned char>(r12_8)) = 0;
    goto addr_cbc3_12;
    addr_cbd0_4:
    rax14 = fun_3ab0(r12_8 + 1, rsi, rdx);
    if (!rax14) 
        goto addr_cbfd_7;
    rax15 = fun_3a80(rax14, rbp5, r12_8);
    r8_12 = rax15;
    goto addr_cbbe_10;
}

unsigned char fun_cc13(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    void** rbx6;
    void** rax7;
    signed char* rbx8;
    int1_t zf9;

    __asm__("cli ");
    rax5 = last_component(rdi, rsi, rdx, rcx);
    rbx6 = rax5;
    if (!*reinterpret_cast<void***>(rax5)) {
        rbx6 = rdi;
    }
    rax7 = base_len(rbx6, rsi, rdx, rcx);
    rbx8 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<unsigned char>(rax7));
    zf9 = *rbx8 == 0;
    *rbx8 = 0;
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!zf9));
}

void fun_cc53() {
    __asm__("cli ");
}

int32_t fun_3a90(void** rdi);

void fun_cc63(void** rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_3a90(rdi);
        goto 0x3960;
    }
}

int32_t fun_3b90();

void fd_safer(int64_t rdi);

void fun_cc93() {
    void* rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_3b90();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax1) - reinterpret_cast<int64_t>(g28));
    if (rdx5) {
        fun_3840();
    } else {
        return;
    }
}

int64_t fun_3bd0();

int64_t fun_cd13(int32_t edi, int32_t esi, void** rdx, void*** rcx, int32_t r8d) {
    int64_t rax6;
    void** rax7;
    void** rax8;
    int64_t rdi9;

    __asm__("cli ");
    if (edi >= 0) {
        rax6 = fun_3bd0();
        if (*reinterpret_cast<int32_t*>(&rax6) != -1 || !rdx) {
            addr_cd45_3:
            if (*reinterpret_cast<int32_t*>(&rax6) != 1) {
                return rax6;
            }
        } else {
            rax7 = fun_36d0();
            if (*reinterpret_cast<void***>(rax7) != 38) {
                return 0xffffffff;
            }
        }
    } else {
        if (!rdx) {
            rax8 = fun_36d0();
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(9);
            return 0xffffffff;
        } else {
            *reinterpret_cast<int32_t*>(&rdi9) = esi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
            rax6 = fun_36b0(rdi9, rdx, rcx, rdi9, rdx, rcx);
            goto addr_cd45_3;
        }
    }
}

int32_t fun_3b00(void** rdi);

void fun_cdb3(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_3b00(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

void fun_ce03(int64_t rdi, void** rsi, void** rdx, struct s35* rcx) {
    void** rax5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        rax5 = xmalloc(24, rsi, rdx);
        rax6 = xstrdup(rsi, rsi);
        *reinterpret_cast<void***>(rax5) = rax6;
        *reinterpret_cast<void***>(rax5 + 8) = *reinterpret_cast<void***>(rdx + 8);
        *reinterpret_cast<void***>(rax5 + 16) = *reinterpret_cast<void***>(rdx);
        rax7 = hash_insert(rdi, rax5, rdx, rcx);
        if (!rax7) {
            xalloc_die();
        } else {
            if (rax5 == rax7) {
                return;
            }
        }
    }
}

void** fun_ce93(int64_t rdi, int64_t rsi, int64_t rdx) {
    void* rax4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax4 = g28;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
    if (rdi) {
        rax5 = hash_lookup(rdi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 40);
        *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(!!rax5);
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rdx6) {
        fun_3840();
    } else {
        return rax5;
    }
}

struct s40 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
    signed char f8;
    signed char f9;
    int16_t fa;
};

void fun_cef3(uint32_t edi, struct s40* rsi) {
    uint32_t eax3;
    int32_t ecx4;
    uint32_t esi5;
    uint32_t ecx6;
    uint32_t ecx7;
    uint32_t ecx8;
    uint32_t ecx9;
    uint32_t ecx10;
    uint32_t ecx11;
    uint32_t ecx12;
    uint32_t ecx13;
    uint32_t ecx14;
    uint32_t ecx15;
    uint32_t ecx16;
    uint32_t ecx17;
    uint32_t ecx18;
    uint32_t ecx19;
    uint32_t ecx20;
    uint32_t ecx21;
    uint32_t ecx22;
    uint32_t ecx23;
    uint32_t ecx24;
    uint32_t eax25;
    uint32_t eax26;

    __asm__("cli ");
    eax3 = edi;
    ecx4 = 45;
    esi5 = edi & reinterpret_cast<uint32_t>(fun_f000);
    if (esi5 != 0x8000 && ((ecx4 = 100, esi5 != 0x4000) && ((ecx4 = 98, esi5 != 0x6000) && ((ecx4 = 99, esi5 != 0x2000) && ((ecx4 = 0x6c, esi5 != 0xa000) && ((ecx4 = 0x70, esi5 != "_ctype_get_mb_cur_max") && (ecx4 = 0x73, esi5 != 0xc000))))))) {
        ecx4 = 63;
    }
    rsi->f0 = *reinterpret_cast<signed char*>(&ecx4);
    ecx6 = eax3 & 0x100;
    ecx7 = (ecx6 - (ecx6 + reinterpret_cast<uint1_t>(ecx6 < ecx6 + reinterpret_cast<uint1_t>(ecx6 < 1))) & 0xffffffbb) + 0x72;
    rsi->f1 = *reinterpret_cast<signed char*>(&ecx7);
    ecx8 = eax3 & 0x80;
    ecx9 = (ecx8 - (ecx8 + reinterpret_cast<uint1_t>(ecx8 < ecx8 + reinterpret_cast<uint1_t>(ecx8 < 1))) & 0xffffffb6) + 0x77;
    rsi->f2 = *reinterpret_cast<signed char*>(&ecx9);
    ecx10 = eax3 & 64;
    ecx11 = ecx10 - (ecx10 + reinterpret_cast<uint1_t>(ecx10 < ecx10 + reinterpret_cast<uint1_t>(ecx10 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 8)) {
        ecx12 = (ecx11 & 0xffffffb5) + 0x78;
    } else {
        ecx12 = (ecx11 & 0xffffffe0) + 0x73;
    }
    rsi->f3 = *reinterpret_cast<signed char*>(&ecx12);
    ecx13 = eax3 & 32;
    ecx14 = (ecx13 - (ecx13 + reinterpret_cast<uint1_t>(ecx13 < ecx13 + reinterpret_cast<uint1_t>(ecx13 < 1))) & 0xffffffbb) + 0x72;
    rsi->f4 = *reinterpret_cast<signed char*>(&ecx14);
    ecx15 = eax3 & 16;
    ecx16 = (ecx15 - (ecx15 + reinterpret_cast<uint1_t>(ecx15 < ecx15 + reinterpret_cast<uint1_t>(ecx15 < 1))) & 0xffffffb6) + 0x77;
    rsi->f5 = *reinterpret_cast<signed char*>(&ecx16);
    ecx17 = eax3 & 8;
    ecx18 = ecx17 - (ecx17 + reinterpret_cast<uint1_t>(ecx17 < ecx17 + reinterpret_cast<uint1_t>(ecx17 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 4)) {
        ecx19 = (ecx18 & 0xffffffb5) + 0x78;
    } else {
        ecx19 = (ecx18 & 0xffffffe0) + 0x73;
    }
    rsi->f6 = *reinterpret_cast<signed char*>(&ecx19);
    ecx20 = eax3 & 4;
    ecx21 = (ecx20 - (ecx20 + reinterpret_cast<uint1_t>(ecx20 < ecx20 + reinterpret_cast<uint1_t>(ecx20 < 1))) & 0xffffffbb) + 0x72;
    rsi->f7 = *reinterpret_cast<signed char*>(&ecx21);
    ecx22 = eax3 & 2;
    ecx23 = (ecx22 - (ecx22 + reinterpret_cast<uint1_t>(ecx22 < ecx22 + reinterpret_cast<uint1_t>(ecx22 < 1))) & 0xffffffb6) + 0x77;
    rsi->f8 = *reinterpret_cast<signed char*>(&ecx23);
    ecx24 = eax3 & 1;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 2)) {
        eax25 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffb5) + 0x78;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax25);
        rsi->fa = 32;
        return;
    } else {
        eax26 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffe0) + 0x74;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax26);
        rsi->fa = 32;
        return;
    }
}

void fun_d073(int64_t rdi) {
    __asm__("cli ");
    goto strmode;
}

int64_t mfile_name_concat();

void fun_d083() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mfile_name_concat();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_d0a3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    void** rax6;
    void** r14_7;
    void** rax8;
    void* rbx9;
    uint1_t zf10;
    int32_t eax11;
    unsigned char v12;
    int1_t zf13;
    int32_t eax14;
    void** rax15;
    void** rax16;
    uint32_t ecx17;
    void** rdi18;
    void** rax19;

    __asm__("cli ");
    rax5 = last_component(rdi, rsi, rdx, rcx);
    rax6 = base_len(rax5, rsi, rdx, rcx);
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(rdi)) + reinterpret_cast<unsigned char>(rax6));
    rax8 = fun_3820(rsi);
    if (!rax6) {
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        zf10 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rsi) == 47);
        eax11 = 46;
        if (!zf10) {
            eax11 = 0;
        }
        *reinterpret_cast<unsigned char*>(&rbx9) = zf10;
        v12 = *reinterpret_cast<unsigned char*>(&eax11);
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_7) + 0xffffffffffffffff) == 47) {
            v12 = 0;
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
            zf13 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 47);
            eax14 = 47;
            if (zf13) {
                eax14 = 0;
            }
            *reinterpret_cast<unsigned char*>(&rbx9) = reinterpret_cast<uint1_t>(!zf13);
            v12 = *reinterpret_cast<unsigned char*>(&eax14);
        }
    }
    rax15 = fun_3ab0(reinterpret_cast<unsigned char>(r14_7) + reinterpret_cast<unsigned char>(rax8) + 1 + reinterpret_cast<uint64_t>(rbx9), rsi, rdx);
    if (rax15) {
        rax16 = fun_3b70(rax15, rdi, r14_7);
        ecx17 = v12;
        rdi18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax16) + reinterpret_cast<uint64_t>(rbx9));
        *reinterpret_cast<void***>(rax16) = *reinterpret_cast<void***>(&ecx17);
        if (rdx) {
            *reinterpret_cast<void***>(rdx) = rdi18;
        }
        rax19 = fun_3b70(rdi18, rsi, rax8);
        *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    }
    return rax15;
}

struct s41 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    uint64_t f20;
    uint64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[8] pad88;
    int64_t f58;
};

int64_t fun_d1a3(struct s41* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    if (rdi->f28 <= rdi->f20 && (rax2 = rdi->f10 - rdi->f8, !!(rdi->f0 & 0x100))) {
        rax2 = rax2 + (rdi->f58 - rdi->f48);
    }
    return rax2;
}

int64_t fun_d1d3(void** rdi, int64_t rsi, int32_t edx) {
    void** rax4;
    int64_t rax5;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 72)))) {
        fun_3a90(rdi);
        rax4 = fun_38b0();
        if (rax4 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax5) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<void***>(rdi + 0x90) = rax4;
            *reinterpret_cast<uint32_t*>(&rax5) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
        return rax5;
    }
}

int64_t safe_write(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t fun_d253(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t r13_4;
    int32_t r12d5;
    int64_t rbp6;
    int64_t rbx7;
    int64_t rdi8;
    int64_t rax9;
    void** rax10;

    __asm__("cli ");
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
    } else {
        r12d5 = edi;
        rbp6 = rsi;
        rbx7 = rdx;
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&rdi8) = r12d5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
            rax9 = safe_write(rdi8, rbp6, rbx7);
            if (rax9 == -1) 
                break;
            if (!rax9) 
                goto addr_d2b0_6;
            r13_4 = r13_4 + rax9;
            rbp6 = rbp6 + rax9;
            rbx7 = rbx7 - rax9;
        } while (rbx7);
    }
    return r13_4;
    addr_d2b0_6:
    rax10 = fun_36d0();
    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(28);
    return r13_4;
}

uint64_t fun_d2d3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_d2f3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s42 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_d753(struct s42* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s43 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_d763(struct s43* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s44 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_d773(struct s44* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s47 {
    signed char[8] pad8;
    struct s47* f8;
};

struct s46 {
    int64_t f0;
    struct s47* f8;
};

struct s45 {
    struct s46* f0;
    struct s46* f8;
};

uint64_t fun_d783(struct s45* rdi) {
    struct s46* rcx2;
    struct s46* rsi3;
    uint64_t r8_4;
    struct s47* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s50 {
    signed char[8] pad8;
    struct s50* f8;
};

struct s49 {
    int64_t f0;
    struct s50* f8;
};

struct s48 {
    struct s49* f0;
    struct s49* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_d7e3(struct s48* rdi) {
    struct s49* rcx2;
    struct s49* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s50* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s53 {
    signed char[8] pad8;
    struct s53* f8;
};

struct s52 {
    int64_t f0;
    struct s53* f8;
};

struct s51 {
    struct s52* f0;
    struct s52* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_d853(struct s51* rdi, void** rsi) {
    void* v3;
    void* r12_4;
    uint64_t r12_5;
    int64_t v6;
    int64_t rbp7;
    void** rbp8;
    void** v9;
    void** rbx10;
    struct s52* rcx11;
    struct s52* rsi12;
    void** r8_13;
    void** rbx14;
    void** r13_15;
    struct s53* rax16;
    uint64_t rdx17;
    void** r9_18;
    void* v19;
    void** r9_20;
    void* v21;
    void** r9_22;
    void* v23;

    __asm__("cli ");
    v3 = r12_4;
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    v6 = rbp7;
    rbp8 = rsi;
    v9 = rbx10;
    rcx11 = rdi->f0;
    rsi12 = rdi->f8;
    r8_13 = rdi->f20;
    rbx14 = rdi->f10;
    r13_15 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx11) < reinterpret_cast<uint64_t>(rsi12)) {
        while (1) {
            if (!rcx11->f0) {
                ++rcx11;
                if (reinterpret_cast<uint64_t>(rsi12) <= reinterpret_cast<uint64_t>(rcx11)) 
                    break;
            } else {
                rax16 = rcx11->f8;
                *reinterpret_cast<int32_t*>(&rdx17) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
                if (rax16) {
                    do {
                        rax16 = rax16->f8;
                        ++rdx17;
                    } while (rax16);
                }
                if (r12_5 < rdx17) {
                    r12_5 = rdx17;
                }
                ++rcx11;
                if (reinterpret_cast<uint64_t>(rsi12) <= reinterpret_cast<uint64_t>(rcx11)) 
                    break;
            }
        }
    }
    fun_3c40(rbp8, 1, "# entries:         %lu\n", r8_13, r8_13, r9_18, v19, v9, v6, v3);
    fun_3c40(rbp8, 1, "# buckets:         %lu\n", rbx14, r8_13, r9_20, v21, v9, v6, v3);
    if (reinterpret_cast<signed char>(r13_15) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x8c8c]");
        if (reinterpret_cast<signed char>(rbx14) >= reinterpret_cast<signed char>(0)) {
            addr_d90a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_d989_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_3c40(rbp8, 1, "# buckets used:    %lu (%.2f%%)\n", r13_15, r8_13, r9_22, v23, v9, v6, v3);
        goto fun_3c40;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x8d0b]");
        if (reinterpret_cast<signed char>(rbx14) < reinterpret_cast<signed char>(0)) 
            goto addr_d989_14; else 
            goto addr_d90a_13;
    }
}

struct s54 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s55 {
    int64_t f0;
    struct s55* f8;
};

int64_t fun_d9b3(struct s54* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s54* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s55* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x3cdf;
    rbx7 = reinterpret_cast<struct s55*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_da18_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_da0b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_da0b_7;
    }
    addr_da1b_10:
    return r12_3;
    addr_da18_5:
    r12_3 = rbx7->f0;
    goto addr_da1b_10;
    addr_da0b_7:
    return 0;
}

struct s56 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_da33(struct s56* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x3ce4;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_da6f_7;
    return *rax2;
    addr_da6f_7:
    goto 0x3ce4;
}

struct s58 {
    int64_t f0;
    struct s58* f8;
};

struct s57 {
    int64_t* f0;
    struct s58* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_da83(struct s57* rdi, int64_t rsi) {
    struct s57* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s58* rax7;
    struct s58* rdx8;
    struct s58* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x3cea;
    rax7 = reinterpret_cast<struct s58*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_dace_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_dace_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_daec_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_daec_10:
    return r8_10;
}

struct s60 {
    int64_t f0;
    struct s60* f8;
};

struct s59 {
    struct s60* f0;
    struct s60* f8;
};

void fun_db13(struct s59* rdi, int64_t rsi, uint64_t rdx) {
    struct s60* r9_4;
    uint64_t rax5;
    struct s60* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_db52_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_db52_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s62 {
    int64_t f0;
    struct s62* f8;
};

struct s61 {
    struct s62* f0;
    struct s62* f8;
};

int64_t fun_db63(struct s61* rdi, int64_t rsi, int64_t rdx) {
    struct s62* r14_4;
    int64_t r12_5;
    struct s61* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s62* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_db8f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_dbd1_10;
            }
            addr_db8f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_db99_11:
    return r12_5;
    addr_dbd1_10:
    goto addr_db99_11;
}

uint64_t fun_dbe3(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s63 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_dc23(struct s63* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_dc53(uint64_t rdi, void** rsi, void** rdx, unsigned char rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    unsigned char rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    uint32_t esi12;
    void** rax13;
    void** rax14;
    void** rdi15;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0xd2d0);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<unsigned char>(0xd2f0);
    }
    rax9 = fun_3ab0(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0x165e0);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((esi12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), rax13 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&esi12)), *reinterpret_cast<void***>(r12_10 + 16) = rax13, rax13 == 0) || (rax14 = fun_39c0(rax13, 16), *reinterpret_cast<void***>(r12_10) = rax14, rax14 == 0))) {
            rdi15 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_3670(rdi15, rdi15);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<unsigned char*>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s66 {
    int64_t f0;
    struct s66* f8;
};

struct s65 {
    int64_t f0;
    struct s66* f8;
};

struct s64 {
    struct s65* f0;
    struct s65* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s66* f48;
};

void fun_dd53(struct s64* rdi) {
    struct s64* rbp2;
    struct s65* r12_3;
    struct s66* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s66* rax7;
    struct s66* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s67 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_de03(struct s67* rdi) {
    struct s67* r12_2;
    void** r13_3;
    void** rax4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** rdi8;
    void** rbx9;
    void** rbx10;
    void** rdi11;
    void** rdi12;

    __asm__("cli ");
    r12_2 = rdi;
    r13_3 = rdi->f0;
    rax4 = rdi->f8;
    rbp5 = r13_3;
    if (!rdi->f40 || !rdi->f20) {
        addr_de73_2:
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp5)) {
            do {
                rbx6 = *reinterpret_cast<void***>(rbp5 + 8);
                if (rbx6) {
                    do {
                        rdi7 = rbx6;
                        rbx6 = *reinterpret_cast<void***>(rbx6 + 8);
                        fun_3670(rdi7);
                    } while (rbx6);
                }
                rbp5 = rbp5 + 16;
            } while (reinterpret_cast<unsigned char>(r12_2->f8) > reinterpret_cast<unsigned char>(rbp5));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_3) < reinterpret_cast<unsigned char>(rax4)) {
            while (1) {
                rdi8 = *reinterpret_cast<void***>(r13_3);
                if (!rdi8) {
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                } else {
                    rbx9 = r13_3;
                    while (r12_2->f40(rdi8), rbx9 = *reinterpret_cast<void***>(rbx9 + 8), !!rbx9) {
                        rdi8 = *reinterpret_cast<void***>(rbx9);
                    }
                    rax4 = r12_2->f8;
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                }
            }
            rbp5 = r12_2->f0;
            goto addr_de73_2;
        }
    }
    rbx10 = r12_2->f48;
    if (rbx10) {
        do {
            rdi11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
            fun_3670(rdi11);
        } while (rbx10);
    }
    rdi12 = r12_2->f0;
    fun_3670(rdi12);
    goto fun_3670;
}

int64_t fun_def3(struct s16* rdi, uint64_t rsi) {
    struct s17* r12_3;
    void* rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    struct s16* r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = rdi->f28;
    rax4 = g28;
    esi5 = r12_3->f10;
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_e030_2;
    if (rdi->f10 == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_39c0(rax6, 16);
        if (!rax8) {
            addr_e030_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<struct s16*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8 - 8 + 8);
            v10 = rdi->f48;
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = rdi->f0;
                fun_3670(rdi12, rdi12);
                rdi->f0 = rax8;
                rdi->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                rdi->f10 = rax6;
                rdi->f18 = 0;
                rdi->f48 = v10;
            } else {
                rdi->f48 = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x3cef;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x3cef;
                fun_3670(rax8, rax8);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rax15) {
        fun_3840();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s68 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_e083(void** rdi, void** rsi, void*** rdx) {
    void* rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s68* v17;
    int32_t r8d18;
    void** rax19;
    void* rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x3cf4;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_e19e_5; else 
                goto addr_e10f_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_e10f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_e19e_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x8411]");
            if (!cf14) 
                goto addr_e1f5_12;
            __asm__("comiss xmm4, [rip+0x83c1]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x8380]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_e1f5_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x3cf4;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_3ab0(16, rsi, rdx6);
                if (!rax19) {
                    addr_e1f5_12:
                    r8d18 = -1;
                } else {
                    goto addr_e152_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_e152_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_e0ce_28:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rax20) {
        fun_3840();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_e152_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_e0ce_28;
}

int32_t hash_insert_if_absent();

int64_t fun_e2a3() {
    void* rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax1) - reinterpret_cast<int64_t>(g28));
    if (rdx6) {
        fun_3840();
    } else {
        return rax3;
    }
}

void** fun_e303(void** rdi, void** rsi) {
    void** rbx3;
    void* rax4;
    void* v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_e390_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_e446_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x822e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x8198]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_3670(rdi15, rdi15);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_e446_5; else 
                goto addr_e390_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v5) - reinterpret_cast<int64_t>(g28));
    if (rax16) {
        fun_3840();
    } else {
        return r12_7;
    }
}

void fun_e493() {
    __asm__("cli ");
    goto hash_remove;
}

struct s69 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_e4a3(struct s69* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

struct s70 {
    signed char[8] pad8;
    int64_t f8;
    int64_t f10;
};

struct s71 {
    signed char[8] pad8;
    int64_t f8;
    int64_t f10;
};

int64_t fun_e4c3(struct s70* rdi, struct s71* rsi) {
    __asm__("cli ");
    if (rdi->f8 != rsi->f8 || rdi->f10 != rsi->f10) {
        return 0;
    }
}

struct s72 {
    int64_t f0;
    uint64_t f8;
};

uint64_t hash_pjw(int64_t rdi);

uint64_t fun_e4f3(struct s72* rdi, int64_t rsi) {
    int64_t rdi3;
    uint64_t rax4;

    __asm__("cli ");
    rdi3 = rdi->f0;
    rax4 = hash_pjw(rdi3);
    return (rax4 ^ rdi->f8) % rsi;
}

struct s73 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s74 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_e523(struct s73* rdi, struct s74* rsi) {
    void** rdx3;
    void** rcx4;
    void** rsi5;
    void** rdi6;
    void** r8_7;
    int64_t rax8;

    __asm__("cli ");
    rdx3 = rsi->f8;
    if (rdi->f8 != rdx3 || (rcx4 = rsi->f10, rdi->f10 != rcx4)) {
        return 0;
    } else {
        rsi5 = rsi->f0;
        rdi6 = rdi->f0;
        rax8 = fun_39e0(rdi6, rsi5, rdx3, rcx4, r8_7);
        *reinterpret_cast<unsigned char*>(&rax8) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0);
        return rax8;
    }
}

void fun_e563(void*** rdi) {
    void** rdi2;

    __asm__("cli ");
    rdi2 = *rdi;
    fun_3670(rdi2, rdi2);
    goto fun_3670;
}

int32_t fun_3830();

void fun_e583() {
    void* rax1;
    unsigned char dl2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (dl2 & 64) {
    }
    eax3 = fun_3830();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax1) - reinterpret_cast<int64_t>(g28));
    if (rdx5) {
        fun_3840();
    } else {
        return;
    }
}

int64_t fun_3bc0(int64_t rdi, void** rsi, ...);

int64_t fun_e603(int64_t rdi, void** rsi, int32_t edx, void*** rcx) {
    int64_t r12_5;
    void** eax6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;
    int64_t rdi10;
    void** r13d11;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    eax6 = openat_safer(rdi, rsi);
    if (reinterpret_cast<signed char>(eax6) >= reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<void***>(&rdi7) = eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        rax8 = fun_3bc0(rdi7, rsi);
        r12_5 = rax8;
        if (!rax8) {
            rax9 = fun_36d0();
            *reinterpret_cast<void***>(&rdi10) = eax6;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            r13d11 = *reinterpret_cast<void***>(rax9);
            fun_3920(rdi10, rsi);
            *reinterpret_cast<void***>(rax9) = r13d11;
        } else {
            *rcx = eax6;
        }
    }
    return r12_5;
}

void fun_3c30(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s75 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s75* fun_3890();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_e663(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s75* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_3c30("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_36c0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_3890();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_36e0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

int32_t get_permissions();

int32_t set_permissions(void* rdi, int64_t rsi, int64_t rdx, void* rcx);

void free_permission_context(void* rdi, int64_t rsi, int64_t rdx, void* rcx);

int64_t fun_e713() {
    void* rax1;
    void* rbp2;
    int32_t eax3;
    int32_t r12d4;
    int64_t rdx5;
    int32_t ecx6;
    int64_t rdx7;
    int32_t eax8;
    void* rax9;
    int64_t rax10;

    __asm__("cli ");
    rax1 = g28;
    rbp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16 + 4);
    eax3 = get_permissions();
    if (eax3) {
        r12d4 = -2;
    } else {
        *reinterpret_cast<int32_t*>(&rdx5) = ecx6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
        eax8 = set_permissions(rbp2, rdx7, rdx5, rbp2);
        r12d4 = eax8;
        free_permission_context(rbp2, rdx7, rdx5, rbp2);
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax1) - reinterpret_cast<int64_t>(g28));
    if (rax9) {
        fun_3840();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = r12d4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

int64_t fun_e793(int64_t rdi, int32_t esi, int32_t edx, void* rcx) {
    void* rax5;
    void* rbp6;
    int64_t rdx7;
    int32_t eax8;
    void* rax9;
    int64_t rax10;

    __asm__("cli ");
    rax5 = g28;
    rbp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 24 + 4);
    *reinterpret_cast<int32_t*>(&rdx7) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
    eax8 = set_permissions(rbp6, rdi, rdx7, rcx);
    free_permission_context(rbp6, rdi, rdx7, rcx);
    rax9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<int64_t>(g28));
    if (rax9) {
        fun_3840();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = eax8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_fee3(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_36d0();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x1c6c0;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_ff23(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1c6c0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_ff43(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1c6c0);
    }
    *rdi = esi;
    return 0x1c6c0;
}

int64_t fun_ff63(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x1c6c0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s76 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_ffa3(struct s76* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s76*>(0x1c6c0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s77 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s77* fun_ffc3(struct s77* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s77*>(0x1c6c0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x3d03;
    if (!rdx) 
        goto 0x3d03;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x1c6c0;
}

struct s78 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_10003(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s78* r8) {
    struct s78* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s78*>(0x1c6c0);
    }
    rax7 = fun_36d0();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x10036);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s79 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_10083(int64_t rdi, int64_t rsi, void*** rdx, struct s79* rcx) {
    struct s79* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s79*>(0x1c6c0);
    }
    rax6 = fun_36d0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x100b1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x1010c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_10173() {
    __asm__("cli ");
}

void** g1c098 = reinterpret_cast<void**>(0xc0);

int64_t slotvec0 = 0x100;

void fun_10183() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_3670(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x1c5c0) {
        fun_3670(rdi7);
        g1c098 = reinterpret_cast<void**>(0x1c5c0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x1c090) {
        fun_3670(r12_2);
        slotvec = reinterpret_cast<void**>(0x1c090);
    }
    nslots = 1;
    return;
}

void fun_10223() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_10243() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_10253(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_10273(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_10293(void** rdi, int32_t esi, int64_t rdx) {
    void* rdx4;
    struct s20* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3d09;
    rcx5 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_3840();
    } else {
        return rax6;
    }
}

void** fun_10323(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s20* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x3d0e;
    rcx6 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx5) - reinterpret_cast<int64_t>(g28));
    if (rdx8) {
        fun_3840();
    } else {
        return rax7;
    }
}

void** fun_103b3(int32_t edi, int64_t rsi) {
    void* rax3;
    struct s20* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x3d13;
    rcx4 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax3) - reinterpret_cast<int64_t>(g28));
    if (rdx6) {
        fun_3840();
    } else {
        return rax5;
    }
}

void** fun_10443(int32_t edi, int64_t rsi, int64_t rdx) {
    void* rax4;
    struct s20* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x3d18;
    rcx5 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_3840();
    } else {
        return rax6;
    }
}

void** fun_104d3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s20* rsp4;
    void* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc1e0]");
    __asm__("movdqa xmm1, [rip+0xc1e8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xc1d1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<int64_t>(g28));
    if (rdx11) {
        fun_3840();
    } else {
        return rax10;
    }
}

void** fun_10573(int64_t rdi, uint32_t esi) {
    struct s20* rsp3;
    void* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc140]");
    __asm__("movdqa xmm1, [rip+0xc148]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xc131]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rdx10) {
        fun_3840();
    } else {
        return rax9;
    }
}

void** fun_10613(int64_t rdi) {
    void* rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc0a0]");
    __asm__("movdqa xmm1, [rip+0xc0a8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xc089]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax2) - reinterpret_cast<int64_t>(g28));
    if (rdx4) {
        fun_3840();
    } else {
        return rax3;
    }
}

void** fun_106a3(int64_t rdi, int64_t rsi) {
    void* rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc010]");
    __asm__("movdqa xmm1, [rip+0xc018]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xc006]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax3) - reinterpret_cast<int64_t>(g28));
    if (rdx5) {
        fun_3840();
    } else {
        return rax4;
    }
}

void** fun_10733(void** rdi, int32_t esi, int64_t rdx) {
    void* rdx4;
    struct s20* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3d1d;
    rcx5 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_3840();
    } else {
        return rax6;
    }
}

void** fun_107d3(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s20* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbeda]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xbed2]");
    __asm__("movdqa xmm2, [rip+0xbeda]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3d22;
    if (!rdx) 
        goto 0x3d22;
    rcx6 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx5) - reinterpret_cast<int64_t>(g28));
    if (rdx8) {
        fun_3840();
    } else {
        return rax7;
    }
}

void** fun_10873(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void* rcx6;
    struct s20* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbe3a]");
    __asm__("movdqa xmm1, [rip+0xbe42]");
    __asm__("movdqa xmm2, [rip+0xbe4a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3d27;
    if (!rdx) 
        goto 0x3d27;
    rcx7 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx6) - reinterpret_cast<int64_t>(g28));
    if (rdx10) {
        fun_3840();
    } else {
        return rax9;
    }
}

void** fun_10923(int64_t rdi, int64_t rsi, int64_t rdx) {
    void* rdx4;
    struct s20* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbd8a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xbd82]");
    __asm__("movdqa xmm2, [rip+0xbd8a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3d2c;
    if (!rsi) 
        goto 0x3d2c;
    rcx5 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_3840();
    } else {
        return rax6;
    }
}

void** fun_109c3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s20* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbcea]");
    __asm__("movdqa xmm1, [rip+0xbcf2]");
    __asm__("movdqa xmm2, [rip+0xbcfa]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3d31;
    if (!rsi) 
        goto 0x3d31;
    rcx6 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx5) - reinterpret_cast<int64_t>(g28));
    if (rdx8) {
        fun_3840();
    } else {
        return rax7;
    }
}

void fun_10a63() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_10a73(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_10a93() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_10ab3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int32_t fun_3af0();

int64_t fun_10ad3(int32_t edi, void** rsi, int32_t edx, void** rcx, int32_t r8d) {
    void* rax6;
    int32_t eax7;
    int32_t r12d8;
    void** rax9;
    void*** rsp10;
    void** r8_11;
    int64_t rax12;
    uint32_t edx13;
    uint32_t r9d14;
    void* rax15;
    int64_t rax16;
    void** rax17;
    void** rax18;
    uint32_t r9d19;
    void** rdi20;
    void** rdx21;
    void** eax22;
    void** rdx23;
    void** rdi24;
    void** eax25;
    uint32_t r9d26;
    void** rdi27;
    void** rdx28;
    void** eax29;
    uint32_t v30;
    uint32_t v31;
    int64_t rdi32;
    int32_t eax33;
    uint32_t v34;
    uint32_t v35;

    __asm__("cli ");
    rax6 = g28;
    eax7 = fun_3af0();
    r12d8 = eax7;
    if (eax7 >= 0 || (rax9 = fun_36d0(), rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8), r8_11 = rax9, *reinterpret_cast<void***>(&rax12) = *reinterpret_cast<void***>(rax9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0, edx13 = static_cast<uint32_t>(rax12 - 22) & 0xffffffef, *reinterpret_cast<unsigned char*>(&edx13) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!edx13)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax12) == 95)))), r9d14 = edx13, !!*reinterpret_cast<unsigned char*>(&edx13))) {
        addr_10b80_2:
        rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax6) - reinterpret_cast<int64_t>(g28));
        if (rax15) {
            fun_3840();
        } else {
            *reinterpret_cast<int32_t*>(&rax16) = r12d8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
            return rax16;
        }
    } else {
        if (!r8d) {
            addr_10bc6_6:
            rax17 = fun_3820(rsi, rsi);
            rax18 = fun_3820(rcx, rcx);
            rsp10 = rsp10 - 8 + 8 - 8 + 8;
            if (!rax17) 
                goto addr_10c05_7;
            r9d19 = *reinterpret_cast<unsigned char*>(&r9d14);
            if (!rax18) 
                goto addr_10c05_7;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rax17) + 0xffffffffffffffff) == 47) 
                goto addr_10c20_10;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rax18) + 0xffffffffffffffff) != 47) 
                goto addr_10c05_7;
        } else {
            if (r8d != 1) {
                *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(95);
                r12d8 = -1;
                goto addr_10b80_2;
            } else {
                *reinterpret_cast<int32_t*>(&rdi20) = edx;
                *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0;
                rdx21 = reinterpret_cast<void**>(rsp10 + 0xa0);
                eax22 = fun_3cb0(rdi20, rcx, rdx21, rdi20, rcx, rdx21);
                rsp10 = rsp10 - 8 + 8;
                r8_11 = r8_11;
                if (!eax22 || *reinterpret_cast<void***>(r8_11) == 75) {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(17);
                    r12d8 = -1;
                    goto addr_10b80_2;
                } else {
                    if (*reinterpret_cast<void***>(r8_11) != 2) 
                        goto addr_10b75_17;
                    r9d14 = 1;
                    goto addr_10bc6_6;
                }
            }
        }
    }
    addr_10c20_10:
    rdx23 = reinterpret_cast<void**>(rsp10 + 16);
    *reinterpret_cast<int32_t*>(&rdi24) = edi;
    *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
    eax25 = fun_3cb0(rdi24, rsi, rdx23, rdi24, rsi, rdx23);
    rsp10 = rsp10 - 8 + 8;
    if (eax25) {
        addr_10b75_17:
        r12d8 = -1;
        goto addr_10b80_2;
    } else {
        r9d26 = *reinterpret_cast<unsigned char*>(&r9d19);
        if (!*reinterpret_cast<signed char*>(&r9d26)) {
            *reinterpret_cast<int32_t*>(&rdi27) = edx;
            *reinterpret_cast<int32_t*>(&rdi27 + 4) = 0;
            rdx28 = reinterpret_cast<void**>(rsp10 + 0xa0);
            eax29 = fun_3cb0(rdi27, rcx, rdx28, rdi27, rcx, rdx28);
            if (!eax29) {
                if ((v30 & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                    if ((v31 & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                        addr_10c05_7:
                        *reinterpret_cast<int32_t*>(&rdi32) = edi;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
                        eax33 = fun_3c00(rdi32, rsi, rdi32, rsi);
                        r12d8 = eax33;
                        goto addr_10b80_2;
                    } else {
                        *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(21);
                        r12d8 = -1;
                        goto addr_10b80_2;
                    }
                } else {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(20);
                    goto addr_10b75_17;
                }
            } else {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r8_11) == 2) && (v34 & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) {
                    goto addr_10c05_7;
                }
            }
        } else {
            if ((v35 & reinterpret_cast<uint32_t>(fun_f000)) == 0x4000) 
                goto addr_10c05_7;
            *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(2);
            r12d8 = -1;
            goto addr_10b80_2;
        }
    }
}

int64_t fun_3790(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_10d33(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_3790(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_36d0();
        if (*reinterpret_cast<void***>(rax9) == 4) 
            continue;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 22)) 
            break;
        if (rbx6 <= 0x7ff00000) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

int64_t fun_10da3(int32_t edi, void** rsi, void** rdx, void** rcx) {
    void* rax5;
    void** rax6;
    void** rax7;
    void** rax8;
    void** rax9;
    uint32_t r9d10;
    uint32_t eax11;
    void** rax12;
    void* rsp13;
    void** rdx14;
    void** rdi15;
    void** rsi16;
    void** eax17;
    void* rsp18;
    void** rax19;
    void** rax20;
    void** rdi21;
    void** rdx22;
    void** eax23;
    int32_t r9d24;
    int64_t v25;
    int64_t v26;
    int64_t v27;
    int64_t v28;
    void* rax29;
    int64_t rax30;

    __asm__("cli ");
    rax5 = g28;
    rax6 = last_component(rsi, rsi, rdx, rcx);
    rax7 = last_component(rcx, rsi, rdx, rcx);
    rax8 = base_len(rax6, rsi, rdx, rcx);
    rax9 = base_len(rax7, rsi, rdx, rcx);
    r9d10 = 0;
    if (rax8 == rax9 && (eax11 = fun_3990(rax6, rax7, rax8), r9d10 = 0, !eax11)) {
        rax12 = dir_name(rsi, rax7, rsi, rax7);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rdx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 16);
        *reinterpret_cast<int32_t*>(&rdi15) = edi;
        *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
        rsi16 = rax12;
        eax17 = fun_3cb0(rdi15, rsi16, rdx14, rdi15, rsi16, rdx14);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
        if (eax17) {
            rax19 = fun_36d0();
            rsi16 = *reinterpret_cast<void***>(rax19);
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            fun_3b80();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
        }
        fun_3670(rax12, rax12);
        rax20 = dir_name(rcx, rsi16, rcx, rsi16);
        *reinterpret_cast<int32_t*>(&rdi21) = *reinterpret_cast<int32_t*>(&rdx);
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8 + 0xa0);
        eax23 = fun_3cb0(rdi21, rax20, rdx22, rdi21, rax20, rdx22);
        if (eax23) {
            fun_36d0();
            fun_3b80();
        }
        r9d24 = 0;
        if (v25 == v26) {
            *reinterpret_cast<unsigned char*>(&r9d24) = reinterpret_cast<uint1_t>(v27 == v28);
        }
        fun_3670(rax20, rax20);
        r9d10 = *reinterpret_cast<unsigned char*>(&r9d24);
    }
    rax29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<int64_t>(g28));
    if (rax29) {
        fun_3840();
    } else {
        *reinterpret_cast<uint32_t*>(&rax30) = r9d10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
        return rax30;
    }
}

void fun_10f43(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto same_nameat;
}

struct s80 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s81 {
    signed char[8] pad8;
    uint64_t f8;
};

int64_t fun_10f63(struct s80* rdi, struct s81* rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>(rdi->f8 > rsi->f8);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdi->f8 < rsi->f8)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

void fun_10f83(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_39e0;
}

void** xirealloc(void** rdi, void* rsi, void** rdx, void** rcx, void** r8);

void fun_3730(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void* fun_37f0(void* rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fun_10f93(int64_t rdi, int32_t esi) {
    void** rsi3;
    void* rax4;
    void* v5;
    void** v6;
    void** r14_7;
    int64_t rbp8;
    void* rbx9;
    void** r13_10;
    void** r15_11;
    void** rax12;
    void*** rsp13;
    void** r12_14;
    struct s37* rax15;
    void** r10_16;
    uint32_t eax17;
    uint32_t eax18;
    void** rax19;
    void*** rsp20;
    void** r10_21;
    void** r11_22;
    void** r8_23;
    void** rcx24;
    void** rax25;
    void** rdx26;
    void** rdi27;
    void** r11_28;
    struct s37* r9_29;
    void** rax30;
    void** rdx31;
    void** rax32;
    void* rax33;
    void** rdi34;
    void** rax35;
    void** rbx36;
    void* rbp37;
    void** rcx38;
    void* rbx39;
    void** r13_40;
    void** rbp41;
    void** rax42;
    void** rsi43;
    void* r12_44;
    void* rax45;
    void** rdi46;
    void** rax47;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi3) = esi;
    *reinterpret_cast<int32_t*>(&rsi3 + 4) = 0;
    rax4 = g28;
    v5 = rax4;
    v6 = *reinterpret_cast<void***>(0x1ba60 + reinterpret_cast<unsigned char>(rsi3) * 8);
    if (!rdi) {
        *reinterpret_cast<int32_t*>(&r14_7) = 0;
        *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
    } else {
        rbp8 = rdi;
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r13_10) = 0;
        *reinterpret_cast<int32_t*>(&r13_10 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_11) = 0;
        *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        rax12 = fun_36d0();
        rsp13 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72 - 8 + 8);
        *reinterpret_cast<int32_t*>(&r14_7) = 0;
        *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
        r12_14 = rax12;
        while (*reinterpret_cast<void***>(r12_14) = reinterpret_cast<void**>(0), rax15 = fun_3aa0(rbp8, rsi3, rbp8, rsi3), rsp13 = rsp13 - 8 + 8, !!rax15) {
            r10_16 = reinterpret_cast<void**>(&rax15->f13);
            eax17 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax15->f13));
            if (*reinterpret_cast<signed char*>(&eax17) != 46 || (eax17 = rax15->f14, *reinterpret_cast<signed char*>(&eax17) != 46)) {
                if (!*reinterpret_cast<signed char*>(&eax17)) 
                    continue;
            } else {
                eax18 = rax15->f15;
                if (!*reinterpret_cast<signed char*>(&eax18)) 
                    continue;
            }
            rax19 = fun_3820(r10_16, r10_16);
            rsp20 = rsp13 - 8 + 8;
            r10_21 = r10_16;
            r11_22 = rax19 + 1;
            if (!v6) {
                if (reinterpret_cast<signed char>(-static_cast<uint64_t>(rbx9)) <= reinterpret_cast<signed char>(r11_22)) {
                    *reinterpret_cast<int32_t*>(&r8_23) = 1;
                    *reinterpret_cast<int32_t*>(&r8_23 + 4) = 0;
                    rcx24 = reinterpret_cast<void**>(0x7ffffffffffffffe);
                    rax25 = xpalloc();
                    rsp20 = rsp20 - 8 + 8;
                    r10_21 = r10_21;
                    r11_22 = r11_22;
                    r14_7 = rax25;
                }
                rdx26 = r11_22;
                rdi27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_7) + reinterpret_cast<uint64_t>(rbx9));
                rsi3 = r10_21;
                fun_3a80(rdi27, rsi3, rdx26, rdi27, rsi3, rdx26);
                rsp13 = rsp20 - 8 + 8;
                r11_28 = r11_22;
            } else {
                r9_29 = rax15;
                if (!r13_10) {
                    rsi3 = reinterpret_cast<void**>(rsp20 + 48);
                    *reinterpret_cast<int32_t*>(&r8_23) = 16;
                    *reinterpret_cast<int32_t*>(&r8_23 + 4) = 0;
                    rcx24 = reinterpret_cast<void**>(0xffffffffffffffff);
                    rax30 = xpalloc();
                    rsp20 = rsp20 - 8 + 8;
                    r10_21 = r10_21;
                    r9_29 = rax15;
                    r11_22 = r11_22;
                    r15_11 = rax30;
                }
                rdx31 = r13_10;
                ++r13_10;
                rax32 = xstrdup(r10_21, rsi3);
                rsp13 = rsp20 - 8 + 8;
                rdx26 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx31) << 4) + reinterpret_cast<unsigned char>(r15_11));
                r11_28 = r11_22;
                *reinterpret_cast<void***>(rdx26) = rax32;
                *reinterpret_cast<void***>(rdx26 + 8) = r9_29->f0;
            }
            rbx9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx9) + reinterpret_cast<unsigned char>(r11_28));
        }
        if (*reinterpret_cast<void***>(r12_14)) 
            goto addr_11253_18; else 
            goto addr_110cc_19;
    }
    addr_11158_20:
    rax33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v5) - reinterpret_cast<int64_t>(g28));
    if (rax33) {
        fun_3840();
    } else {
        return r14_7;
    }
    addr_11253_18:
    fun_3670(r15_11, r15_11);
    rdi34 = r14_7;
    *reinterpret_cast<int32_t*>(&r14_7) = 0;
    *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
    fun_3670(rdi34, rdi34);
    goto addr_11158_20;
    addr_110cc_19:
    if (!v6) {
        if (!rbx9) {
            rax35 = xirealloc(r14_7, reinterpret_cast<uint64_t>(rbx9) + 1, rdx26, rcx24, r8_23);
            r14_7 = rax35;
        }
        rbx36 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx9) + reinterpret_cast<unsigned char>(r14_7));
    } else {
        rbp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx9) + 1);
        if (r13_10) {
            rcx38 = v6;
            *reinterpret_cast<int32_t*>(&rbx39) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx39) + 4) = 0;
            fun_3730(r15_11, r13_10, 16, rcx38, r8_23);
            r13_40 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_10) << 4) + reinterpret_cast<unsigned char>(r15_11));
            rbp41 = r15_11;
            rax42 = ximalloc(rbp37, r13_10, 16, rcx38, r8_23);
            r14_7 = rax42;
            do {
                rsi43 = *reinterpret_cast<void***>(rbp41);
                r12_44 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_7) + reinterpret_cast<uint64_t>(rbx39));
                rbp41 = rbp41 + 16;
                rax45 = fun_37f0(r12_44, rsi43, 16, rcx38, r8_23);
                rdi46 = *reinterpret_cast<void***>(rbp41 + 0xfffffffffffffff0);
                rbx39 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx39) + (reinterpret_cast<int64_t>(rax45) - reinterpret_cast<uint64_t>(r12_44)) + 1);
                fun_3670(rdi46, rdi46);
            } while (r13_40 != rbp41);
            rbx36 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx39) + reinterpret_cast<unsigned char>(r14_7));
        } else {
            rax47 = ximalloc(rbp37, rsi3, rdx26, rcx24, r8_23);
            r14_7 = rax47;
            rbx36 = rax47;
        }
        fun_3670(r15_11, r15_11);
    }
    *reinterpret_cast<void***>(rbx36) = reinterpret_cast<void**>(0);
    goto addr_11158_20;
}

int64_t opendir_safer();

void** streamsavedir(int64_t rdi, void** rsi);

void** fun_11283() {
    int64_t rax1;
    void** rsi2;
    int32_t esi3;
    void** rax4;
    void** rdx5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    rax1 = opendir_safer();
    if (!rax1) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rsi2) = esi3;
        *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
        rax4 = streamsavedir(rax1, rsi2);
        eax7 = fun_3950(rax1, rsi2, rdx5, rcx6);
        if (eax7) {
            fun_3670(rax4, rax4);
            return 0;
        } else {
            return rax4;
        }
    }
}

unsigned char fun_112f3(int32_t edi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(edi != -1));
}

struct s82 {
    unsigned char f0;
    unsigned char f1;
};

int64_t fun_11303(struct s82* rdi) {
    uint32_t eax2;
    struct s82* rcx3;
    struct s82* rdx4;
    uint32_t eax5;

    __asm__("cli ");
    eax2 = rdi->f0;
    rcx3 = rdi;
    do {
        rdx4 = reinterpret_cast<struct s82*>(&rcx3->f1);
        if (*reinterpret_cast<signed char*>(&eax2) != 46) 
            goto addr_1133c_3;
        eax5 = rcx3->f1;
        if (*reinterpret_cast<signed char*>(&eax5) != 47) 
            break;
        while (eax2 = rdx4->f1, rcx3 = reinterpret_cast<struct s82*>(&rdx4->f1), *reinterpret_cast<signed char*>(&eax2) == 47) {
            rdx4 = rcx3;
        }
    } while (*reinterpret_cast<signed char*>(&eax2));
    goto addr_11350_8;
    if (!*reinterpret_cast<signed char*>(&eax5)) {
        addr_11350_8:
        return 0xffffff9c;
    } else {
        addr_1133c_3:
        goto fun_3b90;
    }
}

int64_t fun_11363(void** rdi) {
    void* rax2;
    void** rdx3;
    void** rcx4;
    int32_t eax5;
    void** rax6;
    int64_t rax7;
    int32_t eax8;
    void* rdx9;

    __asm__("cli ");
    rax2 = g28;
    eax5 = fun_3980(rdi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0, rdx3, rcx4);
    rax6 = fun_36d0();
    if (!eax5 || *reinterpret_cast<void***>(rax6) == 75) {
        *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(17);
        *reinterpret_cast<int32_t*>(&rax7) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    } else {
        eax8 = 0;
        *reinterpret_cast<unsigned char*>(&eax8) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 2));
        *reinterpret_cast<int32_t*>(&rax7) = -eax8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    }
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax2) - reinterpret_cast<int64_t>(g28));
    if (rdx9) {
        fun_3840();
    } else {
        return rax7;
    }
}

void fun_113e3() {
    __asm__("cli ");
}

void fun_113f3() {
    __asm__("cli ");
    goto fun_3b90;
}

void* fun_3940(void* rdi, int64_t rsi);

int64_t fun_3c50(void* rdi, int64_t rsi, void** rdx);

void fun_3780(int64_t rdi, void* rsi, void** rdx);

int64_t fun_11413(void** rdi, int32_t esi, int64_t rdx, int64_t rcx, void* r8) {
    int64_t v6;
    void** v7;
    int64_t v8;
    void* v9;
    void* rax10;
    void* v11;
    void** rax12;
    void** rsp13;
    void** v14;
    void** v15;
    void** v16;
    void** rax17;
    void** rdx18;
    void* rax19;
    void* r15_20;
    void* v21;
    void* v22;
    void* rax23;
    void** rsp24;
    uint32_t r9d25;
    int64_t rax26;
    int32_t v27;
    unsigned char v28;
    int32_t v29;
    void* rdx30;
    int64_t rax31;
    void** r13_32;
    uint64_t v33;
    int32_t r8d34;
    uint32_t r9d35;
    void** rcx36;
    void* rbx37;
    uint64_t rax38;
    uint32_t eax39;
    void* v40;
    void** v41;

    __asm__("cli ");
    v6 = rdx;
    v7 = rdi;
    v8 = rcx;
    v9 = r8;
    rax10 = g28;
    v11 = rax10;
    rax12 = fun_36d0();
    rsp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88 - 8 + 8);
    v14 = rax12;
    v15 = *reinterpret_cast<void***>(rax12);
    v16 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 + 80) >> 4);
    rax17 = fun_3820(rdi);
    rdx18 = reinterpret_cast<void**>(esi + reinterpret_cast<uint64_t>(r8));
    if (reinterpret_cast<unsigned char>(rdx18) > reinterpret_cast<unsigned char>(rax17) || (rax19 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax17) - reinterpret_cast<unsigned char>(rdx18)), r15_20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax19)), v21 = rax19, v22 = r15_20, rax23 = fun_3940(r15_20, "X"), rsp24 = rsp13 - 8 + 8 - 8 + 8, r9d25 = reinterpret_cast<uint1_t>(rcx == 0x11360), reinterpret_cast<uint64_t>(rax23) < reinterpret_cast<uint64_t>(r8))) {
        *reinterpret_cast<void***>(v14) = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax26) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    } else {
        v27 = 0x3a2f8;
        v28 = *reinterpret_cast<unsigned char*>(&r9d25);
        v29 = 0;
        goto addr_11500_4;
    }
    addr_11649_5:
    rdx30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v11) - reinterpret_cast<int64_t>(g28));
    if (rdx30) {
        fun_3840();
    } else {
        return rax26;
    }
    addr_1166b_8:
    *reinterpret_cast<void***>(v14) = v15;
    goto addr_11649_5;
    addr_11644_9:
    *reinterpret_cast<int32_t*>(&rax26) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    goto addr_11649_5;
    while (1) {
        *reinterpret_cast<int32_t*>(&rdx18) = 1;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        rax31 = fun_3c50(rsp24 + 88, 8, 1);
        rsp24 = rsp24 - 8 + 8;
        if (rax31 != 8) {
            while (1) {
                fun_3780(1, rsp24 + 96, rdx18);
                rsp24 = rsp24 - 8 + 8;
                r13_32 = reinterpret_cast<void**>((v33 ^ reinterpret_cast<unsigned char>(r13_32)) * 0x27bb2ee687b0b0fd + 0xb504f32d);
                if (reinterpret_cast<unsigned char>(r13_32) > reinterpret_cast<unsigned char>(0xf49998db0aa753ff)) 
                    break;
                addr_115f8_12:
                r8d34 = 9;
                r9d35 = 1;
                while (1) {
                    rcx36 = r13_32;
                    rbx37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx37) + 1);
                    rdx18 = reinterpret_cast<void**>(__intrinsic() >> 4);
                    v16 = rdx18;
                    r13_32 = rdx18;
                    rax38 = reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx18) << 5) - reinterpret_cast<unsigned char>(rdx18);
                    eax39 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") + (reinterpret_cast<unsigned char>(rcx36) - (rax38 + rax38)));
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx37) + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&eax39);
                    if (rbx37 == v40) {
                        v28 = *reinterpret_cast<unsigned char*>(&r9d35);
                        v29 = r8d34;
                        do {
                            rax26 = reinterpret_cast<int64_t>(v8(v7, v6));
                            rsp24 = rsp24 - 8 + 8;
                            if (*reinterpret_cast<int32_t*>(&rax26) >= 0) 
                                goto addr_1166b_8;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v14) == 17)) 
                                goto addr_11644_9;
                            --v27;
                            if (!v27) 
                                goto addr_11644_9;
                            addr_11500_4:
                        } while (!v9);
                        r13_32 = v16;
                        rbx37 = v22;
                        r9d35 = v28;
                        v40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) + reinterpret_cast<uint64_t>(v9) + reinterpret_cast<uint64_t>(v21));
                        r8d34 = v29;
                    }
                    if (!r8d34) 
                        break;
                    --r8d34;
                }
                if (*reinterpret_cast<unsigned char*>(&r9d35)) 
                    break;
            }
        } else {
            r13_32 = v41;
            if (reinterpret_cast<unsigned char>(r13_32) > reinterpret_cast<unsigned char>(0xf49998db0aa753ff)) 
                continue;
            goto addr_115f8_12;
        }
    }
}

int32_t fun_11693(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    eax2 = try_tempname_len(rdi);
    return eax2;
}

int32_t fun_116c3(void** rdi) {
    void* rax2;
    int32_t eax3;
    void* rdx4;

    __asm__("cli ");
    rax2 = g28;
    eax3 = try_tempname_len(rdi);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax2) - reinterpret_cast<int64_t>(g28));
    if (rdx4) {
        fun_3840();
    } else {
        return eax3;
    }
}

void fun_11723() {
    __asm__("cli ");
    goto try_tempname_len;
}

int32_t dup_safer();

int64_t fun_11733(uint32_t edi, void** rsi) {
    int32_t eax3;
    void** rax4;
    int64_t rdi5;
    void** r13d6;
    int64_t rax7;
    int64_t rax8;

    __asm__("cli ");
    if (edi <= 2) {
        eax3 = dup_safer();
        rax4 = fun_36d0();
        *reinterpret_cast<uint32_t*>(&rdi5) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        r13d6 = *reinterpret_cast<void***>(rax4);
        fun_3920(rdi5, rsi);
        *reinterpret_cast<int32_t*>(&rax7) = eax3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        *reinterpret_cast<void***>(rax4) = r13d6;
        return rax7;
    } else {
        *reinterpret_cast<uint32_t*>(&rax8) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
}

uint64_t fun_11793(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_117a3(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

/* ht.1 */
int64_t ht_1 = 0;

/* new_dst_res.0 */
void** new_dst_res_0 = reinterpret_cast<void**>(0);

int64_t fun_117b3(int32_t edi, void** rsi, void** rdx, struct s35* rcx, uint32_t r8d) {
    void* rsp6;
    uint64_t r14_7;
    uint64_t r13_8;
    int64_t r9_9;
    int64_t r10_10;
    int32_t v11;
    int32_t r11d12;
    void* rax13;
    void* v14;
    unsigned char bpl15;
    void** r8d16;
    int32_t r12d17;
    void* eax18;
    int64_t r12_19;
    void* rax20;
    int32_t eax21;
    void** v22;
    int64_t r8_23;
    void** r15_24;
    void** rbx25;
    int64_t rax26;
    void** rsi27;
    int64_t rax28;
    void** rax29;
    int64_t rdi30;
    void** rax31;
    void** rax32;
    int1_t zf33;
    void** rdx34;
    void** rdx35;
    void** rax36;
    void** eax37;
    int1_t zf38;
    void** v39;
    void** rax40;
    void** rdx41;
    uint64_t rdi42;
    int64_t rcx43;
    uint64_t v44;
    void** v45;
    uint64_t v46;
    int64_t rdi47;
    int64_t rsi48;
    int64_t rbx49;
    int32_t eax50;
    int64_t rax51;
    int64_t rax52;
    int32_t esi53;
    uint32_t edx54;
    int64_t rdi55;
    int32_t ecx56;
    int32_t eax57;
    int32_t ecx58;
    int32_t esi59;
    void** v60;
    uint64_t rax61;
    int64_t r8_62;
    int64_t r8_63;
    int32_t r8d64;
    int64_t rax65;
    int64_t rax66;
    int32_t r8d67;
    int64_t rax68;
    int64_t rax69;
    int32_t eax70;
    int32_t v71;
    int32_t edx72;
    int64_t rbx73;
    uint64_t v74;
    int64_t v75;
    void** r9d76;
    uint64_t v77;
    int32_t r13d78;
    void* edi79;
    void** r8d80;
    void** edi81;
    int64_t rax82;
    int64_t rax83;
    int64_t rax84;
    int64_t rax85;
    int64_t rax86;
    int32_t eax87;
    int64_t rax88;
    int64_t rax89;
    int32_t eax90;
    int64_t rax91;
    int64_t rax92;
    int32_t eax93;
    int64_t rdi94;
    void*** rbx95;
    int64_t rax96;
    void** rdi97;
    int64_t v98;
    void** rdx99;
    int64_t v100;
    int32_t v101;
    void** eax102;
    uint64_t rax103;
    uint64_t v104;
    uint64_t rsi105;
    uint64_t v106;
    int64_t rdi107;
    uint64_t v108;
    uint64_t v109;
    uint32_t edx110;
    void** esi111;
    int32_t ecx112;
    int64_t rax113;
    uint32_t eax114;
    void* r12d115;
    uint64_t rax116;

    __asm__("cli ");
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x138);
    r14_7 = *reinterpret_cast<uint64_t*>(rdx + 88);
    r13_8 = rcx->f58;
    r9_9 = rcx->f60;
    r10_10 = *reinterpret_cast<int64_t*>(rdx + 96);
    v11 = edi;
    r11d12 = *reinterpret_cast<int32_t*>(&r9_9);
    rax13 = g28;
    v14 = rax13;
    bpl15 = reinterpret_cast<uint1_t>(r14_7 == r13_8);
    r8d16 = reinterpret_cast<void**>(r8d & 1);
    if (!r8d16) {
        while (1) {
            addr_118e0_2:
            r12d17 = 0;
            *reinterpret_cast<unsigned char*>(&r12d17) = reinterpret_cast<uint1_t>(r11d12 < *reinterpret_cast<int32_t*>(&r10_10));
            eax18 = reinterpret_cast<void*>(0);
            *reinterpret_cast<unsigned char*>(&eax18) = reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(r13_8) < reinterpret_cast<int64_t>(r14_7));
            *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>((r12d17 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(r11d12 > *reinterpret_cast<int32_t*>(&r10_10))) & -static_cast<uint32_t>(bpl15)) + (reinterpret_cast<int32_t>(eax18) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(r13_8) > reinterpret_cast<int64_t>(r14_7)))));
            addr_1190f_3:
            rax20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v14) - reinterpret_cast<int64_t>(g28));
            if (!rax20) 
                break;
            fun_3840();
            addr_11fcb_5:
            *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(10);
            addr_11dab_6:
            eax21 = 0;
            *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(&r12_19) == 0x77359400);
            r13_8 = r13_8 & reinterpret_cast<uint64_t>(static_cast<int64_t>(~eax21));
            __asm__("cdq ");
            r11d12 = *reinterpret_cast<int32_t*>(&r9_9) - r11d12 % reinterpret_cast<signed char>(*reinterpret_cast<void***>(&r12_19));
            bpl15 = reinterpret_cast<uint1_t>(r14_7 == r13_8);
            addr_11dd8_7:
            *reinterpret_cast<void***>(v22 + 8) = *reinterpret_cast<void***>(&r12_19);
            *reinterpret_cast<signed char*>(v22 + 12) = 1;
        }
    } else {
        if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r10_10) == *reinterpret_cast<int32_t*>(&r9_9))) & bpl15) {
            *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0);
            goto addr_1190f_3;
        }
        if (reinterpret_cast<int64_t>(r13_8 + 0xffffffffffffffff) > reinterpret_cast<int64_t>(r14_7)) 
            goto addr_11948_11;
        *reinterpret_cast<void***>(&r12_19) = r8d16;
        if (reinterpret_cast<int64_t>(r14_7 + 0xffffffffffffffff) > reinterpret_cast<int64_t>(r13_8)) 
            goto addr_1190f_3;
        r8_23 = ht_1;
        r15_24 = rsi;
        rbx25 = rdx;
        if (r8_23) 
            goto addr_11846_14;
        rcx = reinterpret_cast<struct s35*>(0x117a0);
        rdx = reinterpret_cast<void**>(0x11790);
        rax26 = hash_initialize(16);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
        r11d12 = *reinterpret_cast<int32_t*>(&r9_9);
        r10_10 = r10_10;
        ht_1 = rax26;
        r9_9 = r9_9;
        r8_23 = rax26;
        if (!rax26) 
            goto addr_119a2_16;
        addr_11846_14:
        rsi27 = new_dst_res_0;
        if (!rsi27) 
            goto addr_11e50_17; else 
            goto addr_11856_18;
    }
    *reinterpret_cast<void***>(&rax28) = *reinterpret_cast<void***>(&r12_19);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
    return rax28;
    addr_11e50_17:
    rax29 = fun_3ab0(16, rsi27, rdx, 16, rsi27, rdx);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
    r11d12 = r11d12;
    r10_10 = r10_10;
    new_dst_res_0 = rax29;
    r9_9 = r9_9;
    rsi27 = rax29;
    if (!rax29) {
        rdi30 = ht_1;
        goto addr_11e00_21;
    } else {
        *reinterpret_cast<void***>(rax29 + 8) = reinterpret_cast<void**>(0x77359400);
        r8_23 = r8_23;
        *reinterpret_cast<signed char*>(rax29 + 12) = 0;
    }
    addr_11856_18:
    rax31 = *reinterpret_cast<void***>(rbx25);
    *reinterpret_cast<void***>(rsi27) = rax31;
    rax32 = hash_insert(r8_23, rsi27, rdx, rcx);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
    r11d12 = r11d12;
    r10_10 = r10_10;
    v22 = rax32;
    r9_9 = r9_9;
    if (rax32) {
        zf33 = new_dst_res_0 == rax32;
        if (zf33) {
            new_dst_res_0 = reinterpret_cast<void**>(0);
            goto addr_1189d_25;
        }
    }
    rdi30 = ht_1;
    if (!rdi30) {
        addr_119a2_16:
        rdx34 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 96);
    } else {
        addr_11e00_21:
        rdx35 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 96);
        rax36 = hash_lookup(rdi30, rdx35);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
        rdx34 = rdx35;
        r11d12 = r11d12;
        v22 = rax36;
        r10_10 = r10_10;
        r9_9 = r9_9;
        if (rax36) {
            addr_1189d_25:
            eax37 = *reinterpret_cast<void***>(v22 + 8);
            zf38 = *reinterpret_cast<signed char*>(v22 + 12) == 0;
            v39 = eax37;
            if (zf38) {
                addr_119c1_27:
                rax40 = *reinterpret_cast<void***>(rbx25 + 72);
                rdx41 = *reinterpret_cast<void***>(rbx25 + 80);
                rdi42 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r10_10)));
                rcx43 = *reinterpret_cast<int64_t*>(rbx25 + 0x70);
                v44 = rdi42;
                v45 = rax40;
                v46 = *reinterpret_cast<uint64_t*>(rbx25 + 0x68);
                rdi47 = reinterpret_cast<int64_t>(rdi42 * 0x66666667) >> 34;
                rsi48 = *reinterpret_cast<int32_t*>(&rdx41) * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rbx49) = *reinterpret_cast<int32_t*>(&rsi48) - (*reinterpret_cast<int32_t*>(&rdx41) >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx49) + 4) = 0;
                eax50 = static_cast<int32_t>(rbx49 + rbx49 * 4);
                rax51 = *reinterpret_cast<int32_t*>(&rcx43) * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax52) = *reinterpret_cast<int32_t*>(&rax51) - (*reinterpret_cast<int32_t*>(&rcx43) >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax52) + 4) = 0;
                esi53 = static_cast<int32_t>(rax52 + rax52 * 4);
                edx54 = *reinterpret_cast<int32_t*>(&rdx41) - reinterpret_cast<uint32_t>(eax50 + eax50) | *reinterpret_cast<int32_t*>(&rcx43) - reinterpret_cast<uint32_t>(esi53 + esi53);
                *reinterpret_cast<int32_t*>(&rdi55) = *reinterpret_cast<int32_t*>(&rdi47) - (*reinterpret_cast<int32_t*>(&r10_10) >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
                ecx56 = static_cast<int32_t>(rdi55 + rdi55 * 4);
                if (*reinterpret_cast<int32_t*>(&r10_10) - reinterpret_cast<uint32_t>(ecx56 + ecx56) | edx54) 
                    goto addr_11dd8_7; else 
                    goto addr_11a49_28;
            } else {
                eax57 = 0;
                *reinterpret_cast<unsigned char*>(&eax57) = reinterpret_cast<uint1_t>(eax37 == 0x77359400);
                r13_8 = r13_8 & reinterpret_cast<uint64_t>(static_cast<int64_t>(~eax57));
                __asm__("cdq ");
                r11d12 = *reinterpret_cast<int32_t*>(&r9_9) - r11d12 % reinterpret_cast<signed char>(eax37);
                bpl15 = reinterpret_cast<uint1_t>(r14_7 == r13_8);
                goto addr_118e0_2;
            }
        }
    }
    v39 = reinterpret_cast<void**>(0x77359400);
    v22 = rdx34;
    goto addr_119c1_27;
    addr_11a49_28:
    ecx58 = *reinterpret_cast<int32_t*>(&rax52);
    esi59 = *reinterpret_cast<int32_t*>(&rdi55);
    if (reinterpret_cast<signed char>(v39) <= reinterpret_cast<signed char>(10)) {
        v60 = reinterpret_cast<void**>(10);
        *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(10);
        rax61 = r13_8;
    } else {
        __asm__("cdq ");
        v60 = reinterpret_cast<void**>(10);
        r8_62 = *reinterpret_cast<int32_t*>(&rax52) * 0x66666667 >> 34;
        *reinterpret_cast<uint32_t*>(&r8_63) = *reinterpret_cast<int32_t*>(&r8_62) - edx54;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_63) + 4) = 0;
        r8d64 = static_cast<int32_t>(r8_63 + r8_63 * 4);
        rax65 = *reinterpret_cast<int32_t*>(&rbx49) * 0x66666667 >> 34;
        *reinterpret_cast<int32_t*>(&rax66) = *reinterpret_cast<int32_t*>(&rax65) - (*reinterpret_cast<int32_t*>(&rbx49) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax66) + 4) = 0;
        r8d67 = static_cast<int32_t>(rax66 + rax66 * 4);
        rax68 = *reinterpret_cast<int32_t*>(&rdi55) * 0x66666667 >> 34;
        *reinterpret_cast<int32_t*>(&rax69) = *reinterpret_cast<int32_t*>(&rax68) - (*reinterpret_cast<int32_t*>(&rdi55) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
        eax70 = static_cast<int32_t>(rax69 + rax69 * 4);
        if (*reinterpret_cast<int32_t*>(&rdi55) - reinterpret_cast<uint32_t>(eax70 + eax70) | (*reinterpret_cast<int32_t*>(&rax52) - reinterpret_cast<uint32_t>(r8d64 + r8d64) | *reinterpret_cast<int32_t*>(&rbx49) - reinterpret_cast<uint32_t>(r8d67 + r8d67))) {
            *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(10);
            rax61 = r13_8;
        } else {
            v71 = r11d12;
            edx72 = *reinterpret_cast<int32_t*>(&rbx49);
            *reinterpret_cast<void***>(&rbx73) = reinterpret_cast<void**>(10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx73) + 4) = 0;
            v74 = r14_7;
            v75 = r9_9;
            r9d76 = v39;
            v77 = r13_8;
            r13d78 = 8;
            do {
                edi79 = reinterpret_cast<void*>(static_cast<uint32_t>(rbx73 + rbx73 * 4));
                r8d80 = *reinterpret_cast<void***>(&rbx73);
                edi81 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(edi79) + reinterpret_cast<uint32_t>(edi79));
                *reinterpret_cast<void***>(&rbx73) = edi81;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx73) + 4) = 0;
                rax82 = edx72 * 0x66666667 >> 34;
                edx72 = *reinterpret_cast<int32_t*>(&rax82) - (edx72 >> 31);
                rax83 = ecx58 * 0x66666667 >> 34;
                ecx58 = *reinterpret_cast<int32_t*>(&rax83) - (ecx58 >> 31);
                rax84 = esi59 * 0x66666667 >> 34;
                esi59 = *reinterpret_cast<int32_t*>(&rax84) - (esi59 >> 31);
                if (reinterpret_cast<signed char>(edi81) >= reinterpret_cast<signed char>(r9d76)) 
                    goto addr_11ee0_37;
                rax85 = edx72 * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax86) = *reinterpret_cast<int32_t*>(&rax85) - (edx72 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax86) + 4) = 0;
                eax87 = static_cast<int32_t>(rax86 + rax86 * 4);
                rax88 = ecx58 * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax89) = *reinterpret_cast<int32_t*>(&rax88) - (ecx58 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
                eax90 = static_cast<int32_t>(rax89 + rax89 * 4);
                rax91 = esi59 * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax92) = *reinterpret_cast<int32_t*>(&rax91) - (esi59 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax92) + 4) = 0;
                eax93 = static_cast<int32_t>(rax92 + rax92 * 4);
                if (esi59 - reinterpret_cast<uint32_t>(eax93 + eax93) | (edx72 - reinterpret_cast<uint32_t>(eax87 + eax87) | ecx58 - reinterpret_cast<uint32_t>(eax90 + eax90))) 
                    goto addr_11f20_39;
                --r13d78;
            } while (r13d78);
            goto addr_11bd1_41;
        }
    }
    addr_11c11_42:
    if (reinterpret_cast<int64_t>(r14_7) > reinterpret_cast<int64_t>(r13_8)) 
        goto addr_1190f_3;
    if (*reinterpret_cast<int32_t*>(&r10_10) < *reinterpret_cast<int32_t*>(&r9_9)) 
        goto addr_11c28_44;
    if (bpl15) 
        goto addr_1190f_3;
    addr_11c28_44:
    if (reinterpret_cast<int64_t>(r14_7) < reinterpret_cast<int64_t>(rax61) || r14_7 == rax61 && *reinterpret_cast<int32_t*>(&r9_9) - r11d12 % reinterpret_cast<signed char>(v60) > *reinterpret_cast<int32_t*>(&r10_10)) {
        addr_11948_11:
        *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0xffffffff);
        goto addr_1190f_3;
    } else {
        *reinterpret_cast<int32_t*>(&rdi94) = v11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi94) + 4) = 0;
        rbx95 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp6) + 0x70);
        rax96 = fun_36b0(rdi94, r15_24, rbx95, rdi94, r15_24, rbx95);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
        r10_10 = r10_10;
        r11d12 = r11d12;
        r9_9 = r9_9;
        if (*reinterpret_cast<int32_t*>(&rax96)) 
            goto addr_11f3a_47;
        *reinterpret_cast<int32_t*>(&rdi97) = v11;
        *reinterpret_cast<int32_t*>(&rdi97 + 4) = 0;
        v98 = r9_9;
        rdx99 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x90);
        v100 = r10_10;
        v101 = r11d12;
        eax102 = fun_3cb0(rdi97, r15_24, rdx99, rdi97, r15_24, rdx99);
        r11d12 = v101;
        r10_10 = v100;
        r9_9 = v98;
        rax103 = v104;
        rsi105 = v106;
        if (!(reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax102))) | (v44 ^ rsi105 | rax103 ^ r14_7))) 
            goto addr_11d2e_49;
    }
    *reinterpret_cast<int32_t*>(&rdi107) = v11;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi107) + 4) = 0;
    fun_36b0(rdi107, r15_24, rbx95, rdi107, r15_24, rbx95);
    r11d12 = v101;
    r10_10 = v100;
    r9_9 = v98;
    if (eax102) {
        addr_11f3a_47:
        *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0xfffffffe);
        goto addr_1190f_3;
    } else {
        rax103 = v108;
        rsi105 = v109;
    }
    addr_11d2e_49:
    edx110 = (*reinterpret_cast<uint32_t*>(&rax103) & 1) * 0x3b9aca00 + *reinterpret_cast<uint32_t*>(&rsi105);
    __asm__("ror eax, 1");
    if (edx110 * 0xcccccccd + 0x19999998 > 0x19999998) 
        goto addr_11dab_6;
    esi111 = v60;
    if (esi111 == 10) 
        goto addr_11fcb_5;
    ecx112 = 9;
    *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(10);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_19) + 4) = 0;
    while (1) {
        rax113 = reinterpret_cast<int32_t>(edx110) * 0x66666667 >> 34;
        eax114 = *reinterpret_cast<int32_t*>(&rax113) - reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(edx110) >> 31);
        edx110 = eax114;
        __asm__("ror eax, 1");
        if (eax114 * 0xcccccccd + 0x19999998 > 0x19999998) 
            goto addr_11dab_6;
        --ecx112;
        if (!ecx112) 
            break;
        r12d115 = reinterpret_cast<void*>(static_cast<uint32_t>(r12_19 + r12_19 * 4));
        *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(r12d115) + reinterpret_cast<uint32_t>(r12d115));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_19) + 4) = 0;
        if (*reinterpret_cast<void***>(&r12_19) == esi111) 
            goto addr_11dab_6;
    }
    *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0x77359400);
    goto addr_11dab_6;
    addr_11ee0_37:
    r13_8 = v77;
    r11d12 = v71;
    v60 = edi81;
    r14_7 = v74;
    r9_9 = v75;
    addr_11ef8_58:
    *reinterpret_cast<void***>(v22 + 8) = *reinterpret_cast<void***>(&rbx73);
    rax61 = reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(~static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r8d80 == 0xbebc200)))))) & r13_8;
    goto addr_11c11_42;
    addr_11f20_39:
    r13_8 = v77;
    r11d12 = v71;
    v60 = edi81;
    r14_7 = v74;
    r9_9 = v75;
    goto addr_11ef8_58;
    addr_11bd1_41:
    r14_7 = v74;
    r13_8 = v77;
    r11d12 = v71;
    rax116 = reinterpret_cast<unsigned char>(v45) | r14_7 | v46;
    r9_9 = v75;
    if (!(*reinterpret_cast<unsigned char*>(&rax116) & 1)) {
        *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(0x77359400);
        rax61 = r13_8 & 0xfffffffffffffffe;
        v60 = reinterpret_cast<void**>(0x77359400);
        goto addr_11c11_42;
    } else {
        v60 = reinterpret_cast<void**>(0x3b9aca00);
        *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(0x3b9aca00);
        rax61 = r13_8;
        goto addr_11c11_42;
    }
}

void fun_11ff3(int64_t rdi, int64_t rsi, int64_t rdx, int32_t ecx) {
    __asm__("cli ");
    goto utimecmpat;
}

struct s83 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void* f18;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void* f30;
};

void fun_3a10(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_12013(void** rdi, void** rsi, void** rdx, void** rcx, struct s83* r8, void** r9) {
    void** r12_7;
    void* v8;
    void** v9;
    int64_t v10;
    void* v11;
    void* v12;
    void** v13;
    int64_t v14;
    void* v15;
    void** rax16;
    void* v17;
    void** v18;
    int64_t v19;
    void* v20;
    void** rax21;
    void* v22;
    void** v23;
    int64_t v24;
    void* v25;
    void* r9_26;
    int64_t r8_27;
    void** rcx28;
    void* r15_29;
    void** r14_30;
    void** r13_31;
    void** r12_32;
    void** rax33;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_3c40(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11);
    } else {
        r9 = rcx;
        fun_3c40(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v12, v13, v14, v15);
    }
    rax16 = fun_3800();
    fun_3c40(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax16, 0x7e6, r9, v17, v18, v19, v20);
    fun_3a10(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax16, 0x7e6, r9);
    rax21 = fun_3800();
    fun_3c40(rdi, 1, rax21, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v22, v23, v24, v25);
    fun_3a10(10, rdi, rax21, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r9_26 = r8->f30;
        r8_27 = r8->f28;
        rcx28 = r8->f20;
        r15_29 = r8->f18;
        r14_30 = r8->f10;
        r13_31 = r8->f8;
        r12_32 = r8->f0;
        rax33 = fun_3800();
        fun_3c40(rdi, 1, rax33, r12_32, r13_31, r14_30, r15_29, rcx28, r8_27, r9_26, rdi, 1, rax33, r12_32, r13_31, r14_30, r15_29, rcx28, r8_27, r9_26);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x16d80 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x16d80;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_12483() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s84 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_124a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s84* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s84* rcx8;
    void* rax9;
    void* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v10) - reinterpret_cast<int64_t>(g28));
    if (rax18) {
        fun_3840();
    } else {
        return;
    }
}

void fun_12543(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void* rax15;
    void* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_125e6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_125f0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v16) - reinterpret_cast<int64_t>(g28));
    if (rax20) {
        fun_3840();
    } else {
        return;
    }
    addr_125e6_5:
    goto addr_125f0_7;
}

void fun_12623() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_3a10(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_3800();
    fun_3b40(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_3800();
    fun_3b40(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_3800();
    goto fun_3b40;
}

/* initialized.1 */
signed char initialized_1 = 0;

/* can_write.0 */
unsigned char can_write_0 = 0;

int64_t fun_126c3() {
    int1_t zf1;
    int64_t rax2;
    int64_t rax3;

    __asm__("cli ");
    zf1 = initialized_1 == 0;
    if (zf1) {
        rax2 = fun_38e0();
        initialized_1 = 1;
        *reinterpret_cast<unsigned char*>(&rax2) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax2) == 0);
        can_write_0 = *reinterpret_cast<unsigned char*>(&rax2);
        return rax2;
    } else {
        *reinterpret_cast<uint32_t*>(&rax3) = can_write_0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    }
}

int64_t fun_3c60();

void fun_12703() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_3c60();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_3740();

void fun_12723(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3740();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_12763(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ab0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_12783(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ab0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_127a3(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ab0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_127c3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;

    __asm__("cli ");
    rax5 = fun_3b10(rdi, rsi, rdx, rcx);
    if (rax5 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_127f3(void** rdi, uint64_t rsi, void** rdx, void** rcx) {
    uint64_t rax5;
    void** rax6;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(rsi == 0);
    rax6 = fun_3b10(rdi, rsi | rax5, rdx, rcx);
    if (!rax6) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_12823(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3740();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_12863() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_3740();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_128a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_3740();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_128d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_3740();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_12923(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_3740();
            if (rax5) 
                break;
            addr_1296d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_1296d_5;
        rax8 = fun_3740();
        if (rax8) 
            goto addr_12956_9;
        if (rbx4) 
            goto addr_1296d_5;
        addr_12956_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_129b3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_3740();
            if (rax8) 
                break;
            addr_129fa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_129fa_5;
        rax11 = fun_3740();
        if (rax11) 
            goto addr_129e2_9;
        if (!rbx6) 
            goto addr_129e2_9;
        if (r12_4) 
            goto addr_129fa_5;
        addr_129e2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_12a43(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_12aed_9:
            rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8));
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<unsigned char>(rdx));
            if (!r13_6) {
                addr_12b00_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_12aa0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_12ac6_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_12b14_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_12abd_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_12b14_14;
            addr_12abd_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_12b14_14;
            addr_12ac6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_3b10(rdi7, rsi9, rdx, rcx10);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_12b14_14;
            if (!rbp13) 
                break;
            addr_12b14_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_12aed_9;
        } else {
            if (!r13_6) 
                goto addr_12b00_10;
            goto addr_12aa0_11;
        }
    }
}

void fun_12b43(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_39c0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_12b73(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_39c0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_12ba3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_39c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_12bc3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_39c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_12be3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ab0(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3a80;
    }
}

void fun_12c23(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ab0(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3a80;
    }
}

void fun_12c63(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ab0(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_3a80;
    }
}

void fun_12ca3(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_3820(rdi);
    rax5 = fun_3ab0(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_3a80;
    }
}

void fun_12ce3() {
    void** rdi1;

    __asm__("cli ");
    fun_3800();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_3b80();
    fun_36c0(rdi1);
}

int64_t fun_39d0(void* rdi, void* rsi, int64_t rdx, void** rcx);

int32_t fun_3a40(int64_t rdi, void* rsi, int64_t rdx, void** rcx);

int64_t fun_12d23() {
    int32_t r12d1;
    void* rsp2;
    void** rcx3;
    void* rax4;
    void* rsi5;
    int64_t rax6;
    signed char* rax7;
    int32_t eax8;
    void* rax9;
    int64_t rax10;

    __asm__("cli ");
    r12d1 = 0;
    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32);
    rcx3 = stdin;
    rax4 = g28;
    rsi5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 16);
    rax6 = fun_39d0(reinterpret_cast<int64_t>(rsp2) + 8, rsi5, 10, rcx3);
    if (!(reinterpret_cast<uint1_t>(rax6 < 0) | reinterpret_cast<uint1_t>(rax6 == 0))) {
        rax7 = reinterpret_cast<signed char*>(rax6 - 1);
        if (*rax7 == 10) {
            *rax7 = 0;
        }
        eax8 = fun_3a40(0, rsi5, 10, rcx3);
        *reinterpret_cast<unsigned char*>(&r12d1) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax8 < 0) | reinterpret_cast<uint1_t>(eax8 == 0)));
    }
    fun_3670(0, 0);
    rax9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rax9) {
        fun_3840();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = r12d1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

void fun_12dc3() {
    __asm__("cli ");
    return;
}

int64_t fun_12dd3() {
    int32_t* rcx1;
    int32_t edx2;

    __asm__("cli ");
    *rcx1 = edx2;
    return 0;
}

int32_t fun_3b60(int64_t rdi, int64_t rsi);

void fun_12de3(int64_t rdi, int32_t esi, int32_t edx) {
    __asm__("cli ");
    if (esi == -1) {
        goto fun_3b60;
    } else {
        goto fun_3b50;
    }
}

int64_t fun_12e03(int32_t* rdi, int64_t rsi, int32_t edx) {
    int64_t rsi4;
    int32_t eax5;
    uint32_t eax6;
    int64_t rax7;
    int64_t rdi8;
    int32_t eax9;
    uint32_t eax10;
    int64_t rax11;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi4) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi4) + 4) = 0;
    if (edx == -1) {
        eax5 = fun_3b60(rsi, rsi4);
        eax6 = reinterpret_cast<uint32_t>(-eax5);
        *reinterpret_cast<uint32_t*>(&rax7) = eax6 - (eax6 + reinterpret_cast<uint1_t>(eax6 < eax6 + reinterpret_cast<uint1_t>(!!eax5)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    } else {
        *reinterpret_cast<int32_t*>(&rdi8) = edx;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        eax9 = fun_3b50(rdi8, rdi8);
        eax10 = reinterpret_cast<uint32_t>(-eax9);
        *reinterpret_cast<uint32_t*>(&rax11) = eax10 - (eax10 + reinterpret_cast<uint1_t>(eax10 < eax10 + reinterpret_cast<uint1_t>(!!eax9)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        return rax11;
    }
}

int64_t fun_3700();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_12e43(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_3700();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_12e9e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_36d0();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_12e9e_3;
            rax6 = fun_36d0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int64_t fun_37d0();

uint32_t fun_3a00(int64_t rdi);

int32_t rpl_fcntl(int64_t rdi, void** rsi, void** rdx);

int64_t fun_12eb3() {
    int64_t rax1;
    uint32_t eax2;
    int64_t rdi3;
    int32_t eax4;
    void** rax5;
    int64_t rdi6;
    int64_t rax7;
    void** r14d8;
    int64_t r13_9;
    int64_t rdi10;
    void** rcx11;

    __asm__("cli ");
    rax1 = fun_37d0();
    if (!rax1 || (eax2 = fun_3a00(rax1), eax2 > 2)) {
        return rax1;
    } else {
        *reinterpret_cast<uint32_t*>(&rdi3) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi3) + 4) = 0;
        eax4 = rpl_fcntl(rdi3, 0x406, 3);
        rax5 = fun_36d0();
        if (eax4 >= 0) {
            *reinterpret_cast<int32_t*>(&rdi6) = eax4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
            rax7 = fun_3bc0(rdi6, 0x406, rdi6, 0x406);
            r14d8 = *reinterpret_cast<void***>(rax5);
            r13_9 = rax7;
            if (!rax7) {
                *reinterpret_cast<int32_t*>(&rdi10) = eax4;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
                fun_3920(rdi10, 0x406, rdi10, 0x406);
            }
        } else {
            r14d8 = *reinterpret_cast<void***>(rax5);
            *reinterpret_cast<int32_t*>(&r13_9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_9) + 4) = 0;
        }
        fun_3950(rax1, 0x406, 3, rcx11);
        *reinterpret_cast<void***>(rax5) = r14d8;
        return r13_9;
    }
}

int64_t fun_37c0(void** rdi);

int64_t fun_12f53(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    void** rax4;
    int32_t eax5;
    void** rax6;
    void** r12d7;
    int64_t rax8;

    __asm__("cli ");
    eax2 = fun_3a90(rdi);
    if (eax2 >= 0) {
        eax3 = fun_3b00(rdi);
        if (!(eax3 && (fun_3a90(rdi), rax4 = fun_38b0(), reinterpret_cast<int1_t>(rax4 == 0xffffffffffffffff)) || (eax5 = rpl_fflush(rdi), eax5 == 0))) {
            rax6 = fun_36d0();
            r12d7 = *reinterpret_cast<void***>(rax6);
            rax8 = fun_37c0(rdi);
            if (r12d7) {
                *reinterpret_cast<void***>(rax6) = r12d7;
                *reinterpret_cast<int32_t*>(&rax8) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            }
            return rax8;
        }
    }
    goto fun_37c0;
}

uint32_t fun_3770(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_12fe3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void* rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    void** rsi16;
    int64_t rdi17;
    uint32_t eax18;
    int64_t rdi19;
    uint32_t eax20;
    void** rax21;
    int64_t rdi22;
    void** r13d23;
    uint32_t eax24;
    void** rax25;
    int64_t rdi26;
    uint32_t eax27;
    uint32_t ecx28;
    int64_t rax29;
    uint32_t eax30;
    uint32_t eax31;
    uint32_t eax32;
    int32_t ecx33;
    int64_t rax34;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_3770(rdi);
        r12d10 = eax9;
        goto addr_130e4_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_3770(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_130e4_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax8) - reinterpret_cast<int64_t>(g28));
                if (rax14) {
                    fun_3840();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_13199_9:
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi17) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
                eax18 = fun_3770(rdi17, rdi17);
                if (reinterpret_cast<int32_t>(eax18) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi16) = 2, *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi19) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0, eax20 = fun_3770(rdi19, rdi19), eax20 == 0xffffffff)) {
                    rax21 = fun_36d0();
                    *reinterpret_cast<uint32_t*>(&rdi22) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
                    r12d10 = 0xffffffff;
                    r13d23 = *reinterpret_cast<void***>(rax21);
                    fun_3920(rdi22, rsi16, rdi22, rsi16);
                    *reinterpret_cast<void***>(rax21) = r13d23;
                    goto addr_130e4_3;
                }
            }
        } else {
            eax24 = fun_3770(rdi, rdi);
            r12d10 = eax24;
            if (reinterpret_cast<int32_t>(eax24) >= reinterpret_cast<int32_t>(0) || (rax25 = fun_36d0(), *reinterpret_cast<int32_t*>(&rdi26) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax25) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_130e4_3;
            } else {
                eax27 = fun_3770(rdi26);
                r12d10 = eax27;
                if (reinterpret_cast<int32_t>(eax27) < reinterpret_cast<int32_t>(0)) 
                    goto addr_130e4_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_13199_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_13049_16;
    ecx28 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx28 > 10) 
        goto addr_1304d_18;
    rax29 = 1 << *reinterpret_cast<unsigned char*>(&ecx28);
    if (!(*reinterpret_cast<uint32_t*>(&rax29) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax29) & 0x502)) {
            addr_1304d_18:
            if (0) {
            }
        } else {
            addr_13095_23:
            eax30 = fun_3770(rdi);
            r12d10 = eax30;
            goto addr_130e4_3;
        }
        eax31 = fun_3770(rdi);
        r12d10 = eax31;
        goto addr_130e4_3;
    }
    if (0) {
    }
    eax32 = fun_3770(rdi);
    r12d10 = eax32;
    goto addr_130e4_3;
    addr_13049_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_1304d_18;
    ecx33 = *reinterpret_cast<int32_t*>(&rsi);
    rax34 = 1 << *reinterpret_cast<unsigned char*>(&ecx33);
    if (!(*reinterpret_cast<uint32_t*>(&rax34) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax34) & 0xa0a) 
            goto addr_13095_23;
        goto addr_1304d_18;
    }
}

uint64_t fun_13253(signed char* rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;

    __asm__("cli ");
    rdx3 = *rdi;
    if (!*reinterpret_cast<signed char*>(&rdx3)) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        do {
            __asm__("rol rax, 0x9");
            ++rdi;
            rax4 = rax4 + rdx3;
            rdx3 = *rdi;
        } while (*reinterpret_cast<signed char*>(&rdx3));
        return rax4 % reinterpret_cast<uint64_t>(rsi);
    }
}

signed char* fun_3ae0(int64_t rdi);

signed char* fun_13293() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_3ae0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_3860(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_132d3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_3860(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<int64_t>(g28));
    if (rax9) {
        fun_3840();
    } else {
        return r12_7;
    }
}

void fun_13363() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t setlocale_null_r();

int64_t fun_13383() {
    void* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax1) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_3840();
    } else {
        return rax3;
    }
}

int64_t fun_13403(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_3b30(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_3820(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_3a80(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_3a80(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_134b3() {
    __asm__("cli ");
    goto fun_3b30;
}

void fun_134c3() {
    __asm__("cli ");
}

void fun_134d7() {
    __asm__("cli ");
    return;
}

void fun_3f94() {
    goto 0x3ea0;
}

void fun_4043() {
    parents_option = 1;
    goto 0x3ea0;
}

void fun_40a8() {
    goto 0x3ea0;
}

void fun_4111() {
    goto 0x3ea0;
}

void** optarg = reinterpret_cast<void**>(0);

void fun_4175() {
    int1_t zf1;
    void** rax2;
    void** rax3;

    zf1 = selinux_enabled == 0;
    rax2 = optarg;
    if (zf1) {
        if (!rax2) 
            goto 0x3ea0;
        fun_3800();
        fun_3b80();
        goto 0x3ea0;
    } else {
        if (!rax2) {
            rax3 = fun_36d0();
            *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(95);
            fun_3800();
            fun_3b80();
            goto 0x3ea0;
        } else {
            goto 0x3ea0;
        }
    }
}

void fun_419c() {
    goto 0x3ea0;
}

void fun_423a() {
    goto 0x3ea0;
}

struct s85 {
    signed char[53] pad53;
    signed char f35;
};

struct s86 {
    signed char[54] pad54;
    signed char f36;
};

void fun_4670() {
    struct s85* rbp1;
    signed char r12b2;
    struct s86* rbp3;
    signed char r12b4;
    int64_t r14_5;

    rbp1->f35 = r12b2;
    rbp3->f36 = r12b4;
    if (!r14_5) {
        goto fun_3670;
    } else {
        goto 0x4610;
    }
}

struct s87 {
    signed char[52] pad52;
    signed char f34;
};

struct s88 {
    signed char[51] pad51;
    signed char f33;
};

void fun_4690() {
    struct s87* rbp1;
    signed char r12b2;
    struct s88* rbp3;
    signed char r12b4;
    int64_t r14_5;

    rbp1->f34 = r12b2;
    rbp3->f33 = r12b4;
    if (r14_5) 
        goto 0x4685; else 
        goto "???";
}

struct s89 {
    signed char[48] pad48;
    signed char f30;
};

void fun_46b8() {
    struct s89* rbp1;
    signed char r12b2;

    rbp1->f30 = r12b2;
    goto 0x4680;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_3c90(int64_t rdi, void** rsi);

uint32_t fun_3c70(void** rdi, void** rsi);

void** fun_3cc0(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_e975() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_3800();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_3800();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_3820(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_ec73_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_ec73_22; else 
                            goto addr_f06d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_f12d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_f480_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_ec70_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_ec70_30; else 
                                goto addr_f499_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_3820(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_f480_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_3990(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_f480_28; else 
                            goto addr_eb1c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_f5e0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_f460_40:
                        if (r11_27 == 1) {
                            addr_efed_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_f5a8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_ec27_44;
                            }
                        } else {
                            goto addr_f470_46;
                        }
                    } else {
                        addr_f5ef_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_efed_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_ec73_22:
                                if (v47 != 1) {
                                    addr_f1c9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_3820(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_f214_54;
                                    }
                                } else {
                                    goto addr_ec80_56;
                                }
                            } else {
                                addr_ec25_57:
                                ebp36 = 0;
                                goto addr_ec27_44;
                            }
                        } else {
                            addr_f454_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_f5ef_47; else 
                                goto addr_f45e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_efed_41;
                        if (v47 == 1) 
                            goto addr_ec80_56; else 
                            goto addr_f1c9_52;
                    }
                }
                addr_ece1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_eb78_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_eb9d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_eea0_65;
                    } else {
                        addr_ed09_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_f558_67;
                    }
                } else {
                    goto addr_ed00_69;
                }
                addr_ebb1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_ebfc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_f558_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_ebfc_81;
                }
                addr_ed00_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_eb9d_64; else 
                    goto addr_ed09_66;
                addr_ec27_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_ecdf_91;
                if (v22) 
                    goto addr_ec3f_93;
                addr_ecdf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_ece1_62;
                addr_f214_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_f99b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_fa0b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_f80f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_3c90(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_3c70(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_f30e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_eccc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_f318_112;
                    }
                } else {
                    addr_f318_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_f3e9_114;
                }
                addr_ecd8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_ecdf_91;
                while (1) {
                    addr_f3e9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_f8f7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_f356_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_f905_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_f3d7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_f3d7_128;
                        }
                    }
                    addr_f385_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_f3d7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_f356_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_f385_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_ebfc_81;
                addr_f905_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_f558_67;
                addr_f99b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_f30e_109;
                addr_fa0b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_f30e_109;
                addr_ec80_56:
                rax93 = fun_3cc0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_eccc_110;
                addr_f45e_59:
                goto addr_f460_40;
                addr_f12d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_ec73_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_ecd8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_ec25_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_ec73_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_f172_160;
                if (!v22) 
                    goto addr_f547_162; else 
                    goto addr_f753_163;
                addr_f172_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_f547_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_f558_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_f01b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_ee83_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_ebb1_70; else 
                    goto addr_ee97_169;
                addr_f01b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_eb78_63;
                goto addr_ed00_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_f454_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_f58f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_ec70_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_eb68_178; else 
                        goto addr_f512_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_f454_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_ec73_22;
                }
                addr_f58f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_ec70_30:
                    r8d42 = 0;
                    goto addr_ec73_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_ece1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_f5a8_42;
                    }
                }
                addr_eb68_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_eb78_63;
                addr_f512_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_f470_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_ece1_62;
                } else {
                    addr_f522_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_ec73_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_fcd2_188;
                if (v28) 
                    goto addr_f547_162;
                addr_fcd2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_ee83_168;
                addr_eb1c_37:
                if (v22) 
                    goto addr_fb13_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_eb33_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_f5e0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_f66b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_ec73_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_eb68_178; else 
                        goto addr_f647_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_f454_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_ec73_22;
                }
                addr_f66b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_ec73_22;
                }
                addr_f647_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_f470_46;
                goto addr_f522_186;
                addr_eb33_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_ec73_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_ec73_22; else 
                    goto addr_eb44_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_fc1e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_faa4_210;
            if (1) 
                goto addr_faa2_212;
            if (!v29) 
                goto addr_f6de_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_3810();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_fc11_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_eea0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_ec5b_219; else 
            goto addr_eeba_220;
        addr_ec3f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_ec53_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_eeba_220; else 
            goto addr_ec5b_219;
        addr_f80f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_eeba_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_3810();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_f82d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_3810();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_fca0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_f706_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_f8f7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_ec53_221;
        addr_f753_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_ec53_221;
        addr_ee97_169:
        goto addr_eea0_65;
        addr_fc1e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_eeba_220;
        goto addr_f82d_222;
        addr_faa4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - reinterpret_cast<int64_t>(g28);
        if (!rax111) 
            goto addr_fafe_236;
        fun_3840();
        rsp25 = rsp25 - 8 + 8;
        goto addr_fca0_225;
        addr_faa2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_faa4_210;
        addr_f6de_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_faa4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_f706_226;
        }
        addr_fc11_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_f06d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x167ec + rax113 * 4) + 0x167ec;
    addr_f499_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x168ec + rax114 * 4) + 0x168ec;
    addr_fb13_190:
    addr_ec5b_219:
    goto 0xe940;
    addr_eb44_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x166ec + rax115 * 4) + 0x166ec;
    addr_fafe_236:
    goto v116;
}

void fun_eb60() {
}

void fun_ed18() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0xea12;
}

void fun_ed71() {
    goto 0xea12;
}

void fun_ee5e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0xece1;
    }
    if (v2) 
        goto 0xf753;
    if (!r10_3) 
        goto addr_f8be_5;
    if (!v4) 
        goto addr_f78e_7;
    addr_f8be_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_f78e_7:
    goto 0xeb94;
}

void fun_ee7c() {
}

void fun_ef27() {
    signed char v1;

    if (v1) {
        goto 0xeeaf;
    } else {
        goto 0xebea;
    }
}

void fun_ef41() {
    signed char v1;

    if (!v1) 
        goto 0xef3a; else 
        goto "???";
}

void fun_ef68() {
    goto 0xee83;
}

void fun_efe8() {
}

void fun_f000() {
}

void fun_f02f() {
    goto 0xee83;
}

void fun_f081() {
    goto 0xf010;
}

void fun_f0b0() {
    goto 0xf010;
}

void fun_f0e3() {
    goto 0xf010;
}

void fun_f4b0() {
    goto 0xeb68;
}

void fun_f7ae() {
    signed char v1;

    if (v1) 
        goto 0xf753;
    goto 0xeb94;
}

void fun_f855() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0xeb94;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0xeb78;
        goto 0xeb94;
    }
}

void fun_fc72() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0xeee0;
    } else {
        goto 0xea12;
    }
}

void fun_120e8() {
    fun_3800();
}

void fun_3f6e() {
    void** rdi1;

    rdi1 = optarg;
    if (rdi1) 
        goto 0x41f4;
    goto 0x3ea0;
}

void fun_3f9e() {
    remove_trailing_slashes = 1;
    goto 0x3ea0;
}

void fun_404f() {
    void** rdi1;
    void** r15_2;
    int64_t rcx3;
    int64_t r8_4;
    void** r9_5;
    int64_t v6;

    rdi1 = optarg;
    decode_preserve_arg(rdi1, r15_2, 0, rcx3, r8_4, r9_5, __return_address(), v6);
    goto 0x3ea0;
}

void fun_40b2() {
    goto 0x3ea0;
}

void fun_411e() {
    goto 0x3ea0;
}

void fun_4244() {
    goto 0x3ea0;
}

struct s90 {
    signed char[29] pad29;
    signed char f1d;
};

void fun_46c0() {
    struct s90* rbp1;
    signed char r12b2;

    rbp1->f1d = r12b2;
    goto 0x4680;
}

void fun_ed9e() {
    goto 0xea12;
}

void fun_ef74() {
    goto 0xef2c;
}

void fun_f03b() {
    goto 0xeb68;
}

void fun_f08d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0xf010;
    goto 0xec3f;
}

void fun_f0bf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0xf01b;
        goto 0xea40;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0xeeba;
        goto 0xec5b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0xf858;
    if (r10_8 > r15_9) 
        goto addr_efa5_9;
    addr_efaa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0xf863;
    goto 0xeb94;
    addr_efa5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_efaa_10;
}

void fun_f0f2() {
    goto 0xec27;
}

void fun_f4c0() {
    goto 0xec27;
}

void fun_fc5f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0xed7c;
    } else {
        goto 0xeee0;
    }
}

void fun_121a0() {
}

void fun_3faa() {
    int64_t r9_1;
    void** rsi2;

    r9_1 = argmatch_die;
    rsi2 = optarg;
    __xargmatch_internal("--sparse", rsi2, 0x1b980, 0x14470, 4, r9_1);
    goto 0x3ea0;
}

void fun_4065() {
    goto 0x3ea0;
}

void fun_40bf() {
    goto 0x3ea0;
}

void fun_412b() {
    goto 0x3ea0;
}

struct s91 {
    signed char[31] pad31;
    signed char f1f;
};

void fun_46d0() {
    struct s91* rbp1;
    signed char r12b2;

    rbp1->f1f = r12b2;
    goto 0x4680;
}

void fun_f0fc() {
    goto 0xf097;
}

void fun_f4ca() {
    goto 0xefed;
}

void fun_12200() {
    fun_3800();
    goto fun_3c40;
}

void fun_3ff1() {
    void** rsi1;
    int64_t r9_2;

    rsi1 = optarg;
    if (!rsi1) {
        goto 0x3ea0;
    } else {
        r9_2 = argmatch_die;
        __xargmatch_internal("--reflink", rsi1, 0x1b960, 0x14460, 4, r9_2);
        goto 0x3ea0;
    }
}

void fun_406f() {
    goto 0x3ea0;
}

void fun_40c9() {
    goto 0x3ea0;
}

void fun_4138() {
    int1_t zf1;

    zf1 = selinux_enabled == 0;
    if (!zf1) {
    }
    goto 0x3ea0;
}

struct s92 {
    signed char[30] pad30;
    signed char f1e;
};

struct s93 {
    signed char[32] pad32;
    signed char f20;
};

void fun_46e0() {
    struct s92* rbp1;
    signed char r12b2;
    uint32_t eax3;
    uint32_t r12d4;
    struct s93* rbp5;

    rbp1->f1e = r12b2;
    eax3 = r12d4 ^ 1;
    rbp5->f20 = *reinterpret_cast<signed char*>(&eax3);
    goto 0x4680;
}

void fun_edcd() {
    goto 0xea12;
}

void fun_f108() {
    goto 0xf097;
}

void fun_f4d7() {
    goto 0xf03e;
}

void fun_12240() {
    fun_3800();
    goto fun_3c40;
}

void fun_4079() {
    goto 0x3ea0;
}

void fun_40d6() {
    goto 0x3ea0;
}

struct s94 {
    signed char[30] pad30;
    signed char f1e;
};

struct s95 {
    signed char[31] pad31;
    signed char f1f;
};

struct s96 {
    signed char[29] pad29;
    signed char f1d;
};

struct s97 {
    signed char[48] pad48;
    signed char f30;
};

struct s98 {
    signed char[32] pad32;
    signed char f20;
};

struct s99 {
    signed char[51] pad51;
    signed char f33;
};

struct s100 {
    signed char[53] pad53;
    signed char f35;
};

void fun_46f0() {
    struct s94* rbp1;
    signed char r12b2;
    uint32_t eax3;
    uint32_t r12d4;
    int1_t zf5;
    struct s95* rbp6;
    signed char r12b7;
    struct s96* rbp8;
    signed char r12b9;
    struct s97* rbp10;
    signed char r12b11;
    struct s98* rbp12;
    struct s99* rbp13;
    signed char r12b14;
    struct s100* rbp15;
    signed char r12b16;

    rbp1->f1e = r12b2;
    eax3 = r12d4 ^ 1;
    zf5 = selinux_enabled == 0;
    rbp6->f1f = r12b7;
    rbp8->f1d = r12b9;
    rbp10->f30 = r12b11;
    rbp12->f20 = *reinterpret_cast<signed char*>(&eax3);
    if (!zf5) {
        rbp13->f33 = r12b14;
    }
    rbp15->f35 = r12b16;
    goto 0x4680;
}

void fun_edfa() {
    goto 0xea12;
}

void fun_f114() {
    goto 0xf010;
}

void fun_12280() {
    fun_3800();
    goto fun_3c40;
}

void fun_4083() {
    goto 0x3ea0;
}

void fun_40e0() {
    goto 0x3ea0;
}

void fun_ee1c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0xf7b0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0xece1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0xece1;
    }
    if (v11) 
        goto 0xfb13;
    if (r10_12 > r15_13) 
        goto addr_fb63_8;
    addr_fb68_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0xf8a1;
    addr_fb63_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_fb68_9;
}

struct s101 {
    signed char[24] pad24;
    void* f18;
};

struct s102 {
    signed char[16] pad16;
    void** f10;
};

struct s103 {
    signed char[8] pad8;
    void** f8;
};

void fun_122d0() {
    void* r15_1;
    struct s101* rbx2;
    void** r14_3;
    struct s102* rbx4;
    void** r13_5;
    struct s103* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    void* v11;
    int64_t v12;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_3800();
    fun_3c40(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x122f2, __return_address(), v11);
    goto v12;
}

void fun_408d() {
    if (__return_address()) {
        fun_3800();
        fun_3b80();
    } else {
        goto 0x3ea0;
    }
}

void fun_40f2() {
    void** rax1;

    rax1 = optarg;
    if (!rax1) {
    }
    goto 0x3ea0;
}

void fun_12328() {
    fun_3800();
    goto 0x122f9;
}

struct s104 {
    signed char[32] pad32;
    void** f20;
};

struct s105 {
    signed char[24] pad24;
    void* f18;
};

struct s106 {
    signed char[16] pad16;
    void** f10;
};

struct s107 {
    signed char[8] pad8;
    void** f8;
};

struct s108 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_12360() {
    void** rcx1;
    struct s104* rbx2;
    void* r15_3;
    struct s105* rbx4;
    void** r14_5;
    struct s106* rbx6;
    void** r13_7;
    struct s107* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s108* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_3800();
    fun_3c40(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x12394, rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x12394);
    goto v15;
}

void fun_123d8() {
    fun_3800();
    goto 0x1239b;
}
