#include <stdint.h>

/* /tmp/tmp7e2uf9hg @ 0x44d0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp7e2uf9hg @ 0x45c0 */
 
uint64_t decode_preserve_arg (int64_t arg_1dh, int64_t arg_1eh, int64_t arg_1fh, int64_t arg_20h, int64_t arg_30h, int64_t arg_33h, int64_t arg_34h, int64_t arg_35h, int64_t arg_36h, int64_t arg2, int64_t arg3) {
    int64_t var_10h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    r14 = "--preserve";
    r13 = 0x00014320;
    r12d = edx;
    ebx = edx;
    rax = xstrdup (rdi);
    rbx = obj_preserve_vals_1;
    *((rsp + 8)) = rax;
    r15 = rax;
    rax = "--no-preserve";
    if (bl != 0) {
        rax = r14;
    }
    *(rsp) = rax;
label_0:
    r14d = 0;
    rax = strchr (r15, 0x2c);
    if (rax != 0) {
        *(rax) = 0;
        r14 = rax + 1;
    }
    _xargmatch_internal (*((rsp + 0x10)), r15, obj.preserve_args.2, rbx, 4, *(obj.argmatch_die));
    if (*((rbx + rax*4)) > 6) {
        void (*0x3cd0)() ();
    }
    eax = *((rbx + rax*4));
    rax = *((r13 + rax*4));
    rax += r13;
    /* switch table (7 cases) at 0x14320 */
    void (*rax)() ();
    *((rbp + 0x35)) = r12b;
    *((rbp + 0x36)) = r12b;
label_1:
    if (r14 == 0) {
        goto label_2;
    }
    do {
        r15 = r14;
        goto label_0;
        *((rbp + 0x34)) = r12b;
        *((rbp + 0x33)) = r12b;
    } while (r14 != 0);
label_2:
    rdi = *((rsp + 8));
    void (*0x3670)() ();
    *((rbp + 0x30)) = r12b;
    goto label_1;
    *((rbp + 0x1d)) = r12b;
    goto label_1;
    *((rbp + 0x1f)) = r12b;
    goto label_1;
    eax = r12d;
    *((rbp + 0x1e)) = r12b;
    eax ^= 1;
    *((rbp + 0x20)) = al;
    goto label_1;
    eax = r12d;
    *((rbp + 0x1e)) = r12b;
    eax ^= 1;
    *((rbp + 0x1f)) = r12b;
    *((rbp + 0x1d)) = r12b;
    *((rbp + 0x30)) = r12b;
    *((rbp + 0x20)) = al;
    if (*(obj.selinux_enabled) != 0) {
        *((rbp + 0x33)) = r12b;
    }
    *((rbp + 0x35)) = r12b;
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0x3cd0 */
 
void decode_preserve_arg_cold (void) {
    /* [16] -r-x section size 63490 named .text */
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x4720 */
 
int64_t dbg_make_dir_parents_private (int64_t arg_10h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_1a0h;
    int64_t var_198h;
    int64_t var_190h;
    int64_t var_188h;
    int64_t var_180h;
    int64_t var_178h;
    void * var_170h;
    void * errname;
    stat stats;
    stat src_st;
    int64_t var_80h;
    int64_t var_70h;
    int64_t var_60h;
    int64_t var_50h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    void * s1;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* _Bool make_dir_parents_private(char const * const_dir,size_t src_offset,int dst_dirfd,char const * verbose_fmt_string,dir_attr ** attr_list,_Bool * new_dst,cp_options const * x); */
    r15d = edx;
    r13 = rdi;
    rbx = rsi;
    *((rbp - 0x198)) = rcx;
    r12 = *((rbp + 0x10));
    *((rbp - 0x188)) = r8;
    *((rbp - 0x178)) = r9;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    rax = dir_len ();
    r14 = rax;
    rax = *((rbp - 0x188));
    *(rax) = 0;
    if (r14 <= rbx) {
label_0:
        eax = 1;
label_6:
        rdx = *((rbp - 0x38));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_13;
        }
        rsp = rbp - 0x28;
        return rax;
    }
    rax = strlen (r13);
    rcx = rsp;
    r8 = rax + 1;
    rax += 0x18;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_14;
    }
    do {
    } while (rsp != rcx);
label_14:
    edx &= 0xfff;
    if (rdx != 0) {
    }
    rdi &= 0xfffffffffffffff0;
    rax = memcpy (rsp + 0xf, r13, r8);
    rdx = rsp;
    *((rbp - 0x168)) = rax;
    rax = r14 + 0x18;
    rcx = rax;
    rax &= 0xfffffffffffff000;
    rdx -= rax;
    rcx &= 0xfffffffffffffff0;
    if (rsp == rdx) {
        goto label_15;
    }
    do {
    } while (rsp != rdx);
label_15:
    ecx &= 0xfff;
    if (rcx != 0) {
    }
    rax = rsp + 0xf;
    rax &= 0xfffffffffffffff0;
    r13 = rax;
    memcpy (rax, *((rbp - 0x168)), r14);
    rsi = r13 + rbx;
    *((r13 + r14)) = 0;
    if (*(rsi) != 0x2f) {
        goto label_16;
    }
    do {
        rsi++;
    } while (*(rsi) == 0x2f);
label_16:
    rax = stats_st_dev;
    ecx = 0;
    edi = r15d;
    rdx = rax;
    *((rbp - 0x180)) = rax;
    eax = fstatat ();
    if (eax == 0) {
        goto label_17;
    }
    rbx += *((rbp - 0x168));
    *((rbp - 0x170)) = rbx;
    r14 = rbx;
    if (*(rbx) != 0x2f) {
        goto label_18;
    }
    do {
        r14++;
    } while (*(r14) == 0x2f);
label_11:
    rax = src_st_st_dev;
    *((rbp - 0x1a0)) = rax;
label_1:
    rax = strchr (r14, 0x2f);
    rbx = rax;
    if (rax == 0) {
        goto label_0;
    }
    *(rbx) = 0;
    rdx = *((rbp - 0x180));
    ecx = 0;
    rsi = r14;
    edi = r15d;
    eax = fstatat ();
    r13d = eax;
    if (eax != 0) {
        goto label_19;
    }
    if ((*((r12 + 0x1c)) & 0xffffff00) != 0) {
        goto label_19;
    }
label_2:
    rsi = *((rbp - 0x168));
    ecx = 0;
    edx = 0;
    r8 = r12;
    rdi = *((rbp - 0x170));
    al = set_process_security_ctx ();
    if (al == 0) {
        goto label_20;
    }
    eax = *(stats.st_mode);
    eax &= case.0xf4aa.9;
    if (eax != 0x4000) {
        goto label_21;
    }
    rax = *((rbp - 0x178));
    *(rax) = 0;
label_4:
    if (*((r12 + 0x28)) == 0) {
        goto label_22;
    }
label_8:
    rdx = r12;
    al = set_file_security_ctx (*((rbp - 0x168)), 0);
    if (al == 0) {
        if (*((r12 + 0x34)) != 0) {
            goto label_20;
        }
    }
label_3:
    *(rbx) = 0x2f;
    rdi = rbx + 1;
    if (*((rbx + 1)) != 0x2f) {
        goto label_1;
    }
    do {
        rdi++;
    } while (*(rdi) == 0x2f);
    goto label_1;
label_19:
    rsi = *((rbp - 0x1a0));
    rdi = *((rbp - 0x170));
    eax = stat ();
    if (eax == 0) {
        goto label_23;
    }
    rax = errno_location ();
    r8d = *(rax);
    if (r8d != 0) {
        goto label_24;
    }
label_5:
    rax = xmalloc (0xa8);
    __asm ("movdqa xmm0, xmmword [src_st.st_dev]");
    __asm ("movdqa xmm1, xmmword [src_st.st_nlink]");
    __asm ("movdqa xmm2, xmmword [src_st.st_gid]");
    __asm ("movdqa xmm3, xmmword [src_st.st_size]");
    r9 = rax;
    __asm ("movups xmmword [rax], xmm0");
    __asm ("movdqa xmm5, xmmword [rbp - 0x80]");
    __asm ("movdqa xmm6, xmmword [rbp - 0x70]");
    __asm ("movdqa xmm7, xmmword [rbp - 0x60]");
    __asm ("movdqa xmm0, xmmword [rbp - 0x50]");
    __asm ("movups xmmword [rax + 0x10], xmm1");
    __asm ("movdqa xmm4, xmmword [src_st.st_blocks]");
    rcx = *((rbp - 0x188));
    __asm ("movups xmmword [rax + 0x20], xmm2");
    __asm ("movups xmmword [rax + 0x30], xmm3");
    __asm ("movups xmmword [rax + 0x40], xmm4");
    __asm ("movups xmmword [rax + 0x50], xmm5");
    __asm ("movups xmmword [rax + 0x60], xmm6");
    __asm ("movups xmmword [rax + 0x70], xmm7");
    __asm ("movups xmmword [rax + 0x80], xmm0");
    rax = rbx;
    rax -= *((rbp - 0x168));
    *((r9 + 0x98)) = rax;
    rax = *(rcx);
    *((r9 + 0x90)) = 0;
    *((r9 + 0xa0)) = rax;
    *(rcx) = r9;
    if (r13d == 0) {
        goto label_2;
    }
    edx = *((r9 + 0x18));
    rsi = *((rbp - 0x168));
    r8 = r12;
    ecx = 1;
    rdi = *((rbp - 0x170));
    *((rbp - 0x190)) = r9;
    al = set_process_security_ctx ();
    if (al == 0) {
        goto label_20;
    }
    rax = *((rbp - 0x178));
    r9 = *((rbp - 0x190));
    *(rax) = 1;
    edx = *((r9 + 0x18));
    if (*((r12 + 0x1d)) == 0) {
        goto label_25;
    }
    r13d = edx;
    r13d &= 0x3f;
    ecx = r13d;
    ecx = ~ecx;
label_7:
    eax = 0x1ff;
    rsi = r14;
    edi = r15d;
    if (*((r12 + 0x20)) != 0) {
        edx = eax;
    }
    *((rbp - 0x190)) = r9;
    edx &= ecx;
    edx &= 0xfff;
    eax = mkdirat ();
    r9 = *((rbp - 0x190));
    if (eax != 0) {
        goto label_26;
    }
    rsi = *((rbp - 0x198));
    if (rsi != 0) {
        rcx = *((rbp - 0x168));
        rdx = *((rbp - 0x170));
        edi = 1;
        eax = 0;
        *((rbp - 0x190)) = r9;
        printf_chk ();
        r9 = *((rbp - 0x190));
    }
    rdx = *((rbp - 0x180));
    ecx = 0x100;
    rsi = r14;
    edi = r15d;
    *((rbp - 0x190)) = r9;
    eax = fstatat ();
    r9 = *((rbp - 0x190));
    if (eax != 0) {
        goto label_27;
    }
    eax = *(stats.st_mode);
    if (*((r12 + 0x1e)) == 0) {
        edx = eax;
        edx = ~edx;
        if ((edx & r13d) != 0) {
            goto label_28;
        }
label_10:
        edx = eax;
        edx &= 0x1c0;
        if (edx == 0x1c0) {
            goto label_29;
        }
label_9:
        r13d |= eax;
        *((r9 + 0x90)) = 1;
        *((r9 + 0x18)) = r13d;
    }
label_29:
    edx = eax;
    edx |= 0x1c0;
    if (edx != eax) {
        ecx = 0x100;
        rsi = r14;
        edi = r15d;
        eax = fchmodat ();
        if (eax != 0) {
            goto label_30;
        }
    }
    rax = *((rbp - 0x178));
    if (*(rax) != 0) {
        goto label_3;
    }
    goto label_4;
label_23:
    eax = *(src_st.st_mode);
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_5;
    }
    r8d = 0x14;
label_24:
    rsi = *((rbp - 0x170));
    edi = 4;
    *((rbp - 0x168)) = r8d;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "failed to get attributes of %s");
    rcx = r12;
    eax = 0;
    eax = error (0, *((rbp - 0x168)), rax);
    eax = 0;
    goto label_6;
label_25:
    if (*((r12 + 0x1e)) == 0) {
        goto label_31;
    }
    r13d = edx;
    r13d &= 0x12;
    ecx = r13d;
    ecx = ~ecx;
    goto label_7;
label_22:
    if (*((r12 + 0x33)) == 0) {
        goto label_3;
    }
    goto label_8;
label_31:
    ecx = 0xffffffff;
    r13d = 0;
    goto label_7;
label_28:
    *((rbp - 0x190)) = r9;
    eax = cached_umask ();
    r9 = *((rbp - 0x190));
    eax = ~eax;
    r13d &= eax;
    eax = *(stats.st_mode);
    edx = *(stats.st_mode);
    edx = ~edx;
    if ((edx & r13d) != 0) {
        goto label_9;
    }
    goto label_10;
label_17:
    eax = *(stats.st_mode);
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        rax = *((rbp - 0x178));
        *(rax) = 0;
        goto label_0;
    }
    rsi = r13;
    do {
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "%s exists but is not a directory");
        rcx = r12;
        eax = 0;
        eax = error (0, 0, rax);
label_20:
        eax = 0;
        goto label_6;
label_18:
        r14 = *((rbp - 0x170));
        goto label_11;
label_27:
        rsi = *((rbp - 0x168));
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r13 = rax;
label_12:
        rax = dcgettext (0, "failed to get attributes of %s");
        r12 = rax;
        rax = errno_location ();
        rcx = r13;
        eax = 0;
        eax = error (0, *(rax), r12);
        eax = 0;
        goto label_6;
label_21:
        rsi = *((rbp - 0x168));
    } while (1);
label_26:
    rsi = *((rbp - 0x168));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot make directory %s";
    r13 = rax;
    goto label_12;
label_30:
    rsi = *((rbp - 0x168));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "setting permissions for %s";
    r13 = rax;
    goto label_12;
label_13:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x4e20 */
 
int64_t dbg_re_protect (int64_t arg1, char * arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_70h;
    int64_t var_68h;
    timespec[2] timespec;
    int64_t var_58h;
    int64_t var_50h;
    int64_t var_48h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    void * s1;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool re_protect(char const * const_dst_name,int dst_dirfd,char const * dst_relname,dir_attr * attr_list,cp_options const * x); */
    r15 = r8;
    r14d = esi;
    r13 = rdi;
    rbx = rcx;
    *((rbp - 0x68)) = rdx;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    rax = strlen (rdi);
    rcx = rsp;
    r8 = rax + 1;
    rax += 0x18;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_5;
    }
    do {
    } while (rsp != rcx);
label_5:
    edx &= 0xfff;
    if (rdx != 0) {
        goto label_6;
    }
label_4:
    rdi &= 0xfffffffffffffff0;
    rax = memcpy (rsp + 0xf, r13, r8);
    r12 = rax;
    rax = *((rbp - 0x68));
    rax -= r13;
    r13 = r12 + rax;
    if (rbx == 0) {
        goto label_7;
    }
    rax = rbp - 0x60;
    *((rbp - 0x70)) = rax;
    while (*((r15 + 0x1f)) == 0) {
label_0:
        if (*((r15 + 0x1d)) != 0) {
            goto label_8;
        }
label_1:
        r8d = *((r15 + 0x1e));
        if (r8b != 0) {
            goto label_9;
        }
label_2:
        if (*((rbx + 0x90)) != 0) {
            goto label_10;
        }
label_3:
        rax = *((rbx + 0x98));
        *((r12 + rax)) = 0x2f;
        rbx = *((rbx + 0xa0));
        if (rbx == 0) {
            goto label_7;
        }
        rax = *((rbx + 0x98));
        *((r12 + rax)) = 0;
    }
    rdx = *((rbx + 0x48));
    rax = *((rbx + 0x50));
    ecx = 0;
    rsi = r13;
    edi = r14d;
    *((rbp - 0x60)) = rdx;
    rdx = *((rbx + 0x58));
    *((rbp - 0x58)) = rax;
    rax = *((rbx + 0x60));
    *((rbp - 0x50)) = rdx;
    rdx = *((rbp - 0x70));
    *((rbp - 0x48)) = rax;
    eax = utimensat ();
    if (eax == 0) {
        goto label_0;
    }
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to preserve times for %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    r8d = 0;
    goto label_11;
label_8:
    ecx = *((rbx + 0x20));
    edx = *((rbx + 0x1c));
    r8d = 0x100;
    rsi = r13;
    edi = r14d;
    eax = fchownat ();
    if (eax == 0) {
        goto label_1;
    }
    rdi = r15;
    al = chown_failure_ok ();
    if (al == 0) {
        goto label_12;
    }
    ecx = *((rbx + 0x20));
    r8d = 0x100;
    rsi = r13;
    edi = r14d;
    edx = 0xffffffff;
    fchownat ();
    r8d = *((r15 + 0x1e));
    if (r8b == 0) {
        goto label_2;
    }
label_9:
    ecx = 0xffffffff;
    eax = copy_acl (r13, 0xffffffff, r12, ecx, *((rbx + 0x18)));
    if (eax == 0) {
        goto label_3;
    }
    r8d = 0;
    goto label_11;
label_10:
    edx = *((rbx + 0x18));
    ecx = 0x100;
    rsi = r13;
    edi = r14d;
    *((rbp - 0x68)) = r8b;
    eax = fchmodat ();
    if (eax == 0) {
        goto label_3;
    }
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    do {
        rax = dcgettext (0, "failed to preserve permissions for %s");
        r12 = rax;
        rax = errno_location ();
        rcx = r13;
        eax = 0;
        error (0, *(rax), r12);
        r8d = *((rbp - 0x68));
        goto label_11;
label_7:
        r8d = 1;
label_11:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_13;
        }
        rsp = rbp - 0x28;
        eax = r8d;
        return rax;
label_6:
        goto label_4;
label_12:
        rsi = r12;
        edi = 4;
        *((rbp - 0x68)) = al;
        rax = quotearg_style ();
        edx = 5;
        rsi = "failed to preserve ownership for %s";
        r13 = rax;
    } while (1);
label_13:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x56d0 */
 
void dbg_do_copy (int32_t arg1, int32_t arg2, uint32_t arg3, int64_t arg5) {
    uint32_t var_110h;
    uint32_t var_108h;
    int64_t var_100h;
    char * var_f8h;
    int32_t var_f4h;
    _Bool new_dst;
    _Bool copy_into_self;
    dir_attr * attr_list;
    _Bool unused;
    stat sb;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    void * s1;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    /* _Bool do_copy(int n_files,char ** file,char const * target_directory,_Bool no_target_directory,cp_options * x); */
    r13 = rsi;
    *((rbp - 0xf4)) = edi;
    *((rbp - 0x108)) = rdx;
    *((rbp - 0x100)) = r8;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = 0;
    *((rbp - 0xe2)) = 0;
    al = (rdx == 0) ? 1 : 0;
    if (eax >= edi) {
        goto label_14;
    }
    *(sb.st_mode) = 0;
    if (cl != 0) {
        goto label_15;
    }
    rdi = *((rbp - 0x108));
    if (rdi == 0) {
        goto label_16;
    }
    rsi = sb_st_dev;
    eax = target_directory_operand (rdi);
    *((rbp - 0xf8)) = eax;
    eax++;
    if (eax == 0) {
        goto label_17;
    }
    if (*((rbp - 0xf4)) != 1) {
        goto label_7;
    }
label_5:
    rax = rbp - 0xd8;
    ebx = 0;
    r14d = 1;
    *((rbp - 0x110)) = rax;
label_0:
    r15 = *((r13 + rbx*8));
    *((rbp - 0xd8)) = 0;
    if (*(obj.remove_trailing_slashes) != 0) {
        goto label_18;
    }
    rdi = r15;
    if (*(obj.parents_option) != 0) {
        goto label_19;
    }
label_1:
    rax = last_component ();
    rdi = rax;
    r12 = rax;
    rax = strlen (rdi);
    rcx = rsp;
    r8 = rax + 1;
    rax += 0x18;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_20;
    }
    do {
    } while (rsp != rcx);
label_20:
    edx &= 0xfff;
    if (rdx != 0) {
    }
    rdi &= 0xfffffffffffffff0;
    rax = memcpy (rsp + 0xf, r12, r8);
    r12 = rax;
    strip_trailing_slashes (rax);
    eax = *(r12);
    eax -= 0x2e;
    if (eax == 0) {
        eax = *((r12 + 1));
        eax -= 0x2e;
        if (eax != 0) {
            goto label_21;
        }
        eax = *((r12 + 2));
    }
label_21:
    rsi = r12;
    rdx = *((rbp - 0x110));
    rdi = *((rbp - 0x108));
    rsi += 0;
    rax = file_name_concat ();
    r12 = rax;
label_2:
    rax = rbp - 0xe1;
    r8d = *((rbp - 0xe2));
    eax = copy (r15, r12, *((rbp - 0xf8)), *((rbp - 0xd8)), r8, *((rbp - 0x100)));
    r14d &= eax;
    if (*(obj.parents_option) != 0) {
        goto label_22;
    }
label_3:
    rdi = r12;
    rbx++;
    fcn_00003670 ();
    if (*((rbp - 0xf4)) > ebx) {
        goto label_0;
    }
    do {
label_8:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_23;
        }
        rsp = rbp - 0x28;
        eax = r14d;
        return rax;
label_15:
        if (*((rbp - 0x108)) != 0) {
            goto label_24;
        }
        if (*((rbp - 0xf4)) > 2) {
            goto label_25;
        }
label_6:
        r14 = *(r13);
        r12 = *((r13 + 8));
        if (*(obj.parents_option) != 0) {
            goto label_26;
        }
        rax = *((rbp - 0x100));
        ebx = *((rbp - 0xe2));
        if (*((rax + 0x16)) != 0) {
            if (*(rax) == 0) {
                goto label_11;
            }
            eax = strcmp (r14, r12);
            if (eax != 0) {
                goto label_11;
            }
            if (bl != 0) {
                goto label_11;
            }
            if (*(sb.st_mode) == 0) {
                goto label_27;
            }
label_10:
            eax = *(sb.st_mode);
            eax &= case.0xf4aa.9;
            if (eax != 0x8000) {
                goto label_11;
            }
            r15 = *((rbp - 0x100));
            rsi = r12;
            edi = 0xffffff9c;
            edx = *(r15);
            rax = find_backup_file_name ();
            rdi = 0x0001c100;
            ecx = 0x16;
            rsi = r15;
            *(rdi) = *(rsi);
            rcx--;
            rsi += 4;
            rdi += 4;
            r12 = rax;
            rax = 0x0001c100;
            *(0x0001c100) = 0;
            *((rbp - 0x100)) = rax;
        }
label_11:
        rax = rbp - 0xd8;
        r8d = (int32_t) bl;
        r8d = -r8d;
        rcx = r12;
        eax = copy (r14, r12, 0xffffff9c, rcx, r8, *((rbp - 0x100)));
        r14d = eax;
    } while (1);
label_18:
    strip_trailing_slashes (r15);
    rdi = r15;
    if (*(obj.parents_option) == 0) {
        goto label_1;
    }
label_19:
    rax = strlen (rdi);
    rcx = rsp;
    r8 = rax + 1;
    rax += 0x18;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_28;
    }
    do {
    } while (rsp != rcx);
label_28:
    edx &= 0xfff;
    if (rdx != 0) {
    }
    rdi &= 0xfffffffffffffff0;
    rax = memcpy (rsp + 0xf, r15, r8);
    rdi = rax;
    r12 = rax;
    strip_trailing_slashes (rdi);
    rdi = *((rbp - 0x108));
    rdx = *((rbp - 0x110));
    rsi = r12;
    rax = file_name_concat ();
    ecx = 0;
    rsi = "%s -> %s\n";
    edx = *((rbp - 0xf8));
    r12 = rax;
    rax = *((rbp - 0x100));
    r9 = rbp - 0xe2;
    r8 = rbp - 0xe0;
    rdi = r12;
    if (*((rax + 0x3c)) != 0) {
        rcx = rsi;
    }
    rsi -= r12;
    al = make_dir_parents_private (rdi, *((rbp - 0xd8)), rdx, rcx, r8, r9);
    rdx = *((rbp - 0xd8));
    if (*(rdx) != 0x2f) {
        goto label_29;
    }
    rdx++;
    do {
        *((rbp - 0xd8)) = rdx;
        rdx++;
    } while (*((rdx - 1)) == 0x2f);
label_29:
    if (al != 0) {
        goto label_2;
    }
    eax = *(obj.parents_option);
    r14d = 0;
label_4:
    if (al == 0) {
        goto label_3;
    }
    r15 = *((rbp - 0xe0));
    if (r15 == 0) {
        goto label_3;
    }
    do {
        rdi = r15;
        r15 = *((r15 + 0xa0));
        *((rbp - 0xe0)) = r15;
        fcn_00003670 ();
    } while (r15 != 0);
    goto label_3;
label_22:
    eax = re_protect (r12, *((rbp - 0xf8)), *((rbp - 0xd8)), *((rbp - 0xe0)), *((rbp - 0x100)));
    r14d &= eax;
    eax = *(obj.parents_option);
    goto label_4;
label_7:
    rbx = *((rbp - 0x100));
    do {
        dest_info_init (*((rbp - 0x100)));
    } while (rcx != 0);
    src_info_init (rbx);
    goto label_5;
label_16:
    rax = *((rbp - 0xf4));
    rbx = *((rsi + rax*8 - 8));
    rsi = sb_st_dev;
    r14 = rax;
    rdi = rbx;
    *((rbp - 0x108)) = rbx;
    eax = target_directory_operand (rdi);
    *((rbp - 0xf8)) = eax;
    eax++;
    if (eax == 0) {
        goto label_30;
    }
    r14d--;
    *((rbp - 0xf4)) = r14d;
    if (rbx == 0) {
        goto label_6;
    }
    if (*((rbp - 0xf4)) > 1) {
        goto label_7;
    }
    r14d = 1;
    if (*((rbp - 0xf4)) == 1) {
        goto label_5;
    }
    goto label_8;
label_30:
    rax = errno_location ();
    r12d = *(rax);
    if (r12d == 2) {
        goto label_31;
    }
label_9:
    if (*((rbp - 0xf4)) <= 2) {
        goto label_6;
    }
    rsi = *((rbp - 0x108));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "target %s");
    rcx = r13;
    eax = 0;
    error (1, r12d, rax);
label_31:
    *((rbp - 0xe2)) = 1;
    goto label_9;
label_27:
    rsi = sb_st_dev;
    rdi = r12;
    eax = stat ();
    if (eax == 0) {
        goto label_10;
    }
    goto label_11;
label_24:
    edx = 5;
    rax = dcgettext (0, "cannot combine --target-directory (-t) and --no-target-directory (-T)");
    eax = 0;
    error (1, 0, rax);
label_25:
    rsi = *((rsi + 0x10));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
label_13:
    rax = dcgettext (0, "extra operand %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    do {
        usage (1);
label_17:
        rsi = *((rbp - 0x108));
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r13 = rax;
        rax = dcgettext (0, "target directory %s");
        r12 = rax;
        rax = errno_location ();
        rcx = r13;
        eax = 0;
        error (1, *(rax), r12);
label_23:
        stack_chk_fail ();
label_14:
        edi--;
        if (edi == 0) {
            goto label_32;
        }
        edx = 5;
label_12:
        rax = dcgettext (0, "missing file operand");
        eax = 0;
        error (0, 0, rax);
    } while (1);
label_26:
    edx = 5;
    rsi = "with --parents, the destination must be a directory";
    goto label_12;
label_32:
    rsi = *(rsi);
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "missing destination file operand after %s";
    r12 = rax;
    goto label_13;
}

/* /tmp/tmp7e2uf9hg @ 0xd2d0 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_raw_hasher ( const * data, size_t n) {
    rdi = data;
    rsi = n;
    /* size_t raw_hasher( const * data,size_t n); */
    rax = rdi;
    edx = 0;
    rax = rotate_right64 (rax, 3);
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xd2f0 */
 
int64_t dbg_raw_comparator ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    /* _Bool raw_comparator( const * a, const * b); */
    __asm ("loopne 0xd357");
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    return __asm ("loopne 0xd2bd");
}

/* /tmp/tmp7e2uf9hg @ 0xd300 */
 
uint8_t rotate_left8 (uint8_t value, uint32_t count) {
    const uint8_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
int64_t dbg_check_tuning (Hash_table * table) {
    rdi = table;
    /* _Bool check_tuning(Hash_table * table); */
    __asm ("loopne 0xd367");
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("loopne 0xd37f");
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    dl = rotate_left8 (dl, 1);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) = rotate_left8 (*(rax), cl);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    ah += cl;
    tmp_0 = edx;
    edx = eax;
    eax = tmp_0;
    *(rax) += al;
    if (*(rax) >= 0) {
        __asm ("addss xmm1, dword [0x000165f4]");
        xmm2 = *((rax + 4));
        __asm ("comiss xmm2, xmm1");
        if (*(rax) <= 0) {
            goto label_0;
        }
        xmm3 = *(0x00016600);
        __asm ("comiss xmm3, xmm2");
        if (*(rax) < 0) {
            goto label_0;
        }
        __asm ("comiss xmm0, xmm1");
        eax = 1;
        if (*(rax) > 0) {
            goto label_1;
        }
    }
label_0:
    *((rdi + 0x28)) = rdx;
    eax = 0;
    return rax;
    eax = 1;
label_1:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xd390 */
 
uint64_t hash_find_entry (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg_48h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    r14 = rdx;
    r13 = rsi;
    r12d = ecx;
    rsi = *((rdi + 0x10));
    rdi = r13;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13);
    if (rax >= *((rbp + 0x10))) {
        void (*0x3cd5)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    *(r14) = rbx;
    rsi = *(rbx);
    if (rsi == 0) {
        goto label_2;
    }
    if (rsi == r13) {
        goto label_3;
    }
    rdi = r13;
    al = uint64_t (*rbp + 0x38)() ();
    if (al == 0) {
        goto label_4;
    }
    rax = *(rbx);
label_1:
    if (r12b == 0) {
        goto label_0;
    }
    rdx = *((rbx + 8));
    if (rdx == 0) {
        goto label_5;
    }
    __asm ("movdqu xmm0, xmmword [rdx]");
    __asm ("movups xmmword [rbx], xmm0");
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
    do {
        rsi = *(rax);
        if (rsi == r13) {
            goto label_6;
        }
        rdi = r13;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_6;
        }
        rbx = *((rbx + 8));
label_4:
        rax = *((rbx + 8));
    } while (rax != 0);
label_2:
    eax = 0;
    do {
label_0:
        return rax;
label_6:
        rdx = *((rbx + 8));
        rax = *(rdx);
    } while (r12b == 0);
    rcx = *((rdx + 8));
    *((rbx + 8)) = rcx;
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
label_5:
    *(rbx) = 0;
    goto label_0;
label_3:
    rax = rsi;
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0x3cd5 */
 
void hash_find_entry_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0xd4a0 */
 
int64_t compute_bucket_size_isra_0 (uint32_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    if (sil == 0) {
        if (rdi < 0) {
            goto label_5;
        }
        xmm1 = 0;
        __asm ("cvtsi2ss xmm1, rdi");
label_2:
        __asm ("divss xmm1, xmm0");
        r8d = 0;
        __asm ("comiss xmm1, dword [0x00016604]");
        if (rdi >= 0) {
            goto label_6;
        }
        __asm ("comiss xmm1, dword [0x00016608]");
        if (rdi < 0) {
            goto label_7;
        }
        __asm ("subss xmm1, dword [0x00016608]");
        __asm ("cvttss2si rdi, xmm1");
        __asm ("btc rdi, 0x3f");
    }
label_4:
    r9 = 0xaaaaaaaaaaaaaaab;
    eax = 0xa;
    if (rdi >= rax) {
        rax = rdi;
    }
    r8 = rax;
    r8 |= 1;
    if (r8 == -1) {
        goto label_1;
    }
label_0:
    rax = r8;
    rdx:rax = rax * r9;
    rax = rdx;
    rdx &= 0xfffffffffffffffe;
    rax >>= 1;
    rdx += rax;
    rax = r8;
    rax -= rdx;
    if (r8 <= 9) {
        goto label_8;
    }
    if (rax == 0) {
        goto label_9;
    }
    edi = 0x10;
    esi = 9;
    ecx = 3;
    while (r8 > rsi) {
        rdi += 8;
        if (rdx == 0) {
            goto label_9;
        }
        rcx += 2;
        rax = r8;
        edx = 0;
        rsi += rdi;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    }
label_3:
    rax = r8;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rdx != 0) {
        goto label_10;
    }
label_9:
    r8 += 2;
    if (r8 != -1) {
        goto label_0;
    }
    do {
label_1:
        r8d = 0;
        rax = r8;
        return rax;
label_10:
        rax = r8;
        rax >>= 0x3d;
        al = (rax != 0) ? 1 : 0;
        eax = (int32_t) al;
    } while (((r8 >> 0x3c) & 1) < 0);
    if (rax != 0) {
        goto label_1;
    }
label_6:
    rax = r8;
    return rax;
label_5:
    rax = rdi;
    edi &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rdi;
    __asm ("cvtsi2ss xmm1, rax");
    __asm ("addss xmm1, xmm1");
    goto label_2;
label_8:
    ecx = 3;
    goto label_3;
label_7:
    __asm ("cvttss2si rdi, xmm1");
    goto label_4;
}

/* /tmp/tmp7e2uf9hg @ 0xd5e0 */
 
uint64_t transfer_entries (uint32_t arg_8h, int64_t arg_18h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r14 = rdi;
    r12d = edx;
    rbx = *(rsi);
    if (rbx < *((rsi + 8))) {
        goto label_3;
    }
    goto label_6;
    do {
label_2:
        rbx += 0x10;
        if (*((rbp + 8)) <= rbx) {
            goto label_6;
        }
label_3:
        r15 = *(rbx);
    } while (r15 == 0);
    r13 = *((rbx + 8));
    if (r13 == 0) {
        goto label_7;
    }
    rsi = *((r14 + 0x10));
    goto label_8;
label_0:
    rcx = *((rax + 8));
    *((r13 + 8)) = rcx;
    *((rax + 8)) = r13;
    if (rdx == 0) {
        goto label_9;
    }
label_1:
    r13 = rdx;
label_8:
    r15 = *(r13);
    rdi = *(r13);
    rax = uint64_t (*r14 + 0x30)() ();
    rsi = *((r14 + 0x10));
    if (rax >= rsi) {
        void (*0x3cda)() ();
    }
    rax <<= 4;
    rax += *(r14);
    rdx = *((r13 + 8));
    if (*(rax) != 0) {
        goto label_0;
    }
    *(rax) = r15;
    rax = *((r14 + 0x48));
    *((r14 + 0x18))++;
    *(r13) = 0;
    *((r13 + 8)) = rax;
    *((r14 + 0x48)) = r13;
    if (rdx != 0) {
        goto label_1;
    }
label_9:
    r15 = *(rbx);
label_7:
    *((rbx + 8)) = 0;
    if (r12b != 0) {
        goto label_2;
    }
    rsi = *((r14 + 0x10));
    rdi = r15;
    rax = uint64_t (*r14 + 0x30)() ();
    r13 = rax;
    if (rax >= *((r14 + 0x10))) {
        void (*0x3cda)() ();
    }
    r13 <<= 4;
    r13 += *(r14);
    if (*(r13) == 0) {
        goto label_10;
    }
    rax = *((r14 + 0x48));
    if (rax == 0) {
        goto label_11;
    }
    rdx = *((rax + 8));
    *((r14 + 0x48)) = rdx;
label_5:
    rdx = *((r13 + 8));
    *(rax) = r15;
    *((rax + 8)) = rdx;
    *((r13 + 8)) = rax;
label_4:
    *(rbx) = 0;
    rbx += 0x10;
    *((rbp + 0x18))--;
    if (*((rbp + 8)) > rbx) {
        goto label_3;
    }
label_6:
    eax = 1;
    return rax;
label_10:
    *(r13) = r15;
    *((r14 + 0x18))++;
    goto label_4;
label_11:
    rax = malloc (0x10);
    if (rax != 0) {
        goto label_5;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x3cda */
 
void transfer_entries_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3cdf */
 
void hash_lookup_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x36c0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmp7e2uf9hg @ 0x3ce4 */
 
void hash_get_first_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3cea */
 
void hash_get_next_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3cef */
 
void hash_rehash_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3cf4 */
 
void hash_insert_if_absent_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0xe7f0 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x00016667;
        rdx = 0x00016658;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0001665f;
        rdx = 0x00016661;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x00016663;
    rdx = 0x0001665c;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x13290 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x3ae0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmp7e2uf9hg @ 0xe8d0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x3cf9)() ();
    }
    rdx = 0x000166c0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x166c0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0001666b;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x00016661;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x000166ec;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x166ec */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0001665f;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x00016661;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0001665f;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x00016661;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x000167ec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x167ec */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x000168ec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x168ec */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x00016661;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0001665f;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0001665f;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x00016661;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmp7e2uf9hg @ 0x3cf9 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0xfcf0 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x3cfe)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                rdi = r14;
                *((rsp + 0x10)) = rsi;
                fcn_00003670 ();
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x3cfe */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d03 */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d09 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d0e */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d13 */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d18 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d1d */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d22 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d27 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d2c */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d31 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x4500 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x4530 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x4570 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00003680 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp7e2uf9hg @ 0x3680 */
 
void fcn_00003680 (void) {
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp7e2uf9hg @ 0x45b0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp7e2uf9hg @ 0x5d60 */
 
uint64_t dbg_subst_suffix (char *** arg1, char *** arg2, char * s) {
    rdi = arg1;
    rsi = arg2;
    rdx = s;
    /* char * subst_suffix(char const * str,char const * suffix,char const * newsuffix); */
    rsi -= rdi;
    r14 = rdx;
    r13 = rdi;
    r12 = rsi;
    strlen (rdx);
    r15 = rax + 1;
    rdi = r15 + r12;
    rax = ximalloc ();
    memcpy (rax + r12, r14, r15);
    rdx = r12;
    rsi = r13;
    rdi = rbp;
    return memcpy ();
}

/* /tmp/tmp7e2uf9hg @ 0x3820 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmp7e2uf9hg @ 0x12780 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x3ab0 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp7e2uf9hg @ 0x12ce0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x000164c5);
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3800 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b80 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b10 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmp7e2uf9hg @ 0x3740 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmp7e2uf9hg @ 0x3a80 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmp7e2uf9hg @ 0x5dc0 */
 
uint64_t dbg_write_zeros (int64_t arg1, func * arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool write_zeros(int fd,off_t n_bytes); */
    r12d = edi;
    if (*(0x0001c560) == 0) {
        goto label_1;
    }
label_0:
    if (rbp != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        rbp -= rax;
        if (rbp == 0) {
            goto label_3;
        }
label_2:
        rbx = *(0x0001c018);
        rsi = fallback.1;
        edi = r12d;
        if (rbp <= rbx) {
            rbx = rbp;
        }
        rax = full_write (rdi, rsi, rbx);
    } while (rax == rbx);
    eax = 0;
    return rax;
label_3:
    eax = 1;
    return rax;
label_1:
    rax = calloc (*(0x0001c018), 1);
    *(0x0001c560) = rax;
    if (rax != 0) {
        goto label_0;
    }
    rax = 0x0001c160;
    *(0x0001c018) = section..dynsym;
    *(0x0001c560) = rax;
    goto label_0;
}

/* /tmp/tmp7e2uf9hg @ 0x5e70 */
 
int64_t dbg_restore_default_fscreatecon_or_die (void) {
    /* void restore_default_fscreatecon_or_die(); */
    errno_location ();
    edx = 5;
    *(rax) = 0x5f;
    rbx = rax;
    rax = dcgettext (0, "failed to restore the default file creation context");
    eax = 0;
    error (1, *(rbx), rax);
}

/* /tmp/tmp7e2uf9hg @ 0x5eb0 */
 
uint64_t dbg_emit_verbose (int64_t arg1, char *** arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void emit_verbose(char const * src,char const * dst,char const * backup_dst_name); */
    r13 = rdi;
    edi = 1;
    rdx = rsi;
    esi = 4;
    rax = quotearg_n_style ();
    edi = 0;
    rdx = r13;
    esi = 4;
    r12 = rax;
    rax = quotearg_n_style ();
    rcx = r12;
    edi = 1;
    rsi = "%s -> %s";
    rdx = rax;
    eax = 0;
    printf_chk ();
    if (rbp != 0) {
        rsi = rbp;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, " (backup: %s)");
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
    }
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmp7e2uf9hg @ 0x5f70 */
 
uint64_t dbg_create_hard_link (int64_t arg_60h_2, int64_t arg_60h, int64_t arg_68h, int64_t arg1, char *** arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool replace;
    _Bool verbose;
    _Bool dereference;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* _Bool create_hard_link(char const * src_name,int src_dirfd,char const * src_relname,char const * dst_name,int dst_dirfd,char const * dst_relname,_Bool replace,_Bool verbose,_Bool dereference); */
    r15 = r9;
    r14 = rdi;
    r13 = rcx;
    rbx = rdx;
    r8d = *((rsp + 0x68));
    r12d = *((rsp + 0x60));
    r9d = *((rsp + 0x60));
    r8d <<= 0xa;
    eax = force_linkat (esi, rbx, r8d, r15, r8, r9);
    if (eax > 0) {
        goto label_2;
    }
    ebp >>= 0x1f;
    r12b &= bpl;
    if (r12b != 0) {
        goto label_3;
    }
    r12d = 1;
    do {
label_0:
        eax = r12d;
        return eax;
label_2:
        r9d = 0;
        if (r14 == 0) {
            goto label_4;
        }
label_1:
        rdx = r14;
        esi = 4;
        edi = 1;
        *((rsp + 8)) = r9;
        rax = quotearg_n_style ();
        rdx = r13;
        esi = 4;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "cannot create hard link %s to %s");
        rcx = r12;
        r8 = rbx;
        eax = 0;
        r12d = 0;
        error (0, ebp, rax);
        rdi = *((rsp + 8));
        fcn_00003670 ();
    } while (1);
label_3:
    rsi = r13;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "removed %s\n");
    rdx = r13;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    goto label_0;
label_4:
    rax = subst_suffix (r13, r15, rbx);
    r14 = rax;
    r9 = rax;
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0xb3f0 */
 
uint64_t dbg_force_linkat (int64_t arg_180h, char *** arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    link_arg arg;
    char[256] buf;
    int64_t var_4h;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_24h;
    int64_t var_30h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int force_linkat(int srcdir,char const * srcname,int dstdir,char const * dstname,int flags,_Bool force,int linkat_errno); */
    r15d = r9d;
    r14d = edi;
    r13 = rcx;
    rbx = rsi;
    rdx = *(fs:0x28);
    *((rsp + 0x138)) = rdx;
    edx = 0;
    eax = *((rsp + 0x180));
    if (eax < 0) {
        goto label_3;
    }
label_0:
    *((rsp + 4)) = r8d;
    if (r15b == 1) {
        if (eax == 0x11) {
            rax = rsp + 0x30;
            rsi = rax;
            *((rsp + 8)) = rax;
            rax = samedir_template (r13, rsi);
            r8d = *((rsp + 4));
            r15 = rax;
            if (rax == 0) {
                goto label_4;
            }
            *((rsp + 0x24)) = r8d;
            *((rsp + 0x10)) = r14d;
            *((rsp + 0x18)) = rbx;
            *((rsp + 0x20)) = ebp;
            eax = try_tempname_len (rax, 0, rsp + 0x10, dbg.try_link, 6);
            if (eax == 0) {
                goto label_5;
            }
            rax = errno_location ();
            r12d = *(rax);
label_2:
            if (r15 == *((rsp + 8))) {
                goto label_1;
            }
            rdi = r15;
            eax = fcn_00003670 ();
        }
    } else {
        r12d = eax;
    }
    do {
label_1:
        rax = *((rsp + 0x138));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        eax = r12d;
        return rax;
label_3:
        edx = ebp;
        *((rsp + 4)) = r8d;
        eax = linkat ();
        r12d = eax;
    } while (eax == 0);
    rax = errno_location ();
    r8d = *((rsp + 4));
    eax = *(rax);
    goto label_0;
label_4:
    rax = errno_location ();
    r12d = *(rax);
    goto label_1;
label_5:
    rcx = r13;
    edx = ebp;
    rsi = r15;
    edi = ebp;
    eax = renameat ();
    r12d = 0xffffffff;
    if (eax != 0) {
        rax = errno_location ();
        r12d = *(rax);
    }
    edx = 0;
    rsi = r15;
    edi = ebp;
    unlinkat ();
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x10290 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, char *** arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x3d09)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x3840 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmp7e2uf9hg @ 0x3670 */
 
void fcn_00003670 (void) {
    /* [14] -r-x section size 32 named .plt.got */
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmp7e2uf9hg @ 0x103b0 */
 
int64_t quotearg_style (uint32_t arg1, char *** arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x3d13)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x3b40 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp7e2uf9hg @ 0x60c0 */
 
void dbg_overwrite_ok (int64_t arg_14h, uint32_t arg_18h, int64_t arg1, char *** arg2, char *** arg3, char *** arg4, uint32_t arg5) {
    char[12] perms;
    char * bp;
    int64_t var_15h;
    int64_t var_16h;
    int64_t var_18h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h_2;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool overwrite_ok(cp_options const * x,char const * dst_name,int dst_dirfd,char const * dst_relname,stat const * dst_sb); */
    r14 = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = *((r8 + 0x18));
    eax &= case.0xf4aa.9;
    while (al != 0) {
label_0:
        rsi = r14;
        edi = 4;
        rax = quotearg_style ();
        r12 = program_name;
        edx = 5;
        rbx = rax;
        rax = dcgettext (0, "%s: overwrite %s? ");
        r8 = rbx;
        rcx = r12;
        esi = 1;
        rdi = stderr;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        void (*0x12d20)() ();
        r12d = edx;
        r13 = rcx;
        rbx = r8;
        al = can_write_any_file ();
    }
    ecx = 0x200;
    edx = 2;
    rsi = r13;
    edi = r12d;
    eax = faccessat ();
    if (eax == 0) {
        goto label_0;
    }
    strmode (*((rbx + 0x18)), rsp + 0xc);
    r12d = *((rbx + 0x18));
    rsi = r14;
    edi = 4;
    *((rsp + 0x16)) = 0;
    rax = quotearg_style ();
    r12d &= 0xfff;
    r13 = program_name;
    rbx = rax;
    if (*((rbp + 0x18)) == 0) {
        if ((*((rbp + 0x14)) & 0xffff00) == 0) {
            goto label_3;
        }
    }
    edx = 5;
    rax = dcgettext (0, "%s: replace %s, overriding mode %04lo (%s)? ");
    rdx = rax;
    do {
        r9 = r12;
        r8 = rbx;
        rcx = r13;
        rax = rsp + 0x15;
        rdi = stderr;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        goto label_1;
label_3:
        edx = 5;
        rax = dcgettext (0, "%s: unwritable %s (mode %04lo, %s); try anyway? ");
        rdx = rax;
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x6240 */
 
uint64_t dbg_create_hole (int64_t arg1, char *** arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool create_hole(int fd,char const * name,_Bool punch_holes,off_t size); */
    r13d = edi;
    r12 = rcx;
    rsi = rcx;
    ebx = edx;
    edx = 1;
    rax = lseek ();
    if (rax < 0) {
        goto label_2;
    }
    while (eax >= 0) {
label_0:
        r12d = 1;
label_1:
        eax = r12d;
        return rax;
        rax -= r12;
        rcx = r12;
        esi = 3;
        edi = r13d;
        rdx = rax;
        eax = fallocate ();
    }
    rax = errno_location ();
    rbx = rax;
    eax = *(rax);
    r12b = (eax == 0x5f) ? 1 : 0;
    al = (eax == 0x26) ? 1 : 0;
    r12b |= al;
    if (r12b != 0) {
        goto label_0;
    }
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "error deallocating %s");
    rcx = r13;
    eax = 0;
    error (0, *(rbx), rax);
    goto label_1;
label_2:
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot lseek %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    r12d = 0;
    error (0, *(rax), r12);
    eax = r12d;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x6350 */
 
uint64_t dbg_sparse_copy (int64_t arg_c0h, int64_t arg_c8h, int64_t arg_d0h, int64_t arg_d8h, int64_t arg_e0h, int64_t arg_e8h, int64_t arg1, int64_t arg2, int64_t arg3, size_t arg4, uint32_t arg5, int64_t arg6) {
    _Bool allow_reflink;
    char const * src_name;
    char const * dst_name;
    off_t * total_n_read;
    _Bool * last_write_made_hole;
    int64_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    uint32_t var_20h;
    size_t nbyte;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_3eh;
    int64_t var_3fh;
    int64_t var_40h;
    int64_t var_48h;
    ssize_t var_50h;
    uint32_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t fildes;
    int64_t var_7ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* _Bool sparse_copy(int src_fd,int dest_fd,char ** abuf,size_t buf_size,size_t hole_size,_Bool punch_holes,_Bool allow_reflink,char const * src_name,char const * dst_name,uintmax_t max_n_read,off_t * total_n_read,_Bool * last_write_made_hole); */
    r14d = esi;
    rsi = *((rsp + 0xc8));
    *((rsp + 0x48)) = rdx;
    *((rsp + 0x28)) = rcx;
    rdx = *((rsp + 0xd8));
    *((rsp + 0x60)) = rsi;
    rsi = *((rsp + 0xd0));
    rcx = *((rsp + 0xe8));
    eax = *((rsp + 0xc0));
    *((rsp + 0x78)) = edi;
    *((rsp + 0x30)) = rsi;
    rsi = *((rsp + 0xe0));
    *(rcx) = 0;
    *((rsp + 0x58)) = r8;
    *((rsp + 0x7c)) = r9d;
    *((rsp + 0x20)) = rdx;
    *((rsp + 0x40)) = rsi;
    *((rsp + 0x70)) = rcx;
    *((rsp + 0x3f)) = r9b;
    *(rsi) = 0;
    if (r8 != 0) {
        goto label_15;
    }
    if (al == 0) {
        goto label_15;
    }
    if (rdx == 0) {
        goto label_11;
    }
    ebx = 1;
    r13d = edi;
    r12 = rdx;
    do {
label_7:
        r8 = 0x7fffffffc0000000;
        r9d = 0;
        edx = r14d;
        edi = r13d;
        if (r12 <= r8) {
            r8 = r12;
        }
        ecx = 0;
        esi = 0;
        rax = copy_file_range ();
        if (rax == 0) {
            goto label_16;
        }
        if (rax < 0) {
            goto label_17;
        }
        rdi = *((rsp + 0x40));
        *(rdi) += rax;
        r12 -= rax;
    } while (r12 != 0);
    do {
label_11:
        r15d = 1;
        goto label_2;
label_15:
    } while (*((rsp + 0x20)) == 0);
label_8:
    rax = *((rsp + 0x58));
    rdi = *((rsp + 0x28));
    *((rsp + 0x38)) = r14d;
    if (rax != 0) {
        rdi = rax;
    }
    r15d = 0;
    r12d = 0;
    r14d = r15d;
    r15 = r12;
    *((rsp + 0x68)) = rdi;
    do {
        rax = *((rsp + 0x48));
        r11 = *(rax);
        if (r11 == 0) {
            goto label_18;
        }
label_3:
        rax = *((rsp + 0x20));
        rdx = *((rsp + 0x28));
        rsi = r11;
        *((rsp + 8)) = r11;
        edi = *((rsp + 0x78));
        if (rax <= rdx) {
            rdx = rax;
        }
        rax = read (rdi, rsi, rdx);
        r11 = *((rsp + 8));
        *((rsp + 0x50)) = rax;
        if (rax >= 0) {
            goto label_19;
        }
        rax = errno_location ();
        rbx = rax;
    } while (*(rax) == 4);
    rsi = *((rsp + 0x60));
    edi = 4;
    r15d = 0;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "error reading %s");
    rcx = r12;
    eax = 0;
    error (0, *(rbx), rax);
label_2:
    eax = r15d;
    return rax;
    if (*(rax) == 4) {
label_19:
        goto label_20;
    }
    rax = *((rsp + 0x40));
    rbp = *((rsp + 0x50));
    ecx = r14d;
    r14 = r11;
    rbx = *((rsp + 0x68));
    *(rax) += rbp;
    rsp + 8 = (*((rsp + 0x58)) != 0) ? 1 : 0;
label_0:
    r12 = rbp;
    if (rbx > rbp) {
        rbx = rbp;
    }
    r13b = (rbx != 0) ? 1 : 0;
    r13b &= *((rsp + 8));
    if (r13b == 0) {
        goto label_21;
    }
    rsi = r14;
    rdx = rbx;
    do {
        if (*(rsi) != 0) {
            goto label_22;
        }
        rsi++;
        rdx--;
        if (rdx == 0) {
            goto label_23;
        }
    } while ((dl & 0xf) != 0);
    *((rsp + 0x18)) = r11;
    *((rsp + 0x14)) = cl;
    eax = memcmp (r14, rsi, rdx);
    ecx = *((rsp + 0x14));
    r11 = *((rsp + 0x18));
    r13b = (eax == 0) ? 1 : 0;
    edx = r13d;
    edx ^= ecx;
    al = (eax != 0) ? 1 : 0;
label_10:
    r9b = (r15 != 0) ? 1 : 0;
    r9d &= edx;
    if (rbx != rbp) {
        goto label_24;
    }
    if (al == 0) {
        goto label_24;
    }
    if (r9b == 0) {
        goto label_25;
    }
    *((rsp + 0x14)) = 1;
    r13d = 0;
label_14:
    *((rsp + 0x18)) = cl;
    if (cl != 0) {
        goto label_26;
    }
    do {
        *((rsp + 0x3e)) = r9b;
        rax = full_write (*((rsp + 0x38)), r11, r15);
        r9d = *((rsp + 0x3e));
        ecx = *((rsp + 0x18));
        if (r15 != rax) {
            goto label_27;
        }
label_1:
        eax = *((rsp + 0x14));
        if (eax == 0) {
            goto label_28;
        }
        if (rbx == 0) {
            goto label_29;
        }
        if (r9b == 0) {
            goto label_30;
        }
        r15 = rbx;
        ecx = r13d;
        r11 = r14;
        ebx = 0;
        goto label_0;
label_21:
        if (rbx != rbp) {
            goto label_31;
        }
        if (cl == 1) {
            goto label_31;
        }
label_4:
        r15 += rbx;
        r9d = 0;
        *((rsp + 0x18)) = cl;
        *((rsp + 0x14)) = 1;
    } while (cl == 0);
label_26:
    edx = *((rsp + 0x3f));
    *((rsp + 0x18)) = r9b;
    al = create_hole (*((rsp + 0x38)), *((rsp + 0x30)), rdx, r15);
    r9d = *((rsp + 0x18));
    if (al != 0) {
        goto label_1;
    }
    r15d = eax;
    goto label_2;
label_23:
    r9d = ecx;
    r9d ^= 1;
    al = (r15 != 0) ? 1 : 0;
    r9d &= eax;
label_24:
    if (r9b != 0) {
        goto label_32;
    }
label_5:
    rax = 0x8000000000000000;
    r15 += rbx;
    rax += r15;
    if (rax < rbx) {
        goto label_33;
    }
    rbp -= rbx;
    r14 += rbx;
    ecx = r13d;
label_6:
    if (rbp != 0) {
        goto label_0;
    }
label_9:
    rdi = *((rsp + 0x50));
    rax = *((rsp + 0x70));
    *(rax) = r13b;
    if (rbp == 0) {
        goto label_34;
    }
    rax = *((rsp + 0x48));
    r14d = r13d;
    r11 = *(rax);
    if (r11 != 0) {
        goto label_3;
    }
label_18:
    eax = getpagesize ();
    rsi = *((rsp + 0x28));
    rdi = (int64_t) eax;
    rax = xalignalloc ();
    r11 = rax;
    rax = *((rsp + 0x48));
    *(rax) = r11;
    goto label_3;
label_31:
    r13d = ecx;
    if (rbx == 0) {
        goto label_4;
    }
    goto label_5;
label_28:
    r11 = r14;
    rbp -= rbx;
    ecx = r13d;
    r14 += rbx;
    r15 = rbx;
    goto label_6;
label_29:
    if (r9b != 0) {
        goto label_35;
    }
    r12d = 0;
label_30:
    r11 = r14;
    ecx = r13d;
    r14 += rbx;
    rbp -= rbx;
    r15d = 0;
    goto label_6;
label_17:
    rax = errno_location ();
    ecx = *(rax);
    if (ecx == 0x26) {
        goto label_36;
    }
    if (ecx > 0x1a) {
        goto label_37;
    }
    if (ecx > 0) {
        rax = rbx;
        rax <<= cl;
        if ((eax & 0x4440200) != 0) {
            goto label_36;
        }
        if (ecx == 1) {
            goto label_38;
        }
    }
    if (ecx == 4) {
        goto label_7;
    }
    goto label_12;
label_37:
    *((rsp + 0x20)) = r12;
    if (ecx == 0x5f) {
        goto label_8;
    }
label_12:
    rdx = *((rsp + 0x30));
    esi = 4;
    edi = 1;
    r15d = 0;
    rax = quotearg_n_style ();
    rdx = *((rsp + 0x60));
    esi = 4;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "error copying %s to %s");
    r8 = rbx;
    rcx = r12;
    eax = 0;
    error (0, *(rbp), rax);
    goto label_2;
label_35:
    r15d = 0;
    goto label_9;
label_36:
    *((rsp + 0x20)) = r12;
    goto label_8;
label_33:
    rsi = *((rsp + 0x60));
    edi = 4;
    r15d = 0;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "overflow reading %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_2;
label_22:
    eax = r13d;
    edx = ecx;
    r13d = 0;
    goto label_10;
label_27:
    rsi = *((rsp + 0x30));
    edi = 4;
    r15d = ecx;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "error writing %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    goto label_2;
label_16:
    rax = *((rsp + 0x40));
    *((rsp + 0x20)) = r12;
    if (*(rax) == 0) {
        goto label_8;
    }
    goto label_11;
label_38:
    rax = *((rsp + 0x40));
    *((rsp + 0x20)) = r12;
    if (*(rax) != 0) {
        goto label_12;
    }
    goto label_8;
label_34:
    r14d = *((rsp + 0x38));
    r8d = r13d;
    r12 = r15;
label_13:
    if (r8b == 0) {
        goto label_11;
    }
    edx = *((rsp + 0x7c));
    rsi = *((rsp + 0x30));
    rcx = r12;
    edi = r14d;
    void (*0x6240)() ();
label_20:
    r12 = r15;
    r15d = r14d;
    r14d = *((rsp + 0x38));
    r8d = r15d;
    goto label_13;
label_25:
    r13d = 0;
    goto label_4;
label_32:
    *((rsp + 0x14)) = 0;
    goto label_14;
}

/* /tmp/tmp7e2uf9hg @ 0x69a0 */
 
int64_t set_owner_isra_0 (int64_t arg_40h, int64_t arg_48h, uint32_t arg_50h, int64_t arg_58h, uint32_t arg1, char *** arg2, char *** arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r14 = rcx;
    r13d = edx;
    r12 = rsi;
    rbx = rdi;
    if (*((rsp + 0x50)) != 0) {
        goto label_4;
    }
    rax = 0xff0000000000ff;
    rax &= *((rdi + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rax = *((rsp + 0x58));
    edx = *((rax + 0x18));
label_0:
    eax = r9d;
    eax = ~eax;
    ah |= 0xe;
    eax &= edx;
    if ((eax & 0xfff) == 0) {
        goto label_4;
    }
    r9d &= edx;
    edx &= 0x1c0;
    eax = qset_acl (r12, ebp, r9d);
    if (eax != 0) {
        goto label_6;
    }
    do {
label_4:
        if (ebp == 0xffffffff) {
            goto label_7;
        }
        edx = *((rsp + 0x48));
        esi = *((rsp + 0x40));
        edi = ebp;
        eax = fchown ();
        if (eax == 0) {
            goto label_8;
        }
        rax = errno_location ();
        r13d = *(rax);
        r15 = rax;
        if (r13d == 1) {
            goto label_9;
        }
        if (r13d == 0x16) {
            goto label_9;
        }
label_1:
        rsi = r12;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "failed to preserve ownership for %s");
        rcx = r12;
        eax = 0;
        error (0, *(r15), rax);
label_3:
        eax = *((rbx + 0x32));
        eax = -eax;
        return rax;
label_5:
    } while (*((rdi + 0x39)) == 0);
    rax = *((rsp + 0x58));
    r9d = *((rdi + 0x10));
    edx = *((rax + 0x18));
    goto label_0;
    do {
label_8:
        eax = 1;
        return rax;
label_7:
        ecx = *((rsp + 0x48));
        edx = *((rsp + 0x40));
        rsi = r14;
        edi = r13d;
        r8d = 0x100;
        eax = fchownat ();
    } while (eax == 0);
    rax = errno_location ();
    ebp = *(rax);
    r15 = rax;
    if (ebp == 1) {
        goto label_10;
    }
    if (ebp != 0x16) {
        goto label_1;
    }
label_10:
    ecx = *((rsp + 0x48));
    r8d = 0x100;
    rsi = r14;
    edi = r13d;
    edx = 0xffffffff;
    eax = fchownat ();
    *(r15) = ebp;
label_2:
    if (*((rbx + 0x1a)) != 0) {
        goto label_1;
    }
    eax = 0;
    return rax;
label_9:
    edx = *((rsp + 0x48));
    esi = 0xffffffff;
    edi = ebp;
    fchown ();
    *(r15) = r13d;
    goto label_2;
label_6:
    rax = errno_location ();
    eax = *(rax);
    if (eax == 1) {
        goto label_11;
    }
    while (*((rbx + 0x1b)) != 0) {
        rsi = r12;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "clearing permissions for %s");
        rcx = r12;
        eax = 0;
        error (0, *(rbp), rax);
        goto label_3;
label_11:
    }
    goto label_3;
}

/* /tmp/tmp7e2uf9hg @ 0x6e20 */
 
int64_t dbg_copy_internal (int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, char *** arg2, char *** arg4, int64_t arg5, int64_t arg6, stat * dst_lstat_sb) {
    int64_t var_3d0h;
    int64_t var_3c4h;
    uint32_t var_3c0h;
    int64_t var_3b8h;
    uint32_t var_3b0h;
    func * var_3a8h;
    int64_t mode;
    int64_t var_398h;
    func * fini;
    int64_t var_390h;
    int64_t var_388h;
    void * s2;
    func main;
    int64_t errname;
    func * rtld_fini;
    uint32_t var_360h;
    func * var_358h;
    func * format;
    size_t var_348h;
    uint32_t var_340h;
    char *** s;
    char *** init;
    uint32_t var_328h;
    char *** ubp_av;
    int64_t var_31ch;
    size_t s1;
    _Bool wrote_hole_at_eof;
    _Bool read_hole;
    char * buf;
    off_t n_read;
    timespec[2] timespec;
    int64_t var_298h;
    int64_t var_290h;
    int64_t var_288h;
    stat src_sb;
    int64_t var_230h;
    int64_t var_220h;
    stat dst_sb;
    stat sb;
    int64_t var_110h;
    int64_t var_100h;
    int64_t var_f0h;
    int64_t var_e0h;
    stat src_open_sb;
    int64_t var_80h;
    int64_t var_70h;
    int64_t var_60h;
    int64_t var_50h;
    char[1] dummy;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    int64_t var_fh;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    rdx = dst_lstat_sb;
    /* _Bool copy_internal(char const * src_name,char const * dst_name,int dst_dirfd,char const * dst_relname,int nonexistent_dst,stat const * parent,dir_list * ancestors,cp_options const * x,_Bool command_line_arg,_Bool * first_dir_created_per_command_line_arg,_Bool * copy_into_self,_Bool * rename_succeeded); */
label_20:
    r15 = rdi;
    rax = *((rbp + 0x10));
    rdi = *((rbp + 0x38));
    *((rbp - 0x330)) = rsi;
    rsi = *((rbp + 0x28));
    *((rbp - segment.UNKNOWN)) = rcx;
    *((rbp - 0x358)) = rax;
    rcx = *((rbp + 0x30));
    eax = *((rbp + 0x20));
    *((rbp - 0x320)) = edx;
    *((rbp - 0x370)) = r9;
    rbx = *((rbp + 0x18));
    *((rbp - 0x328)) = eax;
    *((rbp - 0x350)) = rsi;
    r13d = *((rbx + 0x40));
    *((rbp - segment.NOTE_1)) = rcx;
    *((rbp - 0x340)) = rdi;
    *((rbp - 0x360)) = al;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    *(rcx) = 0;
    eax = *((rbx + 0x18));
    *((rbp - segment.INTERP)) = al;
    if (al != 0) {
        goto label_137;
    }
    rbp - segment.INTERP = (r8d > 0) ? 1 : 0;
    if (r13d == 0) {
        goto label_12;
    }
    do {
        if (r13d == 0x11) {
            goto label_138;
        }
label_2:
        r12 = r15;
        rsi = r15;
        edi = 0xffffff9c;
label_0:
        ecx = 0;
        r9 = src_sb_st_dev;
        *((rbp - 0x31c)) = r8d;
        cl = (*((rbx + 4)) == 2) ? 1 : 0;
        rdx = r9;
        ecx <<= 8;
        eax = fstatat ();
        r8d = *((rbp - 0x31c));
        rsi = r12;
        if (eax != 0) {
            goto label_139;
        }
        eax = *(src_sb.st_mode);
        *((rbp - 0x31c)) = eax;
        eax &= case.0xf4aa.9;
        if (eax == 0x4000) {
            goto label_140;
        }
label_1:
        if (*((rbp - 0x328)) != 0) {
            rdi = *((rbx + 0x50));
            if (rdi == 0) {
                goto label_141;
            }
            eax = *((rbp - 0x31c));
            r9 = src_sb_st_dev;
            eax &= case.0xf4aa.9;
            if (eax != 0x4000) {
                r10d = *(rbx);
                if (r10d == 0) {
                    goto label_142;
                }
            }
label_13:
            *((rbp - 0x348)) = r8d;
            record_file (rdi, r15, r9, rcx, r8, r9);
            r8d = *((rbp - 0x348));
        }
label_141:
        eax = *((rbx + 4));
        if (eax == 4) {
            goto label_143;
        }
        if (eax == 3) {
            if (*((rbp - 0x328)) != 0) {
                goto label_144;
            }
        }
        *((rbp - 0x378)) = 0;
label_6:
        *((rbp - 0x348)) = 0;
        r14d = 0;
        if (r8d <= 0) {
            goto label_145;
        }
label_17:
        if (*((rbp - 0x328)) != 0) {
            goto label_146;
        }
label_3:
        if (*((rbx + 0x3c)) != 0) {
            if (*((rbx + 0x18)) == 0) {
                goto label_147;
            }
        }
label_11:
        if (r13d == 0) {
            goto label_148;
        }
label_4:
        if (*((rbx + 0x38)) == 0) {
            goto label_149;
        }
        eax = *((rbp - 0x31c));
        eax &= case.0xf4aa.9;
        if (eax != 0x4000) {
            goto label_149;
        }
        rdx = *(src_sb.st_dev);
        rdi = *(src_sb.st_ino);
        if (*((rbp - 0x328)) != 0) {
            goto label_150;
        }
        rax = src_to_dest_lookup (rdi, rdx);
        r14 = rax;
label_30:
        if (r14 != 0) {
label_33:
            al = same_nameat (0xffffff9c, r15, *((rbp - 0x320)), r14);
            if (al != 0) {
                goto label_151;
            }
            al = same_nameat (*((rbp - 0x320)), *((rbp - segment.UNKNOWN)), edi, r14);
            if (al != 0) {
                goto label_152;
            }
            eax = *((rbx + 4));
            if (eax == 4) {
                goto label_32;
            }
            if (eax != 3) {
                goto label_153;
            }
            if (*((rbp - 0x328)) == 0) {
                goto label_153;
            }
        }
label_32:
        eax = *((rbx + 0x18));
        goto label_154;
label_137:
        if (r13d < 0) {
            goto label_155;
        }
label_25:
        rdx = *((rbp - 0x340));
        rbp - segment.INTERP = (r13d == 0) ? 1 : 0;
        r8d = *((rbp - segment.INTERP));
        *(rdx) = r8b;
    } while (r13d != 0);
label_12:
    if (*((rbx + 0x3f)) == 0) {
        edi = *((rbp - 0x320));
        r12 = *((rbp - 0x330));
        r13d = 0;
        rsi = *((rbp - segment.UNKNOWN));
        goto label_0;
    }
    *((rbp - 0x31c)) = 0;
    r13d = 0;
    goto label_1;
label_138:
    if (*((rbx + 8)) != 2) {
        goto label_2;
    }
    *((rbp - 0x31c)) = 0;
    goto label_1;
label_149:
    eax = *((rbx + 0x18));
    if (al == 0) {
        goto label_156;
    }
    if (*(src_sb.st_nlink) == 1) {
        goto label_157;
    }
    if (*((rbx + 0x30)) != 0) {
        if (*((rbx + 0x17)) == 0) {
            goto label_158;
        }
    }
    r14d = 0;
label_9:
    if (r13d == 0x11) {
        goto label_159;
    }
label_24:
    if (r13d == 0x16) {
        goto label_160;
    }
    if (r13d != 0x12) {
        goto label_161;
    }
    r12d = *((rbp - 0x31c));
    edx = 0;
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    r12d &= case.0xf4aa.9;
    dl = (r12d == 0x4000) ? 1 : 0;
    edx <<= 9;
    eax = unlinkat ();
    if (eax != 0) {
        rax = errno_location ();
        r13 = rax;
        if (*(rax) != 2) {
            goto label_162;
        }
    }
    al = (r12d != 0x4000) ? 1 : 0;
    al &= *((rbx + 0x3c));
    *((rbp - segment.INTERP)) = al;
    if (al != 0) {
        goto label_163;
    }
    *((rbp - segment.INTERP)) = 1;
    r13d = 1;
    goto label_10;
label_144:
    *((rbp - 0x378)) = 1;
    if (r8d <= 0) {
        goto label_145;
    }
    *((rbp - 0x348)) = 0;
    r14d = 0;
label_146:
    if (*((rbx + 0x48)) == 0) {
        goto label_3;
    }
    r12d = *((rbx + 0x18));
    if (r12b != 0) {
        goto label_3;
    }
    ecx = *(rbx);
    if (ecx != 0) {
        goto label_164;
    }
    rdx = dst_sb_st_dev;
    if (r14b == 0) {
        goto label_165;
    }
label_41:
    eax = *((rdx + 0x18));
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
        goto label_3;
    }
    al = seen_file (*((rbx + 0x48)), *((rbp - segment.UNKNOWN)), rdx, rcx, r8, r9);
    if (al == 0) {
        goto label_3;
    }
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = r15;
    edi = 0;
    esi = 4;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "will not copy %s through just-created symlink %s");
    r8 = rbx;
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    goto label_7;
label_145:
    if (r13d == 0x11) {
        goto label_166;
    }
label_19:
    eax = *((rbp - 0x31c));
    eax &= case.0xf4aa.9;
    if (eax == 0x8000) {
        goto label_167;
    }
    r14b = (eax == 0xa000) ? 1 : 0;
    al = (eax == 0x4000) ? 1 : 0;
    r14d |= eax;
    eax = *((rbx + 0x14));
    eax ^= 1;
    r14b |= al;
    if (r14b == 0) {
        goto label_167;
    }
label_15:
    r8d = 0x100;
label_16:
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    rdx = dst_sb_st_dev;
    ecx = r8d;
    eax = fstatat ();
    if (eax != 0) {
        goto label_168;
    }
label_18:
    if (*((rbx + 8)) == 2) {
        goto label_61;
    }
    rax = *(dst_sb.st_ino);
    if (*(src_sb.st_ino) == rax) {
        goto label_169;
    }
label_37:
    if (*((rbx + 4)) != 2) {
        goto label_61;
    }
    edx = 0;
    r9d = 0;
label_85:
    eax = *(src_sb.st_mode);
    r12 = dst_sb_st_dev;
    r8 = src_sb_st_dev;
    eax &= case.0xf4aa.9;
    if (eax == 0xa000) {
        goto label_170;
    }
label_39:
    esi = *(rbx);
    if (esi == 0) {
        goto label_171;
    }
    if (dl != 0) {
        goto label_172;
    }
    if (*((rbx + 0x18)) != 0) {
        goto label_61;
    }
    if (*((rbx + 4)) == 2) {
        goto label_61;
    }
    eax = *((r8 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
        goto label_61;
    }
    eax = *((r12 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax == 0xa000) {
        goto label_61;
    }
label_62:
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = r15;
    esi = 4;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
label_80:
    rax = dcgettext (0, "%s and %s are the same file");
    r8 = rbx;
    rcx = r12;
label_126:
    eax = 0;
    error (0, 0, rax);
label_5:
    r12d = 0;
    do {
label_7:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_173;
        }
        rsp = rbp - 0x28;
        eax = r12d;
        return rax;
label_148:
        if (*((rbx + 0x18)) == 0) {
            goto label_8;
        }
label_23:
        if (*((rbx + 0x3c)) != 0) {
            goto label_174;
        }
label_45:
        if (*((rbx + 0x28)) != 0) {
            rdx = rbx;
            set_file_security_ctx (*((rbp - 0x330)), 1);
        }
        rax = *((rbp - 0x340));
        if (rax != 0) {
            *(rax) = 1;
        }
        if (*((rbp - 0x328)) != 0) {
            if (*((rbx + 0x3f)) == 0) {
                goto label_175;
            }
        }
label_22:
        r12d = 1;
    } while (1);
label_164:
    if (*((rbx + 0x3c)) != 0) {
        goto label_147;
    }
    if (r13d != 0) {
        goto label_4;
    }
label_8:
    r13d = *((rbp - segment.INTERP));
    r14d = 0;
label_10:
    r12d = *((rbp - 0x31c));
    r12d &= 0xfff;
    if (*((rbx + 0x39)) != 0) {
        r12d = *((rbx + 0x10));
        r12d &= 0xfff;
    }
    if (*((rbx + 0x1d)) == 0) {
        eax = *((rbp - 0x31c));
        eax &= case.0xf4aa.9;
        *((rbp - 0x340)) = eax;
        if (eax == 0x4000) {
            goto label_176;
        }
        edx = *((rbp - 0x31c));
        r8 = rbx;
        ecx = r13d;
        rdi = r15;
        rsi = *((rbp - 0x330));
        al = set_process_security_ctx ();
        *((rbp - 0x380)) = 0;
        if (al != 0) {
            goto label_177;
        }
        goto label_5;
label_143:
        *((rbp - 0x378)) = 1;
        goto label_6;
    }
    edx = *((rbp - 0x31c));
    r8 = rbx;
    ecx = r13d;
    rdi = r15;
    rsi = *((rbp - 0x330));
    al = set_process_security_ctx ();
    if (al == 0) {
        goto label_5;
    }
    eax = r12d;
    eax &= 0x3f;
    *((rbp - 0x380)) = eax;
    eax = *((rbp - 0x31c));
    eax &= case.0xf4aa.9;
    *((rbp - 0x340)) = eax;
    if (eax == 0x4000) {
        goto label_178;
    }
label_177:
    r13d = *((rbx + 0x3a));
    if (r13b == 0) {
        goto label_179;
    }
    if (*(r15) != 0x2f) {
        rdi = *((rbp - segment.UNKNOWN));
        rax = dir_name ();
        r12 = rax;
        if (*((rbp - 0x320)) != 0xffffff9c) {
            goto label_180;
        }
        if (*(rax) != 0x2e) {
            goto label_180;
        }
        if (*((rax + 1)) != 0) {
            goto label_180;
        }
label_31:
        rdi = r12;
        fcn_00003670 ();
    }
    ecx = *((rbx + 0x16));
    eax = force_symlinkat (r15, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)), rcx, 0xffffffff);
    r9d = 0;
    r12d = eax;
    if (eax > 0) {
        goto label_181;
    }
label_27:
    r8b = (*((rbp - 0x340)) != 0x4000) ? 1 : 0;
    if (*((rbp - segment.INTERP)) == 0) {
        eax = *((rbx + 0x14));
        eax ^= 1;
        al &= r8b;
        r12d = eax;
        if (al != 0) {
            goto label_182;
        }
    }
label_36:
    eax = *((rbp - 0x328));
    if (al == 0) {
        goto label_183;
    }
    *((rbp - 0x3a8)) = 0;
    r12d = eax;
    *((rbp - 0x360)) = 0;
label_48:
    if (*((rbx + 0x48)) != 0) {
        rsi = *((rbp - segment.UNKNOWN));
        edi = *((rbp - 0x320));
        r14 = src_open_sb_st_dev;
        ecx = 0x100;
        rdx = r14;
        *((rbp - 0x340)) = r8b;
        *((rbp - 0x328)) = r9b;
        eax = fstatat ();
        r9d = *((rbp - 0x328));
        r8d = *((rbp - 0x340));
        if (eax != 0) {
            goto label_26;
        }
        rax = record_file (*((rbx + 0x48)), *((rbp - segment.UNKNOWN)), r14, rcx, r8, r9);
        r9d = *((rbp - 0x328));
        r8d = *((rbp - 0x340));
    }
label_26:
    if (*((rbx + 0x17)) == 0) {
        goto label_184;
    }
    if (r8b != 0) {
        goto label_7;
    }
label_184:
    if (r9b != 0) {
        goto label_7;
    }
label_21:
    if (*((rbx + 0x1f)) == 0) {
        goto label_185;
    }
    rax = *(src_sb.st_atim);
    ecx = (int32_t) r13b;
    rsi = *((rbp - segment.UNKNOWN));
    rdx = src_open_sb_st_dev;
    edi = *((rbp - 0x320));
    ecx <<= 8;
    *(src_open_sb.st_dev) = rax;
    rax = *((rbp - 0x230));
    *(src_open_sb.st_ino) = rax;
    rax = *(src_sb.st_mtim);
    *(src_open_sb.st_nlink) = rax;
    rax = *((rbp - 0x220));
    *(src_open_sb.st_mode) = rax;
    eax = utimensat ();
    if (eax == 0) {
        goto label_185;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x328)) = rax;
    rax = dcgettext (0, "preserving times for %s");
    r14 = rax;
    rax = errno_location ();
    rcx = *((rbp - 0x328));
    eax = 0;
    error (0, *(rax), r14);
    if (*((rbx + 0x32)) != 0) {
        goto label_5;
    }
label_185:
    if (r13b != 0) {
        goto label_7;
    }
    if (*((rbx + 0x1d)) == 0) {
        goto label_89;
    }
    eax = *(src_sb.st_uid);
    edx = *(src_sb.st_gid);
    if (*((rbp - segment.INTERP)) == 0) {
        if (*(dst_sb.st_uid) == eax) {
            goto label_186;
        }
    }
label_88:
    rcx = dst_sb_st_dev;
    r9d = *(src_sb.st_mode);
    rsi = *((rbp - 0x330));
    rdi = rbx;
    ecx = *((rbp - segment.INTERP));
    r8d = 0xffffffff;
    rcx = *((rbp - segment.UNKNOWN));
    edx = *((rbp - 0x320));
    eax = set_owner_isra_0 ();
    if (eax == 0xffffffff) {
        goto label_5;
    }
    esi = *((rbp - 0x31c));
    edx = *((rbp - 0x31c));
    dh &= 0xf1;
    if (eax != 0) {
        edx = esi;
    }
    *((rbp - 0x31c)) = edx;
label_89:
    rax = 0xff0000000000ff;
    rax &= *((rbx + 0x18));
    if (rax != 0) {
        goto label_187;
    }
    if (*((rbx + 0x39)) != 0) {
        goto label_188;
    }
    eax = *((rbp - segment.INTERP));
    al &= *((rbx + 0x20));
    if (al != 0) {
        goto label_189;
    }
    eax = *((rbp - 0x380));
    if (eax != 0) {
        goto label_190;
    }
label_82:
    if (*((rbp - 0x360)) == 0) {
        goto label_7;
    }
label_83:
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    ecx = 0x100;
    edx = *((rbp - 0x3a8));
    edx |= *((rbp - 0x380));
    eax = fchmodat ();
    if (eax == 0) {
        goto label_7;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "preserving permissions for %s");
    r13 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    al = error (0, *(rax), r13);
label_59:
    if (*((rbx + 0x32)) == 0) {
        goto label_7;
    }
    goto label_5;
label_156:
    if (*((rbx + 0x30)) == 0) {
        goto label_8;
    }
    if (*((rbx + 0x17)) != 0) {
        goto label_191;
    }
label_158:
    if (*(src_sb.st_nlink) > 1) {
        goto label_192;
    }
    edx = *((rbx + 4));
    if (*((rbp - 0x328)) != 0) {
        if (edx == 3) {
            goto label_192;
        }
    }
    r14d = 0;
    if (edx == 4) {
        goto label_192;
    }
label_154:
    if (al != 0) {
        goto label_9;
    }
label_29:
    r13d = *((rbp - segment.INTERP));
    goto label_10;
label_147:
    eax = *((rbp - 0x31c));
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_11;
    }
    emit_verbose (r15, *((rbp - 0x330)), *((rbp - 0x348)));
    goto label_11;
label_155:
    eax = renameatu (0xffffff9c, r15, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)), 1);
    if (eax != 0) {
        goto label_193;
    }
    rax = *((rbp - 0x340));
    r8d = 1;
    *(rax) = 1;
    goto label_12;
label_142:
    rdx = r9;
    *((rbp - 0x378)) = r8d;
    *((rbp - 0x348)) = r9;
    eax = seen_file (rdi, r15, rdx, rcx, r8, r9);
    r12d = eax;
    if (al != 0) {
        goto label_194;
    }
    rdi = *((rbx + 0x50));
    r9 = *((rbp - 0x348));
    r8d = *((rbp - 0x378));
    goto label_13;
label_176:
    edx = *((rbp - 0x31c));
    r8 = rbx;
    ecx = r13d;
    rdi = r15;
    rsi = *((rbp - 0x330));
    al = set_process_security_ctx ();
    if (al == 0) {
        goto label_5;
    }
    eax = r12d;
    eax &= 0x12;
    *((rbp - 0x380)) = eax;
label_178:
    rax = *((rbp - 0x358));
    rdx = *(src_sb.st_ino);
    rcx = *(src_sb.st_dev);
    if (rax != 0) {
        goto label_195;
    }
    goto label_196;
    do {
label_14:
        rax = *(rax);
        if (rax == 0) {
            goto label_196;
        }
label_195:
    } while (*((rax + 8)) != rdx);
    if (*((rax + 0x10)) != rcx) {
        goto label_14;
    }
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot copy cyclic symbolic link %s";
    r12 = rax;
    goto label_87;
label_168:
    rax = errno_location ();
    r12 = rax;
    eax = *(rax);
    if (eax == 0x28) {
        goto label_197;
    }
    if (eax == 2) {
        goto label_198;
    }
label_44:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
label_136:
    rax = dcgettext (0, "cannot stat %s");
    rcx = r13;
    eax = 0;
    r12d = 0;
    error (0, *(r12), rax);
    goto label_7;
label_167:
    r14d = *((rbx + 0x18));
    if (r14b != 0) {
        goto label_15;
    }
    r14d = *((rbx + 0x3a));
    if (r14b != 0) {
        goto label_15;
    }
    r14d = *((rbx + 0x17));
    if (r14b != 0) {
        goto label_15;
    }
    edi = *(rbx);
    if (edi != 0) {
        goto label_199;
    }
    r14d = *((rbx + 0x15));
    if (r14b != 0) {
        goto label_15;
    }
    if (r8d == 0) {
        goto label_16;
    }
label_198:
    *((rbp - segment.INTERP)) = 1;
label_43:
    *((rbp - 0x348)) = 0;
    r14d = 0;
    if (r13d != 0x11) {
        goto label_17;
    }
    goto label_18;
label_140:
    r12d = *((rbx + 0x38));
    if (r12b != 0) {
        goto label_1;
    }
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    if (*((rbx + 0x19)) != 0) {
        goto label_200;
    }
    rax = dcgettext (0, "-r not specified; omitting directory %s");
label_57:
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    goto label_7;
label_166:
    if (*((rbx + 8)) != 2) {
        goto label_19;
    }
    r14d = 0;
    r13d = 0;
    goto label_38;
label_84:
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    ecx = 0x100;
    rdx = dst_sb_st_dev;
    eax = fstatat ();
    if (eax == 0) {
        goto label_201;
    }
label_139:
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
label_46:
    rax = dcgettext (0, "cannot stat %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    goto label_5;
label_179:
    if (*((rbx + 0x17)) != 0) {
        goto label_202;
    }
    eax = *((rbp - 0x340));
    if (eax == 0x8000) {
        goto label_203;
    }
    r9b = (eax != 0xa000) ? 1 : 0;
    r9b &= *((rbx + 0x14));
    if (r9b != 0) {
        goto label_203;
    }
    if (*((rbp - 0x340)) == 0x1000) {
        goto label_204;
    }
    esi = *((rbp - 0x340));
    eax = *((rbp - 0x340));
    ah &= 0xbf;
    if (eax == 0x2000) {
        goto label_205;
    }
    if (esi == 0xc000) {
        goto label_205;
    }
    if (*((rbp - 0x340)) != 0xa000) {
        goto label_206;
    }
    rax = areadlink_with_size (r15, *(src_sb.st_size));
    r13 = rax;
    if (rax == 0) {
        goto label_207;
    }
    ecx = *((rbx + 0x16));
    eax = force_symlinkat (rax, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)), rcx, 0xffffffff);
    r12d = eax;
    if (eax <= 0) {
        goto label_208;
    }
    if (*((rbx + 0x3b)) == 1) {
        if (*((rbp - segment.INTERP)) == 0) {
            goto label_209;
        }
    }
label_109:
    rdi = r13;
    fcn_00003670 ();
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot create symbolic link %s");
    rcx = r13;
    eax = 0;
    rax = error (0, r12d, rax);
label_28:
    if (*((rbx + 0x33)) != 0) {
        goto label_35;
    }
    if (r14 == 0) {
        goto label_210;
    }
label_34:
    rax = *((rbp - 0x348));
    if (rax == 0) {
        goto label_5;
    }
    rcx = *((rbp - segment.UNKNOWN));
    edx = *((rbp - 0x320));
    rsi = rcx;
    rsi -= *((rbp - 0x330));
    edi = edx;
    rsi += rax;
    eax = renameat ();
    if (eax != 0) {
        goto label_211;
    }
    if (*((rbx + 0x3c)) == 0) {
        goto label_5;
    }
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = *((rbp - 0x348));
    edi = 0;
    esi = 4;
    r13 = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s -> %s (unbackup)\n");
    rcx = r13;
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    goto label_5;
label_196:
    rax = rsp;
    if (rsp == rax) {
        goto label_212;
    }
    do {
    } while (rsp != rax);
label_212:
    rdi = *((rbp - 0x358));
    rax = rsp + 0xf;
    rax &= 0xfffffffffffffff0;
    *((rbp - 0x3a0)) = rax;
    *(rax) = rdi;
    *((rax + 8)) = rdx;
    *((rax + 0x10)) = rcx;
    if (*((rbp - segment.INTERP)) == 0) {
        eax = *(dst_sb.st_mode);
        eax &= case.0xf4aa.9;
        if (eax == 0x4000) {
            goto label_213;
        }
    }
    edx = *((rbp - 0x380));
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    edx = ~edx;
    edx &= r12d;
    eax = mkdirat ();
    if (eax != 0) {
        goto label_214;
    }
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    ecx = 0x100;
    rdx = dst_sb_st_dev;
    eax = fstatat ();
    if (eax != 0) {
        goto label_215;
    }
    edi = *(dst_sb.st_mode);
    eax = *(dst_sb.st_mode);
    *((rbp - 0x3a8)) = edi;
    eax &= 0x1c0;
    if (eax == 0x1c0) {
        goto label_216;
    }
    edi |= 0x1c0;
    rsi = *((rbp - segment.UNKNOWN));
    ecx = 0x100;
    edx = edi;
    edi = *((rbp - 0x320));
    eax = fchmodat ();
    *((rbp - 0x3b8)) = 1;
    if (eax != 0) {
        goto label_217;
    }
label_58:
    rax = *((rbp - 0x350));
    if (*(rax) == 0) {
        goto label_218;
    }
label_75:
    if (*((rbx + 0x3c)) != 0) {
        if (*((rbx + 0x18)) == 0) {
            goto label_219;
        }
        rsi = *((rbp - 0x330));
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "created directory %s\n");
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
    }
label_56:
    rax = *((rbp - 0x370));
    r12b = (rax != 0) ? 1 : 0;
    r12b &= *((rbx + 0x1c));
    if (r12b != 0) {
        goto label_220;
    }
label_50:
    __asm ("movdqu xmm0, xmmword [rbx]");
    __asm ("movdqu xmm1, xmmword [rbx + 0x10]");
    esi = 2;
    __asm ("movdqu xmm2, xmmword [rbx + 0x20]");
    __asm ("movdqu xmm3, xmmword [rbx + 0x30]");
    __asm ("movdqu xmm4, xmmword [rbx + 0x40]");
    rax = *((rbx + 0x50));
    *((rbp - 0x300)) = xmm0;
    *((rbp - 0x2f0)) = xmm1;
    *((rbp - 0x2b0)) = rax;
    *((rbp - 0x2e0)) = xmm2;
    *((rbp - 0x2d0)) = xmm3;
    *((rbp - 0x2c0)) = xmm4;
    rax = savedir (r15);
    *((rbp - section..gnu.hash)) = rax;
    if (rax == 0) {
        goto label_221;
    }
    if (*((rbx + 4)) == 3) {
        *((rbp - 0x2fc)) = 2;
    }
    rax = *((rbp - section..gnu.hash));
    if (*(rax) == 0) {
        goto label_222;
    }
    rax = *((rbp - segment.UNKNOWN));
    rax -= *((rbp - 0x330));
    r12d = 1;
    r9 = src_sb_st_dev;
    *((rbp - 0x360)) = rax;
    rax = rbp - 0x308;
    r14 = *((rbp - section..gnu.hash));
    *((rbp - 0x388)) = rax;
    rax = rbp - 0x30a;
    *((rbp - 0x370)) = rax;
    rax = rbp - 0x309;
    *((rbp - 0x378)) = rax;
    rax = rbp - 0x300;
    *((rbp - 0x394)) = r13d;
    r13 = r14;
    *((rbp - 0x3c0)) = rbx;
    rbx = *((rbp - segment.NOTE_1));
    *((rbp - 0x358)) = 0;
    *((rbp - 0x390)) = rax;
    *((rbp - 0x340)) = r12b;
    *((rbp - 0x348)) = r15;
    *((rbp - segment.NOTE_1)) = r9;
    while (r14b == 0) {
        esi = *((rbp - 0x309));
        *((rbp - 0x358)) |= sil;
        strlen (r13);
        r13 = r13 + rax + 1;
        if (*(r13) == 0) {
            goto label_223;
        }
        rdi = *((rbp - 0x348));
        edx = 0;
        rsi = r13;
        rax = file_name_concat ();
        rdi = *((rbp - 0x330));
        edx = 0;
        rsi = r13;
        r12 = rax;
        rax = file_name_concat ();
        r15 = rax;
        rax = *((rbp - 0x350));
        eax = *(rax);
        *((rbp - 0x309)) = al;
        rax = *((rbp - 0x360));
        al = copy_internal (r12, r15, *((rbp - 0x320)), r15 + rax, *((rbp - 0x394)), *((rbp - segment.NOTE_1)));
        goto label_20;
        r14d = *((rbp - 0x30a));
        *(rbx) |= r14b;
        rdi = r15;
        *((rbp - 0x340)) &= al;
        fcn_00003670 ();
        rdi = r12;
        fcn_00003670 ();
    }
label_223:
    r12d = *((rbp - 0x340));
    r15 = *((rbp - 0x348));
    rbx = *((rbp - 0x3c0));
label_96:
    rdi = *((rbp - section..gnu.hash));
    fcn_00003670 ();
    rax = *((rbp - 0x350));
    edi = *((rbp - 0x358));
    *(rax) = dil;
label_49:
    if (*((rbp - 0x328)) != 0) {
        goto label_224;
    }
    eax = *((rbp - 0x3b8));
    r13d = 0;
    *((rbp - 0x360)) = al;
    goto label_21;
label_63:
    rsi = src_open_sb_st_dev;
    rdi = r15;
    eax = stat ();
    if (eax == 0) {
        goto label_225;
    }
label_61:
    r13d = 0;
label_38:
    if (*((rbx + 0x3b)) == 0) {
        goto label_226;
    }
    eax = *((rbp - 0x31c));
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_227;
    }
    r8d = 0;
    if (*((rbx + 0x1f)) != 0) {
        r8d = 1;
        if (*((rbx + 0x18)) == 0) {
            goto label_228;
        }
        rax = *(src_sb.st_dev);
        r8b = (*(dst_sb.st_dev) != rax) ? 1 : 0;
    }
label_228:
    r9 = src_sb_st_dev;
    eax = utimecmpat (*((rbp - 0x320)), *((rbp - segment.UNKNOWN)), dst_sb.st_dev, r9, 0);
    if (eax < 0) {
        goto label_229;
    }
    rax = *((rbp - 0x340));
    if (rax != 0) {
        *(rax) = 1;
    }
    rax = remember_copied (*((rbp - segment.UNKNOWN)), *(src_sb.st_ino), *(src_sb.st_dev), rcx, r8);
    rdx = rax;
    if (rax == 0) {
        goto label_22;
    }
    eax = *((rbp - 0x378));
    eax = *((rbx + 0x3c));
    al = create_hard_link (0, *((rbp - 0x320)), rdx, *((rbp - 0x330)), esi, *((rbp - segment.UNKNOWN)));
    if (al != 0) {
        goto label_22;
    }
    if (*((rbx + 0x33)) == 0) {
        goto label_5;
    }
label_35:
    restore_default_fscreatecon_or_die ();
label_226:
    if (*((rbx + 0x18)) != 0) {
        goto label_55;
    }
    eax = *((rbp - 0x31c));
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_40;
    }
label_54:
    eax = *((rbx + 8));
    if (eax == 2) {
        goto label_22;
    }
    if (eax == 3) {
        goto label_230;
    }
label_40:
    if (r13b != 0) {
        goto label_22;
    }
    eax = *(dst_sb.st_mode);
    edx = *(dst_sb.st_mode);
    edx &= case.0xf4aa.9;
    if (edx == 0x4000) {
        goto label_231;
    }
label_101:
    r13d = *((rbp - 0x31c));
    r13d &= case.0xf4aa.9;
    if (r13d == 0x4000) {
        goto label_232;
    }
    r12d = *(rbx);
    if (*((rbp - 0x328)) != 0) {
label_67:
        if (r12d == 3) {
            goto label_233;
        }
        al = seen_file (*((rbx + 0x48)), *((rbp - segment.UNKNOWN)), dst_sb.st_dev, rcx, r8, r9);
        if (al != 0) {
            goto label_234;
        }
        if (r13d != 0x4000) {
            eax = *(dst_sb.st_mode);
            edx = *(dst_sb.st_mode);
            edx &= case.0xf4aa.9;
            if (edx == 0x4000) {
                goto label_235;
            }
        }
label_51:
        r12d = *(rbx);
    }
    eax = *((rbx + 0x18));
    if (al != 0) {
        goto label_236;
    }
    if (r12d != 0) {
        goto label_237;
    }
label_52:
    edx = *(dst_sb.st_mode);
    r13d = 0x11;
    *((rbp - 0x348)) = 0;
    edx &= case.0xf4aa.9;
    r12b = (edx == 0x4000) ? 1 : 0;
    r12b |= al;
    if (r12b != 0) {
        goto label_17;
    }
    if (*((rbx + 0x15)) != 0) {
        goto label_92;
    }
    if (*((rbx + 0x31)) == 0) {
        goto label_17;
    }
    if (*((rbx + 0x30)) == 0) {
        goto label_238;
    }
    if (*(dst_sb.st_nlink) <= 1) {
        goto label_238;
    }
label_92:
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    edx = 0;
    eax = unlinkat ();
    if (eax != 0) {
        rax = errno_location ();
        r13 = rax;
        if (*(rax) != 2) {
            goto label_239;
        }
    }
    eax = *((rbx + 0x3c));
    *((rbp - segment.INTERP)) = al;
    if (al != 0) {
        goto label_240;
    }
    *((rbp - segment.INTERP)) = 1;
label_53:
    *((rbp - 0x348)) = 0;
    r13d = 0x11;
    goto label_17;
label_159:
    rcx = *((rbp - segment.UNKNOWN));
    edx = *((rbp - 0x320));
    rsi = r15;
    edi = 0xffffff9c;
    eax = renameat ();
    if (eax == 0) {
        goto label_23;
    }
    rax = errno_location ();
    r13d = *(rax);
    if (r13d != 0) {
        goto label_24;
    }
    goto label_23;
label_193:
    rax = errno_location ();
    r13d = *(rax);
    goto label_25;
label_161:
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = r15;
    esi = 4;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot move %s to %s");
    r8 = rbx;
    rcx = r12;
label_42:
    eax = 0;
    error (0, r13d, rax);
    forget_created (*(src_sb.st_ino), *(src_sb.st_dev));
    goto label_5;
label_183:
    *((rbp - 0x3a8)) = 0;
    r12d = 1;
    goto label_26;
label_202:
    eax = 1;
    if (*((rbx + 0x16)) == 0) {
        eax = 0;
        al = (*((rbx + 8)) == 3) ? 1 : 0;
    }
    edi = *((rbp - 0x378));
    rdx = r15;
    al = create_hard_link (r15, 0xffffff9c, rdx, *((rbp - 0x330)), *((rbp - 0x320)), *((rbp - segment.UNKNOWN)));
    r9d = 0;
    if (al != 0) {
        goto label_27;
    }
    goto label_28;
label_191:
    r14d = 0;
    goto label_29;
label_150:
    rax = remember_copied (*((rbp - segment.UNKNOWN)), rdi, rdx, rcx, r8);
    r14 = rax;
    goto label_30;
label_180:
    rsi = sb_st_dev;
    rdi = 0x000162d1;
    eax = stat ();
    if (eax != 0) {
        goto label_31;
    }
    edi = *((rbp - 0x320));
    ecx = 0;
    rdx = src_open_sb_st_dev;
    rsi = r12;
    eax = fstatat ();
    if (eax != 0) {
        goto label_31;
    }
    rax = *(src_open_sb.st_ino);
    if (*(sb.st_ino) == rax) {
        goto label_241;
    }
label_93:
    rdi = r12;
    fcn_00003670 ();
    rdx = *((rbp - 0x330));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
label_87:
    rax = dcgettext (0, "%s: can make relative symbolic links only in current directory");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_28;
label_192:
    rax = remember_copied (*((rbp - segment.UNKNOWN)), *(src_sb.st_ino), *(src_sb.st_dev), rcx, r8);
    r14 = rax;
label_47:
    if (r14 == 0) {
        goto label_32;
    }
    eax = *((rbp - 0x31c));
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_33;
    }
    eax = *((rbp - 0x378));
    eax = *((rbx + 0x3c));
    al = create_hard_link (0, *((rbp - 0x320)), r14, *((rbp - 0x330)), esi, *((rbp - segment.UNKNOWN)));
    if (al != 0) {
        goto label_22;
    }
label_68:
    if (*((rbx + 0x33)) == 0) {
        goto label_34;
    }
    goto label_35;
label_182:
    if (*((rbx + 0x28)) == 0) {
        goto label_242;
    }
label_65:
    rdx = rbx;
    *((rbp - 0x340)) = r9b;
    al = set_file_security_ctx (*((rbp - 0x330)), 0);
    r9d = *((rbp - 0x340));
    r8d = eax;
    if (al != 0) {
        goto label_36;
    }
    eax = *((rbx + 0x34));
    *((rbp - segment.INTERP)) = al;
    if (al != 0) {
        goto label_28;
    }
label_64:
    r8d = r12d;
    goto label_36;
label_169:
    rax = *(dst_sb.st_dev);
    if (*(src_sb.st_dev) != rax) {
        goto label_37;
    }
    r13d = *((rbx + 0x17));
    if (r13b != 0) {
        goto label_38;
    }
    if (*((rbx + 4)) == 2) {
        goto label_243;
    }
    r12 = sb_st_dev;
    edi = *((rbp - 0x320));
    ecx = 0x100;
    rsi = *((rbp - segment.UNKNOWN));
    rdx = r12;
    eax = fstatat ();
    if (eax != 0) {
        goto label_38;
    }
    r8 = src_open_sb_st_dev;
    rsi = r8;
    *((rbp - 0x348)) = r8;
    eax = lstat (r15, rsi);
    if (eax != 0) {
        goto label_38;
    }
    rax = *(sb.st_ino);
    edx = 0;
    r8 = *((rbp - 0x348));
    if (*(src_open_sb.st_ino) == rax) {
        rax = *(sb.st_dev);
        dl = (*(src_open_sb.st_dev) == rax) ? 1 : 0;
    }
    eax = *(src_open_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
        goto label_39;
    }
    eax = *(sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
        goto label_39;
    }
    if (*((rbx + 0x15)) == 0) {
        goto label_39;
    }
    goto label_38;
label_227:
    if (*((rbx + 0x18)) == 0) {
        goto label_40;
    }
label_55:
    eax = *((rbx + 8));
    if (eax == 2) {
        goto label_86;
    }
    if (eax == 3) {
        goto label_244;
    }
    if (eax != 4) {
        goto label_40;
    }
    if (*((rbx + 0x3d)) == 0) {
        goto label_40;
    }
    eax = *(dst_sb.st_mode);
    edx = *(dst_sb.st_mode);
    edx &= case.0xf4aa.9;
    if (edx == 0xa000) {
        goto label_245;
    }
    al = can_write_any_file ();
    if (al != 0) {
        goto label_40;
    }
    edi = *((rbp - 0x320));
    ecx = 0x200;
    edx = 2;
    rsi = *((rbp - segment.UNKNOWN));
    eax = faccessat ();
    if (eax == 0) {
        goto label_40;
    }
label_244:
    al = overwrite_ok (rbx, *((rbp - 0x330)), *((rbp - 0x320)), *((rbp - segment.UNKNOWN)), dst_sb.st_dev, r9);
    if (al != 0) {
        goto label_40;
    }
label_86:
    if (*((rbp - 0x340)) == 0) {
        goto label_22;
    }
    rax = *((rbp - 0x340));
    *(rax) = 1;
    goto label_22;
label_165:
    r14 = src_open_sb_st_dev;
    edi = *((rbp - 0x320));
    ecx = 0x100;
    rsi = *((rbp - segment.UNKNOWN));
    rdx = r14;
    eax = fstatat ();
    if (eax != 0) {
        goto label_3;
    }
    rdx = r14;
    goto label_41;
label_175:
    record_file (*((rbx + 0x48)), *((rbp - segment.UNKNOWN)), src_sb.st_dev, rcx, r8, r9);
    r12d = *((rbp - 0x328));
    goto label_7;
label_162:
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = r15;
    esi = 4;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "inter-device move failed: %s to %s; unable to remove target");
    esi = *(r13);
    r8 = rbx;
    rcx = r12;
    rdx = rax;
    goto label_42;
label_197:
    if (*((rbx + 0x16)) != 0) {
        goto label_43;
    }
    goto label_44;
label_174:
    edx = 5;
    rax = dcgettext (0, "renamed ");
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    emit_verbose (r15, *((rbp - 0x330)), *((rbp - 0x348)));
    goto label_45;
label_211:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot un-backup %s";
    r13 = rax;
    goto label_46;
label_210:
    forget_created (*(src_sb.st_ino), *(src_sb.st_dev));
    goto label_34;
label_157:
    rax = src_to_dest_lookup (*(src_sb.st_ino), *(src_sb.st_dev));
    r14 = rax;
    goto label_47;
label_163:
    edx = 5;
    r13d = 1;
    rax = dcgettext (0, "copied ");
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    emit_verbose (r15, *((rbp - 0x330)), *((rbp - 0x348)));
    goto label_10;
label_160:
    rdx = top_level_dst_name;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = top_level_src_name;
    esi = 4;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot move %s to a subdirectory of itself, %s");
    rcx = r12;
    r8 = rbx;
    eax = 0;
    r12d = 1;
    error (0, 0, rax);
    rax = *((rbp - segment.NOTE_1));
    *(rax) = 1;
    goto label_7;
label_224:
    eax = *((rbp - 0x3b8));
    r9d = 0;
    r13d = 0;
    r8d = 0;
    *((rbp - 0x360)) = al;
    goto label_48;
label_203:
    eax = *(src_sb.st_mode);
    *((rbp - 0x308)) = 0;
    sil = (*((rbx + 4)) == 2) ? 1 : 0;
    *((rbp - 0x358)) = eax;
    eax = *((rbx + 0x31));
    esi <<= 0x11;
    *((rbp - 0x350)) = al;
    eax = 0;
    eax = open_safer (r15, 0, rdx, rcx);
    r13d = eax;
    if (eax < 0) {
        goto label_246;
    }
    eax = fstat (eax, src_open_sb.st_dev);
    if (eax != 0) {
        goto label_247;
    }
    rax = *(src_open_sb.st_ino);
    if (*(src_sb.st_ino) != rax) {
        goto label_248;
    }
    rax = *(src_open_sb.st_dev);
    if (*(src_sb.st_dev) != rax) {
        goto label_248;
    }
    r12d &= 0x1ff;
    *((rbp - 0x3a0)) = r12d;
    if (*((rbp - segment.INTERP)) != 0) {
        goto label_105;
    }
    edx -= edx;
    eax = 0;
    edx &= 0xfffffe00;
    edx += 0x201;
    eax = openat_safer (*((rbp - 0x320)), *((rbp - segment.UNKNOWN)), rdx, rcx, r8);
    r12d = eax;
    rax = errno_location ();
    r8d = *(rax);
    r9 = rax;
    if (r12d < 0) {
        goto label_249;
    }
    if (*((rbx + 0x28)) == 0) {
        goto label_250;
    }
label_114:
    rdx = rbx;
    eax = set_file_security_ctx (*((rbp - 0x330)), 0);
    r9d = eax;
    if (al == 0) {
        if (*((rbx + 0x34)) != 0) {
            goto label_251;
        }
    }
label_113:
    *((rbp - 0x3c4)) = 0;
    *((rbp - section..gnu.hash)) = 0;
label_107:
    if (*((rbp - 0x350)) == 0) {
        goto label_111;
    }
    edx = *((rbx + 0x44));
    if (edx != 0) {
        goto label_252;
    }
label_112:
    eax = fstat (r12d, sb.st_dev);
    if (eax != 0) {
        goto label_253;
    }
    *((rbp - segment.NOTE_1)) = eax;
    esi = *((rbp - section..gnu.hash));
    eax = *(sb.st_mode);
    esi |= eax;
    if (eax != esi) {
        eax = fchmod (r12d, rsi);
        edx = *((rbp - segment.NOTE_1));
        if (eax == 0) {
            edx = *((rbp - section..gnu.hash));
        }
        *((rbp - section..gnu.hash)) = edx;
    }
    if (*((rbp - 0x350)) == 0) {
        goto label_118;
    }
    rax = *(sb.st_blksize);
    *((rbp - 0x3b8)) = rax;
    rdx = rax - 0x20000;
    *((rbp - 0x378)) = rax;
    rax = 0x1ffffffffffe0000;
    if (rdx > rax) {
        rdi = *((rbp - 0x3b8));
        rax += 0x1ffff;
        *((rbp - 0x378)) = 0x20000;
        rdx = rdi - 1;
        eax = 0x200;
        if (rdx <= rax) {
            rax = rdi;
        }
        *((rbp - 0x3b8)) = rax;
    }
    eax = *(src_open_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax != 0x8000) {
        goto label_254;
    }
    rax = *(src_open_sb.st_size);
    ecx = 0x200;
    r8d = 1;
    __asm ("cqo");
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (*(src_open_sb.st_blocks) >= rax) {
        goto label_128;
    }
    edx = 3;
    esi = 0;
    edi = r13d;
    rax = lseek ();
    *((rbp - 0x388)) = rax;
    if (rax >= 0) {
        goto label_255;
    }
    rax = errno_location ();
    r8 = rax;
    eax = *(rax);
    *((rbp - segment.NOTE_1)) = r8;
    if (eax == 6) {
        goto label_255;
    }
    if (eax == 0x16) {
        goto label_256;
    }
    if (eax == 0x5f) {
        goto label_256;
    }
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x350)) = rax;
    rax = dcgettext (0, "cannot lseek %s");
    r8 = *((rbp - segment.NOTE_1));
    rcx = *((rbp - 0x350));
    eax = 0;
    error (0, *(r8), rax);
label_94:
    r9d = 0;
label_95:
    *((rbp - 0x350)) = r9b;
    eax = close (r12d);
    if (eax < 0) {
        goto label_257;
    }
    r12 = *((rbp - 0x308));
    r9d = *((rbp - 0x350));
label_76:
    *((rbp - 0x350)) = r9b;
    eax = close (r13d);
    r9d = *((rbp - 0x350));
    if (eax < 0) {
        goto label_258;
    }
    rdi = r12;
    *((rbp - 0x350)) = r9b;
    r13d = 0;
    fcn_00003670 ();
    r9d = *((rbp - 0x350));
    if (r9b != 0) {
        goto label_27;
    }
    goto label_28;
label_181:
    rdx = r15;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = *((rbp - 0x330));
    edi = 0;
    esi = 4;
    r15 = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot create symbolic link %s to %s");
    r8 = r15;
    rcx = r13;
    eax = 0;
    rax = error (0, r12d, rax);
    goto label_28;
label_220:
    rsi = *(src_sb.st_dev);
    if (*(rax) != rsi) {
        goto label_49;
    }
    goto label_50;
label_214:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
label_69:
    rax = dcgettext (0, "cannot create directory %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    goto label_28;
label_231:
    edx = *((rbp - 0x31c));
    edx &= case.0xf4aa.9;
    if (edx == 0x4000) {
        goto label_51;
    }
label_235:
    if (*((rbx + 0x18)) == 0) {
        goto label_259;
    }
    r12d = *(rbx);
    if (r12d == 0) {
        goto label_259;
    }
label_66:
    edx = *(src_sb.st_mode);
    edx &= case.0xf4aa.9;
    if (edx == 0x4000) {
        goto label_260;
    }
label_72:
    rdi = r15;
    rax = last_component ();
    r13 = rax;
    eax = 1;
    if (*(r13) != 0x2e) {
        goto label_261;
    }
label_71:
    edx = 0;
    dl = (*((r13 + 1)) == 0x2e) ? 1 : 0;
    edx = *((r13 + rdx + 1));
    if (dl == 0) {
        goto label_52;
    }
    if (dl == 0x2f) {
        goto label_52;
    }
    if (al != 0) {
        goto label_261;
    }
label_70:
    eax = *(dst_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_53;
    }
label_261:
    if (r12d != 3) {
        rax = strlen (r13);
        rdi = *((rbp - segment.UNKNOWN));
        *((rbp - segment.INTERP)) = rax;
        rax = last_component ();
        rdi = rax;
        *((rbp - 0x380)) = rax;
        rax = strlen (rdi);
        r12 = simple_backup_suffix;
        *((rbp - 0x348)) = rax;
        rax = strlen (r12);
        rdx = *((rbp - 0x348));
        rax += rdx;
        if (*((rbp - segment.INTERP)) != rax) {
            goto label_125;
        }
        *((rbp - segment.INTERP)) = rdx;
        eax = memcmp (r13, *((rbp - 0x380)), rdx);
        if (eax != 0) {
            goto label_125;
        }
        rdx = *((rbp - segment.INTERP));
        eax = strcmp (r13 + rdx, r12);
        if (eax != 0) {
            goto label_125;
        }
        r13 = *((rbp - segment.UNKNOWN));
        strlen (*((rbp - segment.UNKNOWN)));
        rax = subst_suffix (r13, r13 + rax, r12);
        edi = *((rbp - 0x320));
        ecx = 0;
        rdx = src_open_sb_st_dev;
        rsi = rax;
        r13 = rax;
        eax = fstatat ();
        rdi = r13;
        r12d = eax;
        fcn_00003670 ();
        if (r12d != 0) {
            goto label_125;
        }
        rax = *(src_open_sb.st_ino);
        if (*(src_sb.st_ino) == rax) {
            goto label_262;
        }
    }
label_125:
    r13 = *((rbp - segment.UNKNOWN));
    rax = backup_file_rename (*((rbp - 0x320)), r13, *(rbx));
    r12 = rax;
    if (rax == 0) {
        goto label_263;
    }
    r13 -= *((rbp - 0x330));
    strlen (rax);
    rsi = rsp;
    r8 = rax + 1;
    rax = r13 + rax + 0x18;
    rcx = rax;
    rax &= 0xfffffffffffff000;
    rsi -= rax;
    rcx &= 0xfffffffffffffff0;
    if (rsp == rsi) {
        goto label_264;
    }
    do {
    } while (rsp != rsi);
label_264:
    ecx &= 0xfff;
    if (rcx != 0) {
    }
    rax = rsp + 0xf;
    rsi = *((rbp - 0x330));
    rdx = r13;
    *((rbp - segment.INTERP)) = r8;
    rax &= 0xfffffffffffffff0;
    r13d = 0x11;
    rdi = rax;
    *((rbp - 0x348)) = rax;
    rax = mempcpy ();
    memcpy (rax, r12, *((rbp - segment.INTERP)));
    rdi = r12;
    fcn_00003670 ();
    *((rbp - segment.INTERP)) = 1;
    goto label_17;
label_229:
    if (*((rbx + 0x18)) == 0) {
        goto label_54;
    }
    goto label_55;
label_213:
    if (*((rbx + 0x28)) == 0) {
        goto label_265;
    }
label_77:
    rdx = rbx;
    al = set_file_security_ctx (*((rbp - 0x330)), 0);
    if (al != 0) {
        goto label_78;
    }
    if (*((rbx + 0x34)) != 0) {
        goto label_28;
    }
label_78:
    *((rbp - 0x3b8)) = 0;
    *((rbp - 0x380)) = 0;
    *((rbp - 0x3a8)) = 0;
    goto label_56;
label_194:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
label_60:
    rax = dcgettext (0, "warning: source file %s specified more than once");
    rdx = rax;
    goto label_57;
label_216:
    *((rbp - 0x3b8)) = 0;
    *((rbp - 0x3a8)) = 0;
    goto label_58;
label_187:
    ecx = 0xffffffff;
    eax = copy_acl (r15, 0xffffffff, *((rbp - 0x330)), ecx, *((rbp - 0x31c)));
    if (eax == 0) {
        goto label_7;
    }
    goto label_59;
label_200:
    rsi = "omitting directory %s";
    goto label_60;
label_171:
    ecx = *((rbx + 0x18));
    if (cl != 0) {
        goto label_266;
    }
    if (*((rbx + 0x15)) != 0) {
        goto label_266;
    }
    eax = *((r8 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
label_97:
        eax = *((r12 + 0x18));
        eax &= case.0xf4aa.9;
        if (eax != 0xa000) {
            goto label_267;
        }
label_74:
        ecx = *((rbx + 0x18));
label_73:
        if (cl == 0) {
            goto label_102;
        }
        eax = *(src_sb.st_mode);
        eax &= case.0xf4aa.9;
        if (eax == 0xa000) {
            goto label_268;
        }
    }
label_102:
    if (*((rbx + 0x3a)) == 0) {
        goto label_269;
    }
    eax = *((r12 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax == 0xa000) {
        goto label_61;
    }
label_269:
    if (*((rbx + 4)) != 2) {
        goto label_62;
    }
    eax = *((r8 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax == 0xa000) {
        goto label_63;
    }
    __asm ("movdqa xmm5, xmmword [r8]");
    *(src_open_sb.st_dev) = xmm5;
    __asm ("movdqa xmm6, xmmword [r8 + 0x10]");
    *(src_open_sb.st_nlink) = xmm6;
    __asm ("movdqa xmm7, xmmword [r8 + 0x20]");
    *(src_open_sb.st_gid) = xmm7;
    __asm ("movdqa xmm5, xmmword [r8 + 0x30]");
    *(src_open_sb.st_size) = xmm5;
    __asm ("movdqa xmm6, xmmword [r8 + 0x40]");
    *(src_open_sb.st_blocks) = xmm6;
    __asm ("movdqa xmm7, xmmword [r8 + 0x50]");
    *((rbp - 0x80)) = xmm7;
    __asm ("movdqa xmm5, xmmword [r8 + 0x60]");
    *((rbp - 0x70)) = xmm5;
    __asm ("movdqa xmm6, xmmword [r8 + 0x70]");
    *((rbp - 0x60)) = xmm6;
    __asm ("movdqa xmm7, xmmword [r8 + 0x80]");
    *((rbp - 0x50)) = xmm7;
label_225:
    eax = *((r12 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax == 0xa000) {
        goto label_270;
    }
    __asm ("movdqa xmm5, xmmword [r12]");
    *(sb.st_dev) = xmm5;
    __asm ("movdqa xmm6, xmmword [r12 + 0x10]");
    *(sb.st_nlink) = xmm6;
    __asm ("movdqa xmm7, xmmword [r12 + 0x20]");
    *(sb.st_gid) = xmm7;
    __asm ("movdqa xmm5, xmmword [r12 + 0x30]");
    *(sb.st_size) = xmm5;
    __asm ("movdqa xmm6, xmmword [r12 + 0x40]");
    *(sb.st_blocks) = xmm6;
    __asm ("movdqa xmm7, xmmword [r12 + 0x50]");
    *((rbp - 0x110)) = xmm7;
    __asm ("movdqa xmm5, xmmword [r12 + 0x60]");
    *((rbp - 0x100)) = xmm5;
    __asm ("movdqa xmm6, xmmword [r12 + 0x70]");
    *((rbp - 0xf0)) = xmm6;
    __asm ("movdqa xmm7, xmmword [r12 + 0x80]");
    *((rbp - 0xe0)) = xmm7;
label_115:
    rax = *(sb.st_ino);
    if (*(src_open_sb.st_ino) != rax) {
        goto label_61;
    }
    rax = *(sb.st_dev);
    if (*(src_open_sb.st_dev) != rax) {
        goto label_61;
    }
    if (*((rbx + 0x17)) == 0) {
        goto label_62;
    }
    eax = *((r12 + 0x18));
    eax &= case.0xf4aa.9;
    r13b = (eax != 0xa000) ? 1 : 0;
    goto label_38;
label_242:
    if (*((rbx + 0x33)) == 0) {
        goto label_64;
    }
    goto label_65;
label_232:
    if (*((rbx + 0x18)) == 0) {
        goto label_271;
    }
    r12d = *(rbx);
    if (r12d == 0) {
        goto label_271;
    }
    if (*((rbp - 0x328)) == 0) {
        goto label_66;
    }
    goto label_67;
label_151:
    rdx = top_level_dst_name;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = top_level_src_name;
    esi = 4;
    edi = 0;
    r13 = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot copy a directory, %s, into itself, %s");
    r8 = r13;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    rax = *((rbp - segment.NOTE_1));
    *(rax) = 1;
    goto label_68;
label_205:
    edx = *((rbp - 0x380));
    rcx = *(src_sb.st_rdev);
    r13d = 0;
    *((rbp - 0x350)) = r9b;
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    edx = ~edx;
    edx &= *((rbp - 0x31c));
    eax = mknodat ();
    r9d = *((rbp - 0x350));
    if (eax == 0) {
        goto label_27;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot create special file %s";
    r13 = rax;
    goto label_69;
label_188:
label_81:
    eax = set_acl (*((rbp - 0x330)), 0xffffffff, *((rbx + 0x10)));
    if (eax == 0) {
        goto label_7;
    }
    goto label_5;
label_233:
    if (*((rbx + 0x18)) != 0) {
        goto label_66;
    }
label_237:
    rdi = r15;
    rax = last_component ();
    r13 = rax;
    if (*(rax) != 0x2e) {
        goto label_70;
    }
    eax = 0;
    goto label_71;
label_236:
    eax = *(src_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_272;
    }
label_79:
    if (r12d == 0) {
        goto label_53;
    }
    goto label_72;
label_266:
    eax = *((r12 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax == 0xa000) {
        goto label_61;
    }
    if (dl != 0) {
        if (*((r12 + 0x10)) > 1) {
            goto label_273;
        }
    }
    eax = *((r8 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax == 0xa000) {
        goto label_73;
    }
label_267:
    rax = *((r12 + 8));
    if (*((r8 + 8)) != rax) {
        goto label_61;
    }
    rax = *(r12);
    if (*(r8) != rax) {
        goto label_61;
    }
    r13d = *((rbx + 0x17));
    if (r13b == 0) {
        goto label_74;
    }
    goto label_38;
label_199:
    r14d = 1;
    goto label_15;
label_218:
    remember_copied (*((rbp - segment.UNKNOWN)), *(dst_sb.st_ino), *(dst_sb.st_dev), rcx, r8);
    rax = *((rbp - 0x350));
    *(rax) = 1;
    goto label_75;
label_248:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "skipping file %s, as it was replaced while being copied");
    rcx = r12;
label_90:
    eax = 0;
    r12d = 0;
    error (0, 0, rax);
    r9d = 0;
    goto label_76;
label_265:
    if (*((rbx + 0x33)) != 0) {
        goto label_77;
    }
    goto label_78;
label_215:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot stat %s";
    r13 = rax;
    goto label_69;
label_230:
    al = overwrite_ok (rbx, *((rbp - 0x330)), *((rbp - 0x320)), *((rbp - segment.UNKNOWN)), dst_sb.st_dev, r9);
    if (al != 0) {
        goto label_40;
    }
    goto label_22;
label_272:
    eax = *(dst_sb.st_mode);
label_260:
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_79;
    }
    if (r12d != 0) {
        goto label_72;
    }
    rdx = *((rbp - 0x330));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    rdx = r15;
    esi = 3;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style_colon ();
    edx = 5;
    rsi = "cannot move directory onto non-directory: %s -> %s";
    r12 = rax;
    goto label_80;
label_170:
    eax = *(dst_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
        goto label_39;
    }
    *((rbp - 0x348)) = r9d;
    eax = same_nameat (0xffffff9c, r15, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)));
    r13d = eax;
    if (al != 0) {
        goto label_62;
    }
    if (*(rbx) != 0) {
        goto label_38;
    }
    r9d = *((rbp - 0x348));
    if (r9d == 0) {
        goto label_38;
    }
    r12d = *((rbx + 0x18));
    r13d = 1;
    r12d ^= 1;
label_91:
    if (r12b == 0) {
        goto label_62;
    }
    goto label_38;
label_219:
    emit_verbose (r15, *((rbp - 0x330)), 0);
    goto label_56;
label_153:
    r15 = *((rbp - 0x330));
    rax = subst_suffix (r15, *((rbp - segment.UNKNOWN)), r14);
    esi = 4;
    edi = 1;
    rdx = rax;
    r12 = rax;
    rax = quotearg_n_style ();
    rdx = r15;
    edi = 0;
    esi = 4;
    r14 = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "will not create hard link %s to directory %s");
    r8 = r14;
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    rdi = r12;
    fcn_00003670 ();
    goto label_68;
label_271:
    rdx = r15;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    rsi = "cannot overwrite non-directory %s with directory %s";
    r12 = rax;
    goto label_80;
label_246:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot open %s for reading";
    r13 = rax;
    goto label_69;
label_259:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot overwrite directory %s with non-directory");
    rcx = r12;
    eax = 0;
    r12d = 0;
    error (0, 0, rax);
    goto label_7;
label_189:
    eax = *((rbp - 0x31c));
    ebx = mask.0;
    r13d = 0x1ff;
    eax &= 0x7000;
    eax = 0x1b6;
    if (eax != 0x4000) {
        r13d = eax;
    }
    if (ebx == 0xffffffff) {
        goto label_274;
    }
label_100:
    edx = ebx;
    edx = ~edx;
    edx &= r13d;
    goto label_81;
label_190:
    r13d = mask.0;
    if (r13d == 0xffffffff) {
        goto label_275;
    }
label_103:
    r13d = ~r13d;
    *((rbp - 0x380)) &= r13d;
    if (*((rbp - 0x380)) == 0) {
        goto label_82;
    }
    if (*((rbp - 0x360)) == 1) {
        goto label_83;
    }
    if (*((rbp - segment.INTERP)) != 0) {
        goto label_84;
    }
label_201:
    eax = *(dst_sb.st_mode);
    *((rbp - 0x3a8)) = eax;
    eax = ~eax;
    if ((*((rbp - 0x380)) & eax) != 0) {
        goto label_83;
    }
label_204:
    r12d = *((rbp - 0x380));
    rsi = *((rbp - segment.UNKNOWN));
    ecx = 0;
    *((rbp - 0x350)) = r9b;
    edi = *((rbp - 0x320));
    r12d = ~r12d;
    r12d &= *((rbp - 0x31c));
    edx = r12d;
    eax = mknodat ();
    r9d = *((rbp - 0x350));
    if (eax == 0) {
        goto label_27;
    }
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    edx = r12d;
    dh &= 0xef;
    eax = mkfifoat ();
    r9d = *((rbp - 0x350));
    if (eax == 0) {
        goto label_27;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot create fifo %s";
    r13 = rax;
    goto label_69;
label_243:
    edx = 1;
    r9d = 1;
    goto label_85;
label_152:
    rsi = top_level_src_name;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "warning: source directory %s specified more than once");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    if (*((rbx + 0x18)) != 0) {
        goto label_86;
    }
    goto label_22;
label_206:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "%s has unknown file type";
    r12 = rax;
    goto label_87;
label_186:
    if (*(dst_sb.st_gid) != edx) {
        goto label_88;
    }
    goto label_89;
label_258:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r15 = rax;
    rax = dcgettext (0, "failed to close %s");
    r13 = rax;
    rax = errno_location ();
    rcx = r15;
    eax = 0;
    error (0, *(rax), r13);
    rdi = r12;
    fcn_00003670 ();
    goto label_28;
label_247:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x350)) = rax;
    rax = dcgettext (0, "cannot fstat %s");
    r12 = rax;
    rax = errno_location ();
    rcx = *((rbp - 0x350));
    rdx = r12;
    esi = *(rax);
    goto label_90;
label_217:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "setting permissions for %s";
    r13 = rax;
    goto label_69;
label_105:
    eax = *((rbp - 0x380));
    eax = ~eax;
    eax &= *((rbp - 0x3a0));
    ecx = eax;
    *((rbp - segment.NOTE_1)) = eax;
    eax = 0;
    eax = openat_safer (*((rbp - 0x320)), *((rbp - segment.UNKNOWN)), 0xc1, ecx, r8);
    r12d = eax;
    rax = errno_location ();
    r8d = *(rax);
    r10 = rax;
    eax = r12d;
    eax >>= 0x1f;
    dl = (r8d == 0x11) ? 1 : 0;
    dl &= al;
    *((rbp - segment.INTERP)) = dl;
    if (dl == 0) {
        goto label_276;
    }
    eax = *((rbx + 0x18));
    r8d = 0x11;
    if (al == 0) {
        goto label_277;
    }
label_104:
    *((rbp - segment.INTERP)) = al;
label_106:
    rsi = *((rbp - 0x330));
    edi = 4;
    *((rbp - 0x350)) = r8d;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot create regular file %s");
    esi = *((rbp - 0x350));
    rcx = r12;
    rdx = rax;
    goto label_90;
label_172:
    r13d = 0;
    eax = same_nameat (0xffffff9c, r15, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)));
    eax ^= 1;
    r12d = eax;
    goto label_91;
label_238:
    r13d = 0x11;
    *((rbp - 0x348)) = 0;
    if (*((rbx + 4)) != 2) {
        goto label_17;
    }
    eax = *(src_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax != 0x8000) {
        goto label_92;
    }
    goto label_17;
label_241:
    rax = *(src_open_sb.st_dev);
    if (*(sb.st_dev) != rax) {
        goto label_93;
    }
    goto label_31;
label_111:
    eax = *((rbx + 0x1d));
    eax |= *((rbp - section..gnu.hash));
    *((rbp - 0x350)) = eax;
    if (eax != 0) {
        goto label_278;
    }
    *(sb.st_mode) = 0;
    if (*((rbx + 0x1f)) == 0) {
        goto label_279;
    }
label_99:
    rax = *(src_sb.st_atim);
    *((rbp - 0x2a0)) = rax;
    rax = *((rbp - 0x230));
    *((rbp - 0x298)) = rax;
    rax = *(src_sb.st_mtim);
    *((rbp - 0x290)) = rax;
    rax = *((rbp - 0x220));
    *((rbp - 0x288)) = rax;
    eax = fdutimensat (r12d, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)), rbp - 0x2a0, 0);
    if (eax != 0) {
        goto label_280;
    }
label_98:
    if (*((rbx + 0x1d)) == 0) {
        goto label_279;
    }
    eax = *(src_sb.st_uid);
    edx = *(src_sb.st_gid);
    if (eax == *(sb.st_uid)) {
        if (*(sb.st_gid) == edx) {
            goto label_279;
        }
    }
    rcx = sb_st_dev;
    r9d = *(src_sb.st_mode);
    r8d = r12d;
    rdi = rbx;
    ecx = *((rbp - segment.INTERP));
    rsi = *((rbp - 0x330));
    rcx = *((rbp - segment.UNKNOWN));
    edx = *((rbp - 0x320));
    eax = set_owner_isra_0 ();
    if (eax == 0xffffffff) {
        goto label_94;
    }
    if (eax == 0) {
        *((rbp - 0x358)) &= 0xfffff1ff;
    }
label_279:
    rax = 0xff0000000000ff;
    rax &= *((rbx + 0x18));
    if (rax != 0) {
        goto label_281;
    }
    if (*((rbx + 0x39)) != 0) {
        goto label_282;
    }
    eax = *((rbp - segment.INTERP));
    al &= *((rbx + 0x20));
    *((rbp - 0x358)) = al;
    if (al != 0) {
        goto label_283;
    }
    eax = *((rbp - 0x3c4));
    eax |= *((rbp - 0x350));
    r9d = 1;
    if (eax == 0) {
        goto label_95;
    }
    eax = mask.0;
    if (eax == 0xffffffff) {
        goto label_284;
    }
    edx = eax;
    edx = ~edx;
    edx &= *((rbp - 0x3c4));
    edx |= *((rbp - 0x350));
    if (edx == 0) {
        goto label_95;
    }
label_135:
    eax = ~eax;
    esi &= eax;
    eax = fchmod (r12d, *((rbp - 0x3a0)));
    r9d = 1;
    if (eax == 0) {
        goto label_95;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x358)) = rax;
    rax = dcgettext (0, "preserving permissions for %s");
    *((rbp - 0x350)) = rax;
    rax = errno_location ();
    rcx = *((rbp - 0x358));
    eax = 0;
    error (0, *(rax), *((rbp - 0x350)));
label_108:
    r9d = *((rbx + 0x32));
    r9d ^= 1;
    goto label_95;
label_222:
    *((rbp - 0x358)) = 0;
    r12d = 1;
    goto label_96;
label_234:
    rdx = r15;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 0;
    rbx = rax;
    rax = quotearg_n_style ();
    edx = 5;
    rsi = "will not overwrite just-created %s with %s";
    r12 = rax;
    goto label_80;
label_273:
    *((rbp - 0x348)) = r8;
    al = same_nameat (0xffffff9c, r15, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)));
    r8 = *((rbp - 0x348));
    r13d = eax;
    if (al == 0) {
        goto label_285;
    }
    eax = *((r8 + 0x18));
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
        goto label_97;
    }
    goto label_74;
label_208:
    rdi = r13;
    fcn_00003670 ();
label_110:
    if (*((rbx + 0x33)) != 0) {
        goto label_35;
    }
    r9d = *((rbx + 0x1d));
    r13d = 1;
    if (r9b == 0) {
        goto label_27;
    }
    ecx = *(src_sb.st_gid);
    edx = *(src_sb.st_uid);
    r8d = 0x100;
    *((rbp - 0x350)) = r9b;
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    eax = fchownat ();
    r9d = *((rbp - 0x350));
    if (eax == 0) {
        goto label_286;
    }
    rdi = rbx;
    eax = chown_failure_ok ();
    r9d = 0;
    r13d = eax;
    if (al != 0) {
        goto label_27;
    }
    edx = 5;
    rax = dcgettext (0, "failed to preserve ownership for %s");
    r12 = rax;
    rax = errno_location ();
    rcx = *((rbp - 0x330));
    eax = 0;
    error (0, *(rax), r12);
    if (*((rbx + 0x32)) != 0) {
        goto label_28;
    }
    r13d = *((rbp - 0x350));
    r9d = 0;
    goto label_27;
label_240:
    rsi = *((rbp - 0x330));
    edi = 4;
    r13d = 0x11;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "removed %s\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    *((rbp - 0x348)) = 0;
    goto label_17;
label_254:
    r8d = 1;
label_128:
    eax = *(sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax == 0x8000) {
        goto label_287;
    }
label_116:
    ecx = 2;
    edx = 0;
    esi = 0;
    edi = r13d;
    *((rbp - segment.NOTE_1)) = r8d;
    fdadvise ();
    rdi = *(src_open_sb.st_blksize);
    rax = 0x1ffffffffffe0000;
    rsi = *((rbp - 0x378));
    rdx = rdi - 0x20000;
    eax = 0x20000;
    rdx = 0x7fffffffffffffff;
    if (rdx > rax) {
        rdi = rax;
    }
    rax = buffer_lcm (rdi, rsi);
    r8d = *((rbp - segment.NOTE_1));
    rcx = rax;
    eax = *(src_open_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax == 0x8000) {
        goto label_288;
    }
label_123:
    rax = *((rbp - 0x378));
    edx = 0;
    *((rbp - 0x30a)) = 0;
    rsi = rcx + rax - 1;
    rax = rsi;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    rax = rsi;
    rax -= rdx;
    if (rax > 0) {
        rcx = rax;
    }
    eax = 0;
    al = (*((rbx + 0x44)) != 0) ? 1 : 0;
    *((rbp - 0x378)) = rcx;
    *((rbp - 0x398)) = eax;
    if (r8d == 3) {
        goto label_289;
    }
    *((rbp - 0x3b8)) = 0;
    r9b = (*((rbx + 0xc)) == 3) ? 1 : 0;
label_117:
    rax = rbp - 0x30a;
    rax = rbp - 0x300;
    eax = *((rbp - 0x398));
    eax = sparse_copy (r13d, r12d, rbp - 0x308, *((rbp - 0x378)), *((rbp - 0x3b8)), 0);
    eax ^= 1;
    r8d = eax;
label_121:
    if (r8b != 0) {
        goto label_94;
    }
    if (*((rbp - 0x30a)) != 0) {
        goto label_290;
    }
label_118:
    eax = *((rbp - section..gnu.hash));
    *((rbp - 0x350)) = eax;
    if (*((rbx + 0x1f)) == 0) {
        goto label_98;
    }
    goto label_99;
label_274:
    eax = umask (0);
    edi = eax;
    ebx = eax;
    *(obj.mask.0) = eax;
    umask (edi);
    goto label_100;
label_245:
    if (r13b == 0) {
        goto label_101;
    }
    goto label_22;
label_268:
    if (*((r12 + 0x10)) <= 1) {
        goto label_102;
    }
    rdi = r15;
    *((rbp - 0x348)) = r8;
    rax = canonicalize_file_name ();
    r8 = *((rbp - 0x348));
    r13 = rax;
    if (rax == 0) {
        goto label_102;
    }
    eax = same_nameat (0xffffff9c, rax, *((rbp - 0x320)), *((rbp - segment.UNKNOWN)));
    rdi = r13;
    r13d = 0;
    eax ^= 1;
    r12d = eax;
    fcn_00003670 ();
    goto label_91;
label_275:
    eax = umask (0);
    edi = eax;
    r13d = eax;
    *(obj.mask.0) = eax;
    umask (edi);
    goto label_103;
label_249:
    if (r8d == 2) {
        goto label_127;
    }
    eax = *((rbx + 0x16));
    if (al == 0) {
        goto label_104;
    }
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    edx = 0;
    *((rbp - segment.NOTE_1)) = r9;
    eax = unlinkat ();
    r9 = *((rbp - segment.NOTE_1));
    if (eax != 0) {
        goto label_291;
    }
    if (*((rbx + 0x3c)) != 0) {
        goto label_292;
    }
label_127:
    if (*((rbx + 0x28)) == 0) {
        goto label_105;
    }
    edx = *((rbp - 0x3a0));
    r8 = rbx;
    ecx = 1;
    rdi = r15;
    rsi = *((rbp - 0x330));
    eax = set_process_security_ctx ();
    r9d = eax;
    if (al != 0) {
        goto label_105;
    }
    *((rbp - segment.INTERP)) = 0;
    r12d = 0;
    goto label_76;
label_239:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "cannot remove %s");
    rcx = r14;
    eax = 0;
    error (0, *(r13), rax);
    goto label_7;
label_257:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x350)) = rax;
    rax = dcgettext (0, "failed to close %s");
    r12 = rax;
    rax = errno_location ();
    rcx = *((rbp - 0x350));
    eax = 0;
    error (0, *(rax), r12);
    r12 = *((rbp - 0x308));
    r9d = 0;
    goto label_76;
label_207:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot read symbolic link %s";
    r13 = rax;
    goto label_69;
label_277:
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    rdx = rbp - 0x39;
    ecx = 1;
    *((rbp - 0x378)) = r8d;
    *((rbp - 0x370)) = r10;
    rax = readlinkat ();
    r8d = *((rbp - 0x378));
    if (rax >= 0) {
        r9d = *((rbx + 0x3e));
        if (r9b == 0) {
            goto label_293;
        }
        eax = 0;
        eax = openat_safer (*((rbp - 0x320)), *((rbp - segment.UNKNOWN)), 0x41, *((rbp - segment.NOTE_1)), r8);
        r10 = *((rbp - 0x370));
        r12d = eax;
        r8d = *(r10);
    }
label_276:
    eax = r12d;
    eax >>= 0x1f;
    dl = (r8d == 0x15) ? 1 : 0;
    dl &= al;
    *((rbp - segment.INTERP)) = dl;
    if (dl == 0) {
        goto label_294;
    }
    r12 = *((rbp - 0x330));
    r8d = 0x15;
    if (*(r12) == 0) {
        goto label_106;
    }
    strlen (r12);
    r8d = 0;
    r8b = (*((r12 + rax - 1)) != 0x2f) ? 1 : 0;
    r8d += 0x14;
    goto label_106;
label_294:
    eax = *((rbp - 0x3a0));
    eax = ~eax;
    eax &= *((rbp - segment.NOTE_1));
    *((rbp - section..gnu.hash)) = eax;
    if (r12d < 0) {
        goto label_295;
    }
    eax = *((rbp - 0x380));
    *((rbp - segment.INTERP)) = 1;
    *((rbp - 0x3c4)) = eax;
    goto label_107;
label_281:
    eax = copy_acl (r15, r13d, *((rbp - 0x330)), r12d, *((rbp - 0x358)));
    r9d = 1;
    if (eax == 0) {
        goto label_95;
    }
    goto label_108;
label_209:
    eax = *(dst_sb.st_mode);
    eax &= case.0xf4aa.9;
    if (eax != 0xa000) {
        goto label_109;
    }
    rdx = *(dst_sb.st_size);
    *((rbp - 0x350)) = rdx;
    rax = strlen (r13);
    rdx = *((rbp - 0x350));
    if (rdx != rax) {
        goto label_109;
    }
    rax = areadlinkat_with_size (*((rbp - 0x320)), *((rbp - segment.UNKNOWN)), rdx);
    rdi = rax;
    if (rax == 0) {
        goto label_109;
    }
    *((rbp - 0x350)) = rax;
    eax = strcmp (rdi, r13);
    rdi = *((rbp - 0x350));
    if (eax != 0) {
        goto label_296;
    }
    fcn_00003670 ();
    rdi = r13;
    eax = fcn_00003670 ();
    goto label_110;
label_285:
    r12d = *((rbx + 0x18));
    r12d ^= 1;
    goto label_91;
label_252:
    eax = 0;
    edx = r13d;
    eax = ioctl (r12d, 0x40049409);
    if (eax == 0) {
        goto label_111;
    }
    if (*((rbx + 0x44)) != 2) {
        goto label_112;
    }
    rdx = r15;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = *((rbp - 0x330));
    esi = 4;
    edi = 0;
    *((rbp - segment.NOTE_1)) = rax;
    rax = quotearg_n_style ();
    edx = 5;
    *((rbp - 0x358)) = rax;
    rax = dcgettext (0, "failed to clone %s from %s");
    *((rbp - 0x350)) = rax;
    rax = errno_location ();
    r8 = *((rbp - segment.NOTE_1));
    rcx = *((rbp - 0x358));
    eax = 0;
    error (0, *(rax), *((rbp - 0x350)));
    r9d = 0;
    goto label_95;
label_250:
    if (*((rbx + 0x33)) == 0) {
        goto label_113;
    }
    goto label_114;
label_286:
    r13d = r9d;
    r9d = 0;
    goto label_27;
label_173:
    stack_chk_fail ();
label_270:
    rsi = *((rbp - segment.UNKNOWN));
    edi = *((rbp - 0x320));
    ecx = 0;
    rdx = sb_st_dev;
    eax = fstatat ();
    if (eax == 0) {
        goto label_115;
    }
    goto label_61;
label_282:
    eax = set_acl (*((rbp - 0x330)), r12d, *((rbx + 0x10)));
    r9b = (eax == 0) ? 1 : 0;
    goto label_95;
label_287:
    eax = *((rbx + 0xc));
    if (eax == 3) {
        goto label_297;
    }
    if (r8d == 1) {
        goto label_116;
    }
    if (eax != 2) {
        goto label_116;
    }
label_297:
    ecx = 2;
    edx = 0;
    esi = 0;
    edi = r13d;
    *((rbp - segment.NOTE_1)) = r8d;
    eax = fdadvise ();
    r8d = *((rbp - segment.NOTE_1));
    *((rbp - 0x30a)) = 0;
    if (r8d == 3) {
        goto label_298;
    }
    eax = 0;
    al = (*((rbx + 0x44)) != 0) ? 1 : 0;
    r9d = 0;
    *((rbp - 0x398)) = eax;
    r9b = (*((rbx + 0xc)) == 3) ? 1 : 0;
    goto label_117;
label_290:
    rsi = *((rbp - 0x300));
    edi = r12d;
    eax = ftruncate ();
    if (eax >= 0) {
        goto label_118;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x358)) = rax;
label_124:
    rax = dcgettext (0, "failed to extend %s");
    *((rbp - 0x350)) = rax;
    rax = errno_location ();
    rcx = *((rbp - 0x358));
    eax = 0;
    eax = error (0, *(rax), *((rbp - 0x350)));
    goto label_94;
label_298:
    eax = 0;
    al = (*((rbx + 0x44)) != 0) ? 1 : 0;
    *((rbp - 0x398)) = eax;
    eax = *((rbx + 0xc));
    *((rbp - 0x3c0)) = eax;
label_122:
    rax = *(src_open_sb.st_size);
    *((rbp - segment.NOTE_1)) = rax;
    rax = *((rbp - 0x388));
    if (rax < 0) {
        goto label_299;
    }
    edi = *((rbp - 0x350));
    *((rbp - 0x370)) = r15;
    *((rbp - 0x3a8)) = 0;
    *((rbp - 0x3d0)) = 0;
    *((rbp - 0x394)) = dil;
    edi = 0;
    *((rbp - 0x388)) = r14;
    r14 = rdi;
    *((rbp - 0x390)) = rbx;
    rbx = rax;
    edx = 4;
    rsi = rbx;
    edi = r13d;
    rax = lseek ();
    r15 = rax;
    if (rax >= 0) {
        goto label_300;
    }
    rax = errno_location ();
    if (*(rax) != 6) {
        goto label_301;
    }
    rax = *((rbp - segment.NOTE_1));
    r15 = *((rbp - segment.NOTE_1));
    if (rbx >= rax) {
        goto label_302;
    }
label_120:
    edx = 0;
    rsi = rbx;
    edi = r13d;
    rax = lseek ();
    if (rax < 0) {
        goto label_301;
    }
    rcx = rbx;
    rcx -= *((rbp - 0x3d0));
    rcx -= r14;
    if (rcx == 0) {
        goto label_303;
    }
    eax = *((rbp - 0x3c0));
    if (eax == 1) {
        goto label_304;
    }
    dl = (eax == 3) ? 1 : 0;
    al = create_hole (r12d, *((rbp - 0x330)), 0, rcx);
    *((rbp - 0x394)) = al;
    if (al == 0) {
        goto label_305;
    }
    r15 -= rbx;
    r14 = r15;
label_119:
    rax = rbp - 0x309;
    r15 = *((rbp - 0x370));
    rax = rbp - 0x300;
    eax = *((rbp - 0x398));
    al = sparse_copy (r13d, r12d, rbp - 0x308, *((rbp - 0x378)), *((rbp - 0x3b8)), 1);
    if (al == 0) {
        goto label_305;
    }
    rax = *((rbp - 0x300));
    edi = *((rbp - 0x394));
    edx = *((rbp - 0x309));
    rsi = rbx + rax;
    if (rax != 0) {
        edi = edx;
    }
    *((rbp - 0x3a8)) = rsi;
    *((rbp - 0x394)) = dil;
    if (rax < r14) {
        goto label_306;
    }
    edx = 3;
    edi = r13d;
    rax = lseek ();
    if (rax < 0) {
        void (*0xa928)() ();
    }
label_303:
    r15 -= rbx;
    if (*((rbp - 0x3c0)) != 1) {
        rcx = *((rbp - 0x3b8));
    }
    *((rbp - 0x394)) = 0;
    r14 = r15;
    r8 = rcx;
    goto label_119;
label_300:
    rax = *((rbp - segment.NOTE_1));
    if (rax < r15) {
        rax = r15;
    }
    *((rbp - segment.NOTE_1)) = rax;
    goto label_120;
label_130:
    r8d = *((rbp - 0x350));
label_129:
    r8d ^= 1;
    goto label_121;
label_302:
    edx = 2;
    esi = 0;
    edi = r13d;
    rax = lseek ();
    r15 = rax;
    if (rax < 0) {
        goto label_301;
    }
    if (rbx >= rax) {
        goto label_307;
    }
    *((rbp - segment.NOTE_1)) = rax;
    goto label_120;
label_304:
    al = write_zeros (r12d, rcx);
    if (al == 0) {
        goto label_308;
    }
    r15 -= rbx;
    *((rbp - 0x394)) = 0;
    r8d = 0;
    r14 = r15;
    goto label_119;
label_289:
    *((rbp - 0x3c0)) = 1;
    goto label_122;
label_288:
    rax = *(src_open_sb.st_size);
    if (rax >= *((rbp - 0x378))) {
        goto label_123;
    }
    rax++;
    *((rbp - 0x378)) = rax;
    goto label_123;
label_280:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x370)) = rax;
    rax = dcgettext (0, "preserving times for %s");
    *((rbp - segment.NOTE_1)) = rax;
    rax = errno_location ();
    rcx = *((rbp - 0x370));
    eax = 0;
    error (0, *(rax), *((rbp - segment.NOTE_1)));
    if (*((rbx + 0x32)) == 0) {
        goto label_98;
    }
    goto label_94;
label_253:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot fstat %s";
    *((rbp - 0x358)) = rax;
    goto label_124;
label_251:
    *((rbp - segment.INTERP)) = 0;
    goto label_95;
label_262:
    rax = *(src_open_sb.st_dev);
    if (*(src_sb.st_dev) != rax) {
        goto label_125;
    }
    if (*((rbx + 0x18)) == 0) {
        goto label_309;
    }
    edx = 5;
    rax = dcgettext (0, "backing up %s might destroy source;  %s not moved");
    r12 = rax;
    do {
        rdx = r15;
        esi = 4;
        edi = 1;
        rax = quotearg_n_style ();
        rdx = *((rbp - 0x330));
        esi = 4;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        goto label_126;
label_309:
        edx = 5;
        rax = dcgettext (0, "backing up %s might destroy source;  %s not copied");
        r12 = rax;
    } while (1);
label_291:
    if (*(r9) == 2) {
        goto label_127;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    *((rbp - 0x350)) = r9;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot remove %s");
    r9 = *((rbp - 0x350));
    rcx = r12;
    rdx = rax;
    esi = *(r9);
    goto label_90;
label_256:
    r8d = 2;
    goto label_128;
label_255:
    r8d = 3;
    goto label_128;
label_283:
    if (*(obj.mask.0) == 0xffffffff) {
        goto label_310;
    }
label_134:
    edx = ~edx;
    edx &= 0x1b6;
    eax = set_acl (*((rbp - 0x330)), r12d, *(obj.mask.0));
    eax = *((rbp - 0x358));
    r9b = (eax == 0) ? 1 : 0;
    *((rbp - segment.INTERP)) = al;
    goto label_95;
label_301:
    r14 = *((rbp - 0x388));
    r15 = *((rbp - 0x370));
    rbx = *((rbp - 0x390));
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - segment.NOTE_1)) = rax;
label_133:
    rax = dcgettext (0, "cannot lseek %s");
    *((rbp - 0x350)) = rax;
    rax = errno_location ();
    rcx = *((rbp - segment.NOTE_1));
    eax = 0;
    error (0, *(rax), *((rbp - 0x350)));
label_131:
    r8d = 0;
    goto label_129;
label_306:
    r14 = *((rbp - 0x388));
    r15 = *((rbp - 0x370));
    rbx = *((rbp - 0x390));
    r8 = *((rbp - 0x3a8));
label_132:
    *((rbp - segment.NOTE_1)) = r8;
    void (*0xa944)() ();
label_299:
    *((rbp - 0x3a8)) = 0;
    dl = (*((rbp - segment.NOTE_1)) > 0) ? 1 : 0;
    if (*((rbp - 0x3c0)) == 1) {
        goto label_311;
    }
    rsi = *((rbp - segment.NOTE_1));
    edi = r12d;
    *((rbp - 0x370)) = dl;
    eax = ftruncate ();
    if (eax != 0) {
        goto label_312;
    }
    if (*((rbp - 0x3c0)) != 3) {
        goto label_130;
    }
    edx = *((rbp - 0x370));
    if (dl == 0) {
        goto label_130;
    }
    rcx = *((rbp - segment.NOTE_1));
    rdx = *((rbp - 0x3a8));
    esi = 3;
    edi = r12d;
    rcx -= rdx;
    eax = fallocate ();
    if (eax >= 0) {
        goto label_130;
    }
    rax = errno_location ();
    r9 = rax;
    eax = *(rax);
    r8b = (eax == 0x5f) ? 1 : 0;
    al = (eax == 0x26) ? 1 : 0;
    r8b |= al;
    if (r8b != 0) {
        goto label_130;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    *((rbp - segment.NOTE_1)) = r8b;
    *((rbp - 0x370)) = r9;
    rax = quotearg_style ();
    edx = 5;
    *((rbp - 0x350)) = rax;
    rax = dcgettext (0, "error deallocating %s");
    r9 = *((rbp - 0x370));
    rcx = *((rbp - 0x350));
    eax = 0;
    rax = error (0, *(r9), rax);
    r8d = *((rbp - segment.NOTE_1));
    goto label_129;
label_305:
    r14 = *((rbp - 0x388));
    r15 = *((rbp - 0x370));
    rbx = *((rbp - 0x390));
    goto label_131;
label_307:
    r14 = *((rbp - 0x388));
    r15 = *((rbp - 0x370));
    r8 = rax;
    rbx = *((rbp - 0x390));
    goto label_132;
label_308:
    rdx = *((rbp - 0x330));
    esi = 3;
    edi = 0;
    r14 = *((rbp - 0x388));
    r15 = *((rbp - 0x370));
    rbx = *((rbp - 0x390));
    rax = quotearg_n_style_colon ();
    edx = 5;
    rsi = "%s: write failed";
    *((rbp - segment.NOTE_1)) = rax;
    goto label_133;
label_310:
    eax = umask (0);
    edi = eax;
    *(obj.mask.0) = eax;
    umask (edi);
    goto label_134;
label_311:
    rsi -= *((rbp - 0x3a8));
    al = write_zeros (r12d, *((rbp - segment.NOTE_1)));
    if (al != 0) {
        goto label_130;
    }
label_312:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "failed to extend %s";
    *((rbp - segment.NOTE_1)) = rax;
    goto label_133;
label_292:
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "removed %s\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    goto label_127;
label_284:
    *((rbp - segment.NOTE_1)) = r9b;
    eax = umask (0);
    edi = eax;
    *(obj.mask.0) = eax;
    *((rbp - 0x358)) = eax;
    umask (edi);
    edx = *((rbp - 0x358));
    r9d = *((rbp - segment.NOTE_1));
    eax = edx;
    eax = ~eax;
    eax &= *((rbp - 0x3c4));
    eax |= *((rbp - 0x350));
    if (eax == 0) {
        goto label_95;
    }
    edx++;
    if (edx == 0) {
        eax = umask (0);
        edi = eax;
        *(obj.mask.0) = eax;
        umask (edi);
    }
    eax = mask.0;
    goto label_135;
label_221:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot access %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    r12d = 0;
    error (0, *(rax), r12);
    goto label_49;
label_295:
    *((rbp - segment.INTERP)) = 1;
    goto label_106;
label_293:
    rsi = *((rbp - 0x330));
    edi = 4;
    *((rbp - 0x350)) = r9b;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "not writing through dangling symlink %s");
    rcx = r12;
    eax = 0;
    r12d = 0;
    error (0, 0, rax);
    r9d = *((rbp - 0x350));
    goto label_76;
label_263:
    rax = errno_location ();
    r12 = rax;
    if (*(rax) == 2) {
        *((rbp - segment.INTERP)) = 1;
        r13d = 0x11;
        *((rbp - 0x348)) = 0;
        goto label_17;
label_278:
        *((rbp - 0x350)) = 0;
        goto label_112;
label_296:
        fcn_00003670 ();
        goto label_109;
    }
    rsi = *((rbp - 0x330));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot backup %s";
    r13 = rax;
    goto label_136;
}

/* /tmp/tmp7e2uf9hg @ 0xb170 */
 
int64_t dbg_src_to_dest_hash (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t src_to_dest_hash( const * x,size_t table_size); */
    rax = *(rdi);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xb180 */
 
int64_t dbg_src_to_dest_compare ( const * x,  const * y) {
    rdi = x;
    rsi = y;
    /* _Bool src_to_dest_compare( const * x, const * y); */
    rdx = *(rsi);
    eax = 0;
    if (*(rdi) != rdx) {
        return eax;
    }
    rax = *((rsi + 8));
    al = (*((rdi + 8)) == rax) ? 1 : 0;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xb1a0 */
 
void src_to_dest_free (int64_t arg1) {
    rdi = arg1;
    rdi = *((rdi + 0x10));
    fcn_00003670 ();
    rdi = rbp;
    return void (*0x3670)() ();
}

/* /tmp/tmp7e2uf9hg @ 0xb350 */
 
int64_t dbg_try_link (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int try_link(char * dest,void * arg); */
    rax = rsi;
    edx = *((rsi + 0x10));
    rcx = rdi;
    rsi = *((rsi + 8));
    r8d = *((rax + 0x14));
    edi = *(rax);
    return linkat ();
}

/* /tmp/tmp7e2uf9hg @ 0xb370 */
 
uint64_t dbg_samedir_template (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* char * samedir_template(char const * dstname,char * buf); */
    rbx = rsi;
    rax = last_component ();
    rax -= rbp;
    rdi = rax + 9;
    r12 = rax;
    if (rdi > 0x100) {
        rax = malloc (rdi);
        rbx = rax;
        if (rax == 0) {
            goto label_0;
        }
    }
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    mempcpy ();
    rdx = "CuXXXXXX";
    *(rax) = rdx;
    edx = *(0x00016460);
    *((rax + 8)) = dl;
    rax = rbx;
    do {
        return rax;
label_0:
        eax = 0;
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0xb3d0 */
 
int64_t dbg_try_symlink (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int try_symlink(char * dest,void * arg); */
    rax = rsi;
    rdx = rdi;
    esi = *((rsi + 8));
    rdi = *(rax);
    return symlinkat ();
}

/* /tmp/tmp7e2uf9hg @ 0xbaf0 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x5100)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x3c40 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c20 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp7e2uf9hg @ 0x39b0 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b30 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmp7e2uf9hg @ 0x36e0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmp7e2uf9hg @ 0x10f60 */
 
int64_t dbg_direntry_cmp_inode ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    /* int direntry_cmp_inode( const * a, const * b); */
    rax = *((rsi + 8));
    al = (*((rdi + 8)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x10f80 */
 
void dbg_direntry_cmp_name (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int direntry_cmp_name( const * a, const * b); */
    rsi = *(rsi);
    rdi = *(rdi);
    return strcmp ();
}

/* /tmp/tmp7e2uf9hg @ 0x11360 */
 
int64_t dbg_try_nocreate (const char * path) {
    stat st;
    int64_t var_98h;
    rdi = path;
    if (? == ?) {
        /* int try_nocreate(char * tmpl,void * flags); */
        void (*0x11371)() ();
    }
    __asm ("cli");
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    eax = lstat (rdi, rsp);
    ebx = eax;
    rax = errno_location ();
    if (ebx != 0) {
        edx = *(rax);
        if (edx != 0x4b) {
            eax = 0;
            al = (edx != 2) ? 1 : 0;
            eax = -eax;
        }
    } else {
        *(rax) = 0x11;
        eax = 0xffffffff;
    }
    rdx = *((rsp + 0x98));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x113e0 */
 
void dbg_try_dir (void) {
    /* int try_dir(char * tmpl,void * flags); */
    esi = 0x1c0;
    return mkdir ();
}

/* /tmp/tmp7e2uf9hg @ 0x113f0 */
 
int32_t dbg_try_file (int64_t arg2) {
    rsi = arg2;
    /* int try_file(char * tmpl,void * flags); */
    esi = *(rsi);
    edx = 0x180;
    eax = 0;
    sil &= 0x3c;
    sil |= 0xc2;
    return open ();
}

/* /tmp/tmp7e2uf9hg @ 0x11790 */
 
int64_t dbg_dev_info_hash (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t dev_info_hash( const * x,size_t table_size); */
    rax = *(rdi);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x117a0 */
 
int64_t dbg_dev_info_compare ( const * x,  const * y) {
    rdi = x;
    rsi = y;
    /* _Bool dev_info_compare( const * x, const * y); */
    rax = *(rsi);
    al = (*(rdi) == rax) ? 1 : 0;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x134c0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmp7e2uf9hg @ 0x10240 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0xc860 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0xb750 */
 
uint64_t dbg_set_acl (char *** arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int set_acl(char const * name,int desc,mode_t mode); */
    eax = qset_acl (rdi, rsi, rdx);
    r12d = eax;
    if (eax == 0) {
        eax = r12d;
        return eax;
    }
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "setting permissions for %s");
    r13 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    error (0, *(rax), r13);
    eax = r12d;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xe790 */
 
int64_t dbg_qset_acl (int64_t arg1, int64_t arg2, int64_t arg3) {
    permission_context ctx;
    int64_t var_4h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int qset_acl(char const * name,int desc,mode_t mode); */
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rbp = rsp + 4;
    *((rsp + 4)) = edx;
    eax = set_permissions (rbp, rdi, esi);
    rdi = rbp;
    r12d = eax;
    free_permission_context ();
    rax = *((rsp + 8));
    rax -= *(fs:0x28);
    if (rax == 0) {
        eax = r12d;
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x12e00 */
 
uint32_t dbg_set_permissions (int64_t arg1, int64_t arg2, uint32_t fd) {
    rdi = arg1;
    rsi = arg2;
    rdx = fd;
    /* int set_permissions(permission_context * ctx,char const * name,int desc); */
    r8 = rsi;
    esi = *(rdi);
    if (edx != 0xffffffff) {
        eax = fchmod (edx, rsi);
        eax = -eax;
        eax -= eax;
        return eax;
    }
    eax = chmod (r8, rsi);
    eax = -eax;
    eax -= eax;
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0x3b50 */
 
void fchmod (void) {
    __asm ("bnd jmp qword [reloc.fchmod]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b60 */
 
void chmod (void) {
    __asm ("bnd jmp qword [reloc.chmod]");
}

/* /tmp/tmp7e2uf9hg @ 0x12dc0 */
 
void dbg_free_permission_context (permission_context * ctx) {
    rdi = ctx;
    /* void free_permission_context(permission_context * ctx); */
}

/* /tmp/tmp7e2uf9hg @ 0x10ab0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, uint32_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0x36d0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp7e2uf9hg @ 0x10570 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x10da0 */
 
int64_t dbg_same_nameat (int64_t arg1, int64_t arg2, char *** arg3, char *** arg4) {
    stat source_dir_stats;
    stat dest_dir_stats;
    char *** var_8h;
    int64_t var_fh;
    uint32_t var_10h;
    uint32_t var_18h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool same_nameat(int source_dfd,char const * source,int dest_dfd,char const * dest); */
    r15 = rsi;
    r14 = rcx;
    ebx = edi;
    rdi = rsi;
    *((rsp + 8)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x138)) = rax;
    eax = 0;
    rax = last_component ();
    rdi = r14;
    rax = last_component ();
    r12 = rax;
    rax = base_len (rbp);
    r13 = rax;
    rax = base_len (r12);
    r9d = 0;
    while (eax != 0) {
label_0:
        rax = *((rsp + 0x138));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        eax = r9d;
        return rax;
        *((rsp + 0xf)) = r9b;
        eax = memcmp (rbp, r12, r13);
        r9d = 0;
    }
    rdi = r15;
    rax = dir_name ();
    rdx = rsp + 0x10;
    ecx = 0x100;
    edi = ebx;
    rsi = rax;
    eax = fstatat ();
    if (eax != 0) {
        goto label_3;
    }
label_1:
    rdi = rbp;
    fcn_00003670 ();
    rdi = r14;
    rax = dir_name ();
    edi = *((rsp + 8));
    ecx = 0x100;
    rdx = rsp + 0xa0;
    rsi = rax;
    eax = fstatat ();
    while (1) {
        rax = *((rsp + 0xa8));
        r9d = 0;
        if (*((rsp + 0x18)) == rax) {
            rax = *((rsp + 0xa0));
            r9b = (*((rsp + 0x10)) == rax) ? 1 : 0;
        }
        rdi = rbp;
        *((rsp + 8)) = r9b;
        fcn_00003670 ();
        r9d = *((rsp + 8));
        goto label_0;
        rax = errno_location ();
        rcx = rbp;
        eax = 0;
        error (1, *(rax), 0x000164c5);
    }
label_3:
    rax = errno_location ();
    rcx = rbp;
    eax = 0;
    error (1, *(rax), 0x000164c5);
    goto label_1;
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x12540 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x6ca0 */
 
uint64_t dbg_set_file_security_ctx (char *** arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    /* _Bool set_file_security_ctx(char const * dst_name,_Bool recurse,cp_options const * x); */
    r12 = rdi;
    rbx = rdx;
    rax = errno_location ();
    while (*((rbx + 0x34)) != 0) {
        *(rbp) = 0x5f;
        rdx = r12;
        esi = 4;
        edi = 0;
        rax = quotearg_n_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "failed to set the security context of %s");
        rcx = r12;
        eax = 0;
        eax = error (0, *(rbp), rax);
        eax = 0;
        return rax;
    }
    *(rax) = 0x5f;
    eax = 0;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xd070 */
 
void dbg_filemodestring (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void filemodestring(stat const * statp,char * str); */
    edi = *((rdi + 0x18));
    return void (*0xcef0)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x10080 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x127a0 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xb130 */
 
int32_t cached_umask (void) {
    r12d = mask.0;
    if (r12d != 0xffffffff) {
        eax = r12d;
        return eax;
    }
    eax = umask (0);
    r12d = eax;
    edi = eax;
    *(obj.mask.0) = eax;
    umask (edi);
    eax = r12d;
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0x3a60 */
 
void umask (void) {
    __asm ("bnd jmp qword [reloc.umask]");
}

/* /tmp/tmp7e2uf9hg @ 0xffa0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x132d0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xb310 */
 
uint64_t dbg_hash_init (void) {
    /* void hash_init(); */
    rax = hash_initialize (0x67, 0, dbg.src_to_dest_hash, dbg.src_to_dest_compare, sym.src_to_dest_free);
    *(obj.src_to_dest) = rax;
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xdc50 */
 
uint64_t dbg_hash_initialize (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* Hash_table * hash_initialize(size_t candidate,Hash_tuning const * tuning,Hash_hasher hasher,Hash_comparator comparator,Hash_data_freer data_freer); */
    rax = dbg_raw_hasher;
    r15 = rsi;
    r14 = r8;
    r13 = rdi;
    edi = 0x50;
    rbx = rcx;
    if (rdx == 0) {
    }
    rax = dbg_raw_comparator;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = malloc (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = obj_default_tuning;
    rdi = r12;
    if (r15 == 0) {
        r15 = rax;
    }
    *((r12 + 0x28)) = r15;
    al = check_tuning ();
    if (al == 0) {
        goto label_1;
    }
    esi = *((r15 + 0x10));
    xmm0 = *((r15 + 8));
    rdi = r13;
    rax = compute_bucket_size_isra_0 ();
    *((r12 + 0x10)) = rax;
    r13 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = calloc (rax, 0x10);
    *(r12) = rax;
    if (rax == 0) {
        goto label_1;
    }
    r13 <<= 4;
    *((r12 + 0x30)) = rbp;
    rax += r13;
    *((r12 + 0x38)) = rbx;
    *((r12 + 8)) = rax;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x40)) = r14;
    *((r12 + 0x48)) = 0;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        rdi = r12;
        r12d = 0;
        fcn_00003670 ();
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0x39c0 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmp7e2uf9hg @ 0x10000 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xd850 */
 
int64_t dbg_hash_print_statistics (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void hash_print_statistics(Hash_table const * table,FILE * stream); */
    r12d = 0;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8 = *((rdi + 0x20));
    rbx = *((rdi + 0x10));
    r13 = *((rdi + 0x18));
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_2;
    do {
        rcx += 0x10;
        if (rsi <= rcx) {
            goto label_2;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_3;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_3:
    if (r12 < rdx) {
        r12 = rdx;
    }
    rcx += 0x10;
    if (rsi > rcx) {
        goto label_0;
    }
label_2:
    rcx = r8;
    rdx = "# entries:         %lu\n";
    rdi = rbp;
    eax = 0;
    esi = 1;
    eax = fprintf_chk ();
    eax = 0;
    rcx = rbx;
    esi = 1;
    rdx = "# buckets:         %lu\n";
    rdi = rbp;
    fprintf_chk ();
    if (r13 < 0) {
        goto label_4;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, r13");
    __asm ("mulsd xmm0, qword [0x00016610]");
    if (rbx < 0) {
        goto label_5;
    }
    do {
        xmm1 = 0;
        __asm ("cvtsi2sd xmm1, rbx");
label_1:
        __asm ("divsd xmm0, xmm1");
        rcx = r13;
        rdi = rbp;
        esi = 1;
        rdx = "# buckets used:    %lu (%.2f%%)\n";
        eax = 1;
        eax = fprintf_chk ();
        rcx = r12;
        rdi = rbp;
        rdx = "max bucket length: %lu\n";
        esi = 1;
        eax = 0;
        void (*0x3c40)() ();
label_4:
        rax = r13;
        rdx = r13;
        xmm0 = 0;
        rax >>= 1;
        edx &= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm0, rax");
        __asm ("addsd xmm0, xmm0");
        __asm ("mulsd xmm0, qword [0x00016610]");
    } while (rbx >= 0);
label_5:
    rax = rbx;
    ebx &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rbx;
    __asm ("cvtsi2sd xmm1, rax");
    __asm ("addsd xmm1, xmm1");
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0xe660 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite (0x00016618, 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmp7e2uf9hg @ 0x3890 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c30 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp7e2uf9hg @ 0xbc70 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x3b80)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0x10a90 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0x6df0 */
 
uint64_t chown_failure_ok (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    rax = errno_location ();
    edx = *(rax);
    al = (edx == 1) ? 1 : 0;
    dl = (edx == 0x16) ? 1 : 0;
    al |= dl;
    if (al != 0) {
        eax = *((rbx + 0x1a));
        eax ^= 1;
    }
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x107d0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x3d22)() ();
    }
    if (rdx == 0) {
        void (*0x3d22)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xe2a0 */
 
int64_t dbg_hash_insert (int64_t arg2) {
     const * matched_ent;
    int64_t var_8h;
    rsi = arg2;
    /* void * hash_insert(Hash_table * table, const * entry); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    eax = hash_insert_if_absent ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    rax = rbx;
    rax = *(rsp);
    while (1) {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        eax = 0;
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x6bc0 */
 
uint64_t set_process_security_ctx (int64_t arg1, uint32_t arg5) {
    rdi = arg1;
    r8 = arg5;
    rbx = r8;
    if (*((r8 + 0x33)) == 0) {
        goto label_1;
    }
    rax = errno_location ();
    r13 = rax;
    while (r12b != 0) {
        *(r13) = 0x5f;
        rsi = rbp;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "failed to get security context of %s");
        rcx = r12;
        eax = 0;
        error (0, *(r13), rax);
        r12d = *((rbx + 0x34));
label_0:
        r12d ^= 1;
        eax = r12d;
        return rax;
label_1:
        r12b = (*((r8 + 0x28)) != 0) ? 1 : 0;
        r12b &= cl;
        if (r12b != 0) {
            goto label_2;
        }
        r12d = 1;
        eax = r12d;
        return rax;
        r12d = *((rbx + 0x34));
    }
    *(rax) = 0x5f;
    goto label_0;
label_2:
    errno_location ();
    *(rax) = 0x5f;
    eax = r12d;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xe710 */
 
int64_t dbg_qcopy_acl (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    permission_context ctx;
    int64_t var_4h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int qcopy_acl(char const * src_name,int source_desc,char const * dst_name,int dest_desc,mode_t mode); */
    r13 = rdx;
    edx = r8d;
    r12d = ecx;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rbp = rsp + 4;
    rcx = rbp;
    eax = get_permissions ();
    if (eax != 0) {
        goto label_0;
    }
    eax = set_permissions (rbp, r13, r12d);
    rdi = rbp;
    r12d = eax;
    free_permission_context ();
    do {
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        eax = r12d;
        return rax;
label_0:
        r12d = 0xfffffffe;
    } while (1);
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x10870 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3d27)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x3d27)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x10f90 */
 
int64_t dbg_streamsavedir (int64_t arg1, int64_t arg2) {
    idx_t allocated;
    idx_t entries_allocated;
    int64_t var_10h;
    int64_t var_8h;
    int64_t var_sp_10h;
    int64_t var_18h;
    uint32_t var_28h;
    uint32_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    /* char * streamsavedir(DIR * dirp,savedir_option option); */
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = obj_comparison_function_table;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    rax = *((rax + rsi*8));
    *(rsp) = rax;
    if (rdi == 0) {
        goto label_7;
    }
    ebx = 0;
    r13d = 0;
    r15d = 0;
    rax = errno_location ();
    r14d = 0;
    r12 = rax;
    while (al != 0x2e) {
label_0:
        if (al != 0) {
            goto label_8;
        }
label_1:
        *(r12) = 0;
        rdi = rbp;
        rax = readdir ();
        r9 = rax;
        if (rax == 0) {
            goto label_9;
        }
        r10 = rax + 0x13;
        eax = *((rax + 0x13));
    }
    eax = *((r9 + 0x14));
    if (al != 0x2e) {
        goto label_0;
    }
    eax = *((r9 + 0x15));
    if (al == 0) {
        goto label_1;
    }
label_8:
    rdi = r10;
    *((rsp + 8)) = r10;
    *((rsp + 0x10)) = r9;
    strlen (rdi);
    r10 = *((rsp + 8));
    r11 = rax + 1;
    if (*(rsp) == 0) {
        goto label_10;
    }
    r9 = *((rsp + 0x10));
    if (*((rsp + 0x30)) == r13) {
        goto label_11;
    }
label_3:
    rdx = r13;
    *((rsp + 0x18)) = r9;
    r13++;
    rdx <<= 4;
    *((rsp + 0x10)) = r11;
    rdx += r15;
    *((rsp + 8)) = rdx;
    rax = xstrdup (r10);
    rdx = *((rsp + 8));
    r9 = *((rsp + 0x18));
    r11 = *((rsp + 0x10));
    *(rdx) = rax;
    rax = *(r9);
    *((rdx + 8)) = rax;
label_2:
    rbx += r11;
    goto label_1;
label_9:
    eax = *(r12);
    if (eax != 0) {
        goto label_12;
    }
    if (*(rsp) == 0) {
        goto label_13;
    }
    rbp = rbx + 1;
    if (r13 == 0) {
        rdi = rbp;
        rax = ximalloc ();
        r14 = rax;
        rbx = rax;
        goto label_14;
    }
    rcx = *(rsp);
    rsi = r13;
    edx = 0x10;
    rdi = r15;
    r13 <<= 4;
    ebx = 0;
    qsort ();
    rdi = rbp;
    r13 += r15;
    rax = ximalloc ();
    r14 = rax;
    do {
        rsi = *(rbp);
        r12 = r14 + rbx;
        rbp += 0x10;
        rdi = r12;
        rax = stpcpy ();
        rdi = *((rbp - 0x10));
        rax -= r12;
        rbx = rbx + rax + 1;
        fcn_00003670 ();
    } while (r13 != rbp);
    rbx += r14;
label_14:
    rdi = r15;
    fcn_00003670 ();
    do {
        *(rbx) = 0;
label_6:
        rax = *((rsp + 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        rax = r14;
        return rax;
label_10:
        rax = *((rsp + 0x28));
        rax -= rbx;
        if (rax <= r11) {
            goto label_16;
        }
label_4:
        rdx = r11;
        *((rsp + 8)) = r11;
        memcpy (r14 + rbx, r10, rdx);
        r11 = *((rsp + 8));
        goto label_2;
label_13:
        if (rbx == *((rsp + 0x28))) {
            goto label_17;
        }
label_5:
        rbx += r14;
    } while (1);
label_11:
    r8d = 0x10;
    *((rsp + 0x18)) = r10;
    *((rsp + 8)) = r11;
    rax = xpalloc (r15, rsp + 0x30, 1, 0xffffffffffffffff);
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x10));
    r11 = *((rsp + 8));
    r15 = rax;
    goto label_3;
label_16:
    rdx = r11;
    *((rsp + 0x10)) = r10;
    rdx -= rax;
    r8d = 1;
    *((rsp + 8)) = r11;
    rcx = 0x7ffffffffffffffe;
    rax = xpalloc (r14, rsp + 0x28, rdx, rcx);
    r10 = *((rsp + 0x10));
    r11 = *((rsp + 8));
    r14 = rax;
    goto label_4;
label_17:
    rax = xirealloc (r14, rbx + 1);
    r14 = rax;
    goto label_5;
label_12:
    rdi = r15;
    fcn_00003670 ();
    rdi = r14;
    r14d = 0;
    fcn_00003670 ();
    goto label_6;
label_7:
    r14d = 0;
    goto label_6;
label_15:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xb230 */
 
int64_t dbg_src_to_dest_lookup (uint32_t arg1, uint32_t arg2) {
    Src_to_dest ent;
    uint32_t var_8h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    /* char * src_to_dest_lookup(ino_t ino,dev_t dev); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    *(rsp) = rdi;
    rdi = src_to_dest;
    *((rsp + 8)) = rsi;
    rsi = rsp;
    rax = hash_lookup ();
    if (rax != 0) {
        rax = *((rax + 0x10));
    }
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x11410 */
 
int64_t dbg_try_tempname_len (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    random_value v;
    random_value r;
    timespec tv;
    int64_t var_8h;
    uint32_t var_10h;
    int64_t var_1bh;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int try_tempname_len(char * tmpl,int suffixlen,void * args,int (*)() tryfunc,size_t x_suffix_len); */
    r15 = rdi;
    r14 = r8;
    ebx = esi;
    *((rsp + 0x28)) = rdx;
    *((rsp + 8)) = rdi;
    *((rsp + 0x30)) = rcx;
    *((rsp + 0x10)) = r8;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = errno_location ();
    *((rsp + 0x20)) = rax;
    eax = *(rax);
    *((rsp + 0x4c)) = eax;
    rax = rsp + 0x50;
    rax >>= 4;
    *((rsp + 0x50)) = rax;
    rax = dbg_try_nocreate;
    r9b = (rbp == rax) ? 1 : 0;
    *(rsp) = r9b;
    rax = strlen (r15);
    rdx = (int64_t) ebx;
    rdx += r14;
    if (rdx > rax) {
        goto label_4;
    }
    rax -= rdx;
    r15 += rax;
    *((rsp + 0x38)) = rax;
    rdi = r15;
    *((rsp + 0x40)) = r15;
    rax = strspn (rdi, 0x000164dd);
    r9d = *(rsp);
    if (rax < r14) {
        goto label_4;
    }
    r8d = 0;
    *((rsp + 0x48)) = 0x3a2f8;
    r15 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    r14 = 0x27bb2ee687b0b0fd;
    *((rsp + 0x1b)) = r9b;
    *((rsp + 0x1c)) = r8d;
label_2:
    if (*((rsp + 0x10)) == 0) {
        goto label_5;
    }
    rax = *((rsp + 8));
    rax += *((rsp + 0x10));
    r12d = 0xb504f32d;
    rax += *((rsp + 0x38));
    r13 = *((rsp + 0x50));
    rbx = *((rsp + 0x40));
    r9d = *((rsp + 0x1b));
    *(rsp) = rax;
    r8d = *((rsp + 0x1c));
    while (r8d != 0) {
        r8d--;
label_1:
        rdx = r13;
        rcx = r13;
        rbx++;
        rax = 0x8421084210842109;
        rdx >>= 1;
        rdx:rax = rax * rdx;
        rdx >>= 4;
        rax = rdx;
        *((rsp + 0x50)) = rdx;
        r13 = rdx;
        rax <<= 5;
        rax -= rdx;
        rax += rax;
        rcx -= rax;
        eax = *((r15 + rcx));
        *((rbx - 1)) = al;
        if (rbx == *(rsp)) {
            goto label_6;
        }
    }
    if (r9b != 0) {
        goto label_0;
    }
    do {
        rsi = rsp + 0x60;
        edi = 1;
        clock_gettime ();
        rcx = *((rsp + 0x68));
        rcx ^= r13;
        rcx *= r14;
        r13 = rcx + r12;
        *((rsp + 0x50)) = r13;
        if (r13 <= rbp) {
            goto label_7;
        }
label_0:
        rdi = rsp + 0x58;
        edx = 1;
        esi = 8;
        rax = getrandom ();
    } while (rax != 8);
    r13 = *((rsp + 0x58));
    *((rsp + 0x50)) = r13;
    if (r13 > rbp) {
        goto label_0;
    }
label_7:
    r8d = 9;
    r9d = 1;
    goto label_1;
label_6:
label_5:
    rsi = *((rsp + 0x28));
    rdi = *((rsp + 8));
    rax = *((rsp + 0x30));
    eax = void (*rax)(uint64_t) (r9b);
    if (eax >= 0) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    if (*(rax) != 0x11) {
        goto label_9;
    }
    if (*(rax) != 0x11) {
        goto label_2;
    }
label_9:
    eax = 0xffffffff;
    do {
label_3:
        rdx = *((rsp + 0x78));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_10;
        }
        return rax;
label_8:
        rbx = *((rsp + 0x20));
        edi = *((rsp + 0x4c));
        *(rbx) = edi;
    } while (1);
label_4:
    rax = *((rsp + 0x20));
    *(rax) = 0x16;
    eax = 0xffffffff;
    goto label_3;
label_10:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x12eb0 */
 
uint64_t dbg_opendir_safer (void) {
    /* DIR * opendir_safer(char const * name); */
    rax = opendir ();
    r12 = rax;
    if (rax != 0) {
        rdi = rax;
        eax = dirfd ();
        if (eax <= 2) {
            goto label_1;
        }
    }
    rax = r12;
    return rax;
label_1:
    edi = eax;
    eax = 0;
    eax = rpl_fcntl (edi, 0x406, 3, rcx, r8, r9);
    rax = errno_location ();
    rbx = rax;
    if (ebp >= 0) {
        goto label_2;
    }
    r14d = *(rax);
    r13d = 0;
    do {
label_0:
        rdi = r12;
        r12 = r13;
        closedir ();
        *(rbx) = r14d;
        rax = r12;
        return rax;
label_2:
        edi = ebp;
        rax = fdopendir ();
        r14d = *(rbx);
        r13 = rax;
    } while (rax != 0);
    close (ebp);
    goto label_0;
}

/* /tmp/tmp7e2uf9hg @ 0x112f0 */
 
int8_t dbg_target_dirfd_valid (int32_t fd) {
    rdi = fd;
    /* _Bool target_dirfd_valid(int fd); */
    al = (edi != 0xffffffff) ? 1 : 0;
    return al;
}

/* /tmp/tmp7e2uf9hg @ 0xcb50 */
 
uint64_t dbg_mdir_name (uint32_t arg1) {
    rdi = arg1;
    /* char * mdir_name(char const * file); */
    ebx = 0;
    bl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbp;
    r12 = rax;
    while (rbx < r12) {
        rax = r12 - 1;
        if (*((rbp + r12 - 1)) != 0x2f) {
            goto label_2;
        }
        r12 = rax;
    }
    rax = r12;
    rax ^= 1;
    ebx = eax;
    ebx &= 1;
    rax = malloc (r12 + rax + 1);
    if (rax == 0) {
        goto label_3;
    }
    rax = memcpy (rax, rbp, r12);
    r8 = rax;
    if (bl == 0) {
        goto label_4;
    }
    *(rax) = 0x2e;
    r12d = 1;
    do {
label_0:
        *((r8 + r12)) = 0;
label_1:
        rax = r8;
        return rax;
label_2:
        rax = malloc (r12 + 1);
        r8 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = memcpy (r8, rbp, r12);
        r8 = rax;
    } while (1);
label_4:
    r12d = 1;
    goto label_0;
label_3:
    r8d = 0;
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0xe560 */
 
void dbg_triple_free (int64_t arg1) {
    rdi = arg1;
    /* void triple_free(void * x); */
    rdi = *(rdi);
    fcn_00003670 ();
    rdi = rbp;
    return void (*0x3670)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x10730 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, char *** arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x3d1d)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x134d4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp7e2uf9hg @ 0xcc50 */
 
void fdadvise (void) {
    return posix_fadvise ();
}

/* /tmp/tmp7e2uf9hg @ 0xde00 */
 
int64_t dbg_hash_free (int64_t arg_8h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_free(Hash_table * table); */
    r12 = rdi;
    r13 = *(rdi);
    rax = *((rdi + 8));
    if (*((rdi + 0x40)) == 0) {
        goto label_2;
    }
    if (*((rdi + 0x20)) == 0) {
        goto label_2;
    }
    if (r13 < rax) {
        goto label_0;
    }
    goto label_3;
    do {
        r13 += 0x10;
        if (rax <= r13) {
            goto label_4;
        }
label_0:
        rdi = *(r13);
    } while (rdi == 0);
    rbx = r13;
    while (rbx != 0) {
        rdi = *(rbx);
        uint64_t (*r12 + 0x40)() ();
        rbx = *((rbx + 8));
    }
    rax = *((r12 + 8));
    r13 += 0x10;
    if (rax > r13) {
        goto label_0;
    }
label_4:
    rbp = *(r12);
label_2:
    if (rax <= rbp) {
        goto label_3;
    }
label_1:
    rbx = *((rbp + 8));
    if (rbx == 0) {
        goto label_5;
    }
    do {
        rdi = rbx;
        rbx = *((rbx + 8));
        fcn_00003670 ();
    } while (rbx != 0);
label_5:
    rbp += 0x10;
    if (*((r12 + 8)) > rbp) {
        goto label_1;
    }
label_3:
    rbx = *((r12 + 0x48));
    if (rbx == 0) {
        goto label_6;
    }
    do {
        rdi = rbx;
        rbx = *((rbx + 8));
        fcn_00003670 ();
    } while (rbx != 0);
label_6:
    rdi = *(r12);
    fcn_00003670 ();
    rdi = r12;
    return void (*0x3670)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x12820 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x12920 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xce00 */
 
uint64_t dbg_record_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg1, char *** arg2, uint32_t arg3) {
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void record_file(Hash_table * ht,char const * file,stat const * stats); */
    if (rdi != 0) {
        r13 = rsi;
        r12 = rdi;
        rbx = rdx;
        rax = xmalloc (0x18);
        rax = xstrdup (r13);
        rsi = rbp;
        *(rbp) = rax;
        rax = *((rbx + 8));
        *((rbp + 8)) = rax;
        rax = *(rbx);
        *((rbp + 0x10)) = rax;
        rax = hash_insert (r12);
        if (rax == 0) {
            goto label_0;
        }
        if (rbp != rax) {
            rdi = rbp;
            void (*0xe560)() ();
        }
        return rax;
    }
    return rax;
label_0:
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x127c0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xcaf0 */
 
uint64_t dbg_dir_name (void) {
    /* char * dir_name(char const * file); */
    rax = mdir_name (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xc800 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xd0a0 */
 
uint64_t dbg_mfile_name_concat (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * mfile_name_concat(char const * dir,char const * base,char ** base_in_result); */
    r12 = rsi;
    *((rsp + 8)) = rdx;
    rax = last_component ();
    r13 = rax;
    rax = base_len (rax);
    r13 -= rbp;
    r14 = r13 + rax;
    rbx = rax;
    rax = strlen (r12);
    r13 = rax;
    if (rbx == 0) {
        goto label_1;
    }
    if (*((rbp + r14 - 1)) == 0x2f) {
        goto label_2;
    }
    ebx = 0;
    r15d = 0;
    eax = 0x2f;
    if (*(r12) == 0x2f) {
        eax = r15d;
    }
    bl = (*(r12) != 0x2f) ? 1 : 0;
    *((rsp + 7)) = al;
    do {
label_0:
        rdi += rbx;
        rax = malloc (r14 + r13 + 1);
        r15 = rax;
        if (rax != 0) {
            rdi = rax;
            rdx = r14;
            rsi = rbp;
            mempcpy ();
            ecx = *((rsp + 7));
            rdi = rax + rbx;
            *(rax) = cl;
            rax = *((rsp + 8));
            if (rax != 0) {
                *(rax) = rdi;
            }
            rdx = r13;
            rsi = r12;
            mempcpy ();
            *(rax) = 0;
        }
        rax = r15;
        return rax;
label_1:
        ebx = 0;
        r15d = 0;
        eax = 0x2e;
        if (*(r12) != 0x2f) {
            eax = r15d;
        }
        bl = (*(r12) == 0x2f) ? 1 : 0;
        *((rsp + 7)) = al;
    } while (1);
label_2:
    *((rsp + 7)) = 0;
    ebx = 0;
    goto label_0;
}

/* /tmp/tmp7e2uf9hg @ 0x12700 */
 
uint64_t dbg_xalignalloc (void) {
    /* void * xalignalloc(idx_t alignment,idx_t size); */
    rax = aligned_alloc ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x3c60 */
 
void aligned_alloc (void) {
    __asm ("bnd jmp qword [reloc.aligned_alloc]");
}

/* /tmp/tmp7e2uf9hg @ 0xcdb0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x3ac0)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmp7e2uf9hg @ 0xdbe0 */
 
int64_t dbg_hash_string (int64_t arg1, size_t n_buckets) {
    rdi = arg1;
    rsi = n_buckets;
    /* size_t hash_string(char const * string,size_t n_buckets); */
    ecx = *(rdi);
    edx = 0;
    if (cl == 0) {
        goto label_0;
    }
    do {
        rax = rdx;
        rdi++;
        rax <<= 5;
        rax -= rdx;
        edx = 0;
        rax += rcx;
        ecx = *(rdi);
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
    } while (cl != 0);
label_0:
    rax = rdx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xc6e0 */
 
uint64_t dbg_find_backup_file_name (void) {
    /* char * find_backup_file_name(int dir_fd,char const * file,backup_type backup_type); */
    ecx = 0;
    rax = backupfile_internal (rdi, rsi, rdx);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xbfc0 */
 
int64_t dbg_backupfile_internal (int64_t arg1, void * arg2, uint32_t arg3) {
    int32_t sdir;
    int64_t var_8h;
    size_t s1;
    signed int64_t var_18h;
    signed int64_t var_20h;
    signed int64_t canary;
    void * var_37h;
    void * s2;
    void ** var_40h;
    int64_t var_48h;
    int64_t var_50h;
    size_t var_58h;
    uint32_t var_60h;
    uint32_t var_68h;
    int64_t var_6ch;
    size_t n;
    int64_t var_78h;
    int64_t var_84h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * backupfile_internal(int dir_fd,char const * file,backup_type backup_type,_Bool rename); */
    rbx = rsi;
    *((rsp + 0x6c)) = edi;
    rdi = rsi;
    *((rsp + 0x38)) = rsi;
    *((rsp + 0x68)) = edx;
    *((rsp + 0x37)) = cl;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rax = last_component ();
    r14 = rax;
    rdi = rax;
    *((rsp + 0x60)) = rax;
    rax = base_len (rdi);
    r14 -= rbx;
    rdi = simple_backup_suffix;
    *((rsp + 8)) = rax;
    rax += r14;
    *((rsp + 0x10)) = rax;
    if (rdi == 0) {
        goto label_18;
    }
label_12:
    rax = strlen (rdi);
    rax++;
    rsi = rax;
    *((rsp + 0x70)) = rax;
    eax = 9;
    if (rsi >= rax) {
        rax = rsi;
    }
    rsi = *((rsp + 0x10));
    rax = rsi + rax + 1;
    rdi = rax;
    *((rsp + 0x50)) = rax;
    rax = malloc (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_10;
    }
    *((rsp + 0x58)) = 0;
    eax = *((rsp + 0x6c));
    ebp = 0;
    *((rsp + 0x84)) = eax;
    rax = *((rsp + 8));
    rbx = rax + 4;
label_2:
    memcpy (r12, *((rsp + 0x38)), *((rsp + 0x10)));
    if (*((rsp + 0x68)) == 1) {
        goto label_19;
    }
    if (rbp == 0) {
        goto label_20;
    }
    rdi = rbp;
    rewinddir ();
label_7:
    rax = *((rsp + 0x50));
    *((rsp + 0x18)) = 2;
    *((rsp + 0x20)) = 1;
    *((rsp + 0x28)) = rax;
    do {
label_0:
        rdi = rbp;
        rax = readdir ();
        if (rax == 0) {
            goto label_21;
        }
label_1:
        r13 = rax + 0x13;
        rax = strlen (r13);
    } while (rax < rbx);
    rax = *((rsp + 8));
    r15 = rax + 2;
    eax = memcmp (r12 + r14, r13, r15);
    if (eax != 0) {
        goto label_0;
    }
    r13 += r15;
    eax = *(r13);
    edx = rax - 0x31;
    if (dl > 8) {
        goto label_0;
    }
    eax = *((r13 + 1));
    r8b = (al == 0x39) ? 1 : 0;
    edx = eax;
    eax -= 0x30;
    if (eax > 9) {
        goto label_22;
    }
    r15d = 1;
    do {
        al = (dl == 0x39) ? 1 : 0;
        r15++;
        r8d &= eax;
        eax = *((r13 + r15));
        rcx = r15;
        edx = eax;
        eax -= 0x30;
    } while (eax <= 9);
label_6:
    if (dl != 0x7e) {
        goto label_0;
    }
    if (*((r13 + rcx + 1)) != 0) {
        goto label_0;
    }
    if (*((rsp + 0x20)) >= r15) {
        goto label_23;
    }
    rax = *((rsp + 0x10));
    r9 = rax + 2;
label_3:
    edx = (int32_t) r8b;
    esi = (int32_t) r8b;
    rax = rdx + r15;
    *((rsp + 0x18)) = esi;
    *((rsp + 0x20)) = rax;
    rax = rax + r9 + 2;
    if (rax <= *((rsp + 0x28))) {
        goto label_24;
    }
    rsi = rax;
    rsi >>= 1;
    rsi += rax;
    if (rsi overflow 0) {
        goto label_25;
    }
    *((rsp + 0x28)) = rsi;
label_17:
    rax = *((rsp + 0x28));
    *((rsp + 0x48)) = rdx;
    *((rsp + 0x40)) = rcx;
    sil = (rax == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (r12, 0);
    rcx = *((rsp + 0x40));
    rdx = *((rsp + 0x48));
    r8 = rax;
    if (rax == 0) {
        goto label_26;
    }
label_9:
    rax = *((rsp + 0x10));
    *((rsp + 0x48)) = rcx;
    ecx = 0x7e2e;
    *((rsp + 0x40)) = r8;
    rax += r8;
    *(rax) = cx;
    *((rax + 2)) = 0x30;
    rax = memcpy (rax + rdx + 2, r13, r15 + 2);
    rcx = *((rsp + 0x48));
    r8 = *((rsp + 0x40));
    rdi = rax;
    rdi += rcx;
    edx = *((rdi - 1));
    rax = rdi - 1;
    if (dl != 0x39) {
        goto label_27;
    }
    do {
        *(rax) = 0x30;
        edx = *((rax - 1));
        rax--;
    } while (dl == 0x39);
label_27:
    edx++;
    rdi = rbp;
    r12 = r8;
    *(rax) = dl;
    rax = readdir ();
    if (rax != 0) {
        goto label_1;
    }
label_21:
    if (*((rsp + 0x18)) == 2) {
        goto label_14;
    }
label_16:
    if (*((rsp + 0x18)) == 1) {
        goto label_11;
    }
    if (*((rsp + 0x37)) == 0) {
        goto label_28;
    }
    r15d = *((rsp + 0x37));
label_5:
label_4:
    edi = *((rsp + 0x84));
    eax = renameatu (*(edi), *((rsp + 0x60)), *((rsp + 0x84)), r12 + r14, 1);
    if (eax == 0) {
        goto label_28;
    }
    rax = errno_location ();
    edx = *(rax);
    if (edx != 0x11) {
        goto label_29;
    }
    if (r15b == 1) {
        goto label_2;
    }
label_29:
    r15d = edx;
    rdx = rax;
    if (rbp != 0) {
        rdi = rbp;
        *((rsp + 8)) = rax;
        closedir ();
        rdx = *((rsp + 8));
    }
    rdi = r12;
    *((rsp + 8)) = rdx;
    r12d = 0;
    fcn_00003670 ();
    rdx = *((rsp + 8));
    *(rdx) = r15d;
label_10:
    rax = *((rsp + 0x88));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_30;
    }
    rax = r12;
    return rax;
label_23:
    *((rsp + 0x40)) = r8b;
    if (rax != 0) {
        goto label_0;
    }
    rax = *((rsp + 0x10));
    rdx = rcx;
    *((rsp + 0x48)) = rcx;
    r9 = rax + 2;
    *((rsp + 0x78)) = r9;
    eax = memcmp (r12 + r9, r13, rdx);
    rcx = *((rsp + 0x48));
    r9 = *((rsp + 0x78));
    r8d = *((rsp + 0x40));
    if (eax <= 0) {
        goto label_3;
    }
    goto label_0;
label_19:
    rax = *((rsp + 0x10));
    memcpy (r12 + rax, *(obj.simple_backup_suffix), *((rsp + 0x70)));
    if (*((rsp + 0x37)) == 0) {
        goto label_28;
    }
    rsi = *((rsp + 0x38));
    r15d = *((rsp + 0x37));
    rcx = r12;
    r8d = 0;
    goto label_4;
label_14:
    if (*((rsp + 0x68)) == 2) {
        goto label_31;
    }
label_11:
    rdi = r12;
    r15d = 1;
    rax = last_component ();
    rdi = rax;
    *((rsp + 0x18)) = rax;
    rax = base_len (rdi);
    rcx = rax;
    if (rax > 0xe) {
        goto label_32;
    }
label_8:
    if (*((rsp + 0x37)) == 0) {
        goto label_28;
    }
    if (*((rsp + 0x68)) != 1) {
        goto label_5;
    }
    rsi = *((rsp + 0x38));
    rcx = r12;
    r8d = 0;
    goto label_4;
label_22:
    ecx = 1;
    r15d = 1;
    goto label_6;
label_20:
    r15 = r12 + r14;
    esi = 0x2e;
    r13d = *(r15);
    *(r15) = si;
    rax = opendirat (*((rsp + 0x6c)), r12, 0, rsp + 0x84);
    rax = *((rsp + 8));
    rdx = r15 + rax;
    if (rbp == 0) {
        goto label_33;
    }
    *(r15) = r13w;
    *(rdx) = 0x7e317e2e;
    *((rdx + 4)) = 0;
    goto label_7;
label_32:
    edi = *((rsp + 0x84));
    rdx = *((rsp + 0x18));
    *((rsp + 0x20)) = edi;
    if (*((rsp + 0x58)) == 0) {
        *((rsp + 0x28)) = rax;
        rax = errno_location ();
        edi = *((rsp + 0x20));
        rdx = *((rsp + 0x18));
        rcx = *((rsp + 0x28));
        r15 = rax;
        if (edi < 0) {
            goto label_34;
        }
        *(rax) = 0;
        esi = 3;
        *((rsp + 0x20)) = rcx;
        *((rsp + 0x18)) = rdx;
        rax = fpathconf ();
        rcx = *((rsp + 0x20));
        rdx = *((rsp + 0x18));
        *((rsp + 0x58)) = rax;
label_13:
        rsi = *((rsp + 0x58));
        if (rsi < 0) {
            goto label_35;
        }
    }
    if (rcx <= *((rsp + 0x58))) {
        goto label_36;
    }
    do {
        rax = *((rsp + 0x10));
        rsi = *((rsp + 0x58));
        rax += r12;
        rcx = rsi - 1;
        rax -= rdx;
        if (rax >= rsi) {
            rax = rcx;
        }
        r15d = 0;
        *((rdx + rax)) = 0x7e;
        *((rdx + rax + 1)) = 0;
        goto label_8;
label_24:
        r8 = r12;
        goto label_9;
label_26:
        rdi = rbp;
        closedir ();
label_15:
        rdi = r12;
        r12d = 0;
        fcn_00003670 ();
        errno_location ();
        *(rax) = 0xc;
        goto label_10;
label_35:
        eax = 0xe;
        if (rsi == -1) {
            rax = rsi;
        }
        *((rsp + 0x58)) = rax;
    } while (1);
label_31:
    rax = *((rsp + 0x10));
    memcpy (r12 + rax, *(obj.simple_backup_suffix), *((rsp + 0x70)));
    *((rsp + 0x68)) = 1;
    goto label_11;
label_28:
    if (rbp == 0) {
        goto label_10;
    }
    rdi = rbp;
    closedir ();
    goto label_10;
label_18:
    rax = getenv ("SIMPLE_BACKUP_SUFFIX");
    rdi = 0x000164c8;
    rbx = rax;
    if (rax != 0) {
        if (*(rax) == 0) {
            goto label_37;
        }
        rdi = rax;
        rax = last_component ();
        rdi = rax;
        rax = 0x000164c8;
        if (rbx == rax) {
            rdi = rax;
            goto label_37;
        }
    }
label_37:
    *(obj.simple_backup_suffix) = rdi;
    goto label_12;
label_34:
    eax = 0x2e;
    r8d = *(rdx);
    esi = 3;
    rdi = r12;
    *(rdx) = ax;
    *(r15) = 0;
    *((rsp + 0x20)) = r8d;
    rax = pathconf ();
    rdx = *((rsp + 0x18));
    r8d = *((rsp + 0x20));
    rcx = *((rsp + 0x28));
    *((rsp + 0x58)) = rax;
    *(rdx) = r8w;
    goto label_13;
label_33:
    *((rsp + 0x20)) = rdx;
    rax = errno_location ();
    rdx = *((rsp + 0x20));
    *(r15) = r13w;
    al = (*(rax) == 0xc) ? 1 : 0;
    *(rdx) = 0x7e317e2e;
    eax = (int32_t) al;
    *((rdx + 4)) = 0;
    eax += 2;
    *((rsp + 0x18)) = eax;
    if (eax == 2) {
        goto label_14;
    }
    if (*((rsp + 0x18)) == 3) {
        goto label_15;
    }
    goto label_16;
label_25:
    *((rsp + 0x28)) = rax;
    goto label_17;
label_36:
    r15d = 1;
    goto label_8;
label_30:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xbe30 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xe600 */
 
uint64_t dbg_opendirat (int64_t arg3, int64_t arg4, int32_t fd, const char * path) {
    rdx = arg3;
    rcx = arg4;
    rdi = fd;
    rsi = path;
    /* DIR * opendirat(int dir_fd,char const * dir,int extra_flags,int * pnew_fd); */
    edx |= 0x90900;
    eax = 0;
    r12d = 0;
    rbx = rcx;
    eax = openat_safer (rdi, rsi, rdx, rcx, r8);
    if (eax < 0) {
        goto label_0;
    }
    edi = eax;
    rax = fdopendir ();
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    *(rbx) = ebp;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        rax = errno_location ();
        r13d = *(rax);
        rbx = rax;
        close (ebp);
        *(rbx) = r13d;
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0xe580 */
 
int64_t dbg_openat_safer (int64_t arg_60h, func * arg4, char *** fd, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    func * var_38h;
    rcx = arg4;
    rdi = fd;
    rdx = oflag;
    rsi = path;
    /* int openat_safer(int fd,char const * file,int flags,va_args ...); */
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = openat (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x18;
        ecx = *((rsp + 0x38));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x3bc0 */
 
void fdopendir (void) {
    __asm ("bnd jmp qword [reloc.fdopendir]");
}

/* /tmp/tmp7e2uf9hg @ 0x3920 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmp7e2uf9hg @ 0x12760 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xc6d0 */
 
void dbg_backup_file_rename (char *** arg1, char *** arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_37h;
    char *** var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    char *** var_6ch;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_84h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * backup_file_rename(int dir_fd,char const * file,backup_type backup_type); */
    ecx = 1;
    return void (*0xbfc0)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x10270 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0xe4c0 */
 
int64_t dbg_triple_compare (uint32_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    /* _Bool triple_compare( const * x, const * y); */
    rax = *((rsi + 8));
    while (*((rdi + 0x10)) != rax) {
        eax = 0;
        return rax;
        rax = *((rsi + 0x10));
    }
    rsi = *(rsi);
    rdi = *(rdi);
    return void (*0x10f40)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x12ca0 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3a80)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x12dd0 */
 
int32_t dbg_get_permissions (permission_context * ctx, int32_t desc, mode_t mode, char const * name) {
    rcx = ctx;
    rsi = desc;
    rdx = mode;
    rdi = name;
    /* int get_permissions(char const * name,int desc,mode_t mode,permission_context * ctx); */
    *(rcx) = edx;
    eax = 0;
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0xbb00 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmp7e2uf9hg @ 0x12480 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x12010)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x10f40 */
 
void dbg_same_name (int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    /* _Bool same_name(char const * source,char const * dest); */
    rcx = rsi;
    edx = 0xffffff9c;
    rsi = rdi;
    edi = 0xffffff9c;
    return void (*0x10da0)() ();
}

/* /tmp/tmp7e2uf9hg @ 0xca40 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x000164c5);
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0x13400 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmp7e2uf9hg @ 0x11ff0 */
 
void dbg_utimecmp (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_54h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_6ch;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_e8h;
    int64_t var_f0h;
    int64_t var_128h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int utimecmp(char const * dst_name,stat const * dst_stat,stat const * src_stat,int options); */
    r8d = ecx;
    rcx = rdx;
    rdx = rsi;
    rsi = rdi;
    edi = 0xffffff9c;
    return void (*0x117b0)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x12720 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xd7e0 */
 
int64_t dbg_hash_table_ok (Hash_table const * table) {
    rdi = table;
    /* _Bool hash_table_ok(Hash_table const * table); */
    rcx = *(rdi);
    rsi = *((rdi + 8));
    edx = 0;
    r8d = 0;
    if (rcx < rsi) {
        goto label_1;
    }
    goto label_2;
    do {
label_0:
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_2;
        }
label_1:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    r8++;
    rdx++;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_1;
    }
label_2:
    eax = 0;
    if (*((rdi + 0x18)) != r8) {
        return rax;
    }
    al = (*((rdi + 0x20)) == rdx) ? 1 : 0;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xafe0 */
 
int64_t dbg_copy (int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, uint32_t arg6) {
    _Bool * copy_into_self;
    _Bool * rename_succeeded;
    _Bool first_dir_created_per_command_line_arg;
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r9 = arg6;
    /* _Bool copy(char const * src_name,char const * dst_name,int dst_dirfd,char const * dst_relname,int nonexistent_dst,cp_options const * options,_Bool * copy_into_self,_Bool * rename_succeeded); */
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    r11 = *((rsp + 0x20));
    rbx = *((rsp + 0x28));
    if (*(r9) > 3) {
        goto label_0;
    }
    eax = *((r9 + 0xc));
    r10d = edx;
    edx = rax - 1;
    if (edx > 2) {
        goto label_1;
    }
    edx = *((r9 + 0x44));
    if (edx > 2) {
        goto label_2;
    }
    while (*((r9 + 0x3a)) == 0) {
        if (eax != 2) {
            if (edx == 2) {
                goto label_3;
            }
        }
        *((rsp + 7)) = 0;
        *(obj.top_level_src_name) = rdi;
        *(obj.top_level_dst_name) = rsi;
        rax = rsp + 0x17;
        copy_internal (rdi, rsi, r10d, rcx, r8, 0);
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_4;
        }
        return rax;
    }
    assert_fail ("!(co->hard_link && co->symbolic_link)", "src/copy.c", 0xc12, "valid_options");
label_3:
    assert_fail ("! (co->reflink_mode == REFLINK_ALWAYS && co->sparse_mode != SPARSE_AUTO)", "src/copy.c", 0xc13, "valid_options");
label_2:
    assert_fail ("VALID_REFLINK_MODE (co->reflink_mode)", "src/copy.c", 0xc11, "valid_options");
label_4:
    stack_chk_fail ();
label_1:
    assert_fail ("VALID_SPARSE_MODE (co->sparse_mode)", "src/copy.c", 0xc10, "valid_options");
label_0:
    return assert_fail ("VALID_BACKUP_TYPE (co->backup_type)", "src/copy.c", 0xc0f, "valid_options");
}

/* /tmp/tmp7e2uf9hg @ 0x13380 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x10920 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3d2c)() ();
    }
    if (rax == 0) {
        void (*0x3d2c)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xe080 */
 
int64_t hash_insert_if_absent (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rsi == 0) {
        void (*0x3cf4)() ();
    }
    r12 = rsp;
    r13 = rdx;
    ecx = 0;
    rbx = rdi;
    rdx = r12;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_8;
    }
    r8d = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r13) = rax;
    do {
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        eax = r8d;
        return rax;
label_8:
        rax = *((rbx + 0x18));
        if (rax < 0) {
            goto label_10;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_11;
        }
label_0:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_1:
        rax = *((rbx + 0x28));
        xmm0 = *((rax + 8));
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm5, xmm0");
        if (rax > 0) {
            goto label_12;
        }
label_2:
        r12 = *(rsp);
        if (*(r12) == 0) {
            goto label_13;
        }
        rax = *((rbx + 0x48));
        if (rax == 0) {
            goto label_14;
        }
        rdx = *((rax + 8));
        *((rbx + 0x48)) = rdx;
label_4:
        rdx = *((r12 + 8));
        *(rax) = rbp;
        r8d = 1;
        *((rax + 8)) = rdx;
        *((r12 + 8)) = rax;
        *((rbx + 0x20))++;
    } while (1);
label_10:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_0;
    }
label_11:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_1;
label_12:
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm1 = xmm4;
    xmm0 = *((rax + 8));
    __asm ("mulss xmm1, xmm0");
    __asm ("comiss xmm5, xmm1");
    if (rdx <= 0) {
        goto label_2;
    }
    __asm ("mulss xmm4, dword [rax + 0xc]");
    if (*((rax + 0x10)) == 0) {
        goto label_15;
    }
label_5:
    __asm ("comiss xmm4, dword [0x00016604]");
    if (*((rax + 0x10)) < 0) {
        goto label_16;
    }
    do {
label_6:
        r8d = 0xffffffff;
        goto label_3;
label_13:
        *(r12) = rbp;
        r8d = 1;
        *((rbx + 0x20))++;
        *((rbx + 0x18))++;
        goto label_3;
label_14:
        rax = malloc (0x10);
    } while (rax == 0);
    goto label_4;
label_15:
    __asm ("mulss xmm4, xmm0");
    goto label_5;
label_16:
    __asm ("comiss xmm4, dword [0x00016608]");
    if (rax >= 0) {
        goto label_17;
    }
    __asm ("cvttss2si rsi, xmm4");
label_7:
    rdi = rbx;
    al = hash_rehash ();
    if (al == 0) {
        goto label_6;
    }
    ecx = 0;
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_2;
    }
    void (*0x3cf4)() ();
label_17:
    __asm ("subss xmm4, dword [0x00016608]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_7;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xd760 */
 
int64_t hash_get_n_buckets_used (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x18));
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x106a0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x117b0 */
 
uint32_t rotate_right32 (uint32_t value, uint32_t count) {
    const uint32_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_utimecmpat (char *** arg1, int64_t arg2, uint32_t arg3, uint32_t arg4, int64_t arg5) {
    fs_res tmp_dst_res;
    timespec[2] timespec;
    stat dst_status;
    int64_t var_8h;
    int64_t var_10h;
    uint32_t var_18h;
    uint32_t var_20h;
    int64_t var_28h;
    signed int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    char *** var_50h;
    signed int64_t var_54h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_6ch;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_88h;
    int64_t var_90h;
    uint32_t canary;
    uint32_t var_f0h;
    int64_t var_128h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int utimecmpat(int dfd,char const * dst_name,stat const * dst_stat,stat const * src_stat,int options); */
    r14 = *((rdx + 0x58));
    r13 = *((rcx + 0x58));
    r9 = *((rcx + 0x60));
    r10 = *((rdx + 0x60));
    *((rsp + 0x50)) = edi;
    r11d = r9d;
    rax = *(fs:0x28);
    *((rsp + 0x128)) = rax;
    eax = 0;
    bpl = (r14 == r13) ? 1 : 0;
    r8d &= 1;
    if (r8d != 0) {
        al = (r10d == r9d) ? 1 : 0;
        al &= bpl;
        *((rsp + 8)) = al;
        if (al != 0) {
            goto label_12;
        }
        rax = r13 - 1;
        if (rax > r14) {
            goto label_2;
        }
        rax = r14 - 1;
        r12d = r8d;
        if (rax > r13) {
            goto label_0;
        }
        r8 = Scrt1.o;
        r15 = rsi;
        rbx = rdx;
        if (r8 == 0) {
            goto label_13;
        }
label_1:
        rsi = Scrt1.o;
        if (rsi == 0) {
            goto label_14;
        }
label_7:
        rax = *(rbx);
        *((rsp + 0x28)) = r9;
        *((rsp + 0x20)) = r10;
        *(rsi) = rax;
        *((rsp + 0x18)) = r11d;
        rax = hash_insert (r8);
        r11d = *((rsp + 0x18));
        r10 = *((rsp + 0x20));
        *((rsp + 0x10)) = rax;
        r9 = *((rsp + 0x28));
        if (rax == 0) {
            goto label_15;
        }
        if (*(0x0001c6f8) == rax) {
            goto label_16;
        }
label_5:
        rdi = *((rsp + 0x10));
        eax = *((rdi + 8));
        *((rsp + 0x18)) = eax;
        if (*((rdi + 0xc)) == 0) {
            goto label_17;
        }
        ebx = eax;
        eax = 0;
        al = (ebx == 0x77359400) ? 1 : 0;
        eax = ~eax;
        rax = (int64_t) eax;
        r13 &= rax;
        eax = r11d;
        r11d = r9d;
        edx:eax = (int64_t) eax;
        eax = edx:eax / ebx;
        edx = edx:eax % ebx;
        r11d -= edx;
        bpl = (r14 == r13) ? 1 : 0;
    }
label_3:
    r12d = 0;
    ebp = (int32_t) bpl;
    al = (r11d > r10d) ? 1 : 0;
    r12b = (r11d < r10d) ? 1 : 0;
    ebp = -ebp;
    eax = (int32_t) al;
    r12d -= eax;
    eax = 0;
    r12d &= ebp;
    dl = (r13 > r14) ? 1 : 0;
    al = (r13 < r14) ? 1 : 0;
    edx = (int32_t) dl;
    eax -= edx;
    r12d += eax;
    do {
label_0:
        rax = *((rsp + 0x128));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_18;
        }
        eax = r12d;
        return rax;
label_12:
        r12d = 0;
    } while (1);
label_2:
    r12d = 0xffffffff;
    goto label_0;
label_13:
    *((rsp + 0x20)) = r9;
    *((rsp + 0x18)) = r10;
    *((rsp + 0x10)) = r9d;
    rax = hash_initialize (0x10, 0, dbg.dev_info_hash, dbg.dev_info_compare, *(reloc.free));
    r11d = *((rsp + 0x10));
    r10 = *((rsp + 0x18));
    *(0x0001c700) = rax;
    r9 = *((rsp + 0x20));
    r8 = rax;
    if (rax != 0) {
        goto label_1;
    }
label_4:
    rdx = rsp + 0x60;
label_6:
    *((rsp + 0x68)) = 0x77359400;
    *((rsp + 0x6c)) = 0;
    *((rsp + 0x18)) = 0x77359400;
    *((rsp + 0x10)) = rdx;
label_17:
    rax = *((rbx + 0x48));
    rdx = *((rbx + 0x50));
    rdi = (int64_t) r10d;
    rcx = *((rbx + 0x70));
    *((rsp + 0x40)) = rdi;
    rdi *= 0x66666667;
    *((rsp + 0x20)) = rax;
    rax = *((rbx + 0x68));
    *((rsp + 0x28)) = rax;
    rax = (int64_t) edx;
    rdi >>= 0x22;
    rsi = rax * 0x66666667;
    *((rsp + 0x38)) = rax;
    eax = edx;
    eax >>= 0x1f;
    rsi >>= 0x22;
    ebx = esi;
    esi = ecx;
    ebx -= eax;
    esi >>= 0x1f;
    eax = rbx * 5;
    eax += eax;
    edx -= eax;
    rax = (int64_t) ecx;
    rax *= 0x66666667;
    rax >>= 0x22;
    eax -= esi;
    esi = rax * 5;
    esi += esi;
    ecx -= esi;
    esi = r10d;
    edx |= ecx;
    ecx = r10d;
    ecx >>= 0x1f;
    edi -= ecx;
    ecx = rdi * 5;
    ecx += ecx;
    esi -= ecx;
    esi |= edx;
    if (esi != 0) {
        goto label_19;
    }
    *((rsp + 0x48)) = ebx;
    ecx = eax;
    esi = edi;
    if (*((rsp + 0x18)) <= 0xa) {
        goto label_20;
    }
    r8 = (int64_t) eax;
    edx:eax = (int64_t) eax;
    *((rsp + 0x30)) = 0xa;
    r8 *= 0x66666667;
    r8 >>= 0x22;
    r8d -= edx;
    r8d = r8 * 5;
    r8d += r8d;
    eax -= r8d;
    r8d = ebx;
    edx = eax;
    rax = (int64_t) ebx;
    r8d >>= 0x1f;
    rax *= 0x66666667;
    rax >>= 0x22;
    eax -= r8d;
    r8d = rax * 5;
    eax = ebx;
    ebx = edx;
    r8d += r8d;
    eax -= r8d;
    r8d = edi;
    ebx |= eax;
    rax = (int64_t) edi;
    r8d >>= 0x1f;
    rax *= 0x66666667;
    rax >>= 0x22;
    eax -= r8d;
    eax = rax * 5;
    eax += eax;
    edi -= eax;
    eax = 8;
    edi |= ebx;
    if (edi != 0) {
        goto label_21;
    }
    *((rsp + 0x54)) = r11d;
    edx = *((rsp + 0x48));
    ebx = 0xa;
    *((rsp + 0x58)) = r14;
    *((rsp + 0x48)) = r9;
    r9d = *((rsp + 0x18));
    *((rsp + 0x30)) = r13;
    r13d = eax;
    do {
        rax = (int64_t) edx;
        edx >>= 0x1f;
        edi = rbx * 5;
        r8d = ebx;
        rax *= 0x66666667;
        edi += edi;
        ebx = edi;
        rax >>= 0x22;
        eax -= edx;
        edx = eax;
        rax = (int64_t) ecx;
        ecx >>= 0x1f;
        rax *= 0x66666667;
        rax >>= 0x22;
        eax -= ecx;
        ecx = eax;
        rax = (int64_t) esi;
        esi >>= 0x1f;
        rax *= 0x66666667;
        rax >>= 0x22;
        eax -= esi;
        esi = eax;
        if (edi >= r9d) {
            goto label_22;
        }
        rax = (int64_t) edx;
        r11d = edx;
        r14d = ecx;
        rax *= 0x66666667;
        r11d >>= 0x1f;
        r14d >>= 0x1f;
        rax >>= 0x22;
        eax -= r11d;
        r11d = edx;
        eax = rax * 5;
        eax += eax;
        r11d -= eax;
        rax = (int64_t) ecx;
        rax *= 0x66666667;
        rax >>= 0x22;
        eax -= r14d;
        r14d = ecx;
        eax = rax * 5;
        eax += eax;
        r14d -= eax;
        rax = (int64_t) esi;
        rax *= 0x66666667;
        r11d |= r14d;
        r14d = esi;
        r14d >>= 0x1f;
        rax >>= 0x22;
        eax -= r14d;
        r14d = esi;
        eax = rax * 5;
        eax += eax;
        r14d -= eax;
        r14d |= r11d;
        if (r14d != 0) {
            goto label_23;
        }
        r13d--;
    } while (r13d != 0);
    r14 = *((rsp + 0x58));
    rax = *((rsp + 0x20));
    r13 = *((rsp + 0x30));
    r11d = *((rsp + 0x54));
    rax |= r14;
    rax |= *((rsp + 0x28));
    r9 = *((rsp + 0x48));
    rax = *((rsp + 0x10));
    if ((al & 1) == 0) {
        goto label_24;
    }
    *((rsp + 0x30)) = 0x3b9aca00;
    *((rax + 8)) = 0x3b9aca00;
    rax = r13;
label_8:
    if (r14 > r13) {
        goto label_0;
    }
    if (r10d < r9d) {
        goto label_25;
    }
    if (bpl != 0) {
        goto label_0;
    }
label_25:
    if (r14 < rax) {
        goto label_2;
    }
    if (r14 != rax) {
        goto label_26;
    }
    eax = r11d;
    edx:eax = (int64_t) eax;
    eax = *(edx:eax) / rsp + 0x30;
    edx = *(edx:eax) % rsp + 0x30;
    eax = r9d;
    eax -= edx;
    if (eax > r10d) {
        goto label_2;
    }
label_26:
    rax = *((rsp + 0x20));
    edi = *((rsp + 0x50));
    rbx = rsp + 0x70;
    rsi = r15;
    ecx = 0x100;
    rdx = rbx;
    *((rsp + 0x28)) = r9;
    *((rsp + 0x70)) = rax;
    rax = *((rsp + 0x38));
    *((rsp + 0x18)) = r11d;
    *((rsp + 0x78)) = rax;
    eax = *((rsp + 8));
    *((rsp + 8)) = r10;
    rax |= r14;
    *((rsp + 0x80)) = rax;
    eax = *((rsp + 0x30));
    rax *= 0x38e38e39;
    rax >>= 0x21;
    eax += r10d;
    rax = (int64_t) eax;
    *((rsp + 0x88)) = rax;
    eax = utimensat ();
    r10 = *((rsp + 8));
    r11d = *((rsp + 0x18));
    r9 = *((rsp + 0x28));
    if (eax != 0) {
        goto label_27;
    }
    edi = *((rsp + 0x50));
    ecx = 0x100;
    rsi = r15;
    *((rsp + 0x20)) = r9;
    rdx = rsp + 0x90;
    *((rsp + 0x18)) = r10;
    *((rsp + 8)) = r11d;
    eax = fstatat ();
    rdx = *((rsp + 0x40));
    r11d = *((rsp + 8));
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x20));
    rax = *((rsp + 0xe8));
    rsi = *((rsp + 0xf0));
    rcx = rax;
    rdx ^= rsi;
    rcx ^= r14;
    rdx |= rcx;
    rcx = (int64_t) ebp;
    rcx |= rdx;
    if (rcx != 0) {
        goto label_28;
    }
label_9:
    edx = eax;
    edx &= 1;
    edx *= 0x3b9aca00;
    edx += esi;
    eax = edx * 0xcccccccd;
    eax += 0x19999998;
    eax = rotate_right32 (eax, 1);
    if (eax > 0x19999998) {
        goto label_10;
    }
    esi = *((rsp + 0x30));
    if (esi == 0xa) {
        goto label_29;
    }
    ecx = 9;
    r12d = 0xa;
    while (ecx != 0) {
        r12d = r12 * 5;
        r12d += r12d;
        if (r12d == esi) {
            goto label_10;
        }
        rax = (int64_t) edx;
        edi = edx;
        rax *= 0x66666667;
        edi >>= 0x1f;
        rax >>= 0x22;
        eax -= edi;
        edx = eax;
        eax *= 0xcccccccd;
        eax += 0x19999998;
        eax = rotate_right32 (eax, 1);
        if (eax > 0x19999998) {
            goto label_10;
        }
        ecx--;
    }
    r12d = 0x77359400;
label_10:
    eax = 0;
    al = (r12d == 0x77359400) ? 1 : 0;
    eax = ~eax;
    rax = (int64_t) eax;
    r13 &= rax;
    eax = r11d;
    r11d = r9d;
    edx:eax = (int64_t) eax;
    eax = edx:eax / r12d;
    edx = edx:eax % r12d;
    r11d -= edx;
    bpl = (r14 == r13) ? 1 : 0;
label_19:
    rax = *((rsp + 0x10));
    *((rax + 8)) = r12d;
    *((rax + 0xc)) = 1;
    goto label_3;
label_15:
    rdi = Scrt1.o;
    if (rdi == 0) {
        goto label_4;
    }
label_11:
    rdx = rsp + 0x60;
    rax = *(rbx);
    *((rsp + 0x30)) = r9;
    rsi = rdx;
    *((rsp + 0x28)) = r10;
    *((rsp + 0x20)) = r11d;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x60)) = rax;
    rax = hash_lookup ();
    rdx = *((rsp + 0x18));
    r11d = *((rsp + 0x20));
    *((rsp + 0x10)) = rax;
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    if (rax != 0) {
        goto label_5;
    }
    goto label_6;
label_14:
    *((rsp + 0x28)) = r9;
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r11d;
    *((rsp + 0x10)) = r8;
    rax = malloc (0x10);
    r11d = *((rsp + 0x18));
    r10 = *((rsp + 0x20));
    *(0x0001c6f8) = rax;
    r9 = *((rsp + 0x28));
    rsi = rax;
    if (rax == 0) {
        goto label_30;
    }
    *((rax + 8)) = 0x77359400;
    r8 = *((rsp + 0x10));
    *((rax + 0xc)) = 0;
    goto label_7;
label_16:
    *(0x0001c6f8) = 0;
    goto label_5;
label_24:
    *((rax + 8)) = 0x77359400;
    rax = r13;
    *((rsp + 8)) = 1;
    rax &= 0xfffffffffffffffe;
    *((rsp + 0x30)) = 0x77359400;
    goto label_8;
label_22:
    r13 = *((rsp + 0x30));
    r11d = *((rsp + 0x54));
    *((rsp + 0x30)) = edi;
    r14 = *((rsp + 0x58));
    r9 = *((rsp + 0x48));
    do {
        rax = *((rsp + 0x10));
        rsp + 8 = (r8d == 0xbebc200) ? 1 : 0;
        *((rax + 8)) = ebx;
        al = (r8d == 0xbebc200) ? 1 : 0;
        eax = (int32_t) al;
        eax = ~eax;
        rax = (int64_t) eax;
        rax &= r13;
        goto label_8;
label_23:
        r13 = *((rsp + 0x30));
        r11d = *((rsp + 0x54));
        *((rsp + 0x30)) = edi;
        r14 = *((rsp + 0x58));
        r9 = *((rsp + 0x48));
    } while (1);
    do {
label_27:
        r12d = 0xfffffffe;
        goto label_0;
label_20:
        rax = *((rsp + 0x10));
        *((rsp + 0x30)) = 0xa;
        *((rax + 8)) = 0xa;
        rax = r13;
        goto label_8;
label_21:
        rax = *((rsp + 0x10));
        *((rax + 8)) = 0xa;
        rax = r13;
        goto label_8;
label_28:
        rax = *((rsp + 0x40));
        edi = *((rsp + 0x50));
        rdx = rbx;
        rsi = r15;
        ecx = 0x100;
        *((rsp + 0x80)) = r14;
        *((rsp + 0x88)) = rax;
        utimensat ();
        r11d = *((rsp + 8));
        r10 = *((rsp + 0x18));
        r9 = *((rsp + 0x20));
    } while (ebp != 0);
    rax = *((rsp + 0xe8));
    rsi = *((rsp + 0xf0));
    goto label_9;
label_18:
    stack_chk_fail ();
label_29:
    r12d = 0xa;
    goto label_10;
label_30:
    rdi = Scrt1.o;
    goto label_11;
}

/* /tmp/tmp7e2uf9hg @ 0x12b70 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xff40 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x11720 */
 
void dbg_try_tempname (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_1bh;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int try_tempname(char * tmpl,int suffixlen,void * args,int (*)() tryfunc); */
    r8d = 6;
    return void (*0x11410)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x10a60 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0xfee0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x12be0 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3a80)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x10180 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rdi = *(rbx);
        rbx += 0x10;
        fcn_00003670 ();
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        fcn_00003670 ();
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        rdi = r12;
        fcn_00003670 ();
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x10ad0 */
 
int64_t dbg_renameatu (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    stat src_st;
    stat dst_st;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_28h;
    int64_t var_a0h;
    int64_t var_b8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int renameatu(int fd1,char const * src,int fd2,char const * dst,unsigned int flags); */
    r15d = edi;
    r14d = edx;
    r13 = rcx;
    ebx = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x138)) = rax;
    eax = 0;
    eax = renameat2 ();
    r12d = eax;
    if (eax >= 0) {
        goto label_0;
    }
    rax = errno_location ();
    r8 = rax;
    eax = *(rax);
    edx = rax - 0x16;
    edx &= 0xffffffef;
    dl = (edx != 0) ? 1 : 0;
    al = (eax != 0x5f) ? 1 : 0;
    dl &= al;
    r9d = edx;
    if (dl != 0) {
        goto label_0;
    }
    if (ebx == 0) {
        goto label_3;
    }
    if (ebx != 1) {
        goto label_4;
    }
    ecx = 0x100;
    rsi = r13;
    edi = r14d;
    *(rsp) = r8;
    rdx = rsp + 0xa0;
    eax = fstatat ();
    r8 = *(rsp);
    if (eax == 0) {
        goto label_5;
    }
    eax = *(r8);
    if (eax == 0x4b) {
        goto label_5;
    }
    if (eax == 2) {
        goto label_6;
    }
label_1:
    r12d = 0xffffffff;
    do {
label_0:
        rax = *((rsp + 0x138));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_7;
        }
        eax = r12d;
        return rax;
label_5:
        *(r8) = 0x11;
        r12d = 0xffffffff;
    } while (1);
label_6:
    r9d = 1;
label_3:
    *((rsp + 0xf)) = r9b;
    *(rsp) = r8;
    rax = strlen (rbp);
    rbx = rax;
    rax = strlen (r13);
    if (rbx != 0) {
        r8 = *(rsp);
        r9d = *((rsp + 0xf));
        if (rax == 0) {
            goto label_2;
        }
        if (*((rbp + rbx - 1)) == 0x2f) {
            goto label_8;
        }
        if (*((r13 + rax - 1)) == 0x2f) {
            goto label_8;
        }
    }
label_2:
    rcx = r13;
    edx = r14d;
    rsi = rbp;
    edi = r15d;
    eax = renameat ();
    r12d = eax;
    goto label_0;
label_8:
    rdx = rsp + 0x10;
    ecx = 0x100;
    rsi = rbp;
    edi = r15d;
    *((rsp + 0xf)) = r9b;
    *(rsp) = r8;
    eax = fstatat ();
    if (eax != 0) {
        goto label_1;
    }
    r9d = *((rsp + 0xf));
    r8 = *(rsp);
    if (r9b == 0) {
        goto label_9;
    }
    eax = *((rsp + 0x28));
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_2;
    }
    *(r8) = 2;
    r12d = 0xffffffff;
    goto label_0;
label_4:
    *(r8) = 0x5f;
    r12d = 0xffffffff;
    goto label_0;
label_9:
    ecx = 0x100;
    rsi = r13;
    edi = r14d;
    *(rsp) = r8;
    rdx = rsp + 0xa0;
    eax = fstatat ();
    r8 = *(rsp);
    if (eax == 0) {
        goto label_10;
    }
    if (*(r8) != 2) {
        goto label_1;
    }
    eax = *((rsp + 0x28));
    eax &= case.0xf4aa.9;
    if (eax != 0x4000) {
        goto label_1;
    }
    goto label_2;
label_10:
    eax = *((rsp + 0xb8));
    eax &= case.0xf4aa.9;
    if (eax != 0x4000) {
        *(r8) = 0x14;
        goto label_1;
    }
    eax = *((rsp + 0x28));
    eax &= case.0xf4aa.9;
    if (eax == 0x4000) {
        goto label_2;
    }
    *(r8) = 0x15;
    r12d = 0xffffffff;
    goto label_0;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xd9b0 */
 
uint64_t hash_lookup (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 0x10));
    rdi = r12;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t) (rbx, rbp);
    if (rax >= *((rbp + 0x10))) {
        void (*0x3cdf)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    rsi = *(rbx);
    if (rsi != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rsi = *(rbx);
label_0:
        if (rsi == r12) {
            goto label_2;
        }
        rdi = r12;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_3;
        }
        rbx = *((rbx + 8));
    } while (rbx != 0);
label_1:
    eax = 0;
    return rax;
label_3:
    r12 = *(rbx);
label_2:
    rax = *(rbx);
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xe4f0 */
 
uint64_t dbg_triple_hash (int64_t arg_8h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* size_t triple_hash( const * x,size_t table_size); */
    rbx = rsi;
    rax = hash_pjw (*(rdi));
    edx = 0;
    rax ^= *((rbp + 8));
    rax = rdx:rax / rbx;
    rdx = rdx:rax % rbx;
    rax = rdx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x13250 */
 
uint64_t rotate_left64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
int64_t dbg_hash_pjw (int64_t arg1, size_t tablesize) {
    rdi = arg1;
    rsi = tablesize;
    /* size_t hash_pjw( const * x,size_t tablesize); */
    rdx = *(rdi);
    if (dl == 0) {
        goto label_0;
    }
    eax = 0;
    do {
        rax = rotate_left64 (rax, 9);
        rdi++;
        rax += rdx;
        rdx = *(rdi);
    } while (dl != 0);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    r8 = rdx;
    rax = rdx;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x6d60 */
 
uint64_t dbg_src_info_init (int64_t arg1) {
    rdi = arg1;
    /* void src_info_init(cp_options * x); */
    rbx = rdi;
    rax = hash_initialize (0x3d, 0, dbg.triple_hash_no_name, dbg.triple_compare, dbg.triple_free);
    *((rbx + 0x50)) = rax;
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x38e0 */
 
void geteuid (void) {
    __asm ("bnd jmp qword [reloc.geteuid]");
}

/* /tmp/tmp7e2uf9hg @ 0xdd50 */
 
int64_t dbg_hash_clear (uint32_t arg_8h, int64_t arg_18h, int64_t arg_20h, int64_t arg_40h, int64_t arg_48h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_clear(Hash_table * table); */
    r12 = *(rdi);
    if (r12 < *((rdi + 8))) {
        goto label_0;
    }
    goto label_1;
    do {
        r12 += 0x10;
        if (*((rbp + 8)) <= r12) {
            goto label_1;
        }
label_0:
    } while (*(r12) == 0);
    rbx = *((r12 + 8));
    rdx = *((rbp + 0x40));
    if (rbx != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        rbx = rax;
label_2:
        if (rdx != 0) {
            rdi = *(rbx);
            void (*rdx)() ();
            rdx = *((rbp + 0x40));
        }
        rax = *((rbx + 8));
        rcx = *((rbp + 0x48));
        *(rbx) = 0;
        *((rbx + 8)) = rcx;
        *((rbp + 0x48)) = rbx;
    } while (rax != 0);
label_3:
    if (rdx != 0) {
        rdi = *(r12);
        void (*rdx)() ();
    }
    *(r12) = 0;
    r12 += 0x10;
    *((r12 - 8)) = 0;
    if (*((rbp + 8)) > r12) {
        goto label_0;
    }
label_1:
    *((rbp + 0x18)) = 0;
    *((rbp + 0x20)) = 0;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xdc20 */
 
int64_t dbg_hash_reset_tuning (Hash_tuning * tuning) {
    rdi = tuning;
    /* void hash_reset_tuning(Hash_tuning * tuning); */
    rax = 0x3f80000000000000;
    *((rdi + 0x10)) = 0;
    *(rdi) = rax;
    rax = 0x3fb4fdf43f4ccccd;
    *((rdi + 8)) = rax;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xe520 */
 
int32_t dbg_triple_compare_ino_str (char ** s1, char ** s2) {
    rdi = s1;
    rsi = s2;
    /* _Bool triple_compare_ino_str( const * x, const * y); */
    rdx = *((rsi + 8));
    eax = 0;
    while (*((rdi + 0x10)) != rcx) {
        return eax;
        rcx = *((rsi + 0x10));
    }
    eax = strcmp (*(rdi), *(rsi));
    al = (eax == 0) ? 1 : 0;
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0xb960 */
 
int64_t dbg_areadlinkat_with_size (char *** arg1, char *** arg2, uint32_t arg3) {
    char[128] stackbuf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * areadlinkat_with_size(int fd,char const * file,size_t size); */
    r13d = 0x80;
    ebx = edi;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    r12b = (rdx != 0) ? 1 : 0;
    if (rdx != 0) {
        rcx = rdx + 1;
        eax = 0x401;
        if (rdx < 0x401) {
            rax = rcx;
        }
        r13 = rax;
    }
    rax = rsp + 0x10;
    *((rsp + 8)) = rax;
label_1:
    if (r13 != 0x80) {
        goto label_3;
    }
    do {
        r14 = *((rsp + 8));
        r15d = 0;
        if (r12b != 0) {
            goto label_3;
        }
label_0:
        rcx = r13;
        rdx = r14;
        rsi = rbp;
        edi = ebx;
        rax = readlinkat ();
        if (rax < 0) {
            goto label_4;
        }
        if (r13 > rax) {
            goto label_5;
        }
        rdi = r15;
        fcn_00003670 ();
        rax = 0x3fffffffffffffff;
        if (r13 > rax) {
            goto label_6;
        }
        r13 += r13;
    } while (r13 == 0x80);
label_3:
    rax = malloc (r13);
    r14 = rax;
    if (rax == 0) {
        goto label_7;
    }
    r15 = rax;
    goto label_0;
label_6:
    rax = 0x7fffffffffffffff;
    if (r13 == rax) {
        goto label_8;
    }
    r13 = 0x7fffffffffffffff;
    goto label_1;
label_4:
    rdi = r15;
    r15d = 0;
    fcn_00003670 ();
    do {
label_2:
        rax = *((rsp + 0x98));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        rax = r15;
        return rax;
label_5:
        *((r14 + rax)) = 0;
        r12 = rax + 1;
        if (r15 == 0) {
            goto label_10;
        }
    } while (r13 <= r12);
    rax = realloc (r15, r12);
    if (rax != 0) {
        r15 = rax;
    }
    goto label_2;
label_8:
    errno_location ();
    r15d = 0;
    *(rax) = 0xc;
    goto label_2;
label_10:
    rax = malloc (r12);
    rdi = rax;
    if (rax != 0) {
        rax = memcpy (rdi, r14, r12);
        r15 = rax;
        goto label_2;
    }
label_7:
    r15d = 0;
    goto label_2;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xdb10 */
 
int64_t dbg_hash_get_entries (void ** buffer, size_t buffer_size, Hash_table const * table) {
    rsi = buffer;
    rdx = buffer_size;
    rdi = table;
    /* size_t hash_get_entries(Hash_table const * table,void ** buffer,size_t buffer_size); */
    r9 = *(rdi);
    eax = 0;
    if (r9 >= *((rdi + 8))) {
        goto label_2;
    }
    do {
        if (*(r9) != 0) {
            goto label_3;
        }
label_0:
        r9 += 0x10;
    } while (*((rdi + 8)) > r9);
    return eax;
label_3:
    rcx = r9;
    goto label_4;
label_1:
    r8 = *(rcx);
    rax++;
    *((rsi + rax*8 - 8)) = r8;
    rcx = *((rcx + 8));
    if (rcx == 0) {
        goto label_0;
    }
label_4:
    if (rdx > rax) {
        goto label_1;
    }
label_2:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xca30 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmp7e2uf9hg @ 0x127f0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x129b0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xbc10 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x39e0 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmp7e2uf9hg @ 0xdef0 */
 
int64_t hash_rehash (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t canary;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdi = rsi;
    r12 = *((rbp + 0x28));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    esi = *((r12 + 0x10));
    xmm0 = *((r12 + 8));
    rax = compute_bucket_size_isra_0 ();
    if (rax == 0) {
        goto label_1;
    }
    rbx = rax;
    if (*((rbp + 0x10)) == rax) {
        goto label_2;
    }
    rax = calloc (rax, 0x10);
    *(rsp) = rax;
    if (rax == 0) {
        goto label_1;
    }
    *((rsp + 0x10)) = rbx;
    rbx <<= 4;
    r13 = rsp;
    edx = 0;
    rax += rbx;
    rsi = rbp;
    rdi = r13;
    *((rsp + 0x28)) = r12;
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x30));
    *((rsp + 0x18)) = 0;
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 0x38));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x38)) = rax;
    rax = *((rbp + 0x40));
    *((rsp + 0x40)) = rax;
    rax = *((rbp + 0x48));
    *((rsp + 0x48)) = rax;
    eax = transfer_entries ();
    r12d = eax;
    if (al != 0) {
        goto label_3;
    }
    rax = *((rsp + 0x48));
    edx = 1;
    rsi = r13;
    rdi = rbp;
    *((rbp + 0x48)) = rax;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x3cef)() ();
    }
    edx = 0;
    rsi = r13;
    rdi = rbp;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x3cef)() ();
    }
    rdi = *(rsp);
    fcn_00003670 ();
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        eax = r12d;
        return rax;
label_2:
        r12d = 1;
    } while (1);
label_1:
    r12d = 0;
    goto label_0;
label_3:
    rdi = *(rbp);
    fcn_00003670 ();
    rax = *(rsp);
    *(rbp) = rax;
    rax = *((rsp + 8));
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x10));
    *((rbp + 0x10)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    rax = *((rsp + 0x48));
    *((rbp + 0x48)) = rax;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x126c0 */
 
uint32_t dbg_can_write_any_file (void) {
    /* _Bool can_write_any_file(); */
    if (*(0x0001c709) != 0) {
        eax = *(0x0001c708);
        return eax;
    }
    eax = geteuid ();
    *(0x0001c709) = 1;
    al = (eax == 0) ? 1 : 0;
    *(0x0001c708) = al;
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0x12f50 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x37c0)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp7e2uf9hg @ 0x128d0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x10a70 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0xd1a0 */
 
int64_t dbg_freadahead (FILE * fp) {
    rdi = fp;
    /* size_t freadahead(FILE * fp); */
    rcx = *((rdi + 0x20));
    eax = 0;
    if (*((rdi + 0x28)) <= rcx) {
        rax = *((rdi + 0x10));
        rax -= *((rdi + 8));
        if ((*(rdi) & 0x100) == 0) {
            goto label_0;
        }
        rdx = *((rdi + 0x58));
        rdx -= *((rdi + 0x48));
        rax += rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x109c0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3d31)() ();
    }
    if (rax == 0) {
        void (*0x3d31)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xce90 */
 
int64_t dbg_seen_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg1, char *** arg2, uint32_t arg3) {
    F_triple new_ent;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool seen_file(Hash_table const * ht,char const * file,stat const * stats); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (rdi != 0) {
        rax = *((rdx + 8));
        *(rsp) = rsi;
        rsi = rsp;
        *((rsp + 8)) = rax;
        rax = *(rdx);
        *((rsp + 0x10)) = rax;
        rax = hash_lookup ();
        al = (rax != 0) ? 1 : 0;
    }
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xd780 */
 
int64_t hash_get_max_bucket_length (int64_t arg1) {
    rdi = arg1;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8d = 0;
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_1;
    do {
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_1;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_2;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_2:
    if (r8 < rdx) {
        r8 = rdx;
    }
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_0;
    }
label_1:
    rax = r8;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xb6a0 */
 
uint64_t dbg_copy_acl (int64_t arg1, int64_t arg2, char *** arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int copy_acl(char const * src_name,int source_desc,char const * dst_name,int dest_desc,mode_t mode); */
    r13 = rdi;
    eax = qcopy_acl (rdi, rsi, rdx, rcx, r8);
    r12d = eax;
    if (eax != 0xfffffffe) {
        if (eax == 0xffffffff) {
            rax = quote (rbp, rsi, rdx, rcx, r8);
            edx = 5;
            r14 = rax;
            rax = dcgettext (0, "preserving permissions for %s");
            r13 = rax;
            rax = errno_location ();
            rcx = r14;
            eax = 0;
            error (0, *(rax), r13);
        }
        eax = r12d;
        return rax;
    }
    rax = quote (r13, rsi, rdx, rcx, r8);
    r13 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), 0x000164c5);
    eax = r12d;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xbd00 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmp7e2uf9hg @ 0x12c20 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3a80)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xd250 */
 
uint64_t dbg_full_write (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t full_write(int fd, const * buf,size_t count); */
    if (rdx == 0) {
        goto label_0;
    }
    r12d = edi;
    rbx = rdx;
    r13d = 0;
    while (rax != -1) {
        if (rax == 0) {
            goto label_1;
        }
        r13 += rax;
        rbp += rax;
        rbx -= rax;
        if (rbx == 0) {
            goto label_2;
        }
        rax = safe_write (r12d, rbp, rbx);
    }
    do {
label_2:
        rax = r13;
        return rax;
label_1:
        errno_location ();
        *(rax) = 0x1c;
        rax = r13;
        return rax;
label_0:
        r13d = 0;
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0xbf60 */
 
uint64_t set_simple_backup_suffix (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    if (rdi == 0) {
        goto label_2;
    }
label_1:
    while (rbx != rax) {
label_0:
        rbx = 0x000164c8;
        *(obj.simple_backup_suffix) = rbx;
        return;
        rdi = rbx;
        rax = last_component ();
    }
    *(obj.simple_backup_suffix) = rbx;
    return rax;
label_2:
    rax = getenv ("SIMPLE_BACKUP_SUFFIX");
    rbx = rax;
    if (rax == 0) {
        goto label_0;
    }
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0x12de0 */
 
void chmod_or_fchmod (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = rdi;
    edi = esi;
    esi = edx;
    if (edi != 0xffffffff) {
        void (*0x3b50)() ();
    }
    rdi = r8;
    return chmod ();
}

/* /tmp/tmp7e2uf9hg @ 0xcb10 */
 
uint64_t dir_len (uint32_t arg1) {
    rdi = arg1;
    ebp = 0;
    rbx = rdi;
    bpl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbx;
    while (rax > rbp) {
        rdx = rax - 1;
        if (*((rbx + rax - 1)) != 0x2f) {
            goto label_0;
        }
        rax = rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x13360 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0x12fe0)() ();
}

/* /tmp/tmp7e2uf9hg @ 0xc910 */
 
void dbg_close_stdin_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdin_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmp7e2uf9hg @ 0xe300 */
 
int64_t dbg_hash_remove (int64_t arg_8h, int64_t arg1) {
    hash_entry * bucket;
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_remove(Hash_table * table, const * entry); */
    ecx = 1;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    rax = hash_find_entry ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = *(rsp);
    *((rbx + 0x20))--;
    while (rax <= 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
        rax = *((rbx + 0x18));
        rax--;
        *((rbx + 0x18)) = rax;
        if (rax < 0) {
            goto label_5;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_6;
        }
label_1:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_2:
        rax = *((rbx + 0x28));
        xmm0 = *(rax);
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm0, xmm5");
    }
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm0 = *(rax);
    __asm ("mulss xmm0, xmm4");
    __asm ("comiss xmm0, xmm5");
    if (rax <= 0) {
        goto label_0;
    }
    __asm ("mulss xmm4, dword [rax + 4]");
    if (*((rax + 0x10)) == 0) {
        __asm ("mulss xmm4, dword [rax + 8]");
    }
    __asm ("comiss xmm4, dword [0x00016608]");
    if (*((rax + 0x10)) >= 0) {
        goto label_7;
    }
    __asm ("cvttss2si rsi, xmm4");
label_3:
    rdi = rbx;
    al = hash_rehash ();
    if (al != 0) {
        goto label_0;
    }
    rbp = *((rbx + 0x48));
    if (rbp == 0) {
        goto label_8;
    }
    do {
        rdi = rbp;
        rbp = *((rbp + 8));
        rax = fcn_00003670 ();
    } while (rbp != 0);
label_8:
    *((rbx + 0x48)) = 0;
    goto label_0;
label_5:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_1;
    }
label_6:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_2;
label_7:
    __asm ("subss xmm4, dword [0x00016608]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_3;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x10170 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x10080)() ();
}

/* /tmp/tmp7e2uf9hg @ 0x12d20 */
 
int64_t dbg_yesno (void) {
    char * response;
    size_t response_size;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    /* _Bool yesno(); */
    edx = 0xa;
    r12d = 0;
    rcx = stdin;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rsi = rsp + 0x10;
    rdi = rsp + 8;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    rax = getdelim ();
    if (rax <= 0) {
        goto label_0;
    }
    rdi = *((rsp + 8));
    rax = rdi + rax - 1;
    while (1) {
        eax = rpmatch ();
        r12b = (eax > 0) ? 1 : 0;
label_0:
        rdi = *((rsp + 8));
        fcn_00003670 ();
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        eax = r12d;
        return rax;
        *(rax) = 0;
        rdi = *((rsp + 8));
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xd750 */
 
int64_t hash_get_n_buckets (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x10));
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x10250 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0xda80 */
 
uint64_t hash_get_next (int64_t arg_8h, uint32_t arg_10h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rsi = *((rdi + 0x10));
    rdi = rbx;
    rax = uint64_t (*rbp + 0x30)(uint64_t) (rbx);
    if (rax >= *((rbp + 0x10))) {
        void (*0x3cea)() ();
    }
    rax <<= 4;
    rax += *(rbp);
    rdx = rax;
    while (rcx != rbx) {
        if (rdx == 0) {
            goto label_0;
        }
        rcx = *(rdx);
        rdx = *((rdx + 8));
    }
    if (rdx != 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 8));
    while (rdx > rax) {
        r8 = *(rax);
        if (r8 != 0) {
            goto label_2;
        }
        rax += 0x10;
    }
    r8d = 0;
label_2:
    rax = r8;
    return rax;
label_1:
    r8 = *(rdx);
    rax = r8;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xb7d0 */
 
int64_t dbg_areadlink_with_size (int64_t arg1, uint32_t arg2) {
    char[128] stackbuf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    /* char * areadlink_with_size(char const * file,size_t size); */
    ebx = 0x80;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    r14b = (rsi != 0) ? 1 : 0;
    if (rsi != 0) {
        rbx = rsi + 1;
        eax = 0x401;
        if (rsi < 0x401) {
            rbx = rax;
            goto label_3;
        }
    }
label_3:
    r12 = 0x3fffffffffffffff;
    rax = rsp + 0x10;
    *((rsp + 8)) = rax;
label_1:
    if (rbx != 0x80) {
        goto label_4;
    }
    do {
        r13 = *((rsp + 8));
        r15d = 0;
        if (r14b != 0) {
            goto label_4;
        }
label_0:
        rdx = rbx;
        rsi = r13;
        rdi = rbp;
        rax = readlink ();
        if (rax < 0) {
            goto label_5;
        }
        if (rbx > rax) {
            goto label_6;
        }
        rdi = r15;
        fcn_00003670 ();
        if (rbx > r12) {
            goto label_7;
        }
        rbx += rbx;
    } while (rbx == 0x80);
label_4:
    rax = malloc (rbx);
    r13 = rax;
    if (rax == 0) {
        goto label_8;
    }
    r15 = rax;
    goto label_0;
label_7:
    rax = 0x7fffffffffffffff;
    if (rbx == rax) {
        goto label_8;
    }
    rbx = 0x7fffffffffffffff;
    goto label_1;
label_5:
    rdi = r15;
    r15d = 0;
    fcn_00003670 ();
    do {
label_2:
        rax = *((rsp + 0x98));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        rax = r15;
        return rax;
label_6:
        *((r13 + rax)) = 0;
        r12 = rax + 1;
        if (r15 == 0) {
            goto label_10;
        }
    } while (rbx <= r12);
    rax = realloc (r15, r12);
    if (rax != 0) {
        r15 = rax;
    }
    goto label_2;
label_8:
    errno_location ();
    r15d = 0;
    *(rax) = 0xc;
    goto label_2;
label_10:
    rax = malloc (r12);
    r15 = rax;
    if (rax == 0) {
        goto label_2;
    }
    memcpy (rax, r13, r12);
    goto label_2;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x3d40 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    cp_options x;
    int64_t var_8h;
    int64_t var_10h;
    uint32_t var_18h;
    uint32_t var_25h;
    int64_t var_26h;
    int64_t var_27h;
    int64_t errname;
    int64_t var_30h;
    uint32_t var_34h;
    uint32_t var_38h;
    uint32_t var_3ch;
    int64_t var_40h;
    int64_t var_44h;
    int64_t var_45h;
    int64_t var_46h;
    uint32_t var_47h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_4dh;
    int64_t var_4eh;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_61h;
    int64_t var_62h;
    uint32_t var_63h;
    int64_t var_64h;
    int64_t var_65h;
    uint32_t var_66h;
    int64_t var_67h;
    uint32_t var_68h;
    uint32_t var_6ah;
    int64_t var_6bh;
    int64_t var_6ch;
    int64_t var_6eh;
    char * var_74h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_88h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r14 = obj_long_opts;
    r13 = 0x000140ce;
    r12d = edi;
    rbx = 0x0001433c;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    r15 = rsp + 0x30;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x00016437);
    bindtextdomain (r13, "/usr/local/share/locale");
    r13 = "abdfHilLnprst:uvxPRS:TZ";
    textdomain (r13, rsi);
    rdi = dbg_close_stdin;
    atexit ();
    *(obj.selinux_enabled) = 0;
    cp_options_default (r15);
    r9d = 0;
    r10d = 0;
    *((rsp + 0x50)) = 0;
    rax = 0x200000004;
    *((rsp + 0x74)) = 1;
    *((rsp + 0x38)) = rax;
    rax = 0x100000000;
    *((rsp + 0x4c)) = 0;
    *((rsp + 0x34)) = 1;
    *((rsp + 0x40)) = rax;
    *((rsp + 0x48)) = r9w;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x60)) = 0x100;
    *((rsp + 0x68)) = 0;
    *((rsp + 0x6c)) = r10w;
    rax = getenv ("POSIXLY_CORRECT");
    *((rsp + 0x26)) = 0;
    *((rsp + 0x78)) = 0;
    *((rsp + 0x80)) = 0;
    *((rsp + 0x18)) = 0;
    *(rsp) = 0;
    *((rsp + 0x27)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x25)) = 0;
    rsp + 0x6e = (rax != 0) ? 1 : 0;
    do {
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = rbp;
        edi = r12d;
        eax = getopt_long ();
        if (eax != 0xffffffff) {
            if (eax > 0x88) {
                goto label_9;
            }
            if (eax > 0x47) {
                eax -= 0x48;
                if (eax > 0x40) {
                    goto label_9;
                }
                rax = *((rbx + rax*4));
                rax += rbx;
                /* switch table (65 cases) at 0x1433c */
                eax = void (*rax)() ();
            }
            if (eax == 0xffffff7d) {
                goto label_10;
            }
            if (eax != 0xffffff7e) {
                goto label_9;
            }
            usage (0);
        }
        if (*((rsp + 0x47)) != 0) {
            if (*((rsp + 0x6a)) != 0) {
                goto label_11;
            }
        }
        if (*((rsp + 0x38)) == 2) {
            goto label_12;
        }
        eax = *((rsp + 0x74));
        if (*((rsp + 0x25)) == 0) {
            goto label_13;
        }
        if (eax != 2) {
            goto label_14;
        }
        if (*((rsp + 0x3c)) == 2) {
            goto label_14;
        }
label_8:
        edx = 5;
label_4:
        rax = dcgettext (0, "--reflink can be used only with --sparse=auto");
        eax = 0;
        error (0, 0, rax);
label_9:
        usage (1);
        *((rsp + 0x68)) = 1;
    } while (1);
    rdi = optarg;
    if (rdi == 0) {
        *((rsp + 0x4d)) = 1;
        *((rsp + 0x4e)) = 0x101;
        *((rsp + 0x62)) = 1;
        goto label_0;
        *((rsp + 0x6a)) = 1;
        goto label_0;
        *(obj.remove_trailing_slashes) = 1;
        goto label_0;
        _xargmatch_internal ("--sparse", *(obj.optarg), obj.sparse_type_string, obj.sparse_type, 4, *(obj.argmatch_die));
        rcx = obj_sparse_type;
        eax = *((rcx + rax*4));
        *((rsp + 0x4c)) = eax;
        goto label_0;
        rsi = optarg;
        if (rsi == 0) {
            goto label_15;
        }
        _xargmatch_internal ("--reflink", rsi, obj.reflink_type_string, obj.reflink_type, 4, *(obj.argmatch_die));
        rcx = obj_reflink_type;
        eax = *((rcx + rax*4));
        *((rsp + 0x84)) = eax;
        goto label_0;
        *(obj.parents_option) = 1;
        goto label_0;
        rdi = optarg;
        edx = 0;
        rsi = r15;
        decode_preserve_arg ();
        goto label_0;
        *((rsp + 0x61)) = 0;
        goto label_0;
        *((rsp + 0x4c)) = 1;
        goto label_0;
        *((rsp + 0x6c)) = 1;
        goto label_0;
        *((rsp + 0x6b)) = 1;
        goto label_0;
        if (*(rsp) != 0) {
            goto label_16;
        }
        rax = optarg;
        *(rsp) = rax;
        goto label_0;
        *((rsp + 0x45)) = 1;
        goto label_0;
        *((rsp + 0x38)) = 2;
        goto label_0;
        *((rsp + 0x47)) = 1;
        goto label_0;
        *((rsp + 0x38)) = 3;
        goto label_0;
        *((rsp + 0x46)) = 1;
        goto label_0;
        *((rsp + 0x60)) = 1;
        *((rsp + 0x34)) = 2;
        goto label_0;
        rax = optarg;
        *((rsp + 0x25)) = 1;
        if (rax == 0) {
            rax = *((rsp + 8));
        }
        *((rsp + 8)) = rax;
        goto label_0;
        *((rsp + 0x34)) = 2;
        goto label_0;
        *((rsp + 0x34)) = 4;
        goto label_0;
        *((rsp + 0x34)) = 3;
        goto label_0;
        *((rsp + 0x34)) = 2;
        *((rsp + 0x60)) = 1;
        *((rsp + 0x4d)) = 1;
        *((rsp + 0x4e)) = 0x101;
        *((rsp + 0x62)) = 1;
        if (*(obj.selinux_enabled) != 0) {
            *((rsp + 0x63)) = 1;
        }
        *((rsp + 0x65)) = 1;
        *((rsp + 0x67)) = 0x101;
        goto label_0;
        rax = optarg;
        if (*(obj.selinux_enabled) == 0) {
            goto label_17;
        }
        if (rax == 0) {
            goto label_18;
        }
        *((rsp + 0x18)) = rax;
        goto label_0;
        rax = optarg;
        *((rsp + 0x25)) = 1;
        *((rsp + 0x10)) = rax;
        goto label_0;
label_10:
        rax = "Jim Meyering";
        eax = 0;
        version_etc (*(obj.stdout), 0x00014051, "GNU coreutils", *(obj.Version), "Torbjorn Granlund", "David MacKenzie");
        exit (0);
    }
    edx = 1;
    rsi = r15;
    rax = decode_preserve_arg ();
    *((rsp + 0x62)) = 1;
    goto label_0;
label_17:
    if (rax == 0) {
        goto label_0;
    }
    edx = 5;
    rax = dcgettext (0, "warning: ignoring --context; it requires an SELinux-enabled kernel");
    eax = 0;
    eax = error (0, 0, rax);
    goto label_0;
    *((rsp + 0x27)) = 1;
    goto label_0;
    *((rsp + 0x26)) = 1;
    goto label_0;
label_15:
    *((rsp + 0x74)) = 2;
    goto label_0;
label_12:
    *((rsp + 0x6b)) = 0;
    if (*((rsp + 0x25)) != 0) {
        goto label_19;
    }
label_13:
    eax = 0;
    if (*((rsp + 0x74)) == 2) {
        goto label_20;
    }
label_3:
    rdi = *((rsp + 0x10));
    *((rsp + 0x30)) = eax;
    set_simple_backup_suffix ();
    if (*((rsp + 0x34)) == 1) {
        goto label_21;
    }
label_5:
    if (*((rsp + 0x68)) != 0) {
        goto label_22;
    }
label_2:
    rcx = *((rsp + 0x18));
    rcx |= *((rsp + 0x58));
    eax = *((rsp + 0x64));
    if (rcx == 0) {
        goto label_23;
    }
    if (al != 0) {
        goto label_24;
    }
    *((rsp + 0x63)) = 0;
label_6:
    if (*((rsp + 0x18)) != 0) {
        goto label_25;
    }
    do {
label_1:
        if (*((rsp + 0x66)) != 0) {
            goto label_26;
        }
        hash_init ();
        ecx = *((rsp + 0x26));
        r8 = r15;
        rdx = *(obj.optind);
        edi -= edx;
        eax = do_copy (r12d, rbp + rdx*8, *(rsp), rcx);
        eax ^= 1;
        edi = (int32_t) al;
        al = exit (rdi);
label_23:
    } while (al == 0);
    if (*(obj.selinux_enabled) != 0) {
        goto label_1;
    }
label_7:
    edx = 5;
    rax = dcgettext (0, "cannot preserve security context without an SELinux-enabled kernel");
    eax = 0;
    error (1, 0, rax);
label_21:
    if (*((rsp + 0x68)) != 0) {
        if (*((rsp + 0x47)) != 0) {
            goto label_27;
        }
        *((rsp + 0x34)) = 2;
label_22:
        eax = *((rsp + 0x27));
        *((rsp + 0x44)) = al;
        goto label_2;
label_24:
        if (*((rsp + 0x63)) == 0) {
            goto label_28;
        }
        edx = 5;
        rax = dcgettext (0, "cannot set target context and preserve it");
        eax = 0;
        error (1, 0, rax);
label_14:
        edx = 5;
        rax = dcgettext (0, "backup type");
        rsi = *((rsp + 8));
        xget_version (rax);
        goto label_3;
label_19:
        edx = 5;
        rsi = "options --backup and --no-clobber are mutually exclusive";
        goto label_4;
label_11:
        edx = 5;
        rsi = "cannot make both hard and symbolic links";
        goto label_4;
    }
label_27:
    *((rsp + 0x34)) = 4;
    goto label_5;
label_28:
    if (*(obj.selinux_enabled) != 0) {
        goto label_6;
    }
    goto label_7;
label_20:
    if (*((rsp + 0x3c)) == 2) {
        goto label_3;
    }
    goto label_8;
label_18:
    errno_location ();
    edx = 5;
    *(rax) = 0x5f;
    *((rsp + 0x28)) = rax;
    *((rsp + 0x58)) = 0;
    rax = dcgettext (0, "warning: ignoring --context");
    rcx = *((rsp + 0x28));
    eax = 0;
    error (0, *(rcx), rax);
    goto label_0;
label_16:
    edx = 5;
    rax = dcgettext (0, "multiple target directories specified");
    eax = 0;
    error (1, 0, rax);
label_26:
    edx = 5;
    rax = dcgettext (0, "cannot preserve extended attributes, cp is built without xattr support");
    eax = 0;
    error (1, 0, rax);
label_25:
    errno_location ();
    *(rax) = 0x5f;
    rbx = rax;
    rax = quote (*((rsp + 0x18)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "failed to set default file creation context to %s");
    rcx = r12;
    eax = 0;
    error (1, *(rbx), rax);
}

/* /tmp/tmp7e2uf9hg @ 0x11280 */
 
uint64_t dbg_savedir (int64_t arg2) {
    rsi = arg2;
    /* char * savedir(char const * dir,savedir_option option); */
    r12d = esi;
    rax = opendir_safer ();
    if (rax != 0) {
        rax = streamsavedir (rax, r12d);
        rdi = rbp;
        r12 = rax;
        eax = closedir ();
        if (eax != 0) {
            goto label_0;
        }
        rax = r12;
        return rax;
    }
    r12d = 0;
    rax = r12;
    return rax;
label_0:
    rdi = r12;
    r12d = 0;
    fcn_00003670 ();
    rax = r12;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x3950 */
 
void closedir (void) {
    __asm ("bnd jmp qword [reloc.closedir]");
}

/* /tmp/tmp7e2uf9hg @ 0x5100 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... [-T] SOURCE DEST\n  or:  %s [OPTION]... SOURCE... DIRECTORY\n  or:  %s [OPTION]... -t DIRECTORY SOURCE...\n");
    r8 = r12;
    rcx = r12;
    rdx = r12;
    rsi = rax;
    edi = 1;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -a, --archive                same as -dR --preserve=all\n      --attributes-only        don't copy the file data, just the attributes\n      --backup[=CONTROL]       make a backup of each existing destination file\n  -b                           like --backup but does not accept an argument\n      --copy-contents          copy contents of special files when recursive\n  -d                           same as --no-dereference --preserve=links\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -f, --force                  if an existing destination file cannot be\n                                 opened, remove it and try again (this option\n                                 is ignored when the -n option is also used)\n  -i, --interactive            prompt before overwrite (overrides a previous -n\n                                  option)\n  -H                           follow command-line symbolic links in SOURCE\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -l, --link                   hard link files instead of copying\n  -L, --dereference            always follow symbolic links in SOURCE\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -n, --no-clobber             do not overwrite an existing file (overrides\n                                 a previous -i option)\n  -P, --no-dereference         never follow symbolic links in SOURCE\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -p                           same as --preserve=mode,ownership,timestamps\n      --preserve[=ATTR_LIST]   preserve the specified attributes (default:\n                                 mode,ownership,timestamps), if possible\n                                 additional attributes: context, links, xattr,\n                                 all\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --no-preserve=ATTR_LIST  don't preserve the specified attributes\n      --parents                use full source file name under DIRECTORY\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -R, -r, --recursive          copy directories recursively\n      --reflink[=WHEN]         control clone/CoW copies. See below\n      --remove-destination     remove each existing destination file before\n                                 attempting to open it (contrast with --force)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --sparse=WHEN            control creation of sparse files. See below\n      --strip-trailing-slashes  remove any trailing slashes from each SOURCE\n                                 argument\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -s, --symbolic-link          make symbolic links instead of copying\n  -S, --suffix=SUFFIX          override the usual backup suffix\n  -t, --target-directory=DIRECTORY  copy all SOURCE arguments into DIRECTORY\n  -T, --no-target-directory    treat DEST as a normal file\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -u, --update                 copy only when the SOURCE file is newer\n                                 than the destination file or when the\n                                 destination file is missing\n  -v, --verbose                explain what is being done\n  -x, --one-file-system        stay on this file system\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -Z                           set SELinux security context of destination\n                                 file to default type\n      --context[=CTX]          like -Z, or if CTX is specified then set the\n                                 SELinux or SMACK security context to CTX\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nBy default, sparse SOURCE files are detected by a crude heuristic and the\ncorresponding DEST file is made sparse as well.  That is the behavior\nselected by --sparse=auto.  Specify --sparse=always to create a sparse DEST\nfile whenever the SOURCE file contains a long enough sequence of zero bytes.\nUse --sparse=never to inhibit creation of sparse files.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWhen --reflink[=always] is specified, perform a lightweight copy, where the\ndata blocks are copied only when modified.  If this is not possible the copy\nfails, or if --reflink=auto is specified, fall back to a standard copy.\nUse --reflink=never to ensure a standard copy is performed.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe backup suffix is '~', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\nThe version control method may be selected via the --backup option or through\nthe VERSION_CONTROL environment variable.  Here are the values:\n\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  none, off       never make backups (even if --backup is given)\n  numbered, t     make numbered backups\n  existing, nil   numbered if numbered backups exist, simple otherwise\n  simple, never   always make simple backups\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "\nAs a special case, cp makes a backup of SOURCE when the force and backup\noptions are given and SOURCE and DEST are the same name for an existing,\nregular file.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x00014054;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x63;
    *((rsp + 8)) = rax;
    rax = 0x000140ce;
    edi = 0x70;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x00014051;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000140d8, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x00014070;
    printf_chk ();
    rax = 0x00016437;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000140d8, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x00014051;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x00014070;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x00014051;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmp7e2uf9hg @ 0xff20 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xb1c0 */
 
int64_t dbg_forget_created (uint32_t arg1, uint32_t arg2) {
    Src_to_dest probe;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    /* void forget_created(ino_t ino,dev_t dev); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x10)) = 0;
    rax = hash_remove (*(obj.src_to_dest), rsp);
    if (rax != 0) {
        rdi = *((rax + 0x10));
        fcn_00003670 ();
        rdi = rbp;
        fcn_00003670 ();
    }
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x116c0 */
 
int64_t dbg_gen_tempname (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int gen_tempname(char * tmpl,int suffixlen,int flags,int kind); */
    rcx = (int64_t) ecx;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = obj_tryfunc_0;
    *((rsp + 4)) = edx;
    try_tempname_len (rdi, rsi, rsp + 4, *((rax + rcx*8)), 6);
    rdx = *((rsp + 8));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x10610 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x12fe0 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = section__dynsym;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xcef0 */
 
int32_t dbg_strmode (char * bp, int32_t mode) {
    rsi = bp;
    rdi = mode;
    /* void strmode(mode_t mode,char * str); */
    rdx = rsi;
    esi = edi;
    eax = edi;
    ecx = 0x2d;
    esi &= case.0xf4aa.9;
    if (esi != 0x8000) {
        ecx = 0x64;
        if (esi == 0x4000) {
            goto label_1;
        }
        ecx = 0x62;
        if (esi == 0x6000) {
            goto label_1;
        }
        ecx = 0x63;
        if (esi == 0x2000) {
            goto label_1;
        }
        ecx = 0x6c;
        if (esi == 0xa000) {
            goto label_1;
        }
        ecx = 0x70;
        if (esi == 0x1000) {
            goto label_1;
        }
        ecx = 0x73;
        esi = 0x3f;
        if (esi == 0xc000) {
            ecx = esi;
            goto label_1;
        }
    }
label_1:
    *(rdx) = cl;
    ecx = eax;
    ecx &= 0x100;
    ecx -= ecx;
    ecx &= 0xffffffbb;
    ecx += 0x72;
    *((rdx + 1)) = cl;
    ecx = eax;
    ecx &= 0x80;
    ecx -= ecx;
    ecx &= 0xffffffb6;
    ecx += 0x77;
    *((rdx + 2)) = cl;
    ecx = eax;
    ecx &= 0x40;
    ecx -= ecx;
    if ((ah & 8) == 0) {
        goto label_2;
    }
    ecx &= 0xffffffe0;
    ecx += 0x73;
    do {
        *((rdx + 3)) = cl;
        ecx = eax;
        ecx &= 0x20;
        ecx -= ecx;
        ecx &= 0xffffffbb;
        ecx += 0x72;
        *((rdx + 4)) = cl;
        ecx = eax;
        ecx &= 0x10;
        ecx -= ecx;
        ecx &= 0xffffffb6;
        ecx += 0x77;
        *((rdx + 5)) = cl;
        ecx = eax;
        ecx &= 8;
        ecx -= ecx;
        if ((ah & 4) == 0) {
            goto label_3;
        }
        ecx &= 0xffffffe0;
        ecx += 0x73;
label_0:
        *((rdx + 6)) = cl;
        ecx = eax;
        ecx &= 4;
        ecx -= ecx;
        ecx &= 0xffffffbb;
        ecx += 0x72;
        *((rdx + 7)) = cl;
        ecx = eax;
        ecx &= 2;
        ecx -= ecx;
        ecx &= 0xffffffb6;
        ecx += 0x77;
        *((rdx + 8)) = cl;
        ecx = eax;
        ecx &= 1;
        if ((ah & 2) == 0) {
            goto label_4;
        }
        eax -= eax;
        eax &= 0xffffffe0;
        eax += 0x74;
        *((rdx + 9)) = al;
        eax = 0x20;
        *((rdx + 0xa)) = ax;
        return eax;
label_2:
        ecx &= 0xffffffb5;
        ecx += 0x78;
    } while (1);
label_4:
    eax -= eax;
    eax &= 0xffffffb5;
    eax += 0x78;
    *((rdx + 9)) = al;
    eax = 0x20;
    *((rdx + 0xa)) = ax;
    return eax;
label_3:
    ecx &= 0xffffffb5;
    ecx += 0x78;
    goto label_0;
}

/* /tmp/tmp7e2uf9hg @ 0xe4a0 */
 
int64_t dbg_triple_hash_no_name (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t triple_hash_no_name( const * x,size_t table_size); */
    rax = *((rdi + 8));
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x10440 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x3d18)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xda30 */
 
int64_t hash_get_first (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rax = *(rdi);
    rdx = *((rdi + 8));
    if (rax < rdx) {
        goto label_1;
    }
    void (*0x3ce4)() ();
    do {
        rax += 0x10;
        if (rax >= rdx) {
            goto label_2;
        }
label_1:
        r8 = *(rax);
    } while (r8 == 0);
    rax = r8;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
label_2:
    return hash_get_first_cold ();
}

/* /tmp/tmp7e2uf9hg @ 0x10320 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x3d0e)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xd080 */
 
uint64_t dbg_file_name_concat (void) {
    int64_t var_7h;
    int64_t var_8h;
    /* char * file_name_concat(char const * dir,char const * base,char ** base_in_result); */
    rax = mfile_name_concat (rdi, rsi, rdx);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x11300 */
 
uint32_t dbg_target_directory_operand (int32_t arg1) {
    rdi = arg1;
    /* int target_directory_operand(char const * file,stat * st); */
    eax = *(rdi);
    rcx = rdi;
    while (al != 0x2f) {
        if (al == 0) {
            goto label_1;
        }
        rdx = rcx + 1;
        if (al != 0x2e) {
            goto label_2;
        }
        eax = *((rcx + 1));
        if (al != 0x2f) {
            goto label_3;
        }
label_0:
        eax = *((rdx + 1));
        rcx = rdx + 1;
    }
    rdx = rcx;
    goto label_0;
label_3:
    if (al != 0) {
label_2:
        esi = 0x210000;
        eax = 0;
        void (*0x3b90)() ();
    }
label_1:
    eax = 0xffffff9c;
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0x10d30 */
 
uint64_t dbg_safe_write (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t safe_write(int fd, const * buf,size_t count); */
    r13d = edi;
    rbx = rdx;
    do {
label_0:
        rax = write (r13d, rbp, rbx);
        r12 = rax;
        if (rax >= 0) {
            goto label_1;
        }
        rax = errno_location ();
        eax = *(rax);
    } while (eax == 4);
    if (eax == 0x16) {
        if (rbx <= 0x7ff00000) {
            goto label_1;
        }
        ebx = 0x7ff00000;
        goto label_0;
    }
label_1:
    rax = r12;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x6da0 */
 
int32_t dbg_cp_options_default (int64_t arg1) {
    rdi = arg1;
    /* void cp_options_default(cp_options * x); */
    rbx = rdi;
    rdi = rdi + 8;
    eax = 0;
    *((rdi - 8)) = 0;
    rcx = rbx;
    *((rdi + 0x48)) = 0;
    rdi &= 0xfffffffffffffff8;
    rcx -= rdi;
    ecx += 0x58;
    ecx >>= 3;
    do {
        *(rdi) = rax;
        rcx--;
        rdi += 8;
    } while (rcx != 0);
    eax = geteuid ();
    *((rbx + 0x40)) = 0xffffffff;
    al = (eax == 0) ? 1 : 0;
    *((rbx + 0x1b)) = al;
    *((rbx + 0x1a)) = al;
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0xe490 */
 
void dbg_hash_delete (int64_t arg_8h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_delete(Hash_table * table, const * entry); */
    return void (*0xe300)() ();
}

/* /tmp/tmp7e2uf9hg @ 0xffc0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x3d03)() ();
    }
    if (rdx == 0) {
        void (*0x3d03)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x104d0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001c6d0]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0001c6e0]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x12b40 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xc760 */
 
uint64_t dbg_xget_version (uint32_t arg2) {
    rsi = arg2;
    /* backup_type xget_version(char const * context,char const * version); */
    if (rsi != 0) {
        if (*(rsi) != 0) {
            goto label_0;
        }
    }
    rax = getenv (0x000164e0);
    rsi = rax;
    eax = 2;
    if (rsi != 0) {
        if (*(rsi) != 0) {
            goto label_1;
        }
    }
    return rax;
label_1:
    rbx = obj_backup_types;
    _xargmatch_internal ("$VERSION_CONTROL", rsi, obj.backup_args, rbx, 4, *(obj.argmatch_die));
    eax = *((rbx + rax*4));
    return rax;
label_0:
    rbx = obj_backup_types;
    _xargmatch_internal (rdi, rsi, obj.backup_args, rbx, 4, *(obj.argmatch_die));
    eax = *((rbx + rax*4));
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x12bc0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xcd10 */
 
uint64_t dbg_fdutimensat (int64_t arg1, char *** arg2, char *** arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int fdutimensat(int fd,int dir,char const * file,timespec const * ts,int atflag); */
    r14d = r8d;
    r13d = esi;
    r12 = rcx;
    if (edi >= 0) {
        goto label_2;
    }
    if (rdx == 0) {
        goto label_3;
    }
label_1:
    ecx = r14d;
    rdx = r12;
    rsi = rbp;
    edi = r13d;
    eax = utimensat ();
    do {
label_0:
        if (eax == 1) {
            goto label_3;
        }
        return eax;
label_2:
        rsi = rcx;
        eax = futimens ();
    } while (eax != 0xffffffff);
    if (rbp == 0) {
        goto label_0;
    }
    rax = errno_location ();
    if (*(rax) == 0x26) {
        goto label_1;
    }
    eax = 0xffffffff;
    return rax;
label_3:
    errno_location ();
    *(rax) = 9;
    eax = 0xffffffff;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xcc60 */
 
uint32_t dbg_fadvise (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void fadvise(FILE * fp,fadvice_t advice); */
    if (rdi != 0) {
        r12d = esi;
        eax = fileno (rdi);
        ecx = r12d;
        edx = 0;
        esi = 0;
        edi = eax;
        void (*0x3960)() ();
    }
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0x3a90 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmp7e2uf9hg @ 0x10220 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp7e2uf9hg @ 0x12860 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xdb60 */
 
int64_t dbg_hash_do_for_each (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t hash_do_for_each(Hash_table const * table,Hash_processor processor,void * processor_data); */
    r14 = *(rdi);
    if (r14 >= *((rdi + 8))) {
        goto label_3;
    }
    r15 = rdi;
    r13 = rdx;
    r12d = 0;
    do {
        rdi = *(r14);
        if (rdi != 0) {
            goto label_4;
        }
label_0:
        r14 += 0x10;
    } while (*((r15 + 8)) > r14);
label_2:
    rax = r12;
    return rax;
label_4:
    rbx = r14;
    goto label_5;
label_1:
    rbx = *((rbx + 8));
    r12++;
    if (rbx == 0) {
        goto label_0;
    }
    rdi = *(rbx);
label_5:
    rsi = r13;
    al = void (*rbp)() ();
    if (al != 0) {
        goto label_1;
    }
    goto label_2;
label_3:
    r12d = 0;
    goto label_2;
}

/* /tmp/tmp7e2uf9hg @ 0xd1d0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x3ba0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmp7e2uf9hg @ 0xcc90 */
 
int64_t dbg_open_safer (int64_t arg_60h, int64_t arg3, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    rdx = arg3;
    rsi = oflag;
    rdi = path;
    /* int open_safer(char const * file,int flags,va_args ...); */
    *((rsp + 0x30)) = rdx;
    edx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = open (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x10;
        edx = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0xff60 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x12a40 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmp7e2uf9hg @ 0xc700 */
 
int32_t get_version (uint32_t arg2) {
    rsi = arg2;
    eax = 2;
    if (rsi != 0) {
        if (*(rsi) == 0) {
            return eax;
        }
        rbx = obj_backup_types;
        _xargmatch_internal (rdi, rsi, obj.backup_args, rbx, 4, *(obj.argmatch_die));
        eax = *((rbx + rax*4));
        return eax;
    }
    return eax;
}

/* /tmp/tmp7e2uf9hg @ 0x12620 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmp7e2uf9hg @ 0x3a10 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmp7e2uf9hg @ 0xca20 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name_1) = rdi;
}

/* /tmp/tmp7e2uf9hg @ 0xcc10 */
 
uint64_t dbg_strip_trailing_slashes (int64_t arg1) {
    rdi = arg1;
    /* _Bool strip_trailing_slashes(char * file); */
    rax = last_component ();
    rbx = rax;
    if (*(rax) == 0) {
        rbx = rbp;
    }
    rax = base_len (rbx);
    rbx += rax;
    *(rbx) = 0;
    al = (*(rbx) != 0) ? 1 : 0;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x11690 */
 
uint64_t gen_tempname_len (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    rax = obj_tryfunc_0;
    rcx = (int64_t) ecx;
    *((rsp + 0xc)) = edx;
    try_tempname_len (rdi, rsi, rsp + 0xc, *((rax + rcx*8)), r8);
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xb290 */
 
uint64_t dbg_remember_copied (int64_t arg_8h, int64_t arg_10h, char *** arg1, uint32_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * remember_copied(char const * name,ino_t ino,dev_t dev); */
    r13 = rdi;
    r12 = rsi;
    rbx = rdx;
    rax = xmalloc (0x18);
    rax = xstrdup (r13);
    rsi = rbp;
    *(rbp) = r12;
    *((rbp + 0x10)) = rax;
    *((rbp + 8)) = rbx;
    rax = hash_insert (*(obj.src_to_dest));
    if (rax != 0) {
        rbx = rax;
        eax = 0;
        if (rbp != rbx) {
            rdi = *((rbp + 0x10));
            fcn_00003670 ();
            rdi = rbp;
            fcn_00003670 ();
            rax = *((rbx + 0x10));
        }
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x12e40 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp7e2uf9hg @ 0x3700 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmp7e2uf9hg @ 0x12010 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = "%s (%s) %s\n";
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x00016a92);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x00016d80;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x16d80 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x3c40)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x3c40)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x3c40)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0xc920 */
 
uint64_t dbg_close_stdin (void) {
    /* void close_stdin(); */
    rbp = stdin;
    rdi = stdin;
    rax = freadahead ();
    if (rax != 0) {
        goto label_2;
    }
    eax = close_stream (rbp);
    if (eax != 0) {
        goto label_1;
    }
    do {
        void (*0xca40)() ();
label_2:
        eax = rpl_fseeko (rbp, 0, 1, rcx);
        rdi = stdin;
        if (eax == 0) {
            goto label_3;
        }
label_0:
        eax = close_stream (rdi);
    } while (eax == 0);
label_1:
    edx = 5;
    rax = dcgettext (0, "error closing file");
    r13 = file_name;
    r12 = rax;
    rax = errno_location ();
    if (r13 == 0) {
        goto label_4;
    }
    rax = quotearg_colon (r13, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbp), "%s: %s");
    close_stdout ();
    do {
        rax = exit (*(obj.exit_failure));
label_4:
        rcx = r12;
        eax = 0;
        error (0, *(rax), 0x000164c5);
        close_stdout ();
    } while (1);
label_3:
    eax = rpl_fflush (rdi);
    rdi = stdin;
    if (eax == 0) {
        goto label_0;
    }
    close_stream (rdi);
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0x11730 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0xc890 */
 
int64_t dbg_buffer_lcm (int64_t arg1, int64_t arg3, size_t b) {
    rdi = arg1;
    rdx = arg3;
    rsi = b;
    /* size_t buffer_lcm(size_t a,size_t b,size_t lcm_max); */
    rcx = rdi;
    rdi = rdx;
    if (rcx != 0) {
        goto label_2;
    }
    ecx = 0x2000;
    rcx = rsi;
    while (rsi == 0) {
label_0:
        rax = rdi;
        if (rcx <= rdi) {
            rax = rcx;
        }
        return rax;
label_2:
    }
    r8 = rsi;
    rax = rcx;
label_1:
    edx = 0;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    if (rdx != 0) {
        goto label_3;
    }
    edx = 0;
    rax = rcx;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    r8 = rax;
    rax = rsi;
    rdx:rax = rax * r8;
    __asm ("seto dl");
    edx = (int32_t) dl;
    if (rax > rdi) {
        goto label_0;
    }
    if (rdx != 0) {
        goto label_0;
    }
    return rax;
label_3:
    rax = r8;
    r8 = rdx;
    goto label_1;
}

/* /tmp/tmp7e2uf9hg @ 0x124a0 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x3000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x12ba0 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0x6d20 */
 
uint64_t dbg_dest_info_init (int64_t arg1) {
    rdi = arg1;
    /* void dest_info_init(cp_options * x); */
    rbx = rdi;
    rax = hash_initialize (0x3d, 0, dbg.triple_hash, dbg.triple_compare, dbg.triple_free);
    *((rbx + 0x48)) = rax;
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xd770 */
 
int64_t hash_get_n_entries (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x20));
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x134b0 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmp7e2uf9hg @ 0x128a0 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xb560 */
 
int64_t dbg_force_symlinkat (int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    symlink_arg arg;
    char[256] buf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_118h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int force_symlinkat(char const * srcname,int dstdir,char const * dstname,_Bool force,int symlinkat_errno); */
    r14 = rdi;
    r13 = rdx;
    ebx = ecx;
    rax = *(fs:0x28);
    *((rsp + 0x118)) = rax;
    eax = 0;
    if (r8d < 0) {
        goto label_3;
    }
label_0:
    if (bl == 1) {
        if (r8d == 0x11) {
            rbx = rsp + 0x10;
            rax = samedir_template (r13, rbx);
            r15 = rax;
            if (rax == 0) {
                goto label_4;
            }
            *(rsp) = r14;
            *((rsp + 8)) = ebp;
            eax = try_tempname_len (rax, 0, rsp, dbg.try_symlink, 6);
            if (eax == 0) {
                goto label_5;
            }
            rax = errno_location ();
            r12d = *(rax);
label_2:
            if (r15 == rbx) {
                goto label_1;
            }
            rdi = r15;
            fcn_00003670 ();
        }
    } else {
        r12d = r8d;
    }
    do {
label_1:
        rax = *((rsp + 0x118));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        eax = r12d;
        return rax;
label_3:
        eax = symlinkat ();
        r12d = eax;
    } while (eax == 0);
    rax = errno_location ();
    r8d = *(rax);
    goto label_0;
label_4:
    rax = errno_location ();
    r12d = *(rax);
    goto label_1;
label_5:
    rcx = r13;
    edx = ebp;
    rsi = r15;
    edi = ebp;
    eax = renameat ();
    r12d = 0xffffffff;
    if (eax == 0) {
        goto label_2;
    }
    rax = errno_location ();
    edx = 0;
    rsi = r15;
    edi = ebp;
    r12d = *(rax);
    unlinkat ();
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmp7e2uf9hg @ 0x12c60 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3a80)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp7e2uf9hg @ 0xbf00 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmp7e2uf9hg @ 0x3690 */
 
void getenv (void) {
    /* [15] -r-x section size 1600 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp7e2uf9hg @ 0x36a0 */
 
void mkfifoat (void) {
    __asm ("bnd jmp qword [reloc.mkfifoat]");
}

/* /tmp/tmp7e2uf9hg @ 0x36b0 */
 
void utimensat (void) {
    __asm ("bnd jmp qword [reloc.utimensat]");
}

/* /tmp/tmp7e2uf9hg @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    al = uint64_t (*rdi)() ();
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dh;
    if (*(rax) > 0) {
        void (*0xb2)() ();
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmp7e2uf9hg @ 0x36f0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmp7e2uf9hg @ 0x3710 */
 
void mkdir (void) {
    __asm ("bnd jmp qword [reloc.mkdir]");
}

/* /tmp/tmp7e2uf9hg @ 0x3720 */
 
void unlinkat (void) {
    __asm ("bnd jmp qword [reloc.unlinkat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3730 */
 
void qsort (void) {
    __asm ("bnd jmp qword [reloc.qsort]");
}

/* /tmp/tmp7e2uf9hg @ 0x3750 */
 
void faccessat (void) {
    __asm ("bnd jmp qword [reloc.faccessat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3760 */
 
void readlink (void) {
    __asm ("bnd jmp qword [reloc.readlink]");
}

/* /tmp/tmp7e2uf9hg @ 0x3770 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmp7e2uf9hg @ 0x3780 */
 
void clock_gettime (void) {
    __asm ("bnd jmp qword [reloc.clock_gettime]");
}

/* /tmp/tmp7e2uf9hg @ 0x3790 */
 
void write (void) {
    __asm ("bnd jmp qword [reloc.write]");
}

/* /tmp/tmp7e2uf9hg @ 0x37a0 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmp7e2uf9hg @ 0x37b0 */
 
void pathconf (void) {
    __asm ("bnd jmp qword [reloc.pathconf]");
}

/* /tmp/tmp7e2uf9hg @ 0x37c0 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp7e2uf9hg @ 0x37d0 */
 
void opendir (void) {
    __asm ("bnd jmp qword [reloc.opendir]");
}

/* /tmp/tmp7e2uf9hg @ 0x37e0 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmp7e2uf9hg @ 0x37f0 */
 
void stpcpy (void) {
    __asm ("bnd jmp qword [reloc.stpcpy]");
}

/* /tmp/tmp7e2uf9hg @ 0x3810 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmp7e2uf9hg @ 0x3830 */
 
void openat (void) {
    __asm ("bnd jmp qword [reloc.openat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3850 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmp7e2uf9hg @ 0x3860 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmp7e2uf9hg @ 0x3870 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmp7e2uf9hg @ 0x3880 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmp7e2uf9hg @ 0x38a0 */
 
void ftruncate (void) {
    __asm ("bnd jmp qword [reloc.ftruncate]");
}

/* /tmp/tmp7e2uf9hg @ 0x38b0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmp7e2uf9hg @ 0x38c0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmp7e2uf9hg @ 0x38d0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp7e2uf9hg @ 0x38f0 */
 
void ioctl (void) {
    __asm ("bnd jmp qword [reloc.ioctl]");
}

/* /tmp/tmp7e2uf9hg @ 0x3900 */
 
void copy_file_range (void) {
    __asm ("bnd jmp qword [reloc.copy_file_range]");
}

/* /tmp/tmp7e2uf9hg @ 0x3910 */
 
void canonicalize_file_name (void) {
    __asm ("bnd jmp qword [reloc.canonicalize_file_name]");
}

/* /tmp/tmp7e2uf9hg @ 0x3930 */
 
void rewinddir (void) {
    __asm ("bnd jmp qword [reloc.rewinddir]");
}

/* /tmp/tmp7e2uf9hg @ 0x3940 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmp7e2uf9hg @ 0x3960 */
 
void posix_fadvise (void) {
    __asm ("bnd jmp qword [reloc.posix_fadvise]");
}

/* /tmp/tmp7e2uf9hg @ 0x3970 */
 
void read (void) {
    __asm ("bnd jmp qword [reloc.read]");
}

/* /tmp/tmp7e2uf9hg @ 0x3980 */
 
void lstat (void) {
    __asm ("bnd jmp qword [reloc.lstat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3990 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmp7e2uf9hg @ 0x39a0 */
 
void fallocate (void) {
    __asm ("bnd jmp qword [reloc.fallocate]");
}

/* /tmp/tmp7e2uf9hg @ 0x39d0 */
 
void getdelim (void) {
    __asm ("bnd jmp qword [reloc.__getdelim]");
}

/* /tmp/tmp7e2uf9hg @ 0x39f0 */
 
void readlinkat (void) {
    __asm ("bnd jmp qword [reloc.readlinkat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3a00 */
 
void dirfd (void) {
    __asm ("bnd jmp qword [reloc.dirfd]");
}

/* /tmp/tmp7e2uf9hg @ 0x3a20 */
 
void fpathconf (void) {
    __asm ("bnd jmp qword [reloc.fpathconf]");
}

/* /tmp/tmp7e2uf9hg @ 0x3a30 */
 
void mknodat (void) {
    __asm ("bnd jmp qword [reloc.mknodat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3a40 */
 
void rpmatch (void) {
    __asm ("bnd jmp qword [reloc.rpmatch]");
}

/* /tmp/tmp7e2uf9hg @ 0x3a50 */
 
void mkdirat (void) {
    __asm ("bnd jmp qword [reloc.mkdirat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3a70 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3aa0 */
 
void readdir (void) {
    __asm ("bnd jmp qword [reloc.readdir]");
}

/* /tmp/tmp7e2uf9hg @ 0x3ac0 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmp7e2uf9hg @ 0x3ad0 */
 
void fchmodat (void) {
    __asm ("bnd jmp qword [reloc.fchmodat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3af0 */
 
void renameat2 (void) {
    __asm ("bnd jmp qword [reloc.renameat2]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b00 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b20 */
 
void linkat (void) {
    __asm ("bnd jmp qword [reloc.linkat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b70 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmp7e2uf9hg @ 0x3b90 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmp7e2uf9hg @ 0x3ba0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmp7e2uf9hg @ 0x3bb0 */
 
void fchown (void) {
    __asm ("bnd jmp qword [reloc.fchown]");
}

/* /tmp/tmp7e2uf9hg @ 0x3bd0 */
 
void futimens (void) {
    __asm ("bnd jmp qword [reloc.futimens]");
}

/* /tmp/tmp7e2uf9hg @ 0x3be0 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmp7e2uf9hg @ 0x3bf0 */
 
void fchownat (void) {
    __asm ("bnd jmp qword [reloc.fchownat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c00 */
 
void renameat (void) {
    __asm ("bnd jmp qword [reloc.renameat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c10 */
 
void getpagesize (void) {
    __asm ("bnd jmp qword [reloc.getpagesize]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c50 */
 
void getrandom (void) {
    __asm ("bnd jmp qword [reloc.getrandom]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c70 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c80 */
 
void symlinkat (void) {
    __asm ("bnd jmp qword [reloc.symlinkat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3c90 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmp7e2uf9hg @ 0x3ca0 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3cb0 */
 
void fstatat (void) {
    __asm ("bnd jmp qword [reloc.fstatat]");
}

/* /tmp/tmp7e2uf9hg @ 0x3cc0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmp7e2uf9hg @ 0x3030 */
 
void fcn_00003030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1616 named .plt */
    __asm ("bnd jmp qword [0x0001bc98]");
}

/* /tmp/tmp7e2uf9hg @ 0x3040 */
 
void fcn_00003040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3050 */
 
void fcn_00003050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3060 */
 
void fcn_00003060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3070 */
 
void fcn_00003070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3080 */
 
void fcn_00003080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3090 */
 
void fcn_00003090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x30a0 */
 
void fcn_000030a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x30b0 */
 
void fcn_000030b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x30c0 */
 
void fcn_000030c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x30d0 */
 
void fcn_000030d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x30e0 */
 
void fcn_000030e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x30f0 */
 
void fcn_000030f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3100 */
 
void fcn_00003100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3110 */
 
void fcn_00003110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3120 */
 
void fcn_00003120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3130 */
 
void fcn_00003130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3140 */
 
void fcn_00003140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3150 */
 
void fcn_00003150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3160 */
 
void fcn_00003160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3170 */
 
void fcn_00003170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3180 */
 
void fcn_00003180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3190 */
 
void fcn_00003190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x31a0 */
 
void fcn_000031a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x31b0 */
 
void fcn_000031b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x31c0 */
 
void fcn_000031c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x31d0 */
 
void fcn_000031d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x31e0 */
 
void fcn_000031e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x31f0 */
 
void fcn_000031f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3200 */
 
void fcn_00003200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3210 */
 
void fcn_00003210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3220 */
 
void fcn_00003220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3230 */
 
void fcn_00003230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3240 */
 
void fcn_00003240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3250 */
 
void fcn_00003250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3260 */
 
void fcn_00003260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3270 */
 
void fcn_00003270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3280 */
 
void fcn_00003280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3290 */
 
void fcn_00003290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x32a0 */
 
void fcn_000032a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x32b0 */
 
void fcn_000032b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x32c0 */
 
void fcn_000032c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x32d0 */
 
void fcn_000032d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x32e0 */
 
void fcn_000032e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x32f0 */
 
void fcn_000032f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3300 */
 
void fcn_00003300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3310 */
 
void fcn_00003310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3320 */
 
void fcn_00003320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3330 */
 
void fcn_00003330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3340 */
 
void fcn_00003340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3350 */
 
void fcn_00003350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3360 */
 
void fcn_00003360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3370 */
 
void fcn_00003370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3380 */
 
void fcn_00003380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3390 */
 
void fcn_00003390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x33a0 */
 
void fcn_000033a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x33b0 */
 
void fcn_000033b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x33c0 */
 
void fcn_000033c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x33d0 */
 
void fcn_000033d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x33e0 */
 
void fcn_000033e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x33f0 */
 
void fcn_000033f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3400 */
 
void fcn_00003400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3410 */
 
void fcn_00003410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3420 */
 
void fcn_00003420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3430 */
 
void fcn_00003430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3440 */
 
void fcn_00003440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3450 */
 
void fcn_00003450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3460 */
 
void fcn_00003460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3470 */
 
void fcn_00003470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3480 */
 
void fcn_00003480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3490 */
 
void fcn_00003490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x34a0 */
 
void fcn_000034a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x34b0 */
 
void fcn_000034b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x34c0 */
 
void fcn_000034c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x34d0 */
 
void fcn_000034d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x34e0 */
 
void fcn_000034e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x34f0 */
 
void fcn_000034f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3500 */
 
void fcn_00003500 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3510 */
 
void fcn_00003510 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3520 */
 
void fcn_00003520 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3530 */
 
void fcn_00003530 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3540 */
 
void fcn_00003540 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3550 */
 
void fcn_00003550 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3560 */
 
void fcn_00003560 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3570 */
 
void fcn_00003570 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3580 */
 
void fcn_00003580 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3590 */
 
void fcn_00003590 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x35a0 */
 
void fcn_000035a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x35b0 */
 
void fcn_000035b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x35c0 */
 
void fcn_000035c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x35d0 */
 
void fcn_000035d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x35e0 */
 
void fcn_000035e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x35f0 */
 
void fcn_000035f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3600 */
 
void fcn_00003600 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3610 */
 
void fcn_00003610 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3620 */
 
void fcn_00003620 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3630 */
 
void fcn_00003630 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3640 */
 
void fcn_00003640 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3650 */
 
void fcn_00003650 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp7e2uf9hg @ 0x3660 */
 
void fcn_00003660 (void) {
    return __asm ("bnd jmp section..plt");
}
