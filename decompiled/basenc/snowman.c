
struct s0 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    unsigned char f3;
    unsigned char f4;
    unsigned char f5;
    unsigned char f6;
    unsigned char f7;
};

int64_t decode_8(struct s0* rdi, uint64_t rsi, int64_t* rdx, int64_t* rcx) {
    int32_t r8d5;
    int64_t rsi6;
    uint32_t esi7;
    int64_t r9_8;
    uint32_t r9d9;
    int64_t rax10;
    int64_t r8_11;
    uint32_t r10d12;
    uint32_t esi13;
    int64_t rsi14;
    uint32_t esi15;
    int64_t r10_16;
    uint32_t r10d17;
    uint32_t r9d18;
    uint32_t esi19;
    int64_t rsi20;
    uint32_t r9d21;
    uint32_t esi22;
    uint32_t r10d23;
    int64_t rsi24;
    uint32_t esi25;
    int64_t r10_26;
    uint32_t r10d27;
    uint32_t r9d28;
    uint32_t esi29;
    int64_t rax30;
    uint32_t eax31;
    uint32_t r10d32;

    r8d5 = 0;
    if (reinterpret_cast<int64_t>(rsi) <= reinterpret_cast<int64_t>(7) || ((*reinterpret_cast<uint32_t*>(&rsi6) = rdi->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi6) + 4) = 0, esi7 = *reinterpret_cast<unsigned char*>(0x9ae0 + rsi6), *reinterpret_cast<signed char*>(&esi7) < 0) || (*reinterpret_cast<uint32_t*>(&r9_8) = rdi->f1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_8) + 4) = 0, r9d9 = *reinterpret_cast<unsigned char*>(0x9ae0 + r9_8), *reinterpret_cast<signed char*>(&r9d9) < 0))) {
        addr_4351_2:
        *reinterpret_cast<int32_t*>(&rax10) = r8d5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    } else {
        r8_11 = *rdx;
        if (*rcx) {
            r10d12 = r9d9;
            ++r8_11;
            *reinterpret_cast<signed char*>(&r10d12) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(&r10d12) >> 2);
            esi13 = esi7 << 3 | r10d12;
            *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&esi13);
            *rcx = *rcx - 1;
        }
        *reinterpret_cast<uint32_t*>(&rsi14) = rdi->f2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rsi14) != 61) 
            goto addr_424b_6;
    }
    if (rdi->f3 != 61) 
        goto addr_436a_8;
    if (rdi->f4 != 61) 
        goto addr_436a_8;
    addr_4364_10:
    if (rdi->f5 != 61) 
        goto addr_436a_8;
    addr_43a0_11:
    if (rdi->f6 != 61) {
        addr_436a_8:
        *rdx = r8_11;
        return 0;
    } else {
        if (rdi->f7 == 61) {
            addr_4348_13:
            *rdx = r8_11;
            r8d5 = 1;
            goto addr_4351_2;
        } else {
            goto addr_436a_8;
        }
    }
    addr_424b_6:
    esi15 = *reinterpret_cast<unsigned char*>(0x9ae0 + rsi14);
    if (*reinterpret_cast<signed char*>(&esi15) < 0) 
        goto addr_436a_8;
    *reinterpret_cast<uint32_t*>(&r10_16) = rdi->f3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_16) + 4) = 0;
    r10d17 = *reinterpret_cast<unsigned char*>(0x9ae0 + r10_16);
    if (*reinterpret_cast<signed char*>(&r10d17) < 0) 
        goto addr_436a_8;
    if (*rcx) {
        ++r8_11;
        r9d18 = r10d17;
        *reinterpret_cast<signed char*>(&r9d18) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(&r9d18) >> 4);
        esi19 = esi15 + esi15 | r9d9 << 6 | r9d18;
        *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&esi19);
        *rcx = *rcx - 1;
    }
    *reinterpret_cast<uint32_t*>(&rsi20) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi20) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rsi20) == 61) 
        goto addr_4364_10;
    r9d21 = *reinterpret_cast<unsigned char*>(0x9ae0 + rsi20);
    if (*reinterpret_cast<signed char*>(&r9d21) < 0) 
        goto addr_436a_8;
    if (*rcx) {
        esi22 = r9d21;
        ++r8_11;
        *reinterpret_cast<signed char*>(&esi22) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(&esi22) >> 1);
        r10d23 = r10d17 << 4 | esi22;
        *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&r10d23);
        *rcx = *rcx - 1;
    }
    *reinterpret_cast<uint32_t*>(&rsi24) = rdi->f5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi24) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rsi24) == 61) 
        goto addr_43a0_11;
    esi25 = *reinterpret_cast<unsigned char*>(0x9ae0 + rsi24);
    if (*reinterpret_cast<signed char*>(&esi25) < 0) 
        goto addr_436a_8;
    *reinterpret_cast<uint32_t*>(&r10_26) = rdi->f6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_26) + 4) = 0;
    r10d27 = *reinterpret_cast<unsigned char*>(0x9ae0 + r10_26);
    if (*reinterpret_cast<signed char*>(&r10d27) < 0) 
        goto addr_436a_8;
    if (*rcx) {
        ++r8_11;
        r9d28 = r10d27;
        *reinterpret_cast<signed char*>(&r9d28) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(&r9d28) >> 3);
        esi29 = esi25 << 2 | r9d21 << 7 | r9d28;
        *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&esi29);
        *rcx = *rcx - 1;
    }
    *reinterpret_cast<uint32_t*>(&rax30) = rdi->f7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rax30) == 61) 
        goto addr_4348_13;
    eax31 = *reinterpret_cast<unsigned char*>(0x9ae0 + rax30);
    if (*reinterpret_cast<signed char*>(&eax31) < 0) 
        goto addr_436a_8;
    if (*rcx) {
        ++r8_11;
        r10d32 = r10d27 << 5 | eax31;
        *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&r10d32);
        *rcx = *rcx - 1;
        goto addr_4348_13;
    }
}

int64_t decode_4(struct s0* rdi, uint64_t rsi, int64_t* rdx, int64_t* rcx) {
    int32_t r8d5;
    int64_t rcx6;
    uint32_t ecx7;
    int64_t r10_8;
    uint32_t r10d9;
    int64_t rax10;
    int64_t r8_11;
    uint32_t r11d12;
    uint32_t ecx13;
    int64_t rcx14;
    uint32_t ecx15;
    uint32_t r11d16;
    uint32_t r10d17;
    int64_t rax18;
    uint32_t eax19;
    uint32_t ecx20;

    r8d5 = 0;
    if (reinterpret_cast<int64_t>(rsi) <= reinterpret_cast<int64_t>(1) || ((*reinterpret_cast<uint32_t*>(&rcx6) = rdi->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0, ecx7 = *reinterpret_cast<unsigned char*>(0x9be0 + rcx6), *reinterpret_cast<signed char*>(&ecx7) < 0) || (*reinterpret_cast<uint32_t*>(&r10_8) = rdi->f1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_8) + 4) = 0, r10d9 = *reinterpret_cast<unsigned char*>(0x9be0 + r10_8), *reinterpret_cast<signed char*>(&r10d9) < 0))) {
        addr_4b35_2:
        *reinterpret_cast<int32_t*>(&rax10) = r8d5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    } else {
        r8_11 = *rdx;
        if (*rcx) {
            r11d12 = r10d9;
            ++r8_11;
            *reinterpret_cast<signed char*>(&r11d12) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(&r11d12) >> 4);
            ecx13 = ecx7 << 2 | r11d12;
            *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&ecx13);
            *rcx = *rcx - 1;
        }
        if (rsi == 2) 
            goto addr_4b4c_6;
        *reinterpret_cast<uint32_t*>(&rcx14) = rdi->f2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rcx14) != 61) 
            goto addr_4ad7_8;
    }
    if (rsi != 4 || rdi->f3 != 61) {
        addr_4b4c_6:
        *rdx = r8_11;
        return 0;
    } else {
        addr_4b2c_10:
        *rdx = r8_11;
        r8d5 = 1;
        goto addr_4b35_2;
    }
    addr_4ad7_8:
    ecx15 = *reinterpret_cast<unsigned char*>(0x9be0 + rcx14);
    if (*reinterpret_cast<signed char*>(&ecx15) < 0) 
        goto addr_4b4c_6;
    if (*rcx) {
        r11d16 = ecx15;
        ++r8_11;
        *reinterpret_cast<signed char*>(&r11d16) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(&r11d16) >> 2);
        r10d17 = r10d9 << 4 | r11d16;
        *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&r10d17);
        *rcx = *rcx - 1;
    }
    if (rsi == 3) 
        goto addr_4b4c_6;
    *reinterpret_cast<uint32_t*>(&rax18) = rdi->f3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rax18) != 61) 
        goto addr_4b0d_15;
    if (rsi == 4) 
        goto addr_4b2c_10;
    goto addr_4b4c_6;
    addr_4b0d_15:
    eax19 = *reinterpret_cast<unsigned char*>(0x9be0 + rax18);
    if (*reinterpret_cast<signed char*>(&eax19) < 0) 
        goto addr_4b4c_6;
    if (*rcx) {
        ++r8_11;
        ecx20 = ecx15 << 6 | eax19;
        *reinterpret_cast<signed char*>(r8_11 - 1) = *reinterpret_cast<signed char*>(&ecx20);
        *rcx = *rcx - 1;
        goto addr_4b2c_10;
    }
}

struct s1 {
    unsigned char* f0;
    signed char[7] pad8;
    struct s1* f8;
    signed char[8] pad24;
    struct s1* f18;
};

int64_t fun_2450();

int64_t fun_23a0(struct s1* rdi, ...);

unsigned char* quotearg_buffer_restyled(struct s1* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, uint32_t r8d, uint32_t r9d, unsigned char* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2450();
    if (r8d > 10) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9de0 + rax11 * 4) + 0x9de0;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    unsigned char f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

unsigned char* g28;

int32_t* fun_23b0();

struct s1* slotvec = reinterpret_cast<struct s1*>(0xe070);

uint32_t nslots = 1;

struct s1* xpalloc();

void fun_24f0();

struct s3 {
    unsigned char* f0;
    signed char[7] pad8;
    struct s1* f8;
};

void fun_2380(struct s1* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, int64_t* r8, int64_t r9);

struct s1* xcharalloc(unsigned char* rdi, ...);

int64_t fun_2470();

struct s1* quotearg_n_options(struct s1* rdi, unsigned char* rsi, struct s1* rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    unsigned char* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s1* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s1* rax12;
    struct s1* rax13;
    int64_t rax14;
    int64_t* r8_15;
    struct s3* rbx16;
    uint32_t r15d17;
    unsigned char* rsi18;
    struct s1* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    int64_t r9_23;
    unsigned char* rax24;
    unsigned char* rsi25;
    struct s1* rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x6a2f;
    rax8 = fun_23b0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xe070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x74a1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x6abb;
            fun_24f0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<uint64_t>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        *reinterpret_cast<uint32_t*>(&r9_23) = r15d22;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_23) + 4) = 0;
        rax24 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), *reinterpret_cast<uint32_t*>(&r9_23), &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx16->f0 = rsi25;
            if (r14_19 != 0xe120) {
                fun_2380(r14_19, rsi25, rsi, rdx, r8_15, r9_23);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx16->f8 = rax26;
            v28 = rcx->f30;
            r14_19 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d22, rsi25, v29, v28, 0x6b4a);
        }
        *rax8 = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_2470();
        } else {
            return r14_19;
        }
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
};

int64_t rpl_fclose(unsigned char* rdi);

int32_t fun_2660();

unsigned char* fun_2440();

int32_t fun_2620();

int64_t quotearg_n_style_colon();

int64_t finish_and_exit(unsigned char* rdi, struct s4* rsi, unsigned char* rdx, unsigned char* rcx, void* r8) {
    struct s4* r12_6;
    int64_t rax7;
    uint32_t eax8;
    uint32_t ebx9;

    r12_6 = rsi;
    rax7 = rpl_fclose(rdi);
    if (!*reinterpret_cast<int32_t*>(&rax7)) {
        fun_2660();
    } else {
        eax8 = r12_6->f0 - 45;
        ebx9 = eax8;
        if (eax8) 
            goto addr_373c_4;
        while (1) {
            ebx9 = r12_6->f1;
            addr_373c_4:
            fun_23b0();
            if (!ebx9) {
                fun_2440();
                fun_2620();
            }
            quotearg_n_style_colon();
            fun_2620();
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xe080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s5 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s5* locale_charset();

/* gettext_quote.part.0 */
unsigned char* gettext_quote_part_0(unsigned char* rdi, int32_t esi, unsigned char* rdx) {
    struct s5* rax4;
    uint32_t edx5;
    uint32_t edx6;
    unsigned char* rax7;
    uint32_t edx8;
    uint32_t edx9;
    unsigned char* rax10;
    unsigned char* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<unsigned char*>(0x9d6b);
            if (*rdi != 96) {
                rax7 = reinterpret_cast<unsigned char*>(0x9d64);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<unsigned char*>(0x9d6f);
            if (*rdi != 96) {
                rax10 = reinterpret_cast<unsigned char*>(0x9d60);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<unsigned char*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<unsigned char*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gde18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gde18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2373() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_2383() {
    __asm__("cli ");
    goto free;
}

int64_t strtoimax = 0x2040;

void fun_2393() {
    __asm__("cli ");
    goto strtoimax;
}

int64_t abort = 0x2050;

void fun_23a3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_23b3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23c3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23d3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23e3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_23f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fread_unlocked = 0x20b0;

void fun_2403() {
    __asm__("cli ");
    goto fread_unlocked;
}

int64_t textdomain = 0x20c0;

void fun_2413() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2423() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2433() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2443() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2453() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2463() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2473() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2483() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2493() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2150;

void fun_24a3() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2160;

void fun_24b3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2170;

void fun_24c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_24d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2190;

void fun_24e3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21a0;

void fun_24f3() {
    __asm__("cli ");
    goto memset;
}

int64_t posix_fadvise = 0x21b0;

void fun_2503() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memchr = 0x21c0;

void fun_2513() {
    __asm__("cli ");
    goto memchr;
}

int64_t memcmp = 0x21d0;

void fun_2523() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2533() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2543() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2553() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_2563() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2220;

void fun_2573() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_2583() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_2593() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_25a3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25b3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25c3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2280;

void fun_25d3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2290;

void fun_25e3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22a0;

void fun_25f3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22b0;

void fun_2603() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x22c0;

void fun_2613() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x22d0;

void fun_2623() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22e0;

void fun_2633() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x22f0;

void fun_2643() {
    __asm__("cli ");
    goto fopen;
}

int64_t __cxa_atexit = 0x2300;

void fun_2653() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2310;

void fun_2663() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2320;

void fun_2673() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2330;

void fun_2683() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2340;

void fun_2693() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2350;

void fun_26a3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2360;

void fun_26b3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(int64_t rdi);

unsigned char* fun_25f0(int64_t rdi, ...);

void fun_2430(int64_t rdi, int64_t rsi);

void fun_2410(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int64_t optarg = 0;

unsigned char* quote(int64_t rdi, ...);

int32_t usage();

unsigned char* stdout = reinterpret_cast<unsigned char*>(0);

int64_t Version = 0x9aa9;

void version_etc(unsigned char* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, unsigned char* a8);

int64_t fun_2480(int64_t rdi, int64_t* rsi);

uint32_t xstrtoimax(int64_t rdi);

void fun_2703(int32_t edi, int64_t* rsi) {
    int32_t ebp3;
    int64_t* rbx4;
    int64_t rdi5;
    int32_t v6;
    int64_t rax7;
    int64_t rdi8;
    unsigned char* rax9;
    unsigned char* rdx10;
    int64_t rax11;
    unsigned char* rdi12;
    int64_t rcx13;
    int64_t rdi14;
    int64_t rdi15;
    uint32_t eax16;
    int64_t v17;

    __asm__("cli ");
    ebp3 = edi;
    rbx4 = rsi;
    rdi5 = *rsi;
    set_program_name(rdi5);
    fun_25f0(6, 6);
    fun_2430("coreutils", "/usr/local/share/locale");
    fun_2410("coreutils", "/usr/local/share/locale");
    atexit(0x5200, "/usr/local/share/locale");
    v6 = 0;
    goto addr_278d_2;
    addr_28a7_3:
    goto *reinterpret_cast<int32_t*>(0x9920 + rax7 * 4) + 0x9920;
    addr_303e_4:
    rdi8 = optarg;
    quote(rdi8);
    fun_2440();
    fun_2620();
    fun_2440();
    fun_23b0();
    fun_2620();
    addr_2898_6:
    while (*reinterpret_cast<uint32_t*>(&rax7) = v6 + 0xffffff80, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0, *reinterpret_cast<uint32_t*>(&rax7) > 7) {
        rax9 = fun_2440();
        rdx10 = rax9;
        fun_2620();
        while (1) {
            addr_27dd_8:
            *reinterpret_cast<int32_t*>(&rax11) = usage();
            while (1) {
                addr_27f0_9:
                if (*reinterpret_cast<int32_t*>(&rax11) != 0xffffff7d) 
                    break;
                rdi12 = stdout;
                rcx13 = Version;
                version_etc(rdi12, "basenc", "GNU coreutils", rcx13, "Simon Josefsson", "Assaf Gordon", 0, rdx10);
                *reinterpret_cast<int32_t*>(&rax11) = fun_2660();
                while (1) {
                    v6 = *reinterpret_cast<int32_t*>(&rax11);
                    while (1) {
                        addr_278d_2:
                        rdx10 = reinterpret_cast<unsigned char*>("diw:");
                        *reinterpret_cast<int32_t*>(&rdi14) = ebp3;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                        rax11 = fun_2480(rdi14, rbx4);
                        if (*reinterpret_cast<int32_t*>(&rax11) == -1) 
                            goto addr_2898_6;
                        if (*reinterpret_cast<int32_t*>(&rax11) == 0x69) {
                            continue;
                        }
                        if (*reinterpret_cast<int32_t*>(&rax11) <= 0x69) {
                            if (*reinterpret_cast<int32_t*>(&rax11) == 0xffffff7e) 
                                goto addr_2891_16;
                            if (*reinterpret_cast<int32_t*>(&rax11) != 100) 
                                goto addr_27f0_9;
                            continue;
                        }
                        if (*reinterpret_cast<int32_t*>(&rax11) != 0x77) 
                            break;
                        rdi15 = optarg;
                        eax16 = xstrtoimax(rdi15);
                        if (eax16 > 1) 
                            goto addr_303e_4;
                        if (v17 < 0) 
                            goto addr_303e_4;
                        if (eax16 - 1) 
                            goto addr_288c_23;
                        addr_288c_23:
                    }
                    *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(rax11 - 0x80);
                    *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rdx10) > 7) 
                        goto addr_27dd_8;
                }
            }
        }
        addr_2891_16:
        usage();
    }
    goto addr_28a7_3;
}

int64_t __libc_start_main = 0;

void fun_30b3() {
    __asm__("cli ");
    __libc_start_main(0x2700, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xe008;

void fun_2370(int64_t rdi);

int64_t fun_3153() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2370(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3193() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

int64_t fun_31a3(int32_t edi) {
    int32_t edi2;
    uint64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    edi2 = edi + 2;
    rax3 = edi2 * 0x55555556 >> 32;
    *reinterpret_cast<int32_t*>(&rax4) = *reinterpret_cast<int32_t*>(&rax3) - (edi2 >> 31) << 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

int64_t fun_31c3(int32_t edi) {
    int32_t edi2;
    int64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    edi2 = edi + 4;
    rax3 = edi2 * 0x66666667 >> 33;
    *reinterpret_cast<int32_t*>(&rax4) = *reinterpret_cast<int32_t*>(&rax3) - (edi2 >> 31) << 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

int64_t fun_31e3(int64_t rdi) {
    uint32_t eax2;
    int32_t edi3;
    uint32_t edx4;
    int64_t rax5;

    __asm__("cli ");
    eax2 = static_cast<uint32_t>(rdi - 48);
    *reinterpret_cast<unsigned char*>(&eax2) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&eax2) <= 9);
    edi3 = *reinterpret_cast<int32_t*>(&rdi) - 65;
    *reinterpret_cast<unsigned char*>(&edx4) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&edi3) <= 21);
    *reinterpret_cast<uint32_t*>(&rax5) = eax2 | edx4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    return rax5;
}

int64_t fun_3203(int64_t rdi) {
    uint32_t eax2;
    int32_t edi3;
    uint32_t edx4;
    int64_t rax5;

    __asm__("cli ");
    eax2 = static_cast<uint32_t>(rdi - 48);
    *reinterpret_cast<unsigned char*>(&eax2) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&eax2) <= 9);
    edi3 = *reinterpret_cast<int32_t*>(&rdi) - 65;
    *reinterpret_cast<unsigned char*>(&edx4) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&edi3) <= 5);
    *reinterpret_cast<uint32_t*>(&rax5) = eax2 | edx4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    return rax5;
}

int64_t fun_3223(int64_t rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax2) = static_cast<int32_t>(rdi + rdi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

void fun_3233(unsigned char* rdi, int64_t rsi) {
    unsigned char* rsi3;
    uint32_t eax4;
    int64_t rdx5;
    int64_t tmp64_6;
    uint32_t ecx7;
    int64_t rax8;
    uint32_t eax9;
    int64_t rcx10;
    uint32_t ecx11;

    __asm__("cli ");
    if (rsi) {
        rsi3 = reinterpret_cast<unsigned char*>(rsi + reinterpret_cast<int64_t>(rdi));
        do {
            eax4 = *rdi;
            rdx5 = tmp64_6;
            ++rdi;
            ecx7 = eax4;
            *reinterpret_cast<uint32_t*>(&rax8) = eax4 & 15;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&ecx7) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ecx7) >> 4);
            eax9 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("0123456789ABCDEF") + rax8);
            *reinterpret_cast<uint32_t*>(&rcx10) = ecx7 & 15;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            ecx11 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("0123456789ABCDEF") + rcx10);
            *reinterpret_cast<signed char*>(rdx5 - 2) = *reinterpret_cast<signed char*>(&ecx11);
            *reinterpret_cast<signed char*>(rdx5 - 1) = *reinterpret_cast<signed char*>(&eax9);
        } while (rdi != rsi3);
    }
    return;
}

int64_t fun_3283(int64_t rdi) {
    int64_t rdx2;
    int32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rdx2) = static_cast<int32_t>(rdi + rdi * 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx2) + 4) = 0;
    eax3 = static_cast<int32_t>(rdx2 + 3);
    if (*reinterpret_cast<int32_t*>(&rdx2) >= 0) {
        eax3 = *reinterpret_cast<int32_t*>(&rdx2);
    }
    *reinterpret_cast<int32_t*>(&rax4) = eax3 >> 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

unsigned char fun_32a3(int32_t edi) {
    int32_t edi2;

    __asm__("cli ");
    edi2 = edi - 48;
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&edi2) <= 1));
}

int64_t fun_32b3(int64_t rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax2) = static_cast<int32_t>(rdi * 8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

void fun_32c3(unsigned char* rdi, int64_t rsi, int64_t rdx) {
    int64_t r8_4;
    unsigned char* rsi5;
    uint32_t ecx6;
    int64_t rax7;
    uint32_t edx8;
    uint32_t edx9;

    __asm__("cli ");
    if (rsi) {
        r8_4 = rdx + 8;
        rsi5 = reinterpret_cast<unsigned char*>(rsi + reinterpret_cast<int64_t>(rdi));
        do {
            ecx6 = *rdi;
            rax7 = r8_4 - 8;
            do {
                edx8 = ecx6;
                ++rax7;
                ecx6 = ecx6 + ecx6;
                *reinterpret_cast<unsigned char*>(&edx8) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx8) >> 7);
                edx9 = edx8 + 48;
                *reinterpret_cast<signed char*>(rax7 - 1) = *reinterpret_cast<signed char*>(&edx9);
            } while (rax7 != r8_4);
            ++rdi;
            r8_4 = r8_4 + 8;
        } while (rdi != rsi5);
    }
    return;
}

void fun_3313(unsigned char* rdi, int64_t rsi, int64_t rdx) {
    int64_t r8_4;
    unsigned char* rsi5;
    uint32_t ecx6;
    int64_t rax7;
    uint32_t edx8;
    uint32_t edx9;

    __asm__("cli ");
    if (rsi) {
        r8_4 = rdx + 8;
        rsi5 = reinterpret_cast<unsigned char*>(rsi + reinterpret_cast<int64_t>(rdi));
        do {
            ecx6 = *rdi;
            rax7 = r8_4 - 8;
            do {
                edx8 = ecx6;
                ++rax7;
                *reinterpret_cast<unsigned char*>(&ecx6) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ecx6) >> 1);
                edx9 = (edx8 & 1) + 48;
                *reinterpret_cast<signed char*>(rax7 - 1) = *reinterpret_cast<signed char*>(&edx9);
            } while (rax7 != r8_4);
            ++rdi;
            r8_4 = r8_4 + 8;
        } while (rsi5 != rdi);
    }
    return;
}

struct s6 {
    unsigned char f0;
    unsigned char f1;
};

void fun_3363(struct s6* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rsp5;
    unsigned char* rax6;
    unsigned char* v7;
    void* rax8;
    uint32_t eax9;
    unsigned char* r8_10;
    unsigned char* r9_11;
    int64_t rbp12;
    int64_t r11_13;
    int64_t r10_14;
    unsigned char v15;
    int64_t rax16;
    uint32_t esi17;
    int64_t rax18;
    int64_t rdi19;
    int64_t rsi20;
    unsigned char v21;
    int64_t r12_22;
    uint64_t rsi23;
    unsigned char v24;
    unsigned char v25;
    uint64_t rcx26;
    int64_t rdx27;
    int64_t rax28;
    uint64_t rcx29;
    uint32_t eax30;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16);
    rax6 = g28;
    v7 = rax6;
    if (!rsi) {
        addr_33ef_2:
        rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
        if (rax8) {
            fun_2470();
        } else {
            return;
        }
    } else {
        eax9 = rdi->f0;
        r8_10 = &rdi->f1;
        r9_11 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi) + rsi);
        rbp12 = rdx;
        r11_13 = rcx;
        *reinterpret_cast<int32_t*>(&r10_14) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_14) + 4) = 0;
        v15 = *reinterpret_cast<unsigned char*>(&eax9);
        *reinterpret_cast<int32_t*>(&rax16) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        if (r9_11 != r8_10) {
            do {
                esi17 = *r8_10;
                ++r8_10;
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp5) + *reinterpret_cast<int32_t*>(&rax16) + 4) = *reinterpret_cast<signed char*>(&esi17);
                if (*reinterpret_cast<int32_t*>(&rax16) == 3) {
                    *reinterpret_cast<uint32_t*>(&rax18) = v15;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
                    rdi19 = r10_14 + 4;
                    *reinterpret_cast<uint32_t*>(&rsi20) = v21;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi20) + 4) = 0;
                    r12_22 = rbp12 - r10_14;
                    rsi23 = reinterpret_cast<uint64_t>(rsi20 + ((rax18 << 24) + reinterpret_cast<int32_t>(static_cast<uint32_t>(v24) << 16) + reinterpret_cast<int32_t>(static_cast<uint32_t>(v25) << 8)));
                    while (1) {
                        rcx26 = rsi23;
                        rsi23 = __intrinsic() >> 6;
                        if (rdi19 < r11_13) {
                            rdx27 = __intrinsic() >> 5;
                            rax28 = rdx27 + rdx27 * 4;
                            rcx29 = rcx26 - (rax28 + (rax28 << 4));
                            eax30 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#") + *reinterpret_cast<int32_t*>(&rcx29));
                            *reinterpret_cast<signed char*>(r12_22 + rdi19) = *reinterpret_cast<signed char*>(&eax30);
                        }
                        if (rdi19 == r10_14) 
                            break;
                        --rdi19;
                    }
                    rbp12 = rbp12 + 5;
                    r10_14 = r10_14 + 5;
                    *reinterpret_cast<int32_t*>(&rax16) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                } else {
                    *reinterpret_cast<int32_t*>(&rax16) = static_cast<int32_t>(rax16 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                }
            } while (r9_11 != r8_10);
        }
        if (!*reinterpret_cast<int32_t*>(&rax16)) 
            goto addr_33ef_2;
    }
    fun_2440();
    fun_2620();
}

void fun_34e3(uint32_t edi) {
    uint32_t edx2;

    __asm__("cli ");
    if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edi) == 45)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edi) == 95)) || (edx2 = edi & 0xfffffffb, *reinterpret_cast<signed char*>(&edx2) == 43)) {
        return;
    }
}

void base64_encode(int64_t rdi, int64_t rsi, struct s1* rdx, unsigned char* rcx);

void fun_3523(int64_t rdi, int64_t rsi, struct s1* rdx, unsigned char* rcx) {
    struct s1* rbx5;
    struct s1* rcx6;
    uint32_t eax7;

    __asm__("cli ");
    rbx5 = rdx;
    base64_encode(rdi, rsi, rdx, rcx);
    if (rcx) {
        rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(rbx5) + reinterpret_cast<unsigned char>(rcx));
        while (1) {
            eax7 = reinterpret_cast<unsigned char>(rbx5->f0);
            if (*reinterpret_cast<signed char*>(&eax7) != 43) {
                if (*reinterpret_cast<signed char*>(&eax7) == 47) {
                    rbx5->f0 = reinterpret_cast<unsigned char*>(95);
                }
                rbx5 = reinterpret_cast<struct s1*>(&rbx5->pad8);
                if (rcx6 == rbx5) 
                    break;
            } else {
                rbx5->f0 = reinterpret_cast<unsigned char*>(45);
                rbx5 = reinterpret_cast<struct s1*>(&rbx5->pad8);
                if (rcx6 == rbx5) 
                    break;
            }
        }
    }
    return;
}

struct s7 {
    int64_t f0;
    signed char[8] pad16;
    struct s1* f10;
    int64_t f18;
};

void fun_3573(struct s7* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    rdi->f18 = 0x1068;
    rax2 = xcharalloc(0x1068);
    rdi->f0 = 1;
    rdi->f10 = rax2;
    return;
}

struct s8 {
    int32_t f0;
    signed char f4;
    signed char[11] pad16;
    struct s1* f10;
    int64_t f18;
};

void fun_35a3(struct s8* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    rdi->f18 = 0x1068;
    rax2 = xcharalloc(0x1068);
    rdi->f4 = 0;
    rdi->f10 = rax2;
    rdi->f0 = 0;
    return;
}

struct s9 {
    int32_t f0;
    signed char[1] pad5;
    signed char f5;
    signed char[10] pad16;
    struct s1* f10;
    int64_t f18;
};

void fun_35d3(struct s9* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    rdi->f18 = 0x1068;
    rax2 = xcharalloc(0x1068);
    rdi->f5 = 0;
    rdi->f10 = rax2;
    rdi->f0 = 1;
    return;
}

int64_t fun_24a0(int64_t rdi, ...);

int64_t fun_3603(int64_t rdi) {
    int32_t edx2;
    int64_t rax3;
    int32_t edx4;

    __asm__("cli ");
    if (*reinterpret_cast<signed char*>(&rdi) > 90) {
        edx2 = static_cast<int32_t>(rdi - 97);
        if (*reinterpret_cast<unsigned char*>(&edx2) > 25) {
            rax3 = fun_24a0(".-:+=^!/*?&<>()[]{}@%$#", ".-:+=^!/*?&<>()[]{}@%$#");
            *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(!!rax3);
            return rax3;
        } else {
            addr_364d_4:
            return 1;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&rdi) > 64) 
            goto addr_364d_4;
        edx4 = static_cast<int32_t>(rdi - 48);
        if (*reinterpret_cast<unsigned char*>(&edx4) <= 9) {
            return 1;
        }
    }
}

struct s10 {
    int32_t f0;
    int32_t f4;
};

int64_t base32_decode_ctx(int32_t* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, int64_t* r8);

int64_t fun_3663(struct s10* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, int64_t* r8) {
    int64_t rax6;

    __asm__("cli ");
    rax6 = base32_decode_ctx(&rdi->f4, rsi, rdx, rcx, r8);
    rdi->f0 = rdi->f4;
    return rax6;
}

struct s11 {
    signed char[16] pad16;
    struct s1* f10;
    int64_t f18;
};

void base32_decode_ctx_init(int64_t rdi);

void fun_3683(struct s11* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    base32_decode_ctx_init(reinterpret_cast<int64_t>(rdi) + 4);
    rdi->f18 = 0x1068;
    rax2 = xcharalloc(0x1068);
    rdi->f10 = rax2;
    return;
}

void fun_36b3(int64_t rdi) {
    __asm__("cli ");
    goto base32_decode_ctx_init;
}

struct s12 {
    int32_t f0;
    int32_t f4;
};

int64_t base64_decode_ctx(int32_t* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, int64_t* r8);

int64_t fun_36c3(struct s12* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, int64_t* r8) {
    int64_t rax6;

    __asm__("cli ");
    rax6 = base64_decode_ctx(&rdi->f4, rsi, rdx, rcx, r8);
    rdi->f0 = rdi->f4;
    return rax6;
}

struct s13 {
    signed char[16] pad16;
    struct s1* f10;
    int64_t f18;
};

void base64_decode_ctx_init(int64_t rdi);

void fun_36e3(struct s13* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    base64_decode_ctx_init(reinterpret_cast<int64_t>(rdi) + 4);
    rdi->f18 = 0x1068;
    rax2 = xcharalloc(0x1068);
    rdi->f10 = rax2;
    return;
}

void fun_3713(int64_t rdi) {
    __asm__("cli ");
    goto base64_decode_ctx_init;
}

struct s14 {
    int32_t f0;
    int32_t f4;
    signed char[8] pad16;
    unsigned char* f10;
    signed char[7] pad24;
    unsigned char* f18;
};

unsigned char* xreallocarray(unsigned char* rdi);

void fun_2570(unsigned char* rdi, unsigned char* rsi, unsigned char* rdx);

int64_t fun_37b3(struct s14* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, int64_t* r8) {
    unsigned char* r13_6;
    struct s1* r12_7;
    struct s14* rbp8;
    int64_t* rbx9;
    unsigned char* rdi10;
    unsigned char* rax11;
    unsigned char* rsi12;
    int64_t rax13;
    unsigned char* rcx14;
    uint32_t eax15;
    uint32_t edx16;

    __asm__("cli ");
    r13_6 = rdx;
    r12_7 = rcx;
    rbp8 = rdi;
    rbx9 = r8;
    rdi10 = rdi->f10;
    if (reinterpret_cast<signed char>(rdx) > reinterpret_cast<signed char>(rbp8->f18)) {
        rbp8->f18 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<unsigned char>(rdx));
        rax11 = xreallocarray(rdi10);
        rbp8->f10 = rax11;
        rdi10 = rax11;
    }
    fun_2570(rdi10, rsi, r13_6);
    rsi12 = rbp8->f10;
    if (!r13_6) {
        addr_3829_4:
        rax13 = base64_decode_ctx(&rbp8->f4, rsi12, r13_6, r12_7, rbx9);
        rbp8->f0 = rbp8->f4;
        return rax13;
    } else {
        rcx14 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi12) + reinterpret_cast<unsigned char>(r13_6));
        while (eax15 = *rsi12, edx16 = eax15 & 0xfffffffb, *reinterpret_cast<signed char*>(&edx16) != 43) {
            if (*reinterpret_cast<signed char*>(&eax15) != 45) {
                if (*reinterpret_cast<signed char*>(&eax15) == 95) {
                    *rsi12 = 47;
                }
                ++rsi12;
                if (rsi12 == rcx14) 
                    goto addr_3825_11;
            } else {
                *rsi12 = 43;
                ++rsi12;
                if (rsi12 == rcx14) 
                    goto addr_3825_11;
            }
        }
    }
    *rbx9 = 0;
    return 0;
    addr_3825_11:
    rsi12 = rbp8->f10;
    goto addr_3829_4;
}

void base32_encode(int64_t rdi, int64_t rsi, struct s1* rdx, unsigned char* rcx);

struct s15 {
    unsigned char f0;
    unsigned char f1;
    signed char f2;
};

void fun_24e0(int64_t rdi, struct s15** rsi, struct s15* rdx, int64_t rcx);

void fun_3893(int64_t rdi, int64_t rsi, struct s1* rdx, unsigned char* rcx) {
    struct s1* rdx5;
    struct s1* rcx6;
    uint32_t eax7;
    uint32_t eax8;

    __asm__("cli ");
    base32_encode(rdi, rsi, rdx, rcx);
    if (!rcx) {
        addr_38de_2:
        return;
    } else {
        rdx5 = rdx;
        rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(rdx) + reinterpret_cast<unsigned char>(rcx));
        do {
            eax7 = reinterpret_cast<uint32_t>(rdx5->f0 - 50);
            if (*reinterpret_cast<unsigned char*>(&eax7) > 40) 
                break;
            rdx5 = reinterpret_cast<struct s1*>(&rdx5->pad8);
            eax8 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("QRSTUV89:;<=>?@0123456789ABCDEFGHIJKLMNOP9.1.17-a351f") + *reinterpret_cast<signed char*>(&eax7));
            *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rdx5) - 1) = *reinterpret_cast<signed char*>(&eax8);
        } while (rdx5 != rcx6);
        goto addr_38de_2;
    }
    fun_24e0("0x32 <= *p && *p <= 0x5a", "src/basenc.c", 0x1c4, "base32hex_encode");
}

struct s16 {
    int32_t f0;
    unsigned char f4;
};

int64_t fun_3913(struct s16* rdi, int64_t rsi, int64_t rdx, unsigned char* rcx, int64_t* r8) {
    unsigned char* r10_6;
    int64_t rax7;
    int64_t rax8;
    int64_t r9_9;
    int64_t rdx10;
    int32_t ecx11;
    uint32_t esi12;
    uint32_t edx13;
    uint32_t esi14;
    int32_t ecx15;
    uint32_t esi16;

    __asm__("cli ");
    *r8 = 0;
    r10_6 = rcx;
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&rax7) = rdi->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) == 0);
        return rax7;
    }
    rax8 = rsi + 1;
    r9_9 = rax8 + rdx;
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<unsigned char*>(rax8 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rdx10) == 10) {
            addr_3947_5:
            ++rax8;
            if (rax8 == r9_9) 
                break; else 
                continue;
        } else {
            ecx11 = static_cast<int32_t>(rdx10 - 48);
            if (*reinterpret_cast<unsigned char*>(&ecx11) > 1) 
                goto addr_39c0_7;
            esi12 = rdi->f4;
            edx13 = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rdx10) == 49);
            if (rdi->f0) 
                goto addr_3974_9;
        }
        rdi->f0 = 7;
        esi14 = esi12 | edx13 << 7;
        rdi->f4 = *reinterpret_cast<unsigned char*>(&esi14);
        goto addr_3947_5;
        addr_3974_9:
        ecx15 = rdi->f0 - 1;
        rdi->f0 = ecx15;
        esi16 = esi12 | edx13 << *reinterpret_cast<unsigned char*>(&ecx15);
        rdi->f4 = *reinterpret_cast<unsigned char*>(&esi16);
        if (!ecx15) {
            ++rax8;
            *r10_6 = *reinterpret_cast<unsigned char*>(&esi16);
            ++r10_6;
            rdi->f4 = 0;
            *r8 = *r8 + 1;
            rdi->f0 = 0;
            if (rax8 == r9_9) 
                break;
        }
    }
    return 1;
    addr_39c0_7:
    return 0;
}

struct s17 {
    int32_t f0;
    unsigned char f4;
};

int64_t fun_39d3(struct s17* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t* r8) {
    int64_t r9_6;
    int64_t rax7;
    int64_t rsi8;
    int64_t rdx9;
    int64_t rax10;
    int32_t ecx11;
    int32_t ecx12;
    uint32_t eax13;
    int32_t ecx14;
    unsigned char al15;

    __asm__("cli ");
    *r8 = 0;
    r9_6 = rcx;
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&rax7) = rdi->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) == 0);
        return rax7;
    }
    rsi8 = rsi + 1;
    rdx9 = rdx + rsi8;
    while (1) {
        *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<unsigned char*>(rsi8 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax10) == 10) {
            addr_3a32_5:
            ++rsi8;
            if (rdx9 != rsi8) 
                continue; else 
                break;
        } else {
            ecx11 = static_cast<int32_t>(rax10 - 48);
            if (*reinterpret_cast<unsigned char*>(&ecx11) > 1) 
                goto addr_3a50_7;
            ecx12 = rdi->f0;
            eax13 = static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax10) == 49))) << *reinterpret_cast<unsigned char*>(&ecx12);
            ecx14 = ecx12 + 1;
            al15 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax13) | rdi->f4);
            rdi->f4 = al15;
            rdi->f0 = ecx14;
            if (ecx14 != 8) 
                goto addr_3a32_5;
        }
        rdi->f4 = 0;
        ++r9_6;
        *r8 = *r8 + 1;
        *reinterpret_cast<unsigned char*>(r9_6 - 1) = al15;
        rdi->f0 = 0;
        goto addr_3a32_5;
    }
    return 1;
    addr_3a50_7:
    return 0;
}

struct s18 {
    int32_t f0;
    int32_t f4;
    unsigned char f8;
    unsigned char f9;
    unsigned char fa;
    unsigned char fb;
    unsigned char fc;
};

int64_t fun_3a63(struct s18* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t* r8) {
    int64_t r9_6;
    int64_t rax7;
    int64_t rcx8;
    int64_t rdx9;
    int64_t rax10;
    int32_t r10d11;
    uint32_t r10d12;
    int64_t rax13;
    int32_t r11d14;
    int64_t r10_15;
    int64_t rax16;
    int64_t r10_17;

    __asm__("cli ");
    *r8 = 0;
    r9_6 = rcx;
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&rax7) = rdi->f4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) == 0));
        return rax7;
    }
    rcx8 = rsi + 1;
    rdx9 = rdx + rcx8;
    while (1) {
        *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<unsigned char*>(rcx8 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax10) == 10) {
            addr_3a90_5:
            ++rcx8;
            if (rdx9 == rcx8) 
                break; else 
                continue;
        } else {
            r10d11 = static_cast<int32_t>(rax10 - 33);
            if (*reinterpret_cast<unsigned char*>(&r10d11) > 92) 
                goto addr_3b70_7;
            r10d12 = *reinterpret_cast<unsigned char*>(0x9960 + (*reinterpret_cast<uint32_t*>(&rax10) - 33));
            if (*reinterpret_cast<signed char*>(&r10d12) < 0) 
                goto addr_3b70_7;
            rax13 = rdi->f4;
            r11d14 = static_cast<int32_t>(rax13 + 1);
            rdi->f4 = r11d14;
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi) + rax13 + 8) = *reinterpret_cast<signed char*>(&r10d12);
            if (r11d14 != 5) 
                goto addr_3a90_5;
        }
        *reinterpret_cast<uint32_t*>(&r10_15) = rdi->f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_15) + 4) = 0;
        rax16 = reinterpret_cast<int32_t>(rdi->f9 * 0x95eed + rdi->fa * 0x1c39 + rdi->fb * 85 + rdi->fc) + r10_15 * 0x31c84b1;
        r10_17 = rax16 >> 24;
        if (*reinterpret_cast<uint32_t*>(&r10_17) & 0x700) 
            goto addr_3b70_7;
        ++rcx8;
        *r8 = *r8 + 4;
        r9_6 = r9_6 + 4;
        *reinterpret_cast<uint32_t*>(r9_6 - 4) = *reinterpret_cast<uint32_t*>(&rax16) >> 24 | *reinterpret_cast<uint32_t*>(&rax16) >> 8 & 0xff00 | *reinterpret_cast<uint32_t*>(&rax16) << 8 & 0xff0000 | *reinterpret_cast<uint32_t*>(&rax16) << 24;
        rdi->f4 = 0;
        if (rdx9 == rcx8) 
            break;
    }
    rdi->f0 = rdi->f4;
    return 1;
    addr_3b70_7:
    return 0;
}

struct s19 {
    signed char[4] pad4;
    unsigned char f4;
    unsigned char f5;
};

int64_t fun_3b83(struct s19* rdi, signed char* rsi, int64_t rdx, int64_t rcx, int64_t* r8) {
    int64_t r9_6;
    signed char* r10_7;
    int64_t rax8;
    int64_t rax9;
    uint32_t edx10;
    uint32_t eax11;
    uint32_t ecx12;
    uint32_t edx13;
    uint32_t eax14;

    __asm__("cli ");
    *r8 = 0;
    r9_6 = rcx;
    r10_7 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsi) + rdx);
    if (!rdx) {
        *reinterpret_cast<uint32_t*>(&rax8) = static_cast<uint32_t>(rdi->f5) ^ 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
    do {
        *reinterpret_cast<int32_t*>(&rax9) = *rsi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        ++rsi;
        if (*reinterpret_cast<signed char*>(&rax9) != 10) {
            edx10 = static_cast<uint32_t>(rax9 - 48);
            if (edx10 > 9) {
                if (static_cast<uint32_t>(rax9 - 65) > 5) 
                    goto addr_3c10_7;
                edx10 = static_cast<uint32_t>(rax9 - 55);
            }
            eax11 = rdi->f5;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                ecx12 = rdi->f4;
                *r8 = *r8 + 1;
                ++r9_6;
                edx13 = edx10 + (ecx12 << 4);
                *reinterpret_cast<signed char*>(r9_6 - 1) = *reinterpret_cast<signed char*>(&edx13);
            } else {
                rdi->f4 = *reinterpret_cast<unsigned char*>(&edx10);
            }
            eax14 = eax11 ^ 1;
            rdi->f5 = *reinterpret_cast<unsigned char*>(&eax14);
        }
    } while (rsi != r10_7);
    return 1;
    addr_3c10_7:
    return 0;
}

struct s20 {
    int32_t f0;
    int32_t f4;
    signed char[8] pad16;
    unsigned char* f10;
    signed char[7] pad24;
    unsigned char* f18;
};

int64_t fun_3c23(struct s20* rdi, void* rsi, unsigned char* rdx, struct s1* rcx, int64_t* r8) {
    struct s1* r13_6;
    unsigned char* r12_7;
    struct s20* rbp8;
    void* rbx9;
    unsigned char* r9_10;
    unsigned char* rax11;
    unsigned char* rdx12;
    int64_t rax13;
    int32_t ecx14;
    int32_t ecx15;
    uint32_t eax16;
    int64_t rax17;

    __asm__("cli ");
    r13_6 = rcx;
    r12_7 = rdx;
    rbp8 = rdi;
    rbx9 = rsi;
    r9_10 = rdi->f10;
    if (reinterpret_cast<signed char>(rdx) > reinterpret_cast<signed char>(rdi->f18)) {
        rdi->f18 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<unsigned char>(rdx));
        rax11 = xreallocarray(r9_10);
        r8 = r8;
        rbp8->f10 = rax11;
        r9_10 = rax11;
    }
    if (r12_7) {
        *reinterpret_cast<int32_t*>(&rdx12) = 0;
        *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&rax13) = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rbx9) + reinterpret_cast<unsigned char>(rdx12));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            ecx14 = static_cast<int32_t>(rax13 - 48);
            if (*reinterpret_cast<unsigned char*>(&ecx14) <= 9 || (ecx15 = static_cast<int32_t>(rax13 - 65), *reinterpret_cast<unsigned char*>(&ecx15) <= 21)) {
                eax16 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJ:;<=>?@KLMNOPQRSTUVWXYZ234567") + (*reinterpret_cast<int32_t*>(&rax13) - 48));
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(rdx12)) = *reinterpret_cast<signed char*>(&eax16);
                ++rdx12;
                if (r12_7 == rdx12) 
                    break;
            } else {
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(rdx12)) = *reinterpret_cast<signed char*>(&rax13);
                ++rdx12;
                if (r12_7 == rdx12) 
                    break;
            }
        }
        r9_10 = rbp8->f10;
    }
    rax17 = base32_decode_ctx(&rbp8->f4, r9_10, r12_7, r13_6, r8);
    rbp8->f0 = rbp8->f4;
    return rax17;
}

unsigned char* program_name = reinterpret_cast<unsigned char*>(0);

void fun_2600(int64_t rdi, unsigned char* rsi, unsigned char* rdx, unsigned char* rcx);

void fun_2530(unsigned char* rdi, unsigned char* rsi, int64_t rdx, unsigned char* rcx);

int32_t fun_2550(struct s4* rdi, int64_t rsi, ...);

int32_t fun_23c0(unsigned char* rdi, int64_t rsi, int64_t rdx, unsigned char* rcx);

unsigned char* stderr = reinterpret_cast<unsigned char*>(0);

void fun_2680(unsigned char* rdi, int64_t rsi, unsigned char* rdx, unsigned char* rcx, unsigned char* r8, unsigned char* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3cf3(int32_t edi) {
    unsigned char* r12_2;
    unsigned char* rax3;
    unsigned char* v4;
    unsigned char* rax5;
    unsigned char* rcx6;
    unsigned char* r12_7;
    unsigned char* rax8;
    unsigned char* r12_9;
    unsigned char* rax10;
    unsigned char* r12_11;
    unsigned char* rax12;
    unsigned char* r12_13;
    unsigned char* rax14;
    unsigned char* r12_15;
    unsigned char* rax16;
    unsigned char* r12_17;
    unsigned char* rax18;
    unsigned char* r12_19;
    unsigned char* rax20;
    unsigned char* r12_21;
    unsigned char* rax22;
    unsigned char* r12_23;
    unsigned char* rax24;
    unsigned char* r12_25;
    unsigned char* rax26;
    unsigned char* r12_27;
    unsigned char* rax28;
    unsigned char* r12_29;
    unsigned char* rax30;
    unsigned char* r12_31;
    unsigned char* rax32;
    unsigned char* r12_33;
    unsigned char* rax34;
    unsigned char* r12_35;
    unsigned char* rax36;
    int32_t eax37;
    unsigned char* r13_38;
    unsigned char* rax39;
    unsigned char* rax40;
    int32_t eax41;
    unsigned char* rax42;
    unsigned char* rax43;
    unsigned char* rax44;
    int32_t eax45;
    unsigned char* rax46;
    unsigned char* r15_47;
    unsigned char* rax48;
    unsigned char* rax49;
    unsigned char* rax50;
    unsigned char* rdi51;
    unsigned char* r8_52;
    unsigned char* r9_53;
    int64_t v54;
    int64_t v55;
    int64_t v56;
    int64_t v57;
    int64_t v58;
    int64_t v59;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2440();
            fun_2600(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2440();
            fun_2530(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2440();
            fun_2530(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2440();
            fun_2530(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2440();
            fun_2530(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2440();
            fun_2530(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2440();
            fun_2530(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2440();
            fun_2530(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_2440();
            fun_2530(rax22, r12_21, 5, rcx6);
            r12_23 = stdout;
            rax24 = fun_2440();
            fun_2530(rax24, r12_23, 5, rcx6);
            r12_25 = stdout;
            rax26 = fun_2440();
            fun_2530(rax26, r12_25, 5, rcx6);
            r12_27 = stdout;
            rax28 = fun_2440();
            fun_2530(rax28, r12_27, 5, rcx6);
            r12_29 = stdout;
            rax30 = fun_2440();
            fun_2530(rax30, r12_29, 5, rcx6);
            r12_31 = stdout;
            rax32 = fun_2440();
            fun_2530(rax32, r12_31, 5, rcx6);
            r12_33 = stdout;
            rax34 = fun_2440();
            fun_2530(rax34, r12_33, 5, rcx6);
            r12_35 = stdout;
            rax36 = fun_2440();
            fun_2530(rax36, r12_35, 5, rcx6);
            do {
                if (1) 
                    break;
                eax37 = fun_2550("basenc", 0, "basenc", 0);
            } while (eax37);
            r13_38 = v4;
            if (!r13_38) {
                rax39 = fun_2440();
                fun_2600(1, rax39, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax40 = fun_25f0(5);
                if (!rax40 || (eax41 = fun_23c0(rax40, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax41)) {
                    rax42 = fun_2440();
                    r13_38 = reinterpret_cast<unsigned char*>("basenc");
                    fun_2600(1, rax42, "https://www.gnu.org/software/coreutils/", "basenc");
                    r12_2 = reinterpret_cast<unsigned char*>(" invocation");
                } else {
                    r13_38 = reinterpret_cast<unsigned char*>("basenc");
                    goto addr_41c0_9;
                }
            } else {
                rax43 = fun_2440();
                fun_2600(1, rax43, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax44 = fun_25f0(5);
                if (!rax44 || (eax45 = fun_23c0(rax44, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax45)) {
                    addr_40c6_11:
                    rax46 = fun_2440();
                    fun_2600(1, rax46, "https://www.gnu.org/software/coreutils/", "basenc");
                    r12_2 = reinterpret_cast<unsigned char*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_38 == "basenc")) {
                        r12_2 = reinterpret_cast<unsigned char*>(0xa1a1);
                    }
                } else {
                    addr_41c0_9:
                    r15_47 = stdout;
                    rax48 = fun_2440();
                    fun_2530(rax48, r15_47, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_40c6_11;
                }
            }
            rax49 = fun_2440();
            rcx6 = r12_2;
            fun_2600(1, rax49, r13_38, rcx6);
            addr_3d4e_14:
            fun_2660();
        }
    } else {
        rax50 = fun_2440();
        rdi51 = stderr;
        rcx6 = r12_2;
        fun_2680(rdi51, 1, rax50, rcx6, r8_52, r9_53, v54, v55, v56, v57, v58, v59);
        goto addr_3d4e_14;
    }
}

struct s21 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    unsigned char f3;
    unsigned char f4;
    unsigned char f5;
};

struct s22 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

void fun_43b3(struct s21* rdi, void* rsi, struct s22* rdx, int64_t rcx) {
    struct s22* rax5;
    int64_t rdx6;
    uint32_t ecx7;
    uint32_t r9d8;
    int64_t r9_9;
    uint32_t r9d10;
    struct s21* r9_11;
    struct s21* r11_12;
    struct s21* r10_13;
    struct s21* rbp14;
    struct s21* rbx15;
    uint32_t ecx16;
    uint32_t r8d17;
    uint32_t r12d18;
    int64_t rcx19;
    uint32_t ecx20;
    uint32_t ecx21;
    int64_t rcx22;
    uint32_t ecx23;
    uint32_t r8d24;
    int64_t rcx25;
    uint32_t r12d26;
    int64_t r8_27;
    uint32_t r8d28;
    uint32_t r8d29;
    uint32_t ecx30;
    uint32_t r12d31;
    int64_t r8_32;
    uint32_t r8d33;
    uint32_t r8d34;
    int64_t r8_35;
    uint32_t r8d36;
    uint32_t ecx37;
    uint32_t r8d38;
    uint32_t r12d39;
    int64_t rcx40;
    uint32_t ecx41;
    int64_t r8_42;
    uint32_t ecx43;
    uint32_t r8d44;
    int64_t r8_45;
    uint32_t r8d46;
    int64_t rcx47;
    uint32_t ecx48;
    struct s22* rcx49;
    int64_t r8_50;
    uint32_t ecx51;
    int64_t r8_52;
    uint32_t ecx53;
    int64_t rcx54;
    uint32_t ecx55;

    __asm__("cli ");
    if (!rcx) 
        goto addr_460d_2;
    rax5 = rdx;
    rdx6 = rcx;
    if (!rsi) {
        if (rcx) {
            rax5->f0 = 0;
            goto addr_460d_2;
        }
    }
    ecx7 = rdi->f0;
    r9d8 = ecx7;
    *reinterpret_cast<unsigned char*>(&r9d8) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r9d8) >> 3);
    *reinterpret_cast<uint32_t*>(&r9_9) = r9d8 & 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_9) + 4) = 0;
    r9d10 = *reinterpret_cast<unsigned char*>(0x9ac0 + r9_9);
    rax5->f0 = *reinterpret_cast<signed char*>(&r9d10);
    if (rdx6 == 1) {
        addr_460d_2:
        return;
    } else {
        r9_11 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 1);
        r11_12 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 3);
        r10_13 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 2);
        rbp14 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 5);
        rbx15 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 4);
        do {
            ecx16 = ecx7 << 2;
            if (r9_11 == rdi) 
                break;
            r8d17 = rdi->f1;
            r12d18 = r8d17;
            *reinterpret_cast<unsigned char*>(&r12d18) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d18) >> 6);
            *reinterpret_cast<uint32_t*>(&rcx19) = ecx16 + r12d18 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0;
            ecx20 = *reinterpret_cast<unsigned char*>(0x9ac0 + rcx19);
            rax5->f1 = *reinterpret_cast<signed char*>(&ecx20);
            if (rdx6 == 2) 
                goto addr_45b0_10;
            ecx21 = r8d17;
            *reinterpret_cast<unsigned char*>(&ecx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ecx21) >> 1);
            *reinterpret_cast<uint32_t*>(&rcx22) = ecx21 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
            ecx23 = *reinterpret_cast<unsigned char*>(0x9ac0 + rcx22);
            rax5->f2 = *reinterpret_cast<signed char*>(&ecx23);
            if (rdx6 == 3) 
                goto addr_45b0_10;
            r8d24 = r8d17 << 4;
            if (rdi == r10_13) 
                goto addr_45b8_13;
            *reinterpret_cast<uint32_t*>(&rcx25) = rdi->f2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx25) + 4) = 0;
            r12d26 = *reinterpret_cast<uint32_t*>(&rcx25);
            *reinterpret_cast<unsigned char*>(&r12d26) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d26) >> 4);
            *reinterpret_cast<uint32_t*>(&r8_27) = r8d24 + r12d26 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_27) + 4) = 0;
            r8d28 = *reinterpret_cast<unsigned char*>(0x9ac0 + r8_27);
            rax5->f3 = *reinterpret_cast<signed char*>(&r8d28);
            if (rdx6 == 4) 
                goto addr_45b0_10;
            r8d29 = static_cast<uint32_t>(rcx25 + rcx25);
            if (rdi == r11_12) 
                goto addr_45d0_16;
            ecx30 = rdi->f3;
            r12d31 = ecx30;
            *reinterpret_cast<unsigned char*>(&r12d31) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d31) >> 7);
            *reinterpret_cast<uint32_t*>(&r8_32) = r8d29 + r12d31 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_32) + 4) = 0;
            r8d33 = *reinterpret_cast<unsigned char*>(0x9ac0 + r8_32);
            rax5->f4 = *reinterpret_cast<signed char*>(&r8d33);
            if (rdx6 == 5) 
                goto addr_45b0_10;
            r8d34 = ecx30;
            *reinterpret_cast<unsigned char*>(&r8d34) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d34) >> 2);
            *reinterpret_cast<uint32_t*>(&r8_35) = r8d34 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_35) + 4) = 0;
            r8d36 = *reinterpret_cast<unsigned char*>(0x9ac0 + r8_35);
            rax5->f5 = *reinterpret_cast<signed char*>(&r8d36);
            if (rdx6 == 6) 
                goto addr_45b0_10;
            ecx37 = ecx30 << 3;
            if (rdi == rbx15) 
                goto addr_45e8_20;
            r8d38 = rdi->f4;
            r12d39 = r8d38;
            *reinterpret_cast<unsigned char*>(&r12d39) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d39) >> 5);
            *reinterpret_cast<uint32_t*>(&rcx40) = ecx37 + r12d39 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx40) + 4) = 0;
            ecx41 = *reinterpret_cast<unsigned char*>(0x9ac0 + rcx40);
            rax5->f6 = *reinterpret_cast<signed char*>(&ecx41);
            if (rdx6 == 7) 
                goto addr_45b0_10;
            *reinterpret_cast<uint32_t*>(&r8_42) = r8d38 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_42) + 4) = 0;
            ++rax5;
            ecx43 = *reinterpret_cast<unsigned char*>(0x9ac0 + r8_42);
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax5) - 1) = *reinterpret_cast<signed char*>(&ecx43);
            rdx6 = rdx6 - 8;
            if (!rdx6) 
                goto addr_45b0_10;
            if (rdi == rbp14) 
                goto addr_4600_24;
            ecx7 = rdi->f5;
            rdi = reinterpret_cast<struct s21*>(&rdi->f5);
            r8d44 = ecx7;
            *reinterpret_cast<unsigned char*>(&r8d44) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d44) >> 3);
            *reinterpret_cast<uint32_t*>(&r8_45) = r8d44 & 31;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_45) + 4) = 0;
            r8d46 = *reinterpret_cast<unsigned char*>(0x9ac0 + r8_45);
            rax5->f0 = *reinterpret_cast<signed char*>(&r8d46);
        } while (rdx6 != 1);
        goto addr_45b0_10;
    }
    *reinterpret_cast<uint32_t*>(&rcx47) = ecx16 & 28;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx47) + 4) = 0;
    ecx48 = *reinterpret_cast<unsigned char*>(0x9ac0 + rcx47);
    rax5->f1 = *reinterpret_cast<signed char*>(&ecx48);
    if (rdx6 == 2) 
        goto addr_45b0_10;
    rax5->f2 = 61;
    if (rdx6 == 3) 
        goto addr_45b0_10;
    rax5->f3 = 61;
    if (rdx6 == 4) 
        goto addr_45b0_10;
    rax5->f4 = 61;
    if (rdx6 == 5) 
        goto addr_45b0_10;
    addr_458b_30:
    rax5->f5 = 61;
    if (rdx6 == 6) 
        goto addr_45b0_10;
    rax5->f6 = 61;
    if (rdx6 == 7) 
        goto addr_45b0_10;
    rax5->f7 = 61;
    rcx49 = rax5 + 1;
    if (rdx6 == 8) {
        addr_45b0_10:
        return;
    } else {
        addr_45ad_33:
        rcx49->f0 = 0;
        goto addr_45b0_10;
    }
    addr_45b8_13:
    *reinterpret_cast<uint32_t*>(&r8_50) = r8d24 & 16;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_50) + 4) = 0;
    ecx51 = *reinterpret_cast<unsigned char*>(0x9ac0 + r8_50);
    rax5->f3 = *reinterpret_cast<signed char*>(&ecx51);
    if (rdx6 == 4) {
        return;
    }
    addr_45d0_16:
    *reinterpret_cast<uint32_t*>(&r8_52) = r8d29 & 30;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
    ecx53 = *reinterpret_cast<unsigned char*>(0x9ac0 + r8_52);
    rax5->f4 = *reinterpret_cast<signed char*>(&ecx53);
    if (rdx6 == 5) 
        goto addr_45b0_10;
    goto addr_458b_30;
    addr_45e8_20:
    *reinterpret_cast<uint32_t*>(&rcx54) = ecx37 & 24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx54) + 4) = 0;
    ecx55 = *reinterpret_cast<unsigned char*>(0x9ac0 + rcx54);
    rax5->f6 = *reinterpret_cast<signed char*>(&ecx55);
    if (rdx6 == 7) {
        return;
    }
    addr_4600_24:
    rcx49 = rax5;
    goto addr_45ad_33;
}

struct s1* fun_2590(unsigned char* rdi);

unsigned char* fun_4613(int64_t rdi, int64_t rsi, struct s1** rdx) {
    int64_t rcx4;
    int64_t rdx5;
    unsigned char* r12_6;
    int64_t rax7;
    unsigned char* r12_8;
    unsigned char* r14_9;
    struct s1* rax10;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rcx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
    rdx5 = (__intrinsic() >> 1) - (rsi >> 63);
    *reinterpret_cast<unsigned char*>(&rcx4) = reinterpret_cast<uint1_t>(rsi != rdx5 + rdx5 * 4);
    r12_6 = reinterpret_cast<unsigned char*>((rdx5 + rcx4) * 8);
    if (rsi < 0 || (*reinterpret_cast<uint32_t*>(&rax7) = __intrinsic(), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0, !!rax7)) {
        *rdx = reinterpret_cast<struct s1*>(0);
        *reinterpret_cast<int32_t*>(&r12_8) = 0;
        *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
    } else {
        r14_9 = r12_6 + 1;
        rax10 = fun_2590(r14_9);
        *rdx = rax10;
        if (!rax10) {
            r12_8 = r14_9;
        } else {
            base32_encode(rdi, rsi, rax10, r14_9);
            return r12_6;
        }
    }
    return r12_8;
}

int64_t fun_46c3(unsigned char dil) {
    int64_t rdi2;
    int64_t rax3;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rdi2) = dil;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi2) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rax3) = reinterpret_cast<int32_t>(~static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(0x9ae0 + rdi2)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rax3) >> 7);
    return rax3;
}

void fun_46e3(int32_t* rdi) {
    __asm__("cli ");
    *rdi = 0;
    return;
}

struct s23 {
    int32_t f0;
    unsigned char f4;
};

int64_t fun_2510(struct s0* rdi, int64_t rsi, int64_t rdx);

int64_t fun_46f3(struct s23* rdi, struct s0* rsi, uint64_t rdx, int64_t rcx, int64_t* r8) {
    struct s23* r14_6;
    struct s0* rbx7;
    uint64_t rsi8;
    void* rsp9;
    int64_t r15_10;
    int64_t* v11;
    unsigned char* rax12;
    unsigned char* v13;
    int64_t rax14;
    int1_t zf15;
    int64_t v16;
    unsigned char v17;
    signed char v18;
    unsigned char v19;
    int32_t edx20;
    uint32_t r13d21;
    uint32_t r13d22;
    int64_t* rbp23;
    int64_t rdx24;
    void* r11_25;
    int64_t* r13_26;
    uint64_t r12_27;
    struct s0* rdi28;
    struct s0* r15_29;
    int64_t rcx30;
    int32_t eax31;
    int64_t rax32;
    uint32_t eax33;
    int32_t edx34;
    void* rdx35;

    __asm__("cli ");
    r14_6 = rdi;
    rbx7 = rsi;
    rsi8 = rdx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    r15_10 = *r8;
    v11 = r8;
    rax12 = g28;
    v13 = rax12;
    *reinterpret_cast<uint32_t*>(&rax14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    zf15 = rdi == 0;
    v16 = r15_10;
    v17 = reinterpret_cast<uint1_t>(!zf15);
    if (zf15) {
        v18 = 1;
        v19 = 1;
    } else {
        edx20 = rdi->f0;
        v19 = reinterpret_cast<uint1_t>(!!rdx);
        *reinterpret_cast<uint32_t*>(&rax14) = v19;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r13d21) = reinterpret_cast<uint1_t>(edx20 == 0);
        r13d22 = r13d21 & *reinterpret_cast<uint32_t*>(&rax14);
        v18 = *reinterpret_cast<signed char*>(&r13d22);
    }
    rbp23 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp9) + 32);
    while (1) {
        rdx24 = r15_10;
        if (v18) {
            r11_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx7) + rsi8);
            r13_26 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp9) + 24);
            while (r12_27 = reinterpret_cast<uint64_t>(r11_25) - reinterpret_cast<uint64_t>(rbx7), rax14 = decode_8(rbx7, r12_27, r13_26, rbp23), rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8), !!*reinterpret_cast<unsigned char*>(&rax14)) {
                r15_10 = v16;
                ++rbx7;
            }
            rdx24 = v16;
            rsi8 = r12_27;
        }
        *reinterpret_cast<unsigned char*>(&rax14) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi8 == 0)) & v19);
        if (*reinterpret_cast<unsigned char*>(&rax14)) 
            break;
        if (rsi8) {
            if (rbx7->f0 == 10) {
                if (!r14_6) {
                    v16 = r15_10;
                    rdi28 = rbx7;
                    r15_29 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx7) + rsi8);
                } else {
                    rbx7 = reinterpret_cast<struct s0*>(&rbx7->f1);
                    --rsi8;
                    r15_10 = rdx24;
                    continue;
                }
            } else {
                v16 = r15_10;
                r15_29 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx7) + rsi8);
                if (r14_6) {
                    rcx30 = r14_6->f0;
                    if (*reinterpret_cast<int32_t*>(&rcx30) == 8) {
                        addr_48c0_18:
                        r14_6->f0 = 0;
                        eax31 = 1;
                        *reinterpret_cast<int32_t*>(&rcx30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx30) + 4) = 0;
                    } else {
                        *reinterpret_cast<unsigned char*>(&eax31) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rcx30) == 0);
                    }
                    if (reinterpret_cast<int64_t>(rsi8) <= reinterpret_cast<int64_t>(7) || (!*reinterpret_cast<unsigned char*>(&eax31) || (rax32 = fun_2510(rbx7, 10, 8), rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8), rcx30 = *reinterpret_cast<int32_t*>(&rcx30), !!rax32))) {
                        if (reinterpret_cast<uint64_t>(r15_29) > reinterpret_cast<uint64_t>(rbx7)) {
                            do {
                                eax33 = rbx7->f0;
                                rbx7 = reinterpret_cast<struct s0*>(&rbx7->f1);
                                if (*reinterpret_cast<signed char*>(&eax33) != 10) {
                                    edx34 = static_cast<int32_t>(rcx30 + 1);
                                    r14_6->f0 = edx34;
                                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_6) + rcx30 + 4) = *reinterpret_cast<signed char*>(&eax33);
                                    if (edx34 == 8) 
                                        goto addr_4937_24;
                                    rcx30 = edx34;
                                }
                            } while (rbx7 != r15_29);
                            goto addr_47f3_27;
                        } else {
                            goto addr_47f3_27;
                        }
                    } else {
                        rdi28 = rbx7;
                        *reinterpret_cast<int32_t*>(&rsi8) = 8;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi8) + 4) = 0;
                        ++rbx7;
                        goto addr_4789_30;
                    }
                } else {
                    rdi28 = rbx7;
                    goto addr_4789_30;
                }
            }
        } else {
            v16 = r15_10;
            if (!r14_6) 
                goto addr_4973_33;
            *reinterpret_cast<int32_t*>(&rcx30) = r14_6->f0;
            r15_29 = rbx7;
            if (*reinterpret_cast<int32_t*>(&rcx30) == 8) 
                goto addr_48c0_18; else 
                goto addr_47f3_27;
        }
        addr_47ff_35:
        if (reinterpret_cast<int64_t>(rsi8) > reinterpret_cast<int64_t>(7)) 
            goto addr_4789_30;
        if (!v19) 
            goto addr_4789_30;
        if (v17) 
            goto addr_481b_38;
        addr_4789_30:
        rax14 = decode_8(rdi28, rsi8, reinterpret_cast<int64_t>(rsp9) + 24, rbp23);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        if (!*reinterpret_cast<unsigned char*>(&rax14)) 
            goto addr_4980_39;
        r15_10 = v16;
        rsi8 = reinterpret_cast<uint64_t>(r15_29) - reinterpret_cast<uint64_t>(rbx7);
        continue;
        addr_47f3_27:
        rsi8 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rcx30)));
        rdi28 = reinterpret_cast<struct s0*>(&r14_6->f4);
        if (!rsi8) 
            goto addr_481b_38; else 
            goto addr_47ff_35;
        addr_4937_24:
        *reinterpret_cast<int32_t*>(&rcx30) = 8;
        goto addr_47f3_27;
    }
    addr_4828_41:
    *v11 = *v11 - rdx24;
    rdx35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rdx35) {
        fun_2470();
    } else {
        return rax14;
    }
    addr_481b_38:
    rdx24 = v16;
    *reinterpret_cast<uint32_t*>(&rax14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    goto addr_4828_41;
    addr_4980_39:
    rdx24 = v16;
    goto addr_4828_41;
    addr_4973_33:
    rdx24 = r15_10;
    *reinterpret_cast<uint32_t*>(&rax14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    goto addr_4828_41;
}

int64_t fun_49c3(int32_t* rdi, unsigned char* rsi, unsigned char* rdx, struct s1** rcx, unsigned char** r8, int64_t r9) {
    unsigned char* rax7;
    void* rax8;
    unsigned char* rdi9;
    struct s1* rax10;
    int64_t* r8_11;
    int64_t rax12;
    struct s1* rdi13;
    void* rdx14;

    __asm__("cli ");
    rax7 = g28;
    rax8 = reinterpret_cast<void*>(reinterpret_cast<signed char>(rdx) >> 3);
    rdi9 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax8) + reinterpret_cast<int64_t>(rax8) * 4 + 5);
    rax10 = fun_2590(rdi9);
    *rcx = rax10;
    if (!rax10) 
        goto addr_4a50_2;
    r8_11 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32 - 8 + 8 + 16);
    rax12 = base32_decode_ctx(rdi, rsi, rdx, rax10, r8_11);
    if (!*reinterpret_cast<unsigned char*>(&rax12)) {
        rdi13 = *rcx;
        fun_2380(rdi13, rsi, rdx, rax10, r8_11, r9);
        *rcx = reinterpret_cast<struct s1*>(0);
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<unsigned char*>(&rax12);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
    } else {
        if (!r8) {
            addr_4a50_2:
            *reinterpret_cast<uint32_t*>(&rax12) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        } else {
            *r8 = rdi9;
        }
    }
    rdx14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx14) {
        fun_2470();
    } else {
        return rax12;
    }
}

struct s24 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    unsigned char f3;
};

struct s25 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
};

void fun_4b93(struct s24* rdi, void* rsi, struct s25* rdx, int64_t rcx) {
    void* rax5;
    void* rax6;
    struct s24* rsi7;
    uint32_t eax8;
    uint32_t ecx9;
    int64_t rcx10;
    uint32_t ecx11;
    int64_t rcx12;
    uint32_t r9d13;
    int64_t rax14;
    uint32_t eax15;
    uint32_t eax16;
    int64_t r9_17;
    int64_t rax18;
    uint32_t eax19;
    int64_t rcx20;
    uint32_t ecx21;
    uint32_t eax22;
    uint32_t r9d23;
    int64_t r9_24;
    uint32_t r9d25;
    struct s24* r9_26;
    struct s24* r11_27;
    struct s24* r10_28;
    uint32_t eax29;
    uint32_t esi30;
    uint32_t ebx31;
    int64_t rax32;
    uint32_t eax33;
    uint32_t esi34;
    uint32_t eax35;
    uint32_t ebx36;
    int64_t rsi37;
    uint32_t esi38;
    int64_t rax39;
    uint32_t eax40;
    uint32_t esi41;
    int64_t rsi42;
    uint32_t esi43;
    int64_t rax44;
    uint32_t eax45;
    struct s25* rax46;
    int64_t rsi47;
    uint32_t eax48;

    __asm__("cli ");
    if (!(*reinterpret_cast<unsigned char*>(&rcx) & 3) && (rax5 = reinterpret_cast<void*>(rcx >> 2), rax6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) + reinterpret_cast<int64_t>(rax5) * 2), rax6 == rsi)) {
        if (rax6) {
            rsi7 = reinterpret_cast<struct s24*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rax6));
            do {
                eax8 = rdi->f0;
                ++rdx;
                rdi = reinterpret_cast<struct s24*>(&rdi->f3);
                ecx9 = eax8;
                *reinterpret_cast<unsigned char*>(&ecx9) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ecx9) >> 2);
                *reinterpret_cast<uint32_t*>(&rcx10) = ecx9 & 63;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
                ecx11 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rcx10);
                (rdx - 1)->f0 = *reinterpret_cast<signed char*>(&ecx11);
                *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi) - 2);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
                r9d13 = *reinterpret_cast<uint32_t*>(&rcx12);
                *reinterpret_cast<unsigned char*>(&r9d13) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r9d13) >> 4);
                *reinterpret_cast<uint32_t*>(&rax14) = (eax8 << 4) + r9d13 & 63;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                eax15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rax14);
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdx) - 3) = *reinterpret_cast<signed char*>(&eax15);
                eax16 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi) - 1);
                *reinterpret_cast<uint32_t*>(&r9_17) = eax16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_17) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax18) = eax16 & 63;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&r9_17) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r9_17) >> 6);
                eax19 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rax18);
                *reinterpret_cast<uint32_t*>(&rcx20) = static_cast<uint32_t>(r9_17 + rcx12 * 4) & 63;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
                ecx21 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rcx20);
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdx) - 2) = *reinterpret_cast<signed char*>(&ecx21);
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdx) - 1) = *reinterpret_cast<signed char*>(&eax19);
            } while (rdi != rsi7);
            return;
        }
    }
    if (!rsi) {
        if (rcx) {
            rdx->f0 = 0;
            goto addr_4ce5_9;
        }
    }
    if (!rcx) {
        addr_4ce5_9:
        return;
    } else {
        eax22 = rdi->f0;
        r9d23 = eax22;
        *reinterpret_cast<unsigned char*>(&r9d23) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r9d23) >> 2);
        *reinterpret_cast<uint32_t*>(&r9_24) = r9d23 & 63;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
        r9d25 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + r9_24);
        rdx->f0 = *reinterpret_cast<signed char*>(&r9d25);
        if (rcx == 1) {
            return;
        }
        r9_26 = reinterpret_cast<struct s24*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 1);
        r11_27 = reinterpret_cast<struct s24*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 3);
        r10_28 = reinterpret_cast<struct s24*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rsi) - 2);
        do {
            eax29 = eax22 << 4;
            if (r9_26 == rdi) 
                break;
            esi30 = rdi->f1;
            ebx31 = esi30;
            *reinterpret_cast<unsigned char*>(&ebx31) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebx31) >> 4);
            *reinterpret_cast<uint32_t*>(&rax32) = eax29 + ebx31 & 63;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
            eax33 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rax32);
            rdx->f1 = *reinterpret_cast<signed char*>(&eax33);
            if (rcx == 2) 
                goto addr_4cba_16;
            esi34 = esi30 << 2;
            if (r10_28 == rdi) 
                goto addr_4cc0_18;
            eax35 = rdi->f2;
            ebx36 = eax35;
            *reinterpret_cast<unsigned char*>(&ebx36) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebx36) >> 6);
            *reinterpret_cast<uint32_t*>(&rsi37) = esi34 + ebx36 & 63;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi37) + 4) = 0;
            esi38 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rsi37);
            rdx->f2 = *reinterpret_cast<signed char*>(&esi38);
            if (rcx == 3) 
                goto addr_4cba_16;
            *reinterpret_cast<uint32_t*>(&rax39) = eax35 & 63;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
            ++rdx;
            eax40 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rax39);
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdx) - 1) = *reinterpret_cast<signed char*>(&eax40);
            rcx = rcx - 4;
            if (!rcx) 
                goto addr_4cba_16;
            if (r11_27 == rdi) 
                goto addr_4cd8_22;
            eax22 = rdi->f3;
            rdi = reinterpret_cast<struct s24*>(&rdi->f3);
            esi41 = eax22;
            *reinterpret_cast<unsigned char*>(&esi41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&esi41) >> 2);
            *reinterpret_cast<uint32_t*>(&rsi42) = esi41 & 63;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi42) + 4) = 0;
            esi43 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rsi42);
            rdx->f0 = *reinterpret_cast<signed char*>(&esi43);
        } while (rcx != 1);
        goto addr_4cba_16;
    }
    *reinterpret_cast<uint32_t*>(&rax44) = eax29 & 48;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
    eax45 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rax44);
    rdx->f1 = *reinterpret_cast<signed char*>(&eax45);
    if (rcx == 2) 
        goto addr_4cba_16;
    rdx->f2 = 61;
    if (rcx == 3) 
        goto addr_4cba_16;
    rdx->f3 = 61;
    rax46 = rdx + 1;
    if (rcx == 4) {
        addr_4cba_16:
        return;
    } else {
        addr_4cb7_27:
        rax46->f0 = 0;
        goto addr_4cba_16;
    }
    addr_4cc0_18:
    *reinterpret_cast<uint32_t*>(&rsi47) = esi34 & 60;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi47) + 4) = 0;
    eax48 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n") + rsi47);
    rdx->f2 = *reinterpret_cast<signed char*>(&eax48);
    if (rcx == 3) {
        return;
    }
    addr_4cd8_22:
    rax46 = rdx;
    goto addr_4cb7_27;
}

unsigned char* fun_4d73(int64_t rdi, int64_t rsi, struct s1** rdx) {
    int64_t rcx4;
    int64_t rdx5;
    unsigned char* r12_6;
    int64_t rax7;
    unsigned char* r12_8;
    unsigned char* r14_9;
    struct s1* rax10;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rcx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
    rdx5 = __intrinsic() - (rsi >> 63);
    *reinterpret_cast<unsigned char*>(&rcx4) = reinterpret_cast<uint1_t>(rsi != rdx5 + rdx5 * 2);
    r12_6 = reinterpret_cast<unsigned char*>((rdx5 + rcx4) * 4);
    if (rsi < 0 || (*reinterpret_cast<uint32_t*>(&rax7) = __intrinsic(), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0, !!rax7)) {
        *rdx = reinterpret_cast<struct s1*>(0);
        *reinterpret_cast<int32_t*>(&r12_8) = 0;
        *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
    } else {
        r14_9 = r12_6 + 1;
        rax10 = fun_2590(r14_9);
        *rdx = rax10;
        if (!rax10) {
            r12_8 = r14_9;
        } else {
            base64_encode(rdi, rsi, rax10, r14_9);
            return r12_6;
        }
    }
    return r12_8;
}

int64_t fun_4e23(unsigned char dil) {
    int64_t rdi2;
    int64_t rax3;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rdi2) = dil;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi2) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rax3) = reinterpret_cast<int32_t>(~static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(0x9be0 + rdi2)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rax3) >> 7);
    return rax3;
}

void fun_4e43(int32_t* rdi) {
    __asm__("cli ");
    *rdi = 0;
    return;
}

struct s26 {
    int32_t f0;
    unsigned char f4;
};

int64_t fun_4e53(struct s26* rdi, struct s0* rsi, uint64_t rdx, int64_t rcx, int64_t* r8) {
    struct s26* r14_6;
    struct s0* rbx7;
    uint64_t rsi8;
    void* rsp9;
    int64_t r15_10;
    int64_t* v11;
    unsigned char* rax12;
    unsigned char* v13;
    int64_t rax14;
    int1_t zf15;
    int64_t v16;
    unsigned char v17;
    signed char v18;
    unsigned char v19;
    int32_t edx20;
    uint32_t r13d21;
    uint32_t r13d22;
    int64_t* rbp23;
    int64_t rdx24;
    struct s0* r12_25;
    int64_t* r13_26;
    void* rbx27;
    struct s0* rdi28;
    struct s0* r15_29;
    int64_t rcx30;
    int32_t eax31;
    int64_t rax32;
    uint32_t eax33;
    int32_t edx34;
    void* rdx35;

    __asm__("cli ");
    r14_6 = rdi;
    rbx7 = rsi;
    rsi8 = rdx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    r15_10 = *r8;
    v11 = r8;
    rax12 = g28;
    v13 = rax12;
    *reinterpret_cast<uint32_t*>(&rax14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    zf15 = rdi == 0;
    v16 = r15_10;
    v17 = reinterpret_cast<uint1_t>(!zf15);
    if (zf15) {
        v18 = 1;
        v19 = 1;
    } else {
        edx20 = rdi->f0;
        v19 = reinterpret_cast<uint1_t>(!!rdx);
        *reinterpret_cast<uint32_t*>(&rax14) = v19;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r13d21) = reinterpret_cast<uint1_t>(edx20 == 0);
        r13d22 = r13d21 & *reinterpret_cast<uint32_t*>(&rax14);
        v18 = *reinterpret_cast<signed char*>(&r13d22);
    }
    rbp23 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp9) + 32);
    while (1) {
        rdx24 = r15_10;
        if (v18) {
            r12_25 = rbx7;
            r13_26 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp9) + 24);
            rbx27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx7) + rsi8);
            while (rsi8 = reinterpret_cast<uint64_t>(rbx27) - reinterpret_cast<uint64_t>(r12_25), rax14 = decode_4(r12_25, rsi8, r13_26, rbp23), rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8), !!*reinterpret_cast<unsigned char*>(&rax14)) {
                r15_10 = v16;
                r12_25 = reinterpret_cast<struct s0*>(&r12_25->f4);
            }
            rdx24 = v16;
            rbx7 = r12_25;
        }
        *reinterpret_cast<unsigned char*>(&rax14) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi8 == 0)) & v19);
        if (*reinterpret_cast<unsigned char*>(&rax14)) 
            break;
        if (rsi8) {
            if (rbx7->f0 == 10) {
                if (!r14_6) {
                    v16 = r15_10;
                    rdi28 = rbx7;
                    r15_29 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx7) + rsi8);
                } else {
                    rbx7 = reinterpret_cast<struct s0*>(&rbx7->f1);
                    --rsi8;
                    r15_10 = rdx24;
                    continue;
                }
            } else {
                v16 = r15_10;
                r15_29 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx7) + rsi8);
                if (r14_6) {
                    rcx30 = r14_6->f0;
                    if (*reinterpret_cast<int32_t*>(&rcx30) == 4) {
                        addr_5020_18:
                        r14_6->f0 = 0;
                        eax31 = 1;
                        *reinterpret_cast<int32_t*>(&rcx30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx30) + 4) = 0;
                    } else {
                        *reinterpret_cast<unsigned char*>(&eax31) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rcx30) == 0);
                    }
                    if (reinterpret_cast<int64_t>(rsi8) <= reinterpret_cast<int64_t>(3) || (!*reinterpret_cast<unsigned char*>(&eax31) || (rax32 = fun_2510(rbx7, 10, 4), rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8), rcx30 = *reinterpret_cast<int32_t*>(&rcx30), !!rax32))) {
                        if (reinterpret_cast<uint64_t>(r15_29) > reinterpret_cast<uint64_t>(rbx7)) {
                            do {
                                eax33 = rbx7->f0;
                                rbx7 = reinterpret_cast<struct s0*>(&rbx7->f1);
                                if (*reinterpret_cast<signed char*>(&eax33) != 10) {
                                    edx34 = static_cast<int32_t>(rcx30 + 1);
                                    r14_6->f0 = edx34;
                                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_6) + rcx30 + 4) = *reinterpret_cast<signed char*>(&eax33);
                                    if (edx34 == 4) 
                                        goto addr_5097_24;
                                    rcx30 = edx34;
                                }
                            } while (rbx7 != r15_29);
                            goto addr_4f53_27;
                        } else {
                            goto addr_4f53_27;
                        }
                    } else {
                        rdi28 = rbx7;
                        *reinterpret_cast<int32_t*>(&rsi8) = 4;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi8) + 4) = 0;
                        rbx7 = reinterpret_cast<struct s0*>(&rbx7->f4);
                        goto addr_4ee9_30;
                    }
                } else {
                    rdi28 = rbx7;
                    goto addr_4ee9_30;
                }
            }
        } else {
            v16 = r15_10;
            if (!r14_6) 
                goto addr_50d3_33;
            *reinterpret_cast<int32_t*>(&rcx30) = r14_6->f0;
            r15_29 = rbx7;
            if (*reinterpret_cast<int32_t*>(&rcx30) == 4) 
                goto addr_5020_18; else 
                goto addr_4f53_27;
        }
        addr_4f5f_35:
        if (reinterpret_cast<int64_t>(rsi8) > reinterpret_cast<int64_t>(3)) 
            goto addr_4ee9_30;
        if (!v19) 
            goto addr_4ee9_30;
        if (v17) 
            goto addr_4f7b_38;
        addr_4ee9_30:
        rax14 = decode_4(rdi28, rsi8, reinterpret_cast<int64_t>(rsp9) + 24, rbp23);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        if (!*reinterpret_cast<unsigned char*>(&rax14)) 
            goto addr_50e0_39;
        r15_10 = v16;
        rsi8 = reinterpret_cast<uint64_t>(r15_29) - reinterpret_cast<uint64_t>(rbx7);
        continue;
        addr_4f53_27:
        rsi8 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rcx30)));
        rdi28 = reinterpret_cast<struct s0*>(&r14_6->f4);
        if (!rsi8) 
            goto addr_4f7b_38; else 
            goto addr_4f5f_35;
        addr_5097_24:
        *reinterpret_cast<int32_t*>(&rcx30) = 4;
        goto addr_4f53_27;
    }
    addr_4f88_41:
    *v11 = *v11 - rdx24;
    rdx35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rdx35) {
        fun_2470();
    } else {
        return rax14;
    }
    addr_4f7b_38:
    rdx24 = v16;
    *reinterpret_cast<uint32_t*>(&rax14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    goto addr_4f88_41;
    addr_50e0_39:
    rdx24 = v16;
    goto addr_4f88_41;
    addr_50d3_33:
    rdx24 = r15_10;
    *reinterpret_cast<uint32_t*>(&rax14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    goto addr_4f88_41;
}

int64_t fun_5123(int32_t* rdi, unsigned char* rsi, unsigned char* rdx, struct s1** rcx, unsigned char** r8, int64_t r9) {
    unsigned char* rax7;
    void* rax8;
    unsigned char* rdi9;
    struct s1* rax10;
    int64_t* r8_11;
    int64_t rax12;
    struct s1* rdi13;
    void* rdx14;

    __asm__("cli ");
    rax7 = g28;
    rax8 = reinterpret_cast<void*>(reinterpret_cast<signed char>(rdx) >> 2);
    rdi9 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax8) + reinterpret_cast<int64_t>(rax8) * 2 + 3);
    rax10 = fun_2590(rdi9);
    *rcx = rax10;
    if (!rax10) 
        goto addr_51b0_2;
    r8_11 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32 - 8 + 8 + 16);
    rax12 = base64_decode_ctx(rdi, rsi, rdx, rax10, r8_11);
    if (!*reinterpret_cast<unsigned char*>(&rax12)) {
        rdi13 = *rcx;
        fun_2380(rdi13, rsi, rdx, rax10, r8_11, r9);
        *rcx = reinterpret_cast<struct s1*>(0);
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<unsigned char*>(&rax12);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
    } else {
        if (!r8) {
            addr_51b0_2:
            *reinterpret_cast<uint32_t*>(&rax12) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        } else {
            *r8 = rdi9;
        }
    }
    rdx14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx14) {
        fun_2470();
    } else {
        return rax12;
    }
}

int64_t file_name = 0;

void fun_51e3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_51f3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(unsigned char* rdi);

unsigned char* quotearg_colon();

int32_t exit_failure = 1;

unsigned char* fun_23d0(int64_t rdi, int64_t rsi, int64_t rdx, unsigned char* rcx, unsigned char* r8);

void fun_5203() {
    unsigned char* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    unsigned char* rdi6;
    int32_t eax7;
    unsigned char* rax8;
    int64_t rdi9;
    unsigned char* rax10;
    int64_t rsi11;
    unsigned char* r8_12;
    unsigned char* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23b0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2440();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_5293_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2620();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23d0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_5293_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2620();
    }
}

void fun_52b3() {
    __asm__("cli ");
}

struct s27 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2580(struct s27* rdi);

void fun_52c3(struct s27* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2580(rdi);
        goto 0x2500;
    }
}

int32_t fun_25c0(struct s27* rdi);

int64_t fun_24d0(int64_t rdi, ...);

int32_t rpl_fflush(struct s27* rdi);

int64_t fun_2420(struct s27* rdi);

int64_t fun_52f3(struct s27* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2580(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25c0(rdi);
        if (!(eax3 && (eax4 = fun_2580(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24d0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23b0();
            r12d9 = *rax8;
            rax10 = fun_2420(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2420;
}

void rpl_fseeko(struct s27* rdi);

void fun_5383(struct s27* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25c0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_53d3(struct s27* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2580(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24d0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_2670(struct s1* rdi, int64_t rsi, int64_t rdx, unsigned char* rcx);

struct s28 {
    signed char[1] pad1;
    unsigned char f1;
    signed char[2] pad4;
    unsigned char f4;
};

struct s28* fun_24c0();

unsigned char* __progname = reinterpret_cast<unsigned char*>(0);

unsigned char* __progname_full = reinterpret_cast<unsigned char*>(0);

void fun_5453(unsigned char* rdi) {
    unsigned char* rcx2;
    unsigned char* rbx3;
    struct s28* rax4;
    unsigned char* r12_5;
    unsigned char* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2670("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23a0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24c0();
        if (rax4 && ((r12_5 = reinterpret_cast<unsigned char*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23c0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (*reinterpret_cast<unsigned char*>(&rax4->f1) != 0x6c || (r12_5[1] != 0x74 || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<unsigned char*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_6bf3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23b0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xe220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_6c33(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_6c53(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe220);
    }
    *rdi = esi;
    return 0xe220;
}

int64_t fun_6c73(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xe220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s29 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_6cb3(struct s29* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s29*>(0xe220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s30 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s30* fun_6cd3(struct s30* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s30*>(0xe220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26ca;
    if (!rdx) 
        goto 0x26ca;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xe220;
}

struct s31 {
    uint32_t f0;
    uint32_t f4;
    unsigned char f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

unsigned char* fun_6d13(struct s1* rdi, unsigned char* rsi, unsigned char* rdx, struct s1* rcx, struct s31* r8) {
    struct s31* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    unsigned char* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s31*>(0xe220);
    }
    rax7 = fun_23b0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x6d46);
    *rax7 = r15d8;
    return rax13;
}

struct s32 {
    uint32_t f0;
    uint32_t f4;
    unsigned char f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s1* fun_6d93(unsigned char* rdi, struct s1* rsi, unsigned char** rdx, struct s32* rcx) {
    struct s32* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    unsigned char* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    unsigned char* rax14;
    unsigned char* rsi15;
    struct s1* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s32*>(0xe220);
    }
    rax6 = fun_23b0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<unsigned char*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6dc1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x6e1c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6e83() {
    __asm__("cli ");
}

struct s1* ge078 = reinterpret_cast<struct s1*>(0xe120);

int64_t slotvec0 = 0x100;

void fun_6e93() {
    uint32_t eax1;
    struct s1* r12_2;
    int64_t rax3;
    struct s1** rbx4;
    struct s1** rbp5;
    struct s1* rdi6;
    unsigned char* rsi7;
    unsigned char* rdx8;
    struct s1* rcx9;
    int64_t* r8_10;
    int64_t r9_11;
    struct s1* rdi12;
    unsigned char* rsi13;
    unsigned char* rdx14;
    struct s1* rcx15;
    int64_t* r8_16;
    int64_t r9_17;
    unsigned char* rsi18;
    unsigned char* rdx19;
    struct s1* rcx20;
    int64_t* r8_21;
    int64_t r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s1**>(reinterpret_cast<uint64_t>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 2;
            fun_2380(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = r12_2->f8;
    if (rdi12 != 0xe120) {
        fun_2380(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        ge078 = reinterpret_cast<struct s1*>(0xe120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xe070) {
        fun_2380(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<struct s1*>(0xe070);
    }
    nslots = 1;
    return;
}

void fun_6f33() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6f53() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6f63(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6f83(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s1* fun_6fa3(struct s1* rdi, int32_t esi, unsigned char* rdx) {
    unsigned char* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26d0;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_7033(struct s1* rdi, int32_t esi, unsigned char* rdx, struct s1* rcx) {
    unsigned char* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26d5;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

struct s1* fun_70c3(int32_t edi, unsigned char* rsi) {
    unsigned char* rax3;
    struct s2* rcx4;
    struct s1* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26da;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2470();
    } else {
        return rax5;
    }
}

struct s1* fun_7153(int32_t edi, unsigned char* rsi, struct s1* rdx) {
    unsigned char* rax4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26df;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_71e3(unsigned char* rdi, struct s1* rsi, uint32_t edx) {
    struct s2* rsp4;
    unsigned char* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s1* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7030]");
    __asm__("movdqa xmm1, [rip+0x7038]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x7021]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2470();
    } else {
        return rax10;
    }
}

struct s1* fun_7283(unsigned char* rdi, uint32_t esi) {
    struct s2* rsp3;
    unsigned char* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6f90]");
    __asm__("movdqa xmm1, [rip+0x6f98]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6f81]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2470();
    } else {
        return rax9;
    }
}

struct s1* fun_7323(unsigned char* rdi) {
    unsigned char* rax2;
    struct s1* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6ef0]");
    __asm__("movdqa xmm1, [rip+0x6ef8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6ed9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2470();
    } else {
        return rax3;
    }
}

struct s1* fun_73b3(unsigned char* rdi, struct s1* rsi) {
    unsigned char* rax3;
    struct s1* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6e60]");
    __asm__("movdqa xmm1, [rip+0x6e68]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6e56]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2470();
    } else {
        return rax4;
    }
}

struct s1* fun_7443(struct s1* rdi, int32_t esi, unsigned char* rdx) {
    unsigned char* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26e4;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_74e3(struct s1* rdi, int64_t rsi, int64_t rdx, unsigned char* rcx) {
    unsigned char* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6d2a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6d22]");
    __asm__("movdqa xmm2, [rip+0x6d2a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26e9;
    if (!rdx) 
        goto 0x26e9;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

struct s1* fun_7583(int32_t edi, int64_t rsi, int64_t rdx, unsigned char* rcx, struct s1* r8) {
    unsigned char* rcx6;
    struct s2* rcx7;
    struct s1* rdi8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6c8a]");
    __asm__("movdqa xmm1, [rip+0x6c92]");
    __asm__("movdqa xmm2, [rip+0x6c9a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ee;
    if (!rdx) 
        goto 0x26ee;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2470();
    } else {
        return rax9;
    }
}

struct s1* fun_7633(int64_t rdi, int64_t rsi, unsigned char* rdx) {
    unsigned char* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6bda]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6bd2]");
    __asm__("movdqa xmm2, [rip+0x6bda]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26f3;
    if (!rsi) 
        goto 0x26f3;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_76d3(int64_t rdi, int64_t rsi, unsigned char* rdx, struct s1* rcx) {
    unsigned char* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6b3a]");
    __asm__("movdqa xmm1, [rip+0x6b42]");
    __asm__("movdqa xmm2, [rip+0x6b4a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26f8;
    if (!rsi) 
        goto 0x26f8;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

void fun_7773() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7783(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_77a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_77c3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s33 {
    unsigned char* f0;
    signed char[7] pad8;
    unsigned char* f8;
    signed char[7] pad16;
    unsigned char* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2560(int64_t rdi, unsigned char* rsi, unsigned char* rdx, unsigned char* rcx, unsigned char* r8, unsigned char* r9);

void fun_77e3(unsigned char* rdi, unsigned char* rsi, unsigned char* rdx, unsigned char* rcx, struct s33* r8, unsigned char* r9) {
    unsigned char* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    unsigned char* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    unsigned char* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    unsigned char* r14_40;
    unsigned char* r13_41;
    unsigned char* r12_42;
    unsigned char* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2680(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2680(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2440();
    fun_2680(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2560(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2440();
    fun_2680(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2560(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2440();
        fun_2680(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xa448 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xa448;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_7c53() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s34 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_7c73(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s34* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s34* rcx8;
    unsigned char* rax9;
    unsigned char* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2470();
    } else {
        return;
    }
}

void fun_7d13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    unsigned char* rax15;
    unsigned char* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_7db6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7dc0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2470();
    } else {
        return;
    }
    addr_7db6_5:
    goto addr_7dc0_7;
}

void fun_7df3() {
    unsigned char* rsi1;
    unsigned char* rdx2;
    unsigned char* rcx3;
    unsigned char* r8_4;
    unsigned char* r9_5;
    unsigned char* rax6;
    unsigned char* rcx7;
    unsigned char* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2560(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2440();
    fun_2600(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2440();
    fun_2600(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2440();
    goto fun_2600;
}

int64_t fun_23f0();

void xalloc_die();

void fun_7e93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7ed3(unsigned char* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7ef3(unsigned char* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7f13(unsigned char* rdi) {
    struct s1* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25e0();

void fun_7f33(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25e0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7f63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7f93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7fd3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8013(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8043(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8093(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23f0();
            if (rax5) 
                break;
            addr_80dd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_80dd_5;
        rax8 = fun_23f0();
        if (rax8) 
            goto addr_80c6_9;
        if (rbx4) 
            goto addr_80dd_5;
        addr_80c6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_8123(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23f0();
            if (rax8) 
                break;
            addr_816a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_816a_5;
        rax11 = fun_23f0();
        if (rax11) 
            goto addr_8152_9;
        if (!rbx6) 
            goto addr_8152_9;
        if (r12_4) 
            goto addr_816a_5;
        addr_8152_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_81b3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_825d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_8270_10:
                *r12_8 = 0;
            }
            addr_8210_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_8236_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_8284_14;
            if (rcx10 <= rsi9) 
                goto addr_822d_16;
            if (rsi9 >= 0) 
                goto addr_8284_14;
            addr_822d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_8284_14;
            addr_8236_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25e0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_8284_14;
            if (!rbp13) 
                break;
            addr_8284_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_825d_9;
        } else {
            if (!r13_6) 
                goto addr_8270_10;
            goto addr_8210_11;
        }
    }
}

int64_t fun_2540();

void fun_82b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_82e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8313() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8333() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8353(int64_t rdi, unsigned char* rsi) {
    struct s1* rax3;

    __asm__("cli ");
    rax3 = fun_2590(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

void fun_8393(int64_t rdi, unsigned char* rsi) {
    struct s1* rax3;

    __asm__("cli ");
    rax3 = fun_2590(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

struct s35 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_83d3(int64_t rdi, struct s35* rsi) {
    struct s1* rax3;

    __asm__("cli ");
    rax3 = fun_2590(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_2570;
    }
}

unsigned char* fun_2460(unsigned char* rdi, ...);

void fun_8413(unsigned char* rdi) {
    unsigned char* rax2;
    struct s1* rax3;

    __asm__("cli ");
    rax2 = fun_2460(rdi);
    rax3 = fun_2590(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

void fun_8453() {
    struct s1* rdi1;

    __asm__("cli ");
    fun_2440();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    fun_2620();
    fun_23a0(rdi1);
}

int64_t fun_2390(struct s15* rdi);

int64_t fun_8493(struct s15* rdi, struct s15** rsi, uint32_t edx, int64_t* rcx, int64_t r8) {
    int64_t* v6;
    unsigned char* rax7;
    unsigned char* v8;
    struct s15* rdx9;
    struct s15** rsi10;
    int64_t rax11;
    int64_t rbx12;
    int64_t r10_13;
    uint32_t r12d14;
    struct s15* rax15;
    int64_t r9_16;
    struct s15** rbp17;
    void* rax18;
    int64_t rax19;
    int64_t r14_20;
    int32_t* rax21;
    int64_t rax22;
    int64_t r13_23;
    int64_t rax24;
    int64_t rax25;
    uint32_t eax26;
    int64_t r9_27;
    uint32_t r13d28;
    int64_t r13_29;
    int64_t rax30;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (edx > 36) {
        *reinterpret_cast<uint32_t*>(&rdx9) = 85;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
        rsi10 = reinterpret_cast<struct s15**>("lib/xstrtol.c");
        fun_24e0("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoimax");
        do {
            rax11 = fun_2470();
            while (1) {
                addr_89c9_4:
                if (rbx12 < 0) {
                    rbx12 = r10_13;
                } else {
                    rbx12 = r8;
                }
                while (*reinterpret_cast<int32_t*>(&rsi10) = *reinterpret_cast<int32_t*>(&rsi10) - 1, !!*reinterpret_cast<int32_t*>(&rsi10)) {
                    if (__intrinsic()) 
                        goto addr_89c9_4;
                    rbx12 = rbx12 * rax11;
                }
                break;
            }
            r12d14 = r12d14 | 1;
            rax15 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(rdx9) + r9_16);
            *reinterpret_cast<uint32_t*>(&rdx9) = r12d14 | 2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
            *rbp17 = rax15;
            if (rax15->f0) {
                r12d14 = *reinterpret_cast<uint32_t*>(&rdx9);
            }
            addr_851b_15:
            *v6 = rbx12;
            addr_8522_16:
            rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax18);
        *reinterpret_cast<uint32_t*>(&rax19) = r12d14;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
        return rax19;
    }
    rbp17 = rsi;
    if (!rsi) {
        rbp17 = reinterpret_cast<struct s15**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r14_20 = r8;
    rax21 = fun_23b0();
    *rax21 = 0;
    rsi10 = rbp17;
    rax22 = fun_2390(rdi);
    rdx9 = *rbp17;
    rbx12 = rax22;
    if (rdx9 != rdi) 
        goto addr_84ff_21;
    if (!r14_20) {
        addr_8690_23:
        r12d14 = 4;
        goto addr_8522_16;
    } else {
        *reinterpret_cast<uint32_t*>(&r13_23) = rdx9->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_23) + 4) = 0;
        r12d14 = 4;
        if (!*reinterpret_cast<signed char*>(&r13_23)) 
            goto addr_8522_16;
        *reinterpret_cast<int32_t*>(&rsi10) = *reinterpret_cast<signed char*>(&r13_23);
        r12d14 = 0;
        *reinterpret_cast<int32_t*>(&rbx12) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx12) + 4) = 0;
        rax24 = fun_24a0(r14_20);
        rdx9 = rdx9;
        if (!rax24) 
            goto addr_8690_23;
    }
    addr_85a7_26:
    *reinterpret_cast<uint32_t*>(&r8) = static_cast<uint32_t>(r13_23 - 69);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r9_16) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0;
    if (*reinterpret_cast<unsigned char*>(&r8) <= 47 && (static_cast<int1_t>(0x814400308945 >> r8) && (*reinterpret_cast<int32_t*>(&rsi10) = 48, rax25 = fun_24a0(r14_20), rdx9 = rdx9, *reinterpret_cast<int32_t*>(&r9_16) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0, !!rax25))) {
        eax26 = rdx9->f1;
        if (*reinterpret_cast<signed char*>(&eax26) == 68) {
            *reinterpret_cast<int32_t*>(&r9_16) = 2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0;
        } else {
            if (*reinterpret_cast<signed char*>(&eax26) == 0x69) {
                *reinterpret_cast<int32_t*>(&r9_27) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_27) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&r9_27) = reinterpret_cast<uint1_t>(rdx9->f2 == 66);
                *reinterpret_cast<int32_t*>(&r9_16) = static_cast<int32_t>(r9_27 + r9_27 + 1);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0;
            } else {
                *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<unsigned char*>(&r8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&eax26) != 66) {
                    goto *reinterpret_cast<int32_t*>(0xa5d0 + r8 * 4) + 0xa5d0;
                }
            }
        }
    }
    r13d28 = *reinterpret_cast<uint32_t*>(&r13_23) - 66;
    if (*reinterpret_cast<unsigned char*>(&r13d28) <= 53) {
        *reinterpret_cast<uint32_t*>(&r13_29) = *reinterpret_cast<unsigned char*>(&r13d28);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_29) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa4f8 + r13_29 * 4) + 0xa4f8;
    }
    addr_860b_35:
    r12d14 = r12d14 | 2;
    *v6 = rbx12;
    goto addr_8522_16;
    addr_84ff_21:
    if (*rax21) {
        r12d14 = 4;
        if (*rax21 != 34) 
            goto addr_8522_16;
        r12d14 = 1;
    } else {
        r12d14 = 0;
    }
    if (!r14_20) 
        goto addr_851b_15;
    *reinterpret_cast<uint32_t*>(&r13_23) = rdx9->f0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_23) + 4) = 0;
    if (!*reinterpret_cast<signed char*>(&r13_23)) 
        goto addr_851b_15;
    *reinterpret_cast<int32_t*>(&rsi10) = *reinterpret_cast<signed char*>(&r13_23);
    rax30 = fun_24a0(r14_20);
    rdx9 = rdx9;
    if (rax30) 
        goto addr_85a7_26; else 
        goto addr_860b_35;
}

int64_t fun_23e0();

int64_t fun_8a93(unsigned char* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23e0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_8aee_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23b0();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_8aee_3;
            rax6 = fun_23b0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

signed char* fun_25b0(int64_t rdi);

signed char* fun_8b03() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25b0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2490(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_8b43(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    unsigned char* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2490(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2470();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_8bd3() {
    unsigned char* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax3;
    }
}

int64_t fun_8c53(int64_t rdi, unsigned char* rsi, unsigned char* rdx) {
    unsigned char* rax4;
    int32_t r13d5;
    unsigned char* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25f0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2460(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2570(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2570(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_8d03() {
    __asm__("cli ");
    goto fun_25f0;
}

void fun_8d13() {
    __asm__("cli ");
}

void fun_8d27() {
    __asm__("cli ");
    return;
}

int64_t base_decode_ctx = 0;

int32_t optind = 0;

int64_t base_decode_ctx_init = 0;

int64_t base_length = 0;

int64_t isbase = 0;

unsigned char* base_encode = reinterpret_cast<unsigned char*>(0);

unsigned char* fun_2640(struct s4* rdi, int64_t rsi);

unsigned char* fun_25d0(unsigned char* rdi, uint64_t rsi, unsigned char* rdx, unsigned char* rcx, void* r8);

int32_t fun_24b0(unsigned char* rdi, int64_t rsi, unsigned char* rdx, unsigned char* rcx, void* r8);

unsigned char* stdin = reinterpret_cast<unsigned char*>(0);

void fadvise(unsigned char* rdi, uint64_t rsi, unsigned char* rdx, unsigned char* rcx, void* r8);

unsigned char* xmalloc(int64_t rdi, uint64_t rsi, unsigned char* rdx, unsigned char* rcx, void* r8);

unsigned char* fun_2400(void* rdi, uint64_t rsi);

void fun_2610(signed char* rdi, uint64_t rsi, unsigned char* rdx, unsigned char* rcx, void* r8);

void fun_28b8() {
    void* rsp1;
    unsigned char* rcx2;
    int64_t rax3;
    int32_t edx4;
    int32_t ebp5;
    unsigned char* rdx6;
    int32_t ebp7;
    struct s4* rdi8;
    struct s4** rbx9;
    struct s4* v10;
    int32_t eax11;
    unsigned char* rax12;
    unsigned char* r12_13;
    unsigned char* r10_14;
    int64_t rdi15;
    unsigned char* r14_16;
    uint64_t rsi17;
    void* r8_18;
    int32_t eax19;
    unsigned char* v20;
    unsigned char* r13_21;
    int64_t rdi22;
    int32_t eax23;
    unsigned char** rsp24;
    signed char* v25;
    unsigned char* r10_26;
    unsigned char* rax27;
    unsigned char* r15_28;
    unsigned char eax29;
    unsigned char* v30;
    signed char* r12_31;
    signed char* rbx32;
    unsigned char* v33;
    unsigned char* rbp34;
    unsigned char* v35;
    unsigned char* r15_36;
    unsigned char* rax37;
    unsigned char* rax38;
    int32_t eax39;
    unsigned char* rax40;
    unsigned char* rax41;
    unsigned char* rax42;
    int32_t* rax43;
    unsigned char* v44;
    unsigned char eax45;
    signed char* rdx46;
    int64_t rax47;
    int32_t eax48;
    unsigned char* v49;
    uint64_t rsi50;
    signed char* r15_51;
    unsigned char* rax52;
    unsigned char** rsp53;
    unsigned char* rax54;
    int32_t eax55;
    int64_t rdi56;
    unsigned char* rax57;
    unsigned char* rax58;
    void* rsp59;
    unsigned char* rax60;
    int32_t eax61;
    unsigned char* rax62;
    unsigned char* rax63;
    void* rsp64;
    signed char* rax65;
    int32_t eax66;
    unsigned char* rax67;
    unsigned char* rbp68;
    int64_t rdi69;
    signed char al70;
    int32_t eax71;
    unsigned char eax72;
    int32_t v73;
    unsigned char* rax74;
    int32_t eax75;
    unsigned char* rax76;
    unsigned char* rax77;
    unsigned char* rax78;

    rsp1 = __zero_stack_offset();
    rcx2 = reinterpret_cast<unsigned char*>(0x3310);
    base_decode_ctx = 0x39d0;
    rax3 = optind;
    base_decode_ctx_init = 0x35a0;
    edx4 = ebp5 - *reinterpret_cast<int32_t*>(&rax3);
    base_length = 0x32b0;
    *reinterpret_cast<int32_t*>(&rdx6) = edx4 - 1;
    *reinterpret_cast<int32_t*>(&rdx6 + 4) = 0;
    isbase = 0x32a0;
    base_encode = reinterpret_cast<unsigned char*>(0x3310);
    if (edx4 > 1) 
        goto addr_2c59_3;
    if (*reinterpret_cast<int32_t*>(&rax3) >= ebp7) 
        goto addr_2c94_5;
    rdi8 = rbx9[rax3];
    v10 = rbx9[rax3];
    eax11 = fun_2550(rdi8, "-");
    rsp1 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8);
    if (!eax11) 
        goto addr_2ca0_7;
    rax12 = fun_2640(v10, "rb");
    rsp1 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8);
    r12_13 = rax12;
    if (rax12) 
        goto addr_2957_9;
    addr_300d_10:
    quotearg_n_style_colon();
    fun_23b0();
    fun_2620();
    addr_2fe1_11:
    fun_2440();
    fun_23b0();
    fun_2620();
    goto addr_300d_10;
    addr_2edb_12:
    fun_2440();
    fun_23b0();
    fun_2620();
    while (1) {
        addr_2d62_13:
        r10_14 = base_encode;
        *reinterpret_cast<int32_t*>(&rdi15) = *reinterpret_cast<int32_t*>(&r14_16);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        eax19 = reinterpret_cast<int32_t>(base_length(rdi15, rsi17, rdx6, rcx2, r8_18));
        rdx6 = v20;
        rcx2 = reinterpret_cast<unsigned char*>(static_cast<int64_t>(eax19));
        r10_14(r13_21, r14_16, rdx6, rcx2, r8_18);
        *reinterpret_cast<int32_t*>(&rdi22) = *reinterpret_cast<int32_t*>(&r14_16);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
        eax23 = reinterpret_cast<int32_t>(base_length(rdi22, r14_16, rdx6, rcx2, r8_18));
        rsp24 = rsp24 - 8 + 8 - 8 + 8 - 8 + 8;
        if (!v25) {
            addr_2ead_14:
            r10_26 = reinterpret_cast<unsigned char*>(static_cast<int64_t>(eax23));
            rcx2 = stdout;
            rdx6 = r10_26;
            rax27 = fun_25d0(v20, 1, rdx6, rcx2, r8_18);
            rsp24 = rsp24 - 8 + 8;
            if (reinterpret_cast<unsigned char>(r10_26) <= reinterpret_cast<unsigned char>(rax27)) 
                goto addr_2e4d_15; else 
                goto addr_2edb_12;
        } else {
            r15_28 = reinterpret_cast<unsigned char*>(static_cast<int64_t>(eax23));
            if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r15_28) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r15_28 == 0)) {
                addr_2e4d_15:
                eax29 = *r12_13;
                if (eax29 & 48) 
                    goto addr_2d1b_17;
            } else {
                v30 = r12_13;
                r12_31 = rbx32;
                rbx32 = v25;
                v33 = r13_21;
                r13_21 = rbp34;
                rbp34 = r15_28;
                v35 = r14_16;
                r14_16 = reinterpret_cast<unsigned char*>(0);
                do {
                    r15_36 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp34) - reinterpret_cast<unsigned char>(r14_16));
                    rax37 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbx32) - reinterpret_cast<uint64_t>(r12_31));
                    if (reinterpret_cast<signed char>(r15_36) > reinterpret_cast<signed char>(rax37)) {
                        r15_36 = rax37;
                    }
                    if (!r15_36) {
                        rax38 = *reinterpret_cast<unsigned char**>(r13_21 + 40);
                        if (reinterpret_cast<unsigned char>(rax38) >= reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char**>(r13_21 + 48))) {
                            eax39 = fun_24b0(r13_21, 10, rdx6, rcx2, r8_18);
                            rsp24 = rsp24 - 8 + 8;
                            if (!(eax39 + 1)) 
                                goto addr_2e81_24;
                        } else {
                            rdx6 = rax38 + 1;
                            *reinterpret_cast<unsigned char**>(r13_21 + 40) = rdx6;
                            *rax38 = 10;
                        }
                        *reinterpret_cast<int32_t*>(&r12_31) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_31) + 4) = 0;
                    } else {
                        rcx2 = stdout;
                        rdx6 = r15_36;
                        rax40 = fun_25d0(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(r14_16), 1, rdx6, rcx2, r8_18);
                        rsp24 = rsp24 - 8 + 8;
                        if (reinterpret_cast<unsigned char>(r15_36) > reinterpret_cast<unsigned char>(rax40)) 
                            goto 0x307b;
                        r12_31 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r12_31) + reinterpret_cast<unsigned char>(r15_36));
                        r14_16 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r14_16) + reinterpret_cast<unsigned char>(r15_36));
                    }
                } while (reinterpret_cast<signed char>(r14_16) < reinterpret_cast<signed char>(rbp34));
                goto addr_2e38_30;
            }
        }
        if (r14_16 == 0x7800) 
            goto addr_2cde_32;
        goto addr_2d1b_17;
        addr_2e81_24:
        rax41 = fun_2440();
        r12_13 = rax41;
        fun_23b0();
        eax23 = fun_2620();
        rsp24 = rsp24 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_2ead_14;
        addr_2e38_30:
        rbx32 = r12_31;
        rbp34 = r13_21;
        r12_13 = v30;
        r13_21 = v33;
        r14_16 = v35;
        goto addr_2e4d_15;
        addr_2d36_34:
        rax42 = fun_2440();
        r12_13 = rax42;
        rax43 = fun_23b0();
        rdx6 = r12_13;
        *reinterpret_cast<int32_t*>(&rsi17) = *rax43;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        fun_2620();
        rsp24 = rsp24 - 8 + 8 - 8 + 8 - 8 + 8;
        continue;
        while (1) {
            addr_2b9d_35:
            r13_21 = v20;
            r14_16 = v44;
            eax45 = *r12_13;
            while (1) {
                if (eax45 & 16) 
                    goto addr_2c1b_37;
                if (*reinterpret_cast<uint32_t*>(&rbp34)) 
                    goto addr_29b8_39;
                while (1) {
                    addr_2bb8_40:
                    *reinterpret_cast<int32_t*>(&rdx46) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx46) + 4) = 0;
                    rax47 = base_decode_ctx;
                    if (!*reinterpret_cast<uint32_t*>(&rbp34)) {
                        rdx46 = rbx32;
                    }
                    while (1) {
                        r8_18 = reinterpret_cast<void*>(rsp24 + 56);
                        eax48 = reinterpret_cast<int32_t>(rax47(v25, r13_21, rdx46, r14_16, r8_18));
                        rcx2 = v49;
                        rdx6 = reinterpret_cast<unsigned char*>(0x1068);
                        *reinterpret_cast<int32_t*>(&rsi50) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r15_51) = eax48;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_51) + 4) = 0;
                        rax52 = fun_25d0(r14_16, 1, 0x1068, rcx2, r8_18);
                        rsp24 = rsp24 - 8 + 8 - 8 + 8;
                        if (reinterpret_cast<unsigned char>(rax52) >= reinterpret_cast<unsigned char>(0x1068)) {
                            if (*reinterpret_cast<signed char*>(&r15_51)) 
                                goto addr_2c10_44;
                            fun_2440();
                            fun_2620();
                            rsp24 = rsp24 - 8 + 8 - 8 + 8;
                        }
                        fun_2440();
                        fun_23b0();
                        fun_2620();
                        rsp53 = rsp24 - 8 + 8 - 8 + 8 - 8 + 8;
                        while (1) {
                            addr_2f9f_47:
                            rax54 = fun_2440();
                            r12_13 = rax54;
                            fun_23b0();
                            rdx6 = r12_13;
                            fun_2620();
                            rsp24 = rsp53 - 8 + 8 - 8 + 8 - 8 + 8;
                            while (1) {
                                eax55 = fun_24b0(rbp34, 10, rdx6, rcx2, r8_18);
                                rsp24 = rsp24 - 8 + 8;
                                if (!(eax55 + 1)) 
                                    goto addr_2fe1_11;
                                while (1) {
                                    eax29 = *r12_13;
                                    do {
                                        if (eax29 & 32) 
                                            goto addr_2d36_34;
                                        while (1) {
                                            rax3 = finish_and_exit(r12_13, v10, rdx6, rcx2, r8_18);
                                            rsp1 = reinterpret_cast<void*>(rsp24 - 8 + 8);
                                            addr_2c59_3:
                                            rdi56 = *reinterpret_cast<int64_t*>(rbx32 + rax3 * 8 + 8);
                                            rax57 = quote(rdi56, rdi56);
                                            rax58 = fun_2440();
                                            rcx2 = rax57;
                                            rdx6 = rax58;
                                            fun_2620();
                                            usage();
                                            rsp1 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                            addr_2c94_5:
                                            v10 = reinterpret_cast<struct s4*>("-");
                                            addr_2ca0_7:
                                            r12_13 = stdin;
                                            addr_2957_9:
                                            *reinterpret_cast<int32_t*>(&rsi50) = 2;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
                                            fadvise(r12_13, 2, rdx6, rcx2, r8_18);
                                            rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8);
                                            if (!*reinterpret_cast<signed char*>(&r14_16)) 
                                                break;
                                            rax60 = stdout;
                                            v49 = rax60;
                                            eax61 = reinterpret_cast<int32_t>(base_length(0x1068, 2, rdx6, rcx2, r8_18));
                                            rax62 = xmalloc(static_cast<int64_t>(eax61), 2, rdx6, rcx2, r8_18);
                                            r13_21 = rax62;
                                            rax63 = xmalloc(0x1068, 2, rdx6, rcx2, r8_18);
                                            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8);
                                            r14_16 = rax63;
                                            rax65 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp64) + 64);
                                            v25 = rax65;
                                            base_decode_ctx_init(rax65, 2, rdx6, rcx2, r8_18);
                                            rsp24 = reinterpret_cast<unsigned char**>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
                                            addr_29b8_39:
                                            v20 = r13_21;
                                            *reinterpret_cast<int32_t*>(&rbx32) = 0;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx32) + 4) = 0;
                                            v44 = r14_16;
                                            do {
                                                eax66 = reinterpret_cast<int32_t>(base_length(0x1068, rsi50, rdx6, rcx2, r8_18));
                                                rcx2 = r12_13;
                                                *reinterpret_cast<int32_t*>(&rsi50) = 1;
                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
                                                rdx6 = reinterpret_cast<unsigned char*>(eax66 - reinterpret_cast<uint64_t>(rbx32));
                                                rax67 = fun_2400(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<uint64_t>(rbx32), 1);
                                                rsp53 = rsp24 - 8 + 8 - 8 + 8;
                                                rbp68 = rax67;
                                                if (*reinterpret_cast<signed char*>(&v33) && !(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax67) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax67 == 0))) {
                                                    *reinterpret_cast<int32_t*>(&r14_16) = 0;
                                                    *reinterpret_cast<int32_t*>(&r14_16 + 4) = 0;
                                                    do {
                                                        r13_21 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbx32) + reinterpret_cast<unsigned char>(r14_16));
                                                        r15_51 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(r13_21));
                                                        *reinterpret_cast<int32_t*>(&rdi69) = *r15_51;
                                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi69) + 4) = 0;
                                                        al70 = reinterpret_cast<signed char>(isbase(rdi69, rsi50, rdx6, rcx2, r8_18));
                                                        rsp53 = rsp53 - 8 + 8;
                                                        if (al70 || *r15_51 == 61) {
                                                            ++r14_16;
                                                        } else {
                                                            --rbp68;
                                                            rsi50 = reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(r13_21) + 1;
                                                            rdx6 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp68) - reinterpret_cast<unsigned char>(r14_16));
                                                            fun_2610(r15_51, rsi50, rdx6, rcx2, r8_18);
                                                            rsp53 = rsp53 - 8 + 8;
                                                        }
                                                    } while (reinterpret_cast<signed char>(rbp68) > reinterpret_cast<signed char>(r14_16));
                                                }
                                                rbx32 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbx32) + reinterpret_cast<unsigned char>(rbp68));
                                                *reinterpret_cast<uint32_t*>(&rbp34) = *r12_13 & 32;
                                                *reinterpret_cast<int32_t*>(&rbp34 + 4) = 0;
                                                if (*reinterpret_cast<uint32_t*>(&rbp34)) 
                                                    goto addr_2f9f_47;
                                                eax71 = reinterpret_cast<int32_t>(base_length(0x1068, rsi50, rdx6, rcx2, r8_18));
                                                rsp24 = rsp53 - 8 + 8;
                                                if (reinterpret_cast<int64_t>(rbx32) >= static_cast<int64_t>(eax71)) 
                                                    goto addr_2b9d_35;
                                                eax72 = *r12_13;
                                            } while (!(eax72 & 16));
                                            r13_21 = v20;
                                            r14_16 = v44;
                                            addr_2c1b_37:
                                            if (*reinterpret_cast<uint32_t*>(&rbp34) == 2) 
                                                continue;
                                            if (*reinterpret_cast<uint32_t*>(&rbp34) != 1) 
                                                goto addr_2bb8_40;
                                            if (v73) 
                                                goto addr_2c2d_65;
                                        }
                                        rbp34 = stdout;
                                        *reinterpret_cast<int32_t*>(&rbx32) = 0;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx32) + 4) = 0;
                                        rax74 = xmalloc(0x7800, 2, rdx6, rcx2, r8_18);
                                        r13_21 = rax74;
                                        eax75 = reinterpret_cast<int32_t>(base_length(0x7800, 2, rdx6, rcx2, r8_18));
                                        rax76 = xmalloc(static_cast<int64_t>(eax75), 2, rdx6, rcx2, r8_18);
                                        rsp24 = reinterpret_cast<unsigned char**>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8);
                                        v25 = r15_51;
                                        v20 = rax76;
                                        addr_2cde_32:
                                        *reinterpret_cast<int32_t*>(&r14_16) = 0;
                                        *reinterpret_cast<int32_t*>(&r14_16 + 4) = 0;
                                        while (rcx2 = r12_13, *reinterpret_cast<int32_t*>(&rsi17) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0, rdx6 = reinterpret_cast<unsigned char*>(0x7800 - reinterpret_cast<unsigned char>(r14_16)), rax77 = fun_2400(reinterpret_cast<unsigned char>(r13_21) + reinterpret_cast<unsigned char>(r14_16), 1), rsp24 = rsp24 - 8 + 8, r14_16 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r14_16) + reinterpret_cast<unsigned char>(rax77)), eax29 = *r12_13, (eax29 & 48) == 0) {
                                            if (reinterpret_cast<signed char>(r14_16) > reinterpret_cast<signed char>(0x77ff)) 
                                                goto addr_2d62_13;
                                        }
                                        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r14_16) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r14_16 == 0))) 
                                            goto addr_2d62_13;
                                        addr_2d1b_17:
                                        r15_51 = v25;
                                    } while (reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rbx32) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rbx32 == 0) || !r15_51);
                                    rax78 = *reinterpret_cast<unsigned char**>(rbp34 + 40);
                                    if (reinterpret_cast<unsigned char>(rax78) >= reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char**>(rbp34 + 48))) 
                                        break;
                                    rdx6 = rax78 + 1;
                                    *reinterpret_cast<unsigned char**>(rbp34 + 40) = rdx6;
                                    *rax78 = 10;
                                }
                            }
                        }
                        addr_2c2d_65:
                        rax47 = base_decode_ctx;
                        *reinterpret_cast<int32_t*>(&rdx46) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx46) + 4) = 0;
                    }
                }
                addr_2c10_44:
                eax45 = *r12_13;
                *reinterpret_cast<uint32_t*>(&rbp34) = *reinterpret_cast<uint32_t*>(&rbp34) + 1;
                *reinterpret_cast<int32_t*>(&rbp34 + 4) = 0;
            }
        }
    }
}

void fun_2a85() {
    goto 0x28db;
}

uint32_t fun_2520(unsigned char* rdi, unsigned char* rsi, unsigned char* rdx, unsigned char* rcx);

unsigned char* rpl_mbrtowc(void* rdi, unsigned char* rsi);

int32_t fun_26a0(int64_t rdi, unsigned char* rsi);

uint32_t fun_2690(unsigned char* rdi, unsigned char* rsi);

void** fun_26b0(unsigned char* rdi, unsigned char* rsi, unsigned char* rdx, unsigned char* rcx);

void fun_5685() {
    unsigned char** rsp1;
    int32_t ebp2;
    unsigned char* rax3;
    unsigned char** rsp4;
    unsigned char* r11_5;
    unsigned char* r11_6;
    unsigned char* v7;
    int32_t ebp8;
    unsigned char* rax9;
    unsigned char* rdx10;
    unsigned char* rax11;
    unsigned char* r11_12;
    unsigned char* v13;
    int32_t ebp14;
    unsigned char* rax15;
    unsigned char* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    unsigned char* r13_19;
    void* r14_20;
    signed char* r12_21;
    unsigned char* v22;
    int32_t ebx23;
    unsigned char* rax24;
    unsigned char** rsp25;
    unsigned char* v26;
    unsigned char* r11_27;
    unsigned char* v28;
    unsigned char* v29;
    unsigned char* rsi30;
    unsigned char* v31;
    unsigned char* v32;
    unsigned char* r10_33;
    unsigned char* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    unsigned char* r9_37;
    unsigned char* v38;
    unsigned char* rdi39;
    unsigned char* v40;
    unsigned char* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    unsigned char* rcx44;
    unsigned char al45;
    unsigned char* v46;
    int64_t v47;
    unsigned char* v48;
    unsigned char* v49;
    unsigned char* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    unsigned char* v58;
    unsigned char v59;
    unsigned char* v60;
    unsigned char* v61;
    unsigned char* v62;
    signed char* v63;
    unsigned char* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    unsigned char* r13_69;
    unsigned char* rsi70;
    void* v71;
    unsigned char* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    unsigned char* v107;
    unsigned char* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<unsigned char**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2440();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2440();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *rdx10, !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<unsigned char*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2460(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<unsigned char*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<unsigned char*>(0);
    while (1) {
        v32 = *reinterpret_cast<unsigned char**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<unsigned char*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *rbx41;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_5983_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_5983_22; else 
                            goto addr_5d7d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_5e3d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_6190_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *rbx41;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_5980_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_5980_30; else 
                                goto addr_61a9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2460(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_6190_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2520(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_6190_28; else 
                            goto addr_582c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_62f0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_6170_40:
                        if (r11_27 == 1) {
                            addr_5cfd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_62b8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_5937_44;
                            }
                        } else {
                            goto addr_6180_46;
                        }
                    } else {
                        addr_62ff_47:
                        rax24 = v46;
                        if (!rax24[1]) {
                            goto addr_5cfd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_5983_22:
                                if (v47 != 1) {
                                    addr_5ed9_52:
                                    v48 = reinterpret_cast<unsigned char*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2460(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_5f24_54;
                                    }
                                } else {
                                    goto addr_5990_56;
                                }
                            } else {
                                addr_5935_57:
                                ebp36 = 0;
                                goto addr_5937_44;
                            }
                        } else {
                            addr_6164_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_62ff_47; else 
                                goto addr_616e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_5cfd_41;
                        if (v47 == 1) 
                            goto addr_5990_56; else 
                            goto addr_5ed9_52;
                    }
                }
                addr_59f1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<unsigned char*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_5888_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_58ad_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_5bb0_65;
                    } else {
                        addr_5a19_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_6268_67;
                    }
                } else {
                    goto addr_5a10_69;
                }
                addr_58c1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_590c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_6268_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_590c_81;
                }
                addr_5a10_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_58ad_64; else 
                    goto addr_5a19_66;
                addr_5937_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_59ef_91;
                if (v22) 
                    goto addr_594f_93;
                addr_59ef_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_59f1_62;
                addr_5f24_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_66ab_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_671b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *rdx10 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_651f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26a0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2690(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_601e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_59dc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_6028_112;
                    }
                } else {
                    addr_6028_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_60f9_114;
                }
                addr_59e8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_59ef_91;
                while (1) {
                    addr_60f9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_6607_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_6066_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_6615_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_60e7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_60e7_128;
                        }
                    }
                    addr_6095_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_60e7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_6066_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_6095_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_590c_81;
                addr_6615_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6268_67;
                addr_66ab_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_601e_109;
                addr_671b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_601e_109;
                addr_5990_56:
                rax93 = fun_26b0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_59dc_110;
                addr_616e_59:
                goto addr_6170_40;
                addr_5e3d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_5983_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<unsigned char*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<unsigned char*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_59e8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5935_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_5983_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5e82_160;
                if (!v22) 
                    goto addr_6257_162; else 
                    goto addr_6463_163;
                addr_5e82_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_6257_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_6268_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_5d2b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_5b93_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_58c1_70; else 
                    goto addr_5ba7_169;
                addr_5d2b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_5888_63;
                goto addr_5a10_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_6164_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_629f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5980_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<unsigned char*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<unsigned char*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5878_178; else 
                        goto addr_6222_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6164_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5983_22;
                }
                addr_629f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_5980_30:
                    r8d42 = 0;
                    goto addr_5983_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_59f1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_62b8_42;
                    }
                }
                addr_5878_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5888_63;
                addr_6222_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_6180_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_59f1_62;
                } else {
                    addr_6232_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_5983_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_69e2_188;
                if (v28) 
                    goto addr_6257_162;
                addr_69e2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_5b93_168;
                addr_582c_37:
                if (v22) 
                    goto addr_6823_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *rbx41;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_5843_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_62f0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_637b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5983_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<unsigned char*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<unsigned char*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5878_178; else 
                        goto addr_6357_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6164_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5983_22;
                }
                addr_637b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_5983_22;
                }
                addr_6357_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6180_46;
                goto addr_6232_186;
                addr_5843_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_5983_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_5983_22; else 
                    goto addr_5854_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<unsigned char*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_692e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_67b4_210;
            if (1) 
                goto addr_67b2_212;
            if (!v29) 
                goto addr_63ee_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<unsigned char*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<unsigned char*>("\"");
            if (!0) 
                goto addr_6921_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<unsigned char*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<unsigned char*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_5bb0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_596b_219; else 
            goto addr_5bca_220;
        addr_594f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_5963_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_5bca_220; else 
            goto addr_596b_219;
        addr_651f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_5bca_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_653d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<unsigned char*>("'");
        v29 = reinterpret_cast<unsigned char*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<unsigned char*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<unsigned char*>(1);
        v22 = reinterpret_cast<unsigned char*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<unsigned char*>(0);
            continue;
        }
        addr_69b0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_6416_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<unsigned char*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<unsigned char*>(0);
        v22 = reinterpret_cast<unsigned char*>(0);
        v28 = reinterpret_cast<unsigned char*>(1);
        v26 = reinterpret_cast<unsigned char*>("'");
        continue;
        addr_6607_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5963_221;
        addr_6463_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5963_221;
        addr_5ba7_169:
        goto addr_5bb0_65;
        addr_692e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_5bca_220;
        goto addr_653d_222;
        addr_67b4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *v26, *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_680e_236;
        fun_2470();
        rsp25 = rsp25 - 8 + 8;
        goto addr_69b0_225;
        addr_67b2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_67b4_210;
        addr_63ee_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_67b4_210;
        } else {
            rdx10 = reinterpret_cast<unsigned char*>(0);
            goto addr_6416_226;
        }
        addr_6921_216:
        r13_34 = reinterpret_cast<unsigned char*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<unsigned char*>("\"");
        v29 = reinterpret_cast<unsigned char*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<unsigned char*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<unsigned char*>(1);
        v22 = reinterpret_cast<unsigned char*>(0);
        v31 = reinterpret_cast<unsigned char*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_5d7d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9f0c + rax113 * 4) + 0x9f0c;
    addr_61a9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa00c + rax114 * 4) + 0xa00c;
    addr_6823_190:
    addr_596b_219:
    goto 0x5650;
    addr_5854_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9e0c + rax115 * 4) + 0x9e0c;
    addr_680e_236:
    goto v116;
}

void fun_5870() {
}

void fun_5a28() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x5722;
}

void fun_5a81() {
    goto 0x5722;
}

void fun_5b6e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x59f1;
    }
    if (v2) 
        goto 0x6463;
    if (!r10_3) 
        goto addr_65ce_5;
    if (!v4) 
        goto addr_649e_7;
    addr_65ce_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_649e_7:
    goto 0x58a4;
}

void fun_5b8c() {
}

void fun_5c37() {
    signed char v1;

    if (v1) {
        goto 0x5bbf;
    } else {
        goto 0x58fa;
    }
}

void fun_5c51() {
    signed char v1;

    if (!v1) 
        goto 0x5c4a; else 
        goto "???";
}

void fun_5c78() {
    goto 0x5b93;
}

void fun_5cf8() {
}

void fun_5d10() {
}

void fun_5d3f() {
    goto 0x5b93;
}

void fun_5d91() {
    goto 0x5d20;
}

void fun_5dc0() {
    goto 0x5d20;
}

void fun_5df3() {
    goto 0x5d20;
}

void fun_61c0() {
    goto 0x5878;
}

void fun_64be() {
    signed char v1;

    if (v1) 
        goto 0x6463;
    goto 0x58a4;
}

void fun_6565() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x58a4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x5888;
        goto 0x58a4;
    }
}

void fun_6982() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x5bf0;
    } else {
        goto 0x5722;
    }
}

void fun_78b8() {
    fun_2440();
}

void fun_86f5() {
    int64_t rbx1;
    int64_t rbx2;
    int32_t ecx3;

    if (__intrinsic()) {
        if (rbx1 < 0) {
            goto 0x86db;
        } else {
            goto 0x86db;
        }
    } else {
        if (__intrinsic()) {
            if (rbx2 * ecx3 >= 0) {
            }
            goto 0x86db;
        } else {
            goto 0x86db;
        }
    }
}

void fun_8715() {
    if (!__intrinsic()) 
        goto 0x8710; else 
        goto "???";
}

void fun_8738() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 3;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x86d8;
}

void fun_877a() {
    int64_t rbx1;

    if (!__intrinsic()) 
        goto 0x8710;
    if (rbx1 >= 0) 
        goto 0x872c;
}

void fun_87b8() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 5;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x86d8;
}

void fun_87fa() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 6;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x86d8;
}

void fun_883a() {
    goto 0x86db;
}

void fun_8882() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 8;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x86d8;
}

void fun_88c2() {
    if (!__intrinsic()) 
        goto 0x8710;
    goto 0x8721;
}

void fun_88e0() {
    int32_t esi1;
    int64_t rbx2;

    esi1 = 4;
    do {
        if (__intrinsic()) {
            if (rbx2 < 0) {
                rbx2 = 0x8000000000000000;
            } else {
                rbx2 = 0x7fffffffffffffff;
            }
        } else {
            rbx2 = rbx2 * 0x400;
        }
        --esi1;
    } while (esi1);
}

void fun_2aad() {
    goto 0x28db;
}

void fun_5aae() {
    goto 0x5722;
}

void fun_5c84() {
    goto 0x5c3c;
}

void fun_5d4b() {
    goto 0x5878;
}

void fun_5d9d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x5d20;
    goto 0x594f;
}

void fun_5dcf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x5d2b;
        goto 0x5750;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x5bca;
        goto 0x596b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x6568;
    if (r10_8 > r15_9) 
        goto addr_5cb5_9;
    addr_5cba_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x6573;
    goto 0x58a4;
    addr_5cb5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_5cba_10;
}

void fun_5e02() {
    goto 0x5937;
}

void fun_61d0() {
    goto 0x5937;
}

void fun_696f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x5a8c;
    } else {
        goto 0x5bf0;
    }
}

void fun_7970() {
}

void fun_869b() {
}

void fun_87a6() {
    if (!__intrinsic()) 
        goto 0x8710;
    goto 0x8786;
}

void fun_8842() {
}

void fun_88f0() {
    goto 0x86f8;
}

void fun_2ad5() {
    goto 0x28db;
}

void fun_5e0c() {
    goto 0x5da7;
}

void fun_61da() {
    goto 0x5cfd;
}

void fun_79d0() {
    fun_2440();
    goto fun_2680;
}

void fun_8900() {
    goto 0x8718;
}

void fun_2afd() {
    goto 0x28db;
}

void fun_5add() {
    goto 0x5722;
}

void fun_5e18() {
    goto 0x5da7;
}

void fun_61e7() {
    goto 0x5d4e;
}

void fun_7a10() {
    fun_2440();
    goto fun_2680;
}

void fun_8910() {
    goto 0x873b;
}

void fun_2b25() {
    goto 0x28db;
}

void fun_5b0a() {
    goto 0x5722;
}

void fun_5e24() {
    goto 0x5d20;
}

void fun_7a50() {
    fun_2440();
    goto fun_2680;
}

void fun_8920() {
    goto 0x87fd;
}

void fun_2b4d() {
    goto 0x28db;
}

void fun_5b2c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x64c0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x59f1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x59f1;
    }
    if (v11) 
        goto 0x6823;
    if (r10_12 > r15_13) 
        goto addr_6873_8;
    addr_6878_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x65b1;
    addr_6873_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_6878_9;
}

struct s36 {
    signed char[24] pad24;
    int64_t f18;
};

struct s37 {
    signed char[16] pad16;
    unsigned char* f10;
};

struct s38 {
    signed char[8] pad8;
    unsigned char* f8;
};

void fun_7aa0() {
    int64_t r15_1;
    struct s36* rbx2;
    unsigned char* r14_3;
    struct s37* rbx4;
    unsigned char* r13_5;
    struct s38* rbx6;
    unsigned char* r12_7;
    unsigned char** rbx8;
    unsigned char* rax9;
    unsigned char* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2440();
    fun_2680(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x7ac2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_8930() {
    goto 0x88c5;
}

void fun_2b75() {
    goto 0x28db;
}

void fun_7af8() {
    fun_2440();
    goto 0x7ac9;
}

void fun_8938() {
    goto 0x87bb;
}

struct s39 {
    signed char[32] pad32;
    int64_t f20;
};

struct s40 {
    signed char[24] pad24;
    int64_t f18;
};

struct s41 {
    signed char[16] pad16;
    unsigned char* f10;
};

struct s42 {
    signed char[8] pad8;
    unsigned char* f8;
};

struct s43 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_7b30() {
    int64_t rcx1;
    struct s39* rbx2;
    int64_t r15_3;
    struct s40* rbx4;
    unsigned char* r14_5;
    struct s41* rbx6;
    unsigned char* r13_7;
    struct s42* rbx8;
    unsigned char* r12_9;
    unsigned char** rbx10;
    int64_t v11;
    struct s43* rbx12;
    unsigned char* rax13;
    unsigned char* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2440();
    fun_2680(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x7b64, __return_address(), rcx1);
    goto v15;
}

void fun_8948() {
    goto 0x8845;
}

void fun_7ba8() {
    fun_2440();
    goto 0x7b6b;
}

void fun_8958() {
    goto 0x8885;
}

void fun_8968() {
    goto 0x86db;
}
