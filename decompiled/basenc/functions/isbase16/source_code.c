isbase16 (char ch)
{
  return ('0' <= ch && ch <= '9') || ('A' <= ch && ch <= 'F');
}