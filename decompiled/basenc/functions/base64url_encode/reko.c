void base64url_encode(Eq_11 rcx, Eq_11 rdx, int64 rsi, struct Eq_1628 * rdi)
{
	base64_encode(rcx, rdx, rsi, rdi);
	Eq_11 rbx_13 = rdx;
	if (rcx != 0x00)
	{
		word64 rcx_34 = rdx + rcx;
		do
		{
			byte al_39 = *rbx_13;
			if (al_39 != 0x2B)
			{
				if (al_39 == 0x2F)
					*rbx_13 = 0x5F;
				rbx_13 = (word64) rbx_13 + 1;
				if (rcx_34 != rbx_13)
					continue;
				return;
			}
			*rbx_13 = 0x2D;
			rbx_13 = (word64) rbx_13 + 1;
		} while (rcx_34 != rbx_13);
	}
}