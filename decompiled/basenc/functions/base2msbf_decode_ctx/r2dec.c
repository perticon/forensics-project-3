uint64_t base2msbf_decode_ctx (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r10 = rcx;
    if (rdx == 0) {
        goto label_3;
    }
    rax = rsi + 1;
    r9 = rax + rdx;
    goto label_2;
label_0:
    edx <<= 7;
    *(rdi) = 7;
    esi |= edx;
    *((rdi + 4)) = sil;
    do {
label_1:
        rax++;
        if (rax == r9) {
            goto label_4;
        }
label_2:
        edx = *((rax - 1));
    } while (dl == 0xa);
    ecx = rdx - 0x30;
    if (cl > 1) {
        goto label_5;
    }
    ecx = *(rdi);
    esi = *((rdi + 4));
    dl = (dl == 0x31) ? 1 : 0;
    edx = (int32_t) dl;
    if (ecx == 0) {
        goto label_0;
    }
    ecx--;
    edx <<= cl;
    *(rdi) = ecx;
    esi |= edx;
    *((rdi + 4)) = sil;
    if (ecx != 0) {
        goto label_1;
    }
    rax++;
    *(r10) = sil;
    r10++;
    *((rdi + 4)) = 0;
    *(r8)++;
    *(rdi) = 0;
    if (rax != r9) {
        goto label_2;
    }
label_4:
    eax = 1;
    return rax;
label_3:
    eax = *(rdi);
    al = (eax == 0) ? 1 : 0;
    return rax;
label_5:
    eax = 0;
    return rax;
}