uint base2msbf_decode_ctx(uint *param_1,long param_2,long param_3,byte *param_4,long *param_5)

{
  char cVar1;
  uint uVar2;
  byte bVar3;
  
  *param_5 = 0;
  if (param_3 == 0) {
    return *param_1 & 0xffffff00 | (uint)(*param_1 == 0);
  }
  param_2 = param_2 + 1;
  param_3 = param_2 + param_3;
  do {
    cVar1 = *(char *)(param_2 + -1);
    if (cVar1 != '\n') {
      if (1 < (byte)(cVar1 - 0x30U)) {
        return 0;
      }
      if (*param_1 == 0) {
        *param_1 = 7;
        *(byte *)(param_1 + 1) = *(byte *)(param_1 + 1) | (cVar1 == '1') << 7;
      }
      else {
        uVar2 = *param_1 - 1;
        *param_1 = uVar2;
        bVar3 = *(byte *)(param_1 + 1) | (cVar1 == '1') << ((byte)uVar2 & 0x1f);
        *(byte *)(param_1 + 1) = bVar3;
        if (uVar2 == 0) {
          *param_4 = bVar3;
          param_4 = param_4 + 1;
          *(undefined *)(param_1 + 1) = 0;
          *param_5 = *param_5 + 1;
          *param_1 = 0;
        }
      }
    }
    param_2 = param_2 + 1;
  } while (param_2 != param_3);
  return 1;
}