void base2msbf_decode_ctx(byte * rcx, word64 rdx, word64 rsi, struct Eq_2025 * rdi, word64 * r8)
{
	*r8 = 0x00;
	byte * r10_7 = rcx;
	if (rdx == 0x00)
		return;
	struct Eq_2034 * rax_21 = rsi + 1;
	struct Eq_2034 * r9_22 = rsi + 1 + rdx;
	do
	{
		uint64 rdx_26 = (uint64) rax_21->bFFFFFFFF;
		byte dl_27 = (byte) rdx_26;
		if (dl_27 != 0x0A)
		{
			if ((byte) rdx_26 > 0x31 || (byte) rdx_26 < 0x30)
				return;
			word32 ecx_50 = rdi->dw0000;
			word32 esi_58 = (word32) rdi->b0004;
			word32 edx_54 = (word32) (dl_27 == 0x31);
			if (ecx_50 != 0x00)
			{
				uint64 rcx_68 = (uint64) (ecx_50 - 0x01);
				word32 ecx_72 = (word32) rcx_68;
				rdi->dw0000 = ecx_72;
				byte sil_77 = (byte) esi_58 | (byte) edx_54 << (byte) rcx_68;
				rdi->b0004 = sil_77;
				if (ecx_72 == 0x00)
				{
					*r10_7 = sil_77;
					rdi->b0004 = 0x00;
					++*r8;
					rdi->dw0000 = 0x00;
					++rax_21;
					++r10_7;
					if (rax_21 == r9_22)
						return;
					continue;
				}
			}
			else
			{
				rdi->dw0000 = 0x07;
				rdi->b0004 = (byte) esi_58 | (byte) edx_54 << 0x07;
			}
		}
		++rax_21;
	} while (rax_21 != r9_22);
}