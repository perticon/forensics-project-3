isbase2 (char ch)
{
  return ch == '0' || ch == '1';
}