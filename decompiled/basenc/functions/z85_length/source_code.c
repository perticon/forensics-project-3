z85_length (int len)
{
  /* Z85 does not allow padding, so no need to round to highest integer.  */
  int outlen = (len * 5) / 4;
  return outlen;
}