uint32_t isbase32hex (int64_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    eax = rdi - 0x30;
    al = (al <= 9) ? 1 : 0;
    edi -= 0x41;
    dl = (dil <= 0x15) ? 1 : 0;
    eax |= edx;
    return eax;
}