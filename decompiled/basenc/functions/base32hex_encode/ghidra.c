void base32hex_encode(undefined8 param_1,undefined8 param_2,char *param_3,long param_4)

{
  char *pcVar1;
  char *pcVar2;
  
  base32_encode();
  if (param_4 != 0) {
    pcVar1 = param_3;
    do {
      if (0x28 < (byte)(*pcVar1 - 0x32U)) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("0x32 <= *p && *p <= 0x5a","src/basenc.c",0x1c4,"base32hex_encode");
      }
      pcVar2 = pcVar1 + 1;
      *pcVar1 = "QRSTUV89:;<=>?@0123456789ABCDEFGHIJKLMNOP9.1.17-a351f"[(char)(*pcVar1 - 0x32U)];
      pcVar1 = pcVar2;
    } while (pcVar2 != param_3 + param_4);
  }
  return;
}