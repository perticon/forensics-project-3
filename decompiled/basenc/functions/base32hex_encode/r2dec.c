int64_t base32hex_encode (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    rbx = rdx;
    base32_encode (rdi, rsi, rdx, rcx);
    if (rbp == 0) {
        goto label_0;
    }
    rdx = rbx;
    rsi = "QRSTUV89:;<=>?@0123456789ABCDEFGHIJKLMNOP9.1.17-a351f";
    rcx = rbx + rbp;
    do {
        eax = *(rdx);
        eax -= 0x32;
        if (al > 0x28) {
            goto label_1;
        }
        rax = (int64_t) al;
        rdx++;
        eax = *((rsi + rax));
        *((rdx - 1)) = al;
    } while (rdx != rcx);
label_0:
    return rax;
label_1:
    return assert_fail ("0x32 <= *p && *p <= 0x5a", "src/basenc.c", 0x1c4, "base32hex_encode");
}