void base32hex_encode(Eq_11 rcx, Eq_11 rdx, int64 rsi, struct Eq_1984 * rdi)
{
	base32_encode(rcx, rdx, rsi, rdi);
	if (rcx != 0x00)
	{
		Eq_11 rdx_34 = rdx;
		word64 rcx_36 = rdx + rcx;
		do
		{
			cu8 al_44 = *rdx_34 - 0x32;
			if (al_44 > 0x28)
				fn00000000000024E0("base32hex_encode", 0x01C4, "src/basenc.c", "0x32 <= *p && *p <= 0x5a");
			rdx_34 = (word64) rdx_34 + 1;
			*((word64) rdx_34 - 1) = g_a9A80[(int64) al_44];
		} while (rdx_34 != rcx_36);
	}
}