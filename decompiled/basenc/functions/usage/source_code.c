usage (int status)
{
  if (status != EXIT_SUCCESS)
    emit_try_help ();
  else
    {
      printf (_("\
Usage: %s [OPTION]... [FILE]\n\
"), program_name);

#if BASE_TYPE == 42
      fputs (_("\
basenc encode or decode FILE, or standard input, to standard output.\n\
"), stdout);
#else
      printf (_("\
Base%d encode or decode FILE, or standard input, to standard output.\n\
"), BASE_TYPE);
#endif

      emit_stdin_note ();
      emit_mandatory_arg_note ();
#if BASE_TYPE == 42
      fputs (_("\
      --base64          same as 'base64' program (RFC4648 section 4)\n\
"), stdout);
      fputs (_("\
      --base64url       file- and url-safe base64 (RFC4648 section 5)\n\
"), stdout);
      fputs (_("\
      --base32          same as 'base32' program (RFC4648 section 6)\n\
"), stdout);
      fputs (_("\
      --base32hex       extended hex alphabet base32 (RFC4648 section 7)\n\
"), stdout);
      fputs (_("\
      --base16          hex encoding (RFC4648 section 8)\n\
"), stdout);
      fputs (_("\
      --base2msbf       bit string with most significant bit (msb) first\n\
"), stdout);
      fputs (_("\
      --base2lsbf       bit string with least significant bit (lsb) first\n\
"), stdout);
#endif
      fputs (_("\
  -d, --decode          decode data\n\
  -i, --ignore-garbage  when decoding, ignore non-alphabet characters\n\
  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).\n\
                          Use 0 to disable line wrapping\n\
"), stdout);
#if BASE_TYPE == 42
      fputs (_("\
      --z85             ascii85-like encoding (ZeroMQ spec:32/Z85);\n\
                        when encoding, input length must be a multiple of 4;\n\
                        when decoding, input length must be a multiple of 5\n\
"), stdout);
#endif
      fputs (HELP_OPTION_DESCRIPTION, stdout);
      fputs (VERSION_OPTION_DESCRIPTION, stdout);
#if BASE_TYPE == 42
      fputs (_("\
\n\
When decoding, the input may contain newlines in addition to the bytes of\n\
the formal alphabet.  Use --ignore-garbage to attempt to recover\n\
from any other non-alphabet bytes in the encoded stream.\n\
"), stdout);
#else
      printf (_("\
\n\
The data are encoded as described for the %s alphabet in RFC 4648.\n\
When decoding, the input may contain newlines in addition to the bytes of\n\
the formal %s alphabet.  Use --ignore-garbage to attempt to recover\n\
from any other non-alphabet bytes in the encoded stream.\n"),
              PROGRAM_NAME, PROGRAM_NAME);
#endif
      emit_ancillary_info (PROGRAM_NAME);
    }

  exit (status);
}