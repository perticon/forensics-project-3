void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002680(fn0000000000002440(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000003D4E;
	}
	fn0000000000002600(fn0000000000002440(0x05, "Usage: %s [OPTION]... [FILE]\n", null), 0x01);
	fn0000000000002530(stdout, fn0000000000002440(0x05, "basenc encode or decode FILE, or standard input, to standard output.\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --base64          same as 'base64' program (RFC4648 section 4)\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --base64url       file- and url-safe base64 (RFC4648 section 5)\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --base32          same as 'base32' program (RFC4648 section 6)\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --base32hex       extended hex alphabet base32 (RFC4648 section 7)\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --base16          hex encoding (RFC4648 section 8)\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --base2msbf       bit string with most significant bit (msb) first\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --base2lsbf       bit string with least significant bit (lsb) first\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "  -d, --decode          decode data\n  -i, --ignore-garbage  when decoding, ignore non-alphabet characters\n  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).\n                          Use 0 to disable line wrapping\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --z85             ascii85-like encoding (ZeroMQ spec:32/Z85);\n                        when encoding, input length must be a multiple of 4;\n                        when decoding, input length must be a multiple of 5\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "\nWhen decoding, the input may contain newlines in addition to the bytes of\nthe formal alphabet.  Use --ignore-garbage to attempt to recover\nfrom any other non-alphabet bytes in the encoded stream.\n", null));
	struct Eq_2664 * rbx_295 = fp - 0xB8 + 16;
	do
	{
		char * rsi_297 = rbx_295->qw0000;
		++rbx_295;
	} while (rsi_297 != null && fn0000000000002550(rsi_297, 38728) != 0x00);
	ptr64 r13_310 = rbx_295->qw0008;
	if (r13_310 != 0x00)
	{
		fn0000000000002600(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_11 rax_397 = fn00000000000025F0(null, 0x05);
		if (rax_397 == 0x00 || fn00000000000023C0(0x03, "en_", rax_397) == 0x00)
			goto l00000000000040C6;
	}
	else
	{
		fn0000000000002600(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_11 rax_339 = fn00000000000025F0(null, 0x05);
		if (rax_339 == 0x00 || fn00000000000023C0(0x03, "en_", rax_339) == 0x00)
		{
			fn0000000000002600(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000004103:
			fn0000000000002600(fn0000000000002440(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000003D4E:
			fn0000000000002660(edi);
		}
		r13_310 = 38728;
	}
	fn0000000000002530(stdout, fn0000000000002440(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000040C6:
	fn0000000000002600(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000004103;
}