void base16_decode_ctx_init(undefined4 *param_1)

{
  undefined8 uVar1;
  
  *(undefined8 *)(param_1 + 6) = 0x1068;
  uVar1 = xcharalloc(0x1068);
  *(undefined *)((long)param_1 + 5) = 0;
  *(undefined8 *)(param_1 + 4) = uVar1;
  *param_1 = 1;
  return;
}