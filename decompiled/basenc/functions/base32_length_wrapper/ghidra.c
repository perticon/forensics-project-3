int base32_length_wrapper(int param_1)

{
  return ((param_1 + 4) / 5) * 8;
}