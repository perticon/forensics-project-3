void base64url_decode_ctx_wrapper(Eq_11 rax, Eq_11 rdx, Eq_11 rsi, struct Eq_1894 * rdi, word64 * r8, struct Eq_1795 * fs)
{
	Eq_11 rdi_29 = rdi->t0010;
	if (rdx > rdi->t0018)
	{
		rdi->t0018 = rdx * 0x02;
		xreallocarray(rax, 0x01, rdx * 0x02, rdi_29);
		rdi->t0010 = rax;
		rdi_29 = rax;
	}
	fn0000000000002570(rdx, rsi, rdi_29);
	Eq_11 rsi_100 = rdi->t0010;
	if (rdx != 0x00)
	{
		word64 rcx_55 = rsi_100 + rdx;
		do
		{
			uint64 rax_59 = (uint64) *rsi_100;
			byte al_87 = (byte) rax_59;
			if (((byte) rax_59 & ~0x04) == 0x2B)
			{
				*r8 = 0x00;
				return;
			}
			if (al_87 != 0x2D)
			{
				if (al_87 == 0x5F)
					*rsi_100 = 0x2F;
				rsi_100 = (word64) rsi_100 + 1;
				if (rsi_100 != rcx_55)
					continue;
				break;
			}
			*rsi_100 = 0x2B;
			rsi_100 = (word64) rsi_100 + 1;
		} while (rsi_100 != rcx_55);
		rsi_100 = rdi->t0010;
	}
	base64_decode_ctx(rdx, rsi_100, &rdi->dw0004, r8, (word32) rdx, fs);
	rdi->dw0000 = rdi->dw0004;
}