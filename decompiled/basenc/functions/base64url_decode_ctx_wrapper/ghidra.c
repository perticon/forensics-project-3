base64url_decode_ctx_wrapper
          (undefined4 *param_1,void *param_2,size_t param_3,undefined8 param_4,undefined8 *param_5)

{
  byte *pbVar1;
  byte bVar2;
  undefined8 uVar3;
  void *__dest;
  byte *pbVar4;
  
  __dest = *(void **)(param_1 + 4);
  if (*(long *)(param_1 + 6) < (long)param_3) {
    *(size_t *)(param_1 + 6) = param_3 * 2;
    __dest = (void *)xreallocarray(__dest,param_3 * 2,1);
    *(void **)(param_1 + 4) = __dest;
  }
  memcpy(__dest,param_2,param_3);
  pbVar4 = *(byte **)(param_1 + 4);
  if (param_3 != 0) {
    pbVar1 = pbVar4 + param_3;
    do {
      while( true ) {
        bVar2 = *pbVar4;
        if ((bVar2 & 0xfb) == 0x2b) {
          *param_5 = 0;
          return 0;
        }
        if (bVar2 != 0x2d) break;
        *pbVar4 = 0x2b;
        pbVar4 = pbVar4 + 1;
        if (pbVar4 == pbVar1) goto LAB_00103825;
      }
      if (bVar2 == 0x5f) {
        *pbVar4 = 0x2f;
      }
      pbVar4 = pbVar4 + 1;
    } while (pbVar4 != pbVar1);
LAB_00103825:
    pbVar4 = *(byte **)(param_1 + 4);
  }
  uVar3 = base64_decode_ctx(param_1 + 1,pbVar4,param_3,param_4,param_5);
  *param_1 = param_1[1];
  return uVar3;
}