void main(int param_1,undefined8 *param_2)

{
  char *pcVar1;
  char cVar2;
  uint uVar3;
  int iVar4;
  long lVar5;
  FILE *__stream;
  _IO_FILE *p_Var6;
  size_t sVar7;
  undefined8 *puVar8;
  ulong uVar9;
  FILE *pFVar10;
  undefined8 uVar11;
  undefined8 uVar12;
  int *piVar13;
  FILE *pFVar14;
  void *__ptr;
  void *pvVar15;
  undefined8 *puVar16;
  _IO_FILE *p_Var17;
  ulong __n;
  long in_FS_OFFSET;
  undefined auVar18 [16];
  undefined8 local_a8;
  FILE *local_a0;
  undefined8 *local_98;
  undefined8 local_90;
  void *local_88;
  char *local_80;
  undefined8 *local_70;
  int local_68;
  undefined8 local_58;
  undefined8 local_40;
  
  __ptr = (void *)0x0;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  local_a8 = (_IO_FILE *)((ulong)local_a8._4_4_ << 0x20);
  local_90 = (_IO_FILE *)((ulong)local_90 & 0xffffffffffffff00);
  puVar16 = (undefined8 *)0x4c;
LAB_0010278d:
  do {
    auVar18 = getopt_long(param_1,param_2,&DAT_00109860,long_options,0);
    iVar4 = SUB164(auVar18,0);
    if (iVar4 == -1) {
LAB_00102898:
      switch((undefined4)local_a8) {
      case 0x80:
        isbase = isbase64;
        base_encode = base64_encode;
        base_length = base64_length_wrapper;
        base_decode_ctx_init = base64_decode_ctx_init_wrapper;
        base_decode_ctx = base64_decode_ctx_wrapper;
        break;
      case 0x81:
        base_length = base64_length_wrapper;
        isbase = isbase64url;
        base_encode = base64url_encode;
        base_decode_ctx_init = base64url_decode_ctx_init_wrapper;
        base_decode_ctx = base64url_decode_ctx_wrapper;
        break;
      case 0x82:
        isbase = isbase32;
        base_encode = base32_encode;
        base_length = base32_length_wrapper;
        base_decode_ctx_init = base32_decode_ctx_init_wrapper;
        base_decode_ctx = base32_decode_ctx_wrapper;
        break;
      case 0x83:
        base_length = base32_length_wrapper;
        isbase = isbase32hex;
        base_encode = base32hex_encode;
        base_decode_ctx_init = base32hex_decode_ctx_init_wrapper;
        base_decode_ctx = base32hex_decode_ctx_wrapper;
        break;
      case 0x84:
        base_length = base16_length;
        isbase = isbase16;
        base_encode = base16_encode;
        base_decode_ctx_init = base16_decode_ctx_init;
        base_decode_ctx = base16_decode_ctx;
        break;
      case 0x85:
        base_length = base2_length;
        isbase = isbase2;
        base_encode = base2msbf_encode;
        base_decode_ctx_init = base2_decode_ctx_init;
        base_decode_ctx = base2msbf_decode_ctx;
        break;
      case 0x86:
        base_length = base2_length;
        isbase = isbase2;
        base_encode = base2lsbf_encode;
        base_decode_ctx_init = base2_decode_ctx_init;
        base_decode_ctx = base2lsbf_decode_ctx;
        break;
      case 0x87:
        base_length = z85_length;
        isbase = isz85;
        base_encode = z85_encode;
        base_decode_ctx_init = z85_decode_ctx_init;
        base_decode_ctx = z85_decode_ctx;
        break;
      default:
switchD_001028b5_caseD_8:
        uVar11 = dcgettext(0,"missing encoding type",5);
        error(0,0,uVar11);
        goto LAB_001027dd;
      }
      lVar5 = (long)optind;
      if (1 < param_1 - optind) goto LAB_00102c59;
      if (param_1 <= optind) goto LAB_00102c94;
      local_80 = (char *)param_2[lVar5];
      iVar4 = strcmp(local_80,"-");
      __stream = stdin;
      if ((iVar4 == 0) || (__stream = fopen(local_80,"rb"), __stream != (FILE *)0x0)) {
        do {
          fadvise(__stream,2);
          pFVar14 = stdout;
          if ((char)__ptr == '\0') {
            param_2 = (undefined8 *)0x0;
            p_Var6 = (_IO_FILE *)xmalloc(0x7800);
            iVar4 = (*base_length)(0x7800);
            local_a8 = (_IO_FILE *)xmalloc((long)iVar4);
            local_98 = puVar16;
            goto LAB_00102cde;
          }
          local_a0 = stdout;
          iVar4 = (*base_length)(0x1068);
          p_Var6 = (_IO_FILE *)xmalloc((long)iVar4);
          __ptr = (void *)xmalloc(0x1068);
          local_58 = 0;
          local_98 = (undefined8 *)&local_68;
          (*base_decode_ctx_init)(local_98);
LAB_001029b8:
          param_2 = (undefined8 *)0x0;
          p_Var17 = p_Var6;
          pvVar15 = __ptr;
          local_a8 = p_Var6;
          local_88 = __ptr;
          do {
            iVar4 = (*base_length)(0x1068);
            sVar7 = fread_unlocked((char *)((long)&local_a8->_flags + (long)param_2),1,
                                   (long)iVar4 - (long)param_2,__stream);
            if (((char)local_90 != '\0') && (0 < (long)sVar7)) {
              pvVar15 = (void *)0x0;
              do {
                p_Var17 = (_IO_FILE *)((long)param_2 + (long)pvVar15);
                puVar16 = (undefined8 *)((long)&p_Var17->_flags + (long)&local_a8->_flags);
                cVar2 = (*isbase)((int)*(char *)puVar16);
                if ((cVar2 == '\0') && (*(char *)puVar16 != '=')) {
                  sVar7 = sVar7 - 1;
                  memmove(puVar16,(char *)((long)&local_a8->_flags + 1) + (long)p_Var17,
                          sVar7 - (long)pvVar15);
                }
                else {
                  pvVar15 = (void *)((long)pvVar15 + 1);
                }
              } while ((long)pvVar15 < (long)sVar7);
            }
            param_2 = (undefined8 *)((long)param_2 + sVar7);
            uVar3 = __stream->_flags & 0x20;
            pFVar14 = (FILE *)(ulong)uVar3;
            p_Var6 = p_Var17;
            __ptr = pvVar15;
            if (uVar3 != 0) goto LAB_00102f9f;
            iVar4 = (*base_length)(0x1068);
            __ptr = local_88;
            p_Var6 = local_a8;
            if ((long)iVar4 <= (long)param_2) {
              uVar3 = __stream->_flags;
              goto LAB_00102c17;
            }
          } while ((__stream->_flags & 0x10U) == 0);
          while ((int)pFVar14 != 2) {
            if ((int)pFVar14 != 1) goto LAB_00102bb8;
            if (local_68 == 0) break;
            puVar16 = (undefined8 *)0x0;
LAB_00102bd0:
            local_70 = (undefined8 *)0x1068;
            uVar3 = (*base_decode_ctx)(local_98,p_Var6,puVar16,__ptr,&local_70);
            puVar16 = (undefined8 *)(ulong)uVar3;
            puVar8 = (undefined8 *)fwrite_unlocked(__ptr,1,(size_t)local_70,local_a0);
            if (puVar8 < local_70) {
LAB_00102f73:
              uVar11 = dcgettext(0,"write error",5);
              piVar13 = __errno_location();
              error(1,*piVar13,uVar11);
              goto LAB_00102f9f;
            }
            if ((char)uVar3 == '\0') {
              uVar11 = dcgettext(0,"invalid input",5);
              error(1,0,uVar11);
              goto LAB_00102f73;
            }
            uVar3 = __stream->_flags;
            pFVar14 = (FILE *)(ulong)((int)pFVar14 + 1);
LAB_00102c17:
            if ((uVar3 & 0x10) == 0) {
              if ((int)pFVar14 != 0) goto LAB_001029b8;
LAB_00102bb8:
              puVar16 = (undefined8 *)0x0;
              if ((int)pFVar14 == 0) {
                puVar16 = param_2;
              }
              goto LAB_00102bd0;
            }
          }
LAB_00102c4c:
          lVar5 = finish_and_exit(__stream,local_80);
LAB_00102c59:
          uVar11 = quote(param_2[lVar5 + 1]);
          uVar12 = dcgettext(0,"extra operand %s",5);
          error(0,0,uVar12,uVar11);
          usage(1);
LAB_00102c94:
          local_80 = "-";
          __stream = stdin;
        } while( true );
      }
LAB_0010300d:
      uVar11 = quotearg_n_style_colon(0,3,local_80);
      piVar13 = __errno_location();
      error(1,*piVar13,"%s",uVar11);
LAB_0010303e:
      uVar11 = quote(optarg);
      uVar12 = dcgettext(0,"invalid wrap size",5);
      error(1,0,"%s: %s",uVar12,uVar11);
LAB_0010307b:
      uVar11 = dcgettext(0,"write error",5);
      piVar13 = __errno_location();
      auVar18 = error(1,*piVar13,uVar11);
      p_Var6 = local_a8;
      local_a8 = SUB168(auVar18,0);
      (*(code *)PTR___libc_start_main_0010dfc0)
                (main,p_Var6,&local_a0,0,0,SUB168(auVar18 >> 0x40,0),&local_a8);
      do {
                    /* WARNING: Do nothing block with infinite loop */
      } while( true );
    }
    if (iVar4 == 0x69) {
      local_90 = (_IO_FILE *)CONCAT71(local_90._1_7_,1);
      goto LAB_0010278d;
    }
    if (iVar4 < 0x6a) {
      if (iVar4 == -0x82) {
        usage(0);
        goto LAB_00102898;
      }
      if (iVar4 != 100) {
        while (SUB164(auVar18,0) != -0x83) {
LAB_001027dd:
          auVar18 = usage(1);
        }
        version_etc(stdout,"basenc","GNU coreutils",Version,"Simon Josefsson","Assaf Gordon",0,
                    SUB168(auVar18 >> 0x40,0));
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      __ptr = (void *)0x1;
    }
    else if (iVar4 == 0x77) {
      uVar3 = xstrtoimax(optarg,0,10,&local_70);
      if ((1 < uVar3) || ((long)local_70 < 0)) goto LAB_0010303e;
      puVar16 = local_70;
      if (uVar3 == 1) {
        puVar16 = (undefined8 *)0x0;
      }
    }
    else {
      if (7 < iVar4 - 0x80U) goto LAB_001027dd;
      local_a8 = (_IO_FILE *)((ulong)local_a8 & 0xffffffff00000000 | SUB168(auVar18,0) & 0xffffffff)
      ;
    }
  } while( true );
LAB_00102f9f:
  __stream = (FILE *)dcgettext(0,"read error",5);
  piVar13 = __errno_location();
  error(1,*piVar13,__stream);
LAB_00102fcb:
  iVar4 = __overflow(pFVar14,10);
  if (iVar4 != -1) {
LAB_00102f46:
    uVar3 = __stream->_flags;
LAB_00102d2e:
    if ((uVar3 & 0x20) != 0) {
      __stream = (FILE *)dcgettext(0,"read error",5);
      piVar13 = __errno_location();
      error(1,*piVar13,__stream);
      do {
        local_a0 = (FILE *)base_encode;
        iVar4 = (*base_length)((ulong)__ptr & 0xffffffff);
        (*(code *)local_a0)(p_Var6,__ptr,local_a8,(long)iVar4);
        iVar4 = (*base_length)();
        puVar16 = local_98;
        p_Var17 = pFVar14;
        pvVar15 = __ptr;
        if (local_98 == (undefined8 *)0x0) {
LAB_00102ead:
          pFVar14 = p_Var17;
          local_a0 = (FILE *)(long)iVar4;
          pFVar10 = (FILE *)fwrite_unlocked(local_a8,1,(size_t)local_a0,stdout);
          __ptr = pvVar15;
          if (pFVar10 < local_a0) {
            uVar11 = dcgettext(0,"write error",5);
            piVar13 = __errno_location();
            error(1,*piVar13,uVar11);
            goto switchD_001028b5_caseD_8;
          }
        }
        else {
          p_Var17 = (_IO_FILE *)(long)iVar4;
          pvVar15 = (void *)0x0;
          if (0 < (long)p_Var17) {
            do {
              local_88 = __ptr;
              local_90 = p_Var6;
              local_a0 = __stream;
              uVar9 = (long)puVar16 - (long)param_2;
              __n = (long)p_Var17 - (long)pvVar15;
              if ((long)uVar9 < (long)p_Var17 - (long)pvVar15) {
                __n = uVar9;
              }
              if (__n == 0) {
                pcVar1 = pFVar14->_IO_write_ptr;
                if (pcVar1 < pFVar14->_IO_write_end) {
                  pFVar14->_IO_write_ptr = pcVar1 + 1;
                  *pcVar1 = '\n';
                }
                else {
                  iVar4 = __overflow(pFVar14,10);
                  if (iVar4 == -1) {
                    __stream = (FILE *)dcgettext(0,"write error",5);
                    piVar13 = __errno_location();
                    iVar4 = error(1,*piVar13,__stream);
                    param_2 = puVar16;
                    p_Var6 = pFVar14;
                    goto LAB_00102ead;
                  }
                }
                param_2 = (undefined8 *)0x0;
              }
              else {
                sVar7 = fwrite_unlocked((char *)((long)&local_a8->_flags + (long)pvVar15),1,__n,
                                        stdout);
                if (sVar7 < __n) goto LAB_0010307b;
                param_2 = (undefined8 *)((long)param_2 + __n);
                pvVar15 = (void *)((long)pvVar15 + __n);
              }
              __stream = local_a0;
              p_Var6 = local_90;
              __ptr = local_88;
            } while ((long)pvVar15 < (long)p_Var17);
          }
        }
        uVar3 = __stream->_flags;
        if (((uVar3 & 0x30) != 0) || (__ptr != (void *)0x7800)) goto LAB_00102d1b;
LAB_00102cde:
        __ptr = (void *)0x0;
        do {
          sVar7 = fread_unlocked((char *)((long)&p_Var6->_flags + (long)__ptr),1,
                                 0x7800 - (long)__ptr,__stream);
          __ptr = (void *)((long)__ptr + sVar7);
          uVar3 = __stream->_flags;
          if ((uVar3 & 0x30) != 0) {
            if ((long)__ptr < 1) goto LAB_00102d1b;
            break;
          }
        } while ((long)__ptr < 0x7800);
      } while( true );
    }
    goto LAB_00102c4c;
  }
  uVar11 = dcgettext(0,"write error",5);
  piVar13 = __errno_location();
  error(1,*piVar13,uVar11);
  goto LAB_0010300d;
LAB_00102d1b:
  puVar16 = local_98;
  if (((long)param_2 < 1) || (local_98 == (undefined8 *)0x0)) goto LAB_00102d2e;
  pcVar1 = pFVar14->_IO_write_ptr;
  if (pFVar14->_IO_write_end <= pcVar1) goto LAB_00102fcb;
  pFVar14->_IO_write_ptr = pcVar1 + 1;
  *pcVar1 = '\n';
  goto LAB_00102f46;
}