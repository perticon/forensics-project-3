void main(char * rsi[], word32 edi, struct Eq_418 * fs)
{
	ptr64 fp;
	set_program_name(rsi[0]);
	fn00000000000025F0("", 0x06);
	fn0000000000002430("/usr/local/share/locale", "coreutils");
	fn0000000000002410("coreutils");
	atexit(&g_t5200);
	Eq_11 r15_1087 = 0x4C;
	Eq_11 r14_1093 = 0x00;
	struct option * r13_119 = &g_tDA00;
	struct Eq_1220 ** rbp_115 = (uint64) edi;
	char * rbx_121[] = rsi;
	Eq_11 rsp_1022 = fp - 0xA8;
	char * r12_118 = "diw:";
	while (true)
	{
l000000000000278D:
		int32 ebp_82 = (word32) rbp_115;
		byte r14b_1315 = (byte) r14_1093;
		Eq_11 eax_85 = fn0000000000002480(r13_119, r12_118, rbx_121, ebp_82, null);
		if (eax_85 == ~0x00)
			break;
		if (eax_85 != 0x69)
		{
			if (eax_85 <= 0x69)
			{
				if (eax_85 == ~0x81)
					usage(0x00);
				if (eax_85 == 100)
				{
					r14_1093.u0 = 0x01;
					goto l000000000000278D;
				}
				if (eax_85 == ~0x82)
				{
					struct Eq_1187 * rsp_1264 = rsp_1022 - 8;
					rsp_1264->ptr0000 = r12_118;
					Eq_11 rdi_1266 = stdout;
					rsp_1264->dwFFFFFFFC = 0x00;
					version_etc(38728, rdi_1266, fs);
					fn0000000000002660(0x00);
				}
l00000000000027DD:
				usage(0x01);
			}
			if (eax_85 != 0x77)
			{
				if (eax_85 > 0x87)
					goto l00000000000027DD;
				*rsp_1022.u0 = eax_85;
				goto l000000000000278D;
			}
			rsp_1022.u0 = <invalid>;
			r14_1093.u0 = <invalid>;
			byte r14b_1415;
			up32 eax_129 = xstrtoimax((word32) rsp_1022.u0 + 56, 0x0A, rbp_115, null, optarg, "", fs, out rbx_121, out rbp_115, out r12_118, out r13_119, out r14b_1415);
			if (eax_129 <= 0x01)
			{
				r15_1087 = *((word32) rsp_1022.u0 + 56);
				if (r15_1087 >= 0x00)
				{
					if (eax_129 == 0x01)
						r15_1087.u0 = 0x00;
					goto l000000000000278D;
				}
			}
l000000000000303E:
			quote(fs);
			fn0000000000002440(0x05, "invalid wrap size", null);
			fn0000000000002620(0x983C, 0x00, 0x01);
l000000000000307B:
			Eq_11 rax_1208 = fn0000000000002440(0x05, "write error", null);
			fn0000000000002620(rax_1208, fn00000000000023B0()[0], 0x01);
			_start(rax_1208, *rsp_1022);
		}
		((word64) rsp_1022 + 24)->u0 = 0x01;
	}
	uint64 rax_162 = (uint64) ((word64) *rsp_1022 - 0x0080);
	if ((word32) rax_162 > 0x07)
	{
l0000000000002F07:
		fn0000000000002620(fn0000000000002440(0x05, "missing encoding type", null), 0x00, 0x00);
		goto l00000000000027DD;
	}
	Eq_11 rcx_173;
	<anonymous> * rsi_172;
	<anonymous> * rdi_171;
	<anonymous> * rdx_174;
	<anonymous> * rax_175;
	switch (g_a9920[rax_162 * 0x04] + (word32) 0x9920)
	{
	case 0x00:
		rsi_172 = &g_t4E20;
		rcx_173.u0 = 0x4B90;
		rdi_171 = &g_t31A0;
		rdx_174 = &g_t3710;
		rax_175 = &g_t36C0;
		break;
	case 0x01:
		rdi_171 = &g_t31A0;
		rsi_172 = &g_t34E0;
		rcx_173.u0 = 0x3520;
		rdx_174 = &g_t36E0;
		rax_175 = &g_t37B0;
		break;
	case 0x02:
		rsi_172 = &g_t46C0;
		rcx_173.u0 = 0x43B0;
		rdi_171 = &g_t31C0;
		rdx_174 = &g_t36B0;
		rax_175 = &g_t3660;
		break;
	case 0x03:
		rdi_171 = &g_t31C0;
		rsi_172 = &g_t31E0;
		rcx_173.u0 = 0x3890;
		rdx_174 = &g_t3680;
		rax_175 = &g_t3C20;
		break;
	case 0x04:
		rdi_171 = &g_t3220;
		rsi_172 = &g_t3200;
		rcx_173.u0 = 0x3230;
		rdx_174 = &g_t35D0;
		rax_175 = &g_t3B80;
		break;
	case 0x05:
		rdi_171 = &g_t32B0;
		rsi_172 = &g_t32A0;
		rcx_173.u0 = 12992;
		rdx_174 = &g_t35A0;
		rax_175 = &g_t3910;
		break;
	case 0x06:
		rdi_171 = &g_t32B0;
		rsi_172 = &g_t32A0;
		rcx_173.u0 = 0x3310;
		rdx_174 = &g_t35A0;
		rax_175 = &g_t39D0;
		break;
	case 0x07:
		rdi_171 = &g_t3280;
		rsi_172 = &g_t3600;
		rcx_173.u0 = 0x3360;
		rdx_174 = &g_t3570;
		rax_175 = &g_t3A60;
		break;
	}
	base_decode_ctx = rax_175;
	int64 rax_213 = (int64) g_dwE098;
	base_decode_ctx_init = rdx_174;
	base_length = rdi_171;
	isbase = rsi_172;
	base_encode = rcx_173;
	int32 eax_220 = (word32) rax_213;
	if (ebp_82 - eax_220 > 0x01)
	{
l0000000000002C59:
		quote(fs);
		fn0000000000002620(fn0000000000002440(0x05, "extra operand %s", null), 0x00, 0x00);
		usage(0x01);
	}
	Eq_11 r12_1353;
	if (eax_220 < ebp_82)
	{
		Eq_11 rax_238 = rbx_121[rax_213];
		*((word64) rsp_1022 + 40) = rax_238;
		if (fn0000000000002550("-", rax_238) != 0x00)
		{
			Eq_11 rax_249 = fn0000000000002640("rb", *((word64) rsp_1022 + 40));
			r12_1353 = rax_249;
			if (rax_249 == 0x00)
				goto l000000000000300D;
l0000000000002957:
			Eq_11 rax_1009;
			Eq_11 r13_1100;
			Eq_11 rbx_1028;
			Eq_11 rbp_1039;
			Eq_11 r14_1001;
			Eq_654 al_1014;
			uint64 rsi_1006;
			word64 rsi_258 = fadvise(r12_1353);
			if (r14b_1315 != 0x00)
			{
				Eq_11 rax_309 = stdout;
				*((word64) rsp_1022 + 8) = rax_309;
				word64 rsi_317;
				word64 rax_318;
				base_length();
				r13_1100 = xmalloc((int64) (word32) rax_318);
				Eq_11 rax_335 = xmalloc(4200);
				*((word64) rsp_1022 + 80) = 0x00;
				Eq_11 rax_342 = (word64) rsp_1022 + 64;
				*((word64) rsp_1022 + 16) = rax_342;
				uint64 rsi_1337;
				base_decode_ctx_init();
				r14_1001 = rax_335;
				do
				{
					*rsp_1022 = r13_1100;
					*((word64) rsp_1022 + 32) = r14_1001;
					Eq_11 rbx_358 = 0x00;
					do
					{
						base_length();
						Eq_11 rax_391 = *rsp_1022;
						fn0000000000002400();
						void * rsi_388 = (void *) 0x01;
						Eq_11 rbp_396 = rax_391;
						if (*((word64) rsp_1022 + 24) != 0x00 && rax_391 > 0x00)
						{
							r14_1001.u0 = 0x00;
							do
							{
								Eq_11 rax_407 = *rsp_1022;
								r13_1100 = rbx_358 + r14_1001;
								word64 r15_411 = rax_407 + r13_1100;
								word64 rax_420;
								isbase();
								if ((byte) rax_420 != 0x00 || *r15_411 == 0x3D)
									r14_1001 = (word32) r14_1001.u0 + 1;
								else
								{
									--rbp_396;
									rsi_388 = Mem363[rsp_1022 + 0x00:word64] + 1 + r13_1100;
									fn0000000000002610(rbp_396 - r14_1001, rsi_388, r15_411);
								}
							} while (rbp_396 > r14_1001);
						}
						rbp_1039 = (uint64) (*r12_1353 & 0x20);
						rbx_358 += rbp_396;
						if ((word32) rbp_1039 != 0x00)
							goto l0000000000002F9F;
						Eq_654 al_1231;
						<anonymous> * rax_1248;
						word64 rax_493;
						base_length();
						if (rbx_358 >= (int64) ((word32) rax_493))
						{
							r13_1100 = *rsp_1022;
							r14_1001 = *((word64) rsp_1022 + 32);
							al_1231 = *r12_1353;
							goto l0000000000002C17;
						}
					} while ((*r12_1353 & 0x10) == 0x00);
					r13_1100 = *rsp_1022;
					r14_1001 = *((word64) rsp_1022 + 32);
					do
					{
						word32 ebp_516 = (word32) rbp_1039;
						if (ebp_516 == 0x02)
							goto l0000000000002C4C;
						if (ebp_516 != 0x01)
							goto l0000000000002BB8;
						if (*((word64) rsp_1022 + 64) == 0x00)
							goto l0000000000002C4C;
						((word64) rsp_1022 + 56)->u0 = 4200;
						rax_1248 = base_decode_ctx;
l0000000000002BD0:
						word64 rax_553;
						rax_1248();
						word32 ebp_1226 = (word32) rbp_1039;
						rsi_1337 = 0x01;
						byte r15b_571 = (byte) rax_553;
						if (fn00000000000025D0(*((word64) rsp_1022 + 8), *((word64) rsp_1022 + 56), 0x01, r14_1001) < *((word64) rsp_1022 + 56))
						{
l0000000000002F73:
							fn0000000000002620(fn0000000000002440(0x05, "write error", null), fn00000000000023B0()[0], 0x01);
l0000000000002F9F:
							Eq_11 rax_624 = fn0000000000002440(0x05, "read error", null);
							fn0000000000002620(rax_624, fn00000000000023B0()[0], 0x01);
							rbx_1028 = rbx_358;
							r12_1353 = rax_624;
							goto l0000000000002FCB;
						}
						if (r15b_571 == 0x00)
						{
							fn0000000000002620(fn0000000000002440(0x05, "invalid input", null), 0x00, 0x01);
							goto l0000000000002F73;
						}
						al_1231 = *r12_1353;
						rbp_1039 = (uint64) (ebp_1226 + 0x01);
l0000000000002C17:
						word32 ebp_1237 = (word32) rbp_1039;
					} while ((al_1231 & 0x10) != 0x00);
				} while (ebp_1237 != 0x00);
l0000000000002BB8:
				rax_1248 = base_decode_ctx;
				((word64) rsp_1022 + 56)->u0 = 4200;
				goto l0000000000002BD0;
			}
			rbp_1039 = stdout;
			r13_1100 = xmalloc(0x7800);
			word64 rax_293;
			base_length();
			rax_1009 = xmalloc((int64) (word32) rax_293);
			*((word64) rsp_1022 + 16) = r15_1087;
			*rsp_1022 = rax_1009;
			rbx_1028.u0 = 0x00;
			do
			{
				r14_1001.u0 = 0x00;
				do
				{
					fn0000000000002400();
					rax_1009 = (uint64) *r12_1353;
					rsi_1006 = 0x01;
					r14_1001 += rax_1009;
					al_1014 = (byte) rax_1009;
					if ((al_1014 & 0x30) != 0x00)
					{
						if (r14_1001 <= 0x00)
							goto l0000000000002D1B;
						break;
					}
				} while (r14_1001 <= 0x77FF);
l0000000000002D62:
				Eq_11 r10_764 = base_encode;
				*((word64) rsp_1022 + 8) = r10_764;
				word32 r14d_765 = (word32) r14_1001;
				word64 rax_782;
				base_length();
				word32 eax_793 = (word32) rax_782;
				word64 rsi_796;
				(*((word64) rsp_1022 + 8))();
				word64 rax_808;
				base_length();
				word32 eax_817 = (word32) rax_808;
				if (*((word64) rsp_1022 + 16) != 0x00)
				{
					Eq_11 r15_818 = (int64) eax_817;
					if (r15_818 > 0x00)
					{
						*((word64) rsp_1022 + 8) = r12_1353;
						rbx_1028 = *((word64) rsp_1022 + 16);
						*((word64) rsp_1022 + 24) = r13_1100;
						*((word64) rsp_1022 + 32) = r14_1001;
						Eq_11 r12_824 = rbx_1028;
						r13_1100 = rbp_1039;
						rbp_1039 = r15_818;
						r14_1001.u0 = 0x00;
						do
						{
							Eq_11 r15_836 = r15_818 - r14_1001;
							Eq_11 rax_838 = rbx_1028 - r12_824;
							if (r15_836 > rax_838)
								r15_836 = rax_838;
							if (r15_836 != 0x00)
							{
								if (r15_836 >u fn00000000000025D0(stdout, r15_836, 0x01, Mem849[rsp_1022 + 0x00:word64] + r14_1001))
									goto l000000000000307B;
								r12_824 += r15_836;
								r14_1001 += r15_836;
							}
							else
							{
								byte * rax_850 = *((word64) rbp_1039 + 40);
								if (rax_850 < *((word64) rbp_1039 + 48))
								{
									*((word64) rbp_1039 + 40) = rax_850 + 1;
									*rax_850 = 0x0A;
								}
								else if (fn00000000000024B0(0x0A, rbp_1039) == 0x01)
								{
									Eq_11 rax_870 = fn0000000000002440(0x05, "write error", null);
									fn0000000000002620(rax_870, fn00000000000023B0()[0], 0x01);
									r12_1353 = rax_870;
									eax_817 = 0x00;
									goto l0000000000002EAD;
								}
								r12_824.u0 = 0x00;
							}
						} while (r14_1001 < r15_818);
						rbx_1028 = r12_824;
						r12_1353 = *((word64) rsp_1022 + 8);
						r13_1100 = *((word64) rsp_1022 + 24);
						r14_1001 = *((word64) rsp_1022 + 32);
					}
				}
				else
				{
l0000000000002EAD:
					Eq_11 r10_884 = (int64) eax_817;
					Eq_11 rcx_886 = stdout;
					Eq_11 rdi_890 = *rsp_1022;
					*((word64) rsp_1022 + 8) = r10_884;
					if (*((word64) rsp_1022 + 8) > fn00000000000025D0(rcx_886, r10_884, 0x01, rdi_890))
					{
						fn0000000000002620(fn0000000000002440(0x05, "write error", null), fn00000000000023B0()[0], 0x01);
						goto l0000000000002F07;
					}
				}
				rax_1009 = (uint64) *r12_1353;
				al_1014 = (byte) rax_1009;
			} while ((al_1014 & 0x30) == 0x00 && r14_1001 == 0x7800);
l0000000000002D1B:
			Eq_11 r15_1027 = *((word64) rsp_1022 + 16);
			if (rbx_1028 <= 0x00 || r15_1027 == 0x00)
			{
l0000000000002D2E:
				if ((al_1014 & 0x20) != 0x00)
				{
					Eq_11 rax_750 = fn0000000000002440(0x05, "read error", null);
					rsi_1006 = (uint64) fn00000000000023B0()[0];
					fn0000000000002620(rax_750, (word32) rsi_1006, 0x01);
					r12_1353 = rax_750;
					goto l0000000000002D62;
				}
l0000000000002C4C:
				finish_and_exit(*((word64) rsp_1022 + 40), r12_1353);
				goto l0000000000002C59;
			}
			byte * rax_1044 = *((word64) rbp_1039 + 40);
			if (rax_1044 < *((word64) rbp_1039 + 48))
			{
				*((word64) rbp_1039 + 40) = rax_1044 + 1;
				*rax_1044 = 0x0A;
l0000000000002F46:
				al_1014 = *r12_1353;
				goto l0000000000002D2E;
			}
l0000000000002FCB:
			if (fn00000000000024B0(0x0A, rbp_1039) != 0x01)
				goto l0000000000002F46;
			fn0000000000002620(fn0000000000002440(0x05, "write error", null), fn00000000000023B0()[0], 0x01);
l000000000000300D:
			quotearg_n_style_colon(0x03, 0x00, fs);
			fn0000000000002620(0x9840, fn00000000000023B0()[0], 0x01);
			goto l000000000000303E;
		}
	}
	else
		((word64) rsp_1022 + 40)->u0 = 0x9810;
	r12_1353 = stdin;
	goto l0000000000002957;
}