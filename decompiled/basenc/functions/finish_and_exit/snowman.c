int64_t finish_and_exit(unsigned char* rdi, struct s4* rsi, unsigned char* rdx, unsigned char* rcx, void* r8) {
    struct s4* r12_6;
    int64_t rax7;
    uint32_t eax8;
    uint32_t ebx9;

    r12_6 = rsi;
    rax7 = rpl_fclose(rdi);
    if (!*reinterpret_cast<int32_t*>(&rax7)) {
        fun_2660();
    } else {
        eax8 = r12_6->f0 - 45;
        ebx9 = eax8;
        if (eax8) 
            goto addr_373c_4;
        while (1) {
            ebx9 = r12_6->f1;
            addr_373c_4:
            fun_23b0();
            if (!ebx9) {
                fun_2440();
                fun_2620();
            }
            quotearg_n_style_colon();
            fun_2620();
        }
    }
}