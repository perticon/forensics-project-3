finish_and_exit (FILE *in, char const *infile)
{
  if (fclose (in) != 0)
    {
      if (STREQ (infile, "-"))
        die (EXIT_FAILURE, errno, _("closing standard input"));
      else
        die (EXIT_FAILURE, errno, "%s", quotef (infile));
    }

  exit (EXIT_SUCCESS);
}