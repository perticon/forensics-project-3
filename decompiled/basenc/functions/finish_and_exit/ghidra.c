void finish_and_exit(undefined8 param_1,byte *param_2)

{
  int iVar1;
  uint uVar2;
  int *piVar3;
  undefined8 uVar4;
  
  iVar1 = rpl_fclose();
  if (iVar1 != 0) {
    uVar2 = *param_2 - 0x2d;
    if (uVar2 == 0) goto LAB_00103795;
    do {
      piVar3 = __errno_location();
      if (uVar2 == 0) {
        uVar4 = dcgettext(0,"closing standard input",5);
        error(1,*piVar3,uVar4);
      }
      uVar4 = quotearg_n_style_colon(0,3,param_2);
      error(1,*piVar3,"%s",uVar4);
LAB_00103795:
      uVar2 = (uint)param_2[1];
    } while( true );
  }
                    /* WARNING: Subroutine does not return */
  exit(0);
}