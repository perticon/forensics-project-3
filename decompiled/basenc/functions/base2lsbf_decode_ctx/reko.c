void base2lsbf_decode_ctx(struct Eq_2130 * rcx, word64 rdx, word64 rsi, struct Eq_2133 * rdi, word64 * r8)
{
	*r8 = 0x00;
	struct Eq_2130 * r9_64 = rcx;
	if (rdx == 0x00)
		return;
	struct Eq_2142 * rsi_21 = rsi + 0x01;
	struct Eq_2142 * rdx_22 = rdx + (rsi + 0x01);
	do
	{
		uint64 rax_27 = (uint64) rsi_21->bFFFFFFFF;
		byte al_28 = (byte) rax_27;
		if (al_28 != 0x0A)
		{
			if ((byte) rax_27 > 0x31 || (byte) rax_27 < 0x30)
				return;
			uint64 rcx_44 = (uint64) rdi->dw0000;
			byte al_57 = (byte) (al_28 == 0x31) << (byte) rcx_44 | rdi->b0004;
			rdi->b0004 = al_57;
			word32 ecx_59 = (word32) rcx_44 + 0x01;
			rdi->dw0000 = ecx_59;
			if (ecx_59 == 0x08)
			{
				rdi->b0004 = 0x00;
				++*r8;
				++r9_64;
				r9_64->bFFFFFFFF = al_57;
				rdi->dw0000 = 0x00;
			}
		}
		++rsi_21;
	} while (rdx_22 != rsi_21);
}