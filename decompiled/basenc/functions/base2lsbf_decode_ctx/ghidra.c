uint base2lsbf_decode_ctx(uint *param_1,long param_2,long param_3,byte *param_4,long *param_5)

{
  char cVar1;
  byte bVar2;
  uint uVar3;
  byte *pbVar4;
  
  *param_5 = 0;
  if (param_3 == 0) {
    return *param_1 & 0xffffff00 | (uint)(*param_1 == 0);
  }
  param_2 = param_2 + 1;
  param_3 = param_3 + param_2;
  do {
    cVar1 = *(char *)(param_2 + -1);
    pbVar4 = param_4;
    if (cVar1 != '\n') {
      if (1 < (byte)(cVar1 - 0x30U)) {
        return 0;
      }
      uVar3 = *param_1 + 1;
      bVar2 = (cVar1 == '1') << ((byte)*param_1 & 0x1f) | *(byte *)(param_1 + 1);
      *(byte *)(param_1 + 1) = bVar2;
      *param_1 = uVar3;
      if (uVar3 == 8) {
        *(undefined *)(param_1 + 1) = 0;
        pbVar4 = param_4 + 1;
        *param_5 = *param_5 + 1;
        *param_4 = bVar2;
        *param_1 = 0;
      }
    }
    param_2 = param_2 + 1;
    param_4 = pbVar4;
  } while (param_3 != param_2);
  return 1;
}