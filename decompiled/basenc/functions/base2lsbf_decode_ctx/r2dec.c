uint32_t base2lsbf_decode_ctx (int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r9 = rcx;
    if (rdx == 0) {
        goto label_0;
    }
    rsi++;
    rdx += rsi;
    do {
        eax = *((rsi - 1));
        if (al != 0xa) {
            ecx = rax - 0x30;
            if (cl > 1) {
                goto label_1;
            }
            ecx = *(rdi);
            al = (al == 0x31) ? 1 : 0;
            eax = (int32_t) al;
            eax <<= cl;
            ecx++;
            al |= *((rdi + 4));
            *((rdi + 4)) = al;
            *(rdi) = ecx;
            if (ecx != 8) {
                goto label_2;
            }
            *((rdi + 4)) = 0;
            r9++;
            *(r8)++;
            *((r9 - 1)) = al;
            *(rdi) = 0;
        }
label_2:
        rsi++;
    } while (rdx != rsi);
    eax = 1;
    return eax;
label_0:
    eax = *(rdi);
    al = (eax == 0) ? 1 : 0;
    return eax;
label_1:
    eax = 0;
    return eax;
}