ulong isz85(char param_1)

{
  char *pcVar1;
  
  if (param_1 < '[') {
    if ('@' < param_1) {
      return 1;
    }
    if ((byte)(param_1 - 0x30U) < 10) {
      return 1;
    }
  }
  else if ((byte)(param_1 + 0x9fU) < 0x1a) {
    return 1;
  }
  pcVar1 = strchr(".-:+=^!/*?&<>()[]{}@%$#",(int)param_1);
  return (ulong)pcVar1 & 0xffffffffffffff00 | (ulong)(pcVar1 != (char *)0x0);
}