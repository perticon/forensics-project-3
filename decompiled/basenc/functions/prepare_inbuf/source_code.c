prepare_inbuf (struct base_decode_context *ctx, idx_t inlen)
{
  if (ctx->bufsize < inlen)
    {
      ctx->bufsize = inlen * 2;
      ctx->inbuf = xnrealloc (ctx->inbuf, ctx->bufsize, sizeof (char));
    }
}