void z85_encode(Eq_1472 rcx, word64 rdx, int64 rsi, byte * rdi, struct Eq_1476 * fs)
{
	ptr64 fp;
	byte bLoc33;
	byte bLoc31;
	byte bLoc32;
	word64 rax_21 = fs->qw0028;
	if (rsi != 0x00)
	{
		byte al_42 = *rdi;
		byte * r8_32 = rdi + 1;
		byte * r9_33 = rdi + rsi;
		word64 rbp_135 = rdx;
		Eq_1472 r10_136 = 0x00;
		word32 eax_154 = 0x01;
		if (r9_33 != rdi + 1)
		{
			uint64 rax_365 = 0x01;
			do
			{
				uint64 rax_144;
				word32 eax_53 = (word32) rax_365;
				(fp - 0x34)[(int64) eax_53] = *r8_32;
				++r8_32;
				if (eax_53 != 0x03)
					rax_144 = (uint64) (eax_53 + 0x01);
				else
				{
					Eq_1472 rdi_101 = (word64) r10_136.u1 + 4;
					byte (* r12_75)[] = rbp_135 - r10_136;
					Eq_1516 rsi_100 = (uint64) bLoc31 + ((((uint64) al_42 << 0x18) + (int64) ((word32) bLoc33 << 0x10)) + (int64) ((word32) bLoc32 << 0x08));
					while (true)
					{
						rsi_100 = SLICE(rsi_100 *128 0xC0C0C0C0C0C0C0C1, word64, 64) >> 0x06;
						if (rdi_101 < rcx)
						{
							int64 rdx_108 = SLICE(rsi_100 *s128 0x6060606060606061, word64, 64);
							Mem124[r12_75 + rdi_101:byte] = Mem119[0x00000000000099C0<p64> + CONVERT(SLICE(rsi_100, word32, 0) - (SLICE(rdx_108 >> 0x05, word32, 0) + SLICE(rdx_108 >> 0x05, word32, 0) * 0x04) * 0x11, word32, int64):byte];
						}
						Eq_1472 rax_126 = rdi_101 - 1;
						if (rdi_101 == r10_136)
							break;
						rdi_101 = rax_126;
					}
					rbp_135 += 0x05;
					r10_136 = (word64) r10_136.u1 + 5;
					rax_144 = 0x00;
				}
				eax_154 = (word32) rax_144;
				rax_365 = rax_144;
			} while (r9_33 != r8_32);
		}
		if (eax_154 != 0x00)
		{
			fn0000000000002620(fn0000000000002440(0x05, "invalid input (length must be multiple of 4 characters)", null), 0x00, 0x01);
			isbase64url(0x01);
			return;
		}
	}
	if (rax_21 - fs->qw0028 == 0x00)
		return;
	fn0000000000002470();
}