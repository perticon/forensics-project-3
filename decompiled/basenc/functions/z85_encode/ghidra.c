void z85_encode(byte *param_1,long param_2,long param_3,long param_4)

{
  int iVar1;
  undefined8 uVar2;
  ulong uVar3;
  byte bVar4;
  long lVar5;
  byte *pbVar6;
  long lVar7;
  long in_FS_OFFSET;
  bool bVar8;
  byte local_34 [4];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_2 != 0) {
    local_34[0] = *param_1;
    pbVar6 = param_1 + 1;
    lVar7 = 0;
    iVar1 = 1;
    while (param_1 + param_2 != pbVar6) {
      bVar4 = *pbVar6;
      pbVar6 = pbVar6 + 1;
      local_34[iVar1] = bVar4;
      bVar8 = iVar1 == 3;
      iVar1 = iVar1 + 1;
      if (bVar8) {
        uVar3 = (ulong)local_34[3] +
                (ulong)local_34[0] * 0x1000000 + (long)(int)((uint)local_34[1] << 0x10) +
                (long)(int)((uint)local_34[2] << 8);
        lVar5 = lVar7 + 4;
        do {
          if (lVar5 < param_4) {
            *(char *)((param_3 - lVar7) + lVar5) =
                 "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#"
                 [(int)uVar3 + (int)(uVar3 / 0x55) * -0x55];
          }
          bVar8 = lVar5 != lVar7;
          uVar3 = uVar3 / 0x55;
          lVar5 = lVar5 + -1;
        } while (bVar8);
        param_3 = param_3 + 5;
        lVar7 = lVar7 + 5;
        iVar1 = 0;
      }
    }
    if (iVar1 != 0) {
      uVar2 = dcgettext(0,"invalid input (length must be multiple of 4 characters)",5);
      bVar4 = 1;
      error(1,0,uVar2);
      if ((bVar4 != 0x2d && bVar4 != 0x5f) && ((bVar4 & 0xfb) != 0x2b)) {
        isbase64((int)(char)bVar4);
        return;
      }
      return;
    }
  }
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}