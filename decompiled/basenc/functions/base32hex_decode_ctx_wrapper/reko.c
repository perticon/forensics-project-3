void base32hex_decode_ctx_wrapper(Eq_11 rax, word64 rcx, Eq_1744 rdx, byte (* rsi)[], struct Eq_2443 * rdi, word64 * r8, struct Eq_1749 * fs)
{
	Eq_11 r9_111 = rdi->t0010;
	if (rdx > rdi->t0018)
	{
		rdi->t0018 = rdx * 0x02;
		xreallocarray(rax, 0x01, rdx * 0x02, r9_111);
		rdi->t0010 = rax;
		r9_111 = rax;
	}
	if (rdx != 0x00)
	{
		Eq_1744 rdx_48 = 0x00;
		do
		{
			word32 eax_56 = CONVERT(Mem55[rsi + rdx_48:byte], byte, int32);
			byte al_69 = (byte) eax_56;
			if ((byte) eax_56 > 0x39 && ((byte) eax_56 > 0x56 || (byte) eax_56 < 0x41))
			{
				Mem71[r9_111 + rdx_48:byte] = al_69;
				rdx_48 = (word64) rdx_48 + 1;
				if (rdx != rdx_48)
					continue;
				break;
			}
			Mem89[r9_111 + rdx_48:byte] = Mem55[0x0000000000009A40<p64> + CONVERT(eax_56 - 0x30, word32, int64):byte];
			rdx_48 = (word64) rdx_48 + 1;
		} while (rdx != rdx_48);
		r9_111 = rdi->t0010;
	}
	base32_decode_ctx(rdx, r9_111, &rdi->dw0004, r8, (word32) rcx, fs);
	rdi->dw0000 = rdi->dw0004;
}