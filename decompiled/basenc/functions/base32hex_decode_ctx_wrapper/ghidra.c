void base32hex_decode_ctx_wrapper
               (undefined4 *param_1,long param_2,long param_3,undefined8 param_4,undefined8 param_5)

{
  char cVar1;
  long lVar2;
  long lVar3;
  
  lVar2 = *(long *)(param_1 + 4);
  if (*(long *)(param_1 + 6) < param_3) {
    *(long *)(param_1 + 6) = param_3 * 2;
    lVar2 = xreallocarray(lVar2,param_3 * 2,1);
    *(long *)(param_1 + 4) = lVar2;
  }
  if (param_3 != 0) {
    lVar3 = 0;
    do {
      while( true ) {
        cVar1 = *(char *)(param_2 + lVar3);
        if ((9 < (byte)(cVar1 - 0x30U)) && (0x15 < (byte)(cVar1 + 0xbfU))) break;
        *(char *)(lVar2 + lVar3) = "ABCDEFGHIJ:;<=>?@KLMNOPQRSTUVWXYZ234567"[cVar1 + -0x30];
        lVar3 = lVar3 + 1;
        if (param_3 == lVar3) goto LAB_00103c8f;
      }
      *(char *)(lVar2 + lVar3) = cVar1;
      lVar3 = lVar3 + 1;
    } while (param_3 != lVar3);
LAB_00103c8f:
    lVar2 = *(long *)(param_1 + 4);
  }
  base32_decode_ctx(param_1 + 1,lVar2,param_3,param_4,param_5);
  *param_1 = param_1[1];
  return;
}