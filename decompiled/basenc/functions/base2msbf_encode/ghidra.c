void base2msbf_encode(char *param_1,long param_2,long param_3)

{
  char *pcVar1;
  char *pcVar2;
  char cVar3;
  char *pcVar4;
  char *pcVar5;
  
  if (param_2 != 0) {
    pcVar5 = (char *)(param_3 + 8);
    pcVar4 = param_1 + param_2;
    do {
      pcVar1 = pcVar5 + -8;
      cVar3 = *param_1;
      do {
        pcVar2 = pcVar1 + 1;
        *pcVar1 = '0' - (cVar3 >> 7);
        pcVar1 = pcVar2;
        cVar3 = cVar3 * '\x02';
      } while (pcVar2 != pcVar5);
      param_1 = param_1 + 1;
      pcVar5 = pcVar5 + 8;
    } while (param_1 != pcVar4);
  }
  return;
}