void base2msbf_encode(word64 rdx, word64 rsi, byte * rdi)
{
	if (rsi != 0x00)
	{
		struct Eq_1397 * r8_17 = rdx + 8;
		byte * rsi_11 = rdi + rsi;
		do
		{
			ui32 ecx_19 = (word32) *rdi;
			struct Eq_1397 * rax_18 = r8_17 - 8;
			do
			{
				uint64 rdx_21 = (uint64) ecx_19;
				++rax_18;
				rax_18->bFFFFFFFF = ((byte) rdx_21 >> 0x07) + 0x30;
				ecx_19 *= 0x02;
			} while (rax_18 != r8_17);
			++rdi;
			r8_17 += 8;
		} while (rdi != rsi_11);
	}
}