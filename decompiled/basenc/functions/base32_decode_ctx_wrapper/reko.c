void base32_decode_ctx_wrapper(Eq_1744 rdx, Eq_11 rsi, struct Eq_1746 * rdi, word64 * r8, word64 r13, struct Eq_1749 * fs)
{
	base32_decode_ctx(rdx, rsi, &rdi->dw0004, r8, (word32) r13, fs);
	rdi->dw0000 = rdi->dw0004;
}