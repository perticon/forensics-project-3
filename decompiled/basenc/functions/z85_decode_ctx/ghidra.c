uint z85_decode_ctx(undefined4 *param_1,long param_2,long param_3,uint *param_4,long *param_5)

{
  byte bVar1;
  char cVar2;
  int iVar3;
  uint uVar4;
  long lVar5;
  int iVar6;
  
  *param_5 = 0;
  if (param_3 == 0) {
    return param_1[1] & 0xffffff00 | (uint)((int)param_1[1] < 1);
  }
  param_2 = param_2 + 1;
  param_3 = param_3 + param_2;
  do {
    bVar1 = *(byte *)(param_2 + -1);
    if (bVar1 != 10) {
      if ((0x5c < (byte)(bVar1 - 0x21)) || (cVar2 = z85_decoding[(int)(bVar1 - 0x21)], cVar2 < '\0')
         ) {
        return 0;
      }
      iVar3 = param_1[1];
      iVar6 = iVar3 + 1;
      param_1[1] = iVar6;
      *(char *)((long)param_1 + (long)iVar3 + 8) = cVar2;
      if (iVar6 == 5) {
        lVar5 = (long)(int)((uint)*(byte *)((long)param_1 + 9) * 0x95eed +
                            (uint)*(byte *)((long)param_1 + 10) * 0x1c39 +
                            (uint)*(byte *)((long)param_1 + 0xb) * 0x55 +
                           (uint)*(byte *)(param_1 + 3)) + (ulong)*(byte *)(param_1 + 2) * 0x31c84b1
        ;
        if ((lVar5 >> 0x18 & 0x700U) != 0) {
          return 0;
        }
        *param_5 = *param_5 + 4;
        uVar4 = (uint)lVar5;
        *param_4 = uVar4 >> 0x18 | (uVar4 & 0xff0000) >> 8 | (uVar4 & 0xff00) << 8 | uVar4 << 0x18;
        param_1[1] = 0;
        param_4 = param_4 + 1;
      }
    }
    param_2 = param_2 + 1;
    if (param_3 == param_2) {
      *param_1 = param_1[1];
      return 1;
    }
  } while( true );
}