void z85_decode_ctx(struct Eq_2215 * rcx, word64 rdx, word64 rsi, struct Eq_2218 * rdi, word64 * r8)
{
	*r8 = 0x00;
	struct Eq_2215 * r9_115 = rcx;
	if (rdx == 0x00)
		return;
	struct Eq_2227 * rcx_109 = rsi + 1;
	struct Eq_2227 * rdx_22 = rdx + (rsi + 1);
	do
	{
		uint64 rax_27 = (uint64) rcx_109->bFFFFFFFF;
		word32 eax_36 = (word32) rax_27;
		if ((byte) rax_27 != 0x0A)
		{
			if ((byte) rax_27 <= 0x7D && (byte) rax_27 >= 33)
			{
				ci8 r10b_44 = g_a9960[(int64) (eax_36 - 33)];
				if (r10b_44 < 0x00)
					return;
				int64 rax_49 = (int64) rdi->dw0004;
				word32 r11d_52 = (word32) rax_49 + 0x01;
				rdi->dw0004 = r11d_52;
				rdi->a0008[rax_49].u0 = r10b_44;
				if (r11d_52 != 0x05)
					goto l0000000000003A90;
				int64 rax_97 = (int64) ((word32) rdi->b0009 *s 0x00095EED + (word32) rdi->b000A *s 7225 + (word32) rdi->b000B *s 0x55 + (word32) rdi->b000C) + (uint64) (rdi->a0008)[0] *s 52200625;
				word32 eax_113 = (word32) rax_97;
				if (((word32) (rax_97 >> 0x18) & 0x0700) == 0x00)
				{
					*r8 += 0x04;
					++r9_115;
					r9_115->dwFFFFFFFC = __bswap<word32>(eax_113);
					rdi->dw0004 = 0x00;
					++rcx_109;
					if (rdx_22 == rcx_109)
						break;
					continue;
				}
			}
			return;
		}
l0000000000003A90:
		++rcx_109;
	} while (rdx_22 != rcx_109);
	rdi->dw0000 = rdi->dw0004;
}