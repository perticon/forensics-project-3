int64_t z85_decode_ctx (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r9 = rcx;
    if (rdx == 0) {
        goto label_2;
    }
    rcx = rsi + 1;
    rsi = obj_z85_decoding;
    rdx += rcx;
    while (al == 0xa) {
label_0:
        rcx++;
        if (rdx == rcx) {
            goto label_3;
        }
label_1:
        eax = *((rcx - 1));
    }
    r10d = rax - 0x21;
    if (r10b > 0x5c) {
        goto label_4;
    }
    eax -= 0x21;
    rax = (int64_t) eax;
    r10d = *((rsi + rax));
    if (r10b < 0) {
        goto label_4;
    }
    rax = *((rdi + 4));
    r11d = rax + 1;
    *((rdi + 4)) = r11d;
    *((rdi + rax + 8)) = r10b;
    if (r11d != 5) {
        goto label_0;
    }
    eax = *((rdi + 9));
    r10d = *((rdi + 0xa));
    eax *= 0x95eed;
    r10d *= 0x1c39;
    eax += r10d;
    r10d = *((rdi + 0xb));
    r10d *= 0x55;
    eax += r10d;
    r10d = *((rdi + 0xc));
    eax += r10d;
    r10d = *((rdi + 8));
    rax = (int64_t) eax;
    r10 *= 0x31c84b1;
    rax += r10;
    r10 = rax;
    r10 >>= 0x18;
    if ((r10d & 0x700) != 0) {
        goto label_4;
    }
    rcx++;
    *(r8) += 4;
    eax = SWAP32 (eax);
    r9 += 4;
    *((r9 - 4)) = eax;
    *((rdi + 4)) = 0;
    if (rdx != rcx) {
        goto label_1;
    }
label_3:
    eax = *((rdi + 4));
    *(rdi) = eax;
    eax = 1;
    return rax;
label_2:
    eax = *((rdi + 4));
    al = (eax <= 0) ? 1 : 0;
    return rax;
label_4:
    eax = 0;
    return rax;
}