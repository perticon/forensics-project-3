base16_decode_ctx (struct base_decode_context *ctx,
                   char const *restrict in, idx_t inlen,
                   char *restrict out, idx_t *outlen)
{
  bool ignore_lines = true;  /* for now, always ignore them */

  *outlen = 0;

  /* inlen==0 is request to flush output.
     if there is a dangling high nibble - we are missing the low nibble,
     so return false - indicating an invalid input.  */
  if (inlen == 0)
    return !ctx->ctx.base16.have_nibble;

  while (inlen--)
    {
      if (ignore_lines && *in == '\n')
        {
          ++in;
          continue;
        }

      int nib = *in++;
      if ('0' <= nib && nib <= '9')
        nib -= '0';
      else if ('A' <= nib && nib <= 'F')
        nib -= 'A' - 10;
      else
        return false; /* garbage - return false */

      if (ctx->ctx.base16.have_nibble)
        {
          /* have both nibbles, write octet */
          *out++ = (ctx->ctx.base16.nibble << 4) + nib;
          ++(*outlen);
        }
      else
        {
          /* Store higher nibble until next one arrives */
          ctx->ctx.base16.nibble = nib;
        }
      ctx->ctx.base16.have_nibble = !ctx->ctx.base16.have_nibble;
    }
  return true;
}