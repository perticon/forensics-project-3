int32_t base16_decode_ctx (int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r9 = rcx;
    r10 = rsi + rdx;
    if (rdx != 0) {
        goto label_2;
    }
    goto label_3;
label_0:
    ecx = *((rdi + 4));
    *(r8)++;
    r9++;
    ecx <<= 4;
    edx += ecx;
    *((r9 - 1)) = dl;
label_1:
    eax ^= 1;
    *((rdi + 5)) = al;
    do {
        if (rsi == r10) {
            goto label_4;
        }
label_2:
        eax = *(rsi);
        rsi++;
    } while (al == 0xa);
    edx = rax - 0x30;
    if (edx > 9) {
        edx = rax - 0x41;
        if (edx > 5) {
            goto label_5;
        }
        edx = rax - 0x37;
    }
    eax = *((rdi + 5));
    if (al != 0) {
        goto label_0;
    }
    *((rdi + 4)) = dl;
    goto label_1;
label_4:
    eax = 1;
    return eax;
label_3:
    eax = *((rdi + 5));
    eax ^= 1;
    return eax;
label_5:
    eax = 0;
    return eax;
}