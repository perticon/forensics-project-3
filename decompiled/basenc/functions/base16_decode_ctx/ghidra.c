byte base16_decode_ctx(long param_1,char *param_2,long param_3,char *param_4,long *param_5)

{
  char *pcVar1;
  char cVar2;
  byte bVar3;
  int iVar4;
  uint uVar5;
  char *pcVar6;
  
  *param_5 = 0;
  pcVar1 = param_2 + param_3;
  if (param_3 == 0) {
    return *(byte *)(param_1 + 5) ^ 1;
  }
  do {
    cVar2 = *param_2;
    iVar4 = (int)cVar2;
    param_2 = param_2 + 1;
    pcVar6 = param_4;
    if (cVar2 != '\n') {
      uVar5 = iVar4 - 0x30;
      if (9 < uVar5) {
        if (5 < iVar4 - 0x41U) {
          return 0;
        }
        uVar5 = iVar4 - 0x37;
      }
      bVar3 = *(byte *)(param_1 + 5);
      if (bVar3 == 0) {
        *(char *)(param_1 + 4) = (char)uVar5;
      }
      else {
        cVar2 = *(char *)(param_1 + 4);
        *param_5 = *param_5 + 1;
        pcVar6 = param_4 + 1;
        *param_4 = (char)uVar5 + cVar2 * '\x10';
      }
      *(byte *)(param_1 + 5) = bVar3 ^ 1;
    }
    param_4 = pcVar6;
  } while (param_2 != pcVar1);
  return 1;
}