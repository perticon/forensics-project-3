void base16_decode_ctx(struct Eq_2353 * rcx, word64 rdx, byte * rsi, struct Eq_2356 * rdi, word64 * r8)
{
	*r8 = 0x00;
	struct Eq_2353 * r9_109 = rcx;
	byte * r10_10 = rsi + rdx;
	if (rdx == 0x00)
		return;
	do
	{
		Eq_2367 eax_16 = (int32) *rsi;
		++rsi;
		if ((byte) eax_16 != 0x0A)
		{
			uint64 rdx_151 = (uint64) (eax_16 - 0x30);
			if ((word32) rdx_151 > 0x09)
			{
				if (eax_16 > 0x46 || eax_16 < 0x41)
					return;
				rdx_151 = (uint64) (eax_16 - 55);
			}
			word32 edx_61 = (word32) rdx_151;
			uint64 rax_44 = (uint64) rdi->b0005;
			byte dl_70 = (byte) edx_61;
			word32 eax_73 = (word32) rax_44;
			if ((byte) rax_44 == 0x00)
				rdi->b0004 = dl_70;
			else
			{
				word32 ecx_58 = (word32) rdi->b0004;
				++*r8;
				++r9_109;
				r9_109->bFFFFFFFF = (byte) edx_61 + ((byte) ecx_58 << 0x04);
			}
			rdi->b0005 = (byte) eax_73 ^ 0x01;
		}
	} while (rsi != r10_10);
}