isbase64url (char ch)
{
  return (ch == '-' || ch == '_'
          || (ch != '+' && ch != '/' && isbase64 (ch)));
}