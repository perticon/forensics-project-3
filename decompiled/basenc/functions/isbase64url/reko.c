void isbase64url(word32 edi)
{
	byte dil = (byte) edi;
	if (((int8) (dil == 0x2D) | (int8) (dil == 0x5F)) != 0x00 || ((byte) edi & ~0x04) == 0x2B)
		return;
	isbase64();
}