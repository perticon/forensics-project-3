void base2lsbf_encode(word64 rdx, word64 rsi, byte * rdi)
{
	if (rsi != 0x00)
	{
		struct Eq_1435 * r8_17 = rdx + 8;
		byte * rsi_11 = rdi + rsi;
		do
		{
			word32 ecx_19 = (word32) *rdi;
			struct Eq_1435 * rax_18 = r8_17 - 8;
			do
			{
				ecx_46 = SEQ(ecx_24_8_45, cl_24 >> 0x01);
				++rax_18;
				rax_18->bFFFFFFFF = ((byte) ecx_19 & 0x01) + 0x30;
				bcu8 cl_24 = (byte) ecx_19;
				word24 ecx_24_8_45 = SLICE(ecx_19, word24, 8);
				ecx_19 = ecx_46;
			} while (rax_18 != r8_17);
			++rdi;
			r8_17 += 8;
		} while (rsi_11 != rdi);
	}
}