void base2lsbf_encode(byte *param_1,long param_2,long param_3)

{
  char *pcVar1;
  char *pcVar2;
  byte bVar3;
  byte *pbVar4;
  char *pcVar5;
  
  if (param_2 != 0) {
    pcVar5 = (char *)(param_3 + 8);
    pbVar4 = param_1 + param_2;
    do {
      pcVar1 = pcVar5 + -8;
      bVar3 = *param_1;
      do {
        pcVar2 = pcVar1 + 1;
        *pcVar1 = (bVar3 & 1) + 0x30;
        pcVar1 = pcVar2;
        bVar3 = bVar3 >> 1;
      } while (pcVar2 != pcVar5);
      param_1 = param_1 + 1;
      pcVar5 = pcVar5 + 8;
    } while (pbVar4 != param_1);
  }
  return;
}