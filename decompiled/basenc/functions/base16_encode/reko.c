void base16_encode(struct Eq_1340 * rdx, word64 rsi, byte * rdi)
{
	if (rsi != 0x00)
	{
		byte * rsi_9 = rdi + rsi;
		do
		{
			ui32 eax_19 = (word32) *rdi;
			uint64 rcx_21 = (uint64) eax_19;
			++rdx;
			byte al_37 = g_a9A20[(uint64) (eax_19 & 0x0F)];
			rdx->bFFFFFFFE = g_a9A20[(uint64) (SEQ(SLICE(rcx_21, word24, 8), (byte) rcx_21 >> 0x04) & 0x0F)];
			rdx->bFFFFFFFF = al_37;
			++rdi;
		} while (rdi != rsi_9);
	}
}