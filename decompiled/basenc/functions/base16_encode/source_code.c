base16_encode (char const *restrict in, idx_t inlen,
               char *restrict out, idx_t outlen)
{
  while (inlen--)
    {
      unsigned char c = *in;
      *out++ = base16[c >> 4];
      *out++ = base16[c & 0x0F];
      ++in;
    }
}