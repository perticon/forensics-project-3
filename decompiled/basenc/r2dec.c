#include <stdint.h>

/* /tmp/tmpaqk9hiad @ 0x30b0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpaqk9hiad @ 0x5500 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x00009d6f;
        rdx = 0x00009d60;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x00009d67;
        rdx = 0x00009d69;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x00009d6b;
    rdx = 0x00009d64;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x8b00 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x25b0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpaqk9hiad @ 0x55e0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x26c0)() ();
    }
    rdx = 0x00009de0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x9de0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x00009d73;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x00009d69;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x00009e0c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x9e0c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x00009d67;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x00009d69;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x00009d67;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x00009d69;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x00009f0c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x9f0c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000a00c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xa00c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x00009d69;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x00009d67;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x00009d67;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x00009d69;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpaqk9hiad @ 0x26c0 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 26210 named .text */
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x6a00 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x26c5)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x26c5 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26ca */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x23a0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpaqk9hiad @ 0x26d0 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26d5 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26da */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26df */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26e4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26e9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26ee */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26f3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x26f8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x31a0 */
 
int64_t dbg_base64_length_wrapper (int64_t arg1) {
    rdi = arg1;
    /* int base64_length_wrapper(int len); */
    edi += 2;
    rax = (int64_t) edi;
    edi >>= 0x1f;
    rax *= 0x55555556;
    rax >>= 0x20;
    eax -= edi;
    eax <<= 2;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x31c0 */
 
int64_t dbg_base32_length_wrapper (int64_t arg1) {
    rdi = arg1;
    /* int base32_length_wrapper(int len); */
    edi += 4;
    rax = (int64_t) edi;
    edi >>= 0x1f;
    rax *= 0x66666667;
    rax >>= 0x21;
    eax -= edi;
    eax <<= 3;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x31e0 */
 
uint32_t isbase32hex (int64_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    eax = rdi - 0x30;
    al = (al <= 9) ? 1 : 0;
    edi -= 0x41;
    dl = (dil <= 0x15) ? 1 : 0;
    eax |= edx;
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x3200 */
 
uint32_t dbg_isbase16 (int64_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    /* _Bool isbase16(char ch); */
    eax = rdi - 0x30;
    al = (al <= 9) ? 1 : 0;
    edi -= 0x41;
    dl = (dil <= 5) ? 1 : 0;
    eax |= edx;
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x3220 */
 
uint32_t dbg_base16_length (int32_t len) {
    rdi = len;
    /* int base16_length(int len); */
    eax = rdi + rdi;
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x3230 */
 
uint32_t dbg_base16_encode (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void base16_encode(char const * restrict in,idx_t inlen,char * restrict out,idx_t outlen); */
    if (rsi == 0) {
        goto label_0;
    }
    rsi += rdi;
    r8 = "0123456789ABCDEF";
    do {
        eax = *(rdi);
        rdx += 2;
        rdi++;
        ecx = eax;
        eax &= 0xf;
        cl >>= 4;
        eax = *((r8 + rax));
        ecx &= 0xf;
        ecx = *((r8 + rcx));
        *((rdx - 2)) = cl;
        *((rdx - 1)) = al;
    } while (rdi != rsi);
label_0:
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x3280 */
 
uint32_t dbg_z85_length (int32_t len) {
    rdi = len;
    /* int z85_length(int len); */
    edx = rdi * 5;
    eax = rdx + 3;
    __asm ("cmovns eax, edx");
    eax >>= 2;
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x32a0 */
 
uint8_t isbase2 (int64_t arg1) {
    rdi = arg1;
    edi -= 0x30;
    al = (dil <= 1) ? 1 : 0;
    return al;
}

/* /tmp/tmpaqk9hiad @ 0x32b0 */
 
uint32_t dbg_base2_length (int32_t len) {
    rdi = len;
    /* int base2_length(int len); */
    eax = rdi*8;
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x32c0 */
 
uint64_t dbg_base2msbf_encode (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void base2msbf_encode(char const * restrict in,idx_t inlen,char * restrict out,idx_t outlen); */
    if (rsi == 0) {
        goto label_1;
    }
    r8 = rdx + 8;
    rsi += rdi;
label_0:
    ecx = *(rdi);
    rax = r8 - 8;
    do {
        edx = ecx;
        rax++;
        ecx += ecx;
        dl >>= 7;
        edx += 0x30;
        *((rax - 1)) = dl;
    } while (rax != r8);
    rdi++;
    r8 += 8;
    if (rdi != rsi) {
        goto label_0;
    }
label_1:
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x3310 */
 
uint64_t dbg_base2lsbf_encode (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void base2lsbf_encode(char const * restrict in,idx_t inlen,char * restrict out,idx_t outlen); */
    if (rsi == 0) {
        goto label_1;
    }
    r8 = rdx + 8;
    rsi += rdi;
label_0:
    ecx = *(rdi);
    rax = r8 - 8;
    do {
        edx = ecx;
        rax++;
        cl >>= 1;
        edx &= 1;
        edx += 0x30;
        *((rax - 1)) = dl;
    } while (rax != r8);
    rdi++;
    r8 += 8;
    if (rsi != rdi) {
        goto label_0;
    }
label_1:
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x3360 */
 
int64_t dbg_z85_encode (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    unsigned char[4] quad;
    int64_t var_4h;
    int64_t var_5h;
    int64_t var_6h;
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void z85_encode(char const * restrict in,idx_t inlen,char * restrict out,idx_t outlen); */
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rsi == 0) {
        goto label_1;
    }
    eax = *(rdi);
    r8 = rdi + 1;
    r9 = rdi + rsi;
    r11 = rcx;
    r10d = 0;
    rbx = 0xc0c0c0c0c0c0c0c1;
    r13 = 0x6060606060606061;
    *((rsp + 4)) = al;
    r14 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#";
    eax = 1;
    if (r9 == r8) {
        goto label_2;
    }
    do {
        esi = *(r8);
        rcx = (int64_t) eax;
        r8++;
        edx = rax + 1;
        *((rsp + rcx + 4)) = sil;
        if (eax == 3) {
            goto label_3;
        }
        eax = edx;
label_0:
    } while (r9 != r8);
label_2:
    if (eax != 0) {
        goto label_4;
    }
label_1:
    rax = *((rsp + 8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_5;
    }
    return rax;
label_3:
    edx = *((rsp + 5));
    eax = *((rsp + 4));
    r12 = rbp;
    rdi = r10 + 4;
    esi = *((rsp + 7));
    r12 -= r10;
    edx <<= 0x10;
    rax <<= 0x18;
    rdx = (int64_t) edx;
    rax += rdx;
    edx = *((rsp + 6));
    edx <<= 8;
    rdx = (int64_t) edx;
    rax += rdx;
    rsi += rax;
    do {
        rax = rsi;
        rcx = rsi;
        rdx:rax = rax * rbx;
        rsi = rdx;
        rsi >>= 6;
        if (rdi < r11) {
            rax = rcx;
            rdx:rax = rax * r13;
            rdx >>= 5;
            rax = rdx * 5;
            rdx = rax;
            rdx <<= 4;
            rax += rdx;
            rcx -= rax;
            rcx = (int64_t) ecx;
            eax = *((r14 + rcx));
            *((r12 + rdi)) = al;
        }
        rax = rdi - 1;
        if (rdi == r10) {
            goto label_6;
        }
        rdi = rax;
    } while (1);
label_6:
    edx = 0;
    rbp += 5;
    r10 += 5;
    eax = edx;
    goto label_0;
label_5:
    stack_chk_fail ();
label_4:
    edx = 5;
    rax = dcgettext (0, "invalid input (length must be multiple of 4 characters)");
    eax = 0;
    rax = error (1, 0, rax);
}

/* /tmp/tmpaqk9hiad @ 0x34e0 */
 
int8_t dbg_isbase64url (int64_t arg1) {
    rdi = arg1;
    /* _Bool isbase64url(char ch); */
    al = (dil == 0x2d) ? 1 : 0;
    dl = (dil == 0x5f) ? 1 : 0;
    al |= dl;
    if (al == 0) {
        edx = edi;
        edx &= 0xfffffffb;
        if (dl == 0x2b) {
            goto label_0;
        }
        edi = (int32_t) dil;
        void (*0x4e20)() ();
    }
label_0:
    return al;
}

/* /tmp/tmpaqk9hiad @ 0x3520 */
 
uint32_t dbg_base64url_encode (int64_t arg3, int64_t arg4) {
    rdx = arg3;
    rcx = arg4;
    /* void base64url_encode(char const * restrict in,idx_t inlen,char * restrict out,idx_t outlen); */
    rbx = rdx;
    al = base64_encode (rdi, rsi, rdx, rcx);
    if (rbp == 0) {
        goto label_1;
    }
    rcx = rbx + rbp;
    while (al != 0x2b) {
        if (al == 0x2f) {
            *(rbx) = 0x5f;
        }
        rbx++;
        if (rcx == rbx) {
            goto label_1;
        }
label_0:
        eax = *(rbx);
    }
    *(rbx) = 0x2d;
    rbx++;
    if (rcx != rbx) {
        goto label_0;
    }
label_1:
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x3570 */
 
uint64_t dbg_z85_decode_ctx_init (int64_t arg1) {
    rdi = arg1;
    /* void z85_decode_ctx_init(base_decode_context * ctx); */
    rbx = rdi;
    *((rdi + 0x18)) = 0x1068;
    rax = xcharalloc (0x1068);
    *(rbx) = 1;
    *((rbx + 0x10)) = rax;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x7f10 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x2590 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpaqk9hiad @ 0x8450 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x00009840);
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x2440 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpaqk9hiad @ 0x2620 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpaqk9hiad @ 0x25e0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpaqk9hiad @ 0x23f0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpaqk9hiad @ 0x35a0 */
 
uint64_t dbg_base2_decode_ctx_init (int64_t arg1) {
    rdi = arg1;
    /* void base2_decode_ctx_init(base_decode_context * ctx); */
    rbx = rdi;
    *((rdi + 0x18)) = 0x1068;
    rax = xcharalloc (0x1068);
    *((rbx + 4)) = 0;
    *((rbx + 0x10)) = rax;
    *(rbx) = 0;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x35d0 */
 
uint64_t dbg_base16_decode_ctx_init (int64_t arg1) {
    rdi = arg1;
    /* void base16_decode_ctx_init(base_decode_context * ctx); */
    rbx = rdi;
    *((rdi + 0x18)) = 0x1068;
    rax = xcharalloc (0x1068);
    *((rbx + 5)) = 0;
    *((rbx + 0x10)) = rax;
    *(rbx) = 1;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x3600 */
 
uint64_t dbg_isz85 (int64_t arg1) {
    rdi = arg1;
    /* _Bool isz85(char ch); */
    if (dil > 0x5a) {
        goto label_0;
    }
    eax = 1;
    if (dil > 0x40) {
        goto label_1;
    }
    edx = rdi - 0x30;
    if (dl <= 9) {
        goto label_2;
    }
    do {
        esi = (int32_t) dil;
        rax = strchr (".-:+=^!/*?&<>()[]{}@%$#", rsi);
        al = (rax != 0) ? 1 : 0;
        return rax;
label_0:
        edx = rdi - 0x61;
        eax = 1;
    } while (dl > 0x19);
label_1:
    return rax;
label_2:
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x3660 */
 
void dbg_base32_decode_ctx_wrapper (int64_t arg1) {
    rdi = arg1;
    /* _Bool base32_decode_ctx_wrapper(base_decode_context * ctx,char const * restrict in,idx_t inlen,char * restrict out,idx_t * outlen); */
    rbx = rdi;
    base32_decode_ctx (rdi + 4, rsi, rdx, rcx, r8);
    edx = *((rbx + 4));
    *(rbx) = edx;
}

/* /tmp/tmpaqk9hiad @ 0x46f0 */
 
int64_t dbg_base32_decode_ctx (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    idx_t outleft;
    uint32_t var_9h;
    uint32_t var_ah;
    uint32_t var_bh;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool base32_decode_ctx(base32_decode_context * ctx,char const * restrict in,idx_t inlen,char * restrict out,idx_t * outlen); */
    r14 = rdi;
    rbx = rsi;
    rsi = rdx;
    r15 = *(r8);
    *((rsp + 0x18)) = rcx;
    *((rsp + 0x10)) = r8;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    *((rsp + 0x20)) = r15;
    rsp + 0xb = (rdi != 0) ? 1 : 0;
    if (rdi == 0) {
        goto label_11;
    }
    edx = *(rdi);
    rsp + 9 = (rdx != 0) ? 1 : 0;
    eax = *((rsp + 9));
    r13b = (edx == 0) ? 1 : 0;
    r13d &= eax;
    *((rsp + 0xa)) = r13b;
label_7:
    rbp = rsp + 0x20;
    while (rsi != 0) {
        if (*(rbx) == 0xa) {
            goto label_12;
        }
        rax = r15;
        *((rsp + 0x20)) = r15;
        r15 = rbx + rsi;
        rax -= rdx;
        if (r14 != 0) {
            goto label_13;
        }
label_0:
        r13 = rsp + 0x18;
        al = decode_8 (rbx, rsi, r13, rbp);
        if (al == 0) {
            goto label_14;
        }
        rsi = r15;
        r15 = *((rsp + 0x20));
        rsi -= rbx;
label_2:
        rdx = r15;
        if (*((rsp + 0xa)) != 0) {
            goto label_15;
        }
label_1:
        al = (rsi == 0) ? 1 : 0;
        al &= *((rsp + 9));
        if (al != 0) {
            goto label_8;
        }
    }
    rax = r15;
    *((rsp + 0x20)) = r15;
    rax -= rdx;
    if (r14 == 0) {
        goto label_16;
    }
    ecx = *(r14);
    r15 = rbx;
    if (ecx == 8) {
        goto label_9;
    }
label_3:
    rsi = (int64_t) ecx;
    rdi = r14 + 4;
    if (rsi == 0) {
        goto label_17;
    }
label_6:
    if (rsi > 7) {
        goto label_0;
    }
    if (*((rsp + 9)) == 0) {
        goto label_0;
    }
    if (*((rsp + 0xb)) == 0) {
        goto label_0;
    }
label_17:
    rdx = *((rsp + 0x20));
    eax = 1;
label_8:
    rdi = *((rsp + 0x10));
    *(rdi) -= rdx;
    rdx = *((rsp + 0x28));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_18;
    }
    return rax;
label_15:
    r11 = rbx + rsi;
    r13 = rsp + 0x18;
    while (al != 0) {
        r15 = *((rsp + 0x20));
        rbx += 8;
        r12 = r11;
        r12 -= rbx;
        al = decode_8 (rbx, r12, r13, rbp);
    }
    rdx = *((rsp + 0x20));
    rsi = r12;
    goto label_1;
label_12:
    if (r14 == 0) {
        goto label_19;
    }
    rbx++;
    rsi--;
    r15 = rdx;
    goto label_2;
label_9:
    *(r14) = 0;
    eax = 1;
    ecx = 0;
label_10:
    if (rsi > 7) {
        if (al == 0) {
            goto label_20;
        }
        *((rsp + 0xc)) = ecx;
        rax = memchr (rbx, 0xa, 8);
        rcx = *((rsp + 0xc));
        if (rax == 0) {
            goto label_21;
        }
    }
label_20:
    if (r15 > rbx) {
        goto label_22;
    }
    goto label_3;
label_5:
    rcx = (int64_t) edx;
label_4:
    if (rbx == r15) {
        goto label_3;
    }
label_22:
    eax = *(rbx);
    rbx++;
    if (al == 0xa) {
        goto label_4;
    }
    edx = rcx + 1;
    *(r14) = edx;
    *((r14 + rcx + 4)) = al;
    if (edx != 8) {
        goto label_5;
    }
    ecx = 8;
    goto label_3;
label_19:
    rax = r15;
    *((rsp + 0x20)) = r15;
    rdi = rbx;
    r15 = rbx + rsi;
    rax -= rdx;
    goto label_6;
label_11:
    *((rsp + 0xa)) = 1;
    *((rsp + 9)) = 1;
    goto label_7;
label_16:
    rdx = r15;
    eax = 1;
    goto label_8;
label_14:
    rdx = *((rsp + 0x20));
    goto label_8;
label_21:
    rdi = rbx;
    esi = 8;
    rbx += 8;
    goto label_0;
label_13:
    rcx = *(r14);
    if (ecx == 8) {
        goto label_9;
    }
    al = (ecx == 0) ? 1 : 0;
    goto label_10;
label_18:
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x3680 */
 
uint64_t dbg_base32hex_decode_ctx_init_wrapper (int64_t arg1) {
    rdi = arg1;
    /* void base32hex_decode_ctx_init_wrapper(base_decode_context * ctx); */
    rbx = rdi;
    rdi = rdi + 4;
    base32_decode_ctx_init ();
    *((rbx + 0x18)) = 0x1068;
    rax = xcharalloc (0x1068);
    *((rbx + 0x10)) = rax;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x46e0 */
 
void dbg_base32_decode_ctx_init (base32_decode_context * ctx) {
    rdi = ctx;
    /* void base32_decode_ctx_init(base32_decode_context * ctx); */
    *(rdi) = 0;
}

/* /tmp/tmpaqk9hiad @ 0x36b0 */
 
void dbg_base32_decode_ctx_init_wrapper (int64_t arg1) {
    rdi = arg1;
    /* void base32_decode_ctx_init_wrapper(base_decode_context * ctx); */
    rdi += 4;
    return void (*0x46e0)() ();
}

/* /tmp/tmpaqk9hiad @ 0x36c0 */
 
void dbg_base64_decode_ctx_wrapper (int64_t arg1) {
    rdi = arg1;
    /* _Bool base64_decode_ctx_wrapper(base_decode_context * ctx,char const * restrict in,idx_t inlen,char * restrict out,idx_t * outlen); */
    rbx = rdi;
    base64_decode_ctx (rdi + 4, rsi, rdx, rcx, r8);
    edx = *((rbx + 4));
    *(rbx) = edx;
}

/* /tmp/tmpaqk9hiad @ 0x4e50 */
 
int64_t dbg_base64_decode_ctx (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    idx_t outleft;
    uint32_t var_9h;
    uint32_t var_ah;
    uint32_t var_bh;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool base64_decode_ctx(base64_decode_context * ctx,char const * restrict in,idx_t inlen,char * restrict out,idx_t * outlen); */
    r14 = rdi;
    rbx = rsi;
    rsi = rdx;
    r15 = *(r8);
    *((rsp + 0x18)) = rcx;
    *((rsp + 0x10)) = r8;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    *((rsp + 0x20)) = r15;
    rsp + 0xb = (rdi != 0) ? 1 : 0;
    if (rdi == 0) {
        goto label_11;
    }
    edx = *(rdi);
    rsp + 9 = (rdx != 0) ? 1 : 0;
    eax = *((rsp + 9));
    r13b = (edx == 0) ? 1 : 0;
    r13d &= eax;
    *((rsp + 0xa)) = r13b;
label_7:
    rbp = rsp + 0x20;
    while (rsi != 0) {
        if (*(rbx) == 0xa) {
            goto label_12;
        }
        rax = r15;
        *((rsp + 0x20)) = r15;
        r15 = rbx + rsi;
        rax -= rdx;
        if (r14 != 0) {
            goto label_13;
        }
label_0:
        r13 = rsp + 0x18;
        rcx = rbp;
        al = decode_4 (rbx, rsi, r13);
        if (al == 0) {
            goto label_14;
        }
        rsi = r15;
        r15 = *((rsp + 0x20));
        rsi -= rbx;
label_2:
        rdx = r15;
        if (*((rsp + 0xa)) != 0) {
            goto label_15;
        }
label_1:
        al = (rsi == 0) ? 1 : 0;
        al &= *((rsp + 9));
        if (al != 0) {
            goto label_8;
        }
    }
    rax = r15;
    *((rsp + 0x20)) = r15;
    rax -= rdx;
    if (r14 == 0) {
        goto label_16;
    }
    ecx = *(r14);
    r15 = rbx;
    if (ecx == 4) {
        goto label_9;
    }
label_3:
    rsi = (int64_t) ecx;
    rdi = r14 + 4;
    if (rsi == 0) {
        goto label_17;
    }
label_6:
    if (rsi > 3) {
        goto label_0;
    }
    if (*((rsp + 9)) == 0) {
        goto label_0;
    }
    if (*((rsp + 0xb)) == 0) {
        goto label_0;
    }
label_17:
    rdx = *((rsp + 0x20));
    eax = 1;
label_8:
    rdi = *((rsp + 0x10));
    *(rdi) -= rdx;
    rdx = *((rsp + 0x28));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_18;
    }
    return rax;
label_15:
    r12 = rbx;
    r13 = rsp + 0x18;
    rbx = rbx + rsi;
    while (al != 0) {
        r15 = *((rsp + 0x20));
        r12 += 4;
        rcx = rbp;
        rsi -= r12;
        al = decode_4 (r12, rbx, r13);
    }
    rdx = *((rsp + 0x20));
    rbx = r12;
    goto label_1;
label_12:
    if (r14 == 0) {
        goto label_19;
    }
    rbx++;
    rsi--;
    r15 = rdx;
    goto label_2;
label_9:
    *(r14) = 0;
    eax = 1;
    ecx = 0;
label_10:
    if (rsi > 3) {
        if (al == 0) {
            goto label_20;
        }
        *((rsp + 0xc)) = ecx;
        rax = memchr (rbx, 0xa, 4);
        rcx = *((rsp + 0xc));
        if (rax == 0) {
            goto label_21;
        }
    }
label_20:
    if (r15 > rbx) {
        goto label_22;
    }
    goto label_3;
label_5:
    rcx = (int64_t) edx;
label_4:
    if (rbx == r15) {
        goto label_3;
    }
label_22:
    eax = *(rbx);
    rbx++;
    if (al == 0xa) {
        goto label_4;
    }
    edx = rcx + 1;
    *(r14) = edx;
    *((r14 + rcx + 4)) = al;
    if (edx != 4) {
        goto label_5;
    }
    ecx = 4;
    goto label_3;
label_19:
    rax = r15;
    *((rsp + 0x20)) = r15;
    rdi = rbx;
    r15 = rbx + rsi;
    rax -= rdx;
    goto label_6;
label_11:
    *((rsp + 0xa)) = 1;
    *((rsp + 9)) = 1;
    goto label_7;
label_16:
    rdx = r15;
    eax = 1;
    goto label_8;
label_14:
    rdx = *((rsp + 0x20));
    goto label_8;
label_21:
    rdi = rbx;
    esi = 4;
    rbx += 4;
    goto label_0;
label_13:
    rcx = *(r14);
    if (ecx == 4) {
        goto label_9;
    }
    al = (ecx == 0) ? 1 : 0;
    goto label_10;
label_18:
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x36e0 */
 
uint64_t dbg_base64url_decode_ctx_init_wrapper (int64_t arg1) {
    rdi = arg1;
    /* void base64url_decode_ctx_init_wrapper(base_decode_context * ctx); */
    rbx = rdi;
    rdi = rdi + 4;
    base64_decode_ctx_init ();
    *((rbx + 0x18)) = 0x1068;
    rax = xcharalloc (0x1068);
    *((rbx + 0x10)) = rax;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x4e40 */
 
void dbg_base64_decode_ctx_init (base64_decode_context * ctx) {
    rdi = ctx;
    /* void base64_decode_ctx_init(base64_decode_context * ctx); */
    *(rdi) = 0;
}

/* /tmp/tmpaqk9hiad @ 0x3710 */
 
void dbg_base64_decode_ctx_init_wrapper (int64_t arg1) {
    rdi = arg1;
    /* void base64_decode_ctx_init_wrapper(base_decode_context * ctx); */
    rdi += 4;
    return void (*0x4e40)() ();
}

/* /tmp/tmpaqk9hiad @ 0x3720 */
 
uint64_t dbg_finish_and_exit (int64_t arg2) {
    rsi = arg2;
    /* void finish_and_exit(FILE * in,char const * infile); */
    r12 = rsi;
    eax = rpl_fclose (rdi);
    if (eax == 0) {
        goto label_0;
    }
    eax = *(r12);
    eax -= 0x2d;
    ebx = eax;
    while (1) {
        rax = errno_location ();
        if (ebx == 0) {
            edx = 5;
            rax = dcgettext (0, "closing standard input");
            eax = 0;
            error (1, *(rbp), rax);
        }
        rdx = r12;
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        rcx = rax;
        eax = 0;
        error (1, *(rbp), 0x00009840);
        ebx = *((r12 + 1));
    }
label_0:
    return exit (0);
}

/* /tmp/tmpaqk9hiad @ 0x37b0 */
 
uint64_t dbg_base64url_decode_ctx_wrapper (int64_t arg_4h, int64_t arg_10h, int64_t arg_18h, int64_t arg1, int64_t arg2, signed int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool base64url_decode_ctx_wrapper(base_decode_context * ctx,char const * restrict in,idx_t inlen,char * restrict out,idx_t * outlen); */
    r14 = rsi;
    r13 = rdx;
    r12 = rcx;
    rbx = r8;
    rdi = *((rdi + 0x10));
    if (rdx > *((rbp + 0x18))) {
        goto label_2;
    }
label_1:
    al = memcpy (rdi, r14, r13);
    rsi = *((rbp + 0x10));
    if (r13 == 0) {
        goto label_3;
    }
    rcx = rsi + r13;
    while (al != 0x2d) {
        if (al == 0x5f) {
            *(rsi) = 0x2f;
        }
        rsi++;
        if (rsi == rcx) {
            goto label_4;
        }
label_0:
        eax = *(rsi);
        edx = eax;
        edx &= 0xfffffffb;
        if (dl == 0x2b) {
            goto label_5;
        }
    }
    *(rsi) = 0x2b;
    rsi++;
    if (rsi != rcx) {
        goto label_0;
    }
label_4:
label_3:
    eax = base64_decode_ctx (rbp + 4, *((rbp + 0x10)), r13, r12, rbx);
    edx = *((rbp + 4));
    *(rbp) = edx;
    return eax;
label_5:
    *(rbx) = 0;
    eax = 0;
    return eax;
label_2:
    rsi = rdx + rdx;
    edx = 1;
    *((rbp + 0x18)) = rsi;
    rax = xreallocarray ();
    *((rbp + 0x10)) = rax;
    rdi = rax;
    goto label_1;
}

/* /tmp/tmpaqk9hiad @ 0x3890 */
 
int64_t base32hex_encode (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    rbx = rdx;
    base32_encode (rdi, rsi, rdx, rcx);
    if (rbp == 0) {
        goto label_0;
    }
    rdx = rbx;
    rsi = "QRSTUV89:;<=>?@0123456789ABCDEFGHIJKLMNOP9.1.17-a351f";
    rcx = rbx + rbp;
    do {
        eax = *(rdx);
        eax -= 0x32;
        if (al > 0x28) {
            goto label_1;
        }
        rax = (int64_t) al;
        rdx++;
        eax = *((rsi + rax));
        *((rdx - 1)) = al;
    } while (rdx != rcx);
label_0:
    return rax;
label_1:
    return assert_fail ("0x32 <= *p && *p <= 0x5a", "src/basenc.c", 0x1c4, "base32hex_encode");
}

/* /tmp/tmpaqk9hiad @ 0x3910 */
 
uint64_t base2msbf_decode_ctx (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r10 = rcx;
    if (rdx == 0) {
        goto label_3;
    }
    rax = rsi + 1;
    r9 = rax + rdx;
    goto label_2;
label_0:
    edx <<= 7;
    *(rdi) = 7;
    esi |= edx;
    *((rdi + 4)) = sil;
    do {
label_1:
        rax++;
        if (rax == r9) {
            goto label_4;
        }
label_2:
        edx = *((rax - 1));
    } while (dl == 0xa);
    ecx = rdx - 0x30;
    if (cl > 1) {
        goto label_5;
    }
    ecx = *(rdi);
    esi = *((rdi + 4));
    dl = (dl == 0x31) ? 1 : 0;
    edx = (int32_t) dl;
    if (ecx == 0) {
        goto label_0;
    }
    ecx--;
    edx <<= cl;
    *(rdi) = ecx;
    esi |= edx;
    *((rdi + 4)) = sil;
    if (ecx != 0) {
        goto label_1;
    }
    rax++;
    *(r10) = sil;
    r10++;
    *((rdi + 4)) = 0;
    *(r8)++;
    *(rdi) = 0;
    if (rax != r9) {
        goto label_2;
    }
label_4:
    eax = 1;
    return rax;
label_3:
    eax = *(rdi);
    al = (eax == 0) ? 1 : 0;
    return rax;
label_5:
    eax = 0;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x39d0 */
 
uint32_t base2lsbf_decode_ctx (int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r9 = rcx;
    if (rdx == 0) {
        goto label_0;
    }
    rsi++;
    rdx += rsi;
    do {
        eax = *((rsi - 1));
        if (al != 0xa) {
            ecx = rax - 0x30;
            if (cl > 1) {
                goto label_1;
            }
            ecx = *(rdi);
            al = (al == 0x31) ? 1 : 0;
            eax = (int32_t) al;
            eax <<= cl;
            ecx++;
            al |= *((rdi + 4));
            *((rdi + 4)) = al;
            *(rdi) = ecx;
            if (ecx != 8) {
                goto label_2;
            }
            *((rdi + 4)) = 0;
            r9++;
            *(r8)++;
            *((r9 - 1)) = al;
            *(rdi) = 0;
        }
label_2:
        rsi++;
    } while (rdx != rsi);
    eax = 1;
    return eax;
label_0:
    eax = *(rdi);
    al = (eax == 0) ? 1 : 0;
    return eax;
label_1:
    eax = 0;
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x3a60 */
 
#define SWAP32(n) ((uint32_t) (((n & 0x000000ff) << 24) | \
                               ((n & 0x0000ff00) <<  8) | \
                               ((n & 0x00ff0000) >>  8) | \
                               ((n & 0xff000000) >> 24)))
 
int64_t z85_decode_ctx (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r9 = rcx;
    if (rdx == 0) {
        goto label_2;
    }
    rcx = rsi + 1;
    rsi = obj_z85_decoding;
    rdx += rcx;
    while (al == 0xa) {
label_0:
        rcx++;
        if (rdx == rcx) {
            goto label_3;
        }
label_1:
        eax = *((rcx - 1));
    }
    r10d = rax - 0x21;
    if (r10b > 0x5c) {
        goto label_4;
    }
    eax -= 0x21;
    rax = (int64_t) eax;
    r10d = *((rsi + rax));
    if (r10b < 0) {
        goto label_4;
    }
    rax = *((rdi + 4));
    r11d = rax + 1;
    *((rdi + 4)) = r11d;
    *((rdi + rax + 8)) = r10b;
    if (r11d != 5) {
        goto label_0;
    }
    eax = *((rdi + 9));
    r10d = *((rdi + 0xa));
    eax *= 0x95eed;
    r10d *= 0x1c39;
    eax += r10d;
    r10d = *((rdi + 0xb));
    r10d *= 0x55;
    eax += r10d;
    r10d = *((rdi + 0xc));
    eax += r10d;
    r10d = *((rdi + 8));
    rax = (int64_t) eax;
    r10 *= 0x31c84b1;
    rax += r10;
    r10 = rax;
    r10 >>= 0x18;
    if ((r10d & 0x700) != 0) {
        goto label_4;
    }
    rcx++;
    *(r8) += 4;
    eax = SWAP32 (eax);
    r9 += 4;
    *((r9 - 4)) = eax;
    *((rdi + 4)) = 0;
    if (rdx != rcx) {
        goto label_1;
    }
label_3:
    eax = *((rdi + 4));
    *(rdi) = eax;
    eax = 1;
    return rax;
label_2:
    eax = *((rdi + 4));
    al = (eax <= 0) ? 1 : 0;
    return rax;
label_4:
    eax = 0;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x3b80 */
 
int32_t base16_decode_ctx (int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    *(r8) = 0;
    r9 = rcx;
    r10 = rsi + rdx;
    if (rdx != 0) {
        goto label_2;
    }
    goto label_3;
label_0:
    ecx = *((rdi + 4));
    *(r8)++;
    r9++;
    ecx <<= 4;
    edx += ecx;
    *((r9 - 1)) = dl;
label_1:
    eax ^= 1;
    *((rdi + 5)) = al;
    do {
        if (rsi == r10) {
            goto label_4;
        }
label_2:
        eax = *(rsi);
        rsi++;
    } while (al == 0xa);
    edx = rax - 0x30;
    if (edx > 9) {
        edx = rax - 0x41;
        if (edx > 5) {
            goto label_5;
        }
        edx = rax - 0x37;
    }
    eax = *((rdi + 5));
    if (al != 0) {
        goto label_0;
    }
    *((rdi + 4)) = dl;
    goto label_1;
label_4:
    eax = 1;
    return eax;
label_3:
    eax = *((rdi + 5));
    eax ^= 1;
    return eax;
label_5:
    eax = 0;
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x3c20 */
 
int64_t dbg_base32hex_decode_ctx_wrapper (int64_t arg_4h, int64_t arg_10h, signed int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool base32hex_decode_ctx_wrapper(base_decode_context * ctx,char const * restrict in,idx_t inlen,char * restrict out,idx_t * outlen); */
    r13 = rcx;
    r12 = rdx;
    rbx = rsi;
    r9 = *((rdi + 0x10));
    if (rdx > *((rdi + 0x18))) {
        goto label_2;
    }
label_1:
    if (r12 == 0) {
        goto label_3;
    }
    edx = 0;
    rsi = "ABCDEFGHIJ:;<=>?@KLMNOPQRSTUVWXYZ234567";
    while (cl > 0x15) {
        *((r9 + rdx)) = al;
        rdx++;
        if (r12 == rdx) {
            goto label_4;
        }
label_0:
        eax = *((rbx + rdx));
        ecx = rax - 0x30;
        if (cl <= 9) {
            goto label_5;
        }
        ecx = rax - 0x41;
    }
label_5:
    eax -= 0x30;
    rax = (int64_t) eax;
    eax = *((rsi + rax));
    *((r9 + rdx)) = al;
    rdx++;
    if (r12 != rdx) {
        goto label_0;
    }
label_4:
    r9 = *((rbp + 0x10));
label_3:
    base32_decode_ctx (rbp + 4, r9, r12, r13, r8);
    edx = *((rbp + 4));
    *(rbp) = edx;
    return rax;
label_2:
    rsi = rdx + rdx;
    edx = 1;
    *((rsp + 8)) = r8;
    *((rdi + 0x18)) = rsi;
    rdi = r9;
    rax = xreallocarray ();
    r8 = *((rsp + 8));
    *((rbp + 0x10)) = rax;
    r9 = rax;
    goto label_1;
}

/* /tmp/tmpaqk9hiad @ 0x30e0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x3110 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x3150 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002370 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpaqk9hiad @ 0x2370 */
 
void fcn_00002370 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpaqk9hiad @ 0x3190 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpaqk9hiad @ 0x41f0 */
 
int64_t dbg_decode_8 (int64_t arg1, signed int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool decode_8(char const * restrict in,idx_t inlen,char * restrict * outp,idx_t * outleft); */
    rax = rdi;
    r8d = 0;
    rdi = rdx;
    rdx = rcx;
    if (rsi <= 7) {
        goto label_2;
    }
    esi = *(rax);
    rcx = obj_b32;
    esi = *((rcx + rsi));
    if (sil < 0) {
        goto label_2;
    }
    r9d = *((rax + 1));
    r9d = *((rcx + r9));
    if (r9b < 0) {
        goto label_2;
    }
    r8 = *(rdi);
    while (1) {
        esi = *((rax + 2));
        if (sil != 0x3d) {
            esi = *((rcx + rsi));
            if (sil < 0) {
                goto label_0;
            }
            r10d = *((rax + 3));
            r10d = *((rcx + r10));
            if (r10b < 0) {
                goto label_0;
            }
            if (*(rdx) != 0) {
                r9d <<= 6;
                esi += esi;
                r8++;
                esi |= r9d;
                r9d = r10d;
                r9b >>= 4;
                esi |= r9d;
                *((r8 - 1)) = sil;
                *(rdx)--;
            }
            esi = *((rax + 4));
            if (sil == 0x3d) {
                goto label_3;
            }
            r9d = *((rcx + rsi));
            if (r9b < 0) {
                goto label_0;
            }
            if (*(rdx) != 0) {
                esi = r9d;
                r10d <<= 4;
                r8++;
                sil >>= 1;
                r10d |= esi;
                *((r8 - 1)) = r10b;
                *(rdx)--;
            }
            esi = *((rax + 5));
            if (sil == 0x3d) {
                goto label_4;
            }
            esi = *((rcx + rsi));
            if (sil < 0) {
                goto label_0;
            }
            r10d = *((rax + 6));
            r10d = *((rcx + r10));
            if (r10b < 0) {
                goto label_0;
            }
            if (*(rdx) != 0) {
                r9d <<= 7;
                esi <<= 2;
                r8++;
                esi |= r9d;
                r9d = r10d;
                r9b >>= 3;
                esi |= r9d;
                *((r8 - 1)) = sil;
                *(rdx)--;
            }
            eax = *((rax + 7));
            if (al != 0x3d) {
                eax = *((rcx + rax));
                if (al < 0) {
                    goto label_0;
                }
                if (*(rdx) == 0) {
                    goto label_1;
                }
                r10d <<= 5;
                r8++;
                r10d |= eax;
                *((r8 - 1)) = r10b;
                *(rdx)--;
            }
label_1:
            *(rdi) = r8;
            r8d = 1;
label_2:
            eax = 1;
            return rax;
        }
        if (*((rax + 3)) == 0x3d) {
            if (*((rax + 4)) != 0x3d) {
                goto label_0;
            }
label_3:
            if (*((rax + 5)) == 0x3d) {
                goto label_4;
            }
        }
label_0:
        *(rdi) = r8;
        r8d = 0;
        eax = r8d;
        return rax;
        r10d = r9d;
        esi <<= 3;
        r8++;
        r10b >>= 2;
        esi |= r10d;
        *((r8 - 1)) = sil;
        *(rdx)--;
    }
label_4:
    if (*((rax + 6)) != 0x3d) {
        goto label_0;
    }
    if (*((rax + 7)) == 0x3d) {
        goto label_1;
    }
    goto label_0;
}

/* /tmp/tmpaqk9hiad @ 0x4a80 */
 
int64_t dbg_decode_4 (int64_t arg1, int64_t arg3, int64_t arg4, idx_t inlen) {
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    rsi = inlen;
    /* _Bool decode_4(char const * restrict in,idx_t inlen,char * restrict * outp,idx_t * outleft); */
    r9 = rdx;
    rax = rdi;
    rdx = rcx;
    r8d = 0;
    if (rsi <= 1) {
        goto label_3;
    }
    ecx = *(rax);
    rdi = obj_b64;
    ecx = *((rdi + rcx));
    if (cl < 0) {
        goto label_3;
    }
    r10d = *((rax + 1));
    r10d = *((rdi + r10));
    if (r10b < 0) {
        goto label_3;
    }
    r8 = *(r9);
    if (*(rdx) != 0) {
        goto label_4;
    }
label_0:
    if (rsi == 2) {
        goto label_2;
    }
    ecx = *((rax + 2));
    if (cl == 0x3d) {
        goto label_5;
    }
    ecx = *((rdi + rcx));
    if (cl < 0) {
        goto label_2;
    }
    if (*(rdx) != 0) {
        r11d = ecx;
        r10d <<= 4;
        r8++;
        r11b >>= 2;
        r10d |= r11d;
        *((r8 - 1)) = r10b;
        *(rdx)--;
    }
    if (rsi == 3) {
        goto label_2;
    }
    eax = *((rax + 3));
    if (al == 0x3d) {
        goto label_6;
    }
    eax = *((rdi + rax));
    if (al < 0) {
        goto label_2;
    }
    if (*(rdx) == 0) {
        goto label_1;
    }
    ecx <<= 6;
    r8++;
    ecx |= eax;
    *((r8 - 1)) = cl;
    *(rdx)--;
    do {
label_1:
        *(r9) = r8;
        r8d = 1;
label_3:
        eax = 1;
        return rax;
label_5:
        if (rsi != 4) {
            goto label_2;
        }
    } while (*((rax + 3)) == 0x3d);
label_2:
    *(r9) = r8;
    r8d = 0;
    eax = r8d;
    return rax;
label_4:
    r11d = r10d;
    ecx <<= 2;
    r8++;
    r11b >>= 4;
    ecx |= r11d;
    *((r8 - 1)) = cl;
    *(rdx)--;
    goto label_0;
label_6:
    if (rsi == 4) {
        goto label_1;
    }
    goto label_2;
}

/* /tmp/tmpaqk9hiad @ 0x8d10 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpaqk9hiad @ 0x6f50 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x7280 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x2470 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpaqk9hiad @ 0x7d10 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x6d90 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x23b0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpaqk9hiad @ 0x6cb0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x8b40 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x6fa0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26d0)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x6d10 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x46c0 */
 
uint64_t isbase32 (void) {
    edi = (int32_t) dil;
    rax = obj_b32;
    eax = *((rax + rdi));
    eax = ~eax;
    al >>= 7;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x5450 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite (0x00009d20, 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpaqk9hiad @ 0x24c0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpaqk9hiad @ 0x23c0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpaqk9hiad @ 0x2670 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpaqk9hiad @ 0x74e0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x26e9)() ();
    }
    if (rdx == 0) {
        void (*0x26e9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x7580 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26ee)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x26ee)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x70c0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x26da)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x8490 */
 
int64_t dbg_xstrtoimax (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    char * t_ptr;
    int64_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_1fh;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoimax(char const * s,char ** ptr,int strtol_base,intmax_t * val,char const * valid_suffixes); */
    *(rsp) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_25;
    }
    rax = rsp + 0x20;
    r13 = rdi;
    if (rsi == 0) {
    }
    r12d = edx;
    r14 = r8;
    errno_location ();
    *(rax) = 0;
    r15 = rax;
    rax = strtoimax (r13, rbp, r12d);
    rdx = *(rbp);
    rbx = rax;
    if (rdx == r13) {
        goto label_26;
    }
    eax = *(r15);
    if (eax != 0) {
        goto label_27;
    }
    r12d = 0;
label_0:
    if (r14 != 0) {
        r13d = *(rdx);
        if (r13b != 0) {
            goto label_28;
        }
    }
label_3:
    rax = *(rsp);
    *(rax) = rbx;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_29;
        }
        eax = r12d;
        return rax;
label_27:
        r12d = 4;
    } while (eax != 0x22);
    r12d = 1;
    goto label_0;
label_26:
    if (r14 == 0) {
        goto label_30;
    }
    r13d = *(rdx);
    *((rsp + 8)) = rdx;
    r12d = 4;
    if (r13b == 0) {
        goto label_1;
    }
    esi = (int32_t) r13b;
    r12d = 0;
    ebx = 1;
    rax = strchr (r14, rsi);
    rdx = *((rsp + 8));
    if (rax == 0) {
        goto label_30;
    }
    do {
        r8d = r13 - 0x45;
        r9d = 1;
        ecx = section..dynsym;
        if (r8b <= 0x2f) {
            rax = 0x814400308945;
            if (((rax >> r8) & 1) < 0) {
                goto label_31;
            }
        }
label_2:
        r13d -= 0x42;
        if (r13b > 0x35) {
            goto label_32;
        }
        r13d = (int32_t) r13b;
        rax = *((rsi + r13*4));
        rax += rsi;
        /* switch table (54 cases) at 0xa4f8 */
        void (*rax)() ();
label_28:
        esi = (int32_t) r13b;
        *((rsp + 8)) = rdx;
        rax = strchr (r14, 0x0000a4f8);
        rdx = *((rsp + 8));
    } while (rax != 0);
label_32:
    rax = *(rsp);
    r12d |= 2;
    *(rax) = rbx;
    goto label_1;
label_31:
    *((rsp + 0x18)) = r9d;
    *((rsp + 0x14)) = ecx;
    *((rsp + 8)) = rdx;
    *((rsp + 0x1f)) = r8b;
    rax = strchr (r14, 0x30);
    rdx = *((rsp + 8));
    ecx = section..dynsym;
    r9d = 1;
    if (rax == 0) {
        goto label_2;
    }
    eax = *((rdx + 1));
    if (al == 0x44) {
        goto label_33;
    }
    if (al == 0x69) {
        goto label_34;
    }
    r8d = *((rsp + 0x1f));
    if (al == 0x42) {
        goto label_33;
    }
    rcx = 0x0000a5d0;
    rax = *((rcx + r8*4));
    rax += rcx;
    /* switch table (48 cases) at 0xa5d0 */
    void (*rax)() ();
label_30:
    r12d = 4;
    goto label_1;
    r9 = (int64_t) r9d;
label_10:
    esi = 4;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rax overflow 0) {
            goto label_35;
        }
        rbx = rcx;
label_21:
        esi--;
    } while (esi != 0);
label_6:
    r12d |= edi;
    do {
label_5:
        rax = rdx + r9;
        edx = r12d;
        edx |= 2;
        *(rbp) = rax;
        if (*(rax) != 0) {
            r12d = edx;
        }
        goto label_3;
        r9 = (int64_t) r9d;
label_11:
        rax = (int64_t) ecx;
        rcx = rbx;
        rcx *= rax;
        if (*(rax) overflow 0) {
            goto label_9;
        }
        rax *= rcx;
        if (*(rax) overflow 0) {
            goto label_36;
        }
label_4:
        rbx = rax;
    } while (1);
    r9 = (int64_t) r9d;
label_12:
    rax = (int64_t) ecx;
    rax *= rbx;
    if (*(rax) !overflow 0) {
        goto label_4;
    }
label_9:
    r12d = 1;
    if (rbx < 0) {
        goto label_37;
    }
label_7:
    rbx = 0x7fffffffffffffff;
    goto label_5;
    r9 = (int64_t) r9d;
label_13:
    esi = 3;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rbx overflow 0) {
            goto label_38;
        }
        rbx = rcx;
label_20:
        esi--;
    } while (esi != 0);
    goto label_6;
    rax = rbx * section..dynsym;
    r9 = (int64_t) r9d;
    if (esi !overflow 0) {
        goto label_4;
    }
label_8:
    r12d = 1;
    if (rbx >= 0) {
        goto label_7;
    }
    r12d = 1;
label_37:
    rbx = 0x8000000000000000;
    goto label_5;
    rax = rbx * 2;
    r9 = (int64_t) r9d;
    if (rbx !overflow 0) {
        goto label_4;
    }
    goto label_8;
    r9 = (int64_t) r9d;
label_16:
    esi = 5;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rbx overflow 0) {
            goto label_39;
        }
        rbx = rcx;
label_22:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_14:
    esi = 6;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_40;
        }
        rbx = rcx;
label_23:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
    goto label_5;
    r9 = (int64_t) r9d;
label_17:
    esi = 7;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_41;
        }
        rbx = rcx;
label_19:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_18:
    esi = 8;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_42;
        }
        rbx = rcx;
label_24:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_15:
    rax = rbx * 0x200;
    if (esi !overflow 0) {
        goto label_4;
    }
    goto label_9;
    r9d = 1;
    ecx = section..dynsym;
    goto label_10;
    r9d = 1;
    ecx = section..dynsym;
    goto label_11;
    r9d = 1;
    ecx = section..dynsym;
    goto label_12;
    r9d = 1;
    ecx = section..dynsym;
    goto label_13;
    r9d = 1;
    ecx = section..dynsym;
    goto label_14;
    r9d = 1;
    goto label_15;
    r9d = 1;
    ecx = section..dynsym;
    goto label_16;
    r9d = 1;
    ecx = section..dynsym;
    goto label_17;
    r9d = 1;
    ecx = section..dynsym;
    goto label_18;
    r9d = 1;
    goto label_5;
label_33:
    r9d = 2;
    ecx = 0x3e8;
    goto label_2;
label_34:
    r9d = 0;
    r9b = (*((rdx + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_2;
label_25:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoimax");
label_29:
    stack_chk_fail ();
label_41:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_19;
    }
    rbx = r10;
    goto label_19;
label_38:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_20;
    }
    rbx = r10;
    goto label_20;
label_35:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_21;
    }
    rbx = r10;
    goto label_21;
label_39:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_22;
    }
    rbx = r10;
    goto label_22;
label_40:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_23;
    }
    rbx = r10;
    goto label_23;
label_42:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_24;
    }
    rbx = r10;
    goto label_24;
label_36:
    r12d = 1;
    rbx = 0x8000000000000000;
    rax = 0x7fffffffffffffff;
    __asm ("cmovns rbx, rax");
    goto label_5;
}

/* /tmp/tmpaqk9hiad @ 0x49c0 */
 
int64_t dbg_base32_decode_alloc_ctx (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    idx_t needlen;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool base32_decode_alloc_ctx(base32_decode_context * ctx,char const * in,idx_t inlen,char ** out,idx_t * outlen); */
    r14 = rsi;
    r13 = rdi;
    r12 = rdx;
    rbx = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    rax = rdx;
    rax >>= 3;
    *((rsp + 0x10)) = rdi;
    rax = malloc (rax + rax*4 + 5);
    *(rbx) = rax;
    if (rax == 0) {
        goto label_1;
    }
    al = base32_decode_ctx (r13, r14, r12, rax, rsp + 0x10);
    if (al == 0) {
        goto label_2;
    }
    if (rbp == 0) {
        goto label_1;
    }
    rdx = *((rsp + 0x10));
    *(rbp) = rdx;
    do {
label_0:
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_3;
        }
        return rax;
label_1:
        eax = 1;
    } while (1);
label_2:
    *((rsp + 0xf)) = al;
    free (*(rbx));
    *(rbx) = 0;
    eax = *((rsp + 0xf));
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x7440 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, char * arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26e4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x8d24 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpaqk9hiad @ 0x52b0 */
 
void fdadvise (void) {
    return posix_fadvise ();
}

/* /tmp/tmpaqk9hiad @ 0x7f90 */
 
uint64_t xreallocarray (int64_t arg1, signed int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x8090 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x7f30 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x5380 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x25a0)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpaqk9hiad @ 0x7ed0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x6f80 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x8410 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x2460 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpaqk9hiad @ 0x7ef0 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x7c50 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x77e0)() ();
}

/* /tmp/tmpaqk9hiad @ 0x5200 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x00009840);
    } while (1);
}

/* /tmp/tmpaqk9hiad @ 0x8c50 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpaqk9hiad @ 0x7e90 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x8bd0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x7630 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26f3)() ();
    }
    if (rax == 0) {
        void (*0x26f3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x73b0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x82e0 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x2540 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpaqk9hiad @ 0x6c50 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x7770 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x6bf0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x8350 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x6e90 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x51f0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpaqk9hiad @ 0x7f60 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x8120 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x52f0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2420)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpaqk9hiad @ 0x8040 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x7780 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x76d0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26f8)() ();
    }
    if (rax == 0) {
        void (*0x26f8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x8390 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x6e80 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x6d90)() ();
}

/* /tmp/tmpaqk9hiad @ 0x4b90 */
 
int64_t dbg_base64_encode (uint32_t arg1, uint32_t arg2, int64_t arg3, uint32_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void base64_encode(char const * restrict in,idx_t inlen,char * restrict out,idx_t outlen); */
    if ((cl & 3) == 0) {
        rax = rcx;
        rax >>= 2;
        rax *= 3;
        if (rax == rsi) {
            goto label_1;
        }
    }
    if (rsi == 0) {
        goto label_2;
    }
    if (rcx == 0) {
        goto label_3;
    }
    eax = *(rdi);
    r8 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n";
    r9d = eax;
    r9b >>= 2;
    r9d &= 0x3f;
    r9d = *((r8 + r9));
    *(rdx) = r9b;
    if (rcx == 1) {
        goto label_4;
    }
    r9 = rdi + rsi - 1;
    r11 = rdi + rsi - 3;
    r10 = rdi + rsi - 2;
    while (r9 != rdi) {
        esi = *((rdi + 1));
        ebx = esi;
        bl >>= 4;
        eax += ebx;
        eax &= 0x3f;
        eax = *((r8 + rax));
        *((rdx + 1)) = al;
        if (rcx == 2) {
            goto label_5;
        }
        esi <<= 2;
        if (r10 == rdi) {
            goto label_6;
        }
        eax = *((rdi + 2));
        ebx = eax;
        bl >>= 6;
        esi += ebx;
        esi &= 0x3f;
        esi = *((r8 + rsi));
        *((rdx + 2)) = sil;
        if (rcx == 3) {
            goto label_5;
        }
        eax &= 0x3f;
        rdx += 4;
        eax = *((r8 + rax));
        *((rdx - 1)) = al;
        rcx -= 4;
        if (rcx == 0) {
            goto label_5;
        }
        if (r11 == rdi) {
            goto label_7;
        }
        eax = *((rdi + 3));
        rdi += 3;
        esi = eax;
        sil >>= 2;
        esi &= 0x3f;
        esi = *((r8 + rsi));
        *(rdx) = sil;
        if (rcx == 1) {
            goto label_5;
        }
        eax <<= 4;
    }
    eax &= 0x30;
    eax = *((r8 + rax));
    *((rdx + 1)) = al;
    if (rcx == 2) {
        goto label_5;
    }
    *((rdx + 2)) = 0x3d;
    while (rcx != 3) {
        *((rdx + 3)) = 0x3d;
        rax = rdx + 4;
        if (rcx != 4) {
label_0:
            *(rax) = 0;
        }
label_5:
        return rax;
label_6:
        esi &= 0x3c;
        eax = *((r8 + rsi));
        *((rdx + 2)) = al;
    }
    return rax;
label_7:
    rax = rdx;
    goto label_0;
label_2:
    if (rcx == 0) {
        goto label_3;
    }
    *(rdx) = 0;
    do {
label_3:
        return rax;
label_1:
    } while (rax == 0);
    rsi = rdi + rax;
    r8 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/A NULL argv[0] was passed through an exec system call.\n";
    do {
        eax = *(rdi);
        rdx += 4;
        rdi += 3;
        ecx = eax;
        eax <<= 4;
        cl >>= 2;
        ecx &= 0x3f;
        ecx = *((r8 + rcx));
        *((rdx - 4)) = cl;
        ecx = *((rdi - 2));
        r9d = ecx;
        r9b >>= 4;
        eax += r9d;
        eax &= 0x3f;
        eax = *((r8 + rax));
        *((rdx - 3)) = al;
        eax = *((rdi - 1));
        r9d = eax;
        eax &= 0x3f;
        r9b >>= 6;
        eax = *((r8 + rax));
        ecx = r9 + rcx*4;
        ecx &= 0x3f;
        ecx = *((r8 + rcx));
        *((rdx - 2)) = cl;
        *((rdx - 1)) = al;
    } while (rdi != rsi);
    return rax;
label_4:
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x6f60 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x43b0 */
 
int64_t dbg_base32_encode (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void base32_encode(char const * restrict in,idx_t inlen,char * restrict out,idx_t outlen); */
    if (rcx == 0) {
        goto label_4;
    }
    rax = rdx;
    r8 = rsi;
    rdx = rcx;
    if (rsi == 0) {
        goto label_5;
    }
    ecx = *(rdi);
    rsi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    r9d = ecx;
    r9b >>= 3;
    r9d &= 0x1f;
    r9d = *((rsi + r9));
    *(rax) = r9b;
    if (rdx == 1) {
        goto label_4;
    }
    r9 = rdi + r8 - 1;
    r11 = rdi + r8 - 3;
    r10 = rdi + r8 - 2;
    rbp = rdi + r8 - 5;
    rbx = rdi + r8 - 4;
    while (r9 != rdi) {
        r8d = *((rdi + 1));
        r12d = r8d;
        r12b >>= 6;
        ecx += r12d;
        ecx &= 0x1f;
        ecx = *((rsi + rcx));
        *((rax + 1)) = cl;
        if (rdx == 2) {
            goto label_0;
        }
        ecx = r8d;
        cl >>= 1;
        ecx &= 0x1f;
        ecx = *((rsi + rcx));
        *((rax + 2)) = cl;
        if (rdx == 3) {
            goto label_0;
        }
        r8d <<= 4;
        if (rdi == r10) {
            goto label_6;
        }
        ecx = *((rdi + 2));
        r12d = ecx;
        r12b >>= 4;
        r8d += r12d;
        r8d &= 0x1f;
        r8d = *((rsi + r8));
        *((rax + 3)) = r8b;
        if (rdx == 4) {
            goto label_0;
        }
        r8d = rcx + rcx;
        if (rdi == r11) {
            goto label_7;
        }
        ecx = *((rdi + 3));
        r12d = ecx;
        r12b >>= 7;
        r8d += r12d;
        r8d &= 0x1f;
        r8d = *((rsi + r8));
        *((rax + 4)) = r8b;
        if (rdx == 5) {
            goto label_0;
        }
        r8d = ecx;
        r8b >>= 2;
        r8d &= 0x1f;
        r8d = *((rsi + r8));
        *((rax + 5)) = r8b;
        if (rdx == 6) {
            goto label_0;
        }
        ecx <<= 3;
        if (rdi == rbx) {
            goto label_8;
        }
        r8d = *((rdi + 4));
        r12d = r8d;
        r12b >>= 5;
        ecx += r12d;
        ecx &= 0x1f;
        ecx = *((rsi + rcx));
        *((rax + 6)) = cl;
        if (rdx == 7) {
            goto label_0;
        }
        r8d &= 0x1f;
        rax += 8;
        ecx = *((rsi + r8));
        *((rax - 1)) = cl;
        rdx -= 8;
        if (rdx == 0) {
            goto label_0;
        }
        if (rdi == rbp) {
            goto label_9;
        }
        ecx = *((rdi + 5));
        rdi += 5;
        r8d = ecx;
        r8b >>= 3;
        r8d &= 0x1f;
        r8d = *((rsi + r8));
        *(rax) = r8b;
        if (rdx == 1) {
            goto label_0;
        }
        ecx <<= 2;
    }
    ecx &= 0x1c;
    ecx = *((rsi + rcx));
    *((rax + 1)) = cl;
    if (rdx == 2) {
        goto label_0;
    }
    *((rax + 2)) = 0x3d;
    if (rdx == 3) {
        goto label_0;
    }
    *((rax + 3)) = 0x3d;
    while (rdx != 4) {
        *((rax + 4)) = 0x3d;
        if (rdx != 5) {
label_1:
            *((rax + 5)) = 0x3d;
            if (rdx == 6) {
                goto label_0;
            }
            *((rax + 6)) = 0x3d;
            if (rdx == 7) {
                goto label_0;
            }
label_2:
            *((rax + 7)) = 0x3d;
            rcx = rax + 8;
            if (rdx == 8) {
                goto label_0;
            }
label_3:
            *(rcx) = 0;
        }
label_0:
        r12 = rbx;
        return rax;
label_6:
        r8d &= 0x10;
        ecx = *((rsi + r8));
        *((rax + 3)) = cl;
    }
    r12 = rbx;
    return rax;
label_7:
    r8d &= 0x1e;
    ecx = *((rsi + r8));
    *((rax + 4)) = cl;
    if (rdx == 5) {
        goto label_0;
    }
    goto label_1;
label_8:
    ecx &= 0x18;
    ecx = *((rsi + rcx));
    *((rax + 6)) = cl;
    if (rdx != 7) {
        goto label_2;
    }
    r12 = rbx;
    return rax;
label_9:
    rcx = rax;
    goto label_3;
label_5:
    if (rcx != 0) {
        *(rax) = 0;
    }
label_4:
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x4d70 */
 
int64_t dbg_base64_encode_alloc (int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* idx_t base64_encode_alloc(char const * in,idx_t inlen,char ** out); */
    rax = rsi;
    ecx = 0;
    rbx = rdx;
    rdx = 0x5555555555555556;
    rdx:rax = rax * rdx;
    rax = rsi;
    rax >>= 0x3f;
    rdx -= rax;
    rax = rdx * 3;
    cl = (rsi != rax) ? 1 : 0;
    rdx += rcx;
    r12 = rdx * 4;
    __asm ("seto al");
    if (rsi >= 0) {
        eax = (int32_t) al;
        if (rax != 0) {
            goto label_0;
        }
        r14 = r12 + 1;
        r13 = rdi;
        rax = malloc (r14);
        *(rbx) = rax;
        rdx = rax;
        if (rax == 0) {
            goto label_1;
        }
        base64_encode (r13, rbp, rdx, r14);
        rax = r12;
        return rax;
    }
label_0:
    *(rbx) = 0;
    r12d = 0;
    do {
        rax = r12;
        return rax;
label_1:
        r12 = r14;
    } while (1);
}

/* /tmp/tmpaqk9hiad @ 0x2700 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    idx_t n;
    base_decode_context ctx;
    int64_t var_8h;
    uint32_t var_10h;
    uint32_t var_18h;
    uint32_t var_20h;
    char * filename;
    uint32_t var_38h;
    int64_t var_40h;
    int64_t var_50h;
    int64_t var_68h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15d = 0x4c;
    r14d = 0;
    r13 = obj_long_options;
    r12 = 0x000097e7;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x68)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000a1a1);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = "diw:";
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    *(rsp) = 0;
    *((rsp + 0x18)) = 0;
    do {
label_0:
        r8d = 0;
        rcx = r13;
        rdx = r12;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_19;
        }
        if (eax == 0x69) {
            goto label_20;
        }
        if (eax > 0x69) {
            goto label_21;
        }
        if (eax == 0xffffff7e) {
            goto label_22;
        }
        if (eax != 0x64) {
            goto label_23;
        }
        r14d = 1;
    } while (1);
label_21:
    if (eax == 0x77) {
        goto label_24;
    }
    edx = rax - 0x80;
    if (edx <= 7) {
        goto label_25;
    }
    do {
label_17:
        eax = usage (1);
label_23:
    } while (eax != 0xffffff7d);
    eax = 0;
    version_etc (*(obj.stdout), "basenc", "GNU coreutils", *(obj.Version), "Simon Josefsson", "Assaf Gordon");
    eax = exit (0);
label_25:
    *(rsp) = eax;
    goto label_0;
label_20:
    *((rsp + 0x18)) = 1;
    goto label_0;
label_24:
    eax = xstrtoimax (*(obj.optarg), 0, 0xa, rsp + 0x38, 0x0000a1a1);
    if (eax > 1) {
        goto label_26;
    }
    r15 = *((rsp + 0x38));
    if (r15 < 0) {
        goto label_26;
    }
    eax--;
    eax = 0;
    if (eax == 0) {
        r15 = rax;
    }
    goto label_0;
label_22:
    usage (0);
label_19:
    eax = *(rsp);
    eax += 0xffffff80;
    if (eax > 7) {
        goto label_27;
    }
    rdx = 0x00009920;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (8 cases) at 0x9920 */
    void (*rax)() ();
    rdi = dbg_base2_length;
    rsi = sym_isbase2;
    rcx = dbg_base2lsbf_encode;
    rdx = dbg_base2_decode_ctx_init;
    rax = sym_base2lsbf_decode_ctx;
label_4:
    *(obj.base_decode_ctx) = rax;
    rax = *(obj.optind);
    *(obj.base_decode_ctx_init) = rdx;
    edx = ebp;
    edx -= eax;
    *(obj.base_length) = rdi;
    edx--;
    *(obj.isbase) = rsi;
    *(obj.base_encode) = rcx;
    if (edx > 0) {
        goto label_28;
    }
    if (eax >= ebp) {
        goto label_29;
    }
    rax = *((rbx + rax*8));
    rdi = rax;
    *((rsp + 0x28)) = rax;
    eax = strcmp (rdi, 0x00009810);
    if (eax == 0) {
        goto label_30;
    }
    rax = fopen (*((rsp + 0x28)), 0x0000988c);
    r12 = rax;
    if (rax == 0) {
        goto label_31;
    }
label_10:
    fadvise (r12, 2);
    if (r14b == 0) {
        goto label_32;
    }
    rax = stdout;
    edi = 0x1068;
    eax = uint64_t (*base_length)(uint64_t) (rax);
    rdi = (int64_t) eax;
    rax = xmalloc (rdi);
    r13 = rax;
    rax = xmalloc (0x1068);
    r14 = rax;
    rax = rsp + 0x40;
    rdi = rax;
    uint64_t (*base_decode_ctx_init)(uint64_t) (0);
label_5:
    *(rsp) = r13;
    ebx = 0;
    *((rsp + 0x20)) = r14;
    while (*((rsp + 0x18)) == 0) {
label_1:
        rbx += rbp;
        ebp = *(r12);
        ebp &= 0x20;
        if (ebp != 0) {
            goto label_33;
        }
        edi = 0x1068;
        rax = uint64_t (*base_length)() ();
        rax = (int64_t) eax;
        if (rbx >= rax) {
            goto label_34;
        }
        eax = *(r12);
        if ((al & 0x10) != 0) {
            goto label_35;
        }
        edi = 0x1068;
        eax = uint64_t (*base_length)() ();
        rcx = r12;
        esi = 1;
        rdx = (int64_t) eax;
        rax = *(rsp);
        rdx -= rbx;
        rdi = rax + rbx;
        rax = fread_unlocked ();
    }
    if (rax <= 0) {
        goto label_1;
    }
    r14d = 0;
    goto label_36;
label_2:
    if (*(r15) == 0x3d) {
        goto label_37;
    }
    rbp--;
    rax = *(rsp);
    r9 = rbp;
    r9 -= r14;
    memmove (r15, rax + r13 + 1, r9);
label_3:
    if (rbp <= r14) {
        goto label_1;
    }
label_36:
    rax = *(rsp);
    r13 = rbx + r14;
    r15 = rax + r13;
    edi = *(r15);
    al = uint64_t (*isbase)() ();
    if (al == 0) {
        goto label_2;
    }
label_37:
    r14++;
    goto label_3;
    rdi = dbg_base2_length;
    rsi = sym_isbase2;
    rcx = dbg_base2msbf_encode;
    rdx = dbg_base2_decode_ctx_init;
    rax = sym_base2msbf_decode_ctx;
    goto label_4;
    rdi = dbg_base16_length;
    rsi = dbg_isbase16;
    rcx = dbg_base16_encode;
    rdx = dbg_base16_decode_ctx_init;
    rax = sym_base16_decode_ctx;
    goto label_4;
    rdi = dbg_base32_length_wrapper;
    rsi = sym_isbase32hex;
    rcx = sym_base32hex_encode;
    rdx = dbg_base32hex_decode_ctx_init_wrapper;
    rax = dbg_base32hex_decode_ctx_wrapper;
    goto label_4;
    rsi = sym_isbase32;
    rcx = dbg_base32_encode;
    rdi = dbg_base32_length_wrapper;
    rdx = dbg_base32_decode_ctx_init_wrapper;
    rax = dbg_base32_decode_ctx_wrapper;
    goto label_4;
    rdi = dbg_base64_length_wrapper;
    rsi = dbg_isbase64url;
    rcx = dbg_base64url_encode;
    rdx = dbg_base64url_decode_ctx_init_wrapper;
    rax = dbg_base64url_decode_ctx_wrapper;
    goto label_4;
    rsi = sym_isbase64;
    rcx = dbg_base64_encode;
    rdi = dbg_base64_length_wrapper;
    rdx = dbg_base64_decode_ctx_init_wrapper;
    rax = dbg_base64_decode_ctx_wrapper;
    goto label_4;
    rdi = dbg_z85_length;
    rsi = dbg_isz85;
    rcx = dbg_z85_encode;
    rdx = dbg_z85_decode_ctx_init;
    rax = sym_z85_decode_ctx;
    goto label_4;
label_34:
    r13 = *(rsp);
    r14 = *((rsp + 0x20));
    eax = *(r12);
    goto label_38;
label_6:
    if (ebp != 0) {
        goto label_5;
    }
label_7:
    edx = 0;
    rax = base_decode_ctx;
    *((rsp + 0x38)) = 0x1068;
    if (ebp == 0) {
        rdx = rbx;
    }
label_8:
    rdi = *((rsp + 0x10));
    rcx = r14;
    r8 = rsp + 0x38;
    rsi = r13;
    eax = void (*rax)() ();
    rcx = *((rsp + 8));
    rdx = *((rsp + 0x38));
    rdi = r14;
    esi = 1;
    r15d = eax;
    rax = fwrite_unlocked ();
    if (rax < *((rsp + 0x38))) {
        goto label_39;
    }
    if (r15b == 0) {
        goto label_40;
    }
    eax = *(r12);
    ebp++;
label_38:
    if ((al & 0x10) == 0) {
        goto label_6;
    }
label_9:
    if (ebp == 2) {
        goto label_11;
    }
    if (ebp != 1) {
        goto label_7;
    }
    eax = *((rsp + 0x40));
    if (eax != 0) {
        *((rsp + 0x38)) = 0x1068;
        rax = base_decode_ctx;
        edx = 0;
        goto label_8;
label_35:
        r13 = *(rsp);
        r14 = *((rsp + 0x20));
        goto label_9;
    }
label_11:
    rsi = *((rsp + 0x28));
    finish_and_exit (r12);
label_28:
    rax = quote (*((rbx + rax*8 + 8)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "extra operand %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    usage (1);
label_29:
    rax = 0x00009810;
    *((rsp + 0x28)) = rax;
label_30:
    r12 = stdin;
    goto label_10;
label_32:
    rbp = stdout;
    ebx = 0;
    rax = xmalloc (0x7800);
    edi = 0x7800;
    r13 = rax;
    eax = uint64_t (*base_length)() ();
    rdi = (int64_t) eax;
    rax = xmalloc (rdi);
    *((rsp + 0x10)) = r15;
    *(rsp) = rax;
label_14:
    r14d = 0;
    while ((al & 0x30) == 0) {
        if (r14 > 0x77ff) {
            goto label_41;
        }
        edx = 0x7800;
        rdi = r13 + r14;
        rcx = r12;
        esi = 1;
        rdx -= r14;
        rax = fread_unlocked ();
        r14 += rax;
        eax = *(r12);
    }
    if (r14 > 0) {
        goto label_41;
    }
label_13:
    r15 = *((rsp + 0x10));
    if (rbx > 0) {
        if (r15 != 0) {
            goto label_42;
        }
    }
label_18:
    if ((al & 0x20) == 0) {
        goto label_11;
    }
    edx = 5;
    rax = dcgettext (0, "read error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_41:
    r10 = base_encode;
    edi = r14d;
    eax = uint64_t (*base_length)(uint64_t) (r10);
    rdx = *(rsp);
    rsi = r14;
    rdi = r13;
    r10 = *((rsp + 8));
    rcx = (int64_t) eax;
    void (*r10)() ();
    edi = r14d;
    eax = uint64_t (*base_length)() ();
    if (*((rsp + 0x10)) == 0) {
        goto label_43;
    }
    r15 = (int64_t) eax;
    r9d = 0;
    if (r15 <= 0) {
        goto label_16;
    }
    *((rsp + 8)) = r12;
    r12 = rbx;
    rbx = *((rsp + 0x10));
    *((rsp + 0x18)) = r13;
    r13 = rbp;
    *((rsp + 0x20)) = r14;
    r14 = r9;
    while (r15 == 0) {
        rax = *((r13 + 0x28));
        if (rax >= *((r13 + 0x30))) {
            goto label_44;
        }
        rdx = rax + 1;
        *((r13 + 0x28)) = rdx;
        *(rax) = 0xa;
label_15:
        r12d = 0;
label_12:
        if (r14 >= rbp) {
            goto label_45;
        }
        r15 = rbp;
        rax = rbx;
        r15 -= r14;
        rax -= r12;
        if (r15 > rax) {
            r15 = rax;
        }
    }
    rax = *(rsp);
    rcx = stdout;
    rdx = r15;
    esi = 1;
    rdi = rax + r14;
    rax = fwrite_unlocked ();
    if (r15 > rax) {
        goto label_46;
    }
    r12 += r15;
    r14 += r15;
    goto label_12;
label_45:
    rbx = r12;
    r12 = *((rsp + 8));
    r13 = *((rsp + 0x18));
    r14 = *((rsp + 0x20));
label_16:
    eax = *(r12);
    if ((al & 0x30) != 0) {
        goto label_13;
    }
    if (r14 == 0x7800) {
        goto label_14;
    }
    goto label_13;
label_44:
    esi = 0xa;
    rdi = r13;
    eax = overflow ();
    eax++;
    if (eax != 0) {
        goto label_15;
    }
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    eax = error (1, *(rax), r12);
label_43:
    r10 = (int64_t) eax;
    rcx = stdout;
    rdi = *(rsp);
    esi = 1;
    rdx = r10;
    *((rsp + 8)) = r10;
    rax = fwrite_unlocked ();
    r10 = *((rsp + 8));
    if (r10 <= rax) {
        goto label_16;
    }
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_27:
    edx = 5;
    rax = dcgettext (0, "missing encoding type");
    eax = 0;
    error (0, 0, rax);
    goto label_17;
label_42:
    rax = *((rbp + 0x28));
    if (rax >= *((rbp + 0x30))) {
        goto label_47;
    }
    rdx = rax + 1;
    *((rbp + 0x28)) = rdx;
    *(rax) = 0xa;
    do {
        eax = *(r12);
        goto label_18;
label_40:
        edx = 5;
        rax = dcgettext (0, "invalid input");
        eax = 0;
        error (1, 0, rax);
label_39:
        edx = 5;
        rax = dcgettext (0, "write error");
        r12 = rax;
        rax = errno_location ();
        eax = 0;
        error (1, *(rax), r12);
label_33:
        edx = 5;
        rax = dcgettext (0, "read error");
        r12 = rax;
        rax = errno_location ();
        eax = 0;
        error (1, *(rax), r12);
label_47:
        esi = 0xa;
        rdi = rbp;
        eax = overflow ();
        eax++;
    } while (eax != 0);
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_31:
    rdx = *((rsp + 0x28));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x00009840);
label_26:
    rax = quote (*(obj.optarg), rsi, rdx, rcx, r8);
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "invalid wrap size");
    r8 = rbx;
    rcx = rax;
    eax = 0;
    error (1, 0, "%s: %s");
label_46:
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
}

/* /tmp/tmpaqk9hiad @ 0x3cf0 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "basenc encode or decode FILE, or standard input, to standard output.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --base64          same as 'base64' program (RFC4648 section 4)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --base64url       file- and url-safe base64 (RFC4648 section 5)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --base32          same as 'base32' program (RFC4648 section 6)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --base32hex       extended hex alphabet base32 (RFC4648 section 7)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --base16          hex encoding (RFC4648 section 8)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --base2msbf       bit string with most significant bit (msb) first\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --base2lsbf       bit string with least significant bit (lsb) first\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -d, --decode          decode data\n  -i, --ignore-garbage  when decoding, ignore non-alphabet characters\n  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).\n                          Use 0 to disable line wrapping\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --z85             ascii85-like encoding (ZeroMQ spec:32/Z85);\n                        when encoding, input length must be a multiple of 4;\n                        when decoding, input length must be a multiple of 5\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWhen decoding, the input may contain newlines in addition to the bytes of\nthe formal alphabet.  Use --ignore-garbage to attempt to recover\nfrom any other non-alphabet bytes in the encoded stream.\n");
    rsi = r12;
    r12 = "basenc";
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000976d;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x000097e7;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000097f1, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000a1a1;
    r12 = 0x00009789;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000097f1, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "basenc";
        printf_chk ();
        r12 = 0x00009789;
    }
label_5:
    r13 = "basenc";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpaqk9hiad @ 0x2680 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpaqk9hiad @ 0x2660 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpaqk9hiad @ 0x2600 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpaqk9hiad @ 0x2530 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpaqk9hiad @ 0x2550 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpaqk9hiad @ 0x25f0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpaqk9hiad @ 0x4610 */
 
int64_t dbg_base32_encode_alloc (int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* idx_t base32_encode_alloc(char const * in,idx_t inlen,char ** out); */
    rax = rsi;
    ecx = 0;
    rbx = rdx;
    rdx = 0x6666666666666667;
    rdx:rax = rax * rdx;
    rax = rsi;
    rax >>= 0x3f;
    rdx >>= 1;
    rdx -= rax;
    rax = rdx * 5;
    cl = (rsi != rax) ? 1 : 0;
    rdx += rcx;
    r12 = rdx * 8;
    __asm ("seto al");
    if (rsi >= 0) {
        eax = (int32_t) al;
        if (rax != 0) {
            goto label_0;
        }
        r14 = r12 + 1;
        r13 = rdi;
        rax = malloc (r14);
        *(rbx) = rax;
        rdx = rax;
        if (rax == 0) {
            goto label_1;
        }
        base32_encode (r13, rbp, rdx, r14);
        rax = r12;
        return rax;
    }
label_0:
    *(rbx) = 0;
    r12d = 0;
    do {
        rax = r12;
        return rax;
label_1:
        r12 = r14;
    } while (1);
}

/* /tmp/tmpaqk9hiad @ 0x6c30 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x7320 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x7150 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x26df)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x4e20 */
 
uint64_t isbase64 (void) {
    edi = (int32_t) dil;
    rax = obj_b64;
    eax = *((rax + rdi));
    eax = ~eax;
    al >>= 7;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x7030 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x26d5)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x77c0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x6cd0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x26ca)() ();
    }
    if (rdx == 0) {
        void (*0x26ca)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x71e0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x82b0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x5120 */
 
int64_t dbg_base64_decode_alloc_ctx (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    idx_t needlen;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool base64_decode_alloc_ctx(base64_decode_context * ctx,char const * in,idx_t inlen,char ** out,idx_t * outlen); */
    r14 = rsi;
    r13 = rdi;
    r12 = rdx;
    rbx = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    rax = rdx;
    rax >>= 2;
    *((rsp + 0x10)) = rdi;
    rax = malloc (rax + rax*2 + 3);
    *(rbx) = rax;
    if (rax == 0) {
        goto label_1;
    }
    al = base64_decode_ctx (r13, r14, r12, rax, rsp + 0x10);
    if (al == 0) {
        goto label_2;
    }
    if (rbp == 0) {
        goto label_1;
    }
    rdx = *((rsp + 0x10));
    *(rbp) = rdx;
    do {
label_0:
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_3;
        }
        return rax;
label_1:
        eax = 1;
    } while (1);
label_2:
    *((rsp + 0xf)) = al;
    free (*(rbx));
    *(rbx) = 0;
    eax = *((rsp + 0xf));
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x8330 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x52c0 */
 
uint32_t dbg_fadvise (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void fadvise(FILE * fp,fadvice_t advice); */
    if (rdi != 0) {
        r12d = esi;
        eax = fileno (rdi);
        ecx = r12d;
        edx = 0;
        esi = 0;
        edi = eax;
        void (*0x2500)() ();
    }
    return eax;
}

/* /tmp/tmpaqk9hiad @ 0x2580 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpaqk9hiad @ 0x6f30 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x7fd0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x77a0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpaqk9hiad @ 0x53d0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x2630)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpaqk9hiad @ 0x6c70 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x81b0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpaqk9hiad @ 0x7df0 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpaqk9hiad @ 0x2560 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpaqk9hiad @ 0x51e0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpaqk9hiad @ 0x8a90 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpaqk9hiad @ 0x23e0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpaqk9hiad @ 0x77e0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000a148;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000a15b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000a448;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xa448 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2680)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2680)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2680)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpaqk9hiad @ 0x7c70 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaqk9hiad @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpaqk9hiad @ 0x8310 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x8d00 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpaqk9hiad @ 0x8010 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x83d0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaqk9hiad @ 0x2380 */
 
void free (void) {
    /* [15] -r-x section size 832 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpaqk9hiad @ 0x2390 */
 
void strtoimax (void) {
    __asm ("bnd jmp qword [reloc.strtoimax]");
}

/* /tmp/tmpaqk9hiad @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x10)) += ch;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("insb byte [rdi], dx");
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dil;
    *(0x2800403d) += cl;
    *(rdi) += ah;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) ^= dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) ^= dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    bh += bh;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += dh;
    __asm ("insd dword [rdi], dx");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbp) ^= ebp;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rdi) = ch;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) = ch;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmpaqk9hiad @ 0x23d0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpaqk9hiad @ 0x2400 */
 
void fread_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fread_unlocked]");
}

/* /tmp/tmpaqk9hiad @ 0x2410 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpaqk9hiad @ 0x2420 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpaqk9hiad @ 0x2430 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpaqk9hiad @ 0x2450 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpaqk9hiad @ 0x2480 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpaqk9hiad @ 0x2490 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpaqk9hiad @ 0x24a0 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpaqk9hiad @ 0x24b0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmpaqk9hiad @ 0x24d0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpaqk9hiad @ 0x24e0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpaqk9hiad @ 0x24f0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpaqk9hiad @ 0x2500 */
 
void posix_fadvise (void) {
    __asm ("bnd jmp qword [reloc.posix_fadvise]");
}

/* /tmp/tmpaqk9hiad @ 0x2510 */
 
void memchr (void) {
    __asm ("bnd jmp qword [reloc.memchr]");
}

/* /tmp/tmpaqk9hiad @ 0x2520 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpaqk9hiad @ 0x2570 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpaqk9hiad @ 0x25a0 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpaqk9hiad @ 0x25c0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpaqk9hiad @ 0x25d0 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmpaqk9hiad @ 0x2610 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmpaqk9hiad @ 0x2630 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpaqk9hiad @ 0x2640 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmpaqk9hiad @ 0x2650 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpaqk9hiad @ 0x2690 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpaqk9hiad @ 0x26a0 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpaqk9hiad @ 0x26b0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpaqk9hiad @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 848 named .plt */
    __asm ("bnd jmp qword [0x0000de18]");
}

/* /tmp/tmpaqk9hiad @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaqk9hiad @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}
