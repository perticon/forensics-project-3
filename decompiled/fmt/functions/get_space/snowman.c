uint32_t get_space(void** rdi, uint32_t esi) {
    uint32_t eax3;
    void** rbx4;
    void* eax5;
    void** rax6;
    int64_t rdx7;
    void* eax8;
    int64_t rax9;

    eax3 = esi;
    rbx4 = rdi;
    while (1) {
        if (eax3 == 32) {
            eax5 = in_column;
            in_column = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax5) + 1);
            rax6 = *reinterpret_cast<void***>(rbx4 + 8);
            if (reinterpret_cast<unsigned char>(rax6) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4 + 16))) 
                goto addr_2f34_4; else 
                goto addr_2f5d_5;
        }
        if (eax3 != 9) 
            break;
        *reinterpret_cast<void**>(&rdx7) = in_column;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
        tabs = 1;
        eax8 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx7 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx7)) < reinterpret_cast<int32_t>(0)) 
            goto addr_2f1a_8;
        eax8 = *reinterpret_cast<void**>(&rdx7);
        addr_2f1a_8:
        *reinterpret_cast<int32_t*>(&rax9) = reinterpret_cast<int32_t>(eax8) >> 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        in_column = reinterpret_cast<void*>(static_cast<uint32_t>(rax9 * 8 + 8));
        rax6 = *reinterpret_cast<void***>(rbx4 + 8);
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4 + 16))) {
            addr_2f5d_5:
            eax3 = fun_2380(rbx4);
        } else {
            addr_2f34_4:
            *reinterpret_cast<void***>(rbx4 + 8) = rax6 + 1;
            eax3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax6));
        }
    }
    return eax3;
}