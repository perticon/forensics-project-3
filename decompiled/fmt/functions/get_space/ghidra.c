void get_space(_IO_FILE *param_1,uint param_2)

{
  int iVar1;
  byte *pbVar2;
  
  do {
    while (param_2 != 0x20) {
      if (param_2 != 9) {
        return;
      }
      tabs = 1;
      iVar1 = in_column + 7;
      if (-1 < in_column) {
        iVar1 = in_column;
      }
      in_column = (iVar1 >> 3) * 8 + 8;
      pbVar2 = (byte *)param_1->_IO_read_ptr;
      if (pbVar2 < param_1->_IO_read_end) goto LAB_00102f34;
LAB_00102f5d:
      param_2 = __uflow(param_1);
    }
    in_column = in_column + 1;
    pbVar2 = (byte *)param_1->_IO_read_ptr;
    if (param_1->_IO_read_end <= pbVar2) goto LAB_00102f5d;
LAB_00102f34:
    param_1->_IO_read_ptr = (char *)(pbVar2 + 1);
    param_2 = (uint)*pbVar2;
  } while( true );
}