int32_t get_space(struct _IO_FILE * f, uint32_t c) {
    int64_t v1 = (int64_t)f;
    int64_t * v2 = (int64_t *)(v1 + 8);
    int64_t * v3 = (int64_t *)(v1 + 16);
    int64_t v4 = c; // 0x2ef6
    int64_t v5; // 0x2ef0
    int32_t result; // 0x2ef0
    while (true) {
      lab_0x2f3f_2:
        // 0x2f3f
        result = v4;
        if (result != 32) {
            if (result != 9) {
                // break -> 0x2f70
                break;
            }
            int32_t v6 = in_column; // 0x2f05
            *(char *)&tabs = 1;
            in_column = (v6 >= 0 ? v6 : v6 + 7) + 8 & -8;
            uint64_t v7 = *v2; // 0x2f2a
            v5 = v7;
            if (v7 >= *v3) {
                // 0x2f3f
                v4 = function_2380();
                goto lab_0x2f3f_2;
            } else {
                goto lab_0x2f34;
            }
        } else {
            // 0x2f44
            in_column++;
            uint64_t v8 = *v2; // 0x2f53
            v5 = v8;
            if (v8 < *v3) {
                goto lab_0x2f34;
            } else {
                // 0x2f3f
                v4 = function_2380();
                goto lab_0x2f3f_2;
            }
        }
    }
    // 0x2f70
    return result;
  lab_0x2f34:
    // 0x2f34
    *v2 = v5 + 1;
    // 0x2f3f
    v4 = (int64_t)*(char *)v5;
    goto lab_0x2f3f_2;
}