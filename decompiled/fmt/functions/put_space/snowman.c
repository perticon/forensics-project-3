void put_space(int64_t rdi, void** rsi) {
    int64_t rdx3;
    int1_t zf4;
    int64_t rbx5;
    void* eax6;
    void* eax7;
    int32_t ebp8;
    void** rdi9;
    void** rax10;
    int64_t rdx11;
    void* eax12;
    int64_t rax13;
    void** rdi14;
    void** rax15;
    void* eax16;
    void* eax17;

    *reinterpret_cast<void**>(&rdx3) = out_column;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    zf4 = tabs == 0;
    *reinterpret_cast<void**>(&rbx5) = reinterpret_cast<void*>(static_cast<uint32_t>(rdx3 + rdi));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
    if (!zf4) {
        eax6 = reinterpret_cast<void*>(static_cast<uint32_t>(rbx5 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)) >= reinterpret_cast<int32_t>(0)) {
            eax6 = *reinterpret_cast<void**>(&rbx5);
        }
        eax7 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax6) & 0xfffffff8);
        ebp8 = reinterpret_cast<int32_t>(eax6) >> 3;
        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(rdx3 + 1)) < reinterpret_cast<int32_t>(eax7) && reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx3)) < reinterpret_cast<int32_t>(eax7)) {
            do {
                rdi9 = stdout;
                rax10 = *reinterpret_cast<void***>(rdi9 + 40);
                if (reinterpret_cast<unsigned char>(rax10) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi9 + 48))) {
                    fun_24b0();
                } else {
                    *reinterpret_cast<void***>(rdi9 + 40) = rax10 + 1;
                    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(9);
                }
                *reinterpret_cast<void**>(&rdx11) = out_column;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
                eax12 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx11 + 7));
                if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx11)) >= reinterpret_cast<int32_t>(0)) {
                    eax12 = *reinterpret_cast<void**>(&rdx11);
                }
                *reinterpret_cast<int32_t*>(&rax13) = (reinterpret_cast<int32_t>(eax12) >> 3) + 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
                *reinterpret_cast<void**>(&rdx3) = reinterpret_cast<void*>(static_cast<uint32_t>(rax13 * 8));
                out_column = *reinterpret_cast<void**>(&rdx3);
            } while (ebp8 > *reinterpret_cast<int32_t*>(&rax13));
        }
    }
    if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)) > reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx3))) {
        do {
            rdi14 = stdout;
            rax15 = *reinterpret_cast<void***>(rdi14 + 40);
            if (reinterpret_cast<unsigned char>(rax15) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi14 + 48))) {
                fun_24b0();
            } else {
                *reinterpret_cast<void***>(rdi14 + 40) = rax15 + 1;
                *reinterpret_cast<void***>(rax15) = reinterpret_cast<void**>(32);
            }
            eax16 = out_column;
            eax17 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax16) + 1);
            out_column = eax17;
        } while (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)));
    }
    return;
}