void put_space(int32_t space) {
    uint32_t v1 = out_column; // 0x30b6
    int64_t v2 = v1; // 0x30b6
    uint32_t v3 = v1 + space; // 0x30c3
    uint64_t v4 = (int64_t)v3; // 0x30c3
    int64_t v5 = v2; // 0x30c6
    if (*(char *)&tabs != 0) {
        int64_t v6 = v3 >= 0 ? v4 : v4 + 7; // 0x30d0
        uint64_t v7 = v6 & 0xfffffff8; // 0x30d5
        v5 = v2;
        if (v7 > (int64_t)(v1 + 1) == v7 > v2) {
            int64_t v8 = (int64_t)g31; // 0x30e8
            int64_t * v9 = (int64_t *)(v8 + 40); // 0x30ef
            uint64_t v10 = *v9; // 0x30ef
            if (v10 >= *(int64_t *)(v8 + 48)) {
                // 0x3180
                function_24b0();
            } else {
                // 0x30fd
                *v9 = v10 + 1;
                *(char *)v10 = 9;
            }
            int32_t v11 = out_column; // 0x3108
            uint32_t v12 = ((v11 >= 0 ? v11 : v11 + 7) >> 3) + 1; // 0x3119
            int32_t v13 = 8 * v12; // 0x311c
            out_column = v13;
            while ((int32_t)v6 >> 3 > v12) {
                // 0x30e8
                v8 = (int64_t)g31;
                v9 = (int64_t *)(v8 + 40);
                v10 = *v9;
                if (v10 >= *(int64_t *)(v8 + 48)) {
                    // 0x3180
                    function_24b0();
                } else {
                    // 0x30fd
                    *v9 = v10 + 1;
                    *(char *)v10 = 9;
                }
                // 0x3108
                v11 = out_column;
                v12 = ((v11 >= 0 ? v11 : v11 + 7) >> 3) + 1;
                v13 = 8 * v12;
                out_column = v13;
            }
            // 0x312d
            v5 = v13;
        }
    }
    // 0x312d
    if (v5 >= v4) {
        // 0x3167
        return;
    }
    int64_t v14 = (int64_t)g31; // 0x3138
    int64_t * v15 = (int64_t *)(v14 + 40); // 0x313f
    uint64_t v16 = *v15; // 0x313f
    if (v16 >= *(int64_t *)(v14 + 48)) {
        // 0x3170
        function_24b0();
    } else {
        // 0x3149
        *v15 = v16 + 1;
        *(char *)v16 = 32;
    }
    int32_t v17 = out_column + 1; // 0x315a
    out_column = v17;
    while (v3 > v17) {
        // 0x3138
        v14 = (int64_t)g31;
        v15 = (int64_t *)(v14 + 40);
        v16 = *v15;
        if (v16 >= *(int64_t *)(v14 + 48)) {
            // 0x3170
            function_24b0();
        } else {
            // 0x3149
            *v15 = v16 + 1;
            *(char *)v16 = 32;
        }
        // 0x3154
        v17 = out_column + 1;
        out_column = v17;
    }
}