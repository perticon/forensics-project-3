get_paragraph (FILE *f)
{
  int c;

  last_line_length = 0;
  c = next_char;

  /* Scan (and copy) blank lines, and lines not introduced by the prefix.  */

  while (c == '\n' || c == EOF
         || next_prefix_indent < prefix_lead_space
         || in_column < next_prefix_indent + prefix_full_length)
    {
      c = copy_rest (f, c);
      if (c == EOF)
        {
          next_char = EOF;
          return false;
        }
      putchar ('\n');
      c = get_prefix (f);
    }

  /* Got a suitable first line for a paragraph.  */

  prefix_indent = next_prefix_indent;
  first_indent = in_column;
  wptr = parabuf;
  word_limit = word;
  c = get_line (f, c);
  set_other_indent (same_para (c));

  /* Read rest of paragraph (unless split is specified).  */

  if (split)
    {
      /* empty */
    }
  else if (crown)
    {
      if (same_para (c))
        {
          do
            {			/* for each line till the end of the para */
              c = get_line (f, c);
            }
          while (same_para (c) && in_column == other_indent);
        }
    }
  else if (tagged)
    {
      if (same_para (c) && in_column != first_indent)
        {
          do
            {			/* for each line till the end of the para */
              c = get_line (f, c);
            }
          while (same_para (c) && in_column == other_indent);
        }
    }
  else
    {
      while (same_para (c) && in_column == other_indent)
        c = get_line (f, c);
    }

  /* Tell static analysis tools that using word_limit[-1] is ok.
     word_limit is guaranteed to have been incremented by get_line.  */
  assert (word < word_limit);

  (word_limit - 1)->period = (word_limit - 1)->final = true;
  next_char = c;
  return true;
}