byte main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  byte bVar2;
  byte bVar3;
  uint uVar4;
  int iVar5;
  size_t sVar6;
  char *pcVar7;
  FILE *pFVar8;
  undefined8 uVar9;
  int *piVar10;
  ulong extraout_RDX;
  ulong extraout_RDX_00;
  ulong extraout_RDX_01;
  ulong uVar11;
  ulong extraout_RDX_02;
  ulong extraout_RDX_03;
  undefined8 *puVar12;
  undefined auVar13 [16];
  undefined8 uStack88;
  char *local_50;
  char *local_48;
  char *local_40;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  uniform = 0;
  split = 0;
  tagged = 0;
  crown = 0;
  max_width = 0x4b;
  prefix = "";
  prefix_full_length = 0;
  prefix_lead_space = 0;
  prefix_length = 0;
  local_50 = (char *)0x0;
  if ((1 < param_1) && (pcVar7 = (char *)param_2[1], puVar12 = param_2, *pcVar7 == '-'))
  goto LAB_00102967;
  do {
    local_48 = (char *)0x0;
    while( true ) {
      uVar4 = getopt_long(param_1,param_2,"0123456789cstuw:p:g:",long_options,0);
      pcVar7 = local_50;
      if (uVar4 == 0xffffffff) {
        if (local_50 == (char *)0x0) {
          if (local_48 != (char *)0x0) goto LAB_00102aff;
        }
        else {
          uVar9 = dcgettext(0,"invalid width",5);
          iVar5 = xdectoumax(pcVar7,0,0x9c4,"",uVar9,0);
          max_width = iVar5;
          if (local_48 != (char *)0x0) {
            uVar9 = dcgettext(0,"invalid width",5);
            goal_width = xdectoumax(local_48,0,(long)iVar5,"",uVar9,0);
            uVar11 = extraout_RDX;
            goto LAB_00102a11;
          }
        }
        goal_width = (max_width * 0xbb) / 200;
        uVar11 = (long)(max_width * 0xbb) % 200 & 0xffffffff;
        goto LAB_00102a11;
      }
      if (0x77 < (int)uVar4) goto switchD_0010282b_caseD_64;
      if ((int)uVar4 < 99) break;
      switch(uVar4) {
      case 99:
        crown = 1;
        break;
      default:
        goto switchD_0010282b_caseD_64;
      case 0x67:
        local_48 = optarg;
        break;
      case 0x70:
        prefix_lead_space = 0;
        prefix = optarg;
        if (*optarg == ' ') {
          do {
            prefix_lead_space = (1 - (int)optarg) + (int)prefix;
            prefix = prefix + 1;
          } while (*prefix == ' ');
        }
        local_40 = prefix;
        sVar6 = strlen(prefix);
        prefix_full_length = (int)sVar6;
        pcVar7 = local_40 + prefix_full_length;
        if (local_40 < pcVar7) {
          do {
            if (pcVar7[-1] != ' ') break;
            pcVar7 = pcVar7 + -1;
          } while (local_40 != pcVar7);
        }
        *pcVar7 = '\0';
        prefix_length = (int)pcVar7 - (int)local_40;
        break;
      case 0x73:
        split = 1;
        break;
      case 0x74:
        tagged = 1;
        break;
      case 0x75:
        uniform = 1;
        break;
      case 0x77:
        local_50 = optarg;
      }
    }
    if (uVar4 == 0xffffff7d) {
      version_etc(stdout,&DAT_0010803a,"GNU coreutils",Version,"Ross Paterson",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (uVar4 != 0xffffff7e) break;
    pcVar7 = (char *)usage();
    puVar12 = param_2;
LAB_00102967:
    param_2 = puVar12;
    if ((int)pcVar7[1] - 0x30U < 10) {
      local_50 = pcVar7 + 1;
      param_2 = puVar12 + 1;
      param_1 = param_1 + -1;
      *param_2 = *puVar12;
    }
  } while( true );
switchD_0010282b_caseD_64:
  if (uVar4 - 0x30 < 10) {
    local_50 = (char *)((ulong)local_50 & 0xffffffff00000000 | (ulong)uVar4);
    uVar9 = dcgettext(0,
                      "invalid option -- %c; -WIDTH is recognized only when it is the first\noption; use -w N instead"
                      ,5);
    error(0,0,uVar9,(ulong)local_50 & 0xffffffff);
  }
  usage(1);
LAB_00102aff:
  uVar9 = dcgettext(0,"invalid width",5);
  goal_width = xdectoumax(local_48,0,0x4b,"",uVar9,0);
  max_width = goal_width + 10;
  uVar11 = extraout_RDX_02;
LAB_00102a11:
  if (optind == param_1) {
    bVar3 = fmt(stdin,&DAT_0010812a,uVar11);
  }
  else {
    bVar1 = false;
    bVar3 = 1;
    if (param_1 <= optind) goto LAB_00102ab1;
    do {
      pcVar7 = (char *)param_2[optind];
      if ((*pcVar7 == '-') && (pcVar7[1] == '\0')) {
        bVar1 = true;
        bVar2 = fmt(stdin,pcVar7,uVar11);
        bVar3 = bVar3 & bVar2;
        uVar11 = extraout_RDX_00;
      }
      else {
        pFVar8 = fopen(pcVar7,"r");
        if (pFVar8 == (FILE *)0x0) {
          local_50 = (char *)quotearg_style(4,pcVar7);
          uVar9 = dcgettext(0,"cannot open %s for reading",5);
          piVar10 = __errno_location();
          bVar3 = 0;
          error(0,*piVar10,uVar9,local_50);
          uVar11 = extraout_RDX_03;
        }
        else {
          bVar2 = fmt(pFVar8);
          bVar3 = bVar3 & bVar2;
          uVar11 = extraout_RDX_01;
        }
      }
      optind = optind + 1;
    } while (optind < param_1);
    if (!bVar1) goto LAB_00102ab1;
  }
  iVar5 = rpl_fclose(stdin);
  if (iVar5 != 0) {
    uVar9 = dcgettext(0,"closing standard input",5);
    piVar10 = __errno_location();
    auVar13 = error(1,*piVar10,"%s",uVar9);
    uVar9 = uStack88;
    uStack88 = SUB168(auVar13,0);
    (*(code *)PTR___libc_start_main_0010bfc0)
              (main,uVar9,&local_50,0,0,SUB168(auVar13 >> 0x40,0),&uStack88);
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
LAB_00102ab1:
  return bVar3 ^ 1;
}