void get_line(_IO_FILE *param_1,uint param_2)

{
  byte *pbVar1;
  byte *pbVar2;
  bool bVar3;
  undefined4 *puVar4;
  int iVar5;
  ushort **ppuVar6;
  undefined4 *puVar7;
  char *pcVar8;
  int iVar9;
  byte **ppbVar10;
  byte *pbVar11;
  
  ppuVar6 = __ctype_b_loc();
LAB_001034a0:
  do {
    puVar7 = wptr;
    *word_limit = (byte *)wptr;
    puVar4 = wptr;
    do {
      while( true ) {
        wptr = puVar7;
        if (wptr == &out_column) {
          wptr = puVar4;
          set_other_indent(1);
          flush_paragraph();
        }
        puVar7 = (undefined4 *)((long)wptr + 1);
        *(char *)wptr = (char)param_2;
        pbVar1 = (byte *)param_1->_IO_read_ptr;
        wptr = puVar7;
        if (pbVar1 < param_1->_IO_read_end) {
          param_1->_IO_read_ptr = (char *)(pbVar1 + 1);
          param_2 = (uint)*pbVar1;
        }
        else {
          param_2 = __uflow(param_1);
          puVar7 = wptr;
          if (param_2 == 0xffffffff) goto LAB_001034f7;
        }
        puVar4 = wptr;
        if ((int)param_2 < 0xe) break;
        if (param_2 == 0x20) goto LAB_001034f7;
      }
    } while ((int)param_2 < 9);
LAB_001034f7:
    ppbVar10 = word_limit;
    pbVar2 = *word_limit;
    iVar5 = (int)puVar7 - (int)pbVar2;
    *(int *)(word_limit + 1) = iVar5;
    iVar9 = in_column + iVar5;
    pbVar1 = pbVar2 + (long)iVar5 + -1;
    in_column = iVar9;
    pcVar8 = strchr("([\'`\"",(int)(char)*pbVar2);
    *(byte *)(ppbVar10 + 2) =
         *(byte *)(*ppuVar6 + *pbVar1) & 4 | pcVar8 != (char *)0x0 | *(byte *)(ppbVar10 + 2) & 0xfa;
    if (pbVar2 < pbVar1) {
      do {
        pbVar11 = pbVar1;
        iVar5 = (int)(char)*pbVar11;
        pcVar8 = strchr(")]\'\"",iVar5);
        if (pcVar8 == (char *)0x0) goto LAB_00103594;
        pbVar1 = pbVar11 + -1;
      } while (pbVar2 != pbVar11 + -1);
      iVar5 = (int)(char)pbVar11[-1];
    }
    else {
      iVar5 = (int)(char)*pbVar1;
    }
LAB_00103594:
    pcVar8 = strchr(".?!",iVar5);
    *(byte *)(ppbVar10 + 2) = *(byte *)(ppbVar10 + 2) & 0xfd | (pcVar8 != (char *)0x0) * '\x02';
    param_2 = get_space(param_1);
    ppbVar10 = word_limit;
    iVar9 = in_column - iVar9;
    *(int *)((long)word_limit + 0xc) = iVar9;
    if (param_2 == 0xffffffff) {
      *(byte *)(ppbVar10 + 2) = *(byte *)(ppbVar10 + 2) | 8;
LAB_0010368c:
      *(uint *)((long)ppbVar10 + 0xc) = ((*(byte *)(ppbVar10 + 2) & 8) != 0) + 1;
      if (ppbVar10 == (byte **)(unused_word_type + 0x9bf0)) goto LAB_00103720;
    }
    else {
      if ((*(byte *)(ppbVar10 + 2) & 2) == 0) {
        bVar3 = false;
LAB_00103607:
        *(byte *)(ppbVar10 + 2) = *(byte *)(ppbVar10 + 2) & 0xf7 | bVar3 * '\b';
        if (param_2 == 10) goto LAB_0010368c;
      }
      else {
        bVar3 = 1 < iVar9 || param_2 == 10;
        if (1 < iVar9 || param_2 == 10) goto LAB_00103607;
        *(byte *)(ppbVar10 + 2) = *(byte *)(ppbVar10 + 2) & 0xf7;
      }
      if (uniform != '\0') goto LAB_0010368c;
      if (ppbVar10 != (byte **)(unused_word_type + 0x9bf0)) {
        word_limit = ppbVar10 + 5;
        goto LAB_001034a0;
      }
LAB_00103720:
      set_other_indent(1);
      flush_paragraph();
      ppbVar10 = word_limit;
    }
    word_limit = ppbVar10 + 5;
    if ((param_2 == 10) || (param_2 == 0xffffffff)) {
      get_prefix(param_1);
      return;
    }
  } while( true );
}