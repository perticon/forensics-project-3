uint32_t get_line(void** rdi, void** rsi, void** rdx, void** rcx) {
    uint32_t ebp5;
    void** v6;
    unsigned char** rax7;
    void** rdx8;
    unsigned char** v9;
    void** rbp10;
    void** rax11;
    uint32_t eax12;
    uint32_t esi13;
    uint32_t eax14;
    void* ecx15;
    void* edx16;
    void** rbx17;
    uint32_t edx18;
    void* tmp32_19;
    void** rax20;
    void* ecx21;
    uint32_t eax22;
    void** rbx23;
    void* eax24;
    void** rax25;
    int64_t rdx26;
    void* eax27;
    int64_t rax28;
    void** rax29;
    void** rdx30;
    void** rax31;
    void** rbx32;
    uint32_t eax33;
    void** r13_34;
    void* ecx35;
    void** rbx36;
    void* rax37;
    void* ecx38;
    void* v39;
    void** r14_40;
    int64_t rax41;
    int64_t rsi42;
    unsigned char* rcx43;
    uint32_t eax44;
    uint32_t eax45;
    int64_t rax46;
    void** rax47;
    int64_t rax48;
    uint32_t eax49;
    uint32_t eax50;
    void** rdx51;
    void* eax52;
    int64_t rax53;
    uint32_t ecx54;
    uint32_t ecx55;
    uint32_t eax56;
    int1_t zf57;
    void* eax58;

    ebp5 = *reinterpret_cast<uint32_t*>(&rsi);
    v6 = rdi;
    rax7 = fun_26b0(rdi, rsi, rdx, rcx);
    rdx8 = word_limit;
    v9 = rax7;
    goto addr_34a0_2;
    addr_36c0_3:
    rbp10 = v6;
    rax11 = *reinterpret_cast<void***>(v6 + 8);
    in_column = reinterpret_cast<void*>(0);
    if (reinterpret_cast<unsigned char>(rax11) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v6 + 16))) {
        eax12 = fun_2380(v6, v6);
        esi13 = eax12;
    } else {
        *reinterpret_cast<void***>(v6 + 8) = rax11 + 1;
        esi13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax11));
    }
    eax14 = get_space(rbp10, esi13);
    ecx15 = prefix_length;
    edx16 = in_column;
    if (ecx15) {
        rbx17 = prefix;
        next_prefix_indent = edx16;
        edx18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx17));
        if (*reinterpret_cast<signed char*>(&edx18)) {
            do {
                if (edx18 != eax14) 
                    break;
                tmp32_19 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(in_column) + 1);
                in_column = tmp32_19;
                rax20 = *reinterpret_cast<void***>(rbp10 + 8);
                if (reinterpret_cast<unsigned char>(rax20) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp10 + 16))) {
                    *reinterpret_cast<void***>(rbp10 + 8) = rax20 + 1;
                    eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax20));
                } else {
                    eax14 = fun_2380(rbp10, rbp10);
                }
                edx18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx17 + 1));
                ++rbx17;
            } while (*reinterpret_cast<signed char*>(&edx18));
            goto addr_2ff4_14;
        } else {
            addr_2ff4_14:
            goto addr_2ef0_15;
        }
    } else {
        ecx21 = prefix_lead_space;
        if (reinterpret_cast<int32_t>(edx16) > reinterpret_cast<int32_t>(ecx21)) {
            edx16 = ecx21;
        }
        next_prefix_indent = edx16;
    }
    return eax14;
    addr_2ef0_15:
    eax22 = eax14;
    rbx23 = rbp10;
    while (1) {
        if (eax22 == 32) {
            eax24 = in_column;
            in_column = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax24) + 1);
            rax25 = *reinterpret_cast<void***>(rbx23 + 8);
            if (reinterpret_cast<unsigned char>(rax25) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx23 + 16))) 
                goto addr_2f34_22; else 
                goto addr_2f5d_23;
        }
        if (eax22 != 9) 
            break;
        *reinterpret_cast<void**>(&rdx26) = in_column;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
        tabs = 1;
        eax27 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx26 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx26)) < reinterpret_cast<int32_t>(0)) 
            goto addr_2f1a_26;
        eax27 = *reinterpret_cast<void**>(&rdx26);
        addr_2f1a_26:
        *reinterpret_cast<int32_t*>(&rax28) = reinterpret_cast<int32_t>(eax27) >> 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
        in_column = reinterpret_cast<void*>(static_cast<uint32_t>(rax28 * 8 + 8));
        rax25 = *reinterpret_cast<void***>(rbx23 + 8);
        if (reinterpret_cast<unsigned char>(rax25) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx23 + 16))) {
            addr_2f5d_23:
            eax22 = fun_2380(rbx23);
        } else {
            addr_2f34_22:
            *reinterpret_cast<void***>(rbx23 + 8) = rax25 + 1;
            eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax25));
        }
    }
    return eax22;
    while (1) {
        set_other_indent(1, 1);
        flush_paragraph(1, rsi);
        rax29 = wptr;
        while (1) {
            do {
                rdx30 = rax29 + 1;
                *reinterpret_cast<void***>(rax29) = *reinterpret_cast<void***>(&ebp5);
                rax31 = *reinterpret_cast<void***>(rbx32 + 8);
                wptr = rdx30;
                if (reinterpret_cast<unsigned char>(rax31) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx32 + 16))) {
                    eax33 = fun_2380(rbx32, rbx32);
                    ebp5 = eax33;
                    rax29 = wptr;
                    if (ebp5 == 0xffffffff) 
                        goto addr_34f7_32;
                } else {
                    *reinterpret_cast<void***>(rbx32 + 8) = rax31 + 1;
                    ebp5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax31));
                    rax29 = rdx30;
                }
                if (reinterpret_cast<int32_t>(ebp5) <= reinterpret_cast<int32_t>(13)) 
                    break;
                if (ebp5 == 32) 
                    goto addr_34f7_32;
            } while (!reinterpret_cast<int1_t>(rax29 == 0x17108));
            break;
            if (reinterpret_cast<int32_t>(ebp5) <= reinterpret_cast<int32_t>(8)) {
                addr_34af_39:
                if (rax29 == 0x17108) 
                    break; else 
                    continue;
            } else {
                addr_34f7_32:
                r13_34 = word_limit;
                ecx35 = in_column;
                rbx36 = *reinterpret_cast<void***>(r13_34);
                rax37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax29) - reinterpret_cast<unsigned char>(rbx36));
                *reinterpret_cast<void***>(r13_34 + 8) = *reinterpret_cast<void***>(&rax37);
                ecx38 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(ecx35) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax37)));
                v39 = ecx38;
                r14_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx36) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(&rax37)))) + 0xffffffffffffffff);
                in_column = ecx38;
                rax41 = fun_24a0("(['`\"", "(['`\"");
                *reinterpret_cast<uint32_t*>(&rsi42) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_40));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi42) + 4) = 0;
                rcx43 = *v9;
                eax44 = 4;
                *reinterpret_cast<unsigned char*>(&eax44) = reinterpret_cast<unsigned char>(4 & rcx43[rsi42 * 2]);
                *reinterpret_cast<unsigned char*>(&rcx43) = reinterpret_cast<uint1_t>(!!rax41);
                eax45 = eax44 | *reinterpret_cast<uint32_t*>(&rcx43) | static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_34 + 16))) & 0xfffffffa;
                *reinterpret_cast<void***>(r13_34 + 16) = *reinterpret_cast<void***>(&eax45);
                if (reinterpret_cast<unsigned char>(rbx36) < reinterpret_cast<unsigned char>(r14_40)) {
                    while (rax46 = fun_24a0(")]'\"", ")]'\""), !!rax46) {
                        rax47 = r14_40 + 0xffffffffffffffff;
                        if (rbx36 == rax47) 
                            goto addr_36e0_42;
                        r14_40 = rax47;
                    }
                }
            }
            addr_3594_46:
            rax48 = fun_24a0(".?!", ".?!");
            *reinterpret_cast<uint32_t*>(&rsi) = ebp5;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rdx30) = reinterpret_cast<uint1_t>(!!rax48);
            eax49 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_34 + 16))) & 0xfffffffd | reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx30) + *reinterpret_cast<int32_t*>(&rdx30));
            *reinterpret_cast<void***>(r13_34 + 16) = *reinterpret_cast<void***>(&eax49);
            eax50 = get_space(v6, *reinterpret_cast<uint32_t*>(&rsi));
            rdx51 = word_limit;
            ebp5 = eax50;
            eax52 = in_column;
            *reinterpret_cast<void**>(&rax53) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax52) - reinterpret_cast<uint32_t>(v39));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax53) + 4) = 0;
            *reinterpret_cast<void**>(rdx51 + 12) = *reinterpret_cast<void**>(&rax53);
            if (ebp5 == 0xffffffff) {
                *reinterpret_cast<void***>(rdx51 + 16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16)) | 8);
                goto addr_368c_48;
            }
            ecx54 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16));
            if (!(*reinterpret_cast<unsigned char*>(&ecx54) & 2)) {
                *reinterpret_cast<void**>(&rax53) = reinterpret_cast<void*>(0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax53) + 4) = 0;
                goto addr_3607_51;
            } else {
                *reinterpret_cast<unsigned char*>(&rsi) = reinterpret_cast<uint1_t>(ebp5 == 10);
                *reinterpret_cast<unsigned char*>(&rax53) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rax53)) > reinterpret_cast<int32_t>(1))) | *reinterpret_cast<unsigned char*>(&rsi));
                if (!*reinterpret_cast<unsigned char*>(&rax53)) {
                    ecx55 = ecx54 & 0xfffffff7;
                    *reinterpret_cast<void***>(rdx51 + 16) = *reinterpret_cast<void***>(&ecx55);
                } else {
                    addr_3607_51:
                    eax56 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16))) & 0xfffffff7 | static_cast<uint32_t>(rax53 * 8);
                    *reinterpret_cast<void***>(rdx51 + 16) = *reinterpret_cast<void***>(&eax56);
                    if (ebp5 == 10) 
                        goto addr_368c_48;
                }
                zf57 = uniform == 0;
                if (!zf57) {
                    addr_368c_48:
                    eax58 = reinterpret_cast<void*>(0);
                    *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16)) & 8));
                    *reinterpret_cast<void**>(rdx51 + 12) = reinterpret_cast<void*>(reinterpret_cast<int32_t>(eax58) + 1);
                    if (rdx51 == 0x15d10) {
                        addr_3720_55:
                        set_other_indent(1, 1);
                        flush_paragraph(1, rsi);
                        rdx51 = word_limit;
                        goto addr_36a7_56;
                    } else {
                        addr_36a7_56:
                        rdx8 = rdx51 + 40;
                        word_limit = rdx8;
                        if (ebp5 == 10) 
                            goto addr_36c0_3;
                        if (ebp5 == 0xffffffff) 
                            goto addr_36c0_3;
                    }
                } else {
                    if (rdx51 == 0x15d10) 
                        goto addr_3720_55;
                    rdx8 = rdx51 + 40;
                    word_limit = rdx8;
                }
                addr_34a0_2:
                rax29 = wptr;
                rbx32 = v6;
                *reinterpret_cast<void***>(rdx8) = rax29;
                goto addr_34af_39;
            }
            addr_36e0_42:
            goto addr_3594_46;
        }
    }
}