fmt (FILE *f, char const *file)
{
  fadvise (f, FADVISE_SEQUENTIAL);
  tabs = false;
  other_indent = 0;
  next_char = get_prefix (f);
  while (get_paragraph (f))
    {
      fmt_paragraph ();
      put_paragraph (word_limit);
    }

  int err = ferror (f) ? 0 : -1;
  if (f == stdin)
    clearerr (f);
  else if (fclose (f) != 0 && err < 0)
    err = errno;
  if (0 <= err)
    error (0, err, err ? "%s" : _("read error"), quotef (file));
  return err < 0;
}