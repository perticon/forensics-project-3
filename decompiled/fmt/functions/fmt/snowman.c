uint32_t fmt(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r14_6;
    void** rsi7;
    uint32_t eax8;
    uint32_t ebp9;
    int64_t rdi10;
    void* eax11;
    void* r13d12;
    void* eax13;
    void* r8d14;
    void** r15_15;
    void** rdi16;
    void** rax17;
    void* eax18;
    int1_t less_or_equal19;
    void* edx20;
    void** rsi21;
    uint32_t eax22;
    void* r8d23;
    void** rdi24;
    void** rsi25;
    void* eax26;
    void* eax27;
    int1_t less_or_equal28;
    uint32_t edi29;
    int1_t zf30;
    int1_t zf31;
    int1_t zf32;
    void* eax33;
    void* tmp32_34;
    int1_t zf35;
    uint32_t eax36;
    int1_t zf37;
    void* eax38;
    void* tmp32_39;
    int1_t zf40;
    uint32_t eax41;
    void* eax42;
    int1_t zf43;
    void* tmp32_44;
    int1_t zf45;
    void* tmp32_46;
    int1_t less_or_equal47;
    uint32_t eax48;
    void* eax49;
    int1_t zf50;
    void* tmp32_51;
    int1_t zf52;
    void** r15_53;
    int64_t rdi54;
    void** rdi55;
    void** rax56;
    void** rax57;
    int32_t eax58;
    uint32_t eax59;
    void** rdi60;
    void** rax61;
    uint32_t eax62;
    void** rbp63;
    void* eax64;
    void* tmp32_65;
    int1_t less_or_equal66;
    void** rdi67;
    void** rax68;
    void** rax69;
    int64_t rax70;
    int32_t* rax71;
    int32_t ebp72;
    uint32_t eax73;

    r14_6 = rdi;
    *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(2);
    *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
    fadvise();
    tabs = 0;
    other_indent = reinterpret_cast<void*>(0);
    eax8 = get_prefix(r14_6, 2);
    next_char = eax8;
    ebp9 = eax8;
    while (1) {
        last_line_length = reinterpret_cast<void*>(0);
        while (1) {
            *reinterpret_cast<void**>(&rdi10) = next_prefix_indent;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            eax11 = in_column;
            if (ebp9 == 10 || ebp9 == 0xffffffff) {
                out_column = reinterpret_cast<void*>(0);
                *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(ebp9 != 10);
                *reinterpret_cast<unsigned char*>(&rcx) = reinterpret_cast<uint1_t>(ebp9 != 0xffffffff);
                *reinterpret_cast<void**>(&rdx) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rdx)) & reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)));
                r13d12 = *reinterpret_cast<void**>(&rdx);
                if (reinterpret_cast<int32_t>(eax11) > reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdi10))) {
                    addr_37ee_5:
                    put_space(rdi10, rsi7);
                    eax13 = out_column;
                    r8d14 = in_column;
                    r15_15 = prefix;
                    if (r8d14 != eax13) {
                        do {
                            addr_382f_6:
                            *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_15))));
                            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                            if (!rsi7) 
                                break;
                            rdi16 = stdout;
                            ++r15_15;
                            rax17 = *reinterpret_cast<void***>(rdi16 + 40);
                            if (reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi16 + 48))) {
                                rcx = rax17 + 1;
                                *reinterpret_cast<void***>(rdi16 + 40) = rcx;
                                *reinterpret_cast<void***>(rax17) = rsi7;
                            } else {
                                fun_24b0();
                                r8d14 = in_column;
                            }
                            eax18 = out_column;
                            eax13 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax18) + 1);
                            out_column = eax13;
                        } while (eax13 != r8d14);
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rdx)) {
                        addr_3d7c_13:
                        put_space(rdi10, rsi7);
                        eax13 = out_column;
                        r8d14 = in_column;
                        r15_15 = prefix;
                        if (eax13 != r8d14) {
                            addr_398a_14:
                            r13d12 = reinterpret_cast<void*>(1);
                            goto addr_382f_6;
                        } else {
                            put_space(0, rsi7);
                            if (ebp9 != 0xffffffff) 
                                goto addr_39d1_16; else 
                                goto addr_3dae_17;
                        }
                    } else {
                        addr_392b_18:
                        if (ebp9 == 0xffffffff) 
                            goto addr_388a_19; else 
                            goto addr_3934_20;
                    }
                }
            } else {
                less_or_equal19 = reinterpret_cast<int32_t>(prefix_lead_space) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdi10));
                if (less_or_equal19 && (edx20 = prefix_full_length, *reinterpret_cast<void**>(&rdx) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(edx20) + reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rdi10))), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx)) <= reinterpret_cast<int32_t>(eax11))) {
                    prefix_indent = *reinterpret_cast<void**>(&rdi10);
                    *reinterpret_cast<uint32_t*>(&rsi21) = ebp9;
                    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
                    first_indent = eax11;
                    wptr = reinterpret_cast<void**>(0x15d80);
                    word_limit = reinterpret_cast<void**>(0xc120);
                    eax22 = get_line(r14_6, rsi21, rdx, rcx);
                    *reinterpret_cast<void**>(&rcx) = next_prefix_indent;
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    r8d23 = prefix_indent;
                    *reinterpret_cast<uint32_t*>(&rdi24) = 0;
                    *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi25) = eax22;
                    *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                    if (*reinterpret_cast<void**>(&rcx) == r8d23 && (eax26 = prefix_full_length, eax27 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax26) + reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx))), less_or_equal28 = reinterpret_cast<int32_t>(eax27) <= reinterpret_cast<int32_t>(in_column), less_or_equal28)) {
                        *reinterpret_cast<unsigned char*>(&eax27) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rsi25) != 10);
                        edi29 = 0;
                        *reinterpret_cast<unsigned char*>(&edi29) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff);
                        *reinterpret_cast<uint32_t*>(&rdi24) = edi29 & reinterpret_cast<uint32_t>(eax27);
                        *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
                    }
                    set_other_indent(*reinterpret_cast<signed char*>(&rdi24));
                    zf30 = split == 0;
                    if (zf30) {
                        zf31 = crown == 0;
                        if (zf31) {
                            zf32 = tagged == 0;
                            if (zf32) {
                                if (*reinterpret_cast<void**>(&rcx) == r8d23) {
                                    while ((eax33 = in_column, tmp32_34 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)) + reinterpret_cast<uint32_t>(prefix_full_length)), *reinterpret_cast<void**>(&rcx) = tmp32_34, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rcx)) <= reinterpret_cast<int32_t>(eax33)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff && ((zf35 = other_indent == eax33, zf35) && (rdi24 = r14_6, eax36 = get_line(rdi24, rsi25, rdx, rcx), *reinterpret_cast<void**>(&rcx) = next_prefix_indent, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, zf37 = *reinterpret_cast<void**>(&rcx) == prefix_indent, *reinterpret_cast<uint32_t*>(&rsi25) = eax36, *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, zf37))))) {
                                    }
                                }
                            } else {
                                if (*reinterpret_cast<void**>(&rcx) == r8d23 && ((eax38 = in_column, tmp32_39 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)) + reinterpret_cast<uint32_t>(prefix_full_length)), *reinterpret_cast<void**>(&rcx) = tmp32_39, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rcx)) <= reinterpret_cast<int32_t>(eax38)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && *reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff))) {
                                    zf40 = first_indent == eax38;
                                    if (!zf40) {
                                        do {
                                            rdi24 = r14_6;
                                            eax41 = get_line(rdi24, rsi25, rdx, rcx);
                                            *reinterpret_cast<uint32_t*>(&rsi25) = eax41;
                                            *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                            eax42 = next_prefix_indent;
                                            zf43 = eax42 == prefix_indent;
                                            if (!zf43) 
                                                break;
                                            *reinterpret_cast<void**>(&rdx) = in_column;
                                            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                            tmp32_44 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax42) + reinterpret_cast<uint32_t>(prefix_full_length));
                                        } while (reinterpret_cast<int32_t>(tmp32_44) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff && (zf45 = other_indent == *reinterpret_cast<void**>(&rdx), zf45))));
                                    }
                                }
                            }
                        } else {
                            if (*reinterpret_cast<void**>(&rcx) == r8d23 && ((tmp32_46 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)) + reinterpret_cast<uint32_t>(prefix_full_length)), *reinterpret_cast<void**>(&rcx) = tmp32_46, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, less_or_equal47 = reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rcx)) <= reinterpret_cast<int32_t>(in_column), less_or_equal47) && *reinterpret_cast<uint32_t*>(&rsi25) != 10)) {
                                if (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff) {
                                    do {
                                        rdi24 = r14_6;
                                        eax48 = get_line(rdi24, rsi25, rdx, rcx);
                                        *reinterpret_cast<uint32_t*>(&rsi25) = eax48;
                                        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                        eax49 = next_prefix_indent;
                                        zf50 = eax49 == prefix_indent;
                                        if (!zf50) 
                                            break;
                                        *reinterpret_cast<void**>(&rdx) = in_column;
                                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                        tmp32_51 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax49) + reinterpret_cast<uint32_t>(prefix_full_length));
                                    } while (reinterpret_cast<int32_t>(tmp32_51) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff && (zf52 = other_indent == *reinterpret_cast<void**>(&rdx), zf52))));
                                }
                            }
                        }
                    }
                    r15_53 = word_limit;
                    if (reinterpret_cast<unsigned char>(r15_53) > reinterpret_cast<unsigned char>(0xc120)) 
                        break;
                    rcx = reinterpret_cast<void**>("get_paragraph");
                    *reinterpret_cast<void**>(&rdx) = reinterpret_cast<void*>(0x270);
                    rsi7 = reinterpret_cast<void**>("src/fmt.c");
                    rdi10 = reinterpret_cast<int64_t>("word < word_limit");
                    fun_24e0("word < word_limit", "src/fmt.c");
                    goto addr_3d7c_13;
                }
                out_column = reinterpret_cast<void*>(0);
                if (reinterpret_cast<int32_t>(eax11) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdi10))) 
                    goto addr_3968_45; else 
                    goto addr_37e8_46;
            }
            if (*reinterpret_cast<signed char*>(&r13d12)) {
                *reinterpret_cast<void**>(&rdi54) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(r8d14) - reinterpret_cast<uint32_t>(eax13));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
                put_space(rdi54, rsi7);
                if (ebp9 == 0xffffffff) 
                    goto addr_39a7_49;
            } else {
                if (ebp9 != 0xffffffff) 
                    goto addr_3934_20; else 
                    goto addr_3872_51;
            }
            do {
                addr_39d1_16:
                rdi55 = stdout;
                rax56 = *reinterpret_cast<void***>(rdi55 + 40);
                if (reinterpret_cast<unsigned char>(rax56) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi55 + 48))) {
                    *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebp9))));
                    *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                    fun_24b0();
                } else {
                    rdx = rax56 + 1;
                    *reinterpret_cast<void***>(rdi55 + 40) = rdx;
                    *reinterpret_cast<void***>(rax56) = *reinterpret_cast<void***>(&ebp9);
                }
                rax57 = *reinterpret_cast<void***>(r14_6 + 8);
                if (reinterpret_cast<unsigned char>(rax57) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_6 + 16))) {
                    rdx = rax57 + 1;
                    *reinterpret_cast<void***>(r14_6 + 8) = rdx;
                    ebp9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax57));
                    eax58 = 1;
                } else {
                    eax59 = fun_2380(r14_6, r14_6);
                    ebp9 = eax59;
                    *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<uint1_t>(eax59 != 0xffffffff);
                }
            } while (ebp9 != 10 && *reinterpret_cast<unsigned char*>(&eax58));
            goto addr_392b_18;
            addr_3934_20:
            rdi60 = stdout;
            rax61 = *reinterpret_cast<void***>(rdi60 + 40);
            if (reinterpret_cast<unsigned char>(rax61) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi60 + 48))) {
                *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(10);
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                fun_24b0();
            } else {
                rdx = rax61 + 1;
                *reinterpret_cast<void***>(rdi60 + 40) = rdx;
                *reinterpret_cast<void***>(rax61) = reinterpret_cast<void**>(10);
            }
            eax62 = get_prefix(r14_6, rsi7);
            ebp9 = eax62;
            continue;
            addr_3968_45:
            put_space(rdi10, rsi7);
            eax13 = out_column;
            r8d14 = in_column;
            r15_15 = prefix;
            if (eax13 != r8d14) 
                goto addr_398a_14;
            put_space(0, rsi7);
            goto addr_39d1_16;
            addr_37e8_46:
            r13d12 = reinterpret_cast<void*>(1);
            goto addr_37ee_5;
        }
        *reinterpret_cast<unsigned char*>(r15_53 + 0xffffffffffffffe8) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r15_53 + 0xffffffffffffffe8) | 10);
        next_char = *reinterpret_cast<uint32_t*>(&rsi25);
        fmt_paragraph(rdi24);
        *reinterpret_cast<void**>(&rsi7) = first_indent;
        *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
        put_line(0xc120, rsi7, rdx, rcx);
        rbp63 = gc140;
        if (rbp63 != r15_53) {
            do {
                *reinterpret_cast<void**>(&rsi7) = other_indent;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                put_line(rbp63, rsi7, rdx, rcx);
                rbp63 = *reinterpret_cast<void***>(rbp63 + 32);
            } while (rbp63 != r15_53);
        }
        ebp9 = next_char;
    }
    addr_39a7_49:
    addr_3872_51:
    eax64 = prefix_length;
    tmp32_65 = reinterpret_cast<void*>(reinterpret_cast<int32_t>(eax64) + reinterpret_cast<uint32_t>(next_prefix_indent));
    less_or_equal66 = reinterpret_cast<int32_t>(tmp32_65) <= reinterpret_cast<int32_t>(in_column);
    if (less_or_equal66) {
        rdi67 = stdout;
        rax68 = *reinterpret_cast<void***>(rdi67 + 40);
        if (reinterpret_cast<unsigned char>(rax68) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi67 + 48))) {
            *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(10);
            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
            fun_24b0();
        } else {
            *reinterpret_cast<void***>(rdi67 + 40) = rax68 + 1;
            *reinterpret_cast<void***>(rax68) = reinterpret_cast<void**>(10);
        }
    }
    addr_388a_19:
    next_char = 0xffffffff;
    rax69 = stdin;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_6)) & 32) {
        if (r14_6 == rax69) {
            fun_2400(r14_6, rsi7);
        } else {
            rpl_fclose(r14_6, rsi7);
        }
        quotearg_n_style_colon();
        goto addr_38c8_73;
    }
    if (r14_6 == rax69) {
        fun_2400(r14_6, rsi7);
        return 1;
    }
    rax70 = rpl_fclose(r14_6, rsi7);
    if (!*reinterpret_cast<int32_t*>(&rax70)) {
        return 1;
    }
    rax71 = fun_23b0();
    ebp72 = *rax71;
    eax73 = 1;
    if (ebp72 >= 0) 
        goto addr_3d37_79;
    addr_38f0_80:
    return eax73;
    addr_3d37_79:
    quotearg_n_style_colon();
    if (!ebp72) {
        addr_38c8_73:
        fun_2440();
    }
    fun_2610();
    eax73 = 0;
    goto addr_38f0_80;
    addr_3dae_17:
    goto addr_3872_51;
}