undefined8 fmt(_IO_FILE *param_1,undefined8 param_2)

{
  byte bVar1;
  byte *pbVar2;
  undefined1 *puVar3;
  undefined1 *puVar4;
  uint uVar5;
  int iVar6;
  uint uVar7;
  undefined8 uVar8;
  char *pcVar9;
  int *piVar10;
  uint uVar11;
  ulong uVar12;
  ulong uVar13;
  byte *pbVar14;
  bool bVar15;
  
  fadvise();
  tabs = 0;
  other_indent = 0;
  next_char = get_prefix();
LAB_00103796:
  last_line_length = 0;
  uVar5 = next_char;
  do {
    if ((uVar5 == 10) || (uVar5 == 0xffffffff)) {
      out_column = 0;
      bVar15 = uVar5 != 10 && uVar5 != 0xffffffff;
      if ((int)next_prefix_indent < in_column) goto LAB_001037ee;
      if (bVar15) {
        put_space();
        if (out_column != in_column) goto LAB_0010398a;
        put_space(0);
        goto joined_r0x001039a5;
      }
LAB_0010392b:
      if (uVar5 == 0xffffffff) {
LAB_0010388a:
        next_char = 0xffffffff;
        if ((*(byte *)&param_1->_flags & 0x20) == 0) {
          if (param_1 == stdin) {
            clearerr_unlocked(param_1);
            return 1;
          }
          iVar6 = rpl_fclose();
          if (iVar6 == 0) {
            return 1;
          }
          piVar10 = __errno_location();
          iVar6 = *piVar10;
          if (iVar6 < 0) {
            return 1;
          }
          uVar8 = quotearg_n_style_colon(0,3,param_2);
          if (iVar6 != 0) {
            pcVar9 = "%s";
            goto LAB_001038e0;
          }
        }
        else {
          if (param_1 == stdin) {
            clearerr_unlocked(param_1);
          }
          else {
            rpl_fclose();
          }
          uVar8 = quotearg_n_style_colon(0,3,param_2);
        }
        iVar6 = 0;
        pcVar9 = (char *)dcgettext(0,"read error",5);
LAB_001038e0:
        error(0,iVar6,pcVar9,uVar8);
        return 0;
      }
    }
    else {
      if ((prefix_lead_space <= (int)next_prefix_indent) &&
         ((int)(prefix_full_length + next_prefix_indent) <= in_column)) break;
      out_column = 0;
      if (in_column <= (int)next_prefix_indent) {
        put_space();
        if (out_column != in_column) {
LAB_0010398a:
          bVar15 = true;
          pbVar14 = prefix;
          iVar6 = in_column;
          goto LAB_0010382f;
        }
        put_space(0);
LAB_001039d1:
        do {
          pcVar9 = stdout->_IO_write_ptr;
          if (pcVar9 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar9 + 1;
            *pcVar9 = (char)uVar5;
          }
          else {
            __overflow(stdout,uVar5 & 0xff);
          }
          pbVar14 = (byte *)param_1->_IO_read_ptr;
          if (pbVar14 < param_1->_IO_read_end) {
            param_1->_IO_read_ptr = (char *)(pbVar14 + 1);
            uVar5 = (uint)*pbVar14;
            bVar15 = true;
          }
          else {
            uVar5 = __uflow(param_1);
            bVar15 = uVar5 != 0xffffffff;
          }
        } while ((uVar5 != 10) && (bVar15));
        goto LAB_0010392b;
      }
      bVar15 = true;
LAB_001037ee:
      out_column = 0;
      put_space();
      pbVar14 = prefix;
      iVar6 = in_column;
      if (in_column != out_column) {
LAB_0010382f:
        do {
          bVar1 = *pbVar14;
          if (bVar1 == 0) break;
          pbVar2 = (byte *)stdout->_IO_write_ptr;
          if (pbVar2 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
            *pbVar2 = bVar1;
          }
          else {
            __overflow(stdout,(uint)bVar1);
            iVar6 = in_column;
          }
          out_column = out_column + 1;
          pbVar14 = pbVar14 + 1;
        } while (out_column != iVar6);
      }
      if (bVar15) {
        put_space(iVar6 - out_column);
joined_r0x001039a5:
        if (uVar5 != 0xffffffff) goto LAB_001039d1;
LAB_00103872:
        if ((int)(prefix_length + next_prefix_indent) <= in_column) {
          pcVar9 = stdout->_IO_write_ptr;
          if (pcVar9 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar9 + 1;
            *pcVar9 = '\n';
          }
          else {
            __overflow(stdout,10);
          }
        }
        goto LAB_0010388a;
      }
      if (uVar5 == 0xffffffff) goto LAB_00103872;
    }
    pcVar9 = stdout->_IO_write_ptr;
    if (pcVar9 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar9 + 1;
      *pcVar9 = '\n';
    }
    else {
      __overflow(stdout,10);
    }
    uVar5 = get_prefix();
  } while( true );
  prefix_indent = next_prefix_indent;
  first_indent = in_column;
  wptr = parabuf;
  word_limit = unused_word_type;
  uVar5 = get_line(param_1);
  uVar12 = (ulong)next_prefix_indent;
  bVar15 = false;
  uVar13 = (ulong)uVar5;
  if ((next_prefix_indent == prefix_indent) &&
     ((int)(prefix_full_length + next_prefix_indent) <= in_column)) {
    bVar15 = uVar5 != 10 && uVar5 != 0xffffffff;
  }
  uVar5 = prefix_indent;
  set_other_indent(bVar15);
  uVar7 = (uint)uVar13;
  if (split == '\0') {
    uVar11 = (uint)uVar12;
    if (crown == '\0') {
      if (tagged == '\0') {
        if (uVar11 == uVar5) {
          do {
            uVar7 = (uint)uVar13;
            if ((((in_column < (int)uVar12 + prefix_full_length) || (uVar7 == 10)) ||
                (uVar7 == 0xffffffff)) || (other_indent != in_column)) break;
            uVar7 = get_line(param_1);
            uVar12 = (ulong)next_prefix_indent;
            uVar13 = (ulong)uVar7;
          } while (next_prefix_indent == prefix_indent);
        }
      }
      else if (((uVar11 == uVar5) && ((int)(uVar11 + prefix_full_length) <= in_column)) &&
              ((uVar7 != 10 && ((uVar7 != 0xffffffff && (first_indent != in_column)))))) {
        while (uVar7 = get_line(param_1), next_prefix_indent == prefix_indent) {
          if ((((in_column < (int)(next_prefix_indent + prefix_full_length)) || (uVar7 == 10)) ||
              (uVar7 == 0xffffffff)) || (other_indent != in_column)) break;
        }
      }
    }
    else if (((uVar11 == uVar5) && ((int)(uVar11 + prefix_full_length) <= in_column)) &&
            ((uVar7 != 10 && (uVar7 != 0xffffffff)))) {
      while (((uVar7 = get_line(param_1), next_prefix_indent == prefix_indent &&
              ((int)(next_prefix_indent + prefix_full_length) <= in_column)) && (uVar7 != 10))) {
        if ((uVar7 == 0xffffffff) || (other_indent != in_column)) break;
      }
    }
  }
  puVar4 = word_limit;
  if (word_limit < (undefined1 *)0x10c121) {
                    /* WARNING: Subroutine does not return */
    __assert_fail("word < word_limit","src/fmt.c",0x270,"get_paragraph");
  }
  word_limit[-0x18] = word_limit[-0x18] | 10;
  next_char = uVar7;
  fmt_paragraph();
  put_line();
  for (puVar3 = unused_word_type._32_8_; puVar3 != puVar4; puVar3 = *(undefined1 **)(puVar3 + 0x20))
  {
    put_line();
  }
  goto LAB_00103796;
}