bool fmt(struct _IO_FILE * f, char * file) {
    int64_t v1 = (int64_t)f;
    fadvise(f, 2);
    *(char *)&tabs = 0;
    other_indent = 0;
    int32_t v2 = get_prefix(f); // 0x3789
    next_char = v2;
    int64_t * v3 = (int64_t *)(v1 + 8);
    int32_t v4 = v2; // 0x3794
    int64_t v5; // 0x3750
    int32_t v6; // 0x3750
    uint32_t v7; // 0x3750
    int64_t v8; // 0x3750
    while (true) {
        // 0x3796
        last_line_length = 0;
        v6 = v4;
        int64_t v9; // 0x3750
        while (true) {
          lab_0x37a0:
            // 0x37a0
            v7 = v6;
            v5 = v7;
            int64_t v10 = next_prefix_indent; // 0x37a0
            v8 = v10;
            switch (v7) {
                case 10: {
                    goto lab_0x3900;
                }
                case -1: {
                    goto lab_0x3900;
                }
                default: {
                    // 0x37be
                    if (v10 < (int64_t)prefix_lead_space) {
                        goto lab_0x37d6;
                    } else {
                        // 0x37c6
                        if (in_column < prefix_full_length + next_prefix_indent) {
                            goto lab_0x37d6;
                        } else {
                            // 0x3a57
                            prefix_indent = next_prefix_indent;
                            first_indent = in_column;
                            wptr = (int64_t)&parabuf;
                            *(int64_t *)&word_limit = (int64_t)&unused_word_type;
                            uint32_t v11 = get_line(f, v7); // 0x3a7d
                            int32_t v12 = prefix_indent; // 0x3a88
                            v8 = 0;
                            int64_t v13 = 0; // 0x3a96
                            if (next_prefix_indent == v12) {
                                // 0x3b3d
                                v13 = 0;
                                if (prefix_full_length + next_prefix_indent <= in_column) {
                                    int64_t v14 = !((v11 == 10 | v11 == -1)); // 0x3b60
                                    v8 = v14;
                                    v13 = v14;
                                }
                            }
                            int64_t v15 = v11; // 0x3a91
                            set_other_indent(v13);
                            v9 = v15;
                            if (*(char *)&split != 0) {
                                goto lab_0x3ac0;
                            } else {
                                // 0x3aaa
                                if (*(char *)&crown == 0) {
                                    char v16 = *(char *)&tagged; // 0x3b67
                                    int32_t v17; // 0x3a82
                                    if (v16 == 0) {
                                        // 0x3c00
                                        v9 = v15;
                                        int32_t v18 = v17; // 0x3c03
                                        int64_t v19 = v15; // 0x3c03
                                        if (v17 == v12) {
                                            while (true) {
                                              lab_0x3c09:;
                                                int64_t v20 = v19;
                                                int32_t v21 = v18;
                                                uint32_t v22 = in_column; // 0x3c09
                                                int32_t v23 = prefix_full_length; // 0x3c0f
                                                v9 = v20;
                                                if (v23 + v21 > v22) {
                                                    // break -> 0x3ac0
                                                    break;
                                                }
                                                int32_t v24 = v20; // 0x3c1d
                                                v9 = v20;
                                                v9 = v20;
                                                switch (v24) {
                                                    case 10: {
                                                        goto lab_0x3ac0;
                                                    }
                                                    case -1: {
                                                        goto lab_0x3ac0;
                                                    }
                                                    default: {
                                                        int32_t v25 = other_indent; // 0x3c2f
                                                        v9 = v20;
                                                        if (v25 != v22) {
                                                            // break -> 0x3ac0
                                                            break;
                                                        }
                                                        uint32_t v26 = get_line(f, v24); // 0x3c3e
                                                        int32_t v27 = next_prefix_indent; // 0x3c43
                                                        int32_t v28 = prefix_indent; // 0x3c49
                                                        int64_t v29 = v26; // 0x3c4f
                                                        v9 = v29;
                                                        v18 = v27;
                                                        v19 = v29;
                                                        if (v27 != v28) {
                                                            // break -> 0x3ac0
                                                            break;
                                                        }
                                                        goto lab_0x3c09;
                                                    }
                                                }
                                            }
                                        }
                                        goto lab_0x3ac0;
                                    } else {
                                        // 0x3b74
                                        v9 = v15;
                                        if (v17 != v12) {
                                            goto lab_0x3ac0;
                                        } else {
                                            uint32_t v30 = in_column; // 0x3b7d
                                            int32_t v31 = prefix_full_length; // 0x3b83
                                            v9 = v15;
                                            if (v31 + v17 > v30) {
                                                goto lab_0x3ac0;
                                            } else {
                                                // switch.early.test13
                                                v9 = v15;
                                                v9 = v15;
                                                switch (v11) {
                                                    case -1: {
                                                        goto lab_0x3ac0;
                                                    }
                                                    case 10: {
                                                        goto lab_0x3ac0;
                                                    }
                                                    default: {
                                                        int32_t v32 = first_indent; // 0x3ba3
                                                        v9 = v15;
                                                        if (v32 != v30) {
                                                            int32_t v33 = get_line(f, v11); // 0x3be5
                                                            int64_t v34 = v33; // 0x3bea
                                                            int32_t v35 = next_prefix_indent; // 0x3bec
                                                            int32_t v36 = prefix_indent; // 0x3bf2
                                                            v9 = v34;
                                                            if (v35 == v36) {
                                                                int32_t v37 = in_column; // 0x3bb0
                                                                int32_t v38 = prefix_full_length; // 0x3bb6
                                                                v9 = v34;
                                                                int32_t v39 = v37; // 0x3750
                                                                int32_t v40 = v33; // 0x3750
                                                                int64_t v41 = v34; // 0x3750
                                                                if (v38 + v35 <= v37) {
                                                                    while (true) {
                                                                      lab_switch_early_test:;
                                                                        int64_t v42 = v41;
                                                                        int32_t v43 = v40;
                                                                        v9 = v42;
                                                                        v9 = v42;
                                                                        switch (v43) {
                                                                            case -1: {
                                                                                goto lab_0x3ac0;
                                                                            }
                                                                            case 10: {
                                                                                goto lab_0x3ac0;
                                                                            }
                                                                            default: {
                                                                                int32_t v44 = v39;
                                                                                int32_t v45 = other_indent; // 0x3bd6
                                                                                v9 = v42;
                                                                                if (v45 != v44) {
                                                                                    // break -> 0x3ac0
                                                                                    break;
                                                                                }
                                                                                int32_t v46 = get_line(f, v43); // 0x3be5
                                                                                int64_t v47 = v46; // 0x3bea
                                                                                int32_t v48 = next_prefix_indent; // 0x3bec
                                                                                int32_t v49 = prefix_indent; // 0x3bf2
                                                                                v9 = v47;
                                                                                if (v48 != v49) {
                                                                                    // break -> 0x3ac0
                                                                                    break;
                                                                                }
                                                                                int32_t v50 = in_column; // 0x3bb0
                                                                                int32_t v51 = prefix_full_length; // 0x3bb6
                                                                                v9 = v47;
                                                                                v39 = v50;
                                                                                v40 = v46;
                                                                                v41 = v47;
                                                                                if (v51 + v48 > v50) {
                                                                                    // break -> 0x3ac0
                                                                                    break;
                                                                                }
                                                                                goto lab_switch_early_test;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        goto lab_0x3ac0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    // 0x3ab7
                                    v9 = v15;
                                    if (next_prefix_indent == v12) {
                                        // 0x3c86
                                        v9 = v15;
                                        if (v11 != -1 == v11 != 10 == prefix_full_length + next_prefix_indent <= in_column) {
                                            int32_t v52 = get_line(f, v11); // 0x3ce5
                                            int64_t v53 = v52; // 0x3cea
                                            v9 = v53;
                                            if (next_prefix_indent == prefix_indent) {
                                                // 0x3cfe
                                                v9 = v53;
                                                int32_t v54 = in_column; // 0x3750
                                                int32_t v55 = v52; // 0x3750
                                                int64_t v56 = v53; // 0x3750
                                                if (prefix_full_length + next_prefix_indent <= in_column) {
                                                    while (true) {
                                                      lab_switch_early_test12:;
                                                        int32_t v57 = v55;
                                                        v9 = v56;
                                                        switch (v57) {
                                                            case -1: {
                                                                goto lab_0x3ac0;
                                                            }
                                                            case 10: {
                                                                goto lab_0x3ac0;
                                                            }
                                                            default: {
                                                                // 0x3cd6
                                                                if (other_indent != v54) {
                                                                    // break -> 0x3ac0
                                                                    break;
                                                                }
                                                                int32_t v58 = get_line(f, v57); // 0x3ce5
                                                                int64_t v59 = v58; // 0x3cea
                                                                if (next_prefix_indent != prefix_indent) {
                                                                    // break -> 0x3ac0
                                                                    break;
                                                                }
                                                                // 0x3cfe
                                                                v54 = in_column;
                                                                v55 = v58;
                                                                v56 = v59;
                                                                if (prefix_full_length + next_prefix_indent > in_column) {
                                                                    // break -> 0x3ac0
                                                                    break;
                                                                }
                                                                goto lab_switch_early_test12;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    goto lab_0x3ac0;
                                }
                            }
                        }
                    }
                }
            }
        }
      lab_0x3ad0:
        // 0x3ad0
        next_char = v9;
        fmt_paragraph();
        put_line((int32_t *)&unused_word_type, first_indent);
        if (word_limit != g44) {
            int64_t v60 = (int64_t)g44; // 0x3b0e
            put_line((int32_t *)v60, other_indent);
            v60 += 32;
            while (v60 != (int64_t)word_limit) {
                // 0x3b00
                put_line((int32_t *)v60, other_indent);
                v60 += 32;
            }
        }
        // 0x3b17
        v4 = next_char;
    }
  lab_0x388a:
    // 0x388a
    next_char = -1;
    char v61 = *(char *)&v8; // 0x389b
    v8 = v1;
    if ((v61 & 32) == 0) {
        if (g32 == f) {
            // 0x3d0a
            function_2400();
            return true;
        }
        // 0x3c65
        if (rpl_fclose(f) == 0) {
            // 0x38f0
            return true;
        }
        int32_t v62 = *(int32_t *)function_23b0(); // 0x3d28
        if (v62 < 0) {
            // 0x38f0
            return true;
        }
        // 0x3d37
        v8 = 0;
        quotearg_n_style_colon();
        if (v62 != 0) {
            // 0x38e0
            function_2610();
            // 0x38f0
            return false;
        }
    } else {
        if (g32 == f) {
            // 0x3d00
            function_2400();
        } else {
            // 0x38b1
            rpl_fclose(f);
        }
        // 0x38b6
        v8 = 0;
        quotearg_n_style_colon();
    }
    // 0x38c8
    v8 = 0;
    function_2440();
    // 0x38e0
    function_2610();
    // 0x38f0
    return false;
  lab_0x3872_3:
    // 0x3872
    if (next_prefix_indent + prefix_length > in_column) {
        goto lab_0x388a;
    } else {
        int64_t v63 = (int64_t)g31; // 0x3a20
        v8 = v63;
        int64_t * v64 = (int64_t *)(v63 + 40); // 0x3a27
        uint64_t v65 = *v64; // 0x3a27
        if (v65 >= *(int64_t *)(v63 + 48)) {
            // 0x3b2e
            function_24b0();
            goto lab_0x388a;
        } else {
            // 0x3a35
            *v64 = v65 + 1;
            *(char *)v65 = 10;
            goto lab_0x388a;
        }
    }
  lab_0x3ac0:
    // 0x3ac0
    if (word_limit > (int32_t *)&unused_word_type) {
        // break -> 0x3ad0
        goto lab_0x3ad0;
    }
    // 0x3d5d
    v8 = (int64_t)"word < word_limit";
    function_24e0();
    int32_t v66 = (int32_t)"word < word_limit"; // 0x3d77
    goto lab_0x3d7c;
  lab_0x3900:
    // 0x3900
    out_column = 0;
    int64_t v92 = !((v7 == 10 | v7 == -1)); // 0x391d
    int64_t v67; // 0x3750
    if (next_prefix_indent < in_column) {
        goto lab_0x37ee;
    } else {
        // 0x3923
        v67 = v5;
        v66 = next_prefix_indent;
        if (v7 != 10 && v7 != -1) {
            goto lab_0x3d7c;
        } else {
            goto lab_0x392b;
        }
    }
  lab_0x37d6:
    // 0x37d6
    out_column = 0;
    v92 = 1;
    if (next_prefix_indent < in_column) {
        goto lab_0x37ee;
    } else {
        // 0x3968
        put_space(next_prefix_indent);
        if (out_column == in_column) {
            // 0x3b22
            put_space(0);
            goto lab_0x39d1;
        } else {
            goto lab_0x398a;
        }
    }
  lab_0x37ee:
    // 0x37ee
    put_space(next_prefix_indent);
    int64_t v93 = in_column; // 0x37f9
    int64_t v79 = v93; // 0x380a
    int64_t v80 = v92; // 0x380a
    int32_t v82 = out_column; // 0x380a
    int64_t v83 = v93; // 0x380a
    int64_t v84 = v92; // 0x380a
    if (in_column != out_column) {
        goto lab_0x382f;
    } else {
        goto lab_0x3860;
    }
  lab_0x382f:;
    int64_t v81 = v80;
    v82 = out_column;
    v83 = v79;
    v84 = v81;
    if (*(char *)prefix != 0) {
        int64_t v85 = prefix; // 0x383f
        int64_t v86 = (int64_t)g31; // 0x3838
        v8 = v86;
        char v87; // 0x3750
        int64_t * v88; // 0x3843
        uint64_t v89; // 0x3843
        if (*(int64_t *)(v86 + 40) < *(int64_t *)(v86 + 48)) {
            // 0x3810
            *v88 = v89 + 1;
            *(char *)v89 = v87;
        } else {
            // 0x384d
            function_24b0();
        }
        int64_t v90 = in_column;
        int32_t v91 = out_column + 1; // 0x3821
        out_column = v91;
        v82 = v91;
        v83 = v90;
        v84 = v81;
        while (v91 != (int32_t)v90) {
            // 0x382f
            v85++;
            if (*(char *)v85 == 0) {
                // break -> 0x3860
                break;
            }
            v86 = (int64_t)g31;
            v8 = v86;
            if (*(int64_t *)(v86 + 40) < *(int64_t *)(v86 + 48)) {
                // 0x3810
                *v88 = v89 + 1;
                *(char *)v89 = v87;
            } else {
                // 0x384d
                function_24b0();
            }
            // 0x381b
            v90 = in_column;
            v91 = out_column + 1;
            out_column = v91;
            v82 = v91;
            v83 = v90;
            v84 = v81;
        }
    }
    goto lab_0x3860;
  lab_0x3860:
    // 0x3860
    if ((char)v84 != 0) {
        // 0x3998
        put_space((int32_t)v83 - v82);
        if (v7 == -1) {
            goto lab_0x3872_3;
        }
        goto lab_0x39d1;
    } else {
        if (v7 == -1) {
            goto lab_0x3872_3;
        }
        goto lab_0x3934;
    }
  lab_0x3d7c:
    // 0x3d7c
    put_space(v66);
    if (out_column != in_column) {
        goto lab_0x398a;
    } else {
        // 0x3d9e
        put_space(0);
        if (v7 == -1) {
            goto lab_0x3872_3;
        }
        goto lab_0x39d1;
    }
  lab_0x392b:
    // 0x392b
    if ((int32_t)v67 == -1) {
        // break (via goto) -> 0x388a
        goto lab_0x388a;
    }
    goto lab_0x3934;
  lab_0x398a:
    // 0x398a
    v79 = in_column;
    v80 = 1;
    goto lab_0x382f;
  lab_0x3934:;
    int64_t v68 = (int64_t)g31; // 0x3934
    v8 = v68;
    int64_t * v69 = (int64_t *)(v68 + 40); // 0x393b
    uint64_t v70 = *v69; // 0x393b
    if (v70 >= *(int64_t *)(v68 + 48)) {
        // 0x3a48
        function_24b0();
    } else {
        // 0x3949
        *v69 = v70 + 1;
        *(char *)v70 = 10;
    }
    // 0x3954
    v6 = get_prefix(f);
    goto lab_0x37a0;
  lab_0x39d1:;
    int64_t v71 = v5; // 0x3750
    int64_t v72 = (int64_t)g31; // 0x39d1
    v8 = v72;
    int64_t * v73 = (int64_t *)(v72 + 40); // 0x39d8
    uint64_t v74 = *v73; // 0x39d8
    if (v74 >= *(int64_t *)(v72 + 48)) {
        // 0x3a10
        function_24b0();
    } else {
        // 0x39e2
        *v73 = v74 + 1;
        *(char *)v74 = (char)v71;
    }
    uint64_t v75 = *v3; // 0x39ed
    int64_t v76; // 0x3750
    char v77; // 0x3750
    int64_t v78; // 0x39fa
    if (v75 < *(int64_t *)(v1 + 16)) {
        // 0x39b0
        *v3 = v75 + 1;
        v76 = (int64_t)*(char *)v75;
        v77 = 1;
    } else {
        // 0x39f7
        v78 = function_2380();
        v76 = v78 & 0xffffffff;
        v77 = (int32_t)v78 != -1;
    }
    // 0x39c0
    v67 = v76;
    while (v76 != 10 && v77 != 0) {
        // 0x39d1
        v72 = (int64_t)g31;
        v8 = v72;
        v73 = (int64_t *)(v72 + 40);
        v74 = *v73;
        if (v74 >= *(int64_t *)(v72 + 48)) {
            // 0x3a10
            function_24b0();
        } else {
            // 0x39e2
            *v73 = v74 + 1;
            *(char *)v74 = (char)v76;
        }
        // 0x39ed
        v75 = *v3;
        if (v75 < *(int64_t *)(v1 + 16)) {
            // 0x39b0
            *v3 = v75 + 1;
            v76 = (int64_t)*(char *)v75;
            v77 = 1;
        } else {
            // 0x39f7
            v78 = function_2380();
            v76 = v78 & 0xffffffff;
            v77 = (int32_t)v78 != -1;
        }
        // 0x39c0
        v67 = v76;
    }
    goto lab_0x392b;
}