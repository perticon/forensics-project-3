void put_word(byte **param_1)

{
  byte bVar1;
  byte *pbVar2;
  byte *pbVar3;
  byte *pbVar4;
  uint uVar5;
  
  uVar5 = *(uint *)(param_1 + 1);
  pbVar2 = *param_1;
  if (uVar5 != 0) {
    pbVar3 = pbVar2;
    do {
      pbVar4 = pbVar3 + 1;
      bVar1 = *pbVar3;
      pbVar3 = (byte *)stdout->_IO_write_ptr;
      if (pbVar3 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = (char *)(pbVar3 + 1);
        *pbVar3 = bVar1;
      }
      else {
        __overflow(stdout,(uint)bVar1);
      }
      pbVar3 = pbVar4;
    } while (pbVar4 != pbVar2 + uVar5);
    uVar5 = *(uint *)(param_1 + 1);
  }
  out_column = out_column + uVar5;
  return;
}