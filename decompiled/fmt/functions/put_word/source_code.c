put_word (WORD *w)
{
  char const *s;
  int n;

  s = w->text;
  for (n = w->length; n != 0; n--)
    putchar (*s++);
  out_column += w->length;
}