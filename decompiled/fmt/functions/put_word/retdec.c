void put_word(int32_t * w) {
    int64_t v1 = (int64_t)w;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x3054
    uint32_t v3 = *v2; // 0x3054
    if (v3 == 0) {
        // 0x3096
        return;
    }
    int64_t v4 = (int64_t)g31; // 0x3068
    int64_t * v5 = (int64_t *)(v4 + 40); // 0x3077
    uint64_t v6 = *v5; // 0x3077
    if (v6 >= *(int64_t *)(v4 + 48)) {
        // 0x30a8
        function_24b0();
    } else {
        // 0x3081
        *v5 = v6 + 1;
        *(char *)v6 = *(char *)v1;
    }
    int64_t v7 = v1 + 1; // 0x306f
    while (v7 != (int64_t)v3 + v1) {
        int64_t v8 = v7;
        v4 = (int64_t)g31;
        v5 = (int64_t *)(v4 + 40);
        v6 = *v5;
        if (v6 >= *(int64_t *)(v4 + 48)) {
            // 0x30a8
            function_24b0();
        } else {
            // 0x3081
            *v5 = v6 + 1;
            *(char *)v6 = *(char *)v8;
        }
        // 0x308c
        v7 = v8 + 1;
    }
    // 0x3096
    out_column += *v2;
}