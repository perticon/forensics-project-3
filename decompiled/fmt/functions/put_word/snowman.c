void put_word(void** rdi, void** rsi) {
    void* rbp3;
    void** rbx4;
    void** r12_5;
    void** rbp6;
    void** rdi7;
    uint32_t esi8;
    void** rax9;
    void* tmp32_10;

    *reinterpret_cast<void***>(&rbp3) = *reinterpret_cast<void***>(rdi + 8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    rbx4 = *reinterpret_cast<void***>(rdi);
    if (*reinterpret_cast<void***>(&rbp3)) {
        r12_5 = rdi;
        rbp6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp3) + reinterpret_cast<unsigned char>(rbx4));
        do {
            rdi7 = stdout;
            ++rbx4;
            esi8 = *reinterpret_cast<unsigned char*>(rbx4 + 0xffffffffffffffff);
            rax9 = *reinterpret_cast<void***>(rdi7 + 40);
            if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi7 + 48))) {
                fun_24b0();
            } else {
                *reinterpret_cast<void***>(rdi7 + 40) = rax9 + 1;
                *reinterpret_cast<void***>(rax9) = *reinterpret_cast<void***>(&esi8);
            }
        } while (rbx4 != rbp6);
        *reinterpret_cast<void***>(&rbp3) = *reinterpret_cast<void***>(r12_5 + 8);
    }
    tmp32_10 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(out_column) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbp3)));
    out_column = tmp32_10;
    return;
}