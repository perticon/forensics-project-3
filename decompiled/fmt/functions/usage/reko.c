void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002680(fn0000000000002440(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000003E1E;
	}
	fn0000000000002600(fn0000000000002440(0x05, "Usage: %s [-WIDTH] [OPTION]... [FILE]...\n", null), 0x01);
	fn0000000000002520(stdout, fn0000000000002440(0x05, "Reformat each paragraph in the FILE(s), writing to standard output.\nThe option -WIDTH is an abbreviated form of --width=DIGITS.\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "  -c, --crown-margin        preserve indentation of first two lines\n  -p, --prefix=STRING       reformat only lines beginning with STRING,\n                              reattaching the prefix to reformatted lines\n  -s, --split-only          split long lines, but do not refill\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "  -t, --tagged-paragraph    indentation of first line different from second\n  -u, --uniform-spacing     one space between words, two after sentences\n  -w, --width=WIDTH         maximum line width (default of 75 columns)\n  -g, --goal=WIDTH          goal width (default of 93% of width)\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "      --version     output version information and exit\n", null));
	struct Eq_2633 * rbx_191 = fp - 0xB8 + 16;
	do
	{
		char * rsi_193 = rbx_191->qw0000;
		++rbx_191;
	} while (rsi_193 != null && fn0000000000002540(rsi_193, "fmt") != 0x00);
	ptr64 r13_206 = rbx_191->qw0008;
	if (r13_206 != 0x00)
	{
		fn0000000000002600(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_293 = fn00000000000025F0(null, 0x05);
		if (rax_293 == 0x00 || fn00000000000023C0(0x03, "en_", rax_293) == 0x00)
			goto l0000000000004066;
	}
	else
	{
		fn0000000000002600(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_235 = fn00000000000025F0(null, 0x05);
		if (rax_235 == 0x00 || fn00000000000023C0(0x03, "en_", rax_235) == 0x00)
		{
			fn0000000000002600(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
l00000000000040A3:
			fn0000000000002600(fn0000000000002440(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000003E1E:
			fn0000000000002660(edi);
		}
		r13_206 = 0x803A;
	}
	fn0000000000002520(stdout, fn0000000000002440(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000004066:
	fn0000000000002600(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l00000000000040A3;
}