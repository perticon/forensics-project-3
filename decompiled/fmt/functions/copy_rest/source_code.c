copy_rest (FILE *f, int c)
{
  char const *s;

  out_column = 0;
  if (in_column > next_prefix_indent || (c != '\n' && c != EOF))
    {
      put_space (next_prefix_indent);
      for (s = prefix; out_column != in_column && *s; out_column++)
        putchar (*s++);
      if (c != EOF && c != '\n')
        put_space (in_column - out_column);
      if (c == EOF && in_column >= next_prefix_indent + prefix_length)
        putchar ('\n');
    }
  while (c != '\n' && c != EOF)
    {
      putchar (c);
      c = getc (f);
    }
  return c;
}