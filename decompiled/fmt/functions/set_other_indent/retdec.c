int64_t set_other_indent(int64_t a1) {
    // 0x33e0
    if (*(char *)&split != 0) {
        // 0x33e9
        other_indent = first_indent;
        return first_indent;
    }
    // 0x3400
    if (*(char *)&crown != 0) {
        int32_t result = (char)a1 == 0 ? first_indent : in_column; // 0x3412
        other_indent = result;
        return result;
    }
    int64_t result2 = first_indent; // 0x3427
    if (*(char *)&tagged == 0) {
        // 0x3448
        other_indent = first_indent;
        // 0x344e
        return result2;
    }
    if ((char)a1 != 0) {
        // 0x3434
        if (in_column != first_indent) {
            // 0x343e
            other_indent = in_column;
            return result2;
        }
    }
    // 0x3450
    if (other_indent != first_indent) {
        // 0x344e
        return result2;
    }
    int64_t result3 = first_indent == 0 ? 3 : 0; // 0x345d
    other_indent = result3;
    return result3;
}