int32_t set_other_indent (void) {
    if (*(obj.split) != 0) {
        eax = first_indent;
        *(obj.other_indent) = eax;
        return eax;
    }
    if (*(obj.crown) != 0) {
        eax = in_column;
        if (dil == 0) {
            eax = *(obj.first_indent);
        }
        *(obj.other_indent) = eax;
        return eax;
    }
    eax = first_indent;
    if (*(obj.tagged) != 0) {
        if (dil == 0) {
            goto label_0;
        }
        edx = in_column;
        if (eax == edx) {
            goto label_0;
        }
        *(obj.other_indent) = edx;
        return eax;
    }
    *(obj.other_indent) = eax;
    do {
        return eax;
label_0:
    } while (eax != *(obj.other_indent));
    eax -= eax;
    eax &= 3;
    *(obj.other_indent) = eax;
    return eax;
}