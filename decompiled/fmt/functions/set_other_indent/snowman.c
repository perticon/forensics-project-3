void set_other_indent(signed char dil, ...) {
    int1_t zf2;
    int1_t zf3;
    int1_t zf4;
    void* eax5;
    void* edx6;
    int1_t zf7;
    void* eax8;
    void* eax9;

    zf2 = split == 0;
    if (zf2) {
        zf3 = crown == 0;
        if (zf3) {
            zf4 = tagged == 0;
            eax5 = first_indent;
            if (zf4) {
                other_indent = eax5;
            } else {
                if (!dil || (edx6 = in_column, eax5 == edx6)) {
                    zf7 = eax5 == other_indent;
                    if (zf7) {
                        other_indent = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax5) - (reinterpret_cast<uint32_t>(eax5) + reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(eax5) < reinterpret_cast<uint32_t>(eax5) + reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(eax5) < 1))) & 3);
                        return;
                    }
                } else {
                    other_indent = edx6;
                    return;
                }
            }
            return;
        } else {
            eax8 = in_column;
            if (!dil) {
                eax8 = first_indent;
            }
            other_indent = eax8;
            return;
        }
    } else {
        eax9 = first_indent;
        other_indent = eax9;
        return;
    }
}