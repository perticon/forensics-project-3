void flush_paragraph(void)

{
  long *plVar1;
  int iVar2;
  long lVar3;
  long lVar4;
  long *plVar5;
  long *plVar6;
  long lVar7;
  long *plVar8;
  
  plVar1 = word_limit;
  if (word_limit != (long *)unused_word_type) {
    fmt_paragraph();
    plVar8 = plVar1;
    if (plVar1 != unused_word_type._32_8_) {
      lVar3 = 0x7fffffffffffffff;
      plVar5 = unused_word_type._32_8_;
      lVar7 = unused_word_type._32_8_[3];
      do {
        plVar6 = (long *)plVar5[4];
        lVar7 = lVar7 - plVar6[3];
        if (lVar7 < lVar3) {
          lVar3 = lVar7;
          plVar8 = plVar5;
        }
        if (lVar3 < 0x7ffffffffffffff7) {
          lVar3 = lVar3 + 9;
        }
        plVar5 = plVar6;
        lVar7 = plVar6[3];
      } while (plVar1 != plVar6);
    }
    put_line(unused_word_type,first_indent);
    lVar3 = (long)wptr;
    for (plVar1 = unused_word_type._32_8_; wptr = (undefined1 *)lVar3, plVar8 != plVar1;
        plVar1 = (long *)plVar1[4]) {
      put_line(plVar1,other_indent);
      lVar3 = (long)wptr;
    }
    iVar2 = __memmove_chk(parabuf,*plVar8,lVar3 - *plVar8,5000);
    plVar1 = word_limit;
    lVar7 = *plVar8;
    lVar4 = (long)((int)lVar7 - iVar2);
    wptr = (undefined1 *)(lVar3 - lVar4);
    plVar5 = plVar8;
    if (plVar8 <= word_limit) {
      while( true ) {
        plVar6 = plVar5 + 5;
        *plVar5 = lVar7 - lVar4;
        if (plVar1 < plVar6) break;
        lVar7 = *plVar6;
        plVar5 = plVar6;
      }
    }
    __memmove_chk(unused_word_type,plVar8,(long)plVar1 + (0x28 - (long)plVar8),40000);
    word_limit = (long *)((long)plVar1 - (long)(plVar8 + -0x21824));
    return;
  }
  fwrite_unlocked(parabuf,1,(long)wptr - 0x115d80,stdout);
  wptr = parabuf;
  return;
}