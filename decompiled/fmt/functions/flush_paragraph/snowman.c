uint64_t flush_paragraph(void** rdi, void** rsi) {
    void** r12_3;
    uint64_t rax4;
    void** rdx5;
    void** rbx6;
    void* rsi7;
    void** rax8;
    void** rdi9;
    void* rcx10;
    void** rcx11;
    void** rcx12;
    void** rsi13;
    void** r12_14;
    void** rsi15;
    void** r12_16;
    void** rsi17;
    uint64_t rax18;
    void** rdx19;
    uint64_t rax20;
    uint64_t rax21;
    void** rax22;
    void** r12_23;
    void* rcx24;
    uint64_t rax25;

    r12_3 = word_limit;
    if (r12_3 == 0xc120) {
        rax4 = fun_25d0(0x15d80, 1);
        wptr = reinterpret_cast<void**>(0x15d80);
        return rax4;
    } else {
        fmt_paragraph(rdi);
        rdx5 = gc140;
        if (r12_3 == rdx5) {
            rbx6 = r12_3;
        } else {
            rsi7 = *reinterpret_cast<void**>(rdx5 + 24);
            rbx6 = r12_3;
            rax8 = reinterpret_cast<void**>(0x7fffffffffffffff);
            do {
                rdi9 = rdx5;
                rdx5 = *reinterpret_cast<void***>(rdx5 + 32);
                rcx10 = rsi7;
                rsi7 = *reinterpret_cast<void**>(rdx5 + 24);
                rcx11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx10) - reinterpret_cast<uint64_t>(rsi7));
                if (reinterpret_cast<signed char>(rcx11) < reinterpret_cast<signed char>(rax8)) {
                    rax8 = rcx11;
                    rbx6 = rdi9;
                }
                rcx12 = rax8 + 9;
                if (reinterpret_cast<signed char>(rax8) <= reinterpret_cast<signed char>(0x7ffffffffffffff6)) {
                    rax8 = rcx12;
                }
            } while (r12_3 != rdx5);
        }
        *reinterpret_cast<void**>(&rsi13) = first_indent;
        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
        put_line(0xc120, rsi13, rdx5, rcx12);
        r12_14 = gc140;
        if (rbx6 != r12_14) {
            do {
                *reinterpret_cast<void**>(&rsi15) = other_indent;
                *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
                put_line(r12_14, rsi15, rdx5, rcx12);
                r12_14 = *reinterpret_cast<void***>(r12_14 + 32);
            } while (rbx6 != r12_14);
        }
        r12_16 = wptr;
        rsi17 = *reinterpret_cast<void***>(rbx6);
        rax18 = fun_2560(0x15d80, rsi17);
        rdx19 = *reinterpret_cast<void***>(rbx6);
        rax20 = reinterpret_cast<unsigned char>(rdx19) - rax18;
        rax21 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax20)));
        rax22 = rbx6;
        wptr = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_16) - rax21);
        r12_23 = word_limit;
        rcx24 = reinterpret_cast<void*>(-rax21);
        if (reinterpret_cast<unsigned char>(r12_23) >= reinterpret_cast<unsigned char>(rbx6)) {
            while (rax22 = rax22 + 40, *reinterpret_cast<void***>(rax22 + 0xffffffffffffffd8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx19) + reinterpret_cast<uint64_t>(rcx24)), reinterpret_cast<unsigned char>(r12_23) >= reinterpret_cast<unsigned char>(rax22)) {
                rdx19 = *reinterpret_cast<void***>(rax22);
            }
        }
        rax25 = fun_2560(0xc120, rbx6);
        word_limit = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_23) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(0xc120)));
        return rax25;
    }
}