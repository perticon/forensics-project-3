void flush_paragraph(void) {
    // 0x3250
    if (word_limit == (int32_t *)&unused_word_type) {
        // 0x33a0
        function_25d0();
        wptr = (int64_t)&parabuf;
        return;
    }
    int64_t v1 = (int64_t)word_limit; // 0x3252
    fmt_paragraph();
    int64_t v2 = v1; // 0x327a
    if (word_limit != g44) {
        int64_t v3 = (int64_t)g44; // 0x3270
        int64_t v4 = v1; // 0x329b
        int64_t v5 = 0x7fffffffffffffff;
        int64_t v6 = *(int64_t *)(v3 + 32); // 0x32a3
        int64_t v7 = *(int64_t *)(v6 + 24); // 0x32aa
        int64_t v8 = *(int64_t *)(v3 + 24) - v7; // 0x32ae
        int64_t v9 = v8 < v5 ? v8 : v5;
        v4 = v8 < v5 ? v3 : v4;
        int64_t v10 = v9 - 0x7ffffffffffffff6; // 0x32c0
        int64_t v11 = v10 == 0 | v10 < 0 != (0x7ffffffffffffff5 - v9 & v9) < 0 ? v9 + 9 : v9; // 0x32c3
        v2 = v4;
        while (v6 != v1) {
            int64_t v12 = v7;
            int64_t v13 = v6;
            v5 = v11;
            v6 = *(int64_t *)(v13 + 32);
            v7 = *(int64_t *)(v6 + 24);
            v8 = v12 - v7;
            v9 = v8 < v5 ? v8 : v5;
            v4 = v8 < v5 ? v13 : v4;
            v10 = v9 - 0x7ffffffffffffff6;
            v11 = v10 == 0 | v10 < 0 != (0x7ffffffffffffff5 - v9 & v9) < 0 ? v9 + 9 : v9;
            v2 = v4;
        }
    }
    // 0x32cc
    put_line((int32_t *)&unused_word_type, first_indent);
    int64_t v14 = (int64_t)g44; // 0x32da
    int64_t v15 = v14; // 0x32e4
    if (v2 != v14) {
        put_line((int32_t *)v15, other_indent);
        v15 += 32;
        while (v2 != v15) {
            // 0x32f0
            put_line((int32_t *)v15, other_indent);
            v15 += 32;
        }
    }
    int64_t v16 = function_2560(); // 0x3324
    int64_t * v17 = (int64_t *)v2;
    int64_t v18 = *v17; // 0x3329
    int64_t v19 = 0x100000000 * (v18 - v16) >> 32; // 0x3335
    wptr -= v19;
    uint64_t v20 = (int64_t)word_limit; // 0x3347
    if (v2 > v20) {
        // 0x3373
        function_2560();
        *(int64_t *)&word_limit = (int64_t)&unused_word_type - v2 + v20;
        return;
    }
    int64_t v21 = v2 + 40; // 0x3366
    *v17 = v18 - v19;
    int64_t v22 = v21; // 0x3371
    if (v21 <= v20) {
        int64_t * v23 = (int64_t *)v22;
        v22 += 40;
        *v23 = *v23 - v19;
        while (v22 <= v20) {
            // 0x3360
            v23 = (int64_t *)v22;
            v22 += 40;
            *v23 = *v23 - v19;
        }
    }
    // 0x3373
    function_2560();
    *(int64_t *)&word_limit = (int64_t)&unused_word_type - v2 + v20;
}