void fmt_paragraph(void** rdi) {
    void** rsi2;
    void** r9d3;
    void** rcx4;
    void** eax5;
    void** v6;
    void* r13d7;
    void* r12d8;
    struct s0* r10d9;
    void* r11d10;
    void* r15d11;
    void** ebx12;
    void** rdx13;
    void* rdi14;
    void** r15d15;
    void* rax16;
    int64_t rax17;
    int32_t eax18;
    int64_t rax19;
    int64_t r14_20;
    int32_t r14d21;
    int64_t r14_22;
    void* rax23;
    int64_t r14_24;
    int32_t r14d25;
    int64_t r14_26;
    uint64_t r14_27;
    uint32_t eax28;
    int64_t rax29;
    uint32_t eax30;
    uint64_t r14_31;
    int64_t r14_32;
    uint32_t eax33;

    rsi2 = word_limit;
    r9d3 = max_width;
    rcx4 = rsi2 + 0xffffffffffffffd8;
    eax5 = *reinterpret_cast<void***>(rsi2 + 8);
    *reinterpret_cast<void**>(rsi2 + 24) = reinterpret_cast<void*>(0);
    *reinterpret_cast<void***>(rsi2 + 8) = r9d3;
    v6 = eax5;
    if (reinterpret_cast<unsigned char>(rcx4) >= reinterpret_cast<unsigned char>(0xc120)) {
        r13d7 = other_indent;
        r12d8 = first_indent;
        r10d9 = goal_width;
        r11d10 = last_line_length;
        do {
            r15d11 = r13d7;
            ebx12 = *reinterpret_cast<void***>(rcx4 + 8);
            rdx13 = rcx4;
            if (rcx4 == 0xc120) {
                r15d11 = r12d8;
            }
            rdi14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            r15d15 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(r15d11) + reinterpret_cast<unsigned char>(ebx12));
            do {
                rdx13 = rdx13 + 40;
                if (rsi2 == rdx13) {
                    *reinterpret_cast<int32_t*>(&rax16) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                } else {
                    *reinterpret_cast<void**>(&rax17) = reinterpret_cast<void*>(reinterpret_cast<int32_t>(r10d9) - reinterpret_cast<unsigned char>(r15d15));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    eax18 = static_cast<int32_t>(rax17 + rax17 * 4);
                    rax19 = eax18 + eax18;
                    rax16 = reinterpret_cast<void*>(rax19 * rax19);
                    if (rsi2 != *reinterpret_cast<void***>(rdx13 + 32)) {
                        *reinterpret_cast<void**>(&r14_20) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15d15) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx13 + 20)));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_20) + 4) = 0;
                        r14d21 = static_cast<int32_t>(r14_20 + r14_20 * 4);
                        r14_22 = r14d21 + r14d21;
                        rax16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax16) + (r14_22 * r14_22 >> 1));
                    }
                }
                rax23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax16) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rdx13 + 24)));
                if (reinterpret_cast<int1_t>(rcx4 == 0xc120) && !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r11d10) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(r11d10 == 0))) {
                    *reinterpret_cast<void**>(&r14_24) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15d15) - reinterpret_cast<uint32_t>(r11d10));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_24) + 4) = 0;
                    r14d25 = static_cast<int32_t>(r14_24 + r14_24 * 4);
                    r14_26 = r14d25 + r14d25;
                    rax23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax23) + (r14_26 * r14_26 >> 1));
                }
                if (reinterpret_cast<int64_t>(rax23) < reinterpret_cast<int64_t>(rdi14)) {
                    *reinterpret_cast<void***>(rcx4 + 32) = rdx13;
                    rdi14 = rax23;
                    *reinterpret_cast<void***>(rcx4 + 20) = r15d15;
                }
            } while (rsi2 != rdx13 && (r15d15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15d15) + (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx13 + 8)) + reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(rdx13 + 0xffffffffffffffe4)))), reinterpret_cast<signed char>(r9d3) > reinterpret_cast<signed char>(r15d15)));
            *reinterpret_cast<int32_t*>(&r14_27) = 0x1324;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_27) + 4) = 0;
            if (reinterpret_cast<unsigned char>(rcx4) > reinterpret_cast<unsigned char>(0xc120)) {
                eax28 = *reinterpret_cast<unsigned char*>(rcx4 + 0xffffffffffffffe8);
                if (!(*reinterpret_cast<unsigned char*>(&eax28) & 2)) {
                    *reinterpret_cast<int32_t*>(&r14_27) = reinterpret_cast<int32_t>(".so.6");
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_27) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(&eax28) & 4) && ((*reinterpret_cast<int32_t*>(&r14_27) = 0x1324, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_27) + 4) = 0, reinterpret_cast<unsigned char>(rcx4) > reinterpret_cast<unsigned char>(0xc148)) && *reinterpret_cast<unsigned char*>(rcx4 + 0xffffffffffffffc0) & 8)) {
                        *reinterpret_cast<int32_t*>(&rax29) = *reinterpret_cast<int32_t*>(rcx4 + 0xffffffffffffffe0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
                        r14_27 = 0x9c40 / static_cast<int32_t>(rax29 + 2) + 0x1324;
                    }
                } else {
                    eax30 = eax28 & 8;
                    r14_31 = 0x1324 - (0x1324 + reinterpret_cast<uint1_t>(0x1324 < 0x1324 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&eax30) < 1)));
                    *reinterpret_cast<uint32_t*>(&r14_32) = *reinterpret_cast<uint32_t*>(&r14_31) & 0x58804;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_32) + 4) = 0;
                    r14_27 = reinterpret_cast<uint64_t>(r14_32 + 0x960);
                }
            }
            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx4 + 16));
            if (!(*reinterpret_cast<unsigned char*>(&eax33) & 1)) {
                if (*reinterpret_cast<unsigned char*>(&eax33) & 8) {
                    r14_27 = r14_27 + 0x57e4 / reinterpret_cast<int32_t>(ebx12 + 2);
                }
            } else {
                r14_27 = r14_27 - 0x640;
            }
            rcx4 = rcx4 - 40;
            *reinterpret_cast<uint64_t*>(rcx4 + 64) = reinterpret_cast<uint64_t>(rdi14) + r14_27;
        } while (reinterpret_cast<unsigned char>(rcx4) >= reinterpret_cast<unsigned char>(0xc120));
    }
    *reinterpret_cast<void***>(rsi2 + 8) = v6;
    return;
}