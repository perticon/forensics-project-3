void fmt_paragraph(void)

{
  byte bVar1;
  undefined4 uVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  undefined1 *puVar6;
  int iVar7;
  int iVar8;
  long lVar9;
  undefined1 *puVar10;
  undefined1 *puVar11;
  long lVar13;
  long lVar14;
  int iVar15;
  undefined1 *puVar12;
  
  iVar8 = max_width;
  puVar6 = word_limit;
  puVar10 = word_limit + -0x28;
  uVar2 = *(undefined4 *)(word_limit + 8);
  *(undefined8 *)(word_limit + 0x18) = 0;
  *(int *)(puVar6 + 8) = iVar8;
  iVar7 = goal_width;
  iVar5 = first_indent;
  iVar4 = other_indent;
  iVar3 = last_line_length;
  for (; (undefined1 *)0x10c11f < puVar10; puVar10 = puVar10 + -0x28) {
    iVar15 = iVar4;
    if (puVar10 == unused_word_type) {
      iVar15 = iVar5;
    }
    lVar13 = 0x7fffffffffffffff;
    iVar15 = iVar15 + *(int *)(puVar10 + 8);
    puVar12 = puVar10;
    do {
      puVar11 = puVar12 + 0x28;
      if (puVar6 == puVar11) {
        lVar9 = 0;
      }
      else {
        lVar9 = (long)((iVar7 - iVar15) * 10);
        lVar9 = lVar9 * lVar9;
        if (puVar6 != *(undefined1 **)(puVar12 + 0x48)) {
          lVar14 = (long)((iVar15 - *(int *)(puVar12 + 0x3c)) * 10);
          lVar9 = lVar9 + (lVar14 * lVar14 >> 1);
        }
      }
      lVar9 = lVar9 + *(long *)(puVar12 + 0x40);
      if ((puVar10 == unused_word_type) && (0 < iVar3)) {
        lVar14 = (long)((iVar15 - iVar3) * 10);
        lVar9 = lVar9 + (lVar14 * lVar14 >> 1);
      }
      if (lVar9 < lVar13) {
        *(undefined1 **)(puVar10 + 0x20) = puVar11;
        *(int *)(puVar10 + 0x14) = iVar15;
        lVar13 = lVar9;
      }
    } while ((puVar6 != puVar11) &&
            (iVar15 = iVar15 + *(int *)(puVar12 + 0x30) + *(int *)(puVar12 + 0xc), puVar12 = puVar11
            , iVar15 < iVar8));
    lVar9 = 0x1324;
    if (unused_word_type < puVar10) {
      bVar1 = puVar10[-0x18];
      if ((bVar1 & 2) == 0) {
        lVar9 = 0xce4;
        if ((((bVar1 & 4) == 0) && (lVar9 = 0x1324, (undefined1 *)0x10c148 < puVar10)) &&
           ((puVar10[-0x40] & 8) != 0)) {
          lVar9 = SUB168((SEXT816(40000) & (undefined  [16])0xffffffffffffffff | ZEXT816(40000)) /
                         SEXT816((long)(*(int *)(puVar10 + -0x20) + 2)),0) + 0x1324;
        }
      }
      else {
        lVar9 = (ulong)(-(uint)((bVar1 & 8) == 0) & 0x58804) + 0x960;
      }
    }
    if ((puVar10[0x10] & 1) == 0) {
      if ((puVar10[0x10] & 8) != 0) {
        lVar9 = lVar9 + SUB168((SEXT816(0x57e4) & (undefined  [16])0xffffffffffffffff |
                               ZEXT816(0x57e4)) / SEXT816((long)(*(int *)(puVar10 + 8) + 2)),0);
      }
    }
    else {
      lVar9 = lVar9 + -0x640;
    }
    *(long *)(puVar10 + 0x18) = lVar13 + lVar9;
  }
  *(undefined4 *)(puVar6 + 8) = uVar2;
  return;
}