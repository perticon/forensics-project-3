void fmt_paragraph(void) {
    int32_t v1 = g39; // 0x2d23
    g42 = 0;
    g39 = max_width;
    if (&g38 < &unused_word_type) {
        // 0x2e64
        g39 = v1;
        return;
    }
    int64_t v2 = (int64_t)word_limit; // 0x2d00
    uint64_t v3 = (int64_t)&g38;
    uint32_t v4 = *(int32_t *)(v3 + 8); // 0x2d6e
    int64_t v5 = v4; // 0x2d6e
    int64_t * v6 = (int64_t *)(v3 + 32);
    int32_t * v7 = (int32_t *)(v3 + 20);
    int64_t v8 = 0x7fffffffffffffff; // 0x2d7e
    int32_t v9 = (v3 == (int64_t)&unused_word_type ? first_indent : other_indent) + v4; // 0x2d7e
    uint32_t v10 = v9;
    int64_t v11 = v3;
    int64_t v12 = v11 + 40; // 0x2da7
    if (v12 != v2) {
        // 0x2db0
        if (*(int64_t *)(v11 + 72) != v2) {
            // branch -> 0x2de2
        }
    }
    // 0x2de2
    if (last_line_length >= 1 && v3 == (int64_t)&unused_word_type) {
        // branch -> 0x2d80
    }
    // 0x2d80
    if (v8 < v8) {
        // 0x2d85
        *v6 = v12;
        *v7 = v10;
    }
    int64_t v13 = v8;
    int64_t v14; // 0x2d00
    int64_t v15; // 0x2d00
    int64_t v16; // 0x2d00
    int64_t v17; // 0x2d00
    int64_t v18; // 0x2d00
    int64_t v19; // 0x2d00
    int64_t v20; // 0x2d00
    int64_t v21; // 0x2de2
    int64_t v22; // 0x2dbb
    int64_t v23; // 0x2dfd
    int64_t v24; // 0x2dd8
    int32_t v25; // 0x2d9c
    while (v12 != v2) {
        // 0x2d99
        v25 = *(int32_t *)(v11 + 12);
        v9 = *(int32_t *)(v11 + 48) + v10 + v25;
        v20 = v12;
        v8 = v13;
        if (max_width <= v9) {
            // break -> 0x2e18
            break;
        }
        v10 = v9;
        v11 = v20;
        v15 = v10;
        v12 = v11 + 40;
        v18 = 0;
        if (v12 != v2) {
            // 0x2db0
            v22 = 0xa00000000 * ((int64_t)goal_width - v15) >> 32;
            v14 = v22 * v22;
            v18 = v14;
            if (*(int64_t *)(v11 + 72) != v2) {
                // 0x2dc7
                v24 = (int64_t)(10 * (v10 - *(int32_t *)(v11 + 60)));
                v18 = v24 * v24 / 2 + v14;
            }
        }
        // 0x2de2
        v21 = *(int64_t *)(v11 + 64) + v18;
        v16 = v21;
        if (last_line_length >= 1 && v3 == (int64_t)&unused_word_type) {
            // 0x2df0
            v23 = 0xa00000000 * (v15 - (int64_t)last_line_length) >> 32;
            v16 = v21 + v23 * v23 / 2;
        }
        // 0x2d80
        v17 = v16;
        v19 = v8;
        if (v17 < v8) {
            // 0x2d85
            *v6 = v12;
            *v7 = v10;
            v19 = v17;
        }
        // 0x2d90
        v13 = v19;
    }
    int64_t v26 = &g1; // 0x2e21
    unsigned char v27; // 0x2e23
    int64_t v28; // 0x2e23
    int32_t v29; // 0x2ec6
    if (v3 > (int64_t)&unused_word_type) {
        // 0x2e23
        v27 = *(char *)(v3 - 24);
        v28 = v27;
        if ((v27 & 2) == 0) {
            // 0x2ea0
            v26 = (int64_t)".so.6";
            if ((v28 & 4) == 0) {
                // 0x2eaa
                v26 = &g1;
                if (v3 > (int64_t)&g45) {
                    // 0x2ebc
                    v26 = &g1;
                    if ((*(char *)(v3 - 64) & 8) != 0) {
                        // 0x2ec6
                        v29 = *(int32_t *)(v3 - 32);
                        v26 = (int64_t)((int128_t)(int64_t)&g26 / (int128_t)(int64_t)(v29 + 2)) + (int64_t)&g1;
                    }
                }
            }
        } else {
            // 0x2e2b
            v26 = (v28 & 8) == 0 ? 0x59164 : 2400;
        }
    }
    int64_t v30 = v26;
    unsigned char v31 = *(char *)(v3 + 16); // 0x2e41
    int64_t v32; // 0x2d00
    if (v31 % 2 == 0) {
        // 0x2e80
        v32 = v30;
        if ((v31 & 8) != 0) {
            // 0x2e84
            v32 = 0x57e4 / (0x100000000 * v5 + 0x200000000 >> 32) + v30;
        }
    } else {
        // 0x2e49
        v32 = v30 - (int64_t)&g8;
    }
    int64_t v33 = v3 - 40; // 0x2e53
    *(int64_t *)(v3 + 24) = v32 + v13;
    while (v33 >= (int64_t)&unused_word_type) {
        // 0x2d68
        v3 = v33;
        v4 = *(int32_t *)(v3 + 8);
        v5 = v4;
        v6 = (int64_t *)(v3 + 32);
        v7 = (int32_t *)(v3 + 20);
        v8 = 0x7fffffffffffffff;
        v9 = (v3 == (int64_t)&unused_word_type ? first_indent : other_indent) + v4;
        v10 = v9;
        v11 = v3;
        v15 = v10;
        v12 = v11 + 40;
        v18 = 0;
        if (v12 != v2) {
            // 0x2db0
            v22 = 0xa00000000 * ((int64_t)goal_width - v15) >> 32;
            v14 = v22 * v22;
            v18 = v14;
            if (*(int64_t *)(v11 + 72) != v2) {
                // 0x2dc7
                v24 = (int64_t)(10 * (v10 - *(int32_t *)(v11 + 60)));
                v18 = v24 * v24 / 2 + v14;
            }
        }
        // 0x2de2
        v21 = *(int64_t *)(v11 + 64) + v18;
        v16 = v21;
        if (last_line_length >= 1 && v3 == (int64_t)&unused_word_type) {
            // 0x2df0
            v23 = 0xa00000000 * (v15 - (int64_t)last_line_length) >> 32;
            v16 = v21 + v23 * v23 / 2;
        }
        // 0x2d80
        v17 = v16;
        v19 = v8;
        if (v17 < v8) {
            // 0x2d85
            *v6 = v12;
            *v7 = v10;
            v19 = v17;
        }
        // 0x2d90
        v13 = v19;
        while (v12 != v2) {
            // 0x2d99
            v25 = *(int32_t *)(v11 + 12);
            v9 = *(int32_t *)(v11 + 48) + v10 + v25;
            v20 = v12;
            v8 = v13;
            if (max_width <= v9) {
                // break -> 0x2e18
                break;
            }
            v10 = v9;
            v11 = v20;
            v15 = v10;
            v12 = v11 + 40;
            v18 = 0;
            if (v12 != v2) {
                // 0x2db0
                v22 = 0xa00000000 * ((int64_t)goal_width - v15) >> 32;
                v14 = v22 * v22;
                v18 = v14;
                if (*(int64_t *)(v11 + 72) != v2) {
                    // 0x2dc7
                    v24 = (int64_t)(10 * (v10 - *(int32_t *)(v11 + 60)));
                    v18 = v24 * v24 / 2 + v14;
                }
            }
            // 0x2de2
            v21 = *(int64_t *)(v11 + 64) + v18;
            v16 = v21;
            if (last_line_length >= 1 && v3 == (int64_t)&unused_word_type) {
                // 0x2df0
                v23 = 0xa00000000 * (v15 - (int64_t)last_line_length) >> 32;
                v16 = v21 + v23 * v23 / 2;
            }
            // 0x2d80
            v17 = v16;
            v19 = v8;
            if (v17 < v8) {
                // 0x2d85
                *v6 = v12;
                *v7 = v10;
                v19 = v17;
            }
            // 0x2d90
            v13 = v19;
        }
        // 0x2e18
        v26 = &g1;
        if (v3 > (int64_t)&unused_word_type) {
            // 0x2e23
            v27 = *(char *)(v3 - 24);
            v28 = v27;
            if ((v27 & 2) == 0) {
                // 0x2ea0
                v26 = (int64_t)".so.6";
                if ((v28 & 4) == 0) {
                    // 0x2eaa
                    v26 = &g1;
                    if (v3 > (int64_t)&g45) {
                        // 0x2ebc
                        v26 = &g1;
                        if ((*(char *)(v3 - 64) & 8) != 0) {
                            // 0x2ec6
                            v29 = *(int32_t *)(v3 - 32);
                            v26 = (int64_t)((int128_t)(int64_t)&g26 / (int128_t)(int64_t)(v29 + 2)) + (int64_t)&g1;
                        }
                    }
                }
            } else {
                // 0x2e2b
                v26 = (v28 & 8) == 0 ? 0x59164 : 2400;
            }
        }
        // 0x2e41
        v30 = v26;
        v31 = *(char *)(v3 + 16);
        if (v31 % 2 == 0) {
            // 0x2e80
            v32 = v30;
            if ((v31 & 8) != 0) {
                // 0x2e84
                v32 = 0x57e4 / (0x100000000 * v5 + 0x200000000 >> 32) + v30;
            }
        } else {
            // 0x2e49
            v32 = v30 - (int64_t)&g8;
        }
        // 0x2e50
        v33 = v3 - 40;
        *(int64_t *)(v3 + 24) = v32 + v13;
    }
    // 0x2e64
    g39 = v1;
}