void get_prefix(_IO_FILE *param_1)

{
  byte bVar1;
  byte *pbVar2;
  uint uVar3;
  byte *pbVar4;
  
  pbVar4 = (byte *)param_1->_IO_read_ptr;
  in_column = 0;
  if (pbVar4 < param_1->_IO_read_end) {
    param_1->_IO_read_ptr = (char *)(pbVar4 + 1);
    uVar3 = (uint)*pbVar4;
  }
  else {
    uVar3 = __uflow(param_1);
  }
  uVar3 = get_space(param_1,uVar3);
  next_prefix_indent = in_column;
  if (prefix_length == 0) {
    if (prefix_lead_space < in_column) {
      next_prefix_indent = prefix_lead_space;
    }
  }
  else {
    bVar1 = *prefix;
    pbVar4 = prefix;
    while( true ) {
      if (bVar1 == 0) {
        get_space(param_1,uVar3);
        return;
      }
      if (bVar1 != uVar3) break;
      in_column = in_column + 1;
      pbVar2 = (byte *)param_1->_IO_read_ptr;
      if (pbVar2 < param_1->_IO_read_end) {
        param_1->_IO_read_ptr = (char *)(pbVar2 + 1);
        uVar3 = (uint)*pbVar2;
      }
      else {
        uVar3 = __uflow(param_1);
      }
      bVar1 = pbVar4[1];
      pbVar4 = pbVar4 + 1;
    }
  }
  return;
}