word32 get_prefix(FILE * rdi, union Eq_1114 & ecxOut, union Eq_1114 & edxOut)
{
	uint64 rsi_19;
	byte * rax_13 = rdi->ptr0008;
	g_t1710C.u0 = 0x00;
	if (rax_13 < rdi->ptr0010)
	{
		rdi->ptr0008 = rax_13 + 1;
		rsi_19 = (uint64) *rax_13;
	}
	else
		rsi_19 = (uint64) fn0000000000002380(rdi);
	uint64 rax_143 = (uint64) get_space((word32) rsi_19, rdi);
	word32 eax_88 = (word32) rax_143;
	Eq_464 edx_110 = g_t1710C;
	if (g_dw17114 != 0x00)
	{
		byte * rbx_43 = (word64) prefix + 1;
		g_tC0E4 = edx_110;
		uint64 rdx_198 = (uint64) *rbx_43;
		while ((byte) rdx_198 != 0x00)
		{
			if ((word32) rdx_198 != (word32) rax_143)
				goto l0000000000002FD5;
			uint64 rax_199;
			g_t1710C = (word32) g_t1710C + 1;
			byte * rax_60 = rdi->ptr0008;
			word32 rax_32_32_148 = SLICE(rax_60, word32, 32);
			if (rax_60 >= rdi->ptr0010)
				rax_199 = SEQ(rax_32_32_148, fn0000000000002380(rdi));
			else
			{
				rdi->ptr0008 = rax_60 + 1;
				rax_199 = (uint64) *rax_60;
			}
			rdx_198 = (uint64) *rbx_43;
			eax_88 = (word32) rax_199;
			++rbx_43;
			rax_143 = rax_199;
		}
		uint64 rax_100 = (uint64) get_space(eax_88, rdi);
		ecxOut.u0 = <invalid>;
		edxOut.u0 = <invalid>;
		return (word32) rax_100;
	}
	else
	{
		Eq_464 ecx_107 = g_t17118;
		if (edx_110 > ecx_107)
			edx_110 = ecx_107;
		g_tC0E4 = edx_110;
l0000000000002FD5:
		ecxOut.u0 = <invalid>;
		edxOut.u0 = <invalid>;
		return (word32) rax_143;
	}
}