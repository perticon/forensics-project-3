uint32_t get_prefix(void** rdi, void** rsi) {
    void** rbp3;
    void** rax4;
    uint32_t eax5;
    uint32_t esi6;
    uint32_t eax7;
    void* ecx8;
    void* edx9;
    void** rbx10;
    uint32_t edx11;
    void* tmp32_12;
    void** rax13;
    void* ecx14;
    uint32_t eax15;
    void** rbx16;
    void* eax17;
    void** rax18;
    int64_t rdx19;
    void* eax20;
    int64_t rax21;

    rbp3 = rdi;
    rax4 = *reinterpret_cast<void***>(rdi + 8);
    in_column = reinterpret_cast<void*>(0);
    if (reinterpret_cast<unsigned char>(rax4) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 16))) {
        eax5 = fun_2380(rdi);
        esi6 = eax5;
    } else {
        *reinterpret_cast<void***>(rdi + 8) = rax4 + 1;
        esi6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4));
    }
    eax7 = get_space(rbp3, esi6);
    ecx8 = prefix_length;
    edx9 = in_column;
    if (ecx8) {
        rbx10 = prefix;
        next_prefix_indent = edx9;
        edx11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10));
        if (*reinterpret_cast<signed char*>(&edx11)) {
            do {
                if (edx11 != eax7) 
                    break;
                tmp32_12 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(in_column) + 1);
                in_column = tmp32_12;
                rax13 = *reinterpret_cast<void***>(rbp3 + 8);
                if (reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3 + 16))) {
                    *reinterpret_cast<void***>(rbp3 + 8) = rax13 + 1;
                    eax7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13));
                } else {
                    eax7 = fun_2380(rbp3, rbp3);
                }
                edx11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10 + 1));
                ++rbx10;
            } while (*reinterpret_cast<signed char*>(&edx11));
            goto addr_2ff4_11;
        } else {
            addr_2ff4_11:
            goto addr_2ef0_12;
        }
    } else {
        ecx14 = prefix_lead_space;
        if (reinterpret_cast<int32_t>(edx9) > reinterpret_cast<int32_t>(ecx14)) {
            edx9 = ecx14;
        }
        next_prefix_indent = edx9;
    }
    return eax7;
    addr_2ef0_12:
    eax15 = eax7;
    rbx16 = rbp3;
    while (1) {
        if (eax15 == 32) {
            eax17 = in_column;
            in_column = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax17) + 1);
            rax18 = *reinterpret_cast<void***>(rbx16 + 8);
            if (reinterpret_cast<unsigned char>(rax18) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx16 + 16))) 
                goto addr_2f34_19; else 
                goto addr_2f5d_20;
        }
        if (eax15 != 9) 
            break;
        *reinterpret_cast<void**>(&rdx19) = in_column;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        tabs = 1;
        eax20 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx19 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx19)) < reinterpret_cast<int32_t>(0)) 
            goto addr_2f1a_23;
        eax20 = *reinterpret_cast<void**>(&rdx19);
        addr_2f1a_23:
        *reinterpret_cast<int32_t*>(&rax21) = reinterpret_cast<int32_t>(eax20) >> 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        in_column = reinterpret_cast<void*>(static_cast<uint32_t>(rax21 * 8 + 8));
        rax18 = *reinterpret_cast<void***>(rbx16 + 8);
        if (reinterpret_cast<unsigned char>(rax18) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx16 + 16))) {
            addr_2f5d_20:
            eax15 = fun_2380(rbx16);
        } else {
            addr_2f34_19:
            *reinterpret_cast<void***>(rbx16 + 8) = rax18 + 1;
            eax15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax18));
        }
    }
    return eax15;
}