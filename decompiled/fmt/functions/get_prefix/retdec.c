int32_t get_prefix(struct _IO_FILE * f) {
    int64_t v1 = (int64_t)f;
    int64_t * v2 = (int64_t *)(v1 + 8); // 0x2f89
    uint64_t v3 = *v2; // 0x2f89
    in_column = 0;
    int64_t * v4 = (int64_t *)(v1 + 16); // 0x2f97
    int64_t v5; // 0x2f80
    if (v3 >= *v4) {
        // 0x3040
        v5 = function_2380() & 0xffffffff;
    } else {
        // 0x2fa1
        *v2 = v3 + 1;
        v5 = (int64_t)*(char *)v3;
    }
    int32_t result = get_space(f, (int32_t)v5); // 0x2faf
    int32_t v6 = in_column; // 0x2fba
    if (prefix_length == 0) {
        int32_t v7 = prefix_lead_space; // 0x2fc4
        int32_t v8 = v6 - v7; // 0x2fca
        next_prefix_indent = v8 < 0 == ((v8 ^ v6) & (v7 ^ v6)) < 0 == (v8 != 0) ? v7 : v6;
        // 0x2fd5
        return result;
    }
    int64_t v9 = result; // 0x2faf
    next_prefix_indent = v6;
    char v10 = *(char *)prefix; // 0x2fed
    char v11 = v10; // 0x2ff2
    if (v10 == 0) {
        // 0x2ff4
        return get_space(f, (int32_t)v9);
    }
    int64_t v12 = prefix; // 0x2ff2
    int32_t result2 = v9;
    while ((int32_t)v11 == result2) {
        // 0x3023
        in_column++;
        uint64_t v13 = *v2; // 0x302a
        int64_t v14; // 0x2f80
        if (v13 < *v4) {
            // 0x3008
            *v2 = v13 + 1;
            v14 = (int64_t)*(char *)v13;
        } else {
            // 0x3034
            v14 = function_2380();
        }
        // 0x3013
        v12++;
        v11 = *(char *)v12;
        int64_t v15 = v14; // 0x301d
        if (v11 == 0) {
            // 0x2ff4
            return get_space(f, (int32_t)v15);
        }
        result2 = v14;
    }
    // 0x2fd5
    return result2;
}