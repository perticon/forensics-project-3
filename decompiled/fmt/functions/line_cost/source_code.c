line_cost (WORD *next, int len)
{
  int n;
  COST cost;

  if (next == word_limit)
    return 0;
  n = goal_width - len;
  cost = SHORT_COST (n);
  if (next->next_break != word_limit)
    {
      n = len - next->line_length;
      cost += RAGGED_COST (n);
    }
  return cost;
}