void put_line(long param_1,int param_2)

{
  long lVar1;
  char *pcVar2;
  long lVar3;
  
  out_column = 0;
  put_space(prefix_indent);
  fputs_unlocked(prefix,stdout);
  out_column = prefix_length + out_column;
  put_space(param_2 - out_column);
  lVar1 = *(long *)(param_1 + 0x20) + -0x28;
  lVar3 = param_1;
  if (param_1 != lVar1) {
    do {
      param_1 = lVar3 + 0x28;
      put_word(lVar3);
      put_space(*(undefined4 *)(lVar3 + 0xc));
      lVar3 = param_1;
    } while (lVar1 != param_1);
  }
  put_word(param_1);
  last_line_length = out_column;
  pcVar2 = stdout->_IO_write_ptr;
  if (stdout->_IO_write_end <= pcVar2) {
    __overflow(stdout,10);
    return;
  }
  stdout->_IO_write_ptr = pcVar2 + 1;
  *pcVar2 = '\n';
  return;
}