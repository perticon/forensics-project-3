void put_line(int32_t * w, int32_t indent) {
    int64_t v1 = (int64_t)w;
    out_column = 0;
    put_space(prefix_indent);
    function_2520();
    int32_t v2 = out_column + prefix_length; // 0x31c9
    out_column = v2;
    put_space(indent - v2);
    int64_t v3 = *(int64_t *)(v1 + 32) - 40; // 0x31e2
    int64_t v4 = v1; // 0x31e9
    if (v3 != v1) {
        int64_t v5 = v1 + 40; // 0x31f3
        put_word((int32_t *)v1);
        put_space(*(int32_t *)(v1 + 12));
        v4 = v5;
        while (v3 != v5) {
            int64_t v6 = v5;
            v5 = v6 + 40;
            put_word((int32_t *)v6);
            put_space(*(int32_t *)(v6 + 12));
            v4 = v5;
        }
    }
    // 0x3209
    put_word((int32_t *)v4);
    int64_t v7 = (int64_t)g31; // 0x3217
    last_line_length = out_column;
    int64_t * v8 = (int64_t *)(v7 + 40); // 0x3224
    uint64_t v9 = *v8; // 0x3224
    if (v9 >= *(int64_t *)(v7 + 48)) {
        // 0x3240
        function_24b0();
        return;
    }
    // 0x322e
    *v8 = v9 + 1;
    *(char *)v9 = 10;
}