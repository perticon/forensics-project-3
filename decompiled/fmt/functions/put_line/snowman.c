void put_line(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    int64_t rdi6;
    void** rsi7;
    void** rdi8;
    void* eax9;
    void* tmp32_10;
    int64_t rdi11;
    void** rbx12;
    void** rdi13;
    int64_t rdi14;
    void* eax15;
    void** rdi16;
    void** rax17;

    rbp5 = rdi;
    *reinterpret_cast<void**>(&rdi6) = prefix_indent;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
    out_column = reinterpret_cast<void*>(0);
    put_space(rdi6, rsi);
    rsi7 = stdout;
    rdi8 = prefix;
    fun_2520(rdi8, rsi7, rdx, rcx);
    eax9 = prefix_length;
    tmp32_10 = reinterpret_cast<void*>(reinterpret_cast<int32_t>(eax9) + reinterpret_cast<uint32_t>(out_column));
    out_column = tmp32_10;
    *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<int32_t*>(&rsi) - reinterpret_cast<uint32_t>(tmp32_10);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
    put_space(rdi11, rsi7);
    rbx12 = *reinterpret_cast<void***>(rbp5 + 32) + 0xffffffffffffffd8;
    if (rbp5 != rbx12) {
        do {
            rdi13 = rbp5;
            rbp5 = rbp5 + 40;
            put_word(rdi13, rsi7);
            *reinterpret_cast<void**>(&rdi14) = *reinterpret_cast<void**>(rbp5 + 0xffffffffffffffe4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
            put_space(rdi14, rsi7);
        } while (rbx12 != rbp5);
    }
    put_word(rbp5, rsi7);
    eax15 = out_column;
    rdi16 = stdout;
    last_line_length = eax15;
    rax17 = *reinterpret_cast<void***>(rdi16 + 40);
    if (reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi16 + 48))) {
        *reinterpret_cast<void***>(rdi16 + 40) = rax17 + 1;
        *reinterpret_cast<void***>(rax17) = reinterpret_cast<void**>(10);
        return;
    }
}