#include <stdint.h>

/* /tmp/tmpcdw6mu0x @ 0x2c10 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpcdw6mu0x @ 0x44b0 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000879d;
        rdx = 0x00008790;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000800d;
        rdx = 0x00008797;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x00008799;
    rdx = 0x00008794;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x7a20 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x25b0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpcdw6mu0x @ 0x4590 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x26c0)() ();
    }
    rdx = 0x00008800;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x8800 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x000087a1;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x00008797;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000882c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x882c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000800d;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x00008797;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000800d;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x00008797;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000892c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x892c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x00008a2c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x8a2c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x00008797;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000800d;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000800d;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x00008797;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpcdw6mu0x @ 0x26c0 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 21890 named .text */
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x59b0 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x26c5)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x26c5 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26ca */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x23a0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpcdw6mu0x @ 0x26d0 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26d5 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26da */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26df */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26e4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26e9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26ee */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26f3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x26f8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x2d00 */
 
int64_t dbg_fmt_paragraph (void) {
    /* void fmt_paragraph(); */
    rsi = word_limit;
    r8 = obj_unused_word_type;
    r9d = max_width;
    rcx = rsi - 0x28;
    eax = *((rsi + 8));
    *((rsi + 0x18)) = 0;
    *((rsi + 8)) = r9d;
    *((rsp - 4)) = eax;
    if (rcx < r8) {
        goto label_5;
    }
    r13d = other_indent;
    r12d = first_indent;
    r10d = goal_width;
    r11d = last_line_length;
label_2:
    r15d = r13d;
    ebx = *((rcx + 8));
    rdx = rcx;
    if (rcx == r8) {
        r15d = r12d;
    }
    rdi = rbp;
    r15d += ebx;
    while (rcx != r8) {
label_0:
        if (rax < rdi) {
            *((rcx + 0x20)) = rdx;
            rdi = rax;
            *((rcx + 0x14)) = r15d;
        }
        if (rsi == rdx) {
            goto label_6;
        }
        eax = *((rdx + 8));
        eax += *((rdx - 0x1c));
        r15d += eax;
        if (r9d <= r15d) {
            goto label_6;
        }
        rdx += 0x28;
        if (rsi == rdx) {
            goto label_7;
        }
        eax = r10d;
        eax -= r15d;
        eax = rax * 5;
        eax += eax;
        rax = (int64_t) eax;
        rax *= rax;
        if (rsi != *((rdx + 0x20))) {
            r14d = r15d;
            r14d -= *((rdx + 0x14));
            r14d = r14 * 5;
            r14d += r14d;
            r14 = (int64_t) r14d;
            r14 *= r14;
            r14 >>= 1;
            rax += r14;
        }
label_1:
        rax += *((rdx + 0x18));
    }
    if (r11d <= 0) {
        goto label_0;
    }
    r14d = r15d;
    r14d -= r11d;
    r14d = r14 * 5;
    r14d += r14d;
    r14 = (int64_t) r14d;
    r14 *= r14;
    r14 >>= 1;
    rax += r14;
    goto label_0;
label_7:
    eax = 0;
    goto label_1;
label_6:
    r14d = 0x1324;
    if (rcx > r8) {
        eax = *((rcx - 0x18));
        if ((al & 2) == 0) {
            goto label_8;
        }
        eax &= 8;
        r14 -= r14;
        r14d &= 0x58804;
        r14 += 0x960;
    }
label_4:
    eax = *((rcx + 0x10));
    if ((al & 1) == 0) {
        goto label_9;
    }
    r14 -= 0x640;
label_3:
    rdi += r14;
    rcx -= 0x28;
    *((rcx + 0x40)) = rdi;
    if (rcx >= r8) {
        goto label_2;
    }
label_5:
    eax = *((rsp - 4));
    *((rsi + 8)) = eax;
    return rax;
label_9:
    if ((al & 8) == 0) {
        goto label_3;
    }
    eax = 0x57e4;
    ebx += 2;
    rbx = (int64_t) ebx;
    __asm ("cqo");
    rax = rdx:rax / rbx;
    rdx = rdx:rax % rbx;
    r14 += rax;
    goto label_3;
label_8:
    r14d = 0xce4;
    if ((al & 4) != 0) {
        goto label_4;
    }
    rax = 0x0000c148;
    r14d = 0x1324;
    if (rcx <= rax) {
        goto label_4;
    }
    if ((*((rcx - 0x40)) & 8) == 0) {
        goto label_4;
    }
    eax = *((rcx - 0x20));
    r14d = rax + 2;
    eax = 0x9c40;
    r14 = (int64_t) r14d;
    __asm ("cqo");
    rax = rdx:rax / r14;
    rdx = rdx:rax % r14;
    r14 = rax + 0x1324;
    goto label_4;
}

/* /tmp/tmpcdw6mu0x @ 0x2ef0 */
 
int64_t dbg_get_space (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int get_space(FILE * f,int c); */
    eax = esi;
    rbx = rdi;
    while (eax != 0x20) {
        if (eax != 9) {
            goto label_2;
        }
        edx = in_column;
        *(obj.tabs) = 1;
        eax = rdx + 7;
        __asm ("cmovns eax, edx");
        eax >>= 3;
        eax = rax*8 + 8;
        *(obj.in_column) = eax;
        rax = *((rbx + 8));
        if (rax >= *((rbx + 0x10))) {
            goto label_3;
        }
label_0:
        rdx = rax + 1;
        *((rbx + 8)) = rdx;
        eax = *(rax);
label_1:
    }
    eax = in_column;
    eax++;
    *(obj.in_column) = eax;
    rax = *((rbx + 8));
    if (rax < *((rbx + 0x10))) {
        goto label_0;
    }
label_3:
    rdi = rbx;
    uflow ();
    goto label_1;
label_2:
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x2f80 */
 
int64_t dbg_get_prefix (int64_t arg_8h, uint32_t arg_10h, uint32_t arg1) {
    rdi = arg1;
    /* int get_prefix(FILE * f); */
    rax = *((rdi + 8));
    *(obj.in_column) = 0;
    if (rax >= *((rdi + 0x10))) {
        goto label_4;
    }
    rdx = rax + 1;
    *((rdi + 8)) = rdx;
    esi = *(rax);
label_3:
    eax = get_space (rbp, rsi);
    ecx = prefix_length;
    edx = in_column;
    if (ecx == 0) {
        ecx = prefix_lead_space;
        if (edx > ecx) {
            edx = ecx;
        }
        *(obj.next_prefix_indent) = edx;
label_0:
        return rax;
    }
    rbx = prefix;
    *(obj.next_prefix_indent) = edx;
    edx = *(rbx);
    if (dl != 0) {
        goto label_5;
    }
    do {
        rdi = rbp;
        esi = eax;
        void (*0x2ef0)() ();
label_1:
        rdx = rax + 1;
        *((rbp + 8)) = rdx;
        eax = *(rax);
label_2:
        edx = *((rbx + 1));
        rbx++;
    } while (dl == 0);
label_5:
    if (edx != eax) {
        goto label_0;
    }
    *(obj.in_column)++;
    rax = *((rbp + 8));
    if (rax < *((rbp + 0x10))) {
        goto label_1;
    }
    rdi = rbp;
    uflow ();
    goto label_2;
label_4:
    eax = uflow ();
    esi = eax;
    goto label_3;
}

/* /tmp/tmpcdw6mu0x @ 0x3050 */
 
int64_t dbg_put_word (int64_t arg1) {
    rdi = arg1;
    /* void put_word(WORD * w); */
    ebp = *((rdi + 8));
    rbx = *(rdi);
    if (ebp == 0) {
        goto label_1;
    }
    r12 = rdi;
    rbp += rbx;
    do {
        rdi = stdout;
        rbx++;
        esi = *((rbx - 1));
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_2;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = sil;
label_0:
    } while (rbx != rbp);
    ebp = *((r12 + 8));
label_1:
    *(obj.out_column) += ebp;
    r12 = rbx;
    return rax;
label_2:
    overflow ();
    goto label_0;
}

/* /tmp/tmpcdw6mu0x @ 0x30b0 */
 
uint64_t dbg_put_space (void) {
    /* void put_space(int space); */
    edx = out_column;
    ebx = rdx + rdi;
    if (*(obj.tabs) == 0) {
        goto label_2;
    }
    eax = rbx + 7;
    ecx = rdx + 1;
    __asm ("cmovns eax, ebx");
    eax &= 0xfffffff8;
    ebp >>= 3;
    if (ecx >= eax) {
        goto label_2;
    }
    if (edx >= eax) {
        goto label_2;
    }
    do {
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_3;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 9;
label_1:
        edx = out_column;
        eax = rdx + 7;
        __asm ("cmovns eax, edx");
        eax >>= 3;
        eax++;
        edx = rax*8;
        *(obj.out_column) = edx;
    } while (ebp > eax);
label_2:
    if (ebx <= edx) {
        goto label_4;
    }
    do {
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_5;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0x20;
label_0:
        eax = out_column;
        eax++;
        *(obj.out_column) = eax;
    } while (eax < ebx);
label_4:
    return rax;
label_5:
    esi = 0x20;
    overflow ();
    goto label_0;
label_3:
    esi = 9;
    overflow ();
    goto label_1;
}

/* /tmp/tmpcdw6mu0x @ 0x3190 */
 
int64_t dbg_put_line (int64_t arg_20h, int64_t arg1, int64_t arg2) {
    int64_t var_1ch;
    rdi = arg1;
    rsi = arg2;
    /* void put_line(WORD * w,int indent); */
    ebx = esi;
    edi = prefix_indent;
    *(obj.out_column) = 0;
    put_space ();
    rsi = stdout;
    rdi = prefix;
    fputs_unlocked ();
    eax = prefix_length;
    eax += *(obj.out_column);
    ebx -= eax;
    *(obj.out_column) = eax;
    edi = ebx;
    put_space ();
    rax = *((rbp + 0x20));
    rbx = rax - 0x28;
    if (rbp == rbx) {
        goto label_0;
    }
    do {
        rbp += 0x28;
        put_word (rbp);
        edi = *((rbp - 0x1c));
        put_space ();
    } while (rbx != rbp);
label_0:
    put_word (rbp);
    eax = out_column;
    rdi = stdout;
    *(obj.last_line_length) = eax;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmpcdw6mu0x @ 0x3250 */
 
int64_t dbg_flush_paragraph (void) {
    /* void flush_paragraph(); */
    r12 = word_limit;
    rbp = obj_unused_word_type;
    if (r12 == rbp) {
        goto label_1;
    }
    fmt_paragraph ();
    rdx = Scrt1.o;
    if (r12 == rdx) {
        goto label_2;
    }
    rsi = *((rdx + 0x18));
    rbx = r12;
    rax = 0x7fffffffffffffff;
    r8 = 0x7ffffffffffffff6;
    do {
        rdi = rdx;
        rdx = *((rdx + 0x20));
        rcx = rsi;
        rsi = *((rdx + 0x18));
        rcx -= rsi;
        if (rcx < rax) {
            rax = rcx;
            rbx = rdi;
        }
        rcx = rax + 9;
        if (rax <= r8) {
            rax = rcx;
        }
    } while (r12 != rdx);
label_0:
    put_line (rbp, *(obj.first_indent), rdx);
    r12 = Scrt1.o;
    if (rbx == r12) {
        goto label_3;
    }
    do {
        put_line (r12, *(obj.other_indent), rdx);
        r12 = *((r12 + 0x20));
    } while (rbx != r12);
label_3:
    r12 = wptr;
    rsi = *(rbx);
    ecx = 0x1388;
    rdi = obj_parabuf;
    rdx = r12;
    rdx -= rsi;
    rax = memmove_chk ();
    rdx = *(rbx);
    rdi = rax;
    rax = rdx;
    rax -= rdi;
    rax = (int64_t) eax;
    r12 -= rax;
    rcx = rax;
    rax = rbx;
    *(obj.wptr) = r12;
    r12 = word_limit;
    rcx = -rcx;
    if (r12 >= rbx) {
        goto label_4;
    }
    goto label_5;
    do {
        rdx = *(rax);
label_4:
        rdx += rcx;
        rax += 0x28;
        *((rax - 0x28)) = rdx;
    } while (r12 >= rax);
label_5:
    rdx = r12;
    rsi = rbx;
    rdi = rbp;
    ecx = 0x9c40;
    rdx -= rbx;
    rbx -= rbp;
    r12 -= rbx;
    rdx += 0x28;
    memmove_chk ();
    *(obj.word_limit) = r12;
    return rax;
label_1:
    rdx = wptr;
    rbx = obj_parabuf;
    esi = 1;
    rcx = stdout;
    rdi = rbx;
    rdx -= rbx;
    fwrite_unlocked ();
    *(obj.wptr) = rbx;
    return rax;
label_2:
    rbx = r12;
    goto label_0;
}

/* /tmp/tmpcdw6mu0x @ 0x33e0 */
 
int32_t set_other_indent (void) {
    if (*(obj.split) != 0) {
        eax = first_indent;
        *(obj.other_indent) = eax;
        return eax;
    }
    if (*(obj.crown) != 0) {
        eax = in_column;
        if (dil == 0) {
            eax = *(obj.first_indent);
        }
        *(obj.other_indent) = eax;
        return eax;
    }
    eax = first_indent;
    if (*(obj.tagged) != 0) {
        if (dil == 0) {
            goto label_0;
        }
        edx = in_column;
        if (eax == edx) {
            goto label_0;
        }
        *(obj.other_indent) = edx;
        return eax;
    }
    *(obj.other_indent) = eax;
    do {
        return eax;
label_0:
    } while (eax != *(obj.other_indent));
    eax -= eax;
    eax &= 3;
    *(obj.other_indent) = eax;
    return eax;
}

/* /tmp/tmpcdw6mu0x @ 0x3470 */
 
uint64_t dbg_get_line (int64_t arg_8h, int64_t arg_10h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    /* int get_line(FILE * f,int c); */
    r12 = ")]'\";
    *((rsp + 8)) = rdi;
    rax = ctype_b_loc ();
    rdx = word_limit;
    *((rsp + 0x18)) = rax;
label_0:
    rax = wptr;
    rbx = *((rsp + 8));
    *(rdx) = rax;
    do {
        rdi = obj_out_column;
        if (rax == rdi) {
            goto label_8;
        }
label_2:
        rdx = rax + 1;
        *(rax) = bpl;
        rax = *((rbx + 8));
        *(obj.wptr) = rdx;
        if (rax >= *((rbx + 0x10))) {
            goto label_9;
        }
        rcx = rax + 1;
        *((rbx + 8)) = rcx;
        ebp = *(rax);
        rax = rdx;
label_5:
        if (ebp > 0xd) {
            goto label_10;
        }
    } while (ebp <= 8);
label_1:
    r13 = word_limit;
    ecx = in_column;
    rbx = *(r13);
    rax -= rbx;
    *((r13 + 8)) = eax;
    esi = *(rbx);
    ecx += eax;
    rax = (int64_t) eax;
    *((rsp + 0x14)) = ecx;
    r14 = rbx + rax - 1;
    *(obj.in_column) = ecx;
    rax = strchr ("(['`\", rsi);
    esi = *(r14);
    r8 = rax;
    rax = *((rsp + 0x18));
    rcx = *(rax);
    eax = 4;
    al &= *((rcx + rsi*2));
    cl = (r8 != 0) ? 1 : 0;
    eax |= ecx;
    ecx = *((r13 + 0x10));
    ecx &= 0xfffffffa;
    eax |= ecx;
    *((r13 + 0x10)) = al;
    if (rbx < r14) {
        goto label_11;
    }
    goto label_12;
    do {
        rax = r14 - 1;
        if (rbx == rax) {
            goto label_13;
        }
        r14 = rax;
label_11:
        r15d = *(r14);
        rax = strchr (r12, r15d);
    } while (rax != 0);
label_3:
    rax = strchr (0x0000800f, r15d);
    eax = *((r13 + 0x10));
    dl = (rax != 0) ? 1 : 0;
    edx += edx;
    eax &= 0xfffffffd;
    eax |= edx;
    *((r13 + 0x10)) = al;
    eax = get_space (*((rsp + 8)), ebp);
    rdx = word_limit;
    eax = in_column;
    eax -= *((rsp + 0x14));
    *((rdx + 0xc)) = eax;
    if (ebp == 0xffffffff) {
        goto label_14;
    }
    ecx = *((rdx + 0x10));
    if ((cl & 2) == 0) {
        goto label_15;
    }
    al = (eax > 1) ? 1 : 0;
    sil = (ebp == 0xa) ? 1 : 0;
    al |= sil;
    if (al == 0) {
        goto label_16;
    }
label_4:
    ecx = rax*8;
    eax = *((rdx + 0x10));
    eax &= 0xfffffff7;
    eax |= ecx;
    *((rdx + 0x10)) = al;
    if (ebp == 0xa) {
        goto label_17;
    }
label_7:
    if (*(obj.uniform) != 0) {
        goto label_17;
    }
    rax = 0x00015d10;
    if (rdx == rax) {
        goto label_18;
    }
    rdx += 0x28;
    *(obj.word_limit) = rdx;
    goto label_0;
label_10:
    if (ebp == 0x20) {
        goto label_1;
    }
    rdi = obj_out_column;
    if (rax != rdi) {
        goto label_2;
    }
label_8:
    edi = 1;
    set_other_indent ();
    flush_paragraph ();
    rax = wptr;
    goto label_2;
label_14:
    *((rdx + 0x10)) |= 8;
label_17:
    eax = 0;
    al = ((*((rdx + 0x10)) & 8) != 0) ? 1 : 0;
    eax++;
    *((rdx + 0xc)) = eax;
    rax = 0x00015d10;
    if (rdx == rax) {
        goto label_18;
    }
label_6:
    rdx += 0x28;
    *(obj.word_limit) = rdx;
    if (ebp == 0xa) {
        goto label_19;
    }
    if (ebp != 0xffffffff) {
        goto label_0;
    }
label_19:
    rdi = *((rsp + 8));
    void (*0x2f80)() ();
label_13:
    r15d = *((r14 - 1));
    goto label_3;
label_15:
    eax = 0;
    goto label_4;
label_9:
    rdi = rbx;
    eax = uflow ();
    rax = wptr;
    if (ebp == 0xffffffff) {
        goto label_1;
    }
    goto label_5;
label_18:
    edi = 1;
    set_other_indent ();
    flush_paragraph ();
    rdx = word_limit;
    goto label_6;
label_16:
    ecx &= 0xfffffff7;
    *((rdx + 0x10)) = cl;
    goto label_7;
label_12:
    r15d = *(r14);
    goto label_3;
}

/* /tmp/tmpcdw6mu0x @ 0x3750 */
 
uint64_t dbg_fmt (int64_t arg_20h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool fmt(FILE * f,char const * file); */
    r14 = rdi;
    r12 = rsi;
    rbx = obj_unused_word_type;
    fadvise (rdi, 2);
    *(obj.tabs) = 0;
    *(obj.other_indent) = 0;
    eax = get_prefix (r14, rsi, rdx);
    *(obj.next_char) = eax;
label_11:
    *(obj.last_line_length) = 0;
label_3:
    edi = next_prefix_indent;
    eax = in_column;
    if (ebp == 0xa) {
        goto label_23;
    }
    if (ebp == 0xffffffff) {
        goto label_23;
    }
    if (*(obj.prefix_lead_space) <= edi) {
        edx = prefix_full_length;
        edx += edi;
        if (edx <= eax) {
            goto label_24;
        }
    }
    *(obj.out_column) = 0;
    if (eax <= edi) {
        goto label_25;
    }
    r13d = 1;
label_1:
    put_space ();
    eax = out_column;
    r8d = in_column;
    r15 = prefix;
    if (r8d != eax) {
        goto label_4;
    }
    goto label_26;
    do {
        rcx = rax + 1;
        *((rdi + 0x28)) = rcx;
        *(rax) = sil;
label_0:
        eax = out_column;
        eax++;
        *(obj.out_column) = eax;
        if (eax == r8d) {
            goto label_26;
        }
label_4:
        esi = *(r15);
        if (sil == 0) {
            goto label_26;
        }
        rdi = stdout;
        r15++;
        rax = *((rdi + 0x28));
    } while (rax < *((rdi + 0x30)));
    overflow ();
    r8d = in_column;
    goto label_0;
label_26:
    if (r13b != 0) {
        goto label_27;
    }
    if (ebp != 0xffffffff) {
        goto label_28;
    }
label_5:
    eax = prefix_length;
    eax += *(obj.next_prefix_indent);
    if (eax <= *(obj.in_column)) {
        goto label_29;
    }
label_2:
    *(obj.next_char) = 0xffffffff;
    rax = stdin;
    if ((*(r14) & 0x20) == 0) {
        goto label_30;
    }
    rdi = r14;
    if (r14 == rax) {
        goto label_31;
    }
    rpl_fclose (rdi);
label_18:
    rdx = r12;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
label_20:
    edx = 5;
    ebp = 0;
    rax = dcgettext (0, "read error");
label_21:
    eax = 0;
    rcx = r12;
    eax = error (0, ebp, rax);
    eax = 0;
label_19:
    return rax;
label_23:
    *(obj.out_column) = 0;
    dl = (ebp != 0xa) ? 1 : 0;
    cl = (ebp != 0xffffffff) ? 1 : 0;
    edx &= ecx;
    r13d = edx;
    if (eax > edi) {
        goto label_1;
    }
    if (dl != 0) {
        goto label_32;
    }
label_6:
    if (ebp == 0xffffffff) {
        goto label_2;
    }
label_28:
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_33;
    }
    *((rdi + 0x28)) = rdx;
    *(rax) = 0xa;
label_10:
    eax = get_prefix (r14, rsi, rax + 1);
    goto label_3;
label_25:
    put_space ();
    eax = out_column;
    r8d = in_column;
    r15 = prefix;
    if (eax == r8d) {
        goto label_34;
    }
label_22:
    r13d = 1;
    goto label_4;
label_27:
    edi = r8d;
    edi -= eax;
    rax = put_space ();
    if (ebp != 0xffffffff) {
        goto label_12;
    }
    goto label_5;
label_7:
    rdx = rax + 1;
    *((r14 + 8)) = rdx;
    ebp = *(rax);
    eax = 1;
label_8:
    if (ebp == 0xa) {
        goto label_6;
    }
    if (al == 0) {
        goto label_6;
    }
label_12:
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_35;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = bpl;
label_9:
    rax = *((r14 + 8));
    if (rax < *((r14 + 0x10))) {
        goto label_7;
    }
    rdi = r14;
    eax = uflow ();
    al = (eax != 0xffffffff) ? 1 : 0;
    goto label_8;
label_35:
    esi = (int32_t) bpl;
    overflow ();
    goto label_9;
label_29:
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_36;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0xa;
    goto label_2;
label_33:
    esi = 0xa;
    eax = overflow ();
    goto label_10;
label_24:
    *(obj.prefix_indent) = edi;
    *(obj.first_indent) = eax;
    rax = obj_parabuf;
    *(obj.wptr) = rax;
    *(obj.word_limit) = rbx;
    eax = get_line (r14, ebp, rdx, rcx);
    ecx = next_prefix_indent;
    r8d = prefix_indent;
    edi = 0;
    esi = eax;
    if (ecx == r8d) {
        goto label_37;
    }
label_13:
    set_other_indent ();
    if (*(obj.split) == 0) {
        if (*(obj.crown) == 0) {
            goto label_38;
        }
        if (ecx == r8d) {
            goto label_39;
        }
    }
label_14:
    r15 = word_limit;
    if (r15 <= rbx) {
        goto label_40;
    }
    *((r15 - 0x18)) |= 0xa;
    *(obj.next_char) = esi;
    fmt_paragraph ();
    put_line (rbx, *(obj.first_indent), rdx);
    rbp = Scrt1.o;
    if (rbp == r15) {
        goto label_41;
    }
    do {
        put_line (rbp, *(obj.other_indent), rdx);
        rbp = *((rbp + 0x20));
    } while (rbp != r15);
label_41:
    ebp = next_char;
    goto label_11;
label_34:
    edi = 0;
    put_space ();
    goto label_12;
label_36:
    esi = 0xa;
    overflow ();
    goto label_2;
label_37:
    eax = prefix_full_length;
    eax += ecx;
    if (eax > *(obj.in_column)) {
        goto label_13;
    }
    al = (esi != 0xa) ? 1 : 0;
    edi = 0;
    dil = (esi != 0xffffffff) ? 1 : 0;
    edi &= eax;
    goto label_13;
label_38:
    if (*(obj.tagged) == 0) {
        goto label_42;
    }
    if (ecx != r8d) {
        goto label_14;
    }
    eax = in_column;
    ecx += *(obj.prefix_full_length);
    if (ecx > eax) {
        goto label_14;
    }
    if (esi == 0xa) {
        goto label_14;
    }
    if (esi == 0xffffffff) {
        goto label_14;
    }
    if (*(obj.first_indent) != eax) {
        goto label_43;
    }
    goto label_14;
label_15:
    edx = in_column;
    eax += *(obj.prefix_full_length);
    if (eax > edx) {
        goto label_14;
    }
    if (esi == 0xa) {
        goto label_14;
    }
    if (esi == 0xffffffff) {
        goto label_14;
    }
    if (*(obj.other_indent) != edx) {
        goto label_14;
    }
label_43:
    eax = get_line (r14, rsi, rdx, rcx);
    esi = eax;
    eax = next_prefix_indent;
    if (eax != *(obj.prefix_indent)) {
        goto label_14;
    }
    goto label_15;
label_42:
    if (ecx != r8d) {
        goto label_14;
    }
label_16:
    eax = in_column;
    ecx += *(obj.prefix_full_length);
    if (ecx > eax) {
        goto label_14;
    }
    if (esi == 0xa) {
        goto label_14;
    }
    if (esi == 0xffffffff) {
        goto label_14;
    }
    if (*(obj.other_indent) != eax) {
        goto label_14;
    }
    eax = get_line (r14, rsi, rdx, rcx);
    ecx = next_prefix_indent;
    esi = eax;
    if (ecx != *(obj.prefix_indent)) {
        goto label_14;
    }
    goto label_16;
label_30:
    rdi = r14;
    if (r14 == rax) {
        goto label_44;
    }
    eax = rpl_fclose (rdi);
    if (eax != 0) {
        goto label_45;
    }
    eax = 1;
    return rax;
label_39:
    ecx += *(obj.prefix_full_length);
    if (ecx > *(obj.in_column)) {
        goto label_14;
    }
    if (esi == 0xa) {
        goto label_14;
    }
    if (esi != 0xffffffff) {
        goto label_46;
    }
    goto label_14;
label_17:
    edx = in_column;
    eax += *(obj.prefix_full_length);
    if (eax > edx) {
        goto label_14;
    }
    if (esi == 0xa) {
        goto label_14;
    }
    if (esi == 0xffffffff) {
        goto label_14;
    }
    if (*(obj.other_indent) != edx) {
        goto label_14;
    }
label_46:
    eax = get_line (r14, rsi, rdx, rcx);
    esi = eax;
    eax = next_prefix_indent;
    if (eax != *(obj.prefix_indent)) {
        goto label_14;
    }
    goto label_17;
label_31:
    clearerr_unlocked ();
    goto label_18;
label_44:
    clearerr_unlocked ();
    eax = 1;
    return rax;
label_45:
    rax = errno_location ();
    ebp = *(rax);
    eax = 1;
    if (ebp < 0) {
        goto label_19;
    }
    rdx = r12;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    if (ebp == 0) {
        goto label_20;
    }
    rdx = 0x0000874b;
    goto label_21;
label_40:
    assert_fail ("word < word_limit", "src/fmt.c", 0x270, "get_paragraph");
label_32:
    put_space ();
    eax = out_column;
    r8d = in_column;
    r15 = prefix;
    if (eax != r8d) {
        goto label_22;
    }
    edi = 0;
    put_space ();
    if (ebp != 0xffffffff) {
        goto label_12;
    }
    goto label_5;
}

/* /tmp/tmpcdw6mu0x @ 0x2c40 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x2c70 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x2cb0 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002370 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpcdw6mu0x @ 0x2370 */
 
void fcn_00002370 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpcdw6mu0x @ 0x2cf0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpcdw6mu0x @ 0x7c30 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpcdw6mu0x @ 0x5f00 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x6230 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x2470 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpcdw6mu0x @ 0x6cc0 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x5d40 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x23b0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpcdw6mu0x @ 0x6ec0 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x2590 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpcdw6mu0x @ 0x7400 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000874b);
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x2440 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpcdw6mu0x @ 0x2610 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpcdw6mu0x @ 0x25e0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpcdw6mu0x @ 0x23f0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpcdw6mu0x @ 0x5c60 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x7a60 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x5f50 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26d0)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x5cc0 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x4400 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpcdw6mu0x @ 0x24c0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpcdw6mu0x @ 0x23c0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpcdw6mu0x @ 0x2670 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpcdw6mu0x @ 0x6490 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x26e9)() ();
    }
    if (rdx == 0) {
        void (*0x26e9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x6530 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26ee)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x26ee)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x6070 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x26da)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x63f0 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26e4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x7c44 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpcdw6mu0x @ 0x4260 */
 
void fdadvise (void) {
    return posix_fadvise ();
}

/* /tmp/tmpcdw6mu0x @ 0x6f40 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x7040 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x6ee0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x4330 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x25a0)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpcdw6mu0x @ 0x6e80 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x5f30 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x73c0 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x2460 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpcdw6mu0x @ 0x6ea0 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x6c00 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x6790)() ();
}

/* /tmp/tmpcdw6mu0x @ 0x41b0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000874b);
    } while (1);
}

/* /tmp/tmpcdw6mu0x @ 0x7b70 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpcdw6mu0x @ 0x6e40 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x7af0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x65e0 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26f3)() ();
    }
    if (rax == 0) {
        void (*0x26f3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x6360 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x7290 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x2530 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpcdw6mu0x @ 0x5c00 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x6720 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x5ba0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x7300 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x5e40 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x7550 */
 
void dbg_xdectoumax (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* uintmax_t xdectoumax(char const * n_str,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    xnumtoumax (rdi, 0xa, rsi, rdx, rcx, r8);
}

/* /tmp/tmpcdw6mu0x @ 0x7440 */
 
int64_t dbg_xnumtoumax (uint32_t status, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg6) {
    uintmax_t tnum;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    /* uintmax_t xnumtoumax(char const * n_str,int base,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    r14 = r9;
    r13 = rcx;
    r12 = rdx;
    edx = esi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, edx, rsp, r8);
    if (eax == 0) {
        r15 = *(rsp);
        if (r15 < r12) {
            goto label_2;
        }
        if (r15 > r13) {
            goto label_2;
        }
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r15;
        return rax;
    }
    ebx = eax;
    rax = errno_location ();
    r12 = rax;
    if (ebx != 1) {
        if (ebx != 3) {
            goto label_1;
        }
        *(rax) = 0;
    } else {
label_0:
        *(r12) = 0x4b;
    }
label_1:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    esi = *(r12);
    r8 = rax;
    while (1) {
        if (*((rsp + 0x50)) == 0) {
            *((rsp + 0x50)) = 1;
        }
        rcx = r14;
        eax = 0;
        error (*((rsp + 0x50)), rsi, "%s: %s");
        esi = 0;
    }
label_2:
    rax = errno_location ();
    r12 = rax;
    if (r15 > 0x3fffffff) {
        goto label_0;
    }
    *(rax) = 0x22;
    goto label_1;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x7580 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = section..dynsym;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x00008f18;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0x8f18 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = section..dynsym;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmpcdw6mu0x @ 0x6770 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x41a0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpcdw6mu0x @ 0x6f10 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x70d0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x42a0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2420)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpcdw6mu0x @ 0x6ff0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x6730 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x6680 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26f8)() ();
    }
    if (rax == 0) {
        void (*0x26f8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x7340 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x5e30 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x5d40)() ();
}

/* /tmp/tmpcdw6mu0x @ 0x5f10 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x2700 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    int64_t var_8h;
    uint32_t var_10h;
    size_t * var_18h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15 = 0x00008bc1;
    r12 = 0x000080b8;
    rbx = rsi;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, r15);
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    *(obj.uniform) = 0;
    *(obj.split) = 0;
    *(obj.tagged) = 0;
    *(obj.crown) = 0;
    *(obj.max_width) = 0x4b;
    *(obj.prefix) = r15;
    *(obj.prefix_full_length) = 0;
    *(obj.prefix_lead_space) = 0;
    *(obj.prefix_length) = 0;
    *((rsp + 8)) = 0;
    if (ebp > 1) {
        rax = *((rbx + 8));
        if (*(rax) == 0x2d) {
            goto label_5;
        }
    }
label_1:
    *((rsp + 0x10)) = 0;
    r14 = obj_long_options;
    r13 = "0123456789cstuw:p:g:";
    r12 = 0x000086c8;
    do {
label_0:
        rcx = r14;
        r8d = 0;
        rdx = r13;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        ecx = eax;
        if (eax == 0xffffffff) {
            goto label_6;
        }
        if (ecx > 0x77) {
            goto label_7;
        }
        if (ecx <= 0x62) {
            goto label_8;
        }
        eax = rcx - 0x63;
        if (eax > 0x14) {
            goto label_7;
        }
        rax = *((r12 + rax*4));
        rax += r12;
        /* switch table (21 cases) at 0x86c8 */
        void (*rax)() ();
        rax = optarg;
        *((rsp + 8)) = rax;
    } while (1);
    *(obj.uniform) = 1;
    goto label_0;
    *(obj.tagged) = 1;
    goto label_0;
    *(obj.split) = 1;
    goto label_0;
    rdx = optarg;
    *(obj.prefix_lead_space) = 0;
    if (*(rdx) != 0x20) {
        goto label_9;
    }
    eax = 1;
    eax -= edx;
    do {
        ecx = rax + rdx;
        rdx++;
    } while (*(rdx) == 0x20);
    *(obj.prefix_lead_space) = ecx;
label_9:
    rdi = rdx;
    *(obj.prefix) = rdx;
    *((rsp + 0x18)) = rdx;
    eax = strlen (rdi);
    rdx = *((rsp + 0x18));
    *(obj.prefix_full_length) = eax;
    rax = (int64_t) eax;
    rax += rdx;
    if (rdx < rax) {
        goto label_10;
    }
    goto label_11;
    do {
        rax--;
        if (rdx == rax) {
            goto label_11;
        }
label_10:
    } while (*((rax - 1)) == 0x20);
label_11:
    *(rax) = 0;
    rax -= rdx;
    *(obj.prefix_length) = eax;
    goto label_0;
    rax = optarg;
    *((rsp + 0x10)) = rax;
    goto label_0;
    *(obj.crown) = 1;
    goto label_0;
label_8:
    if (ecx == 0xffffff7d) {
        eax = 0;
        version_etc (*(obj.stdout), 0x0000803a, "GNU coreutils", *(obj.Version), "Ross Paterson", 0);
        exit (0);
    }
    if (ecx != 0xffffff7e) {
        goto label_7;
    }
    rax = usage (0);
label_5:
    edx = *((rax + 1));
    edx -= 0x30;
    if (edx > 9) {
        goto label_1;
    }
    rax++;
    rbx += 8;
    ebp--;
    *((rsp + 8)) = rax;
    rax = *((rbx - 8));
    *(rbx) = rax;
    goto label_1;
label_6:
    r14 = *((rsp + 8));
    if (r14 == 0) {
        goto label_12;
    }
    r13 = "invalid width";
    edx = 5;
    rax = dcgettext (0, r13);
    eax = xdectoumax (r14, 0, 0x9c4, r15, rax, 0);
    *(obj.max_width) = eax;
    r12 = rax;
    if (*((rsp + 0x10)) == 0) {
        goto label_13;
    }
    edx = 5;
    rax = dcgettext (0, r13);
    rdx = (int64_t) r12d;
    eax = xdectoumax (*((rsp + 0x10)), 0, rdx, r15, rax, 0);
    *(obj.goal_width) = eax;
label_3:
    eax = optind;
    if (eax == ebp) {
        goto label_14;
    }
    r13d = 0;
    r12d = 1;
    r14 = 0x00008038;
    if (eax < ebp) {
        goto label_15;
    }
    goto label_16;
    do {
        if (*((r15 + 1)) != 0) {
            goto label_17;
        }
        r13d = 1;
        eax = fmt (*(obj.stdin), r15, rdx);
        r12d &= eax;
label_2:
        eax = optind;
        eax++;
        *(obj.optind) = eax;
        if (eax >= ebp) {
            goto label_18;
        }
label_15:
        rax = (int64_t) eax;
        r15 = *((rbx + rax*8));
    } while (*(r15) == 0x2d);
label_17:
    rax = fopen (r15, r14);
    rsi = r15;
    rdi = rax;
    if (rax == 0) {
        goto label_19;
    }
    eax = fmt (rdi, rsi, rdx);
    r12d &= eax;
    goto label_2;
label_18:
    while (1) {
        eax = rpl_fclose (*(obj.stdin));
        if (eax != 0) {
            goto label_20;
        }
label_16:
        r12d ^= 1;
        eax = (int32_t) r12b;
        return rax;
label_12:
        if (*((rsp + 0x10)) == 0) {
label_13:
            eax = *(obj.max_width) * 0xbb;
            ecx = 0xc8;
            edx:eax = (int64_t) eax;
            eax = edx:eax / ecx;
            edx = edx:eax % ecx;
            *(obj.goal_width) = eax;
            goto label_3;
label_7:
            eax = rcx - 0x30;
            if (eax <= 9) {
                goto label_21;
            }
label_4:
            usage (1);
        }
        edx = 5;
        rax = dcgettext (0, "invalid width");
        eax = xdectoumax (*((rsp + 0x10)), 0, 0x4b, r15, rax, 0);
        *(obj.goal_width) = eax;
        eax += 0xa;
        *(obj.max_width) = eax;
        goto label_3;
label_14:
        eax = fmt (*(obj.stdin), 0x0000812a, rdx);
        r12d = eax;
    }
label_21:
    edx = 5;
    *((rsp + 8)) = ecx;
    rax = dcgettext (0, "invalid option -- %c; -WIDTH is recognized only when it is the first\noption; use -w N instead");
    ecx = *((rsp + 8));
    eax = 0;
    error (0, 0, rax);
    goto label_4;
label_19:
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rsp + 8)) = rax;
    rax = dcgettext (0, "cannot open %s for reading");
    r12 = rax;
    rax = errno_location ();
    rcx = *((rsp + 8));
    eax = 0;
    r12d = 0;
    error (0, *(rax), r12);
    goto label_2;
label_20:
    edx = 5;
    rax = dcgettext (0, "closing standard input");
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000874b);
}

/* /tmp/tmpcdw6mu0x @ 0x3dc0 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [-WIDTH] [OPTION]... [FILE]...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Reformat each paragraph in the FILE(s), writing to standard output.\nThe option -WIDTH is an abbreviated form of --width=DIGITS.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -c, --crown-margin        preserve indentation of first two lines\n  -p, --prefix=STRING       reformat only lines beginning with STRING,\n                              reattaching the prefix to reformatted lines\n  -s, --split-only          split long lines, but do not refill\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -t, --tagged-paragraph    indentation of first line different from second\n  -u, --uniform-spacing     one space between words, two after sentences\n  -w, --width=WIDTH         maximum line width (default of 75 columns)\n  -g, --goal=WIDTH          goal width (default of 93% of width)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    r12 = 0x0000803a;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000803e;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x000080b8;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000080c2, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x00008bc1;
    r12 = 0x0000805a;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000080c2, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = 0x0000803a;
        printf_chk ();
        r12 = 0x0000805a;
    }
label_5:
    r13 = 0x0000803a;
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpcdw6mu0x @ 0x2680 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpcdw6mu0x @ 0x2660 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpcdw6mu0x @ 0x2600 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpcdw6mu0x @ 0x2520 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpcdw6mu0x @ 0x2540 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpcdw6mu0x @ 0x25f0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpcdw6mu0x @ 0x5be0 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x62d0 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x6100 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x26df)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x5fe0 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x26d5)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x5c80 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x26ca)() ();
    }
    if (rdx == 0) {
        void (*0x26ca)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x6190 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00017270]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00017280]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x7260 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x72e0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x4270 */
 
uint32_t dbg_fadvise (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void fadvise(FILE * fp,fadvice_t advice); */
    if (rdi != 0) {
        r12d = esi;
        eax = fileno (rdi);
        ecx = r12d;
        edx = 0;
        esi = 0;
        edi = eax;
        void (*0x2500)() ();
    }
    return eax;
}

/* /tmp/tmpcdw6mu0x @ 0x2580 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpcdw6mu0x @ 0x5ee0 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x6f80 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x6750 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpcdw6mu0x @ 0x4380 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x2620)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpcdw6mu0x @ 0x5c20 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x7160 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpcdw6mu0x @ 0x6da0 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpcdw6mu0x @ 0x2550 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpcdw6mu0x @ 0x4190 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpcdw6mu0x @ 0x79b0 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpcdw6mu0x @ 0x23e0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpcdw6mu0x @ 0x6790 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x00008b68;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x00008b7b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x00008e68;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x8e68 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2680)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2680)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2680)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpcdw6mu0x @ 0x6c20 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpcdw6mu0x @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpcdw6mu0x @ 0x72c0 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x7c20 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpcdw6mu0x @ 0x6fc0 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x7380 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2570)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpcdw6mu0x @ 0x2380 */
 
void uflow (void) {
    /* [15] -r-x section size 832 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpcdw6mu0x @ 0x2390 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpcdw6mu0x @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rsi) += bh;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += *(rax);
    *(rax) += al;
    eax |= 0x28004000;
    *(rdi) += ah;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("loopne 0xe8");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("loopne 0xf0");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al = 0;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x24)) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmpcdw6mu0x @ 0x23d0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpcdw6mu0x @ 0x2400 */
 
void clearerr_unlocked (void) {
    __asm ("bnd jmp qword [reloc.clearerr_unlocked]");
}

/* /tmp/tmpcdw6mu0x @ 0x2410 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpcdw6mu0x @ 0x2420 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpcdw6mu0x @ 0x2430 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpcdw6mu0x @ 0x2450 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpcdw6mu0x @ 0x2480 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpcdw6mu0x @ 0x2490 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpcdw6mu0x @ 0x24a0 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpcdw6mu0x @ 0x24b0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmpcdw6mu0x @ 0x24d0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpcdw6mu0x @ 0x24e0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpcdw6mu0x @ 0x24f0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpcdw6mu0x @ 0x2500 */
 
void posix_fadvise (void) {
    __asm ("bnd jmp qword [reloc.posix_fadvise]");
}

/* /tmp/tmpcdw6mu0x @ 0x2510 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpcdw6mu0x @ 0x2560 */
 
void memmove_chk (void) {
    __asm ("bnd jmp qword [reloc.__memmove_chk]");
}

/* /tmp/tmpcdw6mu0x @ 0x2570 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpcdw6mu0x @ 0x25a0 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpcdw6mu0x @ 0x25c0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpcdw6mu0x @ 0x25d0 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmpcdw6mu0x @ 0x2620 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpcdw6mu0x @ 0x2630 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmpcdw6mu0x @ 0x2640 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmpcdw6mu0x @ 0x2650 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpcdw6mu0x @ 0x2690 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpcdw6mu0x @ 0x26a0 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpcdw6mu0x @ 0x26b0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpcdw6mu0x @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 848 named .plt */
    __asm ("bnd jmp qword [0x0000be18]");
}

/* /tmp/tmpcdw6mu0x @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpcdw6mu0x @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}
