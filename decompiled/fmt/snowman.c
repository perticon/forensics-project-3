
void fadvise();

signed char tabs = 0;

void* other_indent = reinterpret_cast<void*>(0);

uint32_t get_prefix(void** rdi, void** rsi);

uint32_t next_char = 0;

void* last_line_length = reinterpret_cast<void*>(0);

void* next_prefix_indent = reinterpret_cast<void*>(0);

void* in_column = reinterpret_cast<void*>(0);

void* out_column = reinterpret_cast<void*>(0);

void put_space(int64_t rdi, void** rsi);

void** prefix = reinterpret_cast<void**>(0);

void** stdout = reinterpret_cast<void**>(0);

void fun_24b0();

void* prefix_lead_space = reinterpret_cast<void*>(0);

void* prefix_full_length = reinterpret_cast<void*>(0);

void* prefix_indent = reinterpret_cast<void*>(0);

void* first_indent = reinterpret_cast<void*>(0);

void** wptr = reinterpret_cast<void**>(0);

void** word_limit = reinterpret_cast<void**>(0);

uint32_t get_line(void** rdi, void** rsi, void** rdx, void** rcx);

void set_other_indent(signed char dil, ...);

signed char split = 0;

signed char crown = 0;

signed char tagged = 0;

void fun_24e0(int64_t rdi, void** rsi, ...);

uint32_t fun_2380(void** rdi, ...);

void fmt_paragraph(void** rdi);

void put_line(void** rdi, void** rsi, void** rdx, void** rcx);

void** gc140 = reinterpret_cast<void**>(0);

void* prefix_length = reinterpret_cast<void*>(0);

void** stdin = reinterpret_cast<void**>(0);

void fun_2400(void** rdi, void** rsi);

int64_t rpl_fclose(void** rdi, void** rsi, ...);

int64_t quotearg_n_style_colon();

int32_t* fun_23b0();

void** fun_2440();

void fun_2610();

uint32_t fmt(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r14_6;
    void** rsi7;
    uint32_t eax8;
    uint32_t ebp9;
    int64_t rdi10;
    void* eax11;
    void* r13d12;
    void* eax13;
    void* r8d14;
    void** r15_15;
    void** rdi16;
    void** rax17;
    void* eax18;
    int1_t less_or_equal19;
    void* edx20;
    void** rsi21;
    uint32_t eax22;
    void* r8d23;
    void** rdi24;
    void** rsi25;
    void* eax26;
    void* eax27;
    int1_t less_or_equal28;
    uint32_t edi29;
    int1_t zf30;
    int1_t zf31;
    int1_t zf32;
    void* eax33;
    void* tmp32_34;
    int1_t zf35;
    uint32_t eax36;
    int1_t zf37;
    void* eax38;
    void* tmp32_39;
    int1_t zf40;
    uint32_t eax41;
    void* eax42;
    int1_t zf43;
    void* tmp32_44;
    int1_t zf45;
    void* tmp32_46;
    int1_t less_or_equal47;
    uint32_t eax48;
    void* eax49;
    int1_t zf50;
    void* tmp32_51;
    int1_t zf52;
    void** r15_53;
    int64_t rdi54;
    void** rdi55;
    void** rax56;
    void** rax57;
    int32_t eax58;
    uint32_t eax59;
    void** rdi60;
    void** rax61;
    uint32_t eax62;
    void** rbp63;
    void* eax64;
    void* tmp32_65;
    int1_t less_or_equal66;
    void** rdi67;
    void** rax68;
    void** rax69;
    int64_t rax70;
    int32_t* rax71;
    int32_t ebp72;
    uint32_t eax73;

    r14_6 = rdi;
    *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(2);
    *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
    fadvise();
    tabs = 0;
    other_indent = reinterpret_cast<void*>(0);
    eax8 = get_prefix(r14_6, 2);
    next_char = eax8;
    ebp9 = eax8;
    while (1) {
        last_line_length = reinterpret_cast<void*>(0);
        while (1) {
            *reinterpret_cast<void**>(&rdi10) = next_prefix_indent;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            eax11 = in_column;
            if (ebp9 == 10 || ebp9 == 0xffffffff) {
                out_column = reinterpret_cast<void*>(0);
                *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(ebp9 != 10);
                *reinterpret_cast<unsigned char*>(&rcx) = reinterpret_cast<uint1_t>(ebp9 != 0xffffffff);
                *reinterpret_cast<void**>(&rdx) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rdx)) & reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)));
                r13d12 = *reinterpret_cast<void**>(&rdx);
                if (reinterpret_cast<int32_t>(eax11) > reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdi10))) {
                    addr_37ee_5:
                    put_space(rdi10, rsi7);
                    eax13 = out_column;
                    r8d14 = in_column;
                    r15_15 = prefix;
                    if (r8d14 != eax13) {
                        do {
                            addr_382f_6:
                            *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_15))));
                            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                            if (!rsi7) 
                                break;
                            rdi16 = stdout;
                            ++r15_15;
                            rax17 = *reinterpret_cast<void***>(rdi16 + 40);
                            if (reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi16 + 48))) {
                                rcx = rax17 + 1;
                                *reinterpret_cast<void***>(rdi16 + 40) = rcx;
                                *reinterpret_cast<void***>(rax17) = rsi7;
                            } else {
                                fun_24b0();
                                r8d14 = in_column;
                            }
                            eax18 = out_column;
                            eax13 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax18) + 1);
                            out_column = eax13;
                        } while (eax13 != r8d14);
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rdx)) {
                        addr_3d7c_13:
                        put_space(rdi10, rsi7);
                        eax13 = out_column;
                        r8d14 = in_column;
                        r15_15 = prefix;
                        if (eax13 != r8d14) {
                            addr_398a_14:
                            r13d12 = reinterpret_cast<void*>(1);
                            goto addr_382f_6;
                        } else {
                            put_space(0, rsi7);
                            if (ebp9 != 0xffffffff) 
                                goto addr_39d1_16; else 
                                goto addr_3dae_17;
                        }
                    } else {
                        addr_392b_18:
                        if (ebp9 == 0xffffffff) 
                            goto addr_388a_19; else 
                            goto addr_3934_20;
                    }
                }
            } else {
                less_or_equal19 = reinterpret_cast<int32_t>(prefix_lead_space) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdi10));
                if (less_or_equal19 && (edx20 = prefix_full_length, *reinterpret_cast<void**>(&rdx) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(edx20) + reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rdi10))), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx)) <= reinterpret_cast<int32_t>(eax11))) {
                    prefix_indent = *reinterpret_cast<void**>(&rdi10);
                    *reinterpret_cast<uint32_t*>(&rsi21) = ebp9;
                    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
                    first_indent = eax11;
                    wptr = reinterpret_cast<void**>(0x15d80);
                    word_limit = reinterpret_cast<void**>(0xc120);
                    eax22 = get_line(r14_6, rsi21, rdx, rcx);
                    *reinterpret_cast<void**>(&rcx) = next_prefix_indent;
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    r8d23 = prefix_indent;
                    *reinterpret_cast<uint32_t*>(&rdi24) = 0;
                    *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi25) = eax22;
                    *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                    if (*reinterpret_cast<void**>(&rcx) == r8d23 && (eax26 = prefix_full_length, eax27 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax26) + reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx))), less_or_equal28 = reinterpret_cast<int32_t>(eax27) <= reinterpret_cast<int32_t>(in_column), less_or_equal28)) {
                        *reinterpret_cast<unsigned char*>(&eax27) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rsi25) != 10);
                        edi29 = 0;
                        *reinterpret_cast<unsigned char*>(&edi29) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff);
                        *reinterpret_cast<uint32_t*>(&rdi24) = edi29 & reinterpret_cast<uint32_t>(eax27);
                        *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
                    }
                    set_other_indent(*reinterpret_cast<signed char*>(&rdi24));
                    zf30 = split == 0;
                    if (zf30) {
                        zf31 = crown == 0;
                        if (zf31) {
                            zf32 = tagged == 0;
                            if (zf32) {
                                if (*reinterpret_cast<void**>(&rcx) == r8d23) {
                                    while ((eax33 = in_column, tmp32_34 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)) + reinterpret_cast<uint32_t>(prefix_full_length)), *reinterpret_cast<void**>(&rcx) = tmp32_34, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rcx)) <= reinterpret_cast<int32_t>(eax33)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff && ((zf35 = other_indent == eax33, zf35) && (rdi24 = r14_6, eax36 = get_line(rdi24, rsi25, rdx, rcx), *reinterpret_cast<void**>(&rcx) = next_prefix_indent, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, zf37 = *reinterpret_cast<void**>(&rcx) == prefix_indent, *reinterpret_cast<uint32_t*>(&rsi25) = eax36, *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, zf37))))) {
                                    }
                                }
                            } else {
                                if (*reinterpret_cast<void**>(&rcx) == r8d23 && ((eax38 = in_column, tmp32_39 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)) + reinterpret_cast<uint32_t>(prefix_full_length)), *reinterpret_cast<void**>(&rcx) = tmp32_39, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rcx)) <= reinterpret_cast<int32_t>(eax38)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && *reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff))) {
                                    zf40 = first_indent == eax38;
                                    if (!zf40) {
                                        do {
                                            rdi24 = r14_6;
                                            eax41 = get_line(rdi24, rsi25, rdx, rcx);
                                            *reinterpret_cast<uint32_t*>(&rsi25) = eax41;
                                            *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                            eax42 = next_prefix_indent;
                                            zf43 = eax42 == prefix_indent;
                                            if (!zf43) 
                                                break;
                                            *reinterpret_cast<void**>(&rdx) = in_column;
                                            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                            tmp32_44 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax42) + reinterpret_cast<uint32_t>(prefix_full_length));
                                        } while (reinterpret_cast<int32_t>(tmp32_44) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff && (zf45 = other_indent == *reinterpret_cast<void**>(&rdx), zf45))));
                                    }
                                }
                            }
                        } else {
                            if (*reinterpret_cast<void**>(&rcx) == r8d23 && ((tmp32_46 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rcx)) + reinterpret_cast<uint32_t>(prefix_full_length)), *reinterpret_cast<void**>(&rcx) = tmp32_46, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, less_or_equal47 = reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rcx)) <= reinterpret_cast<int32_t>(in_column), less_or_equal47) && *reinterpret_cast<uint32_t*>(&rsi25) != 10)) {
                                if (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff) {
                                    do {
                                        rdi24 = r14_6;
                                        eax48 = get_line(rdi24, rsi25, rdx, rcx);
                                        *reinterpret_cast<uint32_t*>(&rsi25) = eax48;
                                        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                        eax49 = next_prefix_indent;
                                        zf50 = eax49 == prefix_indent;
                                        if (!zf50) 
                                            break;
                                        *reinterpret_cast<void**>(&rdx) = in_column;
                                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                        tmp32_51 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax49) + reinterpret_cast<uint32_t>(prefix_full_length));
                                    } while (reinterpret_cast<int32_t>(tmp32_51) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx)) && (*reinterpret_cast<uint32_t*>(&rsi25) != 10 && (*reinterpret_cast<uint32_t*>(&rsi25) != 0xffffffff && (zf52 = other_indent == *reinterpret_cast<void**>(&rdx), zf52))));
                                }
                            }
                        }
                    }
                    r15_53 = word_limit;
                    if (reinterpret_cast<unsigned char>(r15_53) > reinterpret_cast<unsigned char>(0xc120)) 
                        break;
                    rcx = reinterpret_cast<void**>("get_paragraph");
                    *reinterpret_cast<void**>(&rdx) = reinterpret_cast<void*>(0x270);
                    rsi7 = reinterpret_cast<void**>("src/fmt.c");
                    rdi10 = reinterpret_cast<int64_t>("word < word_limit");
                    fun_24e0("word < word_limit", "src/fmt.c");
                    goto addr_3d7c_13;
                }
                out_column = reinterpret_cast<void*>(0);
                if (reinterpret_cast<int32_t>(eax11) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdi10))) 
                    goto addr_3968_45; else 
                    goto addr_37e8_46;
            }
            if (*reinterpret_cast<signed char*>(&r13d12)) {
                *reinterpret_cast<void**>(&rdi54) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(r8d14) - reinterpret_cast<uint32_t>(eax13));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
                put_space(rdi54, rsi7);
                if (ebp9 == 0xffffffff) 
                    goto addr_39a7_49;
            } else {
                if (ebp9 != 0xffffffff) 
                    goto addr_3934_20; else 
                    goto addr_3872_51;
            }
            do {
                addr_39d1_16:
                rdi55 = stdout;
                rax56 = *reinterpret_cast<void***>(rdi55 + 40);
                if (reinterpret_cast<unsigned char>(rax56) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi55 + 48))) {
                    *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebp9))));
                    *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                    fun_24b0();
                } else {
                    rdx = rax56 + 1;
                    *reinterpret_cast<void***>(rdi55 + 40) = rdx;
                    *reinterpret_cast<void***>(rax56) = *reinterpret_cast<void***>(&ebp9);
                }
                rax57 = *reinterpret_cast<void***>(r14_6 + 8);
                if (reinterpret_cast<unsigned char>(rax57) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_6 + 16))) {
                    rdx = rax57 + 1;
                    *reinterpret_cast<void***>(r14_6 + 8) = rdx;
                    ebp9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax57));
                    eax58 = 1;
                } else {
                    eax59 = fun_2380(r14_6, r14_6);
                    ebp9 = eax59;
                    *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<uint1_t>(eax59 != 0xffffffff);
                }
            } while (ebp9 != 10 && *reinterpret_cast<unsigned char*>(&eax58));
            goto addr_392b_18;
            addr_3934_20:
            rdi60 = stdout;
            rax61 = *reinterpret_cast<void***>(rdi60 + 40);
            if (reinterpret_cast<unsigned char>(rax61) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi60 + 48))) {
                *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(10);
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                fun_24b0();
            } else {
                rdx = rax61 + 1;
                *reinterpret_cast<void***>(rdi60 + 40) = rdx;
                *reinterpret_cast<void***>(rax61) = reinterpret_cast<void**>(10);
            }
            eax62 = get_prefix(r14_6, rsi7);
            ebp9 = eax62;
            continue;
            addr_3968_45:
            put_space(rdi10, rsi7);
            eax13 = out_column;
            r8d14 = in_column;
            r15_15 = prefix;
            if (eax13 != r8d14) 
                goto addr_398a_14;
            put_space(0, rsi7);
            goto addr_39d1_16;
            addr_37e8_46:
            r13d12 = reinterpret_cast<void*>(1);
            goto addr_37ee_5;
        }
        *reinterpret_cast<unsigned char*>(r15_53 + 0xffffffffffffffe8) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r15_53 + 0xffffffffffffffe8) | 10);
        next_char = *reinterpret_cast<uint32_t*>(&rsi25);
        fmt_paragraph(rdi24);
        *reinterpret_cast<void**>(&rsi7) = first_indent;
        *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
        put_line(0xc120, rsi7, rdx, rcx);
        rbp63 = gc140;
        if (rbp63 != r15_53) {
            do {
                *reinterpret_cast<void**>(&rsi7) = other_indent;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                put_line(rbp63, rsi7, rdx, rcx);
                rbp63 = *reinterpret_cast<void***>(rbp63 + 32);
            } while (rbp63 != r15_53);
        }
        ebp9 = next_char;
    }
    addr_39a7_49:
    addr_3872_51:
    eax64 = prefix_length;
    tmp32_65 = reinterpret_cast<void*>(reinterpret_cast<int32_t>(eax64) + reinterpret_cast<uint32_t>(next_prefix_indent));
    less_or_equal66 = reinterpret_cast<int32_t>(tmp32_65) <= reinterpret_cast<int32_t>(in_column);
    if (less_or_equal66) {
        rdi67 = stdout;
        rax68 = *reinterpret_cast<void***>(rdi67 + 40);
        if (reinterpret_cast<unsigned char>(rax68) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi67 + 48))) {
            *reinterpret_cast<void**>(&rsi7) = reinterpret_cast<void*>(10);
            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
            fun_24b0();
        } else {
            *reinterpret_cast<void***>(rdi67 + 40) = rax68 + 1;
            *reinterpret_cast<void***>(rax68) = reinterpret_cast<void**>(10);
        }
    }
    addr_388a_19:
    next_char = 0xffffffff;
    rax69 = stdin;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_6)) & 32) {
        if (r14_6 == rax69) {
            fun_2400(r14_6, rsi7);
        } else {
            rpl_fclose(r14_6, rsi7);
        }
        quotearg_n_style_colon();
        goto addr_38c8_73;
    }
    if (r14_6 == rax69) {
        fun_2400(r14_6, rsi7);
        return 1;
    }
    rax70 = rpl_fclose(r14_6, rsi7);
    if (!*reinterpret_cast<int32_t*>(&rax70)) {
        return 1;
    }
    rax71 = fun_23b0();
    ebp72 = *rax71;
    eax73 = 1;
    if (ebp72 >= 0) 
        goto addr_3d37_79;
    addr_38f0_80:
    return eax73;
    addr_3d37_79:
    quotearg_n_style_colon();
    if (!ebp72) {
        addr_38c8_73:
        fun_2440();
    }
    fun_2610();
    eax73 = 0;
    goto addr_38f0_80;
    addr_3dae_17:
    goto addr_3872_51;
}

uint32_t get_space(void** rdi, uint32_t esi) {
    uint32_t eax3;
    void** rbx4;
    void* eax5;
    void** rax6;
    int64_t rdx7;
    void* eax8;
    int64_t rax9;

    eax3 = esi;
    rbx4 = rdi;
    while (1) {
        if (eax3 == 32) {
            eax5 = in_column;
            in_column = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax5) + 1);
            rax6 = *reinterpret_cast<void***>(rbx4 + 8);
            if (reinterpret_cast<unsigned char>(rax6) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4 + 16))) 
                goto addr_2f34_4; else 
                goto addr_2f5d_5;
        }
        if (eax3 != 9) 
            break;
        *reinterpret_cast<void**>(&rdx7) = in_column;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
        tabs = 1;
        eax8 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx7 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx7)) < reinterpret_cast<int32_t>(0)) 
            goto addr_2f1a_8;
        eax8 = *reinterpret_cast<void**>(&rdx7);
        addr_2f1a_8:
        *reinterpret_cast<int32_t*>(&rax9) = reinterpret_cast<int32_t>(eax8) >> 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        in_column = reinterpret_cast<void*>(static_cast<uint32_t>(rax9 * 8 + 8));
        rax6 = *reinterpret_cast<void***>(rbx4 + 8);
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4 + 16))) {
            addr_2f5d_5:
            eax3 = fun_2380(rbx4);
        } else {
            addr_2f34_4:
            *reinterpret_cast<void***>(rbx4 + 8) = rax6 + 1;
            eax3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax6));
        }
    }
    return eax3;
}

void put_word(void** rdi, void** rsi) {
    void* rbp3;
    void** rbx4;
    void** r12_5;
    void** rbp6;
    void** rdi7;
    uint32_t esi8;
    void** rax9;
    void* tmp32_10;

    *reinterpret_cast<void***>(&rbp3) = *reinterpret_cast<void***>(rdi + 8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    rbx4 = *reinterpret_cast<void***>(rdi);
    if (*reinterpret_cast<void***>(&rbp3)) {
        r12_5 = rdi;
        rbp6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp3) + reinterpret_cast<unsigned char>(rbx4));
        do {
            rdi7 = stdout;
            ++rbx4;
            esi8 = *reinterpret_cast<unsigned char*>(rbx4 + 0xffffffffffffffff);
            rax9 = *reinterpret_cast<void***>(rdi7 + 40);
            if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi7 + 48))) {
                fun_24b0();
            } else {
                *reinterpret_cast<void***>(rdi7 + 40) = rax9 + 1;
                *reinterpret_cast<void***>(rax9) = *reinterpret_cast<void***>(&esi8);
            }
        } while (rbx4 != rbp6);
        *reinterpret_cast<void***>(&rbp3) = *reinterpret_cast<void***>(r12_5 + 8);
    }
    tmp32_10 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(out_column) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbp3)));
    out_column = tmp32_10;
    return;
}

void put_space(int64_t rdi, void** rsi) {
    int64_t rdx3;
    int1_t zf4;
    int64_t rbx5;
    void* eax6;
    void* eax7;
    int32_t ebp8;
    void** rdi9;
    void** rax10;
    int64_t rdx11;
    void* eax12;
    int64_t rax13;
    void** rdi14;
    void** rax15;
    void* eax16;
    void* eax17;

    *reinterpret_cast<void**>(&rdx3) = out_column;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    zf4 = tabs == 0;
    *reinterpret_cast<void**>(&rbx5) = reinterpret_cast<void*>(static_cast<uint32_t>(rdx3 + rdi));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
    if (!zf4) {
        eax6 = reinterpret_cast<void*>(static_cast<uint32_t>(rbx5 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)) >= reinterpret_cast<int32_t>(0)) {
            eax6 = *reinterpret_cast<void**>(&rbx5);
        }
        eax7 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax6) & 0xfffffff8);
        ebp8 = reinterpret_cast<int32_t>(eax6) >> 3;
        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(rdx3 + 1)) < reinterpret_cast<int32_t>(eax7) && reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx3)) < reinterpret_cast<int32_t>(eax7)) {
            do {
                rdi9 = stdout;
                rax10 = *reinterpret_cast<void***>(rdi9 + 40);
                if (reinterpret_cast<unsigned char>(rax10) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi9 + 48))) {
                    fun_24b0();
                } else {
                    *reinterpret_cast<void***>(rdi9 + 40) = rax10 + 1;
                    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(9);
                }
                *reinterpret_cast<void**>(&rdx11) = out_column;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
                eax12 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx11 + 7));
                if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx11)) >= reinterpret_cast<int32_t>(0)) {
                    eax12 = *reinterpret_cast<void**>(&rdx11);
                }
                *reinterpret_cast<int32_t*>(&rax13) = (reinterpret_cast<int32_t>(eax12) >> 3) + 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
                *reinterpret_cast<void**>(&rdx3) = reinterpret_cast<void*>(static_cast<uint32_t>(rax13 * 8));
                out_column = *reinterpret_cast<void**>(&rdx3);
            } while (ebp8 > *reinterpret_cast<int32_t*>(&rax13));
        }
    }
    if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)) > reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx3))) {
        do {
            rdi14 = stdout;
            rax15 = *reinterpret_cast<void***>(rdi14 + 40);
            if (reinterpret_cast<unsigned char>(rax15) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi14 + 48))) {
                fun_24b0();
            } else {
                *reinterpret_cast<void***>(rdi14 + 40) = rax15 + 1;
                *reinterpret_cast<void***>(rax15) = reinterpret_cast<void**>(32);
            }
            eax16 = out_column;
            eax17 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax16) + 1);
            out_column = eax17;
        } while (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)));
    }
    return;
}

void** max_width = reinterpret_cast<void**>(0);

struct s0 {
    signed char[10] pad10;
    void** fa;
};

struct s0* goal_width = reinterpret_cast<struct s0*>(0);

void fmt_paragraph(void** rdi) {
    void** rsi2;
    void** r9d3;
    void** rcx4;
    void** eax5;
    void** v6;
    void* r13d7;
    void* r12d8;
    struct s0* r10d9;
    void* r11d10;
    void* r15d11;
    void** ebx12;
    void** rdx13;
    void* rdi14;
    void** r15d15;
    void* rax16;
    int64_t rax17;
    int32_t eax18;
    int64_t rax19;
    int64_t r14_20;
    int32_t r14d21;
    int64_t r14_22;
    void* rax23;
    int64_t r14_24;
    int32_t r14d25;
    int64_t r14_26;
    uint64_t r14_27;
    uint32_t eax28;
    int64_t rax29;
    uint32_t eax30;
    uint64_t r14_31;
    int64_t r14_32;
    uint32_t eax33;

    rsi2 = word_limit;
    r9d3 = max_width;
    rcx4 = rsi2 + 0xffffffffffffffd8;
    eax5 = *reinterpret_cast<void***>(rsi2 + 8);
    *reinterpret_cast<void**>(rsi2 + 24) = reinterpret_cast<void*>(0);
    *reinterpret_cast<void***>(rsi2 + 8) = r9d3;
    v6 = eax5;
    if (reinterpret_cast<unsigned char>(rcx4) >= reinterpret_cast<unsigned char>(0xc120)) {
        r13d7 = other_indent;
        r12d8 = first_indent;
        r10d9 = goal_width;
        r11d10 = last_line_length;
        do {
            r15d11 = r13d7;
            ebx12 = *reinterpret_cast<void***>(rcx4 + 8);
            rdx13 = rcx4;
            if (rcx4 == 0xc120) {
                r15d11 = r12d8;
            }
            rdi14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            r15d15 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(r15d11) + reinterpret_cast<unsigned char>(ebx12));
            do {
                rdx13 = rdx13 + 40;
                if (rsi2 == rdx13) {
                    *reinterpret_cast<int32_t*>(&rax16) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                } else {
                    *reinterpret_cast<void**>(&rax17) = reinterpret_cast<void*>(reinterpret_cast<int32_t>(r10d9) - reinterpret_cast<unsigned char>(r15d15));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    eax18 = static_cast<int32_t>(rax17 + rax17 * 4);
                    rax19 = eax18 + eax18;
                    rax16 = reinterpret_cast<void*>(rax19 * rax19);
                    if (rsi2 != *reinterpret_cast<void***>(rdx13 + 32)) {
                        *reinterpret_cast<void**>(&r14_20) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15d15) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx13 + 20)));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_20) + 4) = 0;
                        r14d21 = static_cast<int32_t>(r14_20 + r14_20 * 4);
                        r14_22 = r14d21 + r14d21;
                        rax16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax16) + (r14_22 * r14_22 >> 1));
                    }
                }
                rax23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax16) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rdx13 + 24)));
                if (reinterpret_cast<int1_t>(rcx4 == 0xc120) && !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r11d10) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(r11d10 == 0))) {
                    *reinterpret_cast<void**>(&r14_24) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15d15) - reinterpret_cast<uint32_t>(r11d10));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_24) + 4) = 0;
                    r14d25 = static_cast<int32_t>(r14_24 + r14_24 * 4);
                    r14_26 = r14d25 + r14d25;
                    rax23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax23) + (r14_26 * r14_26 >> 1));
                }
                if (reinterpret_cast<int64_t>(rax23) < reinterpret_cast<int64_t>(rdi14)) {
                    *reinterpret_cast<void***>(rcx4 + 32) = rdx13;
                    rdi14 = rax23;
                    *reinterpret_cast<void***>(rcx4 + 20) = r15d15;
                }
            } while (rsi2 != rdx13 && (r15d15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15d15) + (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx13 + 8)) + reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(rdx13 + 0xffffffffffffffe4)))), reinterpret_cast<signed char>(r9d3) > reinterpret_cast<signed char>(r15d15)));
            *reinterpret_cast<int32_t*>(&r14_27) = 0x1324;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_27) + 4) = 0;
            if (reinterpret_cast<unsigned char>(rcx4) > reinterpret_cast<unsigned char>(0xc120)) {
                eax28 = *reinterpret_cast<unsigned char*>(rcx4 + 0xffffffffffffffe8);
                if (!(*reinterpret_cast<unsigned char*>(&eax28) & 2)) {
                    *reinterpret_cast<int32_t*>(&r14_27) = reinterpret_cast<int32_t>(".so.6");
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_27) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(&eax28) & 4) && ((*reinterpret_cast<int32_t*>(&r14_27) = 0x1324, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_27) + 4) = 0, reinterpret_cast<unsigned char>(rcx4) > reinterpret_cast<unsigned char>(0xc148)) && *reinterpret_cast<unsigned char*>(rcx4 + 0xffffffffffffffc0) & 8)) {
                        *reinterpret_cast<int32_t*>(&rax29) = *reinterpret_cast<int32_t*>(rcx4 + 0xffffffffffffffe0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
                        r14_27 = 0x9c40 / static_cast<int32_t>(rax29 + 2) + 0x1324;
                    }
                } else {
                    eax30 = eax28 & 8;
                    r14_31 = 0x1324 - (0x1324 + reinterpret_cast<uint1_t>(0x1324 < 0x1324 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&eax30) < 1)));
                    *reinterpret_cast<uint32_t*>(&r14_32) = *reinterpret_cast<uint32_t*>(&r14_31) & 0x58804;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_32) + 4) = 0;
                    r14_27 = reinterpret_cast<uint64_t>(r14_32 + 0x960);
                }
            }
            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx4 + 16));
            if (!(*reinterpret_cast<unsigned char*>(&eax33) & 1)) {
                if (*reinterpret_cast<unsigned char*>(&eax33) & 8) {
                    r14_27 = r14_27 + 0x57e4 / reinterpret_cast<int32_t>(ebx12 + 2);
                }
            } else {
                r14_27 = r14_27 - 0x640;
            }
            rcx4 = rcx4 - 40;
            *reinterpret_cast<uint64_t*>(rcx4 + 64) = reinterpret_cast<uint64_t>(rdi14) + r14_27;
        } while (reinterpret_cast<unsigned char>(rcx4) >= reinterpret_cast<unsigned char>(0xc120));
    }
    *reinterpret_cast<void***>(rsi2 + 8) = v6;
    return;
}

void fun_2520(void** rdi, void** rsi, void** rdx, void** rcx);

void put_line(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    int64_t rdi6;
    void** rsi7;
    void** rdi8;
    void* eax9;
    void* tmp32_10;
    int64_t rdi11;
    void** rbx12;
    void** rdi13;
    int64_t rdi14;
    void* eax15;
    void** rdi16;
    void** rax17;

    rbp5 = rdi;
    *reinterpret_cast<void**>(&rdi6) = prefix_indent;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
    out_column = reinterpret_cast<void*>(0);
    put_space(rdi6, rsi);
    rsi7 = stdout;
    rdi8 = prefix;
    fun_2520(rdi8, rsi7, rdx, rcx);
    eax9 = prefix_length;
    tmp32_10 = reinterpret_cast<void*>(reinterpret_cast<int32_t>(eax9) + reinterpret_cast<uint32_t>(out_column));
    out_column = tmp32_10;
    *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<int32_t*>(&rsi) - reinterpret_cast<uint32_t>(tmp32_10);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
    put_space(rdi11, rsi7);
    rbx12 = *reinterpret_cast<void***>(rbp5 + 32) + 0xffffffffffffffd8;
    if (rbp5 != rbx12) {
        do {
            rdi13 = rbp5;
            rbp5 = rbp5 + 40;
            put_word(rdi13, rsi7);
            *reinterpret_cast<void**>(&rdi14) = *reinterpret_cast<void**>(rbp5 + 0xffffffffffffffe4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
            put_space(rdi14, rsi7);
        } while (rbx12 != rbp5);
    }
    put_word(rbp5, rsi7);
    eax15 = out_column;
    rdi16 = stdout;
    last_line_length = eax15;
    rax17 = *reinterpret_cast<void***>(rdi16 + 40);
    if (reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi16 + 48))) {
        *reinterpret_cast<void***>(rdi16 + 40) = rax17 + 1;
        *reinterpret_cast<void***>(rax17) = reinterpret_cast<void**>(10);
        return;
    }
}

void set_other_indent(signed char dil, ...) {
    int1_t zf2;
    int1_t zf3;
    int1_t zf4;
    void* eax5;
    void* edx6;
    int1_t zf7;
    void* eax8;
    void* eax9;

    zf2 = split == 0;
    if (zf2) {
        zf3 = crown == 0;
        if (zf3) {
            zf4 = tagged == 0;
            eax5 = first_indent;
            if (zf4) {
                other_indent = eax5;
            } else {
                if (!dil || (edx6 = in_column, eax5 == edx6)) {
                    zf7 = eax5 == other_indent;
                    if (zf7) {
                        other_indent = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax5) - (reinterpret_cast<uint32_t>(eax5) + reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(eax5) < reinterpret_cast<uint32_t>(eax5) + reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(eax5) < 1))) & 3);
                        return;
                    }
                } else {
                    other_indent = edx6;
                    return;
                }
            }
            return;
        } else {
            eax8 = in_column;
            if (!dil) {
                eax8 = first_indent;
            }
            other_indent = eax8;
            return;
        }
    } else {
        eax9 = first_indent;
        other_indent = eax9;
        return;
    }
}

uint64_t fun_25d0(int64_t rdi, int64_t rsi);

uint64_t fun_2560(int64_t rdi, void** rsi);

uint64_t flush_paragraph(void** rdi, void** rsi) {
    void** r12_3;
    uint64_t rax4;
    void** rdx5;
    void** rbx6;
    void* rsi7;
    void** rax8;
    void** rdi9;
    void* rcx10;
    void** rcx11;
    void** rcx12;
    void** rsi13;
    void** r12_14;
    void** rsi15;
    void** r12_16;
    void** rsi17;
    uint64_t rax18;
    void** rdx19;
    uint64_t rax20;
    uint64_t rax21;
    void** rax22;
    void** r12_23;
    void* rcx24;
    uint64_t rax25;

    r12_3 = word_limit;
    if (r12_3 == 0xc120) {
        rax4 = fun_25d0(0x15d80, 1);
        wptr = reinterpret_cast<void**>(0x15d80);
        return rax4;
    } else {
        fmt_paragraph(rdi);
        rdx5 = gc140;
        if (r12_3 == rdx5) {
            rbx6 = r12_3;
        } else {
            rsi7 = *reinterpret_cast<void**>(rdx5 + 24);
            rbx6 = r12_3;
            rax8 = reinterpret_cast<void**>(0x7fffffffffffffff);
            do {
                rdi9 = rdx5;
                rdx5 = *reinterpret_cast<void***>(rdx5 + 32);
                rcx10 = rsi7;
                rsi7 = *reinterpret_cast<void**>(rdx5 + 24);
                rcx11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx10) - reinterpret_cast<uint64_t>(rsi7));
                if (reinterpret_cast<signed char>(rcx11) < reinterpret_cast<signed char>(rax8)) {
                    rax8 = rcx11;
                    rbx6 = rdi9;
                }
                rcx12 = rax8 + 9;
                if (reinterpret_cast<signed char>(rax8) <= reinterpret_cast<signed char>(0x7ffffffffffffff6)) {
                    rax8 = rcx12;
                }
            } while (r12_3 != rdx5);
        }
        *reinterpret_cast<void**>(&rsi13) = first_indent;
        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
        put_line(0xc120, rsi13, rdx5, rcx12);
        r12_14 = gc140;
        if (rbx6 != r12_14) {
            do {
                *reinterpret_cast<void**>(&rsi15) = other_indent;
                *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
                put_line(r12_14, rsi15, rdx5, rcx12);
                r12_14 = *reinterpret_cast<void***>(r12_14 + 32);
            } while (rbx6 != r12_14);
        }
        r12_16 = wptr;
        rsi17 = *reinterpret_cast<void***>(rbx6);
        rax18 = fun_2560(0x15d80, rsi17);
        rdx19 = *reinterpret_cast<void***>(rbx6);
        rax20 = reinterpret_cast<unsigned char>(rdx19) - rax18;
        rax21 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax20)));
        rax22 = rbx6;
        wptr = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_16) - rax21);
        r12_23 = word_limit;
        rcx24 = reinterpret_cast<void*>(-rax21);
        if (reinterpret_cast<unsigned char>(r12_23) >= reinterpret_cast<unsigned char>(rbx6)) {
            while (rax22 = rax22 + 40, *reinterpret_cast<void***>(rax22 + 0xffffffffffffffd8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx19) + reinterpret_cast<uint64_t>(rcx24)), reinterpret_cast<unsigned char>(r12_23) >= reinterpret_cast<unsigned char>(rax22)) {
                rdx19 = *reinterpret_cast<void***>(rax22);
            }
        }
        rax25 = fun_2560(0xc120, rbx6);
        word_limit = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_23) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(0xc120)));
        return rax25;
    }
}

uint32_t get_prefix(void** rdi, void** rsi) {
    void** rbp3;
    void** rax4;
    uint32_t eax5;
    uint32_t esi6;
    uint32_t eax7;
    void* ecx8;
    void* edx9;
    void** rbx10;
    uint32_t edx11;
    void* tmp32_12;
    void** rax13;
    void* ecx14;
    uint32_t eax15;
    void** rbx16;
    void* eax17;
    void** rax18;
    int64_t rdx19;
    void* eax20;
    int64_t rax21;

    rbp3 = rdi;
    rax4 = *reinterpret_cast<void***>(rdi + 8);
    in_column = reinterpret_cast<void*>(0);
    if (reinterpret_cast<unsigned char>(rax4) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 16))) {
        eax5 = fun_2380(rdi);
        esi6 = eax5;
    } else {
        *reinterpret_cast<void***>(rdi + 8) = rax4 + 1;
        esi6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4));
    }
    eax7 = get_space(rbp3, esi6);
    ecx8 = prefix_length;
    edx9 = in_column;
    if (ecx8) {
        rbx10 = prefix;
        next_prefix_indent = edx9;
        edx11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10));
        if (*reinterpret_cast<signed char*>(&edx11)) {
            do {
                if (edx11 != eax7) 
                    break;
                tmp32_12 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(in_column) + 1);
                in_column = tmp32_12;
                rax13 = *reinterpret_cast<void***>(rbp3 + 8);
                if (reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3 + 16))) {
                    *reinterpret_cast<void***>(rbp3 + 8) = rax13 + 1;
                    eax7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13));
                } else {
                    eax7 = fun_2380(rbp3, rbp3);
                }
                edx11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10 + 1));
                ++rbx10;
            } while (*reinterpret_cast<signed char*>(&edx11));
            goto addr_2ff4_11;
        } else {
            addr_2ff4_11:
            goto addr_2ef0_12;
        }
    } else {
        ecx14 = prefix_lead_space;
        if (reinterpret_cast<int32_t>(edx9) > reinterpret_cast<int32_t>(ecx14)) {
            edx9 = ecx14;
        }
        next_prefix_indent = edx9;
    }
    return eax7;
    addr_2ef0_12:
    eax15 = eax7;
    rbx16 = rbp3;
    while (1) {
        if (eax15 == 32) {
            eax17 = in_column;
            in_column = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax17) + 1);
            rax18 = *reinterpret_cast<void***>(rbx16 + 8);
            if (reinterpret_cast<unsigned char>(rax18) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx16 + 16))) 
                goto addr_2f34_19; else 
                goto addr_2f5d_20;
        }
        if (eax15 != 9) 
            break;
        *reinterpret_cast<void**>(&rdx19) = in_column;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        tabs = 1;
        eax20 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx19 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx19)) < reinterpret_cast<int32_t>(0)) 
            goto addr_2f1a_23;
        eax20 = *reinterpret_cast<void**>(&rdx19);
        addr_2f1a_23:
        *reinterpret_cast<int32_t*>(&rax21) = reinterpret_cast<int32_t>(eax20) >> 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        in_column = reinterpret_cast<void*>(static_cast<uint32_t>(rax21 * 8 + 8));
        rax18 = *reinterpret_cast<void***>(rbx16 + 8);
        if (reinterpret_cast<unsigned char>(rax18) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx16 + 16))) {
            addr_2f5d_20:
            eax15 = fun_2380(rbx16);
        } else {
            addr_2f34_19:
            *reinterpret_cast<void***>(rbx16 + 8) = rax18 + 1;
            eax15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax18));
        }
    }
    return eax15;
}

unsigned char** fun_26b0(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_24a0(void** rdi, ...);

signed char uniform = 0;

uint32_t get_line(void** rdi, void** rsi, void** rdx, void** rcx) {
    uint32_t ebp5;
    void** v6;
    unsigned char** rax7;
    void** rdx8;
    unsigned char** v9;
    void** rbp10;
    void** rax11;
    uint32_t eax12;
    uint32_t esi13;
    uint32_t eax14;
    void* ecx15;
    void* edx16;
    void** rbx17;
    uint32_t edx18;
    void* tmp32_19;
    void** rax20;
    void* ecx21;
    uint32_t eax22;
    void** rbx23;
    void* eax24;
    void** rax25;
    int64_t rdx26;
    void* eax27;
    int64_t rax28;
    void** rax29;
    void** rdx30;
    void** rax31;
    void** rbx32;
    uint32_t eax33;
    void** r13_34;
    void* ecx35;
    void** rbx36;
    void* rax37;
    void* ecx38;
    void* v39;
    void** r14_40;
    int64_t rax41;
    int64_t rsi42;
    unsigned char* rcx43;
    uint32_t eax44;
    uint32_t eax45;
    int64_t rax46;
    void** rax47;
    int64_t rax48;
    uint32_t eax49;
    uint32_t eax50;
    void** rdx51;
    void* eax52;
    int64_t rax53;
    uint32_t ecx54;
    uint32_t ecx55;
    uint32_t eax56;
    int1_t zf57;
    void* eax58;

    ebp5 = *reinterpret_cast<uint32_t*>(&rsi);
    v6 = rdi;
    rax7 = fun_26b0(rdi, rsi, rdx, rcx);
    rdx8 = word_limit;
    v9 = rax7;
    goto addr_34a0_2;
    addr_36c0_3:
    rbp10 = v6;
    rax11 = *reinterpret_cast<void***>(v6 + 8);
    in_column = reinterpret_cast<void*>(0);
    if (reinterpret_cast<unsigned char>(rax11) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v6 + 16))) {
        eax12 = fun_2380(v6, v6);
        esi13 = eax12;
    } else {
        *reinterpret_cast<void***>(v6 + 8) = rax11 + 1;
        esi13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax11));
    }
    eax14 = get_space(rbp10, esi13);
    ecx15 = prefix_length;
    edx16 = in_column;
    if (ecx15) {
        rbx17 = prefix;
        next_prefix_indent = edx16;
        edx18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx17));
        if (*reinterpret_cast<signed char*>(&edx18)) {
            do {
                if (edx18 != eax14) 
                    break;
                tmp32_19 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(in_column) + 1);
                in_column = tmp32_19;
                rax20 = *reinterpret_cast<void***>(rbp10 + 8);
                if (reinterpret_cast<unsigned char>(rax20) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp10 + 16))) {
                    *reinterpret_cast<void***>(rbp10 + 8) = rax20 + 1;
                    eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax20));
                } else {
                    eax14 = fun_2380(rbp10, rbp10);
                }
                edx18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx17 + 1));
                ++rbx17;
            } while (*reinterpret_cast<signed char*>(&edx18));
            goto addr_2ff4_14;
        } else {
            addr_2ff4_14:
            goto addr_2ef0_15;
        }
    } else {
        ecx21 = prefix_lead_space;
        if (reinterpret_cast<int32_t>(edx16) > reinterpret_cast<int32_t>(ecx21)) {
            edx16 = ecx21;
        }
        next_prefix_indent = edx16;
    }
    return eax14;
    addr_2ef0_15:
    eax22 = eax14;
    rbx23 = rbp10;
    while (1) {
        if (eax22 == 32) {
            eax24 = in_column;
            in_column = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax24) + 1);
            rax25 = *reinterpret_cast<void***>(rbx23 + 8);
            if (reinterpret_cast<unsigned char>(rax25) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx23 + 16))) 
                goto addr_2f34_22; else 
                goto addr_2f5d_23;
        }
        if (eax22 != 9) 
            break;
        *reinterpret_cast<void**>(&rdx26) = in_column;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
        tabs = 1;
        eax27 = reinterpret_cast<void*>(static_cast<uint32_t>(rdx26 + 7));
        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx26)) < reinterpret_cast<int32_t>(0)) 
            goto addr_2f1a_26;
        eax27 = *reinterpret_cast<void**>(&rdx26);
        addr_2f1a_26:
        *reinterpret_cast<int32_t*>(&rax28) = reinterpret_cast<int32_t>(eax27) >> 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
        in_column = reinterpret_cast<void*>(static_cast<uint32_t>(rax28 * 8 + 8));
        rax25 = *reinterpret_cast<void***>(rbx23 + 8);
        if (reinterpret_cast<unsigned char>(rax25) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx23 + 16))) {
            addr_2f5d_23:
            eax22 = fun_2380(rbx23);
        } else {
            addr_2f34_22:
            *reinterpret_cast<void***>(rbx23 + 8) = rax25 + 1;
            eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax25));
        }
    }
    return eax22;
    while (1) {
        set_other_indent(1, 1);
        flush_paragraph(1, rsi);
        rax29 = wptr;
        while (1) {
            do {
                rdx30 = rax29 + 1;
                *reinterpret_cast<void***>(rax29) = *reinterpret_cast<void***>(&ebp5);
                rax31 = *reinterpret_cast<void***>(rbx32 + 8);
                wptr = rdx30;
                if (reinterpret_cast<unsigned char>(rax31) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx32 + 16))) {
                    eax33 = fun_2380(rbx32, rbx32);
                    ebp5 = eax33;
                    rax29 = wptr;
                    if (ebp5 == 0xffffffff) 
                        goto addr_34f7_32;
                } else {
                    *reinterpret_cast<void***>(rbx32 + 8) = rax31 + 1;
                    ebp5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax31));
                    rax29 = rdx30;
                }
                if (reinterpret_cast<int32_t>(ebp5) <= reinterpret_cast<int32_t>(13)) 
                    break;
                if (ebp5 == 32) 
                    goto addr_34f7_32;
            } while (!reinterpret_cast<int1_t>(rax29 == 0x17108));
            break;
            if (reinterpret_cast<int32_t>(ebp5) <= reinterpret_cast<int32_t>(8)) {
                addr_34af_39:
                if (rax29 == 0x17108) 
                    break; else 
                    continue;
            } else {
                addr_34f7_32:
                r13_34 = word_limit;
                ecx35 = in_column;
                rbx36 = *reinterpret_cast<void***>(r13_34);
                rax37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax29) - reinterpret_cast<unsigned char>(rbx36));
                *reinterpret_cast<void***>(r13_34 + 8) = *reinterpret_cast<void***>(&rax37);
                ecx38 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(ecx35) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax37)));
                v39 = ecx38;
                r14_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx36) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(&rax37)))) + 0xffffffffffffffff);
                in_column = ecx38;
                rax41 = fun_24a0("(['`\"", "(['`\"");
                *reinterpret_cast<uint32_t*>(&rsi42) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_40));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi42) + 4) = 0;
                rcx43 = *v9;
                eax44 = 4;
                *reinterpret_cast<unsigned char*>(&eax44) = reinterpret_cast<unsigned char>(4 & rcx43[rsi42 * 2]);
                *reinterpret_cast<unsigned char*>(&rcx43) = reinterpret_cast<uint1_t>(!!rax41);
                eax45 = eax44 | *reinterpret_cast<uint32_t*>(&rcx43) | static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_34 + 16))) & 0xfffffffa;
                *reinterpret_cast<void***>(r13_34 + 16) = *reinterpret_cast<void***>(&eax45);
                if (reinterpret_cast<unsigned char>(rbx36) < reinterpret_cast<unsigned char>(r14_40)) {
                    while (rax46 = fun_24a0(")]'\"", ")]'\""), !!rax46) {
                        rax47 = r14_40 + 0xffffffffffffffff;
                        if (rbx36 == rax47) 
                            goto addr_36e0_42;
                        r14_40 = rax47;
                    }
                }
            }
            addr_3594_46:
            rax48 = fun_24a0(".?!", ".?!");
            *reinterpret_cast<uint32_t*>(&rsi) = ebp5;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rdx30) = reinterpret_cast<uint1_t>(!!rax48);
            eax49 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_34 + 16))) & 0xfffffffd | reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx30) + *reinterpret_cast<int32_t*>(&rdx30));
            *reinterpret_cast<void***>(r13_34 + 16) = *reinterpret_cast<void***>(&eax49);
            eax50 = get_space(v6, *reinterpret_cast<uint32_t*>(&rsi));
            rdx51 = word_limit;
            ebp5 = eax50;
            eax52 = in_column;
            *reinterpret_cast<void**>(&rax53) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(eax52) - reinterpret_cast<uint32_t>(v39));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax53) + 4) = 0;
            *reinterpret_cast<void**>(rdx51 + 12) = *reinterpret_cast<void**>(&rax53);
            if (ebp5 == 0xffffffff) {
                *reinterpret_cast<void***>(rdx51 + 16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16)) | 8);
                goto addr_368c_48;
            }
            ecx54 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16));
            if (!(*reinterpret_cast<unsigned char*>(&ecx54) & 2)) {
                *reinterpret_cast<void**>(&rax53) = reinterpret_cast<void*>(0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax53) + 4) = 0;
                goto addr_3607_51;
            } else {
                *reinterpret_cast<unsigned char*>(&rsi) = reinterpret_cast<uint1_t>(ebp5 == 10);
                *reinterpret_cast<unsigned char*>(&rax53) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rax53)) > reinterpret_cast<int32_t>(1))) | *reinterpret_cast<unsigned char*>(&rsi));
                if (!*reinterpret_cast<unsigned char*>(&rax53)) {
                    ecx55 = ecx54 & 0xfffffff7;
                    *reinterpret_cast<void***>(rdx51 + 16) = *reinterpret_cast<void***>(&ecx55);
                } else {
                    addr_3607_51:
                    eax56 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16))) & 0xfffffff7 | static_cast<uint32_t>(rax53 * 8);
                    *reinterpret_cast<void***>(rdx51 + 16) = *reinterpret_cast<void***>(&eax56);
                    if (ebp5 == 10) 
                        goto addr_368c_48;
                }
                zf57 = uniform == 0;
                if (!zf57) {
                    addr_368c_48:
                    eax58 = reinterpret_cast<void*>(0);
                    *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx51 + 16)) & 8));
                    *reinterpret_cast<void**>(rdx51 + 12) = reinterpret_cast<void*>(reinterpret_cast<int32_t>(eax58) + 1);
                    if (rdx51 == 0x15d10) {
                        addr_3720_55:
                        set_other_indent(1, 1);
                        flush_paragraph(1, rsi);
                        rdx51 = word_limit;
                        goto addr_36a7_56;
                    } else {
                        addr_36a7_56:
                        rdx8 = rdx51 + 40;
                        word_limit = rdx8;
                        if (ebp5 == 10) 
                            goto addr_36c0_3;
                        if (ebp5 == 0xffffffff) 
                            goto addr_36c0_3;
                    }
                } else {
                    if (rdx51 == 0x15d10) 
                        goto addr_3720_55;
                    rdx8 = rdx51 + 40;
                    word_limit = rdx8;
                }
                addr_34a0_2:
                rax29 = wptr;
                rbx32 = v6;
                *reinterpret_cast<void***>(rdx8) = rax29;
                goto addr_34af_39;
            }
            addr_36e0_42:
            goto addr_3594_46;
        }
    }
}

struct s1 {
    uint64_t f0;
    struct s1* f8;
    signed char[8] pad24;
    struct s1* f18;
};

int64_t fun_2450();

int64_t fun_23a0(struct s1* rdi, ...);

uint64_t quotearg_buffer_restyled(struct s1* rdi, uint64_t rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, uint64_t a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2450();
    if (r8d > 10) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8800 + rax11 * 4) + 0x8800;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

struct s1* slotvec = reinterpret_cast<struct s1*>(0xc070);

uint32_t nslots = 1;

struct s1* xpalloc();

void fun_24f0();

struct s3 {
    uint64_t f0;
    struct s1* f8;
};

void fun_2390(struct s1* rdi);

struct s1* xcharalloc(uint64_t rdi, ...);

void fun_2470();

struct s1* quotearg_n_options(struct s1* rdi, int64_t rsi, int64_t rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    struct s1* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s1* rax12;
    struct s1* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    uint64_t rsi18;
    struct s1* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    uint64_t rax23;
    uint64_t rsi24;
    struct s1* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x59df;
    rax8 = fun_23b0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x64f1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5a6b;
            fun_24f0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<int64_t>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->pad40, v21, v20, v7);
        if (rsi18 <= rax23) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x17160) {
                fun_2390(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5afa);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2470();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x8799);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x8794);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x879d);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x8790);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbe18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbe18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2373() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_2383() {
    __asm__("cli ");
    goto __uflow;
}

int64_t free = 0x2040;

void fun_2393() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_23a3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_23b3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23c3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23d3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23e3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_23f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clearerr_unlocked = 0x20b0;

void fun_2403() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x20c0;

void fun_2413() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2423() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2433() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2443() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2453() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2463() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2473() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2483() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2493() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2150;

void fun_24a3() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2160;

void fun_24b3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2170;

void fun_24c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_24d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2190;

void fun_24e3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21a0;

void fun_24f3() {
    __asm__("cli ");
    goto memset;
}

int64_t posix_fadvise = 0x21b0;

void fun_2503() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x21c0;

void fun_2513() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21d0;

void fun_2523() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21e0;

void fun_2533() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21f0;

void fun_2543() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2200;

void fun_2553() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t __memmove_chk = 0x2210;

void fun_2563() {
    __asm__("cli ");
    goto __memmove_chk;
}

int64_t memcpy = 0x2220;

void fun_2573() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_2583() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_2593() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_25a3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25b3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25c3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2280;

void fun_25d3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2290;

void fun_25e3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22a0;

void fun_25f3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22b0;

void fun_2603() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22c0;

void fun_2613() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22d0;

void fun_2623() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x22e0;

void fun_2633() {
    __asm__("cli ");
    goto fopen;
}

int64_t strtoumax = 0x22f0;

void fun_2643() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2300;

void fun_2653() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2310;

void fun_2663() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2320;

void fun_2673() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2330;

void fun_2683() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2340;

void fun_2693() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2350;

void fun_26a3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2360;

void fun_26b3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

struct s6 {
    signed char f0;
    signed char f1;
};

struct s5 {
    void** f0;
    signed char[7] pad8;
    struct s6* f8;
};

void set_program_name(void** rdi);

void** fun_25f0(int64_t rdi, ...);

void fun_2430(int64_t rdi, int64_t rsi);

void fun_2410(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2480(int64_t rdi, struct s5* rsi, int64_t rdx, int64_t rcx);

int64_t Version = 0x872e;

void version_etc(void** rdi, int64_t rsi, int64_t rdx);

void fun_2660();

struct s6* usage();

int64_t xdectoumax(signed char* rdi);

int32_t optind = 0;

void** fun_2630(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t fun_2703(int32_t edi, struct s5* rsi) {
    int32_t ebp3;
    struct s5* rbx4;
    void** rdi5;
    signed char* v6;
    struct s6* rax7;
    void** r8_8;
    int64_t rdi9;
    int32_t eax10;
    int64_t rcx11;
    void** rdi12;
    void** rax13;
    void** rcx14;
    void** rdx15;
    int64_t rax16;
    uint32_t eax17;
    void** rax18;
    int64_t rax19;
    int64_t r12_20;
    void** rax21;
    int64_t rax22;
    int32_t eax23;
    void** rdi24;
    void** rsi25;
    uint32_t eax26;
    uint32_t r12d27;
    int32_t r13d28;
    void** r15_29;
    void** rax30;
    void** rax31;
    void** rax32;
    int32_t* rax33;
    uint32_t eax34;
    void** rdi35;
    uint32_t eax36;
    int32_t eax37;
    void** rdi38;
    int64_t rax39;
    uint32_t r12d40;
    int64_t rax41;
    int64_t rax42;

    __asm__("cli ");
    ebp3 = edi;
    rbx4 = rsi;
    rdi5 = rsi->f0;
    set_program_name(rdi5);
    fun_25f0(6, 6);
    fun_2430("coreutils", "/usr/local/share/locale");
    fun_2410("coreutils", "/usr/local/share/locale");
    atexit(0x41b0, "/usr/local/share/locale");
    uniform = 0;
    split = 0;
    tagged = 0;
    crown = 0;
    max_width = reinterpret_cast<void**>(75);
    prefix = reinterpret_cast<void**>(0x8bc1);
    prefix_full_length = reinterpret_cast<void*>(0);
    prefix_lead_space = reinterpret_cast<void*>(0);
    prefix_length = reinterpret_cast<void*>(0);
    v6 = reinterpret_cast<signed char*>(0);
    if (ebp3 <= 1) 
        goto addr_27c3_2;
    rax7 = rbx4->f8;
    if (rax7->f0 == 45) 
        goto addr_2967_4;
    while (1) {
        addr_27c3_2:
        *reinterpret_cast<int32_t*>(&r8_8) = 0;
        *reinterpret_cast<int32_t*>(&r8_8 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi9) = ebp3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
        eax10 = fun_2480(rdi9, rbx4, "0123456789cstuw:p:g:", 0xba80);
        *reinterpret_cast<int32_t*>(&rcx11) = eax10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
        if (eax10 == -1) 
            break;
        if (*reinterpret_cast<int32_t*>(&rcx11) > 0x77) 
            goto addr_2aed_7;
        if (*reinterpret_cast<int32_t*>(&rcx11) > 98) 
            goto addr_2818_9;
        if (*reinterpret_cast<int32_t*>(&rcx11) == 0xffffff7d) {
            rdi12 = stdout;
            rcx11 = Version;
            version_etc(rdi12, "fmt", "GNU coreutils");
            fun_2660();
        }
        if (*reinterpret_cast<int32_t*>(&rcx11) != 0xffffff7e) 
            goto addr_2aed_7;
        rax7 = usage();
        addr_2967_4:
        if (rax7->f1 - 48 > 9) 
            continue;
        rbx4 = reinterpret_cast<struct s5*>(&rbx4->f8);
        --ebp3;
        v6 = &rax7->f1;
        rbx4->f0 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx4) - 8);
    }
    if (!v6) {
        if (0) {
            addr_2aff_17:
            rax13 = fun_2440();
            rcx14 = reinterpret_cast<void**>(0x8bc1);
            r8_8 = rax13;
            *reinterpret_cast<int32_t*>(&rdx15) = 75;
            *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
            rax16 = xdectoumax(0);
            goal_width = *reinterpret_cast<struct s0**>(&rax16);
            max_width = reinterpret_cast<void**>(&(*reinterpret_cast<struct s0**>(&rax16))->fa);
        } else {
            addr_2ad0_18:
            eax17 = reinterpret_cast<unsigned char>(max_width) * 0xbb;
            *reinterpret_cast<int32_t*>(&rcx14) = 0xc8;
            *reinterpret_cast<int32_t*>(&rcx14 + 4) = 0;
            __asm__("cdq ");
            *reinterpret_cast<int32_t*>(&rdx15) = reinterpret_cast<int32_t>(eax17) % 0xc8;
            *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
            goal_width = reinterpret_cast<struct s0*>(reinterpret_cast<int32_t>(eax17) / 0xc8);
        }
    } else {
        rax18 = fun_2440();
        r8_8 = rax18;
        rax19 = xdectoumax(v6);
        max_width = *reinterpret_cast<void***>(&rax19);
        r12_20 = rax19;
        if (1) 
            goto addr_2ad0_18;
        rax21 = fun_2440();
        rdx15 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r12_20)));
        r8_8 = rax21;
        rcx14 = reinterpret_cast<void**>(0x8bc1);
        rax22 = xdectoumax(0);
        goal_width = *reinterpret_cast<struct s0**>(&rax22);
    }
    eax23 = optind;
    if (eax23 == ebp3) {
        rdi24 = stdin;
        rsi25 = reinterpret_cast<void**>("-");
        eax26 = fmt(rdi24, "-", rdx15, rcx14, r8_8);
        r12d27 = eax26;
    } else {
        r13d28 = 0;
        r12d27 = 1;
        if (eax23 < ebp3) {
            do {
                r15_29 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx4) + eax23 * 8);
                if (*reinterpret_cast<void***>(r15_29) != 45 || *reinterpret_cast<void***>(r15_29 + 1)) {
                    rax30 = fun_2630(r15_29, "r", rdx15, rcx14, r8_8);
                    rsi25 = r15_29;
                    if (!rax30) {
                        rax31 = quotearg_style(4, rsi25, rdx15, rcx14, r8_8);
                        rax32 = fun_2440();
                        rax33 = fun_23b0();
                        rcx14 = rax31;
                        rdx15 = rax32;
                        *reinterpret_cast<int32_t*>(&rsi25) = *rax33;
                        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                        r12d27 = 0;
                        fun_2610();
                    } else {
                        eax34 = fmt(rax30, rsi25, rdx15, rcx14, r8_8);
                        r12d27 = r12d27 & eax34;
                    }
                } else {
                    rdi35 = stdin;
                    rsi25 = r15_29;
                    r13d28 = 1;
                    eax36 = fmt(rdi35, rsi25, rdx15, rcx14, r8_8);
                    r12d27 = r12d27 & eax36;
                }
                eax37 = optind;
                eax23 = eax37 + 1;
                optind = eax23;
            } while (eax23 < ebp3);
            if (!*reinterpret_cast<signed char*>(&r13d28)) 
                goto addr_2ab1_31;
        } else {
            goto addr_2ab1_31;
        }
    }
    rdi38 = stdin;
    rax39 = rpl_fclose(rdi38, rsi25, rdi38, rsi25);
    if (*reinterpret_cast<int32_t*>(&rax39)) {
        fun_2440();
        fun_23b0();
        fun_2610();
    }
    addr_2ab1_31:
    r12d40 = r12d27 ^ 1;
    *reinterpret_cast<uint32_t*>(&rax41) = *reinterpret_cast<unsigned char*>(&r12d40);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax41) + 4) = 0;
    return rax41;
    addr_2aed_7:
    if (static_cast<uint32_t>(rcx11 - 48) <= 9) {
        fun_2440();
        fun_2610();
    }
    usage();
    goto addr_2aff_17;
    addr_2818_9:
    *reinterpret_cast<uint32_t*>(&rax42) = static_cast<uint32_t>(rcx11 - 99);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax42) <= 20) {
        goto *reinterpret_cast<int32_t*>(0x86c8 + rax42 * 4) + 0x86c8;
    }
}

int64_t __libc_start_main = 0;

void fun_2c13() {
    __asm__("cli ");
    __libc_start_main(0x2700, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2370(int64_t rdi);

int64_t fun_2cb3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2370(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2cf3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2600(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2540(int64_t rdi);

int32_t fun_23c0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2680(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3dc3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    int32_t eax21;
    void** r13_22;
    void** rax23;
    void** rax24;
    int32_t eax25;
    void** rax26;
    void** rax27;
    void** rax28;
    int32_t eax29;
    void** rax30;
    void** r15_31;
    void** rax32;
    void** rax33;
    void** rax34;
    void** rdi35;
    void** r8_36;
    void** r9_37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2440();
            fun_2600(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2440();
            fun_2520(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2440();
            fun_2520(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2440();
            fun_2520(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2440();
            fun_2520(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2440();
            fun_2520(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2440();
            fun_2520(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2440();
            fun_2520(rax20, r12_19, 5, rcx6);
            do {
                if (1) 
                    break;
                eax21 = fun_2540("fmt");
            } while (eax21);
            r13_22 = v4;
            if (!r13_22) {
                rax23 = fun_2440();
                fun_2600(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax24 = fun_25f0(5);
                if (!rax24 || (eax25 = fun_23c0(rax24, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax25)) {
                    rax26 = fun_2440();
                    r13_22 = reinterpret_cast<void**>("fmt");
                    fun_2600(1, rax26, "https://www.gnu.org/software/coreutils/", "fmt");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_22 = reinterpret_cast<void**>("fmt");
                    goto addr_4160_9;
                }
            } else {
                rax27 = fun_2440();
                fun_2600(1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax28 = fun_25f0(5);
                if (!rax28 || (eax29 = fun_23c0(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    addr_4066_11:
                    rax30 = fun_2440();
                    fun_2600(1, rax30, "https://www.gnu.org/software/coreutils/", "fmt");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_22 == "fmt")) {
                        r12_2 = reinterpret_cast<void**>(0x8bc1);
                    }
                } else {
                    addr_4160_9:
                    r15_31 = stdout;
                    rax32 = fun_2440();
                    fun_2520(rax32, r15_31, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_4066_11;
                }
            }
            rax33 = fun_2440();
            rcx6 = r12_2;
            fun_2600(1, rax33, r13_22, rcx6);
            addr_3e1e_14:
            fun_2660();
        }
    } else {
        rax34 = fun_2440();
        rdi35 = stderr;
        rcx6 = r12_2;
        fun_2680(rdi35, 1, rax34, rcx6, r8_36, r9_37, v38, v39, v40, v41, v42, v43);
        goto addr_3e1e_14;
    }
}

int64_t file_name = 0;

void fun_4193(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_41a3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_23d0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_41b3() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23b0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2440();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4243_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2610();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23d0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4243_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2610();
    }
}

void fun_4263() {
    __asm__("cli ");
}

struct s7 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2580(struct s7* rdi);

void fun_4273(struct s7* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2580(rdi);
        goto 0x2500;
    }
}

int32_t fun_25c0(struct s7* rdi);

int64_t fun_24d0(int64_t rdi, ...);

int32_t rpl_fflush(struct s7* rdi);

int64_t fun_2420(struct s7* rdi);

int64_t fun_42a3(struct s7* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2580(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25c0(rdi);
        if (!(eax3 && (eax4 = fun_2580(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24d0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23b0();
            r12d9 = *rax8;
            rax10 = fun_2420(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2420;
}

void rpl_fseeko(struct s7* rdi);

void fun_4333(struct s7* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25c0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_4383(struct s7* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2580(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24d0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_2670(struct s1* rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s8 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s8* fun_24c0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4403(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s8* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2670("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23a0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24c0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23c0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5ba3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23b0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x17260;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5be3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x17260);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5c03(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x17260);
    }
    *rdi = esi;
    return 0x17260;
}

int64_t fun_5c23(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x17260);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s9 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5c63(struct s9* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0x17260);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s10 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s10* fun_5c83(struct s10* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s10*>(0x17260);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26ca;
    if (!rdx) 
        goto 0x26ca;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x17260;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

uint64_t fun_5cc3(struct s1* rdi, uint64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    struct s11* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    uint64_t rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s11*>(0x17260);
    }
    rax7 = fun_23b0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->pad40, v12, v10, 0x5cf6);
    *rax7 = r15d8;
    return rax13;
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

struct s1* fun_5d43(int64_t rdi, int64_t rsi, uint64_t* rdx, struct s12* rcx) {
    struct s12* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    uint64_t r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    uint64_t rax14;
    uint64_t rsi15;
    struct s1* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s12*>(0x17260);
    }
    rax6 = fun_23b0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<uint64_t>(&rbx5->pad40);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5d71);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5dcc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5e33() {
    __asm__("cli ");
}

struct s1* gc078 = reinterpret_cast<struct s1*>(0x17160);

int64_t slotvec0 = 0x100;

void fun_5e43() {
    uint32_t eax1;
    struct s1* r12_2;
    int64_t rax3;
    struct s1** rbx4;
    struct s1** rbp5;
    struct s1* rdi6;
    struct s1* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s1**>(reinterpret_cast<int64_t>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 2;
            fun_2390(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0x17160) {
        fun_2390(rdi7);
        gc078 = reinterpret_cast<struct s1*>(0x17160);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc070) {
        fun_2390(r12_2);
        slotvec = reinterpret_cast<struct s1*>(0xc070);
    }
    nslots = 1;
    return;
}

void fun_5ee3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f03() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f13(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f33(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s1* fun_5f53(struct s1* rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26d0;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_5fe3(struct s1* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26d5;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

struct s1* fun_6073(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s2* rcx4;
    struct s1* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26da;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2470();
    } else {
        return rax5;
    }
}

struct s1* fun_6103(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26df;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_6193(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s2* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s1* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x110c0]");
    __asm__("movdqa xmm1, [rip+0x110c8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x110b1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2470();
    } else {
        return rax10;
    }
}

struct s1* fun_6233(int64_t rdi, uint32_t esi) {
    struct s2* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x11020]");
    __asm__("movdqa xmm1, [rip+0x11028]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x11011]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2470();
    } else {
        return rax9;
    }
}

struct s1* fun_62d3(int64_t rdi) {
    void** rax2;
    struct s1* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x10f80]");
    __asm__("movdqa xmm1, [rip+0x10f88]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x10f69]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2470();
    } else {
        return rax3;
    }
}

struct s1* fun_6363(int64_t rdi, int64_t rsi) {
    void** rax3;
    struct s1* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x10ef0]");
    __asm__("movdqa xmm1, [rip+0x10ef8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x10ee6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2470();
    } else {
        return rax4;
    }
}

struct s1* fun_63f3(struct s1* rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26e4;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_6493(struct s1* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x10dba]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x10db2]");
    __asm__("movdqa xmm2, [rip+0x10dba]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26e9;
    if (!rdx) 
        goto 0x26e9;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

struct s1* fun_6533(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s2* rcx7;
    struct s1* rdi8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x10d1a]");
    __asm__("movdqa xmm1, [rip+0x10d22]");
    __asm__("movdqa xmm2, [rip+0x10d2a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ee;
    if (!rdx) 
        goto 0x26ee;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2470();
    } else {
        return rax9;
    }
}

struct s1* fun_65e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x10c6a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x10c62]");
    __asm__("movdqa xmm2, [rip+0x10c6a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26f3;
    if (!rsi) 
        goto 0x26f3;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

struct s1* fun_6683(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x10bca]");
    __asm__("movdqa xmm1, [rip+0x10bd2]");
    __asm__("movdqa xmm2, [rip+0x10bda]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26f8;
    if (!rsi) 
        goto 0x26f8;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

void fun_6723() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6733(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6753() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6773(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s13 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2550(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_6793(void** rdi, void** rsi, void** rdx, void** rcx, struct s13* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2680(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2680(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2440();
    fun_2680(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2550(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2440();
    fun_2680(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2550(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2440();
        fun_2680(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8e68 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x8e68;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6c03() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6c23(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s14* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s14* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2470();
    } else {
        return;
    }
}

void fun_6cc3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6d66_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6d70_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2470();
    } else {
        return;
    }
    addr_6d66_5:
    goto addr_6d70_7;
}

void fun_6da3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2550(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2440();
    fun_2600(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2440();
    fun_2600(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2440();
    goto fun_2600;
}

int64_t fun_23f0();

void xalloc_die();

void fun_6e43(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2590(void** rdi);

void fun_6e83(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6ea3(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6ec3(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25e0();

void fun_6ee3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25e0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6f13() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6f43(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6f83() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6fc3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6ff3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7043(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23f0();
            if (rax5) 
                break;
            addr_708d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_708d_5;
        rax8 = fun_23f0();
        if (rax8) 
            goto addr_7076_9;
        if (rbx4) 
            goto addr_708d_5;
        addr_7076_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_70d3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23f0();
            if (rax8) 
                break;
            addr_711a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_711a_5;
        rax11 = fun_23f0();
        if (rax11) 
            goto addr_7102_9;
        if (!rbx6) 
            goto addr_7102_9;
        if (r12_4) 
            goto addr_711a_5;
        addr_7102_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7163(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_720d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7220_10:
                *r12_8 = 0;
            }
            addr_71c0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_71e6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7234_14;
            if (rcx10 <= rsi9) 
                goto addr_71dd_16;
            if (rsi9 >= 0) 
                goto addr_7234_14;
            addr_71dd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7234_14;
            addr_71e6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25e0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7234_14;
            if (!rbp13) 
                break;
            addr_7234_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_720d_9;
        } else {
            if (!r13_6) 
                goto addr_7220_10;
            goto addr_71c0_11;
        }
    }
}

int64_t fun_2530();

void fun_7263() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2530();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7293() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2530();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2530();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2530();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2570(signed char* rdi, void** rsi, void** rdx);

void fun_7303(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2590(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

void fun_7343(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2590(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

struct s15 {
    signed char[1] pad1;
    void** f1;
};

void fun_7383(int64_t rdi, struct s15* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2590(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2570;
    }
}

void** fun_2460(void** rdi, ...);

void fun_73c3(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2460(rdi);
    rax3 = fun_2590(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

void fun_7403() {
    struct s1* rdi1;

    __asm__("cli ");
    fun_2440();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    fun_2610();
    fun_23a0(rdi1);
}

int32_t xstrtoumax();

int64_t quote(int64_t rdi);

uint64_t fun_7443(int64_t rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    void** rax5;
    int32_t eax6;
    int32_t* rax7;
    int32_t* r12_8;
    uint64_t v9;
    void* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax5 = g28;
    eax6 = xstrtoumax();
    if (eax6) {
        rax7 = fun_23b0();
        r12_8 = rax7;
        if (eax6 == 1) {
            addr_74e0_3:
            *r12_8 = 75;
        } else {
            if (eax6 == 3) {
                *rax7 = 0;
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (rax10) {
                fun_2470();
            } else {
                return v9;
            }
        }
        rax11 = fun_23b0();
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_74e0_3;
        *rax11 = 34;
    }
    quote(rdi);
    if (*r12_8 != 22) 
        goto addr_74fc_13;
    while (1) {
        addr_74fc_13:
        if (!1) {
        }
        fun_2610();
    }
}

void xnumtoumax();

void fun_7553() {
    __asm__("cli ");
    xnumtoumax();
    return;
}

void** fun_2640(void** rdi);

int64_t fun_7583(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    unsigned char* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    unsigned char** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<unsigned char*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_24e0("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", "0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c");
        do {
            fun_2470();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_78f4_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_78f4_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_763d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_7645_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_23b0();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_26b0(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!((rcx9 + rdx23 * 2)[1] & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_767b_21;
    rsi = r15_14;
    rax24 = fun_2640(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_24a0(r13_18), r8 = r8, rax26 == 0))) {
            addr_767b_21:
            r12d11 = 4;
            goto addr_7645_13;
        } else {
            addr_76b9_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_24a0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<unsigned char*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x8f18 + rbp33 * 4) + 0x8f18;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_767b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_763d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_763d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_24a0(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_76b9_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_7645_13;
}

int64_t fun_23e0();

int64_t fun_79b3(void** rdi, void** rsi) {
    int64_t rax3;
    uint32_t ebx4;
    int64_t rax5;
    int32_t* rax6;
    int32_t* rax7;

    __asm__("cli ");
    rax3 = fun_23e0();
    ebx4 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax5 = rpl_fclose(rdi, rsi);
    if (ebx4) {
        if (*reinterpret_cast<int32_t*>(&rax5)) {
            addr_7a0e_3:
            *reinterpret_cast<int32_t*>(&rax5) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        } else {
            rax6 = fun_23b0();
            *rax6 = 0;
            *reinterpret_cast<int32_t*>(&rax5) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax5)) {
            if (rax3) 
                goto addr_7a0e_3;
            rax7 = fun_23b0();
            *reinterpret_cast<int32_t*>(&rax5) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax7 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
    }
    return rax5;
}

signed char* fun_25b0(int64_t rdi);

signed char* fun_7a23() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25b0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2490(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_7a63(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2490(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2470();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_7af3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2470();
    } else {
        return rax3;
    }
}

int64_t fun_7b73(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25f0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2460(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2570(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2570(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7c23() {
    __asm__("cli ");
    goto fun_25f0;
}

void fun_7c33() {
    __asm__("cli ");
}

void fun_7c47() {
    __asm__("cli ");
    return;
}

void fun_2830() {
    goto 0x27e8;
}

void fun_28cd() {
    signed char* rax1;
    signed char* rdx2;
    int64_t rax3;
    int64_t rdx4;

    do {
        --rax1;
        if (rdx2 == rax1) 
            break;
    } while (*(rax1 - 1) == 32);
    *rax1 = 0;
    rax3 = reinterpret_cast<int64_t>(rax1) - rdx4;
    prefix_length = *reinterpret_cast<void**>(&rax3);
    goto 0x27e8;
}

void fun_28f0() {
    goto 0x27e8;
}

uint32_t fun_2510(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_26a0(int64_t rdi, void** rsi);

uint32_t fun_2690(void** rdi, void** rsi);

void fun_4635() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    unsigned char** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2440();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2440();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2460(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4933_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4933_22; else 
                            goto addr_4d2d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4ded_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5140_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4930_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4930_30; else 
                                goto addr_5159_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2460(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5140_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2510(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5140_28; else 
                            goto addr_47dc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_52a0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5120_40:
                        if (r11_27 == 1) {
                            addr_4cad_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5268_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_48e7_44;
                            }
                        } else {
                            goto addr_5130_46;
                        }
                    } else {
                        addr_52af_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_4cad_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4933_22:
                                if (v47 != 1) {
                                    addr_4e89_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2460(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4ed4_54;
                                    }
                                } else {
                                    goto addr_4940_56;
                                }
                            } else {
                                addr_48e5_57:
                                ebp36 = 0;
                                goto addr_48e7_44;
                            }
                        } else {
                            addr_5114_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_52af_47; else 
                                goto addr_511e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4cad_41;
                        if (v47 == 1) 
                            goto addr_4940_56; else 
                            goto addr_4e89_52;
                    }
                }
                addr_49a1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4838_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_485d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4b60_65;
                    } else {
                        addr_49c9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5218_67;
                    }
                } else {
                    goto addr_49c0_69;
                }
                addr_4871_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_48bc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5218_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_48bc_81;
                }
                addr_49c0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_485d_64; else 
                    goto addr_49c9_66;
                addr_48e7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_499f_91;
                if (v22) 
                    goto addr_48ff_93;
                addr_499f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_49a1_62;
                addr_4ed4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_565b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_56cb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_54cf_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26a0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2690(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_4fce_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_498c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4fd8_112;
                    }
                } else {
                    addr_4fd8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_50a9_114;
                }
                addr_4998_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_499f_91;
                while (1) {
                    addr_50a9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_55b7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_5016_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_55c5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_5097_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_5097_128;
                        }
                    }
                    addr_5045_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_5097_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_5016_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5045_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_48bc_81;
                addr_55c5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5218_67;
                addr_565b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_4fce_109;
                addr_56cb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_4fce_109;
                addr_4940_56:
                rax93 = fun_26b0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>(((*rax93 + reinterpret_cast<unsigned char>(rax24) * 2)[1] & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_498c_110;
                addr_511e_59:
                goto addr_5120_40;
                addr_4ded_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4933_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4998_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_48e5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4933_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4e32_160;
                if (!v22) 
                    goto addr_5207_162; else 
                    goto addr_5413_163;
                addr_4e32_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5207_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5218_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4cdb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4b43_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4871_70; else 
                    goto addr_4b57_169;
                addr_4cdb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4838_63;
                goto addr_49c0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5114_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_524f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4930_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4828_178; else 
                        goto addr_51d2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5114_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4933_22;
                }
                addr_524f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4930_30:
                    r8d42 = 0;
                    goto addr_4933_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_49a1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5268_42;
                    }
                }
                addr_4828_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4838_63;
                addr_51d2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5130_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_49a1_62;
                } else {
                    addr_51e2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4933_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5992_188;
                if (v28) 
                    goto addr_5207_162;
                addr_5992_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4b43_168;
                addr_47dc_37:
                if (v22) 
                    goto addr_57d3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_47f3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_52a0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_532b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4933_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4828_178; else 
                        goto addr_5307_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5114_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4933_22;
                }
                addr_532b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4933_22;
                }
                addr_5307_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5130_46;
                goto addr_51e2_186;
                addr_47f3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4933_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4933_22; else 
                    goto addr_4804_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_58de_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5764_210;
            if (1) 
                goto addr_5762_212;
            if (!v29) 
                goto addr_539e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_58d1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4b60_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_491b_219; else 
            goto addr_4b7a_220;
        addr_48ff_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4913_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_4b7a_220; else 
            goto addr_491b_219;
        addr_54cf_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_4b7a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_54ed_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_5960_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_53c6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_55b7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4913_221;
        addr_5413_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4913_221;
        addr_4b57_169:
        goto addr_4b60_65;
        addr_58de_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_4b7a_220;
        goto addr_54ed_222;
        addr_5764_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_57be_236;
        fun_2470();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5960_225;
        addr_5762_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5764_210;
        addr_539e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5764_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_53c6_226;
        }
        addr_58d1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4d2d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x892c + rax113 * 4) + 0x892c;
    addr_5159_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8a2c + rax114 * 4) + 0x8a2c;
    addr_57d3_190:
    addr_491b_219:
    goto 0x4600;
    addr_4804_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x882c + rax115 * 4) + 0x882c;
    addr_57be_236:
    goto v116;
}

void fun_4820() {
}

void fun_49d8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x46d2;
}

void fun_4a31() {
    goto 0x46d2;
}

void fun_4b1e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x49a1;
    }
    if (v2) 
        goto 0x5413;
    if (!r10_3) 
        goto addr_557e_5;
    if (!v4) 
        goto addr_544e_7;
    addr_557e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_544e_7:
    goto 0x4854;
}

void fun_4b3c() {
}

void fun_4be7() {
    signed char v1;

    if (v1) {
        goto 0x4b6f;
    } else {
        goto 0x48aa;
    }
}

void fun_4c01() {
    signed char v1;

    if (!v1) 
        goto 0x4bfa; else 
        goto "???";
}

void fun_4c28() {
    goto 0x4b43;
}

void fun_4ca8() {
}

void fun_4cc0() {
}

void fun_4cef() {
    goto 0x4b43;
}

void fun_4d41() {
    goto 0x4cd0;
}

void fun_4d70() {
    goto 0x4cd0;
}

void fun_4da3() {
    goto 0x4cd0;
}

void fun_5170() {
    goto 0x4828;
}

void fun_546e() {
    signed char v1;

    if (v1) 
        goto 0x5413;
    goto 0x4854;
}

void fun_5515() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4854;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4838;
        goto 0x4854;
    }
}

void fun_5932() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4ba0;
    } else {
        goto 0x46d2;
    }
}

void fun_6868() {
    fun_2440();
}

void fun_772c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x773b;
}

void fun_77fc() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x7809;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x773b;
}

void fun_7820() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_784c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x773b;
}

void fun_786d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x773b;
}

void fun_7891() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x773b;
}

void fun_78b5() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7844;
}

void fun_78d9() {
}

void fun_78f9() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7844;
}

void fun_7915() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7844;
}

void fun_2840() {
    uniform = 1;
    goto 0x27e8;
}

void fun_2908() {
    crown = 1;
    goto 0x27e8;
}

void fun_4a5e() {
    goto 0x46d2;
}

void fun_4c34() {
    goto 0x4bec;
}

void fun_4cfb() {
    goto 0x4828;
}

void fun_4d4d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4cd0;
    goto 0x48ff;
}

void fun_4d7f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4cdb;
        goto 0x4700;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4b7a;
        goto 0x491b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5518;
    if (r10_8 > r15_9) 
        goto addr_4c65_9;
    addr_4c6a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5523;
    goto 0x4854;
    addr_4c65_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4c6a_10;
}

void fun_4db2() {
    goto 0x48e7;
}

void fun_5180() {
    goto 0x48e7;
}

void fun_591f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4a3c;
    } else {
        goto 0x4ba0;
    }
}

void fun_6920() {
}

void fun_77cf() {
    if (__intrinsic()) 
        goto 0x7809; else 
        goto "???";
}

void fun_2850() {
    tagged = 1;
    goto 0x27e8;
}

void fun_4dbc() {
    goto 0x4d57;
}

void fun_518a() {
    goto 0x4cad;
}

void fun_6980() {
    fun_2440();
    goto fun_2680;
}

void fun_2860() {
    split = 1;
    goto 0x27e8;
}

void fun_4a8d() {
    goto 0x46d2;
}

void fun_4dc8() {
    goto 0x4d57;
}

void fun_5197() {
    goto 0x4cfe;
}

void fun_69c0() {
    fun_2440();
    goto fun_2680;
}

void** optarg = reinterpret_cast<void**>(0);

void fun_2870() {
    void** rdx1;
    int64_t rax2;
    void* ecx3;
    void** rax4;

    rdx1 = optarg;
    prefix_lead_space = reinterpret_cast<void*>(0);
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx1) == 32)) {
        *reinterpret_cast<int32_t*>(&rax2) = 1 - *reinterpret_cast<int32_t*>(&rdx1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
        do {
            ecx3 = reinterpret_cast<void*>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(rax2 + reinterpret_cast<unsigned char>(rdx1))));
            ++rdx1;
        } while (*reinterpret_cast<void***>(rdx1) == 32);
        prefix_lead_space = ecx3;
    }
    prefix = rdx1;
    rax4 = fun_2460(rdx1);
    prefix_full_length = *reinterpret_cast<void**>(&rax4);
    if (reinterpret_cast<unsigned char>(rdx1) < reinterpret_cast<unsigned char>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rax4))) + reinterpret_cast<unsigned char>(rdx1))) 
        goto 0x28d9;
    goto 0x28df;
}

void fun_4aba() {
    goto 0x46d2;
}

void fun_4dd4() {
    goto 0x4cd0;
}

void fun_6a00() {
    fun_2440();
    goto fun_2680;
}

void fun_4adc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5470;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x49a1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x49a1;
    }
    if (v11) 
        goto 0x57d3;
    if (r10_12 > r15_13) 
        goto addr_5823_8;
    addr_5828_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5561;
    addr_5823_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5828_9;
}

struct s16 {
    signed char[24] pad24;
    int64_t f18;
};

struct s17 {
    signed char[16] pad16;
    void** f10;
};

struct s18 {
    signed char[8] pad8;
    void** f8;
};

void fun_6a50() {
    int64_t r15_1;
    struct s16* rbx2;
    void** r14_3;
    struct s17* rbx4;
    void** r13_5;
    struct s18* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2440();
    fun_2680(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6a72, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6aa8() {
    fun_2440();
    goto 0x6a79;
}

struct s19 {
    signed char[32] pad32;
    int64_t f20;
};

struct s20 {
    signed char[24] pad24;
    int64_t f18;
};

struct s21 {
    signed char[16] pad16;
    void** f10;
};

struct s22 {
    signed char[8] pad8;
    void** f8;
};

struct s23 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6ae0() {
    int64_t rcx1;
    struct s19* rbx2;
    int64_t r15_3;
    struct s20* rbx4;
    void** r14_5;
    struct s21* rbx6;
    void** r13_7;
    struct s22* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s23* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2440();
    fun_2680(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6b14, __return_address(), rcx1);
    goto v15;
}

void fun_6b58() {
    fun_2440();
    goto 0x6b1b;
}
