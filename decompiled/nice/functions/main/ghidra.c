char main(uint param_1,undefined8 *param_2,undefined8 param_3,char **param_4)

{
  undefined8 *puVar1;
  char **__argv;
  char *pcVar2;
  uint uVar3;
  int iVar4;
  int *piVar5;
  undefined8 uVar6;
  ulong uVar7;
  ulong extraout_RDX;
  uint uVar8;
  undefined8 uVar9;
  char cVar10;
  char *pcVar11;
  undefined *unaff_R14;
  undefined1 *unaff_R15;
  long in_FS_OFFSET;
  char *local_48;
  long local_40;
  
  piVar5 = (int *)(ulong)param_1;
  uVar8 = 1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils");
  textdomain("coreutils");
  exit_failure = 0x7d;
  atexit(close_stdout);
  if ((int)param_1 < 2) goto LAB_0010297a;
  pcVar11 = (char *)0x0;
  unaff_R15 = longopts;
  unaff_R14 = &DAT_001070c4;
LAB_00102715:
  param_4 = (char **)((long)(int)uVar8 * 8);
  pcVar2 = (char *)param_2[(int)uVar8];
  if (*pcVar2 != '-') goto LAB_00102808;
  if (9 < (int)pcVar2[(ulong)((pcVar2[1] - 0x2bU & 0xfd) == 0) + 1] - 0x30U) goto LAB_00102808;
  uVar8 = uVar8 + 1;
  pcVar11 = pcVar2 + 1;
LAB_0010275b:
  if ((int)piVar5 <= (int)uVar8) {
    do {
      param_1 = (uint)piVar5;
      if (pcVar11 == (char *)0x0) {
LAB_0010297a:
        piVar5 = __errno_location();
        if (param_1 == uVar8) {
          *piVar5 = 0;
          uVar8 = getpriority(PRIO_PROCESS,0);
          uVar7 = (ulong)uVar8;
          if ((uVar8 == 0xffffffff) && (*piVar5 != 0)) {
            uVar6 = dcgettext(0,"cannot get niceness",5);
            error(0x7d,*piVar5,uVar6);
            uVar7 = extraout_RDX;
          }
          cVar10 = '\0';
          __printf_chk(1,&DAT_00107102,uVar7);
          goto LAB_00102954;
        }
LAB_00102a54:
        pcVar11 = (char *)0xa;
      }
      else {
        param_4 = &local_48;
        uVar3 = xstrtol(pcVar11,0,10,&local_48,"");
        if (1 < uVar3) {
          param_4 = (char **)quote(pcVar11);
          uVar6 = dcgettext(0,"invalid adjustment %s",5);
          error(0x7d,0,uVar6);
          goto LAB_00102a54;
        }
        pcVar11 = local_48;
        if (0x27 < (long)local_48) {
          pcVar11 = (char *)0x27;
        }
        if ((long)pcVar11 < -0x27) {
          pcVar11 = (char *)0xffffffffffffffd9;
        }
        if (uVar8 == param_1) {
          uVar6 = dcgettext(0,"a command must be given with an adjustment",5);
          error(0,0,uVar6);
          goto LAB_00102a13;
        }
      }
      piVar5 = __errno_location();
      *piVar5 = 0;
      iVar4 = getpriority(PRIO_PROCESS,0);
      if ((iVar4 != -1) || (*piVar5 == 0)) goto LAB_001028b7;
      uVar6 = dcgettext(0,"cannot get niceness",5);
      error(0x7d,*piVar5,uVar6);
LAB_00102808:
      puVar1 = (undefined8 *)((long)(param_2 + -1) + (long)param_4);
      *puVar1 = *param_2;
      optind = 0;
      param_4 = (char **)unaff_R15;
      iVar4 = getopt_long((int)piVar5 - (uVar8 - 1),puVar1,unaff_R14,unaff_R15,0);
      uVar8 = (uVar8 - 1) + optind;
    } while (iVar4 == -1);
    if (-1 < iVar4) goto LAB_00102890;
    if (iVar4 == -0x83) {
      version_etc(stdout,&DAT_00107004,"GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar4 == -0x82) {
      iVar4 = usage();
LAB_001028b7:
      iVar4 = setpriority(PRIO_PROCESS,0,iVar4 + (int)pcVar11);
      if (iVar4 == 0) {
LAB_00102912:
        __argv = (char **)(param_2 + (int)uVar8);
        execvp(*__argv,__argv);
        iVar4 = *piVar5;
        uVar6 = quote(*__argv);
        cVar10 = (iVar4 == 2) + '~';
        error(0,*piVar5,"%s",uVar6);
      }
      else {
        uVar6 = dcgettext(0,"cannot set niceness",5);
        iVar4 = *piVar5;
        if ((iVar4 == 0xd) || (uVar9 = 0x7d, iVar4 == 1)) {
          uVar9 = 0;
        }
        cVar10 = '}';
        error(uVar9,iVar4,uVar6);
        if ((*stderr & 0x20) == 0) goto LAB_00102912;
      }
LAB_00102954:
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        return cVar10;
      }
      goto LAB_00102a1d;
    }
    goto LAB_00102a13;
  }
  goto LAB_00102715;
LAB_00102890:
  pcVar11 = optarg;
  if (iVar4 != 0x6e) {
LAB_00102a13:
    usage(0x7d);
LAB_00102a1d:
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  goto LAB_0010275b;
}