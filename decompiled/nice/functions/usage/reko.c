void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002600(fn00000000000023F0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002BAE;
	}
	fn0000000000002590(fn00000000000023F0(0x05, "Usage: %s [OPTION] [COMMAND [ARG]...]\n", null), 0x01);
	fn0000000000002590(fn00000000000023F0(0x05, "Run COMMAND with an adjusted niceness, which affects process scheduling.\nWith no COMMAND, print the current niceness.  Niceness values range from\n%d (most favorable to the process) to %d (least favorable to the process).\n", null), 0x01);
	fn00000000000024C0(stdout, fn00000000000023F0(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn00000000000024C0(stdout, fn00000000000023F0(0x05, "  -n, --adjustment=N   add integer N to the niceness (default 10)\n", null));
	fn00000000000024C0(stdout, fn00000000000023F0(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024C0(stdout, fn00000000000023F0(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002590(fn00000000000023F0(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_915 * rbx_193 = fp - 0xB8 + 16;
	do
	{
		char * rsi_195 = rbx_193->qw0000;
		++rbx_193;
	} while (rsi_195 != null && fn00000000000024E0(rsi_195, "nice") != 0x00);
	ptr64 r13_208 = rbx_193->qw0008;
	if (r13_208 != 0x00)
	{
		fn0000000000002590(fn00000000000023F0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_29 rax_295 = fn0000000000002580(null, 0x05);
		if (rax_295 == 0x00 || fn0000000000002380(0x03, "en_", rax_295) == 0x00)
			goto l0000000000002DD6;
	}
	else
	{
		fn0000000000002590(fn00000000000023F0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_29 rax_237 = fn0000000000002580(null, 0x05);
		if (rax_237 == 0x00 || fn0000000000002380(0x03, "en_", rax_237) == 0x00)
		{
			fn0000000000002590(fn00000000000023F0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002E13:
			fn0000000000002590(fn00000000000023F0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002BAE:
			fn00000000000025E0(edi);
		}
		r13_208 = 0x7004;
	}
	fn00000000000024C0(stdout, fn00000000000023F0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002DD6:
	fn0000000000002590(fn00000000000023F0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002E13;
}