
int64_t fun_2560();

int64_t fun_2480(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2560();
    if (r8d > 10) {
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb780 + rax11 * 4) + 0xb780;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

void*** fun_2490(void** rdi, ...);

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_2600();

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2470(void** rdi, ...);

void** xcharalloc(void** rdi, ...);

void fun_2590();

void** quotearg_n_options(void** rdi, void** rsi, int64_t rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void*** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s1* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x535f;
    rax8 = fun_2490(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xf070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x9b71]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x53eb;
            fun_2600();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xf100) {
                fun_2470(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x547a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2590();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xf080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s2 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s2* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s2* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xb70b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xb704);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xb70f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xb700);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gedb8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gedb8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2443() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __snprintf_chk = 0x2030;

void fun_2453() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t raise = 0x2040;

void fun_2463() {
    __asm__("cli ");
    goto raise;
}

int64_t free = 0x2050;

void fun_2473() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_2483() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2493() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_24a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_24b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20a0;

void fun_24c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t mkdir = 0x20b0;

void fun_24d3() {
    __asm__("cli ");
    goto mkdir;
}

int64_t lchmod = 0x20c0;

void fun_24e3() {
    __asm__("cli ");
    goto lchmod;
}

int64_t reallocarray = 0x20d0;

void fun_24f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20e0;

void fun_2503() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20f0;

void fun_2513() {
    __asm__("cli ");
    goto textdomain;
}

int64_t __open_2 = 0x2100;

void fun_2523() {
    __asm__("cli ");
    goto __open_2;
}

int64_t fclose = 0x2110;

void fun_2533() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2120;

void fun_2543() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2130;

void fun_2553() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2140;

void fun_2563() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2150;

void fun_2573() {
    __asm__("cli ");
    goto strlen;
}

int64_t chdir = 0x2160;

void fun_2583() {
    __asm__("cli ");
    goto chdir;
}

int64_t __stack_chk_fail = 0x2170;

void fun_2593() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2180;

void fun_25a3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2190;

void fun_25b3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x21a0;

void fun_25c3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21b0;

void fun_25d3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21c0;

void fun_25e3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21d0;

void fun_25f3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21e0;

void fun_2603() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x21f0;

void fun_2613() {
    __asm__("cli ");
    goto close;
}

int64_t memcmp = 0x2200;

void fun_2623() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2210;

void fun_2633() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2220;

void fun_2643() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_2653() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_2663() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t umask = 0x2250;

void fun_2673() {
    __asm__("cli ");
    goto umask;
}

int64_t stat = 0x2260;

void fun_2683() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2270;

void fun_2693() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2280;

void fun_26a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2290;

void fun_26b3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22a0;

void fun_26c3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22b0;

void fun_26d3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t lchown = 0x22c0;

void fun_26e3() {
    __asm__("cli ");
    goto lchown;
}

int64_t __freading = 0x22d0;

void fun_26f3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x22e0;

void fun_2703() {
    __asm__("cli ");
    goto fchdir;
}

int64_t fwrite_unlocked = 0x22f0;

void fun_2713() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t chown = 0x2300;

void fun_2723() {
    __asm__("cli ");
    goto chown;
}

int64_t realloc = 0x2310;

void fun_2733() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2320;

void fun_2743() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2330;

void fun_2753() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t fchmod = 0x2340;

void fun_2763() {
    __asm__("cli ");
    goto fchmod;
}

int64_t chmod = 0x2350;

void fun_2773() {
    __asm__("cli ");
    goto chmod;
}

int64_t error = 0x2360;

void fun_2783() {
    __asm__("cli ");
    goto error;
}

int64_t waitpid = 0x2370;

void fun_2793() {
    __asm__("cli ");
    goto waitpid;
}

int64_t open = 0x2380;

void fun_27a3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2390;

void fun_27b3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fchown = 0x23a0;

void fun_27c3() {
    __asm__("cli ");
    goto fchown;
}

int64_t __cxa_atexit = 0x23b0;

void fun_27d3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x23c0;

void fun_27e3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23d0;

void fun_27f3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23e0;

void fun_2803() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23f0;

void fun_2813() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2400;

void fun_2823() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2410;

void fun_2833() {
    __asm__("cli ");
    goto fstat;
}

int64_t fork = 0x2420;

void fun_2843() {
    __asm__("cli ");
    goto fork;
}

int64_t __ctype_b_loc = 0x2430;

void fun_2853() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2740(int64_t rdi, ...);

void fun_2540(int64_t rdi, int64_t rsi);

void fun_2510(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t optind = 0;

void** fun_2550();

void fun_2780();

int32_t usage();

int32_t fun_25a0(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void** optarg = reinterpret_cast<void**>(0);

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0xb5a4;

void version_etc(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8);

void fun_27e0();

void savewd_process_files();

int32_t fun_2670();

void** mode_compile(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8);

int64_t quote(void** rdi, void** rsi, ...);

int32_t mode_adjust(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void* r8);

void fun_28b3(int32_t edi, void** rsi) {
    void** r14_3;
    int32_t ebp4;
    void** rbx5;
    void** rdi6;
    void** rax7;
    void** v8;
    int64_t v9;
    int64_t* rsp10;
    int1_t zf11;
    int32_t eax12;
    int64_t r8_13;
    int64_t rcx14;
    int64_t rdx15;
    void** rsi16;
    int64_t rdi17;
    int1_t zf18;
    void** rdi19;
    int64_t rdx20;
    void* rdx21;
    int32_t eax22;
    void* rsp23;
    int32_t r12d24;
    int32_t eax25;
    void** rax26;
    int64_t rdx27;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r14_3) = 0;
    *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
    ebp4 = edi;
    rbx5 = rsi;
    rdi6 = *reinterpret_cast<void***>(rsi);
    rax7 = g28;
    v8 = rax7;
    v9 = 0;
    set_program_name(rdi6);
    fun_2740(6, 6);
    fun_2540("coreutils", "/usr/local/share/locale");
    fun_2510("coreutils", "/usr/local/share/locale");
    atexit(0x3310, "/usr/local/share/locale");
    rsp10 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_2955_2;
    addr_2a54_3:
    while (zf11 = optind == ebp4, zf11) {
        while (1) {
            fun_2550();
            fun_2780();
            rsp10 = rsp10 - 1 + 1 - 1 + 1;
            do {
                while (1) {
                    eax12 = usage();
                    rsp10 = rsp10 - 1 + 1;
                    while (1) {
                        if (eax12 != 0x76) 
                            break;
                        fun_2550();
                        rsp10 = rsp10 - 1 + 1;
                        while (1) {
                            addr_2955_2:
                            *reinterpret_cast<int32_t*>(&r8_13) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_13) + 4) = 0;
                            rcx14 = 0xea80;
                            rdx15 = reinterpret_cast<int64_t>("pm:vZ");
                            rsi16 = rbx5;
                            *reinterpret_cast<int32_t*>(&rdi17) = ebp4;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
                            eax12 = fun_25a0(rdi17, rsi16, "pm:vZ", 0xea80);
                            rsp10 = rsp10 - 1 + 1;
                            if (eax12 == -1) 
                                goto addr_2a54_3;
                            if (eax12 == 0x6d) {
                                r14_3 = optarg;
                            } else {
                                if (eax12 > 0x6d) {
                                    if (eax12 != 0x70) 
                                        break;
                                    v9 = 0x2d20;
                                } else {
                                    if (eax12 == 0xffffff7e) 
                                        goto addr_2b29_14;
                                    if (eax12 != 90) 
                                        goto addr_29d8_16;
                                    zf18 = optarg == 0;
                                    if (zf18) 
                                        continue;
                                    fun_2550();
                                    fun_2780();
                                    rsp10 = rsp10 - 1 + 1 - 1 + 1;
                                }
                            }
                        }
                    }
                }
                addr_29d8_16:
            } while (eax12 != 0xffffff7d);
            break;
            addr_2b29_14:
            usage();
            rsp10 = rsp10 - 1 + 1;
        }
        rdi19 = stdout;
        rcx14 = Version;
        r8_13 = reinterpret_cast<int64_t>("David MacKenzie");
        rdx15 = reinterpret_cast<int64_t>("GNU coreutils");
        rsi16 = reinterpret_cast<void**>("mkdir");
        version_etc(rdi19, "mkdir", "GNU coreutils", rcx14, "David MacKenzie");
        fun_27e0();
        rsp10 = rsp10 - 1 + 1 - 1 + 1;
    }
    if (!v9) {
        if (!r14_3) {
            addr_2aec_22:
            rdx20 = optind;
            rsi16 = rbx5 + rdx20 * 8;
            savewd_process_files();
            rdx21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
            if (rdx21) {
                fun_2590();
                goto addr_2b5b_24;
            } else {
                return;
            }
        } else {
            eax22 = fun_2670();
            rsp23 = reinterpret_cast<void*>(rsp10 - 1 + 1);
            r12d24 = eax22;
            goto addr_2aa0_27;
        }
    } else {
        eax25 = fun_2670();
        rsp23 = reinterpret_cast<void*>(rsp10 - 1 + 1);
        r12d24 = eax25;
        if (r14_3) {
            addr_2aa0_27:
            rax26 = mode_compile(r14_3, rsi16, rdx15, rcx14, r8_13);
            if (!rax26) {
                addr_2b5b_24:
                quote(r14_3, rsi16, r14_3, rsi16);
                fun_2550();
                fun_2780();
            } else {
                *reinterpret_cast<int32_t*>(&rdx27) = r12d24;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
                mode_adjust(0x1ff, 1, rdx27, rax26, reinterpret_cast<int64_t>(rsp23) - 8 + 8 + 20);
                fun_2470(rax26, rax26);
            }
        }
        fun_2670();
        goto addr_2aec_22;
    }
}

int64_t __libc_start_main = 0;

void fun_2b93() {
    __asm__("cli ");
    __libc_start_main(0x28b0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xf008;

void fun_2440(int64_t rdi);

int64_t fun_2c33() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2440(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2c73() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s3 {
    int64_t f0;
    signed char[8] pad16;
    int32_t f10;
    int32_t f14;
    int64_t f18;
};

uint32_t make_dir_parents(void** rdi, int64_t rsi, int64_t rdx, struct s3* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10);

int64_t fun_2c83(void** rdi, int64_t rsi, struct s3* rdx) {
    int64_t rdx4;
    void*** rax5;
    int64_t rax6;
    int64_t r8_7;
    uint32_t eax8;
    uint32_t eax9;
    void*** rax10;
    int64_t rax11;

    __asm__("cli ");
    rdx4 = rdx->f0;
    if (rdx->f18 && !rdx4) {
        rax5 = fun_2490(rdi);
        rsi = rsi;
        rdi = rdi;
        *rax5 = reinterpret_cast<void**>(95);
        rdx4 = rdx4;
    }
    *reinterpret_cast<int32_t*>(&rax6) = rdx->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_7) = rdx->f10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_7) + 4) = 0;
    eax8 = make_dir_parents(rdi, rsi, rdx4, rdx, r8_7, 0x2de0, rax6, 0xff, 0xff, 1);
    eax9 = eax8 ^ 1;
    if (!*reinterpret_cast<unsigned char*>(&eax9) && (rdx->f18 && rdx->f0)) {
        rax10 = fun_2490(rdi, rdi);
        *rax10 = reinterpret_cast<void**>(95);
    }
    *reinterpret_cast<uint32_t*>(&rax11) = *reinterpret_cast<unsigned char*>(&eax9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    return rax11;
}

struct s4 {
    signed char[8] pad8;
    uint32_t f8;
    uint32_t fc;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

void** fun_24d0(void** rdi, void** rsi);

int64_t quotearg_style(int64_t rdi, void** rsi);

void prog_fprintf(void** rdi, int64_t rsi, int64_t rdx);

int64_t fun_2d23(void** rdi, void** rsi, struct s4* rdx) {
    void*** rax4;
    void** eax5;
    void** r12d6;
    void*** rax7;
    void** r15d8;
    int64_t rax9;
    int64_t rsi10;
    void** rdi11;
    int64_t rax12;

    __asm__("cli ");
    if (rdx->f18) {
        rax4 = fun_2490(rdi);
        *rax4 = reinterpret_cast<void**>(95);
    }
    if (rdx->f8 != rdx->fc) {
        fun_2670();
    }
    eax5 = fun_24d0(rsi, 0x1ff);
    r12d6 = eax5;
    if (rdx->f8 != rdx->fc) {
        rax7 = fun_2490(rsi, rsi);
        r15d8 = *rax7;
        fun_2670();
        *rax7 = r15d8;
    }
    if (!r12d6 && (r12d6 = reinterpret_cast<void**>(rdx->f8 >> 8 & 1), !!rdx->f20)) {
        rax9 = quotearg_style(4, rdi);
        rsi10 = rdx->f20;
        rdi11 = stdout;
        prog_fprintf(rdi11, rsi10, rax9);
    }
    *reinterpret_cast<void***>(&rax12) = r12d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
    return rax12;
}

struct s5 {
    signed char[32] pad32;
    int64_t f20;
};

void fun_2de3(void** rdi, struct s5* rsi) {
    __asm__("cli ");
    if (!rsi->f20) {
        return;
    } else {
        quotearg_style(4, rdi);
        goto prog_fprintf;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2750(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2630(void** rdi, void** rsi, int64_t rdx, void** rcx);

int32_t fun_2650(int64_t rdi);

int32_t fun_24a0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2800(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2e23(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    int32_t eax19;
    void** r13_20;
    void** rax21;
    void** rax22;
    int32_t eax23;
    void** rax24;
    void** rax25;
    void** rax26;
    int32_t eax27;
    void** rax28;
    void** r15_29;
    void** rax30;
    void** rax31;
    void** rax32;
    void** rdi33;
    void** r8_34;
    void** r9_35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2550();
            fun_2750(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2550();
            fun_2630(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2550();
            fun_2630(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2550();
            fun_2630(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2550();
            fun_2630(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2550();
            fun_2630(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2550();
            fun_2630(rax18, r12_17, 5, rcx6);
            do {
                if (1) 
                    break;
                eax19 = fun_2650("mkdir");
            } while (eax19);
            r13_20 = v4;
            if (!r13_20) {
                rax21 = fun_2550();
                fun_2750(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax22 = fun_2740(5);
                if (!rax22 || (eax23 = fun_24a0(rax22, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax23)) {
                    rax24 = fun_2550();
                    r13_20 = reinterpret_cast<void**>("mkdir");
                    fun_2750(1, rax24, "https://www.gnu.org/software/coreutils/", "mkdir");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_20 = reinterpret_cast<void**>("mkdir");
                    goto addr_31a0_9;
                }
            } else {
                rax25 = fun_2550();
                fun_2750(1, rax25, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax26 = fun_2740(5);
                if (!rax26 || (eax27 = fun_24a0(rax26, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax27)) {
                    addr_30a6_11:
                    rax28 = fun_2550();
                    fun_2750(1, rax28, "https://www.gnu.org/software/coreutils/", "mkdir");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_20 == "mkdir")) {
                        r12_2 = reinterpret_cast<void**>(0xbbce);
                    }
                } else {
                    addr_31a0_9:
                    r15_29 = stdout;
                    rax30 = fun_2550();
                    fun_2630(rax30, r15_29, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_30a6_11;
                }
            }
            rax31 = fun_2550();
            rcx6 = r12_2;
            fun_2750(1, rax31, r13_20, rcx6);
            addr_2e7e_14:
            fun_27e0();
        }
    } else {
        rax32 = fun_2550();
        rdi33 = stderr;
        rcx6 = r12_2;
        fun_2800(rdi33, 1, rax32, rcx6, r8_34, r9_35, v36, v37, v38, v39, v40, v41);
        goto addr_2e7e_14;
    }
}

void fun_2710(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

void rpl_vfprintf(void** rdi, int64_t rsi, void* rdx, void** rcx);

void fun_25c0(void** rdi, int64_t rsi, void* rdx, void** rcx);

void fun_31d3(void** rdi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8, int64_t r9) {
    signed char al7;
    void** rax8;
    void** rdi9;
    void* rdx10;
    void** rax11;
    void* rax12;

    __asm__("cli ");
    if (al7) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax8 = g28;
    rdi9 = program_name;
    fun_2630(rdi9, rdi, rdx, rcx);
    fun_2710(": ", 1, 2, rdi);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0xd8 - 8 + 8 - 8 + 8);
    rpl_vfprintf(rdi, rsi, rdx10, rdi);
    rax11 = *reinterpret_cast<void***>(rdi + 40);
    if (reinterpret_cast<unsigned char>(rax11) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48))) {
        fun_25c0(rdi, 10, rdx10, rdi);
    } else {
        *reinterpret_cast<void***>(rdi + 40) = rax11 + 1;
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(10);
    }
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2590();
    } else {
        return;
    }
}

int64_t file_name = 0;

void fun_32f3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3303(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_24b0(int64_t rdi, void** rsi, void* rdx, void** rcx, void** r8);

void fun_3313() {
    void** rdi1;
    int32_t eax2;
    void*** rax3;
    int1_t zf4;
    void*** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    void** rsi11;
    void** r8_12;
    void** rcx13;
    void* rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2490(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*rax3 == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2550();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_33a3_5;
        rax10 = quotearg_colon();
        rsi11 = *rbx5;
        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<void*>("%s: %s");
        fun_2780();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_24b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_33a3_5:
        rsi11 = *rbx5;
        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<void*>("%s");
        fun_2780();
    }
}

void* mkancesdirs(void** rdi, void** rsi, void*** rdx, int64_t rcx);

uint32_t fun_2680(void** rdi, void** rsi);

int32_t savewd_chdir(void** rdi, void** rsi, ...);

int32_t dirchownmod(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

int64_t fun_33c3(void** rdi, void** rsi, void*** rdx, int64_t rcx, uint32_t r8d, int64_t r9, uint32_t a7, uint32_t a8, uint32_t a9, int32_t a10) {
    int64_t r11_11;
    void** r13_12;
    uint32_t r12d13;
    void** rbp14;
    void*** rbx15;
    void* rsp16;
    int32_t v17;
    void** rax18;
    void** v19;
    void** r15d20;
    void** r14_21;
    void* rax22;
    void*** rax23;
    uint32_t ecx24;
    uint32_t v25;
    uint32_t v26;
    void** rdi27;
    void** eax28;
    void* rsp29;
    void** eax30;
    void*** rax31;
    uint32_t eax32;
    int32_t eax33;
    int64_t r9_34;
    int64_t r8_35;
    int64_t rcx36;
    int64_t rdx37;
    void** rdi38;
    int32_t v39;
    int32_t eax40;
    void* rax41;
    void*** rax42;
    void*** rax43;
    int64_t rax44;
    uint32_t v45;

    __asm__("cli ");
    r11_11 = rcx;
    r13_12 = rsi;
    r12d13 = r8d;
    rbp14 = rdi;
    rbx15 = rdx;
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xd8);
    v17 = a10;
    rax18 = g28;
    v19 = rax18;
    if (*reinterpret_cast<void***>(rdi) != 47 && *reinterpret_cast<void***>(rsi) == 4) {
        r15d20 = *reinterpret_cast<void***>(rsi + 4);
        if (r15d20) 
            goto addr_34da_3;
        if (rbx15) 
            goto addr_341e_5;
        goto addr_3540_7;
    }
    if (!rbx15) {
        addr_3540_7:
        r14_21 = rbp14;
    } else {
        addr_341e_5:
        rsi = r13_12;
        rax22 = mkancesdirs(rbp14, rsi, rbx15, r11_11);
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
        if (reinterpret_cast<int64_t>(rax22) < reinterpret_cast<int64_t>(0)) {
            if (!reinterpret_cast<int1_t>(rax22 == 0xffffffffffffffff)) 
                goto addr_35ab_10;
            rax23 = fun_2490(rbp14, rbp14);
            r15d20 = *rax23;
            goto addr_34da_3;
        } else {
            r11_11 = r11_11;
            r14_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp14) + reinterpret_cast<uint64_t>(rax22));
        }
    }
    ecx24 = a8 & a9;
    v25 = ecx24;
    if (ecx24 == 0xffffffff) {
        if (a7 & reinterpret_cast<uint32_t>("strcmp") | r12d13 & 0x200) {
            v26 = r12d13 & 0xffffffed;
        } else {
            *reinterpret_cast<uint32_t*>(&rsi) = r12d13;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rdi27 = r14_21;
            eax28 = fun_24d0(rdi27, rsi);
            rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
            r15d20 = eax28;
            if (!eax28) {
                r9(rbp14, r11_11);
                if (a7 & r12d13 & 0x1ff) 
                    goto addr_37a2_18; else 
                    goto addr_35ab_10;
            }
        }
    } else {
        v26 = r12d13 & 0xffffffc0;
    }
    *reinterpret_cast<uint32_t*>(&rsi) = v26;
    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
    rdi27 = r14_21;
    eax30 = fun_24d0(rdi27, rsi);
    rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
    r15d20 = eax30;
    if (!eax30) {
        r9(rbp14, r11_11);
    } else {
        rax31 = fun_2490(rdi27, rdi27);
        r15d20 = *rax31;
        if (!*reinterpret_cast<signed char*>(&v17)) {
            v26 = 0xffffffff;
        } else {
            if (!r15d20) 
                goto addr_35ab_10;
            *reinterpret_cast<unsigned char*>(&r12d13) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(r15d20 == 2))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rbx15)));
            if (!*reinterpret_cast<unsigned char*>(&r12d13)) 
                goto addr_34da_3;
            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp29) - 8 + 8 + 48);
            eax32 = fun_2680(r14_21, rsi);
            if (!eax32) 
                goto addr_3758_27; else 
                goto addr_35ef_28;
        }
    }
    while (1) {
        eax33 = savewd_chdir(r13_12, r14_21, r13_12, r14_21);
        if (eax33 < -1) {
            addr_35ab_10:
            r12d13 = 1;
        } else {
            *reinterpret_cast<uint32_t*>(&r9_34) = r12d13;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_34) + 4) = 0;
            if (!eax33) {
                r14_21 = reinterpret_cast<void**>(".");
            }
            rsi = r14_21;
            *reinterpret_cast<uint32_t*>(&r8_35) = a9;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_35) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx36) = a8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx36) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdx37) = v26;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx37) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi38) = v39;
            *reinterpret_cast<int32_t*>(&rdi38 + 4) = 0;
            eax40 = dirchownmod(rdi38, rsi, rdx37, rcx36, r8_35, r9_34);
            if (!eax40) 
                goto addr_35ab_10; else 
                goto addr_36e0_33;
        }
        addr_350d_34:
        rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v19) - reinterpret_cast<unsigned char>(g28));
        if (!rax41) 
            break;
        fun_2590();
        addr_37a2_18:
        v26 = r12d13;
        continue;
        addr_36e0_33:
        if (!r15d20) {
            rax42 = fun_2490(rdi38, rdi38);
            rbx15 = rax42;
            goto addr_370d_37;
        }
        if (r15d20 == 2 || (!rbx15 || (rax43 = fun_2490(rdi38, rdi38), rbx15 = rax43, *rax43 == 20))) {
            addr_34da_3:
            quote(rbp14, rsi, rbp14, rsi);
            fun_2550();
            r12d13 = 0;
            fun_2780();
            goto addr_350d_34;
        } else {
            addr_370d_37:
            quote(rbp14, rsi, rbp14, rsi);
            if (v25 != 0xffffffff) {
            }
        }
        fun_2550();
        r12d13 = 0;
        fun_2780();
        goto addr_350d_34;
    }
    *reinterpret_cast<uint32_t*>(&rax44) = r12d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
    return rax44;
    addr_3758_27:
    if ((v45 & 0xf000) != 0x4000) 
        goto addr_34da_3;
    goto addr_350d_34;
    addr_35ef_28:
    if (!reinterpret_cast<int1_t>(r15d20 == 17)) 
        goto addr_34da_3;
    if (*rax31 == 2) 
        goto addr_34da_3;
    if (*rax31 == 20) 
        goto addr_34da_3;
    quote(rbp14, rsi);
    fun_2550();
    r12d13 = 0;
    fun_2780();
    goto addr_350d_34;
}

struct s6 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
};

void** xnmalloc();

void** xmalloc(int64_t rdi, void** rsi);

struct s7 {
    void** f0;
    signed char f1;
    signed char[2] pad4;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
};

void** fun_37c3(struct s6* rdi, void** rsi) {
    struct s6* rbx3;
    int64_t rcx4;
    int32_t eax5;
    struct s6* rdx6;
    void** rax7;
    uint64_t rdi8;
    void** r9_9;
    struct s6* rax10;
    int64_t rbp11;
    int32_t edx12;
    uint32_t ebx13;
    void** rax14;
    int64_t r8_15;
    int32_t r8d16;
    int64_t rax17;
    struct s6* rcx18;
    uint32_t edx19;
    int32_t ebx20;
    struct s7* rdi21;
    uint32_t esi22;
    uint32_t r10d23;
    uint32_t edx24;
    uint64_t r11_25;
    int1_t less_or_equal26;
    int1_t less_or_equal27;
    uint32_t eax28;
    int64_t rdx29;
    int32_t r8d30;

    __asm__("cli ");
    rbx3 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rdi->f0)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
    eax5 = static_cast<int32_t>(rcx4 - 48);
    if (*reinterpret_cast<unsigned char*>(&eax5) > 7) {
        rdx6 = rdi;
        if (*reinterpret_cast<unsigned char*>(&rcx4)) {
            do {
                if (*reinterpret_cast<unsigned char*>(&rcx4) <= 61) {
                }
                *reinterpret_cast<uint32_t*>(&rcx4) = rdx6->f1;
                rdx6 = reinterpret_cast<struct s6*>(&rdx6->f1);
            } while (*reinterpret_cast<unsigned char*>(&rcx4));
        }
        rax7 = xnmalloc();
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        r9_9 = rax7;
        goto addr_382e_8;
    }
    rax10 = rdi;
    *reinterpret_cast<void***>(&rbp11) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp11) + 4) = 0;
    do {
        *reinterpret_cast<void***>(&rbp11) = reinterpret_cast<void**>(static_cast<uint32_t>(rcx4 + rbp11 * 8 - 48));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp11) + 4) = 0;
        rax10 = reinterpret_cast<struct s6*>(&rax10->f1);
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbp11)) > reinterpret_cast<unsigned char>(0xfff)) 
            break;
        *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rax10->f0)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
        edx12 = static_cast<int32_t>(rcx4 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edx12) <= 7);
    goto addr_3ac0_12;
    return 0;
    addr_3ac0_12:
    *reinterpret_cast<int32_t*>(&r9_9) = 0;
    *reinterpret_cast<int32_t*>(&r9_9 + 4) = 0;
    if (!*reinterpret_cast<unsigned char*>(&rcx4)) {
        ebx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbp11)) & reinterpret_cast<uint32_t>("strcmp") | 0x3ff;
        if (reinterpret_cast<int64_t>(rax10) - reinterpret_cast<int64_t>(rbx3) >= 5) {
            ebx13 = 0xfff;
        }
        rax14 = xmalloc(32, rsi);
        *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(0x13d);
        *reinterpret_cast<void***>(rax14 + 4) = reinterpret_cast<void**>(0xfff);
        *reinterpret_cast<void***>(rax14 + 8) = *reinterpret_cast<void***>(&rbp11);
        *reinterpret_cast<uint32_t*>(rax14 + 12) = ebx13;
        *reinterpret_cast<signed char*>(rax14 + 17) = 0;
        return rax14;
    }
    addr_385b_17:
    return r9_9;
    addr_3995_18:
    *reinterpret_cast<uint32_t*>(&r8_15) = *reinterpret_cast<unsigned char*>(&r8d16);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb63c + r8_15 * 4) + 0xb63c;
    while (1) {
        addr_38f0_19:
        *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
        rcx18 = reinterpret_cast<struct s6*>(&rbx3->f2);
        edx19 = 7;
        ebx20 = 3;
        do {
            addr_38a9_20:
            rdi21->f0 = *reinterpret_cast<void***>(&esi22);
            rdi21->f1 = *reinterpret_cast<signed char*>(&ebx20);
            rdi21->f4 = r10d23;
            rdi21->f8 = edx19;
            if (r10d23) {
                edx19 = edx19 & r10d23;
            }
            esi22 = *reinterpret_cast<uint32_t*>(&rax17);
            rbx3 = rcx18;
            while (1) {
                rdi21->fc = edx19;
                ++rdi21;
                edx24 = *reinterpret_cast<uint32_t*>(&rax17) & 0xffffffef;
                if (*reinterpret_cast<signed char*>(&edx24) == 45 || *reinterpret_cast<signed char*>(&rax17) == 43) {
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rbx3->f1)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    ++r11_25;
                    rcx18 = reinterpret_cast<struct s6*>(&rbx3->f1);
                    less_or_equal26 = *reinterpret_cast<signed char*>(&rax17) <= 0x6f;
                    if (*reinterpret_cast<signed char*>(&rax17) == 0x6f) 
                        goto addr_38f0_19;
                } else {
                    if (*reinterpret_cast<signed char*>(&rax17) != 44) 
                        goto addr_3b3a_26;
                    rbx3 = reinterpret_cast<struct s6*>(&rcx18->f1);
                    rdi8 = r11_25;
                    addr_382e_8:
                    esi22 = rbx3->f0;
                    r10d23 = 0;
                    less_or_equal27 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&esi22)) <= reinterpret_cast<signed char>(0x67);
                    if (*reinterpret_cast<void***>(&esi22) != 0x67) 
                        goto addr_383e_28;
                    while (1) {
                        r10d23 = r10d23 | 0x438;
                        rbx3 = reinterpret_cast<struct s6*>(&rbx3->f1);
                        while (esi22 = rbx3->f0, less_or_equal27 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&esi22)) <= reinterpret_cast<signed char>(0x67), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi22) == 0x67)) {
                            addr_383e_28:
                            if (!less_or_equal27) {
                                if (*reinterpret_cast<void***>(&esi22) == 0x6f) {
                                    r10d23 = r10d23 | 0x207;
                                    rbx3 = reinterpret_cast<struct s6*>(&rbx3->f1);
                                } else {
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi22) == 0x75)) 
                                        goto addr_3850_34;
                                    r10d23 = r10d23 | 0x9c0;
                                    rbx3 = reinterpret_cast<struct s6*>(&rbx3->f1);
                                }
                            } else {
                                if (*reinterpret_cast<void***>(&esi22) != 97) 
                                    goto addr_384e_37;
                                r10d23 = 0xfff;
                                rbx3 = reinterpret_cast<struct s6*>(&rbx3->f1);
                            }
                        }
                    }
                    addr_384e_37:
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&esi22)) > reinterpret_cast<signed char>(97)) 
                        goto addr_3850_34;
                    eax28 = esi22 & 0xffffffef;
                    if (*reinterpret_cast<signed char*>(&eax28) == 45) 
                        goto addr_3877_40;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi22) == 43)) 
                        goto addr_3850_34;
                    addr_3877_40:
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rbx3->f1)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    r11_25 = rdi8 + 1;
                    rcx18 = reinterpret_cast<struct s6*>(&rbx3->f1);
                    rdi21 = reinterpret_cast<struct s7*>((rdi8 << 4) + reinterpret_cast<unsigned char>(r9_9));
                    less_or_equal26 = *reinterpret_cast<signed char*>(&rax17) <= 0x6f;
                    if (*reinterpret_cast<signed char*>(&rax17) == 0x6f) 
                        goto addr_38f0_19;
                }
                if (!less_or_equal26) 
                    break;
                if (*reinterpret_cast<signed char*>(&rax17) > 55) 
                    goto addr_3908_45;
                if (*reinterpret_cast<signed char*>(&rax17) <= 47) 
                    goto addr_38a2_47;
                *reinterpret_cast<uint32_t*>(&rdx29) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx29) = static_cast<uint32_t>(rax17 + rdx29 * 8 - 48);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                    rcx18 = reinterpret_cast<struct s6*>(&rcx18->f1);
                    if (*reinterpret_cast<uint32_t*>(&rdx29) > 0xfff) 
                        goto addr_3850_34;
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rcx18->f0)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    r8d30 = static_cast<int32_t>(rax17 - 48);
                } while (*reinterpret_cast<unsigned char*>(&r8d30) <= 7);
                if (r10d23) 
                    goto addr_3850_34;
                if (!*reinterpret_cast<signed char*>(&rax17)) 
                    goto addr_39e0_53;
                if (*reinterpret_cast<signed char*>(&rax17) != 44) 
                    goto addr_3850_34;
                addr_39e0_53:
                rdi21->f0 = *reinterpret_cast<void***>(&esi22);
                rbx3 = rcx18;
                esi22 = *reinterpret_cast<uint32_t*>(&rax17);
                r10d23 = 0xfff;
                rdi21->f8 = *reinterpret_cast<uint32_t*>(&rdx29);
                edx19 = 0xfff;
                rdi21->f1 = 1;
                rdi21->f4 = 0xfff;
            }
            if (*reinterpret_cast<signed char*>(&rax17) == 0x75) {
                *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
                rcx18 = reinterpret_cast<struct s6*>(&rbx3->f2);
                edx19 = 0x1c0;
                ebx20 = 3;
                goto addr_38a9_20;
            }
            ebx20 = 1;
            edx19 = 0;
            continue;
            addr_3908_45:
            if (*reinterpret_cast<signed char*>(&rax17) == 0x67) {
                *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
                rcx18 = reinterpret_cast<struct s6*>(&rbx3->f2);
                edx19 = 56;
                ebx20 = 3;
                goto addr_38a9_20;
            }
            addr_38a2_47:
            ebx20 = 1;
            edx19 = 0;
            goto addr_38a9_20;
            r8d16 = static_cast<int32_t>(rax17 - 88);
        } while (*reinterpret_cast<unsigned char*>(&r8d16) > 32);
        goto addr_3995_18;
    }
    addr_3b3a_26:
    if (*reinterpret_cast<signed char*>(&rax17)) {
        addr_3850_34:
        fun_2470(r9_9);
        *reinterpret_cast<int32_t*>(&r9_9) = 0;
        *reinterpret_cast<int32_t*>(&r9_9 + 4) = 0;
        goto addr_385b_17;
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r9_9) + (r11_25 << 4) + 1) = 0;
        goto addr_385b_17;
    }
}

void** fun_3b63(void** rdi) {
    void** rax2;
    void** rsi3;
    uint32_t eax4;
    void** rax5;
    void** v6;
    void* rdx7;

    __asm__("cli ");
    rax2 = g28;
    rsi3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
    eax4 = fun_2680(rdi, rsi3);
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
    if (!eax4) {
        rax5 = xmalloc(32, rsi3);
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0x13d);
        *reinterpret_cast<void***>(rax5 + 4) = reinterpret_cast<void**>(0xfff);
        *reinterpret_cast<void***>(rax5 + 8) = v6;
        *reinterpret_cast<uint32_t*>(rax5 + 12) = 0xfff;
        *reinterpret_cast<signed char*>(rax5 + 17) = 0;
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax5;
    }
}

struct s8 {
    unsigned char f0;
    unsigned char f1;
    signed char[2] pad4;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
    signed char[1] pad17;
    unsigned char f11;
};

int64_t fun_3be3(uint32_t edi, int32_t esi, int32_t edx, struct s8* rcx, uint32_t* r8) {
    uint32_t r9d6;
    uint32_t edi7;
    int64_t rax8;
    int32_t r11d9;
    uint32_t r10d10;
    uint32_t edx11;
    uint32_t eax12;
    uint32_t esi13;
    uint32_t ebx14;
    uint32_t eax15;
    uint32_t edi16;
    uint32_t edi17;
    uint32_t edi18;
    uint32_t r12d19;
    uint32_t edi20;
    uint32_t eax21;
    uint32_t eax22;
    int64_t rax23;

    __asm__("cli ");
    r9d6 = edi & 0xfff;
    edi7 = rcx->f1;
    if (!*reinterpret_cast<signed char*>(&edi7)) {
        if (r8) {
            *r8 = 0;
        }
        *reinterpret_cast<uint32_t*>(&rax8) = r9d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
    r11d9 = esi;
    r10d10 = 0;
    edx11 = reinterpret_cast<uint32_t>(~edx);
    while (1) {
        eax12 = rcx->f8;
        if (*reinterpret_cast<signed char*>(&r11d9)) {
            esi13 = rcx->fc | 0xfffff3ff;
            ebx14 = ~rcx->fc & reinterpret_cast<uint32_t>("strcmp");
            if (*reinterpret_cast<signed char*>(&edi7) != 2) {
                addr_3c61_8:
                if (*reinterpret_cast<signed char*>(&edi7) == 3) {
                    eax15 = eax12 & r9d6;
                    edi16 = eax15 & 0x124;
                    edi17 = -edi16;
                    edi18 = edi17 - (edi17 + reinterpret_cast<uint1_t>(edi17 < edi17 + reinterpret_cast<uint1_t>(!!edi16))) & 0x124;
                    r12d19 = edi18;
                    *reinterpret_cast<unsigned char*>(&r12d19) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d19) | 0x92);
                    if (*reinterpret_cast<unsigned char*>(&eax15) & 0x92) {
                        edi18 = r12d19;
                    }
                    if (*reinterpret_cast<unsigned char*>(&eax15) & 73) {
                        edi18 = edi18 | 73;
                    }
                    eax12 = eax15 | edi18;
                }
            } else {
                addr_3d15_14:
                eax12 = eax12 | 73;
                esi13 = ~ebx14;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&edi7) == 2) {
                ebx14 = r9d6 & 73;
                if (ebx14) {
                    ebx14 = 0;
                    goto addr_3d15_14;
                } else {
                    esi13 = 0xffffffff;
                }
            } else {
                esi13 = 0xffffffff;
                ebx14 = 0;
                goto addr_3c61_8;
            }
        }
        edi20 = rcx->f0;
        eax21 = eax12 & esi13;
        if (!rcx->f4) {
            eax22 = eax21 & edx11;
            if (*reinterpret_cast<signed char*>(&edi20) == 45) {
                r10d10 = r10d10 | eax22;
                r9d6 = r9d6 & ~eax22;
            } else {
                if (*reinterpret_cast<signed char*>(&edi20) == 61) {
                    addr_3cbf_24:
                    edi7 = rcx->f11;
                    rcx = reinterpret_cast<struct s8*>(&rcx->pad17);
                    r10d10 = r10d10 | esi13 & 0xfff;
                    r9d6 = ebx14 & r9d6 | eax22;
                    if (*reinterpret_cast<signed char*>(&edi7)) 
                        continue; else 
                        break;
                } else {
                    addr_3c26_25:
                    if (*reinterpret_cast<signed char*>(&edi20) == 43) {
                        r10d10 = r10d10 | eax22;
                        r9d6 = r9d6 | eax22;
                    }
                }
            }
            edi7 = rcx->f11;
            rcx = reinterpret_cast<struct s8*>(&rcx->pad17);
            if (!*reinterpret_cast<signed char*>(&edi7)) 
                break;
        } else {
            eax22 = eax21 & rcx->f4;
            if (*reinterpret_cast<signed char*>(&edi20) != 45) {
                if (*reinterpret_cast<signed char*>(&edi20) != 61) 
                    goto addr_3c26_25;
                ebx14 = ebx14 | ~rcx->f4;
                esi13 = ~ebx14;
                goto addr_3cbf_24;
            }
        }
    }
    if (r8) 
        goto addr_3ce6_32;
    addr_3ce9_33:
    *reinterpret_cast<uint32_t*>(&rax23) = r9d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
    return rax23;
    addr_3ce6_32:
    *r8 = r10d10;
    goto addr_3ce9_33;
}

uint64_t fun_27f0(void** rdi, void* rsi, uint64_t rdx, void** rcx);

struct s9 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s9* fun_25d0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_3d83(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s9* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_27f0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2480("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_25d0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_24a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(void** rdi, int64_t rsi);

void fun_5523(void** rdi) {
    void** rbp2;
    void*** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2490(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<void**>(0xf200);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5563(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5583(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf200);
    }
    *rdi = esi;
    return 0xf200;
}

int64_t fun_55a3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xf200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s10 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_55e3(struct s10* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s10*>(0xf200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s11 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s11* fun_5603(struct s11* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s11*>(0xf200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x286a;
    if (!rdx) 
        goto 0x286a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xf200;
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5643(void** rdi, void** rsi, void** rdx, int64_t rcx, struct s12* r8) {
    struct s12* rbx6;
    void*** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s12*>(0xf200);
    }
    rax7 = fun_2490(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5676);
    *rax7 = r15d8;
    return rax13;
}

struct s13 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_56c3(void** rdi, int64_t rsi, void*** rdx, struct s13* rcx) {
    struct s13* rbx5;
    void*** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s13*>(0xf200);
    }
    rax6 = fun_2490(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x56f1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x574c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_57b3() {
    __asm__("cli ");
}

void** gf078 = reinterpret_cast<void**>(0);

int64_t slotvec0 = 0x100;

void fun_57c3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2470(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xf100) {
        fun_2470(rdi7);
        gf078 = reinterpret_cast<void**>(0xf100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xf070) {
        fun_2470(r12_2);
        slotvec = reinterpret_cast<void**>(0xf070);
    }
    nslots = 1;
    return;
}

void fun_5863() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5883() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5893(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_58b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_58d3(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2870;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_5963(void** rdi, int32_t esi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2875;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void** fun_59f3(int32_t edi, void** rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x287a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2590();
    } else {
        return rax5;
    }
}

void** fun_5a83(int32_t edi, void** rsi, int64_t rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x287f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_5b13(void** rdi, int64_t rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x96e0]");
    __asm__("movdqa xmm1, [rip+0x96e8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x96d1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2590();
    } else {
        return rax10;
    }
}

void** fun_5bb3(void** rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x9640]");
    __asm__("movdqa xmm1, [rip+0x9648]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x9631]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2590();
    } else {
        return rax9;
    }
}

void** fun_5c53(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x95a0]");
    __asm__("movdqa xmm1, [rip+0x95a8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x9589]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2590();
    } else {
        return rax3;
    }
}

void** fun_5ce3(void** rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x9510]");
    __asm__("movdqa xmm1, [rip+0x9518]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x9506]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2590();
    } else {
        return rax4;
    }
}

void** fun_5d73(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2884;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_5e13(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x93da]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x93d2]");
    __asm__("movdqa xmm2, [rip+0x93da]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2889;
    if (!rdx) 
        goto 0x2889;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void** fun_5eb3(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x933a]");
    __asm__("movdqa xmm1, [rip+0x9342]");
    __asm__("movdqa xmm2, [rip+0x934a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x288e;
    if (!rdx) 
        goto 0x288e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2590();
    } else {
        return rax9;
    }
}

void** fun_5f63(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x928a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x9282]");
    __asm__("movdqa xmm2, [rip+0x928a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2893;
    if (!rsi) 
        goto 0x2893;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_6003(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x91ea]");
    __asm__("movdqa xmm1, [rip+0x91f2]");
    __asm__("movdqa xmm2, [rip+0x91fa]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2898;
    if (!rsi) 
        goto 0x2898;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void fun_60a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_60b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_60d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_60f3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6113(int32_t* rdi) {
    __asm__("cli ");
    *rdi = 0;
    return;
}

struct s14 {
    int32_t f0;
    int32_t f4;
};

int64_t fun_6123(struct s14* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (rdi->f0 == 4) {
        *reinterpret_cast<int32_t*>(&rax2) = rdi->f4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
        return rax2;
    } else {
        return 0;
    }
}

void** fun_2520(void** rdi);

void** fun_2840(void** rdi, ...);

void** fun_2580(void** rdi, void** rsi, void* rdx, void** rcx);

void** fun_25f0(void** rdi, void** rsi, void* rdx, void** rcx);

int64_t fun_2700(void** rdi, void** rsi, void* rdx, void** rcx);

int64_t fun_2610(void** rdi, void** rsi, ...);

void** open_safer(void** rdi);

int64_t fun_6143(void** rdi, void** rsi, void** edx, void** rcx) {
    void* rdx3;
    void** r14_5;
    void** r12_6;
    void** rbx7;
    void** ebp8;
    void** eax9;
    void** eax10;
    void*** rax11;
    void*** rax12;
    void** r13d13;
    void** eax14;
    int32_t eax15;
    int64_t rax16;
    void** eax17;
    void** eax18;
    void*** rax19;
    int64_t rax20;
    void*** rax21;
    void** rdi22;
    void** r12d23;
    void** eax24;
    void*** rax25;

    *reinterpret_cast<void***>(&rdx3) = edx;
    __asm__("cli ");
    r14_5 = rsi;
    r12_6 = rdi;
    rbx7 = rcx;
    if (!rcx) {
        ebp8 = reinterpret_cast<void**>(0xffffffff);
        if (!(*reinterpret_cast<unsigned char*>(&rdx3) & 1)) 
            goto addr_627e_3;
        rdi = r14_5;
        *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rdx3)) << 17 & 0x20000 | 0x10900;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax9 = fun_2520(rdi);
        ebp8 = eax9;
    } else {
        rdi = r14_5;
        *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rdx3)) << 17 & 0x20000 | 0x10900;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax10 = fun_2520(rdi);
        *reinterpret_cast<void***>(rbx7) = eax10;
        ebp8 = eax10;
        rax11 = fun_2490(rdi);
        *reinterpret_cast<void***>(rbx7 + 4) = *rax11;
    }
    if (reinterpret_cast<signed char>(ebp8) < reinterpret_cast<signed char>(0)) {
        rax12 = fun_2490(rdi);
        if (!reinterpret_cast<int1_t>(*rax12 == 13)) {
            r13d13 = reinterpret_cast<void**>(0xffffffff);
        } else {
            addr_627e_3:
            if (*reinterpret_cast<void***>(r12_6) == 3) {
                *reinterpret_cast<void***>(&rdx3) = *reinterpret_cast<void***>(r12_6 + 4);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
                if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rdx3)) < reinterpret_cast<signed char>(0)) {
                    eax14 = fun_2840(rdi);
                    *reinterpret_cast<void***>(r12_6 + 4) = eax14;
                    if (eax14) 
                        goto addr_6400_11;
                    goto addr_61e0_13;
                }
            }
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rdx3)) & 2) {
            addr_6218_15:
            *reinterpret_cast<unsigned char*>(&eax15) = reinterpret_cast<uint1_t>(rbx7 == 0);
            r13d13 = reinterpret_cast<void**>(0);
            goto addr_6221_16;
        } else {
            if (*reinterpret_cast<void***>(r12_6) == 3) {
                if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_6 + 4)) >= reinterpret_cast<signed char>(0)) 
                    goto addr_6258_19;
                eax14 = fun_2840(rdi);
                *reinterpret_cast<void***>(r12_6 + 4) = eax14;
                if (eax14) 
                    goto addr_6400_11;
                goto addr_6258_19;
            }
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_6)) > reinterpret_cast<unsigned char>(3)) 
                goto addr_6250_23; else 
                goto addr_61ad_24;
        }
    }
    addr_623e_25:
    *reinterpret_cast<void***>(&rax16) = r13d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    return rax16;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_6)) > reinterpret_cast<unsigned char>(3)) {
        if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r12_6) - 4) <= 1) {
            addr_61e0_13:
            while (rdi = r14_5, eax17 = fun_2580(rdi, rsi, rdx3, rcx), r13d13 = eax17, !eax17) {
                do {
                    eax18 = *reinterpret_cast<void***>(r12_6);
                    if (eax18 != 3) 
                        goto addr_61fc_29;
                    r13d13 = *reinterpret_cast<void***>(r12_6 + 4);
                    if (!r13d13) 
                        goto addr_6266_31;
                    rcx = reinterpret_cast<void**>("savewd_chdir");
                    *reinterpret_cast<void***>(&rdx3) = reinterpret_cast<void**>(0x94);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
                    rsi = reinterpret_cast<void**>("lib/savewd.c");
                    rdi = reinterpret_cast<void**>("wd->val.child == 0");
                    fun_25f0("wd->val.child == 0", "lib/savewd.c", 0x94, "savewd_chdir");
                    addr_63e0_33:
                    *reinterpret_cast<void***>(r12_6) = reinterpret_cast<void**>(0xffffffff00000003);
                    eax14 = fun_2840(rdi, rdi);
                    *reinterpret_cast<void***>(r12_6 + 4) = eax14;
                    if (eax14) {
                        addr_6400_11:
                        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax14) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(eax14 == 0))) 
                            goto addr_6420_34;
                        *reinterpret_cast<void***>(r12_6) = reinterpret_cast<void**>(4);
                        rax19 = fun_2490(rdi, rdi);
                        *reinterpret_cast<void***>(r12_6 + 4) = *rax19;
                    }
                    addr_61da_36:
                    if (reinterpret_cast<signed char>(ebp8) < reinterpret_cast<signed char>(0)) 
                        break;
                    addr_6258_19:
                    rdi = ebp8;
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    rax20 = fun_2700(rdi, rsi, rdx3, rcx);
                    r13d13 = *reinterpret_cast<void***>(&rax20);
                } while (!*reinterpret_cast<void***>(&rax20));
                goto addr_6266_31;
            }
            goto addr_623e_25;
        } else {
            addr_62ac_38:
            rsi = reinterpret_cast<void**>("lib/savewd.c");
            rdi = reinterpret_cast<void**>("0");
            eax18 = fun_25f0("0", "lib/savewd.c", 92, "savewd_save");
            goto addr_62d0_39;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_6)) {
            goto addr_61e0_13;
        }
    }
    addr_61fc_29:
    if (reinterpret_cast<unsigned char>(eax18) > reinterpret_cast<unsigned char>(3)) {
        while (reinterpret_cast<uint32_t>(eax18 - 4) > 1) {
            addr_62d9_44:
            rsi = reinterpret_cast<void**>("lib/savewd.c");
            rdi = reinterpret_cast<void**>("0");
            eax18 = fun_25f0("0", "lib/savewd.c", 0x98, "savewd_chdir");
        }
        goto addr_6218_15;
    } else {
        if (!reinterpret_cast<int1_t>(eax18 == 1)) {
            addr_62d0_39:
            if (eax18 == 2) 
                goto addr_6218_15; else 
                goto addr_62d9_44;
        } else {
            *reinterpret_cast<void***>(r12_6) = reinterpret_cast<void**>(2);
            goto addr_6218_15;
        }
    }
    addr_6266_31:
    *reinterpret_cast<unsigned char*>(&eax15) = reinterpret_cast<uint1_t>(rbx7 == 0);
    addr_6221_16:
    if (reinterpret_cast<signed char>(ebp8) >= reinterpret_cast<signed char>(0) && *reinterpret_cast<unsigned char*>(&eax15)) {
        rax21 = fun_2490(rdi, rdi);
        rdi22 = ebp8;
        *reinterpret_cast<int32_t*>(&rdi22 + 4) = 0;
        r12d23 = *rax21;
        fun_2610(rdi22, rsi, rdi22, rsi);
        *rax21 = r12d23;
        goto addr_623e_25;
    }
    addr_6420_34:
    eax15 = 1;
    r13d13 = reinterpret_cast<void**>(0xfffffffe);
    goto addr_6221_16;
    addr_61b5_48:
    *reinterpret_cast<uint32_t*>(&rsi) = 0;
    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
    rdi = reinterpret_cast<void**>(".");
    eax24 = open_safer(".");
    if (reinterpret_cast<signed char>(eax24) < reinterpret_cast<signed char>(0)) {
        rax25 = fun_2490(".");
        *reinterpret_cast<void***>(&rdx3) = *rax25;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
        if (*reinterpret_cast<void***>(&rdx3) == 13) 
            goto addr_63e0_33;
        if (*reinterpret_cast<void***>(&rdx3) == 0x74) 
            goto addr_63e0_33;
        *reinterpret_cast<void***>(r12_6) = reinterpret_cast<void**>(4);
        *reinterpret_cast<void***>(r12_6 + 4) = *rax25;
        goto addr_61da_36;
    } else {
        *reinterpret_cast<void***>(r12_6) = reinterpret_cast<void**>(1);
        *reinterpret_cast<void***>(r12_6 + 4) = eax24;
        goto addr_61da_36;
    }
    addr_6250_23:
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r12_6) - 4) > 1) 
        goto addr_62ac_38; else 
        goto addr_6258_19;
    addr_61ad_24:
    if (*reinterpret_cast<void***>(r12_6)) 
        goto addr_6258_19; else 
        goto addr_61b5_48;
}

int32_t fun_2790(void** rdi);

void fun_2460();

int64_t fun_6443(void** rdi, void** rsi, void* rdx, void** rcx, void** r8) {
    void** rbx6;
    void** rax7;
    void** v8;
    void** ebp9;
    uint1_t zf10;
    int64_t rdi11;
    void** r12_12;
    void** rdi13;
    int32_t eax14;
    void*** rax15;
    void** r12d16;
    void*** rax17;
    void*** rbp18;
    int64_t rax19;
    void** rdi20;
    void*** rax21;
    void** rdi22;
    uint32_t eax23;
    uint32_t v24;
    uint32_t v25;

    __asm__("cli ");
    rbx6 = rdi;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<void***>(rdi) == 3) {
        ebp9 = *reinterpret_cast<void***>(rdi + 4);
        zf10 = reinterpret_cast<uint1_t>(ebp9 == 0);
        if (zf10) {
            addr_6596_3:
            *reinterpret_cast<int32_t*>(&rdi11) = *reinterpret_cast<int32_t*>(&rsi);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
            fun_24b0(rdi11, rsi, rdx, rcx, r8);
        } else {
            r12_12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16 + 4);
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(ebp9) < reinterpret_cast<signed char>(0)) | zf10)) {
                do {
                    rsi = r12_12;
                    rdi13 = ebp9;
                    *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
                    eax14 = fun_2790(rdi13);
                    if (eax14 >= 0) 
                        break;
                    rax15 = fun_2490(rdi13);
                } while (reinterpret_cast<int1_t>(*rax15 == 4));
                goto addr_6553_7;
            } else {
                goto addr_64c8_9;
            }
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) > reinterpret_cast<unsigned char>(3)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 4)) {
                addr_6572_12:
                rcx = reinterpret_cast<void**>("savewd_restore");
                *reinterpret_cast<int32_t*>(&rdx) = 0xdb;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx) + 4) = 0;
                rsi = reinterpret_cast<void**>("lib/savewd.c");
                fun_25f0("0", "lib/savewd.c", 0xdb, "savewd_restore");
                goto addr_6591_13;
            } else {
                r12d16 = *reinterpret_cast<void***>(rdi + 4);
                rax17 = fun_2490(rdi);
                rbp18 = rax17;
                goto addr_649d_15;
            }
        } else {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 2)) {
                addr_64c8_9:
                *reinterpret_cast<uint32_t*>(&rax19) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
                goto addr_64ca_17;
            } else {
                rdi20 = *reinterpret_cast<void***>(rdi + 4);
                *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0;
                rax19 = fun_2700(rdi20, rsi, rdx, rcx);
                if (*reinterpret_cast<uint32_t*>(&rax19)) {
                    rax21 = fun_2490(rdi20);
                    rdi22 = *reinterpret_cast<void***>(rbx6 + 4);
                    *reinterpret_cast<int32_t*>(&rdi22 + 4) = 0;
                    r12d16 = *rax21;
                    rbp18 = rax21;
                    fun_2610(rdi22, rsi);
                    *reinterpret_cast<void***>(rbx6) = reinterpret_cast<void**>(4);
                    *reinterpret_cast<void***>(rbx6 + 4) = r12d16;
                    goto addr_649d_15;
                } else {
                    *reinterpret_cast<void***>(rbx6) = reinterpret_cast<void**>(1);
                    goto addr_64ca_17;
                }
            }
        }
    }
    eax23 = v24;
    *reinterpret_cast<void***>(rbx6 + 4) = reinterpret_cast<void**>(0xffffffff);
    if (eax23 & 0x7f) {
        fun_2460();
        eax23 = v25;
    }
    *reinterpret_cast<uint32_t*>(&rax19) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax23) + 1);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
    addr_64ca_17:
    rdx = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rdx) {
        return rax19;
    }
    addr_6591_13:
    fun_2590();
    goto addr_6596_3;
    addr_6553_7:
    fun_25f0("(*__errno_location ()) == 4", "lib/savewd.c", 0xd1, "savewd_restore");
    goto addr_6572_12;
    addr_649d_15:
    *rbp18 = r12d16;
    *reinterpret_cast<uint32_t*>(&rax19) = 0xffffffff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
    goto addr_64ca_17;
}

int64_t fun_65a3(void** rdi, void** rsi) {
    int64_t rax3;
    void** rbx4;
    void** rdi5;

    __asm__("cli ");
    *reinterpret_cast<void***>(&rax3) = *reinterpret_cast<void***>(rdi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rbx4 = rdi;
    if (*reinterpret_cast<void***>(&rax3) != 3) {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax3)) > reinterpret_cast<unsigned char>(3)) {
            if (*reinterpret_cast<void***>(&rax3) == 4) {
                addr_65bd_4:
                *reinterpret_cast<void***>(rbx4) = reinterpret_cast<void**>(5);
                return rax3;
            } else {
                rdi = reinterpret_cast<void**>("0");
                fun_25f0("0", "lib/savewd.c", 0xf4, "savewd_finish");
            }
        } else {
            if (*reinterpret_cast<void***>(&rax3)) {
                rdi5 = *reinterpret_cast<void***>(rdi + 4);
                *reinterpret_cast<int32_t*>(&rdi5 + 4) = 0;
                rax3 = fun_2610(rdi5, rsi);
                goto addr_65bd_4;
            }
        }
    }
    *reinterpret_cast<void***>(&rax3) = *reinterpret_cast<void***>(rdi + 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax3)) >= reinterpret_cast<signed char>(0)) {
        fun_25f0("wd->val.child < 0", "lib/savewd.c", 0xf0, "savewd_finish");
    }
}

struct s15 {
    signed char* f0;
    signed char* f8;
};

int32_t savewd_restore(struct s15* rdi, struct s15* rsi, signed char* rdx);

void savewd_finish(struct s15* rdi, struct s15* rsi, signed char* rdx);

int64_t fun_6623(int64_t rdi, struct s15* rsi, signed char* rdx, signed char* rcx) {
    signed char* r12_5;
    signed char* rbp6;
    void* rsp7;
    int32_t v8;
    struct s15* v9;
    void** rax10;
    void** v11;
    int32_t eax12;
    int32_t v13;
    int64_t r14_14;
    signed char** rax15;
    int32_t r15d16;
    struct s15* rbx17;
    int64_t rax18;
    struct s15* r13_19;
    struct s15* v20;
    signed char* rdi21;
    int32_t eax22;
    int32_t v23;
    int32_t eax24;
    int64_t rax25;
    int64_t rdx26;
    signed char** r13_27;
    signed char** r14_28;
    signed char* rdi29;
    int32_t eax30;
    void* rax31;
    int64_t rax32;

    __asm__("cli ");
    r12_5 = rcx;
    rbp6 = rdx;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    v8 = *reinterpret_cast<int32_t*>(&rdi);
    v9 = rsi;
    rax10 = g28;
    v11 = rax10;
    eax12 = static_cast<int32_t>(rdi - 1);
    v13 = eax12;
    if (eax12 < 0) 
        goto addr_6710_2;
    *reinterpret_cast<int32_t*>(&r14_14) = eax12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_14) + 4) = 0;
    rax15 = reinterpret_cast<signed char**>(reinterpret_cast<int64_t>(rsi) + eax12 * 8);
    do {
        rdx = *rax15;
        if (*rdx != 47) 
            break;
        *reinterpret_cast<int32_t*>(&r14_14) = *reinterpret_cast<int32_t*>(&r14_14) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_14) + 4) = 0;
        --rax15;
    } while (*reinterpret_cast<int32_t*>(&r14_14) != -1);
    goto addr_6710_2;
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r14_14) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r14_14) == 0)) {
        addr_6710_2:
        r15d16 = 0;
        *reinterpret_cast<int32_t*>(&r14_14) = 0;
        rbx17 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(rsp7) + 32);
    } else {
        *reinterpret_cast<int32_t*>(&rax18) = static_cast<int32_t>(r14_14 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
        r15d16 = 0;
        rbx17 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(rsp7) + 32);
        r13_19 = v9;
        v20 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(v9) + rax18 * 8 + 8);
        while (1) {
            if (1) {
                addr_66c0_9:
                rdi21 = r13_19->f0;
                rdx = r12_5;
                rsi = rbx17;
                eax22 = reinterpret_cast<int32_t>(rbp6(rdi21, rsi, rdx));
                if (r15d16 < eax22) {
                    r15d16 = eax22;
                    goto addr_66d3_11;
                }
            } else {
                if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(v23 < 0) | reinterpret_cast<uint1_t>(v23 == 0))) {
                    addr_66d3_11:
                    if (*r13_19->f8 != 47 && (*reinterpret_cast<int32_t*>(&rsi) = r15d16, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi) + 4) = 0, eax24 = savewd_restore(rbx17, rsi, rdx), r15d16 < eax24)) {
                        r15d16 = eax24;
                    }
                } else {
                    goto addr_66c0_9;
                }
            }
            r13_19 = reinterpret_cast<struct s15*>(&r13_19->f8);
            if (r13_19 == v20) 
                break;
        }
    }
    savewd_finish(rbx17, rsi, rdx);
    if (v8 > *reinterpret_cast<int32_t*>(&r14_14)) {
        rax25 = *reinterpret_cast<int32_t*>(&r14_14);
        *reinterpret_cast<int32_t*>(&rdx26) = v13 - *reinterpret_cast<int32_t*>(&r14_14);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
        r13_27 = reinterpret_cast<signed char**>(reinterpret_cast<int64_t>(v9) + rax25 * 8);
        r14_28 = reinterpret_cast<signed char**>(reinterpret_cast<int64_t>(v9) + (rdx26 + rax25) * 8 + 8);
        do {
            rdi29 = *r13_27;
            eax30 = reinterpret_cast<int32_t>(rbp6(rdi29, rbx17, r12_5));
            if (r15d16 < eax30) {
                r15d16 = eax30;
            }
            ++r13_27;
        } while (r13_27 != r14_28);
    }
    rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax31) {
        fun_2590();
    } else {
        *reinterpret_cast<int32_t*>(&rax32) = r15d16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
        return rax32;
    }
}

struct s16 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2660(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_67a3(void** rdi, void** rsi, void** rdx, void** rcx, struct s16* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2800(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2800(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2550();
    fun_2800(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2660(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2550();
    fun_2800(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2660(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2550();
        fun_2800(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xbe78 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xbe78;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6c13() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s17 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6c33(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s17* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s17* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2590();
    } else {
        return;
    }
}

void fun_6cd3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6d76_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6d80_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2590();
    } else {
        return;
    }
    addr_6d76_5:
    goto addr_6d80_7;
}

void fun_6db3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2660(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2550();
    fun_2750(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2550();
    fun_2750(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2550();
    goto fun_2750;
}

int64_t fun_24f0();

void xalloc_die();

void fun_6e53(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_26b0(void** rdi, void** rsi, ...);

void fun_6e93(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6eb3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6ed3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2730(void** rdi, void** rsi, ...);

void fun_6ef3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2730(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6f23(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2730(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6f53(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6f93() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6fd3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7003(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7053(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24f0();
            if (rax5) 
                break;
            addr_709d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_709d_5;
        rax8 = fun_24f0();
        if (rax8) 
            goto addr_7086_9;
        if (rbx4) 
            goto addr_709d_5;
        addr_7086_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_70e3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24f0();
            if (rax8) 
                break;
            addr_712a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_712a_5;
        rax11 = fun_24f0();
        if (rax11) 
            goto addr_7112_9;
        if (!rbx6) 
            goto addr_7112_9;
        if (r12_4) 
            goto addr_712a_5;
        addr_7112_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7173(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_721d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_7230_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_71d0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_71f6_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7244_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_71ed_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_7244_14;
            addr_71ed_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7244_14;
            addr_71f6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2730(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7244_14;
            if (!rbp13) 
                break;
            addr_7244_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_721d_9;
        } else {
            if (!r13_6) 
                goto addr_7230_10;
            goto addr_71d0_11;
        }
    }
}

int64_t fun_2640();

void fun_7273() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2690(void** rdi, void** rsi, void** rdx);

void fun_7313(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2690;
    }
}

void fun_7353(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2690;
    }
}

void fun_7393(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2690;
    }
}

void** fun_2570(void** rdi, ...);

void fun_73d3(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_2570(rdi);
    rax4 = fun_26b0(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2690;
    }
}

void fun_7413() {
    void** rdi1;

    __asm__("cli ");
    fun_2550();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2780();
    fun_2480(rdi1);
}

void** vasnprintf(void** rdi, void* rsi, uint64_t rdx, void** rcx);

void fseterr(void** rdi, void* rsi, uint64_t rdx, void** rcx);

int64_t fun_7453(void** rdi, uint64_t rsi, void** rdx) {
    void** rcx4;
    uint64_t rdx5;
    void* rsp6;
    void** rax7;
    void** r13_8;
    void* rsi9;
    void** rax10;
    int64_t rax11;
    void** rdi12;
    uint64_t rax13;
    void* rdx14;
    void*** rax15;

    __asm__("cli ");
    rcx4 = rdx;
    rdx5 = rsi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x808);
    rax7 = g28;
    r13_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    rsi9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 24);
    rax10 = vasnprintf(r13_8, rsi9, rdx5, rcx4);
    if (!rax10) {
        addr_7527_2:
        fseterr(rdi, rsi9, rdx5, rcx4);
        *reinterpret_cast<int32_t*>(&rax11) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    } else {
        rcx4 = rdi;
        rdx5 = reinterpret_cast<uint64_t>(".");
        *reinterpret_cast<int32_t*>(&rsi9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rdi12 = rax10;
        rax13 = fun_27f0(rdi12, 1, ".", rcx4);
        if (rax13 < reinterpret_cast<uint64_t>(".")) {
            *reinterpret_cast<int32_t*>(&rax11) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (rax10 != r13_8) {
                fun_2470(rax10, rax10);
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
        } else {
            if (rax10 != r13_8) {
                rdi12 = rax10;
                fun_2470(rdi12, rdi12);
            }
            if (0) 
                goto addr_751c_9; else 
                goto addr_74da_10;
        }
    }
    addr_74dc_11:
    rdx14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx14) {
        fun_2590();
    } else {
        return rax11;
    }
    addr_751c_9:
    rax15 = fun_2490(rdi12, rdi12);
    *rax15 = reinterpret_cast<void**>(75);
    goto addr_7527_2;
    addr_74da_10:
    *reinterpret_cast<int32_t*>(&rax11) = reinterpret_cast<int32_t>(".");
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    goto addr_74dc_11;
}

int64_t fun_24c0();

int64_t rpl_fclose(void** rdi);

int64_t fun_7543(void** rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void*** rax5;
    void*** rax6;

    __asm__("cli ");
    rax2 = fun_24c0();
    ebx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_759e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2490(rdi);
            *rax5 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_759e_3;
            rax6 = fun_2490(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*rax6 == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_26a0(void** rdi);

int32_t fun_26f0(void** rdi);

int64_t fun_25e0(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_2530(void** rdi);

int64_t fun_75b3(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void*** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_26a0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_26f0(rdi);
        if (!(eax3 && (eax4 = fun_26a0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_25e0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2490(rdi, rdi);
            r12d9 = *rax8;
            rax10 = fun_2530(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2530;
}

int32_t fun_27a0();

void fd_safer(int64_t rdi);

void fun_7643() {
    void** rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_27a0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2590();
    } else {
        return;
    }
}

void rpl_fseeko(void** rdi);

void fun_76c3(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_26f0(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7713(void** rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax4 = fun_26a0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_25e0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_7793(uint32_t* rdi) {
    __asm__("cli ");
    *rdi = *rdi | 32;
    return;
}

signed char* fun_26d0(int64_t rdi);

signed char* fun_77a3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_26d0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_25b0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_77e3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_25b0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2590();
    } else {
        return r12_7;
    }
}

struct s18 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

void* fun_7873(void** rdi, void** rsi, int64_t rdx, int64_t rcx) {
    int64_t v5;
    uint32_t edx6;
    void** v7;
    int64_t v8;
    void* rax9;
    void** r15_10;
    struct s18* rbx11;
    void** r12_12;
    void** r13_13;
    uint32_t eax14;
    void** rbp15;
    void* rdx16;
    int32_t eax17;
    void** v18;
    void*** rax19;
    int32_t eax20;
    void*** rax21;

    __asm__("cli ");
    v5 = rdx;
    edx6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    v7 = rsi;
    v8 = rcx;
    if (!*reinterpret_cast<signed char*>(&edx6)) {
        *reinterpret_cast<int32_t*>(&rax9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    } else {
        r15_10 = rdi;
        rbx11 = reinterpret_cast<struct s18*>(rdi + 2);
        r12_12 = rdi;
        *reinterpret_cast<int32_t*>(&r13_13) = 0;
        *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
        while (1) {
            eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx11) + 0xffffffffffffffff));
            rbp15 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx11) + 0xffffffffffffffff);
            if (*reinterpret_cast<signed char*>(&eax14) != 47) {
                if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&eax14))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edx6) == 47))) {
                    if (r13_13) {
                        rdx16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_13) - reinterpret_cast<unsigned char>(r12_12));
                        if (rdx16 == 1) {
                            if (*reinterpret_cast<void***>(r12_12) == 46) {
                                r12_12 = rbp15;
                            } else {
                                *reinterpret_cast<void***>(r13_13) = reinterpret_cast<void**>(0);
                                goto addr_790c_11;
                            }
                        } else {
                            *reinterpret_cast<void***>(r13_13) = reinterpret_cast<void**>(0);
                            if (rdx16 != 2 || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_12) == 46) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_12 + 1) == 46))) {
                                addr_790c_11:
                                eax17 = reinterpret_cast<int32_t>(v5(r15_10, r12_12, v8));
                                v18 = reinterpret_cast<void**>(0);
                                if (eax17 < 0) {
                                    rax19 = fun_2490(r15_10, r15_10);
                                    v18 = *rax19;
                                }
                            } else {
                                v18 = reinterpret_cast<void**>(0);
                            }
                            eax20 = savewd_chdir(v7, r12_12);
                            if (eax20 == -1) 
                                goto addr_79e8_16;
                            *reinterpret_cast<void***>(r13_13) = reinterpret_cast<void**>(47);
                            if (eax20) 
                                goto addr_79e8_16;
                            eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx11) + 0xffffffffffffffff));
                            r12_12 = rbp15;
                            if (!*reinterpret_cast<signed char*>(&eax14)) 
                                goto addr_798a_19;
                        }
                    }
                } else {
                    if (!*reinterpret_cast<signed char*>(&eax14)) 
                        goto addr_7990_21;
                }
            } else {
                if (*reinterpret_cast<signed char*>(&edx6) != 47) {
                    r13_13 = rbp15;
                }
            }
            rbx11 = reinterpret_cast<struct s18*>(&rbx11->pad2);
            edx6 = eax14;
        }
    }
    addr_7996_26:
    return rax9;
    addr_79e8_16:
    if (v18 && (rax21 = fun_2490(v7, v7), reinterpret_cast<int1_t>(*rax21 == 2))) {
        *rax21 = v18;
    }
    rax9 = reinterpret_cast<void*>(static_cast<int64_t>(eax20));
    goto addr_7996_26;
    addr_798a_19:
    addr_7990_21:
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_12) - reinterpret_cast<unsigned char>(r15_10));
    goto addr_7996_26;
}

uint32_t fun_2720(void** rdi, void** rsi, int64_t rdx);

uint32_t fun_26e0(void** rdi, void** rsi, int64_t rdx);

uint32_t fun_27c0(void** rdi, void** rsi, int64_t rdx);

uint32_t fun_2770(void** rdi);

uint32_t fun_24e0(void** rdi);

uint32_t fun_2760(void** rdi);

uint32_t fun_2830();

int64_t fun_7a23(void** rdi, void** rsi, int32_t edx, uint32_t ecx, int32_t r8d, uint32_t r9d, uint32_t a7) {
    int32_t r15d8;
    int32_t r14d9;
    uint32_t r13d10;
    void** rbp11;
    void*** rbx12;
    int32_t v13;
    void** rsi14;
    void** rax15;
    void** v16;
    uint32_t eax17;
    uint32_t r12d18;
    uint32_t ecx19;
    uint32_t v20;
    void*** rax21;
    uint32_t v22;
    int32_t v23;
    int64_t rdx24;
    uint32_t eax25;
    uint32_t eax26;
    int64_t rdx27;
    uint32_t eax28;
    uint32_t edx29;
    void*** rax30;
    void* rax31;
    uint32_t eax32;
    uint32_t eax33;
    uint32_t eax34;
    int64_t rax35;
    int64_t rax36;
    uint32_t eax37;
    uint32_t v38;
    void*** rax39;

    __asm__("cli ");
    r15d8 = *reinterpret_cast<int32_t*>(&rdi);
    r14d9 = r8d;
    r13d10 = ecx;
    rbp11 = rsi;
    *reinterpret_cast<uint32_t*>(&rbx12) = r9d;
    v13 = edx;
    rsi14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 + 16);
    rax15 = g28;
    v16 = rax15;
    if (*reinterpret_cast<int32_t*>(&rdi) < 0) {
        rdi = rbp11;
        eax17 = fun_2680(rdi, rsi14);
        r12d18 = eax17;
        if (eax17) 
            goto addr_7b03_3;
        while (1) {
            ecx19 = v20;
            if ((ecx19 & 0xf000) != 0x4000) {
                rax21 = fun_2490(rdi, rdi);
                r12d18 = 0xffffffff;
                *rax21 = reinterpret_cast<void**>(20);
                goto addr_7b03_3;
            }
            addr_7a8e_6:
            if (r13d10 == 0xffffffff) 
                goto addr_7b58_7;
            if (v22 != r13d10) 
                goto addr_7aa3_9;
            addr_7b58_7:
            if (r14d9 == -1) 
                goto addr_7b69_10;
            if (v23 == r14d9) 
                goto addr_7b69_10;
            addr_7aa3_9:
            if (r15d8 < 0) {
                *reinterpret_cast<int32_t*>(&rdx24) = r14d9;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rsi14) = r13d10;
                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                rdi = rbp11;
                if (v13 == -1) {
                    eax25 = fun_2720(rdi, rsi14, rdx24);
                    ecx19 = ecx19;
                    r12d18 = eax25;
                } else {
                    eax26 = fun_26e0(rdi, rsi14, rdx24);
                    ecx19 = ecx19;
                    r12d18 = eax26;
                }
                if (r12d18) 
                    goto addr_7b03_3; else 
                    goto addr_7ad5_16;
            }
            *reinterpret_cast<int32_t*>(&rdx27) = r14d9;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi14) = r13d10;
            *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi) = r15d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax28 = fun_27c0(rdi, rsi14, rdx27);
            ecx19 = ecx19;
            r12d18 = eax28;
            if (!eax28) {
                addr_7ad5_16:
                if (!(*reinterpret_cast<unsigned char*>(&ecx19) & 73)) {
                    addr_7b69_10:
                    edx29 = 0;
                } else {
                    edx29 = ecx19 & reinterpret_cast<uint32_t>("strcmp");
                }
            } else {
                addr_7bd9_19:
                rax30 = fun_2490(rdi, rdi);
                rbp11 = *rax30;
                *reinterpret_cast<int32_t*>(&rbp11 + 4) = 0;
                rbx12 = rax30;
                goto addr_7b49_20;
            }
            if (!(a7 & (ecx19 ^ *reinterpret_cast<uint32_t*>(&rbx12) | edx29))) {
                r12d18 = 0;
                if (r15d8 < 0) {
                    addr_7b03_3:
                    rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
                    if (!rax31) 
                        break;
                } else {
                    goto addr_7c00_24;
                }
            } else {
                *reinterpret_cast<uint32_t*>(&rsi14) = ~a7 & ecx19 & 0xfff | *reinterpret_cast<uint32_t*>(&rbx12);
                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                if (r15d8 < 0) {
                    rdi = rbp11;
                    if (v13 == -1) {
                        eax32 = fun_2770(rdi);
                        r12d18 = eax32;
                        goto addr_7b03_3;
                    } else {
                        eax33 = fun_24e0(rdi);
                        r12d18 = eax33;
                        goto addr_7b03_3;
                    }
                }
                *reinterpret_cast<int32_t*>(&rdi) = r15d8;
                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                eax34 = fun_2760(rdi);
                r12d18 = eax34;
                if (!eax34) 
                    goto addr_7c00_24; else 
                    goto addr_7c37_30;
            }
            fun_2590();
            continue;
            addr_7c00_24:
            *reinterpret_cast<int32_t*>(&rdi) = r15d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rax35 = fun_2610(rdi, rsi14);
            r12d18 = *reinterpret_cast<uint32_t*>(&rax35);
            goto addr_7b03_3;
            addr_7c37_30:
            goto addr_7bd9_19;
            addr_7b49_20:
            *reinterpret_cast<int32_t*>(&rdi) = r15d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            fun_2610(rdi, rsi14, rdi, rsi14);
            *rbx12 = rbp11;
            goto addr_7b03_3;
        }
        *reinterpret_cast<uint32_t*>(&rax36) = r12d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax36) + 4) = 0;
        return rax36;
    } else {
        eax37 = fun_2830();
        r12d18 = eax37;
        if (eax37) 
            goto addr_7bd9_19;
        ecx19 = v38;
        if ((ecx19 & 0xf000) == 0x4000) 
            goto addr_7a8e_6;
        rax39 = fun_2490(rdi, rdi);
        rbp11 = reinterpret_cast<void**>(20);
        *reinterpret_cast<int32_t*>(&rbp11 + 4) = 0;
        r12d18 = 0xffffffff;
        *rax39 = reinterpret_cast<void**>(20);
        rbx12 = rax39;
        goto addr_7b49_20;
    }
}

int32_t dup_safer();

int64_t fun_7c83(void** rdi, void** rsi) {
    int32_t eax3;
    void*** rax4;
    void** rdi5;
    void** r13d6;
    int64_t rax7;
    int64_t rax8;

    __asm__("cli ");
    if (*reinterpret_cast<uint32_t*>(&rdi) <= 2) {
        eax3 = dup_safer();
        rax4 = fun_2490(rdi);
        *reinterpret_cast<uint32_t*>(&rdi5) = *reinterpret_cast<uint32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(&rdi5 + 4) = 0;
        r13d6 = *rax4;
        fun_2610(rdi5, rsi);
        *reinterpret_cast<int32_t*>(&rax7) = eax3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        *rax4 = r13d6;
        return rax7;
    } else {
        *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<uint32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s19 {
    signed char[7] pad7;
    void** f7;
};

struct s20 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s21 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_7ce3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void** rdi11;
    void*** v12;
    void** rax13;
    void** v14;
    int32_t eax15;
    void* rsp16;
    void** r10_17;
    void* rax18;
    int64_t* rsp19;
    void** rbx20;
    int64_t* rsp21;
    void** rax22;
    void** v23;
    int64_t* rsp24;
    void** v25;
    int64_t* rsp26;
    void** v27;
    int64_t* rsp28;
    void** v29;
    int64_t* rsp30;
    void*** rax31;
    void** r15_32;
    void*** v33;
    int64_t* rsp34;
    int64_t* rsp35;
    void** v36;
    int64_t* rsp37;
    void** v38;
    int64_t* rsp39;
    void** rsi40;
    int32_t eax41;
    void*** rax42;
    void** rax43;
    struct s19* v44;
    void** tmp64_45;
    void* v46;
    void** r8_47;
    void** tmp64_48;
    uint1_t cf49;
    void* rax50;
    void* rcx51;
    uint64_t rdx52;
    void* rdx53;
    void** v54;
    void** rax55;
    void*** rax56;
    struct s20* r14_57;
    struct s20* v58;
    void** r9_59;
    void** r8_60;
    int64_t v61;
    int64_t v62;
    uint32_t edx63;
    void** tmp64_64;
    void** r10_65;
    int64_t* rsp66;
    int64_t* rsp67;
    void** rax68;
    int64_t* rsp69;
    void** rax70;
    int64_t* rsp71;
    void** rax72;
    int64_t* rsp73;
    void** rax74;
    int64_t* rsp75;
    void** rax76;
    int64_t* rsp77;
    void** rax78;
    void** tmp64_79;
    int64_t* rsp80;
    void** rax81;
    int64_t* rsp82;
    void** rax83;
    int64_t* rsp84;
    void** rax85;
    uint32_t ecx86;
    uint32_t* v87;
    int64_t r13_88;
    int32_t eax89;
    void** rsi90;
    void** rax91;
    int64_t* rsp92;
    void** rsi93;
    void** rax94;
    int64_t* rsp95;
    int64_t rax96;
    uint32_t eax97;
    int32_t v98;
    struct s21* rcx99;
    int64_t rax100;
    void** tmp64_101;
    void** r15_102;
    void*** rax103;
    int64_t rax104;
    int64_t* rsp105;
    void** rax106;
    int64_t* rsp107;
    void*** rax108;
    int64_t* rsp109;
    int64_t* rsp110;
    void** rax111;
    int64_t* rsp112;
    void*** rax113;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    rdi11 = r13_8;
    v12 = rsi;
    rax13 = g28;
    v14 = rax13;
    eax15 = printf_parse(rdi11, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax15 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_17) = 0;
            *reinterpret_cast<int32_t*>(&r10_17 + 4) = 0;
            while (rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28)), !!rax18) {
                rsp19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp19 = 0x8c1a;
                fun_2590();
                rsp16 = reinterpret_cast<void*>(rsp19 + 1);
                addr_8c1a_5:
                if (rbx20 != 0xffffffffffffffff) 
                    goto addr_8c24_6;
                addr_8b0e_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_17) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx20) > reinterpret_cast<unsigned char>(r13_8) && (r10_17 != v10 && (rsp21 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp21 = 0x8b33, rax22 = fun_2730(r10_17, r13_8, r10_17, r13_8), rsp16 = reinterpret_cast<void*>(rsp21 + 1), r10_17 = r10_17, !!rax22))) {
                    r10_17 = rax22;
                }
                if (v23) {
                    rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp24 = 0x8b59;
                    fun_2470(v23, v23);
                    rsp16 = reinterpret_cast<void*>(rsp24 + 1);
                    r10_17 = r10_17;
                }
                if (v25 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp26 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp26 = 0x8b7f;
                    fun_2470(v25, v25);
                    rsp16 = reinterpret_cast<void*>(rsp26 + 1);
                    r10_17 = r10_17;
                }
                rdi11 = v27;
                if (rdi11 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp28 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp28 = 0x8ba5;
                    fun_2470(rdi11, rdi11);
                    rsp16 = reinterpret_cast<void*>(rsp28 + 1);
                    r10_17 = r10_17;
                }
                *v12 = r12_9;
            }
            break;
            addr_8c24_6:
            addr_8808_16:
            v29 = r10_17;
            addr_880f_17:
            rsp30 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp30 = 0x8814;
            rax31 = fun_2490(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(rsp30 + 1);
            r15_32 = v29;
            v33 = rax31;
            addr_8822_18:
            *v33 = reinterpret_cast<void**>(12);
            if (r15_32 != v10) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp34 = 0x83c1;
                fun_2470(r15_32, r15_32);
                rsp16 = reinterpret_cast<void*>(rsp34 + 1);
            }
            if (v23) {
                rsp35 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp35 = 0x83d9;
                fun_2470(v23, v23);
                rsp16 = reinterpret_cast<void*>(rsp35 + 1);
            }
            addr_8018_23:
            if (v36 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp37 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp37 = 0x8030;
                fun_2470(v36, v36);
                rsp16 = reinterpret_cast<void*>(rsp37 + 1);
            }
            rdi11 = v38;
            if (rdi11 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp39 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp39 = 0x8048;
            fun_2470(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(rsp39 + 1);
        }
        return r10_17;
    }
    rsi40 = r14_7;
    rdi11 = r12_9;
    eax41 = printf_fetchargs(rdi11, rsi40, r14_7);
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax41 < 0) {
        rax42 = fun_2490(rdi11, rdi11);
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        *rax42 = reinterpret_cast<void**>(22);
        goto addr_8018_23;
    }
    rax43 = reinterpret_cast<void**>(&v44->f7);
    if (reinterpret_cast<uint64_t>(v44) >= 0xfffffffffffffff9) {
        rax43 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax43) + reinterpret_cast<uint64_t>(v46));
    if (reinterpret_cast<unsigned char>(tmp64_45) < reinterpret_cast<unsigned char>(rax43)) 
        goto addr_800d_33;
    *reinterpret_cast<int32_t*>(&r8_47) = 0;
    *reinterpret_cast<int32_t*>(&r8_47 + 4) = 0;
    tmp64_48 = tmp64_45 + 6;
    cf49 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_48) < reinterpret_cast<unsigned char>(tmp64_45));
    rdi11 = tmp64_48;
    *reinterpret_cast<unsigned char*>(&r8_47) = cf49;
    if (cf49) 
        goto addr_800d_33;
    if (reinterpret_cast<unsigned char>(rdi11) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax50 = reinterpret_cast<void*>(tmp64_45 + 29);
        rcx51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - (reinterpret_cast<uint64_t>(rax50) & 0xfffffffffffff000));
        rdx52 = reinterpret_cast<uint64_t>(rax50) & 0xfffffffffffffff0;
        if (rsp16 != rcx51) {
            do {
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 0x1000);
            } while (rsp16 != rcx51);
        }
        *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(&rdx52) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx53) + 4) = 0;
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - reinterpret_cast<int64_t>(rdx53));
        if (rdx53) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp16) + reinterpret_cast<int64_t>(rdx53) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp16) + reinterpret_cast<int64_t>(rdx53) - 8);
        }
        v23 = reinterpret_cast<void**>(0);
        v54 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp16) + 15 & 0xfffffffffffffff0);
    } else {
        if (rdi11 == 0xffffffffffffffff || (rax55 = fun_26b0(rdi11, rsi40), rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8), v54 = rax55, rax55 == 0)) {
            addr_800d_33:
            rax56 = fun_2490(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
            *rax56 = reinterpret_cast<void**>(12);
            goto addr_8018_23;
        } else {
            v23 = rax55;
            r8_47 = r8_47;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
    if (v10) {
        rbx20 = *v12;
    }
    r14_57 = v58;
    r9_59 = r8_47;
    r8_60 = r13_8;
    v61 = 0;
    r15_32 = v10;
    r13_8 = r14_57->f0;
    if (r13_8 != r8_60) 
        goto addr_7e0c_46;
    while (1) {
        addr_8764_47:
        r12_9 = r9_59;
        r10_17 = r15_32;
        while (v62 != v61) {
            edx63 = r14_57->f48;
            if (*reinterpret_cast<signed char*>(&edx63) != 37) 
                goto addr_7ecf_50;
            if (r14_57->f50 != -1) 
                goto 0x289d;
            r9_59 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_59 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(r9_59)) {
                addr_873f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_17) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_32 = r10_17;
            } else {
                if (!rbx20) {
                    *reinterpret_cast<int32_t*>(&rbx20) = 12;
                    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
                        goto addr_8808_16;
                    rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
                }
                if (reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(r9_59)) {
                    rbx20 = r9_59;
                }
                if (rbx20 == 0xffffffffffffffff) 
                    goto addr_8808_16;
                if (r10_17 == v10) 
                    goto addr_8a54_64; else 
                    goto addr_8713_65;
            }
            r8_60 = r14_57->f8;
            r13_8 = r14_57->f58;
            r14_57 = reinterpret_cast<struct s20*>(&r14_57->f58);
            ++v61;
            if (r13_8 == r8_60) 
                goto addr_8764_47;
            addr_7e0c_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_60));
            tmp64_64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_59) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_64;
            if (reinterpret_cast<unsigned char>(tmp64_64) < reinterpret_cast<unsigned char>(r9_59)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_65 = r15_32;
            } else {
                if (!rbx20) {
                    *reinterpret_cast<int32_t*>(&rbx20) = 12;
                    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
                        goto addr_88c0_73;
                    rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
                }
                if (reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx20 = r12_9;
                }
                if (rbx20 == 0xffffffffffffffff) 
                    goto addr_88c0_73;
                if (r15_32 == v10) 
                    goto addr_8850_79; else 
                    goto addr_7e67_80;
            }
            addr_7e8c_81:
            rdi11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_65) + reinterpret_cast<unsigned char>(r9_59));
            rsi40 = r8_60;
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp66 = 0x7ea2;
            fun_2690(rdi11, rsi40, r13_8);
            rsp16 = reinterpret_cast<void*>(rsp66 + 1);
            r10_17 = r10_65;
            continue;
            addr_8850_79:
            rdi11 = rbx20;
            rsp67 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp67 = 0x8858;
            rax68 = fun_26b0(rdi11, rsi40, rdi11, rsi40);
            rsp16 = reinterpret_cast<void*>(rsp67 + 1);
            r9_59 = r9_59;
            r8_60 = r8_60;
            r10_65 = rax68;
            if (!rax68) 
                goto addr_88c0_73;
            if (!r9_59) 
                goto addr_7e8c_81;
            rsp69 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp69 = 0x8897;
            rax70 = fun_2690(rax68, v10, r9_59);
            rsp16 = reinterpret_cast<void*>(rsp69 + 1);
            r9_59 = r9_59;
            r8_60 = r8_60;
            r10_65 = rax70;
            goto addr_7e8c_81;
            addr_7e67_80:
            rdi11 = r15_32;
            rsp71 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp71 = 0x7e72;
            rax72 = fun_2730(rdi11, rbx20, rdi11, rbx20);
            rsp16 = reinterpret_cast<void*>(rsp71 + 1);
            r9_59 = r9_59;
            r8_60 = r8_60;
            r10_65 = rax72;
            if (!rax72) 
                goto addr_88c0_73; else 
                goto addr_7e8c_81;
            addr_8a54_64:
            rdi11 = rbx20;
            rsp73 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp73 = 0x8a6a;
            rax74 = fun_26b0(rdi11, rsi40);
            rsp16 = reinterpret_cast<void*>(rsp73 + 1);
            r9_59 = r9_59;
            if (!rax74) 
                goto addr_8c29_84;
            if (r12_9) 
                goto addr_8a8a_86;
            r10_17 = rax74;
            goto addr_873f_55;
            addr_8a8a_86:
            rsi40 = r10_17;
            rdi11 = rax74;
            rsp75 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp75 = 0x8a9f;
            rax76 = fun_2690(rdi11, rsi40, r12_9);
            rsp16 = reinterpret_cast<void*>(rsp75 + 1);
            r9_59 = r9_59;
            r10_17 = rax76;
            goto addr_873f_55;
            addr_8713_65:
            rsi40 = rbx20;
            rdi11 = r10_17;
            v29 = r10_17;
            rsp77 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp77 = 0x872c;
            rax78 = fun_2730(rdi11, rsi40);
            rsp16 = reinterpret_cast<void*>(rsp77 + 1);
            r9_59 = r9_59;
            if (!rax78) 
                goto addr_880f_17;
            r10_17 = rax78;
            goto addr_873f_55;
        }
        break;
    }
    tmp64_79 = r12_9 + 1;
    r13_8 = tmp64_79;
    if (reinterpret_cast<unsigned char>(tmp64_79) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_8c1a_5;
    if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_8b0e_7;
    if (rbx20) {
        if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
            goto addr_8808_16;
        rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
        if (reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_8bcd_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_8bcd_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_8808_16; else 
                goto addr_8bd7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx20) = 12;
            *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
        }
    }
    addr_8ae3_98:
    if (r10_17 == v10) {
        rdi11 = rbx20;
        rsp80 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp80 = 0x8bee;
        rax81 = fun_26b0(rdi11, rsi40, rdi11, rsi40);
        rsp16 = reinterpret_cast<void*>(rsp80 + 1);
        if (rax81) {
            if (!r12_9) {
                r10_17 = rax81;
                goto addr_8b0e_7;
            } else {
                rsp82 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp82 = 0x8c0d;
                rax83 = fun_2690(rax81, r10_17, r12_9);
                rsp16 = reinterpret_cast<void*>(rsp82 + 1);
                r10_17 = rax83;
                goto addr_8b0e_7;
            }
        }
    } else {
        rdi11 = r10_17;
        v29 = r10_17;
        rsp84 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp84 = 0x8b02;
        rax85 = fun_2730(rdi11, rbx20, rdi11, rbx20);
        rsp16 = reinterpret_cast<void*>(rsp84 + 1);
        r10_17 = rax85;
        if (!rax85) 
            goto addr_880f_17; else 
            goto addr_8b0e_7;
    }
    addr_8bd7_96:
    rbx20 = r13_8;
    goto addr_8ae3_98;
    addr_7ecf_50:
    if (r14_57->f50 == -1) 
        goto 0x289d;
    ecx86 = *reinterpret_cast<uint32_t*>((r14_57->f50 << 5) + reinterpret_cast<int64_t>(v87));
    if (*reinterpret_cast<signed char*>(&edx63) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_88) = ecx86 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_88) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_88) > 4) 
            goto 0x28a2;
        goto *reinterpret_cast<int32_t*>(0xbf58 + r13_88 * 4) + 0xbf58;
    }
    eax89 = r14_57->f10;
    *reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(37);
    r13_8 = v54 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax89) & 1) {
        *reinterpret_cast<void***>(v54 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v54 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi90 = r14_57->f18;
    if (rsi90 != r14_57->f20) {
        rax91 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_57->f20) - reinterpret_cast<unsigned char>(rsi90));
        rdi11 = r13_8;
        rsp92 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp92 = 0x7f9f;
        fun_2690(rdi11, rsi90, rax91);
        rsp16 = reinterpret_cast<void*>(rsp92 + 1);
        r10_17 = r10_17;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax91));
    }
    rsi93 = r14_57->f30;
    if (rsi93 != r14_57->f38) {
        rax94 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_57->f38) - reinterpret_cast<unsigned char>(rsi93));
        rdi11 = r13_8;
        rsp95 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp95 = 0x7fd9;
        fun_2690(rdi11, rsi93, rax94);
        rsp16 = reinterpret_cast<void*>(rsp95 + 1);
        r10_17 = r10_17;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax94));
    }
    *reinterpret_cast<uint32_t*>(&rax96) = ecx86 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax96) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax96) <= 9) {
        goto *reinterpret_cast<int32_t*>(0xbee8 + rax96 * 4) + 0xbee8;
    }
    eax97 = r14_57->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax97);
    if (r14_57->f28 == -1) {
        v98 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_57->f28 << 5) + reinterpret_cast<int64_t>(v87)) != 5) 
            goto 0x289d;
        v98 = 1;
    }
    if (r14_57->f40 != -1) {
        rcx99 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(v87) + (r14_57->f40 << 5));
        if (rcx99->f0 != 5) 
            goto 0x289d;
        *reinterpret_cast<int32_t*>(&rax100) = v98;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax100) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax100 * 4 - 0x3b8) = rcx99->f10;
    }
    tmp64_101 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_101) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_8112_135;
    if (rbx20 != 0xffffffffffffffff) {
        goto addr_8808_16;
    }
    addr_8112_135:
    if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(tmp64_101)) {
        r15_102 = r10_17;
    } else {
        if (rbx20) {
            if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
                goto addr_8808_16;
            rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
            if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(tmp64_101)) 
                goto addr_8133_142; else 
                goto addr_89c2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_101) > reinterpret_cast<unsigned char>(12)) {
                addr_89c2_143:
                if (tmp64_101 == 0xffffffffffffffff) 
                    goto addr_8808_16; else 
                    goto addr_89cc_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx20) = 12;
                *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
                goto addr_8133_142;
            }
        }
    }
    addr_8165_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_102) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8) = 0x816f;
    rax103 = fun_2490(rdi11, rdi11);
    *rax103 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rax104) = ecx86;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx20) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax104) > 17) 
        goto 0x28a2;
    goto *reinterpret_cast<int32_t*>(0xbf10 + rax104 * 4) + 0xbf10;
    addr_8133_142:
    if (r10_17 == v10) {
        rdi11 = rbx20;
        rsp105 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp105 = 0x8a28;
        rax106 = fun_26b0(rdi11, rsi93);
        rsp16 = reinterpret_cast<void*>(rsp105 + 1);
        r15_102 = rax106;
        if (!rax106) {
            addr_8c29_84:
            rsp107 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp107 = 0x8c2e;
            rax108 = fun_2490(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(rsp107 + 1);
            r15_32 = v10;
            v33 = rax108;
            goto addr_8822_18;
        } else {
            if (r12_9) {
                rdi11 = rax106;
                rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp109 = 0x8a4f;
                fun_2690(rdi11, v10, r12_9);
                rsp16 = reinterpret_cast<void*>(rsp109 + 1);
                goto addr_8165_147;
            }
        }
    } else {
        rdi11 = r10_17;
        rsp110 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp110 = 0x8152;
        rax111 = fun_2730(rdi11, rbx20);
        rsp16 = reinterpret_cast<void*>(rsp110 + 1);
        r10_17 = r10_17;
        r15_102 = rax111;
        if (!rax111) 
            goto addr_8808_16; else 
            goto addr_8165_147;
    }
    addr_89cc_145:
    rbx20 = tmp64_101;
    goto addr_8133_142;
    addr_88c0_73:
    rsp112 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
    *rsp112 = 0x88c5;
    rax113 = fun_2490(rdi11, rdi11);
    rsp16 = reinterpret_cast<void*>(rsp112 + 1);
    v33 = rax113;
    goto addr_8822_18;
}

int32_t setlocale_null_r();

int64_t fun_8c63() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax3;
    }
}

int64_t fun_8ce3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2740(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2570(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2690(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2690(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_8d93() {
    __asm__("cli ");
    goto fun_2740;
}

void fun_8da3() {
    __asm__("cli ");
}

struct s22 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_8dc3(int64_t rdi, struct s22* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_8df9_5;
    return 0xffffffff;
    addr_8df9_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xbf80 + rdx3 * 4) + 0xbf80;
}

struct s23 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s24 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s25 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_8ff3(void** rdi, struct s23* rsi, struct s24* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s24* r15_7;
    struct s23* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    struct s18* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s18* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    struct s18* rbx65;
    void* rsi66;
    int32_t eax67;
    struct s18* rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rax88;
    void** rax89;
    void*** rax90;
    void*** rax91;
    void** rdi92;
    void*** rax93;
    struct s25* rbx94;
    void* rdi95;
    int32_t eax96;
    struct s25* rcx97;
    void* rax98;
    void* rdx99;
    void* rdx100;
    void* tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_90a8_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_9095_7:
    return rax15;
    addr_90a8_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_9119_11;
    } else {
        addr_9119_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_16 + 16)) | 1);
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_9130_14;
        } else {
            goto addr_9130_14;
        }
    }
    rax23 = reinterpret_cast<struct s18*>(rax5 + 2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s18*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_95f8_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s18*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s18*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_95f8_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<void**>(&rcx26->f2);
    goto addr_9119_11;
    addr_9130_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xbffc + rax33 * 4) + 0xbffc;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_9277_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_9979_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_997e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_9974_42;
            }
        } else {
            addr_915d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_9378_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_9167_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<unsigned char*>(rbx14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_9b64_56; else 
                        goto addr_93b5_57;
                }
            } else {
                addr_9167_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_9188_60;
                } else {
                    goto addr_9188_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_96fb_65;
    addr_9277_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_95f8_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_929c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_931a_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_9c1b_75; else 
            goto addr_92c3_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_95fc_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_9167_52;
        goto addr_9378_44;
    }
    addr_997e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_915d_43;
    addr_9c1b_75:
    if (v11 != rdx53) {
        fun_2470(rdx53);
        r10_4 = r10_4;
        goto addr_9a2a_84;
    } else {
        goto addr_9a2a_84;
    }
    addr_92c3_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_26b0(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2730(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_9c1b_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_2690(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_931a_68;
    addr_96fb_65:
    rbx65 = reinterpret_cast<struct s18*>(rbx14 + 2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s18*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_95f8_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s18*>(&rbx65->pad2);
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s18*>(&rbx65->pad2);
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_95f8_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = reinterpret_cast<void**>(&rdx68->f2);
    goto addr_929c_67;
    label_41:
    addr_9974_42:
    goto addr_9979_36;
    addr_9b64_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_9b8a_104;
    addr_93b5_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_95f8_22:
            r8_54 = r15_7->f8;
            goto addr_95fc_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_93c4_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_9a72_111;
    } else {
        addr_93d1_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_9407_116;
        }
    }
    rdx53 = r8_54;
    goto addr_9c1b_75;
    addr_9a72_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_26b0(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_9a2a_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_2470(rdi86, rdi86);
            }
        } else {
            addr_9cb0_120:
            rdx87 = r15_7->f0;
            rax88 = fun_2690(rdi84, r8_85, reinterpret_cast<unsigned char>(rdx87) << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_9acf_121;
        }
    } else {
        rax89 = fun_2730(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_9c1b_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_9cb0_120;
            }
        }
    }
    rax90 = fun_2490(rdi86, rdi86);
    *rax90 = reinterpret_cast<void**>(12);
    return 0xffffffff;
    addr_9acf_121:
    r15_7->f8 = r8_54;
    goto addr_93d1_112;
    addr_9407_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_95fc_80:
            if (v11 != r8_54) {
                fun_2470(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_9167_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_9167_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_2470(rdi92, rdi92);
    }
    rax93 = fun_2490(rdi92, rdi92);
    *rax93 = reinterpret_cast<void**>(22);
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_9095_7;
    addr_9b8a_104:
    rbx94 = reinterpret_cast<struct s25*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s25*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (reinterpret_cast<uint64_t>(rdi95) > 0x1999999999999999) {
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi95) + reinterpret_cast<uint64_t>(rdi95) * 4);
            rdx99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx99) + reinterpret_cast<uint64_t>(rax98)), rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_101) < reinterpret_cast<uint64_t>(rdx99)) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_95f8_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s25*>(&rbx94->pad2);
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s25*>(&rbx94->pad2);
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_101) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_95f8_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx97->f2);
    goto addr_93c4_107;
    addr_9188_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xc160 + rax105 * 4) + 0xc160;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xc0a4 + rax106 * 4) + 0xc0a4;
    }
}

uint32_t fun_2500(void** rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_9d83(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    void** rsi16;
    void** rdi17;
    uint32_t eax18;
    uint32_t eax19;
    void*** rax20;
    void** rdi21;
    void** r13d22;
    uint32_t eax23;
    void*** rax24;
    void** rdi25;
    uint32_t eax26;
    uint32_t ecx27;
    int64_t rax28;
    uint32_t eax29;
    uint32_t eax30;
    uint32_t eax31;
    int32_t ecx32;
    int64_t rax33;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_2500(rdi);
        r12d10 = eax9;
        goto addr_9e84_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_2500(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_9e84_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2590();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_9f39_9:
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi17) = r12d10;
                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                eax18 = fun_2500(rdi17, rdi17);
                if (reinterpret_cast<int32_t>(eax18) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi16) = 2, *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi17) = r12d10, *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0, eax19 = fun_2500(rdi17, rdi17), eax19 == 0xffffffff)) {
                    rax20 = fun_2490(rdi17, rdi17);
                    *reinterpret_cast<uint32_t*>(&rdi21) = r12d10;
                    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
                    r12d10 = 0xffffffff;
                    r13d22 = *rax20;
                    fun_2610(rdi21, rsi16, rdi21, rsi16);
                    *rax20 = r13d22;
                    goto addr_9e84_3;
                }
            }
        } else {
            eax23 = fun_2500(rdi, rdi);
            r12d10 = eax23;
            if (reinterpret_cast<int32_t>(eax23) >= reinterpret_cast<int32_t>(0) || (rax24 = fun_2490(rdi, rdi), *reinterpret_cast<int32_t*>(&rdi25) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(&rdi25 + 4) = 0, !reinterpret_cast<int1_t>(*rax24 == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_9e84_3;
            } else {
                eax26 = fun_2500(rdi25);
                r12d10 = eax26;
                if (reinterpret_cast<int32_t>(eax26) < reinterpret_cast<int32_t>(0)) 
                    goto addr_9e84_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_9f39_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_9de9_16;
    ecx27 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx27 > 10) 
        goto addr_9ded_18;
    rax28 = 1 << *reinterpret_cast<unsigned char*>(&ecx27);
    if (!(*reinterpret_cast<uint32_t*>(&rax28) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax28) & 0x502)) {
            addr_9ded_18:
            if (0) {
            }
        } else {
            addr_9e35_23:
            eax29 = fun_2500(rdi);
            r12d10 = eax29;
            goto addr_9e84_3;
        }
        eax30 = fun_2500(rdi);
        r12d10 = eax30;
        goto addr_9e84_3;
    }
    if (0) {
    }
    eax31 = fun_2500(rdi);
    r12d10 = eax31;
    goto addr_9e84_3;
    addr_9de9_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_9ded_18;
    ecx32 = *reinterpret_cast<int32_t*>(&rsi);
    rax33 = 1 << *reinterpret_cast<unsigned char*>(&ecx32);
    if (!(*reinterpret_cast<uint32_t*>(&rax33) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax33) & 0xa0a) 
            goto addr_9e35_23;
        goto addr_9ded_18;
    }
}

void fun_9ff3() {
    __asm__("cli ");
}

void fun_a007() {
    __asm__("cli ");
    return;
}

void fun_3b20() {
    goto 0x3987;
}

uint32_t fun_2620(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2820(int64_t rdi, void** rsi);

uint32_t fun_2810(void** rdi, void** rsi);

void** fun_2850(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_3fb5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2550();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2550();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2570(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_42b3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_42b3_22; else 
                            goto addr_46ad_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_476d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4ac0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_42b0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_42b0_30; else 
                                goto addr_4ad9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2570(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4ac0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2620(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4ac0_28; else 
                            goto addr_415c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4c20_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4aa0_40:
                        if (r11_27 == 1) {
                            addr_462d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4be8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4267_44;
                            }
                        } else {
                            goto addr_4ab0_46;
                        }
                    } else {
                        addr_4c2f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_462d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_42b3_22:
                                if (v47 != 1) {
                                    addr_4809_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2570(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4854_54;
                                    }
                                } else {
                                    goto addr_42c0_56;
                                }
                            } else {
                                addr_4265_57:
                                ebp36 = 0;
                                goto addr_4267_44;
                            }
                        } else {
                            addr_4a94_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_4c2f_47; else 
                                goto addr_4a9e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_462d_41;
                        if (v47 == 1) 
                            goto addr_42c0_56; else 
                            goto addr_4809_52;
                    }
                }
                addr_4321_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_41b8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_41dd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_44e0_65;
                    } else {
                        addr_4349_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4b98_67;
                    }
                } else {
                    goto addr_4340_69;
                }
                addr_41f1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_423c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4b98_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_423c_81;
                }
                addr_4340_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_41dd_64; else 
                    goto addr_4349_66;
                addr_4267_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_431f_91;
                if (v22) 
                    goto addr_427f_93;
                addr_431f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4321_62;
                addr_4854_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4fdb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_504b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_4e4f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2820(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2810(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_494e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_430c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4958_112;
                    }
                } else {
                    addr_4958_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4a29_114;
                }
                addr_4318_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_431f_91;
                while (1) {
                    addr_4a29_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4f37_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4996_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4f45_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4a17_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4a17_128;
                        }
                    }
                    addr_49c5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4a17_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_4996_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_49c5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_423c_81;
                addr_4f45_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4b98_67;
                addr_4fdb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_494e_109;
                addr_504b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_494e_109;
                addr_42c0_56:
                rax93 = fun_2850(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_430c_110;
                addr_4a9e_59:
                goto addr_4aa0_40;
                addr_476d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_42b3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4318_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4265_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_42b3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_47b2_160;
                if (!v22) 
                    goto addr_4b87_162; else 
                    goto addr_4d93_163;
                addr_47b2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4b87_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4b98_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_465b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_44c3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_41f1_70; else 
                    goto addr_44d7_169;
                addr_465b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_41b8_63;
                goto addr_4340_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4a94_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4bcf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_42b0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_41a8_178; else 
                        goto addr_4b52_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4a94_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_42b3_22;
                }
                addr_4bcf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_42b0_30:
                    r8d42 = 0;
                    goto addr_42b3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4321_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4be8_42;
                    }
                }
                addr_41a8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_41b8_63;
                addr_4b52_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4ab0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4321_62;
                } else {
                    addr_4b62_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_42b3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5312_188;
                if (v28) 
                    goto addr_4b87_162;
                addr_5312_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_44c3_168;
                addr_415c_37:
                if (v22) 
                    goto addr_5153_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4173_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4c20_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4cab_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_42b3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_41a8_178; else 
                        goto addr_4c87_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4a94_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_42b3_22;
                }
                addr_4cab_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_42b3_22;
                }
                addr_4c87_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4ab0_46;
                goto addr_4b62_186;
                addr_4173_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_42b3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_42b3_22; else 
                    goto addr_4184_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_525e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_50e4_210;
            if (1) 
                goto addr_50e2_212;
            if (!v29) 
                goto addr_4d1e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_5251_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_44e0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_429b_219; else 
            goto addr_44fa_220;
        addr_427f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4293_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_44fa_220; else 
            goto addr_429b_219;
        addr_4e4f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_44fa_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_4e6d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_52e0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4d46_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_4f37_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4293_221;
        addr_4d93_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4293_221;
        addr_44d7_169:
        goto addr_44e0_65;
        addr_525e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_44fa_220;
        goto addr_4e6d_222;
        addr_50e4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_513e_236;
        fun_2590();
        rsp25 = rsp25 - 8 + 8;
        goto addr_52e0_225;
        addr_50e2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_50e4_210;
        addr_4d1e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_50e4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_4d46_226;
        }
        addr_5251_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_46ad_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb8ac + rax113 * 4) + 0xb8ac;
    addr_4ad9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb9ac + rax114 * 4) + 0xb9ac;
    addr_5153_190:
    addr_429b_219:
    goto 0x3f80;
    addr_4184_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb7ac + rax115 * 4) + 0xb7ac;
    addr_513e_236:
    goto v116;
}

void fun_41a0() {
}

void fun_4358() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4052;
}

void fun_43b1() {
    goto 0x4052;
}

void fun_449e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4321;
    }
    if (v2) 
        goto 0x4d93;
    if (!r10_3) 
        goto addr_4efe_5;
    if (!v4) 
        goto addr_4dce_7;
    addr_4efe_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_4dce_7:
    goto 0x41d4;
}

void fun_44bc() {
}

void fun_4567() {
    signed char v1;

    if (v1) {
        goto 0x44ef;
    } else {
        goto 0x422a;
    }
}

void fun_4581() {
    signed char v1;

    if (!v1) 
        goto 0x457a; else 
        goto "???";
}

void fun_45a8() {
    goto 0x44c3;
}

void fun_4628() {
}

void fun_4640() {
}

void fun_466f() {
    goto 0x44c3;
}

void fun_46c1() {
    goto 0x4650;
}

void fun_46f0() {
    goto 0x4650;
}

void fun_4723() {
    goto 0x4650;
}

void fun_4af0() {
    goto 0x41a8;
}

void fun_4dee() {
    signed char v1;

    if (v1) 
        goto 0x4d93;
    goto 0x41d4;
}

void fun_4e95() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x41d4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x41b8;
        goto 0x41d4;
    }
}

void fun_52b2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4520;
    } else {
        goto 0x4052;
    }
}

void fun_6878() {
    fun_2550();
}

int32_t fun_2450(int64_t rdi);

struct s26 {
    signed char[1] pad1;
    signed char f1;
};

struct s27 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_81d8() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    void** rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    void** rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    void** rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    void** rax30;
    void** rcx31;
    void* rbx32;
    void* rbx33;
    void** tmp64_34;
    void* r12_35;
    void** rax36;
    void** rbx37;
    int64_t r15_38;
    int64_t rbp39;
    void** r15_40;
    void** rax41;
    void** rax42;
    int64_t r12_43;
    void** r15_44;
    void** r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s27* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_2450(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<void***>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8363_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_2450(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_2450(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8363_5;
    }
    rcx19 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x289d;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_826d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0x8747;
        }
    } else {
        addr_8265_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_826d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0x83b0;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0x8188;
        goto 0x8822;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0x8822;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_82a6_26;
    rax36 = rcx31;
    addr_82a6_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0x8188;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0x8822;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_2730(r15_40, rax36);
        if (!rax41) 
            goto 0x8822;
        goto 0x8188;
    }
    rax42 = fun_26b0(rax36, rsi10);
    if (!rax42) 
        goto 0x8822;
    if (r12_43) 
        goto addr_866a_36;
    goto 0x8188;
    addr_866a_36:
    fun_2690(rax42, r15_44, r12_45);
    goto 0x8188;
    addr_8363_5:
    if ((*reinterpret_cast<struct s26**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s26**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0x8188;
    }
    if (eax7 >= 0) 
        goto addr_8265_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0x83b0;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_83a7_42;
    eax50 = 84;
    addr_83a7_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_82f0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_83e0() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x8525;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_2450(r15_4 + r12_5);
            goto 0x823d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x8213;
        }
    }
}

void fun_8428() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_2450(rdi5);
            goto 0x823d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_2450(rdi5);
    goto 0x823d;
}

void fun_8490() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8316;
}

void fun_84e0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x84c0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x831f;
}

void fun_8590() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8316;
    goto 0x84c0;
}

void fun_8623() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x8092;
}

void fun_87c0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x8747;
}

struct s28 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s29 {
    signed char[8] pad8;
    int64_t f8;
};

struct s30 {
    signed char[16] pad16;
    int64_t f10;
};

struct s31 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8e08() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s28* rcx3;
    struct s29* rcx4;
    int64_t r11_5;
    struct s30* rcx6;
    uint32_t* rcx7;
    struct s31* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x8df0; else 
        goto "???";
}

struct s32 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s33 {
    signed char[8] pad8;
    int64_t f8;
};

struct s34 {
    signed char[16] pad16;
    int64_t f10;
};

struct s35 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8e40() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s32* rcx3;
    struct s33* rcx4;
    int64_t r11_5;
    struct s34* rcx6;
    uint32_t* rcx7;
    struct s35* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x8e26;
}

struct s36 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s37 {
    signed char[8] pad8;
    int64_t f8;
};

struct s38 {
    signed char[16] pad16;
    int64_t f10;
};

struct s39 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_8e60() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s36* rcx3;
    struct s37* rcx4;
    int64_t r11_5;
    struct s38* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s39* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x8e26;
}

struct s40 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s41 {
    signed char[8] pad8;
    int64_t f8;
};

struct s42 {
    signed char[16] pad16;
    int64_t f10;
};

struct s43 {
    signed char[16] pad16;
    signed char f10;
};

void fun_8e80() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s40* rcx3;
    struct s41* rcx4;
    int64_t r11_5;
    struct s42* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s43* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x8e26;
}

struct s44 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s45 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_8f00() {
    struct s44* rcx1;
    struct s45* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x8e26;
}

struct s46 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s47 {
    signed char[8] pad8;
    int64_t f8;
};

struct s48 {
    signed char[16] pad16;
    int64_t f10;
};

struct s49 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8f50() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s46* rcx3;
    struct s47* rcx4;
    int64_t r11_5;
    struct s48* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s49* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x8e26;
}

void fun_919c() {
}

void fun_91cb() {
    goto 0x91a8;
}

void fun_9220() {
}

void fun_9430() {
    goto 0x9223;
}

struct s50 {
    signed char[8] pad8;
    void** f8;
};

struct s51 {
    signed char[8] pad8;
    int64_t f8;
};

struct s52 {
    signed char[8] pad8;
    void** f8;
};

struct s53 {
    signed char[8] pad8;
    void** f8;
};

void fun_94d8() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s50* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s51* r15_14;
    void** rax15;
    struct s52* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s53* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x9c17;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x9c17;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_26b0(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x9a38; else 
                goto "???";
        }
    } else {
        rax15 = fun_2730(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x9c17;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_9d6a_9; else 
            goto addr_954e_10;
    }
    addr_96a4_11:
    rax19 = fun_2690(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_954e_10:
    r14_20->f8 = rcx12;
    goto 0x9069;
    addr_9d6a_9:
    r13_18 = *r14_21;
    goto addr_96a4_11;
}

struct s54 {
    signed char[11] pad11;
    void** fb;
};

struct s55 {
    signed char[80] pad80;
    int64_t f50;
};

struct s56 {
    signed char[80] pad80;
    int64_t f50;
};

struct s57 {
    signed char[8] pad8;
    void** f8;
};

struct s58 {
    signed char[8] pad8;
    void** f8;
};

struct s59 {
    signed char[8] pad8;
    void** f8;
};

struct s60 {
    signed char[72] pad72;
    signed char f48;
};

struct s61 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_9761() {
    void** ecx1;
    int32_t edx2;
    struct s54* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s55* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s56* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s57* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s58* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s59* r15_37;
    void*** r13_38;
    struct s60* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s61* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s54*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x95f8;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x9d4c;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_98bd_12;
    } else {
        addr_9464_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_949f_17;
        }
    }
    rax25 = fun_26b0(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x9a2a;
    addr_99d0_19:
    rdx30 = *r15_31;
    rax32 = fun_2690(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_9906_20:
    r15_33->f8 = r8_12;
    goto addr_9464_13;
    addr_98bd_12:
    rax34 = fun_2730(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x9c17;
    if (v36 != r15_37->f8) 
        goto addr_9906_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_99d0_19;
    addr_949f_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x95fc;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x94e0;
    goto 0x9069;
}

void fun_977f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x9448;
    if (dl2 & 4) 
        goto 0x9448;
    if (edx3 > 7) 
        goto 0x9448;
    if (dl4 & 2) 
        goto 0x9448;
    goto 0x9448;
}

void fun_97c8() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x9448;
    if (dl2 & 4) 
        goto 0x9448;
    if (edx3 > 7) 
        goto 0x9448;
    if (dl4 & 2) 
        goto 0x9448;
    goto 0x9448;
}

void fun_9810() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x9448;
    if (dl2 & 4) 
        goto 0x9448;
    if (edx3 > 7) 
        goto 0x9448;
    if (dl4 & 2) 
        goto 0x9448;
    goto 0x9448;
}

void fun_9858() {
    goto 0x9448;
}

void fun_3a70() {
}

void fun_43de() {
    goto 0x4052;
}

void fun_45b4() {
    goto 0x456c;
}

void fun_467b() {
    goto 0x41a8;
}

void fun_46cd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4650;
    goto 0x427f;
}

void fun_46ff() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x465b;
        goto 0x4080;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x44fa;
        goto 0x429b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4e98;
    if (r10_8 > r15_9) 
        goto addr_45e5_9;
    addr_45ea_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4ea3;
    goto 0x41d4;
    addr_45e5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_45ea_10;
}

void fun_4732() {
    goto 0x4267;
}

void fun_4b00() {
    goto 0x4267;
}

void fun_529f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x43bc;
    } else {
        goto 0x4520;
    }
}

void fun_6930() {
}

struct s62 {
    signed char[1] pad1;
    signed char f1;
};

void fun_8080() {
    signed char* r13_1;
    struct s62* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_87ce() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x8747;
}

struct s63 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s64 {
    signed char[8] pad8;
    int64_t f8;
};

struct s65 {
    signed char[16] pad16;
    int64_t f10;
};

struct s66 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8f20() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s63* rcx3;
    struct s64* rcx4;
    int64_t r11_5;
    struct s65* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s66* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x8e26;
}

void fun_91d5() {
    goto 0x91a8;
}

void fun_9b24() {
    goto 0x9448;
}

void fun_9438() {
}

void fun_9868() {
    goto 0x9448;
}

void fun_3a88() {
    goto 0x3a78;
}

void fun_473c() {
    goto 0x46d7;
}

void fun_4b0a() {
    goto 0x462d;
}

void fun_6990() {
    fun_2550();
    goto fun_2800;
}

void fun_8560() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8316;
    goto 0x84c0;
}

void fun_87dc() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x8747;
}

struct s67 {
    int32_t f0;
    int32_t f4;
};

struct s68 {
    int32_t f0;
    int32_t f4;
};

struct s69 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s70 {
    signed char[8] pad8;
    int64_t f8;
};

struct s71 {
    signed char[8] pad8;
    int64_t f8;
};

struct s72 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_8ed0(struct s67* rdi, struct s68* rsi) {
    struct s69* rcx3;
    struct s70* rcx4;
    struct s71* rcx5;
    struct s72* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x8e26;
}

void fun_91df() {
    goto 0x91a8;
}

void fun_9b2e() {
    goto 0x9448;
}

void fun_3a90() {
    goto 0x3a78;
}

void fun_440d() {
    goto 0x4052;
}

void fun_4748() {
    goto 0x46d7;
}

void fun_4b17() {
    goto 0x467e;
}

void fun_69d0() {
    fun_2550();
    goto fun_2800;
}

void fun_87eb() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x8747;
}

void fun_91e9() {
    goto 0x91a8;
}

void fun_3a98() {
    goto 0x3a78;
}

void fun_443a() {
    goto 0x4052;
}

void fun_4754() {
    goto 0x4650;
}

void fun_6a10() {
    fun_2550();
    goto fun_2800;
}

void fun_91f3() {
    goto 0x91a8;
}

void fun_3aa0() {
    goto 0x3a78;
}

void fun_445c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4df0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4321;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4321;
    }
    if (v11) 
        goto 0x5153;
    if (r10_12 > r15_13) 
        goto addr_51a3_8;
    addr_51a8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4ee1;
    addr_51a3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_51a8_9;
}

struct s73 {
    signed char[24] pad24;
    int64_t f18;
};

struct s74 {
    signed char[16] pad16;
    void** f10;
};

struct s75 {
    signed char[8] pad8;
    void** f8;
};

void fun_6a60() {
    int64_t r15_1;
    struct s73* rbx2;
    void** r14_3;
    struct s74* rbx4;
    void** r13_5;
    struct s75* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2550();
    fun_2800(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6a82, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6ab8() {
    fun_2550();
    goto 0x6a89;
}

struct s76 {
    signed char[32] pad32;
    int64_t f20;
};

struct s77 {
    signed char[24] pad24;
    int64_t f18;
};

struct s78 {
    signed char[16] pad16;
    void** f10;
};

struct s79 {
    signed char[8] pad8;
    void** f8;
};

struct s80 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6af0() {
    int64_t rcx1;
    struct s76* rbx2;
    int64_t r15_3;
    struct s77* rbx4;
    void** r14_5;
    struct s78* rbx6;
    void** r13_7;
    struct s79* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s80* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2550();
    fun_2800(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6b24, __return_address(), rcx1);
    goto v15;
}

void fun_6b68() {
    fun_2550();
    goto 0x6b2b;
}
