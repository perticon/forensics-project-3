void main(int param_1,undefined8 *param_2)

{
  code *pcVar1;
  long lVar2;
  int iVar3;
  uint uVar4;
  undefined8 uVar5;
  void *__ptr;
  undefined8 uVar6;
  long lVar7;
  long in_FS_OFFSET;
  undefined auVar8 [16];
  code *local_68;
  uint local_60;
  uint local_5c;
  undefined8 local_58;
  undefined8 local_50;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_68 = (code *)0x0;
  local_58 = 0x1ff;
  local_50 = 0;
  local_48 = 0;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  lVar2 = 0;
LAB_00102955:
  lVar7 = lVar2;
  iVar3 = getopt_long(param_1,param_2,"pm:vZ",longopts,0);
  if (iVar3 != -1) goto code_r0x00102971;
  if (optind != param_1) {
    if (local_68 == (code *)0x0) {
      if (lVar7 == 0) goto LAB_00102aec;
      local_5c = umask(0);
      local_60 = local_5c & 0xffffff3f;
LAB_00102aa0:
      __ptr = (void *)mode_compile(lVar7);
      if (__ptr == (void *)0x0) {
        uVar5 = quote(lVar7);
        uVar6 = dcgettext(0,"invalid mode %s",5);
        auVar8 = error(1,0,uVar6,uVar5);
        pcVar1 = local_68;
        local_68 = SUB168(auVar8,0);
        (*(code *)PTR___libc_start_main_0010efc8)
                  (main,pcVar1,&local_60,0,0,SUB168(auVar8 >> 0x40,0),&local_68);
        do {
                    /* WARNING: Do nothing block with infinite loop */
        } while( true );
      }
      uVar4 = mode_adjust(0x1ff,1,local_5c,__ptr,(long)&local_58 + 4);
      local_58 = local_58 & 0xffffffff00000000 | (ulong)uVar4;
      local_5c = ~uVar4 & local_5c;
      free(__ptr);
    }
    else {
      local_5c = umask(0);
      local_60 = local_5c & 0xffffff3f;
      if (lVar7 != 0) goto LAB_00102aa0;
      local_58 = CONCAT44(local_58._4_4_,0x1ff);
    }
    umask(local_5c);
LAB_00102aec:
    savewd_process_files(param_1 - optind,param_2 + optind,process_dir,&local_68);
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return;
  }
  goto LAB_00102b30;
code_r0x00102971:
  lVar2 = optarg;
  if (iVar3 != 0x6d) {
    if (iVar3 < 0x6e) {
      if (iVar3 == -0x82) {
        usage(0);
LAB_00102b30:
        uVar5 = dcgettext(0,"missing operand",5);
        error(0,0,uVar5);
      }
      else {
        if (iVar3 == 0x5a) {
          lVar2 = lVar7;
          if (optarg != 0) {
            uVar5 = dcgettext(0,
                              "warning: ignoring --context; it requires an SELinux/SMACK-enabled kernel"
                              ,5);
            error(0,0,uVar5);
          }
          goto LAB_00102955;
        }
        if (iVar3 == -0x83) {
          version_etc(stdout,"mkdir","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
      }
      do {
        iVar3 = usage();
LAB_001029f0:
      } while (iVar3 != 0x76);
      local_48 = dcgettext(0,"created directory %s",5);
      lVar2 = lVar7;
    }
    else {
      if (iVar3 != 0x70) goto LAB_001029f0;
      local_68 = make_ancestor;
      lVar2 = lVar7;
    }
  }
  goto LAB_00102955;
}