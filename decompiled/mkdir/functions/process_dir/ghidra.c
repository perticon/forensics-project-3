byte process_dir(undefined8 param_1,undefined8 param_2,long *param_3)

{
  long lVar1;
  byte bVar2;
  int *piVar3;
  
  lVar1 = *param_3;
  if ((param_3[3] != 0) && (lVar1 == 0)) {
    piVar3 = __errno_location();
    *piVar3 = 0x5f;
  }
  bVar2 = make_dir_parents(param_1,param_2,lVar1,param_3,*(undefined4 *)(param_3 + 2),announce_mkdir
                           ,*(undefined4 *)((long)param_3 + 0x14),0xffffffffffffffff,
                           0xffffffffffffffff,1);
  if ((((bVar2 ^ 1) == 0) && (param_3[3] != 0)) && (*param_3 != 0)) {
    piVar3 = __errno_location();
    *piVar3 = 0x5f;
  }
  return bVar2 ^ 1;
}