make_ancestor (char const *dir, char const *component, void *options)
{
  struct mkdir_options const *o = options;

  if (o->set_security_context
      && defaultcon (o->set_security_context, component, S_IFDIR) < 0
      && ! ignorable_ctx_err (errno))
    error (0, errno, _("failed to set default creation context for %s"),
           quoteaf (dir));

  if (o->umask_ancestor != o->umask_self)
    umask (o->umask_ancestor);
  int r = mkdir (component, S_IRWXUGO);
  if (o->umask_ancestor != o->umask_self)
    {
      int mkdir_errno = errno;
      umask (o->umask_self);
      errno = mkdir_errno;
    }
  if (r == 0)
    {
      r = (o->umask_ancestor & S_IRUSR) != 0;
      announce_mkdir (dir, options);
    }
  return r;
}