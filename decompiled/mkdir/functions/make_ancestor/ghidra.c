uint make_ancestor(undefined8 param_1,char *param_2,long param_3)

{
  __mode_t __mask;
  int iVar1;
  uint uVar2;
  int *piVar3;
  undefined8 uVar4;
  
  if (*(long *)(param_3 + 0x18) != 0) {
    piVar3 = __errno_location();
    *piVar3 = 0x5f;
  }
  if (*(__mode_t *)(param_3 + 8) != *(__mode_t *)(param_3 + 0xc)) {
    umask(*(__mode_t *)(param_3 + 8));
  }
  uVar2 = mkdir(param_2,0x1ff);
  __mask = *(__mode_t *)(param_3 + 0xc);
  if (*(__mode_t *)(param_3 + 8) != __mask) {
    piVar3 = __errno_location();
    iVar1 = *piVar3;
    umask(__mask);
    *piVar3 = iVar1;
  }
  if ((uVar2 == 0) && (uVar2 = *(uint *)(param_3 + 8) >> 8 & 1, *(long *)(param_3 + 0x20) != 0)) {
    uVar4 = quotearg_style(4,param_1);
    prog_fprintf(stdout,*(undefined8 *)(param_3 + 0x20),uVar4);
  }
  return uVar2;
}