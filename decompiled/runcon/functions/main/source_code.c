main (int argc, char **argv)
{
  char *role = NULL;
  char *range = NULL;
  char *user = NULL;
  char *type = NULL;
  char *context = NULL;
  char *cur_context = NULL;
  char *file_context = NULL;
  char *new_context = NULL;
  bool compute_trans = false;

  context_t con;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  while (true)
    {
      int option_index = 0;
      int c = getopt_long (argc, argv, "+r:t:u:l:c", long_options,
                           &option_index);
      if (c == -1)
        break;
      switch (c)
        {
        case 'r':
          if (role)
            die (EXIT_FAILURE, 0, _("multiple roles"));
          role = optarg;
          break;
        case 't':
          if (type)
            die (EXIT_FAILURE, 0, _("multiple types"));
          type = optarg;
          break;
        case 'u':
          if (user)
            die (EXIT_FAILURE, 0, _("multiple users"));
          user = optarg;
          break;
        case 'l':
          if (range)
            die (EXIT_FAILURE, 0, _("multiple levelranges"));
          range = optarg;
          break;
        case 'c':
          compute_trans = true;
          break;

        case_GETOPT_HELP_CHAR;
        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);
        default:
          usage (EXIT_FAILURE);
          break;
        }
    }

  if (argc - optind == 0)
    {
      if (getcon (&cur_context) < 0)
        die (EXIT_FAILURE, errno, _("failed to get current context"));
      fputs (cur_context, stdout);
      fputc ('\n', stdout);
      return EXIT_SUCCESS;
    }

  if (!(user || role || type || range || compute_trans))
    {
      if (optind >= argc)
        {
          error (0, 0, _("you must specify -c, -t, -u, -l, -r, or context"));
          usage (EXIT_FAILURE);
        }
      context = argv[optind++];
    }

  if (optind >= argc)
    {
      error (0, 0, _("no command specified"));
      usage (EXIT_FAILURE);
    }

  if (is_selinux_enabled () != 1)
    die (EXIT_FAILURE, 0, _("%s may be used only on a SELinux kernel"),
         program_name);

  if (context)
    {
      con = context_new (context);
      if (!con)
        die (EXIT_FAILURE, errno, _("failed to create security context: %s"),
             quote (context));
    }
  else
    {
      if (getcon (&cur_context) < 0)
        die (EXIT_FAILURE, errno, _("failed to get current context"));

      /* We will generate context based on process transition */
      if (compute_trans)
        {
          /* Get context of file to be executed */
          if (getfilecon (argv[optind], &file_context) == -1)
            die (EXIT_FAILURE, errno,
                 _("failed to get security context of %s"),
                 quoteaf (argv[optind]));
          /* compute result of process transition */
          if (security_compute_create (cur_context, file_context,
                                       string_to_security_class ("process"),
                                       &new_context) != 0)
            die (EXIT_FAILURE, errno, _("failed to compute a new context"));
          /* free contexts */
          freecon (file_context);
          freecon (cur_context);

          /* set cur_context equal to new_context */
          cur_context = new_context;
        }

      con = context_new (cur_context);
      if (!con)
        die (EXIT_FAILURE, errno, _("failed to create security context: %s"),
             quote (cur_context));
      if (user && context_user_set (con, user))
        die (EXIT_FAILURE, errno, _("failed to set new user: %s"),
             quote (user));
      if (type && context_type_set (con, type))
        die (EXIT_FAILURE, errno, _("failed to set new type: %s"),
             quote (type));
      if (range && context_range_set (con, range))
        die (EXIT_FAILURE, errno, _("failed to set new range: %s"),
             quote (range));
      if (role && context_role_set (con, role))
        die (EXIT_FAILURE, errno, _("failed to set new role: %s"),
             quote (role));
    }

  if (security_check_context (context_str (con)) < 0)
    die (EXIT_FAILURE, errno, _("invalid context: %s"),
         quote (context_str (con)));

  if (setexeccon (context_str (con)) != 0)
    die (EXIT_FAILURE, errno, _("unable to set security context %s"),
         quote (context_str (con)));
  if (cur_context != NULL)
    freecon (cur_context);

  execvp (argv[optind], argv + optind);

  int exit_status = errno == ENOENT ? EXIT_ENOENT : EXIT_CANNOT_INVOKE;
  error (0, errno, "%s", quote (argv[optind]));
  return exit_status;
}