void main(int param_1,undefined8 *param_2)

{
  int iVar1;
  int *piVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  char *pcVar5;
  long in_FS_OFFSET;
  undefined auVar6 [16];
  undefined8 uStack120;
  ulong local_70;
  ulong local_68;
  ulong local_60;
  ulong local_58;
  char local_49;
  undefined4 local_44;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  local_49 = '\0';
  local_58 = 0;
  local_68 = 0;
  local_60 = 0;
  local_70 = 0;
  while( true ) {
    local_44 = 0;
    iVar1 = getopt_long(param_1,param_2,"+r:t:u:l:c",long_options,&local_44);
    if (iVar1 == -1) break;
    if (iVar1 < 0x76) {
      if (iVar1 < 99) {
        if (iVar1 == -0x83) {
          version_etc(stdout,"runcon","GNU coreutils",Version,"Russell Coker",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar1 == -0x82) {
          usage(0);
          break;
        }
        goto switchD_001026aa_caseD_64;
      }
      switch(iVar1) {
      case 99:
        local_49 = '\x01';
        break;
      default:
        goto switchD_001026aa_caseD_64;
      case 0x6c:
        if (local_60 != 0) goto LAB_001028da;
        local_60 = optarg;
        break;
      case 0x72:
        if (local_70 != 0) goto LAB_001028b6;
        local_70 = optarg;
        break;
      case 0x74:
        if (local_58 != 0) goto LAB_00102892;
        local_58 = optarg;
        break;
      case 0x75:
        goto switchD_001026aa_caseD_75;
      }
    }
    else {
switchD_001026aa_caseD_64:
      usage();
switchD_001026aa_caseD_75:
      if (local_68 != 0) goto LAB_0010286e;
      local_68 = optarg;
    }
  }
  if (optind != param_1) {
    if ((((local_70 | local_68) == 0) && ((local_60 | local_58) == 0)) && (local_49 == '\0')) {
      if (optind < param_1) {
        optind = optind + 1;
        goto LAB_001026e7;
      }
      pcVar5 = "you must specify -c, -t, -u, -l, -r, or context";
    }
    else {
LAB_001026e7:
      if (optind < param_1) goto LAB_00102840;
      pcVar5 = "no command specified";
    }
    uVar4 = dcgettext(0,pcVar5,5);
    error(0,0,uVar4);
    goto switchD_001026aa_caseD_64;
  }
  piVar2 = __errno_location();
  *piVar2 = 0x5f;
  uVar4 = dcgettext(0,"failed to get current context",5);
  error(1,*piVar2,uVar4);
LAB_00102840:
  uVar4 = program_name;
  uVar3 = dcgettext(0,"%s may be used only on a SELinux kernel",5);
  error(1,0,uVar3,uVar4);
LAB_0010286e:
  uVar4 = dcgettext(0,"multiple users",5);
  error(1,0,uVar4);
LAB_00102892:
  uVar4 = dcgettext(0,"multiple types",5);
  error(1,0,uVar4);
LAB_001028b6:
  uVar4 = dcgettext(0,"multiple roles",5);
  error(1,0,uVar4);
LAB_001028da:
  uVar4 = dcgettext(0,"multiple levelranges",5);
  auVar6 = error(1,0,uVar4);
  uVar4 = uStack120;
  uStack120 = SUB168(auVar6,0);
  (*(code *)PTR___libc_start_main_0010afd0)
            (main,uVar4,&local_70,0,0,SUB168(auVar6 >> 0x40,0),&uStack120);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}