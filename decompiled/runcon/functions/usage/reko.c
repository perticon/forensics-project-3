void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002540(fn0000000000002380(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002A4E;
	}
	fn00000000000024E0(fn0000000000002380(0x05, "Usage: %s CONTEXT COMMAND [args]\n  or:  %s [ -c ] [-u USER] [-r ROLE] [-t TYPE] [-l RANGE] COMMAND [args]\n", null), 0x01);
	fn0000000000002420(stdout, fn0000000000002380(0x05, "Run a program in a different SELinux security context.\nWith neither CONTEXT nor COMMAND, print the current security context.\n", null));
	fn0000000000002420(stdout, fn0000000000002380(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002420(stdout, fn0000000000002380(0x05, "  CONTEXT            Complete security context\n  -c, --compute      compute process transition context before modifying\n  -t, --type=TYPE    type (for same role as parent)\n  -u, --user=USER    user identity\n  -r, --role=ROLE    role\n  -l, --range=RANGE  levelrange\n", null));
	fn0000000000002420(stdout, fn0000000000002380(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002420(stdout, fn0000000000002380(0x05, "      --version     output version information and exit\n", null));
	struct Eq_664 * rbx_166 = fp - 0xB8 + 16;
	do
	{
		char * rsi_168 = rbx_166->qw0000;
		++rbx_166;
	} while (rsi_168 != null && fn0000000000002440(rsi_168, "runcon") != 0x00);
	ptr64 r13_181 = rbx_166->qw0008;
	if (r13_181 != 0x00)
	{
		fn00000000000024E0(fn0000000000002380(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_268 = fn00000000000024D0(null, 0x05);
		if (rax_268 == 0x00 || fn0000000000002310(0x03, "en_", rax_268) == 0x00)
			goto l0000000000002C56;
	}
	else
	{
		fn00000000000024E0(fn0000000000002380(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_210 = fn00000000000024D0(null, 0x05);
		if (rax_210 == 0x00 || fn0000000000002310(0x03, "en_", rax_210) == 0x00)
		{
			fn00000000000024E0(fn0000000000002380(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002C93:
			fn00000000000024E0(fn0000000000002380(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002A4E:
			fn0000000000002520(edi);
		}
		r13_181 = 0x7004;
	}
	fn0000000000002420(stdout, fn0000000000002380(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002C56:
	fn00000000000024E0(fn0000000000002380(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002C93;
}