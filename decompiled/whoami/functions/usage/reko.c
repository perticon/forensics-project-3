void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000025A0(fn00000000000023D0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000028AE;
	}
	fn0000000000002540(fn00000000000023D0(0x05, "Usage: %s [OPTION]...\n", null), 0x01);
	fn0000000000002480(stdout, fn00000000000023D0(0x05, "Print the user name associated with the current effective user ID.\nSame as id -un.\n\n", null));
	fn0000000000002480(stdout, fn00000000000023D0(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002480(stdout, fn00000000000023D0(0x05, "      --version     output version information and exit\n", null));
	struct Eq_598 * rbx_139 = fp - 0xB8 + 16;
	do
	{
		char * rsi_141 = rbx_139->qw0000;
		++rbx_139;
	} while (rsi_141 != null && fn00000000000024A0(rsi_141, "whoami") != 0x00);
	ptr64 r13_154 = rbx_139->qw0008;
	if (r13_154 != 0x00)
	{
		fn0000000000002540(fn00000000000023D0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_241 = fn0000000000002530(null, 0x05);
		if (rax_241 == 0x00 || fn0000000000002340(0x03, "en_", rax_241) == 0x00)
			goto l0000000000002A66;
	}
	else
	{
		fn0000000000002540(fn00000000000023D0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_183 = fn0000000000002530(null, 0x05);
		if (rax_183 == 0x00 || fn0000000000002340(0x03, "en_", rax_183) == 0x00)
		{
			fn0000000000002540(fn00000000000023D0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002AA3:
			fn0000000000002540(fn00000000000023D0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000028AE:
			fn0000000000002580(edi);
		}
		r13_154 = 0x7004;
	}
	fn0000000000002480(stdout, fn00000000000023D0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002A66:
	fn0000000000002540(fn00000000000023D0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002AA3;
}