undefined8 main(int param_1,undefined8 *param_2)

{
  __uid_t __uid;
  long lVar1;
  int *piVar2;
  passwd *ppVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  uVar5 = 0x102670;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"whoami","GNU coreutils",Version,1,usage,"Richard Mlynarik",0,uVar5);
  lVar1 = (long)optind;
  if (optind == param_1) {
    piVar2 = __errno_location();
    *piVar2 = 0;
    __uid = geteuid();
    param_2 = (undefined8 *)(ulong)__uid;
    if ((__uid != 0xffffffff) || (*piVar2 == 0)) {
      ppVar3 = getpwuid(__uid);
      if (ppVar3 != (passwd *)0x0) {
        puts(ppVar3->pw_name);
        return 0;
      }
    }
    uVar5 = dcgettext(0,"cannot find name for user ID %lu",5);
    lVar1 = error(1,*piVar2,uVar5,param_2);
  }
  uVar5 = quote(param_2[lVar1]);
  uVar4 = dcgettext(0,"extra operand %s",5);
  error(0,0,uVar4,uVar5);
                    /* WARNING: Subroutine does not return */
  usage(1);
}