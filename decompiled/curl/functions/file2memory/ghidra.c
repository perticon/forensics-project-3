undefined8 file2memory(undefined8 *param_1,undefined8 *param_2,FILE *param_3)

{
  int iVar1;
  size_t sVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  undefined auStack4200 [32];
  undefined local_1048 [4104];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 == (FILE *)0x0) {
    *param_2 = 0;
    uVar3 = 0;
    *param_1 = 0;
  }
  else {
    curlx_dyn_init(auStack4200,0x40000000);
    do {
      sVar2 = fread(local_1048,1,0x1000,param_3);
      iVar1 = ferror(param_3);
      if (iVar1 != 0) {
        curlx_dyn_free(auStack4200);
        *param_2 = 0;
        uVar3 = 0x15;
        *param_1 = 0;
        goto LAB_0011c9da;
      }
      if (sVar2 != 0) {
        iVar1 = curlx_dyn_addn(auStack4200,local_1048,sVar2);
        if (iVar1 != 0) {
          uVar3 = 0xe;
          goto LAB_0011c9da;
        }
      }
      iVar1 = feof(param_3);
    } while (iVar1 == 0);
    uVar3 = curlx_dyn_len(auStack4200);
    *param_2 = uVar3;
    uVar3 = curlx_dyn_ptr(auStack4200);
    *param_1 = uVar3;
    uVar3 = 0;
  }
LAB_0011c9da:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}