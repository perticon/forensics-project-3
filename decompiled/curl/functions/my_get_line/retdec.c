int64_t my_get_line(int64_t a1, int64_t * a2, int64_t a3) {
    int64_t v1 = __readfsqword(40); // 0x1d316
    char * v2 = (char *)a3; // 0x1d32c
    *v2 = 0;
    int64_t result; // 0x1d300
    while (function_bf50() != 0) {
        // 0x1d340
        if ((int32_t)curlx_dyn_add() != 0) {
            // 0x1d3a8
            *v2 = 1;
            result = 0;
            goto lab_0x1d384;
        }
        // 0x1d34f
        result = 1;
        if (function_b990() != 0) {
            goto lab_0x1d384;
        }
    }
    int64_t v3 = curlx_dyn_len(); // 0x1d379
    result = v3 & -256 | (int64_t)(v3 != 0);
    goto lab_0x1d384;
  lab_0x1d384:
    // 0x1d384
    if (v1 != __readfsqword(40)) {
        // 0x1d3bf
        return function_bd40();
    }
    // 0x1d397
    return result;
}