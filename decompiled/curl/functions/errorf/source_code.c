void errorf(struct GlobalConfig *config, const char *fmt, ...)
{
  if(!config->mute) {
    va_list ap;
    va_start(ap, fmt);
    voutf(config, ERROR_PREFIX, fmt, ap);
    va_end(ap);
  }
}