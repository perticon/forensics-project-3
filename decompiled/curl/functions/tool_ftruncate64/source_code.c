int tool_ftruncate64(int fd, curl_off_t where)
{
  intptr_t handle = _get_osfhandle(fd);

  if(_lseeki64(fd, where, SEEK_SET) < 0)
    return -1;

  if(!SetEndOfFile((HANDLE)handle))
    return -1;

  return 0;
}