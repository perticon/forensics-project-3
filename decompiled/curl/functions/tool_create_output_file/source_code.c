bool tool_create_output_file(struct OutStruct *outs,
                             struct OperationConfig *config)
{
  struct GlobalConfig *global;
  FILE *file = NULL;
  char *fname = outs->filename;
  char *aname = NULL;
  DEBUGASSERT(outs);
  DEBUGASSERT(config);
  global = config->global;
  if(!fname || !*fname) {
    warnf(global, "Remote filename has no length!\n");
    return FALSE;
  }

  if(config->output_dir && outs->is_cd_filename) {
    aname = aprintf("%s/%s", config->output_dir, fname);
    if(!aname) {
      errorf(global, "out of memory\n");
      return FALSE;
    }
    fname = aname;
  }

  if(config->file_clobber_mode == CLOBBER_ALWAYS ||
     (config->file_clobber_mode == CLOBBER_DEFAULT &&
      !outs->is_cd_filename)) {
    /* open file for writing */
    file = fopen(fname, "wb");
  }
  else {
    int fd;
    do {
      fd = open(fname, O_CREAT | O_WRONLY | O_EXCL | O_BINARY, OPENMODE);
      /* Keep retrying in the hope that it isn't interrupted sometime */
    } while(fd == -1 && errno == EINTR);
    if(config->file_clobber_mode == CLOBBER_NEVER && fd == -1) {
      int next_num = 1;
      size_t len = strlen(fname);
      size_t newlen = len + 13; /* nul + 1-11 digits + dot */
      char *newname;
      /* Guard against wraparound in new filename */
      if(newlen < len) {
        free(aname);
        errorf(global, "overflow in filename generation\n");
        return FALSE;
      }
      newname = malloc(newlen);
      if(!newname) {
        errorf(global, "out of memory\n");
        free(aname);
        return FALSE;
      }
      memcpy(newname, fname, len);
      newname[len] = '.';
      while(fd == -1 && /* haven't successfully opened a file */
            (errno == EEXIST || errno == EISDIR) &&
            /* because we keep having files that already exist */
            next_num < 100 /* and we haven't reached the retry limit */ ) {
        curlx_msnprintf(newname + len + 1, 12, "%d", next_num);
        next_num++;
        do {
          fd = open(newname, O_CREAT | O_WRONLY | O_EXCL | O_BINARY, OPENMODE);
          /* Keep retrying in the hope that it isn't interrupted sometime */
        } while(fd == -1 && errno == EINTR);
      }
      outs->filename = newname; /* remember the new one */
      outs->alloc_filename = TRUE;
    }
    /* An else statement to not overwrite existing files and not retry with
       new numbered names (which would cover
       config->file_clobber_mode == CLOBBER_DEFAULT && outs->is_cd_filename)
       is not needed because we would have failed earlier, in the while loop
       and `fd` would now be -1 */
    if(fd != -1) {
      file = fdopen(fd, "wb");
      if(!file)
        close(fd);
    }
  }

  if(!file) {
    warnf(global, "Failed to open the file %s: %s\n", fname,
          strerror(errno));
    free(aname);
    return FALSE;
  }
  free(aname);
  outs->s_isreg = TRUE;
  outs->fopened = TRUE;
  outs->stream = file;
  outs->bytes = 0;
  outs->init = 0;
  return TRUE;
}