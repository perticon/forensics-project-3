undefined8 tool_create_output_file(char **param_1,long param_2)

{
  int iVar1;
  undefined8 uVar2;
  int __fd;
  FILE *pFVar3;
  uint *puVar4;
  char *pcVar5;
  size_t __n;
  char *__file;
  uint __errnum;
  char *__ptr;
  
  __file = *param_1;
  uVar2 = *(undefined8 *)(param_2 + 0x4d8);
  if ((__file == (char *)0x0) || (*__file == '\0')) {
    warnf(uVar2,"Remote filename has no length!\n");
    return 0;
  }
  __ptr = *(char **)(param_2 + 0xb8);
  if (__ptr != (char *)0x0) {
    if (*(char *)((long)param_1 + 9) == '\0') {
      __ptr = (char *)0x0;
    }
    else {
      __file = (char *)curl_maprintf("%s/%s",__ptr,__file);
      __ptr = __file;
      if (__file == (char *)0x0) {
        errorf(uVar2,"out of memory\n");
        return 0;
      }
    }
  }
  if ((*(int *)(param_2 + 0x4d0) != 2) &&
     ((*(int *)(param_2 + 0x4d0) != 0 || (*(char *)((long)param_1 + 9) != '\0')))) {
    do {
      __fd = open(__file,0xc1,0x1b6);
      if (__fd != -1) goto LAB_0010da9e;
      puVar4 = (uint *)__errno_location();
      __errnum = *puVar4;
    } while (__errnum == 4);
    if (*(int *)(param_2 + 0x4d0) == 1) {
      __n = strlen(__file);
      pcVar5 = (char *)malloc(__n + 0xd);
      if (pcVar5 == (char *)0x0) {
        errorf(uVar2,"out of memory\n");
        free(__ptr);
        return 0;
      }
      memcpy(pcVar5,__file,__n);
      iVar1 = 1;
      __errnum = *puVar4;
      pcVar5[__n] = '.';
      while (((__errnum & 0xfffffffb) == 0x11 && (iVar1 != 100))) {
        curl_msnprintf(pcVar5 + __n + 1,0xc,&DAT_001235bc,iVar1);
        iVar1 = iVar1 + 1;
        do {
          __fd = open(pcVar5,0xc1,0x1b6);
          if (__fd != -1) {
            *param_1 = pcVar5;
            *(undefined *)(param_1 + 1) = 1;
            goto LAB_0010da9e;
          }
          __errnum = *puVar4;
        } while (__errnum == 4);
      }
      *param_1 = pcVar5;
      *(undefined *)(param_1 + 1) = 1;
    }
    goto LAB_0010da36;
  }
  pFVar3 = fopen(__file,"wb");
  if (pFVar3 != (FILE *)0x0) goto LAB_0010dabc;
LAB_0010da2f:
  puVar4 = (uint *)__errno_location();
  __errnum = *puVar4;
LAB_0010da36:
  pcVar5 = strerror(__errnum);
  warnf(uVar2,"Failed to open the file %s: %s\n",__file,pcVar5);
  free(__ptr);
  return 0;
LAB_0010da9e:
  pFVar3 = fdopen(__fd,"wb");
  if (pFVar3 != (FILE *)0x0) {
LAB_0010dabc:
    free(__ptr);
    param_1[2] = (char *)pFVar3;
    param_1[3] = (char *)0x0;
    param_1[4] = (char *)0x0;
    *(undefined2 *)((long)param_1 + 10) = 0x101;
    return 1;
  }
  close(__fd);
  goto LAB_0010da2f;
}