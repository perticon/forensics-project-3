CURLcode glob_match_url(char **result, char *filename, struct URLGlob *glob)
{
  char numbuf[18];
  char *appendthis = (char *)"";
  size_t appendlen = 0;
  struct curlx_dynbuf dyn;

  *result = NULL;

  /* We cannot use the glob_buffer for storage since the filename may be
   * longer than the URL we use.
   */
  curlx_dyn_init(&dyn, MAX_OUTPUT_GLOB_LENGTH);

  while(*filename) {
    if(*filename == '#' && ISDIGIT(filename[1])) {
      char *ptr = filename;
      unsigned long num = strtoul(&filename[1], &filename, 10);
      struct URLPattern *pat = NULL;

      if(num && (num < glob->size)) {
        unsigned long i;
        num--; /* make it zero based */
        /* find the correct glob entry */
        for(i = 0; i<glob->size; i++) {
          if(glob->pattern[i].globindex == (int)num) {
            pat = &glob->pattern[i];
            break;
          }
        }
      }

      if(pat) {
        switch(pat->type) {
        case UPTSet:
          if(pat->content.Set.elements) {
            appendthis = pat->content.Set.elements[pat->content.Set.ptr_s];
            appendlen =
              strlen(pat->content.Set.elements[pat->content.Set.ptr_s]);
          }
          break;
        case UPTCharRange:
          numbuf[0] = pat->content.CharRange.ptr_c;
          numbuf[1] = 0;
          appendthis = numbuf;
          appendlen = 1;
          break;
        case UPTNumRange:
          msnprintf(numbuf, sizeof(numbuf), "%0*lu",
                    pat->content.NumRange.padlength,
                    pat->content.NumRange.ptr_n);
          appendthis = numbuf;
          appendlen = strlen(numbuf);
          break;
        default:
          fprintf(stderr, "internal error: invalid pattern type (%d)\n",
                  (int)pat->type);
          curlx_dyn_free(&dyn);
          return CURLE_FAILED_INIT;
        }
      }
      else {
        /* #[num] out of range, use the #[num] in the output */
        filename = ptr;
        appendthis = filename++;
        appendlen = 1;
      }
    }
    else {
      appendthis = filename++;
      appendlen = 1;
    }
    if(curlx_dyn_addn(&dyn, appendthis, appendlen))
      return CURLE_OUT_OF_MEMORY;
  }

  if(curlx_dyn_addn(&dyn, "", 0))
    return CURLE_OUT_OF_MEMORY;

#if defined(MSDOS) || defined(WIN32)
  {
    char *sanitized;
    SANITIZEcode sc = sanitize_file_name(&sanitized, curlx_dyn_ptr(&dyn),
                                         (SANITIZE_ALLOW_PATH |
                                          SANITIZE_ALLOW_RESERVED));
    curlx_dyn_free(&dyn);
    if(sc)
      return CURLE_URL_MALFORMAT;
    *result = sanitized;
    return CURLE_OK;
  }
#else
  *result = curlx_dyn_ptr(&dyn);
  return CURLE_OK;
#endif /* MSDOS || WIN32 */
}