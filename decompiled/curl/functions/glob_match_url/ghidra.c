int glob_match_url(undefined8 *param_1,char *param_2,long param_3)

{
  char *__nptr;
  char *pcVar1;
  int iVar2;
  ulong uVar3;
  undefined8 uVar4;
  ulong uVar5;
  int *piVar6;
  size_t sVar7;
  char *__s;
  long in_FS_OFFSET;
  char *local_80;
  undefined local_78 [32];
  char local_58;
  undefined local_57;
  long local_40;
  
  sVar7 = 0;
  __s = "";
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  *param_1 = 0;
  local_80 = param_2;
  curlx_dyn_init(local_78,0x2800);
  do {
    if (*local_80 == '\0') {
      iVar2 = curlx_dyn_addn(local_78,&DAT_00124101,0);
      if (iVar2 == 0) {
        uVar4 = curlx_dyn_ptr(local_78);
        *param_1 = uVar4;
        goto LAB_001206a6;
      }
      break;
    }
    if ((*local_80 == '#') && (iVar2 = Curl_isdigit(local_80[1]), pcVar1 = local_80, iVar2 != 0)) {
      __nptr = local_80 + 1;
      uVar3 = strtoul(__nptr,&local_80,10);
      if ((uVar3 != 0) && (uVar3 < *(ulong *)(param_3 + 0x12c0))) {
        piVar6 = (int *)(param_3 + 4);
        uVar5 = 0;
        do {
          if (*piVar6 == (int)uVar3 + -1) {
            piVar6 = (int *)(uVar5 * 0x30 + param_3);
            iVar2 = *piVar6;
            if (iVar2 == 2) {
              local_58 = *(char *)((long)piVar6 + 10);
              local_57 = 0;
              sVar7 = 1;
              __s = &local_58;
              goto LAB_00120612;
            }
            if (iVar2 == 3) {
              __s = &local_58;
              curl_msnprintf(__s,0x12,"%0*lu",piVar6[6],*(undefined8 *)(piVar6 + 8));
              sVar7 = strlen(__s);
              goto LAB_00120612;
            }
            if (iVar2 == 1) {
              if (*(long *)(piVar6 + 2) != 0) {
                __s = *(char **)(*(long *)(piVar6 + 2) + (long)piVar6[5] * 8);
                sVar7 = strlen(__s);
              }
              goto LAB_00120612;
            }
            iVar2 = 2;
            curl_mfprintf(stderr,"internal error: invalid pattern type (%d)\n");
            curlx_dyn_free(local_78);
            goto LAB_001206a6;
          }
          uVar5 = uVar5 + 1;
          piVar6 = piVar6 + 0xc;
        } while (*(ulong *)(param_3 + 0x12c0) != uVar5);
      }
      sVar7 = 1;
      __s = pcVar1;
      local_80 = __nptr;
    }
    else {
      sVar7 = 1;
      __s = local_80;
      local_80 = local_80 + 1;
    }
LAB_00120612:
    iVar2 = curlx_dyn_addn(local_78,__s,sVar7);
  } while (iVar2 == 0);
  iVar2 = 0x1b;
LAB_001206a6:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar2;
}