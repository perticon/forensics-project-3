ulong oct2nummax(long *param_1,long param_2,long param_3)

{
  ulong uVar1;
  
  if (param_2 != 0) {
    uVar1 = getnum_part_0(param_1,param_2,8);
    if ((int)uVar1 == 0) {
      if (param_3 < *param_1) {
        uVar1 = 0x11;
      }
      else {
        uVar1 = uVar1 & 0xffffffff;
        if (*param_1 < 0) {
          uVar1 = 0xb;
        }
      }
    }
    return uVar1;
  }
  return 10;
}