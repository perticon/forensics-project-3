undefined8 max5data(long param_1,undefined8 param_2)

{
  if (param_1 < 100000) {
    curl_msnprintf(param_2,6,&DAT_00139854,param_1);
    return param_2;
  }
  if (param_1 < 0x9c4000) {
    curl_msnprintf(param_2,6,"%4ldk",param_1 >> 10);
  }
  else {
    if (param_1 < 0x6400000) {
      curl_msnprintf(param_2,6,"%2ld.%0ldM",param_1 >> 0x14,
                     (ulong)((uint)param_1 & 0xfffff) / 0x19999);
      return param_2;
    }
    if (param_1 < 0x271000000) {
      curl_msnprintf(param_2,6,"%4ldM",param_1 >> 0x14);
    }
    else if (param_1 < 0x1900000000) {
      curl_msnprintf(param_2,6,"%2ld.%0ldG",param_1 >> 0x1e,
                     (ulong)(((uint)param_1 & 0x3fffffff) >> 1) / 0x3333333);
    }
    else if (param_1 < 0x9c400000000) {
      curl_msnprintf(param_2,6,"%4ldG",param_1 >> 0x1e);
    }
    else if (param_1 < 0x27100000000000) {
      curl_msnprintf(param_2,6,"%4ldT",param_1 >> 0x28);
    }
    else {
      curl_msnprintf(param_2,6,"%4ldP",param_1 >> 0x32);
    }
  }
  return param_2;
}