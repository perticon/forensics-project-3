void** max5data(void** rdi, void** rsi) {
    void** rcx3;

    if (reinterpret_cast<signed char>(rdi) <= reinterpret_cast<signed char>(0x1869f)) {
        fun_ba50(rsi, 6, "%5ld", rdi);
        return rsi;
    } else {
        if (reinterpret_cast<signed char>(rdi) <= reinterpret_cast<signed char>(0x9c3fff)) {
            fun_ba50(rsi, 6, "%4ldk", reinterpret_cast<signed char>(rdi) >> 10);
        } else {
            if (reinterpret_cast<signed char>(rdi) <= reinterpret_cast<signed char>(0x63fffff)) {
                fun_ba50(rsi, 6, "%2ld.%0ldM", reinterpret_cast<signed char>(rdi) >> 20);
                return rsi;
            } else {
                if (reinterpret_cast<signed char>(rdi) <= reinterpret_cast<signed char>(0x270ffffff)) {
                    fun_ba50(rsi, 6, "%4ldM", reinterpret_cast<signed char>(rdi) >> 20);
                } else {
                    if (reinterpret_cast<signed char>(rdi) <= reinterpret_cast<signed char>(0x18ffffffff)) {
                        rcx3 = reinterpret_cast<void**>(reinterpret_cast<signed char>(rdi) >> 30);
                        fun_ba50(rsi, 6, "%2ld.%0ldG", rcx3, rsi, 6, "%2ld.%0ldG", rcx3);
                    } else {
                        if (reinterpret_cast<signed char>(rdi) <= reinterpret_cast<signed char>(0x9c3ffffffff)) {
                            fun_ba50(rsi, 6, "%4ldG", reinterpret_cast<signed char>(rdi) >> 30);
                        } else {
                            if (reinterpret_cast<signed char>(rdi) > reinterpret_cast<signed char>(0x270fffffffffff)) {
                                fun_ba50(rsi, 6, "%4ldP", reinterpret_cast<signed char>(rdi) >> 50);
                            } else {
                                fun_ba50(rsi, 6, "%4ldT", reinterpret_cast<signed char>(rdi) >> 40);
                            }
                        }
                    }
                }
            }
        }
        return rsi;
    }
}