writeOffset(undefined8 param_1,undefined8 *param_2,long param_3,undefined8 param_4,char param_5)

{
  int iVar1;
  long in_FS_OFFSET;
  undefined8 local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_28 = 0;
  if (*(int *)((long)param_2 + 0xc) != 0) {
    iVar1 = curl_easy_getinfo(*(undefined8 *)(param_3 + 0x18),*(int *)((long)param_2 + 0xc),
                              &local_28);
    if (iVar1 == 0) {
      if (param_5 != '\0') {
        curl_mfprintf(param_1,"\"%s\":",*param_2);
      }
      curl_mfprintf(param_1,"%ld",local_28);
      goto LAB_00120a9c;
    }
  }
  if (param_5 != '\0') {
    curl_mfprintf(param_1,"\"%s\":null",*param_2);
  }
LAB_00120a9c:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}