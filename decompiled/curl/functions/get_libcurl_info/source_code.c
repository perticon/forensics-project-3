CURLcode get_libcurl_info(void)
{
  const char *const *proto;

  /* Pointer to libcurl's run-time version information */
  curlinfo = curl_version_info(CURLVERSION_NOW);
  if(!curlinfo)
    return CURLE_FAILED_INIT;

  /* Build CURLPROTO_* bit pattern with libcurl's built-in protocols */
  built_in_protos = 0;
  if(curlinfo->protocols) {
    for(proto = curlinfo->protocols; *proto; proto++) {
      struct proto_name_pattern const *p;
      for(p = possibly_built_in; p->proto_name; p++) {
        if(curl_strequal(*proto, p->proto_name)) {
          built_in_protos |= p->proto_pattern;
          break;
        }
      }
    }
  }

  return CURLE_OK;
}