void** tool_mime_new_filedata(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int64_t rbp7;
    void** rdi8;
    void* rax9;
    void** rax10;
    void** rax11;
    void** rdx12;
    void** rdi13;
    void** eax14;
    void** rdi15;
    void** v16;
    void** rax17;
    void* rsp18;
    void** r13_19;
    void** rdi20;
    int32_t eax21;
    uint32_t v22;
    int32_t eax23;
    int64_t r14_24;
    void** rax25;
    void** r15_26;
    void* v27;
    void* rdx28;
    int64_t v29;
    void** rsi30;
    int32_t eax31;
    int64_t rax32;
    int64_t rax33;
    int64_t v34;
    int32_t eax35;
    void** rsi36;
    int32_t eax37;
    void** rsi38;
    int32_t eax39;
    void** rsi40;
    int32_t eax41;
    void** rax42;
    void** rdx43;

    *reinterpret_cast<uint32_t*>(&rbp7) = *reinterpret_cast<uint32_t*>(&rdx);
    rdi8 = rsi;
    rax9 = g28;
    *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(27);
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 45) || *reinterpret_cast<void***>(rsi + 1)) {
        rax10 = fun_c010(rdi8);
        if (!rax10) {
            addr_103d8_3:
            *reinterpret_cast<int32_t*>(&rax11) = 0;
            *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rsi) = 0x70;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi8) = 1;
            *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
            rax11 = fun_baf0(1, 0x70, rdx, rcx, r8, r9);
            if (!rax11) {
                rdi8 = rax10;
                fun_bde0(rdi8);
                rax11 = rax11;
            } else {
                *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(3);
                *reinterpret_cast<void***>(rax11 + 8) = rdi;
                if (rdi) {
                    rdx12 = *reinterpret_cast<void***>(rdi + 72);
                    *reinterpret_cast<void***>(rdi + 72) = rax11;
                    *reinterpret_cast<void***>(rax11 + 16) = rdx12;
                }
                *reinterpret_cast<uint32_t*>(&rbp7) = *reinterpret_cast<unsigned char*>(&rbp7);
                *reinterpret_cast<void***>(rax11 + 24) = rax10;
                *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(4 - *reinterpret_cast<uint32_t*>(&rbp7));
            }
        }
    } else {
        rdi13 = stdin;
        eax14 = fun_bcc0(rdi13);
        rdi15 = stdin;
        v16 = reinterpret_cast<void**>(0);
        rax17 = fun_b870(rdi15);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8 - 8 + 8 - 8 + 8);
        r13_19 = rax17;
        if (reinterpret_cast<signed char>(eax14) < reinterpret_cast<signed char>(0) || (reinterpret_cast<signed char>(rax17) < reinterpret_cast<signed char>(0) || ((rdi20 = eax14, *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0, eax21 = fun_bfa0(rdi20, reinterpret_cast<int64_t>(rsp18) + 32, rdx), rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8), !!eax21) || (v22 & 0xf000) != 0x8000))) {
            rdx = stdin;
            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 24);
            rdi8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 16);
            eax23 = file2memory(rdi8, rsi, rdi8, rsi);
            if (eax23 == 14) 
                goto addr_103d8_3;
            *reinterpret_cast<void***>(&r14_24) = reinterpret_cast<void**>(26);
            if (eax23 == 21) 
                goto addr_10282_12;
            *reinterpret_cast<void***>(&r14_24) = reinterpret_cast<void**>(0);
            if (!1) 
                goto addr_10282_12;
            rdi8 = reinterpret_cast<void**>(0x24101);
            rax25 = fun_c010(0x24101);
            v16 = rax25;
            if (rax25) 
                goto addr_10282_12; else 
                goto addr_103d2_15;
        } else {
            r15_26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v27) - reinterpret_cast<unsigned char>(r13_19));
            if (reinterpret_cast<signed char>(r15_26) < reinterpret_cast<signed char>(0)) {
                r15_26 = reinterpret_cast<void**>(0);
            }
            *reinterpret_cast<void***>(&r14_24) = reinterpret_cast<void**>(0);
            goto addr_10290_19;
        }
    }
    addr_102ed_20:
    rdx28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax9) - reinterpret_cast<uint64_t>(g28));
    if (!rdx28) {
        return rax11;
    }
    fun_bd40();
    if (rsi) 
        goto addr_10415_24;
    goto v29;
    addr_10415_24:
    rsi30 = *reinterpret_cast<void***>(rsi + 16);
    eax31 = tool2curlparts(rdi8, rsi30, rdx28);
    if (!eax31) {
        rax32 = fun_b900(rdx28, rsi30);
        if (!rax32) {
            goto addr_10437_28;
        } else {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi)) <= reinterpret_cast<unsigned char>(6)) {
                *reinterpret_cast<void***>(&rax33) = *reinterpret_cast<void***>(rsi);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
                goto *reinterpret_cast<int32_t*>(0x23c9c + rax33 * 4) + 0x23c9c;
            }
            if (!*reinterpret_cast<void***>(rsi + 40)) 
                goto addr_10485_32;
        }
    } else {
        addr_10437_28:
        goto v34;
    }
    eax35 = fun_b8b0(rax32, rax32);
    if (eax35) 
        goto addr_1050a_35;
    addr_10485_32:
    rsi36 = *reinterpret_cast<void***>(rsi + 48);
    eax37 = fun_c000(rax32, rsi36);
    if (!eax37 && ((rsi38 = *reinterpret_cast<void***>(rsi + 64), eax39 = fun_ba40(rax32, rsi38), !eax39) && (rsi40 = *reinterpret_cast<void***>(rsi + 56), eax41 = fun_bee0(rax32, rsi40), !eax41))) {
    }
    addr_1050a_35:
    goto addr_10437_28;
    addr_10282_12:
    *reinterpret_cast<int32_t*>(&r13_19) = 0;
    *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
    rax42 = curlx_uztoso(0, rsi, rdx, rcx);
    r15_26 = rax42;
    addr_10290_19:
    *reinterpret_cast<int32_t*>(&rsi) = 0x70;
    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
    rax11 = fun_baf0(1, 0x70, rdx, rcx, r8, r9);
    rdi8 = v16;
    if (!rax11) {
        fun_bde0(rdi8, rdi8);
        rax11 = rax11;
        goto addr_102ed_20;
    } else {
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(5);
        *reinterpret_cast<void***>(rax11 + 8) = rdi;
        if (rdi) {
            rdx43 = *reinterpret_cast<void***>(rdi + 72);
            *reinterpret_cast<void***>(rdi + 72) = rax11;
            *reinterpret_cast<void***>(rax11 + 16) = rdx43;
        }
        *reinterpret_cast<uint32_t*>(&rbp7) = *reinterpret_cast<unsigned char*>(&rbp7);
        *reinterpret_cast<void***>(rax11 + 24) = rdi8;
        *reinterpret_cast<void***>(rax11 + 80) = r13_19;
        *reinterpret_cast<void***>(rax11 + 88) = r15_26;
        *reinterpret_cast<void***>(rax11 + 96) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(6 - *reinterpret_cast<uint32_t*>(&rbp7));
        *reinterpret_cast<void***>(rcx) = *reinterpret_cast<void***>(&r14_24);
        goto addr_102ed_20;
    }
    addr_103d2_15:
    goto addr_103d8_3;
}