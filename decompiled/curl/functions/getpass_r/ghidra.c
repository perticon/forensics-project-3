undefined * getpass_r(char *param_1,undefined *param_2,size_t param_3)

{
  int __fd;
  ssize_t sVar1;
  
  __fd = open("/dev/tty",0);
  if (__fd == -1) {
    __fd = 0;
  }
  ttyecho_part_0(__fd);
  fputs(param_1,stderr);
  sVar1 = read(__fd,param_2,param_3);
  if (sVar1 < 1) {
    *param_2 = 0;
  }
  else {
    param_2[sVar1 + -1] = 0;
  }
  fputc(10,stderr);
  tcsetattr(__fd,2,(termios *)withecho_1);
  if (__fd == 0) {
    return param_2;
  }
  close(__fd);
  return param_2;
}