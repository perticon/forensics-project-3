undefined8 check_protocol(long param_1)

{
  int iVar1;
  long lVar2;
  long *plVar3;
  
  lVar2 = curl_version_info(9);
  if (param_1 == 0) {
    return 3;
  }
  plVar3 = *(long **)(lVar2 + 0x40);
  lVar2 = *plVar3;
  while( true ) {
    if (lVar2 == 0) {
      return 0xd;
    }
    iVar1 = curl_strequal(lVar2,param_1);
    if (iVar1 != 0) break;
    lVar2 = plVar3[1];
    plVar3 = plVar3 + 1;
  }
  return 0;
}