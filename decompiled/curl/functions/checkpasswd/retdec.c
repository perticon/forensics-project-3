int64_t checkpasswd(char * a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5) {
    // 0x1c570
    int128_t v1; // 0x1c570
    int128_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x1c584
    int64_t result = 0; // 0x1c59a
    if (a4 != 0) {
        // 0x1c5a0
        result = 0;
        if (!(((char)a4 == 59 | function_b990() != 0))) {
            int64_t v4 = function_b990(); // 0x1c5fe
            int128_t v5 = __asm_pxor(v2, v2); // 0x1c608
            int64_t v6; // bp-2104, 0x1c570
            __asm_rep_stosq_memset((char *)&v6, 0, 254);
            __asm_movaps(v5);
            curlx_dyn_init();
            if (v4 == 0) {
                // 0x1c694
                function_ba50();
                getpass_r();
            } else {
                char * v7 = (char *)v4;
                *v7 = 0;
                function_ba50();
                getpass_r();
                *v7 = 59;
            }
            int64_t v8 = curlx_dyn_addf(); // 0x1c6a6
            result = 27;
            if ((int32_t)v8 == 0) {
                // 0x1c6af
                function_bde0();
                *(int64_t *)a4 = curlx_dyn_ptr();
                result = v8 & 0xffffffff;
            }
        }
    }
    // 0x1c5c4
    if (v3 != __readfsqword(40)) {
        // 0x1c71b
        return function_bd40();
    }
    // 0x1c5db
    return result;
}