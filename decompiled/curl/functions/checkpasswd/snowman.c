int64_t checkpasswd(void** rdi, void** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void** r15_6;
    void* rax7;
    void* v8;
    int32_t r8d9;
    void** r13_10;
    void** r12_11;
    int32_t r14d12;
    void** rbx13;
    void** rax14;
    void* rax15;
    int64_t rax16;
    void** rax17;
    void* rsp18;
    int64_t rcx19;
    void** r15_20;
    void** rbp21;
    void** rsi22;
    void* rsp23;
    void* rax24;
    void** rax25;
    void** rdx26;
    void** rsi27;
    void** rax28;
    void* rdx29;
    int64_t v30;
    void** rax31;
    void** r8_32;
    void** r14_33;
    void** r9_34;
    void** rcx35;
    void* rsp36;
    int32_t eax37;
    void** rdi38;
    void** rax39;

    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x948);
    r15_6 = *reinterpret_cast<void***>(rcx);
    rax7 = g28;
    v8 = rax7;
    if (!r15_6) {
        r8d9 = 0;
        goto addr_1c5c4_3;
    }
    r13_10 = rdi;
    r12_11 = rsi;
    rdi = r15_6;
    *reinterpret_cast<int32_t*>(&rsi) = 58;
    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
    r14d12 = *reinterpret_cast<int32_t*>(&rdx);
    rbx13 = rcx;
    rax14 = fun_b990(rdi, 58);
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    r8d9 = 0;
    if (rax14 || *reinterpret_cast<void***>(r15_6) == 59) {
        addr_1c5c4_3:
        rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
        if (!rax15) {
            *reinterpret_cast<int32_t*>(&rax16) = r8d9;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
            return rax16;
        }
    } else {
        rax17 = fun_b990(r15_6, 59);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        *reinterpret_cast<int32_t*>(&rcx19) = 0xfe;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0;
        __asm__("pxor xmm0, xmm0");
        r15_20 = rax17;
        rbp21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 16);
        *reinterpret_cast<int32_t*>(&rsi22) = 0x19000;
        *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
        while (rcx19) {
            --rcx19;
            rsi22 = rsi22 + 8;
        }
        __asm__("movaps [rsp+0x130], xmm0");
        curlx_dyn_init(rbp21, rsi22, rdx);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        if (!r15_20) 
            goto addr_1c640_10; else 
            goto addr_1c63c_11;
    }
    fun_bd40();
    rax24 = g28;
    rax25 = fun_b880();
    *reinterpret_cast<int32_t*>(&rdx26) = *reinterpret_cast<int32_t*>(&rdx);
    *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
    rsi27 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 - 8 - 8 - 8 - 24 - 8 + 8);
    *reinterpret_cast<void***>(rax25) = reinterpret_cast<void**>(0);
    rax28 = fun_be90(rsi, rsi27, rsi, rsi27);
    if (*reinterpret_cast<void***>(rax25) != 34) 
        goto addr_1c775_14;
    addr_1c793_15:
    rdx29 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax24) - reinterpret_cast<uint64_t>(g28));
    if (rdx29) {
        fun_bd40();
    } else {
        goto v30;
    }
    addr_1c775_14:
    if (!rsi) {
        goto addr_1c793_15;
    } else {
        rax31 = fun_b9d0(rsi, rsi27, rdx26, rsi, rsi27, rdx26);
        if (!(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rax31))) {
            *reinterpret_cast<void***>(rdi) = rax28;
            goto addr_1c793_15;
        }
    }
    addr_1c640_10:
    r8_32 = *reinterpret_cast<void***>(rbx13);
    if (r12_11 || !*reinterpret_cast<signed char*>(&r14d12)) {
        r14_33 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 48);
        r9_34 = r12_11 + 1;
        rcx35 = r13_10;
        fun_ba50(r14_33, 0x100, "Enter %s password for user '%s' on URL #%zu:", rcx35, r14_33, 0x100, "Enter %s password for user '%s' on URL #%zu:", rcx35);
        rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
    } else {
        r14_33 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 48);
        rcx35 = r13_10;
        fun_ba50(r14_33, 0x100, "Enter %s password for user '%s':", rcx35, r14_33, 0x100, "Enter %s password for user '%s':", rcx35);
        rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
    }
    r12_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp36) + 0x130);
    getpass_r(r14_33, r12_11, 0x800, rcx35, r8_32, r9_34);
    if (r15_20) {
        *reinterpret_cast<void***>(r15_20) = reinterpret_cast<void**>(59);
    }
    rdx = *reinterpret_cast<void***>(rbx13);
    rsi = reinterpret_cast<void**>("%s:%s");
    rdi = rbp21;
    eax37 = curlx_dyn_addf(rdi, "%s:%s", rdx, r12_11, r8_32, r9_34);
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8 - 8 + 8);
    if (eax37) {
        r8d9 = 27;
        goto addr_1c5c4_3;
    } else {
        rdi38 = *reinterpret_cast<void***>(rbx13);
        fun_bde0(rdi38, rdi38);
        rdi = rbp21;
        rax39 = curlx_dyn_ptr(rdi, "%s:%s", rdi, "%s:%s");
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8);
        r8d9 = eax37;
        *reinterpret_cast<void***>(rbx13) = rax39;
        goto addr_1c5c4_3;
    }
    addr_1c63c_11:
    *reinterpret_cast<void***>(r15_20) = reinterpret_cast<void**>(0);
    goto addr_1c640_10;
}