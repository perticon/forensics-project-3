int checkpasswd(undefined8 param_1,long param_2,char param_3,char **param_4)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  long lVar4;
  undefined8 *puVar5;
  long in_FS_OFFSET;
  byte bVar6;
  undefined local_968 [32];
  undefined local_948 [256];
  undefined local_848 [16];
  undefined8 local_838 [255];
  long local_40;
  
  bVar6 = 0;
  pcVar3 = *param_4;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (pcVar3 == (char *)0x0) {
    iVar1 = 0;
  }
  else {
    pcVar2 = strchr(pcVar3,0x3a);
    iVar1 = 0;
    if ((pcVar2 == (char *)0x0) && (*pcVar3 != ';')) {
      pcVar3 = strchr(pcVar3,0x3b);
      puVar5 = local_838;
      for (lVar4 = 0xfe; lVar4 != 0; lVar4 = lVar4 + -1) {
        *puVar5 = 0;
        puVar5 = puVar5 + (ulong)bVar6 * -2 + 1;
      }
      local_848 = (undefined  [16])0x0;
      curlx_dyn_init(0,local_968,0x19000);
      if (pcVar3 != (char *)0x0) {
        *pcVar3 = '\0';
      }
      if ((param_2 == 0) && (param_3 != '\0')) {
        curl_msnprintf(local_948,0x100,"Enter %s password for user \'%s\':",param_1,*param_4);
      }
      else {
        curl_msnprintf(local_948,0x100,"Enter %s password for user \'%s\' on URL #%zu:",param_1,
                       *param_4,param_2 + 1);
      }
      getpass_r(local_948,local_848,0x800);
      if (pcVar3 != (char *)0x0) {
        *pcVar3 = ';';
      }
      iVar1 = curlx_dyn_addf(local_968,"%s:%s",*param_4,local_848);
      if (iVar1 == 0) {
        free(*param_4);
        pcVar3 = (char *)curlx_dyn_ptr(local_968);
        *param_4 = pcVar3;
      }
      else {
        iVar1 = 0x1b;
      }
    }
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}