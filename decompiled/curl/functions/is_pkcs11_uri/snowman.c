unsigned char is_pkcs11_uri(void** rdi, void** rsi, void** rdx, void** rcx) {
    int64_t rax5;

    rax5 = fun_ba20(rdi, "pkcs11:", 7, rcx);
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<int32_t*>(&rax5)));
}