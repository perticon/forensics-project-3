uint is_pkcs11_uri(undefined8 param_1)

{
  uint uVar1;
  
  uVar1 = curl_strnequal(param_1,"pkcs11:",7);
  return uVar1 & 0xffffff00 | (uint)(uVar1 != 0);
}