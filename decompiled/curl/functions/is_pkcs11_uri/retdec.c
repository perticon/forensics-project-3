int64_t is_pkcs11_uri(int64_t a1) {
    int64_t v1 = function_ba20(); // 0x16780
    return v1 & -256 | (int64_t)((int32_t)v1 != 0);
}