int tool_setopt(undefined8 param_1,byte param_2,long param_3,long param_4,char *param_5,uint param_6
               ,char *param_7)

{
  int iVar1;
  void *__ptr;
  char *pcVar2;
  undefined8 uVar3;
  undefined1 *puVar4;
  long in_FS_OFFSET;
  char cVar5;
  bool bVar6;
  char *local_1b0;
  char local_178 [264];
  long local_70;
  
  local_70 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_6 < 10000) {
    puVar4 = setopt_nv_CURLNONZERODEFAULTS;
    pcVar2 = "CURLOPT_SSL_VERIFYPEER";
    do {
      iVar1 = strcmp(param_5,pcVar2);
      if (iVar1 == 0) {
        pcVar2 = *(char **)(puVar4 + 8);
        goto LAB_0011f2f1;
      }
      pcVar2 = *(char **)(puVar4 + 0x10);
      puVar4 = puVar4 + 0x10;
    } while (pcVar2 != (char *)0x0);
    pcVar2 = (char *)0x0;
LAB_0011f2f1:
    curl_msnprintf(local_178,0x100,&DAT_00139a78);
    iVar1 = curl_easy_setopt(param_1,param_6,param_7);
    if (pcVar2 != param_7) {
LAB_0011f32e:
      local_1b0 = local_178;
      param_2 = 0;
      bVar6 = false;
      cVar5 = '\0';
      goto LAB_0011f33b;
    }
  }
  else {
    if (param_6 < 30000) {
      if (param_6 < 20000) {
        param_2 = param_7 != (char *)0x0 & param_2;
        if (param_2 == 0) {
          cVar5 = param_7 != (char *)0x0;
          local_1b0 = "objectpointer";
          if (param_7 == (char *)0x0) {
            local_1b0 = (char *)0x0;
          }
          bVar6 = param_7 == (char *)0x0;
        }
        else {
          bVar6 = false;
          cVar5 = false;
          local_1b0 = param_7;
        }
      }
      else {
        local_1b0 = "functionpointer";
        if (param_7 == (char *)0x0) {
          local_1b0 = (char *)0x0;
        }
        param_2 = 0;
        bVar6 = param_7 == (char *)0x0;
        cVar5 = !bVar6;
      }
      iVar1 = curl_easy_setopt(param_1,param_6);
    }
    else {
      if (param_6 < 40000) {
        curl_msnprintf(local_178,0x100,"(curl_off_t)%ld");
        iVar1 = curl_easy_setopt(param_1,param_6,param_7);
        if (param_7 != (char *)0x0) goto LAB_0011f32e;
        goto LAB_0011f528;
      }
      local_1b0 = "blobpointer";
      if (param_7 == (char *)0x0) {
        local_1b0 = (char *)0x0;
      }
      bVar6 = param_7 == (char *)0x0;
      cVar5 = '\x01' - bVar6;
      iVar1 = curl_easy_setopt(param_1,param_6);
      param_2 = 0;
    }
LAB_0011f33b:
    __ptr = *(void **)(param_3 + 0x38);
    if (__ptr == (void *)0x0) goto LAB_0011f3b8;
    if ((!bVar6) && (iVar1 == 0)) {
      if (cVar5 == '\0') {
        if (param_2 == 0) {
          __ptr = (void *)0x0;
          iVar1 = easysrc_addf(&easysrc_code,"curl_easy_setopt(hnd, %s, %s);",param_5,local_1b0);
        }
        else {
          uVar3 = 0xffffffffffffffff;
          if (param_6 == 0x271f) {
            uVar3 = *(undefined8 *)(param_4 + 0x88);
          }
          __ptr = (void *)c_escape(local_1b0,uVar3);
          if (__ptr == (void *)0x0) {
            iVar1 = 0x1b;
          }
          else {
            iVar1 = easysrc_addf(&easysrc_code,"curl_easy_setopt(hnd, %s, \"%s\");",param_5,__ptr);
          }
        }
      }
      else {
        __ptr = (void *)0x0;
        iVar1 = easysrc_addf(&easysrc_toohard,"%s set to a %s",param_5,local_1b0);
      }
      goto LAB_0011f3b8;
    }
  }
LAB_0011f528:
  __ptr = (void *)0x0;
LAB_0011f3b8:
  free(__ptr);
  if (local_70 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}