int tool_setopt_bitmask(undefined8 param_1,long param_2,undefined8 param_3,undefined4 param_4,
                       long *param_5,ulong param_6)

{
  long *plVar1;
  long lVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  size_t sVar6;
  long in_FS_OFFSET;
  char acStack152 [88];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar3 = curl_easy_setopt(param_1,param_4,param_6);
  iVar5 = iVar3;
  if (((param_6 != 0) && (*(long *)(param_2 + 0x38) != 0)) && (iVar3 == 0)) {
    curl_msnprintf(acStack152,0x50,"curl_easy_setopt(hnd, %s, ",param_3);
    lVar2 = *param_5;
    while (lVar2 != 0) {
      if ((~param_6 & param_5[1]) == 0) {
        param_6 = param_6 & ~param_5[1];
        if (param_6 == 0) {
          iVar4 = easysrc_addf(&easysrc_code,"%s(long)%s%s",acStack152,lVar2,");");
          iVar5 = iVar3;
          if (iVar4 != 0) {
            iVar5 = iVar4;
          }
          goto LAB_0011f024;
        }
        iVar5 = easysrc_addf(&easysrc_code,"%s(long)%s%s",acStack152,lVar2,&DAT_00139a41);
        if (iVar5 != 0) goto LAB_0011f024;
        sVar6 = strlen(acStack152);
        curl_msnprintf(acStack152,0x50,&DAT_00139a34,sVar6,&DAT_00124101);
      }
      plVar1 = param_5 + 2;
      param_5 = param_5 + 2;
      lVar2 = *plVar1;
    }
    iVar4 = easysrc_addf(&easysrc_code,"%s%luUL);",acStack152,param_6);
    iVar5 = iVar3;
    if (iVar4 != 0) {
      iVar5 = iVar4;
    }
  }
LAB_0011f024:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}