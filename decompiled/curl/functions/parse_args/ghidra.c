int parse_args(long param_1,int param_2,long param_3)

{
  long lVar1;
  int iVar2;
  char *__ptr;
  undefined8 uVar3;
  void *pvVar4;
  char *__ptr_00;
  int iVar5;
  long lVar6;
  char cVar7;
  long in_FS_OFFSET;
  char local_41;
  long local_40;
  
  lVar6 = *(long *)(param_1 + 0x70);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_2 < 2) {
    iVar2 = 0;
    if (*(char *)(lVar6 + 0x460) == '\0') goto LAB_001153f1;
LAB_00115452:
    if (*(char *)(lVar6 + 0x1b7) == '\0') {
      if (*(char *)(lVar6 + 0x49) == '\0') goto LAB_001155b0;
      uVar3 = 0x14;
      iVar2 = 0x14;
    }
    else {
      uVar3 = 0x13;
      iVar2 = 0x13;
    }
    __ptr = (char *)0x0;
    uVar3 = param2text(uVar3);
LAB_001153be:
    helpf(*(undefined8 *)(param_1 + 8),&DAT_00123744,uVar3);
    if (__ptr != (char *)0x0) {
LAB_001153df:
      free(__ptr);
    }
  }
  else {
    cVar7 = '\x01';
    iVar5 = 1;
    do {
      __ptr = strdup(*(char **)(param_3 + (long)iVar5 * 8));
      if (__ptr == (char *)0x0) {
        iVar2 = 0xe;
        goto LAB_001153f1;
      }
      if ((cVar7 == '\0') || (*__ptr != '-')) {
        iVar2 = getparameter(&DAT_00124150,__ptr,&local_41,param_1);
        if (iVar2 != 0) goto LAB_00115389;
      }
      else if ((__ptr[1] == '-') && (__ptr[2] == '\0')) {
        cVar7 = '\0';
      }
      else {
        if (iVar5 < param_2 + -1) {
          __ptr_00 = strdup(*(char **)(param_3 + 8 + (long)iVar5 * 8));
          iVar2 = getparameter(__ptr,__ptr_00,&local_41,param_1);
          if (__ptr_00 != (char *)0x0) {
            free(__ptr_00);
          }
        }
        else {
          iVar2 = getparameter(__ptr,0,&local_41,param_1);
        }
        lVar1 = *(long *)(param_1 + 0x80);
        lVar6 = lVar1;
        if (iVar2 == 0xf) {
          if ((*(long *)(lVar1 + 0x1c8) != 0) && (*(long *)(*(long *)(lVar1 + 0x1c8) + 8) != 0)) {
            pvVar4 = malloc(0x548);
            *(void **)(lVar1 + 0x4e8) = pvVar4;
            if (pvVar4 == (void *)0x0) {
              iVar2 = 0xe;
              goto LAB_00115389;
            }
            config_init(pvVar4);
            lVar6 = *(long *)(lVar1 + 0x4e8);
            *(long *)(lVar6 + 0x4d8) = param_1;
            *(long *)(param_1 + 0x80) = lVar6;
            *(long *)(lVar6 + 0x4e0) = lVar1;
          }
        }
        else {
          if (iVar2 != 0) {
LAB_00115389:
            if (iVar2 - 5U < 4) goto LAB_001153df;
            uVar3 = param2text(iVar2);
            if ((*__ptr == ':') && (__ptr[1] == '\0')) goto LAB_001153be;
            helpf(*(undefined8 *)(param_1 + 8),"option %s: %s\n",__ptr,uVar3);
            goto LAB_001153df;
          }
          if (local_41 != '\0') {
            iVar5 = iVar5 + 1;
            cVar7 = local_41;
          }
        }
      }
      iVar5 = iVar5 + 1;
      free(__ptr);
    } while (iVar5 < param_2);
    if (*(char *)(lVar6 + 0x460) != '\0') goto LAB_00115452;
LAB_001155b0:
    iVar2 = 0;
  }
LAB_001153f1:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar2;
}