byte get_url_file_name(char **param_1,char *param_2)

{
  char *pcVar1;
  char *pcVar2;
  
  *param_1 = (char *)0x0;
  pcVar1 = strstr(param_2,"://");
  if (pcVar1 != (char *)0x0) {
    param_2 = pcVar1 + 3;
  }
  pcVar2 = strrchr(param_2,0x5c);
  pcVar1 = strrchr(param_2,0x2f);
  if (pcVar2 == (char *)0x0) {
    if (pcVar1 == (char *)0x0) {
      pcVar1 = "";
      goto LAB_0011c4df;
    }
  }
  else if ((pcVar1 == (char *)0x0) || (pcVar1 < pcVar2)) {
    pcVar1 = pcVar2;
  }
  pcVar1 = pcVar1 + 1;
LAB_0011c4df:
  pcVar1 = strdup(pcVar1);
  *param_1 = pcVar1;
  return -(pcVar1 == (char *)0x0) & 0x1b;
}