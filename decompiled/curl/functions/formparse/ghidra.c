undefined8 formparse(long param_1,char *param_2,long *param_3,long *param_4,byte param_5)

{
  char cVar1;
  undefined4 *puVar2;
  undefined8 uVar3;
  int iVar4;
  char *__s;
  char *pcVar5;
  undefined8 uVar6;
  undefined4 *puVar7;
  long lVar8;
  byte bVar9;
  long in_FS_OFFSET;
  char *local_90;
  int local_74;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = (char *)0x0;
  local_58 = (char *)0x0;
  local_50 = (char *)0x0;
  local_48 = 0;
  if (*param_4 == 0) {
    puVar7 = (undefined4 *)calloc(1,0x70);
    if (puVar7 == (undefined4 *)0x0) {
      *param_3 = 0;
      warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
      curl_slist_free_all(local_48);
      uVar6 = 1;
      goto LAB_00110965;
    }
    *param_3 = (long)puVar7;
    *puVar7 = 1;
    *(undefined8 *)(puVar7 + 2) = 0;
    *param_4 = (long)puVar7;
  }
  __s = strdup(param_2);
  if (__s == (char *)0x0) {
    warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
    curl_slist_free_all(local_48);
    uVar6 = 2;
    goto LAB_00110965;
  }
  pcVar5 = strchr(__s,0x3d);
  if (pcVar5 == (char *)0x0) {
    local_70 = pcVar5;
    warnf(*(undefined8 *)(param_1 + 0x4d8),"Illegally formatted input field!\n");
    free(__s);
    uVar6 = 0x17;
    goto LAB_00110965;
  }
  cVar1 = pcVar5[1];
  local_70 = pcVar5 + 1;
  bVar9 = cVar1 != '(' | param_5;
  if (__s < pcVar5) {
    *pcVar5 = '\0';
    local_90 = __s;
    if (bVar9 == 0) {
LAB_001109d1:
      iVar4 = get_param_part(param_1,0,&local_70,&local_68,&local_60,0,0,&local_48);
      if (iVar4 < 0) {
        free(__s);
        uVar6 = 3;
        goto LAB_00110965;
      }
      lVar8 = *param_4;
      puVar7 = (undefined4 *)calloc(1,0x70);
      uVar6 = local_48;
      if (puVar7 == (undefined4 *)0x0) {
        warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
        curl_slist_free_all(local_48);
        free(__s);
        uVar6 = 4;
        goto LAB_00110965;
      }
      *puVar7 = 1;
      *(long *)(puVar7 + 2) = lVar8;
      if (lVar8 != 0) {
        *(undefined8 *)(puVar7 + 4) = *(undefined8 *)(lVar8 + 0x48);
        *(undefined4 **)(lVar8 + 0x48) = puVar7;
      }
      *param_4 = (long)puVar7;
      local_48 = 0;
      *(undefined8 *)(puVar7 + 0x10) = uVar6;
      if (local_60 != (char *)0x0) {
        pcVar5 = strdup(local_60);
        *(char **)(puVar7 + 0xc) = pcVar5;
        if (pcVar5 == (char *)0x0) {
          warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
          curl_slist_free_all(local_48);
          free(__s);
          uVar6 = 5;
          goto LAB_00110965;
        }
      }
    }
    else {
LAB_001107bb:
      if (cVar1 == '@') {
        if (param_5 != 0) goto LAB_00110878;
        puVar7 = (undefined4 *)0x0;
        do {
          local_70 = local_70 + 1;
          iVar4 = get_param_part(param_1,0x2c,&local_70,&local_68,&local_60,&local_58,&local_50,
                                 &local_48);
          if (iVar4 < 0) {
            free(__s);
            uVar6 = 7;
            goto LAB_00110965;
          }
          if ((puVar7 == (undefined4 *)0x0) &&
             (puVar2 = (undefined4 *)*param_4, puVar7 = puVar2, iVar4 == 0x2c)) {
            puVar7 = (undefined4 *)calloc(1,0x70);
            if (puVar7 == (undefined4 *)0x0) {
              warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
              curl_slist_free_all(local_48);
              free(__s);
              uVar6 = 8;
              goto LAB_00110965;
            }
            *puVar7 = 1;
            *(undefined4 **)(puVar7 + 2) = puVar2;
            if (puVar2 != (undefined4 *)0x0) {
              *(undefined8 *)(puVar7 + 4) = *(undefined8 *)(puVar2 + 0x12);
              *(undefined4 **)(puVar2 + 0x12) = puVar7;
            }
          }
          lVar8 = tool_mime_new_filedata(puVar7,local_68,1,&local_74);
          uVar6 = local_48;
          if (lVar8 == 0) {
            warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
            curl_slist_free_all(local_48);
            free(__s);
            uVar6 = 9;
            goto LAB_00110965;
          }
          uVar3 = *(undefined8 *)(param_1 + 0x4d8);
          local_48 = 0;
          *(undefined8 *)(lVar8 + 0x40) = uVar6;
          *(undefined8 *)(lVar8 + 0x68) = uVar3;
          if (local_74 == 0x1a) {
            if (0 < *(long *)(lVar8 + 0x58)) {
              warnf(uVar3,"error while reading standard input\n");
              free(__s);
              uVar6 = 10;
              goto LAB_00110965;
            }
            free(*(void **)(lVar8 + 0x18));
            local_74 = 0;
            *(undefined8 *)(lVar8 + 0x18) = 0;
            *(undefined8 *)(lVar8 + 0x58) = 0xffffffffffffffff;
          }
          if (local_58 != (char *)0x0) {
            pcVar5 = strdup(local_58);
            *(char **)(lVar8 + 0x28) = pcVar5;
            if (pcVar5 == (char *)0x0) {
              warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
              curl_slist_free_all(local_48);
              free(__s);
              uVar6 = 0xb;
              goto LAB_00110965;
            }
          }
          if (local_60 != (char *)0x0) {
            pcVar5 = strdup(local_60);
            *(char **)(lVar8 + 0x30) = pcVar5;
            if (pcVar5 == (char *)0x0) {
              warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
              curl_slist_free_all(local_48);
              free(__s);
              uVar6 = 0xc;
              goto LAB_00110965;
            }
          }
          if (local_50 != (char *)0x0) {
            pcVar5 = strdup(local_50);
            *(char **)(lVar8 + 0x38) = pcVar5;
            if (pcVar5 == (char *)0x0) {
              warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
              curl_slist_free_all(local_48);
              free(__s);
              uVar6 = 0xd;
              goto LAB_00110965;
            }
          }
        } while (iVar4 != 0);
        puVar7 = *(undefined4 **)(*param_4 + 0x48);
      }
      else {
        if (cVar1 != '<') {
          if (param_5 != 0) goto LAB_00110878;
          iVar4 = get_param_part(param_1,0,&local_70,&local_68,&local_60,&local_58,&local_50,
                                 &local_48);
          if (iVar4 < 0) {
            free(__s);
            uVar6 = 0x11;
            goto LAB_00110965;
          }
LAB_0011087f:
          lVar8 = *param_4;
          pcVar5 = strdup(local_68);
          if (pcVar5 != (char *)0x0) {
            puVar7 = (undefined4 *)calloc(1,0x70);
            uVar6 = local_48;
            if (puVar7 != (undefined4 *)0x0) {
              *puVar7 = 2;
              *(long *)(puVar7 + 2) = lVar8;
              if (lVar8 != 0) {
                *(undefined8 *)(puVar7 + 4) = *(undefined8 *)(lVar8 + 0x48);
                *(undefined4 **)(lVar8 + 0x48) = puVar7;
              }
              *(char **)(puVar7 + 6) = pcVar5;
              local_48 = 0;
              *(undefined8 *)(puVar7 + 0x10) = uVar6;
              goto LAB_001108e0;
            }
            free(pcVar5);
          }
          warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
          curl_slist_free_all(local_48);
          free(__s);
          uVar6 = 0x12;
          goto LAB_00110965;
        }
        if (param_5 != 0) {
LAB_00110878:
          iVar4 = 0;
          local_68 = local_70;
          goto LAB_0011087f;
        }
        local_70 = pcVar5 + 2;
        iVar4 = get_param_part(param_1,0,&local_70,&local_68,&local_60,0,&local_50,&local_48);
        if (iVar4 < 0) {
          free(__s);
          uVar6 = 0xe;
          goto LAB_00110965;
        }
        puVar7 = (undefined4 *)tool_mime_new_filedata(*param_4,local_68,0,&local_74);
        uVar6 = local_48;
        if (puVar7 == (undefined4 *)0x0) {
          warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
          curl_slist_free_all(local_48);
          free(__s);
          uVar6 = 0xf;
          goto LAB_00110965;
        }
        uVar3 = *(undefined8 *)(param_1 + 0x4d8);
        local_48 = 0;
        *(undefined8 *)(puVar7 + 0x10) = uVar6;
        *(undefined8 *)(puVar7 + 0x1a) = uVar3;
        if (local_74 == 0x1a) {
          if (0 < *(long *)(puVar7 + 0x16)) {
            warnf(uVar3,"error while reading standard input\n");
            free(__s);
            uVar6 = 0x10;
            goto LAB_00110965;
          }
          free(*(void **)(puVar7 + 6));
          *(undefined8 *)(puVar7 + 6) = 0;
          *(undefined8 *)(puVar7 + 0x16) = 0xffffffffffffffff;
          local_74 = 0;
        }
LAB_001108e0:
        if (local_58 != (char *)0x0) {
          pcVar5 = strdup(local_58);
          *(char **)(puVar7 + 10) = pcVar5;
          if (pcVar5 == (char *)0x0) {
            warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
            curl_slist_free_all(local_48);
            free(__s);
            uVar6 = 0x13;
            goto LAB_00110965;
          }
        }
        if (local_60 != (char *)0x0) {
          pcVar5 = strdup(local_60);
          *(char **)(puVar7 + 0xc) = pcVar5;
          if (pcVar5 == (char *)0x0) {
            warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
            curl_slist_free_all(local_48);
            free(__s);
            uVar6 = 0x14;
            goto LAB_00110965;
          }
        }
        if (local_50 != (char *)0x0) {
          pcVar5 = strdup(local_50);
          *(char **)(puVar7 + 0xe) = pcVar5;
          if (pcVar5 == (char *)0x0) {
            warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
            curl_slist_free_all(local_48);
            free(__s);
            uVar6 = 0x15;
            goto LAB_00110965;
          }
        }
        if (iVar4 != 0) {
          *local_70 = (char)iVar4;
          warnf(*(undefined8 *)(param_1 + 0x4d8),"garbage at end of field specification: %s\n",
                local_70);
        }
      }
    }
    if (local_90 != (char *)0x0) {
      pcVar5 = strdup(local_90);
      *(char **)(puVar7 + 8) = pcVar5;
      if (pcVar5 == (char *)0x0) {
        warnf(*(undefined8 *)(param_1 + 0x4d8),"out of memory!\n");
        curl_slist_free_all(local_48);
        free(__s);
        uVar6 = 0x16;
        goto LAB_00110965;
      }
    }
  }
  else {
    *pcVar5 = '\0';
    if (bVar9 == 0) {
      local_90 = (char *)0x0;
      goto LAB_001109d1;
    }
    if ((pcVar5[1] != ')') || (pcVar5[2] != '\0')) {
      local_90 = (char *)0x0;
      goto LAB_001107bb;
    }
    if (param_5 != 0) {
      local_90 = (char *)0x0;
      goto LAB_00110878;
    }
    if (*param_4 == *param_3) {
      warnf(*(undefined8 *)(param_1 + 0x4d8),"no multipart to terminate!\n");
      free(__s);
      uVar6 = 6;
      goto LAB_00110965;
    }
    *param_4 = *(long *)(*param_4 + 8);
  }
  free(__s);
  uVar6 = 0;
LAB_00110965:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar6;
}