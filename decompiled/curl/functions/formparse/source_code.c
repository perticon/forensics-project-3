int formparse(struct OperationConfig *config,
              const char *input,
              struct tool_mime **mimeroot,
              struct tool_mime **mimecurrent,
              bool literal_value)
{
  /* input MUST be a string in the format 'name=contents' and we'll
     build a linked list with the info */
  char *name = NULL;
  char *contents = NULL;
  char *contp;
  char *data;
  char *type = NULL;
  char *filename = NULL;
  char *encoder = NULL;
  struct curl_slist *headers = NULL;
  struct tool_mime *part = NULL;
  CURLcode res;

  /* Allocate the main mime structure if needed. */
  if(!*mimecurrent) {
    NULL_CHECK(*mimeroot, tool_mime_new_parts(NULL), 1);
    *mimecurrent = *mimeroot;
  }

  /* Make a copy we can overwrite. */
  NULL_CHECK(contents, strdup(input), 2);

  /* Scan for the end of the name. */
  contp = strchr(contents, '=');
  if(contp) {
    int sep = '\0';
    if(contp > contents)
      name = contents;
    *contp++ = '\0';

    if(*contp == '(' && !literal_value) {
      /* Starting a multipart. */
      sep = get_param_part(config, '\0',
                           &contp, &data, &type, NULL, NULL, &headers);
      if(sep < 0) {
        Curl_safefree(contents);
        return 3;
      }
      NULL_CHECK(part, tool_mime_new_parts(*mimecurrent), 4);
      *mimecurrent = part;
      part->headers = headers;
      headers = NULL;
      SET_TOOL_MIME_PTR(part, type, 5);
    }
    else if(!name && !strcmp(contp, ")") && !literal_value) {
      /* Ending a multipart. */
      if(*mimecurrent == *mimeroot) {
        warnf(config->global, "no multipart to terminate!\n");
        Curl_safefree(contents);
        return 6;
        }
      *mimecurrent = (*mimecurrent)->parent;
    }
    else if('@' == contp[0] && !literal_value) {

      /* we use the @-letter to indicate file name(s) */

      struct tool_mime *subparts = NULL;

      do {
        /* since this was a file, it may have a content-type specifier
           at the end too, or a filename. Or both. */
        ++contp;
        sep = get_param_part(config, ',', &contp,
                             &data, &type, &filename, &encoder, &headers);
        if(sep < 0) {
          Curl_safefree(contents);
          return 7;
        }

        /* now contp point to comma or string end.
           If more files to come, make sure we have multiparts. */
        if(!subparts) {
          if(sep != ',')    /* If there is a single file. */
            subparts = *mimecurrent;
          else
            NULL_CHECK(subparts, tool_mime_new_parts(*mimecurrent), 8);
        }

        /* Store that file in a part. */
        NULL_CHECK(part,
                   tool_mime_new_filedata(subparts, data, TRUE, &res), 9);
        part->headers = headers;
        headers = NULL;
        part->config = config->global;
        if(res == CURLE_READ_ERROR) {
            /* An error occurred while reading stdin: if read has started,
               issue the error now. Else, delay it until processed by
               libcurl. */
          if(part->size > 0) {
            warnf(config->global,
                  "error while reading standard input\n");
            Curl_safefree(contents);
            return 10;
          }
          CONST_SAFEFREE(part->data);
          part->data = NULL;
          part->size = -1;
          res = CURLE_OK;
        }
        SET_TOOL_MIME_PTR(part, filename, 11);
        SET_TOOL_MIME_PTR(part, type, 12);
        SET_TOOL_MIME_PTR(part, encoder, 13);

        /* *contp could be '\0', so we just check with the delimiter */
      } while(sep); /* loop if there's another file name */
      part = (*mimecurrent)->subparts;  /* Set name on group. */
    }
    else {
      if(*contp == '<' && !literal_value) {
        ++contp;
        sep = get_param_part(config, '\0', &contp,
                             &data, &type, NULL, &encoder, &headers);
        if(sep < 0) {
          Curl_safefree(contents);
          return 14;
        }

        NULL_CHECK(part, tool_mime_new_filedata(*mimecurrent, data, FALSE,
                                                &res), 15);
        part->headers = headers;
        headers = NULL;
        part->config = config->global;
        if(res == CURLE_READ_ERROR) {
            /* An error occurred while reading stdin: if read has started,
               issue the error now. Else, delay it until processed by
               libcurl. */
          if(part->size > 0) {
            warnf(config->global,
                  "error while reading standard input\n");
            Curl_safefree(contents);
            return 16;
          }
          CONST_SAFEFREE(part->data);
          part->data = NULL;
          part->size = -1;
          res = CURLE_OK;
        }
      }
      else {
        if(literal_value)
          data = contp;
        else {
          sep = get_param_part(config, '\0', &contp,
                               &data, &type, &filename, &encoder, &headers);
          if(sep < 0) {
            Curl_safefree(contents);
            return 17;
          }
        }

        NULL_CHECK(part, tool_mime_new_data(*mimecurrent, data), 18);
        part->headers = headers;
        headers = NULL;
      }

      SET_TOOL_MIME_PTR(part, filename, 19);
      SET_TOOL_MIME_PTR(part, type, 20);
      SET_TOOL_MIME_PTR(part, encoder, 21);

      if(sep) {
        *contp = (char) sep;
        warnf(config->global,
              "garbage at end of field specification: %s\n", contp);
      }
    }

    /* Set part name. */
    SET_TOOL_MIME_PTR(part, name, 22);
  }
  else {
    warnf(config->global, "Illegally formatted input field!\n");
    Curl_safefree(contents);
    return 23;
  }
  Curl_safefree(contents);
  return 0;
}