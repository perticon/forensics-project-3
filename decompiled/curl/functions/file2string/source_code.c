ParameterError file2string(char **bufp, FILE *file)
{
  struct curlx_dynbuf dyn;
  curlx_dyn_init(&dyn, MAX_FILE2STRING);
  if(file) {
    char buffer[256];

    while(fgets(buffer, sizeof(buffer), file)) {
      char *ptr = strchr(buffer, '\r');
      if(ptr)
        *ptr = '\0';
      ptr = strchr(buffer, '\n');
      if(ptr)
        *ptr = '\0';
      if(curlx_dyn_add(&dyn, buffer))
        return PARAM_NO_MEM;
    }
  }
  *bufp = curlx_dyn_ptr(&dyn);
  return PARAM_OK;
}