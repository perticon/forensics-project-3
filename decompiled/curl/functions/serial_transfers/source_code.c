static CURLcode serial_transfers(struct GlobalConfig *global,
                                 CURLSH *share)
{
  CURLcode returncode = CURLE_OK;
  CURLcode result = CURLE_OK;
  struct per_transfer *per;
  bool added = FALSE;

  result = create_transfer(global, share, &added);
  if(result)
    return result;
  if(!added) {
    errorf(global, "no transfer performed\n");
    return CURLE_READ_ERROR;
  }
  for(per = transfers; per;) {
    bool retry;
    long delay_ms;
    bool bailout = FALSE;
    struct timeval start;
    result = pre_transfer(global, per);
    if(result)
      break;

#ifndef CURL_DISABLE_LIBCURL_OPTION
    if(global->libcurl) {
      result = easysrc_perform();
      if(result)
        break;
    }
#endif
    start = tvnow();
#ifdef CURLDEBUG
    if(global->test_event_based)
      result = curl_easy_perform_ev(per->curl);
    else
#endif
      result = curl_easy_perform(per->curl);

    returncode = post_per_transfer(global, per, result, &retry, &delay_ms);
    if(retry) {
      tool_go_sleep(delay_ms);
      continue;
    }

    /* Bail out upon critical errors or --fail-early */
    if(is_fatal_error(returncode) || (returncode && global->fail_early))
      bailout = TRUE;
    else {
      /* setup the next one just before we delete this */
      result = create_transfer(global, share, &added);
      if(result)
        bailout = TRUE;
    }

    per = del_per_transfer(per);

    if(bailout)
      break;

    if(per && global->ms_per_transfer) {
      /* how long time did the most recent transfer take in number of
         milliseconds */
      long milli = tvdiff(tvnow(), start);
      if(milli < global->ms_per_transfer) {
        notef(global, "Transfer took %ld ms, waits %ldms as set by --rate\n",
              milli, global->ms_per_transfer - milli);
        /* The transfer took less time than wanted. Wait a little. */
        tool_go_sleep(global->ms_per_transfer - milli);
      }
    }
  }
  if(returncode)
    /* returncode errors have priority */
    result = returncode;

  if(result)
    single_transfer_cleanup(global->current);

  return result;
}