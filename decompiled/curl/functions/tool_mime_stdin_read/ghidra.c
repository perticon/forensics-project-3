size_t tool_mime_stdin_read(void *param_1,undefined8 param_2,size_t param_3,long param_4)

{
  int iVar1;
  long lVar2;
  long lVar3;
  int *piVar4;
  char *pcVar5;
  
  lVar2 = *(long *)(param_4 + 0x58);
  if (-1 < lVar2) {
    if (lVar2 <= *(long *)(param_4 + 0x60)) {
      return 0;
    }
    lVar2 = lVar2 - *(long *)(param_4 + 0x60);
    lVar3 = curlx_uztoso(param_3);
    if (lVar2 < lVar3) {
      param_3 = curlx_sotouz(lVar2);
    }
  }
  if (param_3 == 0) {
    return 0;
  }
  lVar2 = *(long *)(param_4 + 0x18);
  if (lVar2 == 0) {
    param_3 = fread(param_1,1,param_3,stdin);
    iVar1 = ferror(stdin);
    if (iVar1 != 0) {
      if (*(long *)(param_4 + 0x68) != 0) {
        piVar4 = __errno_location();
        pcVar5 = strerror(*piVar4);
        warnf(*(undefined8 *)(param_4 + 0x68),"stdin: %s\n",pcVar5);
        *(undefined8 *)(param_4 + 0x68) = 0;
        return 0x10000000;
      }
      return 0x10000000;
    }
  }
  else {
    lVar3 = curlx_sotouz(*(undefined8 *)(param_4 + 0x60));
    memcpy(param_1,(void *)(lVar2 + lVar3),param_3);
  }
  lVar2 = curlx_uztoso(param_3);
  *(long *)(param_4 + 0x60) = *(long *)(param_4 + 0x60) + lVar2;
  return param_3;
}