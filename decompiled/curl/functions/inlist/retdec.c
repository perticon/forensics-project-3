int64_t inlist(int64_t a1, char * a2) {
    int64_t v1 = function_b9d0(); // 0x1c51d
    if (a1 == 0) {
        // 0x1c559
        return 0;
    }
    if ((int32_t)function_ba20() != 0) {
        // 0x1c542
        if ((*(char *)(*(int64_t *)a1 + v1) & -2) == 58) {
            // break -> 0x1c559
            break;
        }
    }
    int64_t v2 = *(int64_t *)(a1 + 8); // 0x1c550
    int64_t result = 0; // 0x1c557
    while (v2 != 0) {
        int64_t v3 = v2;
        if ((int32_t)function_ba20() != 0) {
            // 0x1c542
            result = 1;
            if ((*(char *)(*(int64_t *)v3 + v1) & -2) == 58) {
                // break -> 0x1c559
                break;
            }
        }
        // 0x1c550
        v2 = *(int64_t *)(v3 + 8);
        result = 0;
    }
    // 0x1c559
    return result;
}