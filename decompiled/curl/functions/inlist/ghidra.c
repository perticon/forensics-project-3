undefined8 inlist(long *param_1,char *param_2)

{
  int iVar1;
  size_t sVar2;
  
  sVar2 = strlen(param_2);
  if (param_1 != (long *)0x0) {
    do {
      iVar1 = curl_strnequal(*param_1,param_2,sVar2);
      if ((iVar1 != 0) && ((byte)(*(char *)(*param_1 + sVar2) - 0x3aU) < 2)) {
        return 1;
      }
      param_1 = (long *)param_1[1];
    } while (param_1 != (long *)0x0);
  }
  return 0;
}