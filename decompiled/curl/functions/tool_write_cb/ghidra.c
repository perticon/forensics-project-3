ulong tool_write_cb(void *param_1,size_t param_2,size_t param_3,long param_4)

{
  char cVar1;
  long lVar2;
  ulong uVar3;
  char cVar4;
  int iVar5;
  size_t sVar6;
  void *pvVar7;
  size_t __n;
  
  __n = param_2 * param_3;
  lVar2 = *(long *)(param_4 + 0x10);
  cVar1 = *(char *)(*(long *)(lVar2 + 0x4d8) + 6);
  uVar3 = (ulong)(__n == 0);
  sVar6 = uVar3;
  if ((*(long *)(param_4 + 200) != 0) ||
     (cVar4 = tool_create_output_file(param_4 + 0xb8), cVar4 != '\0')) {
    if ((cVar1 == '\0') ||
       (((1999 < *(long *)(param_4 + 0xd0) || (*(char *)(lVar2 + 0x310) != '\0')) ||
        (pvVar7 = memchr(param_1,0,__n), pvVar7 == (void *)0x0)))) {
      sVar6 = fwrite(param_1,param_2,param_3,*(FILE **)(param_4 + 200));
      if (__n - sVar6 == 0) {
        *(long *)(param_4 + 0xd0) = *(long *)(param_4 + 0xd0) + sVar6;
      }
      if (*(char *)(lVar2 + 0x30a) != '\0') {
        *(undefined *)(lVar2 + 0x30a) = 0;
        curl_easy_pause(*(undefined8 *)(param_4 + 0x18),0);
      }
      if ((*(char *)(lVar2 + 0x309) != '\0') &&
         (iVar5 = fflush(*(FILE **)(param_4 + 200)), iVar5 != 0)) {
        sVar6 = uVar3;
      }
    }
    else {
      warnf(*(undefined8 *)(lVar2 + 0x4d8),
            "Binary output can mess up your terminal. Use \"--output -\" to tell curl to output it to your terminal anyway, or consider \"--output <FILE>\" to save to a file.\n"
           );
      *(undefined4 *)(lVar2 + 0x4ac) = 1;
    }
  }
  return sVar6;
}