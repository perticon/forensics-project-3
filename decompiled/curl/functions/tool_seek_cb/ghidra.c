char tool_seek_cb(int *param_1,__off_t param_2,int param_3)

{
  __off_t _Var1;
  
  _Var1 = lseek(*param_1,param_2,param_3);
  return (_Var1 == -1) * '\x02';
}