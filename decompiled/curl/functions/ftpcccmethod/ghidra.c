undefined8 ftpcccmethod(long param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  
  iVar1 = curl_strequal("passive");
  uVar2 = 1;
  if (iVar1 == 0) {
    iVar1 = curl_strequal("active",param_2);
    uVar2 = 2;
    if (iVar1 == 0) {
      warnf(*(undefined8 *)(param_1 + 0x4d8),"unrecognized ftp CCC method \'%s\', using default\n",
            param_2);
      return 1;
    }
  }
  return uVar2;
}