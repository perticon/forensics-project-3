void ourWriteOut(char *param_1,long param_2,int param_3)

{
  long lVar1;
  char cVar2;
  char cVar3;
  int iVar4;
  int iVar5;
  char *pcVar6;
  char *pcVar7;
  FILE *__stream;
  char *pcVar8;
  char **ppcVar9;
  long in_FS_OFFSET;
  bool bVar10;
  long local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((param_1 != (char *)0x0) && (cVar2 = *param_1, __stream = stdout, cVar2 != '\0')) {
    do {
LAB_00120f0d:
      if (cVar2 != '%') {
        if ((cVar2 != '\\') || (cVar3 = param_1[1], cVar3 == '\0')) goto LAB_00120fb0;
        if (cVar3 == 'r') {
          fputc(0xd,__stream);
        }
        else if (cVar3 == 't') {
          fputc(9,__stream);
        }
        else if (cVar3 == 'n') {
          fputc(10,__stream);
        }
        else {
          fputc(0x5c,__stream);
          fputc((int)param_1[1],__stream);
        }
LAB_00120ef8:
        cVar2 = param_1[2];
        param_1 = param_1 + 2;
        goto joined_r0x00120fc4;
      }
      cVar3 = param_1[1];
      if (cVar3 == '\0') {
LAB_00120fb0:
        fputc((int)cVar2,__stream);
        cVar2 = param_1[1];
        pcVar6 = param_1;
joined_r0x00120fc4:
        param_1 = pcVar6 + 1;
joined_r0x00120fc4:
        if (cVar2 == '\0') break;
        goto LAB_00120f0d;
      }
      iVar4 = 0x25;
      if (cVar3 == '%') {
LAB_00120fe5:
        fputc(iVar4,__stream);
        goto LAB_00120ef8;
      }
      if (cVar3 != '{') {
        iVar4 = strncmp("header{",param_1 + 1,7);
        if (iVar4 != 0) {
          fputc(0x25,__stream);
          iVar4 = (int)param_1[1];
          goto LAB_00120fe5;
        }
        pcVar7 = param_1 + 8;
        pcVar6 = strchr(pcVar7,0x7d);
        if (pcVar6 == (char *)0x0) {
          fwrite("%header{",1,8,__stream);
          cVar2 = param_1[8];
          param_1 = pcVar7;
          goto joined_r0x00120fc4;
        }
        *pcVar6 = '\0';
        iVar4 = curl_easy_header(*(undefined8 *)(param_2 + 0x18),pcVar7,0,1,0xffffffff,&local_48);
        if (iVar4 == 0) {
          fputs(*(char **)(local_48 + 8),__stream);
        }
        cVar2 = pcVar6[1];
        goto joined_r0x00120fc4;
      }
      pcVar6 = param_1 + 2;
      pcVar7 = strchr(param_1,0x7d);
      if (pcVar7 == (char *)0x0) {
        fwrite(&DAT_0013a4f6,1,2,__stream);
        cVar2 = param_1[2];
        param_1 = pcVar6;
        goto joined_r0x00120fc4;
      }
      cVar3 = *pcVar7;
      ppcVar9 = (char **)(variables + 0x18);
      iVar4 = 0;
      *pcVar7 = '\0';
      pcVar8 = "content_type";
      do {
        iVar5 = curl_strequal(pcVar6,pcVar8);
        if (iVar5 != 0) {
          switch(*(undefined4 *)(variables + (long)iVar4 * 0x18 + 8)) {
          case 10:
            headerJSON(__stream,param_2);
            bVar10 = false;
            break;
          default:
            lVar1 = (long)iVar4 * 0x18;
            (**(code **)(variables + lVar1 + 0x10))(__stream,variables + lVar1,param_2,param_3,0);
            bVar10 = false;
            break;
          case 0x10:
            ourWriteOutJSON(__stream,variables,param_2);
            bVar10 = false;
            break;
          case 0x16:
            bVar10 = param_3 == 0;
            break;
          case 0x27:
            bVar10 = false;
            __stream = stderr;
            break;
          case 0x28:
            bVar10 = false;
            __stream = stdout;
          }
          goto LAB_001210e2;
        }
        pcVar8 = *ppcVar9;
        ppcVar9 = ppcVar9 + 3;
        iVar4 = iVar4 + 1;
      } while (pcVar8 != (char *)0x0);
      curl_mfprintf(stderr,"curl: unknown --write-out variable: \'%s\'\n",pcVar6);
      bVar10 = false;
LAB_001210e2:
      cVar2 = pcVar7[1];
      param_1 = pcVar7 + 1;
      *pcVar7 = cVar3;
    } while ((cVar2 != '\0') && (!bVar10));
  }
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}