undefined8 tool_mime_stdin_seek(long param_1,long param_2,int param_3)

{
  int iVar1;
  
  if (param_3 == 1) {
    param_2 = param_2 + *(long *)(param_1 + 0x60);
  }
  else if (param_3 == 2) {
    param_2 = param_2 + *(long *)(param_1 + 0x58);
  }
  if ((-1 < param_2) &&
     ((*(long *)(param_1 + 0x18) != 0 ||
      (iVar1 = fseek(stdin,*(long *)(param_1 + 0x50) + param_2,0), iVar1 == 0)))) {
    *(long *)(param_1 + 0x60) = param_2;
    return 0;
  }
  return 2;
}