long scheme2protocol(const char *scheme)
{
  struct proto_name_pattern const *p;
  for(p = possibly_built_in; p->proto_name; p++) {
    if(curl_strequal(scheme, p->proto_name))
      return p->proto_pattern;
  }
  return 0;
}