uint32_t get_param_part(void** rdi, int32_t esi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8) {
    int32_t r12d9;
    void** rbx10;
    void* rsp11;
    void** v12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    void** v17;
    unsigned char v18;
    void* rax19;
    void* v20;
    void** v21;
    int32_t eax22;
    void*** rax23;
    void** rcx24;
    void*** rsi25;
    int32_t v26;
    void*** v27;
    void*** v28;
    void** rax29;
    void* rsp30;
    void** rdx31;
    void** v32;
    int32_t eax33;
    void** v34;
    uint32_t r13d35;
    void** v36;
    void** v37;
    uint32_t eax38;
    void* rdx39;
    void*** rsp40;
    void** rax41;
    void** rdi42;
    int32_t eax43;
    void** r14_44;
    int64_t rax45;
    void* rsp46;
    void** rax47;
    int32_t eax48;
    void** rbp49;
    void** r12_50;
    void** v51;
    int32_t eax52;
    void* rsp53;
    void** rax54;
    void** rax55;
    uint32_t ebx56;
    int32_t eax57;
    void** rax58;
    void** rdx59;
    int64_t rax60;
    void* rsp61;
    int64_t rax62;
    int64_t rax63;
    void* rsp64;
    int64_t rax65;
    int64_t rax66;
    void** rdx67;
    int64_t rax68;
    int32_t eax69;
    void** rax70;
    void* rsp71;
    void** v72;
    void** rax73;
    int32_t eax74;
    void** rax75;
    void* rsp76;
    void** rbp77;
    void** rax78;
    int64_t rax79;
    void** rax80;
    void** rdi81;
    void** rbx82;
    int32_t eax83;
    void** rdx84;
    void** rsi85;
    void** v86;
    void** rax87;
    int32_t v88;
    void* r12_89;
    signed char v90;
    int32_t eax91;
    int32_t ebx92;
    int32_t eax93;
    int32_t eax94;
    void** r15_95;
    void** rax96;
    void** rdi97;
    int32_t eax98;
    void* rsp99;
    void** rdi100;
    int32_t eax101;
    void** rax102;
    void** rax103;
    int32_t eax104;
    void** rax105;
    void** rax106;
    void** rbx107;
    int32_t eax108;
    void** rax109;
    int32_t eax110;
    void** rax111;
    void** v112;
    void** rbx113;
    int32_t eax114;
    uint32_t ebx115;
    int32_t eax116;
    void** rax117;
    void** rdx118;
    void** rdi119;
    void** rsi120;
    void** rdi121;
    void* rsp122;
    void** rdi123;
    void** rdi124;
    void** rdi125;
    void** rdi126;

    __asm__("pxor xmm0, xmm0");
    r12d9 = esi;
    rbx10 = rcx;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x588);
    v12 = rdi;
    v13 = rdx;
    v14 = a7;
    v15 = r8;
    v16 = r9;
    v17 = a8;
    v18 = *reinterpret_cast<unsigned char*>(&esi);
    rax19 = g28;
    v20 = rax19;
    __asm__("movaps [rsp+0x90], xmm0");
    __asm__("movaps [rsp+0xa0], xmm0");
    __asm__("movaps [rsp+0xb0], xmm0");
    __asm__("movaps [rsp+0xc0], xmm0");
    __asm__("movaps [rsp+0xd0], xmm0");
    __asm__("movaps [rsp+0xe0], xmm0");
    v21 = *reinterpret_cast<void***>(rdx);
    __asm__("movaps [rsp+0xf0], xmm0");
    __asm__("movaps [rsp+0x100], xmm0");
    __asm__("movaps [rsp+0x110], xmm0");
    __asm__("movaps [rsp+0x120], xmm0");
    __asm__("movaps [rsp+0x130], xmm0");
    __asm__("movaps [rsp+0x140], xmm0");
    __asm__("movaps [rsp+0x150], xmm0");
    *reinterpret_cast<void***>(r8) = reinterpret_cast<void**>(0);
    __asm__("movaps [rsp+0x160], xmm0");
    __asm__("movaps [rsp+0x170], xmm0");
    __asm__("movaps [rsp+0x180], xmm0");
    if (r9) {
        *reinterpret_cast<void***>(r9) = reinterpret_cast<void**>(0);
    }
    *reinterpret_cast<void***>(v17) = reinterpret_cast<void**>(0);
    if (v14) {
        *reinterpret_cast<void***>(v14) = reinterpret_cast<void**>(0);
    }
    while (eax22 = Curl_isspace(), rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8), !!eax22) {
        ++v21;
    }
    rax23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp11) + 0x88);
    *reinterpret_cast<int32_t*>(&rcx24) = *reinterpret_cast<signed char*>(&r12d9);
    *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
    rsi25 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    v26 = *reinterpret_cast<int32_t*>(&rcx24);
    v27 = rax23;
    v28 = rsi25;
    rax29 = get_param_word(v12, rsi25, rax23, *reinterpret_cast<int32_t*>(&rcx24));
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
    rdx31 = v32;
    *reinterpret_cast<void***>(rbx10) = rax29;
    if (rax29 == v21) {
        if (reinterpret_cast<unsigned char>(rax29) < reinterpret_cast<unsigned char>(rdx31)) {
            do {
                eax33 = Curl_isspace();
                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                if (!eax33) 
                    break;
                rdx31 = v34 + 0xffffffffffffffff;
                v34 = rdx31;
            } while (reinterpret_cast<unsigned char>(rdx31) > reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10)));
            goto addr_f71b_11;
        } else {
            goto addr_f71b_11;
        }
    } else {
        addr_f71b_11:
        r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v21));
        *reinterpret_cast<void***>(rdx31) = reinterpret_cast<void**>(0);
        if (*reinterpret_cast<unsigned char*>(&r13d35) != 59) {
            *reinterpret_cast<void***>(v15) = reinterpret_cast<void**>(0);
            if (v16) {
                *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(0);
            }
            v36 = reinterpret_cast<void**>(0);
            v37 = reinterpret_cast<void**>(0);
            if (v14) {
                while (1) {
                    *reinterpret_cast<void***>(v14) = v37;
                    while (1) {
                        addr_f8e2_17:
                        *reinterpret_cast<void***>(v17) = v36;
                        *reinterpret_cast<void***>(v13) = v21;
                        eax38 = *reinterpret_cast<unsigned char*>(&r13d35);
                        while (rdx39 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v20) - reinterpret_cast<uint64_t>(g28)), !!rdx39) {
                            fun_bd40();
                            rsp40 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            while (1) {
                                addr_10170_20:
                                fun_b880();
                                rax41 = fun_b960();
                                rcx24 = rax41;
                                rdi42 = *reinterpret_cast<void***>(v12 + 0x4d8);
                                warnf(rdi42, "Cannot read from %s: %s\n", rdi42, "Cannot read from %s: %s\n");
                                rsp30 = reinterpret_cast<void*>(rsp40 - 8 + 8 - 8 + 8 - 8 + 8);
                                while (*reinterpret_cast<unsigned char*>(&r13d35) == 59) {
                                    do {
                                        addr_f768_22:
                                        ++v21;
                                        eax43 = Curl_isspace();
                                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                    } while (eax43);
                                    if (!r14_44) {
                                        rax45 = fun_ba20(v21, "type=", 5, rcx24);
                                        rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                        if (*reinterpret_cast<int32_t*>(&rax45)) {
                                            rax47 = v21 + 5;
                                            while (v21 = rax47, eax48 = Curl_isspace(), rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8), !!eax48) {
                                                rax47 = v21 + 1;
                                            }
                                            rbp49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp46) + 0x110);
                                            r12_50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp46) + 0x90);
                                            v51 = v21;
                                            rcx24 = rbp49;
                                            eax52 = fun_bc50(v21, "%127[^/ ]/%127[^;, \n]", r12_50, rcx24, r8);
                                            rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8);
                                            if (eax52 != 2) 
                                                goto addr_10061_29;
                                            rax54 = fun_b9d0(r12_50, "%127[^/ ]/%127[^;, \n]", r12_50, r12_50, "%127[^/ ]/%127[^;, \n]", r12_50);
                                            rax55 = fun_b9d0(rbp49, "%127[^/ ]/%127[^;, \n]", r12_50, rbp49, "%127[^/ ]/%127[^;, \n]", r12_50);
                                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8 - 8 + 8);
                                            r14_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax54) + reinterpret_cast<unsigned char>(rax55) + 1 + reinterpret_cast<unsigned char>(v51));
                                            v21 = r14_44;
                                            r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_44));
                                            if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<unsigned char*>(&r13d35))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&r13d35) != 59)))) 
                                                continue;
                                            ebx56 = v18;
                                            do {
                                                if (*reinterpret_cast<unsigned char*>(&ebx56) == *reinterpret_cast<unsigned char*>(&r13d35)) 
                                                    break;
                                                eax57 = Curl_isspace();
                                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                                rax58 = v21;
                                                rdx59 = rax58 + 1;
                                                if (!eax57) {
                                                    r14_44 = rax58 + 1;
                                                    rdx59 = r14_44;
                                                }
                                                v21 = rdx59;
                                                r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax58 + 1));
                                            } while (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<unsigned char*>(&r13d35))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&r13d35) != 59)));
                                            continue;
                                            continue;
                                        }
                                        rax60 = fun_ba20(v21, "filename=", 9, rcx24);
                                        rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8);
                                        if (*reinterpret_cast<int32_t*>(&rax60)) 
                                            goto addr_fb0a_38;
                                    } else {
                                        rax62 = fun_ba20(v21, "filename=", 9, rcx24);
                                        rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                        if (*reinterpret_cast<int32_t*>(&rax62)) {
                                            *reinterpret_cast<void***>(r14_44) = reinterpret_cast<void**>(0);
                                            goto addr_fb0a_38;
                                        } else {
                                            rax63 = fun_ba20(v21, "headers=", 8, rcx24);
                                            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                                            if (*reinterpret_cast<int32_t*>(&rax63)) {
                                                *reinterpret_cast<void***>(r14_44) = reinterpret_cast<void**>(0);
                                                goto addr_f7d8_43;
                                            }
                                            rax65 = fun_ba20(v21, "encoder=", 8, rcx24);
                                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
                                            if (*reinterpret_cast<int32_t*>(&rax65)) 
                                                goto addr_fec8_45; else 
                                                goto addr_fbd9_46;
                                        }
                                    }
                                    rax66 = fun_ba20(v21, "headers=", 8, rcx24);
                                    rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                                    if (*reinterpret_cast<int32_t*>(&rax66)) {
                                        addr_f7d8_43:
                                        rdx67 = v21 + 8;
                                        *reinterpret_cast<uint32_t*>(&rax68) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v21 + 8));
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax68) + 4) = 0;
                                        v21 = rdx67;
                                        if (!(static_cast<uint32_t>(rax68 - 60) & 0xfb)) {
                                            while (v21 = rdx67 + 1, eax69 = Curl_isspace(), rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8), !!eax69) {
                                                rdx67 = v21;
                                            }
                                            *reinterpret_cast<int32_t*>(&rcx24) = v26;
                                            *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                            rax70 = get_param_word(v12, v28, v27, *reinterpret_cast<int32_t*>(&rcx24));
                                            rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
                                            v72 = rax70;
                                            rax73 = v34;
                                            if (v21 != rax70) 
                                                goto addr_f987_51;
                                        } else {
                                            while (eax74 = Curl_isspace(), rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8), !!eax74) {
                                                ++v21;
                                            }
                                            *reinterpret_cast<int32_t*>(&rcx24) = v26;
                                            *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                            rax75 = get_param_word(v12, v28, v27, *reinterpret_cast<int32_t*>(&rcx24));
                                            rsp76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
                                            rbp77 = rax75;
                                            rax78 = v34;
                                            if (v21 == rbp77) 
                                                goto addr_fa95_56; else 
                                                goto addr_f85d_57;
                                        }
                                    } else {
                                        rax79 = fun_ba20(v21, "encoder=", 8, rcx24);
                                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
                                        if (!*reinterpret_cast<int32_t*>(&rax79)) {
                                            *reinterpret_cast<int32_t*>(&rcx24) = v26;
                                            *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                            rax80 = get_param_word(v12, v28, v27, *reinterpret_cast<int32_t*>(&rcx24));
                                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                            r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v21));
                                            *reinterpret_cast<void***>(v34) = reinterpret_cast<void**>(0);
                                            if (!*reinterpret_cast<void***>(rax80)) 
                                                continue;
                                            rcx24 = v12;
                                            rdi81 = *reinterpret_cast<void***>(rcx24 + 0x4d8);
                                            warnf(rdi81, "skip unknown form field: %s\n", rdi81, "skip unknown form field: %s\n");
                                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                            continue;
                                        }
                                    }
                                    rbx82 = v72;
                                    if (reinterpret_cast<unsigned char>(v72) < reinterpret_cast<unsigned char>(rax73)) {
                                        do {
                                            eax83 = Curl_isspace();
                                            rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
                                            rax73 = v34;
                                            if (!eax83) 
                                                break;
                                            --rax73;
                                            v34 = rax73;
                                        } while (reinterpret_cast<unsigned char>(rax73) > reinterpret_cast<unsigned char>(rbx82));
                                        goto addr_f987_51;
                                    } else {
                                        goto addr_f987_51;
                                    }
                                    addr_f987_51:
                                    rdx84 = v21;
                                    rsi85 = reinterpret_cast<void**>("r");
                                    r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx84));
                                    *reinterpret_cast<void***>(rax73) = reinterpret_cast<void**>(0);
                                    rax87 = fun_bdb0(v72, "r", rdx84, rcx24, r8, r9, v86);
                                    rsp40 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
                                    r14_44 = rax87;
                                    if (!rax87) 
                                        goto addr_10170_20;
                                    v88 = 1;
                                    *reinterpret_cast<int32_t*>(&r12_89) = 0;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_89) + 4) = 0;
                                    v90 = 0;
                                    v86 = reinterpret_cast<void**>(0);
                                    while (1) {
                                        eax91 = fun_b910(r14_44, rsi85, rdx84, rcx24);
                                        rsp40 = rsp40 - 8 + 8;
                                        ebx92 = eax91;
                                        if (eax91 == -1) {
                                            addr_fa3b_68:
                                            while (r12_89) {
                                                eax93 = Curl_isspace();
                                                rsp40 = rsp40 - 8 + 8;
                                                if (!eax93) 
                                                    goto addr_fe20_70;
                                                r12_89 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_89) - 1);
                                            }
                                            goto addr_fa40_72;
                                        } else {
                                            if (!v86) {
                                                eax94 = Curl_isspace();
                                                rsp40 = rsp40 - 8 + 8;
                                                if (eax94) {
                                                    addr_fa40_72:
                                                    if (ebx92 == 13) 
                                                        continue; else 
                                                        goto addr_fa45_75;
                                                } else {
                                                    goto addr_fa3b_68;
                                                }
                                            } else {
                                                if (eax91 == 10) {
                                                    addr_fa55_78:
                                                    ++v88;
                                                    v90 = 0;
                                                    v86 = reinterpret_cast<void**>(0);
                                                    continue;
                                                } else {
                                                    if (eax91 == 13) 
                                                        continue; else 
                                                        goto addr_f9f4_80;
                                                }
                                            }
                                        }
                                        addr_fe20_70:
                                        rsi85 = r15_95;
                                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp40) + reinterpret_cast<uint64_t>(r12_89) + 0x190) = 0;
                                        rax96 = fun_b9a0();
                                        rsp40 = rsp40 - 8 + 8;
                                        if (!rax96) 
                                            goto addr_10140_81;
                                        v36 = rax96;
                                        *reinterpret_cast<int32_t*>(&r12_89) = 0;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_89) + 4) = 0;
                                        goto addr_fa40_72;
                                        addr_fa45_75:
                                        if (ebx92 > 13) {
                                            if (ebx92 == 35 && !v86) {
                                                v90 = 1;
                                                v86 = reinterpret_cast<void**>(1);
                                                continue;
                                            }
                                        } else {
                                            if (ebx92 == -1) 
                                                break;
                                            if (ebx92 == 10) 
                                                goto addr_fa55_78;
                                        }
                                        addr_f9f4_80:
                                        ++v86;
                                        if (v90) 
                                            continue;
                                        if (r12_89 == 0x3e6) {
                                            *reinterpret_cast<int32_t*>(&rcx24) = v88;
                                            *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                            rsi85 = reinterpret_cast<void**>("File %s line %d: header too long (truncated)\n");
                                            rdx84 = v72;
                                            rdi97 = *reinterpret_cast<void***>(v12 + 0x4d8);
                                            warnf(rdi97, "File %s line %d: header too long (truncated)\n", rdi97, "File %s line %d: header too long (truncated)\n");
                                            rsp40 = rsp40 - 8 + 8;
                                            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp40) + reinterpret_cast<uint64_t>(r12_89) + 0x190) = 32;
                                            r12_89 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_89) + 1);
                                        } else {
                                            if (reinterpret_cast<uint64_t>(r12_89) > 0x3e6) 
                                                continue;
                                            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp40) + reinterpret_cast<uint64_t>(r12_89) + 0x190) = *reinterpret_cast<signed char*>(&ebx92);
                                            r12_89 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_89) + 1);
                                        }
                                    }
                                    eax98 = fun_c030(r14_44, rsi85, rdx84, rcx24);
                                    rsp99 = reinterpret_cast<void*>(rsp40 - 8 + 8);
                                    if (eax98) 
                                        goto addr_100c1_92;
                                    rdi100 = r14_44;
                                    *reinterpret_cast<int32_t*>(&r14_44) = 0;
                                    *reinterpret_cast<int32_t*>(&r14_44 + 4) = 0;
                                    fun_bc70(rdi100, rsi85, rdx84, rdi100, rsi85, rdx84);
                                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8);
                                    continue;
                                    addr_fa95_56:
                                    while (reinterpret_cast<unsigned char>(rax78) > reinterpret_cast<unsigned char>(rbp77)) {
                                        eax101 = Curl_isspace();
                                        rsp76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp76) - 8 + 8);
                                        rax78 = v34;
                                        if (!eax101) 
                                            goto addr_f85d_57;
                                        --rax78;
                                        v34 = rax78;
                                    }
                                    addr_f85d_57:
                                    r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v21));
                                    *reinterpret_cast<void***>(rax78) = reinterpret_cast<void**>(0);
                                    rax102 = fun_b9a0();
                                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp76) - 8 + 8);
                                    if (!rax102) 
                                        goto addr_1008f_97;
                                    v36 = rax102;
                                    *reinterpret_cast<int32_t*>(&r14_44) = 0;
                                    *reinterpret_cast<int32_t*>(&r14_44 + 4) = 0;
                                    continue;
                                    addr_fecc_99:
                                    rax103 = v21 + 8;
                                    while (v21 = rax103, eax104 = Curl_isspace(), rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8), !!eax104) {
                                        rax103 = v21 + 1;
                                    }
                                    *reinterpret_cast<int32_t*>(&rcx24) = v26;
                                    *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                    rax105 = get_param_word(v12, v28, v27, *reinterpret_cast<int32_t*>(&rcx24));
                                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                    v37 = rax105;
                                    rax106 = v34;
                                    if (v21 != rax105) {
                                        addr_fb76_103:
                                        *reinterpret_cast<int32_t*>(&r14_44) = 0;
                                        *reinterpret_cast<int32_t*>(&r14_44 + 4) = 0;
                                        r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v21));
                                        *reinterpret_cast<void***>(rax106) = reinterpret_cast<void**>(0);
                                        continue;
                                    } else {
                                        rbx107 = v37;
                                        if (reinterpret_cast<unsigned char>(rax106) > reinterpret_cast<unsigned char>(v37)) {
                                            do {
                                                eax108 = Curl_isspace();
                                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                                rax106 = v34;
                                                if (!eax108) 
                                                    break;
                                                --rax106;
                                                v34 = rax106;
                                            } while (reinterpret_cast<unsigned char>(rax106) > reinterpret_cast<unsigned char>(rbx107));
                                            goto addr_fb76_103;
                                        } else {
                                            goto addr_fb76_103;
                                        }
                                    }
                                    goto addr_fb76_103;
                                    addr_fb0a_38:
                                    rax109 = v21 + 9;
                                    while (v21 = rax109, eax110 = Curl_isspace(), rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8), !!eax110) {
                                        rax109 = v21 + 1;
                                    }
                                    *reinterpret_cast<int32_t*>(&rcx24) = v26;
                                    *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                    rax111 = get_param_word(v12, v28, v27, *reinterpret_cast<int32_t*>(&rcx24));
                                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                                    v112 = rax111;
                                    rax106 = v34;
                                    if (v21 != rax111) 
                                        goto addr_fb76_103;
                                    rbx113 = v112;
                                    if (reinterpret_cast<unsigned char>(v112) < reinterpret_cast<unsigned char>(rax106)) {
                                        do {
                                            eax114 = Curl_isspace();
                                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                            rax106 = v34;
                                            if (!eax114) 
                                                break;
                                            --rax106;
                                            v34 = rax106;
                                        } while (reinterpret_cast<unsigned char>(rax106) > reinterpret_cast<unsigned char>(rbx113));
                                        goto addr_fb76_103;
                                        goto addr_fb76_103;
                                    } else {
                                        goto addr_fb76_103;
                                    }
                                    addr_fec8_45:
                                    *reinterpret_cast<void***>(r14_44) = reinterpret_cast<void**>(0);
                                    goto addr_fecc_99;
                                    addr_fbd9_46:
                                    r14_44 = v21;
                                    r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_44));
                                    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&r13d35) != 59)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<unsigned char*>(&r13d35))))) 
                                        continue;
                                    ebx115 = v18;
                                    if (v18 == *reinterpret_cast<unsigned char*>(&r13d35)) 
                                        goto addr_fc06_118;
                                    do {
                                        eax116 = Curl_isspace();
                                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                        rax117 = v21;
                                        rdx118 = rax117 + 1;
                                        if (!eax116) {
                                            r14_44 = rax117 + 1;
                                            rdx118 = r14_44;
                                        }
                                        v21 = rdx118;
                                        r13d35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax117 + 1));
                                        if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<unsigned char*>(&r13d35))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&r13d35) != 59)))) 
                                            break;
                                    } while (*reinterpret_cast<unsigned char*>(&ebx115) != *reinterpret_cast<unsigned char*>(&r13d35));
                                    continue;
                                    continue;
                                    addr_fc06_118:
                                }
                                goto addr_f89a_124;
                            }
                            addr_10061_29:
                            rdi119 = *reinterpret_cast<void***>(v12 + 0x4d8);
                            warnf(rdi119, "Illegally formatted content-type field!\n", rdi119, "Illegally formatted content-type field!\n");
                            fun_bdf0(v36, "Illegally formatted content-type field!\n", v36, "Illegally formatted content-type field!\n");
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8 - 8 + 8);
                            eax38 = 0xffffffff;
                            continue;
                            addr_10140_81:
                            rsi120 = reinterpret_cast<void**>("Out of memory for field headers!\n");
                            rdi121 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(v12 + 0x4d8) + 8);
                            fun_bd70(rdi121, "Out of memory for field headers!\n", rdi121, "Out of memory for field headers!\n");
                            fun_bc70(r14_44, "Out of memory for field headers!\n", rdx84, r14_44, "Out of memory for field headers!\n", rdx84);
                            rsp122 = reinterpret_cast<void*>(rsp40 - 8 + 8 - 8 + 8);
                            addr_100ad_125:
                            fun_bdf0(v36, rsi120, v36, rsi120);
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp122) - 8 + 8);
                            eax38 = 0xffffffff;
                            continue;
                            addr_100c1_92:
                            fun_b880();
                            fun_b960();
                            rsi120 = reinterpret_cast<void**>("Header file %s read error: %s\n");
                            rdi123 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(v12 + 0x4d8) + 8);
                            fun_bd70(rdi123, "Header file %s read error: %s\n", rdi123, "Header file %s read error: %s\n");
                            fun_bc70(r14_44, "Header file %s read error: %s\n", v72, r14_44, "Header file %s read error: %s\n", v72);
                            rsp122 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                            goto addr_100ad_125;
                            addr_1008f_97:
                            rsi120 = reinterpret_cast<void**>("Out of memory for field header!\n");
                            rdi124 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(v12 + 0x4d8) + 8);
                            fun_bd70(rdi124, "Out of memory for field header!\n", rdi124, "Out of memory for field header!\n");
                            rsp122 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            goto addr_100ad_125;
                        }
                        goto addr_f91a_126;
                        addr_f89a_124:
                        if (r14_44) {
                            *reinterpret_cast<void***>(r14_44) = reinterpret_cast<void**>(0);
                        }
                        *reinterpret_cast<void***>(v15) = v51;
                        if (!v16) {
                            if (v112) {
                                rdi125 = *reinterpret_cast<void***>(v12 + 0x4d8);
                                warnf(rdi125, "Field file name not allowed here: %s\n");
                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            }
                        } else {
                            *reinterpret_cast<void***>(v16) = v112;
                        }
                        if (v14) 
                            break;
                        if (!v37) 
                            continue;
                        rdi126 = *reinterpret_cast<void***>(v12 + 0x4d8);
                        warnf(rdi126, "Field encoder not allowed here: %s\n");
                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                    }
                }
                addr_f91a_126:
                return eax38;
            } else {
                goto addr_f8e2_17;
            }
        } else {
            v36 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&r14_44) = 0;
            *reinterpret_cast<int32_t*>(&r14_44 + 4) = 0;
            r15_95 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) + 0x190);
            v37 = reinterpret_cast<void**>(0);
            v112 = reinterpret_cast<void**>(0);
            v51 = reinterpret_cast<void**>(0);
            goto addr_f768_22;
        }
    }
    rdx31 = v34;
    goto addr_f71b_11;
}