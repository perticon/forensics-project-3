ulong get_param_part(long param_1,byte param_2,FILE **param_3,FILE **param_4,FILE **param_5,
                    FILE **param_6,FILE **param_7,long *param_8)

{
  char cVar1;
  bool bVar2;
  char **ppcVar3;
  ulong uVar4;
  int iVar5;
  uint uVar6;
  FILE *pFVar7;
  long lVar8;
  FILE *pFVar9;
  size_t sVar10;
  size_t sVar11;
  ulong uVar12;
  int *piVar13;
  char *pcVar14;
  byte bVar15;
  long in_FS_OFFSET;
  long local_5b8;
  long local_590;
  FILE *local_588;
  FILE *local_570;
  FILE *local_560;
  FILE *local_538;
  FILE *local_530;
  undefined local_528 [16];
  undefined local_518 [16];
  undefined local_508 [16];
  undefined local_4f8 [16];
  undefined local_4e8 [16];
  undefined local_4d8 [16];
  undefined local_4c8 [16];
  undefined local_4b8 [16];
  undefined local_4a8 [16];
  undefined local_498 [16];
  undefined local_488 [16];
  undefined local_478 [16];
  undefined local_468 [16];
  undefined local_458 [16];
  undefined local_448 [16];
  undefined local_438 [16];
  undefined local_428 [998];
  undefined uStack66;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_538 = *param_3;
  local_528 = (undefined  [16])0x0;
  local_518 = (undefined  [16])0x0;
  local_508 = (undefined  [16])0x0;
  local_4f8 = (undefined  [16])0x0;
  local_4e8 = (undefined  [16])0x0;
  local_4d8 = (undefined  [16])0x0;
  local_4c8 = (undefined  [16])0x0;
  local_4b8 = (undefined  [16])0x0;
  local_4a8 = (undefined  [16])0x0;
  local_498 = (undefined  [16])0x0;
  local_488 = (undefined  [16])0x0;
  local_478 = (undefined  [16])0x0;
  local_468 = (undefined  [16])0x0;
  *param_5 = (FILE *)0x0;
  local_458 = (undefined  [16])0x0;
  local_448 = (undefined  [16])0x0;
  local_438 = (undefined  [16])0x0;
  if (param_6 != (FILE **)0x0) {
    *param_6 = (FILE *)0x0;
  }
  *param_8 = 0;
  if (param_7 != (FILE **)0x0) {
    *param_7 = (FILE *)0x0;
  }
  while (iVar5 = Curl_isspace(*(char *)&local_538->_flags), pFVar9 = local_538, iVar5 != 0) {
    local_538 = (FILE *)((long)&local_538->_flags + 1);
  }
  pFVar7 = (FILE *)get_param_word(param_1,&local_538,&local_530);
  *param_4 = pFVar7;
  if ((pFVar7 == pFVar9) && (pFVar7 < local_530)) {
    do {
      iVar5 = Curl_isspace(local_530[-1]._unused2[0x13]);
      if (iVar5 == 0) break;
      local_530 = (FILE *)(local_530[-1]._unused2 + 0x13);
    } while (*param_4 <= local_530 && local_530 != *param_4);
  }
  bVar15 = *(byte *)&local_538->_flags;
  *(char *)&local_530->_flags = '\0';
  if (bVar15 == 0x3b) {
    local_590 = 0;
    pFVar9 = (FILE *)0x0;
    local_560 = (FILE *)0x0;
    local_588 = (FILE *)0x0;
    local_570 = (FILE *)0x0;
    do {
      do {
        ppcVar3 = (char **)&local_538->_flags;
        local_538 = (FILE *)((long)&local_538->_flags + 1);
        iVar5 = Curl_isspace(*(char *)((long)ppcVar3 + 1));
      } while (iVar5 != 0);
      if (pFVar9 == (FILE *)0x0) {
        iVar5 = curl_strnequal(local_538,"type=",5);
        if (iVar5 == 0) {
          iVar5 = curl_strnequal(local_538,"filename=",9);
          if (iVar5 != 0) goto LAB_0010fb0a;
          iVar5 = curl_strnequal(local_538,"headers=",8);
          if (iVar5 != 0) goto LAB_0010f7d8;
          iVar5 = curl_strnequal(local_538,"encoder=",8);
          if (iVar5 != 0) goto LAB_0010fecc;
          pcVar14 = (char *)get_param_word(param_1,&local_538,&local_530);
          bVar15 = *(byte *)&local_538->_flags;
          *(char *)&local_530->_flags = '\0';
          if (*pcVar14 != '\0') {
            warnf(*(undefined8 *)(param_1 + 0x4d8),"skip unknown form field: %s\n",pcVar14);
          }
        }
        else {
          local_538 = (FILE *)&local_538->field_0x5;
          while (iVar5 = Curl_isspace(*(char *)&local_538->_flags), pFVar9 = local_538, iVar5 != 0)
          {
            local_538 = (FILE *)((long)&local_538->_flags + 1);
          }
          local_570 = local_538;
          iVar5 = __isoc99_sscanf(local_538,"%127[^/ ]/%127[^;, \n]",local_528);
          if (iVar5 != 2) {
            warnf(*(undefined8 *)(param_1 + 0x4d8),"Illegally formatted content-type field!\n");
            curl_slist_free_all(local_590);
            uVar12 = 0xffffffff;
            goto LAB_0010f903;
          }
          sVar10 = strlen(local_528);
          sVar11 = strlen(local_4a8);
          pFVar9 = (FILE *)((long)&pFVar9->_flags + sVar10 + 1 + sVar11);
          bVar15 = *(byte *)&pFVar9->_flags;
          local_538 = pFVar9;
          if (bVar15 != 0 && bVar15 != 0x3b) {
            do {
              if (param_2 == bVar15) break;
              iVar5 = Curl_isspace(bVar15);
              if (iVar5 == 0) {
                pFVar9 = (FILE *)((long)&local_538->_flags + 1);
              }
              pFVar7 = (FILE *)((long)&local_538->_flags + 1);
              bVar15 = *(byte *)((long)&local_538->_flags + 1);
              local_538 = pFVar7;
            } while (bVar15 != 0 && bVar15 != 0x3b);
          }
        }
      }
      else {
        iVar5 = curl_strnequal(local_538,"filename=",9);
        if (iVar5 == 0) {
          iVar5 = curl_strnequal(local_538,"headers=",8);
          if (iVar5 == 0) {
            iVar5 = curl_strnequal(local_538,"encoder=",8);
            if (iVar5 != 0) {
              *(char *)&pFVar9->_flags = '\0';
LAB_0010fecc:
              local_538 = (FILE *)&local_538->_IO_read_ptr;
              while (iVar5 = Curl_isspace(*(char *)&local_538->_flags), pFVar9 = local_538,
                    iVar5 != 0) {
                local_538 = (FILE *)((long)&local_538->_flags + 1);
              }
              local_560 = (FILE *)get_param_word(param_1,&local_538,&local_530);
              if (pFVar9 == local_560) {
                while ((local_560 < local_530 &&
                       (iVar5 = Curl_isspace(local_530[-1]._unused2[0x13]), iVar5 != 0))) {
                  local_530 = (FILE *)(local_530[-1]._unused2 + 0x13);
                }
              }
              goto LAB_0010fb76;
            }
            bVar15 = *(byte *)&local_538->_flags;
            pFVar9 = local_538;
            if (bVar15 != 0x3b && bVar15 != 0) {
              do {
                if (param_2 == bVar15) break;
                iVar5 = Curl_isspace(bVar15);
                if (iVar5 == 0) {
                  pFVar9 = (FILE *)((long)&local_538->_flags + 1);
                }
                pFVar7 = (FILE *)((long)&local_538->_flags + 1);
                bVar15 = *(byte *)((long)&local_538->_flags + 1);
                local_538 = pFVar7;
              } while (bVar15 != 0 && bVar15 != 0x3b);
            }
          }
          else {
            *(char *)&pFVar9->_flags = '\0';
LAB_0010f7d8:
            ppcVar3 = &local_538->_IO_read_ptr;
            cVar1 = *(char *)&local_538->_IO_read_ptr;
            local_538 = (FILE *)ppcVar3;
            if ((cVar1 - 0x3cU & 0xfb) == 0) {
              do {
                ppcVar3 = (char **)&local_538->_flags;
                local_538 = (FILE *)((long)&local_538->_flags + 1);
                iVar5 = Curl_isspace(*(char *)((long)ppcVar3 + 1));
                pFVar9 = local_538;
              } while (iVar5 != 0);
              pFVar7 = (FILE *)get_param_word(param_1,&local_538,&local_530);
              if (pFVar9 == pFVar7) {
                while ((pFVar7 < local_530 &&
                       (iVar5 = Curl_isspace(local_530[-1]._unused2[0x13]), iVar5 != 0))) {
                  local_530 = (FILE *)(local_530[-1]._unused2 + 0x13);
                }
              }
              bVar15 = *(byte *)&local_538->_flags;
              *(char *)&local_530->_flags = '\0';
              pFVar9 = fopen((char *)pFVar7,"r");
              if (pFVar9 == (FILE *)0x0) {
                piVar13 = __errno_location();
                strerror(*piVar13);
                warnf(*(undefined8 *)(param_1 + 0x4d8),"Cannot read from %s: %s\n",pFVar7);
              }
              else {
                uVar12 = 0;
                bVar2 = false;
                local_5b8 = 0;
LAB_0010f9d0:
                uVar6 = getc(pFVar9);
                uVar4 = uVar12;
                if (uVar6 != 0xffffffff) {
                  if (local_5b8 == 0) {
                    iVar5 = Curl_isspace(uVar6 & 0xff);
                    if (iVar5 == 0) goto LAB_0010fa3b;
                    goto LAB_0010fa40;
                  }
                  if (uVar6 == 10) goto LAB_0010fa55;
                  if (uVar6 == 0xd) goto LAB_0010f9d0;
LAB_0010f9f4:
                  local_5b8 = local_5b8 + 1;
                  if (!bVar2) {
                    if (uVar12 == 0x3e6) {
                      warnf(*(undefined8 *)(param_1 + 0x4d8),
                            "File %s line %d: header too long (truncated)\n",pFVar7);
                      uStack66 = 0x20;
                      uVar12 = 999;
                    }
                    else if (uVar12 < 999) {
                      local_428[uVar12] = (char)uVar6;
                      uVar12 = uVar12 + 1;
                    }
                  }
                  goto LAB_0010f9d0;
                }
LAB_0010fa3b:
                do {
                  uVar12 = uVar4;
                  if (uVar12 == 0) goto LAB_0010fa40;
                  iVar5 = Curl_isspace(local_438[uVar12 + 0xf]);
                  uVar4 = uVar12 - 1;
                } while (iVar5 != 0);
                local_428[uVar12] = 0;
                lVar8 = curl_slist_append(local_590,local_428);
                if (lVar8 == 0) {
                  curl_mfprintf(*(undefined8 *)(*(long *)(param_1 + 0x4d8) + 8),
                                "Out of memory for field headers!\n");
                  fclose(pFVar9);
                  goto LAB_001100ad;
                }
                uVar12 = 0;
                local_590 = lVar8;
LAB_0010fa40:
                if (uVar6 == 0xd) goto LAB_0010f9d0;
                if (0xd < (int)uVar6) {
                  if ((uVar6 == 0x23) && (local_5b8 == 0)) {
                    bVar2 = true;
                    local_5b8 = 1;
                    goto LAB_0010f9d0;
                  }
                  goto LAB_0010f9f4;
                }
                if (uVar6 != 0xffffffff) {
                  if (uVar6 != 10) goto LAB_0010f9f4;
LAB_0010fa55:
                  bVar2 = false;
                  local_5b8 = 0;
                  goto LAB_0010f9d0;
                }
                iVar5 = ferror(pFVar9);
                if (iVar5 != 0) {
                  piVar13 = __errno_location();
                  pcVar14 = strerror(*piVar13);
                  curl_mfprintf(*(undefined8 *)(*(long *)(param_1 + 0x4d8) + 8),
                                "Header file %s read error: %s\n",pFVar7,pcVar14);
                  fclose(pFVar9);
                  goto LAB_001100ad;
                }
                fclose(pFVar9);
                pFVar9 = (FILE *)0x0;
              }
            }
            else {
              while (iVar5 = Curl_isspace(cVar1), pFVar9 = local_538, iVar5 != 0) {
                cVar1 = *(char *)((long)&local_538->_flags + 1);
                local_538 = (FILE *)((long)&local_538->_flags + 1);
              }
              pFVar7 = (FILE *)get_param_word(param_1,&local_538,&local_530);
              if (pFVar9 == pFVar7) {
                while ((pFVar7 < local_530 &&
                       (iVar5 = Curl_isspace(local_530[-1]._unused2[0x13]), iVar5 != 0))) {
                  local_530 = (FILE *)(local_530[-1]._unused2 + 0x13);
                }
              }
              bVar15 = *(byte *)&local_538->_flags;
              *(char *)&local_530->_flags = '\0';
              lVar8 = curl_slist_append(local_590,pFVar7);
              if (lVar8 == 0) {
                curl_mfprintf(*(undefined8 *)(*(long *)(param_1 + 0x4d8) + 8),
                              "Out of memory for field header!\n");
LAB_001100ad:
                curl_slist_free_all(local_590);
                uVar12 = 0xffffffff;
                goto LAB_0010f903;
              }
              pFVar9 = (FILE *)0x0;
              local_590 = lVar8;
            }
          }
        }
        else {
          *(char *)&pFVar9->_flags = '\0';
LAB_0010fb0a:
          ppcVar3 = &local_538->_IO_read_ptr;
          while( true ) {
            local_538 = (FILE *)((long)ppcVar3 + 1);
            iVar5 = Curl_isspace(*(char *)&local_538->_flags);
            pFVar9 = local_538;
            if (iVar5 == 0) break;
            ppcVar3 = (char **)&local_538->_flags;
          }
          local_588 = (FILE *)get_param_word(param_1,&local_538,&local_530);
          if (pFVar9 == local_588) {
            while ((local_588 < local_530 &&
                   (iVar5 = Curl_isspace(local_530[-1]._unused2[0x13]), iVar5 != 0))) {
              local_530 = (FILE *)(local_530[-1]._unused2 + 0x13);
            }
          }
LAB_0010fb76:
          pFVar9 = (FILE *)0x0;
          bVar15 = *(byte *)&local_538->_flags;
          *(char *)&local_530->_flags = '\0';
        }
      }
    } while (bVar15 == 0x3b);
    if (pFVar9 != (FILE *)0x0) {
      *(char *)&pFVar9->_flags = '\0';
    }
    *param_5 = local_570;
    if (param_6 == (FILE **)0x0) {
      if (local_588 != (FILE *)0x0) {
        warnf(*(undefined8 *)(param_1 + 0x4d8),"Field file name not allowed here: %s\n");
      }
    }
    else {
      *param_6 = local_588;
    }
    if (param_7 == (FILE **)0x0) {
      if (local_560 != (FILE *)0x0) {
        warnf(*(undefined8 *)(param_1 + 0x4d8),"Field encoder not allowed here: %s\n");
      }
      goto LAB_0010f8e2;
    }
  }
  else {
    *param_5 = (FILE *)0x0;
    if (param_6 != (FILE **)0x0) {
      *param_6 = (FILE *)0x0;
    }
    local_590 = 0;
    local_560 = (FILE *)0x0;
    if (param_7 == (FILE **)0x0) goto LAB_0010f8e2;
  }
  *param_7 = local_560;
LAB_0010f8e2:
  *param_8 = local_590;
  *param_3 = local_538;
  uVar12 = (ulong)bVar15;
LAB_0010f903:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar12;
}