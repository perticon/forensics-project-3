int64_t get_param_part(int64_t a1, uint32_t a2, int64_t * a3, int64_t * a4, int64_t * a5, int64_t * a6, int64_t * a7, int64_t * a8) {
    // 0xf590
    int128_t v1; // 0xf590
    int128_t v2 = v1;
    int128_t v3 = __asm_pxor(v2, v2); // 0xf592
    int64_t v4 = __readfsqword(40); // 0xf5de
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    int64_t v5 = (int64_t)a3; // bp-1336, 0xf622
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    *a5 = 0;
    __asm_movaps(v3);
    __asm_movaps(v3);
    __asm_movaps(v3);
    if (a6 != NULL) {
        // 0xf686
        *a6 = 0;
    }
    // 0xf68d
    *a8 = 0;
    if (a7 != NULL) {
        // 0xf6a1
        *a7 = 0;
    }
    // 0xf6c4
    if ((int32_t)Curl_isspace() != 0) {
        v5++;
        while ((int32_t)Curl_isspace() != 0) {
            // 0xf6b0
            v5++;
        }
    }
    uint64_t v6 = 0x100000000000000 * (int64_t)a2 >> 56; // 0xf6dd
    char * v7; // bp-1328, 0xf590
    uint64_t v8 = get_param_word(a1, &v5, (int64_t *)&v7, v6); // 0xf702
    *a4 = v8;
    char * v9 = v7; // 0xf715
    if (v8 == v5 == v8 < (int64_t)v7) {
        char * v10 = v7;
        v9 = v10;
        while ((int32_t)Curl_isspace() != 0) {
            uint64_t v11 = (int64_t)v10 - 1; // 0xffd8
            char * v12 = (char *)v11;
            v7 = v12;
            v9 = v12;
            if (v11 <= v6) {
                // break -> 0xf71b
                break;
            }
            v10 = v7;
            v9 = v10;
        }
    }
    unsigned char v13 = *(char *)v5; // 0xf723
    int64_t v14 = v13; // 0xf723
    *v9 = 0;
    int64_t v15 = 0; // 0xf72e
    int64_t v16 = 0; // 0xf72e
    int64_t v17 = 0; // 0xf72e
    int64_t v18 = 0; // 0xf72e
    int64_t v19 = v14; // 0xf72e
    int64_t v20 = 0; // 0xf72e
    int64_t v21; // 0xf590
    int64_t v22; // 0xf590
    int64_t v23; // 0xf590
    int64_t v24; // 0xf590
    int64_t v25; // 0xf590
    int64_t v26; // 0xf590
    if (v13 != 59) {
        // 0x100fd
        *a5 = 0;
        if (a6 != NULL) {
            // 0x10111
            *a6 = 0;
        }
        // 0x1011d
        v25 = 0;
        v23 = 0;
        v21 = v14;
        v26 = 0;
        v24 = 0;
        v22 = v14;
        if (a7 != NULL) {
            goto lab_0xf8d5;
        } else {
            goto lab_0xf8e2;
        }
    } else {
        goto lab_0xf768;
    }
  lab_0xfa40:;
    // 0xfa40
    int64_t v27; // 0xf590
    int64_t v28 = v27;
    int64_t v29; // 0xf590
    int64_t v30 = v29;
    int64_t v31 = v30; // 0xfa43
    char v32; // 0xf590
    char v33 = v32; // 0xfa43
    int64_t v34 = v28; // 0xfa43
    int64_t v35; // 0xf590
    int64_t v36; // 0xf590
    int64_t v37; // 0xf590
    int64_t v38; // 0xf590
    int64_t v39; // 0xf590
    int64_t v40; // 0xf590
    int64_t v41; // 0xf590
    int64_t v42; // 0xf590
    int64_t v43; // 0xf590
    int64_t v44; // 0xf590
    int64_t v45; // 0xf590
    int64_t v46; // 0xf590
    int64_t v47; // 0xf590
    int64_t v48; // 0xf590
    int64_t v49; // 0xf590
    int64_t v50; // 0xf590
    int64_t v51; // bp-1464, 0xf590
    int32_t v52; // 0xf9da
    int64_t v53; // 0xf99b
    int64_t v54; // 0xf9a2
    if (v52 == 13) {
        goto lab_0xf9d0;
    } else {
        if (v52 > 13) {
            // 0xfaa0
            v48 = v30;
            v35 = v28;
            if (v52 != 35) {
                goto lab_0xf9f4;
            } else {
                // 0xfaa9
                if (v51 != 0) {
                    goto lab_0xf9f4;
                } else {
                    // 0xfab4
                    v51 = 1;
                    v31 = v30;
                    v33 = 1;
                    v34 = v28;
                    goto lab_0xf9d0;
                }
            }
        } else {
            if (v52 == -1) {
                // 0xfc60
                if ((int32_t)function_c030() != 0) {
                    // 0x100c1
                    function_b880();
                    function_b960();
                    function_bd70();
                    function_bc70();
                    v50 = v30;
                    v38 = v53;
                    v40 = v54;
                    goto lab_0x100ad;
                } else {
                    // 0xfc70
                    function_bc70();
                    v47 = v30;
                    v42 = v41;
                    v46 = v45;
                    v44 = v43;
                    v37 = v53;
                    v39 = 0;
                    goto lab_0xf890;
                }
            }
            // 0xfa50
            v48 = v30;
            v35 = v28;
            v49 = v30;
            v36 = v28;
            if (v52 != 10) {
                goto lab_0xf9f4;
            } else {
                goto lab_0xfa55;
            }
        }
    }
  lab_0xfa3b:;
    // 0xfa3b
    int64_t v55; // 0xf590
    int64_t v56 = v55; // 0xfa3e
    int64_t v57; // 0xf590
    v29 = v57;
    v27 = v55;
    int64_t v58; // 0xf590
    if (v55 != 0) {
        int64_t v59 = v56;
        while ((int32_t)Curl_isspace() != 0) {
            int64_t v60 = v59 - 1; // 0xfa26
            v56 = v60;
            v29 = v57;
            v27 = v60;
            if (v60 == 0) {
                goto lab_0xfa40;
            }
            v59 = v56;
        }
        // 0xfe20
        *(char *)(v59 + v58) = 0;
        int64_t v61 = function_b9a0(); // 0xfe31
        v29 = v61;
        v27 = 0;
        if (v61 == 0) {
            // break -> 0x10140
            goto lab_0x10140;
        }
    }
    goto lab_0xfa40;
  lab_0xf9d0:;
    int64_t v62 = v31; // 0xf590
    char v63 = v33; // 0xf590
    int64_t v64 = v34; // 0xf590
    goto lab_0xf9d0_2;
  lab_0xfa55:
    // 0xfa55
    v51 = 0;
    v31 = v49;
    v33 = 0;
    v34 = v36;
    goto lab_0xf9d0;
  lab_0xf9f4:;
    int64_t v66 = v35;
    int64_t v67 = v48;
    v51++;
    v31 = v67;
    v33 = v32;
    v34 = v66;
    char * v68; // 0xf590
    int64_t v65; // 0xf9d3
    if (v32 == 0) {
        if (v66 == 998) {
            // 0xfe8f
            warnf();
            *v68 = 32;
            v31 = v67;
            v33 = v32;
            v34 = 999;
        } else {
            // 0xfa0d
            v31 = v67;
            v33 = v32;
            v34 = v66;
            if (v66 < 999) {
                // 0xfa0f
                *(char *)(v66 + v58) = (char)v65;
                v31 = v67;
                v33 = v32;
                v34 = v66 + 1;
            }
        }
    }
    goto lab_0xf9d0;
  lab_0xf768:;
    int64_t v69 = &v51; // 0xf5a4
    int64_t v70 = v20;
    int64_t v71 = v19;
    v43 = v18;
    v45 = v17;
    v41 = v16;
    int64_t v72 = v15;
    v5++;
    while ((int32_t)Curl_isspace() != 0) {
        // 0xf768
        v5++;
    }
    int64_t v73 = function_ba20();
    int64_t v74; // 0xf590
    int64_t v75; // 0xf590
    int64_t v76; // 0xf590
    int64_t v77; // 0xf590
    int64_t v78; // 0xf590
    int64_t v79; // 0xf590
    if (v70 == 0) {
        if ((int32_t)v73 != 0) {
            // 0xfd1c
            v5 += 5;
            int64_t v80 = v5; // 0xfd46
            int64_t v81 = v80 + 1;
            int64_t v82 = v81; // 0xfd4e
            while ((int32_t)Curl_isspace() != 0) {
                // 0xfd34
                v5 = v82;
                v80 = v5;
                v81 = v80 + 1;
                v82 = v81;
            }
            // 0xfd50
            if ((int32_t)function_bc50() != 2) {
                // 0x10061
                warnf();
                function_bdf0();
                v79 = v72;
                v76 = v41;
                v78 = v45;
                v77 = v80;
                v74 = v71;
                v75 = 0;
                goto lab_0xf903;
            } else {
                int64_t v83 = function_b9d0(); // 0xfd88
                int64_t v84 = v83 + v81 + function_b9d0(); // 0xfd9d
                v5 = v84;
                unsigned char v85 = *(char *)v84; // 0xfdaa
                int64_t v86 = v85; // 0xfdaa
                v47 = v72;
                v42 = v41;
                v46 = v45;
                v44 = v80;
                v37 = v86;
                v39 = v84;
                if (v85 != 0 && v85 != 59) {
                    int64_t v87 = v86; // 0xfdf8
                    int64_t v88 = v84;
                    int64_t v89 = v88; // 0xfe15
                    while ((char)(0x1000000 * a2 / 0x1000000) != (char)v87) {
                        int64_t v90 = v5 + 1; // 0xfde3
                        int64_t v91 = (int32_t)Curl_isspace() != 0 ? v88 : v90;
                        v5 = v90;
                        unsigned char v92 = *(char *)v90; // 0xfdf8
                        v87 = v92;
                        v89 = v91;
                        if (v92 == 0 || v92 == 59) {
                            // break -> 0xf890
                            break;
                        }
                        v88 = v91;
                        v89 = v88;
                    }
                    // 0xf890
                    v47 = v72;
                    v42 = v41;
                    v46 = v45;
                    v44 = v80;
                    v37 = v87;
                    v39 = v89;
                }
                goto lab_0xf890;
            }
        } else {
            // 0xfae9
            if ((int32_t)function_ba20() == 0) {
                // 0xfc80
                if ((int32_t)function_ba20() != 0) {
                    goto lab_0xf7d8;
                } else {
                    // 0xfca1
                    if ((int32_t)function_ba20() != 0) {
                        goto lab_0xfecc;
                    } else {
                        int64_t v93 = get_param_word(a1, &v5, (int64_t *)&v7, v6 & 0xffffffff); // 0xfcd5
                        int64_t v94 = (int64_t)*(char *)v5; // 0xfce2
                        *v7 = 0;
                        v47 = v72;
                        v42 = v41;
                        v46 = v45;
                        v44 = v43;
                        v37 = v94;
                        v39 = 0;
                        if (*(char *)v93 != 0) {
                            // 0xfcfa
                            warnf();
                            v47 = v72;
                            v42 = v41;
                            v46 = v45;
                            v44 = v43;
                            v37 = v94;
                            v39 = 0;
                        }
                        goto lab_0xf890;
                    }
                }
            } else {
                goto lab_0xfb0a;
            }
        }
    } else {
        if ((int32_t)v73 != 0) {
            // 0xfb90
            *(char *)v70 = 0;
            goto lab_0xfb0a;
        } else {
            // 0xf7b3
            if ((int32_t)function_ba20() == 0) {
                // 0xfbb8
                if ((int32_t)function_ba20() != 0) {
                    // 0xfec8
                    *(char *)v70 = 0;
                    goto lab_0xfecc;
                } else {
                    unsigned char v95 = *(char *)v5; // 0xfbe1
                    v47 = v72;
                    v42 = v41;
                    v46 = v45;
                    v44 = v43;
                    v37 = v95;
                    v39 = v5;
                    if (v95 != 59 && v95 != 0) {
                        // 0xfbfa
                        int32_t v96; // 0xf5d9
                        char v97 = v96; // 0xfbff
                        v47 = v72;
                        v42 = v41;
                        v46 = v45;
                        v44 = v43;
                        int64_t v98; // 0xfbe1
                        v37 = v98;
                        int64_t v99; // 0xfbd9
                        v39 = v99;
                        int64_t v100 = v99; // 0xfc04
                        if (v95 != v97) {
                            int64_t v101 = v100;
                            int64_t v102 = v5; // 0xfc24
                            int64_t v103 = v102 + 1; // 0xfc2c
                            int64_t v104 = (int32_t)Curl_isspace() != 0 ? v101 : v103;
                            v5 = v103;
                            unsigned char v105 = *(char *)v103; // 0xfc41
                            v100 = v104;
                            while (v105 != v97 && v105 != 0 && v105 != 59) {
                                // 0xfc19
                                v101 = v100;
                                v102 = v5;
                                v103 = v102 + 1;
                                v104 = (int32_t)Curl_isspace() != 0 ? v101 : v103;
                                v5 = v103;
                                v105 = *(char *)v103;
                                v100 = v104;
                            }
                            // 0xf890
                            v47 = v72;
                            v42 = v41;
                            v46 = v45;
                            v44 = v43;
                            v37 = v105;
                            v39 = v104;
                        }
                    }
                    goto lab_0xf890;
                }
            } else {
                // 0xf7d4
                *(char *)v70 = 0;
                goto lab_0xf7d8;
            }
        }
    }
  lab_0xf8d5:
    // 0xf8d5
    *a7 = v23;
    v26 = v25;
    v24 = v23;
    int64_t v106; // 0xf590
    int64_t v107 = v106; // 0xf8df
    int64_t v108; // 0xf590
    int64_t v109 = v108; // 0xf8df
    v22 = v21;
    int64_t v110; // 0xf590
    int64_t v111 = v110; // 0xf8df
    goto lab_0xf8e2;
  lab_0xf8e2:
    // 0xf8e2
    *a8 = v26;
    *a3 = v5;
    v79 = v26;
    v76 = v24;
    v78 = v107;
    v77 = v109;
    v74 = v22;
    v75 = v111;
    goto lab_0xf903;
  lab_0xf903:
    // 0xf903
    if (v4 == __readfsqword(40)) {
        // 0xf91a
        int64_t v112; // 0xf590
        int64_t result = v112;
        return result;
    }
    // 0x1016b
    function_bd40();
    int64_t v113 = v79; // 0x1016b
    int64_t v114 = v76; // 0x1016b
    int64_t v115 = v78; // 0x1016b
    int64_t v116 = v77; // 0x1016b
    int64_t v117 = v74; // 0x1016b
    int64_t v118 = v75; // 0x1016b
    goto lab_0x10170;
  lab_0x10170:
    // 0x10170
    function_b880();
    function_b960();
    warnf();
    v47 = v113;
    v42 = v114;
    v46 = v115;
    v44 = v116;
    v37 = v117;
    v39 = v118;
    goto lab_0xf890;
  lab_0xfb0a:
    // 0xfb0a
    v5 += 9;
    if ((int32_t)Curl_isspace() != 0) {
        v5++;
        while ((int32_t)Curl_isspace() != 0) {
            // 0xfb20
            v5++;
        }
    }
    int64_t v149 = v5; // 0xfb53
    int64_t v150 = get_param_word(a1, &v5, (int64_t *)&v7, v6 & 0xffffffff); // 0xfb5b
    int64_t v142 = v41; // 0xfb70
    int64_t v143 = v150; // 0xfb70
    char * v141 = v7; // 0xfb70
    if (v149 == v150 == v150 < (int64_t)v7) {
        char * v151 = v7; // 0xfe80
        v142 = v41;
        v143 = v149;
        v141 = v151;
        while ((int32_t)Curl_isspace() != 0) {
            uint64_t v152 = (int64_t)v151 - 1; // 0xfe60
            char * v153 = (char *)v152;
            v7 = v153;
            v142 = v41;
            v143 = v149;
            v141 = v153;
            if (v152 <= v149) {
                // break -> 0xfb76
                break;
            }
            v151 = v7;
            v142 = v41;
            v143 = v149;
            v141 = v151;
        }
    }
    goto lab_0xfb76;
  lab_0xf890:;
    int64_t v119 = v39;
    int64_t v120 = v37;
    int64_t v121 = v44;
    int64_t v122 = v46;
    int64_t v123 = v42;
    int64_t v124 = v47;
    v15 = v124;
    v16 = v123;
    v17 = v122;
    v18 = v121;
    v19 = v120;
    v20 = v119;
    if ((char)v120 == 59) {
        goto lab_0xf768;
    } else {
        if (v119 != 0) {
            // 0xf89f
            *(char *)v119 = 0;
        }
        // 0xf8a3
        *a5 = v121;
        if (a6 == NULL) {
            if (v122 != 0) {
                // 0x10042
                warnf();
            }
        } else {
            // 0xf8bc
            *a6 = v122;
        }
        // 0xf8c9
        v25 = v124;
        v23 = v123;
        v106 = v122;
        v108 = v121;
        v21 = v120;
        v110 = v119;
        if (a7 == NULL) {
            // 0x10007
            v26 = v124;
            v24 = 0;
            v107 = v122;
            v109 = v121;
            v22 = v120;
            v111 = v119;
            if (v123 != 0) {
                // 0x10015
                warnf();
                v26 = v124;
                v24 = v123;
                v107 = v122;
                v109 = v121;
                v22 = v120;
                v111 = v119;
            }
            goto lab_0xf8e2;
        } else {
            goto lab_0xf8d5;
        }
    }
  lab_0xf7d8:;
    int64_t v125 = v5; // 0xf7d8
    int64_t v126 = v125 + 8; // 0xf7e0
    v5 = v126;
    if ((*(char *)v126 - 60 & -5) == 0) {
        // 0xf938
        v5 = v125 + 9;
        if ((int32_t)Curl_isspace() != 0) {
            v5++;
            while ((int32_t)Curl_isspace() != 0) {
                // 0xf930
                v5++;
            }
        }
        uint64_t v127 = v5; // 0xf964
        uint64_t v128 = get_param_word(a1, &v5, (int64_t *)&v7, v6 & 0xffffffff); // 0xf96c
        char * v129 = v7; // 0xf981
        if (v127 == v128 == v128 < (int64_t)v7) {
            char * v130 = v7; // 0xffb0
            v129 = v130;
            while ((int32_t)Curl_isspace() != 0) {
                uint64_t v131 = (int64_t)v130 - 1; // 0xff90
                char * v132 = (char *)v131;
                v7 = v132;
                v129 = v132;
                if (v131 <= v127) {
                    // break -> 0xf987
                    break;
                }
                v130 = v7;
                v129 = v130;
            }
        }
        // 0xf987
        v53 = (int64_t)*(char *)v5;
        *v129 = 0;
        v54 = function_bdb0();
        v113 = v72;
        v114 = v41;
        v115 = v45;
        v116 = v43;
        v117 = v53;
        v118 = 0;
        if (v54 == 0) {
            goto lab_0x10170;
        } else {
            // 0xf9b3
            v51 = 0;
            v58 = v69 + 400;
            v68 = (char *)(v69 + 1398);
            v62 = v72;
            v63 = 0;
            v64 = 0;
            while (true) {
              lab_0xf9d0_2:
                // 0xf9d0
                v55 = v64;
                v32 = v63;
                v57 = v62;
                v65 = function_b910();
                v52 = v65;
                if (v52 == -1) {
                    goto lab_0xfa3b;
                } else {
                    // 0xf9df
                    if (v51 == 0) {
                        // 0xfba0
                        v29 = v57;
                        v27 = v55;
                        if ((int32_t)Curl_isspace() != 0) {
                            goto lab_0xfa40;
                        } else {
                            goto lab_0xfa3b;
                        }
                    } else {
                        // 0xf9ea
                        v48 = v57;
                        v35 = v55;
                        v31 = v57;
                        v33 = v32;
                        v34 = v55;
                        v49 = v57;
                        v36 = v55;
                        switch (v52) {
                            case 10: {
                                goto lab_0xfa55;
                            }
                            case 13: {
                                goto lab_0xf9d0;
                            }
                            default: {
                                goto lab_0xf9f4;
                            }
                        }
                    }
                }
            }
          lab_0x10140:
            // 0x10140
            function_bd70();
            function_bc70();
            v50 = v57;
            v38 = v53;
            v40 = v54;
            goto lab_0x100ad;
        }
    } else {
        // 0xf820
        if ((int32_t)Curl_isspace() != 0) {
            v5++;
            while ((int32_t)Curl_isspace() != 0) {
                // 0xf808
                v5++;
            }
        }
        uint64_t v133 = v5; // 0xf83c
        int64_t v134 = get_param_word(a1, &v5, (int64_t *)&v7, v6 & 0xffffffff); // 0xf844
        char * v135 = v7; // 0xf857
        if (v133 == v134 == v133 < (int64_t)v7) {
            char * v136 = v7; // 0xfa7b
            v135 = v136;
            while ((int32_t)Curl_isspace() != 0) {
                uint64_t v137 = (int64_t)v136 - 1; // 0xfa89
                char * v138 = (char *)v137;
                v7 = v138;
                v135 = v138;
                if (v137 <= v133) {
                    // break -> 0xf85d
                    break;
                }
                v136 = v7;
                v135 = v136;
            }
        }
        int64_t v139 = (int64_t)*(char *)v5; // 0xf86d
        *v135 = 0;
        int64_t v140 = function_b9a0(); // 0xf874
        v47 = v140;
        v42 = v41;
        v46 = v45;
        v44 = v43;
        v37 = v139;
        v39 = 0;
        if (v140 == 0) {
            // 0x1008f
            function_bd70();
            v50 = v72;
            v38 = v139;
            v40 = v70;
            goto lab_0x100ad;
        } else {
            goto lab_0xf890;
        }
    }
  lab_0xfb76:
    // 0xfb76
    *v141 = 0;
    v47 = v72;
    v42 = v142;
    v46 = v143;
    v44 = v43;
    v37 = (int64_t)*(char *)v5;
    v39 = 0;
    goto lab_0xf890;
  lab_0xfecc:
    // 0xfecc
    v5 += 8;
    if ((int32_t)Curl_isspace() != 0) {
        v5++;
        while ((int32_t)Curl_isspace() != 0) {
            // 0xfee0
            v5++;
        }
    }
    int64_t v144 = v5; // 0xff13
    int64_t v145 = get_param_word(a1, &v5, (int64_t *)&v7, v6 & 0xffffffff); // 0xff1b
    v142 = v145;
    v143 = v45;
    v141 = v7;
    if (v144 == v145 == v145 < (int64_t)v7) {
        char * v146 = v7; // 0xff70
        v142 = v144;
        v143 = v45;
        v141 = v146;
        while ((int32_t)Curl_isspace() != 0) {
            uint64_t v147 = (int64_t)v146 - 1; // 0xff50
            char * v148 = (char *)v147;
            v7 = v148;
            v142 = v144;
            v143 = v45;
            v141 = v148;
            if (v147 <= v144) {
                // break -> 0xfb76
                break;
            }
            v146 = v7;
            v142 = v144;
            v143 = v45;
            v141 = v146;
        }
    }
    goto lab_0xfb76;
  lab_0x100ad:
    // 0x100ad
    function_bdf0();
    v79 = v50;
    v76 = v41;
    v78 = v45;
    v77 = v43;
    v74 = v38;
    v75 = v40;
    goto lab_0xf903;
}