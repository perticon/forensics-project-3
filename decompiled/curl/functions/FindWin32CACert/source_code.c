CURLcode FindWin32CACert(struct OperationConfig *config,
                         curl_sslbackend backend,
                         const TCHAR *bundle_file)
{
  CURLcode result = CURLE_OK;

  /* Search and set cert file only if libcurl supports SSL.
   *
   * If Schannel is the selected SSL backend then these locations are
   * ignored. We allow setting CA location for schannel only when explicitly
   * specified by the user via CURLOPT_CAINFO / --cacert.
   */
  if((curlinfo->features & CURL_VERSION_SSL) &&
     backend != CURLSSLBACKEND_SCHANNEL) {

    DWORD res_len;
    TCHAR buf[PATH_MAX];
    TCHAR *ptr = NULL;

    buf[0] = TEXT('\0');

    res_len = SearchPath(NULL, bundle_file, NULL, PATH_MAX, buf, &ptr);
    if(res_len > 0) {
      char *mstr = curlx_convert_tchar_to_UTF8(buf);
      Curl_safefree(config->cacert);
      if(mstr)
        config->cacert = strdup(mstr);
      curlx_unicodefree(mstr);
      if(!config->cacert)
        result = CURLE_OUT_OF_MEMORY;
    }
  }

  return result;
}