undefined8 glob_next_url(char **param_1,int *param_2)

{
  int iVar1;
  bool bVar2;
  int *piVar3;
  size_t sVar4;
  ulong uVar5;
  char cVar6;
  ulong uVar7;
  long lVar8;
  ulong uVar9;
  char *pcVar10;
  char *pcVar11;
  
  lVar8 = *(long *)(param_2 + 0x4b2);
  pcVar11 = *(char **)(param_2 + 0x4b4);
  *param_1 = (char *)0x0;
  uVar9 = *(ulong *)(param_2 + 0x4b0);
  lVar8 = lVar8 + 1;
  if (*(char *)(param_2 + 0x4b6) == '\0') {
    *(undefined *)(param_2 + 0x4b6) = 1;
    if (uVar9 != 0) {
LAB_001203d8:
      uVar9 = 0;
      iVar1 = *param_2;
      piVar3 = param_2;
      pcVar10 = pcVar11;
      if (iVar1 == 2) goto LAB_001204c5;
      do {
        if (iVar1 == 3) {
          curl_msnprintf(pcVar11,lVar8,"%0*lu",piVar3[6],*(undefined8 *)(piVar3 + 8));
          sVar4 = strlen(pcVar11);
          pcVar11 = pcVar11 + sVar4;
          lVar8 = lVar8 - sVar4;
        }
        else {
          if (iVar1 != 1) {
LAB_00120406:
            curl_mprintf("internal error: invalid pattern type (%d)\n");
            return 2;
          }
          if (*(long *)(piVar3 + 2) != 0) {
            curl_msnprintf(pcVar11,lVar8,"%s",
                           *(undefined8 *)(*(long *)(piVar3 + 2) + (long)piVar3[5] * 8));
            sVar4 = strlen(pcVar11);
            lVar8 = lVar8 - sVar4;
            pcVar11 = pcVar11 + sVar4;
          }
        }
        while( true ) {
          uVar9 = uVar9 + 1;
          piVar3 = piVar3 + 0xc;
          if (*(ulong *)(param_2 + 0x4b0) < uVar9 || *(ulong *)(param_2 + 0x4b0) == uVar9)
          goto LAB_0012043c;
          iVar1 = *piVar3;
          pcVar10 = pcVar11;
          if (iVar1 != 2) break;
LAB_001204c5:
          pcVar11 = pcVar10;
          if (lVar8 != 0) {
            cVar6 = *(char *)((long)piVar3 + 10);
            lVar8 = lVar8 + -1;
            pcVar10[1] = '\0';
            pcVar11 = pcVar10 + 1;
            *pcVar10 = cVar6;
          }
        }
      } while( true );
    }
LAB_0012043c:
    pcVar11 = strdup(*(char **)(param_2 + 0x4b4));
    *param_1 = pcVar11;
    if (pcVar11 == (char *)0x0) {
      return 0x1b;
    }
  }
  else if (uVar9 != 0) {
    piVar3 = param_2 + uVar9 * 0xc + -0xc;
    uVar5 = 1;
    do {
      iVar1 = *piVar3;
      if (iVar1 == 2) {
        cVar6 = *(char *)((long)piVar3 + 10) + *(char *)(piVar3 + 3);
        *(char *)((long)piVar3 + 10) = cVar6;
        if (cVar6 <= *(char *)((long)piVar3 + 9)) goto LAB_001203d8;
        *(undefined *)((long)piVar3 + 10) = *(undefined *)(piVar3 + 2);
      }
      else if (iVar1 == 3) {
        uVar7 = *(long *)(piVar3 + 10) + *(long *)(piVar3 + 8);
        *(ulong *)(piVar3 + 8) = uVar7;
        if (uVar7 < *(ulong *)(piVar3 + 4) || uVar7 == *(ulong *)(piVar3 + 4)) goto LAB_001203d8;
        *(undefined8 *)(piVar3 + 8) = *(undefined8 *)(piVar3 + 2);
      }
      else {
        if (iVar1 != 1) goto LAB_00120406;
        if ((*(long *)(piVar3 + 2) == 0) ||
           (iVar1 = piVar3[5], piVar3[5] = iVar1 + 1, iVar1 + 1 != piVar3[4])) goto LAB_001203d8;
        piVar3[5] = 0;
      }
      piVar3 = piVar3 + -0xc;
      bVar2 = uVar5 < uVar9;
      uVar5 = uVar5 + 1;
    } while (bVar2);
  }
  return 0;
}