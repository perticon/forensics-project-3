long tvdiff(long param_1,long param_2,long param_3,long param_4)

{
  return (param_1 - param_3) * 1000 + (param_2 - param_4) / 1000;
}