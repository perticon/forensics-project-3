undefined8 delegation(long param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  
  iVar1 = curl_strequal(&DAT_0013976b);
  uVar2 = 0;
  if (iVar1 == 0) {
    iVar1 = curl_strequal("policy",param_2);
    uVar2 = 1;
    if (iVar1 == 0) {
      iVar1 = curl_strequal("always",param_2);
      uVar2 = 2;
      if (iVar1 == 0) {
        warnf(*(undefined8 *)(param_1 + 0x4d8),"unrecognized delegation method \'%s\', using none\n"
              ,param_2);
        return 0;
      }
    }
  }
  return uVar2;
}