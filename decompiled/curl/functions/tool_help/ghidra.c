void tool_help(void *param_1)

{
  int iVar1;
  char *pcVar2;
  char **ppcVar3;
  char *pcVar4;
  ulong uVar5;
  
  puts("Usage: curl [options...] <url>");
  if (param_1 == (void *)0x0) {
    print_category(0x200);
    puts(
        "\nThis is not the full help, this menu is stripped into categories.\nUse \"--help category\" to get an overview of all categories.\nFor all options use the manual or \"--help all\"."
        );
    free((void *)0x0);
    return;
  }
  iVar1 = curl_strequal(param_1,"all");
  if (iVar1 == 0) {
    iVar1 = curl_strequal(param_1,"category");
    if (iVar1 != 0) {
      pcVar2 = "Different types of authentication methods";
      pcVar4 = "auth";
      ppcVar3 = (char **)(categories + 0x18);
      while( true ) {
        curl_mprintf(" %-11s %s\n",pcVar4,pcVar2);
        pcVar4 = *ppcVar3;
        if (pcVar4 == (char *)0x0) break;
        pcVar2 = ppcVar3[1];
        ppcVar3 = ppcVar3 + 3;
      }
      free(param_1);
      return;
    }
    uVar5 = 0;
    pcVar2 = "auth";
    ppcVar3 = (char **)(categories + 0x18);
    do {
      iVar1 = curl_strequal(pcVar2,param_1);
      if (iVar1 != 0) {
        curl_mprintf("%s: %s\n",pcVar2,*(undefined8 *)(categories + uVar5 * 0x18 + 8));
        print_category(*(undefined4 *)(categories + uVar5 * 0x18 + 0x10));
        goto LAB_001158c1;
      }
      pcVar2 = *ppcVar3;
      ppcVar3 = ppcVar3 + 3;
      uVar5 = (ulong)((int)uVar5 + 1);
    } while (pcVar2 != (char *)0x0);
    puts("Invalid category provided, here is a list of all categories:\n");
    pcVar2 = "Different types of authentication methods";
    pcVar4 = "auth";
    ppcVar3 = (char **)(categories + 0x18);
    while( true ) {
      curl_mprintf(" %-11s %s\n",pcVar4,pcVar2);
      pcVar4 = *ppcVar3;
      if (pcVar4 == (char *)0x0) break;
      pcVar2 = ppcVar3[1];
      ppcVar3 = ppcVar3 + 3;
    }
  }
  else {
    print_category(0xfffffffe);
  }
LAB_001158c1:
  free(param_1);
  return;
}