static CURLcode libcurl_generate_mime_part(CURL *curl,
                                           struct GlobalConfig *config,
                                           struct tool_mime *part,
                                           int mimeno)
{
  CURLcode ret = CURLE_OK;
  int submimeno = 0;
  char *escaped = NULL;
  const char *data = NULL;
  const char *filename = part->filename;

  /* Parts are linked in reverse order. */
  if(part->prev) {
    ret = libcurl_generate_mime_part(curl, config, part->prev, mimeno);
    if(ret)
      return ret;
  }

  /* Create the part. */
  CODE2("part%d = curl_mime_addpart(mime%d);", mimeno, mimeno);

  switch(part->kind) {
  case TOOLMIME_PARTS:
    ret = libcurl_generate_mime(curl, config, part, &submimeno);
    if(!ret) {
      CODE2("curl_mime_subparts(part%d, mime%d);", mimeno, submimeno);
      CODE1("mime%d = NULL;", submimeno);   /* Avoid freeing in CLEAN. */
    }
    break;

  case TOOLMIME_DATA:
    data = part->data;
    if(!ret) {
      Curl_safefree(escaped);
      escaped = c_escape(data, ZERO_TERMINATED);
      NULL_CHECK(escaped);
      CODE2("curl_mime_data(part%d, \"%s\", CURL_ZERO_TERMINATED);",
                            mimeno, escaped);
    }
    break;

  case TOOLMIME_FILE:
  case TOOLMIME_FILEDATA:
    escaped = c_escape(part->data, ZERO_TERMINATED);
    NULL_CHECK(escaped);
    CODE2("curl_mime_filedata(part%d, \"%s\");", mimeno, escaped);
    if(part->kind == TOOLMIME_FILEDATA && !filename) {
      CODE1("curl_mime_filename(part%d, NULL);", mimeno);
    }
    break;

  case TOOLMIME_STDIN:
    if(!filename)
      filename = "-";
    /* FALLTHROUGH */
  case TOOLMIME_STDINDATA:
    /* Can only be reading stdin in the current context. */
    CODE1("curl_mime_data_cb(part%d, -1, (curl_read_callback) fread, \\",
          mimeno);
    CODE0("                  (curl_seek_callback) fseek, NULL, stdin);");
    break;
  default:
    /* Other cases not possible in this context. */
    break;
  }

  if(!ret && part->encoder) {
    Curl_safefree(escaped);
    escaped = c_escape(part->encoder, ZERO_TERMINATED);
    NULL_CHECK(escaped);
    CODE2("curl_mime_encoder(part%d, \"%s\");", mimeno, escaped);
  }

  if(!ret && filename) {
    Curl_safefree(escaped);
    escaped = c_escape(filename, ZERO_TERMINATED);
    NULL_CHECK(escaped);
    CODE2("curl_mime_filename(part%d, \"%s\");", mimeno, escaped);
  }

  if(!ret && part->name) {
    Curl_safefree(escaped);
    escaped = c_escape(part->name, ZERO_TERMINATED);
    NULL_CHECK(escaped);
    CODE2("curl_mime_name(part%d, \"%s\");", mimeno, escaped);
  }

  if(!ret && part->type) {
    Curl_safefree(escaped);
    escaped = c_escape(part->type, ZERO_TERMINATED);
    NULL_CHECK(escaped);
    CODE2("curl_mime_type(part%d, \"%s\");", mimeno, escaped);
  }

  if(!ret && part->headers) {
    int slistno;

    ret = libcurl_generate_slist(part->headers, &slistno);
    if(!ret) {
      CODE2("curl_mime_headers(part%d, slist%d, 1);", mimeno, slistno);
      CODE1("slist%d = NULL;", slistno); /* Prevent CLEANing. */
    }
  }

nomem:
  Curl_safefree(escaped);
  return ret;
}