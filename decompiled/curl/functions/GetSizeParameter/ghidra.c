undefined8 GetSizeParameter(undefined8 param_1,undefined8 param_2,undefined8 param_3,long *param_4)

{
  char cVar1;
  int iVar2;
  size_t sVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  char *local_40;
  long local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  iVar2 = curlx_strtoofft(param_2,&local_40,0,&local_38);
  if (iVar2 != 0) {
    warnf(param_1,"invalid number specified for %s\n",param_3);
    uVar4 = 4;
    goto LAB_001111b9;
  }
  uVar4 = 0;
  cVar1 = *local_40;
  if (cVar1 == '\0') goto LAB_001111b5;
  sVar3 = strlen(local_40);
  if (sVar3 < 2) {
    switch(cVar1) {
    case 'B':
    case 'b':
      break;
    default:
      goto switchD_00111231_caseD_43;
    case 'G':
    case 'g':
      if (0x1ffffffff < local_38) {
LAB_001112c0:
        uVar4 = 0x11;
        goto LAB_001111b9;
      }
      local_38 = local_38 << 0x1e;
      break;
    case 'K':
    case 'k':
      if (0x1fffffffffffff < local_38) goto LAB_001112c0;
      local_38 = local_38 << 10;
      break;
    case 'M':
    case 'm':
      if (0x7ffffffffff < local_38) goto LAB_001112c0;
      local_38 = local_38 << 0x14;
    }
LAB_001111b5:
    *param_4 = local_38;
  }
  else {
    local_40 = "w";
switchD_00111231_caseD_43:
    warnf(param_1,"unsupported %s unit. Use G, M, K or B!\n",param_3);
    uVar4 = 4;
  }
LAB_001111b9:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}