int64_t GetSizeParameter(int64_t a1, int64_t a2, char * a3, int64_t * a4, int64_t a5) {
    int64_t v1 = __readfsqword(40); // 0x1117a
    int64_t v2 = curlx_strtoofft(); // 0x11194
    int64_t result; // 0x11160
    if ((int32_t)v2 != 0) {
        // 0x11298
        warnf();
        result = 4;
    } else {
        // 0x111a1
        char * v3; // 0x11160
        unsigned char v4 = *v3; // 0x111a9
        if (v4 != 0) {
            // 0x111e0
            if (function_b9d0() < 2) {
                int64_t v5 = (int64_t)v4 + 0xffffffbe; // 0x11218
                if ((char)v5 < 44) {
                    int32_t v6 = *(int32_t *)((4 * v5 & 1020) + (int64_t)&g9); // 0x1122a
                    return (int64_t)v6 + (int64_t)&g9;
                }
            }
            // 0x111f7
            warnf();
            result = 4;
        } else {
            // 0x111b0
            result = v2 & 0xffffffff;
        }
    }
    // 0x111b9
    if (v1 != __readfsqword(40)) {
        // 0x112cb
        return function_bd40();
    }
    // 0x111cd
    return result;
}