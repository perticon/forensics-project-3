int32_t GetSizeParameter(void** rdi, int64_t rsi, void** rdx, int64_t* rcx) {
    void** rdx5;
    void* rsp6;
    void* rax7;
    void** rcx8;
    void** rsi9;
    int32_t eax10;
    void** rdi11;
    int64_t r12_12;
    void** v13;
    int64_t rbx14;
    void** rax15;
    int64_t rbx16;
    int64_t v17;
    void* rax18;
    void** r12_19;
    void** rbp20;
    void** rax21;
    void** rdx22;
    int64_t rax23;
    int64_t rax24;
    void** rax25;
    int64_t v26;
    void** rdi27;
    void** rax28;
    void** rbx29;
    int64_t v30;
    uint32_t eax31;
    void** rax32;
    uint32_t eax33;
    int64_t v34;
    void** rdi35;
    void** rax36;

    *reinterpret_cast<int32_t*>(&rdx5) = 0;
    *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32);
    rax7 = g28;
    rcx8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 16);
    rsi9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 8);
    eax10 = curlx_strtoofft(rsi);
    if (eax10) {
        rdx5 = rdx;
        rsi9 = reinterpret_cast<void**>("invalid number specified for %s\n");
        rdi11 = rdi;
        warnf(rdi11, "invalid number specified for %s\n", rdi11, "invalid number specified for %s\n");
        *reinterpret_cast<int32_t*>(&r12_12) = 4;
    } else {
        rdi11 = v13;
        *reinterpret_cast<int32_t*>(&r12_12) = eax10;
        *reinterpret_cast<uint32_t*>(&rbx14) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi11));
        if (*reinterpret_cast<unsigned char*>(&rbx14)) {
            rax15 = fun_b9d0(rdi11, rsi9, 0, rdi11, rsi9, 0);
            if (reinterpret_cast<unsigned char>(rax15) <= reinterpret_cast<unsigned char>(1)) {
                *reinterpret_cast<uint32_t*>(&rbx14) = *reinterpret_cast<uint32_t*>(&rbx14) - 66;
                if (*reinterpret_cast<unsigned char*>(&rbx14) <= 43) {
                    *reinterpret_cast<uint32_t*>(&rbx16) = *reinterpret_cast<unsigned char*>(&rbx14);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
                    goto *reinterpret_cast<int32_t*>(0x2489c + rbx16 * 4) + 0x2489c;
                }
            }
            rdx5 = rdx;
            rsi9 = reinterpret_cast<void**>("unsupported %s unit. Use G, M, K or B!\n");
            rdi11 = rdi;
            warnf(rdi11, "unsupported %s unit. Use G, M, K or B!\n", rdi11, "unsupported %s unit. Use G, M, K or B!\n");
            *reinterpret_cast<int32_t*>(&r12_12) = 4;
        } else {
            *rcx = v17;
        }
    }
    rax18 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax7) - reinterpret_cast<uint64_t>(g28));
    if (!rax18) {
        return *reinterpret_cast<int32_t*>(&r12_12);
    }
    fun_bd40();
    r12_19 = rdx5;
    rbp20 = rdi11;
    rax21 = fun_b9d0(rdi11, rsi9, rdx5, rdi11, rsi9, rdx5);
    *reinterpret_cast<void***>(rsi9) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_19) = reinterpret_cast<void**>(0);
    if (!rax21) 
        goto addr_112fb_15;
    *reinterpret_cast<int32_t*>(&rdx22) = 7;
    *reinterpret_cast<int32_t*>(&rdx22 + 4) = 0;
    rax23 = fun_ba20(rbp20, "pkcs11:", 7, rcx8);
    if (*reinterpret_cast<int32_t*>(&rax23) || (rax24 = fun_bfe0(rbp20, ":\\", 7, rcx8), rax24 == 0)) {
        rax25 = fun_c010(rbp20, rbp20);
        *reinterpret_cast<void***>(rsi9) = rax25;
        goto v26;
    }
    rdi27 = rax21 + 1;
    rax28 = fun_be30(rdi27, ":\\", 7, rdi27, ":\\", 7);
    rbx29 = rax28;
    if (!rax28) {
        addr_112fb_15:
        goto v30;
    } else {
        *reinterpret_cast<void***>(rsi9) = rax28;
        eax31 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp20));
        while (*reinterpret_cast<signed char*>(&eax31)) {
            rax32 = fun_b8c0(rbp20, ":\\", rdx22, rcx8);
            rdx22 = rax32;
            fun_ba30(rbx29, rbp20, rdx22, rcx8);
            rbp20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp20) + reinterpret_cast<unsigned char>(rax32));
            rbx29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx29) + reinterpret_cast<unsigned char>(rax32));
            eax31 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp20));
            if (*reinterpret_cast<signed char*>(&eax31) == 58) 
                goto addr_113b8_22;
            if (*reinterpret_cast<signed char*>(&eax31) != 92) 
                continue;
            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp20 + 1));
            if (*reinterpret_cast<void***>(&eax33) == 58) {
                *reinterpret_cast<void***>(rbx29) = reinterpret_cast<void**>(58);
            } else {
                *reinterpret_cast<void***>(rbx29) = reinterpret_cast<void**>(92);
                if (*reinterpret_cast<void***>(&eax33) != 92) {
                    if (!*reinterpret_cast<void***>(&eax33)) 
                        goto addr_11400_28;
                    *reinterpret_cast<void***>(rbx29 + 1) = *reinterpret_cast<void***>(&eax33);
                    rbp20 = rbp20 + 2;
                    eax31 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp20));
                    rbx29 = rbx29 + 2;
                    continue;
                }
            }
            eax31 = *reinterpret_cast<unsigned char*>(rbp20 + 2);
            ++rbx29;
            rbp20 = rbp20 + 2;
        }
    }
    addr_113be_31:
    *reinterpret_cast<void***>(rbx29) = reinterpret_cast<void**>(0);
    goto v34;
    addr_113b8_22:
    if (*reinterpret_cast<void***>(rbp20 + 1)) {
        rdi35 = rbp20 + 1;
        rax36 = fun_c010(rdi35, rdi35);
        *reinterpret_cast<void***>(r12_19) = rax36;
        goto addr_113be_31;
    }
    addr_11400_28:
    ++rbx29;
    goto addr_113be_31;
}