void easysrc_free(struct s0** rdi, void** rsi) {
    struct s0** rdi3;
    struct s0** rdi4;
    struct s0** rdi5;
    struct s0** rdi6;
    struct s0** rdi7;

    rdi3 = easysrc_decl;
    slist_wc_free_all(rdi3, rsi);
    rdi4 = easysrc_data;
    easysrc_decl = reinterpret_cast<struct s0**>(0);
    slist_wc_free_all(rdi4, rsi);
    rdi5 = easysrc_code;
    easysrc_data = reinterpret_cast<struct s0**>(0);
    slist_wc_free_all(rdi5, rsi);
    rdi6 = easysrc_toohard;
    easysrc_code = reinterpret_cast<struct s0**>(0);
    slist_wc_free_all(rdi6, rsi);
    rdi7 = easysrc_clean;
    easysrc_toohard = reinterpret_cast<struct s0**>(0);
    slist_wc_free_all(rdi7, rsi);
    easysrc_clean = reinterpret_cast<struct s0**>(0);
    return;
}