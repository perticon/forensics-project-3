void easysrc_free(void)

{
  slist_wc_free_all(easysrc_decl);
  easysrc_decl = 0;
  slist_wc_free_all(easysrc_data);
  easysrc_data = 0;
  slist_wc_free_all(easysrc_code);
  easysrc_code = 0;
  slist_wc_free_all(easysrc_toohard);
  easysrc_toohard = 0;
  slist_wc_free_all(easysrc_clean);
  easysrc_clean = 0;
  return;
}