char * add_file_name_to_url(char *param_1,char *param_2)

{
  char *pcVar1;
  long lVar2;
  char *pcVar3;
  char *__s;
  char *pcVar4;
  long lVar5;
  
  pcVar1 = strstr(param_1,"://");
  lVar2 = curl_easy_init();
  if (lVar2 == 0) {
    return (char *)0x0;
  }
  pcVar3 = pcVar1 + 3;
  if (pcVar1 == (char *)0x0) {
    pcVar3 = param_1;
  }
  pcVar3 = strrchr(pcVar3,0x2f);
  pcVar1 = param_1;
  if (pcVar3 != (char *)0x0) {
    if (pcVar3[1] != '\0') goto LAB_0011c406;
    pcVar3 = pcVar3 + 1;
  }
  __s = strrchr(param_2,0x2f);
  if (__s == (char *)0x0) {
    pcVar4 = strrchr(param_2,0x5c);
    if (pcVar4 != (char *)0x0) goto LAB_0011c3bc;
  }
  else {
    pcVar4 = strrchr(__s,0x5c);
    if (pcVar4 == (char *)0x0) {
      param_2 = __s + 1;
    }
    else {
LAB_0011c3bc:
      param_2 = pcVar4 + 1;
    }
  }
  lVar5 = curl_easy_escape(lVar2,param_2,0);
  if (lVar5 != 0) {
    if (pcVar3 == (char *)0x0) {
      pcVar1 = (char *)curl_maprintf("%s/%s",param_1,lVar5);
    }
    else {
      pcVar1 = (char *)curl_maprintf("%s%s",param_1,lVar5);
    }
    curl_free(lVar5);
    if (pcVar1 != (char *)0x0) {
      free(param_1);
    }
  }
LAB_0011c406:
  curl_easy_cleanup(lVar2);
  return pcVar1;
}