void tool_setopt_enum(undefined8 param_1,long param_2,undefined8 param_3,undefined4 param_4,
                     long *param_5,long param_6)

{
  int iVar1;
  long lVar2;
  char *pcVar3;
  
  iVar1 = curl_easy_setopt(param_1,param_4,param_6);
  if (((param_6 == 0) || (*(long *)(param_2 + 0x38) == 0)) || (iVar1 != 0)) {
    return;
  }
  do {
    lVar2 = *param_5;
    if (lVar2 == 0) {
      pcVar3 = "curl_easy_setopt(hnd, %s, %ldL);";
      lVar2 = param_6;
LAB_0011edc2:
      easysrc_addf(&easysrc_code,pcVar3,param_3,lVar2);
      return;
    }
    if (param_5[1] == param_6) {
      pcVar3 = "curl_easy_setopt(hnd, %s, (long)%s);";
      goto LAB_0011edc2;
    }
    param_5 = param_5 + 2;
  } while( true );
}