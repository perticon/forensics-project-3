int64_t tool2curlparts(int64_t a1, int64_t a2, int64_t a3) {
    // 0x10410
    if (a2 == 0) {
        // 0x10437
        return 0;
    }
    int64_t result = tool2curlparts(a1, *(int64_t *)(a2 + 16), a3); // 0x1042e
    if ((int32_t)result != 0) {
        // 0x10437
        return result;
    }
    // 0x10450
    if (function_b900() == 0) {
        // 0x10437
        return 27;
    }
    if ((int32_t)a2 < 7) {
        int32_t v1 = *(int32_t *)((4 * a2 & 0x3fffffffc) + (int64_t)&g6); // 0x10476
        return (int64_t)v1 + (int64_t)&g6;
    }
    // 0x10480
    if (*(int64_t *)(a2 + 40) != 0) {
        int64_t result2 = function_b8b0(); // 0x104fd
        if ((int32_t)result2 != 0) {
            // 0x10437
            return result2;
        }
    }
    int64_t result3 = function_c000(); // 0x1048c
    if ((int32_t)result3 != 0) {
        // 0x10437
        return result3;
    }
    int64_t result4 = function_ba40(); // 0x1049e
    if ((int32_t)result4 != 0) {
        // 0x10437
        return result4;
    }
    int64_t result5 = function_bee0(); // 0x104ae
    if ((int32_t)result5 != 0) {
        // 0x10437
        return result5;
    }
    // 0x104b7
    return function_bc20();
}