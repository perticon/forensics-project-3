int32_t tool2curlparts(void** rdi, void** rsi, void* rdx) {
    void** rsi4;
    int32_t eax5;
    int64_t rax6;
    int64_t rax7;
    void** rsi8;
    void** rsi9;
    void** rsi10;

    if (!rsi) {
        return 0;
    }
    rsi4 = *reinterpret_cast<void***>(rsi + 16);
    eax5 = tool2curlparts(rdi, rsi4, rdx);
    if (!eax5) {
        rax6 = fun_b900(rdx, rsi4);
        if (!rax6) {
            eax5 = 27;
            goto addr_10437_6;
        } else {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi)) <= reinterpret_cast<unsigned char>(6)) {
                *reinterpret_cast<void***>(&rax7) = *reinterpret_cast<void***>(rsi);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
                goto *reinterpret_cast<int32_t*>(0x23c9c + rax7 * 4) + 0x23c9c;
            }
            if (!*reinterpret_cast<void***>(rsi + 40)) 
                goto addr_10485_10;
        }
    } else {
        addr_10437_6:
        return eax5;
    }
    eax5 = fun_b8b0(rax6, rax6);
    if (eax5) 
        goto addr_1050a_13;
    addr_10485_10:
    rsi8 = *reinterpret_cast<void***>(rsi + 48);
    eax5 = fun_c000(rax6, rsi8);
    if (!eax5 && ((rsi9 = *reinterpret_cast<void***>(rsi + 64), eax5 = fun_ba40(rax6, rsi9), !eax5) && (rsi10 = *reinterpret_cast<void***>(rsi + 56), eax5 = fun_bee0(rax6, rsi10), !eax5))) {
    }
    addr_1050a_13:
    goto addr_10437_6;
}