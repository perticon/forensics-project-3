ulong tool2curlparts(undefined8 param_1,int *param_2,undefined8 param_3)

{
  uint uVar1;
  ulong uVar2;
  long lVar3;
  long lVar4;
  undefined *puVar5;
  bool bVar6;
  
  if (param_2 == (int *)0x0) {
    return 0;
  }
  uVar2 = tool2curlparts(param_1,*(undefined8 *)(param_2 + 4));
  if ((int)uVar2 != 0) {
    return uVar2;
  }
  lVar3 = curl_mime_addpart(param_3);
  if (lVar3 == 0) {
    return 0x1b;
  }
  puVar5 = *(undefined **)(param_2 + 10);
  switch(*param_2) {
  case 1:
    lVar4 = curl_mime_init(param_1);
    if (lVar4 == 0) {
      curl_mime_free(0);
      return 0x1b;
    }
    uVar1 = tool2curlparts(param_1,*(undefined8 *)(param_2 + 0x12),lVar4);
    if ((uVar1 != 0) || (uVar1 = curl_mime_subparts(lVar3,lVar4), uVar1 != 0)) {
      curl_mime_free(lVar4);
      return (ulong)uVar1;
    }
    break;
  case 2:
    uVar2 = curl_mime_data(lVar3,*(undefined8 *)(param_2 + 6),0xffffffffffffffff);
    bVar6 = puVar5 != (undefined *)0x0;
    goto LAB_001104ea;
  case 3:
  case 4:
    uVar2 = curl_mime_filedata(lVar3,*(undefined8 *)(param_2 + 6));
    if ((int)uVar2 != 0) {
      return uVar2;
    }
    if (*param_2 != 4) break;
    if (puVar5 != (undefined *)0x0) goto LAB_001104f7;
    uVar2 = curl_mime_filename(lVar3,0);
    goto LAB_00110502;
  case 5:
    bVar6 = true;
    if (puVar5 == (undefined *)0x0) {
      puVar5 = &DAT_0012413c;
    }
    goto LAB_001105a7;
  case 6:
    bVar6 = puVar5 != (undefined *)0x0;
LAB_001105a7:
    uVar2 = curl_mime_data_cb(lVar3,*(undefined8 *)(param_2 + 0x16),tool_mime_stdin_read,
                              tool_mime_stdin_seek,0,param_2);
LAB_001104ea:
    if ((int)uVar2 != 0) {
      return uVar2;
    }
    if (bVar6) goto LAB_001104f7;
    goto LAB_00110502;
  }
  if (puVar5 != (undefined *)0x0) {
LAB_001104f7:
    uVar2 = curl_mime_filename(lVar3,puVar5);
LAB_00110502:
    if ((int)uVar2 != 0) {
      return uVar2;
    }
  }
  uVar2 = curl_mime_type(lVar3,*(undefined8 *)(param_2 + 0xc));
  if ((((int)uVar2 == 0) &&
      (uVar2 = curl_mime_headers(lVar3,*(undefined8 *)(param_2 + 0x10),0), (int)uVar2 == 0)) &&
     (uVar2 = curl_mime_encoder(lVar3,*(undefined8 *)(param_2 + 0xe)), (int)uVar2 == 0)) {
    uVar2 = (*(code *)PTR_curl_mime_name_00143d60)(lVar3,*(undefined8 *)(param_2 + 8));
    return uVar2;
  }
  return uVar2;
}