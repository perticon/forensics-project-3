int tool2curlmime(undefined8 param_1,long param_2,long *param_3)

{
  int iVar1;
  long lVar2;
  
  lVar2 = curl_mime_init();
  *param_3 = lVar2;
  if (lVar2 == 0) {
    iVar1 = 0x1b;
  }
  else {
    iVar1 = tool2curlparts(param_1,*(undefined8 *)(param_2 + 0x48));
    if (iVar1 == 0) {
      return 0;
    }
    lVar2 = *param_3;
  }
  curl_mime_free(lVar2);
  *param_3 = 0;
  return iVar1;
}