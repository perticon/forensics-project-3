void setfiletime(long param_1,char *param_2,undefined8 param_3)

{
  int iVar1;
  int *piVar2;
  char *pcVar3;
  long in_FS_OFFSET;
  timeval local_48;
  long local_38;
  undefined8 local_30;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (-1 < param_1) {
    local_30 = 0;
    local_48.tv_usec = 0;
    local_48.tv_sec = param_1;
    local_38 = param_1;
    iVar1 = utimes(param_2,&local_48);
    if (iVar1 != 0) {
      piVar2 = __errno_location();
      pcVar3 = strerror(*piVar2);
      warnf(param_3,"Failed to set filetime %ld on \'%s\': %s\n",param_1,param_2,pcVar3);
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}