int fwrite_xattr(undefined8 param_1,int param_2)

{
  bool bVar1;
  char *pcVar2;
  int iVar3;
  int iVar4;
  size_t sVar5;
  int iVar6;
  char *__name;
  long lVar7;
  undefined1 *puVar8;
  long in_FS_OFFSET;
  char *local_50;
  char *local_48;
  long local_40;
  
  lVar7 = 0;
  __name = "user.xdg.referrer.url";
  iVar6 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
LAB_00121724:
  puVar8 = mappings + lVar7 * 0x10;
  do {
    iVar4 = *(int *)(puVar8 + 8);
    local_50 = (char *)0x0;
    iVar6 = iVar6 + 1;
    iVar3 = curl_easy_getinfo(param_1,iVar4,&local_50);
    pcVar2 = local_50;
    if ((iVar3 == 0) && (local_50 != (char *)0x0)) {
      if (iVar4 != 0x100001) {
        sVar5 = strlen(local_50);
        iVar4 = fsetxattr(param_2,__name,pcVar2,sVar5,0);
        goto LAB_0012179d;
      }
      lVar7 = curl_url();
      if (lVar7 == 0) {
LAB_001217dd:
        bVar1 = false;
        curl_url_cleanup(lVar7);
      }
      else {
        iVar4 = curl_url_set(lVar7,0,local_50,0);
        if (iVar4 != 0) goto LAB_001217dd;
        iVar4 = curl_url_set(lVar7,2,0,0);
        if (iVar4 != 0) goto LAB_001217dd;
        iVar4 = curl_url_set(lVar7,3,0,0);
        if (iVar4 != 0) goto LAB_001217dd;
        iVar4 = curl_url_get(lVar7,0,&local_48);
        if (iVar4 != 0) goto LAB_001217dd;
        bVar1 = true;
        curl_url_cleanup(lVar7);
        local_50 = local_48;
      }
      pcVar2 = local_50;
      if (local_50 != (char *)0x0) break;
    }
    __name = *(char **)(puVar8 + 0x10);
    puVar8 = puVar8 + 0x10;
    if (__name == (char *)0x0) {
      iVar4 = 0;
      goto LAB_001217a5;
    }
  } while( true );
  sVar5 = strlen(local_50);
  iVar4 = fsetxattr(param_2,__name,pcVar2,sVar5,0);
  if (bVar1) {
    curl_free(local_50);
  }
LAB_0012179d:
  if (iVar4 != 0) goto LAB_001217a5;
  lVar7 = (long)iVar6;
  __name = *(char **)(mappings + lVar7 * 0x10);
  if (__name == (char *)0x0) {
    iVar4 = 0;
LAB_001217a5:
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return iVar4;
    }
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  goto LAB_00121724;
}