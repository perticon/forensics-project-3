char * param2text(undefined4 param_1)

{
  switch(param_1) {
  default:
    return "unknown error";
  case 1:
    return "is ambiguous";
  case 2:
    return "is unknown";
  case 3:
    return "requires parameter";
  case 4:
    return "is badly used here";
  case 9:
    return "had unsupported trailing garbage";
  case 10:
    return "expected a proper numerical parameter";
  case 0xb:
    return "expected a positive numerical parameter";
  case 0xc:
    return "the installed libcurl version doesn\'t support this";
  case 0xd:
    return "a specified protocol is unsupported by libcurl";
  case 0xe:
    return "out of memory";
  case 0x10:
    return "the given option can\'t be reversed with a --no- prefix";
  case 0x11:
    return "too large number";
  case 0x12:
    return "used \'--no-\' for option that isn\'t a boolean";
  case 0x13:
    return "showing headers and --remote-header-name cannot be combined";
  case 0x14:
    return "--continue-at and --remote-header-name cannot be combined";
  case 0x15:
    return "error encountered when reading a file";
  }
}