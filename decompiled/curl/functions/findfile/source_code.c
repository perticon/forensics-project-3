char *findfile(const char *fname, int dotscore)
{
  int i;
  bool xdg = FALSE;
  DEBUGASSERT(fname && fname[0]);
  DEBUGASSERT((dotscore != 1) || (fname[0] == '.'));

  if(!fname[0])
    return NULL;

  for(i = 0; list[i].env; i++) {
    char *home = curl_getenv(list[i].env);
    if(home) {
      char *path;
      const char *filename = fname;
      if(i == 1 /* XDG_CONFIG_HOME */)
        xdg = TRUE;
      if(!home[0]) {
        curl_free(home);
        continue;
      }
      if(list[i].append) {
        char *c = curl_maprintf("%s%s", home, list[i].append);
        curl_free(home);
        if(!c)
          return NULL;
        home = c;
      }
      if(list[i].withoutdot) {
        if(!dotscore || xdg) {
          /* this is not looking for .curlrc, or the XDG_CONFIG_HOME was
             defined so we skip the extended check */
          curl_free(home);
          continue;
        }
        filename++; /* move past the leading dot */
        dotscore = 0; /* disable it for this check */
      }
      path = checkhome(home, filename, dotscore ? dotscore - 1 : 0);
      curl_free(home);
      if(path)
        return path;
    }
  }
#if defined(HAVE_GETPWUID) && defined(HAVE_GETEUID)
  {
    struct passwd *pw = getpwuid(geteuid());
    if(pw) {
      char *home = pw->pw_dir;
      if(home && home[0])
        return checkhome(home, fname, FALSE);
    }
  }
#endif /* PWD-stuff */
  return NULL;
}