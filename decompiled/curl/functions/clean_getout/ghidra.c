void clean_getout(long param_1)

{
  undefined8 *puVar1;
  undefined8 *__ptr;
  
  if (param_1 == 0) {
    return;
  }
  __ptr = *(undefined8 **)(param_1 + 0x1c8);
  while (__ptr != (undefined8 *)0x0) {
    puVar1 = (undefined8 *)*__ptr;
    free((void *)__ptr[1]);
    free((void *)__ptr[2]);
    free((void *)__ptr[3]);
    free(__ptr);
    __ptr = puVar1;
  }
  *(undefined8 *)(param_1 + 0x1c8) = 0;
  return;
}