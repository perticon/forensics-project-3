void time2str(undefined8 *param_1,long param_2)

{
  if (param_2 < 1) {
    *(undefined *)(param_1 + 1) = 0;
    *param_1 = 0x2d2d3a2d2d3a2d2d;
    return;
  }
  if (359999 < param_2) {
    if (param_2 < 86400000) {
      curl_msnprintf(param_1,9,"%3ldd %02ldh",param_2 / 0x15180,(param_2 % 0x15180) / 0xe10);
      return;
    }
    curl_msnprintf(param_1,9,"%7ldd");
    return;
  }
  curl_msnprintf(param_1,9,"%2ld:%02ld:%02ld",param_2 / 0xe10,(param_2 % 0xe10) / 0x3c,
                 (param_2 % 0xe10) % 0x3c);
  return;
}