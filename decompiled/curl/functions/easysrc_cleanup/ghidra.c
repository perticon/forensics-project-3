undefined8 easysrc_cleanup(void)

{
  long lVar1;
  
  lVar1 = slist_wc_append(easysrc_code,"curl_easy_cleanup(hnd);");
  if (lVar1 != 0) {
    easysrc_code = lVar1;
    lVar1 = slist_wc_append(lVar1,"hnd = NULL;");
    if (lVar1 != 0) {
      easysrc_code = lVar1;
      return 0;
    }
  }
  easysrc_free();
  return 0x1b;
}