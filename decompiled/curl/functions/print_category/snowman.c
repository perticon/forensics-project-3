void print_category(uint32_t edi, void** rsi, void** rdx) {
    void** r13_4;
    uint32_t r15d5;
    void** rdx6;
    void** rdi7;
    int64_t rbp8;
    void** r12_9;
    int64_t rax10;
    struct s1* r14_11;
    void** rax12;
    void** rdi13;
    void** rax14;
    int64_t rbp15;
    int64_t rax16;

    r13_4 = helptext;
    if (!r13_4) {
        addr_15860_2:
        return;
    } else {
        r15d5 = edi;
        *reinterpret_cast<int32_t*>(&rdx6) = 5;
        *reinterpret_cast<int32_t*>(&rdx6 + 4) = 0;
        rdi7 = r13_4;
        *reinterpret_cast<int32_t*>(&rbp8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp8) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r12_9) = 5;
        *reinterpret_cast<int32_t*>(&r12_9 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rax10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        do {
            r14_11 = reinterpret_cast<struct s1*>(0x41460 + (rax10 + rax10 * 2) * 8);
            if (r15d5 & r14_11->f10) {
                rax12 = fun_b9d0(rdi7, rsi, rdx6);
                rdi13 = r14_11->f8;
                if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(rax12)) {
                    r12_9 = rax12;
                }
                rax14 = fun_b9d0(rdi13, rsi, rdx6);
                rdx6 = rdx6;
                if (reinterpret_cast<unsigned char>(rdx6) < reinterpret_cast<unsigned char>(rax14)) {
                    rdx6 = rax14;
                }
            }
            *reinterpret_cast<int32_t*>(&rax10) = static_cast<int32_t>(rbp8 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            rbp8 = rax10;
            rdi7 = *reinterpret_cast<void***>(0x41460 + (rax10 + rax10 * 2) * 8);
        } while (rdi7);
        if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_9) + reinterpret_cast<unsigned char>(rdx6)) <= 80) 
            goto addr_15815_11;
    }
    addr_15815_11:
    *reinterpret_cast<int32_t*>(&rbp15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp15) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rax16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    do {
        if (r15d5 & *reinterpret_cast<uint32_t*>(0x41460 + (rax16 + rax16 * 2) * 8 + 16)) {
            fun_ba80(" %-*s %s\n", " %-*s %s\n");
        }
        *reinterpret_cast<int32_t*>(&rax16) = static_cast<int32_t>(rbp15 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        rbp15 = rax16;
    } while (*reinterpret_cast<void***>(0x41460 + (rax16 + rax16 * 2) * 8));
    goto addr_15860_2;
}