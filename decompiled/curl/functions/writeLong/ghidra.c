undefined8 writeLong(undefined8 param_1,undefined8 *param_2,long param_3,ulong param_4,char param_5)

{
  int iVar1;
  long in_FS_OFFSET;
  ulong local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_28 = 0;
  if (*(int *)((long)param_2 + 0xc) == 0) {
    iVar1 = *(int *)(param_2 + 1);
    if (iVar1 != 0x15) {
      if (iVar1 == 0x2a) {
        param_4 = (ulong)*(uint *)(param_3 + 0x50);
        if (-1 < (int)*(uint *)(param_3 + 0x50)) goto LAB_00120bd2;
      }
      else if (iVar1 == 8) {
LAB_00120bd2:
        local_28 = param_4 & 0xffffffff;
        goto LAB_00120bd4;
      }
LAB_00120b67:
      if (param_5 != '\0') {
        curl_mfprintf(param_1,"\"%s\":null",*param_2);
      }
      goto LAB_00120b70;
    }
    local_28 = *(ulong *)(param_3 + 0x170);
LAB_00120bd4:
    if (param_5 != '\0') goto LAB_00120bf0;
  }
  else {
    iVar1 = curl_easy_getinfo(*(undefined8 *)(param_3 + 0x18),*(int *)((long)param_2 + 0xc),
                              &local_28);
    if (iVar1 != 0) goto LAB_00120b67;
    if (param_5 != '\0') {
LAB_00120bf0:
      curl_mfprintf(param_1,"\"%s\":%ld",*param_2,local_28);
      goto LAB_00120b70;
    }
    if (*(int *)(param_2 + 1) - 0xcU < 2) {
      curl_mfprintf(param_1,"%03ld",local_28);
      goto LAB_00120b70;
    }
  }
  curl_mfprintf(param_1,"%ld",local_28);
LAB_00120b70:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 1;
}