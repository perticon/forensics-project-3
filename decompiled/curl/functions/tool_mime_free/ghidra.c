void tool_mime_free(void *param_1)

{
  if (param_1 != (void *)0x0) {
    if (*(long *)((long)param_1 + 0x48) != 0) {
      tool_mime_free();
    }
    if (*(long *)((long)param_1 + 0x10) != 0) {
      tool_mime_free();
    }
    free(*(void **)((long)param_1 + 0x20));
    *(undefined8 *)((long)param_1 + 0x20) = 0;
    free(*(void **)((long)param_1 + 0x28));
    *(undefined8 *)((long)param_1 + 0x28) = 0;
    free(*(void **)((long)param_1 + 0x30));
    *(undefined8 *)((long)param_1 + 0x30) = 0;
    free(*(void **)((long)param_1 + 0x38));
    *(undefined8 *)((long)param_1 + 0x38) = 0;
    free(*(void **)((long)param_1 + 0x18));
    *(undefined8 *)((long)param_1 + 0x18) = 0;
    curl_slist_free_all(*(undefined8 *)((long)param_1 + 0x40));
    free(param_1);
    return;
  }
  return;
}