undefined8 str2offset(long *param_1,char *param_2)

{
  undefined8 uVar1;
  int *piVar2;
  long lVar3;
  size_t sVar4;
  long in_FS_OFFSET;
  char *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = 0xb;
  if (*param_2 != '-') {
    piVar2 = __errno_location();
    *piVar2 = 0;
    lVar3 = strtol(param_2,&local_28,0);
    *param_1 = lVar3;
    if ((lVar3 + 0x7fffffffffffffffU < 0xfffffffffffffffe) || (*piVar2 != 0x22)) {
      if (local_28 == param_2) {
        uVar1 = 10;
      }
      else {
        sVar4 = strlen(param_2);
        uVar1 = 10;
        if (local_28 == param_2 + sVar4) {
          uVar1 = 0;
        }
      }
    }
    else {
      uVar1 = 0x11;
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}