__time_t tvnow(void)

{
  int iVar1;
  long in_FS_OFFSET;
  timeval local_38;
  timespec local_28;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = clock_gettime(1,&local_28);
  if (iVar1 == 0) {
    local_38.tv_sec = local_28.tv_sec;
    local_38.tv_usec = (__suseconds_t)(int)(local_28.tv_nsec / 1000);
  }
  else {
    gettimeofday(&local_38,(__timezone_ptr_t)0x0);
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return local_38.tv_sec;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}