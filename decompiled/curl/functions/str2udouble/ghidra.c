undefined8 str2udouble(double *param_1,char *param_2,long param_3)

{
  undefined8 uVar1;
  int *piVar2;
  size_t sVar3;
  long in_FS_OFFSET;
  double dVar4;
  char *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = 10;
  if (param_2 != (char *)0x0) {
    piVar2 = __errno_location();
    *piVar2 = 0;
    dVar4 = strtod(param_2,&local_38);
    if ((*piVar2 == 0x22) || ((double)param_3 < dVar4)) {
      uVar1 = 0x11;
    }
    else {
      if (param_2 != local_38) {
        sVar3 = strlen(param_2);
        if (local_38 == param_2 + sVar3) {
          if (dVar4 < 0.0) {
            uVar1 = 0xb;
          }
          else {
            *param_1 = dVar4;
            uVar1 = 0;
          }
          goto LAB_0011cc30;
        }
      }
      uVar1 = 10;
    }
  }
LAB_0011cc30:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}