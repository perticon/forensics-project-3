void tool_version_info(void)

{
  uint uVar1;
  long lVar2;
  int iVar3;
  undefined8 uVar4;
  undefined8 *puVar5;
  uint uVar6;
  long *plVar7;
  size_t __nmemb;
  size_t sVar8;
  long in_FS_OFFSET;
  undefined8 local_128 [31];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar4 = curl_version();
  curl_mprintf("curl 7.84.0-DEV (x86_64-pc-linux-gnu) %s\n",uVar4);
  curl_mprintf("Release-Date: %s\n");
  if (*(long *)(curlinfo + 0x40) != 0) {
    curl_mprintf("Protocols: ");
    plVar7 = *(long **)(curlinfo + 0x40);
    lVar2 = *plVar7;
    while (lVar2 != 0) {
      plVar7 = plVar7 + 1;
      curl_mprintf(&DAT_00123093);
      lVar2 = *plVar7;
    }
    puts("");
  }
  if (*(int *)(curlinfo + 0x20) != 0) {
    __nmemb = 0;
    curl_mprintf("Features:");
    uVar6 = 0x80;
    uVar1 = *(uint *)(curlinfo + 0x20);
    puVar5 = (undefined8 *)feats;
    while( true ) {
      if ((uVar1 & uVar6) != 0) {
        local_128[__nmemb] = *puVar5;
        __nmemb = __nmemb + 1;
      }
      if (puVar5 + 2 == (undefined8 *)&DAT_00141050) break;
      uVar6 = *(uint *)(puVar5 + 3);
      puVar5 = puVar5 + 2;
    }
    sVar8 = 0;
    qsort(local_128,__nmemb,8,featcomp);
    if (__nmemb != 0) {
      do {
        puVar5 = local_128 + sVar8;
        sVar8 = sVar8 + 1;
        curl_mprintf(" %s",*puVar5);
      } while (__nmemb != sVar8);
    }
    puts("");
  }
  iVar3 = strcmp("7.84.0-DEV",*(char **)(curlinfo + 8));
  if (iVar3 == 0) {
    if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
  }
  else if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    curl_mprintf("WARNING: curl and libcurl versions do not match. Functionality may be affected.\n"
                );
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}