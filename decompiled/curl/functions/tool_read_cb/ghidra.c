ssize_t tool_read_cb(void *param_1,long param_2,long param_3,int *param_4)

{
  long lVar1;
  ssize_t sVar2;
  int *piVar3;
  
  sVar2 = read(*param_4,param_1,param_3 * param_2);
  if (sVar2 < 0) {
    piVar3 = __errno_location();
    sVar2 = 0;
    if (*piVar3 == 0xb) {
      lVar1 = *(long *)(param_4 + 2);
      *piVar3 = 0;
      *(undefined *)(lVar1 + 0x30a) = 1;
      return 0x10000001;
    }
  }
  *(undefined *)(*(long *)(param_4 + 2) + 0x30a) = 0;
  return sVar2;
}