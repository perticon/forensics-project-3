undefined8 ftpfilemethod(long param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  
  uVar2 = 3;
  iVar1 = curl_strequal("singlecwd");
  if (iVar1 == 0) {
    uVar2 = 2;
    iVar1 = curl_strequal("nocwd",param_2);
    if (iVar1 == 0) {
      uVar2 = 1;
      iVar1 = curl_strequal("multicwd",param_2);
      if (iVar1 == 0) {
        warnf(*(undefined8 *)(param_1 + 0x4d8),
              "unrecognized ftp file method \'%s\', using default\n",param_2);
      }
    }
  }
  return uVar2;
}