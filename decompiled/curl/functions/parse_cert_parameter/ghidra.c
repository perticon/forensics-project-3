void parse_cert_parameter(char *param_1,char **param_2,char **param_3)

{
  char cVar1;
  int iVar2;
  size_t sVar3;
  char *pcVar4;
  char *pcVar5;
  
  sVar3 = strlen(param_1);
  *param_2 = (char *)0x0;
  *param_3 = (char *)0x0;
  if (sVar3 == 0) {
    return;
  }
  iVar2 = curl_strnequal(param_1,"pkcs11:",7);
  if ((iVar2 != 0) || (pcVar4 = strpbrk(param_1,":\\"), pcVar4 == (char *)0x0)) {
    pcVar4 = strdup(param_1);
    *param_2 = pcVar4;
    return;
  }
  pcVar4 = (char *)malloc(sVar3 + 1);
  if (pcVar4 == (char *)0x0) {
    return;
  }
  *param_2 = pcVar4;
  cVar1 = *param_1;
LAB_0011135b:
  do {
    if (cVar1 == '\0') {
LAB_001113be:
      *pcVar4 = '\0';
      return;
    }
    sVar3 = strcspn(param_1,":\\");
    strncpy(pcVar4,param_1,sVar3);
    param_1 = param_1 + sVar3;
    pcVar4 = pcVar4 + sVar3;
    cVar1 = *param_1;
    if (cVar1 == ':') {
      if (param_1[1] != '\0') {
        pcVar5 = strdup(param_1 + 1);
        *param_3 = pcVar5;
      }
      goto LAB_001113be;
    }
  } while (cVar1 != '\\');
  cVar1 = param_1[1];
  if (cVar1 == ':') {
    *pcVar4 = ':';
  }
  else {
    *pcVar4 = '\\';
    if (cVar1 != '\\') {
      if (cVar1 == '\0') {
        pcVar4 = pcVar4 + 1;
        goto LAB_001113be;
      }
      pcVar4[1] = cVar1;
      param_1 = param_1 + 2;
      cVar1 = *param_1;
      pcVar4 = pcVar4 + 2;
      goto LAB_0011135b;
    }
  }
  cVar1 = param_1[2];
  pcVar4 = pcVar4 + 1;
  param_1 = param_1 + 2;
  goto LAB_0011135b;
}