void parse_cert_parameter(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void** rax7;
    void** rdx8;
    int64_t rax9;
    int64_t rax10;
    void** rax11;
    void** rax12;
    void** rbx13;
    uint32_t eax14;
    void** rax15;
    uint32_t eax16;
    void** rdi17;
    void** rax18;

    r12_5 = rdx;
    rbp6 = rdi;
    rax7 = fun_b9d0(rdi, rsi, rdx);
    *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_5) = reinterpret_cast<void**>(0);
    if (rax7) {
        *reinterpret_cast<int32_t*>(&rdx8) = 7;
        *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
        rax9 = fun_ba20(rbp6, "pkcs11:", 7, rcx);
        if (*reinterpret_cast<int32_t*>(&rax9) || (rax10 = fun_bfe0(rbp6, ":\\", 7, rcx), rax10 == 0)) {
            rax11 = fun_c010(rbp6, rbp6);
            *reinterpret_cast<void***>(rsi) = rax11;
            return;
        }
        rax12 = fun_be30(rax7 + 1, ":\\", 7);
        rbx13 = rax12;
        if (!rax12) 
            goto addr_112fb_5;
    } else {
        addr_112fb_5:
        return;
    }
    *reinterpret_cast<void***>(rsi) = rax12;
    eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6));
    while (*reinterpret_cast<signed char*>(&eax14)) {
        rax15 = fun_b8c0(rbp6, ":\\", rdx8, rcx);
        rdx8 = rax15;
        fun_ba30(rbx13, rbp6, rdx8, rcx);
        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) + reinterpret_cast<unsigned char>(rax15));
        rbx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<unsigned char>(rax15));
        eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6));
        if (*reinterpret_cast<signed char*>(&eax14) == 58) 
            goto addr_113b8_9;
        if (*reinterpret_cast<signed char*>(&eax14) != 92) 
            continue;
        eax16 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 1));
        if (*reinterpret_cast<void***>(&eax16) == 58) {
            *reinterpret_cast<void***>(rbx13) = reinterpret_cast<void**>(58);
        } else {
            *reinterpret_cast<void***>(rbx13) = reinterpret_cast<void**>(92);
            if (*reinterpret_cast<void***>(&eax16) != 92) {
                if (!*reinterpret_cast<void***>(&eax16)) 
                    goto addr_11400_15;
                *reinterpret_cast<void***>(rbx13 + 1) = *reinterpret_cast<void***>(&eax16);
                rbp6 = rbp6 + 2;
                eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6));
                rbx13 = rbx13 + 2;
                continue;
            }
        }
        eax14 = *reinterpret_cast<unsigned char*>(rbp6 + 2);
        ++rbx13;
        rbp6 = rbp6 + 2;
    }
    addr_113be_18:
    *reinterpret_cast<void***>(rbx13) = reinterpret_cast<void**>(0);
    return;
    addr_113b8_9:
    if (*reinterpret_cast<void***>(rbp6 + 1)) {
        rdi17 = rbp6 + 1;
        rax18 = fun_c010(rdi17, rdi17);
        *reinterpret_cast<void***>(r12_5) = rax18;
        goto addr_113be_18;
    }
    addr_11400_15:
    ++rbx13;
    goto addr_113be_18;
}