char * get_param_word(long param_1,char **param_2,char **param_3,char param_4)

{
  char cVar1;
  char *pcVar2;
  bool bVar3;
  char cVar4;
  int iVar5;
  char *pcVar6;
  char *pcVar7;
  char *pcVar8;
  
  pcVar2 = *param_2;
  cVar1 = *pcVar2;
  if (cVar1 == '\"') {
    pcVar8 = pcVar2 + 1;
    cVar4 = pcVar2[1];
    if (cVar4 != '\0') {
      pcVar6 = (char *)0x0;
      pcVar7 = pcVar8;
      do {
        if (cVar4 == '\\') {
          cVar4 = pcVar7[1];
          if ((cVar4 != '\\') && (cVar4 != '\"')) goto LAB_0010f470;
          cVar4 = pcVar7[2];
          if (pcVar6 == (char *)0x0) {
            pcVar6 = pcVar7;
          }
          pcVar7 = pcVar7 + 2;
        }
        else {
          if (cVar4 == '\"') {
            *param_3 = pcVar7;
            pcVar2 = pcVar6;
            if (pcVar6 == (char *)0x0) goto LAB_0010f503;
            goto LAB_0010f4c8;
          }
          cVar4 = pcVar7[1];
LAB_0010f470:
          pcVar7 = pcVar7 + 1;
        }
      } while (cVar4 != '\0');
    }
  }
  pcVar7 = pcVar2;
  if (cVar1 != '\0' && cVar1 != ';') {
    do {
      if (cVar1 == param_4) break;
      cVar1 = pcVar7[1];
      pcVar7 = pcVar7 + 1;
    } while (cVar1 != '\0' && cVar1 != ';');
  }
  *param_3 = pcVar7;
  pcVar8 = pcVar2;
LAB_0010f42e:
  *param_2 = pcVar7;
  return pcVar8;
  while( true ) {
    pcVar6 = pcVar6 + 2;
    *pcVar7 = cVar4;
    pcVar2 = pcVar7 + 1;
    if (*param_3 < pcVar6 || *param_3 == pcVar6) break;
LAB_0010f4c8:
    pcVar7 = pcVar2;
    cVar1 = *pcVar6;
    if ((cVar1 != '\\') || ((cVar4 = pcVar6[1], cVar4 != '\\' && (cVar4 != '\"')))) {
      pcVar6 = pcVar6 + 1;
      *pcVar7 = cVar1;
      pcVar2 = pcVar7 + 1;
      if (*param_3 < pcVar6 || *param_3 == pcVar6) break;
      goto LAB_0010f4c8;
    }
  }
  *param_3 = pcVar7 + 1;
  pcVar7 = pcVar6;
LAB_0010f503:
  cVar1 = pcVar7[1];
  pcVar7 = pcVar7 + 1;
  if ((cVar1 != ';' && cVar1 != '\0') && param_4 != cVar1) {
    bVar3 = false;
    do {
      iVar5 = Curl_isspace();
      if (iVar5 == 0) {
        bVar3 = (cVar1 != ';' && cVar1 != '\0') && param_4 != cVar1;
      }
      cVar4 = pcVar7[1];
      pcVar7 = pcVar7 + 1;
    } while ((cVar4 != '\0' && cVar4 != ';') && (cVar4 != param_4));
    if (bVar3) {
      warnf(*(undefined8 *)(param_1 + 0x4d8),"Trailing data after quoted form parameter\n");
    }
  }
  goto LAB_0010f42e;
}