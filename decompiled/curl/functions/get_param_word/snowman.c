void** get_param_word(void** rdi, void*** rsi, void*** rdx, int32_t ecx) {
    int32_t r8d5;
    int32_t r15d6;
    void*** r13_7;
    void*** rsi8;
    void** rbp9;
    void** r9_10;
    uint32_t edi11;
    void** rax12;
    void** v13;
    void** rdx14;
    void** rax15;
    void** rbx16;
    uint32_t eax17;
    void** rcx18;
    uint32_t edx19;
    uint32_t edi20;
    uint32_t edx21;
    uint32_t edi22;
    uint32_t r14d23;
    uint32_t r14d24;
    uint32_t r12d25;
    int32_t eax26;
    uint32_t edi27;
    void** rdi28;

    r8d5 = ecx;
    r15d6 = ecx;
    r13_7 = rsi;
    rsi8 = rdx;
    rbp9 = rdi;
    r9_10 = *r13_7;
    edi11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9_10));
    if (*reinterpret_cast<signed char*>(&edi11) != 34 || (rax12 = r9_10 + 1, v13 = rax12, rdx14 = rax12, *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9_10 + 1)), *reinterpret_cast<unsigned char*>(&rax15) == 0)) {
        addr_f3eb_2:
        rbx16 = r9_10;
        if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&edi11))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edi11) != 59))) {
            if (*reinterpret_cast<signed char*>(&edi11) != *reinterpret_cast<signed char*>(&r8d5)) {
                do {
                    eax17 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx16 + 1));
                    ++rbx16;
                    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&eax17))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax17) != 59)))) 
                        break;
                } while (*reinterpret_cast<signed char*>(&r15d6) != *reinterpret_cast<signed char*>(&eax17));
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rcx18) = 0;
        *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
        while (1) {
            if (*reinterpret_cast<unsigned char*>(&rax15) != 92) {
                if (*reinterpret_cast<unsigned char*>(&rax15) == 34) 
                    goto addr_f4a0_10;
                *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx14 + 1));
                goto addr_f470_12;
            } else {
                *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx14 + 1));
                if (*reinterpret_cast<unsigned char*>(&rax15) == 92 || *reinterpret_cast<unsigned char*>(&rax15) == 34) {
                    *reinterpret_cast<uint32_t*>(&rax15) = *reinterpret_cast<unsigned char*>(rdx14 + 2);
                    if (!rcx18) {
                        rcx18 = rdx14;
                    }
                    rdx14 = rdx14 + 2;
                } else {
                    addr_f470_12:
                    ++rdx14;
                }
                if (!*reinterpret_cast<unsigned char*>(&rax15)) 
                    goto addr_f3eb_2;
            }
        }
    }
    *rsi8 = rbx16;
    addr_f42e_19:
    *r13_7 = rbx16;
    return r9_10;
    addr_f4a0_10:
    *rsi8 = rdx14;
    if (!rcx18) {
        rcx18 = rdx14;
    } else {
        rax15 = rcx18;
        while (1) {
            edx19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx18));
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&edx19) == 92) || (edi20 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx18 + 1)), *reinterpret_cast<signed char*>(&edi20) != 92) && *reinterpret_cast<signed char*>(&edi20) != 34) {
                ++rax15;
                ++rcx18;
                *reinterpret_cast<void***>(rax15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&edx19);
                if (reinterpret_cast<unsigned char>(*rsi8) <= reinterpret_cast<unsigned char>(rcx18)) 
                    goto addr_f500_24;
            } else {
                edx21 = edi20;
                ++rax15;
                rcx18 = rcx18 + 1 + 1;
                *reinterpret_cast<void***>(rax15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&edx21);
                if (reinterpret_cast<unsigned char>(*rsi8) <= reinterpret_cast<unsigned char>(rcx18)) 
                    goto addr_f4f6_26;
            }
        }
    }
    addr_f503_27:
    edi22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx18 + 1));
    rbx16 = rcx18 + 1;
    *reinterpret_cast<unsigned char*>(&r14d23) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edi22) != 59);
    *reinterpret_cast<unsigned char*>(&rax15) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&edi22));
    r14d24 = r14d23 & *reinterpret_cast<uint32_t*>(&rax15);
    *reinterpret_cast<unsigned char*>(&r14d24) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r14d24) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&r8d5) != *reinterpret_cast<signed char*>(&edi22))));
    if (!*reinterpret_cast<unsigned char*>(&r14d24)) {
        addr_f573_28:
        r9_10 = v13;
        goto addr_f42e_19;
    } else {
        r12d25 = 0;
        do {
            eax26 = Curl_isspace();
            if (!eax26) {
                r12d25 = r14d24;
            }
            edi27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx16 + 1));
            ++rbx16;
        } while (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&edi27))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edi27) != 59)) && *reinterpret_cast<signed char*>(&edi27) != *reinterpret_cast<signed char*>(&r15d6));
        if (!*reinterpret_cast<signed char*>(&r12d25)) 
            goto addr_f573_28;
    }
    rdi28 = *reinterpret_cast<void***>(rbp9 + 0x4d8);
    warnf(rdi28, "Trailing data after quoted form parameter\n");
    goto addr_f573_28;
    addr_f500_24:
    *rsi8 = rax15;
    goto addr_f503_27;
    addr_f4f6_26:
    goto addr_f500_24;
}