ulong output_expected(undefined8 param_1,long param_2)

{
  int iVar1;
  ulong uVar2;
  
  if (param_2 == 0) {
    return 1;
  }
  iVar1 = curl_strnequal(param_1,&DAT_001395e8,7);
  if (iVar1 != 0) {
    return 1;
  }
  uVar2 = curl_strnequal(param_1,"https://",8);
  return uVar2 & 0xffffffffffffff00 | (ulong)((int)uVar2 != 0);
}