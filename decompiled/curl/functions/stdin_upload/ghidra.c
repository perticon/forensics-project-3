uint stdin_upload(char *param_1)

{
  char cVar1;
  uint in_R8D;
  uint uVar2;
  
  cVar1 = *param_1;
  if ((cVar1 == '-') && (in_R8D = 0, param_1[1] == '\0')) {
    return 1;
  }
  if (cVar1 == '.') {
    uVar2 = in_R8D & 0xffffff00 | (uint)(param_1[1] == '\0');
  }
  else {
    uVar2 = in_R8D & 0xffffff00 | (uint)(cVar1 == '.');
  }
  return uVar2;
}