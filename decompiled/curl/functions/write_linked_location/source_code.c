void write_linked_location(CURL *curl, const char *location, size_t loclen,
                           FILE *stream) {
  /* This would so simple if CURLINFO_REDIRECT_URL were available here */
  CURLU *u = NULL;
  char *copyloc = NULL, *locurl = NULL, *scheme = NULL, *finalurl = NULL;
  const char *loc = location;
  size_t llen = loclen;

  /* Strip leading whitespace of the redirect URL */
  while(llen && *loc == ' ') {
    ++loc;
    --llen;
  }

  /* Strip the trailing end-of-line characters, normally "\r\n" */
  while(llen && (loc[llen-1] == '\n' || loc[llen-1] == '\r'))
    --llen;

  /* CURLU makes it easy to handle the relative URL case */
  u = curl_url();
  if(!u)
    goto locout;

  /* Create a NUL-terminated and whitespace-stripped copy of Location: */
  copyloc = malloc(llen + 1);
  if(!copyloc)
    goto locout;
  memcpy(copyloc, loc, llen);
  copyloc[llen] = 0;

  /* The original URL to use as a base for a relative redirect URL */
  if(curl_easy_getinfo(curl, CURLINFO_EFFECTIVE_URL, &locurl))
    goto locout;
  if(curl_url_set(u, CURLUPART_URL, locurl, 0))
    goto locout;

  /* Redirected location. This can be either absolute or relative. */
  if(curl_url_set(u, CURLUPART_URL, copyloc, 0))
    goto locout;

  if(curl_url_get(u, CURLUPART_URL, &finalurl, CURLU_NO_DEFAULT_PORT))
    goto locout;

  if(curl_url_get(u, CURLUPART_SCHEME, &scheme, 0))
    goto locout;

  if(!strcmp("http", scheme) ||
     !strcmp("https", scheme) ||
     !strcmp("ftp", scheme) ||
     !strcmp("ftps", scheme)) {
    fprintf(stream, LINK "%s" LINKST "%.*s" LINKOFF,
            finalurl, loclen, location);
    goto locdone;
  }

  /* Not a "safe" URL: don't linkify it */

locout:
  /* Write the normal output in case of error or unsafe */
  fwrite(location, loclen, 1, stream);

locdone:
  if(u) {
    curl_free(finalurl);
    curl_free(scheme);
    curl_url_cleanup(u);
    free(copyloc);
  }
}