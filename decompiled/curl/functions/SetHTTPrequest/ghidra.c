undefined8 SetHTTPrequest(long param_1,uint param_2,uint *param_3)

{
  uint uVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  char *local_48 [4];
  char *local_28;
  char *local_20;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_48[0] = "";
  local_48[1] = "GET (-G, --get)";
  local_48[2] = "HEAD (-I, --head)";
  local_48[3] = "multipart formpost (-F, --form)";
  local_28 = "POST (-d, --data)";
  local_20 = "PUT (-T, --upload-file)";
  uVar1 = *param_3;
  if ((uVar1 == 0) || (uVar1 == param_2)) {
    *param_3 = param_2;
    uVar2 = 0;
  }
  else {
    warnf(*(undefined8 *)(param_1 + 0x4d8),
          "You can only select one HTTP request method! You asked for both %s and %s.\n",
          local_48[param_2],local_48[uVar1]);
    uVar2 = 1;
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}