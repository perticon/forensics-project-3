bool progress_meter(struct GlobalConfig *global,
                    struct timeval *start,
                    bool final)
{
  static struct timeval stamp;
  static bool header = FALSE;
  struct timeval now;
  long diff;

  if(global->noprogress)
    return FALSE;

  now = tvnow();
  diff = tvdiff(now, stamp);

  if(!header) {
    header = TRUE;
    fputs("DL% UL%  Dled  Uled  Xfers  Live   Qd "
          "Total     Current  Left    Speed\n",
          global->errors);
  }
  if(final || (diff > 500)) {
    char time_left[10];
    char time_total[10];
    char time_spent[10];
    char buffer[3][6];
    curl_off_t spent = tvdiff(now, *start)/1000;
    char dlpercen[4]="--";
    char ulpercen[4]="--";
    struct per_transfer *per;
    curl_off_t all_dlnow = 0;
    curl_off_t all_ulnow = 0;
    bool dlknown = TRUE;
    bool ulknown = TRUE;
    curl_off_t all_running = 0; /* in progress */
    curl_off_t all_queued = 0;  /* pending */
    curl_off_t speed = 0;
    unsigned int i;
    stamp = now;

    /* first add the amounts of the already completed transfers */
    all_dlnow += all_dlalready;
    all_ulnow += all_ulalready;

    for(per = transfers; per; per = per->next) {
      all_dlnow += per->dlnow;
      all_ulnow += per->ulnow;
      if(!per->dltotal)
        dlknown = FALSE;
      else if(!per->dltotal_added) {
        /* only add this amount once */
        all_dltotal += per->dltotal;
        per->dltotal_added = TRUE;
      }
      if(!per->ultotal)
        ulknown = FALSE;
      else if(!per->ultotal_added) {
        /* only add this amount once */
        all_ultotal += per->ultotal;
        per->ultotal_added = TRUE;
      }
      if(!per->added)
        all_queued++;
      else
        all_running++;
    }
    if(dlknown && all_dltotal)
      /* TODO: handle integer overflow */
      msnprintf(dlpercen, sizeof(dlpercen), "%3" CURL_FORMAT_CURL_OFF_T,
                all_dlnow * 100 / all_dltotal);
    if(ulknown && all_ultotal)
      /* TODO: handle integer overflow */
      msnprintf(ulpercen, sizeof(ulpercen), "%3" CURL_FORMAT_CURL_OFF_T,
                all_ulnow * 100 / all_ultotal);

    /* get the transfer speed, the higher of the two */

    i = speedindex;
    speedstore[i].dl = all_dlnow;
    speedstore[i].ul = all_ulnow;
    speedstore[i].stamp = now;
    if(++speedindex >= SPEEDCNT) {
      indexwrapped = TRUE;
      speedindex = 0;
    }

    {
      long deltams;
      curl_off_t dl;
      curl_off_t ul;
      curl_off_t dls;
      curl_off_t uls;
      if(indexwrapped) {
        /* 'speedindex' is the oldest stored data */
        deltams = tvdiff(now, speedstore[speedindex].stamp);
        dl = all_dlnow - speedstore[speedindex].dl;
        ul = all_ulnow - speedstore[speedindex].ul;
      }
      else {
        /* since the beginning */
        deltams = tvdiff(now, *start);
        dl = all_dlnow;
        ul = all_ulnow;
      }
      dls = (curl_off_t)((double)dl / ((double)deltams/1000.0));
      uls = (curl_off_t)((double)ul / ((double)deltams/1000.0));
      speed = dls > uls ? dls : uls;
    }


    if(dlknown && speed) {
      curl_off_t est = all_dltotal / speed;
      curl_off_t left = (all_dltotal - all_dlnow) / speed;
      time2str(time_left, left);
      time2str(time_total, est);
    }
    else {
      time2str(time_left, 0);
      time2str(time_total, 0);
    }
    time2str(time_spent, spent);

    fprintf(global->errors,
            "\r"
            "%-3s " /* percent downloaded */
            "%-3s " /* percent uploaded */
            "%s " /* Dled */
            "%s " /* Uled */
            "%5" CURL_FORMAT_CURL_OFF_T " " /* Xfers */
            "%5" CURL_FORMAT_CURL_OFF_T " " /* Live */
            "%5" CURL_FORMAT_CURL_OFF_T " " /* Queued */
            "%s "  /* Total time */
            "%s "  /* Current time */
            "%s "  /* Time left */
            "%s "  /* Speed */
            "%5s" /* final newline */,

            dlpercen,  /* 3 letters */
            ulpercen,  /* 3 letters */
            max5data(all_dlnow, buffer[0]),
            max5data(all_ulnow, buffer[1]),
            all_xfers,
            all_running,
            all_queued,
            time_total,
            time_spent,
            time_left,
            max5data(speed, buffer[2]), /* speed */
            final ? "\n" :"");
    return TRUE;
  }
  return FALSE;
}