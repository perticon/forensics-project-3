ulong progress_meter(long param_1,undefined8 *param_2,byte param_3)

{
  undefined8 uVar1;
  ulong uVar2;
  long lVar3;
  long *plVar4;
  ulong uVar5;
  long lVar6;
  long lVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  long lVar11;
  undefined *puVar12;
  ulong uVar13;
  long lVar14;
  long lVar15;
  byte bVar16;
  ulong uVar17;
  byte bVar18;
  ulong uVar19;
  byte bVar20;
  long in_FS_OFFSET;
  undefined4 *local_c8;
  long local_b8;
  long local_b0;
  undefined local_78 [6];
  undefined local_72 [6];
  undefined local_6c [6];
  undefined4 local_66;
  undefined4 local_62;
  undefined8 local_5e;
  undefined local_56;
  undefined8 local_54;
  undefined local_4c;
  undefined local_4a [10];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(char *)(param_1 + 5) == '\0') {
    stamp_1 = tvnow();
    uVar8 = SUB168(stamp_1 >> 0x40,0);
    uVar1 = SUB168(stamp_1,0);
    uVar2 = tvdiff(uVar1,uVar8,stamp_1._0_8_,stamp_1._8_8_);
    if (header_0 == '\0') {
      header_0 = '\x01';
      fwrite("DL% UL%  Dled  Uled  Xfers  Live   Qd Total     Current  Left    Speed\n",1,0x47,
             *(FILE **)(param_1 + 8));
    }
    bVar20 = 500 < (long)uVar2 | param_3;
    uVar5 = (ulong)bVar20;
    uVar2 = uVar2 & 0xffffffffffffff00 | uVar5;
    if (bVar20 != 0) {
      lVar3 = tvdiff(uVar1,uVar8,*param_2,param_2[1]);
      local_66 = 0x2d2d;
      local_62 = 0x2d2d;
      lVar14 = all_dlalready;
      lVar15 = all_ulalready;
      if (transfers == (long *)0x0) {
        local_b0 = 0;
        local_b8 = 0;
        bVar18 = bVar20;
LAB_0011e2e5:
        bVar16 = bVar20;
        if (all_dltotal != 0) {
          curl_msnprintf(&local_66,4,&DAT_0013988d,(lVar14 * 100) / all_dltotal);
        }
      }
      else {
        local_b8 = 0;
        uVar13 = 0;
        local_b0 = 0;
        bVar16 = 0;
        plVar4 = transfers;
        lVar11 = all_ultotal;
        lVar7 = all_dltotal;
        uVar17 = uVar5;
        uVar19 = uVar5;
        do {
          lVar14 = lVar14 + plVar4[0x53];
          lVar15 = lVar15 + plVar4[0x55];
          if (plVar4[0x52] == 0) {
            lVar6 = plVar4[0x54];
            uVar17 = 0;
            if (lVar6 == 0) goto LAB_0011df47;
LAB_0011def3:
            if (*(char *)((long)plVar4 + 0x2b1) == '\0') {
              *(undefined *)((long)plVar4 + 0x2b1) = 1;
              lVar11 = lVar11 + lVar6;
              uVar13 = uVar5;
            }
            if (*(char *)((long)plVar4 + 0x279) == '\0') goto LAB_0011df12;
LAB_0011df53:
            local_b8 = local_b8 + 1;
          }
          else {
            if (*(char *)(plVar4 + 0x56) == '\0') {
              *(undefined *)(plVar4 + 0x56) = 1;
              lVar7 = lVar7 + plVar4[0x52];
              bVar16 = bVar20;
            }
            lVar6 = plVar4[0x54];
            if (lVar6 != 0) goto LAB_0011def3;
LAB_0011df47:
            uVar19 = 0;
            if (*(char *)((long)plVar4 + 0x279) != '\0') goto LAB_0011df53;
LAB_0011df12:
            local_b0 = local_b0 + 1;
          }
          bVar18 = (byte)uVar19;
          plVar4 = (long *)*plVar4;
        } while (plVar4 != (long *)0x0);
        if ((char)uVar13 != '\0') {
          all_ultotal = lVar11;
        }
        if (bVar16 != 0) {
          all_dltotal = lVar7;
        }
        bVar16 = (byte)uVar17;
        if ((byte)uVar17 != 0) goto LAB_0011e2e5;
      }
      local_c8 = &local_66;
      if ((bVar18 != 0) && (all_ultotal != 0)) {
        curl_msnprintf(&local_62,4,&DAT_0013988d,(lVar15 * 100) / all_ultotal);
      }
      lVar11 = (ulong)speedindex * 0x20;
      speedindex = speedindex + 1;
      uVar5 = (ulong)speedindex;
      *(long *)(speedstore + lVar11) = lVar14;
      *(long *)(speedstore + lVar11 + 8) = lVar15;
      *(undefined8 *)(speedstore + lVar11 + 0x10) = uVar1;
      *(undefined8 *)(speedstore + lVar11 + 0x18) = uVar8;
      if (speedindex < 10) {
        if (indexwrapped != '\0') goto LAB_0011e23b;
        lVar6 = tvdiff(uVar1,uVar8,*param_2,param_2[1]);
        lVar11 = lVar14;
        lVar7 = lVar15;
      }
      else {
        indexwrapped = '\x01';
        uVar5 = 0;
        speedindex = 0;
LAB_0011e23b:
        lVar6 = tvdiff(uVar1,uVar8,*(undefined8 *)(speedstore + uVar5 * 0x20 + 0x10),
                       *(undefined8 *)(speedstore + uVar5 * 0x20 + 0x18));
        lVar11 = lVar14 - *(long *)(speedstore + (ulong)speedindex * 0x20);
        lVar7 = lVar15 - *(long *)(speedstore + (ulong)speedindex * 0x20 + 8);
      }
      lVar11 = (long)((double)lVar11 / ((double)lVar6 / DAT_001395d8));
      lVar7 = (long)((double)lVar7 / ((double)lVar6 / DAT_001395d8));
      if (lVar11 < lVar7) {
        lVar11 = lVar7;
      }
      if ((lVar11 == 0) || (bVar16 == 0)) {
        local_56 = 0;
        local_5e = 0x2d2d3a2d2d3a2d2d;
        local_54 = 0x2d2d3a2d2d3a2d2d;
        local_4c = 0;
      }
      else {
        lVar7 = all_dltotal / lVar11;
        time2str(&local_5e,(all_dltotal - lVar14) / lVar11,(all_dltotal - lVar14) % lVar11);
        time2str(&local_54,lVar7);
      }
      time2str(local_4a,lVar3 / 1000);
      puVar12 = &DAT_00124100;
      if (param_3 == 0) {
        puVar12 = &DAT_00124101;
      }
      uVar8 = max5data(lVar11,local_6c);
      uVar1 = all_xfers;
      uVar9 = max5data(lVar15,local_72);
      uVar10 = max5data(lVar14,local_78);
      curl_mfprintf(*(undefined8 *)(param_1 + 8),"\r%-3s %-3s %s %s %5ld %5ld %5ld %s %s %s %s %5s",
                    local_c8,&local_62,uVar10,uVar9,uVar1,local_b8,local_b0,&local_54,local_4a,
                    &local_5e,uVar8,puVar12);
      goto LAB_0011ddb1;
    }
  }
  uVar2 = 0;
LAB_0011ddb1:
  stamp_1._8_8_ = SUB168(stamp_1 >> 0x40,0);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2 & 0xffffffff;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}