void progress_finalize(long param_1)

{
  all_dlalready = all_dlalready + *(long *)(param_1 + 0x298);
  all_ulalready = all_ulalready + *(long *)(param_1 + 0x2a8);
  if (*(char *)(param_1 + 0x2b0) == '\0') {
    all_dltotal = all_dltotal + *(long *)(param_1 + 0x290);
    *(undefined *)(param_1 + 0x2b0) = 1;
  }
  if (*(char *)(param_1 + 0x2b1) == '\0') {
    all_ultotal = all_ultotal + *(long *)(param_1 + 0x2a0);
    *(undefined *)(param_1 + 0x2b1) = 1;
  }
  return;
}