static int writeString(FILE *stream, const struct writeoutvar *wovar,
                       struct per_transfer *per, CURLcode per_result,
                       bool use_json)
{
  bool valid = false;
  const char *strinfo = NULL;

  DEBUGASSERT(wovar->writefunc == writeString);

  if(wovar->ci) {
    if(wovar->ci == CURLINFO_HTTP_VERSION) {
      long version = 0;
      if(!curl_easy_getinfo(per->curl, CURLINFO_HTTP_VERSION, &version)) {
        const struct httpmap *m = &http_version[0];
        while(m->str) {
          if(m->num == version) {
            strinfo = m->str;
            valid = true;
            break;
          }
          m++;
        }
      }
    }
    else {
      if(!curl_easy_getinfo(per->curl, wovar->ci, &strinfo) && strinfo)
        valid = true;
    }
  }
  else {
    switch(wovar->id) {
    case VAR_ERRORMSG:
      if(per_result) {
        strinfo = per->errorbuffer[0] ? per->errorbuffer :
                  curl_easy_strerror(per_result);
        valid = true;
      }
      break;
    case VAR_EFFECTIVE_FILENAME:
      if(per->outs.filename) {
        strinfo = per->outs.filename;
        valid = true;
      }
      break;
    case VAR_INPUT_URL:
      if(per->this_url) {
        strinfo = per->this_url;
        valid = true;
      }
      break;
    default:
      DEBUGASSERT(0);
      break;
    }
  }

  if(valid) {
    DEBUGASSERT(strinfo);
    if(use_json) {
      fprintf(stream, "\"%s\":", wovar->name);
      jsonWriteString(stream, strinfo, FALSE);
    }
    else
      fputs(strinfo, stream);
  }
  else {
    if(use_json)
      fprintf(stream, "\"%s\":null", wovar->name);
  }

  return 1; /* return 1 if anything was written */
}