undefined8 create_dir_hierarchy(char *param_1,undefined8 param_2)

{
  int iVar1;
  size_t sVar2;
  char *__s;
  char *__s_00;
  char *pcVar3;
  char *pcVar4;
  size_t sVar5;
  int *piVar6;
  undefined8 uVar7;
  
  sVar2 = strlen(param_1);
  __s = strdup(param_1);
  if (__s == (char *)0x0) {
    return 0x1b;
  }
  __s_00 = (char *)malloc(sVar2 + 1);
  if (__s_00 == (char *)0x0) {
    free(__s);
    return 0x1b;
  }
  *__s_00 = '\0';
  pcVar3 = strtok(__s,"/");
  do {
    do {
      if ((pcVar3 == (char *)0x0) || (pcVar4 = strtok((char *)0x0,"/"), pcVar4 == (char *)0x0)) {
        uVar7 = 0;
        goto LAB_0010e6db;
      }
      sVar5 = strlen(__s_00);
      if (sVar5 == 0) {
        if (pcVar3 == __s) {
          strcpy(__s_00,__s);
        }
        else {
          curl_msnprintf(__s_00,sVar2,"%s%s",&DAT_001236fe,pcVar3);
        }
      }
      else {
        curl_msnprintf(__s_00 + sVar5,sVar2 - sVar5,"%s%s",&DAT_001236fe,pcVar3);
      }
      iVar1 = mkdir(__s_00,0x1e8);
      pcVar3 = pcVar4;
    } while (iVar1 != -1);
    piVar6 = __errno_location();
    iVar1 = *piVar6;
  } while ((iVar1 - 0xdU & 0xfffffffb) == 0);
  if (iVar1 == 0x1e) {
    uVar7 = 0x17;
    curl_mfprintf(param_2,"%s resides on a read-only file system.\n",__s_00);
    goto LAB_0010e6db;
  }
  if (iVar1 < 0x1f) {
    if (iVar1 == 0xd) {
      uVar7 = 0x17;
      curl_mfprintf(param_2,"You don\'t have permission to create %s.\n",__s_00);
      goto LAB_0010e6db;
    }
    if (iVar1 == 0x1c) {
      uVar7 = 0x17;
      curl_mfprintf(param_2,"No space left on the file system that will contain the directory %s.\n"
                    ,__s_00);
      goto LAB_0010e6db;
    }
  }
  else {
    if (iVar1 == 0x24) {
      uVar7 = 0x17;
      curl_mfprintf(param_2,"The directory name %s is too long.\n",__s_00);
      goto LAB_0010e6db;
    }
    if (iVar1 == 0x7a) {
      uVar7 = 0x17;
      curl_mfprintf(param_2,"Cannot create directory %s because you exceeded your quota.\n",__s_00);
      goto LAB_0010e6db;
    }
  }
  uVar7 = 0x17;
  curl_mfprintf(param_2,"Error creating directory %s.\n",__s_00);
LAB_0010e6db:
  free(__s_00);
  free(__s);
  return uVar7;
}