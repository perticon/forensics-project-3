ulong tool_header_cb(char *param_1,size_t param_2,size_t param_3,long param_4)

{
  long *plVar1;
  long lVar2;
  FILE *__s;
  undefined8 uVar3;
  char cVar4;
  int iVar5;
  ulong uVar6;
  ulong uVar7;
  void *pvVar8;
  size_t __n;
  size_t sVar9;
  char *pcVar10;
  long lVar11;
  char *pcVar12;
  char *pcVar13;
  ulong __n_00;
  size_t sVar14;
  long *plVar15;
  long *plVar16;
  long in_FS_OFFSET;
  ulong local_60;
  undefined8 local_58;
  char *local_50;
  undefined8 local_48;
  long local_40;
  
  __n_00 = param_2 * param_3;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = 0;
  uVar6 = (ulong)(param_2 == 0 || param_3 == 0);
  lVar11 = *(long *)(param_4 + 0x10);
  uVar7 = uVar6;
  if (lVar11 == 0) goto LAB_0010cb21;
  if ((*(long *)(lVar11 + 0xc0) != 0) && (*(FILE **)(param_4 + 0xf0) != (FILE *)0x0)) {
    uVar7 = fwrite(param_1,param_2,param_3,*(FILE **)(param_4 + 0xf0));
    if (__n_00 - uVar7 != 0) goto LAB_0010cb21;
    fflush(*(FILE **)(param_4 + 0xf0));
    lVar11 = *(long *)(param_4 + 0x10);
  }
  plVar1 = (long *)(param_1 + __n_00);
  if ((((*(long *)(lVar11 + 0x2c8) != 0) && (*(long *)(param_4 + 0x118) != 0)) &&
      (iVar5 = curl_strnequal(param_1,"etag:",5), iVar5 != 0)) &&
     (*(char *)((long)plVar1 - 1) == '\n')) {
    pcVar13 = (char *)((long)plVar1 - 1);
    for (pcVar12 = param_1 + 5; (iVar5 = Curl_isspace(*pcVar12), iVar5 != 0 && (pcVar12 < pcVar13));
        pcVar12 = pcVar12 + 1) {
    }
    while (iVar5 = Curl_isspace(*pcVar13), iVar5 != 0) {
      pcVar13 = pcVar13 + -1;
    }
    if (pcVar12 <= pcVar13) {
      fwrite(pcVar12,param_2,(size_t)(pcVar13 + (1 - (long)pcVar12)),*(FILE **)(param_4 + 0x118));
      fputc(10,*(FILE **)(param_4 + 0x118));
      fflush(*(FILE **)(param_4 + 0x118));
    }
  }
  lVar11 = param_4 + 0xb8;
  curl_easy_getinfo(*(undefined8 *)(param_4 + 0x18),0x200030,&local_60);
  if (((*(char *)(param_4 + 0x168) != '\0') && (0x14 < __n_00)) &&
     ((iVar5 = curl_strnequal(param_1,"Content-disposition:",0x14), iVar5 != 0 &&
      ((local_60 & 3) != 0)))) {
    cVar4 = param_1[0x14];
    plVar16 = (long *)(param_1 + 0x14);
LAB_0010cd83:
    while (((cVar4 != '\0' && (plVar16 < plVar1)) && (iVar5 = Curl_isalpha(cVar4), iVar5 == 0))) {
      cVar4 = *(char *)((long)plVar16 + 1);
      plVar16 = (long *)((long)plVar16 + 1);
    }
    if ((long *)((long)plVar1 - 9U) < plVar16) goto LAB_0010d1a5;
    if ((*plVar16 != 0x656d616e656c6966) || (*(char *)(plVar16 + 1) != '=')) {
      if (plVar16 < plVar1) {
        do {
          plVar15 = plVar16;
          cVar4 = *(char *)plVar15;
          plVar16 = plVar15;
          if (cVar4 == ';') goto LAB_0010cd83;
          plVar16 = (long *)((long)plVar15 + 1U);
        } while (plVar1 != (long *)((long)plVar15 + 1U));
        cVar4 = *(char *)((long)plVar15 + 1);
        plVar16 = plVar1;
      }
      else {
        cVar4 = *(char *)plVar16;
      }
      goto LAB_0010cd83;
    }
    sVar14 = __n_00 - ((long)(char *)((long)plVar16 + 9) - (long)param_1);
    pcVar12 = (char *)malloc(sVar14 + 1);
    if (pcVar12 != (char *)0x0) {
      memcpy(pcVar12,(char *)((long)plVar16 + 9),sVar14);
      pcVar12[sVar14] = '\0';
      cVar4 = *pcVar12;
      if ((cVar4 == '\'') || (iVar5 = 0x3b, pcVar13 = pcVar12, cVar4 == '\"')) {
        pcVar13 = pcVar12 + 1;
        iVar5 = (int)cVar4;
      }
      pcVar10 = strchr(pcVar13,iVar5);
      if (pcVar10 != (char *)0x0) {
        *pcVar10 = '\0';
      }
      pcVar10 = strrchr(pcVar13,0x2f);
      if (((pcVar10 == (char *)0x0) || (pcVar13 = pcVar10 + 1, pcVar10[1] != '\0')) &&
         ((pcVar10 = strrchr(pcVar13,0x5c), pcVar10 == (char *)0x0 ||
          (pcVar13 = pcVar10 + 1, pcVar10[1] != '\0')))) {
        pcVar10 = strchr(pcVar13,0xd);
        if (pcVar10 != (char *)0x0) {
          *pcVar10 = '\0';
        }
        pcVar10 = strchr(pcVar13,10);
        if (pcVar10 != (char *)0x0) {
          *pcVar10 = '\0';
        }
        if (pcVar12 != pcVar13) {
          sVar14 = strlen(pcVar13);
          memmove(pcVar12,pcVar13,sVar14 + 1);
        }
        if (*(long *)(param_4 + 200) != 0) {
          free(pcVar12);
          uVar7 = uVar6;
          goto LAB_0010cb21;
        }
        *(char **)(param_4 + 0xb8) = pcVar12;
        *(undefined4 *)(param_4 + 0xc0) = 0x10101;
        *(undefined *)(param_4 + 0x168) = 0;
        cVar4 = tool_create_output_file(lVar11,*(undefined8 *)(param_4 + 0x10));
        uVar7 = uVar6;
        if (cVar4 == '\0') goto LAB_0010cb21;
      }
      else {
        free(pcVar12);
      }
    }
LAB_0010d1a5:
    if ((*(long *)(param_4 + 200) == 0) &&
       (cVar4 = tool_create_output_file(lVar11,*(undefined8 *)(param_4 + 0x10)), uVar7 = uVar6,
       cVar4 == '\0')) goto LAB_0010cb21;
  }
  lVar2 = *(long *)(param_4 + 0x148);
  if (*(long *)(lVar2 + 800) != 0) {
    pvVar8 = memchr(param_1,0x3a,__n_00);
    if (pvVar8 == (void *)0x0) {
      if ((*param_1 == '\r') || (*param_1 == '\n')) {
        *(undefined *)(param_4 + 0x178) = 1;
      }
    }
    else if (*(char *)(param_4 + 0x178) == '\0') {
      *(undefined *)(param_4 + 0x178) = 0;
      *(long *)(param_4 + 0x170) = *(long *)(param_4 + 0x170) + 1;
    }
    else {
      *(undefined *)(param_4 + 0x178) = 0;
      *(undefined8 *)(param_4 + 0x170) = 1;
    }
  }
  uVar7 = __n_00;
  if (((*(char *)(lVar2 + 0x1b7) == '\0') || ((local_60 & 0x40403) == 0)) ||
     ((*(long *)(param_4 + 200) == 0 &&
      (cVar4 = tool_create_output_file(lVar11,*(undefined8 *)(param_4 + 0x10)), uVar7 = uVar6,
      cVar4 == '\0')))) goto LAB_0010cb21;
  if (((*(char *)(*(long *)(param_4 + 0x140) + 6) == '\0') ||
      (*(char *)(*(long *)(param_4 + 0x140) + 0x41) == '\0')) ||
     (pvVar8 = memchr(param_1,0x3a,__n_00), pvVar8 == (void *)0x0)) {
    fwrite(param_1,__n_00,1,*(FILE **)(param_4 + 200));
    uVar7 = __n_00;
    goto LAB_0010cb21;
  }
  pcVar12 = (char *)((long)pvVar8 + 1);
  lVar11 = (long)pvVar8 - (long)param_1;
  curl_mfprintf(*(undefined8 *)(param_4 + 200),&DAT_00123129,lVar11,param_1);
  iVar5 = curl_strnequal("Location",param_1,lVar11);
  sVar14 = (__n_00 - 1) - lVar11;
  if (iVar5 == 0) {
    fwrite(pcVar12,sVar14,1,*(FILE **)(param_4 + 200));
    uVar7 = __n_00;
    goto LAB_0010cb21;
  }
  __s = *(FILE **)(param_4 + 200);
  local_58 = 0;
  local_50 = (char *)0x0;
  uVar3 = *(undefined8 *)(param_4 + 0x18);
  local_48 = 0;
  pcVar13 = pcVar12;
  for (__n = sVar14; __n != 0; __n = __n - 1) {
    if (*pcVar13 != ' ') goto LAB_0010ce49;
    pcVar13 = pcVar13 + 1;
  }
  goto LAB_0010d22e;
  while (__n = sVar9, sVar9 != 0) {
LAB_0010ce49:
    sVar9 = __n - 1;
    if ((pcVar13[sVar9] != '\n') && (pcVar13[sVar9] != '\r')) goto LAB_0010ce5e;
  }
LAB_0010d22e:
  __n = 0;
LAB_0010ce5e:
  lVar11 = curl_url();
  if (lVar11 == 0) {
    fwrite(pcVar12,sVar14,1,__s);
    uVar7 = __n_00;
    goto LAB_0010cb21;
  }
  pvVar8 = malloc(__n + 1);
  if (pvVar8 == (void *)0x0) {
LAB_0010cf89:
    fwrite(pcVar12,sVar14,1,__s);
  }
  else {
    memcpy(pvVar8,pcVar13,__n);
    *(undefined *)((long)pvVar8 + __n) = 0;
    iVar5 = curl_easy_getinfo(uVar3,0x100001,&local_58);
    if ((((iVar5 != 0) || (iVar5 = curl_url_set(lVar11,0,local_58,0), iVar5 != 0)) ||
        ((iVar5 = curl_url_set(lVar11,0,pvVar8,0), iVar5 != 0 ||
         ((iVar5 = curl_url_get(lVar11,0,&local_48,2), iVar5 != 0 ||
          (iVar5 = curl_url_get(lVar11,1,&local_50,0), pcVar13 = local_50, iVar5 != 0)))))) ||
       ((iVar5 = strcmp("http",local_50), iVar5 != 0 &&
        (((iVar5 = strcmp("https",pcVar13), iVar5 != 0 &&
          (iVar5 = strcmp("ftp",pcVar13), iVar5 != 0)) &&
         (iVar5 = strcmp("ftps",pcVar13), iVar5 != 0)))))) goto LAB_0010cf89;
    curl_mfprintf(__s,&DAT_00123150,local_48,sVar14,pcVar12);
  }
  curl_free(local_48);
  curl_free(local_50);
  curl_url_cleanup(lVar11);
  free(pvVar8);
  uVar7 = __n_00;
LAB_0010cb21:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}