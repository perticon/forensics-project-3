void single_transfer_cleanup(long param_1)

{
  if (param_1 != 0) {
    if (*(long *)(param_1 + 0x500) != 0) {
      glob_cleanup();
      *(undefined8 *)(param_1 + 0x500) = 0;
    }
    free(*(void **)(param_1 + 0x508));
    *(undefined8 *)(param_1 + 0x508) = 0;
    free(*(void **)(param_1 + 0x510));
    *(undefined8 *)(param_1 + 0x510) = 0;
    free(*(void **)(param_1 + 0x518));
    *(undefined8 *)(param_1 + 0x518) = 0;
    if (*(long *)(param_1 + 0x4f8) != 0) {
      glob_cleanup();
      *(undefined8 *)(param_1 + 0x4f8) = 0;
    }
    return;
  }
  return;
}