void single_transfer_cleanup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rdi6;
    void** rdi7;
    void** rdi8;
    void** rdi9;
    void** rdi10;

    if (!rdi) {
        return;
    } else {
        rdi6 = *reinterpret_cast<void***>(rdi + 0x500);
        if (rdi6) {
            glob_cleanup(rdi6);
            *reinterpret_cast<void***>(rdi + 0x500) = reinterpret_cast<void**>(0);
        }
        rdi7 = *reinterpret_cast<void***>(rdi + 0x508);
        fun_bde0(rdi7);
        rdi8 = *reinterpret_cast<void***>(rdi + 0x510);
        *reinterpret_cast<void***>(rdi + 0x508) = reinterpret_cast<void**>(0);
        fun_bde0(rdi8);
        rdi9 = *reinterpret_cast<void***>(rdi + 0x518);
        *reinterpret_cast<void***>(rdi + 0x510) = reinterpret_cast<void**>(0);
        fun_bde0(rdi9);
        rdi10 = *reinterpret_cast<void***>(rdi + 0x4f8);
        *reinterpret_cast<void***>(rdi + 0x518) = reinterpret_cast<void**>(0);
        if (rdi10) {
            glob_cleanup(rdi10);
            *reinterpret_cast<void***>(rdi + 0x4f8) = reinterpret_cast<void**>(0);
        }
        return;
    }
}