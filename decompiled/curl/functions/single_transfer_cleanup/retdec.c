int64_t single_transfer_cleanup(int64_t a1) {
    if (a1 == 0) {
        // 0x1675a
        int64_t result; // 0x166d0
        return result;
    }
    int64_t * v1 = (int64_t *)(a1 + (int64_t)&g210); // 0x166dd
    if (*v1 != 0) {
        // 0x166e9
        glob_cleanup();
        *v1 = 0;
    }
    // 0x166f9
    function_bde0();
    *(int64_t *)(a1 + (int64_t)&g211) = 0;
    function_bde0();
    *(int64_t *)(a1 + (int64_t)&g212) = 0;
    int64_t v2 = function_bde0(); // 0x1672e
    int64_t * v3 = (int64_t *)(a1 + (int64_t)&g209); // 0x16733
    *(int64_t *)(a1 + (int64_t)&g213) = 0;
    int64_t result2 = v2; // 0x16748
    if (*v3 != 0) {
        // 0x1674a
        result2 = glob_cleanup();
        *v3 = 0;
    }
    // 0x1675a
    return result2;
}