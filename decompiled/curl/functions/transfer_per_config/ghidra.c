char ** transfer_per_config(int *param_1,char **param_2,undefined8 param_3,char *param_4)

{
  undefined uVar1;
  undefined2 uVar2;
  byte *pbVar3;
  undefined8 *puVar4;
  bool bVar5;
  char cVar6;
  byte bVar7;
  uint uVar8;
  int iVar9;
  FILE *pFVar10;
  void *__ptr;
  char **ppcVar11;
  char **ppcVar12;
  char *pcVar13;
  char *pcVar14;
  long lVar15;
  ulong uVar16;
  char **ppcVar17;
  int *piVar18;
  char **ppcVar19;
  undefined8 extraout_RDX;
  char **extraout_RDX_00;
  char **extraout_RDX_01;
  char **extraout_RDX_02;
  char **extraout_RDX_03;
  char **extraout_RDX_04;
  char **extraout_RDX_05;
  char **extraout_RDX_06;
  char **extraout_RDX_07;
  char **extraout_RDX_08;
  char **extraout_RDX_09;
  char **extraout_RDX_10;
  char **extraout_RDX_11;
  char **extraout_RDX_12;
  char **extraout_RDX_13;
  char **extraout_RDX_14;
  char **extraout_RDX_15;
  char **extraout_RDX_16;
  char **extraout_RDX_17;
  char **extraout_RDX_18;
  char **extraout_RDX_19;
  char **extraout_RDX_20;
  char **extraout_RDX_21;
  char **extraout_RDX_22;
  char **extraout_RDX_23;
  char **extraout_RDX_24;
  char **extraout_RDX_25;
  char **extraout_RDX_26;
  char **extraout_RDX_27;
  char **extraout_RDX_28;
  char **extraout_RDX_29;
  char **extraout_RDX_30;
  char **extraout_RDX_31;
  char **extraout_RDX_32;
  char **extraout_RDX_33;
  char *pcVar20;
  char *pcVar21;
  char **ppcVar22;
  undefined8 uVar23;
  char **ppcVar24;
  char **ppcVar25;
  char **ppcVar26;
  undefined8 *puVar27;
  char **ppcVar28;
  long in_FS_OFFSET;
  bool bVar29;
  undefined auVar30 [16];
  char **ppcVar31;
  undefined *puVar32;
  undefined8 uVar33;
  char **ppcVar34;
  ulong local_160;
  char *local_158;
  char *local_130;
  char *local_128 [4];
  undefined local_108 [8];
  undefined8 uStack256;
  undefined local_f8 [8];
  undefined8 uStack240;
  char *local_e8;
  stat local_d8;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  pcVar21 = param_2[0x39];
  *param_4 = '\0';
  if ((pcVar21 == (char *)0x0) || (*(long *)(pcVar21 + 8) == 0)) {
    ppcVar28 = (char **)0x2;
    helpf(*(undefined8 *)(param_1 + 2),"no URL specified!\n");
    goto LAB_0011765e;
  }
  bVar5 = false;
  if (((param_2[0x47] == (char *)0x0) && (param_2[0x49] == (char *)0x0)) &&
     ((*(char *)((long)param_2 + 0x30d) == '\0' ||
      ((param_2[0x3e] != (char *)0x0 && (*(char *)((long)param_2 + 0x30e) == '\0')))))) {
    uVar33 = curl_easy_init();
    _local_108 = ZEXT816(uStack256) << 0x40;
    uVar8 = curl_easy_getinfo(uVar33,0x40002d,local_108);
    ppcVar28 = (char **)(ulong)uVar8;
    if (uVar8 != 0) goto LAB_0011765e;
    if (*local_108 == 8) {
LAB_00119360:
      bVar5 = false;
    }
    else {
      pcVar21 = (char *)curl_getenv("CURL_CA_BUNDLE");
      if (pcVar21 == (char *)0x0) {
        pcVar21 = (char *)curl_getenv("SSL_CERT_DIR");
        if (pcVar21 == (char *)0x0) {
          pcVar21 = (char *)curl_getenv("SSL_CERT_FILE");
          if (pcVar21 == (char *)0x0) goto LAB_00119360;
          goto LAB_00117778;
        }
        pcVar13 = strdup(pcVar21);
        param_2[0x49] = pcVar13;
        if (pcVar13 == (char *)0x0) {
          ppcVar28 = (char **)0x1b;
          curl_free(pcVar21);
          helpf(*(undefined8 *)(param_1 + 2),"out of memory\n");
          goto LAB_0011765e;
        }
        bVar5 = true;
      }
      else {
LAB_00117778:
        pcVar13 = strdup(pcVar21);
        param_2[0x47] = pcVar13;
        if (pcVar13 == (char *)0x0) {
          ppcVar28 = (char **)0x1b;
          curl_free(pcVar21);
          errorf(param_1,"out of memory\n");
          goto LAB_0011765e;
        }
        bVar5 = false;
      }
      curl_free(pcVar21);
    }
    curl_easy_cleanup(uVar33);
  }
  uVar1 = *(undefined *)((long)param_1 + 5);
  pcVar21 = param_2[0x10];
  uVar2 = *(undefined2 *)((long)param_1 + 5);
  local_158 = param_2[0xa2];
  *param_4 = '\0';
  if (pcVar21 == (char *)0x0) {
LAB_00117429:
    puVar27 = (undefined8 *)param_2[0x9e];
    if (puVar27 == (undefined8 *)0x0) {
      puVar27 = (undefined8 *)param_2[0x39];
      param_2[0xa4] = (char *)0x1;
      param_2[0x9e] = (char *)puVar27;
      goto joined_r0x00118f8f;
    }
    do {
      pcVar21 = param_2[0x9f];
      pcVar13 = (char *)puVar27[2];
      local_130 = pcVar21;
      if (puVar27[1] != 0) {
        if ((pcVar13 != (char *)0x0) && (param_2[0xa1] == (char *)0x0)) {
          pcVar13 = strdup(pcVar13);
          param_2[0xa1] = pcVar13;
          if (pcVar13 != (char *)0x0) goto LAB_00117471;
LAB_001176db:
          pcVar21 = "out of memory\n";
LAB_00118e63:
          errorf(param_1,pcVar21);
          goto LAB_00117634;
        }
LAB_00117471:
        pcVar13 = (char *)puVar27[3];
        if (((*(char *)((long)param_2 + 0x30b) == '\0') && (pcVar13 != (char *)0x0)) &&
           (pcVar21 == (char *)0x0)) {
          uVar33 = 0;
          if (*param_1 != 0) {
            uVar33 = *(undefined8 *)(param_1 + 2);
          }
          uVar8 = glob_url(&local_130,pcVar13,param_2 + 0xa4,uVar33);
          if (uVar8 != 0) {
LAB_00118fcf:
            ppcVar28 = (char **)(ulong)uVar8;
            goto LAB_00118f52;
          }
          param_2[0x9f] = local_130;
          pcVar20 = param_2[0xa5];
LAB_001174dd:
          if (param_2[0xa3] != (char *)0x0) goto LAB_001174ec;
          pcVar21 = local_130;
          if (local_130 == (char *)0x0) {
            if (pcVar20 == (char *)0x0) goto LAB_001176c2;
          }
          else {
LAB_00118fae:
            uVar8 = glob_next_url(param_2 + 0xa3,pcVar21);
            if (uVar8 == 0x1b) {
              ppcVar28 = (char **)0x1b;
              errorf(param_1,"out of memory\n");
              goto LAB_00118f52;
            }
            if (uVar8 != 0) goto LAB_00118fcf;
          }
        }
        else {
          pcVar20 = param_2[0xa5];
          if (pcVar20 != (char *)0x0) goto LAB_001174dd;
          if ((pcVar13 != (char *)0x0) && (param_2[0xa3] == (char *)0x0)) {
            if (pcVar21 != (char *)0x0) goto LAB_00118fae;
LAB_001176c2:
            pcVar21 = strdup(pcVar13);
            param_2[0xa3] = pcVar21;
            if (pcVar21 == (char *)0x0) goto LAB_001176db;
          }
        }
LAB_001174ec:
        pcVar21 = param_2[0xa6];
        if (pcVar21 == (char *)0x0) {
          if (*(char *)((long)param_2 + 0x30b) == '\0') {
            uVar33 = 0;
            if (*param_1 != 0) {
              uVar33 = *(undefined8 *)(param_1 + 2);
            }
            uVar8 = glob_url(param_2 + 0xa0,puVar27[1],param_2 + 0xa6,uVar33);
            if (uVar8 != 0) goto LAB_00118fcf;
            pcVar21 = param_2[0xa6];
          }
          else {
            pcVar21 = (char *)0x1;
          }
        }
        if (param_2[0xa4] <= param_2[0xa5]) {
          free((void *)puVar27[2]);
          puVar27[2] = 0;
          free((void *)puVar27[3]);
          puVar27[3] = 0;
          pcVar21 = param_2[0xa0];
          *(undefined4 *)(puVar27 + 4) = 0;
          glob_cleanup(pcVar21);
          param_2[0xa0] = (char *)0x0;
          param_2[0xa6] = (char *)0x0;
          free(param_2[0xa1]);
          param_2[0xa1] = (char *)0x0;
          free(param_2[0xa3]);
          param_2[0xa3] = (char *)0x0;
          if (param_2[0x9f] != (char *)0x0) {
            glob_cleanup();
            param_2[0x9f] = (char *)0x0;
          }
          puVar27 = (undefined8 *)*puVar27;
          param_2[0xa5] = (char *)0x0;
          param_2[0x9e] = (char *)puVar27;
          goto joined_r0x00118f8f;
        }
        local_e8 = (char *)0x0;
        _local_f8 = ZEXT816(stdout);
        _local_108 = (undefined  [16])0x0;
        if (param_2[0x5a] != (char *)0x0) {
          local_128[0] = (char *)0x0;
          pFVar10 = fopen(param_2[0x5a],"r");
          if ((pFVar10 == (FILE *)0x0) && (param_2[0x59] == (char *)0x0)) {
            ppcVar28 = (char **)0x1a;
            errorf(param_1,"Failed to open %s\n",param_2[0x5a]);
            free(param_2[0xa1]);
            param_2[0xa1] = (char *)0x0;
            goto LAB_00117653;
          }
          iVar9 = file2string(local_128,pFVar10);
          if ((iVar9 == 0) && (local_128[0] != (char *)0x0)) {
            __ptr = (void *)curl_maprintf("If-None-Match: %s");
            free(local_128[0]);
            local_128[0] = (char *)0x0;
          }
          else {
            __ptr = (void *)curl_maprintf("If-None-Match: \"\"");
          }
          if (__ptr == (void *)0x0) {
            if (pFVar10 != (FILE *)0x0) {
              fclose(pFVar10);
            }
            pcVar21 = "Failed to allocate memory for custom etag header\n";
            goto LAB_00118e63;
          }
          iVar9 = add2list(param_2 + 0x6f,__ptr);
          free(__ptr);
          if (pFVar10 != (FILE *)0x0) {
            fclose(pFVar10);
          }
          if (iVar9 != 0) goto LAB_00117634;
        }
        pcVar13 = param_2[0x59];
        if ((pcVar13 != (char *)0x0) && ((*pcVar13 != '-' || (pcVar13[1] != '\0')))) {
          pFVar10 = fopen(pcVar13,"wb");
          auVar30 = _local_108;
          if (pFVar10 == (FILE *)0x0) {
            ppcVar28 = (char **)0x0;
            warnf(param_1,"Failed creating file for saving etags: \"%s\". Skip this transfer\n");
            free(param_2[0xa1]);
            param_2[0xa1] = (char *)0x0;
            glob_cleanup(param_2[0xa0]);
            goto LAB_0011765e;
          }
          _local_108 = CONCAT88(uStack256,param_2[0x59]);
          uStack256._4_4_ = SUB164(auVar30,0xc);
          _local_f8 = CONCAT88(uStack240,pFVar10);
        }
        ppcVar11 = (char **)curl_easy_init();
        if ((ppcVar11 == (char **)0x0) ||
           (ppcVar12 = (char **)calloc(0x2c0,1), ppcVar12 == (char **)0x0)) {
          ppcVar28 = (char **)0x1b;
          curl_easy_cleanup(ppcVar11);
          if (uStack256._3_1_ != '\0') {
            fclose(local_f8);
          }
          goto LAB_00118f52;
        }
        ppcVar28 = ppcVar12;
        if (transfers != (char **)0x0) {
          *transfersl = (char *)ppcVar12;
          ppcVar12[1] = (char *)transfersl;
          ppcVar28 = transfers;
        }
        transfers = ppcVar28;
        pcVar13 = param_2[0xa3];
        all_xfers = all_xfers + 1;
        transfersl = ppcVar12;
        ppcVar12[0x25] = local_e8;
        *(undefined4 *)(ppcVar12 + 0x21) = local_108._0_4_;
        *(undefined4 *)((long)ppcVar12 + 0x10c) = local_108._4_4_;
        *(undefined4 *)(ppcVar12 + 0x22) = (undefined4)uStack256;
        *(undefined4 *)((long)ppcVar12 + 0x114) = uStack256._4_4_;
        *(undefined4 *)(ppcVar12 + 0x23) = local_f8._0_4_;
        *(undefined4 *)((long)ppcVar12 + 0x11c) = local_f8._4_4_;
        *(undefined4 *)(ppcVar12 + 0x24) = (undefined4)uStack240;
        *(undefined4 *)((long)ppcVar12 + 0x124) = uStack240._4_4_;
        if (pcVar13 != (char *)0x0) {
          pcVar13 = strdup(pcVar13);
          ppcVar12[0x57] = pcVar13;
          if (pcVar13 == (char *)0x0) {
            ppcVar28 = (char **)0x1b;
            curl_easy_cleanup(ppcVar11);
            free(param_2[0xa1]);
            param_2[0xa1] = (char *)0x0;
            goto LAB_00117653;
          }
          iVar9 = SetHTTPrequest(param_2,5,param_2 + 0x77);
          if (iVar9 != 0) {
            ppcVar28 = (char **)0x2;
            free(ppcVar12[0x57]);
            ppcVar12[0x57] = (char *)0x0;
            curl_easy_cleanup(ppcVar11);
            free(param_2[0xa1]);
            param_2[0xa1] = (char *)0x0;
            goto LAB_00117653;
          }
        }
        pcVar13 = param_2[0x18];
        *param_4 = '\x01';
        ppcVar12[2] = (char *)param_2;
        ppcVar12[3] = (char *)ppcVar11;
        *(undefined4 *)(ppcVar12 + 10) = *(undefined4 *)((long)puVar27 + 0x24);
        pcVar20 = stdout;
        ppcVar12[0x1e] = stdout;
        if ((pcVar13 != (char *)0x0) && ((*pcVar13 != '-' || (pcVar13[1] != '\0')))) {
          pcVar20 = "wb";
          if (ppcVar12[1] != (char *)0x0) {
            pcVar20 = "ab";
          }
          pFVar10 = fopen(pcVar13,pcVar20);
          if (pFVar10 == (FILE *)0x0) {
            ppcVar28 = (char **)0x17;
            warnf(param_1,"Failed to open %s\n");
            free(param_2[0xa1]);
            param_2[0xa1] = (char *)0x0;
            goto LAB_00117653;
          }
          ppcVar12[0x1c] = param_2[0x18];
          *(undefined2 *)((long)ppcVar12 + 0xea) = 0x101;
          ppcVar12[0x1e] = (char *)pFVar10;
          pcVar20 = stdout;
        }
        pcVar13 = param_2[0xa0];
        ppcVar12[0xb] = (char *)0x0;
        *(undefined *)(ppcVar12 + 0xc) = 0;
        *(undefined4 *)((long)ppcVar12 + 100) = 0;
        ppcVar12[0x19] = pcVar20;
        if (pcVar13 == (char *)0x0) {
          if (param_2[0xa7] != (char *)0x0) {
            ppcVar12[9] = (char *)0x0;
            break;
          }
          pcVar13 = strdup((char *)puVar27[1]);
          ppcVar12[9] = pcVar13;
          if (pcVar13 == (char *)0x0) goto LAB_00117634;
        }
        else {
          uVar8 = glob_next_url(ppcVar12 + 9);
          ppcVar28 = (char **)(ulong)uVar8;
          if (uVar8 != 0) goto LAB_00118f52;
          if (ppcVar12[9] == (char *)0x0) break;
        }
        pcVar13 = param_2[0xa1];
        if (pcVar13 == (char *)0x0) {
          pcVar20 = ppcVar12[0xb];
          if ((*(byte *)(puVar27 + 4) & 4) != 0) {
            if (pcVar20 != (char *)0x0) goto LAB_00119200;
            uVar8 = get_url_file_name(ppcVar12 + 0xb);
            ppcVar28 = (char **)(ulong)uVar8;
            if (uVar8 != 0) {
              errorf(param_1,
                     "Failed to extract a sensible file name from the URL to use for storage!\n");
              goto LAB_00118f52;
            }
            pcVar13 = ppcVar12[0xb];
            if ((*pcVar13 != '\0') || (*(char *)(param_2 + 0x8c) != '\0')) goto LAB_00119246;
            ppcVar28 = (char **)0x17;
            errorf(param_1,"Remote file name has no length!\n");
            free(param_2[0xa1]);
            param_2[0xa1] = (char *)0x0;
            goto LAB_00117653;
          }
          if (pcVar20 != (char *)0x0) goto LAB_001179d2;
        }
        else {
          pcVar20 = strdup(pcVar13);
          ppcVar12[0xb] = pcVar20;
          if (pcVar20 == (char *)0x0) {
            ppcVar28 = (char **)0x1b;
            free(pcVar13);
            param_2[0xa1] = (char *)0x0;
            goto LAB_00117653;
          }
          if ((*(byte *)(puVar27 + 4) & 4) == 0) {
LAB_001179d2:
            if ((*pcVar20 == '-') && (pcVar20[1] == '\0')) goto LAB_001179e7;
          }
LAB_00119200:
          if (param_2[0xa0] == (char *)0x0) {
            pcVar13 = ppcVar12[0xb];
          }
          else {
            uVar8 = glob_match_url(ppcVar12 + 0xb,pcVar20);
            ppcVar28 = (char **)(ulong)uVar8;
            free(pcVar20);
            if (uVar8 != 0) {
              warnf(param_1,"bad output glob!\n");
              goto LAB_00118f52;
            }
            pcVar13 = ppcVar12[0xb];
            if (*pcVar13 == '\0') {
              ppcVar28 = (char **)0x17;
              warnf(param_1,"output glob produces empty string!\n");
              free(param_2[0xa1]);
              param_2[0xa1] = (char *)0x0;
              goto LAB_00117653;
            }
          }
LAB_00119246:
          pcVar20 = param_2[0x17];
          if ((pcVar20 != (char *)0x0) && (*pcVar20 != '\0')) {
            pcVar13 = (char *)curl_maprintf("%s/%s",pcVar20,pcVar13);
            if (pcVar13 == (char *)0x0) {
              ppcVar28 = (char **)0x17;
              free(param_2[0xa1]);
              param_2[0xa1] = (char *)0x0;
              goto LAB_00117653;
            }
            free(ppcVar12[0xb]);
            ppcVar12[0xb] = pcVar13;
          }
          if (*(char *)((long)param_2 + 0x313) != '\0') {
            uVar8 = create_dir_hierarchy(pcVar13);
            ppcVar28 = (char **)(ulong)uVar8;
            if (uVar8 != 0) goto LAB_00118f52;
            pcVar13 = ppcVar12[0xb];
          }
          if (*(char *)((long)param_2 + 0x49) == '\0') {
            local_d8.st_size = (__off_t)param_2[0xf];
LAB_0011927c:
            if ((char *)local_d8.st_size == (char *)0x0) goto LAB_00119285;
            pFVar10 = fopen(pcVar13,"ab");
            if (pFVar10 == (FILE *)0x0) {
              ppcVar28 = (char **)0x17;
              errorf(param_1,"Can\'t open \'%s\'!\n",ppcVar12[0xb]);
              free(param_2[0xa1]);
              param_2[0xa1] = (char *)0x0;
              goto LAB_00117653;
            }
            ppcVar12[0x19] = (char *)pFVar10;
            pcVar20 = param_2[0xf];
            *(undefined *)((long)ppcVar12 + 0xc3) = 1;
            pcVar13 = ppcVar12[0xb];
            ppcVar12[0x1b] = pcVar20;
          }
          else {
            iVar9 = stat(pcVar13,&local_d8);
            if (iVar9 == 0) {
              pcVar13 = ppcVar12[0xb];
              param_2[0xf] = (char *)local_d8.st_size;
              goto LAB_0011927c;
            }
            param_2[0xf] = (char *)0x0;
            pcVar13 = ppcVar12[0xb];
LAB_00119285:
            ppcVar12[0x19] = (char *)0x0;
          }
          ppcVar12[0x17] = pcVar13;
          *(undefined *)((long)ppcVar12 + 0xc2) = 1;
        }
LAB_001179e7:
        if (ppcVar12[0x57] != (char *)0x0) {
          cVar6 = stdin_upload();
          if (cVar6 == '\0') {
            pcVar13 = (char *)add_file_name_to_url(ppcVar12[9],ppcVar12[0x57]);
            if (pcVar13 == (char *)0x0) goto LAB_00117634;
            ppcVar12[9] = pcVar13;
            goto LAB_00117a90;
          }
          if (ppcVar12[0x57] != (char *)0x0) {
            cVar6 = stdin_upload();
            if (cVar6 == '\0') goto LAB_00117a90;
            bVar29 = false;
            uVar8 = 0;
            goto LAB_00117a30;
          }
        }
        pcVar13 = (char *)0x0;
        goto LAB_00117aad;
      }
      free(pcVar13);
      puVar27[2] = 0;
      free((void *)puVar27[3]);
      puVar27[3] = 0;
      bVar29 = warn_more_options_1 == '\0';
      *(undefined4 *)(puVar27 + 4) = 0;
      puVar27 = (undefined8 *)*puVar27;
      param_2[0xa5] = (char *)0x0;
      param_2[0x9e] = (char *)puVar27;
      if (bVar29) {
        warnf(param_2[0x9b],"Got more output options than URLs\n");
        warn_more_options_1 = '\x01';
        puVar27 = (undefined8 *)param_2[0x9e];
      }
joined_r0x00118f8f:
    } while (puVar27 != (undefined8 *)0x0);
    ppcVar28 = (char **)0x0;
    free(param_2[0xa1]);
    cVar6 = *param_4;
    param_2[0xa1] = (char *)0x0;
    if (cVar6 == '\0') goto LAB_00117653;
LAB_001191d0:
    ppcVar28 = (char **)0x0;
    goto LAB_0011765e;
  }
  if (*(char *)((long)param_2 + 0x30c) != '\0') {
    if (local_158 == (char *)0x0) {
      local_158 = strdup(pcVar21);
      param_2[0xa2] = local_158;
      free(pcVar21);
      param_2[0x10] = (char *)0x0;
      if (local_158 == (char *)0x0) {
        ppcVar28 = (char **)0x1b;
        errorf(param_1,"out of memory\n");
        goto LAB_00117656;
      }
      iVar9 = SetHTTPrequest(param_2,*(byte *)(param_2 + 0x37) + 1,param_2 + 0x77);
      if (iVar9 != 0) goto LAB_00118ffd;
    }
    goto LAB_00117429;
  }
  iVar9 = SetHTTPrequest(param_2,4,param_2 + 0x77);
  if (iVar9 == 0) goto LAB_00117429;
LAB_00118ffd:
  ppcVar28 = (char **)0x2;
  goto LAB_00117656;
  while (uVar8 != 0x20) {
LAB_00117a30:
    uVar16 = (ulong)uVar8;
    uVar8 = uVar8 + 1;
    if (((ulong)param_2[8] >> (uVar16 & 0x3f) & 1) != 0) {
      if (bVar29) goto LAB_00117a55;
      bVar29 = true;
    }
  }
  if (*(char *)((long)param_2 + 0x31a) != '\0') {
LAB_00117a55:
    warnf(param_1,
          "Using --anyauth or --proxy-anyauth with upload from stdin involves a big risk of it not working. Use a temporary file or a fixed auth type instead!\n"
         );
  }
  pcVar13 = ppcVar12[0x57];
  if ((*pcVar13 == '.') && (pcVar13[1] == '\0')) {
    iVar9 = curlx_nonblock(*(undefined4 *)((long)ppcVar12 + 100),1);
    if (iVar9 < 0) {
      piVar18 = __errno_location();
      pcVar13 = strerror(*piVar18);
      warnf(param_1,"fcntl failed on fd=%d: %s\n",*(undefined4 *)((long)ppcVar12 + 100),pcVar13);
    }
LAB_00117a90:
    pcVar13 = ppcVar12[0x57];
    if (pcVar13 != (char *)0x0) goto LAB_00117a9c;
  }
  else {
LAB_00117a9c:
    if (*(char *)((long)param_2 + 0x49) != '\0') {
      param_2[0xf] = (char *)0xffffffffffffffff;
    }
  }
LAB_00117aad:
  cVar6 = output_expected(ppcVar12[9],pcVar13);
  if ((cVar6 == '\0') || ((FILE *)ppcVar12[0x19] == (FILE *)0x0)) {
LAB_001192a8:
    *(undefined *)(ppcVar12 + 0xd) = uVar1;
    *(undefined2 *)((long)param_1 + 5) = uVar2;
  }
  else {
    iVar9 = fileno((FILE *)ppcVar12[0x19]);
    iVar9 = isatty(iVar9);
    if (iVar9 == 0) goto LAB_001192a8;
    *(undefined *)(ppcVar12 + 0xd) = 1;
    *(undefined2 *)((long)param_1 + 5) = 0x101;
  }
  if (local_158 == (char *)0x0) {
LAB_00117b79:
    if (*(long *)(param_1 + 2) == 0) {
      *(undefined8 *)(param_1 + 2) = stderr;
    }
    pbVar3 = (byte *)ppcVar12[0xb];
    bVar29 = false;
    if (pbVar3 != (byte *)0x0) {
      uVar8 = *pbVar3 - 0x2d;
      if (uVar8 == 0) {
        uVar8 = (uint)pbVar3[1];
      }
      bVar29 = uVar8 == 0;
    }
    *(bool *)(param_2 + 0x62) = bVar29;
    uVar8 = curl_easy_setopt(ppcVar11,0x2774,param_3);
    ppcVar28 = (char **)(ulong)uVar8;
    if (uVar8 == 0) {
      pcVar13 = ppcVar12[9];
      uVar33 = 0x117bd1;
      lVar15 = curl_url();
      local_160 = 0;
      if (lVar15 != 0) {
        if ((pcVar13 == (char *)0x0) || (iVar9 = curl_url_set(lVar15,0,pcVar13,0x208), iVar9 != 0))
        {
          local_160 = 0;
        }
        else {
          local_128[0] = (char *)0x0;
          iVar9 = curl_url_get(lVar15,1,local_128,4);
          if ((iVar9 == 0) && (local_128[0] != (char *)0x0)) {
            local_160 = scheme2protocol();
            curl_free(local_128[0]);
          }
        }
        uVar33 = 0x117c1c;
        curl_url_cleanup(lVar15);
      }
      if (*(char *)(param_2 + 0x80) == '\0') {
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TCP_NODELAY",0x79,0);
      }
      if (*(char *)((long)param_2 + 0x401) != '\0') {
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TCP_FASTOPEN",0xf4,1);
      }
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_WRITEDATA",0x2711,ppcVar12);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_INTERLEAVEDATA",0x27d3,ppcVar12);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_WRITEFUNCTION",0x4e2b,
                  PTR_tool_write_cb_00143fb8);
      ppcVar12[0x27] = (char *)param_2;
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_READDATA",0x2719,ppcVar12 + 0x26);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_READFUNCTION",0x4e2c,PTR_tool_read_cb_00143fd0
                 );
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SEEKDATA",0x27b8,ppcVar12 + 0x26);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SEEKFUNCTION",0x4ec7,PTR_tool_seek_cb_00143fc0
                 );
      pcVar13 = param_2[0x79];
      if ((pcVar13 == (char *)0x0) || (0x18fff < (long)pcVar13)) {
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_BUFFERSIZE",0x62,0x19000);
      }
      else {
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_BUFFERSIZE",0x62,pcVar13);
      }
      tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_URL",0x2712,ppcVar12[9]);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_NOPROGRESS",0x2b,
                  *(undefined *)((long)param_1 + 5));
      if (*(char *)(param_2 + 0x37) != '\0') {
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_NOBODY",0x2c,1);
      }
      if (param_2[0x90] != (char *)0x0) {
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_XOAUTH2_BEARER",0x27ec,param_2[0x90]);
      }
      iVar9 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY",0x2714,param_2[0x2e]);
      if (param_2[0x2e] != (char *)0x0) {
        if (iVar9 != 0) {
          ppcVar28 = (char **)&DAT_00000004;
          errorf(param_1,"proxy support is disabled in this libcurl\n");
          *(undefined4 *)((long)param_2 + 0x4ac) = 1;
          free(param_2[0xa1]);
          param_2[0xa1] = (char *)0x0;
          goto LAB_00117653;
        }
        uVar33 = 0x117eae;
        tool_setopt_enum(ppcVar11,param_1,"CURLOPT_PROXYTYPE",0x65,setopt_nv_CURLPROXY,
                         (long)*(int *)(param_2 + 0x2f));
      }
      tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXYUSERPWD",0x2716,param_2[0x2d],uVar33);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HTTPPROXYTUNNEL",0x3d,
                  *(char *)((long)param_2 + 0x1b1));
      if (param_2[0x7b] != (char *)0x0) {
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PRE_PROXY",0x2816,param_2[0x7b],uVar33);
      }
      uVar23 = 0xffffffffffffffef;
      if ((((*(char *)((long)param_2 + 0x31a) == '\0') &&
           (uVar23 = 4, *(char *)((long)param_2 + 0x316) == '\0')) &&
          (uVar23 = 8, *(char *)((long)param_2 + 0x317) == '\0')) &&
         (uVar23 = 2, *(char *)(param_2 + 99) == '\0')) {
        if (*(char *)((long)param_2 + 0x319) != '\0') {
          uVar23 = 1;
          goto LAB_00119553;
        }
      }
      else {
LAB_00119553:
        uVar33 = 0x119572;
        tool_setopt_bitmask(ppcVar11,param_1,"CURLOPT_PROXYAUTH",0x6f,setopt_nv_CURLAUTH,uVar23);
      }
      tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_NOPROXY",0x27c1,param_2[0x30],uVar33);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SUPPRESS_CONNECT_HEADERS",0x109,
                  *(char *)(param_2 + 0x95));
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FAILONERROR",0x2d,
                  *(char *)((long)param_2 + 0x1b5));
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_REQUEST_TARGET",0x281a,param_2[0x5f]);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_UPLOAD",0x2e,ppcVar12[0x57] != (char *)0x0);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_DIRLISTONLY",0x30,
                  *(char *)((long)param_2 + 0x1b9));
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_APPEND",0x32,*(char *)((long)param_2 + 0x1b2))
      ;
      uVar33 = 1;
      if (*(char *)((long)param_2 + 0x1bc) == '\0') {
        if ((*(char *)((long)param_2 + 0x1bd) == '\0') && (param_2[0x38] == (char *)0x0)) {
          uVar33 = 0;
        }
        else {
          uVar33 = 2;
        }
      }
      uVar23 = 0x118138;
      tool_setopt_enum(ppcVar11,param_1,"CURLOPT_NETRC",0x33,setopt_nv_CURL_NETRC,uVar33);
      if (param_2[0x38] != (char *)0x0) {
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_NETRC_FILE",0x2786,param_2[0x38]);
      }
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TRANSFERTEXT",0x35,
                  *(char *)((long)param_2 + 0x1b3));
      if (param_2[0x26] != (char *)0x0) {
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_LOGIN_OPTIONS",0x27f0,param_2[0x26]);
      }
      tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_USERPWD",0x2715,param_2[0x25]);
      tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_RANGE",0x2717,param_2[0x1e]);
      tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_ERRORBUFFER",0x271a,(long)ppcVar12 + 0x179);
      uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TIMEOUT_MS",0x9b,
                          (long)(DAT_001395d8 * (double)param_2[0x13]));
      ppcVar28 = (char **)(ulong)uVar8;
      if (*(int *)(param_2 + 0x77) == 3) {
        curl_mime_free(param_2[0x73]);
        param_2[0x73] = (char *)0x0;
        uVar8 = tool2curlmime(ppcVar11,param_2[0x71],param_2 + 0x73);
        ppcVar28 = (char **)(ulong)uVar8;
        if (uVar8 != 0) goto LAB_00118f52;
        uVar23 = 0x119718;
        uVar8 = tool_setopt_mimepost(ppcVar11,param_1,"CURLOPT_MIMEPOST",0x281d,param_2[0x73]);
        ppcVar28 = (char **)(ulong)uVar8;
      }
      else if (*(int *)(param_2 + 0x77) == 4) {
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_POSTFIELDS",0x271f,param_2[0x10]);
        uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_POSTFIELDSIZE_LARGE",0x75a8,
                            param_2[0x11]);
        ppcVar28 = (char **)(ulong)uVar8;
      }
      if ((int)ppcVar28 == 0) {
        if (param_2[0x88] != (char *)0x0) {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_MIME_OPTIONS",0x13b,param_2[0x88],uVar23);
        }
        if (param_2[8] != (char *)0x0) {
          tool_setopt_bitmask(ppcVar11,param_1,"CURLOPT_HTTPAUTH",0x6b,setopt_nv_CURLAUTH);
        }
        ppcVar34 = (char **)0x11836b;
        tool_setopt_slist(ppcVar11,param_1,"CURLOPT_HTTPHEADER",0x2727,param_2[0x6f]);
        if ((built_in_protos & 0x40001) != 0) {
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_REFERER",0x2720,param_2[0x12]);
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_USERAGENT",0x2722,param_2[1]);
        }
        if ((built_in_protos & 1) != 0) {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FOLLOWLOCATION",0x34,
                      *(char *)((long)param_2 + 0x1ba));
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_UNRESTRICTED_AUTH",0x69,
                      *(char *)((long)param_2 + 0x1bb));
          uVar16 = (ulong)*(byte *)((long)param_2 + 0x1b4);
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_AUTOREFERER",0x3a);
          ppcVar28 = ppcVar34;
          if (param_2[0x70] != (char *)0x0) {
            ppcVar28 = (char **)0x118437;
            tool_setopt_slist(ppcVar11,param_1,"CURLOPT_PROXYHEADER",0x27f4,param_2[0x70],uVar16);
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HEADEROPT",0xe5,1);
          }
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_MAXREDIRS",0x44,param_2[0x15]);
          pcVar13 = param_2[0x60];
          if (pcVar13 == (char *)0x0) {
            if ((*(byte *)(curlinfo + 0x22) & 1) != 0) {
              pcVar13 = &DAT_00000004;
              goto LAB_0011a5da;
            }
          }
          else {
LAB_0011a5da:
            ppcVar28 = (char **)0x11a5f9;
            tool_setopt_enum(ppcVar11,param_1,"CURLOPT_HTTP_VERSION",0x54,
                             setopt_nv_CURL_HTTP_VERSION,pcVar13);
          }
          bVar7 = *(byte *)((long)param_2 + 0x454);
          if (*(char *)((long)param_2 + 0x455) != '\0') {
            bVar7 = bVar7 | 2;
          }
          if (*(char *)((long)param_2 + 0x456) != '\0') {
            bVar7 = bVar7 | 4;
          }
          ppcVar19 = ppcVar11;
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_POSTREDIR",0xa1,bVar7);
          if (*(char *)((long)param_2 + 0x39) != '\0') {
            ppcVar19 = ppcVar11;
            tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_ACCEPT_ENCODING",0x2776,&DAT_00124101,
                        ppcVar34);
            ppcVar28 = ppcVar34;
          }
          ppcVar34 = ppcVar28;
          if (*(char *)((long)param_2 + 0x3a) != '\0') {
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TRANSFER_ENCODING",0xcf,1,ppcVar19);
            ppcVar34 = ppcVar19;
          }
          uVar16 = (ulong)*(byte *)(param_2 + 0x61);
          uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HTTP09_ALLOWED",0x11d);
          ppcVar28 = (char **)(ulong)uVar8;
          if (uVar8 != 0) {
            errorf(param_1,"HTTP/0.9 is not supported in this build!\n",extraout_RDX,uVar16);
            goto LAB_0011765e;
          }
        }
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_FTPPORT",0x2721,param_2[0x19]);
        ppcVar19 = (char **)param_2[0x1f];
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_LOW_SPEED_LIMIT",0x13,ppcVar19);
        ppcVar22 = (char **)param_2[0x20];
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_LOW_SPEED_TIME",0x14,ppcVar22);
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_MAX_SEND_SPEED_LARGE",0x75c1,param_2[0x78]);
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_MAX_RECV_SPEED_LARGE",0x75c2,param_2[0x79]);
        if (*(char *)(param_2 + 9) == '\0') {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_RESUME_FROM_LARGE",0x75a4,0);
        }
        else {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_RESUME_FROM_LARGE",0x75a4,param_2[0xf]);
        }
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_KEYPASSWD",0x272a,param_2[0x53]);
        ppcVar24 = (char **)0x2812;
        pcVar20 = "CURLOPT_PROXY_KEYPASSWD";
        pcVar13 = (char *)0x1;
        ppcVar26 = param_2;
        tool_setopt(ppcVar11,1,param_1);
        ppcVar28 = ppcVar11;
        ppcVar31 = ppcVar34;
        if ((local_160 & 0x30) != 0) {
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSH_PRIVATE_KEYFILE",0x27a9,param_2[0x4f],
                      pcVar20);
          ppcVar19 = (char **)param_2[0x55];
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSH_PUBLIC_KEYFILE",0x27a8,ppcVar19);
          ppcVar22 = (char **)param_2[0x56];
          ppcVar31 = (char **)pcVar20;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSH_HOST_PUBLIC_KEY_MD5",0x27b2,ppcVar22);
          ppcVar28 = (char **)param_2[0x57];
          ppcVar24 = (char **)0x2847;
          pcVar20 = "CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256";
          pcVar13 = (char *)0x1;
          ppcVar25 = param_2;
          tool_setopt(ppcVar11,1,param_1);
          ppcVar26 = ppcVar25;
          ppcVar34 = extraout_RDX_19;
          if (*(char *)(param_2 + 0x96) != '\0') {
            ppcVar24 = (char **)0x10c;
            pcVar20 = "CURLOPT_SSH_COMPRESSION";
            pcVar13 = (char *)0x1;
            ppcVar26 = param_2;
            tool_setopt(ppcVar11,0,param_1);
            ppcVar34 = extraout_RDX_20;
            ppcVar31 = ppcVar25;
          }
        }
        if (param_2[0x47] != (char *)0x0) {
          ppcVar24 = (char **)0x2751;
          pcVar20 = "CURLOPT_CAINFO";
          pcVar13 = (char *)0x1;
          ppcVar26 = param_2;
          ppcVar34 = ppcVar31;
          tool_setopt(ppcVar11,1,param_1);
          ppcVar31 = ppcVar34;
        }
        if ((char **)param_2[0x48] != (char **)0x0) {
          ppcVar24 = (char **)0x2806;
          pcVar20 = "CURLOPT_PROXY_CAINFO";
          pcVar13 = (char *)0x1;
          ppcVar26 = param_2;
          ppcVar22 = (char **)param_2[0x48];
          ppcVar28 = ppcVar31;
          tool_setopt(ppcVar11,1,param_1);
          ppcVar34 = extraout_RDX_00;
          ppcVar31 = ppcVar28;
        }
        ppcVar25 = (char **)param_2[0x49];
        if (ppcVar25 == (char **)0x0) {
          pcVar14 = param_2[0x4a];
          ppcVar25 = ppcVar24;
          ppcVar24 = ppcVar31;
joined_r0x0011a5c9:
          ppcVar31 = ppcVar19;
          if (pcVar14 != (char *)0x0) goto LAB_001187ac;
        }
        else {
          pcVar20 = "CURLOPT_CAPATH";
          pcVar13 = (char *)0x1;
          ppcVar26 = param_2;
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_CAPATH",0x2771,ppcVar25);
          ppcVar28 = (char **)(ulong)uVar8;
          if (uVar8 == 4) {
            pcVar14 = "SSL_CERT_DIR environment variable";
            pcVar13 = "ignoring %s, not supported by libcurl\n";
            if (!bVar5) {
              pcVar14 = "--capath";
            }
            ppcVar24 = (char **)0x11a9b2;
            warnf(param_1,"ignoring %s, not supported by libcurl\n",pcVar14);
            ppcVar34 = extraout_RDX_24;
          }
          else {
            ppcVar34 = extraout_RDX_01;
            ppcVar24 = ppcVar31;
            if (uVar8 != 0) goto LAB_00118f52;
          }
          pcVar14 = param_2[0x4a];
          if (pcVar14 == (char *)0x0) {
            pcVar14 = param_2[0x49];
            ppcVar19 = ppcVar31;
            goto joined_r0x0011a5c9;
          }
LAB_001187ac:
          pcVar20 = (char *)ppcVar24;
          ppcVar25 = (char **)0x2807;
          pcVar13 = (char *)0x1;
          ppcVar26 = param_2;
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_CAPATH",0x2807,pcVar14,
                              pcVar20);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar34 = extraout_RDX_02;
          ppcVar24 = (char **)pcVar20;
          if (uVar8 == 4) {
            if (param_2[0x4a] != (char *)0x0) {
              pcVar13 = "ignoring --proxy-capath, not supported by libcurl\n";
              ppcVar24 = (char **)0x11a8f2;
              warnf(param_1);
              ppcVar34 = extraout_RDX_22;
            }
          }
          else if (uVar8 != 0) goto LAB_00118f52;
        }
        ppcVar19 = (char **)param_2[0x4b];
        if (ppcVar19 == (char **)0x0) {
          ppcVar17 = (char **)param_2[0x4c];
          ppcVar19 = ppcVar26;
          ppcVar34 = ppcVar24;
joined_r0x0011a810:
          if (ppcVar17 != (char **)0x0) goto LAB_00118836;
        }
        else {
          ppcVar25 = (char **)0x27b9;
          pcVar20 = "CURLOPT_CRLFILE";
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_CRLFILE",0x27b9,ppcVar19,ppcVar34);
          ppcVar17 = (char **)param_2[0x4c];
          if (ppcVar17 == (char **)0x0) {
            ppcVar17 = (char **)param_2[0x4b];
            pcVar13 = (char *)ppcVar34;
            goto joined_r0x0011a810;
          }
LAB_00118836:
          ppcVar25 = (char **)0x2814;
          pcVar20 = "CURLOPT_PROXY_CRLFILE";
          pcVar13 = (char *)0x1;
          ppcVar19 = param_2;
          ppcVar28 = ppcVar17;
          tool_setopt(ppcVar11,1,param_1);
        }
        ppcVar26 = (char **)param_2[0x4d];
        if (ppcVar26 != (char **)0x0) {
          pcVar20 = "CURLOPT_PINNEDPUBLICKEY";
          pcVar13 = (char *)0x1;
          ppcVar19 = param_2;
          ppcVar31 = ppcVar26;
          ppcVar22 = ppcVar25;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PINNEDPUBLICKEY",0x27f6,ppcVar26,ppcVar25)
          ;
          ppcVar34 = ppcVar22;
        }
        if (param_2[0x5d] != (char *)0x0) {
          ppcVar19 = param_2;
          pcVar20 = pcVar13;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSL_EC_CURVES",0x283a,param_2[0x5d],
                      pcVar13);
          ppcVar34 = (char **)pcVar20;
        }
        ppcVar26 = ppcVar28;
        if ((*(byte *)(curlinfo + 0x20) & 4) != 0) {
          if (((param_2[0x43] != (char *)0x0) && (param_2[0x45] == (char *)0x0)) &&
             (cVar6 = is_pkcs11_uri(), cVar6 != '\0')) {
            pcVar13 = strdup("ENG");
            param_2[0x45] = pcVar13;
          }
          if (((param_2[0x4f] != (char *)0x0) && (param_2[0x51] == (char *)0x0)) &&
             (cVar6 = is_pkcs11_uri(), cVar6 != '\0')) {
            pcVar13 = strdup("ENG");
            param_2[0x51] = pcVar13;
          }
          if (((param_2[0x44] != (char *)0x0) && (param_2[0x46] == (char *)0x0)) &&
             (cVar6 = is_pkcs11_uri(), cVar6 != '\0')) {
            pcVar13 = strdup("ENG");
            param_2[0x46] = pcVar13;
          }
          if (((param_2[0x50] != (char *)0x0) && (param_2[0x52] == (char *)0x0)) &&
             (cVar6 = is_pkcs11_uri(), cVar6 != '\0')) {
            pcVar13 = strdup("ENG");
            param_2[0x52] = pcVar13;
          }
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSLCERT",0x2729,param_2[0x43],pcVar20);
          ppcVar31 = (char **)param_2[0x44];
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_SSLCERT",0x280e,ppcVar31);
          ppcVar22 = (char **)param_2[0x45];
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSLCERTTYPE",0x2766,ppcVar22);
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_SSLCERTTYPE",0x280f,param_2[0x46]);
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSLKEY",0x2767,param_2[0x4f]);
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_SSLKEY",0x2810,param_2[0x50]);
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSLKEYTYPE",0x2768,param_2[0x51]);
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_SSLKEYTYPE",0x2811,param_2[0x52]);
          uVar33 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_AWS_SIGV4",0x2841,param_2[0x99]);
          ppcVar28 = param_2;
          if (*(char *)((long)param_2 + 0x30d) == '\0') {
            pcVar13 = "CURLOPT_SSL_VERIFYPEER";
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SSL_VERIFYPEER",0x40,1,uVar33);
          }
          else {
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SSL_VERIFYPEER",0x40,0,pcVar20);
            pcVar13 = "CURLOPT_SSL_VERIFYHOST";
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SSL_VERIFYHOST",0x51,0);
          }
          if (*(char *)((long)param_2 + 0x30e) != '\0') {
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_DOH_SSL_VERIFYPEER",0x132,0,ppcVar22);
            pcVar13 = "CURLOPT_DOH_SSL_VERIFYHOST";
            ppcVar28 = param_2;
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_DOH_SSL_VERIFYHOST",0x133,0);
          }
          if (*(char *)((long)param_2 + 0x30f) == '\0') {
            pcVar20 = "CURLOPT_PROXY_SSL_VERIFYPEER";
            uVar33 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_PROXY_SSL_VERIFYPEER",0xf8,1,
                                 ppcVar28);
          }
          else {
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_PROXY_SSL_VERIFYPEER",0xf8,0,pcVar13);
            pcVar20 = "CURLOPT_PROXY_SSL_VERIFYHOST";
            ppcVar31 = (char **)pcVar13;
            uVar33 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_PROXY_SSL_VERIFYHOST",0xf9,0);
          }
          if (*(char *)((long)param_2 + 0x311) != '\0') {
            pcVar20 = "CURLOPT_SSL_VERIFYSTATUS";
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SSL_VERIFYSTATUS",0xe8,1,uVar33);
          }
          if (*(char *)((long)param_2 + 0x312) != '\0') {
            pcVar20 = "CURLOPT_DOH_SSL_VERIFYSTATUS";
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_DOH_SSL_VERIFYSTATUS",0x134,1,ppcVar22);
          }
          if (*(char *)((long)param_2 + 0x499) != '\0') {
            ppcVar31 = (char **)pcVar20;
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SSL_FALSESTART",0xe9,1,pcVar20);
          }
          ppcVar19 = (char **)0x20;
          ppcVar34 = (char **)0x118c09;
          tool_setopt_enum(ppcVar11,param_1,"CURLOPT_SSLVERSION",0x20,setopt_nv_CURL_SSLVERSION,
                           (ulong)param_2[0x68] | (ulong)param_2[0x69]);
          if (param_2[0x2e] != (char *)0x0) {
            ppcVar19 = (char **)0xfa;
            ppcVar34 = (char **)0x118c3a;
            tool_setopt_enum(ppcVar11,param_1,"CURLOPT_PROXY_SSLVERSION",0xfa,
                             setopt_nv_CURL_SSLVERSION,param_2[0x6a]);
          }
          uVar8 = (uint)*(byte *)((long)param_2 + 0x47b) << 3 |
                  (uint)*(byte *)((long)param_2 + 0x47a) * 2 | (uint)*(byte *)(param_2 + 0x8f) |
                  (uint)*(byte *)((long)param_2 + 0x47c) << 4 |
                  (uint)*(byte *)((long)param_2 + 0x47d) << 5;
          if (uVar8 != 0) {
            ppcVar19 = (char **)0xd8;
            ppcVar34 = (char **)0x11b039;
            tool_setopt_bitmask(ppcVar11,param_1,"CURLOPT_SSL_OPTIONS",0xd8,setopt_nv_CURLSSLOPT,
                                uVar8);
          }
          uVar8 = (uint)*(byte *)((long)param_2 + 0x47e) << 5;
          ppcVar26 = ppcVar11;
          if ((uVar8 | *(byte *)((long)param_2 + 0x479)) != 0) {
            ppcVar19 = (char **)0x105;
            ppcVar34 = (char **)0x11b00b;
            tool_setopt_bitmask(ppcVar11,param_1,"CURLOPT_PROXY_SSL_OPTIONS",0x105,
                                setopt_nv_CURLSSLOPT,uVar8 | *(byte *)((long)param_2 + 0x479));
          }
        }
        if (*(char *)((long)param_2 + 0x49a) != '\0') {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_PATH_AS_IS",0xea,1,ppcVar19);
          ppcVar34 = ppcVar19;
        }
        if (((local_160 & 0x30) != 0) && (*(char *)((long)param_2 + 0x30d) == '\0')) {
          lVar15 = findfile(".ssh/known_hosts",0);
          if (lVar15 == 0) {
            ppcVar34 = (char **)0x11b220;
            warnf(param_1,"Couldn\'t find a known_hosts file!");
          }
          else {
            uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSH_KNOWNHOSTS",0x27c7,lVar15,
                                ppcVar26);
            ppcVar28 = (char **)(ulong)uVar8;
            curl_free(lVar15);
            ppcVar34 = ppcVar26;
            if ((uVar8 != 0) && (uVar8 != 0x30)) goto LAB_00118f52;
          }
        }
        if ((*(char *)(param_2 + 0x37) != '\0') || (*(char *)param_2 != '\0')) {
          ppcVar31 = (char **)0x1;
          ppcVar22 = ppcVar34;
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FILETIME",0x45,1,ppcVar34);
          ppcVar34 = ppcVar22;
        }
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_CRLF",0x1b,*(char *)(param_2 + 0x5b),
                    ppcVar34);
        tool_setopt_slist(ppcVar11,param_1,"CURLOPT_QUOTE",0x272c,param_2[0x65]);
        tool_setopt_slist(ppcVar11,param_1,"CURLOPT_POSTQUOTE",0x2737,param_2[0x66]);
        ppcVar28 = (char **)0x276d;
        tool_setopt_slist(ppcVar11,param_1,"CURLOPT_PREQUOTE",0x276d,param_2[0x67]);
        pcVar13 = (char *)ppcVar34;
        if (param_2[2] != (char *)0x0) {
          curlx_dyn_init(local_128,0x1000);
          pcVar13 = (char *)ppcVar28;
          for (puVar4 = (undefined8 *)param_2[2]; puVar4 != (undefined8 *)0x0;
              puVar4 = (undefined8 *)puVar4[1]) {
            if (puVar4 == (undefined8 *)param_2[2]) {
              iVar9 = curlx_dyn_addf(local_128,"%s",*puVar4);
            }
            else {
              iVar9 = curlx_dyn_addf(local_128,&DAT_0013875b,*puVar4);
            }
            if (iVar9 != 0) break;
          }
          ppcVar31 = local_128;
          uVar33 = curlx_dyn_ptr(local_128);
          ppcVar34 = (char **)0x2726;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_COOKIE",0x2726,uVar33,pcVar13);
          curlx_dyn_free(local_128);
        }
        for (puVar4 = (undefined8 *)param_2[4]; puVar4 != (undefined8 *)0x0;
            puVar4 = (undefined8 *)puVar4[1]) {
          ppcVar34 = (char **)0x272f;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_COOKIEFILE",0x272f,*puVar4,pcVar13);
        }
        ppcVar28 = (char **)param_2[3];
        if (ppcVar28 != (char **)0x0) {
          ppcVar31 = ppcVar28;
          ppcVar22 = ppcVar34;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_COOKIEJAR",0x2762,ppcVar28,ppcVar34);
          pcVar13 = (char *)ppcVar22;
        }
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_COOKIESESSION",0x60,*(char *)(param_2 + 7),
                    pcVar13);
        tool_setopt_enum(ppcVar11,param_1,"CURLOPT_TIMECONDITION",0x21,setopt_nv_CURL_TIMECOND,
                         *(undefined4 *)(param_2 + 0x6d));
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TIMEVALUE_LARGE",0x763e,param_2[0x6e]);
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_CUSTOMREQUEST",0x2734,param_2[0x5c]);
        customrequest_helper(param_2,*(undefined4 *)(param_2 + 0x77),param_2[0x5c]);
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_STDERR",0x2735,*(undefined8 *)(param_1 + 2))
        ;
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_INTERFACE",0x274e,param_2[0x1a]);
        ppcVar19 = (char **)0x274f;
        ppcVar28 = param_2;
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_KRBLEVEL",0x274f,param_2[0x5e]);
        ppcVar34 = (char **)progressbarinit(ppcVar12 + 0xe,param_2);
        if (((param_1[0xd] == 1) && (*(char *)((long)param_1 + 5) == '\0')) &&
           (puVar32 = PTR_tool_progress_cb_00143fa8, *(char *)(param_1 + 1) == '\0')) {
LAB_001199e6:
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_XFERINFOFUNCTION",0x4efb,puVar32,ppcVar34)
          ;
          ppcVar19 = (char **)0x2749;
          pcVar13 = "CURLOPT_XFERINFODATA";
          ppcVar28 = param_2;
          ppcVar22 = ppcVar12;
          tool_setopt(ppcVar11,0,param_1);
        }
        else {
          ppcVar26 = (char **)ppcVar12[0x57];
          ppcVar34 = ppcVar11;
          if (((ppcVar26 != (char **)0x0) && (*(char *)ppcVar26 == '.')) &&
             (*(char *)((long)ppcVar26 + 1) == '\0')) {
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_NOPROGRESS",0x2b,0,ppcVar26);
            puVar32 = PTR_tool_readbusy_cb_00143ff8;
            ppcVar34 = ppcVar26;
            goto LAB_001199e6;
          }
        }
        ppcVar26 = (char **)param_2[0x21];
        if (ppcVar26 != (char **)0x0) {
          ppcVar28 = param_2;
          ppcVar19 = ppcVar26;
          ppcVar31 = (char **)pcVar13;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_DNS_SERVERS",0x27e3,ppcVar26,pcVar13);
        }
        if (param_2[0x22] != (char *)0x0) {
          ppcVar19 = (char **)0x27ed;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_DNS_INTERFACE",0x27ed,param_2[0x22],
                      ppcVar28);
        }
        if (param_2[0x23] != (char *)0x0) {
          ppcVar19 = (char **)0x27ee;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_DNS_LOCAL_IP4",0x27ee,param_2[0x23],
                      ppcVar34);
        }
        ppcVar28 = (char **)param_2[0x24];
        if (ppcVar28 != (char **)0x0) {
          ppcVar31 = ppcVar28;
          ppcVar22 = ppcVar19;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_DNS_LOCAL_IP6",0x27ef,ppcVar28,ppcVar19);
        }
        pcVar13 = (char *)0x119b15;
        tool_setopt_slist(ppcVar11,param_1,"CURLOPT_TELNETOPTIONS",0x2756,param_2[0x74]);
        ppcVar24 = (char **)0x0;
        ppcVar26 = (char **)0x9c;
        ppcVar34 = param_2;
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_CONNECTTIMEOUT_MS",0x9c,
                    (long)(DAT_001395d8 * (double)param_2[0x14]),pcVar13);
        ppcVar28 = (char **)param_2[0x3e];
        ppcVar19 = (char **)pcVar13;
        if (ppcVar28 != (char **)0x0) {
          ppcVar26 = (char **)0x2827;
          pcVar13 = "CURLOPT_DOH_URL";
          ppcVar34 = ppcVar28;
          ppcVar24 = extraout_RDX_03;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_DOH_URL",0x2827,ppcVar28,extraout_RDX_03);
          ppcVar19 = ppcVar24;
        }
        ppcVar28 = ppcVar11;
        if ((char **)param_2[0x3f] != (char **)0x0) {
          ppcVar26 = (char **)0x2763;
          pcVar13 = "CURLOPT_SSL_CIPHER_LIST";
          ppcVar24 = (char **)0x1;
          ppcVar34 = param_2;
          ppcVar28 = (char **)param_2[0x3f];
          tool_setopt(ppcVar11,1,param_1);
          ppcVar19 = ppcVar22;
        }
        ppcVar22 = (char **)param_2[0x40];
        pcVar20 = pcVar13;
        if (ppcVar22 != (char **)0x0) {
          pcVar20 = "CURLOPT_PROXY_SSL_CIPHER_LIST";
          ppcVar24 = (char **)0x1;
          ppcVar34 = param_2;
          ppcVar26 = ppcVar22;
          ppcVar31 = (char **)pcVar13;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_SSL_CIPHER_LIST",0x2813,ppcVar22,
                      pcVar13);
          ppcVar19 = ppcVar31;
        }
        if ((char **)param_2[0x41] != (char **)0x0) {
          ppcVar26 = (char **)0x2824;
          pcVar20 = "CURLOPT_TLS13_CIPHERS";
          ppcVar24 = (char **)param_2[0x41];
          tool_setopt(ppcVar11,1,param_1);
          ppcVar19 = ppcVar34;
        }
        ppcVar34 = (char **)param_2[0x42];
        if (ppcVar34 != (char **)0x0) {
          ppcVar26 = (char **)0x2825;
          pcVar20 = "CURLOPT_PROXY_TLS13_CIPHERS";
          ppcVar24 = (char **)0x1;
          tool_setopt(ppcVar11,1,param_1);
          ppcVar19 = ppcVar28;
        }
        if (*(char *)((long)param_2 + 0x4a) != '\0') {
          pcVar20 = "CURLOPT_FTP_USE_EPSV";
          ppcVar31 = (char **)0x0;
          ppcVar24 = (char **)0x0;
          ppcVar34 = (char **)tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FTP_USE_EPSV",0x55,0,
                                          ppcVar26);
          ppcVar19 = ppcVar26;
        }
        if (*(char *)((long)param_2 + 0x4b) != '\0') {
          pcVar20 = (char *)ppcVar24;
          ppcVar34 = (char **)tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FTP_USE_EPRT",0x6a,0,
                                          ppcVar24);
          ppcVar19 = (char **)pcVar20;
        }
        if (param_1[0xb] != 0) {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_DEBUGFUNCTION",0x4e7e,
                      PTR_tool_debug_cb_00143fc8,ppcVar34);
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_DEBUGDATA",0x276f,param_2);
          pcVar20 = "CURLOPT_VERBOSE";
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_VERBOSE",0x29,1);
          ppcVar19 = ppcVar34;
        }
        if (param_2[0x58] != (char *)0x0) {
          ppcVar31 = (char **)pcVar20;
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SSLENGINE",0x2769,param_2[0x58],
                              pcVar20);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = ppcVar31;
          if (uVar8 != 0) goto LAB_00118f52;
        }
        ppcVar28 = param_2;
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FTP_CREATE_MISSING_DIRS",0x6e,
                    (ulong)*(byte *)((long)param_2 + 0x314) * 2,ppcVar19);
        if (param_2[0x16] != (char *)0x0) {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_MAXFILESIZE_LARGE",0x75a5,param_2[0x16],
                      ppcVar28);
          ppcVar19 = ppcVar28;
        }
        ppcVar28 = ppcVar11;
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_IPRESOLVE",0x71,param_2[0x6b],ppcVar19);
        uVar33 = 3;
        if ((*(char *)((long)param_2 + 0x3d1) == '\0') &&
           (uVar33 = 1, *(char *)(param_2 + 0x7a) == '\0')) {
          ppcVar34 = ppcVar19;
          if (*(char *)((long)param_2 + 0x3d2) != '\0') {
            uVar33 = 2;
            goto LAB_0011a821;
          }
        }
        else {
LAB_0011a821:
          ppcVar34 = (char **)0x11a840;
          ppcVar28 = ppcVar11;
          tool_setopt_enum(ppcVar11,param_1,"CURLOPT_USE_SSL",0x77,setopt_nv_CURLUSESSL,uVar33);
          ppcVar19 = extraout_RDX_21;
        }
        if (*(char *)((long)param_2 + 0x3d3) != '\0') {
          ppcVar34 = (char **)0x11ad8e;
          ppcVar28 = ppcVar11;
          tool_setopt_enum(ppcVar11,param_1,"CURLOPT_FTP_SSL_CCC",0x9a,setopt_nv_CURLFTPSSL_CCC,
                           (long)*(int *)((long)param_2 + 0x3d4));
          ppcVar19 = extraout_RDX_31;
        }
        if (*(int *)(param_2 + 0x7c) != 0) {
          ppcVar28 = ppcVar11;
          ppcVar34 = ppcVar31;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SOCKS5_GSSAPI_NEC",0xb4,
                      *(int *)(param_2 + 0x7c),ppcVar31);
          ppcVar19 = extraout_RDX_30;
        }
        if (param_2[0x7d] != (char *)0x0) {
          ppcVar34 = (char **)0x11ad32;
          ppcVar28 = ppcVar11;
          tool_setopt_bitmask(ppcVar11,param_1,"CURLOPT_SOCKS5_AUTH",0x10b,setopt_nv_CURLAUTH);
          ppcVar19 = extraout_RDX_29;
        }
        if (param_2[0x7e] != (char *)0x0) {
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_SERVICE_NAME",0x27fb,param_2[0x7e],
                      ppcVar28);
          ppcVar19 = extraout_RDX_04;
          ppcVar34 = ppcVar28;
        }
        if (param_2[0x7f] != (char *)0x0) {
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SERVICE_NAME",0x27fc,param_2[0x7f],
                      ppcVar19);
          ppcVar34 = ppcVar19;
        }
        tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_FTP_ACCOUNT",0x2796,param_2[0x85],ppcVar34);
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_IGNORE_CONTENT_LENGTH",0x88,
                    *(char *)((long)param_2 + 0x451));
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FTP_SKIP_PASV_IP",0x89,
                    *(char *)((long)param_2 + 0x315));
        lVar15 = (long)*(int *)(param_2 + 0x87);
        ppcVar28 = ppcVar11;
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FTP_FILEMETHOD",0x8a,lVar15);
        if (param_2[0x1b] != (char *)0x0) {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_LOCALPORT",0x8b,param_2[0x1b],ppcVar28);
          ppcVar31 = ppcVar28;
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_LOCALPORTRANGE",0x8c,param_2[0x1c]);
          ppcVar34 = ppcVar31;
        }
        pcVar13 = "CURLOPT_FTP_ALTERNATIVE_TO_USER";
        uVar33 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_FTP_ALTERNATIVE_TO_USER",0x27a3,
                             param_2[0x86],ppcVar34);
        if (*(char *)((long)param_2 + 0x452) != '\0') {
          pcVar13 = "CURLOPT_SSL_SESSIONID_CACHE";
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_SSL_SESSIONID_CACHE",0x96,0,uVar33);
        }
        if (*(char *)((long)param_2 + 0x453) != '\0') {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HTTP_CONTENT_DECODING",0x9e,0,lVar15);
          pcVar13 = "CURLOPT_HTTP_TRANSFER_DECODING";
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HTTP_TRANSFER_DECODING",0x9d,0);
        }
        ppcVar28 = ppcVar11;
        if (*(char *)((long)param_2 + 0x457) == '\0') {
          ppcVar31 = (char **)pcVar13;
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TCP_KEEPALIVE",0xd5,1,pcVar13);
          ppcVar19 = extraout_RDX_05;
          ppcVar34 = ppcVar31;
          if (param_2[0x8b] != (char *)0x0) {
            ppcVar28 = extraout_RDX_05;
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TCP_KEEPIDLE",0xd6,param_2[0x8b],
                        extraout_RDX_05);
            tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TCP_KEEPINTVL",0xd7,param_2[0x8b]);
            ppcVar19 = extraout_RDX_33;
            ppcVar34 = ppcVar28;
          }
        }
        else {
          ppcVar34 = ppcVar31;
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TCP_KEEPALIVE",0xd5,0,ppcVar31);
          ppcVar19 = extraout_RDX_23;
        }
        if (param_2[0x89] != (char *)0x0) {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TFTP_BLKSIZE",0xb2,param_2[0x89],ppcVar28)
          ;
          ppcVar19 = extraout_RDX_32;
          ppcVar34 = ppcVar28;
        }
        if (param_2[0x31] != (char *)0x0) {
          tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_MAIL_FROM",0x27ca,param_2[0x31],ppcVar19);
          ppcVar34 = ppcVar19;
        }
        if (param_2[0x32] != (char *)0x0) {
          ppcVar34 = (char **)0x119fe0;
          tool_setopt_slist(ppcVar11,param_1,"CURLOPT_MAIL_RCPT",0x27cb);
        }
        pcVar13 = (char *)(ulong)*(byte *)(param_2 + 0x34);
        pcVar20 = "CURLOPT_MAIL_RCPT_ALLLOWFAILS";
        ppcVar28 = param_2;
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_MAIL_RCPT_ALLLOWFAILS",0x122,pcVar13,
                    ppcVar34);
        if (*(char *)((long)param_2 + 0x4c) != '\0') {
          ppcVar28 = param_2;
          ppcVar31 = (char **)pcVar20;
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_FTP_USE_PRET",0xbc,1,pcVar20);
          ppcVar34 = ppcVar31;
        }
        if (param_2[0x6c] != (char *)0x0) {
          tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_NEW_FILE_PERMS",0x9f,param_2[0x6c],
                      ppcVar28);
          ppcVar34 = ppcVar28;
        }
        if (*(char *)(param_2 + 0xb) != '\0') {
          ppcVar34 = (char **)0x11ae39;
          tool_setopt_flags(ppcVar11,param_1,"CURLOPT_PROTOCOLS",0xb5,setopt_nv_CURLPROTO,
                            param_2[10]);
        }
        if (*(char *)(param_2 + 0xd) != '\0') {
          ppcVar34 = (char **)0x11ac82;
          tool_setopt_flags(ppcVar11,param_1,"CURLOPT_REDIR_PROTOCOLS",0xb6,setopt_nv_CURLPROTO,
                            param_2[0xc]);
        }
        bVar7 = *(byte *)(param_2 + 0x8c);
        if (bVar7 != 0) {
          bVar7 = (byte)(*(uint *)(puVar27 + 4) >> 2) & 1;
        }
        *(byte *)(ppcVar12 + 0x2d) = bVar7;
        ppcVar12[0x28] = (char *)param_1;
        ppcVar12[0x2a] = (char *)(ppcVar12 + 0x17);
        ppcVar12[0x2b] = (char *)(ppcVar12 + 0x1c);
        ppcVar12[0x2c] = local_108;
        ppcVar12[0x29] = (char *)param_2;
        tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HEADERFUNCTION",0x4e6f,
                    PTR_tool_header_cb_00143fe0,ppcVar34);
        ppcVar26 = (char **)0x272d;
        ppcVar19 = param_2;
        ppcVar22 = ppcVar11;
        uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HEADERDATA",0x272d,ppcVar12);
        if (param_2[0x75] != (char *)0x0) {
          ppcVar19 = (char **)0x27db;
          ppcVar22 = ppcVar11;
          uVar8 = tool_setopt_slist(ppcVar11,param_1,"CURLOPT_RESOLVE");
          ppcVar34 = extraout_RDX_06;
        }
        pcVar20 = param_2[0x76];
        if ((char **)pcVar20 != (char **)0x0) {
          ppcVar19 = (char **)0x2803;
          ppcVar22 = ppcVar11;
          uVar8 = tool_setopt_slist(ppcVar11,param_1,"CURLOPT_CONNECT_TO");
          ppcVar34 = extraout_RDX_07;
        }
        ppcVar28 = (char **)(ulong)uVar8;
        ppcVar24 = ppcVar22;
        if ((*(byte *)(curlinfo + 0x21) & 0x40) != 0) {
          ppcVar24 = ppcVar19;
          pcVar14 = pcVar20;
          if (param_2[0x27] != (char *)0x0) {
            ppcVar26 = (char **)0x27dc;
            pcVar14 = "CURLOPT_TLSAUTH_USERNAME";
            ppcVar24 = param_2;
            ppcVar22 = ppcVar11;
            uVar8 = tool_setopt(ppcVar11,1,param_1);
            ppcVar28 = (char **)(ulong)uVar8;
            ppcVar34 = extraout_RDX_08;
          }
          ppcVar19 = (char **)param_2[0x28];
          pcVar20 = pcVar14;
          if (ppcVar19 != (char **)0x0) {
            pcVar20 = "CURLOPT_TLSAUTH_PASSWORD";
            ppcVar24 = param_2;
            ppcVar22 = ppcVar11;
            ppcVar26 = ppcVar19;
            ppcVar31 = (char **)pcVar14;
            uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_TLSAUTH_PASSWORD",0x27dd,
                                ppcVar19,pcVar14);
            ppcVar28 = (char **)(ulong)uVar8;
            ppcVar34 = extraout_RDX_09;
          }
          ppcVar19 = ppcVar24;
          if (param_2[0x29] != (char *)0x0) {
            ppcVar26 = (char **)0x27de;
            pcVar20 = "CURLOPT_TLSAUTH_TYPE";
            ppcVar19 = param_2;
            ppcVar22 = ppcVar24;
            uVar8 = tool_setopt(ppcVar11,1,param_1);
            ppcVar28 = (char **)(ulong)uVar8;
            ppcVar34 = extraout_RDX_10;
          }
          if (param_2[0x2a] != (char *)0x0) {
            ppcVar26 = (char **)0x280b;
            pcVar20 = "CURLOPT_PROXY_TLSAUTH_USERNAME";
            ppcVar19 = param_2;
            ppcVar22 = ppcVar11;
            ppcVar34 = ppcVar28;
            uVar8 = tool_setopt(ppcVar11,1,param_1);
            ppcVar28 = (char **)(ulong)uVar8;
          }
          if (param_2[0x2b] != (char *)0x0) {
            ppcVar26 = (char **)0x280c;
            pcVar20 = "CURLOPT_PROXY_TLSAUTH_PASSWORD";
            ppcVar19 = param_2;
            ppcVar22 = ppcVar11;
            pcVar13 = param_2[0x2b];
            uVar8 = tool_setopt(ppcVar11,1,param_1);
            ppcVar28 = (char **)(ulong)uVar8;
            ppcVar34 = extraout_RDX_11;
          }
          ppcVar25 = (char **)param_2[0x2c];
          ppcVar24 = ppcVar22;
          if (ppcVar25 != (char **)0x0) {
            ppcVar19 = param_2;
            ppcVar24 = ppcVar11;
            pcVar20 = (char *)ppcVar25;
            ppcVar26 = ppcVar22;
            uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_PROXY_TLSAUTH_TYPE",0x280d,
                                ppcVar25,ppcVar22);
            ppcVar28 = (char **)(ulong)uVar8;
            ppcVar34 = extraout_RDX_12;
          }
        }
        ppcVar22 = (char **)param_2[0x8e];
        if (ppcVar22 != (char **)0x0) {
          ppcVar26 = (char **)0xd2;
          pcVar20 = "CURLOPT_GSSAPI_DELEGATION";
          ppcVar24 = ppcVar11;
          ppcVar19 = ppcVar22;
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_GSSAPI_DELEGATION",0xd2,ppcVar22,
                              ppcVar34);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar34 = extraout_RDX_27;
        }
        if (param_2[0x33] != (char *)0x0) {
          ppcVar26 = (char **)0x27e9;
          pcVar20 = "CURLOPT_MAIL_AUTH";
          ppcVar19 = param_2;
          ppcVar24 = ppcVar11;
          uVar8 = tool_setopt(ppcVar11,1,param_1);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar34 = extraout_RDX_13;
        }
        ppcVar22 = (char **)param_2[0x35];
        auVar30 = CONCAT88(ppcVar34,ppcVar22);
        pcVar14 = pcVar20;
        if (ppcVar22 != (char **)0x0) {
          pcVar14 = "CURLOPT_SASL_AUTHZID";
          ppcVar19 = param_2;
          ppcVar24 = ppcVar11;
          ppcVar26 = ppcVar22;
          ppcVar31 = (char **)pcVar20;
          auVar30 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_SASL_AUTHZID",0x2831,ppcVar22,
                                pcVar20);
          ppcVar28 = (char **)(SUB168(auVar30,0) & 0xffffffff);
        }
        ppcVar34 = ppcVar19;
        if (*(char *)(param_2 + 0x36) != '\0') {
          ppcVar26 = (char **)0xda;
          pcVar14 = "CURLOPT_SASL_IR";
          ppcVar34 = param_2;
          ppcVar24 = ppcVar19;
          auVar30 = tool_setopt(ppcVar11,0,param_1);
          ppcVar28 = (char **)(SUB168(auVar30,0) & 0xffffffff);
        }
        ppcVar19 = SUB168(auVar30 >> 0x40,0);
        if (*(char *)(param_2 + 0x91) != '\0') {
          ppcVar26 = (char **)0xe1;
          pcVar14 = "CURLOPT_SSL_ENABLE_NPN";
          ppcVar34 = param_2;
          ppcVar24 = ppcVar11;
          ppcVar19 = SUB168(auVar30,0);
          uVar8 = tool_setopt(ppcVar11,0,param_1);
          ppcVar28 = (char **)(ulong)uVar8;
        }
        if (*(char *)((long)param_2 + 0x489) != '\0') {
          ppcVar26 = (char **)0xe2;
          pcVar14 = "CURLOPT_SSL_ENABLE_ALPN";
          ppcVar34 = param_2;
          ppcVar24 = ppcVar11;
          uVar8 = tool_setopt(ppcVar11,0,param_1);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_26;
        }
        ppcVar22 = (char **)param_2[0x92];
        if (ppcVar22 != (char **)0x0) {
          if (*(char *)(param_2 + 0x93) == '\0') {
            pcVar20 = "CURLOPT_UNIX_SOCKET_PATH";
            uVar33 = 0x27f7;
            pcVar14 = (char *)ppcVar26;
          }
          else {
            uVar33 = 0x2818;
            pcVar20 = "CURLOPT_ABSTRACT_UNIX_SOCKET";
            pcVar14 = (char *)ppcVar31;
          }
          ppcVar34 = param_2;
          ppcVar24 = ppcVar22;
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,pcVar20,uVar33,ppcVar22,pcVar14);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_14;
        }
        ppcVar22 = (char **)param_2[0xe];
        if (ppcVar22 != (char **)0x0) {
          pcVar14 = "CURLOPT_DEFAULT_PROTOCOL";
          ppcVar24 = ppcVar11;
          ppcVar34 = ppcVar22;
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_DEFAULT_PROTOCOL",0x27fe,ppcVar22,
                              ppcVar19);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_15;
        }
        if (_DAT_001395e0 < (double)param_2[0x94]) {
          pcVar14 = "CURLOPT_EXPECT_100_TIMEOUT_MS";
          ppcVar34 = param_2;
          ppcVar24 = ppcVar11;
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_EXPECT_100_TIMEOUT_MS",0xe3,
                              (long)((double)param_2[0x94] * DAT_001395d8),pcVar13);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_16;
        }
        if (*(char *)(param_2 + 0x8a) != '\0') {
          ppcVar34 = param_2;
          ppcVar24 = ppcVar11;
          ppcVar31 = (char **)pcVar14;
          uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_TFTP_NO_OPTIONS",0xf2,1,pcVar14);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_25;
        }
        if (param_2[0x97] != (char *)0xc8) {
          ppcVar24 = ppcVar34;
          uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS",0x10f,
                              param_2[0x97],ppcVar34);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_17;
        }
        if (*(char *)(param_2 + 0x98) != '\0') {
          ppcVar24 = ppcVar11;
          ppcVar19 = ppcVar28;
          uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_HAPROXYPROTOCOL",0x112,1,ppcVar28)
          ;
          ppcVar28 = (char **)(ulong)uVar8;
        }
        if (*(char *)((long)param_2 + 0x4c1) != '\0') {
          ppcVar24 = ppcVar11;
          uVar8 = tool_setopt(ppcVar11,0,param_1,param_2,"CURLOPT_DISALLOW_USERNAME_IN_URL",0x116,1,
                              ppcVar31);
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_28;
        }
        if (param_2[5] != (char *)0x0) {
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_ALTSVC",0x282f,param_2[5],ppcVar24
                             );
          ppcVar28 = (char **)(ulong)uVar8;
          ppcVar19 = extraout_RDX_18;
        }
        if (param_2[6] != (char *)0x0) {
          uVar8 = tool_setopt(ppcVar11,1,param_1,param_2,"CURLOPT_HSTS",0x283c,param_2[6],ppcVar19);
          ppcVar28 = (char **)(ulong)uVar8;
        }
        pcVar13 = (char *)0x3e8;
        if (param_2[0x83] != (char *)0x0) {
          pcVar13 = (char *)((long)param_2[0x83] * 1000);
        }
        pcVar20 = param_2[0x81];
        ppcVar12[5] = pcVar13;
        ppcVar12[6] = pcVar13;
        ppcVar12[4] = pcVar20;
        auVar30 = tvnow();
        ppcVar12[7] = SUB168(auVar30,0);
        pcVar13 = param_2[0xa7];
        ppcVar12[8] = SUB168(auVar30 >> 0x40,0);
        if (pcVar13 + 1 < pcVar21) {
          param_2[0xa7] = pcVar13 + 1;
        }
        else {
          param_2[0xa7] = (char *)0x0;
          param_2[0xa6] = (char *)0x0;
          glob_cleanup(param_2[0xa0]);
          param_2[0xa5] = param_2[0xa5] + 1;
          param_2[0xa0] = (char *)0x0;
          free(param_2[0xa3]);
          param_2[0xa3] = (char *)0x0;
        }
        free(param_2[0xa1]);
        cVar6 = *param_4;
        param_2[0xa1] = (char *)0x0;
        if ((cVar6 == '\x01') && ((int)ppcVar28 == 0)) goto LAB_001191d0;
        goto LAB_00117653;
      }
    }
LAB_00118f52:
    free(param_2[0xa1]);
    param_2[0xa1] = (char *)0x0;
  }
  else {
    pcVar13 = ppcVar12[9];
    pcVar14 = strstr(pcVar13,"://");
    pcVar20 = pcVar14 + 3;
    if (pcVar14 == (char *)0x0) {
      pcVar20 = pcVar13;
    }
    pcVar20 = strrchr(pcVar20,0x2f);
    if (pcVar20 == (char *)0x0) {
      pcVar13 = (char *)curl_maprintf("%s/?%s",pcVar13,local_158);
    }
    else {
      pcVar20 = strchr(pcVar20,0x3f);
      pcVar13 = (char *)curl_maprintf("%s%c%s",pcVar13,(-(pcVar20 == (char *)0x0) & 0x19U) + 0x26,
                                      local_158);
    }
    if (pcVar13 != (char *)0x0) {
      free(ppcVar12[9]);
      ppcVar12[9] = pcVar13;
      goto LAB_00117b79;
    }
LAB_00117634:
    ppcVar28 = (char **)0x1b;
    free(param_2[0xa1]);
    param_2[0xa1] = (char *)0x0;
  }
LAB_00117653:
  *param_4 = '\0';
LAB_00117656:
  single_transfer_cleanup(param_2);
LAB_0011765e:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return ppcVar28;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}