undefined8 tool_debug_cb(undefined8 param_1,ulong param_2,void *param_3,ulong param_4,long param_5)

{
  int iVar1;
  FILE *pFVar2;
  FILE *pFVar3;
  uint uVar4;
  ulong uVar5;
  tm *ptVar6;
  FILE *__s;
  char *pcVar7;
  char cVar8;
  long lVar9;
  long lVar10;
  void *__ptr;
  ulong uVar11;
  size_t __size;
  long in_FS_OFFSET;
  bool bVar12;
  undefined auVar13 [16];
  undefined8 uVar14;
  ulong local_78;
  long local_60;
  undefined local_58 [24];
  long local_40;
  
  param_2 = param_2 & 0xffffffff;
  lVar9 = *(long *)(param_5 + 0x4d8);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  pFVar2 = *(FILE **)(lVar9 + 8);
  if (*(char *)(lVar9 + 0x30) == '\0') {
    __s = *(FILE **)(lVar9 + 0x20);
    local_58[0] = 0;
    pFVar3 = stdout;
  }
  else {
    auVar13 = tvnow();
    if (known_offset_4 == 0) {
      local_60 = time((time_t *)0x0);
      known_offset_4 = 1;
      epoch_offset_3 = local_60 - SUB168(auVar13,0);
    }
    else {
      local_60 = epoch_offset_3 + SUB168(auVar13,0);
    }
    uVar14 = 0x10c684;
    ptVar6 = localtime(&local_60);
    curl_msnprintf(local_58,0x14,"%02d:%02d:%02d.%06ld ",ptVar6->tm_hour,ptVar6->tm_min,
                   ptVar6->tm_sec,SUB168(auVar13 >> 0x40,0),uVar14);
    __s = *(FILE **)(lVar9 + 0x20);
    pFVar3 = stdout;
  }
  stdout = pFVar3;
  if (__s == (FILE *)0x0) {
    pcVar7 = *(char **)(lVar9 + 0x18);
    if ((*pcVar7 == '-') && (pcVar7[1] == '\0')) {
      *(FILE **)(lVar9 + 0x20) = pFVar3;
      __s = pFVar3;
    }
    else if ((*pcVar7 == '%') && (pcVar7[1] == '\0')) {
      __s = *(FILE **)(lVar9 + 8);
      *(FILE **)(lVar9 + 0x20) = __s;
    }
    else {
      __s = fopen(pcVar7,"w");
      *(undefined *)(lVar9 + 0x28) = 1;
      *(FILE **)(lVar9 + 0x20) = __s;
    }
    if ((__s == (FILE *)0x0) && (__s = pFVar2, pFVar2 == (FILE *)0x0)) {
      warnf(lVar9,"Failed to create/open output");
      goto switchD_0010c4c8_caseD_7;
    }
  }
  iVar1 = *(int *)(lVar9 + 0x2c);
  uVar4 = (uint)param_2;
  if (iVar1 == 3) {
    if (uVar4 == 2) {
      bVar12 = false;
      if (param_4 != 0) {
        cVar8 = newl_2;
        if (param_4 == 1) {
          __size = 1;
          __ptr = param_3;
        }
        else {
          lVar9 = 0;
          lVar10 = 0;
          do {
            pcVar7 = (char *)((long)param_3 + lVar10);
            lVar10 = lVar10 + 1;
            if (*pcVar7 == '\n') {
              if (cVar8 == '\0') {
                curl_mfprintf(__s,&DAT_00123091,local_58,">");
              }
              fwrite((void *)((long)param_3 + lVar9),lVar10 - lVar9,1,__s);
              newl_2 = '\0';
              cVar8 = '\0';
              lVar9 = lVar10;
            }
          } while (param_4 - 1 != lVar10);
          __size = param_4 - lVar9;
          __ptr = (void *)((long)param_3 + lVar9);
        }
        if (cVar8 == '\0') {
          curl_mfprintf(__s,&DAT_00123091,local_58,">");
        }
        fwrite(__ptr,__size,1,__s);
        bVar12 = *(char *)((long)param_3 + (param_4 - 1)) != '\n';
      }
    }
    else {
      if (2 < uVar4) {
        if (uVar4 - 3 < 4) {
          if ((traced_data_0 == '\0') &&
             ((*(char *)(lVar9 + 6) == '\0' || ((stderr != __s && (stdout != __s)))))) {
            if (newl_2 == '\0') {
              curl_mfprintf(__s,&DAT_00123091,local_58,*(undefined8 *)(s_infotype_1 + param_2 * 8));
            }
            curl_mfprintf(__s,"[%zu bytes data]\n",param_4);
            newl_2 = '\0';
            traced_data_0 = '\x01';
          }
        }
        else {
          newl_2 = '\0';
          traced_data_0 = '\0';
        }
        goto switchD_0010c4c8_caseD_7;
      }
      if (newl_2 == '\0') {
        curl_mfprintf(__s,&DAT_00123091,local_58,*(undefined8 *)(s_infotype_1 + param_2 * 8));
      }
      fwrite(param_3,param_4,1,__s);
      bVar12 = false;
      if (param_4 != 0) {
        bVar12 = *(char *)((long)param_3 + (param_4 - 1)) != '\n';
      }
    }
    traced_data_0 = '\0';
    newl_2 = bVar12;
    goto switchD_0010c4c8_caseD_7;
  }
  switch(param_2) {
  case 0:
    curl_mfprintf(__s,"%s== Info: %.*s",local_58,param_4 & 0xffffffff,param_3);
    goto switchD_0010c4c8_caseD_7;
  case 1:
    pcVar7 = "<= Recv header";
    break;
  case 2:
    pcVar7 = "=> Send header";
    break;
  case 3:
    pcVar7 = "<= Recv data";
    break;
  case 4:
    pcVar7 = "=> Send data";
    break;
  case 5:
    pcVar7 = "<= Recv SSL data";
    break;
  case 6:
    pcVar7 = "=> Send SSL data";
    break;
  default:
    goto switchD_0010c4c8_caseD_7;
  }
  uVar4 = 0x10;
  if (iVar1 == 2) {
    uVar4 = 0x40;
  }
  curl_mfprintf(__s,"%s%s, %zu bytes (0x%zx)\n",local_58,pcVar7,param_4,param_4);
  local_78 = 0;
  uVar5 = (ulong)uVar4;
  if (param_4 != 0) {
    do {
      curl_mfprintf(__s,"%04zx: ",local_78);
      if (iVar1 == 1) {
        uVar11 = local_78;
        do {
          if (uVar11 < param_4) {
            curl_mfprintf(__s,"%02x ",*(undefined *)((long)param_3 + uVar11));
          }
          else {
            fwrite(&DAT_001230e0,1,3,__s);
          }
          uVar11 = uVar11 + 1;
        } while (uVar5 + local_78 != uVar11);
      }
      uVar11 = local_78;
      if (local_78 < param_4) {
        do {
          cVar8 = *(char *)((long)param_3 + uVar11);
          if (iVar1 == 2) {
            if (uVar11 + 1 < param_4) {
              if (cVar8 == '\r') {
                if (*(char *)((long)param_3 + uVar11 + 1) == '\n') {
                  local_78 = (uVar11 - uVar5) + 2;
                  break;
                }
              }
              else if ((byte)(cVar8 - 0x20U) < 0x60) goto LAB_0010c5c3;
LAB_0010c7ab:
              cVar8 = '.';
            }
            else if (0x5f < (byte)(cVar8 - 0x20U)) goto LAB_0010c7ab;
LAB_0010c5c3:
            curl_mfprintf(__s,&DAT_0013a3c0,cVar8);
            if (((uVar11 + 2 < param_4) && (*(char *)((long)param_3 + uVar11 + 1) == '\r')) &&
               (*(char *)((long)param_3 + uVar11 + 2) == '\n')) {
              local_78 = (uVar11 - uVar5) + 3;
              break;
            }
          }
          else if ((byte)(cVar8 - 0x20U) < 0x60) {
            curl_mfprintf(__s,&DAT_0013a3c0);
          }
          else {
            curl_mfprintf(__s,&DAT_0013a3c0,0x2e);
          }
          if ((uVar11 == (uVar5 - 1) + local_78) || (uVar11 = uVar11 + 1, param_4 <= uVar11)) break;
        } while( true );
      }
      fputc(10,__s);
      local_78 = local_78 + uVar5;
    } while (local_78 < param_4);
  }
  fflush(__s);
switchD_0010c4c8_caseD_7:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}