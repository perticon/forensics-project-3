static int writeTime(FILE *stream, const struct writeoutvar *wovar,
                     struct per_transfer *per, CURLcode per_result,
                     bool use_json)
{
  bool valid = false;
  curl_off_t us = 0;

  (void)per;
  (void)per_result;
  DEBUGASSERT(wovar->writefunc == writeTime);

  if(wovar->ci) {
    if(!curl_easy_getinfo(per->curl, wovar->ci, &us))
      valid = true;
  }
  else {
    DEBUGASSERT(0);
  }

  if(valid) {
    curl_off_t secs = us / 1000000;
    us %= 1000000;

    if(use_json)
      fprintf(stream, "\"%s\":", wovar->name);

    fprintf(stream, "%" CURL_FORMAT_CURL_OFF_TU
            ".%06" CURL_FORMAT_CURL_OFF_TU, secs, us);
  }
  else {
    if(use_json)
      fprintf(stream, "\"%s\":null", wovar->name);
  }

  return 1; /* return 1 if anything was written */
}