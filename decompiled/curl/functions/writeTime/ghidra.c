writeTime(undefined8 param_1,undefined8 *param_2,long param_3,undefined8 param_4,char param_5)

{
  long lVar1;
  int iVar2;
  long in_FS_OFFSET;
  long local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = 0;
  if (*(int *)((long)param_2 + 0xc) != 0) {
    iVar2 = curl_easy_getinfo(*(undefined8 *)(param_3 + 0x18),*(int *)((long)param_2 + 0xc),
                              &local_38);
    if (iVar2 == 0) {
      lVar1 = local_38 / 1000000;
      local_38 = local_38 % 1000000;
      if (param_5 != '\0') {
        curl_mfprintf(param_1,"\"%s\":",*param_2);
      }
      curl_mfprintf(param_1,"%lu.%06lu",lVar1,local_38);
      goto LAB_00120992;
    }
  }
  if (param_5 != '\0') {
    curl_mfprintf(param_1,"\"%s\":null",*param_2);
  }
LAB_00120992:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}