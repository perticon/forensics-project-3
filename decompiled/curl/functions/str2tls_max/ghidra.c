undefined8 str2tls_max(undefined8 *param_1,char *param_2)

{
  int iVar1;
  long lVar2;
  undefined1 *puVar3;
  
  if (param_2 == (char *)0x0) {
    return 3;
  }
  puVar3 = tls_max_array_0;
  lVar2 = 0;
  iVar1 = strcmp(param_2,"default");
  while( true ) {
    if (iVar1 == 0) {
      *param_1 = *(undefined8 *)(tls_max_array_0 + lVar2 * 0x10 + 8);
      return 0;
    }
    lVar2 = lVar2 + 1;
    puVar3 = (undefined1 *)((long)puVar3 + 0x10);
    if (lVar2 == 5) break;
    iVar1 = strcmp(param_2,*(char **)puVar3);
  }
  return 4;
}