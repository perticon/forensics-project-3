void tool_setopt_slist(undefined8 param_1,long param_2,undefined8 param_3,undefined4 param_4,
                      long param_5)

{
  int iVar1;
  long in_FS_OFFSET;
  undefined4 local_24;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = curl_easy_setopt(param_1,param_4,param_5);
  if (((*(long *)(param_2 + 0x38) != 0) && (param_5 != 0)) && (iVar1 == 0)) {
    iVar1 = libcurl_generate_slist(param_5,&local_24);
    if (iVar1 == 0) {
      easysrc_addf(&easysrc_code,"curl_easy_setopt(hnd, %s, slist%d);",param_3,local_24);
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}