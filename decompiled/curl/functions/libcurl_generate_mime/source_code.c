static CURLcode libcurl_generate_mime(CURL *curl,
                                      struct GlobalConfig *config,
                                      struct tool_mime *toolmime,
                                      int *mimeno)
{
  CURLcode ret = CURLE_OK;

  /* May need several mime variables, so invent name. */
  *mimeno = ++easysrc_mime_count;
  DECL1("curl_mime *mime%d;", *mimeno);
  DATA1("mime%d = NULL;", *mimeno);
  CODE1("mime%d = curl_mime_init(hnd);", *mimeno);
  CLEAN1("curl_mime_free(mime%d);", *mimeno);
  CLEAN1("mime%d = NULL;", *mimeno);

  if(toolmime->subparts) {
    DECL1("curl_mimepart *part%d;", *mimeno);
    ret = libcurl_generate_mime_part(curl, config,
                                     toolmime->subparts, *mimeno);
  }

nomem:
  return ret;
}