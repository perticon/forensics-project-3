void customrequest_helper(long param_1,uint param_2,long param_3)

{
  char *pcVar1;
  int iVar2;
  long in_FS_OFFSET;
  char *local_58 [4];
  char *local_38;
  undefined *local_30;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_58[0] = "GET";
  local_58[1] = "GET";
  local_58[2] = &DAT_001258f6;
  local_58[3] = "POST";
  local_38 = "POST";
  local_30 = &DAT_001258fb;
  if (param_3 != 0) {
    pcVar1 = local_58[param_2];
    iVar2 = curl_strequal(param_3,pcVar1);
    if (iVar2 != 0) {
      if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
        notef(*(undefined8 *)(param_1 + 0x4d8),
              "Unnecessary use of -X or --request, %s is already inferred.\n",pcVar1);
        return;
      }
      goto LAB_00115fcd;
    }
    iVar2 = curl_strequal(param_3,"head");
    if (iVar2 != 0) {
      if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
        warnf(*(undefined8 *)(param_1 + 0x4d8),
              "Setting custom HTTP method to HEAD with -X/--request may not work the way you want. Consider using -I/--head instead.\n"
             );
        return;
      }
      goto LAB_00115fcd;
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_00115fcd:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}