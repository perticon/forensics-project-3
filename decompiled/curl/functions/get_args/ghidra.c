undefined8 get_args(long param_1,undefined8 param_2)

{
  char cVar1;
  undefined8 uVar2;
  long lVar3;
  char *pcVar4;
  bool bVar5;
  
  bVar5 = *(long *)(param_1 + 0x4e8) == 0;
  if (*(char *)(param_1 + 0x31b) == '\0') {
LAB_0011d133:
    if (((*(long *)(param_1 + 0x128) != 0) && (*(long *)(param_1 + 0x480) == 0)) &&
       (uVar2 = checkpasswd("host",param_2,bVar5,param_1 + 0x128), (int)uVar2 != 0)) {
      return uVar2;
    }
    if ((*(long *)(param_1 + 0x168) != 0) &&
       (uVar2 = checkpasswd("proxy",param_2,bVar5,param_1 + 0x168), (int)uVar2 != 0)) {
      return uVar2;
    }
    if (*(long *)(param_1 + 8) == 0) {
      pcVar4 = strdup("curl/7.84.0-DEV");
      *(char **)(param_1 + 8) = pcVar4;
      if (pcVar4 == (char *)0x0) {
        errorf(*(undefined8 *)(param_1 + 0x4d8),"out of memory\n");
        goto LAB_0011d230;
      }
    }
    uVar2 = 0;
  }
  else {
    cVar1 = inlist(*(undefined8 *)(param_1 + 0x378),"Content-Type");
    lVar3 = *(long *)(param_1 + 0x378);
    if (cVar1 != '\0') {
LAB_0011d1c1:
      cVar1 = inlist(lVar3,"Accept");
      if (cVar1 == '\0') {
        lVar3 = curl_slist_append(*(undefined8 *)(param_1 + 0x378),"Accept: application/json");
        if (lVar3 == 0) goto LAB_0011d230;
        *(long *)(param_1 + 0x378) = lVar3;
      }
      goto LAB_0011d133;
    }
    lVar3 = curl_slist_append(lVar3,"Content-Type: application/json");
    if (lVar3 != 0) {
      *(long *)(param_1 + 0x378) = lVar3;
      goto LAB_0011d1c1;
    }
LAB_0011d230:
    uVar2 = 0x1b;
  }
  return uVar2;
}