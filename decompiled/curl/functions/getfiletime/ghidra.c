__time_t getfiletime(char *param_1,undefined8 param_2)

{
  int iVar1;
  int *piVar2;
  char *pcVar3;
  long in_FS_OFFSET;
  stat sStack184;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = stat(param_1,&sStack184);
  if (iVar1 == -1) {
    piVar2 = __errno_location();
    sStack184.st_mtim.tv_sec = -1;
    if (*piVar2 != 2) {
      pcVar3 = strerror(*piVar2);
      warnf(param_2,"Failed to get filetime: %s\n",pcVar3);
    }
  }
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return sStack184.st_mtim.tv_sec;
}