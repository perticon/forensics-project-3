void** libcurl_generate_slist(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** rbp6;
    void** rbx7;
    int64_t rax8;
    void** edx9;
    void** eax10;
    void** r12d11;
    void** eax12;
    void** eax13;
    void** rdx14;
    void** eax15;
    void** rdi16;
    void** rax17;
    void** eax18;

    *reinterpret_cast<int32_t*>(&r15_5) = 0;
    *reinterpret_cast<int32_t*>(&r15_5 + 4) = 0;
    rbp6 = rsi;
    rbx7 = rdi;
    *reinterpret_cast<void***>(&rax8) = easysrc_slist_count;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    edx9 = reinterpret_cast<void**>(static_cast<uint32_t>(rax8 + 1));
    easysrc_slist_count = edx9;
    *reinterpret_cast<void***>(rsi) = edx9;
    eax10 = easysrc_addf(0x44090, "struct curl_slist *slist%d;");
    r12d11 = eax10;
    if (eax10 || ((eax12 = easysrc_addf(0x44088, "slist%d = NULL;", 0x44088, "slist%d = NULL;"), r12d11 = eax12, !!eax12) || ((eax13 = easysrc_addf(0x44070, "curl_slist_free_all(slist%d);", 0x44070, "curl_slist_free_all(slist%d);"), r12d11 = eax13, !!eax13) || ((rdx14 = *reinterpret_cast<void***>(rbp6), *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0, eax15 = easysrc_addf(0x44070, "slist%d = NULL;", 0x44070, "slist%d = NULL;"), r12d11 = eax15, !!eax15) || !rbx7)))) {
        addr_1e784_2:
        fun_bde0(r15_5, r15_5);
    } else {
        do {
            fun_bde0(r15_5, r15_5);
            rdi16 = *reinterpret_cast<void***>(rbx7);
            rax17 = c_escape(rdi16, 0xffffffffffffffff, rdx14, rdi16, 0xffffffffffffffff, rdx14);
            r15_5 = rax17;
            if (!rax17) 
                goto addr_1e85c_5;
            rdx14 = *reinterpret_cast<void***>(rbp6);
            *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
            eax18 = easysrc_addf(0x44088, "slist%d = curl_slist_append(slist%d, \"%s\");");
            r12d11 = eax18;
        } while (!eax18 && (rbx7 = *reinterpret_cast<void***>(rbx7 + 8), !!rbx7));
        goto addr_1e784_2;
    }
    addr_1e78c_7:
    return r12d11;
    addr_1e85c_5:
    r12d11 = reinterpret_cast<void**>(27);
    goto addr_1e78c_7;
}