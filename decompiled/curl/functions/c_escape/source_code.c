static char *c_escape(const char *str, curl_off_t len)
{
  const char *s;
  unsigned char c;
  char *escaped, *e;
  unsigned int cutoff = 0;

  if(len == ZERO_TERMINATED)
    len = strlen(str);

  if(len > MAX_STRING_LENGTH_OUTPUT) {
    /* cap ridiculously long strings */
    len = MAX_STRING_LENGTH_OUTPUT;
    cutoff = 3;
  }

  /* Allocate space based on worst-case */
  escaped = malloc(4 * (size_t)len + 1 + cutoff);
  if(!escaped)
    return NULL;

  e = escaped;
  for(s = str; len; s++, len--) {
    c = *s;
    if(c == '\n') {
      strcpy(e, "\\n");
      e += 2;
    }
    else if(c == '\r') {
      strcpy(e, "\\r");
      e += 2;
    }
    else if(c == '\t') {
      strcpy(e, "\\t");
      e += 2;
    }
    else if(c == '\\') {
      strcpy(e, "\\\\");
      e += 2;
    }
    else if(c == '"') {
      strcpy(e, "\\\"");
      e += 2;
    }
    else if(c == '?') {
      /* escape question marks as well, to prevent generating accidental
         trigraphs */
      strcpy(e, "\\?");
      e += 2;
    }
    else if(!isprint(c)) {
      msnprintf(e, 5, "\\x%02x", (unsigned)c);
      e += 4;
    }
    else
      *e++ = c;
  }
  while(cutoff--)
    *e++ = '.';
  *e = '\0';
  return escaped;
}