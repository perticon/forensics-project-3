undefined2 * c_escape(byte *param_1,size_t param_2)

{
  byte *pbVar1;
  byte bVar2;
  undefined2 *puVar3;
  ushort **ppuVar4;
  undefined2 *__s;
  undefined2 *puVar5;
  int local_40;
  uint local_3c;
  
  if (param_2 == 0xffffffffffffffff) {
    param_2 = strlen((char *)param_1);
  }
  if ((long)param_2 < 0x7d1) {
    puVar3 = (undefined2 *)malloc(param_2 * 4 + 1);
    if (puVar3 == (undefined2 *)0x0) {
      return (undefined2 *)0x0;
    }
    puVar5 = puVar3;
    if (param_2 == 0) goto LAB_0011e660;
    local_3c = 0xffffffff;
    local_40 = 0;
  }
  else {
    puVar3 = (undefined2 *)malloc(0x1f44);
    if (puVar3 == (undefined2 *)0x0) {
      return (undefined2 *)0x0;
    }
    local_3c = 2;
    param_2 = 2000;
    local_40 = 3;
  }
  pbVar1 = param_1 + param_2;
  puVar5 = puVar3;
  do {
    while( true ) {
      bVar2 = *param_1;
      if (bVar2 != 10) break;
      param_1 = param_1 + 1;
      *(undefined *)(puVar5 + 1) = 0;
      __s = puVar5 + 1;
      *puVar5 = 0x6e5c;
      puVar5 = __s;
      if (param_1 == pbVar1) goto LAB_0011e63d;
    }
    if (bVar2 == 0xd) {
      *(undefined *)(puVar5 + 1) = 0;
      __s = puVar5 + 1;
      *puVar5 = 0x725c;
    }
    else if (bVar2 == 9) {
      *(undefined *)(puVar5 + 1) = 0;
      __s = puVar5 + 1;
      *puVar5 = 0x745c;
    }
    else if (bVar2 == 0x5c) {
      *(undefined *)(puVar5 + 1) = 0;
      __s = puVar5 + 1;
      *puVar5 = 0x5c5c;
    }
    else if (bVar2 == 0x22) {
      *(undefined *)(puVar5 + 1) = 0;
      __s = puVar5 + 1;
      *puVar5 = 0x225c;
    }
    else if (bVar2 == 0x3f) {
      *(undefined *)(puVar5 + 1) = 0;
      __s = puVar5 + 1;
      *puVar5 = 0x3f5c;
    }
    else {
      ppuVar4 = __ctype_b_loc();
      if ((*(byte *)((long)*ppuVar4 + (ulong)bVar2 * 2 + 1) & 0x40) == 0) {
        __s = puVar5 + 2;
        curl_msnprintf(puVar5,5,"\\x%02x",bVar2);
      }
      else {
        *(byte *)puVar5 = bVar2;
        __s = (undefined2 *)((long)puVar5 + 1);
      }
    }
    param_1 = param_1 + 1;
    puVar5 = __s;
  } while (param_1 != pbVar1);
LAB_0011e63d:
  puVar5 = __s;
  if (local_40 != 0) {
    puVar5 = (undefined2 *)((long)__s + (ulong)local_3c + 1);
    memset(__s,0x2e,(ulong)local_3c + 1);
  }
LAB_0011e660:
  *(undefined *)puVar5 = 0;
  return puVar3;
}