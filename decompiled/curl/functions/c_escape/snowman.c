void** c_escape(void** rdi, void** rsi, void** rdx, ...) {
    void** r14_4;
    void** rax5;
    void** rdi6;
    void** rax7;
    void** r12_8;
    int32_t v9;
    int32_t v10;
    void** rax11;
    void** r15_12;
    void** rbp13;
    void** r13_14;
    uint32_t ebx15;
    void** rcx16;
    void*** rax17;
    int64_t rax18;
    void* rbp19;
    void** rdi20;
    void* rbp21;

    r14_4 = rsi;
    if (reinterpret_cast<int1_t>(rsi == 0xffffffffffffffff)) {
        rax5 = fun_b9d0(rdi, rsi, rdx);
        r14_4 = rax5;
    }
    if (reinterpret_cast<signed char>(r14_4) > reinterpret_cast<signed char>(0x7d0)) {
        *reinterpret_cast<int32_t*>(&rdi6) = 0x1f44;
        *reinterpret_cast<int32_t*>(&rdi6 + 4) = 0;
        rax7 = fun_be30(0x1f44, rsi, rdx);
        r12_8 = rax7;
        if (!rax7) {
            addr_1e738_5:
            *reinterpret_cast<int32_t*>(&r12_8) = 0;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
        } else {
            v9 = 2;
            *reinterpret_cast<int32_t*>(&r14_4) = 0x7d0;
            *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
            v10 = 3;
            goto addr_1e5a4_7;
        }
    } else {
        rdi6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_4) * 4 + 1);
        rax11 = fun_be30(rdi6, rsi, rdx);
        r12_8 = rax11;
        if (!rax11) 
            goto addr_1e738_5;
        r15_12 = rax11;
        if (!r14_4) 
            goto addr_1e660_10; else 
            goto addr_1e560_11;
    }
    addr_1e664_12:
    return r12_8;
    addr_1e5a4_7:
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_4));
    r15_12 = r12_8;
    r13_14 = rdi;
    while (1) {
        ebx15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_14));
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&ebx15) == 10)) {
            if (*reinterpret_cast<void***>(&ebx15) == 13) {
                *reinterpret_cast<unsigned char*>(r15_12 + 2) = 0;
                r15_12 = r15_12 + 2;
                *reinterpret_cast<int16_t*>(r15_12 + 0xfffffffffffffffe) = 0x725c;
            } else {
                if (*reinterpret_cast<void***>(&ebx15) == 9) {
                    *reinterpret_cast<int32_t*>(&rdi6) = 0x745c;
                    *reinterpret_cast<int32_t*>(&rdi6 + 4) = 0;
                    *reinterpret_cast<unsigned char*>(r15_12 + 2) = 0;
                    r15_12 = r15_12 + 2;
                    *reinterpret_cast<int16_t*>(r15_12 + 0xfffffffffffffffe) = 0x745c;
                } else {
                    if (*reinterpret_cast<void***>(&ebx15) == 92) {
                        *reinterpret_cast<int32_t*>(&rsi) = 0x5c5c;
                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                        *reinterpret_cast<unsigned char*>(r15_12 + 2) = 0;
                        r15_12 = r15_12 + 2;
                        *reinterpret_cast<int16_t*>(r15_12 + 0xfffffffffffffffe) = 0x5c5c;
                    } else {
                        if (*reinterpret_cast<void***>(&ebx15) == 34) {
                            *reinterpret_cast<uint32_t*>(&rcx16) = 0x225c;
                            *reinterpret_cast<int32_t*>(&rcx16 + 4) = 0;
                            *reinterpret_cast<unsigned char*>(r15_12 + 2) = 0;
                            r15_12 = r15_12 + 2;
                            *reinterpret_cast<int16_t*>(r15_12 + 0xfffffffffffffffe) = 0x225c;
                        } else {
                            if (*reinterpret_cast<void***>(&ebx15) == 63) {
                                *reinterpret_cast<int32_t*>(&rdx) = 0x3f5c;
                                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                *reinterpret_cast<unsigned char*>(r15_12 + 2) = 0;
                                r15_12 = r15_12 + 2;
                                *reinterpret_cast<int16_t*>(r15_12 + 0xfffffffffffffffe) = 0x3f5c;
                            } else {
                                rax17 = fun_bce0(rdi6, rsi, rdx, rcx16);
                                *reinterpret_cast<uint32_t*>(&rax18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebx15));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
                                rdx = *rax17;
                                if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<uint64_t>(rax18 * 2) + 1) & 64)) {
                                    rdi6 = r15_12;
                                    *reinterpret_cast<uint32_t*>(&rcx16) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebx15));
                                    *reinterpret_cast<int32_t*>(&rcx16 + 4) = 0;
                                    rdx = reinterpret_cast<void**>("\\x%02x");
                                    *reinterpret_cast<int32_t*>(&rsi) = 5;
                                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                    r15_12 = r15_12 + 4;
                                    fun_ba50(rdi6, 5, "\\x%02x", rcx16);
                                } else {
                                    *reinterpret_cast<void***>(r15_12) = *reinterpret_cast<void***>(&ebx15);
                                    ++r15_12;
                                }
                            }
                        }
                    }
                }
            }
            ++r13_14;
            if (r13_14 == rbp13) 
                break;
        } else {
            ++r13_14;
            *reinterpret_cast<unsigned char*>(r15_12 + 2) = 0;
            r15_12 = r15_12 + 2;
            *reinterpret_cast<int16_t*>(r15_12 + 0xfffffffffffffffe) = 0x6e5c;
            if (r13_14 == rbp13) 
                break;
        }
    }
    if (v10) {
        *reinterpret_cast<int32_t*>(&rbp19) = v9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp19) + 4) = 0;
        rdi20 = r15_12;
        rbp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp19) + 1);
        r15_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_12) + reinterpret_cast<uint64_t>(rbp21));
        fun_ba60(rdi20, 46, rbp21, rcx16);
    }
    addr_1e660_10:
    *reinterpret_cast<void***>(r15_12) = reinterpret_cast<void**>(0);
    goto addr_1e664_12;
    addr_1e560_11:
    v9 = -1;
    v10 = 0;
    goto addr_1e5a4_7;
}