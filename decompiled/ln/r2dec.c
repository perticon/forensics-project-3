#include <stdint.h>

/* /tmp/tmp0rcdmt2j @ 0x3110 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp0rcdmt2j @ 0x5ef0 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_raw_hasher ( const * data, size_t n) {
    rdi = data;
    rsi = n;
    /* size_t raw_hasher( const * data,size_t n); */
    rax = rdi;
    edx = 0;
    rax = rotate_right64 (rax, 3);
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x5f10 */
 
int64_t dbg_raw_comparator ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    /* _Bool raw_comparator( const * a, const * b); */
    __asm ("loopne 0x5ee3");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    return __asm ("loopne 0x5f09");
}

/* /tmp/tmp0rcdmt2j @ 0x5f20 */
 
int64_t dbg_check_tuning (Hash_table * table) {
    rdi = table;
    /* _Bool check_tuning(Hash_table * table); */
    __asm ("loopne 0x5ef3");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("loopne 0x5f0b");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rdx + rsi*2 + 0x29720000)) += ch;
    __asm ("addss xmm1, dword [0x0000d1f4]");
    xmm2 = *((rax + 4));
    __asm ("comiss xmm2, xmm1");
    if (*((rdx + rsi*2 + 0x29720000)) > 0) {
        xmm3 = *(0x0000d200);
        __asm ("comiss xmm3, xmm2");
        if (*((rdx + rsi*2 + 0x29720000)) < 0) {
            goto label_0;
        }
        __asm ("comiss xmm0, xmm1");
        eax = 1;
        if (*((rdx + rsi*2 + 0x29720000)) > 0) {
            goto label_1;
        }
    }
label_0:
    *((rdi + 0x28)) = rdx;
    eax = 0;
    return rax;
    eax = 1;
label_1:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x5fb0 */
 
uint64_t hash_find_entry (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg_48h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    r14 = rdx;
    r13 = rsi;
    r12d = ecx;
    rsi = *((rdi + 0x10));
    rdi = r13;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13);
    if (rax >= *((rbp + 0x10))) {
        void (*0x29a0)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    *(r14) = rbx;
    rsi = *(rbx);
    if (rsi == 0) {
        goto label_2;
    }
    if (rsi == r13) {
        goto label_3;
    }
    rdi = r13;
    al = uint64_t (*rbp + 0x38)() ();
    if (al == 0) {
        goto label_4;
    }
    rax = *(rbx);
label_1:
    if (r12b == 0) {
        goto label_0;
    }
    rdx = *((rbx + 8));
    if (rdx == 0) {
        goto label_5;
    }
    __asm ("movdqu xmm0, xmmword [rdx]");
    __asm ("movups xmmword [rbx], xmm0");
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
    do {
        rsi = *(rax);
        if (rsi == r13) {
            goto label_6;
        }
        rdi = r13;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_6;
        }
        rbx = *((rbx + 8));
label_4:
        rax = *((rbx + 8));
    } while (rax != 0);
label_2:
    eax = 0;
    do {
label_0:
        return rax;
label_6:
        rdx = *((rbx + 8));
        rax = *(rdx);
    } while (r12b == 0);
    rcx = *((rdx + 8));
    *((rbx + 8)) = rcx;
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
label_5:
    *(rbx) = 0;
    goto label_0;
label_3:
    rax = rsi;
    goto label_1;
}

/* /tmp/tmp0rcdmt2j @ 0x29a0 */
 
void hash_find_entry_cold (void) {
    /* [16] -r-x section size 36370 named .text */
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x60c0 */
 
int64_t compute_bucket_size_isra_0 (uint32_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    if (sil == 0) {
        if (rdi < 0) {
            goto label_5;
        }
        xmm1 = 0;
        __asm ("cvtsi2ss xmm1, rdi");
label_2:
        __asm ("divss xmm1, xmm0");
        r8d = 0;
        __asm ("comiss xmm1, dword [0x0000d204]");
        if (rdi >= 0) {
            goto label_6;
        }
        __asm ("comiss xmm1, dword [0x0000d208]");
        if (rdi < 0) {
            goto label_7;
        }
        __asm ("subss xmm1, dword [0x0000d208]");
        __asm ("cvttss2si rdi, xmm1");
        __asm ("btc rdi, 0x3f");
    }
label_4:
    r9 = 0xaaaaaaaaaaaaaaab;
    eax = 0xa;
    if (rdi >= rax) {
        rax = rdi;
    }
    r8 = rax;
    r8 |= 1;
    if (r8 == -1) {
        goto label_1;
    }
label_0:
    rax = r8;
    rdx:rax = rax * r9;
    rax = rdx;
    rdx &= 0xfffffffffffffffe;
    rax >>= 1;
    rdx += rax;
    rax = r8;
    rax -= rdx;
    if (r8 <= 9) {
        goto label_8;
    }
    if (rax == 0) {
        goto label_9;
    }
    edi = 0x10;
    esi = 9;
    ecx = 3;
    while (r8 > rsi) {
        rdi += 8;
        if (rdx == 0) {
            goto label_9;
        }
        rcx += 2;
        rax = r8;
        edx = 0;
        rsi += rdi;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    }
label_3:
    rax = r8;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rdx != 0) {
        goto label_10;
    }
label_9:
    r8 += 2;
    if (r8 != -1) {
        goto label_0;
    }
    do {
label_1:
        r8d = 0;
        rax = r8;
        return rax;
label_10:
        rax = r8;
        rax >>= 0x3d;
        al = (rax != 0) ? 1 : 0;
        eax = (int32_t) al;
    } while (((r8 >> 0x3c) & 1) < 0);
    if (rax != 0) {
        goto label_1;
    }
label_6:
    rax = r8;
    return rax;
label_5:
    rax = rdi;
    edi &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rdi;
    __asm ("cvtsi2ss xmm1, rax");
    __asm ("addss xmm1, xmm1");
    goto label_2;
label_8:
    ecx = 3;
    goto label_3;
label_7:
    __asm ("cvttss2si rdi, xmm1");
    goto label_4;
}

/* /tmp/tmp0rcdmt2j @ 0x6200 */
 
uint64_t transfer_entries (uint32_t arg_8h, int64_t arg_18h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r14 = rdi;
    r12d = edx;
    rbx = *(rsi);
    if (rbx < *((rsi + 8))) {
        goto label_3;
    }
    goto label_6;
    do {
label_2:
        rbx += 0x10;
        if (*((rbp + 8)) <= rbx) {
            goto label_6;
        }
label_3:
        r15 = *(rbx);
    } while (r15 == 0);
    r13 = *((rbx + 8));
    if (r13 == 0) {
        goto label_7;
    }
    rsi = *((r14 + 0x10));
    goto label_8;
label_0:
    rcx = *((rax + 8));
    *((r13 + 8)) = rcx;
    *((rax + 8)) = r13;
    if (rdx == 0) {
        goto label_9;
    }
label_1:
    r13 = rdx;
label_8:
    r15 = *(r13);
    rdi = *(r13);
    rax = uint64_t (*r14 + 0x30)() ();
    rsi = *((r14 + 0x10));
    if (rax >= rsi) {
        void (*0x29a5)() ();
    }
    rax <<= 4;
    rax += *(r14);
    rdx = *((r13 + 8));
    if (*(rax) != 0) {
        goto label_0;
    }
    *(rax) = r15;
    rax = *((r14 + 0x48));
    *((r14 + 0x18))++;
    *(r13) = 0;
    *((r13 + 8)) = rax;
    *((r14 + 0x48)) = r13;
    if (rdx != 0) {
        goto label_1;
    }
label_9:
    r15 = *(rbx);
label_7:
    *((rbx + 8)) = 0;
    if (r12b != 0) {
        goto label_2;
    }
    rsi = *((r14 + 0x10));
    rdi = r15;
    rax = uint64_t (*r14 + 0x30)() ();
    r13 = rax;
    if (rax >= *((r14 + 0x10))) {
        void (*0x29a5)() ();
    }
    r13 <<= 4;
    r13 += *(r14);
    if (*(r13) == 0) {
        goto label_10;
    }
    rax = *((r14 + 0x48));
    if (rax == 0) {
        goto label_11;
    }
    rdx = *((rax + 8));
    *((r14 + 0x48)) = rdx;
label_5:
    rdx = *((r13 + 8));
    *(rax) = r15;
    *((rax + 8)) = rdx;
    *((r13 + 8)) = rax;
label_4:
    *(rbx) = 0;
    rbx += 0x10;
    *((rbp + 0x18))--;
    if (*((rbp + 8)) > rbx) {
        goto label_3;
    }
label_6:
    eax = 1;
    return rax;
label_10:
    *(r13) = r15;
    *((r14 + 0x18))++;
    goto label_4;
label_11:
    rax = malloc (0x10);
    if (rax != 0) {
        goto label_5;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x29a5 */
 
void transfer_entries_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29aa */
 
void hash_lookup_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x2510 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmp0rcdmt2j @ 0x29af */
 
void hash_get_first_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29b5 */
 
void hash_get_next_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29ba */
 
void hash_rehash_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29bf */
 
void hash_insert_if_absent_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x7330 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000d267;
        rdx = 0x0000d258;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000d25f;
        rdx = 0x0000d261;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000d263;
    rdx = 0x0000d25c;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xb300 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x2820 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmp0rcdmt2j @ 0x7410 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x29c4)() ();
    }
    rdx = 0x0000d2c0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xd2c0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000d26b;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000d261;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000d2ec;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xd2ec */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000d25f;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000d261;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000d25f;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000d261;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000d3ec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xd3ec */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000d4ec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xd4ec */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000d261;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000d25f;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000d25f;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000d261;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmp0rcdmt2j @ 0x29c4 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x8830 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x29c9)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x29c9 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29ce */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29d4 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29d9 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29de */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29e3 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29e8 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29ed */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29f2 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29f7 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x29fc */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x3200 */
 
uint64_t atomic_link (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    if (*(obj.symbolic_link) == 0) {
        goto label_1;
    }
    if (*(obj.relative) != 0) {
        goto label_2;
    }
    eax = symlinkat ();
    if (eax < 0) {
        goto label_3;
    }
    do {
        eax = 0;
label_0:
        return eax;
label_1:
        if (*(obj.beware_hard_dir_link) != 0) {
            goto label_2;
        }
        r8d = *(obj.logical);
        rcx = rdx;
        edx = esi;
        rsi = rdi;
        edi = 0xffffff9c;
        r8d <<= 0xa;
        eax = linkat ();
    } while (eax >= 0);
label_3:
    rax = errno_location ();
    eax = *(rax);
    return rax;
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp0rcdmt2j @ 0x3280 */
 
void dbg_do_link (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    stat source_stats;
    stat dest_stats;
    int64_t var_10h_2;
    void * var_8h;
    void * ptr;
    int64_t var_1ch;
    uint32_t var_20h;
    uint32_t var_28h;
    uint32_t var_30h;
    int64_t var_38h;
    int64_t var_b0h;
    int64_t var_b8h;
    int64_t var_c8h;
    int64_t var_148h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool do_link(char const * source,int destdir_fd,char const * dest_base,char const * dest,int link_errno); */
    r10d = r8d;
    r14 = rcx;
    r13 = rdx;
    r12 = rdi;
    ecx = *(obj.logical);
    rax = *(fs:0x28);
    *((rsp + 0x148)) = rax;
    eax = 0;
    ecx ^= 1;
    ecx = (int32_t) cl;
    ecx <<= 8;
    while (1) {
        r9d = *(obj.symbolic_link);
        if (r10d == 0) {
            r15 = dest_set;
            ebx = 0;
            if (r15 == 0) {
                goto label_17;
            }
            if (r9b == 0) {
                goto label_18;
            }
            ebx = 0;
            r15d = 0;
            if (*(obj.verbose) != 0) {
                goto label_19;
            }
label_1:
            *(rsp) = r9b;
            free (r15);
            free (rbx);
            r9d = *(rsp);
label_3:
            rax = *((rsp + 0x148));
            rax -= *(fs:0x28);
            if (rax != 0) {
                goto label_20;
            }
            eax = r9d;
            return rax;
        }
        r15d = 1;
        if (r9b == 0) {
            goto label_21;
        }
label_2:
        ebx = 0;
        if (*(obj.relative) != 0) {
            goto label_22;
        }
label_6:
        if (*(obj.remove_existing_files) == 0) {
            goto label_23;
        }
label_0:
        ecx = 0x100;
        rsi = r13;
        edi = ebp;
        *(rsp) = r10d;
        rdx = rsp + 0xb0;
        *((rsp + 8)) = rdx;
        eax = fstatat ();
        r10d = *(rsp);
        if (eax != 0) {
            goto label_24;
        }
        eax = *((rsp + 0xc8));
        rdx = *((rsp + 8));
        eax &= 0xf000;
        if (eax == 0x4000) {
            goto label_25;
        }
        *(rsp) = r10d;
        al = seen_file (*(obj.dest_set), r14, rdx, rcx, r8, r9);
        r10d = *(rsp);
        if (al != 0) {
            goto label_26;
        }
        edi = backup_type;
        eax = *(obj.remove_existing_files);
        if (edi != 0) {
            eax = *(obj.symbolic_link);
            eax ^= 1;
        }
        if (al != 0) {
            if (r15d != 0) {
                goto label_27;
            }
label_14:
            rax = *((rsp + 0xb8));
            if (*((rsp + 0x28)) == rax) {
                goto label_28;
            }
        }
label_13:
        if (r10d < 0) {
            goto label_29;
        }
        if (r10d == 0x11) {
            goto label_29;
        }
label_11:
        r9d = 1;
        r15d = 0;
        goto label_12;
        *(rsp) = ecx;
        eax = atomic_link ();
        ecx = *(rsp);
        r10d = eax;
    }
label_23:
    if (*(obj.interactive) != 0) {
        goto label_0;
    }
    r8d = backup_type;
    if (r8d != 0) {
        goto label_0;
    }
label_4:
    r9d = 0;
    r15d = 0;
label_12:
    if (*(obj.symbolic_link) != 0) {
        goto label_30;
    }
    r8d = *(obj.logical);
    r8d <<= 0xa;
    eax = force_linkat (0xffffff9c, r12, ebp, r13, r8, r9);
    r9d = eax;
label_7:
    if (r9d <= 0) {
        goto label_31;
    }
    rdx = r12;
    esi = 4;
    edi = 1;
    *((rsp + 8)) = r9d;
    rax = quotearg_n_style ();
    edi = 0;
    rdx = r14;
    esi = 4;
    *(rsp) = rax;
    rax = quotearg_n_style ();
    r9d = *((rsp + 8));
    rcx = rax;
    if (*(obj.symbolic_link) == 0) {
        goto label_32;
    }
    if (r9d != 0x24) {
        if (*(r12) != 0) {
            goto label_33;
        }
    }
    *((rsp + 0x10)) = r9d;
    edx = 5;
    *((rsp + 8)) = rcx;
label_5:
    rax = dcgettext (0, "failed to create symbolic link %s -> %s");
    rcx = *((rsp + 8));
    r9d = *((rsp + 0x10));
label_10:
    r8 = *(rsp);
    eax = 0;
    error (0, r9d, rax);
    r9d = 0;
    if (r15 == 0) {
        goto label_1;
    }
    rcx = r13;
    edx = ebp;
    rsi = r15;
    edi = ebp;
    *(rsp) = r9b;
    eax = renameat ();
    r9d = *(rsp);
    if (eax == 0) {
        goto label_1;
    }
    rsi = r14;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot un-backup %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    r9d = *(rsp);
    goto label_1;
label_21:
    rdx = rsp + 0x20;
    rsi = r12;
    edi = 0xffffff9c;
    *(rsp) = r10d;
    eax = fstatat ();
    r10d = *(rsp);
    r15d = eax;
    if (eax != 0) {
        goto label_34;
    }
    r9d = *(obj.hard_dir_link);
    if (r9b != 0) {
        goto label_2;
    }
    eax = *((rsp + 0x38));
    eax &= 0xf000;
    if (eax != 0x4000) {
        goto label_2;
    }
    rdx = r12;
    esi = 3;
    edi = 0;
    *(rsp) = r9b;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: hard link not allowed for directory");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    r9d = *(rsp);
    goto label_3;
label_31:
    r9d = *(obj.symbolic_link);
label_17:
    if (r9b == 0) {
        goto label_35;
    }
label_8:
    r9d = 1;
    if (*(obj.verbose) == 0) {
        goto label_1;
    }
    if (r15 == 0) {
        goto label_19;
    }
    r13 -= r14;
    rdx = r15;
    ebp = 0;
    if (r13 > 0) {
        strlen (r15);
        rax = xmalloc (r13 + rax + 1);
        memcpy (rax, r14, r13);
        *(rsp) = rbp;
        strcpy (rbp + r13, r15);
        rdx = *(rsp);
    }
    esi = 4;
    edi = 2;
    rax = quotearg_n_style ();
    r13 = rax;
    free (rbp);
    rcx = 0x0000c004;
    do {
        rdx = r12;
        esi = 4;
        edi = 1;
        *(rsp) = rcx;
        rax = quotearg_n_style ();
        rdx = r14;
        esi = 4;
        ebp -= ebp;
        edi = 0;
        r12 = rax;
        rax = quotearg_n_style ();
        ebp &= 0x10;
        rdx = r13;
        ebp += 0x2d;
        rcx = *((rsp + 0x10));
        r8 = rax;
        r9d = ebp;
        rsi = "%s%s%s %c> %s\n";
        edi = 1;
        eax = 0;
        printf_chk ();
        r9d = 1;
        goto label_1;
label_19:
        rcx = 0x0000d6d8;
        r13 = rcx;
    } while (1);
label_24:
    *(rsp) = r10d;
    rax = errno_location ();
    r10d = *(rsp);
    r15 = rax;
    if (*(rax) == 2) {
        goto label_4;
    }
    rsi = r14;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "failed to access %s");
    rcx = r12;
    eax = 0;
    error (0, *(r15), rax);
label_9:
    free (rbx);
    r9d = 0;
    goto label_3;
label_32:
    if (r9d == 0x1f) {
        goto label_36;
    }
    if (r9d != 0x7a) {
        if (r9d == 0x11) {
            goto label_37;
        }
        eax = r9d;
        eax &= 0xfffffffd;
        if (eax != 0x1c) {
            goto label_38;
        }
    }
label_37:
    *((rsp + 0x10)) = r9d;
    edx = 5;
    rsi = "failed to create hard link %s";
    *((rsp + 8)) = rcx;
    goto label_5;
label_22:
    rdi = r14;
    *((rsp + 0x1c)) = r10d;
    rax = dir_name ();
    esi = 2;
    rdi = rax;
    *((rsp + 0x10)) = rax;
    rax = canonicalize_filename_mode ();
    esi = 2;
    rdi = r12;
    *(rsp) = rax;
    rbx = rax;
    rax = canonicalize_filename_mode ();
    *((rsp + 8)) = rax;
    if (rbx == 0) {
        goto label_39;
    }
    if (rax == 0) {
        goto label_39;
    }
    rax = xmalloc (0x1000);
    rdx = rax;
    rbx = rax;
    al = relpath (*((rsp + 8)), *(rsp), rdx, 0x1000);
    r10d = *((rsp + 0x1c));
    if (al == 0) {
        goto label_40;
    }
    *((rsp + 0x1c)) = r10d;
    r12 = rbx;
    free (*((rsp + 0x10)));
    free (*(rsp));
    free (*((rsp + 8)));
    r10d = *((rsp + 0x1c));
    goto label_6;
label_30:
    eax = force_symlinkat (r12, ebp, r13, r9d, r10d);
    r9d = eax;
    goto label_7;
label_18:
    rdx = rsp + 0x20;
    rsi = r12;
    edi = 0xffffff9c;
    *(rsp) = rdx;
    eax = fstatat ();
    if (eax != 0) {
        goto label_34;
    }
    ebx = 0;
    r15d = 0;
    do {
        record_file (*(obj.dest_set), r14, *(rsp), rcx, r8, r9);
        goto label_8;
label_35:
        rdx = rsp + 0x20;
    } while (1);
label_34:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to access %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    r9d = 0;
    goto label_3;
label_26:
    rdx = r12;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = r14;
    esi = 4;
    edi = 0;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
label_16:
    rax = dcgettext (0, "will not overwrite just-created %s with %s");
    r8 = rbp;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_9;
label_40:
    free (rbx);
label_39:
    free (*((rsp + 0x10)));
    free (*(rsp));
    free (*((rsp + 8)));
    r10d = *((rsp + 0x1c));
    *(rsp) = r10d;
    rax = xstrdup (r12);
    r10d = *(rsp);
    r12 = rax;
    rbx = rax;
    goto label_6;
label_33:
    *((rsp + 0x10)) = r9d;
    edx = 5;
    rsi = "failed to create symbolic link %s";
    *((rsp + 8)) = rax;
    goto label_5;
label_36:
    *((rsp + 0x10)) = r9d;
    edx = 5;
    rsi = "failed to create hard link to %.0s%s";
    *((rsp + 8)) = rax;
    goto label_5;
label_38:
    edx = 5;
    *((rsp + 0x10)) = r9d;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "failed to create hard link %s => %s");
    r9d = *((rsp + 0x10));
    rcx = *((rsp + 8));
    rdx = rax;
    goto label_10;
label_29:
    r9d = *(obj.interactive);
    if (r9b != 0) {
        goto label_41;
    }
label_15:
    edx = backup_type;
    if (edx == 0) {
        goto label_11;
    }
    rsi = r13;
    edi = ebp;
    *(rsp) = r10d;
    rax = find_backup_file_name ();
    edx = ebp;
    rsi = r13;
    edi = ebp;
    rcx = rax;
    r15 = rax;
    eax = renameat ();
    r10d = *(rsp);
    r9d = 1;
    if (eax == 0) {
        goto label_12;
    }
    *((rsp + 8)) = r10d;
    rax = errno_location ();
    eax = *(rax);
    *(rsp) = eax;
    free (r15);
    r10d = *((rsp + 8));
    if (*(rsp) == 2) {
        goto label_4;
    }
    rsi = r14;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot backup %s");
    rcx = r12;
    eax = 0;
    error (0, *(rsp), rax);
    goto label_9;
label_25:
    rdx = r14;
    edi = 0;
    esi = 3;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: cannot overwrite directory");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_9;
label_27:
    rdx = rsp + 0x20;
    rdi = r12;
    *(rsp) = r10d;
    rsi = rdx;
    eax = stat ();
    r10d = *(rsp);
    if (eax != 0) {
        goto label_13;
    }
    goto label_14;
label_41:
    rsi = r14;
    edi = 4;
    *((rsp + 0x10)) = r10d;
    *((rsp + 8)) = r9b;
    rax = quotearg_style ();
    r15 = program_name;
    edx = 5;
    *(rsp) = rax;
    rax = dcgettext (0, "%s: replace %s? ");
    r8 = *(rsp);
    rcx = r15;
    rdi = stderr;
    rdx = rax;
    esi = 1;
    eax = 0;
    fprintf_chk ();
    al = yesno ();
    r9d = *((rsp + 8));
    r10d = *((rsp + 0x10));
    if (al != 0) {
        goto label_15;
    }
    *(rsp) = r9b;
    free (rbx);
    r9d = *(rsp);
    goto label_3;
label_28:
    rax = *((rsp + 0xb0));
    if (*((rsp + 0x20)) != rax) {
        goto label_13;
    }
    if (*((rsp + 0x30)) == 1) {
        goto label_42;
    }
    *(rsp) = r10d;
    al = same_nameat (0xffffff9c, r12, ebp, r13);
    r10d = *(rsp);
    if (al == 0) {
        goto label_13;
    }
label_42:
    rdx = r14;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = r12;
    esi = 4;
    edi = 0;
    rax = quotearg_n_style ();
    edx = 5;
    rsi = "%s and %s are the same file";
    r12 = rax;
    goto label_16;
label_20:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x3140 */
 
uint64_t deregister_tm_clones (void) {
    rdi = loc__edata;
    rax = loc__edata;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x3170 */
 
int64_t register_tm_clones (void) {
    rdi = loc__edata;
    rsi = loc__edata;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x31b0 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_000024e0 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp0rcdmt2j @ 0x24e0 */
 
void fcn_000024e0 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp0rcdmt2j @ 0x31f0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp0rcdmt2j @ 0x4100 */
 
int64_t dbg_try_link (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int try_link(char * dest,void * arg); */
    rax = rsi;
    edx = *((rsi + 0x10));
    rcx = rdi;
    rsi = *((rsi + 8));
    r8d = *((rax + 0x14));
    edi = *(rax);
    return linkat ();
}

/* /tmp/tmp0rcdmt2j @ 0x4120 */
 
uint64_t dbg_samedir_template (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* char * samedir_template(char const * dstname,char * buf); */
    rbx = rsi;
    rax = last_component ();
    rax -= rbp;
    rdi = rax + 9;
    r12 = rax;
    if (rdi > 0x100) {
        rax = malloc (rdi);
        rbx = rax;
        if (rax == 0) {
            goto label_0;
        }
    }
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    mempcpy ();
    rdx = "CuXXXXXX";
    *(rax) = rdx;
    edx = *(0x0000d088);
    *((rax + 8)) = dl;
    rax = rbx;
    do {
        return rax;
label_0:
        eax = 0;
    } while (1);
}

/* /tmp/tmp0rcdmt2j @ 0x4180 */
 
int64_t dbg_try_symlink (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int try_symlink(char * dest,void * arg); */
    rax = rsi;
    rdx = rdi;
    esi = *((rsi + 8));
    rdi = *(rax);
    return symlinkat ();
}

/* /tmp/tmp0rcdmt2j @ 0x4450 */
 
uint64_t dbg_buffer_or_output (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool buffer_or_output(char const * str,char ** pbuf,size_t * plen); */
    r13 = rdi;
    r14 = *(rsi);
    if (r14 != 0) {
        rbx = rsi;
        rax = strlen (rdi);
        r12 = rax;
        eax = 1;
        if (*(rbp) <= r12) {
            return rax;
        }
        eax = memcpy (r14, r13, r12 + 1);
        *(rbx) += r12;
        eax = 0;
        *(rbp) -= r12;
        return rax;
    }
    rsi = stdout;
    eax = fputs_unlocked ();
    eax = 0;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x2640 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmp0rcdmt2j @ 0x27d0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmp0rcdmt2j @ 0x2740 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmp0rcdmt2j @ 0x4ff0 */
 
int64_t dbg_canonicalize_filename_mode_stk (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_1h;
    stat st;
    scratch_buffer extra_buffer;
    scratch_buffer link_buffer;
    uint32_t var_8h;
    uint32_t var_10h;
    size_t n;
    size_t * s1;
    uint32_t var_28h;
    size_t * s2;
    int64_t var_3ch;
    int64_t var_40h;
    uint32_t var_4bh;
    signed int64_t var_4ch;
    size_t * var_50h;
    size_t * var_58h;
    int64_t var_60h;
    int64_t var_f0h;
    uint32_t var_f8h;
    int64_t var_100h;
    int64_t var_500h;
    int64_t var_508h;
    int64_t var_510h;
    int64_t var_918h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * canonicalize_filename_mode_stk(char const * name,canonicalize_mode_t can_mode,scratch_buffer * rname_buf); */
    r14d = esi;
    r14d &= 3;
    rax = *(fs:0x28);
    *((rsp + 0x918)) = rax;
    eax = 0;
    eax = r14 - 1;
    if ((eax & r14d) != 0) {
        goto label_21;
    }
    if (rdi == 0) {
        goto label_21;
    }
    if (*(rdi) == 0) {
        goto label_22;
    }
    rax = rsp + 0x100;
    ebx = esi;
    *((rdx + 8)) = section..dynsym;
    r12 = rdx;
    *((rsp + 8)) = rax;
    esi = section..dynsym;
    *((rsp + 0xf0)) = rax;
    rax = rsp + 0x510;
    *(rsp) = rax;
    *((rsp + 0x500)) = rax;
    rax = rdx + 0x10;
    *(rdx) = rax;
    r13 = rax;
    *((rsp + 0xf8)) = section..dynsym;
    *((rsp + 0x508)) = section..dynsym;
    *((rsp + 0x10)) = rax;
    if (*(rdi) == 0x2f) {
        goto label_23;
    }
    do {
        rdi = r13;
        rax = getcwd ();
        if (rax != 0) {
            goto label_24;
        }
        rax = errno_location ();
        eax = *(rax);
        if (eax == 0xc) {
            goto label_12;
        }
        if (eax != 0x22) {
            r15 = r13;
            ebx = 1;
label_4:
            rdi = *((rsp + 0xf0));
            if (rdi != *((rsp + 8))) {
                free (rdi);
            }
            rdi = *((rsp + 0x500));
            if (rdi != *(rsp)) {
                free (rdi);
            }
            if (bl != 0) {
                goto label_25;
            }
            *(r13) = 0;
            rsi -= r15;
            rax = gl_scratch_buffer_dupfree (r12, r13 + 1);
            if (rax == 0) {
                goto label_12;
            }
label_0:
            rdx = *((rsp + 0x918));
            rdx -= *(fs:0x28);
            if (rdx != 0) {
                goto label_26;
            }
            return rax;
        }
        al = gl_scratch_buffer_grow (r12);
        if (al == 0) {
            goto label_12;
        }
        r13 = *(r12);
        rsi = *((r12 + 8));
    } while (1);
label_21:
    errno_location ();
    *(rax) = 0x16;
    eax = 0;
    goto label_0;
label_1:
    if (dl == 0x2e) {
        goto label_17;
    }
label_2:
    if (*((r13 - 1)) != 0x2f) {
        *(r13) = 0x2f;
        r13++;
    }
    rax = *((rsp + 0x18));
    rsi = rax + 2;
    rax = *((r12 + 8));
    rax += r15;
    rax -= r13;
    if (rax >= rsi) {
        goto label_27;
    }
    *((rsp + 0x30)) = rbx;
    rcx = r13;
    rbx = rsi;
    r13 = r12;
    r12 = rbp;
    while (al != 0) {
        rax = *((r13 + 8));
        r15 = *(r13);
        rax -= rbp;
        rcx = r15 + rbp;
        if (rax >= rbx) {
            goto label_28;
        }
        rcx -= r15;
        al = gl_scratch_buffer_grow_preserve (r13);
    }
label_12:
    xalloc_die ();
label_23:
    *((rdx + 0x10)) = 0x2f;
    r15 = *((rsp + 0x10));
    r13 = rdx + 0x11;
label_11:
    eax = *(rbp);
    if (al == 0) {
        goto label_29;
    }
    ebx &= 4;
    rcx = rsp + 0x500;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x3c)) = ebx;
    *((rsp + 0x4b)) = 0;
    *((rsp + 0x4c)) = 0;
    *((rsp + 0x40)) = rcx;
label_3:
    if (al != 0x2f) {
        goto label_30;
    }
    do {
        edx = *((rbp + 1));
        rbp++;
    } while (dl == 0x2f);
    if (dl == 0) {
        goto label_6;
    }
    *((rsp + 0x20)) = rbp;
    do {
label_7:
        rbx = rbp;
        eax = *((rbp + 1));
        rbp++;
        if (al == 0) {
            goto label_31;
        }
    } while (al != 0x2f);
label_31:
    rcx = rbp;
    rcx -= *((rsp + 0x20));
    *((rsp + 0x18)) = rcx;
    if (rcx == 0) {
        goto label_6;
    }
    if (rcx == 1) {
        goto label_1;
    }
    if (*((rsp + 0x18)) != 2) {
        goto label_2;
    }
    if (dl != 0x2e) {
        goto label_2;
    }
    rcx = *((rsp + 0x20));
    if (*((rcx + 1)) != 0x2e) {
        goto label_2;
    }
    rdx = r15 + 1;
    if (r13 <= rdx) {
        goto label_17;
    }
    r13--;
    if (r13 <= r15) {
        goto label_17;
    }
    do {
        if (*((r13 - 1)) == 0x2f) {
            goto label_17;
        }
        r13--;
    } while (r13 != r15);
label_17:
    if (al != 0) {
        goto label_3;
    }
label_6:
    rax = r15 + 1;
    ebx = 0;
    if (r13 > rax) {
        eax = 0;
        al = (*((r13 - 1)) == 0x2f) ? 1 : 0;
        r13 -= rax;
    }
label_10:
    rdi = *((rsp + 0x28));
    if (rdi == 0) {
        goto label_4;
    }
    eax = hash_free (rdi, rsi);
    goto label_4;
label_25:
    rdi = *(r12);
    eax = 0;
    if (*((rsp + 0x10)) == rdi) {
        goto label_0;
    }
    *(rsp) = rax;
    free (rdi);
    rax = *(rsp);
    goto label_0;
label_22:
    errno_location ();
    *(rax) = 2;
    eax = 0;
    goto label_0;
label_28:
    rbx = *((rsp + 0x30));
    r12 = r13;
    r13 = rcx;
label_27:
    rdx = *((rsp + 0x18));
    rsi = *((rsp + 0x20));
    rdi = r13;
    mempcpy ();
    ecx = *((rsp + 0x3c));
    *(rax) = 0;
    r13 = rax;
    if (ecx == 0) {
        goto label_32;
    }
label_14:
    if (r14d == 2) {
        goto label_9;
    }
    eax = *(rbp);
    if (al != 0x2f) {
        goto label_33;
    }
    rdx = rbp;
    do {
label_5:
        rsi = rdx;
        ecx = *((rdx + 1));
        rdx++;
    } while (cl == 0x2f);
    rsi += 2;
    if (cl == 0) {
        goto label_34;
    }
    if (cl != 0x2e) {
        goto label_33;
    }
    ecx = *((rdx + 1));
    if (cl == 0) {
        goto label_34;
    }
    if (cl == 0x2e) {
        goto label_35;
    }
    if (cl != 0x2f) {
        goto label_33;
    }
    rdx = rsi;
    goto label_5;
label_8:
    if (al != 0) {
        goto label_3;
    }
label_13:
    edx = 0;
    ecx = 0x200;
    rsi = r15;
    edi = 0xffffff9c;
    eax = faccessat ();
    if (eax != 0) {
        goto label_36;
    }
label_9:
    eax = *((rbx + 1));
    if (al != 0) {
        goto label_3;
    }
    goto label_6;
label_30:
    *((rsp + 0x20)) = rbp;
    edx = eax;
    goto label_7;
label_35:
    edx = *((rdx + 2));
    if (dl == 0) {
        goto label_34;
    }
    if (dl == 0x2f) {
        goto label_34;
    }
label_33:
    edx = *((rsp + 0x3c));
    if (edx != 0) {
        goto label_8;
    }
    rax = errno_location ();
    if (*(rax) == 0x16) {
        goto label_9;
    }
label_36:
    if (r14d != 1) {
        goto label_15;
    }
    rax = errno_location ();
    if (*(rax) != 2) {
        goto label_15;
    }
    strspn (rbp, 0x0000d732);
    if (*((rbp + rax)) == 0) {
        goto label_9;
    }
label_15:
    ebx = 1;
    goto label_10;
label_24:
    rdi = r13;
    esi = 0;
    r15 = r13;
    rax = rawmemchr ();
    r13 = rax;
    goto label_11;
label_32:
    *((rsp + 0x18)) = rax;
    r13 = *((rsp + 0x40));
    *((rsp + 0x30)) = rbx;
    *((rsp + 0x50)) = rbp;
    do {
        rax = *((rsp + 0x508));
        rbx = *((rsp + 0x500));
        rdi = r15;
        rbp = rax - 1;
        rsi = rbx;
        rdx = rbp;
        rax = readlink ();
        if (rbp > rax) {
            goto label_37;
        }
        al = gl_scratch_buffer_grow (r13);
    } while (al != 0);
    goto label_12;
label_34:
    eax = *(obj.dir_suffix);
    *(r13) = ax;
    goto label_13;
label_37:
    r10 = rbx;
    r13 = *((rsp + 0x18));
    rbx = *((rsp + 0x30));
    r9 = rax;
    rbp = *((rsp + 0x50));
    if (rax < 0) {
        goto label_14;
    }
    if (*((rsp + 0x4c)) <= 0x13) {
        goto label_38;
    }
    rax = *((rsp + 0x20));
    if (*(rax) == 0) {
        goto label_18;
    }
    rdx = rax;
    r11 = rsp + 0x60;
    *((rsp + 0x58)) = r9;
    rdi = 0x0000d08b;
    rdx -= rbp;
    rsi = r11;
    *((rsp + 0x50)) = r10;
    rdx += r13;
    *((rsp + 0x18)) = r11;
    *(rdx) = 0;
    if (*(r15) != 0) {
        rdi = r15;
    }
    *((rsp + 0x30)) = rdx;
    eax = stat ();
    if (eax != 0) {
        goto label_15;
    }
    rax = *((rsp + 0x20));
    rdx = *((rsp + 0x30));
    r11 = *((rsp + 0x18));
    eax = *(rax);
    r10 = *((rsp + 0x50));
    r9 = *((rsp + 0x58));
    *(rdx) = al;
    if (*((rsp + 0x28)) == 0) {
        goto label_39;
    }
label_20:
    rdx = r11;
    *((rsp + 0x50)) = r9;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x18)) = r11;
    al = seen_file (*((rsp + 0x28)), *((rsp + 0x20)), rdx, rcx, r8, r9);
    r11 = *((rsp + 0x18));
    r10 = *((rsp + 0x30));
    r9 = *((rsp + 0x50));
    if (al != 0) {
        goto label_40;
    }
    *((rsp + 0x30)) = r9;
    *((rsp + 0x18)) = r10;
    record_file (*((rsp + 0x28)), *((rsp + 0x20)), r11, rcx, r8, r9);
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x30));
label_18:
    *((r10 + r9)) = 0;
    r8 = *((rsp + 0xf0));
    if (*((rsp + 0x4b)) == 0) {
        goto label_41;
    }
    rax = rbp;
    *((rsp + 0x58)) = r9;
    rax -= r8;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x30)) = r8;
    *((rsp + 0x20)) = rax;
    rax = strlen (rbp);
    r9 = *((rsp + 0x58));
    r8 = *((rsp + 0x30));
    *((rsp + 0x18)) = rax;
    r10 = *((rsp + 0x50));
    rax += r9;
    if (*((rsp + 0xf8)) > rax) {
        goto label_42;
    }
label_16:
    rbx = rsp + 0xf0;
    *((rsp + 0x58)) = rbp;
    *((rsp + 0x30)) = r10;
    rbx = rax;
    *((rsp + 0x50)) = r9;
    while (al != 0) {
        r8 = *((rsp + 0xf0));
        if (*((rsp + 0xf8)) > rbx) {
            goto label_43;
        }
        al = gl_scratch_buffer_grow_preserve (rbp);
    }
    goto label_12;
label_29:
    rax = r15 + 1;
    if (r13 <= rax) {
        goto label_44;
    }
    if (*((r13 - 1)) != 0x2f) {
        goto label_44;
    }
    r13--;
    ebx = 0;
    goto label_4;
label_40:
    if (r14d == 2) {
        goto label_9;
    }
    errno_location ();
    *(rax) = 0x28;
    goto label_15;
label_44:
    ebx = 0;
    goto label_4;
label_41:
    *((rsp + 0x20)) = r9;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x30)) = r8;
    rax = strlen (rbp);
    r9 = *((rsp + 0x20));
    r8 = *((rsp + 0x30));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x18)) = rax;
    r10 = *((rsp + 0x50));
    rax += r9;
    if (rax >= *((rsp + 0xf8))) {
        goto label_16;
    }
label_19:
    *((rsp + 0x30)) = r10;
    *((rsp + 0x18)) = r9;
    rdx++;
    *((rsp + 0x20)) = r8;
    memmove (r8 + r9, rbp, *((rsp + 0x18)));
    r10 = *((rsp + 0x30));
    rsi = r10;
    *((rsp + 0x18)) = r10;
    rax = memcpy (*((rsp + 0x20)), rsi, *((rsp + 0x18)));
    r10 = *((rsp + 0x18));
    rdx = r15 + 1;
    if (*(r10) == 0x2f) {
        goto label_45;
    }
    *((rsp + 0x4b)) = 1;
    eax = *(rax);
    if (r13 <= rdx) {
        goto label_17;
    }
    do {
        r13--;
        if (r13 == r15) {
            goto label_46;
        }
    } while (*((r13 - 1)) != 0x2f);
label_46:
    *((rsp + 0x4b)) = 1;
    goto label_17;
label_38:
    goto label_18;
label_43:
    r10 = *((rsp + 0x30));
    r9 = *((rsp + 0x50));
    rbp = *((rsp + 0x58));
    if (*((rsp + 0x4b)) == 0) {
        goto label_19;
    }
label_42:
    rbp = *((rsp + 0x20));
    rbp += r8;
    goto label_19;
label_39:
    *((rsp + 0x50)) = r11;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x18)) = r10;
    rax = hash_initialize (7, 0, dbg.triple_hash, dbg.triple_compare_ino_str, dbg.triple_free);
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x30));
    *((rsp + 0x28)) = rax;
    r11 = *((rsp + 0x50));
    if (rax != 0) {
        goto label_20;
    }
    goto label_12;
label_45:
    *(r15) = 0x2f;
    r13 = rdx;
    eax = *(rax);
    *((rsp + 0x4b)) = 1;
    goto label_17;
label_26:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x9bd0 */
 
int64_t dbg_try_nocreate (const char * path) {
    stat st;
    int64_t var_98h;
    rdi = path;
    if (? == ?) {
        /* int try_nocreate(char * tmpl,void * flags); */
        void (*0x9be1)() ();
    }
    __asm ("cli");
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    eax = lstat (rdi, rsp);
    ebx = eax;
    rax = errno_location ();
    if (ebx != 0) {
        edx = *(rax);
        if (edx != 0x4b) {
            eax = 0;
            al = (edx != 2) ? 1 : 0;
            eax = -eax;
        }
    } else {
        *(rax) = 0x11;
        eax = 0xffffffff;
    }
    rdx = *((rsp + 0x98));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x9c50 */
 
void dbg_try_dir (void) {
    /* int try_dir(char * tmpl,void * flags); */
    esi = 0x1c0;
    return mkdir ();
}

/* /tmp/tmp0rcdmt2j @ 0x9c60 */
 
int32_t dbg_try_file (int64_t arg2) {
    rsi = arg2;
    /* int try_file(char * tmpl,void * flags); */
    esi = *(rsi);
    edx = 0x180;
    eax = 0;
    sil &= 0x3c;
    sil |= 0xc2;
    return open ();
}

/* /tmp/tmp0rcdmt2j @ 0xad50 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x3ca0)() ();
}

/* /tmp/tmp0rcdmt2j @ 0x2620 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmp0rcdmt2j @ 0x2930 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp0rcdmt2j @ 0x2910 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp0rcdmt2j @ 0x2880 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp0rcdmt2j @ 0x2870 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmp0rcdmt2j @ 0x2530 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmp0rcdmt2j @ 0xb7a0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmp0rcdmt2j @ 0x8d80 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0x4fc0 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmp0rcdmt2j @ 0x90b0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x2660 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmp0rcdmt2j @ 0x9870 */
 
int64_t dbg_same_nameat (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    stat source_dir_stats;
    stat dest_dir_stats;
    int64_t var_8h;
    int64_t var_fh;
    uint32_t var_10h;
    uint32_t var_18h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool same_nameat(int source_dfd,char const * source,int dest_dfd,char const * dest); */
    r15 = rsi;
    r14 = rcx;
    ebx = edi;
    rdi = rsi;
    *((rsp + 8)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x138)) = rax;
    eax = 0;
    rax = last_component ();
    rdi = r14;
    rax = last_component ();
    r12 = rax;
    rax = base_len (rbp);
    r13 = rax;
    rax = base_len (r12);
    r9d = 0;
    while (eax != 0) {
label_0:
        rax = *((rsp + 0x138));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        eax = r9d;
        return rax;
        *((rsp + 0xf)) = r9b;
        eax = memcmp (rbp, r12, r13);
        r9d = 0;
    }
    rdi = r15;
    rax = dir_name ();
    rdx = rsp + 0x10;
    ecx = 0x100;
    edi = ebx;
    rsi = rax;
    eax = fstatat ();
    if (eax != 0) {
        goto label_3;
    }
label_1:
    free (rbp);
    rdi = r14;
    rax = dir_name ();
    edi = *((rsp + 8));
    ecx = 0x100;
    rdx = rsp + 0xa0;
    rsi = rax;
    eax = fstatat ();
    while (1) {
        rax = *((rsp + 0xa8));
        r9d = 0;
        if (*((rsp + 0x18)) == rax) {
            rax = *((rsp + 0xa0));
            r9b = (*((rsp + 0x10)) == rax) ? 1 : 0;
        }
        *((rsp + 8)) = r9b;
        free (rbp);
        r9d = *((rsp + 8));
        goto label_0;
        rax = errno_location ();
        rcx = rbp;
        eax = 0;
        error (1, *(rax), 0x0000da57);
    }
label_3:
    rax = errno_location ();
    rcx = rbp;
    eax = 0;
    error (1, *(rax), 0x0000da57);
    goto label_1;
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xa530 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x8bc0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x2520 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp0rcdmt2j @ 0xa730 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x2800 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp0rcdmt2j @ 0xac70 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000da57);
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x28b0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmp0rcdmt2j @ 0x2850 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmp0rcdmt2j @ 0x2590 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmp0rcdmt2j @ 0x8ae0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xb340 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x8dd0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x29d4)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x8b40 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x6470 */
 
int64_t dbg_hash_print_statistics (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void hash_print_statistics(Hash_table const * table,FILE * stream); */
    r12d = 0;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8 = *((rdi + 0x20));
    rbx = *((rdi + 0x10));
    r13 = *((rdi + 0x18));
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_2;
    do {
        rcx += 0x10;
        if (rsi <= rcx) {
            goto label_2;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_3;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_3:
    if (r12 < rdx) {
        r12 = rdx;
    }
    rcx += 0x10;
    if (rsi > rcx) {
        goto label_0;
    }
label_2:
    rcx = r8;
    rdx = "# entries:         %lu\n";
    rdi = rbp;
    eax = 0;
    esi = 1;
    eax = fprintf_chk ();
    eax = 0;
    rcx = rbx;
    esi = 1;
    rdx = "# buckets:         %lu\n";
    rdi = rbp;
    fprintf_chk ();
    if (r13 < 0) {
        goto label_4;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, r13");
    __asm ("mulsd xmm0, qword [0x0000d210]");
    if (rbx < 0) {
        goto label_5;
    }
    do {
        xmm1 = 0;
        __asm ("cvtsi2sd xmm1, rbx");
label_1:
        __asm ("divsd xmm0, xmm1");
        rcx = r13;
        rdi = rbp;
        esi = 1;
        rdx = "# buckets used:    %lu (%.2f%%)\n";
        eax = 1;
        eax = fprintf_chk ();
        rcx = r12;
        rdi = rbp;
        rdx = "max bucket length: %lu\n";
        esi = 1;
        eax = 0;
        void (*0x2930)() ();
label_4:
        rax = r13;
        rdx = r13;
        xmm0 = 0;
        rax >>= 1;
        edx &= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm0, rax");
        __asm ("addsd xmm0, xmm0");
        __asm ("mulsd xmm0, qword [0x0000d210]");
    } while (rbx >= 0);
label_5:
    rax = rbx;
    ebx &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rbx;
    __asm ("cvtsi2sd xmm1, rax");
    __asm ("addsd xmm1, xmm1");
    goto label_1;
}

/* /tmp/tmp0rcdmt2j @ 0x7280 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite (0x0000d218, 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmp0rcdmt2j @ 0x26a0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmp0rcdmt2j @ 0x2920 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp0rcdmt2j @ 0xaed0 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x28b0)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmp0rcdmt2j @ 0x95d0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0x9310 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x29ed)() ();
    }
    if (rdx == 0) {
        void (*0x29ed)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x6ec0 */
 
int64_t dbg_hash_insert (int64_t arg2) {
     const * matched_ent;
    int64_t var_8h;
    rsi = arg2;
    /* void * hash_insert(Hash_table * table, const * entry); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    eax = hash_insert_if_absent ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    rax = rbx;
    rax = *(rsp);
    while (1) {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        eax = 0;
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x93b0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x29f2)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x29f2)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x8ef0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x29de)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x9c80 */
 
int64_t dbg_try_tempname_len (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    random_value v;
    random_value r;
    timespec tv;
    int64_t var_8h;
    uint32_t var_10h;
    int64_t var_1bh;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int try_tempname_len(char * tmpl,int suffixlen,void * args,int (*)() tryfunc,size_t x_suffix_len); */
    r15 = rdi;
    r14 = r8;
    ebx = esi;
    *((rsp + 0x28)) = rdx;
    *((rsp + 8)) = rdi;
    *((rsp + 0x30)) = rcx;
    *((rsp + 0x10)) = r8;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = errno_location ();
    *((rsp + 0x20)) = rax;
    eax = *(rax);
    *((rsp + 0x4c)) = eax;
    rax = rsp + 0x50;
    rax >>= 4;
    *((rsp + 0x50)) = rax;
    rax = dbg_try_nocreate;
    r9b = (rbp == rax) ? 1 : 0;
    *(rsp) = r9b;
    rax = strlen (r15);
    rdx = (int64_t) ebx;
    rdx += r14;
    if (rdx > rax) {
        goto label_4;
    }
    rax -= rdx;
    r15 += rax;
    *((rsp + 0x38)) = rax;
    rdi = r15;
    *((rsp + 0x40)) = r15;
    rax = strspn (rdi, 0x0000d0c8);
    r9d = *(rsp);
    if (rax < r14) {
        goto label_4;
    }
    r8d = 0;
    *((rsp + 0x48)) = 0x3a2f8;
    r15 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    r14 = 0x27bb2ee687b0b0fd;
    *((rsp + 0x1b)) = r9b;
    *((rsp + 0x1c)) = r8d;
label_2:
    if (*((rsp + 0x10)) == 0) {
        goto label_5;
    }
    rax = *((rsp + 8));
    rax += *((rsp + 0x10));
    r12d = 0xb504f32d;
    rax += *((rsp + 0x38));
    r13 = *((rsp + 0x50));
    rbx = *((rsp + 0x40));
    r9d = *((rsp + 0x1b));
    *(rsp) = rax;
    r8d = *((rsp + 0x1c));
    while (r8d != 0) {
        r8d--;
label_1:
        rdx = r13;
        rcx = r13;
        rbx++;
        rax = 0x8421084210842109;
        rdx >>= 1;
        rdx:rax = rax * rdx;
        rdx >>= 4;
        rax = rdx;
        *((rsp + 0x50)) = rdx;
        r13 = rdx;
        rax <<= 5;
        rax -= rdx;
        rax += rax;
        rcx -= rax;
        eax = *((r15 + rcx));
        *((rbx - 1)) = al;
        if (rbx == *(rsp)) {
            goto label_6;
        }
    }
    if (r9b != 0) {
        goto label_0;
    }
    do {
        rsi = rsp + 0x60;
        edi = 1;
        clock_gettime ();
        rcx = *((rsp + 0x68));
        rcx ^= r13;
        rcx *= r14;
        r13 = rcx + r12;
        *((rsp + 0x50)) = r13;
        if (r13 <= rbp) {
            goto label_7;
        }
label_0:
        rdi = rsp + 0x58;
        edx = 1;
        esi = 8;
        rax = getrandom ();
    } while (rax != 8);
    r13 = *((rsp + 0x58));
    *((rsp + 0x50)) = r13;
    if (r13 > rbp) {
        goto label_0;
    }
label_7:
    r8d = 9;
    r9d = 1;
    goto label_1;
label_6:
label_5:
    rsi = *((rsp + 0x28));
    rdi = *((rsp + 8));
    rax = *((rsp + 0x30));
    eax = void (*rax)(uint64_t) (r9b);
    if (eax >= 0) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    if (*(rax) != 0x11) {
        goto label_9;
    }
    if (*(rax) != 0x11) {
        goto label_2;
    }
label_9:
    eax = 0xffffffff;
    do {
label_3:
        rdx = *((rsp + 0x78));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_10;
        }
        return rax;
label_8:
        rbx = *((rsp + 0x20));
        edi = *((rsp + 0x4c));
        *(rbx) = edi;
    } while (1);
label_4:
    rax = *((rsp + 0x20));
    *(rax) = 0x16;
    eax = 0xffffffff;
    goto label_3;
label_10:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x5ae0 */
 
uint64_t dbg_mdir_name (uint32_t arg1) {
    rdi = arg1;
    /* char * mdir_name(char const * file); */
    ebx = 0;
    bl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbp;
    r12 = rax;
    while (rbx < r12) {
        rax = r12 - 1;
        if (*((rbp + r12 - 1)) != 0x2f) {
            goto label_2;
        }
        r12 = rax;
    }
    rax = r12;
    rax ^= 1;
    ebx = eax;
    ebx &= 1;
    rax = malloc (r12 + rax + 1);
    if (rax == 0) {
        goto label_3;
    }
    rax = memcpy (rax, rbp, r12);
    r8 = rax;
    if (bl == 0) {
        goto label_4;
    }
    *(rax) = 0x2e;
    r12d = 1;
    do {
label_0:
        *((r8 + r12)) = 0;
label_1:
        rax = r8;
        return rax;
label_2:
        rax = malloc (r12 + 1);
        r8 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = memcpy (r8, rbp, r12);
        r8 = rax;
    } while (1);
label_4:
    r12d = 1;
    goto label_0;
label_3:
    r8d = 0;
    goto label_1;
}

/* /tmp/tmp0rcdmt2j @ 0x7180 */
 
void dbg_triple_free (void ** ptr) {
    rdi = ptr;
    /* void triple_free(void * x); */
    free (*(rdi));
    rdi = rbp;
    return free ();
}

/* /tmp/tmp0rcdmt2j @ 0x2500 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmp0rcdmt2j @ 0x9270 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x29e8)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xb7b4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp0rcdmt2j @ 0x6a20 */
 
int64_t dbg_hash_free (int64_t arg_8h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_free(Hash_table * table); */
    r12 = rdi;
    r13 = *(rdi);
    rax = *((rdi + 8));
    if (*((rdi + 0x40)) == 0) {
        goto label_2;
    }
    if (*((rdi + 0x20)) == 0) {
        goto label_2;
    }
    if (r13 < rax) {
        goto label_0;
    }
    goto label_3;
    do {
        r13 += 0x10;
        if (rax <= r13) {
            goto label_4;
        }
label_0:
        rdi = *(r13);
    } while (rdi == 0);
    rbx = r13;
    while (rbx != 0) {
        rdi = *(rbx);
        uint64_t (*r12 + 0x40)() ();
        rbx = *((rbx + 8));
    }
    rax = *((r12 + 8));
    r13 += 0x10;
    if (rax > r13) {
        goto label_0;
    }
label_4:
    rbp = *(r12);
label_2:
    if (rax <= rbp) {
        goto label_3;
    }
label_1:
    rbx = *((rbp + 8));
    if (rbx == 0) {
        goto label_5;
    }
    do {
        rbx = *((rbx + 8));
        free (rbx);
    } while (rbx != 0);
label_5:
    rbp += 0x10;
    if (*((r12 + 8)) > rbp) {
        goto label_1;
    }
label_3:
    rbx = *((r12 + 0x48));
    if (rbx == 0) {
        goto label_6;
    }
    do {
        rbx = *((rbx + 8));
        free (rbx);
    } while (rbx != 0);
label_6:
    free (*(r12));
    rdi = r12;
    return free ();
}

/* /tmp/tmp0rcdmt2j @ 0xa7b0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0xa8b0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x5c30 */
 
uint64_t dbg_record_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg1, uint32_t arg2, int64_t arg3) {
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void record_file(Hash_table * ht,char const * file,stat const * stats); */
    if (rdi != 0) {
        r13 = rsi;
        r12 = rdi;
        rbx = rdx;
        rax = xmalloc (0x18);
        rax = xstrdup (r13);
        rsi = rbp;
        *(rbp) = rax;
        rax = *((rbx + 8));
        *((rbp + 8)) = rax;
        rax = *(rbx);
        *((rbp + 0x10)) = rax;
        rax = hash_insert (r12);
        if (rax == 0) {
            goto label_0;
        }
        if (rbp != rax) {
            rdi = rbp;
            void (*0x7180)() ();
        }
        return rax;
    }
    return rax;
label_0:
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0xa750 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x44d0 */
 
int64_t dbg_relpath (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool relpath(char const * can_fname,char const * can_reldir,char * buf,size_t len); */
    *(rsp) = rcx;
    cl = (*((rsi + 1)) == 0x2f) ? 1 : 0;
    *((rsp + 8)) = rdx;
    dl = (*((rdi + 1)) == 0x2f) ? 1 : 0;
    eax = 0;
    if (cl != dl) {
        goto label_4;
    }
    edx = *(rsi);
    rbx = rsi;
    if (dl == 0) {
        goto label_4;
    }
    r10 = rdi;
    rax = rdi;
    r8d = 0;
    rdi = rsi;
    esi = 0;
    while (cl == dl) {
        if (r9b != 0) {
            goto label_5;
        }
        esi++;
        if (dl == 0x2f) {
            r8d = esi;
        }
        edx = *((rdi + 1));
        rdi++;
        rax++;
        if (dl == 0) {
            goto label_6;
        }
        ecx = *(rax);
        r9b = (cl == 0) ? 1 : 0;
    }
label_5:
    if (dl != 0x2f) {
        goto label_7;
    }
    r8d = esi;
    while (al != 0x2f) {
label_7:
        if (r8d == 0) {
            goto label_8;
        }
label_0:
        r8 = (int64_t) r8d;
        rbx += r8;
        rbp = r10 + r8;
        eax = *(rbx);
        if (al == 0x2f) {
            eax = *((rbx + 1));
            rbx++;
        }
        edx = 0;
        dl = (*(rbp) == 0x2f) ? 1 : 0;
        rbp += rdx;
        if (al != 0) {
            goto label_9;
        }
        rax = 0x0000d08b;
        rdx = rsp;
        if (*(rbp) == 0) {
        }
        eax = buffer_or_output (rbp, rsp + 8, rdx);
        r12d = eax;
label_3:
        if (r12b != 0) {
            goto label_10;
        }
label_1:
        eax = r12d;
        eax ^= 1;
label_4:
        return rax;
label_6:
        eax = *(rax);
        if (al == 0) {
            goto label_11;
        }
    }
label_11:
    r8d = esi;
    goto label_0;
label_10:
    edx = 5;
    rax = dcgettext (0, "generating relative path");
    rcx = rax;
    eax = 0;
    error (0, 0x24, 0x0000da57);
    goto label_1;
label_9:
    r14 = rsp;
    r13 = rsp + 8;
    eax = buffer_or_output (0x0000d08a, r13, r14);
    r12d = eax;
    eax = *(rbx);
    if (al == 0) {
        goto label_12;
    }
    r15 = 0x0000d089;
    while (al != 0x2f) {
        eax = *((rbx + 1));
        rbx++;
        if (al == 0) {
            goto label_12;
        }
label_2:
    }
    rbx++;
    eax = buffer_or_output (r15, r13, r14);
    r12d |= eax;
    eax = *(rbx);
    if (al != 0) {
        goto label_2;
    }
label_12:
    if (*(rbp) == 0) {
        goto label_3;
    }
    eax = buffer_or_output (0x0000d732, r13, r14);
    ebx = eax;
    eax = buffer_or_output (rbp, r13, r14);
    ebx |= eax;
    r12d |= ebx;
    goto label_3;
label_8:
    eax = 0;
    goto label_4;
}

/* /tmp/tmp0rcdmt2j @ 0x5a80 */
 
uint64_t dbg_dir_name (void) {
    /* char * dir_name(char const * file); */
    rax = mdir_name (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x4f60 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x5d40 */
 
uint64_t dbg_mfile_name_concat (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * mfile_name_concat(char const * dir,char const * base,char ** base_in_result); */
    r12 = rsi;
    *((rsp + 8)) = rdx;
    rax = last_component ();
    r13 = rax;
    rax = base_len (rax);
    r13 -= rbp;
    r14 = r13 + rax;
    rbx = rax;
    rax = strlen (r12);
    r13 = rax;
    if (rbx == 0) {
        goto label_1;
    }
    if (*((rbp + r14 - 1)) == 0x2f) {
        goto label_2;
    }
    ebx = 0;
    r15d = 0;
    eax = 0x2f;
    if (*(r12) == 0x2f) {
        eax = r15d;
    }
    bl = (*(r12) != 0x2f) ? 1 : 0;
    *((rsp + 7)) = al;
    do {
label_0:
        rdi += rbx;
        rax = malloc (r14 + r13 + 1);
        r15 = rax;
        if (rax != 0) {
            rdi = rax;
            rdx = r14;
            rsi = rbp;
            mempcpy ();
            ecx = *((rsp + 7));
            rdi = rax + rbx;
            *(rax) = cl;
            rax = *((rsp + 8));
            if (rax != 0) {
                *(rax) = rdi;
            }
            rdx = r13;
            rsi = r12;
            mempcpy ();
            *(rax) = 0;
        }
        rax = r15;
        return rax;
label_1:
        ebx = 0;
        r15d = 0;
        eax = 0x2e;
        if (*(r12) != 0x2f) {
            eax = r15d;
        }
        bl = (*(r12) == 0x2f) ? 1 : 0;
        *((rsp + 7)) = al;
    } while (1);
label_2:
    *((rsp + 7)) = 0;
    ebx = 0;
    goto label_0;
}

/* /tmp/tmp0rcdmt2j @ 0x5be0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2810)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmp0rcdmt2j @ 0x6800 */
 
int64_t dbg_hash_string (int64_t arg1, size_t n_buckets) {
    rdi = arg1;
    rsi = n_buckets;
    /* size_t hash_string(char const * string,size_t n_buckets); */
    ecx = *(rdi);
    edx = 0;
    if (cl == 0) {
        goto label_0;
    }
    do {
        rax = rdx;
        rdi++;
        rax <<= 5;
        rax -= rdx;
        edx = 0;
        rax += rcx;
        ecx = *(rdi);
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
    } while (cl != 0);
label_0:
    rax = rdx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x4e40 */
 
uint64_t dbg_find_backup_file_name (void) {
    /* char * find_backup_file_name(int dir_fd,char const * file,backup_type backup_type); */
    ecx = 0;
    rax = backupfile_internal (rdi, rsi, rdx);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x4730 */
 
int64_t dbg_backupfile_internal (int64_t arg1, void * arg2, uint32_t arg3) {
    int32_t sdir;
    int64_t var_8h;
    size_t s1;
    signed int64_t var_18h;
    signed int64_t var_20h;
    signed int64_t canary;
    void * var_37h;
    void * s2;
    void ** var_40h;
    int64_t var_48h;
    int64_t var_50h;
    size_t var_58h;
    uint32_t var_60h;
    uint32_t var_68h;
    int64_t var_6ch;
    size_t n;
    int64_t var_78h;
    int64_t var_84h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * backupfile_internal(int dir_fd,char const * file,backup_type backup_type,_Bool rename); */
    rbx = rsi;
    *((rsp + 0x6c)) = edi;
    rdi = rsi;
    *((rsp + 0x38)) = rsi;
    *((rsp + 0x68)) = edx;
    *((rsp + 0x37)) = cl;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rax = last_component ();
    r14 = rax;
    rdi = rax;
    *((rsp + 0x60)) = rax;
    rax = base_len (rdi);
    r14 -= rbx;
    rdi = simple_backup_suffix;
    *((rsp + 8)) = rax;
    rax += r14;
    *((rsp + 0x10)) = rax;
    if (rdi == 0) {
        goto label_18;
    }
label_12:
    rax = strlen (rdi);
    rax++;
    rsi = rax;
    *((rsp + 0x70)) = rax;
    eax = 9;
    if (rsi >= rax) {
        rax = rsi;
    }
    rsi = *((rsp + 0x10));
    rax = rsi + rax + 1;
    rdi = rax;
    *((rsp + 0x50)) = rax;
    rax = malloc (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_10;
    }
    *((rsp + 0x58)) = 0;
    eax = *((rsp + 0x6c));
    ebp = 0;
    *((rsp + 0x84)) = eax;
    rax = *((rsp + 8));
    rbx = rax + 4;
label_2:
    memcpy (r12, *((rsp + 0x38)), *((rsp + 0x10)));
    if (*((rsp + 0x68)) == 1) {
        goto label_19;
    }
    if (rbp == 0) {
        goto label_20;
    }
    rdi = rbp;
    rewinddir ();
label_7:
    rax = *((rsp + 0x50));
    *((rsp + 0x18)) = 2;
    *((rsp + 0x20)) = 1;
    *((rsp + 0x28)) = rax;
    do {
label_0:
        rdi = rbp;
        rax = readdir ();
        if (rax == 0) {
            goto label_21;
        }
label_1:
        r13 = rax + 0x13;
        rax = strlen (r13);
    } while (rax < rbx);
    rax = *((rsp + 8));
    r15 = rax + 2;
    eax = memcmp (r12 + r14, r13, r15);
    if (eax != 0) {
        goto label_0;
    }
    r13 += r15;
    eax = *(r13);
    edx = rax - 0x31;
    if (dl > 8) {
        goto label_0;
    }
    eax = *((r13 + 1));
    r8b = (al == 0x39) ? 1 : 0;
    edx = eax;
    eax -= 0x30;
    if (eax > 9) {
        goto label_22;
    }
    r15d = 1;
    do {
        al = (dl == 0x39) ? 1 : 0;
        r15++;
        r8d &= eax;
        eax = *((r13 + r15));
        rcx = r15;
        edx = eax;
        eax -= 0x30;
    } while (eax <= 9);
label_6:
    if (dl != 0x7e) {
        goto label_0;
    }
    if (*((r13 + rcx + 1)) != 0) {
        goto label_0;
    }
    if (*((rsp + 0x20)) >= r15) {
        goto label_23;
    }
    rax = *((rsp + 0x10));
    r9 = rax + 2;
label_3:
    edx = (int32_t) r8b;
    esi = (int32_t) r8b;
    rax = rdx + r15;
    *((rsp + 0x18)) = esi;
    *((rsp + 0x20)) = rax;
    rax = rax + r9 + 2;
    if (rax <= *((rsp + 0x28))) {
        goto label_24;
    }
    rsi = rax;
    rsi >>= 1;
    rsi += rax;
    if (rsi overflow 0) {
        goto label_25;
    }
    *((rsp + 0x28)) = rsi;
label_17:
    rax = *((rsp + 0x28));
    *((rsp + 0x48)) = rdx;
    *((rsp + 0x40)) = rcx;
    sil = (rax == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (r12, 0);
    rcx = *((rsp + 0x40));
    rdx = *((rsp + 0x48));
    r8 = rax;
    if (rax == 0) {
        goto label_26;
    }
label_9:
    rax = *((rsp + 0x10));
    *((rsp + 0x48)) = rcx;
    ecx = 0x7e2e;
    *((rsp + 0x40)) = r8;
    rax += r8;
    *(rax) = cx;
    *((rax + 2)) = 0x30;
    rax = memcpy (rax + rdx + 2, r13, r15 + 2);
    rcx = *((rsp + 0x48));
    r8 = *((rsp + 0x40));
    rdi = rax;
    rdi += rcx;
    edx = *((rdi - 1));
    rax = rdi - 1;
    if (dl != 0x39) {
        goto label_27;
    }
    do {
        *(rax) = 0x30;
        edx = *((rax - 1));
        rax--;
    } while (dl == 0x39);
label_27:
    edx++;
    rdi = rbp;
    r12 = r8;
    *(rax) = dl;
    rax = readdir ();
    if (rax != 0) {
        goto label_1;
    }
label_21:
    if (*((rsp + 0x18)) == 2) {
        goto label_14;
    }
label_16:
    if (*((rsp + 0x18)) == 1) {
        goto label_11;
    }
    if (*((rsp + 0x37)) == 0) {
        goto label_28;
    }
    r15d = *((rsp + 0x37));
label_5:
label_4:
    edi = *((rsp + 0x84));
    eax = renameatu (*(edi), *((rsp + 0x60)), *((rsp + 0x84)), r12 + r14, 1);
    if (eax == 0) {
        goto label_28;
    }
    rax = errno_location ();
    edx = *(rax);
    if (edx != 0x11) {
        goto label_29;
    }
    if (r15b == 1) {
        goto label_2;
    }
label_29:
    r15d = edx;
    rdx = rax;
    if (rbp != 0) {
        rdi = rbp;
        *((rsp + 8)) = rax;
        closedir ();
        rdx = *((rsp + 8));
    }
    *((rsp + 8)) = rdx;
    r12d = 0;
    free (r12);
    rdx = *((rsp + 8));
    *(rdx) = r15d;
label_10:
    rax = *((rsp + 0x88));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_30;
    }
    rax = r12;
    return rax;
label_23:
    *((rsp + 0x40)) = r8b;
    if (rax != 0) {
        goto label_0;
    }
    rax = *((rsp + 0x10));
    rdx = rcx;
    *((rsp + 0x48)) = rcx;
    r9 = rax + 2;
    *((rsp + 0x78)) = r9;
    eax = memcmp (r12 + r9, r13, rdx);
    rcx = *((rsp + 0x48));
    r9 = *((rsp + 0x78));
    r8d = *((rsp + 0x40));
    if (eax <= 0) {
        goto label_3;
    }
    goto label_0;
label_19:
    rax = *((rsp + 0x10));
    memcpy (r12 + rax, *(obj.simple_backup_suffix), *((rsp + 0x70)));
    if (*((rsp + 0x37)) == 0) {
        goto label_28;
    }
    rsi = *((rsp + 0x38));
    r15d = *((rsp + 0x37));
    rcx = r12;
    r8d = 0;
    goto label_4;
label_14:
    if (*((rsp + 0x68)) == 2) {
        goto label_31;
    }
label_11:
    rdi = r12;
    r15d = 1;
    rax = last_component ();
    rdi = rax;
    *((rsp + 0x18)) = rax;
    rax = base_len (rdi);
    rcx = rax;
    if (rax > 0xe) {
        goto label_32;
    }
label_8:
    if (*((rsp + 0x37)) == 0) {
        goto label_28;
    }
    if (*((rsp + 0x68)) != 1) {
        goto label_5;
    }
    rsi = *((rsp + 0x38));
    rcx = r12;
    r8d = 0;
    goto label_4;
label_22:
    ecx = 1;
    r15d = 1;
    goto label_6;
label_20:
    r15 = r12 + r14;
    esi = 0x2e;
    r13d = *(r15);
    *(r15) = si;
    rax = opendirat (*((rsp + 0x6c)), r12, 0, rsp + 0x84);
    rax = *((rsp + 8));
    rdx = r15 + rax;
    if (rbp == 0) {
        goto label_33;
    }
    *(r15) = r13w;
    *(rdx) = 0x7e317e2e;
    *((rdx + 4)) = 0;
    goto label_7;
label_32:
    edi = *((rsp + 0x84));
    rdx = *((rsp + 0x18));
    *((rsp + 0x20)) = edi;
    if (*((rsp + 0x58)) == 0) {
        *((rsp + 0x28)) = rax;
        rax = errno_location ();
        edi = *((rsp + 0x20));
        rdx = *((rsp + 0x18));
        rcx = *((rsp + 0x28));
        r15 = rax;
        if (edi < 0) {
            goto label_34;
        }
        *(rax) = 0;
        esi = 3;
        *((rsp + 0x20)) = rcx;
        *((rsp + 0x18)) = rdx;
        rax = fpathconf ();
        rcx = *((rsp + 0x20));
        rdx = *((rsp + 0x18));
        *((rsp + 0x58)) = rax;
label_13:
        rsi = *((rsp + 0x58));
        if (rsi < 0) {
            goto label_35;
        }
    }
    if (rcx <= *((rsp + 0x58))) {
        goto label_36;
    }
    do {
        rax = *((rsp + 0x10));
        rsi = *((rsp + 0x58));
        rax += r12;
        rcx = rsi - 1;
        rax -= rdx;
        if (rax >= rsi) {
            rax = rcx;
        }
        r15d = 0;
        *((rdx + rax)) = 0x7e;
        *((rdx + rax + 1)) = 0;
        goto label_8;
label_24:
        r8 = r12;
        goto label_9;
label_26:
        rdi = rbp;
        closedir ();
label_15:
        r12d = 0;
        free (r12);
        errno_location ();
        *(rax) = 0xc;
        goto label_10;
label_35:
        eax = 0xe;
        if (rsi == -1) {
            rax = rsi;
        }
        *((rsp + 0x58)) = rax;
    } while (1);
label_31:
    rax = *((rsp + 0x10));
    memcpy (r12 + rax, *(obj.simple_backup_suffix), *((rsp + 0x70)));
    *((rsp + 0x68)) = 1;
    goto label_11;
label_28:
    if (rbp == 0) {
        goto label_10;
    }
    rdi = rbp;
    closedir ();
    goto label_10;
label_18:
    rax = getenv ("SIMPLE_BACKUP_SUFFIX");
    rdi = 0x0000d0b3;
    rbx = rax;
    if (rax != 0) {
        if (*(rax) == 0) {
            goto label_37;
        }
        rdi = rax;
        rax = last_component ();
        rdi = rax;
        rax = 0x0000d0b3;
        if (rbx == rax) {
            rdi = rax;
            goto label_37;
        }
    }
label_37:
    *(obj.simple_backup_suffix) = rdi;
    goto label_12;
label_34:
    eax = 0x2e;
    r8d = *(rdx);
    esi = 3;
    rdi = r12;
    *(rdx) = ax;
    *(r15) = 0;
    *((rsp + 0x20)) = r8d;
    rax = pathconf ();
    rdx = *((rsp + 0x18));
    r8d = *((rsp + 0x20));
    rcx = *((rsp + 0x28));
    *((rsp + 0x58)) = rax;
    *(rdx) = r8w;
    goto label_13;
label_33:
    *((rsp + 0x20)) = rdx;
    rax = errno_location ();
    rdx = *((rsp + 0x20));
    *(r15) = r13w;
    al = (*(rax) == 0xc) ? 1 : 0;
    *(rdx) = 0x7e317e2e;
    eax = (int32_t) al;
    *((rdx + 4)) = 0;
    eax += 2;
    *((rsp + 0x18)) = eax;
    if (eax == 2) {
        goto label_14;
    }
    if (*((rsp + 0x18)) == 3) {
        goto label_15;
    }
    goto label_16;
label_25:
    *((rsp + 0x28)) = rax;
    goto label_17;
label_36:
    r15d = 1;
    goto label_8;
label_30:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xb090 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x7220 */
 
uint64_t dbg_opendirat (int64_t arg3, int64_t arg4, int32_t fd, const char * path) {
    rdx = arg3;
    rcx = arg4;
    rdi = fd;
    rsi = path;
    /* DIR * opendirat(int dir_fd,char const * dir,int extra_flags,int * pnew_fd); */
    edx |= 0x90900;
    eax = 0;
    r12d = 0;
    rbx = rcx;
    eax = openat_safer (rdi, rsi, rdx, rcx, r8);
    if (eax < 0) {
        goto label_0;
    }
    edi = eax;
    rax = fdopendir ();
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    *(rbx) = ebp;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        rax = errno_location ();
        r13d = *(rax);
        rbx = rax;
        close (ebp);
        *(rbx) = r13d;
    } while (1);
}

/* /tmp/tmp0rcdmt2j @ 0x71a0 */
 
int64_t dbg_openat_safer (int64_t arg_60h, int64_t arg4, int32_t fd, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_38h;
    rcx = arg4;
    rdi = fd;
    rdx = oflag;
    rsi = path;
    /* int openat_safer(int fd,char const * file,int flags,va_args ...); */
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = openat (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x18;
        ecx = *((rsp + 0x38));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x28e0 */
 
void fdopendir (void) {
    __asm ("bnd jmp qword [reloc.fdopendir]");
}

/* /tmp/tmp0rcdmt2j @ 0x26e0 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmp0rcdmt2j @ 0xa6f0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x8db0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0x70e0 */
 
int64_t dbg_triple_compare (uint32_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    /* _Bool triple_compare( const * x, const * y); */
    rax = *((rsi + 8));
    while (*((rdi + 0x10)) != rax) {
        eax = 0;
        return rax;
        rax = *((rsi + 0x10));
    }
    rsi = *(rsi);
    rdi = *(rdi);
    return void (*0x9a10)() ();
}

/* /tmp/tmp0rcdmt2j @ 0xac30 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0xa710 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0xad60 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmp0rcdmt2j @ 0xa470 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0xa000)() ();
}

/* /tmp/tmp0rcdmt2j @ 0x9a10 */
 
void dbg_same_name (int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    /* _Bool same_name(char const * source,char const * dest); */
    rcx = rsi;
    edx = 0xffffff9c;
    rsi = rdi;
    edi = 0xffffff9c;
    return void (*0x9870)() ();
}

/* /tmp/tmp0rcdmt2j @ 0x59d0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000da57);
    } while (1);
}

/* /tmp/tmp0rcdmt2j @ 0xb6e0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmp0rcdmt2j @ 0xa6b0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x6400 */
 
int64_t dbg_hash_table_ok (Hash_table const * table) {
    rdi = table;
    /* _Bool hash_table_ok(Hash_table const * table); */
    rcx = *(rdi);
    rsi = *((rdi + 8));
    edx = 0;
    r8d = 0;
    if (rcx < rsi) {
        goto label_1;
    }
    goto label_2;
    do {
label_0:
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_2;
        }
label_1:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    r8++;
    rdx++;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_1;
    }
label_2:
    eax = 0;
    if (*((rdi + 0x18)) != r8) {
        return rax;
    }
    al = (*((rdi + 0x20)) == rdx) ? 1 : 0;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xb660 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x9460 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x29f7)() ();
    }
    if (rax == 0) {
        void (*0x29f7)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x6ca0 */
 
int64_t hash_insert_if_absent (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rsi == 0) {
        void (*0x29bf)() ();
    }
    r12 = rsp;
    r13 = rdx;
    ecx = 0;
    rbx = rdi;
    rdx = r12;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_8;
    }
    r8d = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r13) = rax;
    do {
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        eax = r8d;
        return rax;
label_8:
        rax = *((rbx + 0x18));
        if (rax < 0) {
            goto label_10;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_11;
        }
label_0:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_1:
        rax = *((rbx + 0x28));
        xmm0 = *((rax + 8));
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm5, xmm0");
        if (rax > 0) {
            goto label_12;
        }
label_2:
        r12 = *(rsp);
        if (*(r12) == 0) {
            goto label_13;
        }
        rax = *((rbx + 0x48));
        if (rax == 0) {
            goto label_14;
        }
        rdx = *((rax + 8));
        *((rbx + 0x48)) = rdx;
label_4:
        rdx = *((r12 + 8));
        *(rax) = rbp;
        r8d = 1;
        *((rax + 8)) = rdx;
        *((r12 + 8)) = rax;
        *((rbx + 0x20))++;
    } while (1);
label_10:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_0;
    }
label_11:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_1;
label_12:
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm1 = xmm4;
    xmm0 = *((rax + 8));
    __asm ("mulss xmm1, xmm0");
    __asm ("comiss xmm5, xmm1");
    if (rdx <= 0) {
        goto label_2;
    }
    __asm ("mulss xmm4, dword [rax + 0xc]");
    if (*((rax + 0x10)) == 0) {
        goto label_15;
    }
label_5:
    __asm ("comiss xmm4, dword [0x0000d204]");
    if (*((rax + 0x10)) < 0) {
        goto label_16;
    }
    do {
label_6:
        r8d = 0xffffffff;
        goto label_3;
label_13:
        *(r12) = rbp;
        r8d = 1;
        *((rbx + 0x20))++;
        *((rbx + 0x18))++;
        goto label_3;
label_14:
        rax = malloc (0x10);
    } while (rax == 0);
    goto label_4;
label_15:
    __asm ("mulss xmm4, xmm0");
    goto label_5;
label_16:
    __asm ("comiss xmm4, dword [0x0000d208]");
    if (rax >= 0) {
        goto label_17;
    }
    __asm ("cvttss2si rsi, xmm4");
label_7:
    rdi = rbx;
    al = hash_rehash ();
    if (al == 0) {
        goto label_6;
    }
    ecx = 0;
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_2;
    }
    void (*0x29bf)() ();
label_17:
    __asm ("subss xmm4, dword [0x0000d208]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_7;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x6380 */
 
int64_t hash_get_n_buckets_used (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x18));
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x91e0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xab00 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x2760 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmp0rcdmt2j @ 0x8a80 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x6870 */
 
uint64_t dbg_hash_initialize (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* Hash_table * hash_initialize(size_t candidate,Hash_tuning const * tuning,Hash_hasher hasher,Hash_comparator comparator,Hash_data_freer data_freer); */
    rax = dbg_raw_hasher;
    r15 = rsi;
    r14 = r8;
    r13 = rdi;
    edi = 0x50;
    rbx = rcx;
    if (rdx == 0) {
    }
    rax = dbg_raw_comparator;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = malloc (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = obj_default_tuning;
    rdi = r12;
    if (r15 == 0) {
        r15 = rax;
    }
    *((r12 + 0x28)) = r15;
    al = check_tuning ();
    if (al == 0) {
        goto label_1;
    }
    esi = *((r15 + 0x10));
    xmm0 = *((r15 + 8));
    rdi = r13;
    rax = compute_bucket_size_isra_0 ();
    *((r12 + 0x10)) = rax;
    r13 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = calloc (rax, 0x10);
    *(r12) = rax;
    if (rax == 0) {
        goto label_1;
    }
    r13 <<= 4;
    *((r12 + 0x30)) = rbp;
    rax += r13;
    *((r12 + 0x38)) = rbx;
    *((r12 + 8)) = rax;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x40)) = r14;
    *((r12 + 0x48)) = 0;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        r12d = 0;
        free (r12);
    } while (1);
}

/* /tmp/tmp0rcdmt2j @ 0x9f90 */
 
void dbg_try_tempname (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_1bh;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int try_tempname(char * tmpl,int suffixlen,void * args,int (*)() tryfunc); */
    r8d = 6;
    return void (*0x9c80)() ();
}

/* /tmp/tmp0rcdmt2j @ 0x95a0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0x8a20 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xab70 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x8cc0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x9610 */
 
int64_t dbg_renameatu (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    stat src_st;
    stat dst_st;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_28h;
    int64_t var_a0h;
    int64_t var_b8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int renameatu(int fd1,char const * src,int fd2,char const * dst,unsigned int flags); */
    r15d = edi;
    r14d = edx;
    r13 = rcx;
    ebx = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x138)) = rax;
    eax = 0;
    eax = renameat2 ();
    r12d = eax;
    if (eax >= 0) {
        goto label_0;
    }
    rax = errno_location ();
    r8 = rax;
    eax = *(rax);
    edx = rax - 0x16;
    edx &= 0xffffffef;
    dl = (edx != 0) ? 1 : 0;
    al = (eax != 0x5f) ? 1 : 0;
    dl &= al;
    r9d = edx;
    if (dl != 0) {
        goto label_0;
    }
    if (ebx == 0) {
        goto label_3;
    }
    if (ebx != 1) {
        goto label_4;
    }
    ecx = 0x100;
    rsi = r13;
    edi = r14d;
    *(rsp) = r8;
    rdx = rsp + 0xa0;
    eax = fstatat ();
    r8 = *(rsp);
    if (eax == 0) {
        goto label_5;
    }
    eax = *(r8);
    if (eax == 0x4b) {
        goto label_5;
    }
    if (eax == 2) {
        goto label_6;
    }
label_1:
    r12d = 0xffffffff;
    do {
label_0:
        rax = *((rsp + 0x138));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_7;
        }
        eax = r12d;
        return rax;
label_5:
        *(r8) = 0x11;
        r12d = 0xffffffff;
    } while (1);
label_6:
    r9d = 1;
label_3:
    *((rsp + 0xf)) = r9b;
    *(rsp) = r8;
    rax = strlen (rbp);
    rbx = rax;
    rax = strlen (r13);
    if (rbx != 0) {
        r8 = *(rsp);
        r9d = *((rsp + 0xf));
        if (rax == 0) {
            goto label_2;
        }
        if (*((rbp + rbx - 1)) == 0x2f) {
            goto label_8;
        }
        if (*((r13 + rax - 1)) == 0x2f) {
            goto label_8;
        }
    }
label_2:
    rcx = r13;
    edx = r14d;
    rsi = rbp;
    edi = r15d;
    eax = renameat ();
    r12d = eax;
    goto label_0;
label_8:
    rdx = rsp + 0x10;
    ecx = 0x100;
    rsi = rbp;
    edi = r15d;
    *((rsp + 0xf)) = r9b;
    *(rsp) = r8;
    eax = fstatat ();
    if (eax != 0) {
        goto label_1;
    }
    r9d = *((rsp + 0xf));
    r8 = *(rsp);
    if (r9b == 0) {
        goto label_9;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax == 0x4000) {
        goto label_2;
    }
    *(r8) = 2;
    r12d = 0xffffffff;
    goto label_0;
label_4:
    *(r8) = 0x5f;
    r12d = 0xffffffff;
    goto label_0;
label_9:
    ecx = 0x100;
    rsi = r13;
    edi = r14d;
    *(rsp) = r8;
    rdx = rsp + 0xa0;
    eax = fstatat ();
    r8 = *(rsp);
    if (eax == 0) {
        goto label_10;
    }
    if (*(r8) != 2) {
        goto label_1;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax != 0x4000) {
        goto label_1;
    }
    goto label_2;
label_10:
    eax = *((rsp + 0xb8));
    eax &= 0xf000;
    if (eax != 0x4000) {
        *(r8) = 0x14;
        goto label_1;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax == 0x4000) {
        goto label_2;
    }
    *(r8) = 0x15;
    r12d = 0xffffffff;
    goto label_0;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x65d0 */
 
uint64_t hash_lookup (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 0x10));
    rdi = r12;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t) (rbx, rbp);
    if (rax >= *((rbp + 0x10))) {
        void (*0x29aa)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    rsi = *(rbx);
    if (rsi != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rsi = *(rbx);
label_0:
        if (rsi == r12) {
            goto label_2;
        }
        rdi = r12;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_3;
        }
        rbx = *((rbx + 8));
    } while (rbx != 0);
label_1:
    eax = 0;
    return rax;
label_3:
    r12 = *(rbx);
label_2:
    rax = *(rbx);
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x7110 */
 
uint64_t dbg_triple_hash (int64_t arg_8h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* size_t triple_hash( const * x,size_t table_size); */
    rbx = rsi;
    rax = hash_pjw (*(rdi));
    edx = 0;
    rax ^= *((rbp + 8));
    rax = rdx:rax / rbx;
    rdx = rdx:rax % rbx;
    rax = rdx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xb2c0 */
 
uint64_t rotate_left64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
int64_t dbg_hash_pjw (int64_t arg1, size_t tablesize) {
    rdi = arg1;
    rsi = tablesize;
    /* size_t hash_pjw( const * x,size_t tablesize); */
    rdx = *(rdi);
    if (dl == 0) {
        goto label_0;
    }
    eax = 0;
    do {
        rax = rotate_left64 (rax, 9);
        rdi++;
        rax += rdx;
        rdx = *(rdi);
    } while (dl != 0);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    r8 = rdx;
    rax = rdx;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x6970 */
 
int64_t dbg_hash_clear (uint32_t arg_8h, int64_t arg_18h, int64_t arg_20h, int64_t arg_40h, int64_t arg_48h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_clear(Hash_table * table); */
    r12 = *(rdi);
    if (r12 < *((rdi + 8))) {
        goto label_0;
    }
    goto label_1;
    do {
        r12 += 0x10;
        if (*((rbp + 8)) <= r12) {
            goto label_1;
        }
label_0:
    } while (*(r12) == 0);
    rbx = *((r12 + 8));
    rdx = *((rbp + 0x40));
    if (rbx != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        rbx = rax;
label_2:
        if (rdx != 0) {
            rdi = *(rbx);
            void (*rdx)() ();
            rdx = *((rbp + 0x40));
        }
        rax = *((rbx + 8));
        rcx = *((rbp + 0x48));
        *(rbx) = 0;
        *((rbx + 8)) = rcx;
        *((rbp + 0x48)) = rbx;
    } while (rax != 0);
label_3:
    if (rdx != 0) {
        rdi = *(r12);
        void (*rdx)() ();
    }
    *(r12) = 0;
    r12 += 0x10;
    *((r12 - 8)) = 0;
    if (*((rbp + 8)) > r12) {
        goto label_0;
    }
label_1:
    *((rbp + 0x18)) = 0;
    *((rbp + 0x20)) = 0;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x6840 */
 
int64_t dbg_hash_reset_tuning (Hash_tuning * tuning) {
    rdi = tuning;
    /* void hash_reset_tuning(Hash_tuning * tuning); */
    rax = 0x3f80000000000000;
    *((rdi + 0x10)) = 0;
    *(rdi) = rax;
    rax = 0x3fb4fdf43f4ccccd;
    *((rdi + 8)) = rax;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x7140 */
 
int32_t dbg_triple_compare_ino_str (char ** s1, char ** s2) {
    rdi = s1;
    rsi = s2;
    /* _Bool triple_compare_ino_str( const * x, const * y); */
    rdx = *((rsi + 8));
    eax = 0;
    while (*((rdi + 0x10)) != rcx) {
        return eax;
        rcx = *((rsi + 0x10));
    }
    eax = strcmp (*(rdi), *(rsi));
    al = (eax == 0) ? 1 : 0;
    return eax;
}

/* /tmp/tmp0rcdmt2j @ 0x6730 */
 
int64_t dbg_hash_get_entries (void ** buffer, size_t buffer_size, Hash_table const * table) {
    rsi = buffer;
    rdx = buffer_size;
    rdi = table;
    /* size_t hash_get_entries(Hash_table const * table,void ** buffer,size_t buffer_size); */
    r9 = *(rdi);
    eax = 0;
    if (r9 >= *((rdi + 8))) {
        goto label_2;
    }
    do {
        if (*(r9) != 0) {
            goto label_3;
        }
label_0:
        r9 += 0x10;
    } while (*((rdi + 8)) > r9);
    return eax;
label_3:
    rcx = r9;
    goto label_4;
label_1:
    r8 = *(rcx);
    rax++;
    *((rsi + rax*8 - 8)) = r8;
    rcx = *((rcx + 8));
    if (rcx == 0) {
        goto label_0;
    }
label_4:
    if (rdx > rax) {
        goto label_1;
    }
label_2:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x59c0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmp0rcdmt2j @ 0xa780 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0xa940 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xae70 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x2780 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmp0rcdmt2j @ 0x6b10 */
 
int64_t hash_rehash (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t canary;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdi = rsi;
    r12 = *((rbp + 0x28));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    esi = *((r12 + 0x10));
    xmm0 = *((r12 + 8));
    rax = compute_bucket_size_isra_0 ();
    if (rax == 0) {
        goto label_1;
    }
    rbx = rax;
    if (*((rbp + 0x10)) == rax) {
        goto label_2;
    }
    rax = calloc (rax, 0x10);
    *(rsp) = rax;
    if (rax == 0) {
        goto label_1;
    }
    *((rsp + 0x10)) = rbx;
    rbx <<= 4;
    r13 = rsp;
    edx = 0;
    rax += rbx;
    rsi = rbp;
    rdi = r13;
    *((rsp + 0x28)) = r12;
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x30));
    *((rsp + 0x18)) = 0;
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 0x38));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x38)) = rax;
    rax = *((rbp + 0x40));
    *((rsp + 0x40)) = rax;
    rax = *((rbp + 0x48));
    *((rsp + 0x48)) = rax;
    eax = transfer_entries ();
    r12d = eax;
    if (al != 0) {
        goto label_3;
    }
    rax = *((rsp + 0x48));
    edx = 1;
    rsi = r13;
    rdi = rbp;
    *((rbp + 0x48)) = rax;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x29ba)() ();
    }
    edx = 0;
    rsi = r13;
    rdi = rbp;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x29ba)() ();
    }
    free (*(rsp));
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        eax = r12d;
        return rax;
label_2:
        r12d = 1;
    } while (1);
label_1:
    r12d = 0;
    goto label_0;
label_3:
    free (*(rbp));
    rax = *(rsp);
    *(rbp) = rax;
    rax = *((rsp + 8));
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x10));
    *((rbp + 0x10)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    rax = *((rsp + 0x48));
    *((rbp + 0x48)) = rax;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xb230 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2600)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp0rcdmt2j @ 0xa860 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x9a30 */
 
uint64_t dbg_gl_scratch_buffer_dupfree (uint32_t arg1, size_t size) {
    size_t var_8h;
    rdi = arg1;
    rsi = size;
    /* void * gl_scratch_buffer_dupfree(scratch_buffer * buffer,size_t size); */
    rdi += 0x10;
    r12 = *((rdi - 0x10));
    if (r12 == rdi) {
        goto label_1;
    }
    rax = realloc (r12, rsi);
    if (rax == 0) {
        goto label_2;
    }
    do {
label_0:
        return rax;
label_1:
        rdi = rsi;
        *((rsp + 8)) = rsi;
        rax = malloc (rdi);
        rdi = rax;
    } while (rax == 0);
    rdx = *((rsp + 8));
    rsi = r12;
    void (*0x27d0)() ();
label_2:
    rax = r12;
    goto label_0;
}

/* /tmp/tmp0rcdmt2j @ 0x95b0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0x5e40 */
 
int64_t dbg_freadahead (FILE * fp) {
    rdi = fp;
    /* size_t freadahead(FILE * fp); */
    rcx = *((rdi + 0x20));
    eax = 0;
    if (*((rdi + 0x28)) <= rcx) {
        rax = *((rdi + 0x10));
        rax -= *((rdi + 8));
        if ((*(rdi) & 0x100) == 0) {
            goto label_0;
        }
        rdx = *((rdi + 0x58));
        rdx -= *((rdi + 0x48));
        rax += rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x9500 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x29fc)() ();
    }
    if (rax == 0) {
        void (*0x29fc)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x5cc0 */
 
int64_t dbg_seen_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg1, uint32_t arg2, int64_t arg3) {
    F_triple new_ent;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool seen_file(Hash_table const * ht,char const * file,stat const * stats); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (rdi != 0) {
        rax = *((rdx + 8));
        *(rsp) = rsi;
        rsi = rsp;
        *((rsp + 8)) = rax;
        rax = *(rdx);
        *((rsp + 0x10)) = rax;
        rax = hash_lookup ();
        al = (rax != 0) ? 1 : 0;
    }
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x63a0 */
 
int64_t hash_get_max_bucket_length (int64_t arg1) {
    rdi = arg1;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8d = 0;
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_1;
    do {
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_1;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_2;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_2:
    if (r8 < rdx) {
        r8 = rdx;
    }
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_0;
    }
label_1:
    rax = r8;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xaf60 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmp0rcdmt2j @ 0xabb0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x46d0 */
 
uint64_t set_simple_backup_suffix (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    if (rdi == 0) {
        goto label_2;
    }
label_1:
    while (rbx != rax) {
label_0:
        rbx = 0x0000d0b3;
        *(obj.simple_backup_suffix) = rbx;
        return;
        rdi = rbx;
        rax = last_component ();
    }
    *(obj.simple_backup_suffix) = rbx;
    return rax;
label_2:
    rax = getenv ("SIMPLE_BACKUP_SUFFIX");
    rbx = rax;
    if (rax == 0) {
        goto label_0;
    }
    goto label_1;
}

/* /tmp/tmp0rcdmt2j @ 0x5aa0 */
 
uint64_t dir_len (uint32_t arg1) {
    rdi = arg1;
    ebp = 0;
    rbx = rdi;
    bpl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbx;
    while (rax > rbp) {
        rdx = rax - 1;
        if (*((rbx + rax - 1)) != 0x2f) {
            goto label_0;
        }
        rax = rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xb3d0 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0xb3f0)() ();
}

/* /tmp/tmp0rcdmt2j @ 0x58a0 */
 
void dbg_close_stdin_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdin_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmp0rcdmt2j @ 0x6f20 */
 
int64_t dbg_hash_remove (int64_t arg_8h, int64_t arg1) {
    hash_entry * bucket;
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_remove(Hash_table * table, const * entry); */
    ecx = 1;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    rax = hash_find_entry ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = *(rsp);
    *((rbx + 0x20))--;
    while (rax <= 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
        rax = *((rbx + 0x18));
        rax--;
        *((rbx + 0x18)) = rax;
        if (rax < 0) {
            goto label_5;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_6;
        }
label_1:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_2:
        rax = *((rbx + 0x28));
        xmm0 = *(rax);
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm0, xmm5");
    }
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm0 = *(rax);
    __asm ("mulss xmm0, xmm4");
    __asm ("comiss xmm0, xmm5");
    if (rax <= 0) {
        goto label_0;
    }
    __asm ("mulss xmm4, dword [rax + 4]");
    if (*((rax + 0x10)) == 0) {
        __asm ("mulss xmm4, dword [rax + 8]");
    }
    __asm ("comiss xmm4, dword [0x0000d208]");
    if (*((rax + 0x10)) >= 0) {
        goto label_7;
    }
    __asm ("cvttss2si rsi, xmm4");
label_3:
    rdi = rbx;
    al = hash_rehash ();
    if (al != 0) {
        goto label_0;
    }
    rbp = *((rbx + 0x48));
    if (rbp == 0) {
        goto label_8;
    }
    do {
        rbp = *((rbp + 8));
        rax = free (rbp);
    } while (rbp != 0);
label_8:
    *((rbx + 0x48)) = 0;
    goto label_0;
label_5:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_1;
    }
label_6:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_2;
label_7:
    __asm ("subss xmm4, dword [0x0000d208]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_3;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x8cb0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x8bc0)() ();
}

/* /tmp/tmp0rcdmt2j @ 0xacb0 */
 
int64_t dbg_yesno (void) {
    char * response;
    size_t response_size;
    void * ptr;
    int64_t var_10h;
    int64_t var_18h;
    /* _Bool yesno(); */
    edx = 0xa;
    r12d = 0;
    rcx = stdin;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rsi = rsp + 0x10;
    rdi = rsp + 8;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    rax = getdelim ();
    if (rax <= 0) {
        goto label_0;
    }
    rdi = *((rsp + 8));
    rax = rdi + rax - 1;
    while (1) {
        eax = rpmatch ();
        r12b = (eax > 0) ? 1 : 0;
label_0:
        free (*((rsp + 8)));
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        eax = r12d;
        return rax;
        *(rax) = 0;
        rdi = *((rsp + 8));
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x6370 */
 
int64_t hash_get_n_buckets (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x10));
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x8d90 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0x66a0 */
 
uint64_t hash_get_next (int64_t arg_8h, uint32_t arg_10h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rsi = *((rdi + 0x10));
    rdi = rbx;
    rax = uint64_t (*rbp + 0x30)(uint64_t) (rbx);
    if (rax >= *((rbp + 0x10))) {
        void (*0x29b5)() ();
    }
    rax <<= 4;
    rax += *(rbp);
    rdx = rax;
    while (rcx != rbx) {
        if (rdx == 0) {
            goto label_0;
        }
        rcx = *(rdx);
        rdx = *((rdx + 8));
    }
    if (rdx != 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 8));
    while (rdx > rax) {
        r8 = *(rax);
        if (r8 != 0) {
            goto label_2;
        }
        rax += 0x10;
    }
    r8d = 0;
label_2:
    rax = r8;
    return rax;
label_1:
    r8 = *(rdx);
    rax = r8;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x2a10 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    char * dest_base;
    stat st;
    int64_t var_8h;
    int64_t var_10h;
    int32_t var_18h;
    uint32_t var_1ch;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_48h;
    int64_t var_c8h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15d = 0;
    r14 = obj_long_options;
    r13 = "bdfinrst:vFLPS:T";
    r12 = 0x0000c11b;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0xc8)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000d6d8);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = 0x0000cfbc;
    textdomain (r12, rsi);
    rdi = dbg_close_stdin;
    atexit ();
    *(obj.hard_dir_link) = 0;
    *(obj.verbose) = 0;
    *(obj.interactive) = 0;
    *(obj.remove_existing_files) = 0;
    *(obj.symbolic_link) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *(rsp) = 0;
    do {
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_11;
        }
        if (eax <= 0x76) {
            if (eax > 0x45) {
                eax -= 0x46;
                if (eax > 0x30) {
                    goto label_2;
                }
                rax = *((r12 + rax*4));
                rax += r12;
                /* switch table (49 cases) at 0xcfbc */
                eax = void (*rax)() ();
            }
            if (eax == 0xffffff7d) {
                goto label_12;
            }
            if (eax != 0xffffff7e) {
                goto label_2;
            }
            usage (0);
label_1:
            edx = 5;
            rax = dcgettext (0, "missing file operand");
            eax = 0;
            error (0, 0, rax);
        }
label_2:
        usage (1);
        *(obj.hard_dir_link) = 1;
    } while (1);
    *(obj.verbose) = 1;
    goto label_0;
    if (r15 != 0) {
        goto label_13;
    }
    rdi = optarg;
    rsi = rsp + 0x30;
    eax = stat ();
    if (eax != 0) {
        goto label_14;
    }
    eax = *((rsp + 0x48));
    r15 = optarg;
    eax &= 0xf000;
    if (eax == 0x4000) {
        goto label_0;
    }
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "target %s is not a directory");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
    *(obj.symbolic_link) = 1;
    goto label_0;
    *(obj.relative) = 1;
    goto label_0;
    *(obj.dereference_dest_dir_symlinks) = 0;
    goto label_0;
    *(obj.logical) = 1;
    goto label_0;
    rax = optarg;
    *(rsp) = 1;
    *((rsp + 0x10)) = rax;
    goto label_0;
    *(obj.logical) = 0;
    goto label_0;
    *(obj.remove_existing_files) = 0;
    *(obj.interactive) = 1;
    goto label_0;
    *(obj.remove_existing_files) = 1;
    *(obj.interactive) = 0;
    goto label_0;
    rax = optarg;
    *(rsp) = 1;
    if (rax == 0) {
        rax = *((rsp + 8));
    }
    *((rsp + 8)) = rax;
    goto label_0;
label_12:
    eax = 0;
    version_etc (*(obj.stdout), 0x0000c09b, "GNU coreutils", *(obj.Version), "Mike Parker", "David MacKenzie");
    eax = exit (0);
    *((rsp + 0x18)) = 1;
    goto label_0;
label_11:
    r8d = eax;
    rax = *(obj.optind);
    ebp -= eax;
    if (ebp <= 0) {
        goto label_1;
    }
    if (*(obj.relative) != 0) {
        if (*(obj.symbolic_link) != 0) {
            goto label_15;
        }
        edx = 5;
        rax = dcgettext (0, "cannot do --relative without --symbolic");
        eax = 0;
        error (1, 0, rax);
    }
label_15:
    if (*(obj.hard_dir_link) == 0) {
        *(obj.beware_hard_dir_link) = 0;
    }
    rbx = rbx + rax*8;
    if (*((rsp + 0x18)) != 0) {
        goto label_16;
    }
    r13b = (r15 == 0) ? 1 : 0;
    if (ebp > 1) {
        goto label_17;
    }
    if (r13b == 0) {
        goto label_17;
    }
    if (*(rsp) != 0) {
        goto label_18;
    }
    rdi = *((rsp + 0x10));
    r12 = 0x0000d08b;
    *(obj.backup_type) = 0;
    set_simple_backup_suffix ();
    *((rsp + 0x18)) = 1;
    *((rsp + 0x1c)) = 0xffffff9c;
label_3:
    *(rsp) = r12;
    ebx = *((rsp + 0x1c));
    r15d = 0;
    r13d = 1;
    r14 = rsp + 0x28;
    while (*((rsp + 0x18)) > r15d) {
        rdi = *((rbp + r15*8));
        rax = last_component ();
        rdi = *(rsp);
        rdx = r14;
        rsi = rax;
        rax = file_name_concat ();
        r12 = rax;
        strip_trailing_slashes (*((rsp + 0x28)));
        r8d |= 0xffffffff;
        r15++;
        eax = do_link (*((rbp + r15*8)), ebx, *((rsp + 0x28)), r12, r8);
        r13d &= eax;
        free (r12);
    }
label_9:
    r13d ^= 1;
    edi = (int32_t) r13b;
    exit (rdi);
label_16:
    if (r15 != 0) {
        goto label_19;
    }
    if (ebp == 2) {
        goto label_10;
    }
    ebp--;
    if (ebp == 0) {
        goto label_20;
    }
    rsi = *((rbx + 0x10));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
label_7:
    rax = dcgettext (0, "extra operand %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_2;
label_17:
    if (ebp != 2) {
        goto label_21;
    }
    if (r13b == 0) {
        goto label_21;
    }
    rdx = *((rbx + 8));
    rdi = *(rbx);
    esi = 0xffffff9c;
    eax = atomic_link ();
    r8d = eax;
    if (eax >= 0) {
        if (eax == 0x11) {
            goto label_6;
        }
        eax &= 0xfffffffd;
        if (eax != 0x14) {
            goto label_22;
        }
    }
label_6:
    rax = (int64_t) ebp;
    r12 = *((rbx + rax*8 - 8));
label_5:
    *((rsp + 0x18)) = r8d;
    edx -= edx;
    eax = 0;
    edx &= 0x20000;
    edx += 0x210000;
    eax = openat_safer (0xffffff9c, r12, rdx, rcx, r8);
    *((rsp + 0x1c)) = eax;
    rax = errno_location ();
    r8d = *((rsp + 0x18));
    r14d = *(rax);
    if (*((rsp + 0x1c)) < 0) {
        goto label_23;
    }
    r13d = (int32_t) r13b;
    ebp -= r13d;
    *((rsp + 0x18)) = ebp;
    if (*(rsp) != 0) {
        goto label_8;
    }
    eax = 0;
label_4:
    rdi = *((rsp + 0x10));
    *(rsp) = r8d;
    *(obj.backup_type) = eax;
    set_simple_backup_suffix ();
    r8d = *(rsp);
    if (r12 == 0) {
        goto label_24;
    }
    if (*((rsp + 0x18)) <= 1) {
        goto label_3;
    }
    if (*(obj.remove_existing_files) == 0) {
        goto label_3;
    }
    if (*(obj.symbolic_link) != 0) {
        goto label_3;
    }
    if (*(obj.backup_type) == 3) {
        goto label_3;
    }
    rax = hash_initialize (0x3d, 0, dbg.triple_hash, dbg.triple_compare, dbg.triple_free);
    *(obj.dest_set) = rax;
    if (rax != 0) {
        goto label_3;
    }
    xalloc_die ();
label_10:
    if (*(rsp) == 0) {
        goto label_25;
    }
    *((rsp + 0x18)) = 2;
    r12d = 0;
label_8:
    edx = 5;
    *(rsp) = r8d;
    rax = dcgettext (0, "backup type");
    rsi = *((rsp + 8));
    xget_version (rax);
    r8d = *(rsp);
    goto label_4;
label_21:
    r12 = r15;
    if (r15 != 0) {
        goto label_5;
    }
    goto label_6;
label_20:
    rsi = *(rbx);
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "missing destination file operand after %s";
    r12 = rax;
    goto label_7;
label_22:
    r12d = 0;
    *((rsp + 0x18)) = 2;
    if (*(rsp) != 0) {
        goto label_8;
    }
label_25:
    rdi = *((rsp + 0x10));
    *(rsp) = r8d;
    *(obj.backup_type) = 0;
    set_simple_backup_suffix ();
label_24:
    eax = do_link (*(rbx), 0xffffff9c, *((rbx + 8)), rdx, *(rsp));
    r13d = eax;
    goto label_9;
label_23:
    if (ebp != 2) {
        goto label_26;
    }
    if (r15 == 0) {
        goto label_10;
    }
label_26:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "target %s");
    rcx = r12;
    eax = 0;
    error (1, r14d, rax);
label_18:
    *((rsp + 0x18)) = 1;
    r12 = 0x0000d08b;
    *((rsp + 0x1c)) = 0xffffff9c;
    goto label_8;
label_14:
    rsi = optarg;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to access %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_19:
    edx = 5;
    rax = dcgettext (0, "cannot combine --target-directory and --no-target-directory");
    eax = 0;
    error (1, 0, rax);
label_13:
    edx = 5;
    rax = dcgettext (0, "multiple target directories specified");
    eax = 0;
    return error (1, 0, rax);
}

/* /tmp/tmp0rcdmt2j @ 0x3ca0 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... [-T] TARGET LINK_NAME\n  or:  %s [OPTION]... TARGET\n  or:  %s [OPTION]... TARGET... DIRECTORY\n  or:  %s [OPTION]... -t DIRECTORY TARGET...\n");
    r9 = r12;
    r8 = r12;
    rcx = r12;
    rsi = rax;
    rdx = r12;
    edi = 1;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "In the 1st form, create a link to TARGET with the name LINK_NAME.\nIn the 2nd form, create a link to TARGET in the current directory.\nIn the 3rd and 4th forms, create links to each TARGET in DIRECTORY.\nCreate hard links by default, symbolic links with --symbolic.\nBy default, each destination (name of new link) should not already exist.\nWhen creating hard links, each TARGET must exist.  Symbolic links\ncan hold arbitrary text; if later resolved, a relative link is\ninterpreted in relation to its parent directory.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --backup[=CONTROL]      make a backup of each existing destination file\n  -b                          like --backup but does not accept an argument\n  -d, -F, --directory         allow the superuser to attempt to hard link\n                                directories (note: will probably fail due to\n                                system restrictions, even for the superuser)\n  -f, --force                 remove existing destination files\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -i, --interactive           prompt whether to remove destinations\n  -L, --logical               dereference TARGETs that are symbolic links\n  -n, --no-dereference        treat LINK_NAME as a normal file if\n                                it is a symbolic link to a directory\n  -P, --physical              make hard links directly to symbolic links\n  -r, --relative              with -s, create links relative to link location\n  -s, --symbolic              make symbolic links instead of hard links\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -S, --suffix=SUFFIX         override the usual backup suffix\n  -t, --target-directory=DIRECTORY  specify the DIRECTORY in which to create\n                                the links\n  -T, --no-target-directory   treat LINK_NAME as a normal file always\n  -v, --verbose               print name of each linked file\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe backup suffix is '~', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\nThe version control method may be selected via the --backup option or through\nthe VERSION_CONTROL environment variable.  Here are the values:\n\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "  none, off       never make backups (even if --backup is given)\n  numbered, t     make numbered backups\n  existing, nil   numbered if numbered backups exist, simple otherwise\n  simple, never   always make simple backups\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    rax = dcgettext (0, "\nUsing -s ignores -L and -P.  Otherwise, the last option specified controls\nbehavior when a TARGET is a symbolic link, defaulting to %s.\n");
    rdx = 0x0000c09e;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000c0a1;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x6c;
    *((rsp + 8)) = rax;
    rax = 0x0000c11b;
    edi = 0x6e;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x0000c09b;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000c125, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x0000c0bd;
    printf_chk ();
    rax = 0x0000d6d8;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000c125, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x0000c09b;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x0000c0bd;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x0000c09b;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmp0rcdmt2j @ 0x8a60 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x9f30 */
 
int64_t dbg_gen_tempname (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int gen_tempname(char * tmpl,int suffixlen,int flags,int kind); */
    rcx = (int64_t) ecx;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = obj_tryfunc_0;
    *((rsp + 4)) = edx;
    try_tempname_len (rdi, rsi, rsp + 4, *((rax + rcx*8)), 6);
    rdx = *((rsp + 8));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x9150 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xb3f0 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = section__dynsym;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x70c0 */
 
int64_t dbg_triple_hash_no_name (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t triple_hash_no_name( const * x,size_t table_size); */
    rax = *((rdi + 8));
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x8f80 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x29e3)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x6650 */
 
int64_t hash_get_first (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rax = *(rdi);
    rdx = *((rdi + 8));
    if (rax < rdx) {
        goto label_1;
    }
    void (*0x29af)() ();
    do {
        rax += 0x10;
        if (rax >= rdx) {
            goto label_2;
        }
label_1:
        r8 = *(rax);
    } while (r8 == 0);
    rax = r8;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
label_2:
    return hash_get_first_cold ();
}

/* /tmp/tmp0rcdmt2j @ 0x8e60 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x29d9)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x5d20 */
 
uint64_t dbg_file_name_concat (void) {
    int64_t var_7h;
    int64_t var_8h;
    /* char * file_name_concat(char const * dir,char const * base,char ** base_in_result); */
    rax = mfile_name_concat (rdi, rsi, rdx);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x95f0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0x70b0 */
 
void dbg_hash_delete (int64_t arg_8h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_delete(Hash_table * table, const * entry); */
    return void (*0x6f20)() ();
}

/* /tmp/tmp0rcdmt2j @ 0x8b00 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x29ce)() ();
    }
    if (rdx == 0) {
        void (*0x29ce)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x9010 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00012270]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00012280]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xaad0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x4ec0 */
 
uint64_t dbg_xget_version (uint32_t arg2) {
    rsi = arg2;
    /* backup_type xget_version(char const * context,char const * version); */
    if (rsi != 0) {
        if (*(rsi) != 0) {
            goto label_0;
        }
    }
    rax = getenv (0x0000d0cb);
    rsi = rax;
    eax = 2;
    if (rsi != 0) {
        if (*(rsi) != 0) {
            goto label_1;
        }
    }
    return rax;
label_1:
    rbx = obj_backup_types;
    _xargmatch_internal ("$VERSION_CONTROL", rsi, obj.backup_args, rbx, 4, *(obj.argmatch_die));
    eax = *((rbx + rax*4));
    return rax;
label_0:
    rbx = obj_backup_types;
    _xargmatch_internal (rdi, rsi, obj.backup_args, rbx, 4, *(obj.argmatch_die));
    eax = *((rbx + rax*4));
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xab50 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x8d60 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp0rcdmt2j @ 0xa7f0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x6780 */
 
int64_t dbg_hash_do_for_each (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t hash_do_for_each(Hash_table const * table,Hash_processor processor,void * processor_data); */
    r14 = *(rdi);
    if (r14 >= *((rdi + 8))) {
        goto label_3;
    }
    r15 = rdi;
    r13 = rdx;
    r12d = 0;
    do {
        rdi = *(r14);
        if (rdi != 0) {
            goto label_4;
        }
label_0:
        r14 += 0x10;
    } while (*((r15 + 8)) > r14);
label_2:
    rax = r12;
    return rax;
label_4:
    rbx = r14;
    goto label_5;
label_1:
    rbx = *((rbx + 8));
    r12++;
    if (rbx == 0) {
        goto label_0;
    }
    rdi = *(rbx);
label_5:
    rsi = r13;
    al = void (*rbp)() ();
    if (al != 0) {
        goto label_1;
    }
    goto label_2;
label_3:
    r12d = 0;
    goto label_2;
}

/* /tmp/tmp0rcdmt2j @ 0x5e70 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x28d0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmp0rcdmt2j @ 0x8aa0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xa9d0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmp0rcdmt2j @ 0x5850 */
 
int64_t dbg_canonicalize_filename_mode (void) {
    scratch_buffer rname_buffer;
    int64_t var_418h;
    /* char * canonicalize_filename_mode(char const * name,canonicalize_mode_t can_mode); */
    rax = *(fs:0x28);
    *((rsp + 0x418)) = rax;
    eax = 0;
    canonicalize_filename_mode_stk (rdi, rsi, rsp);
    rdx = *((rsp + 0x418));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x4e60 */
 
int32_t get_version (uint32_t arg2) {
    rsi = arg2;
    eax = 2;
    if (rsi != 0) {
        if (*(rsi) == 0) {
            return eax;
        }
        rbx = obj_backup_types;
        _xargmatch_internal (rdi, rsi, obj.backup_args, rbx, 4, *(obj.argmatch_die));
        eax = *((rbx + rax*4));
        return eax;
    }
    return eax;
}

/* /tmp/tmp0rcdmt2j @ 0xa610 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmp0rcdmt2j @ 0x2790 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmp0rcdmt2j @ 0x59b0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name_1) = rdi;
}

/* /tmp/tmp0rcdmt2j @ 0x5ba0 */
 
uint64_t dbg_strip_trailing_slashes (int64_t arg1) {
    rdi = arg1;
    /* _Bool strip_trailing_slashes(char * file); */
    rax = last_component ();
    rbx = rax;
    if (*(rax) == 0) {
        rbx = rbp;
    }
    rax = base_len (rbx);
    rbx += rax;
    *(rbx) = 0;
    al = (*(rbx) != 0) ? 1 : 0;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x9f00 */
 
uint64_t gen_tempname_len (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    rax = obj_tryfunc_0;
    rcx = (int64_t) ecx;
    *((rsp + 0xc)) = edx;
    try_tempname_len (rdi, rsi, rsp + 0xc, *((rax + rcx*8)), r8);
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x9b10 */
 
uint64_t dbg_gl_scratch_buffer_grow_preserve (int64_t arg1) {
    rdi = arg1;
    /* _Bool gl_scratch_buffer_grow_preserve(scratch_buffer * buffer); */
    r14 = rdi + 0x10;
    r13 = *((rdi + 8));
    rbx = rdi;
    r12 = *(rdi);
    rbp = r13 + r13;
    if (r12 == r14) {
        goto label_1;
    }
    if (r13 > rbp) {
        goto label_2;
    }
    rax = realloc (r12, rbp);
    rcx = rax;
    if (rax == 0) {
        goto label_3;
    }
    do {
        *(rbx) = rcx;
        eax = 1;
        *((rbx + 8)) = rbp;
label_0:
        return rax;
label_1:
        rax = malloc (rbp);
        if (rax == 0) {
            goto label_4;
        }
        rax = memcpy (rax, r12, r13);
        rcx = rax;
    } while (1);
label_2:
    errno_location ();
    *(rax) = 0xc;
    do {
        eax = free (r12);
        *(rbx) = r14;
        eax = 0;
        *((rbx + 8)) = section..dynsym;
        goto label_0;
label_3:
        r12 = *(rbx);
    } while (1);
label_4:
    eax = 0;
    goto label_0;
}

/* /tmp/tmp0rcdmt2j @ 0xb1c0 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp0rcdmt2j @ 0x2560 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmp0rcdmt2j @ 0x9a90 */
 
int64_t dbg_gl_scratch_buffer_grow (uint32_t arg1) {
    rdi = arg1;
    /* _Bool gl_scratch_buffer_grow(scratch_buffer * buffer); */
    rbx = rdi;
    rax = *((rdi + 8));
    rdi = *(rdi);
    r12 = rbx + 0x10;
    rbp = rax + rax;
    if (rdi != r12) {
        free (rdi);
        rax = *((rbx + 8));
    }
    if (rax <= rbp) {
        rax = malloc (rbp);
        if (rax == 0) {
            goto label_0;
        }
        r8d = 1;
        *(rbx) = rax;
        *((rbx + 8)) = rbp;
        eax = r8d;
        return rax;
    }
    errno_location ();
    *(rax) = 0xc;
label_0:
    rax = r12;
    r8d = 0;
    *(rbx) = rax;
    eax = r8d;
    *((rbx + 8)) = rbp;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x41a0 */
 
uint64_t dbg_force_linkat (int64_t arg_180h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    link_arg arg;
    char[256] buf;
    int64_t var_4h;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_24h;
    int64_t var_30h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int force_linkat(int srcdir,char const * srcname,int dstdir,char const * dstname,int flags,_Bool force,int linkat_errno); */
    r15d = r9d;
    r14d = edi;
    r13 = rcx;
    rbx = rsi;
    rdx = *(fs:0x28);
    *((rsp + 0x138)) = rdx;
    edx = 0;
    eax = *((rsp + 0x180));
    if (eax < 0) {
        goto label_3;
    }
label_0:
    *((rsp + 4)) = r8d;
    if (r15b == 1) {
        if (eax == 0x11) {
            rax = rsp + 0x30;
            rsi = rax;
            *((rsp + 8)) = rax;
            rax = samedir_template (r13, rsi);
            r8d = *((rsp + 4));
            r15 = rax;
            if (rax == 0) {
                goto label_4;
            }
            *((rsp + 0x24)) = r8d;
            *((rsp + 0x10)) = r14d;
            *((rsp + 0x18)) = rbx;
            *((rsp + 0x20)) = ebp;
            eax = try_tempname_len (rax, 0, rsp + 0x10, dbg.try_link, 6);
            if (eax == 0) {
                goto label_5;
            }
            rax = errno_location ();
            r12d = *(rax);
label_2:
            if (r15 == *((rsp + 8))) {
                goto label_1;
            }
            eax = free (r15);
        }
    } else {
        r12d = eax;
    }
    do {
label_1:
        rax = *((rsp + 0x138));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        eax = r12d;
        return rax;
label_3:
        edx = ebp;
        *((rsp + 4)) = r8d;
        eax = linkat ();
        r12d = eax;
    } while (eax == 0);
    rax = errno_location ();
    r8d = *((rsp + 4));
    eax = *(rax);
    goto label_0;
label_4:
    rax = errno_location ();
    r12d = *(rax);
    goto label_1;
label_5:
    rcx = r13;
    edx = ebp;
    rsi = r15;
    edi = ebp;
    eax = renameat ();
    r12d = 0xffffffff;
    if (eax != 0) {
        rax = errno_location ();
        r12d = *(rax);
    }
    edx = 0;
    rsi = r15;
    edi = ebp;
    unlinkat ();
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xa000 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = "%s (%s) %s\n";
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000d692);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000d980;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xd980 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2930)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2930)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2930)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmp0rcdmt2j @ 0x58b0 */
 
uint64_t dbg_close_stdin (void) {
    /* void close_stdin(); */
    rbp = stdin;
    rdi = stdin;
    rax = freadahead ();
    if (rax != 0) {
        goto label_2;
    }
    eax = close_stream (rbp);
    if (eax != 0) {
        goto label_1;
    }
    do {
        void (*0x59d0)() ();
label_2:
        eax = rpl_fseeko (rbp, 0, 1, rcx);
        rdi = stdin;
        if (eax == 0) {
            goto label_3;
        }
label_0:
        eax = close_stream (rdi);
    } while (eax == 0);
label_1:
    edx = 5;
    rax = dcgettext (0, "error closing file");
    r13 = file_name;
    r12 = rax;
    rax = errno_location ();
    if (r13 == 0) {
        goto label_4;
    }
    rax = quotearg_colon (r13, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbp), "%s: %s");
    close_stdout ();
    do {
        rax = exit (*(obj.exit_failure));
label_4:
        rcx = r12;
        eax = 0;
        error (0, *(rax), 0x0000da57);
        close_stdout ();
    } while (1);
label_3:
    eax = rpl_fflush (rdi);
    rdi = stdin;
    if (eax == 0) {
        goto label_0;
    }
    close_stream (rdi);
    goto label_1;
}

/* /tmp/tmp0rcdmt2j @ 0x9fa0 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xa490 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xab30 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x6390 */
 
int64_t hash_get_n_entries (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x20));
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0xb790 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmp0rcdmt2j @ 0xa830 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0x4310 */
 
int64_t dbg_force_symlinkat (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    symlink_arg arg;
    char[256] buf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_118h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int force_symlinkat(char const * srcname,int dstdir,char const * dstname,_Bool force,int symlinkat_errno); */
    r14 = rdi;
    r13 = rdx;
    ebx = ecx;
    rax = *(fs:0x28);
    *((rsp + 0x118)) = rax;
    eax = 0;
    if (r8d < 0) {
        goto label_3;
    }
label_0:
    if (bl == 1) {
        if (r8d == 0x11) {
            rbx = rsp + 0x10;
            rax = samedir_template (r13, rbx);
            r15 = rax;
            if (rax == 0) {
                goto label_4;
            }
            *(rsp) = r14;
            *((rsp + 8)) = ebp;
            eax = try_tempname_len (rax, 0, rsp, dbg.try_symlink, 6);
            if (eax == 0) {
                goto label_5;
            }
            rax = errno_location ();
            r12d = *(rax);
label_2:
            if (r15 == rbx) {
                goto label_1;
            }
            free (r15);
        }
    } else {
        r12d = r8d;
    }
    do {
label_1:
        rax = *((rsp + 0x118));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        eax = r12d;
        return rax;
label_3:
        eax = symlinkat ();
        r12d = eax;
    } while (eax == 0);
    rax = errno_location ();
    r8d = *(rax);
    goto label_0;
label_4:
    rax = errno_location ();
    r12d = *(rax);
    goto label_1;
label_5:
    rcx = r13;
    edx = ebp;
    rsi = r15;
    edi = ebp;
    eax = renameat ();
    r12d = 0xffffffff;
    if (eax == 0) {
        goto label_2;
    }
    rax = errno_location ();
    edx = 0;
    rsi = r15;
    edi = ebp;
    r12d = *(rax);
    unlinkat ();
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmp0rcdmt2j @ 0xabf0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp0rcdmt2j @ 0xb160 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmp0rcdmt2j @ 0x24f0 */
 
void getenv (void) {
    /* [15] -r-x section size 1200 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp0rcdmt2j @ 0x0 */
 
uint32_t rotate_left32 (uint32_t value, uint32_t count) {
    const uint32_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax |= 0x28004000;
    *(rdi) += ah;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += bl;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    al += bl;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += cl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("clc");
    *(rax) += al;
    __asm ("clc");
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    cl += al;
    tmp_0 = edi;
    edi = eax;
    eax = tmp_0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) = rotate_left32 (*(rdi), 0);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    al += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax = (int32_t) ax;
    *(ds:rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax = (int32_t) ax;
    *(ds:rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmp0rcdmt2j @ 0x2540 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmp0rcdmt2j @ 0x2550 */
 
void strcpy (void) {
    __asm ("bnd jmp qword [reloc.strcpy]");
}

/* /tmp/tmp0rcdmt2j @ 0x2570 */
 
void mkdir (void) {
    __asm ("bnd jmp qword [reloc.mkdir]");
}

/* /tmp/tmp0rcdmt2j @ 0x2580 */
 
void unlinkat (void) {
    __asm ("bnd jmp qword [reloc.unlinkat]");
}

/* /tmp/tmp0rcdmt2j @ 0x25a0 */
 
void faccessat (void) {
    __asm ("bnd jmp qword [reloc.faccessat]");
}

/* /tmp/tmp0rcdmt2j @ 0x25b0 */
 
void readlink (void) {
    __asm ("bnd jmp qword [reloc.readlink]");
}

/* /tmp/tmp0rcdmt2j @ 0x25c0 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmp0rcdmt2j @ 0x25d0 */
 
void clock_gettime (void) {
    __asm ("bnd jmp qword [reloc.clock_gettime]");
}

/* /tmp/tmp0rcdmt2j @ 0x25e0 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmp0rcdmt2j @ 0x25f0 */
 
void pathconf (void) {
    __asm ("bnd jmp qword [reloc.pathconf]");
}

/* /tmp/tmp0rcdmt2j @ 0x2600 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp0rcdmt2j @ 0x2610 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmp0rcdmt2j @ 0x2630 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmp0rcdmt2j @ 0x2650 */
 
void openat (void) {
    __asm ("bnd jmp qword [reloc.openat]");
}

/* /tmp/tmp0rcdmt2j @ 0x2670 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmp0rcdmt2j @ 0x2680 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmp0rcdmt2j @ 0x2690 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmp0rcdmt2j @ 0x26b0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmp0rcdmt2j @ 0x26c0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp0rcdmt2j @ 0x26d0 */
 
void getcwd (void) {
    __asm ("bnd jmp qword [reloc.getcwd]");
}

/* /tmp/tmp0rcdmt2j @ 0x26f0 */
 
void rewinddir (void) {
    __asm ("bnd jmp qword [reloc.rewinddir]");
}

/* /tmp/tmp0rcdmt2j @ 0x2700 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmp0rcdmt2j @ 0x2710 */
 
void closedir (void) {
    __asm ("bnd jmp qword [reloc.closedir]");
}

/* /tmp/tmp0rcdmt2j @ 0x2720 */
 
void lstat (void) {
    __asm ("bnd jmp qword [reloc.lstat]");
}

/* /tmp/tmp0rcdmt2j @ 0x2730 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmp0rcdmt2j @ 0x2750 */
 
void rawmemchr (void) {
    __asm ("bnd jmp qword [reloc.rawmemchr]");
}

/* /tmp/tmp0rcdmt2j @ 0x2770 */
 
void getdelim (void) {
    __asm ("bnd jmp qword [reloc.__getdelim]");
}

/* /tmp/tmp0rcdmt2j @ 0x27a0 */
 
void fpathconf (void) {
    __asm ("bnd jmp qword [reloc.fpathconf]");
}

/* /tmp/tmp0rcdmt2j @ 0x27b0 */
 
void rpmatch (void) {
    __asm ("bnd jmp qword [reloc.rpmatch]");
}

/* /tmp/tmp0rcdmt2j @ 0x27c0 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmp0rcdmt2j @ 0x27e0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmp0rcdmt2j @ 0x27f0 */
 
void readdir (void) {
    __asm ("bnd jmp qword [reloc.readdir]");
}

/* /tmp/tmp0rcdmt2j @ 0x2810 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmp0rcdmt2j @ 0x2830 */
 
void renameat2 (void) {
    __asm ("bnd jmp qword [reloc.renameat2]");
}

/* /tmp/tmp0rcdmt2j @ 0x2840 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmp0rcdmt2j @ 0x2860 */
 
void linkat (void) {
    __asm ("bnd jmp qword [reloc.linkat]");
}

/* /tmp/tmp0rcdmt2j @ 0x2890 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmp0rcdmt2j @ 0x28a0 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmp0rcdmt2j @ 0x28c0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmp0rcdmt2j @ 0x28d0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmp0rcdmt2j @ 0x28f0 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmp0rcdmt2j @ 0x2900 */
 
void renameat (void) {
    __asm ("bnd jmp qword [reloc.renameat]");
}

/* /tmp/tmp0rcdmt2j @ 0x2940 */
 
void getrandom (void) {
    __asm ("bnd jmp qword [reloc.getrandom]");
}

/* /tmp/tmp0rcdmt2j @ 0x2950 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmp0rcdmt2j @ 0x2960 */
 
void symlinkat (void) {
    __asm ("bnd jmp qword [reloc.symlinkat]");
}

/* /tmp/tmp0rcdmt2j @ 0x2970 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmp0rcdmt2j @ 0x2980 */
 
void fstatat (void) {
    __asm ("bnd jmp qword [reloc.fstatat]");
}

/* /tmp/tmp0rcdmt2j @ 0x2990 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmp0rcdmt2j @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1216 named .plt */
    __asm ("bnd jmp qword [0x00011d78]");
}

/* /tmp/tmp0rcdmt2j @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2440 */
 
void fcn_00002440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2450 */
 
void fcn_00002450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2460 */
 
void fcn_00002460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2470 */
 
void fcn_00002470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2480 */
 
void fcn_00002480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x2490 */
 
void fcn_00002490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x24a0 */
 
void fcn_000024a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x24b0 */
 
void fcn_000024b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x24c0 */
 
void fcn_000024c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp0rcdmt2j @ 0x24d0 */
 
void fcn_000024d0 (void) {
    return __asm ("bnd jmp section..plt");
}
