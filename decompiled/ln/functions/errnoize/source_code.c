errnoize (int status)
{
  return status < 0 ? errno : 0;
}