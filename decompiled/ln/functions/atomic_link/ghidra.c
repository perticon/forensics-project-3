int atomic_link(char *param_1,int param_2,char *param_3)

{
  int iVar1;
  int *piVar2;
  
  if (symbolic_link == '\0') {
    if (beware_hard_dir_link != '\0') {
      return -1;
    }
    iVar1 = linkat(-100,param_1,param_2,param_3,(uint)logical << 10);
  }
  else {
    if (relative != '\0') {
      return -1;
    }
    iVar1 = symlinkat(param_1,param_2,param_3);
  }
  if (iVar1 < 0) {
    piVar2 = __errno_location();
    return *piVar2;
  }
  return 0;
}