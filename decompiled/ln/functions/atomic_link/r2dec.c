uint64_t atomic_link (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    if (*(obj.symbolic_link) == 0) {
        goto label_1;
    }
    if (*(obj.relative) != 0) {
        goto label_2;
    }
    eax = symlinkat ();
    if (eax < 0) {
        goto label_3;
    }
    do {
        eax = 0;
label_0:
        return eax;
label_1:
        if (*(obj.beware_hard_dir_link) != 0) {
            goto label_2;
        }
        r8d = *(obj.logical);
        rcx = rdx;
        edx = esi;
        rsi = rdi;
        edi = 0xffffff9c;
        r8d <<= 0xa;
        eax = linkat ();
    } while (eax >= 0);
label_3:
    rax = errno_location ();
    eax = *(rax);
    return rax;
label_2:
    eax = 0xffffffff;
    goto label_0;
}