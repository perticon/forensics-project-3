void** atomic_link(void** rdi, void** esi, void** rdx, ...) {
    int1_t zf4;
    int1_t zf5;
    void** eax6;
    void** rdx7;
    void** eax8;
    void** rax9;
    int1_t zf10;
    void** eax11;

    zf4 = symbolic_link == 0;
    if (zf4) {
        zf5 = beware_hard_dir_link == 0;
        if (!zf5) {
            eax6 = reinterpret_cast<void**>(0xffffffff);
        } else {
            rdx7 = esi;
            *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
            eax8 = fun_2860(0xffffff9c, rdi, rdx7, rdx);
            if (reinterpret_cast<signed char>(eax8) >= reinterpret_cast<signed char>(0)) {
                addr_321f_5:
                eax6 = reinterpret_cast<void**>(0);
            } else {
                addr_325b_6:
                rax9 = fun_2520();
                return *reinterpret_cast<void***>(rax9);
            }
        }
        return eax6;
    } else {
        zf10 = relative == 0;
        if (zf10) {
            eax11 = fun_2960();
            if (reinterpret_cast<signed char>(eax11) < reinterpret_cast<signed char>(0)) 
                goto addr_325b_6; else 
                goto addr_321f_5;
        }
    }
}