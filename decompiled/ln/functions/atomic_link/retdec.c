int64_t atomic_link(int64_t a1, int64_t a2, int64_t a3) {
    // 0x3200
    if (*(char *)&symbolic_link == 0) {
        // 0x3230
        if (*(char *)&beware_hard_dir_link != 0) {
            // 0x3221
            return 0xffffffff;
        }
        // 0x3239
        if ((int32_t)function_2860() >= 0) {
            // 0x3221
            return 0;
        }
    } else {
        // 0x320d
        if (*(char *)&relative != 0) {
            // 0x3221
            return 0xffffffff;
        }
        // 0x3216
        if ((int32_t)function_2960() >= 0) {
            // 0x3221
            return 0;
        }
    }
    // 0x325b
    return (int64_t)*(int32_t *)function_2520();
}