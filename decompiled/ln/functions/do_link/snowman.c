uint32_t do_link(void** rdi, void** esi, void** rdx, void** rcx, void** r8, int64_t r9) {
    void** rsi2;
    void** r10_7;
    void** r13_8;
    void** r12_9;
    void** rbp10;
    void*** rsp11;
    uint32_t ecx12;
    int64_t rax13;
    uint32_t ecx14;
    void** rcx15;
    void** v16;
    void** eax17;
    uint32_t r9d18;
    int32_t r15d19;
    int32_t eax20;
    uint32_t r9d21;
    uint32_t v22;
    void** rbx23;
    int1_t zf24;
    int64_t v25;
    void** rax26;
    void** v27;
    void** rax28;
    void** rsi29;
    void** rax30;
    void* rsp31;
    void** v32;
    void** rax33;
    void** rax34;
    signed char al35;
    void* rsp36;
    uint32_t r9d37;
    void** r15_38;
    void** rdx39;
    int1_t zf40;
    void** rcx41;
    void** r13_42;
    void** r13_43;
    void** rax44;
    void** rax45;
    void** rax46;
    void** rax47;
    uint1_t cf48;
    void** rax49;
    void** r9_50;
    void** v51;
    int64_t v52;
    int64_t v53;
    int64_t v54;
    int64_t v55;
    int64_t v56;
    void** rdx57;
    int32_t eax58;
    int1_t zf59;
    int1_t zf60;
    int1_t zf61;
    int64_t rdi62;
    void** rdx63;
    int32_t eax64;
    void*** rsp65;
    void** rax66;
    void** r9d67;
    void** rsi68;
    uint32_t v69;
    void** rdi70;
    signed char al71;
    void** edi72;
    uint32_t eax73;
    uint32_t eax74;
    int64_t rax75;
    void** rsi76;
    int32_t eax77;
    int64_t v78;
    int64_t v79;
    int64_t v80;
    int64_t v81;
    int64_t v82;
    int64_t rdx83;
    signed char al84;
    uint32_t r9d85;
    void** rax86;
    void** r15_87;
    void** rax88;
    void** rdi89;
    signed char al90;
    uint32_t r9d91;
    void** edx92;
    int64_t rdi93;
    void** rax94;
    int64_t rdx95;
    int64_t rdi96;
    int32_t eax97;
    int1_t zf98;
    void** eax99;
    void** r9d100;
    int64_t rdx101;
    uint32_t r8d102;
    void** eax103;
    void** rax104;
    void** v105;
    void** rax106;
    int1_t zf107;
    void** v108;
    void** r9d109;
    int64_t rdx110;
    int64_t rdi111;
    int32_t eax112;
    void** rax113;
    void** rdi114;

    rsi2 = esi;
    r10_7 = r8;
    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
    r13_8 = rdx;
    r12_9 = rdi;
    rbp10 = rsi2;
    rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x158);
    ecx12 = logical;
    rax13 = g28;
    ecx14 = ecx12 ^ 1;
    rcx15 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ecx14)) << 8);
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    if (reinterpret_cast<signed char>(r8) < reinterpret_cast<signed char>(0)) {
        v16 = reinterpret_cast<void**>(0x3448);
        eax17 = atomic_link(rdi, rsi2, rdx);
        rsp11 = rsp11 - 8 + 8;
        rcx15 = rcx15;
        *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
        r10_7 = eax17;
        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
    }
    r9d18 = symbolic_link;
    if (r10_7) {
        r15d19 = 1;
        if (*reinterpret_cast<void***>(&r9d18)) 
            goto addr_335f_5;
        rdx = reinterpret_cast<void**>(rsp11 + 32);
        rsi2 = r12_9;
        v16 = reinterpret_cast<void**>(0x35d6);
        eax20 = fun_2980(0xffffff9c, rsi2, rdx, rcx15);
        rsp11 = rsp11 - 8 + 8;
        r10_7 = r10_7;
        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
        r15d19 = eax20;
        if (eax20) 
            goto addr_3920_7;
        r9d21 = hard_dir_link;
        if (*reinterpret_cast<void***>(&r9d21) || (v22 & 0xf000) != 0x4000) {
            addr_335f_5:
            *reinterpret_cast<int32_t*>(&rbx23) = 0;
            *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
            zf24 = relative == 0;
            if (!zf24) {
                *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4) = r10_7;
                rax26 = dir_name(rcx, rsi2, rdx, rcx15);
                v27 = rax26;
                rax28 = canonicalize_filename_mode(rax26, 2, rdx, rcx15);
                *reinterpret_cast<int32_t*>(&rsi29) = 2;
                *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
                rax30 = canonicalize_filename_mode(r12_9, 2, rdx, rcx15);
                rsp31 = reinterpret_cast<void*>(rsp11 - 8 + 8 - 8 + 8 - 8 + 8);
                v32 = rax30;
                if (!rax28 || !rax30) {
                    addr_39c8_10:
                    fun_2500(v27, rsi29, v27, rsi29);
                    fun_2500(rax28, rsi29, rax28, rsi29);
                    fun_2500(v32, rsi29, v32, rsi29);
                    v16 = reinterpret_cast<void**>(0x39f6);
                    rax33 = xstrdup(r12_9, rsi29, rdx, rcx15);
                    rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    r10_7 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4);
                    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                    r12_9 = rax33;
                    rbx23 = rax33;
                } else {
                    rax34 = xmalloc("34", 2, rdx, rcx15, r8);
                    rsi29 = rax28;
                    rcx15 = reinterpret_cast<void**>("34");
                    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                    rdx = rax34;
                    rbx23 = rax34;
                    al35 = relpath(v32, rsi29, rdx, "34");
                    rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
                    if (!al35) {
                        fun_2500(rbx23, rsi29, rbx23, rsi29);
                        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
                        goto addr_39c8_10;
                    } else {
                        *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4) = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4);
                        r12_9 = rbx23;
                        fun_2500(v27, rsi29, v27, rsi29);
                        fun_2500(rax28, rsi29, rax28, rsi29);
                        v16 = reinterpret_cast<void**>(0x389f);
                        fun_2500(v32, rsi29, v32, rsi29);
                        rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp36) - 8 + 8 - 8 + 8 - 8 + 8);
                        r10_7 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4);
                        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                    }
                }
            }
        } else {
            quotearg_n_style_colon();
            fun_2620();
            fun_28b0();
            r9d37 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d21));
            goto addr_3321_15;
        }
    } else {
        r15_38 = dest_set;
        *reinterpret_cast<int32_t*>(&rbx23) = 0;
        *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
        if (!r15_38) {
            addr_3658_17:
            if (!*reinterpret_cast<void***>(&r9d18)) {
                rdx39 = reinterpret_cast<void**>(rsp11 + 32);
                goto addr_38f3_19;
            } else {
                addr_3661_20:
                zf40 = verbose == 0;
                r9d18 = 1;
                if (!zf40) {
                    if (!r15_38) {
                        addr_3750_22:
                        rcx41 = reinterpret_cast<void**>(0xd6d8);
                        r13_42 = reinterpret_cast<void**>(0xd6d8);
                    } else {
                        r13_43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(rcx));
                        rbp10 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
                        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r13_43) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r13_43 == 0))) {
                            rax44 = fun_2640(r15_38, r15_38);
                            rax45 = xmalloc(reinterpret_cast<unsigned char>(r13_43) + reinterpret_cast<unsigned char>(rax44) + 1, rsi2, r15_38, rcx15, r8);
                            rbp10 = rax45;
                            fun_27d0(rax45, rcx, r13_43, rax45, rcx, r13_43);
                            fun_2550(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r13_43), r15_38, r13_43, rcx15, r8);
                        }
                        rax46 = quotearg_n_style();
                        r13_42 = rax46;
                        fun_2500(rbp10, 4, rbp10, 4);
                        rcx41 = reinterpret_cast<void**>(" ~ ");
                    }
                    rax47 = quotearg_n_style();
                    cf48 = reinterpret_cast<uint1_t>(symbolic_link < 1);
                    rax49 = quotearg_n_style();
                    *reinterpret_cast<uint32_t*>(&r9_50) = (reinterpret_cast<unsigned char>(rbp10) - (reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp10) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp10) + cf48))) & 16) + 45;
                    *reinterpret_cast<int32_t*>(&r9_50 + 4) = 0;
                    rsi2 = reinterpret_cast<void**>("%s%s%s %c> %s\n");
                    fun_2880(1, "%s%s%s %c> %s\n", r13_42, rcx41, rax49, r9_50, rax47, 0x3714, rcx41, v32, v27, v25, v51, v52, v53, v54, v55, v56);
                    r9d18 = 1;
                    goto addr_3308_27;
                }
            }
        } else {
            if (!*reinterpret_cast<void***>(&r9d18)) {
                rdx57 = reinterpret_cast<void**>(rsp11 + 32);
                eax58 = fun_2980(0xffffff9c, r12_9, rdx57, rcx15);
                if (eax58) {
                    addr_3920_7:
                    quotearg_style(4, r12_9, 4, r12_9);
                    fun_2620();
                    fun_2520();
                    fun_28b0();
                    r9d37 = 0;
                    goto addr_3321_15;
                } else {
                    rdx39 = rdx57;
                    *reinterpret_cast<int32_t*>(&rbx23) = 0;
                    *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r15_38) = 0;
                    *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
                    goto addr_38f3_19;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rbx23) = 0;
                *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r15_38) = 0;
                *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
                zf59 = verbose == 0;
                if (!zf59) 
                    goto addr_3750_22;
                goto addr_3308_27;
            }
        }
    }
    zf60 = remove_existing_files == 0;
    if (!zf60) 
        goto addr_337b_34;
    zf61 = interactive == 0;
    if (!zf61) 
        goto addr_337b_34;
    r8 = backup_type;
    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
    if (!r8) 
        goto addr_3475_37;
    addr_337b_34:
    *reinterpret_cast<void***>(&rdi62) = rbp10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi62) + 4) = 0;
    rdx63 = reinterpret_cast<void**>(rsp11 + 0xb0);
    v32 = rdx63;
    eax64 = fun_2980(rdi62, r13_8, rdx63, 0x100);
    rsp65 = rsp11 - 8 + 8;
    if (eax64) {
        v16 = reinterpret_cast<void**>(0x3769);
        rax66 = fun_2520();
        rsp11 = rsp65 - 8 + 8;
        r10_7 = r10_7;
        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
        if (*reinterpret_cast<void***>(rax66) == 2) {
            addr_3475_37:
            r9d67 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&r15_38) = 0;
            *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
            goto addr_347b_39;
        } else {
            quotearg_style(4, rcx);
            fun_2620();
            rsi68 = *reinterpret_cast<void***>(rax66);
            *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
            fun_28b0();
        }
    } else {
        if ((v69 & 0xf000) == 0x4000) {
            quotearg_n_style_colon();
            fun_2620();
            rsi68 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
            fun_28b0();
        } else {
            rdi70 = dest_set;
            v16 = reinterpret_cast<void**>(0x33d6);
            al71 = seen_file(rdi70, rcx, v32, 0x100);
            rsp11 = rsp65 - 8 + 8;
            r10_7 = r10_7;
            *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
            if (al71) {
                quotearg_n_style();
                quotearg_n_style();
                goto addr_399b_45;
            } else {
                edi72 = backup_type;
                eax73 = remove_existing_files;
                if (edi72) {
                    eax74 = symbolic_link;
                    eax73 = eax74 ^ 1;
                }
                if (!*reinterpret_cast<signed char*>(&eax73)) 
                    goto addr_341d_49;
                if (r15d19) 
                    goto addr_3b73_51; else 
                    goto addr_340a_52;
            }
        }
    }
    addr_37ae_53:
    fun_2500(rbx23, rsi68, rbx23, rsi68);
    r9d37 = 0;
    addr_3321_15:
    rax75 = rax13 - g28;
    if (rax75) {
        fun_2660();
    } else {
        return r9d37;
    }
    addr_399b_45:
    fun_2620();
    rsi68 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
    fun_28b0();
    goto addr_37ae_53;
    addr_3b73_51:
    rsi76 = reinterpret_cast<void**>(rsp11 + 32);
    v16 = reinterpret_cast<void**>(0x3b87);
    eax77 = fun_27c0(r12_9, rsi76, r12_9, rsi76);
    rsp11 = rsp11 - 8 + 8;
    r10_7 = r10_7;
    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
    if (eax77) 
        goto addr_341d_49;
    addr_340a_52:
    if (v78 != v79 || (v80 != v81 || v82 != 1 && (*reinterpret_cast<void***>(&rdx83) = rbp10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx83) + 4) = 0, v16 = reinterpret_cast<void**>(0x3c49), al84 = same_nameat(0xffffff9c, r12_9, rdx83, r13_8), rsp11 = rsp11 - 8 + 8, r10_7 = r10_7, *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0, al84 == 0))) {
        addr_341d_49:
        if (reinterpret_cast<signed char>(r10_7) < reinterpret_cast<signed char>(0)) 
            goto addr_3a80_58;
        if (r10_7 != 17) 
            goto addr_3430_60;
    } else {
        quotearg_n_style();
        quotearg_n_style();
        goto addr_399b_45;
    }
    addr_3a80_58:
    r9d85 = interactive;
    if (!*reinterpret_cast<unsigned char*>(&r9d85) || (v27 = r10_7, *reinterpret_cast<unsigned char*>(&v32) = *reinterpret_cast<unsigned char*>(&r9d85), rax86 = quotearg_style(4, rcx, 4, rcx), r15_87 = program_name, rax88 = fun_2620(), r8 = rax86, rdi89 = stderr, fun_2930(rdi89, 1, rax88, r15_87, r8), v16 = reinterpret_cast<void**>(0x3bef), al90 = yesno(rdi89, 1, rax88, r15_87, r8), rsp11 = rsp11 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8, r9d91 = *reinterpret_cast<unsigned char*>(&v32), r10_7 = v27, *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0, !!al90)) {
        edx92 = backup_type;
        if (!edx92) {
            addr_3430_60:
            r9d67 = reinterpret_cast<void**>(1);
            *reinterpret_cast<int32_t*>(&r15_38) = 0;
            *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
            goto addr_347b_39;
        } else {
            *reinterpret_cast<void***>(&rdi93) = rbp10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi93) + 4) = 0;
            rax94 = find_backup_file_name(rdi93, r13_8);
            *reinterpret_cast<void***>(&rdx95) = rbp10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx95) + 4) = 0;
            *reinterpret_cast<void***>(&rdi96) = rbp10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
            r15_38 = rax94;
            v16 = reinterpret_cast<void**>(0x3abf);
            eax97 = fun_2900(rdi96, r13_8, rdx95, rax94, r8);
            rsp11 = rsp11 - 8 + 8 - 8 + 8;
            r10_7 = r10_7;
            *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
            r9d67 = reinterpret_cast<void**>(1);
            if (!eax97) {
                addr_347b_39:
                zf98 = symbolic_link == 0;
                if (!zf98) {
                    rcx15 = r9d67;
                    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                    r8 = r10_7;
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    rsi2 = rbp10;
                    *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
                    eax99 = force_symlinkat(r12_9, rsi2, r13_8, rcx15, r8);
                    rsp11 = rsp11 - 8 + 8;
                    r9d100 = eax99;
                } else {
                    *reinterpret_cast<void***>(&rdx101) = rbp10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx101) + 4) = 0;
                    r8d102 = logical;
                    r8 = reinterpret_cast<void**>(r8d102 << 10);
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    eax103 = force_linkat(0xffffff9c, r12_9, rdx101, r13_8);
                    rcx15 = r10_7;
                    rsi2 = v16;
                    rsp11 = rsp11 - 8 - 8 - 8 + 8 + 8 + 8;
                    r9d100 = eax103;
                }
            } else {
                v32 = r10_7;
                rax104 = fun_2520();
                v105 = *reinterpret_cast<void***>(rax104);
                v16 = reinterpret_cast<void**>(0x3ae8);
                fun_2500(r15_38, r13_8, r15_38, r13_8);
                rsp11 = rsp11 - 8 + 8 - 8 + 8;
                r10_7 = v32;
                *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                if (v105 == 2) 
                    goto addr_3475_37;
                quotearg_style(4, rcx, 4, rcx);
                fun_2620();
                rsi68 = v105;
                *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
                fun_28b0();
                goto addr_37ae_53;
            }
        }
    } else {
        fun_2500(rbx23, 1, rbx23, 1);
        r9d37 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d91));
        goto addr_3321_15;
    }
    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r9d100) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r9d100 == 0)) {
        r9d18 = symbolic_link;
        goto addr_3658_17;
    } else {
        rax106 = quotearg_n_style();
        quotearg_n_style();
        zf107 = symbolic_link == 0;
        if (zf107) {
            if (r9d100 == 31) {
                v108 = r9d100;
            } else {
                if (r9d100 == 0x7a || (r9d100 == 17 || (reinterpret_cast<unsigned char>(r9d100) & 0xfffffffd) == 28)) {
                    v108 = r9d100;
                } else {
                    fun_2620();
                    r9d109 = r9d100;
                    goto addr_3534_77;
                }
            }
        } else {
            if (r9d100 == 36 || !*reinterpret_cast<void***>(r12_9)) {
                v108 = r9d100;
            } else {
                v108 = r9d100;
            }
        }
    }
    fun_2620();
    r9d109 = v108;
    addr_3534_77:
    rsi2 = r9d109;
    *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
    fun_28b0();
    r9d18 = 0;
    if (r15_38 && (*reinterpret_cast<void***>(&rdx110) = rbp10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx110) + 4) = 0, rsi2 = r15_38, *reinterpret_cast<void***>(&rdi111) = rbp10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi111) + 4) = 0, eax112 = fun_2900(rdi111, rsi2, rdx110, r13_8, rax106), r9d18 = 0, !!eax112)) {
        quotearg_style(4, rcx, 4, rcx);
        fun_2620();
        rax113 = fun_2520();
        rsi2 = *reinterpret_cast<void***>(rax113);
        *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
        fun_28b0();
        r9d18 = 0;
    }
    addr_3308_27:
    fun_2500(r15_38, rsi2, r15_38, rsi2);
    fun_2500(rbx23, rsi2, rbx23, rsi2);
    r9d37 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d18));
    goto addr_3321_15;
    addr_38f3_19:
    rdi114 = dest_set;
    rsi2 = rcx;
    record_file(rdi114, rsi2, rdx39, rcx15, r8);
    goto addr_3661_20;
}