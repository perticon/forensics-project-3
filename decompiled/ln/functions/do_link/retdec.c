bool do_link(char * source, int32_t destdir_fd, char * dest_base, char * dest, uint32_t link_errno) {
    int64_t v1 = (int64_t)dest_base;
    int64_t v2 = (int64_t)source;
    __readfsqword(40);
    int64_t v3 = link_errno; // 0x32c5
    bool v4; // bp-392, 0x3280
    if (link_errno < 0) {
        // 0x3440
        v4 = false;
        v3 = atomic_link(v2, (int64_t)destdir_fd, v1) & 0xffffffff;
    }
    char v5 = *(char *)&symbolic_link; // 0x32cb
    int64_t v6; // 0x3280
    int64_t v7; // 0x3280
    int64_t v8; // 0x3280
    int64_t v9; // 0x3280
    char v10; // 0x3280
    int64_t v11; // 0x3280
    char v12; // 0x3280
    int64_t v13; // 0x3280
    int64_t v14; // 0x3280
    int32_t v15; // 0x3280
    if (v3 != 0) {
        // 0x3350
        v6 = v3;
        v7 = 1;
        if (v5 == 0) {
            // 0x35c0
            v4 = v3 % 2 != 0;
            int64_t v16 = function_2980(); // 0x35d1
            if ((int32_t)v16 != 0) {
                goto lab_0x3920;
            } else {
                int64_t v17 = v4 ? 0xffffffff : 0; // 0x35d6
                int64_t v18 = v16 & 0xffffffff; // 0x35dc
                unsigned char v19 = *(char *)&hard_dir_link; // 0x35e5
                v6 = v17;
                v7 = v18;
                if (v19 != 0) {
                    goto lab_0x335f;
                } else {
                    // 0x35f6
                    v6 = v17;
                    v7 = v18;
                    if ((v15 & (int32_t)&g47) != 0x4000) {
                        goto lab_0x335f;
                    } else {
                        // 0x360a
                        v4 = v19 % 2 != 0;
                        quotearg_n_style_colon();
                        function_2620();
                        function_28b0();
                        v11 = v4 ? 255 : 0;
                        goto lab_0x3321;
                    }
                }
            }
        } else {
            goto lab_0x335f;
        }
    } else {
        // 0x32d8
        v12 = v5;
        v8 = (int64_t)dest_set;
        if (dest_set == NULL) {
            goto lab_0x3658;
        } else {
            if (v5 == 0) {
                // 0x38d0
                v4 = false;
                if ((int32_t)function_2980() != 0) {
                    goto lab_0x3920;
                } else {
                    // 0x38ea
                    v14 = v4;
                    v9 = 0;
                    goto lab_0x38f3;
                }
            } else {
                // 0x32f3
                v10 = v5;
                v13 = &g45;
                if (*(char *)&verbose != 0) {
                    goto lab_0x36e3;
                } else {
                    goto lab_0x3308;
                }
            }
        }
    }
  lab_0x335f:;
    int64_t v20 = v6; // 0x3368
    int64_t v21 = v2; // 0x3368
    if (*(char *)&relative != 0) {
        char * v22 = canonicalize_filename_mode(dir_name(dest), 2); // 0x381a
        v4 = (int64_t)v22 % 2 != 0;
        char * v23 = canonicalize_filename_mode(source, 2); // 0x382e
        if (v22 == NULL || v23 == NULL) {
            goto lab_0x39c8;
        } else {
            int64_t v24 = xmalloc(); // 0x384f
            if (!relpath(v23, (char *)(int64_t)v4, (char *)v24, (int64_t)"34")) {
                // 0x39c0
                function_2500();
                goto lab_0x39c8;
            } else {
                // 0x387a
                function_2500();
                function_2500();
                function_2500();
                v20 = v6 % 2 != 0 ? 0xffffffff : 0;
                v21 = v24;
                goto lab_0x336e;
            }
        }
    } else {
        goto lab_0x336e;
    }
  lab_0x3658:;
    int64_t v25 = v8; // 0x365b
    int64_t v26; // bp-360, 0x3280
    if (v12 == 0) {
        // 0x3910
        v14 = &v26;
        v9 = v8;
        goto lab_0x38f3;
    } else {
        goto lab_0x3661;
    }
  lab_0x3920:
    // 0x3920
    quotearg_style();
    function_2620();
    function_2520();
    function_28b0();
    v11 = 0;
    goto lab_0x3321;
  lab_0x336e:;
    // 0x336e
    int64_t v27; // 0x3280
    int64_t v28; // 0x3280
    int64_t v29; // 0x3280
    if (*(char *)&remove_existing_files == 0) {
        // 0x3458
        if (*(char *)&interactive != 0) {
            goto lab_0x337b;
        } else {
            // 0x3465
            v29 = 0;
            v27 = v20;
            v28 = 0;
            if (backup_type != 0) {
                goto lab_0x337b;
            } else {
                goto lab_0x347b;
            }
        }
    } else {
        goto lab_0x337b;
    }
  lab_0x3661:
    // 0x3661
    v10 = 1;
    if (*(char *)&verbose == 0) {
        goto lab_0x3308;
    } else {
        // 0x3674
        v13 = &g45;
        if (v25 != 0) {
            if (v1 - (int64_t)dest >= 1) {
                // 0x368a
                function_2640();
                uint64_t v30 = xmalloc(); // 0x3697
                function_27d0();
                v4 = v30 % 2 != 0;
                function_2550();
            }
            // 0x36c2
            quotearg_n_style();
            function_2500();
            v13 = (int64_t)" ~ ";
        }
        goto lab_0x36e3;
    }
  lab_0x3321:
    // 0x3321
    if (*(int64_t *)((int64_t)&v4 + 328) != __readfsqword(40)) {
        // 0x3c8d
        return function_2660() % 2 != 0;
    }
    // 0x3338
    return v11 % 2 != 0;
  lab_0x39c8:
    // 0x39c8
    function_2500();
    function_2500();
    function_2500();
    v4 = v6 % 2 != 0;
    v20 = v4 ? 0xffffffff : 0;
    v21 = (int64_t)xstrdup(source);
    goto lab_0x336e;
  lab_0x337b:
    // 0x337b
    v4 = v20 % 2 != 0;
    uint64_t v31 = v4 ? 0xffffffff : 0; // 0x339b
    int64_t v32; // 0x3280
    int64_t v33; // bp-216, 0x3280
    int64_t v34; // 0x3b87
    if ((int32_t)function_2980() != 0) {
        // 0x3760
        v4 = v31 % 2 != 0;
        int64_t v35 = function_2520(); // 0x3764
        v29 = 0;
        v27 = v4 ? 0xffffffff : 0;
        v28 = 0;
        if (*(int32_t *)v35 == 2) {
            goto lab_0x347b;
        } else {
            // 0x3779
            quotearg_style();
            function_2620();
            function_28b0();
            // 0x37ae
            function_2500();
            v11 = 0;
            goto lab_0x3321;
        }
    } else {
        if ((v15 & (int32_t)&g47) == 0x4000) {
            // 0x3b38
            quotearg_n_style_colon();
            function_2620();
            function_28b0();
            // 0x37ae
            function_2500();
            v11 = 0;
            goto lab_0x3321;
        } else {
            // 0x33c3
            v4 = v31 % 2 != 0;
            if (seen_file(dest_set, dest, (struct stat *)&v33)) {
                // 0x3968
                quotearg_n_style();
                quotearg_n_style();
                goto lab_0x399b;
            } else {
                int64_t v36 = v4 ? 0xffffffff : 0; // 0x33d6
                char v37 = *(char *)&symbolic_link;
                v32 = v36;
                if ((backup_type == 0 ? *(char *)&remove_existing_files : v37 ^ 1) == 0) {
                    goto lab_0x341d;
                } else {
                    // 0x3401
                    v34 = v36;
                    if (v7 != 0) {
                        // 0x3b73
                        v4 = v36 % 2 != 0;
                        v34 = v4 ? 0xffffffff : 0;
                        v32 = v34;
                        if ((int32_t)function_27c0() != 0) {
                            goto lab_0x341d;
                        } else {
                            goto lab_0x340a;
                        }
                    } else {
                        goto lab_0x340a;
                    }
                }
            }
        }
    }
  lab_0x38f3:
    // 0x38f3
    record_file(dest_set, dest, (struct stat *)v14);
    v25 = v9;
    goto lab_0x3661;
  lab_0x3308:
    // 0x3308
    *(char *)&v4 = v10;
    function_2500();
    function_2500();
    v11 = (int64_t)*(char *)&v4;
    goto lab_0x3321;
  lab_0x36e3:
    // 0x36e3
    v4 = v13 % 2 != 0;
    quotearg_n_style();
    quotearg_n_style();
    function_2880();
    v10 = 1;
    goto lab_0x3308;
  lab_0x347b:;
    // 0x347b
    int32_t v38; // 0x3280
    if (*(char *)&symbolic_link != 0) {
        // 0x38b0
        v38 = force_symlinkat((char *)v21, destdir_fd, dest_base, v29 != 0, (int32_t)v27);
    } else {
        unsigned char v39 = *(char *)&logical; // 0x3496
        v38 = force_linkat(-100, (char *)v21, destdir_fd, dest_base, 1024 * (int32_t)v39, v29 != 0, (int32_t)(v27 % 2 != 0));
    }
    // 0x34b1
    if (v38 < 1) {
        // 0x3650
        v12 = *(char *)&symbolic_link;
        v8 = v28;
        goto lab_0x3658;
    } else {
        // 0x34ba
        v4 = quotearg_n_style() % 2 != 0;
        quotearg_n_style();
        if (*(char *)&symbolic_link == 0) {
            switch (v38) {
                case 31: {
                    // 0x3520
                    function_2620();
                    goto lab_0x3534;
                }
                case 122: {
                    // 0x3520
                    function_2620();
                    goto lab_0x3534;
                }
                case 17: {
                    // 0x3520
                    function_2620();
                    goto lab_0x3534;
                }
                default: {
                    if ((v38 - 28 & -3) != 0) {
                        // 0x3a48
                        function_2620();
                        goto lab_0x3534;
                    } else {
                        // 0x3520
                        function_2620();
                        goto lab_0x3534;
                    }
                }
            }
        } else {
            // 0x3520
            function_2620();
            goto lab_0x3534;
        }
    }
  lab_0x399b:
    // 0x399b
    function_2620();
    function_28b0();
    // 0x37ae
    function_2500();
    v11 = 0;
    goto lab_0x3321;
  lab_0x341d:;
    int32_t v40 = v32; // 0x341d
    v29 = 1;
    v27 = v32;
    v28 = 0;
    int64_t v41; // 0x3280
    if (v40 < 0 || v40 == 17) {
        unsigned char v42 = *(char *)&interactive; // 0x3a80
        v41 = v32;
        if (v42 != 0) {
            // 0x3b98
            v4 = quotearg_style() % 2 != 0;
            function_2620();
            function_2930();
            v41 = v32 & 0xffffffff;
            if (yesno()) {
                goto lab_0x3a91;
            } else {
                // 0x3c02
                v4 = v42 % 2 != 0;
                function_2500();
                v11 = v4 ? 255 : 0;
                goto lab_0x3321;
            }
        } else {
            goto lab_0x3a91;
        }
    } else {
        goto lab_0x347b;
    }
  lab_0x3534:
    // 0x3534
    function_28b0();
    v10 = 0;
    if (v28 != 0) {
        // 0x3550
        v4 = false;
        v10 = v4;
        if ((int32_t)function_2900() != 0) {
            // 0x3570
            quotearg_style();
            function_2620();
            function_2520();
            function_28b0();
            v10 = v4;
        }
    }
    goto lab_0x3308;
  lab_0x340a:
    // 0x340a
    v32 = v34;
    // 0x3c18
    v32 = v34;
    if (v26 != v33) {
        goto lab_0x341d;
    } else {
        int64_t v43; // 0x3280
        if (v43 == 1) {
            // 0x3c55
            quotearg_n_style();
            quotearg_n_style();
            goto lab_0x399b;
        } else {
            // 0x3c33
            v4 = v34 % 2 != 0;
            v32 = v4 ? 0xffffffff : 0;
            if (!same_nameat(-100, (char *)v21, destdir_fd, dest_base)) {
                goto lab_0x341d;
            } else {
                // 0x3c55
                quotearg_n_style();
                quotearg_n_style();
                goto lab_0x399b;
            }
        }
    }
  lab_0x3a91:
    // 0x3a91
    v29 = 1;
    v27 = v41;
    v28 = 0;
    if (backup_type == 0) {
        goto lab_0x347b;
    } else {
        // 0x3a9f
        v4 = v41 % 2 != 0;
        v29 = 1;
        v27 = v4 ? 0xffffffff : 0;
        v28 = (int64_t)find_backup_file_name(destdir_fd, dest_base, backup_type);
        if ((int32_t)function_2900() == 0) {
            goto lab_0x347b;
        } else {
            // 0x3ad1
            v4 = *(int32_t *)function_2520() % 2 != 0;
            function_2500();
            quotearg_style();
            function_2620();
            function_28b0();
            // 0x37ae
            function_2500();
            v11 = 0;
            goto lab_0x3321;
        }
    }
}