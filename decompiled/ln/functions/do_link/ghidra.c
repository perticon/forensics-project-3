byte do_link(char *param_1,int param_2,char *param_3,void *param_4,int param_5)

{
  char cVar1;
  byte bVar2;
  uint uVar3;
  int iVar4;
  size_t sVar5;
  char *pcVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  void *__ptr;
  void *__ptr_00;
  void *__ptr_01;
  char *__ptr_02;
  int *piVar9;
  char *pcVar10;
  int iVar11;
  char *pcVar12;
  size_t __n;
  long in_FS_OFFSET;
  bool bVar13;
  undefined8 uVar14;
  byte local_188;
  stat local_168;
  stat local_d8;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar11 = (logical ^ 1) << 8;
  if (param_5 < 0) {
    param_5 = atomic_link();
  }
  if (param_5 == 0) {
    __ptr_02 = (char *)0x0;
    pcVar10 = dest_set;
    if (dest_set == (char *)0x0) {
LAB_00103658:
      if (symbolic_link == 0) goto LAB_001038f3;
LAB_00103661:
      local_188 = 1;
      if (verbose == '\0') goto LAB_00103308;
      if (pcVar10 == (char *)0x0) goto LAB_00103750;
      __n = (long)param_3 - (long)param_4;
      pcVar6 = pcVar10;
      pcVar12 = (char *)0x0;
      if (0 < (long)__n) {
        sVar5 = strlen(pcVar10);
        pcVar6 = (char *)xmalloc(__n + 1 + sVar5);
        memcpy(pcVar6,param_4,__n);
        strcpy(pcVar6 + __n,pcVar10);
        pcVar12 = pcVar6;
      }
      pcVar6 = (char *)quotearg_n_style(2,4,pcVar6);
      free(pcVar12);
      pcVar12 = " ~ ";
    }
    else {
      if (symbolic_link == 0) {
        iVar11 = fstatat(-100,param_1,&local_168,iVar11);
        if (iVar11 != 0) goto LAB_00103920;
        __ptr_02 = (char *)0x0;
        pcVar10 = (char *)0x0;
LAB_001038f3:
        record_file(dest_set,param_4,&local_168);
        goto LAB_00103661;
      }
      __ptr_02 = (char *)0x0;
      pcVar10 = (char *)0x0;
      local_188 = symbolic_link;
      if (verbose == '\0') goto LAB_00103308;
LAB_00103750:
      pcVar12 = "";
      pcVar6 = pcVar12;
    }
    uVar7 = quotearg_n_style(1,4,param_1);
    bVar13 = symbolic_link == 0;
    uVar14 = 0x103714;
    uVar8 = quotearg_n_style(0,4,param_4);
    __printf_chk(1,"%s%s%s %c> %s\n",pcVar6,pcVar12,uVar8,(-bVar13 & 0x10U) + 0x2d,uVar7,uVar14);
    local_188 = 1;
  }
  else {
    iVar4 = 1;
    if (symbolic_link == 0) {
      iVar4 = fstatat(-100,param_1,&local_168,iVar11);
      if (iVar4 != 0) {
LAB_00103920:
        uVar7 = quotearg_style(4,param_1);
        uVar8 = dcgettext(0,"failed to access %s",5);
        piVar9 = __errno_location();
        error(0,*piVar9,uVar8,uVar7);
        local_188 = 0;
        goto LAB_00103321;
      }
      if ((hard_dir_link == '\0') && ((local_168.st_mode & 0xf000) == 0x4000)) {
        uVar7 = quotearg_n_style_colon(0,3,param_1);
        uVar8 = dcgettext(0,"%s: hard link not allowed for directory",5);
        error(0,0,uVar8,uVar7);
        local_188 = 0;
        goto LAB_00103321;
      }
    }
    __ptr_02 = (char *)0x0;
    if (relative != '\0') {
      __ptr = (void *)dir_name(param_4);
      __ptr_00 = (void *)canonicalize_filename_mode(__ptr,2);
      __ptr_01 = (void *)canonicalize_filename_mode(param_1,2);
      if ((__ptr_00 != (void *)0x0) && (__ptr_01 != (void *)0x0)) {
        __ptr_02 = (char *)xmalloc(0x1000);
        cVar1 = relpath(__ptr_01,__ptr_00,__ptr_02,0x1000);
        if (cVar1 != '\0') {
          free(__ptr);
          free(__ptr_00);
          free(__ptr_01);
          param_1 = __ptr_02;
          goto LAB_0010336e;
        }
        free(__ptr_02);
      }
      free(__ptr);
      free(__ptr_00);
      free(__ptr_01);
      __ptr_02 = (char *)xstrdup();
      param_1 = __ptr_02;
    }
LAB_0010336e:
    if (((remove_existing_files == 0) && (interactive == 0)) && (backup_type == 0)) {
LAB_00103475:
      uVar7 = 0;
      pcVar10 = (char *)0x0;
    }
    else {
      iVar11 = fstatat(param_2,param_3,&local_d8,0x100);
      if (iVar11 != 0) {
        piVar9 = __errno_location();
        if (*piVar9 != 2) {
          uVar7 = quotearg_style(4,param_4);
          uVar8 = dcgettext(0,"failed to access %s",5);
          error(0,*piVar9,uVar8,uVar7);
          goto LAB_001037ae;
        }
        goto LAB_00103475;
      }
      if ((local_d8.st_mode & 0xf000) == 0x4000) {
        uVar7 = quotearg_n_style_colon(0,3,param_4);
        uVar8 = dcgettext(0,"%s: cannot overwrite directory",5);
        error(0,0,uVar8,uVar7);
LAB_001037ae:
        free(__ptr_02);
        local_188 = 0;
        goto LAB_00103321;
      }
      cVar1 = seen_file(dest_set,param_4,&local_d8);
      if (cVar1 != '\0') {
        uVar7 = quotearg_n_style(1,4,param_1);
        uVar8 = quotearg_n_style(0,4,param_4);
        pcVar10 = "will not overwrite just-created %s with %s";
LAB_0010399b:
        uVar14 = dcgettext(0,pcVar10,5);
        error(0,0,uVar14,uVar8,uVar7);
        goto LAB_001037ae;
      }
      bVar2 = remove_existing_files;
      if (backup_type != 0) {
        bVar2 = symbolic_link ^ 1;
      }
      if ((((bVar2 != 0) && ((iVar4 == 0 || (iVar11 = stat(param_1,&local_168), iVar11 == 0)))) &&
          (local_168.st_ino == local_d8.st_ino)) &&
         ((local_168.st_dev == local_d8.st_dev &&
          ((local_168.st_nlink == 1 ||
           (cVar1 = same_nameat(0xffffff9c,param_1,param_2,param_3), cVar1 != '\0')))))) {
        uVar7 = quotearg_n_style(1,4,param_4);
        uVar8 = quotearg_n_style(0,4,param_1);
        pcVar10 = "%s and %s are the same file";
        goto LAB_0010399b;
      }
      local_188 = interactive;
      if ((param_5 < 0) || (param_5 == 0x11)) {
        if (interactive != 0) {
          uVar8 = quotearg_style(4,param_4);
          uVar7 = program_name;
          uVar14 = dcgettext(0,"%s: replace %s? ",5);
          __fprintf_chk(stderr,1,uVar14,uVar7,uVar8);
          cVar1 = yesno();
          if (cVar1 == '\0') {
            free(__ptr_02);
            goto LAB_00103321;
          }
        }
        if (backup_type == 0) goto LAB_00103430;
        pcVar10 = (char *)find_backup_file_name(param_2,param_3);
        iVar11 = renameat(param_2,param_3,param_2,pcVar10);
        uVar7 = 1;
        if (iVar11 != 0) {
          piVar9 = __errno_location();
          iVar11 = *piVar9;
          free(pcVar10);
          if (iVar11 == 2) goto LAB_00103475;
          uVar7 = quotearg_style(4,param_4);
          uVar8 = dcgettext(0,"cannot backup %s",5);
          error(0,iVar11,uVar8,uVar7);
          goto LAB_001037ae;
        }
      }
      else {
LAB_00103430:
        uVar7 = 1;
        pcVar10 = (char *)0x0;
      }
    }
    if (symbolic_link == 0) {
      uVar3 = force_linkat(0xffffff9c,param_1,param_2,param_3,(ulong)logical << 10,uVar7,param_5);
    }
    else {
      uVar3 = force_symlinkat(param_1,param_2,param_3,uVar7,param_5);
    }
    if ((int)uVar3 < 1) goto LAB_00103658;
    uVar7 = quotearg_n_style(1,4,param_1);
    uVar8 = quotearg_n_style(0,4,param_4);
    if (symbolic_link == 0) {
      if (uVar3 == 0x1f) {
        uVar3 = 0x1f;
        pcVar6 = "failed to create hard link to %.0s%s";
        goto LAB_00103520;
      }
      if (((uVar3 == 0x7a) || (uVar3 == 0x11)) || ((uVar3 & 0xfffffffd) == 0x1c)) {
        pcVar6 = "failed to create hard link %s";
        goto LAB_00103520;
      }
      uVar14 = dcgettext(0,"failed to create hard link %s => %s",5);
    }
    else {
      if ((uVar3 == 0x24) || (*param_1 == '\0')) {
        pcVar6 = "failed to create symbolic link %s -> %s";
      }
      else {
        pcVar6 = "failed to create symbolic link %s";
      }
LAB_00103520:
      uVar14 = dcgettext(0,pcVar6,5);
    }
    error(0,uVar3,uVar14,uVar8,uVar7);
    local_188 = 0;
    if (pcVar10 != (char *)0x0) {
      iVar11 = renameat(param_2,pcVar10,param_2,param_3);
      local_188 = 0;
      if (iVar11 != 0) {
        uVar7 = quotearg_style(4,param_4);
        uVar8 = dcgettext(0,"cannot un-backup %s",5);
        piVar9 = __errno_location();
        error(0,*piVar9,uVar8,uVar7);
      }
    }
  }
LAB_00103308:
  free(pcVar10);
  free(__ptr_02);
LAB_00103321:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_188;
}