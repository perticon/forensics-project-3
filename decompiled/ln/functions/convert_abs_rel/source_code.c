convert_abs_rel (char const *from, char const *target)
{
  /* Get dirname to generate paths relative to.  We don't resolve
     the full TARGET as the last component could be an existing symlink.  */
  char *targetdir = dir_name (target);

  char *realdest = canonicalize_filename_mode (targetdir, CAN_MISSING);
  char *realfrom = canonicalize_filename_mode (from, CAN_MISSING);

  char *relative_from = NULL;
  if (realdest && realfrom)
    {
      /* Write to a PATH_MAX buffer.  */
      relative_from = xmalloc (PATH_MAX);

      if (!relpath (realfrom, realdest, relative_from, PATH_MAX))
        {
          free (relative_from);
          relative_from = NULL;
        }
    }

  free (targetdir);
  free (realdest);
  free (realfrom);

  return relative_from ? relative_from : xstrdup (from);
}