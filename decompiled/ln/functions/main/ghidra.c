void main(int param_1,undefined8 *param_2)

{
  int iVar1;
  uint uVar2;
  undefined8 uVar3;
  long lVar4;
  void *__ptr;
  int *piVar5;
  undefined8 uVar6;
  undefined *puVar7;
  uint uVar8;
  long in_FS_OFFSET;
  int local_f0;
  int local_ec;
  undefined8 local_e0 [20];
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdin);
  hard_dir_link = '\0';
  verbose = 0;
  interactive = 0;
  remove_existing_files = '\0';
  symbolic_link = '\0';
  do {
    uVar3 = getopt_long(param_1,param_2,"bdfinrst:vFLPS:T",long_options);
    uVar8 = (uint)uVar3;
    if (uVar8 == 0xffffffff) {
      lVar4 = (long)optind;
      param_1 = param_1 - optind;
      if (0 < param_1) {
        if ((relative != '\0') && (symbolic_link == '\0')) {
          uVar3 = dcgettext(0,"cannot do --relative without --symbolic",5);
          lVar4 = error(1,0,uVar3);
        }
        if (hard_dir_link == '\0') {
          beware_hard_dir_link = 0;
        }
        param_2 = param_2 + lVar4;
        if (param_1 < 2) {
          puVar7 = &DAT_0010d08b;
          backup_type = 0;
          set_simple_backup_suffix();
          local_f0 = 1;
          local_ec = -100;
LAB_00102d71:
          uVar8 = 1;
          for (lVar4 = 0; (int)lVar4 < local_f0; lVar4 = lVar4 + 1) {
            uVar3 = last_component(param_2[lVar4]);
            __ptr = (void *)file_name_concat(puVar7,uVar3,local_e0);
            strip_trailing_slashes(local_e0[0]);
            uVar2 = do_link(param_2[lVar4],local_ec,local_e0[0],__ptr,0xffffffff);
            uVar8 = uVar8 & uVar2;
            free(__ptr);
          }
        }
        else {
          if ((((param_1 == 2) &&
               (uVar8 = atomic_link(*param_2,0xffffff9c,param_2[1]), -1 < (int)uVar8)) &&
              (uVar8 != 0x11)) && ((uVar8 & 0xfffffffd) != 0x14)) {
LAB_00102feb:
            backup_type = 0;
            set_simple_backup_suffix(0);
          }
          else {
            puVar7 = (undefined *)param_2[(long)param_1 + -1];
            local_ec = openat_safer(0xffffff9c,puVar7,
                                    (-(uint)(dereference_dest_dir_symlinks == '\0') & 0x20000) +
                                    0x210000);
            piVar5 = __errno_location();
            iVar1 = *piVar5;
            if (local_ec < 0) {
              if (param_1 == 2) goto LAB_00102feb;
              uVar3 = quotearg_style(4,puVar7);
              uVar6 = dcgettext(0,"target %s",5);
              error(1,iVar1,uVar6,uVar3);
              local_f0 = 1;
              puVar7 = &DAT_0010d08b;
              local_ec = -100;
              uVar3 = dcgettext(0,"backup type",5);
              backup_type = xget_version(uVar3,0);
            }
            else {
              local_f0 = param_1 + -1;
              backup_type = 0;
            }
            set_simple_backup_suffix();
            if (puVar7 != (undefined *)0x0) {
              if (((1 < local_f0) && (remove_existing_files != '\0')) &&
                 ((symbolic_link == '\0' &&
                  ((backup_type != 3 &&
                   (dest_set = hash_initialize(0x3d,0,triple_hash,triple_compare,triple_free),
                   dest_set == 0)))))) {
                    /* WARNING: Subroutine does not return */
                xalloc_die();
              }
              goto LAB_00102d71;
            }
          }
          uVar8 = do_link(*param_2,0xffffff9c,param_2[1],param_2[1],uVar8);
        }
                    /* WARNING: Subroutine does not return */
        exit((uVar8 ^ 1) & 0xff);
      }
LAB_00102b29:
      uVar3 = dcgettext(0,"missing file operand",5);
      error(0,0,uVar3);
    }
    else if ((int)uVar8 < 0x77) {
      if ((int)uVar8 < 0x46) {
        if (uVar8 == 0xffffff7d) {
          version_etc(stdout,&DAT_0010c09b,"GNU coreutils",Version,"Mike Parker","David MacKenzie",0
                      ,uVar3);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (uVar8 == 0xffffff7e) {
          usage(0);
          goto LAB_00102b29;
        }
      }
      else if (uVar8 - 0x46 < 0x31) {
                    /* WARNING: Could not recover jumptable at 0x00102b0b. Too many branches */
                    /* WARNING: Treating indirect jump as call */
        (*(code *)(&DAT_0010cfbc + *(int *)(&DAT_0010cfbc + (ulong)(uVar8 - 0x46) * 4)))();
        return;
      }
    }
    usage();
    hard_dir_link = '\x01';
  } while( true );
}