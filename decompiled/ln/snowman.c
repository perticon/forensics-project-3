
unsigned char symbolic_link = 0;

signed char beware_hard_dir_link = 0;

void** fun_2860(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** fun_2520();

signed char relative = 0;

void** fun_2960();

void** atomic_link(void** rdi, void** esi, void** rdx, ...) {
    int1_t zf4;
    int1_t zf5;
    void** eax6;
    void** rdx7;
    void** eax8;
    void** rax9;
    int1_t zf10;
    void** eax11;

    zf4 = symbolic_link == 0;
    if (zf4) {
        zf5 = beware_hard_dir_link == 0;
        if (!zf5) {
            eax6 = reinterpret_cast<void**>(0xffffffff);
        } else {
            rdx7 = esi;
            *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
            eax8 = fun_2860(0xffffff9c, rdi, rdx7, rdx);
            if (reinterpret_cast<signed char>(eax8) >= reinterpret_cast<signed char>(0)) {
                addr_321f_5:
                eax6 = reinterpret_cast<void**>(0);
            } else {
                addr_325b_6:
                rax9 = fun_2520();
                return *reinterpret_cast<void***>(rax9);
            }
        }
        return eax6;
    } else {
        zf10 = relative == 0;
        if (zf10) {
            eax11 = fun_2960();
            if (reinterpret_cast<signed char>(eax11) < reinterpret_cast<signed char>(0)) 
                goto addr_325b_6; else 
                goto addr_321f_5;
        }
    }
}

void** last_component(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_2800(void** rdi, void** rsi, void** rdx);

void** fun_2890(void** rdi, void** rsi, void** rdx);

void** simple_pattern = reinterpret_cast<void**>(67);

unsigned char gd088 = 0;

void** samedir_template(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rax7;
    void** rdi8;
    void** rax9;
    void** rax10;
    void** rdx11;
    uint32_t edx12;
    void** rax13;

    rbx5 = rsi;
    rax6 = last_component(rdi, rsi, rdx, rcx);
    rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(rdi));
    rdi8 = rax7 + 9;
    if (reinterpret_cast<unsigned char>(rdi8) <= reinterpret_cast<unsigned char>(0x100) || (rax9 = fun_2800(rdi8, rsi, rdx), rbx5 = rax9, !!rax9)) {
        rax10 = fun_2890(rbx5, rdi, rax7);
        rdx11 = simple_pattern;
        *reinterpret_cast<void***>(rax10) = rdx11;
        edx12 = gd088;
        *reinterpret_cast<void***>(rax10 + 8) = *reinterpret_cast<void***>(&edx12);
        rax13 = rbx5;
    } else {
        *reinterpret_cast<int32_t*>(&rax13) = 0;
        *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
    }
    return rax13;
}

void** stdout = reinterpret_cast<void**>(0);

void fun_2740(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, int64_t* a8, void** a9, void** a10, void** a11, int64_t a12, void** a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18);

void** fun_2640(void** rdi, ...);

void** fun_27d0(void** rdi, void** rsi, void** rdx, ...);

uint32_t buffer_or_output(void** rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12) {
    void** r14_13;
    void** rsi14;
    void** rbx15;
    int64_t* rbp16;
    void** r12_17;
    void** r13_18;
    void** r14_19;
    void** rax20;

    r14_13 = *rsi;
    if (!r14_13) {
        rsi14 = stdout;
        fun_2740(rdi, rsi14, rdx, rcx, r8, r9, rbx15, rbp16, r12_17, r13_18, r14_19, __return_address(), a7, a8, a9, a10, a11, a12);
        return 0;
    } else {
        rax20 = fun_2640(rdi);
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx)) > reinterpret_cast<unsigned char>(rax20)) {
            fun_27d0(r14_13, rdi, rax20 + 1);
            *rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*rsi) + reinterpret_cast<unsigned char>(rax20));
            *reinterpret_cast<void***>(rdx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx)) - reinterpret_cast<unsigned char>(rax20));
            return 0;
        } else {
            return 1;
        }
    }
}

int64_t g28;

signed char gl_scratch_buffer_grow_preserve(void** rdi, void** rsi);

void** fun_25b0(void** rdi, void** rsi);

signed char gl_scratch_buffer_grow(void** rdi, void** rsi);

int32_t fun_27c0(void** rdi, void** rsi, ...);

void** hash_initialize(int64_t rdi);

signed char seen_file(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void fun_28a0(void* rdi, void** rsi);

void record_file(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_25a0(int64_t rdi, void** rsi);

void* fun_2700(void** rdi, void** rsi);

uint16_t dir_suffix = 47;

void fun_2500(void** rdi, void** rsi, ...);

void** gl_scratch_buffer_dupfree(void** rdi);

void xalloc_die();

void hash_free();

int64_t fun_26d0(void** rdi, void** rsi);

void fun_2660();

void** fun_2750(void** rdi);

void** canonicalize_filename_mode_stk(void** rdi, uint32_t esi, void** rdx) {
    int64_t r14_4;
    void*** rsp5;
    int64_t rax6;
    int64_t v7;
    void** rbp8;
    void** rax9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** rbx13;
    void** r12_14;
    void** v15;
    void** rsi16;
    void** v17;
    void** rax18;
    void** v19;
    void** v20;
    void** rax21;
    int1_t zf22;
    void** r13_23;
    void** v24;
    void** r15_25;
    uint32_t eax26;
    void** v27;
    uint32_t v28;
    signed char v29;
    int32_t v30;
    void** v31;
    void** v32;
    void** rcx33;
    void** v34;
    void** v35;
    void** rcx36;
    void** r13_37;
    signed char al38;
    void** rax39;
    void** rcx40;
    void** v41;
    void** r13_42;
    void** v43;
    void** v44;
    void** rax45;
    signed char al46;
    void** r10_47;
    void** r9_48;
    void** rax49;
    void** rdx50;
    void** rsi51;
    uint32_t ecx52;
    uint32_t ecx53;
    void** rax54;
    void** r9_55;
    void** r8_56;
    void* v57;
    void** r10_58;
    void** rax59;
    void** rax60;
    void** r11_61;
    void** rdi62;
    int32_t eax63;
    void** r11_64;
    uint32_t eax65;
    void** r10_66;
    void** r9_67;
    void** rax68;
    signed char al69;
    void** v70;
    void** v71;
    void** v72;
    signed char al73;
    void** rax74;
    signed char v75;
    int32_t eax76;
    void** rax77;
    void* rax78;
    uint32_t edx79;
    uint32_t eax80;
    uint64_t rax81;
    void** rax82;
    int64_t rax83;
    void* rsp84;
    void** rax85;
    signed char al86;
    int64_t rdx87;
    void** rdi88;
    void** rax89;

    *reinterpret_cast<uint32_t*>(&r14_4) = esi & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_4) + 4) = 0;
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x928);
    rax6 = g28;
    v7 = rax6;
    if (static_cast<uint32_t>(r14_4 - 1) & *reinterpret_cast<uint32_t*>(&r14_4) || (rbp8 = rdi, rdi == 0)) {
        rax9 = fun_2520();
        *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax10) = 0;
        *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    } else {
        if (!*reinterpret_cast<void***>(rdi)) {
            rax11 = fun_2520();
            *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(2);
            *reinterpret_cast<int32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
        } else {
            rax12 = reinterpret_cast<void**>(rsp5 + 0x100);
            *reinterpret_cast<uint32_t*>(&rbx13) = esi;
            *reinterpret_cast<void***>(rdx + 8) = reinterpret_cast<void**>(0x400);
            r12_14 = rdx;
            v15 = rax12;
            *reinterpret_cast<int32_t*>(&rsi16) = 0x400;
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            v17 = rax12;
            rax18 = reinterpret_cast<void**>(rsp5 + 0x510);
            v19 = rax18;
            v20 = rax18;
            rax21 = rdx + 16;
            *reinterpret_cast<void***>(rdx) = rax21;
            zf22 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 47);
            r13_23 = rax21;
            v24 = rax21;
            if (zf22) {
                while (1) {
                    *reinterpret_cast<void***>(rdx + 16) = reinterpret_cast<void**>(47);
                    r15_25 = v24;
                    r13_23 = rdx + 17;
                    addr_520d_7:
                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                    if (!*reinterpret_cast<signed char*>(&eax26)) {
                        if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25 + 1) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47)) {
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        } else {
                            --r13_23;
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        }
                    } else {
                        v27 = reinterpret_cast<void**>(0);
                        v28 = *reinterpret_cast<uint32_t*>(&rbx13) & 4;
                        v29 = 0;
                        v30 = 0;
                        v31 = reinterpret_cast<void**>(rsp5 + 0x500);
                        while (1) {
                            if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                v32 = rbp8;
                                *reinterpret_cast<uint32_t*>(&rdx) = eax26;
                                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            } else {
                                do {
                                    *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                    ++rbp8;
                                } while (*reinterpret_cast<signed char*>(&rdx) == 47);
                                if (!*reinterpret_cast<signed char*>(&rdx)) 
                                    goto addr_52f0_17;
                                v32 = rbp8;
                            }
                            do {
                                rbx13 = rbp8;
                                eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                ++rbp8;
                                if (!*reinterpret_cast<signed char*>(&eax26)) 
                                    break;
                            } while (*reinterpret_cast<signed char*>(&eax26) != 47);
                            rcx33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v32));
                            v34 = rcx33;
                            if (!rcx33) 
                                goto addr_52f0_17;
                            if (rcx33 != 1) {
                                if (!reinterpret_cast<int1_t>(v34 == 2)) 
                                    goto addr_5189_24;
                                if (*reinterpret_cast<signed char*>(&rdx) != 46) 
                                    goto addr_5189_24;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v32 + 1) == 46)) 
                                    goto addr_5189_24;
                                rdx = r15_25 + 1;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(rdx)) 
                                    goto addr_52e8_28;
                                --r13_23;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25)) 
                                    goto addr_52e8_28;
                                do {
                                    if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47) 
                                        goto addr_52e8_28;
                                    --r13_23;
                                } while (r13_23 != r15_25);
                                goto addr_52e8_28;
                            }
                            if (*reinterpret_cast<signed char*>(&rdx) == 46) {
                                addr_52e8_28:
                                if (*reinterpret_cast<signed char*>(&eax26)) 
                                    continue; else 
                                    goto addr_52f0_17;
                            } else {
                                addr_5189_24:
                                if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) != 47) {
                                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(47);
                                    ++r13_23;
                                }
                            }
                            rsi16 = v34 + 2;
                            if (reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_14 + 8)) + reinterpret_cast<unsigned char>(r15_25)) - reinterpret_cast<unsigned char>(r13_23)) < reinterpret_cast<unsigned char>(rsi16)) {
                                v35 = rbx13;
                                rcx36 = r13_23;
                                rbx13 = rsi16;
                                r13_37 = r12_14;
                                r12_14 = rbp8;
                                do {
                                    rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx36) - reinterpret_cast<unsigned char>(r15_25));
                                    al38 = gl_scratch_buffer_grow_preserve(r13_37, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (!al38) 
                                        goto addr_51fa_38;
                                    r15_25 = *reinterpret_cast<void***>(r13_37);
                                    rcx36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_25) + reinterpret_cast<unsigned char>(rbp8));
                                } while (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_37 + 8)) - reinterpret_cast<unsigned char>(rbp8)) < reinterpret_cast<unsigned char>(rbx13));
                                rbx13 = v35;
                                rbp8 = r12_14;
                                r12_14 = r13_37;
                                r13_23 = rcx36;
                            }
                            rdx = v34;
                            rsi16 = v32;
                            rax39 = fun_2890(r13_23, rsi16, rdx);
                            rsp5 = rsp5 - 8 + 8;
                            *reinterpret_cast<uint32_t*>(&rcx40) = v28;
                            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                            *reinterpret_cast<void***>(rax39) = reinterpret_cast<void**>(0);
                            r13_23 = rax39;
                            if (!*reinterpret_cast<uint32_t*>(&rcx40)) {
                                v41 = rax39;
                                r13_42 = v31;
                                v43 = rbx13;
                                v44 = rbp8;
                                do {
                                    rbx13 = v20;
                                    rbp8 = reinterpret_cast<void**>(0x3ff);
                                    rsi16 = rbx13;
                                    rdx = reinterpret_cast<void**>(0x3ff);
                                    rax45 = fun_25b0(r15_25, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (reinterpret_cast<signed char>(0x3ff) > reinterpret_cast<signed char>(rax45)) 
                                        break;
                                    al46 = gl_scratch_buffer_grow(r13_42, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                } while (al46);
                                goto addr_54f3_45;
                                r10_47 = rbx13;
                                r13_23 = v41;
                                rbx13 = v43;
                                r9_48 = rax45;
                                rbp8 = v44;
                                if (reinterpret_cast<signed char>(rax45) < reinterpret_cast<signed char>(0)) 
                                    goto addr_5392_47;
                            } else {
                                addr_5392_47:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) {
                                    addr_5410_48:
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1));
                                    if (*reinterpret_cast<signed char*>(&eax26)) 
                                        continue; else 
                                        goto addr_541c_49;
                                } else {
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                                    if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                        addr_5450_51:
                                        *reinterpret_cast<uint32_t*>(&rdx) = v28;
                                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                        if (*reinterpret_cast<uint32_t*>(&rdx)) {
                                            if (*reinterpret_cast<signed char*>(&eax26)) 
                                                continue; else 
                                                goto addr_53f4_53;
                                        } else {
                                            rax49 = fun_2520();
                                            rsp5 = rsp5 - 8 + 8;
                                            if (*reinterpret_cast<void***>(rax49) == 22) 
                                                goto addr_5410_48; else 
                                                goto addr_5462_55;
                                        }
                                    } else {
                                        rdx50 = rbp8;
                                        while (1) {
                                            rsi51 = rdx50;
                                            ecx52 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx50 + 1));
                                            ++rdx50;
                                            if (*reinterpret_cast<signed char*>(&ecx52) == 47) 
                                                continue;
                                            rsi16 = rsi51 + 2;
                                            if (!*reinterpret_cast<signed char*>(&ecx52)) 
                                                goto addr_54f8_59;
                                            if (*reinterpret_cast<signed char*>(&ecx52) != 46) 
                                                goto addr_5450_51;
                                            ecx53 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx50 + 1));
                                            if (!*reinterpret_cast<signed char*>(&ecx53)) 
                                                goto addr_54f8_59;
                                            if (*reinterpret_cast<signed char*>(&ecx53) == 46) 
                                                goto addr_5434_63;
                                            if (*reinterpret_cast<signed char*>(&ecx53) != 47) 
                                                goto addr_5450_51;
                                            rdx50 = rsi16;
                                        }
                                    }
                                }
                            }
                            if (v30 <= 19) {
                                ++v30;
                                goto addr_560c_68;
                            }
                            if (!*reinterpret_cast<void***>(v32)) {
                                addr_560c_68:
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_47) + reinterpret_cast<unsigned char>(r9_48)) = 0;
                                if (!v29) {
                                    rax54 = fun_2640(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_55 = r9_48;
                                    r8_56 = v17;
                                    v57 = reinterpret_cast<void*>(0);
                                    r10_58 = r10_47;
                                    rax59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax54) + reinterpret_cast<unsigned char>(r9_55));
                                    if (reinterpret_cast<unsigned char>(rax59) < reinterpret_cast<unsigned char>(0x400)) 
                                        goto addr_5732_71;
                                } else {
                                    v57 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v17));
                                    rax60 = fun_2640(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_55 = r9_48;
                                    r8_56 = v17;
                                    r10_58 = r10_47;
                                    rax59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<unsigned char>(r9_55));
                                    if (reinterpret_cast<unsigned char>(0x400) > reinterpret_cast<unsigned char>(rax59)) 
                                        goto addr_57d7_73;
                                }
                            } else {
                                r11_61 = reinterpret_cast<void**>(rsp5 + 96);
                                rdi62 = reinterpret_cast<void**>(".");
                                rsi16 = r11_61;
                                rdx = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v32) - reinterpret_cast<unsigned char>(rbp8)) + reinterpret_cast<unsigned char>(r13_23));
                                *reinterpret_cast<void***>(rdx) = reinterpret_cast<void**>(0);
                                if (*reinterpret_cast<void***>(r15_25)) {
                                    rdi62 = r15_25;
                                }
                                eax63 = fun_27c0(rdi62, rsi16, rdi62, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (eax63) 
                                    goto addr_5488_77;
                                r11_64 = r11_61;
                                eax65 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v32));
                                r10_66 = r10_47;
                                r9_67 = r9_48;
                                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax65);
                                if (v27) 
                                    goto addr_55ae_79;
                                r8_56 = reinterpret_cast<void**>(0x7180);
                                rcx40 = reinterpret_cast<void**>(0x7140);
                                *reinterpret_cast<int32_t*>(&rsi16) = 0;
                                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                                rdx = reinterpret_cast<void**>(0x7110);
                                rax68 = hash_initialize(7);
                                rsp5 = rsp5 - 8 + 8;
                                r10_66 = r10_66;
                                r9_67 = r9_67;
                                v27 = rax68;
                                r11_64 = r11_64;
                                if (!rax68) 
                                    goto addr_5831_81;
                                addr_55ae_79:
                                rsi16 = v32;
                                rdx = r11_64;
                                al69 = seen_file(v27, rsi16, rdx, rcx40, v27, rsi16, rdx, rcx40);
                                rsp5 = rsp5 - 8 + 8;
                                if (al69) 
                                    goto addr_56cc_82; else 
                                    goto addr_55e6_83;
                            }
                            v70 = rbp8;
                            v71 = r10_58;
                            rbp8 = reinterpret_cast<void**>(rsp5 + 0xf0);
                            rbx13 = rax59;
                            v72 = r9_55;
                            do {
                                al73 = gl_scratch_buffer_grow_preserve(rbp8, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (!al73) 
                                    goto addr_56ac_86;
                                r8_56 = v17;
                            } while (reinterpret_cast<unsigned char>(0x400) <= reinterpret_cast<unsigned char>(rbx13));
                            r10_58 = v71;
                            r9_55 = v72;
                            rbp8 = v70;
                            if (!v29) {
                                addr_5732_71:
                                fun_28a0(reinterpret_cast<unsigned char>(r8_56) + reinterpret_cast<unsigned char>(r9_55), rbp8);
                                rsi16 = r10_58;
                                rax74 = fun_27d0(r8_56, rsi16, r9_55, r8_56, rsi16, r9_55);
                                rsp5 = rsp5 - 8 + 8 - 8 + 8;
                                rdx = r15_25 + 1;
                                rbp8 = rax74;
                                if (v75 == 47) {
                                    *reinterpret_cast<void***>(r15_25) = reinterpret_cast<void**>(47);
                                    r13_23 = rdx;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74));
                                    v29 = 1;
                                    goto addr_52e8_28;
                                } else {
                                    v29 = 1;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74));
                                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(rdx)) {
                                        do {
                                            --r13_23;
                                            if (r13_23 == r15_25) 
                                                break;
                                        } while (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47));
                                        v29 = 1;
                                        goto addr_52e8_28;
                                    }
                                }
                            } else {
                                addr_57d7_73:
                                rbp8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v57) + reinterpret_cast<unsigned char>(r8_56));
                                goto addr_5732_71;
                            }
                            addr_56cc_82:
                            if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) 
                                goto addr_5410_48; else 
                                goto addr_56d6_94;
                            addr_55e6_83:
                            rsi16 = v32;
                            rdx = r11_64;
                            record_file(v27, rsi16, rdx, rcx40, r8_56);
                            rsp5 = rsp5 - 8 + 8;
                            r10_47 = r10_66;
                            r9_48 = r9_67;
                            goto addr_560c_68;
                            addr_53f4_53:
                            *reinterpret_cast<uint32_t*>(&rdx) = 0;
                            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            rsi16 = r15_25;
                            eax76 = fun_25a0(0xffffff9c, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            if (eax76) {
                                addr_5462_55:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) != 1) 
                                    goto addr_5488_77;
                                rax77 = fun_2520();
                                rsp5 = rsp5 - 8 + 8;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax77) == 2)) 
                                    goto addr_5488_77;
                                rsi16 = reinterpret_cast<void**>("/");
                                rax78 = fun_2700(rbp8, "/");
                                rsp5 = rsp5 - 8 + 8;
                                if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<uint64_t>(rax78))) 
                                    goto addr_5410_48; else 
                                    goto addr_5488_77;
                            } else {
                                goto addr_5410_48;
                            }
                            addr_5434_63:
                            edx79 = *reinterpret_cast<unsigned char*>(rdx50 + 2);
                            if (!*reinterpret_cast<signed char*>(&edx79) || *reinterpret_cast<signed char*>(&edx79) == 47) {
                                addr_54f8_59:
                                eax80 = dir_suffix;
                                *reinterpret_cast<void***>(r13_23) = *reinterpret_cast<void***>(&eax80);
                                goto addr_53f4_53;
                            } else {
                                goto addr_5450_51;
                            }
                        }
                    }
                    addr_50da_99:
                    if (v17 != v15) {
                        fun_2500(v17, rsi16, v17, rsi16);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (v20 != v19) {
                        fun_2500(v20, rsi16, v20, rsi16);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx13)) 
                        goto addr_5320_104;
                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(0);
                    rsi16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23 + 1) - reinterpret_cast<unsigned char>(r15_25));
                    rax10 = gl_scratch_buffer_dupfree(r12_14);
                    rsp5 = rsp5 - 8 + 8;
                    if (rax10) 
                        break;
                    addr_51fa_38:
                    xalloc_die();
                    rsp5 = rsp5 - 8 + 8;
                    continue;
                    addr_52f0_17:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(r15_25 + 1)) {
                        *reinterpret_cast<int32_t*>(&rax81) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax81) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax81) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47);
                        r13_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23) - rax81);
                    }
                    addr_5308_107:
                    if (v27) {
                        hash_free();
                        rsp5 = rsp5 - 8 + 8;
                        goto addr_50da_99;
                    }
                    addr_54f3_45:
                    goto addr_51fa_38;
                    addr_541c_49:
                    goto addr_52f0_17;
                    addr_5488_77:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
                    goto addr_5308_107;
                    addr_56ac_86:
                    goto addr_51fa_38;
                    addr_5831_81:
                    goto addr_51fa_38;
                    addr_56d6_94:
                    rax82 = fun_2520();
                    rsp5 = rsp5 - 8 + 8;
                    *reinterpret_cast<void***>(rax82) = reinterpret_cast<void**>(40);
                    goto addr_5488_77;
                }
            } else {
                while (rax83 = fun_26d0(r13_23, rsi16), rsp84 = reinterpret_cast<void*>(rsp5 - 8 + 8), !rax83) {
                    rax85 = fun_2520();
                    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
                    if (*reinterpret_cast<void***>(rax85) == 12) 
                        goto addr_51fa_38;
                    if (*reinterpret_cast<void***>(rax85) != 34) 
                        goto addr_50d2_112;
                    al86 = gl_scratch_buffer_grow(r12_14, rsi16);
                    rsp5 = rsp5 - 8 + 8;
                    if (!al86) 
                        goto addr_51fa_38;
                    r13_23 = *reinterpret_cast<void***>(r12_14);
                    rsi16 = *reinterpret_cast<void***>(r12_14 + 8);
                }
                goto addr_5492_115;
            }
        }
    }
    addr_5126_116:
    rdx87 = v7 - g28;
    if (rdx87) {
        fun_2660();
    } else {
        return rax10;
    }
    addr_5320_104:
    rdi88 = *reinterpret_cast<void***>(r12_14);
    *reinterpret_cast<int32_t*>(&rax10) = 0;
    *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    if (v24 != rdi88) {
        fun_2500(rdi88, rsi16, rdi88, rsi16);
        rax10 = reinterpret_cast<void**>(0);
        goto addr_5126_116;
    }
    addr_5492_115:
    *reinterpret_cast<int32_t*>(&rsi16) = 0;
    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
    r15_25 = r13_23;
    rax89 = fun_2750(r13_23);
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
    r13_23 = rax89;
    goto addr_520d_7;
    addr_50d2_112:
    r15_25 = r13_23;
    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
    goto addr_50da_99;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0xd1e0);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0xd1e0);
    if (*reinterpret_cast<void***>(rdi + 40) != 0xd1e0) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x72b8]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0xd1e0);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x711f]");
        if (1) 
            goto addr_61cc_6;
        __asm__("comiss xmm1, [rip+0x7116]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x7108]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_61ac_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_61a2_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_61ac_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_61a2_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_61a2_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_61ac_13:
        return 0;
    } else {
        addr_61cc_6:
        return r8_3;
    }
}

struct s0 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_2510(void** rdi, ...);

int32_t transfer_entries(void** rdi, void** rsi, int32_t edx) {
    void** rdx3;
    void** r14_4;
    int32_t r12d5;
    void** rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s0* rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s1* r13_18;
    void** rax19;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = *reinterpret_cast<void***>(rsi);
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) {
        do {
            addr_6236_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_6228_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_6236_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = *reinterpret_cast<void***>(r14_4 + 16);
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi10)), rsi10 = *reinterpret_cast<void***>(r14_4 + 16), reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_62ae_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax15 = *reinterpret_cast<void***>(r14_4 + 72);
                            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            *reinterpret_cast<void***>(r14_4 + 72) = r13_9;
                            if (!rdx3) 
                                goto addr_62ae_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_29a5_12;
                    addr_62ae_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_6228_3;
            }
            rsi16 = *reinterpret_cast<void***>(r14_4 + 16);
            rdi12 = r15_8;
            rax17 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 16))) 
                goto addr_29a5_12;
            r13_18 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
            if (r13_18->f0) 
                goto addr_62e8_16;
            r13_18->f0 = r15_8;
            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
            continue;
            addr_62e8_16:
            rax19 = *reinterpret_cast<void***>(r14_4 + 72);
            if (!rax19) {
                rax19 = fun_2800(16, rsi16, rdx3);
                if (!rax19) 
                    goto addr_635a_19;
            } else {
                *reinterpret_cast<void***>(r14_4 + 72) = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx3 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx3;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            *reinterpret_cast<void***>(rbp6 + 24) = *reinterpret_cast<void***>(rbp6 + 24) - 1;
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_29a5_12:
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    fun_2510(rdi12, rdi12);
    addr_635a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
        fun_2510(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_605f_22:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_6004_25;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<int64_t*>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_6070_29;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<int64_t*>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_6070_29;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_605f_22;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_6004_25;
            }
        }
    }
    addr_6061_33:
    return rax11;
    addr_6004_25:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_6061_33;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_6070_29:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_2630();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2630();
    if (r8d > 10) {
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xd2c0 + rax11 * 4) + 0xd2c0;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_26c0();

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x885f;
    rax8 = fun_2520();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
        fun_2510(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x12090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x9691]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x88eb;
            fun_26c0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x12160) {
                fun_2500(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x897a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = rax6 - g28;
        if (rax29) {
            fun_2660();
        } else {
            return r14_19;
        }
    }
}

unsigned char logical = 0;

int32_t fun_2980(int64_t rdi, void** rsi, void** rdx, void** rcx);

unsigned char hard_dir_link = 0;

void** dir_name(void** rdi, void** rsi, void** rdx, void** rcx);

void** canonicalize_filename_mode(void** rdi, void** rsi, void** rdx, void** rcx);

void** xstrdup(void** rdi, void** rsi, void** rdx, void** rcx);

void** xmalloc(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

signed char relpath(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t quotearg_n_style_colon();

void** fun_2620();

int64_t fun_28b0();

void** dest_set = reinterpret_cast<void**>(0);

signed char verbose = 0;

void fun_2550(void* rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** quotearg_n_style();

void fun_2880(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, int64_t* a8, void** a9, void** a10, void** a11, int64_t a12, void** a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18);

void** quotearg_style(int64_t rdi, void** rsi, ...);

unsigned char remove_existing_files = 0;

unsigned char interactive = 0;

void** backup_type = reinterpret_cast<void**>(0);

signed char same_nameat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void** program_name = reinterpret_cast<void**>(0);

void** stderr = reinterpret_cast<void**>(0);

void fun_2930(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

signed char yesno(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** find_backup_file_name(int64_t rdi, void** rsi);

int32_t fun_2900(int64_t rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

void** force_symlinkat(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** force_linkat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

uint32_t do_link(void** rdi, void** esi, void** rdx, void** rcx, void** r8, int64_t r9) {
    void** rsi2;
    void** r10_7;
    void** r13_8;
    void** r12_9;
    void** rbp10;
    void*** rsp11;
    uint32_t ecx12;
    int64_t rax13;
    uint32_t ecx14;
    void** rcx15;
    void** v16;
    void** eax17;
    uint32_t r9d18;
    int32_t r15d19;
    int32_t eax20;
    uint32_t r9d21;
    uint32_t v22;
    void** rbx23;
    int1_t zf24;
    int64_t v25;
    void** rax26;
    void** v27;
    void** rax28;
    void** rsi29;
    void** rax30;
    void* rsp31;
    void** v32;
    void** rax33;
    void** rax34;
    signed char al35;
    void* rsp36;
    uint32_t r9d37;
    void** r15_38;
    void** rdx39;
    int1_t zf40;
    void** rcx41;
    void** r13_42;
    void** r13_43;
    void** rax44;
    void** rax45;
    void** rax46;
    void** rax47;
    uint1_t cf48;
    void** rax49;
    void** r9_50;
    void** v51;
    int64_t v52;
    int64_t v53;
    int64_t v54;
    int64_t v55;
    int64_t v56;
    void** rdx57;
    int32_t eax58;
    int1_t zf59;
    int1_t zf60;
    int1_t zf61;
    int64_t rdi62;
    void** rdx63;
    int32_t eax64;
    void*** rsp65;
    void** rax66;
    void** r9d67;
    void** rsi68;
    uint32_t v69;
    void** rdi70;
    signed char al71;
    void** edi72;
    uint32_t eax73;
    uint32_t eax74;
    int64_t rax75;
    void** rsi76;
    int32_t eax77;
    int64_t v78;
    int64_t v79;
    int64_t v80;
    int64_t v81;
    int64_t v82;
    int64_t rdx83;
    signed char al84;
    uint32_t r9d85;
    void** rax86;
    void** r15_87;
    void** rax88;
    void** rdi89;
    signed char al90;
    uint32_t r9d91;
    void** edx92;
    int64_t rdi93;
    void** rax94;
    int64_t rdx95;
    int64_t rdi96;
    int32_t eax97;
    int1_t zf98;
    void** eax99;
    void** r9d100;
    int64_t rdx101;
    uint32_t r8d102;
    void** eax103;
    void** rax104;
    void** v105;
    void** rax106;
    int1_t zf107;
    void** v108;
    void** r9d109;
    int64_t rdx110;
    int64_t rdi111;
    int32_t eax112;
    void** rax113;
    void** rdi114;

    rsi2 = esi;
    r10_7 = r8;
    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
    r13_8 = rdx;
    r12_9 = rdi;
    rbp10 = rsi2;
    rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x158);
    ecx12 = logical;
    rax13 = g28;
    ecx14 = ecx12 ^ 1;
    rcx15 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ecx14)) << 8);
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    if (reinterpret_cast<signed char>(r8) < reinterpret_cast<signed char>(0)) {
        v16 = reinterpret_cast<void**>(0x3448);
        eax17 = atomic_link(rdi, rsi2, rdx);
        rsp11 = rsp11 - 8 + 8;
        rcx15 = rcx15;
        *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
        r10_7 = eax17;
        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
    }
    r9d18 = symbolic_link;
    if (r10_7) {
        r15d19 = 1;
        if (*reinterpret_cast<void***>(&r9d18)) 
            goto addr_335f_5;
        rdx = reinterpret_cast<void**>(rsp11 + 32);
        rsi2 = r12_9;
        v16 = reinterpret_cast<void**>(0x35d6);
        eax20 = fun_2980(0xffffff9c, rsi2, rdx, rcx15);
        rsp11 = rsp11 - 8 + 8;
        r10_7 = r10_7;
        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
        r15d19 = eax20;
        if (eax20) 
            goto addr_3920_7;
        r9d21 = hard_dir_link;
        if (*reinterpret_cast<void***>(&r9d21) || (v22 & 0xf000) != 0x4000) {
            addr_335f_5:
            *reinterpret_cast<int32_t*>(&rbx23) = 0;
            *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
            zf24 = relative == 0;
            if (!zf24) {
                *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4) = r10_7;
                rax26 = dir_name(rcx, rsi2, rdx, rcx15);
                v27 = rax26;
                rax28 = canonicalize_filename_mode(rax26, 2, rdx, rcx15);
                *reinterpret_cast<int32_t*>(&rsi29) = 2;
                *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
                rax30 = canonicalize_filename_mode(r12_9, 2, rdx, rcx15);
                rsp31 = reinterpret_cast<void*>(rsp11 - 8 + 8 - 8 + 8 - 8 + 8);
                v32 = rax30;
                if (!rax28 || !rax30) {
                    addr_39c8_10:
                    fun_2500(v27, rsi29, v27, rsi29);
                    fun_2500(rax28, rsi29, rax28, rsi29);
                    fun_2500(v32, rsi29, v32, rsi29);
                    v16 = reinterpret_cast<void**>(0x39f6);
                    rax33 = xstrdup(r12_9, rsi29, rdx, rcx15);
                    rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    r10_7 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4);
                    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                    r12_9 = rax33;
                    rbx23 = rax33;
                } else {
                    rax34 = xmalloc("34", 2, rdx, rcx15, r8);
                    rsi29 = rax28;
                    rcx15 = reinterpret_cast<void**>("34");
                    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                    rdx = rax34;
                    rbx23 = rax34;
                    al35 = relpath(v32, rsi29, rdx, "34");
                    rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
                    if (!al35) {
                        fun_2500(rbx23, rsi29, rbx23, rsi29);
                        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
                        goto addr_39c8_10;
                    } else {
                        *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4) = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4);
                        r12_9 = rbx23;
                        fun_2500(v27, rsi29, v27, rsi29);
                        fun_2500(rax28, rsi29, rax28, rsi29);
                        v16 = reinterpret_cast<void**>(0x389f);
                        fun_2500(v32, rsi29, v32, rsi29);
                        rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp36) - 8 + 8 - 8 + 8 - 8 + 8);
                        r10_7 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v25) + 4);
                        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                    }
                }
            }
        } else {
            quotearg_n_style_colon();
            fun_2620();
            fun_28b0();
            r9d37 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d21));
            goto addr_3321_15;
        }
    } else {
        r15_38 = dest_set;
        *reinterpret_cast<int32_t*>(&rbx23) = 0;
        *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
        if (!r15_38) {
            addr_3658_17:
            if (!*reinterpret_cast<void***>(&r9d18)) {
                rdx39 = reinterpret_cast<void**>(rsp11 + 32);
                goto addr_38f3_19;
            } else {
                addr_3661_20:
                zf40 = verbose == 0;
                r9d18 = 1;
                if (!zf40) {
                    if (!r15_38) {
                        addr_3750_22:
                        rcx41 = reinterpret_cast<void**>(0xd6d8);
                        r13_42 = reinterpret_cast<void**>(0xd6d8);
                    } else {
                        r13_43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(rcx));
                        rbp10 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
                        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r13_43) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r13_43 == 0))) {
                            rax44 = fun_2640(r15_38, r15_38);
                            rax45 = xmalloc(reinterpret_cast<unsigned char>(r13_43) + reinterpret_cast<unsigned char>(rax44) + 1, rsi2, r15_38, rcx15, r8);
                            rbp10 = rax45;
                            fun_27d0(rax45, rcx, r13_43, rax45, rcx, r13_43);
                            fun_2550(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r13_43), r15_38, r13_43, rcx15, r8);
                        }
                        rax46 = quotearg_n_style();
                        r13_42 = rax46;
                        fun_2500(rbp10, 4, rbp10, 4);
                        rcx41 = reinterpret_cast<void**>(" ~ ");
                    }
                    rax47 = quotearg_n_style();
                    cf48 = reinterpret_cast<uint1_t>(symbolic_link < 1);
                    rax49 = quotearg_n_style();
                    *reinterpret_cast<uint32_t*>(&r9_50) = (reinterpret_cast<unsigned char>(rbp10) - (reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp10) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp10) + cf48))) & 16) + 45;
                    *reinterpret_cast<int32_t*>(&r9_50 + 4) = 0;
                    rsi2 = reinterpret_cast<void**>("%s%s%s %c> %s\n");
                    fun_2880(1, "%s%s%s %c> %s\n", r13_42, rcx41, rax49, r9_50, rax47, 0x3714, rcx41, v32, v27, v25, v51, v52, v53, v54, v55, v56);
                    r9d18 = 1;
                    goto addr_3308_27;
                }
            }
        } else {
            if (!*reinterpret_cast<void***>(&r9d18)) {
                rdx57 = reinterpret_cast<void**>(rsp11 + 32);
                eax58 = fun_2980(0xffffff9c, r12_9, rdx57, rcx15);
                if (eax58) {
                    addr_3920_7:
                    quotearg_style(4, r12_9, 4, r12_9);
                    fun_2620();
                    fun_2520();
                    fun_28b0();
                    r9d37 = 0;
                    goto addr_3321_15;
                } else {
                    rdx39 = rdx57;
                    *reinterpret_cast<int32_t*>(&rbx23) = 0;
                    *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r15_38) = 0;
                    *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
                    goto addr_38f3_19;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rbx23) = 0;
                *reinterpret_cast<int32_t*>(&rbx23 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r15_38) = 0;
                *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
                zf59 = verbose == 0;
                if (!zf59) 
                    goto addr_3750_22;
                goto addr_3308_27;
            }
        }
    }
    zf60 = remove_existing_files == 0;
    if (!zf60) 
        goto addr_337b_34;
    zf61 = interactive == 0;
    if (!zf61) 
        goto addr_337b_34;
    r8 = backup_type;
    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
    if (!r8) 
        goto addr_3475_37;
    addr_337b_34:
    *reinterpret_cast<void***>(&rdi62) = rbp10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi62) + 4) = 0;
    rdx63 = reinterpret_cast<void**>(rsp11 + 0xb0);
    v32 = rdx63;
    eax64 = fun_2980(rdi62, r13_8, rdx63, 0x100);
    rsp65 = rsp11 - 8 + 8;
    if (eax64) {
        v16 = reinterpret_cast<void**>(0x3769);
        rax66 = fun_2520();
        rsp11 = rsp65 - 8 + 8;
        r10_7 = r10_7;
        *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
        if (*reinterpret_cast<void***>(rax66) == 2) {
            addr_3475_37:
            r9d67 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&r15_38) = 0;
            *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
            goto addr_347b_39;
        } else {
            quotearg_style(4, rcx);
            fun_2620();
            rsi68 = *reinterpret_cast<void***>(rax66);
            *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
            fun_28b0();
        }
    } else {
        if ((v69 & 0xf000) == 0x4000) {
            quotearg_n_style_colon();
            fun_2620();
            rsi68 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
            fun_28b0();
        } else {
            rdi70 = dest_set;
            v16 = reinterpret_cast<void**>(0x33d6);
            al71 = seen_file(rdi70, rcx, v32, 0x100);
            rsp11 = rsp65 - 8 + 8;
            r10_7 = r10_7;
            *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
            if (al71) {
                quotearg_n_style();
                quotearg_n_style();
                goto addr_399b_45;
            } else {
                edi72 = backup_type;
                eax73 = remove_existing_files;
                if (edi72) {
                    eax74 = symbolic_link;
                    eax73 = eax74 ^ 1;
                }
                if (!*reinterpret_cast<signed char*>(&eax73)) 
                    goto addr_341d_49;
                if (r15d19) 
                    goto addr_3b73_51; else 
                    goto addr_340a_52;
            }
        }
    }
    addr_37ae_53:
    fun_2500(rbx23, rsi68, rbx23, rsi68);
    r9d37 = 0;
    addr_3321_15:
    rax75 = rax13 - g28;
    if (rax75) {
        fun_2660();
    } else {
        return r9d37;
    }
    addr_399b_45:
    fun_2620();
    rsi68 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
    fun_28b0();
    goto addr_37ae_53;
    addr_3b73_51:
    rsi76 = reinterpret_cast<void**>(rsp11 + 32);
    v16 = reinterpret_cast<void**>(0x3b87);
    eax77 = fun_27c0(r12_9, rsi76, r12_9, rsi76);
    rsp11 = rsp11 - 8 + 8;
    r10_7 = r10_7;
    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
    if (eax77) 
        goto addr_341d_49;
    addr_340a_52:
    if (v78 != v79 || (v80 != v81 || v82 != 1 && (*reinterpret_cast<void***>(&rdx83) = rbp10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx83) + 4) = 0, v16 = reinterpret_cast<void**>(0x3c49), al84 = same_nameat(0xffffff9c, r12_9, rdx83, r13_8), rsp11 = rsp11 - 8 + 8, r10_7 = r10_7, *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0, al84 == 0))) {
        addr_341d_49:
        if (reinterpret_cast<signed char>(r10_7) < reinterpret_cast<signed char>(0)) 
            goto addr_3a80_58;
        if (r10_7 != 17) 
            goto addr_3430_60;
    } else {
        quotearg_n_style();
        quotearg_n_style();
        goto addr_399b_45;
    }
    addr_3a80_58:
    r9d85 = interactive;
    if (!*reinterpret_cast<unsigned char*>(&r9d85) || (v27 = r10_7, *reinterpret_cast<unsigned char*>(&v32) = *reinterpret_cast<unsigned char*>(&r9d85), rax86 = quotearg_style(4, rcx, 4, rcx), r15_87 = program_name, rax88 = fun_2620(), r8 = rax86, rdi89 = stderr, fun_2930(rdi89, 1, rax88, r15_87, r8), v16 = reinterpret_cast<void**>(0x3bef), al90 = yesno(rdi89, 1, rax88, r15_87, r8), rsp11 = rsp11 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8, r9d91 = *reinterpret_cast<unsigned char*>(&v32), r10_7 = v27, *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0, !!al90)) {
        edx92 = backup_type;
        if (!edx92) {
            addr_3430_60:
            r9d67 = reinterpret_cast<void**>(1);
            *reinterpret_cast<int32_t*>(&r15_38) = 0;
            *reinterpret_cast<int32_t*>(&r15_38 + 4) = 0;
            goto addr_347b_39;
        } else {
            *reinterpret_cast<void***>(&rdi93) = rbp10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi93) + 4) = 0;
            rax94 = find_backup_file_name(rdi93, r13_8);
            *reinterpret_cast<void***>(&rdx95) = rbp10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx95) + 4) = 0;
            *reinterpret_cast<void***>(&rdi96) = rbp10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
            r15_38 = rax94;
            v16 = reinterpret_cast<void**>(0x3abf);
            eax97 = fun_2900(rdi96, r13_8, rdx95, rax94, r8);
            rsp11 = rsp11 - 8 + 8 - 8 + 8;
            r10_7 = r10_7;
            *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
            r9d67 = reinterpret_cast<void**>(1);
            if (!eax97) {
                addr_347b_39:
                zf98 = symbolic_link == 0;
                if (!zf98) {
                    rcx15 = r9d67;
                    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                    r8 = r10_7;
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    rsi2 = rbp10;
                    *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
                    eax99 = force_symlinkat(r12_9, rsi2, r13_8, rcx15, r8);
                    rsp11 = rsp11 - 8 + 8;
                    r9d100 = eax99;
                } else {
                    *reinterpret_cast<void***>(&rdx101) = rbp10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx101) + 4) = 0;
                    r8d102 = logical;
                    r8 = reinterpret_cast<void**>(r8d102 << 10);
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    eax103 = force_linkat(0xffffff9c, r12_9, rdx101, r13_8);
                    rcx15 = r10_7;
                    rsi2 = v16;
                    rsp11 = rsp11 - 8 - 8 - 8 + 8 + 8 + 8;
                    r9d100 = eax103;
                }
            } else {
                v32 = r10_7;
                rax104 = fun_2520();
                v105 = *reinterpret_cast<void***>(rax104);
                v16 = reinterpret_cast<void**>(0x3ae8);
                fun_2500(r15_38, r13_8, r15_38, r13_8);
                rsp11 = rsp11 - 8 + 8 - 8 + 8;
                r10_7 = v32;
                *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                if (v105 == 2) 
                    goto addr_3475_37;
                quotearg_style(4, rcx, 4, rcx);
                fun_2620();
                rsi68 = v105;
                *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
                fun_28b0();
                goto addr_37ae_53;
            }
        }
    } else {
        fun_2500(rbx23, 1, rbx23, 1);
        r9d37 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d91));
        goto addr_3321_15;
    }
    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r9d100) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r9d100 == 0)) {
        r9d18 = symbolic_link;
        goto addr_3658_17;
    } else {
        rax106 = quotearg_n_style();
        quotearg_n_style();
        zf107 = symbolic_link == 0;
        if (zf107) {
            if (r9d100 == 31) {
                v108 = r9d100;
            } else {
                if (r9d100 == 0x7a || (r9d100 == 17 || (reinterpret_cast<unsigned char>(r9d100) & 0xfffffffd) == 28)) {
                    v108 = r9d100;
                } else {
                    fun_2620();
                    r9d109 = r9d100;
                    goto addr_3534_77;
                }
            }
        } else {
            if (r9d100 == 36 || !*reinterpret_cast<void***>(r12_9)) {
                v108 = r9d100;
            } else {
                v108 = r9d100;
            }
        }
    }
    fun_2620();
    r9d109 = v108;
    addr_3534_77:
    rsi2 = r9d109;
    *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
    fun_28b0();
    r9d18 = 0;
    if (r15_38 && (*reinterpret_cast<void***>(&rdx110) = rbp10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx110) + 4) = 0, rsi2 = r15_38, *reinterpret_cast<void***>(&rdi111) = rbp10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi111) + 4) = 0, eax112 = fun_2900(rdi111, rsi2, rdx110, r13_8, rax106), r9d18 = 0, !!eax112)) {
        quotearg_style(4, rcx, 4, rcx);
        fun_2620();
        rax113 = fun_2520();
        rsi2 = *reinterpret_cast<void***>(rax113);
        *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
        fun_28b0();
        r9d18 = 0;
    }
    addr_3308_27:
    fun_2500(r15_38, rsi2, r15_38, rsi2);
    fun_2500(rbx23, rsi2, rbx23, rsi2);
    r9d37 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d18));
    goto addr_3321_15;
    addr_38f3_19:
    rdi114 = dest_set;
    rsi2 = rcx;
    record_file(rdi114, rsi2, rdx39, rcx15, r8);
    goto addr_3661_20;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x120a8;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xd263);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xd25c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xd267);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xd258);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g11d78 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g11d78;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2493() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24d3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_24e3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_24f3() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2040;

void fun_2503() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2513() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2523() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2533() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2543() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x2090;

void fun_2553() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x20a0;

void fun_2563() {
    __asm__("cli ");
    goto __fpending;
}

int64_t mkdir = 0x20b0;

void fun_2573() {
    __asm__("cli ");
    goto mkdir;
}

int64_t unlinkat = 0x20c0;

void fun_2583() {
    __asm__("cli ");
    goto unlinkat;
}

int64_t reallocarray = 0x20d0;

void fun_2593() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t faccessat = 0x20e0;

void fun_25a3() {
    __asm__("cli ");
    goto faccessat;
}

int64_t readlink = 0x20f0;

void fun_25b3() {
    __asm__("cli ");
    goto readlink;
}

int64_t fcntl = 0x2100;

void fun_25c3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clock_gettime = 0x2110;

void fun_25d3() {
    __asm__("cli ");
    goto clock_gettime;
}

int64_t textdomain = 0x2120;

void fun_25e3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t pathconf = 0x2130;

void fun_25f3() {
    __asm__("cli ");
    goto pathconf;
}

int64_t fclose = 0x2140;

void fun_2603() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2150;

void fun_2613() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2160;

void fun_2623() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2170;

void fun_2633() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2180;

void fun_2643() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x2190;

void fun_2653() {
    __asm__("cli ");
    goto openat;
}

int64_t __stack_chk_fail = 0x21a0;

void fun_2663() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x21b0;

void fun_2673() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x21c0;

void fun_2683() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x21d0;

void fun_2693() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21e0;

void fun_26a3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21f0;

void fun_26b3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2200;

void fun_26c3() {
    __asm__("cli ");
    goto memset;
}

int64_t getcwd = 0x2210;

void fun_26d3() {
    __asm__("cli ");
    goto getcwd;
}

int64_t close = 0x2220;

void fun_26e3() {
    __asm__("cli ");
    goto close;
}

int64_t rewinddir = 0x2230;

void fun_26f3() {
    __asm__("cli ");
    goto rewinddir;
}

int64_t strspn = 0x2240;

void fun_2703() {
    __asm__("cli ");
    goto strspn;
}

int64_t closedir = 0x2250;

void fun_2713() {
    __asm__("cli ");
    goto closedir;
}

int64_t lstat = 0x2260;

void fun_2723() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x2270;

void fun_2733() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2280;

void fun_2743() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x2290;

void fun_2753() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x22a0;

void fun_2763() {
    __asm__("cli ");
    goto calloc;
}

int64_t __getdelim = 0x22b0;

void fun_2773() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x22c0;

void fun_2783() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x22d0;

void fun_2793() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t fpathconf = 0x22e0;

void fun_27a3() {
    __asm__("cli ");
    goto fpathconf;
}

int64_t rpmatch = 0x22f0;

void fun_27b3() {
    __asm__("cli ");
    goto rpmatch;
}

int64_t stat = 0x2300;

void fun_27c3() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2310;

void fun_27d3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2320;

void fun_27e3() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x2330;

void fun_27f3() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x2340;

void fun_2803() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2350;

void fun_2813() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2360;

void fun_2823() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t renameat2 = 0x2370;

void fun_2833() {
    __asm__("cli ");
    goto renameat2;
}

int64_t __freading = 0x2380;

void fun_2843() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2390;

void fun_2853() {
    __asm__("cli ");
    goto realloc;
}

int64_t linkat = 0x23a0;

void fun_2863() {
    __asm__("cli ");
    goto linkat;
}

int64_t setlocale = 0x23b0;

void fun_2873() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x23c0;

void fun_2883() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x23d0;

void fun_2893() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x23e0;

void fun_28a3() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x23f0;

void fun_28b3() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x2400;

void fun_28c3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2410;

void fun_28d3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fdopendir = 0x2420;

void fun_28e3() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t __cxa_atexit = 0x2430;

void fun_28f3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t renameat = 0x2440;

void fun_2903() {
    __asm__("cli ");
    goto renameat;
}

int64_t exit = 0x2450;

void fun_2913() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2460;

void fun_2923() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2470;

void fun_2933() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getrandom = 0x2480;

void fun_2943() {
    __asm__("cli ");
    goto getrandom;
}

int64_t mbsinit = 0x2490;

void fun_2953() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t symlinkat = 0x24a0;

void fun_2963() {
    __asm__("cli ");
    goto symlinkat;
}

int64_t iswprint = 0x24b0;

void fun_2973() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstatat = 0x24c0;

void fun_2983() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x24d0;

void fun_2993() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2870(int64_t rdi, ...);

void fun_2610(int64_t rdi, int64_t rsi);

void fun_25e0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

void set_simple_backup_suffix(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2910();

void usage();

int64_t fun_2670(int64_t rdi, void** rsi);

int64_t Version = 0xd0a6;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t optind = 0;

unsigned char dereference_dest_dir_symlinks = 1;

void** openat_safer(int64_t rdi, void** rsi);

void** xget_version(void** rdi, void** rsi, void** rdx, void** rcx);

void** file_name_concat(void** rdi, void** rsi, void** rdx, void** rcx);

void strip_trailing_slashes(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2a13(void** edi, void** rsi) {
    void** r15_3;
    void** r14_4;
    void** r13_5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** r12_9;
    void*** rsp10;
    void** v11;
    void** v12;
    void** v13;
    int64_t rax14;
    void** r8_15;
    void** rsi16;
    void** rdx17;
    void** rcx18;
    void*** rsp19;
    void*** rsp20;
    void** rdi21;
    uint32_t eax22;
    uint32_t r13d23;
    void** rsi24;
    void** rax25;
    void* rsp26;
    void** rsi27;
    void** rax28;
    int64_t rdi29;
    int64_t rax30;
    void** rdi31;
    int64_t rcx32;
    int64_t rax33;
    int1_t zf34;
    int1_t zf35;
    void** rax36;
    int1_t zf37;
    unsigned char r13b38;
    void** r12_39;
    uint1_t cf40;
    void** eax41;
    void** v42;
    void** rax43;
    void** rax44;
    void** rax45;
    void** eax46;
    int1_t zf47;
    int1_t zf48;
    int1_t zf49;
    void** rax50;
    void** rdi51;
    void** eax52;
    void** rdi53;
    void** rax54;
    void** rax55;
    int64_t v56;
    void** rdi57;
    void** v58;
    uint32_t eax59;

    __asm__("cli ");
    r15_3 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&r15_3 + 4) = 0;
    r14_4 = reinterpret_cast<void**>(0x118a0);
    r13_5 = reinterpret_cast<void**>("bdfinrst:vFLPS:T");
    rbp6 = edi;
    rbx7 = rsi;
    rdi8 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi8);
    fun_2870(6, 6);
    fun_2610("coreutils", "/usr/local/share/locale");
    r12_9 = reinterpret_cast<void**>(0xcfbc);
    fun_25e0("coreutils", "/usr/local/share/locale");
    atexit(0x58b0, "/usr/local/share/locale");
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xd8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    hard_dir_link = 0;
    verbose = 0;
    interactive = 0;
    remove_existing_files = 0;
    symbolic_link = 0;
    *reinterpret_cast<signed char*>(&v11) = 0;
    v12 = reinterpret_cast<void**>(0);
    v13 = reinterpret_cast<void**>(0);
    goto addr_2ad6_2;
    addr_30c8_3:
    fun_2620();
    fun_28b0();
    fun_2620();
    fun_28b0();
    addr_2b04_5:
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_9) + reinterpret_cast<uint64_t>(rax14 * 4)))) + reinterpret_cast<unsigned char>(r12_9);
    while (1) {
        addr_2f6a_6:
        if (!v13) {
            while (1) {
                addr_2feb_7:
                v13 = r8_15;
                backup_type = reinterpret_cast<void**>(0);
                set_simple_backup_suffix(0, rsi16, rdx17, rcx18);
                rsp19 = rsp20 - 8 + 8;
                r8_15 = v13;
                *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0;
                while (1) {
                    rdx17 = *reinterpret_cast<void***>(rbx7 + 8);
                    rdi21 = *reinterpret_cast<void***>(rbx7);
                    rsi16 = reinterpret_cast<void**>(0xffffff9c);
                    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                    rcx18 = rdx17;
                    eax22 = do_link(rdi21, 0xffffff9c, rdx17, rcx18, r8_15, "David MacKenzie");
                    rsp19 = rsp19 - 8 + 8;
                    r13d23 = eax22;
                    while (1) {
                        *reinterpret_cast<uint32_t*>(&r13_5) = r13d23 ^ 1;
                        *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
                        fun_2910();
                        rsp20 = rsp19 - 8 + 8;
                        do {
                            if (r15_3) 
                                goto addr_30c8_3;
                            if (rbp6 == 2) 
                                goto addr_2f6a_6;
                            --rbp6;
                            if (!rbp6) {
                                rsi24 = *reinterpret_cast<void***>(rbx7);
                                rax25 = quotearg_style(4, rsi24, 4, rsi24);
                                rsp26 = reinterpret_cast<void*>(rsp20 - 8 + 8);
                                r12_9 = rax25;
                            } else {
                                rsi27 = *reinterpret_cast<void***>(rbx7 + 16);
                                rax28 = quotearg_style(4, rsi27, 4, rsi27);
                                rsp26 = reinterpret_cast<void*>(rsp20 - 8 + 8);
                                r12_9 = rax28;
                            }
                            fun_2620();
                            fun_28b0();
                            rsp20 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
                            while (1) {
                                addr_2b4a_16:
                                usage();
                                rsp10 = rsp20 - 8 + 8;
                                hard_dir_link = 1;
                                addr_2ad6_2:
                                while (rcx18 = r14_4, rdx17 = r13_5, rsi16 = rbx7, *reinterpret_cast<void***>(&rdi29) = rbp6, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0, rax30 = fun_2670(rdi29, rsi16), rsp20 = rsp10 - 8 + 8, *reinterpret_cast<void***>(&rax30) != 0xffffffff) {
                                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax30)) > reinterpret_cast<signed char>(0x76)) 
                                        goto addr_2b4a_16;
                                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax30)) > reinterpret_cast<signed char>(69)) 
                                        goto addr_2afc_19;
                                    if (*reinterpret_cast<void***>(&rax30) != 0xffffff7d) 
                                        goto addr_2b1b_21;
                                    rdi31 = stdout;
                                    rcx32 = Version;
                                    version_etc(rdi31, "ln", "GNU coreutils", rcx32, "Mike Parker", "David MacKenzie", 0, rax30);
                                    fun_2910();
                                    rsp10 = rsp20 - 8 - 8 - 8 + 8 - 8 + 8;
                                    *reinterpret_cast<signed char*>(&v12) = 1;
                                }
                                r8_15 = *reinterpret_cast<void***>(&rax30);
                                rax33 = optind;
                                rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) - *reinterpret_cast<uint32_t*>(&rax33));
                                if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rbp6) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rbp6 == 0))) 
                                    break;
                                addr_2b29_24:
                                fun_2620();
                                fun_28b0();
                                rsp20 = rsp20 - 8 + 8 - 8 + 8;
                                continue;
                                addr_2afc_19:
                                *reinterpret_cast<uint32_t*>(&rax14) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rax30) - 70);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                                if (*reinterpret_cast<uint32_t*>(&rax14) > 48) 
                                    continue; else 
                                    goto addr_2b04_5;
                                addr_2b1b_21:
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax30) == 0xffffff7e)) 
                                    continue;
                                usage();
                                rsp20 = rsp20 - 8 + 8;
                                goto addr_2b29_24;
                            }
                            zf34 = relative == 0;
                            if (!zf34 && (zf35 = symbolic_link == 0, zf35)) {
                                rax36 = fun_2620();
                                rsi16 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                                rdx17 = rax36;
                                rax33 = fun_28b0();
                                rsp20 = rsp20 - 8 + 8 - 8 + 8;
                            }
                            zf37 = hard_dir_link == 0;
                            if (zf37) {
                                beware_hard_dir_link = 0;
                            }
                            rbx7 = rbx7 + rax33 * 8;
                        } while (*reinterpret_cast<signed char*>(&v11));
                        r13b38 = reinterpret_cast<uint1_t>(r15_3 == 0);
                        if (reinterpret_cast<signed char>(rbp6) > reinterpret_cast<signed char>(1) || !r13b38) {
                            if (!reinterpret_cast<int1_t>(rbp6 == 2) || !r13b38) {
                                r12_39 = r15_3;
                                if (r15_3) {
                                    addr_2e85_34:
                                    cf40 = reinterpret_cast<uint1_t>(dereference_dest_dir_symlinks < 1);
                                    rsi16 = r12_39;
                                    v11 = r8_15;
                                    *reinterpret_cast<uint32_t*>(&rdx17) = (*reinterpret_cast<uint32_t*>(&rdx17) - (*reinterpret_cast<uint32_t*>(&rdx17) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx17) < *reinterpret_cast<uint32_t*>(&rdx17) + cf40)) & 0x20000) + 0x210000;
                                    *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
                                    eax41 = openat_safer(0xffffff9c, rsi16);
                                    v42 = eax41;
                                    rax43 = fun_2520();
                                    rsp20 = rsp20 - 8 + 8 - 8 + 8;
                                    r8_15 = v11;
                                    r14_4 = *reinterpret_cast<void***>(rax43);
                                    *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
                                    if (reinterpret_cast<signed char>(v42) < reinterpret_cast<signed char>(0)) {
                                        if (!reinterpret_cast<int1_t>(rbp6 == 2)) 
                                            goto addr_3031_36;
                                        if (!r15_3) 
                                            goto addr_2f6a_6;
                                        addr_3031_36:
                                        rax44 = quotearg_style(4, r12_39, 4, r12_39);
                                        fun_2620();
                                        rcx18 = rax44;
                                        fun_28b0();
                                        rsp20 = rsp20 - 8 + 8 - 8 + 8 - 8 + 8;
                                        goto addr_3069_38;
                                    } else {
                                        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) - r13b38);
                                        v11 = rbp6;
                                        if (v13) {
                                            addr_2f7b_40:
                                            *reinterpret_cast<uint32_t*>(&rdx17) = 5;
                                            *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
                                            rax45 = fun_2620();
                                            rsi16 = v12;
                                            eax46 = xget_version(rax45, rsi16, 5, rcx18);
                                            rsp20 = rsp20 - 8 + 8 - 8 + 8;
                                            r8_15 = r8_15;
                                        } else {
                                            eax46 = reinterpret_cast<void**>(0);
                                        }
                                        v13 = r8_15;
                                        backup_type = eax46;
                                        set_simple_backup_suffix(0, rsi16, rdx17, rcx18);
                                        rsp19 = rsp20 - 8 + 8;
                                        r8_15 = v13;
                                        *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0;
                                        if (!r12_39) 
                                            break;
                                        if (reinterpret_cast<signed char>(v11) <= reinterpret_cast<signed char>(1)) 
                                            goto addr_2d71_44;
                                        zf47 = remove_existing_files == 0;
                                        if (zf47) 
                                            goto addr_2d71_44;
                                        zf48 = symbolic_link == 0;
                                        if (!zf48) 
                                            goto addr_2d71_44;
                                        zf49 = reinterpret_cast<int1_t>(backup_type == 3);
                                        if (zf49) 
                                            goto addr_2d71_44;
                                        r8_15 = reinterpret_cast<void**>(0x7180);
                                        rcx18 = reinterpret_cast<void**>(0x70e0);
                                        rsi16 = reinterpret_cast<void**>(0);
                                        *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                                        rdx17 = reinterpret_cast<void**>(0x7110);
                                        rax50 = hash_initialize(61);
                                        rsp19 = rsp19 - 8 + 8;
                                        dest_set = rax50;
                                        if (!rax50) 
                                            goto addr_2f65_49;
                                    }
                                } else {
                                    goto addr_2e7d_51;
                                }
                            } else {
                                rdx17 = *reinterpret_cast<void***>(rbx7 + 8);
                                rdi51 = *reinterpret_cast<void***>(rbx7);
                                rsi16 = reinterpret_cast<void**>(0xffffff9c);
                                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                                eax52 = atomic_link(rdi51, 0xffffff9c, rdx17, rdi51, 0xffffff9c, rdx17);
                                rsp20 = rsp20 - 8 + 8;
                                r8_15 = eax52;
                                if (reinterpret_cast<signed char>(eax52) >= reinterpret_cast<signed char>(0) && (eax52 != 17 && (reinterpret_cast<unsigned char>(eax52) & 0xfffffffd) != 20)) {
                                    *reinterpret_cast<int32_t*>(&r12_39) = 0;
                                    *reinterpret_cast<int32_t*>(&r12_39 + 4) = 0;
                                    v11 = reinterpret_cast<void**>(2);
                                    if (v13) 
                                        goto addr_2f7b_40; else 
                                        goto addr_2feb_7;
                                }
                            }
                        } else {
                            if (v13) {
                                addr_3069_38:
                                v11 = reinterpret_cast<void**>(1);
                                r12_39 = reinterpret_cast<void**>(".");
                                v42 = reinterpret_cast<void**>(0xffffff9c);
                                goto addr_2f7b_40;
                            } else {
                                r12_39 = reinterpret_cast<void**>(".");
                                backup_type = reinterpret_cast<void**>(0);
                                set_simple_backup_suffix(0, rsi16, rdx17, rcx18);
                                rsp19 = rsp20 - 8 + 8;
                                v11 = reinterpret_cast<void**>(1);
                                v42 = reinterpret_cast<void**>(0xffffff9c);
                            }
                        }
                        addr_2d71_44:
                        rbp6 = rbx7;
                        v13 = r12_39;
                        rbx7 = v42;
                        *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0;
                        r15_3 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&r15_3 + 4) = 0;
                        r13d23 = 1;
                        r14_4 = reinterpret_cast<void**>(rsp19 + 40);
                        while (reinterpret_cast<signed char>(v11) > reinterpret_cast<signed char>(r15_3)) {
                            rdi53 = *reinterpret_cast<void***>(rbp6 + reinterpret_cast<unsigned char>(r15_3) * 8);
                            rax54 = last_component(rdi53, rsi16, rdx17, rcx18);
                            rax55 = file_name_concat(v13, rax54, r14_4, rcx18);
                            strip_trailing_slashes(v56, rax54, r14_4, rcx18);
                            rdi57 = *reinterpret_cast<void***>(rbp6 + reinterpret_cast<unsigned char>(r15_3) * 8);
                            r8_15 = reinterpret_cast<void**>(0xffffffff);
                            rsi16 = rbx7;
                            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                            rdx17 = v58;
                            rcx18 = rax55;
                            ++r15_3;
                            eax59 = do_link(rdi57, rsi16, rdx17, rcx18, 0xffffffff, "David MacKenzie");
                            r13d23 = r13d23 & eax59;
                            fun_2500(rax55, rsi16, rax55, rsi16);
                            rsp19 = rsp19 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                        }
                        continue;
                        addr_2e7d_51:
                        r12_39 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7 + reinterpret_cast<int32_t>(rbp6) * 8) - 8);
                        goto addr_2e85_34;
                    }
                }
            }
            addr_2f65_49:
            xalloc_die();
            rsp20 = rsp19 - 8 + 8;
        } else {
            v11 = reinterpret_cast<void**>(2);
            *reinterpret_cast<int32_t*>(&r12_39) = 0;
            *reinterpret_cast<int32_t*>(&r12_39 + 4) = 0;
            goto addr_2f7b_40;
        }
    }
}

int64_t __libc_start_main = 0;

void fun_3113() {
    __asm__("cli ");
    __libc_start_main(0x2a10, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x12008;

void fun_24e0(int64_t rdi);

int64_t fun_31b3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_24e0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_31f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

unsigned char __cxa_finalize;

unsigned char g1;

signed char g2;

int32_t fun_2530(void** rdi, void** rsi, void** rdx, ...);

void fun_3ca3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** r9_4;
    void** r8_5;
    void** r12_6;
    void** rax7;
    void** r12_8;
    void** rax9;
    void** r12_10;
    void** rax11;
    void** r12_12;
    void** rax13;
    void** r12_14;
    void** rax15;
    void** r12_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** r12_22;
    void** rax23;
    void** rax24;
    uint32_t ecx25;
    uint32_t ecx26;
    int1_t zf27;
    void** rax28;
    void** rax29;
    int32_t eax30;
    void** rax31;
    void** r13_32;
    void** rax33;
    void** rax34;
    int32_t eax35;
    void** rax36;
    void** r14_37;
    void** rax38;
    void** rax39;
    void** rax40;
    void** rdi41;
    void** r8_42;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_2620();
            r9_4 = r12_2;
            r8_5 = r12_2;
            fun_2880(1, rax3, r12_2, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_6 = stdout;
            rax7 = fun_2620();
            fun_2740(rax7, r12_6, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_8 = stdout;
            rax9 = fun_2620();
            fun_2740(rax9, r12_8, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_10 = stdout;
            rax11 = fun_2620();
            fun_2740(rax11, r12_10, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_12 = stdout;
            rax13 = fun_2620();
            fun_2740(rax13, r12_12, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_14 = stdout;
            rax15 = fun_2620();
            fun_2740(rax15, r12_14, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_16 = stdout;
            rax17 = fun_2620();
            fun_2740(rax17, r12_16, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_18 = stdout;
            rax19 = fun_2620();
            fun_2740(rax19, r12_18, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_20 = stdout;
            rax21 = fun_2620();
            fun_2740(rax21, r12_20, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_22 = stdout;
            rax23 = fun_2620();
            fun_2740(rax23, r12_22, 5, r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            rax24 = fun_2620();
            fun_2880(1, rax24, "-P", r12_2, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            do {
                if (1) 
                    break;
                ecx25 = __cxa_finalize;
            } while (0x6c != ecx25 || ((ecx26 = g1, 0x6e != ecx26) || (zf27 = g2 == 0, !zf27)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax28 = fun_2620();
                fun_2880(1, rax28, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                rax29 = fun_2870(5);
                if (!rax29 || (eax30 = fun_2530(rax29, "en_", 3, rax29, "en_", 3), !eax30)) {
                    rax31 = fun_2620();
                    r12_2 = reinterpret_cast<void**>("ln");
                    r13_32 = reinterpret_cast<void**>(" invocation");
                    fun_2880(1, rax31, "https://www.gnu.org/software/coreutils/", "ln", r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                } else {
                    r12_2 = reinterpret_cast<void**>("ln");
                    goto addr_40d3_9;
                }
            } else {
                rax33 = fun_2620();
                fun_2880(1, rax33, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                rax34 = fun_2870(5);
                if (!rax34 || (eax35 = fun_2530(rax34, "en_", 3, rax34, "en_", 3), !eax35)) {
                    addr_3fd6_11:
                    rax36 = fun_2620();
                    r13_32 = reinterpret_cast<void**>(" invocation");
                    fun_2880(1, rax36, "https://www.gnu.org/software/coreutils/", "ln", r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                    if (!reinterpret_cast<int1_t>(r12_2 == "ln")) {
                        r13_32 = reinterpret_cast<void**>(0xd6d8);
                    }
                } else {
                    addr_40d3_9:
                    r14_37 = stdout;
                    rax38 = fun_2620();
                    fun_2740(rax38, r14_37, 5, "https://www.gnu.org/software/coreutils/", r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                    goto addr_3fd6_11;
                }
            }
            rax39 = fun_2620();
            fun_2880(1, rax39, r12_2, r13_32, r8_5, r9_4, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            addr_3cf9_14:
            fun_2910();
        }
    } else {
        rax40 = fun_2620();
        rdi41 = stderr;
        fun_2930(rdi41, 1, rax40, r12_2, r8_42);
        goto addr_3cf9_14;
    }
}

void fun_4103(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_2860;
}

void fun_4183(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_2960;
}

int32_t try_tempname_len(void** rdi, ...);

void fun_2580(int64_t rdi, void** rsi);

int64_t fun_41a3(int64_t rdi, void** rsi, int32_t edx, void** rcx, int32_t r8d, int32_t r9d, void** a7) {
    int32_t r15d8;
    void* rsp9;
    int64_t rdx10;
    void** rdx11;
    void** eax12;
    void** eax13;
    void** r12d14;
    int64_t rax15;
    int64_t rax16;
    void** rax17;
    void** rax18;
    void** rax19;
    void** rax20;
    void** rsi21;
    int32_t eax22;
    int64_t rdx23;
    int64_t rdi24;
    int32_t eax25;
    void** rax26;
    int64_t rdi27;
    void** rax28;

    __asm__("cli ");
    r15d8 = r9d;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rdx10 = g28;
    *reinterpret_cast<int32_t*>(&rdx11) = 0;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    eax12 = a7;
    if (reinterpret_cast<signed char>(eax12) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&rdx11) = edx;
        *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
        eax13 = fun_2860(rdi, rsi, rdx11, rcx);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        r12d14 = eax13;
        if (!eax13) {
            addr_4273_3:
            rax15 = rdx10 - g28;
            if (rax15) {
                fun_2660();
            } else {
                *reinterpret_cast<void***>(&rax16) = r12d14;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                return rax16;
            }
        } else {
            rax17 = fun_2520();
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            eax12 = *reinterpret_cast<void***>(rax17);
        }
    }
    if (*reinterpret_cast<signed char*>(&r15d8) != 1 || !reinterpret_cast<int1_t>(eax12 == 17)) {
        r12d14 = eax12;
        goto addr_4273_3;
    } else {
        rax18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 48);
        rax19 = samedir_template(rcx, rax18, rdx11, rcx);
        if (!rax19) {
            rax20 = fun_2520();
            r12d14 = *reinterpret_cast<void***>(rax20);
            goto addr_4273_3;
        } else {
            *reinterpret_cast<int32_t*>(&rsi21) = 0;
            *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
            eax22 = try_tempname_len(rax19);
            if (!eax22) {
                *reinterpret_cast<int32_t*>(&rdx23) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi24) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
                eax25 = fun_2900(rdi24, rax19, rdx23, rcx, 6);
                r12d14 = reinterpret_cast<void**>(0xffffffff);
                if (eax25) {
                    rax26 = fun_2520();
                    r12d14 = *reinterpret_cast<void***>(rax26);
                }
                rsi21 = rax19;
                *reinterpret_cast<int32_t*>(&rdi27) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi27) + 4) = 0;
                fun_2580(rdi27, rsi21);
            } else {
                rax28 = fun_2520();
                r12d14 = *reinterpret_cast<void***>(rax28);
            }
            if (rax19 != rax18) {
                fun_2500(rax19, rsi21, rax19, rsi21);
                goto addr_4273_3;
            }
        }
    }
}

int64_t fun_4313(int64_t rdi, int32_t esi, void** rdx, void** rcx, void** r8d) {
    int32_t ebx6;
    void* rsp7;
    int64_t rax8;
    void** eax9;
    void** r12d10;
    int64_t rax11;
    int64_t rax12;
    void** rax13;
    void** rbx14;
    void** rax15;
    void** rax16;
    void** rsi17;
    int32_t eax18;
    int64_t rdx19;
    int64_t rdi20;
    int32_t eax21;
    void** rax22;
    int64_t rdi23;
    void** rax24;

    __asm__("cli ");
    ebx6 = *reinterpret_cast<int32_t*>(&rcx);
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128);
    rax8 = g28;
    if (reinterpret_cast<signed char>(r8d) < reinterpret_cast<signed char>(0)) {
        eax9 = fun_2960();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        r12d10 = eax9;
        if (!eax9) {
            addr_43b3_3:
            rax11 = rax8 - g28;
            if (rax11) {
                fun_2660();
            } else {
                *reinterpret_cast<void***>(&rax12) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                return rax12;
            }
        } else {
            rax13 = fun_2520();
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            r8d = *reinterpret_cast<void***>(rax13);
        }
    }
    if (*reinterpret_cast<signed char*>(&ebx6) != 1 || !reinterpret_cast<int1_t>(r8d == 17)) {
        r12d10 = r8d;
        goto addr_43b3_3;
    } else {
        rbx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
        rax15 = samedir_template(rdx, rbx14, rdx, rcx);
        if (!rax15) {
            rax16 = fun_2520();
            r12d10 = *reinterpret_cast<void***>(rax16);
            goto addr_43b3_3;
        } else {
            *reinterpret_cast<int32_t*>(&rsi17) = 0;
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            eax18 = try_tempname_len(rax15, rax15);
            if (!eax18) {
                *reinterpret_cast<int32_t*>(&rdx19) = esi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
                rsi17 = rax15;
                *reinterpret_cast<int32_t*>(&rdi20) = esi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                eax21 = fun_2900(rdi20, rsi17, rdx19, rdx, 6);
                r12d10 = reinterpret_cast<void**>(0xffffffff);
                if (eax21) {
                    rax22 = fun_2520();
                    rsi17 = rax15;
                    *reinterpret_cast<int32_t*>(&rdi23) = esi;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
                    r12d10 = *reinterpret_cast<void***>(rax22);
                    fun_2580(rdi23, rsi17);
                }
            } else {
                rax24 = fun_2520();
                r12d10 = *reinterpret_cast<void***>(rax24);
            }
            if (rax15 != rbx14) {
                fun_2500(rax15, rsi17, rax15, rsi17);
                goto addr_43b3_3;
            }
        }
    }
}

struct s5 {
    unsigned char f0;
    signed char f1;
};

struct s6 {
    unsigned char f0;
    unsigned char f1;
};

struct s7 {
    unsigned char f0;
    unsigned char f1;
};

int64_t fun_44d3(struct s5* rdi, struct s6* rsi, int64_t rdx, void** rcx) {
    int64_t v5;
    int64_t r12_6;
    int64_t v7;
    int64_t rbp8;
    int64_t v9;
    int64_t rbx10;
    void** rsp11;
    uint1_t zf12;
    void** v13;
    uint1_t zf14;
    int64_t v15;
    int64_t rax16;
    uint32_t edx17;
    struct s6* rbx18;
    struct s5* r10_19;
    struct s5* rax20;
    int32_t r8d21;
    struct s6* rdi22;
    int32_t esi23;
    void** rcx24;
    void** r9_25;
    void** r8_26;
    struct s7* rbx27;
    signed char* rbp28;
    uint32_t eax29;
    void* rdx30;
    void** rbp31;
    void** r14_32;
    void*** r13_33;
    int64_t v34;
    uint32_t eax35;
    uint32_t r12d36;
    uint32_t eax37;
    int64_t v38;
    uint32_t eax39;
    int64_t v40;
    uint32_t eax41;
    int64_t v42;
    uint32_t eax43;
    int64_t v44;
    uint32_t eax45;
    uint32_t eax46;

    __asm__("cli ");
    v5 = r12_6;
    v7 = rbp8;
    v9 = rbx10;
    rsp11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    zf12 = reinterpret_cast<uint1_t>(rsi->f1 == 47);
    v13 = rcx;
    zf14 = reinterpret_cast<uint1_t>(rdi->f1 == 47);
    v15 = rdx;
    *reinterpret_cast<uint32_t*>(&rax16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    if (static_cast<unsigned char>(zf12) != static_cast<unsigned char>(zf14) || (edx17 = rsi->f0, rbx18 = rsi, *reinterpret_cast<signed char*>(&edx17) == 0)) {
        addr_45c7_2:
        return rax16;
    } else {
        r10_19 = rdi;
        rax20 = rdi;
        r8d21 = 0;
        rdi22 = rsi;
        esi23 = 0;
        do {
            *reinterpret_cast<uint32_t*>(&rcx24) = rax20->f0;
            *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
            *reinterpret_cast<unsigned char*>(&r9_25) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rcx24) == 0);
            if (*reinterpret_cast<signed char*>(&rcx24) != *reinterpret_cast<signed char*>(&edx17)) 
                break;
            if (*reinterpret_cast<unsigned char*>(&r9_25)) 
                break;
            ++esi23;
            if (*reinterpret_cast<signed char*>(&edx17) == 47) {
                r8d21 = esi23;
            }
            edx17 = rdi22->f1;
            rdi22 = reinterpret_cast<struct s6*>(&rdi22->f1);
            rax20 = reinterpret_cast<struct s5*>(&rax20->f1);
        } while (*reinterpret_cast<signed char*>(&edx17));
        goto addr_45e0_9;
    }
    if (*reinterpret_cast<signed char*>(&edx17) == 47 && *reinterpret_cast<unsigned char*>(&r9_25)) {
        r8d21 = esi23;
    }
    if (!r8d21) {
        *reinterpret_cast<uint32_t*>(&rax16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        goto addr_45c7_2;
    } else {
        addr_456d_14:
        r8_26 = reinterpret_cast<void**>(static_cast<int64_t>(r8d21));
        rbx27 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(rbx18) + reinterpret_cast<unsigned char>(r8_26));
        rbp28 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r10_19) + reinterpret_cast<unsigned char>(r8_26));
        eax29 = rbx27->f0;
        if (*reinterpret_cast<signed char*>(&eax29) == 47) {
            eax29 = rbx27->f1;
            rbx27 = reinterpret_cast<struct s7*>(&rbx27->f1);
        }
    }
    *reinterpret_cast<int32_t*>(&rdx30) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx30) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rdx30) = reinterpret_cast<uint1_t>(*rbp28 == 47);
    rbp31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp28) + reinterpret_cast<uint64_t>(rdx30));
    if (*reinterpret_cast<signed char*>(&eax29)) {
        r14_32 = rsp11;
        r13_33 = reinterpret_cast<void***>(rsp11 + 8);
        eax35 = buffer_or_output("..", r13_33, r14_32, rcx24, r8_26, r9_25, v13, v15, v34, v9, v7, v5);
        r12d36 = eax35;
        eax37 = rbx27->f0;
        if (!*reinterpret_cast<signed char*>(&eax37)) {
            addr_4690_18:
            if (*reinterpret_cast<void***>(rbp31)) {
                eax39 = buffer_or_output("/", r13_33, r14_32, rcx24, r8_26, r9_25, v13, v15, v38, v9, v7, v5);
                eax41 = buffer_or_output(rbp31, r13_33, r14_32, rcx24, r8_26, r9_25, v13, v15, v40, v9, v7, v5);
                r12d36 = r12d36 | (eax39 | eax41);
            }
        } else {
            while (1) {
                if (*reinterpret_cast<signed char*>(&eax37) != 47) {
                    eax37 = rbx27->f1;
                    rbx27 = reinterpret_cast<struct s7*>(&rbx27->f1);
                    if (!*reinterpret_cast<signed char*>(&eax37)) 
                        goto addr_4690_18;
                } else {
                    rbx27 = reinterpret_cast<struct s7*>(&rbx27->f1);
                    eax43 = buffer_or_output("/..", r13_33, r14_32, rcx24, r8_26, r9_25, v13, v15, v42, v9, v7, v5);
                    r12d36 = r12d36 | eax43;
                    eax37 = rbx27->f0;
                    if (!*reinterpret_cast<signed char*>(&eax37)) 
                        goto addr_468c_24;
                }
            }
        }
    } else {
        if (!*reinterpret_cast<void***>(rbp31)) {
            rbp31 = reinterpret_cast<void**>(".");
        }
        eax45 = buffer_or_output(rbp31, rsp11 + 8, rsp11, rcx24, r8_26, r9_25, v13, v15, v44, v9, v7, v5);
        r12d36 = eax45;
    }
    if (*reinterpret_cast<signed char*>(&r12d36)) {
        fun_2620();
        fun_28b0();
    }
    *reinterpret_cast<uint32_t*>(&rax16) = r12d36 ^ 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    goto addr_45c7_2;
    addr_468c_24:
    goto addr_4690_18;
    addr_45e0_9:
    eax46 = rax20->f0;
    if (!*reinterpret_cast<signed char*>(&eax46) || *reinterpret_cast<signed char*>(&eax46) == 47) {
        r8d21 = esi23;
        goto addr_456d_14;
    }
}

void** fun_24f0(int64_t rdi);

void** simple_backup_suffix = reinterpret_cast<void**>(0);

void fun_46d3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rbx5 = rdi;
    if (!rdi) {
        rax6 = fun_24f0("SIMPLE_BACKUP_SUFFIX");
        rbx5 = rax6;
        if (!rax6) {
            simple_backup_suffix = reinterpret_cast<void**>("~");
            return;
        }
    }
    if (*reinterpret_cast<void***>(rbx5) && (rax7 = last_component(rbx5, rsi, rdx, rcx), rbx5 == rax7)) {
        simple_backup_suffix = rbx5;
        return;
    }
}

void** base_len(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t opendirat(int64_t rdi, void** rsi);

void fun_26f0(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t renameatu();

void* fun_25f0(void** rdi, int64_t rsi);

void* fun_27a0();

struct s8 {
    signed char[19] pad19;
    void** f13;
};

struct s8* fun_27f0(int64_t rdi, void** rsi, ...);

uint32_t fun_2730(void** rdi, void** rsi, void** rdx, ...);

void** fun_2850(void** rdi, void** rsi);

struct s9 {
    int16_t f0;
    signed char f2;
};

void fun_2710(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** fun_4733(uint32_t edi, void** rsi, void** rdx, void** rcx) {
    uint32_t v5;
    void** v6;
    int32_t v7;
    unsigned char v8;
    int64_t rax9;
    int64_t v10;
    void** rax11;
    void** v12;
    void** rax13;
    void* rsp14;
    void* r14_15;
    void** rdi16;
    void** v17;
    void** v18;
    void** rax19;
    void** rax20;
    void** rax21;
    void** rax22;
    void** v23;
    void** rax24;
    void** rax25;
    void** v26;
    void** rax27;
    void* rsp28;
    void** r12_29;
    int64_t rax30;
    void** v31;
    int64_t rbp32;
    uint32_t v33;
    void** rbx34;
    void** rdx35;
    void** rsi36;
    void* rsp37;
    void** rdi38;
    uint32_t r15d39;
    void** r8_40;
    uint16_t* r15_41;
    int64_t rdi42;
    uint32_t r13d43;
    int64_t rax44;
    void* rsp45;
    void** rax46;
    unsigned char al47;
    uint32_t eax48;
    uint32_t v49;
    void** rdi50;
    int32_t eax51;
    void** rax52;
    void** rax53;
    void** rax54;
    void** rax55;
    void* rsp56;
    uint32_t r8d57;
    void* rax58;
    uint32_t r8d59;
    void* rax60;
    void** rax61;
    void** v62;
    void** v63;
    struct s8* rax64;
    void** r13_65;
    void** rax66;
    void** rdi67;
    void** r15_68;
    uint32_t eax69;
    void** r13_70;
    int64_t rax71;
    uint32_t eax72;
    uint32_t eax73;
    void** r15_74;
    uint32_t eax75;
    unsigned char* r9_76;
    uint32_t eax77;
    unsigned char* r9_78;
    void** rdx79;
    void** rax80;
    void** rax81;
    void** r8_82;
    uint64_t rsi83;
    void** rax84;
    struct s9* rax85;
    void** rax86;
    void* rsp87;
    void* rdi88;
    uint32_t edx89;
    unsigned char* rax90;
    void** rax91;
    void** r15d92;
    void** rdx93;
    void** rdi94;
    void** rdi95;
    void** rax96;

    __asm__("cli ");
    v5 = edi;
    v6 = rsi;
    v7 = *reinterpret_cast<int32_t*>(&rdx);
    v8 = *reinterpret_cast<unsigned char*>(&rcx);
    rax9 = g28;
    v10 = rax9;
    rax11 = last_component(rsi, rsi, rdx, rcx);
    v12 = rax11;
    rax13 = base_len(rax11, rsi, rdx, rcx);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8);
    r14_15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(rsi));
    rdi16 = simple_backup_suffix;
    v17 = rax13;
    v18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(r14_15));
    if (!rdi16) {
        rax19 = fun_24f0("SIMPLE_BACKUP_SUFFIX");
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
        rdi16 = reinterpret_cast<void**>("~");
        if (rax19 && (*reinterpret_cast<void***>(rax19) && (rax20 = last_component(rax19, rsi, rdx, rcx), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8), rdi16 = rax20, rax19 != rax20))) {
            rdi16 = reinterpret_cast<void**>("~");
        }
        simple_backup_suffix = rdi16;
    }
    rax21 = fun_2640(rdi16);
    rax22 = rax21 + 1;
    v23 = rax22;
    *reinterpret_cast<int32_t*>(&rax24) = 9;
    *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
    if (reinterpret_cast<signed char>(rax22) >= reinterpret_cast<signed char>(9)) {
        rax24 = rax22;
    }
    rax25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(rax24) + 1);
    v26 = rax25;
    rax27 = fun_2800(rax25, v18, rdx);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8);
    r12_29 = rax27;
    if (!rax27) {
        addr_4aa0_8:
        rax30 = v10 - g28;
        if (rax30) {
            fun_2660();
        } else {
            return r12_29;
        }
    } else {
        v31 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rbp32) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp32) + 4) = 0;
        v33 = v5;
        rbx34 = v17 + 4;
        do {
            rdx35 = v18;
            rsi36 = v6;
            fun_27d0(r12_29, rsi36, rdx35, r12_29, rsi36, rdx35);
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            if (v7 == 1) {
                rdx35 = v23;
                rsi36 = simple_backup_suffix;
                rdi38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<unsigned char>(v18));
                fun_27d0(rdi38, rsi36, rdx35, rdi38, rsi36, rdx35);
                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                if (!v8) 
                    break;
                rsi36 = v6;
                r15d39 = v8;
                rcx = r12_29;
                *reinterpret_cast<uint32_t*>(&r8_40) = 0;
            } else {
                if (!rbp32) {
                    r15_41 = reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                    *reinterpret_cast<uint32_t*>(&rdi42) = v5;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi42) + 4) = 0;
                    r13d43 = *r15_41;
                    *r15_41 = 46;
                    rcx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x84);
                    rsi36 = r12_29;
                    rax44 = opendirat(rdi42, rsi36);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                    rbp32 = rax44;
                    rdx35 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r15_41) + reinterpret_cast<unsigned char>(v17));
                    if (!rbp32) {
                        rax46 = fun_2520();
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rdx35 = rdx35;
                        *r15_41 = *reinterpret_cast<uint16_t*>(&r13d43);
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax46) == 12);
                        *reinterpret_cast<void***>(rdx35) = reinterpret_cast<void**>(0x7e317e2e);
                        *reinterpret_cast<signed char*>(rdx35 + 4) = 0;
                        eax48 = al47 + 2;
                        v49 = eax48;
                        if (eax48 == 2) {
                            addr_4b60_18:
                            if (v7 == 2) {
                                rdx35 = v23;
                                rsi36 = simple_backup_suffix;
                                rdi50 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<unsigned char>(v18));
                                fun_27d0(rdi50, rsi36, rdx35, rdi50, rsi36, rdx35);
                                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                v7 = 1;
                                goto addr_4b6b_20;
                            }
                        } else {
                            if (v49 == 3) 
                                goto addr_4cdc_22;
                            goto addr_4a0b_24;
                        }
                    } else {
                        *r15_41 = *reinterpret_cast<uint16_t*>(&r13d43);
                        *reinterpret_cast<void***>(rdx35) = reinterpret_cast<void**>(0x7e317e2e);
                        *reinterpret_cast<signed char*>(rdx35 + 4) = 0;
                        goto addr_4831_26;
                    }
                } else {
                    fun_26f0(rbp32, rsi36, rdx35, rcx);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                    goto addr_4831_26;
                }
            }
            addr_4a36_28:
            *reinterpret_cast<uint32_t*>(&rdx35) = v33;
            *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
            eax51 = renameatu();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            if (!eax51) 
                break; else 
                continue;
            addr_4b6b_20:
            r15d39 = 1;
            rax52 = last_component(r12_29, rsi36, rdx35, rcx);
            rax53 = base_len(rax52, rsi36, rdx35, rcx);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8);
            rcx = rax53;
            if (reinterpret_cast<signed char>(rax53) > reinterpret_cast<signed char>(14)) {
                rdx35 = rax52;
                if (v31) {
                    addr_4c95_30:
                    if (reinterpret_cast<signed char>(rcx) <= reinterpret_cast<signed char>(v31)) {
                        r15d39 = 1;
                        goto addr_4b93_32;
                    } else {
                        addr_4ca0_33:
                        rsi36 = v31;
                        rcx = rsi36 + 0xffffffffffffffff;
                        rax54 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r12_29)) - reinterpret_cast<unsigned char>(rdx35));
                        if (reinterpret_cast<signed char>(rax54) >= reinterpret_cast<signed char>(rsi36)) {
                            rax54 = rcx;
                        }
                    }
                } else {
                    rax55 = fun_2520();
                    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (reinterpret_cast<int32_t>(v33) < reinterpret_cast<int32_t>(0)) {
                        r8d57 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rax52));
                        *reinterpret_cast<void***>(rax52) = reinterpret_cast<void**>(46);
                        *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(0);
                        rax58 = fun_25f0(r12_29, 3);
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                        rdx35 = rax52;
                        r8d59 = r8d57;
                        rcx = rax53;
                        v31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax58) - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax58) < static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax55)) < reinterpret_cast<unsigned char>(1)))));
                        *reinterpret_cast<void***>(rdx35) = *reinterpret_cast<void***>(&r8d59);
                    } else {
                        *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(0);
                        rax60 = fun_27a0();
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                        rcx = rax53;
                        rdx35 = rax52;
                        v31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax60) - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax60) < static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax55)) < reinterpret_cast<unsigned char>(1)))));
                    }
                    rsi36 = v31;
                    if (reinterpret_cast<signed char>(rsi36) < reinterpret_cast<signed char>(0)) 
                        goto addr_4cf7_39; else 
                        goto addr_4c95_30;
                }
            } else {
                addr_4b93_32:
                if (!v8) 
                    break; else 
                    goto addr_4b9e_40;
            }
            r15d39 = 0;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx35) + reinterpret_cast<unsigned char>(rax54)) = 0x7e;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx35) + reinterpret_cast<unsigned char>(rax54) + 1) = 0;
            goto addr_4b93_32;
            addr_4cf7_39:
            *reinterpret_cast<int32_t*>(&rax61) = 14;
            *reinterpret_cast<int32_t*>(&rax61 + 4) = 0;
            if (rsi36 == 0xffffffffffffffff) {
                rax61 = rsi36;
            }
            v31 = rax61;
            goto addr_4ca0_33;
            addr_4b9e_40:
            if (v7 != 1) {
                addr_4a27_44:
                rsi36 = v12;
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                *reinterpret_cast<uint32_t*>(&r8_40) = 1;
                goto addr_4a36_28;
            } else {
                rsi36 = v6;
                rcx = r12_29;
                *reinterpret_cast<uint32_t*>(&r8_40) = 0;
                goto addr_4a36_28;
            }
            addr_4a0b_24:
            if (v49 != 1) {
                if (!v8) 
                    break;
                r15d39 = v8;
                goto addr_4a27_44;
            }
            addr_4831_26:
            v49 = 2;
            v62 = reinterpret_cast<void**>(1);
            v63 = v26;
            addr_4850_48:
            while (rax64 = fun_27f0(rbp32, rsi36, rbp32, rsi36), rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), !!rax64) {
                do {
                    r13_65 = reinterpret_cast<void**>(&rax64->f13);
                    rax66 = fun_2640(r13_65, r13_65);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (reinterpret_cast<unsigned char>(rax66) < reinterpret_cast<unsigned char>(rbx34)) 
                        goto addr_4850_48;
                    rdi67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                    rsi36 = r13_65;
                    r15_68 = v17 + 2;
                    rdx35 = r15_68;
                    eax69 = fun_2730(rdi67, rsi36, rdx35, rdi67, rsi36, rdx35);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    if (eax69) 
                        goto addr_4850_48;
                    r13_70 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_65) + reinterpret_cast<unsigned char>(r15_68));
                    *reinterpret_cast<uint32_t*>(&rax71) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_70));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax71) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rdx35) = static_cast<uint32_t>(rax71 - 49);
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&rdx35) > 8) 
                        goto addr_4850_48;
                    eax72 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_70 + 1))));
                    *reinterpret_cast<unsigned char*>(&r8_40) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax71) == 57);
                    *reinterpret_cast<uint32_t*>(&rdx35) = eax72;
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    eax73 = eax72 - 48;
                    if (eax73 > 9) {
                        *reinterpret_cast<int32_t*>(&rcx) = 1;
                        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r15_74) = 1;
                        *reinterpret_cast<int32_t*>(&r15_74 + 4) = 0;
                    } else {
                        *reinterpret_cast<int32_t*>(&r15_74) = 1;
                        *reinterpret_cast<int32_t*>(&r15_74 + 4) = 0;
                        do {
                            *reinterpret_cast<unsigned char*>(&eax73) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rdx35) == 57);
                            ++r15_74;
                            *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<uint32_t*>(&r8_40) & eax73;
                            eax75 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_70) + reinterpret_cast<unsigned char>(r15_74))));
                            rcx = r15_74;
                            *reinterpret_cast<uint32_t*>(&rdx35) = eax75;
                            *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                            eax73 = eax75 - 48;
                        } while (eax73 <= 9);
                    }
                    if (*reinterpret_cast<unsigned char*>(&rdx35) != 0x7e) 
                        goto addr_4850_48;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_70) + reinterpret_cast<unsigned char>(rcx) + 1)) 
                        goto addr_4850_48;
                    if (reinterpret_cast<signed char>(v62) >= reinterpret_cast<signed char>(r15_74)) {
                        if (v62 != r15_74) 
                            goto addr_4850_48;
                        rdx35 = rcx;
                        rsi36 = r13_70;
                        r9_76 = reinterpret_cast<unsigned char*>(v18 + 2);
                        eax77 = fun_2730(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r9_76), rsi36, rdx35);
                        rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rcx = rcx;
                        r9_78 = r9_76;
                        *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<unsigned char*>(&r8_40);
                        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax77) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax77 == 0))) 
                            break;
                    } else {
                        r9_78 = reinterpret_cast<unsigned char*>(v18 + 2);
                    }
                    *reinterpret_cast<uint32_t*>(&rdx79) = *reinterpret_cast<unsigned char*>(&r8_40);
                    *reinterpret_cast<int32_t*>(&rdx79 + 4) = 0;
                    rax80 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx79) + reinterpret_cast<unsigned char>(r15_74));
                    v49 = *reinterpret_cast<unsigned char*>(&r8_40);
                    v62 = rax80;
                    rax81 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax80) + reinterpret_cast<uint64_t>(r9_78) + 2);
                    if (reinterpret_cast<signed char>(rax81) <= reinterpret_cast<signed char>(v63)) {
                        r8_82 = r12_29;
                    } else {
                        if (__intrinsic()) {
                            v63 = rax81;
                        } else {
                            v63 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rax81) >> 1) + reinterpret_cast<unsigned char>(rax81));
                        }
                        *reinterpret_cast<int32_t*>(&rsi83) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi83) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rsi83) = reinterpret_cast<uint1_t>(v63 == 0);
                        rsi36 = reinterpret_cast<void**>(rsi83 | reinterpret_cast<unsigned char>(v63));
                        rax84 = fun_2850(r12_29, rsi36);
                        rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rcx = rcx;
                        rdx79 = rdx79;
                        r8_82 = rax84;
                        if (!rax84) 
                            goto addr_4cd4_68;
                    }
                    rsi36 = r13_70;
                    rax85 = reinterpret_cast<struct s9*>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r8_82));
                    rax85->f0 = 0x7e2e;
                    rax85->f2 = 48;
                    rax86 = fun_27d0(reinterpret_cast<uint64_t>(rax85) + reinterpret_cast<unsigned char>(rdx79) + 2, rsi36, r15_74 + 2);
                    rsp87 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    rcx = rcx;
                    r8_40 = r8_82;
                    rdi88 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax86) + reinterpret_cast<unsigned char>(rcx));
                    edx89 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi88) + 0xffffffffffffffff);
                    rax90 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi88) + 0xffffffffffffffff);
                    if (*reinterpret_cast<signed char*>(&edx89) == 57) {
                        do {
                            *rax90 = 48;
                            edx89 = *(rax90 - 1);
                            --rax90;
                        } while (*reinterpret_cast<signed char*>(&edx89) == 57);
                    }
                    *reinterpret_cast<uint32_t*>(&rdx35) = edx89 + 1;
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    r12_29 = r8_40;
                    *rax90 = *reinterpret_cast<unsigned char*>(&rdx35);
                    rax64 = fun_27f0(rbp32, rsi36);
                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp87) - 8 + 8);
                } while (rax64);
                goto addr_49f9_73;
            }
            addr_4a00_75:
            if (v49 == 2) 
                goto addr_4b60_18; else 
                goto addr_4a0b_24;
            addr_49f9_73:
            goto addr_4a00_75;
            rax91 = fun_2520();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
        } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax91) == 17) && *reinterpret_cast<signed char*>(&r15d39) == 1);
        goto addr_4a62_77;
    }
    if (rbp32) {
        fun_2710(rbp32, rsi36, rdx35, rcx);
        goto addr_4aa0_8;
    }
    addr_4a62_77:
    r15d92 = *reinterpret_cast<void***>(rax91);
    rdx93 = rax91;
    if (rbp32) {
        fun_2710(rbp32, rsi36, rdx93, rcx);
        rdx93 = rax91;
    }
    rdi94 = r12_29;
    *reinterpret_cast<int32_t*>(&r12_29) = 0;
    *reinterpret_cast<int32_t*>(&r12_29 + 4) = 0;
    fun_2500(rdi94, rsi36);
    *reinterpret_cast<void***>(rdx93) = r15d92;
    goto addr_4aa0_8;
    addr_4cdc_22:
    rdi95 = r12_29;
    *reinterpret_cast<int32_t*>(&r12_29) = 0;
    *reinterpret_cast<int32_t*>(&r12_29 + 4) = 0;
    fun_2500(rdi95, rsi36);
    rax96 = fun_2520();
    *reinterpret_cast<void***>(rax96) = reinterpret_cast<void**>(12);
    goto addr_4aa0_8;
    addr_4cd4_68:
    fun_2710(rbp32, rsi36, rdx79, rcx);
    goto addr_4cdc_22;
}

int64_t backupfile_internal();

void fun_4e43() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = backupfile_internal();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t __xargmatch_internal(int64_t rdi);

int64_t fun_4e63(int64_t rdi, signed char* rsi) {
    int64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    if (!rsi) {
        return 2;
    } else {
        if (*rsi) {
            rax3 = __xargmatch_internal(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = *reinterpret_cast<int32_t*>(0xd120 + rax3 * 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
            return rax4;
        } else {
            return 2;
        }
    }
}

int64_t fun_4ec3(int64_t rdi, signed char* rsi) {
    void** rax3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!rsi || !*rsi) {
        rax3 = fun_24f0("VERSION_CONTROL");
        if (!rax3 || !*reinterpret_cast<void***>(rax3)) {
            return 2;
        } else {
            rax4 = __xargmatch_internal("$VERSION_CONTROL");
            *reinterpret_cast<int32_t*>(&rax5) = *reinterpret_cast<int32_t*>(0xd120 + rax4 * 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
            return rax5;
        }
    } else {
        rax6 = __xargmatch_internal(rdi);
        *reinterpret_cast<int32_t*>(&rax7) = *reinterpret_cast<int32_t*>(0xd120 + rax6 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

struct s10 {
    unsigned char f0;
    unsigned char f1;
};

struct s10* fun_4f63(struct s10* rdi) {
    uint32_t edx2;
    struct s10* rax3;
    struct s10* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s10*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s10*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s10*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_4fc3(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2640(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

void** fun_5853(void** rdi, uint32_t esi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    rax3 = g28;
    rax4 = canonicalize_filename_mode_stk(rdi, esi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x428);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_2660();
    } else {
        return rax4;
    }
}

int64_t file_name = 0;

void fun_58a3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

void** stdin = reinterpret_cast<void**>(0);

int64_t freadahead(void** rdi);

int32_t rpl_fseeko(void** rdi);

int32_t rpl_fflush(void** rdi);

int32_t close_stream(void** rdi);

void close_stdout();

int32_t exit_failure = 1;

void** fun_2540(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void** quotearg_colon(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_58b3() {
    void** rbp1;
    int64_t rax2;
    int32_t eax3;
    void** rdi4;
    int32_t eax5;
    int32_t eax6;
    void** rax7;
    int64_t r13_8;
    void** r12_9;
    void** rax10;
    int64_t rsi11;
    void** rcx12;
    int64_t rdx13;
    int64_t rdi14;
    void** r8_15;
    void** rax16;
    int32_t eax17;

    __asm__("cli ");
    rbp1 = stdin;
    rax2 = freadahead(rbp1);
    if (rax2) {
        eax3 = rpl_fseeko(rbp1);
        rdi4 = stdin;
        if (eax3 || (eax5 = rpl_fflush(rdi4), rdi4 = stdin, eax5 == 0)) {
            eax6 = close_stream(rdi4);
            if (!eax6) {
                addr_58d9_4:
                goto close_stdout;
            } else {
                addr_590f_5:
                rax7 = fun_2620();
                r13_8 = file_name;
                r12_9 = rax7;
                rax10 = fun_2520();
                if (!r13_8) {
                    while (1) {
                        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                        rcx12 = r12_9;
                        rdx13 = reinterpret_cast<int64_t>("%s");
                        fun_28b0();
                        close_stdout();
                        addr_595f_7:
                        *reinterpret_cast<int32_t*>(&rdi14) = exit_failure;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                        rax10 = fun_2540(rdi14, rsi11, rdx13, rcx12, r8_15);
                    }
                } else {
                    rax16 = quotearg_colon(r13_8, "error closing file", 5);
                    *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                    r8_15 = r12_9;
                    rcx12 = rax16;
                    rdx13 = reinterpret_cast<int64_t>("%s: %s");
                    fun_28b0();
                    close_stdout();
                    goto addr_595f_7;
                }
            }
        } else {
            close_stream(rdi4);
            goto addr_590f_5;
        }
    } else {
        eax17 = close_stream(rbp1);
        if (eax17) 
            goto addr_590f_5; else 
            goto addr_58d9_4;
    }
}

int64_t file_name = 0;

void fun_59b3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_59c3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

void fun_59d3() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2520(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2620();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_5a63_5;
        rax10 = quotearg_colon(rdi9, "write error", 5);
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_28b0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2540(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_5a63_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_28b0();
    }
}

int64_t mdir_name();

void fun_5a83() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mdir_name();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5aa3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void* rbp5;
    void** rbx6;
    void** rax7;
    void* rax8;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp5) + 4) = 0;
    rbx6 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp5) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax7 = last_component(rdi, rsi, rdx, rcx);
    rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(rbx6));
    while (reinterpret_cast<uint64_t>(rax8) > reinterpret_cast<uint64_t>(rbp5) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(rax8) + 0xffffffffffffffff) == 47) {
        rax8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax8) + 0xffffffffffffffff);
    }
    return;
}

void** fun_5ae3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    void** rbx6;
    void** rax7;
    void** r12_8;
    void* rax9;
    uint32_t ebx10;
    void** rax11;
    void** r8_12;
    void** rax13;
    void** rax14;
    void** rax15;

    __asm__("cli ");
    rbp5 = rdi;
    *reinterpret_cast<int32_t*>(&rbx6) = 0;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx6) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax7 = last_component(rdi, rsi, rdx, rcx);
    r12_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(rbp5));
    while (reinterpret_cast<unsigned char>(rbx6) < reinterpret_cast<unsigned char>(r12_8)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r12_8) + 0xffffffffffffffff) != 47) 
            goto addr_5b60_4;
        --r12_8;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_8) ^ 1);
    ebx10 = *reinterpret_cast<uint32_t*>(&rax9) & 1;
    rax11 = fun_2800(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<uint64_t>(rax9) + 1, rsi, rdx);
    if (!rax11) {
        addr_5b8d_7:
        *reinterpret_cast<int32_t*>(&r8_12) = 0;
        *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
    } else {
        rax13 = fun_27d0(rax11, rbp5, r12_8);
        r8_12 = rax13;
        if (!*reinterpret_cast<signed char*>(&ebx10)) {
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_5b4e_10;
        } else {
            *reinterpret_cast<void***>(rax13) = reinterpret_cast<void**>(46);
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_5b4e_10;
        }
    }
    addr_5b53_12:
    return r8_12;
    addr_5b4e_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_12) + reinterpret_cast<unsigned char>(r12_8)) = 0;
    goto addr_5b53_12;
    addr_5b60_4:
    rax14 = fun_2800(r12_8 + 1, rsi, rdx);
    if (!rax14) 
        goto addr_5b8d_7;
    rax15 = fun_27d0(rax14, rbp5, r12_8);
    r8_12 = rax15;
    goto addr_5b4e_10;
}

unsigned char fun_5ba3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    void** rbx6;
    void** rax7;
    signed char* rbx8;
    int1_t zf9;

    __asm__("cli ");
    rax5 = last_component(rdi, rsi, rdx, rcx);
    rbx6 = rax5;
    if (!*reinterpret_cast<void***>(rax5)) {
        rbx6 = rdi;
    }
    rax7 = base_len(rbx6, rsi, rdx, rcx);
    rbx8 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<unsigned char>(rax7));
    zf9 = *rbx8 == 0;
    *rbx8 = 0;
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!zf9));
}

int32_t fun_2840(void** rdi);

void fun_5be3(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2840(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

void** hash_insert(int64_t rdi, void** rsi);

void fun_5c33(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        rax6 = xmalloc(24, rsi, rdx, rcx, r8);
        rax7 = xstrdup(rsi, rsi, rdx, rcx);
        *reinterpret_cast<void***>(rax6) = rax7;
        *reinterpret_cast<void***>(rax6 + 8) = *reinterpret_cast<void***>(rdx + 8);
        *reinterpret_cast<void***>(rax6 + 16) = *reinterpret_cast<void***>(rdx);
        rax8 = hash_insert(rdi, rax6);
        if (!rax8) {
            xalloc_die();
        } else {
            if (rax6 == rax8) {
                return;
            }
        }
    }
}

int64_t hash_lookup();

int64_t fun_5cc3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    int64_t rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax4 = g28;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (rdi) {
        rax5 = hash_lookup();
        *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(!!rax5);
    }
    rdx6 = rax4 - g28;
    if (rdx6) {
        fun_2660();
    } else {
        return rax5;
    }
}

int64_t mfile_name_concat();

void fun_5d23() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mfile_name_concat();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_5d43(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    void** rax6;
    void** r14_7;
    void** rax8;
    void* rbx9;
    uint1_t zf10;
    int32_t eax11;
    unsigned char v12;
    int1_t zf13;
    int32_t eax14;
    void** rax15;
    void** rax16;
    uint32_t ecx17;
    void** rdi18;
    void** rax19;

    __asm__("cli ");
    rax5 = last_component(rdi, rsi, rdx, rcx);
    rax6 = base_len(rax5, rsi, rdx, rcx);
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(rdi)) + reinterpret_cast<unsigned char>(rax6));
    rax8 = fun_2640(rsi);
    if (!rax6) {
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        zf10 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rsi) == 47);
        eax11 = 46;
        if (!zf10) {
            eax11 = 0;
        }
        *reinterpret_cast<unsigned char*>(&rbx9) = zf10;
        v12 = *reinterpret_cast<unsigned char*>(&eax11);
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_7) + 0xffffffffffffffff) == 47) {
            v12 = 0;
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
            zf13 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 47);
            eax14 = 47;
            if (zf13) {
                eax14 = 0;
            }
            *reinterpret_cast<unsigned char*>(&rbx9) = reinterpret_cast<uint1_t>(!zf13);
            v12 = *reinterpret_cast<unsigned char*>(&eax14);
        }
    }
    rax15 = fun_2800(reinterpret_cast<unsigned char>(r14_7) + reinterpret_cast<unsigned char>(rax8) + 1 + reinterpret_cast<uint64_t>(rbx9), rsi, rdx);
    if (rax15) {
        rax16 = fun_2890(rax15, rdi, r14_7);
        ecx17 = v12;
        rdi18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax16) + reinterpret_cast<uint64_t>(rbx9));
        *reinterpret_cast<void***>(rax16) = *reinterpret_cast<void***>(&ecx17);
        if (rdx) {
            *reinterpret_cast<void***>(rdx) = rdi18;
        }
        rax19 = fun_2890(rdi18, rsi, rax8);
        *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    }
    return rax15;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    uint64_t f20;
    uint64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[8] pad88;
    int64_t f58;
};

int64_t fun_5e43(struct s11* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    if (rdi->f28 <= rdi->f20 && (rax2 = rdi->f10 - rdi->f8, !!(rdi->f0 & 0x100))) {
        rax2 = rax2 + (rdi->f58 - rdi->f48);
    }
    return rax2;
}

int32_t fun_27e0(void** rdi);

int64_t fun_26b0(int64_t rdi, ...);

int64_t fun_5e73(void** rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 72)))) {
        eax4 = fun_27e0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_26b0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

uint64_t fun_5ef3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_5f13(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s12 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_6373(struct s12* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s13 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_6383(struct s13* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s14 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_6393(struct s14* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s17 {
    signed char[8] pad8;
    struct s17* f8;
};

struct s16 {
    int64_t f0;
    struct s17* f8;
};

struct s15 {
    struct s16* f0;
    struct s16* f8;
};

uint64_t fun_63a3(struct s15* rdi) {
    struct s16* rcx2;
    struct s16* rsi3;
    uint64_t r8_4;
    struct s17* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s20 {
    signed char[8] pad8;
    struct s20* f8;
};

struct s19 {
    int64_t f0;
    struct s20* f8;
};

struct s18 {
    struct s19* f0;
    struct s19* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_6403(struct s18* rdi) {
    struct s19* rcx2;
    struct s19* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s20* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s23 {
    signed char[8] pad8;
    struct s23* f8;
};

struct s22 {
    int64_t f0;
    struct s23* f8;
};

struct s21 {
    struct s22* f0;
    struct s22* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_6473(struct s21* rdi, void** rsi) {
    uint64_t r12_3;
    void** rbp4;
    struct s22* rcx5;
    struct s22* rsi6;
    void** r8_7;
    void** rbx8;
    void** r13_9;
    struct s23* rax10;
    uint64_t rdx11;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_3) + 4) = 0;
    rbp4 = rsi;
    rcx5 = rdi->f0;
    rsi6 = rdi->f8;
    r8_7 = rdi->f20;
    rbx8 = rdi->f10;
    r13_9 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx5) < reinterpret_cast<uint64_t>(rsi6)) {
        while (1) {
            if (!rcx5->f0) {
                ++rcx5;
                if (reinterpret_cast<uint64_t>(rsi6) <= reinterpret_cast<uint64_t>(rcx5)) 
                    break;
            } else {
                rax10 = rcx5->f8;
                *reinterpret_cast<int32_t*>(&rdx11) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
                if (rax10) {
                    do {
                        rax10 = rax10->f8;
                        ++rdx11;
                    } while (rax10);
                }
                if (r12_3 < rdx11) {
                    r12_3 = rdx11;
                }
                ++rcx5;
                if (reinterpret_cast<uint64_t>(rsi6) <= reinterpret_cast<uint64_t>(rcx5)) 
                    break;
            }
        }
    }
    fun_2930(rbp4, 1, "# entries:         %lu\n", r8_7, r8_7);
    fun_2930(rbp4, 1, "# buckets:         %lu\n", rbx8, r8_7);
    if (reinterpret_cast<signed char>(r13_9) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x6c6c]");
        if (reinterpret_cast<signed char>(rbx8) >= reinterpret_cast<signed char>(0)) {
            addr_652a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_65a9_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_2930(rbp4, 1, "# buckets used:    %lu (%.2f%%)\n", r13_9, r8_7);
        goto fun_2930;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x6ceb]");
        if (reinterpret_cast<signed char>(rbx8) < reinterpret_cast<signed char>(0)) 
            goto addr_65a9_14; else 
            goto addr_652a_13;
    }
}

struct s24 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s25 {
    int64_t f0;
    struct s25* f8;
};

int64_t fun_65d3(struct s24* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s24* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s25* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x29aa;
    rbx7 = reinterpret_cast<struct s25*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_6638_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_662b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_662b_7;
    }
    addr_663b_10:
    return r12_3;
    addr_6638_5:
    r12_3 = rbx7->f0;
    goto addr_663b_10;
    addr_662b_7:
    return 0;
}

struct s26 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_6653(struct s26* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x29af;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_668f_7;
    return *rax2;
    addr_668f_7:
    goto 0x29af;
}

struct s28 {
    int64_t f0;
    struct s28* f8;
};

struct s27 {
    int64_t* f0;
    struct s28* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_66a3(struct s27* rdi, int64_t rsi) {
    struct s27* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s28* rax7;
    struct s28* rdx8;
    struct s28* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x29b5;
    rax7 = reinterpret_cast<struct s28*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_66ee_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_66ee_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_670c_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_670c_10:
    return r8_10;
}

struct s30 {
    int64_t f0;
    struct s30* f8;
};

struct s29 {
    struct s30* f0;
    struct s30* f8;
};

void fun_6733(struct s29* rdi, int64_t rsi, uint64_t rdx) {
    struct s30* r9_4;
    uint64_t rax5;
    struct s30* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_6772_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_6772_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s32 {
    int64_t f0;
    struct s32* f8;
};

struct s31 {
    struct s32* f0;
    struct s32* f8;
};

int64_t fun_6783(struct s31* rdi, int64_t rsi, int64_t rdx) {
    struct s32* r14_4;
    int64_t r12_5;
    struct s31* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s32* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_67af_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_67f1_10;
            }
            addr_67af_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_67b9_11:
    return r12_5;
    addr_67f1_10:
    goto addr_67b9_11;
}

uint64_t fun_6803(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s33 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_6843(struct s33* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_2760(void** rdi, void** rsi);

void** fun_6873(uint64_t rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8) {
    void** r15_6;
    void** rbp7;
    int64_t rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    void** rax12;
    void** rax13;
    void** rdi14;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x5ef0);
    }
    if (!rcx) {
        rbx8 = 0x5f10;
    }
    rax9 = fun_2800(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0xd1e0);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((*reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax12 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&rsi)), *reinterpret_cast<void***>(r12_10 + 16) = rax12, rax12 == 0) || (*reinterpret_cast<uint32_t*>(&rsi) = 16, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax13 = fun_2760(rax12, 16), *reinterpret_cast<void***>(r12_10) = rax13, rax13 == 0))) {
            rdi14 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_2500(rdi14, rsi);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<int64_t*>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int64_t*>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s36 {
    int64_t f0;
    struct s36* f8;
};

struct s35 {
    int64_t f0;
    struct s36* f8;
};

struct s34 {
    struct s35* f0;
    struct s35* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s36* f48;
};

void fun_6973(struct s34* rdi) {
    struct s34* rbp2;
    struct s35* r12_3;
    struct s36* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s36* rax7;
    struct s36* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s37 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_6a23(struct s37* rdi, void** rsi) {
    struct s37* r12_3;
    void** r13_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rdi9;
    void** rbx10;
    void** rbx11;
    void** rdi12;
    void** rdi13;

    __asm__("cli ");
    r12_3 = rdi;
    r13_4 = rdi->f0;
    rax5 = rdi->f8;
    rbp6 = r13_4;
    if (!rdi->f40 || !rdi->f20) {
        addr_6a93_2:
        if (reinterpret_cast<unsigned char>(rax5) > reinterpret_cast<unsigned char>(rbp6)) {
            do {
                rbx7 = *reinterpret_cast<void***>(rbp6 + 8);
                if (rbx7) {
                    do {
                        rdi8 = rbx7;
                        rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
                        fun_2500(rdi8, rsi);
                    } while (rbx7);
                }
                rbp6 = rbp6 + 16;
            } while (reinterpret_cast<unsigned char>(r12_3->f8) > reinterpret_cast<unsigned char>(rbp6));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax5)) {
            while (1) {
                rdi9 = *reinterpret_cast<void***>(r13_4);
                if (!rdi9) {
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                } else {
                    rbx10 = r13_4;
                    while (r12_3->f40(rdi9), rbx10 = *reinterpret_cast<void***>(rbx10 + 8), !!rbx10) {
                        rdi9 = *reinterpret_cast<void***>(rbx10);
                    }
                    rax5 = r12_3->f8;
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                }
            }
            rbp6 = r12_3->f0;
            goto addr_6a93_2;
        }
    }
    rbx11 = r12_3->f48;
    if (rbx11) {
        do {
            rdi12 = rbx11;
            rbx11 = *reinterpret_cast<void***>(rbx11 + 8);
            fun_2500(rdi12, rsi);
        } while (rbx11);
    }
    rdi13 = r12_3->f0;
    fun_2500(rdi13, rsi);
    goto fun_2500;
}

int64_t fun_6b13(void** rdi, uint64_t rsi) {
    void** r12_3;
    int64_t rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    void** r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    int64_t rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi + 40);
    rax4 = g28;
    esi5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 16));
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_6c50_2;
    if (*reinterpret_cast<void***>(rdi + 16) == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_2760(rax6, 16);
        if (!rax8) {
            addr_6c50_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8) - 8 + 8);
            v10 = *reinterpret_cast<void***>(rdi + 72);
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = *reinterpret_cast<void***>(rdi);
                fun_2500(rdi12, rdi);
                *reinterpret_cast<void***>(rdi) = rax8;
                *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                *reinterpret_cast<void***>(rdi + 16) = rax6;
                *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rdi + 72) = v10;
            } else {
                *reinterpret_cast<void***>(rdi + 72) = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x29ba;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x29ba;
                fun_2500(rax8, r13_9);
            }
        }
    }
    rax15 = rax4 - g28;
    if (rax15) {
        fun_2660();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s38 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_6ca3(void** rdi, void** rsi, void*** rdx) {
    int64_t rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s38* v17;
    int32_t r8d18;
    void** rax19;
    int64_t rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x29bf;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_6dbe_5; else 
                goto addr_6d2f_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_6d2f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_6dbe_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x63f1]");
            if (!cf14) 
                goto addr_6e15_12;
            __asm__("comiss xmm4, [rip+0x63a1]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x6360]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_6e15_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x29bf;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_2800(16, rsi, rdx6);
                if (!rax19) {
                    addr_6e15_12:
                    r8d18 = -1;
                } else {
                    goto addr_6d72_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_6d72_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_6cee_28:
    rax20 = rax4 - g28;
    if (rax20) {
        fun_2660();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_6d72_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_6cee_28;
}

int32_t hash_insert_if_absent();

int64_t fun_6ec3() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    int64_t rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = rax1 - g28;
    if (rdx6) {
        fun_2660();
    } else {
        return rax3;
    }
}

void** fun_6f23(void** rdi, void** rsi) {
    void** rbx3;
    int64_t rax4;
    int64_t v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    int64_t rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_6fb0_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_7066_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x620e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x6178]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_2500(rdi15, rsi, rdi15, rsi);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_7066_5; else 
                goto addr_6fb0_4;
        }
    }
    rax16 = v5 - g28;
    if (rax16) {
        fun_2660();
    } else {
        return r12_7;
    }
}

void fun_70b3() {
    __asm__("cli ");
    goto 0x6f20;
}

struct s39 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_70c3(struct s39* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

struct s40 {
    signed char[8] pad8;
    int64_t f8;
    int64_t f10;
};

struct s41 {
    signed char[8] pad8;
    int64_t f8;
    int64_t f10;
};

int64_t fun_70e3(struct s40* rdi, struct s41* rsi) {
    __asm__("cli ");
    if (rdi->f8 != rsi->f8 || rdi->f10 != rsi->f10) {
        return 0;
    }
}

struct s42 {
    int64_t f0;
    uint64_t f8;
};

uint64_t hash_pjw(int64_t rdi);

uint64_t fun_7113(struct s42* rdi, int64_t rsi) {
    int64_t rdi3;
    uint64_t rax4;

    __asm__("cli ");
    rdi3 = rdi->f0;
    rax4 = hash_pjw(rdi3);
    return (rax4 ^ rdi->f8) % rsi;
}

struct s43 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
};

struct s44 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
};

int64_t fun_2780(int64_t rdi, int64_t rsi);

int64_t fun_7143(struct s43* rdi, struct s44* rsi) {
    int64_t rsi3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    if (rdi->f8 != rsi->f8 || rdi->f10 != rsi->f10) {
        return 0;
    } else {
        rsi3 = rsi->f0;
        rdi4 = rdi->f0;
        rax5 = fun_2780(rdi4, rsi3);
        *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax5) == 0);
        return rax5;
    }
}

void fun_7183(void*** rdi, void** rsi) {
    void** rdi3;

    __asm__("cli ");
    rdi3 = *rdi;
    fun_2500(rdi3, rsi, rdi3, rsi);
    goto fun_2500;
}

int32_t fun_2650();

void fd_safer(int64_t rdi);

void fun_71a3() {
    int64_t rax1;
    unsigned char dl2;
    int32_t eax3;
    int64_t rdi4;
    int64_t rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (dl2 & 64) {
    }
    eax3 = fun_2650();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = rax1 - g28;
    if (rdx5) {
        fun_2660();
    } else {
        return;
    }
}

int64_t fun_28e0(int64_t rdi);

void fun_26e0(int64_t rdi, void** rsi, ...);

int64_t fun_7223(int64_t rdi, void** rsi, int32_t edx, void*** rcx) {
    int64_t r12_5;
    void** eax6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;
    int64_t rdi10;
    void** r13d11;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    eax6 = openat_safer(rdi, rsi);
    if (reinterpret_cast<signed char>(eax6) >= reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<void***>(&rdi7) = eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        rax8 = fun_28e0(rdi7);
        r12_5 = rax8;
        if (!rax8) {
            rax9 = fun_2520();
            *reinterpret_cast<void***>(&rdi10) = eax6;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            r13d11 = *reinterpret_cast<void***>(rax9);
            fun_26e0(rdi10, rsi);
            *reinterpret_cast<void***>(rax9) = r13d11;
        } else {
            *rcx = eax6;
        }
    }
    return r12_5;
}

void fun_2920(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s45 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s45* fun_26a0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_7283(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s45* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2920("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2510("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_26a0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_2530(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_8a23(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2520();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x12260;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_8a63(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x12260);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_8a83(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x12260);
    }
    *rdi = esi;
    return 0x12260;
}

int64_t fun_8aa3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x12260);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s46 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_8ae3(struct s46* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s46*>(0x12260);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s47 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s47* fun_8b03(struct s47* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s47*>(0x12260);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x29ce;
    if (!rdx) 
        goto 0x29ce;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x12260;
}

struct s48 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8b43(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s48* r8) {
    struct s48* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s48*>(0x12260);
    }
    rax7 = fun_2520();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x8b76);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s49 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8bc3(int64_t rdi, int64_t rsi, void*** rdx, struct s49* rcx) {
    struct s49* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s49*>(0x12260);
    }
    rax6 = fun_2520();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x8bf1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x8c4c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_8cb3() {
    __asm__("cli ");
}

void** g12098 = reinterpret_cast<void**>(96);

int64_t slotvec0 = 0x100;

void fun_8cc3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2500(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0x12160) {
        fun_2500(rdi8, rsi9);
        g12098 = reinterpret_cast<void**>(0x12160);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x12090) {
        fun_2500(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0x12090);
    }
    nslots = 1;
    return;
}

void fun_8d63() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8d83() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8d93(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8db3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_8dd3(void** rdi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s2* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x29d4;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_8e63(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s2* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x29d9;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2660();
    } else {
        return rax7;
    }
}

void** fun_8ef3(int32_t edi, int64_t rsi) {
    int64_t rax3;
    struct s2* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x29de;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_2660();
    } else {
        return rax5;
    }
}

void** fun_8f83(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s2* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x29e3;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_9013(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s2* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x9240]");
    __asm__("movdqa xmm1, [rip+0x9248]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x9231]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_2660();
    } else {
        return rax10;
    }
}

void** fun_90b3(int64_t rdi, uint32_t esi) {
    struct s2* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x91a0]");
    __asm__("movdqa xmm1, [rip+0x91a8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x9191]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_2660();
    } else {
        return rax9;
    }
}

void** fun_9153(int64_t rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x9100]");
    __asm__("movdqa xmm1, [rip+0x9108]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x90e9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_2660();
    } else {
        return rax3;
    }
}

void** fun_91e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x9070]");
    __asm__("movdqa xmm1, [rip+0x9078]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x9066]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_2660();
    } else {
        return rax4;
    }
}

void** fun_9273(void** rdi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s2* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x29e8;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_9313(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s2* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8f3a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x8f32]");
    __asm__("movdqa xmm2, [rip+0x8f3a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x29ed;
    if (!rdx) 
        goto 0x29ed;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2660();
    } else {
        return rax7;
    }
}

void** fun_93b3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rcx6;
    struct s2* rcx7;
    void** rdi8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8e9a]");
    __asm__("movdqa xmm1, [rip+0x8ea2]");
    __asm__("movdqa xmm2, [rip+0x8eaa]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x29f2;
    if (!rdx) 
        goto 0x29f2;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = rcx6 - g28;
    if (rdx10) {
        fun_2660();
    } else {
        return rax9;
    }
}

void** fun_9463(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    struct s2* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8dea]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x8de2]");
    __asm__("movdqa xmm2, [rip+0x8dea]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x29f7;
    if (!rsi) 
        goto 0x29f7;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_9503(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s2* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8d4a]");
    __asm__("movdqa xmm1, [rip+0x8d52]");
    __asm__("movdqa xmm2, [rip+0x8d5a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x29fc;
    if (!rsi) 
        goto 0x29fc;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2660();
    } else {
        return rax7;
    }
}

void fun_95a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_95b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_95d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_95f3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int32_t fun_2830();

int64_t fun_9613(int32_t edi, void** rsi, int32_t edx, void** rcx, int32_t r8d) {
    int64_t rax6;
    int32_t eax7;
    int32_t r12d8;
    void** rax9;
    void*** rsp10;
    void** r8_11;
    int64_t rax12;
    uint32_t edx13;
    uint32_t r9d14;
    int64_t rax15;
    int64_t rax16;
    void** rax17;
    void** rax18;
    uint32_t r9d19;
    int64_t rdi20;
    int32_t eax21;
    int64_t rdi22;
    int32_t eax23;
    uint32_t r9d24;
    int64_t rdi25;
    int32_t eax26;
    uint32_t v27;
    uint32_t v28;
    int64_t rdx29;
    int64_t rdi30;
    int32_t eax31;
    uint32_t v32;
    uint32_t v33;

    __asm__("cli ");
    rax6 = g28;
    eax7 = fun_2830();
    r12d8 = eax7;
    if (eax7 >= 0 || (rax9 = fun_2520(), rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8), r8_11 = rax9, *reinterpret_cast<void***>(&rax12) = *reinterpret_cast<void***>(rax9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0, edx13 = static_cast<uint32_t>(rax12 - 22) & 0xffffffef, *reinterpret_cast<unsigned char*>(&edx13) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!edx13)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax12) == 95)))), r9d14 = edx13, !!*reinterpret_cast<unsigned char*>(&edx13))) {
        addr_96c0_2:
        rax15 = rax6 - g28;
        if (rax15) {
            fun_2660();
        } else {
            *reinterpret_cast<int32_t*>(&rax16) = r12d8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
            return rax16;
        }
    } else {
        if (!r8d) {
            addr_9706_6:
            rax17 = fun_2640(rsi, rsi);
            rax18 = fun_2640(rcx, rcx);
            rsp10 = rsp10 - 8 + 8 - 8 + 8;
            if (!rax17) 
                goto addr_9745_7;
            r8_11 = r8_11;
            r9d19 = *reinterpret_cast<unsigned char*>(&r9d14);
            if (!rax18) 
                goto addr_9745_7;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rax17) + 0xffffffffffffffff) == 47) 
                goto addr_9760_10;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rax18) + 0xffffffffffffffff) != 47) 
                goto addr_9745_7;
        } else {
            if (r8d != 1) {
                *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(95);
                r12d8 = -1;
                goto addr_96c0_2;
            } else {
                *reinterpret_cast<int32_t*>(&rdi20) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                eax21 = fun_2980(rdi20, rcx, rsp10 + 0xa0, 0x100);
                rsp10 = rsp10 - 8 + 8;
                r8_11 = r8_11;
                if (!eax21 || *reinterpret_cast<void***>(r8_11) == 75) {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(17);
                    r12d8 = -1;
                    goto addr_96c0_2;
                } else {
                    if (*reinterpret_cast<void***>(r8_11) != 2) 
                        goto addr_96b5_17;
                    r9d14 = 1;
                    goto addr_9706_6;
                }
            }
        }
    }
    addr_9760_10:
    *reinterpret_cast<int32_t*>(&rdi22) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
    eax23 = fun_2980(rdi22, rsi, rsp10 + 16, 0x100);
    rsp10 = rsp10 - 8 + 8;
    if (eax23) {
        addr_96b5_17:
        r12d8 = -1;
        goto addr_96c0_2;
    } else {
        r9d24 = *reinterpret_cast<unsigned char*>(&r9d19);
        r8_11 = r8_11;
        if (!*reinterpret_cast<signed char*>(&r9d24)) {
            *reinterpret_cast<int32_t*>(&rdi25) = edx;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
            eax26 = fun_2980(rdi25, rcx, rsp10 + 0xa0, 0x100);
            r8_11 = r8_11;
            if (!eax26) {
                if ((v27 & 0xf000) == 0x4000) {
                    if ((v28 & 0xf000) == 0x4000) {
                        addr_9745_7:
                        *reinterpret_cast<int32_t*>(&rdx29) = edx;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi30) = edi;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
                        eax31 = fun_2900(rdi30, rsi, rdx29, rcx, r8_11);
                        r12d8 = eax31;
                        goto addr_96c0_2;
                    } else {
                        *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(21);
                        r12d8 = -1;
                        goto addr_96c0_2;
                    }
                } else {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(20);
                    goto addr_96b5_17;
                }
            } else {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r8_11) == 2) && (v32 & 0xf000) == 0x4000) {
                    goto addr_9745_7;
                }
            }
        } else {
            if ((v33 & 0xf000) == 0x4000) 
                goto addr_9745_7;
            *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(2);
            r12d8 = -1;
            goto addr_96c0_2;
        }
    }
}

int64_t fun_9873(int32_t edi, void** rsi, void** rdx, void** rcx) {
    int64_t rax5;
    void** rax6;
    void** rax7;
    void** rax8;
    void** rax9;
    uint32_t r9d10;
    uint32_t eax11;
    void** rax12;
    void* rsp13;
    void** rdx14;
    void** rcx15;
    int64_t rdi16;
    void** rsi17;
    int32_t eax18;
    void* rsp19;
    void** rax20;
    void** rax21;
    int64_t rdi22;
    void** rsi23;
    int32_t eax24;
    void** rax25;
    int32_t r9d26;
    int64_t v27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t rax31;
    int64_t rax32;

    __asm__("cli ");
    rax5 = g28;
    rax6 = last_component(rsi, rsi, rdx, rcx);
    rax7 = last_component(rcx, rsi, rdx, rcx);
    rax8 = base_len(rax6, rsi, rdx, rcx);
    rax9 = base_len(rax7, rsi, rdx, rcx);
    r9d10 = 0;
    if (rax8 == rax9 && (eax11 = fun_2730(rax6, rax7, rax8), r9d10 = 0, !eax11)) {
        rax12 = dir_name(rsi, rax7, rax8, rcx);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rdx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 16);
        *reinterpret_cast<int32_t*>(&rcx15) = 0x100;
        *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi16) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
        rsi17 = rax12;
        eax18 = fun_2980(rdi16, rsi17, rdx14, 0x100);
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
        if (eax18) {
            rax20 = fun_2520();
            rcx15 = rax12;
            rdx14 = reinterpret_cast<void**>("%s");
            rsi17 = *reinterpret_cast<void***>(rax20);
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            fun_28b0();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
        }
        fun_2500(rax12, rsi17, rax12, rsi17);
        rax21 = dir_name(rcx, rsi17, rdx14, rcx15);
        *reinterpret_cast<int32_t*>(&rdi22) = *reinterpret_cast<int32_t*>(&rdx);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
        rsi23 = rax21;
        eax24 = fun_2980(rdi22, rsi23, reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 + 0xa0, 0x100);
        if (eax24) {
            rax25 = fun_2520();
            rsi23 = *reinterpret_cast<void***>(rax25);
            *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
            fun_28b0();
        }
        r9d26 = 0;
        if (v27 == v28) {
            *reinterpret_cast<unsigned char*>(&r9d26) = reinterpret_cast<uint1_t>(v29 == v30);
        }
        fun_2500(rax21, rsi23, rax21, rsi23);
        r9d10 = *reinterpret_cast<unsigned char*>(&r9d26);
    }
    rax31 = rax5 - g28;
    if (rax31) {
        fun_2660();
    } else {
        *reinterpret_cast<uint32_t*>(&rax32) = r9d10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
        return rax32;
    }
}

void fun_9a13(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto same_nameat;
}

struct s50 {
    signed char[16] pad16;
    void** f10;
};

void** fun_9a33(struct s50* rdi, void** rsi, void** rdx) {
    void** rdi4;
    void** r12_5;
    void** rax6;

    __asm__("cli ");
    rdi4 = reinterpret_cast<void**>(&rdi->f10);
    r12_5 = *reinterpret_cast<void***>(rdi4 + 0xfffffffffffffff0);
    if (r12_5 == rdi4) {
        rax6 = fun_2800(rsi, rsi, rdx);
        if (rax6) {
            goto fun_27d0;
        }
    } else {
        rax6 = fun_2850(r12_5, rsi);
        if (!rax6) {
            rax6 = r12_5;
        }
    }
    return rax6;
}

struct s51 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_9a93(struct s51* rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rdi5;
    void** r12_6;
    void** rbp7;
    void** rax8;
    void** rax9;

    __asm__("cli ");
    rax4 = rdi->f8;
    rdi5 = rdi->f0;
    r12_6 = reinterpret_cast<void**>(&rdi->f10);
    rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rax4));
    if (rdi5 != r12_6) {
        fun_2500(rdi5, rsi);
        rax4 = rdi->f8;
    }
    if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp7)) {
        rax8 = fun_2520();
        *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(12);
    } else {
        rax9 = fun_2800(rbp7, rsi, rdx);
        if (rax9) {
            rdi->f0 = rax9;
            rdi->f8 = rbp7;
            return 1;
        }
    }
    rdi->f0 = r12_6;
    rdi->f8 = reinterpret_cast<void**>(0x400);
    return 0;
}

struct s52 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_9b13(struct s52* rdi, void** rsi, void** rdx) {
    void** r14_4;
    void** r13_5;
    void** r12_6;
    void** rbp7;
    void** rax8;
    int64_t rax9;
    void** rax10;
    void** rcx11;
    void** rax12;
    void** rax13;

    __asm__("cli ");
    r14_4 = reinterpret_cast<void**>(&rdi->f10);
    r13_5 = rdi->f8;
    r12_6 = rdi->f0;
    rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_5) + reinterpret_cast<unsigned char>(r13_5));
    if (r12_6 == r14_4) {
        rax8 = fun_2800(rbp7, rsi, rdx);
        if (!rax8) {
            *reinterpret_cast<int32_t*>(&rax9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        } else {
            rax10 = fun_27d0(rax8, r12_6, r13_5);
            rcx11 = rax10;
            goto addr_9b4c_5;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_5) > reinterpret_cast<unsigned char>(rbp7)) {
            rax12 = fun_2520();
            *reinterpret_cast<void***>(rax12) = reinterpret_cast<void**>(12);
            goto addr_9b9b_8;
        } else {
            rsi = rbp7;
            rax13 = fun_2850(r12_6, rsi);
            rcx11 = rax13;
            if (!rax13) {
                r12_6 = rdi->f0;
                goto addr_9b9b_8;
            } else {
                addr_9b4c_5:
                rdi->f0 = rcx11;
                *reinterpret_cast<int32_t*>(&rax9) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                rdi->f8 = rbp7;
            }
        }
    }
    addr_9b58_11:
    return rax9;
    addr_9b9b_8:
    fun_2500(r12_6, rsi);
    rdi->f0 = r14_4;
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    rdi->f8 = reinterpret_cast<void**>(0x400);
    goto addr_9b58_11;
}

int32_t fun_2720();

int64_t fun_9bd3() {
    int64_t rax1;
    int32_t eax2;
    void** rax3;
    int64_t rax4;
    int32_t eax5;
    int64_t rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = fun_2720();
    rax3 = fun_2520();
    if (!eax2 || *reinterpret_cast<void***>(rax3) == 75) {
        *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(17);
        *reinterpret_cast<int32_t*>(&rax4) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    } else {
        eax5 = 0;
        *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 2));
        *reinterpret_cast<int32_t*>(&rax4) = -eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    }
    rdx6 = rax1 - g28;
    if (rdx6) {
        fun_2660();
    } else {
        return rax4;
    }
}

void fun_9c53() {
    __asm__("cli ");
}

void fun_9c63() {
    __asm__("cli ");
}

int64_t fun_2940(void* rdi, int64_t rsi, void** rdx);

void fun_25d0(int64_t rdi, void* rsi, void** rdx);

int64_t fun_9c83(void** rdi, int32_t esi, int64_t rdx, int64_t rcx, void* r8) {
    int64_t v6;
    void** v7;
    int64_t v8;
    void* v9;
    int64_t rax10;
    int64_t v11;
    void** rax12;
    void*** rsp13;
    void** v14;
    void** v15;
    void** v16;
    void** rax17;
    void** rdx18;
    void* rax19;
    void** r15_20;
    void* v21;
    void** v22;
    void* rax23;
    void*** rsp24;
    uint32_t r9d25;
    int64_t rax26;
    int32_t v27;
    unsigned char v28;
    int32_t v29;
    int64_t rdx30;
    int64_t rax31;
    void** r13_32;
    uint64_t v33;
    int32_t r8d34;
    uint32_t r9d35;
    void** rcx36;
    void** rbx37;
    uint64_t rax38;
    uint32_t eax39;
    void** v40;
    void** v41;

    __asm__("cli ");
    v6 = rdx;
    v7 = rdi;
    v8 = rcx;
    v9 = r8;
    rax10 = g28;
    v11 = rax10;
    rax12 = fun_2520();
    rsp13 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88 - 8 + 8);
    v14 = rax12;
    v15 = *reinterpret_cast<void***>(rax12);
    v16 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 + 80) >> 4);
    rax17 = fun_2640(rdi);
    rdx18 = reinterpret_cast<void**>(esi + reinterpret_cast<uint64_t>(r8));
    if (reinterpret_cast<unsigned char>(rdx18) > reinterpret_cast<unsigned char>(rax17) || (rax19 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax17) - reinterpret_cast<unsigned char>(rdx18)), r15_20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax19)), v21 = rax19, v22 = r15_20, rax23 = fun_2700(r15_20, "X"), rsp24 = rsp13 - 8 + 8 - 8 + 8, r9d25 = reinterpret_cast<uint1_t>(rcx == 0x9bd0), reinterpret_cast<uint64_t>(rax23) < reinterpret_cast<uint64_t>(r8))) {
        *reinterpret_cast<void***>(v14) = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax26) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    } else {
        v27 = 0x3a2f8;
        v28 = *reinterpret_cast<unsigned char*>(&r9d25);
        v29 = 0;
        goto addr_9d70_4;
    }
    addr_9eb9_5:
    rdx30 = v11 - g28;
    if (rdx30) {
        fun_2660();
    } else {
        return rax26;
    }
    addr_9edb_8:
    *reinterpret_cast<void***>(v14) = v15;
    goto addr_9eb9_5;
    addr_9eb4_9:
    *reinterpret_cast<int32_t*>(&rax26) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    goto addr_9eb9_5;
    while (1) {
        *reinterpret_cast<int32_t*>(&rdx18) = 1;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        rax31 = fun_2940(rsp24 + 88, 8, 1);
        rsp24 = rsp24 - 8 + 8;
        if (rax31 != 8) {
            while (1) {
                fun_25d0(1, rsp24 + 96, rdx18);
                rsp24 = rsp24 - 8 + 8;
                r13_32 = reinterpret_cast<void**>((v33 ^ reinterpret_cast<unsigned char>(r13_32)) * 0x27bb2ee687b0b0fd + 0xb504f32d);
                if (reinterpret_cast<unsigned char>(r13_32) > reinterpret_cast<unsigned char>(0xf49998db0aa753ff)) 
                    break;
                addr_9e68_12:
                r8d34 = 9;
                r9d35 = 1;
                while (1) {
                    rcx36 = r13_32;
                    ++rbx37;
                    rdx18 = reinterpret_cast<void**>(__intrinsic() >> 4);
                    v16 = rdx18;
                    r13_32 = rdx18;
                    rax38 = reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx18) << 5) - reinterpret_cast<unsigned char>(rdx18);
                    eax39 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") + (reinterpret_cast<unsigned char>(rcx36) - (rax38 + rax38)));
                    *reinterpret_cast<void***>(rbx37 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax39);
                    if (rbx37 == v40) {
                        v28 = *reinterpret_cast<unsigned char*>(&r9d35);
                        v29 = r8d34;
                        do {
                            rax26 = reinterpret_cast<int64_t>(v8(v7, v6));
                            rsp24 = rsp24 - 8 + 8;
                            if (*reinterpret_cast<int32_t*>(&rax26) >= 0) 
                                goto addr_9edb_8;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v14) == 17)) 
                                goto addr_9eb4_9;
                            --v27;
                            if (!v27) 
                                goto addr_9eb4_9;
                            addr_9d70_4:
                        } while (!v9);
                        r13_32 = v16;
                        rbx37 = v22;
                        r9d35 = v28;
                        v40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v7) + reinterpret_cast<uint64_t>(v9) + reinterpret_cast<uint64_t>(v21));
                        r8d34 = v29;
                    }
                    if (!r8d34) 
                        break;
                    --r8d34;
                }
                if (*reinterpret_cast<unsigned char*>(&r9d35)) 
                    break;
            }
        } else {
            r13_32 = v41;
            if (reinterpret_cast<unsigned char>(r13_32) > reinterpret_cast<unsigned char>(0xf49998db0aa753ff)) 
                continue;
            goto addr_9e68_12;
        }
    }
}

int32_t fun_9f03(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    eax2 = try_tempname_len(rdi);
    return eax2;
}

int32_t fun_9f33(void** rdi) {
    int64_t rax2;
    int32_t eax3;
    int64_t rdx4;

    __asm__("cli ");
    rax2 = g28;
    eax3 = try_tempname_len(rdi);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_2660();
    } else {
        return eax3;
    }
}

void fun_9f93() {
    __asm__("cli ");
    goto try_tempname_len;
}

int32_t dup_safer();

int64_t fun_9fa3(uint32_t edi, void** rsi) {
    int32_t eax3;
    void** rax4;
    int64_t rdi5;
    void** r13d6;
    int64_t rax7;
    int64_t rax8;

    __asm__("cli ");
    if (edi <= 2) {
        eax3 = dup_safer();
        rax4 = fun_2520();
        *reinterpret_cast<uint32_t*>(&rdi5) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        r13d6 = *reinterpret_cast<void***>(rax4);
        fun_26e0(rdi5, rsi);
        *reinterpret_cast<int32_t*>(&rax7) = eax3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        *reinterpret_cast<void***>(rax4) = r13d6;
        return rax7;
    } else {
        *reinterpret_cast<uint32_t*>(&rax8) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
}

struct s53 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2790(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_a003(void** rdi, void** rsi, void** rdx, void** rcx, struct s53* r8, void** r9) {
    void** r12_7;
    void** rax8;
    void** rax9;
    void** r13_10;
    void** r12_11;
    void** rax12;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2930(rdi, 1, "%s %s\n", rdx, rcx);
    } else {
        r9 = rcx;
        fun_2930(rdi, 1, "%s (%s) %s\n", rsi, rdx, rdi, 1, "%s (%s) %s\n", rsi, rdx);
    }
    rax8 = fun_2620();
    fun_2930(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6);
    fun_2790(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2620();
    fun_2930(rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6);
    fun_2790(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r13_10 = r8->f8;
        r12_11 = r8->f0;
        rax12 = fun_2620();
        fun_2930(rdi, 1, rax12, r12_11, r13_10, rdi, 1, rax12, r12_11, r13_10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xd980 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xd980;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_a473() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s54 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_a493(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s54* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s54* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_2660();
    } else {
        return;
    }
}

void fun_a533(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_a5d6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_a5e0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_2660();
    } else {
        return;
    }
    addr_a5d6_5:
    goto addr_a5e0_7;
}

void fun_a613() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    int64_t v14;
    void** v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    int64_t v20;
    void** rax21;
    void** r8_22;
    void** r9_23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    int64_t v28;
    void** v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2790(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2620();
    fun_2880(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20);
    rax21 = fun_2620();
    fun_2880(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_22, r9_23, v24, __return_address(), v25, v26, v27, v28, v29, v30, v31, v32, v33, v34);
    fun_2620();
    goto fun_2880;
}

int64_t fun_2590();

void fun_a6b3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2590();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a6f3(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a713(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a733(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a753(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2850(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a783(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2850(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a7b3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2590();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a7f3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2590();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a833(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2590();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a863(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2590();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a8b3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2590();
            if (rax5) 
                break;
            addr_a8fd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_a8fd_5;
        rax8 = fun_2590();
        if (rax8) 
            goto addr_a8e6_9;
        if (rbx4) 
            goto addr_a8fd_5;
        addr_a8e6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_a943(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2590();
            if (rax8) 
                break;
            addr_a98a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_a98a_5;
        rax11 = fun_2590();
        if (rax11) 
            goto addr_a972_9;
        if (!rbx6) 
            goto addr_a972_9;
        if (r12_4) 
            goto addr_a98a_5;
        addr_a972_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_a9d3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_aa7d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_aa90_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_aa30_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_aa56_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_aaa4_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_aa4d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_aaa4_14;
            addr_aa4d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_aaa4_14;
            addr_aa56_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2850(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_aaa4_14;
            if (!rbp13) 
                break;
            addr_aaa4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_aa7d_9;
        } else {
            if (!r13_6) 
                goto addr_aa90_10;
            goto addr_aa30_11;
        }
    }
}

void fun_aad3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2760(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab03(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2760(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab33(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2760(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab53(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2760(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab73(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_27d0;
    }
}

void fun_abb3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_27d0;
    }
}

void fun_abf3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_27d0;
    }
}

void fun_ac33(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_2640(rdi);
    rax5 = fun_2800(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_27d0;
    }
}

void fun_ac73() {
    void** rdi1;

    __asm__("cli ");
    fun_2620();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_28b0();
    fun_2510(rdi1);
}

int64_t fun_2770(void* rdi, void** rsi, int64_t rdx, void** rcx);

int32_t fun_27b0(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

int64_t fun_acb3() {
    int32_t r12d1;
    void* rsp2;
    void** rcx3;
    int64_t rax4;
    void** rsi5;
    int64_t rax6;
    signed char* rax7;
    int32_t eax8;
    int64_t rax9;
    int64_t rax10;

    __asm__("cli ");
    r12d1 = 0;
    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32);
    rcx3 = stdin;
    rax4 = g28;
    rsi5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp2) + 16);
    rax6 = fun_2770(reinterpret_cast<int64_t>(rsp2) + 8, rsi5, 10, rcx3);
    if (!(reinterpret_cast<uint1_t>(rax6 < 0) | reinterpret_cast<uint1_t>(rax6 == 0))) {
        rax7 = reinterpret_cast<signed char*>(rax6 - 1);
        if (*rax7 == 10) {
            *rax7 = 0;
        }
        eax8 = fun_27b0(0, rsi5, 10, rcx3);
        *reinterpret_cast<unsigned char*>(&r12d1) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax8 < 0) | reinterpret_cast<uint1_t>(eax8 == 0)));
    }
    fun_2500(0, rsi5, 0, rsi5);
    rax9 = rax4 - g28;
    if (rax9) {
        fun_2660();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = r12d1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

void fun_ad53() {
    __asm__("cli ");
    goto usage;
}

int64_t fun_ad63(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2640(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2530(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_ade3_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_ae30_6; else 
                    continue;
            } else {
                rax18 = fun_2640(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_ae60_8;
                if (v16 == -1) 
                    goto addr_ae1e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_ade3_5;
            } else {
                eax19 = fun_2730(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_ade3_5;
            }
            addr_ae1e_10:
            v16 = rbx15;
            goto addr_ade3_5;
        }
    }
    addr_ae45_16:
    return v12;
    addr_ae30_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_ae45_16;
    addr_ae60_8:
    v12 = rbx15;
    goto addr_ae45_16;
}

int64_t fun_ae73(int64_t rdi, int64_t* rsi) {
    int64_t r12_3;
    int64_t rdi4;
    int64_t* rbp5;
    int64_t rbx6;
    int64_t rax7;

    __asm__("cli ");
    r12_3 = rdi;
    rdi4 = *rsi;
    if (!rdi4) {
        addr_aeb8_2:
        return -1;
    } else {
        rbp5 = rsi;
        *reinterpret_cast<int32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        do {
            rax7 = fun_2780(rdi4, r12_3);
            if (!*reinterpret_cast<int32_t*>(&rax7)) 
                break;
            ++rbx6;
            rdi4 = rbp5[rbx6];
        } while (rdi4);
        goto addr_aeb8_2;
    }
    return rbx6;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_aed3(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2620();
    } else {
        fun_2620();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_28b0;
}

void** quote(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_af63(int64_t* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8) {
    void** r13_9;
    void** r12_10;
    void** rdx11;
    void** rbp12;
    void** r14_13;
    int64_t* v14;
    void** rax15;
    void** rsi16;
    void** v17;
    void** v18;
    void** rbx19;
    void** rbp20;
    int64_t r12_21;
    void** r13_22;
    int64_t r14_23;
    int64_t r15_24;
    int64_t r15_25;
    int64_t rbx26;
    uint32_t eax27;
    void** rax28;
    void** rdi29;
    void** rax30;
    void** rdi31;
    void** rdi32;
    void** rax33;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_9) = 0;
    *reinterpret_cast<int32_t*>(&r13_9 + 4) = 0;
    r12_10 = rdx;
    *reinterpret_cast<int32_t*>(&rdx11) = 5;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    rbp12 = rsi;
    r14_13 = stderr;
    v14 = rdi;
    rax15 = fun_2620();
    rsi16 = r14_13;
    fun_2740(rax15, rsi16, 5, rcx, r8, r9, v17, v14, v18, rbx19, rbp20, r12_21, r13_22, r14_23, r15_24, __return_address(), a7, a8);
    r15_25 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx26) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx26) + 4) = 0;
    if (r15_25) {
        do {
            if (!rbx26 || (rdx11 = r12_10, rsi16 = rbp12, eax27 = fun_2730(r13_9, rsi16, rdx11, r13_9, rsi16, rdx11), !!eax27)) {
                r13_9 = rbp12;
                rax28 = quote(r15_25, rsi16, rdx11, rcx);
                rdi29 = stderr;
                rdx11 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                rcx = rax28;
                fun_2930(rdi29, 1, "\n  - %s", rcx, r8);
            } else {
                rax30 = quote(r15_25, rsi16, rdx11, rcx);
                rdi31 = stderr;
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                rdx11 = reinterpret_cast<void**>(", %s");
                rcx = rax30;
                fun_2930(rdi31, 1, ", %s", rcx, r8);
            }
            ++rbx26;
            rbp12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp12) + reinterpret_cast<unsigned char>(r12_10));
            r15_25 = v14[rbx26];
        } while (r15_25);
    }
    rdi32 = stderr;
    rax33 = *reinterpret_cast<void***>(rdi32 + 40);
    if (reinterpret_cast<unsigned char>(rax33) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi32 + 48))) {
        *reinterpret_cast<void***>(rdi32 + 40) = rax33 + 1;
        *reinterpret_cast<void***>(rax33) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(int64_t* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_b093(int64_t rdi, int64_t rsi, int64_t* rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    int64_t r14_9;
    int64_t r13_10;
    int64_t r12_11;
    int64_t* rbp12;
    int64_t v13;
    int64_t rax14;
    int64_t rdi15;
    int64_t rbx16;
    int64_t rax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_b0d7_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_b14e_4;
        } else {
            addr_b14e_4:
            return rax14;
        }
    }
    rdi15 = *rdx;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            rax17 = fun_2780(rdi15, r14_9);
            if (!*reinterpret_cast<int32_t*>(&rax17)) 
                break;
            ++rbx16;
            rdi15 = rbp12[rbx16];
        } while (rdi15);
        goto addr_b0d0_8;
    } else {
        goto addr_b0d0_8;
    }
    return rbx16;
    addr_b0d0_8:
    rax14 = -1;
    goto addr_b0d7_3;
}

struct s55 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_b163(void** rdi, struct s55* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2730(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t fun_2560();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_b1c3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_2560();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_b21e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2520();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_b21e_3;
            rax6 = fun_2520();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int64_t fun_2600(void** rdi);

int64_t fun_b233(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_27e0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2840(rdi);
        if (!(eax3 && (eax4 = fun_27e0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_26b0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2520();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_2600(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2600;
}

uint64_t fun_b2c3(signed char* rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;

    __asm__("cli ");
    rdx3 = *rdi;
    if (!*reinterpret_cast<signed char*>(&rdx3)) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        do {
            __asm__("rol rax, 0x9");
            ++rdi;
            rax4 = rax4 + rdx3;
            rdx3 = *rdi;
        } while (*reinterpret_cast<signed char*>(&rdx3));
        return rax4 % reinterpret_cast<uint64_t>(rsi);
    }
}

signed char* fun_2820(int64_t rdi);

signed char* fun_b303() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2820(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2680(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_b343(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2680(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_2660();
    } else {
        return r12_7;
    }
}

void fun_b3d3() {
    __asm__("cli ");
}

uint32_t fun_25c0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_b3f3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    int64_t rax14;
    int64_t rax15;
    void** rsi16;
    int64_t rdi17;
    uint32_t eax18;
    int64_t rdi19;
    uint32_t eax20;
    void** rax21;
    int64_t rdi22;
    void** r13d23;
    uint32_t eax24;
    void** rax25;
    int64_t rdi26;
    uint32_t eax27;
    uint32_t ecx28;
    int64_t rax29;
    uint32_t eax30;
    uint32_t eax31;
    uint32_t eax32;
    int32_t ecx33;
    int64_t rax34;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_25c0(rdi);
        r12d10 = eax9;
        goto addr_b4f4_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_25c0(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_b4f4_3:
                rax14 = rax8 - g28;
                if (rax14) {
                    fun_2660();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_b5a9_9:
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi17) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
                eax18 = fun_25c0(rdi17, rdi17);
                if (reinterpret_cast<int32_t>(eax18) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi16) = 2, *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi19) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0, eax20 = fun_25c0(rdi19, rdi19), eax20 == 0xffffffff)) {
                    rax21 = fun_2520();
                    *reinterpret_cast<uint32_t*>(&rdi22) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
                    r12d10 = 0xffffffff;
                    r13d23 = *reinterpret_cast<void***>(rax21);
                    fun_26e0(rdi22, rsi16, rdi22, rsi16);
                    *reinterpret_cast<void***>(rax21) = r13d23;
                    goto addr_b4f4_3;
                }
            }
        } else {
            eax24 = fun_25c0(rdi, rdi);
            r12d10 = eax24;
            if (reinterpret_cast<int32_t>(eax24) >= reinterpret_cast<int32_t>(0) || (rax25 = fun_2520(), *reinterpret_cast<int32_t*>(&rdi26) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax25) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_b4f4_3;
            } else {
                eax27 = fun_25c0(rdi26);
                r12d10 = eax27;
                if (reinterpret_cast<int32_t>(eax27) < reinterpret_cast<int32_t>(0)) 
                    goto addr_b4f4_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_b5a9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_b459_16;
    ecx28 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx28 > 10) 
        goto addr_b45d_18;
    rax29 = 1 << *reinterpret_cast<unsigned char*>(&ecx28);
    if (!(*reinterpret_cast<uint32_t*>(&rax29) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax29) & 0x502)) {
            addr_b45d_18:
            if (0) {
            }
        } else {
            addr_b4a5_23:
            eax30 = fun_25c0(rdi);
            r12d10 = eax30;
            goto addr_b4f4_3;
        }
        eax31 = fun_25c0(rdi);
        r12d10 = eax31;
        goto addr_b4f4_3;
    }
    if (0) {
    }
    eax32 = fun_25c0(rdi);
    r12d10 = eax32;
    goto addr_b4f4_3;
    addr_b459_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_b45d_18;
    ecx33 = *reinterpret_cast<int32_t*>(&rsi);
    rax34 = 1 << *reinterpret_cast<unsigned char*>(&ecx33);
    if (!(*reinterpret_cast<uint32_t*>(&rax34) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax34) & 0xa0a) 
            goto addr_b4a5_23;
        goto addr_b45d_18;
    }
}

int32_t setlocale_null_r();

int64_t fun_b663() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_2660();
    } else {
        return rax3;
    }
}

int64_t fun_b6e3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2870(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2640(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_27d0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_27d0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_b793() {
    __asm__("cli ");
    goto fun_2870;
}

void fun_b7a3() {
    __asm__("cli ");
}

void fun_b7b7() {
    __asm__("cli ");
    return;
}

void fun_2bec() {
    relative = 1;
    goto 0x2ad6;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2970(int64_t rdi, void** rsi);

uint32_t fun_2950(void** rdi, void** rsi);

void** fun_2990(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_74b5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2620();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2620();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2640(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_77b3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_77b3_22; else 
                            goto addr_7bad_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_7c6d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_7fc0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_77b0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_77b0_30; else 
                                goto addr_7fd9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2640(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_7fc0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2730(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_7fc0_28; else 
                            goto addr_765c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_8120_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_7fa0_40:
                        if (r11_27 == 1) {
                            addr_7b2d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_80e8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_7767_44;
                            }
                        } else {
                            goto addr_7fb0_46;
                        }
                    } else {
                        addr_812f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_7b2d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_77b3_22:
                                if (v47 != 1) {
                                    addr_7d09_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2640(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_7d54_54;
                                    }
                                } else {
                                    goto addr_77c0_56;
                                }
                            } else {
                                addr_7765_57:
                                ebp36 = 0;
                                goto addr_7767_44;
                            }
                        } else {
                            addr_7f94_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_812f_47; else 
                                goto addr_7f9e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_7b2d_41;
                        if (v47 == 1) 
                            goto addr_77c0_56; else 
                            goto addr_7d09_52;
                    }
                }
                addr_7821_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_76b8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_76dd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_79e0_65;
                    } else {
                        addr_7849_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_8098_67;
                    }
                } else {
                    goto addr_7840_69;
                }
                addr_76f1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_773c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_8098_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_773c_81;
                }
                addr_7840_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_76dd_64; else 
                    goto addr_7849_66;
                addr_7767_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_781f_91;
                if (v22) 
                    goto addr_777f_93;
                addr_781f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_7821_62;
                addr_7d54_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_84db_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_854b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_834f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2970(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2950(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_7e4e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_780c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_7e58_112;
                    }
                } else {
                    addr_7e58_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_7f29_114;
                }
                addr_7818_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_781f_91;
                while (1) {
                    addr_7f29_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_8437_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_7e96_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_8445_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_7f17_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_7f17_128;
                        }
                    }
                    addr_7ec5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_7f17_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_7e96_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_7ec5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_773c_81;
                addr_8445_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8098_67;
                addr_84db_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_7e4e_109;
                addr_854b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_7e4e_109;
                addr_77c0_56:
                rax93 = fun_2990(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_780c_110;
                addr_7f9e_59:
                goto addr_7fa0_40;
                addr_7c6d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_77b3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_7818_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_7765_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_77b3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_7cb2_160;
                if (!v22) 
                    goto addr_8087_162; else 
                    goto addr_8293_163;
                addr_7cb2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_8087_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_8098_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_7b5b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_79c3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_76f1_70; else 
                    goto addr_79d7_169;
                addr_7b5b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_76b8_63;
                goto addr_7840_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_7f94_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_80cf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_77b0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_76a8_178; else 
                        goto addr_8052_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7f94_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_77b3_22;
                }
                addr_80cf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_77b0_30:
                    r8d42 = 0;
                    goto addr_77b3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_7821_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_80e8_42;
                    }
                }
                addr_76a8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_76b8_63;
                addr_8052_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_7fb0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_7821_62;
                } else {
                    addr_8062_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_77b3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_8812_188;
                if (v28) 
                    goto addr_8087_162;
                addr_8812_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_79c3_168;
                addr_765c_37:
                if (v22) 
                    goto addr_8653_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_7673_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_8120_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_81ab_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_77b3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_76a8_178; else 
                        goto addr_8187_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7f94_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_77b3_22;
                }
                addr_81ab_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_77b3_22;
                }
                addr_8187_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_7fb0_46;
                goto addr_8062_186;
                addr_7673_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_77b3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_77b3_22; else 
                    goto addr_7684_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_875e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_85e4_210;
            if (1) 
                goto addr_85e2_212;
            if (!v29) 
                goto addr_821e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2630();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_8751_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_79e0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_779b_219; else 
            goto addr_79fa_220;
        addr_777f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_7793_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_79fa_220; else 
            goto addr_779b_219;
        addr_834f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_79fa_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2630();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_836d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2630();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_87e0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_8246_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_8437_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_7793_221;
        addr_8293_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_7793_221;
        addr_79d7_169:
        goto addr_79e0_65;
        addr_875e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_79fa_220;
        goto addr_836d_222;
        addr_85e4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - g28;
        if (!rax111) 
            goto addr_863e_236;
        fun_2660();
        rsp25 = rsp25 - 8 + 8;
        goto addr_87e0_225;
        addr_85e2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_85e4_210;
        addr_821e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_85e4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_8246_226;
        }
        addr_8751_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_7bad_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd3ec + rax113 * 4) + 0xd3ec;
    addr_7fd9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd4ec + rax114 * 4) + 0xd4ec;
    addr_8653_190:
    addr_779b_219:
    goto 0x7480;
    addr_7684_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd2ec + rax115 * 4) + 0xd2ec;
    addr_863e_236:
    goto v116;
}

void fun_76a0() {
}

void fun_7858() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x7552;
}

void fun_78b1() {
    goto 0x7552;
}

void fun_799e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x7821;
    }
    if (v2) 
        goto 0x8293;
    if (!r10_3) 
        goto addr_83fe_5;
    if (!v4) 
        goto addr_82ce_7;
    addr_83fe_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_82ce_7:
    goto 0x76d4;
}

void fun_79bc() {
}

void fun_7a67() {
    signed char v1;

    if (v1) {
        goto 0x79ef;
    } else {
        goto 0x772a;
    }
}

void fun_7a81() {
    signed char v1;

    if (!v1) 
        goto 0x7a7a; else 
        goto "???";
}

void fun_7aa8() {
    goto 0x79c3;
}

void fun_7b28() {
}

void fun_7b40() {
}

void fun_7b6f() {
    goto 0x79c3;
}

void fun_7bc1() {
    goto 0x7b50;
}

void fun_7bf0() {
    goto 0x7b50;
}

void fun_7c23() {
    goto 0x7b50;
}

void fun_7ff0() {
    goto 0x76a8;
}

void fun_82ee() {
    signed char v1;

    if (v1) 
        goto 0x8293;
    goto 0x76d4;
}

void fun_8395() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x76d4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x76b8;
        goto 0x76d4;
    }
}

void fun_87b2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x7a20;
    } else {
        goto 0x7552;
    }
}

void fun_a0d8() {
    fun_2620();
}

void fun_2b60() {
    verbose = 1;
    goto 0x2ad6;
}

void fun_2bf8() {
    dereference_dest_dir_symlinks = 0;
    goto 0x2ad6;
}

void fun_78de() {
    goto 0x7552;
}

void fun_7ab4() {
    goto 0x7a6c;
}

void fun_7b7b() {
    goto 0x76a8;
}

void fun_7bcd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x7b50;
    goto 0x777f;
}

void fun_7bff() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x7b5b;
        goto 0x7580;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x79fa;
        goto 0x779b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x8398;
    if (r10_8 > r15_9) 
        goto addr_7ae5_9;
    addr_7aea_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x83a3;
    goto 0x76d4;
    addr_7ae5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_7aea_10;
}

void fun_7c32() {
    goto 0x7767;
}

void fun_8000() {
    goto 0x7767;
}

void fun_879f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x78bc;
    } else {
        goto 0x7a20;
    }
}

void fun_a190() {
}

void** optarg = reinterpret_cast<void**>(0);

void fun_2b6c() {
    int64_t r15_1;
    void** rdi2;
    int32_t eax3;
    void** rsi4;
    void** r15_5;
    uint32_t v6;

    if (r15_1) 
        goto 0x30ec;
    rdi2 = optarg;
    eax3 = fun_27c0(rdi2, reinterpret_cast<int64_t>(__zero_stack_offset()) + 48);
    if (eax3) {
        rsi4 = optarg;
        quotearg_style(4, rsi4);
        fun_2620();
        fun_2520();
        fun_28b0();
    } else {
        r15_5 = optarg;
        if ((v6 & 0xf000) == 0x4000) 
            goto 0x2ad6;
        quotearg_style(4, r15_5);
        fun_2620();
        fun_28b0();
        symbolic_link = 1;
        goto 0x2ad6;
    }
}

void fun_2c04() {
    logical = 1;
    goto 0x2ad6;
}

void fun_7c3c() {
    goto 0x7bd7;
}

void fun_800a() {
    goto 0x7b2d;
}

void fun_a1f0() {
    fun_2620();
    goto fun_2930;
}

void fun_2c10() {
    goto 0x2ad6;
}

void fun_790d() {
    goto 0x7552;
}

void fun_7c48() {
    goto 0x7bd7;
}

void fun_8017() {
    goto 0x7b7e;
}

void fun_a230() {
    fun_2620();
    goto fun_2930;
}

void fun_2c25() {
    logical = 0;
    goto 0x2ad6;
}

void fun_793a() {
    goto 0x7552;
}

void fun_7c54() {
    goto 0x7b50;
}

void fun_a270() {
    fun_2620();
    goto fun_2930;
}

void fun_2c31() {
    remove_existing_files = 0;
    interactive = 1;
    goto 0x2ad6;
}

void fun_795c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x82f0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x7821;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x7821;
    }
    if (v11) 
        goto 0x8653;
    if (r10_12 > r15_13) 
        goto addr_86a3_8;
    addr_86a8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x83e1;
    addr_86a3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_86a8_9;
}

struct s56 {
    signed char[8] pad8;
    void** f8;
};

void fun_a2c0() {
    void** r13_1;
    struct s56* rbx2;
    void** r12_3;
    void*** rbx4;
    void** rax5;
    void** rbp6;
    int64_t v7;

    r13_1 = rbx2->f8;
    r12_3 = *rbx4;
    rax5 = fun_2620();
    fun_2930(rbp6, 1, rax5, r12_3, r13_1, rbp6, 1, rax5, r12_3, r13_1);
    goto v7;
}

void fun_2c44() {
    remove_existing_files = 1;
    interactive = 0;
    goto 0x2ad6;
}

void fun_a318() {
    fun_2620();
    goto 0xa2e9;
}

void fun_2c57() {
    void** rax1;

    rax1 = optarg;
    if (!rax1) {
    }
    goto 0x2ad6;
}

struct s57 {
    signed char[8] pad8;
    void** f8;
};

void fun_a350() {
    void** r13_1;
    struct s57* rbx2;
    void** r12_3;
    void*** rbx4;
    void** rax5;
    void** rbp6;
    int64_t v7;

    r13_1 = rbx2->f8;
    r12_3 = *rbx4;
    rax5 = fun_2620();
    fun_2930(rbp6, 1, rax5, r12_3, r13_1, rbp6, 1, rax5, r12_3, r13_1);
    goto v7;
}

void fun_a3c8() {
    fun_2620();
    goto 0xa38b;
}
