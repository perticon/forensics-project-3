bool dc_parse_stream(struct _IO_FILE * fp, char * filename) {
    int64_t v1 = __readfsqword(40); // 0x307d
    function_2370();
    function_2370();
    int32_t v2 = 3; // 0x30fc
    bool v3 = true; // 0x30fc
    int64_t v4 = (int64_t)"# Configuration file for dircolors, a utility to help you set the\x00# LS_COLORS environment variable used by GNU ls with the --color option.\x00# Copyright (C) 1996-2022 Free Software Foundation, Inc.\x00# Copying and distribution of this file, with or without modification,\x00# are permitted provided the copyright notice and this notice are preserved.\x00# The keywords COLOR, OPTIONS, and EIGHTBIT (honored by the\x00# slackware version of dircolors) are recognized but ignored.\x00# Global config options can be specified before TERM or COLORTERM entries\x00# Below are TERM or COLORTERM entries, which can be glob patterns, which\x00# restrict following config to systems with matching environment variables.\x00\x43OLORTERM ?*\x00TERM Eterm\x00TERM ansi\x00TERM *color*\x00TERM con[0-9]*x[0-9]*\x00TERM cons25\x00TERM console\x00TERM cygwin\x00TERM *direct*\x00TERM dtterm\x00TERM gnome\x00TERM hurd\x00TERM jfbterm\x00TERM konsole\x00TERM kterm\x00TERM linux\x00TERM linux-c\x00TERM mlterm\x00TERM putty\x00TERM rxvt*\x00TERM screen*\x00TERM st\x00TERM terminator\x00TERM tmux*\x00TERM vt100\x00TERM xterm*\x00# Below are the color init strings for the basic file types.\x00# One can use codes for 256 or more colors supported by modern terminals.\x00# The default color codes use the capabilities of an 8 color terminal\x00# with some additional attributes as per the following codes:\x00# Attribute codes:\x00# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed\x00# Text color codes:\x00# 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white\x00# Background color codes:\x00# 40=black 41=red 42=green 43=yellow 44=blue 45=magenta 46=cyan 47=white\x00#NORMAL 00 # no color code at all\x00#FILE 00 # regular file: use no color at all\x00RESET 0 # reset to \"normal\" color\x00\x44IR 01;34 # directory\x00LINK 01;36 # symbolic link. (If you set this to 'target' instead of a\x00 # numerical value, the color is as for the file pointed to.)\x00MULTIHARDLINK 00 # regular file with more than one link\x00\x46IFO 40;33 # pipe\x00SOCK 01;35 # socket\x00\x44OOR 01;35 # door\x00\x42LK 40;33;01 # block device driver\x00\x43HR 40;33;01 # character device driver\x00ORPHAN 40;31;01 # symlink to nonexistent file, or non-stat'able file ...\x00MISSING 00 # ... and the files they point to\x00SETUID 37;41 # file that is setuid (u+s)\x00SETGID 30;43 # file that is setgid (g+s)\x00\x43\x41PABILITY 00 # file with capability (very expensive to lookup)\x00STICKY_OTHER_WRITABLE 30;42 # dir that is sticky and other-writable (+t,o+w)\x00OTHER_WRITABLE 34;42 # dir that is other-writable (o+w) and not sticky\x00STICKY 37;44 # dir with the sticky bit set (+t) and not other-writable\x00# This is for files with execute permission:\x00\x45XEC 01;32\x00# List any file extensions like '.gz' or '.tar' that you would like ls\x00# to color below. Put the extension, a space, and the color init string.\x00# (and any comments you want to add after a '#')\x00# If you use DOS-style suffixes, you may want to uncomment the following:\x00#.cmd 01;32 # executables (bright green)\x00#.exe 01;32\x00#.com 01;32\x00#.btm 01;32\x00#.bat 01;32\x00# Or if you want to color scripts even if they do not have the\x00# executable bit actually set.\x00#.sh 01;32\x00#.csh 01;32\x00 # archives or compressed (bright red)\x00.tar 01;31\x00.tgz 01;31\x00.arc 01;31\x00.arj 01;31\x00.taz 01;31\x00.lha 01;31\x00.lz4 01;31\x00.lzh 01;31\x00.lzma 01;31\x00.tlz 01;31\x00.txz 01;31\x00.tzo 01;31\x00.t7z 01;31\x00.zip 01;31\x00.z 01;31\x00.dz 01;31\x00.gz 01;31\x00.lrz 01;31\x00.lz 01;31\x00.lzo 01;31\x00.xz 01;31\x00.zst 01;31\x00.tzst 01;31\x00.bz2 01;31\x00.bz 01;31\x00.tbz 01;31\x00.tbz2 01;31\x00.tz 01;31\x00.deb 01;31\x00.rpm 01;31\x00.jar 01;31\x00.war 01;31\x00.ear 01;31\x00.sar 01;31\x00.rar 01;31\x00.alz 01;31\x00.ace 01;31\x00.zoo 01;31\x00.cpio 01;31\x00.7z 01;31\x00.rz 01;31\x00.cab 01;31\x00.wim 01;31\x00.swm 01;31\x00.dwm 01;31\x00.esd 01;31\x00# image formats\x00.avif 01;35\x00.jpg 01;35\x00.jpeg 01;35\x00.mjpg 01;35\x00.mjpeg 01;35\x00.gif 01;35\x00.bmp 01;35\x00.pbm 01;35\x00.pgm 01;35\x00.ppm 01;35\x00.tga 01;35\x00.xbm 01;35\x00.xpm 01;35\x00.tif 01;35\x00.tiff 01;35\x00.png 01;35\x00.svg 01;35\x00.svgz 01;35\x00.mng 01;35\x00.pcx 01;35\x00.mov 01;35\x00.mpg 01;35\x00.mpeg 01;35\x00.m2v 01;35\x00.mkv 01;35\x00.webm 01;35\x00.webp 01;35\x00.ogm 01;35\x00.mp4 01;35\x00.m4v 01;35\x00.mp4v 01;35\x00.vob 01;35\x00.qt 01;35\x00.nuv 01;35\x00.wmv 01;35\x00.asf 01;35\x00.rm 01;35\x00.rmvb 01;35\x00.flc 01;35\x00.avi 01;35\x00.fli 01;35\x00.flv 01;35\x00.gl 01;35\x00.dl 01;35\x00.xcf 01;35\x00.xwd 01;35\x00.yuv 01;35\x00.cgm 01;35\x00.emf 01;35\x00# https://wiki.xiph.org/MIME_Types_and_File_Extensions\x00.ogv 01;35\x00.ogx 01;35\x00# audio formats\x00.aac 00;36\x00.au 00;36\x00.flac 00;36\x00.m4a 00;36\x00.mid 00;36\x00.midi 00;36\x00.mka 00;36\x00.mp3 00;36\x00.mpc 00;36\x00.ogg 00;36\x00.ra 00;36\x00.wav 00;36\x00# https://wiki.xiph.org/MIME_Types_and_File_Extensions\x00.oga 00;36\x00.opus 00;36\x00.spx 00;36\x00.xspf 00;36\x00# backup files\x00*~ 00;90\x00*# 00;90\x00.bak 00;90\x00.old 00;90\x00.orig 00;90\x00.part 00;90\x00.rej 00;90\x00.swp 00;90\x00.tmp 00;90\x00.dpkg-dist 00;90\x00.dpkg-old 00;90\x00.ucf-dist 00;90\x00.ucf-new 00;90\x00.ucf-old 00;90\x00.rpmnew 00;90\x00.rpmorig 00;90\x00.rpmsave 00;90\x00# Subsequent TERM or COLORTERM entries, can be used to add / override\x00# config specific to those matching environment variables."; // 0x30fc
    int64_t v5; // 0x3060
    bool v6; // 0x3060
    bool v7; // 0x3060
    int32_t v8; // 0x3060
    int32_t v9; // 0x3060
    while (true) {
      lab_0x3100_2:
        // 0x3100
        v8 = v2;
        v6 = v3;
        v9 = v2;
        v7 = v3;
        v5 = v4;
        if (fp == NULL) {
            goto lab_0x3260;
        } else {
            goto lab_0x310d;
        }
    }
  lab_0x3260:;
    bool result = v7; // 0x326a
    if (v5 == (int64_t)"9.1.17-a351f") {
        // break -> 0x3444
        goto lab_0x3444_2;
    }
    int32_t v10 = v9; // 0x3280
    bool v11 = v7; // 0x3280
    int64_t v12 = v5 + 1 + function_2440(); // 0x3280
    int64_t v13 = v5; // 0x3280
    goto lab_0x3132;
  lab_0x310d:;
    bool v50 = v6;
    v10 = v8;
    v11 = v50;
    v12 = v4;
    v13 = 0;
    if (function_2520() < 1) {
        // 0x343a
        function_2340();
        result = v50;
        goto lab_0x3444_2;
    }
    goto lab_0x3132;
  lab_0x3132:
    // 0x3132
    v4 = v12;
    bool v14 = v11;
    v2 = v10;
    int64_t * v15 = (int64_t *)function_2670(); // 0x3137
    int64_t v16 = *v15; // 0x3137
    int64_t v17 = v13;
    unsigned char v18 = *(char *)v17; // 0x313d
    int64_t v19 = v17 + 1; // 0x3147
    while ((*(char *)(v16 + 1 + 2 * (int64_t)v18) & 32) != 0) {
        // 0x313d
        v17 = v19;
        v18 = *(char *)v17;
        v19 = v17 + 1;
    }
    int64_t v20 = v17; // 0x3060
    v3 = v14;
    bool v21; // 0x3060
    int32_t v22; // 0x3060
    int32_t v23; // 0x3060
    char * v24; // 0x3188
    char * v25; // 0x3200
    char v26; // 0x32f5
    switch (v18) {
        case 0: {
            goto lab_0x3100;
        }
        case 35: {
            goto lab_0x3100;
        }
        default: {
            int64_t v27 = v20 + 1; // 0x3160
            char * v28 = (char *)v27;
            unsigned char v29 = *v28; // 0x3160
            uint16_t v30 = *(int16_t *)(2 * (int64_t)v29 + v16); // 0x316b
            v20 = v27;
            while ((((char)(v30 / 0x2000) ^ 1) & (char)(v29 != 0)) != 0) {
                // 0x3160
                v27 = v20 + 1;
                v28 = (char *)v27;
                v29 = *v28;
                v30 = *(int16_t *)(2 * (int64_t)v29 + v16);
                v20 = v27;
            }
            // 0x317f
            v24 = ximemdup0((int32_t *)v17, v27 - v17);
            if (*v28 == 0) {
                goto lab_0x33c0;
            } else {
                int64_t v31 = *v15 + 1; // 0x31ab
                int64_t v32 = v27;
                int64_t v33 = v32 + 1; // 0x31a3
                unsigned char v34 = *(char *)v33; // 0x31a3
                while ((*(char *)(2 * (int64_t)v34 + v31) & 32) != 0) {
                    // 0x31a0
                    v32 = v33;
                    v33 = v32 + 1;
                    v34 = *(char *)v33;
                }
                int64_t v35 = v33; // 0x3060
                switch (v34) {
                    case 0: {
                        goto lab_0x33c0;
                    }
                    case 35: {
                        goto lab_0x33c0;
                    }
                    default: {
                        int64_t v36 = v35;
                        int64_t v37 = v36 + 1; // 0x31d3
                        char v38 = *(char *)v37; // 0x31d7
                        while (v38 != 0 && v38 != 35) {
                            // 0x31d0
                            v36 = v37;
                            v37 = v36 + 1;
                            v38 = *(char *)v37;
                        }
                        int64_t v39 = v36; // 0x31f4
                        int64_t v40 = v36; // 0x31f4
                        if ((*(char *)(2 * (int64_t)*(char *)v36 + v31) & 32) != 0) {
                            int64_t v41 = v40 - 1; // 0x3288
                            v39 = v41;
                            v40 = v41;
                            while ((*(char *)(2 * (int64_t)*(char *)v41 + v31) & 32) != 0) {
                                // 0x3288
                                v41 = v40 - 1;
                                v39 = v41;
                                v40 = v41;
                            }
                        }
                        // 0x31fa
                        v25 = ximemdup0((int32_t *)v33, v39 - v32);
                        if (c_strcasecmp(v24, "TERM") != 0) {
                            // 0x32b0
                            if (c_strcasecmp(v24, "COLORTERM") != 0) {
                                if (v2 == 2) {
                                    char v42 = *v24; // 0x3410
                                    v23 = 1;
                                    v26 = v42;
                                    if (v42 != 46) {
                                        goto lab_0x3302;
                                    } else {
                                        goto lab_0x3425;
                                    }
                                } else {
                                    // 0x32ed
                                    v22 = 0;
                                    v21 = v14;
                                    if (v2 == 0) {
                                        goto lab_0x3243;
                                    } else {
                                        // 0x32f5
                                        v26 = *v24;
                                        v23 = v2;
                                        if (v26 == 46) {
                                            goto lab_0x3425;
                                        } else {
                                            goto lab_0x3302;
                                        }
                                    }
                                }
                            } else {
                                // 0x32c3
                                v22 = 2;
                                v21 = v14;
                                if (v2 == 2) {
                                    goto lab_0x3243;
                                } else {
                                    goto lab_0x322d;
                                }
                            }
                        } else {
                            // 0x321f
                            v22 = 2;
                            v21 = v14;
                            if (v2 == 2) {
                                goto lab_0x3243;
                            } else {
                                goto lab_0x322d;
                            }
                        }
                    }
                }
            }
        }
    }
  lab_0x3100:
    // 0x3100
    goto lab_0x3100_2;
  lab_0x33c0:
    // 0x33c0
    quotearg_n_style_colon();
    function_2420();
    function_25e0();
    function_2340();
    v3 = false;
    goto lab_0x3100;
  lab_0x3243:
    // 0x3243
    function_2340();
    function_2340();
    v8 = v22;
    v6 = v21;
    v9 = v22;
    v7 = v21;
    v5 = v4;
    if (fp != NULL) {
        goto lab_0x310d;
    } else {
        goto lab_0x3260;
    }
  lab_0x322d:
    // 0x322d
    v22 = 2 * (int32_t)((int32_t)function_24b0() == 0);
    v21 = v14;
    goto lab_0x3243;
  lab_0x3302:;
    int32_t v43 = v23;
    if (v26 == 42) {
        // 0x346c
        append_entry(0, v24, v25);
        v22 = v43;
        v21 = v14;
    } else {
        // 0x330a
        v22 = v43;
        v21 = v14;
        if (c_strcasecmp(v24, "OPTIONS") != 0) {
            // 0x3321
            v22 = v43;
            v21 = v14;
            if (c_strcasecmp(v24, "COLOR") != 0) {
                // 0x3338
                v22 = v43;
                v21 = v14;
                int64_t v44 = (int64_t)"NORMAL"; // 0x3349
                if (c_strcasecmp(v24, "EIGHTBIT") != 0) {
                    int64_t v45 = 0;
                    while (c_strcasecmp(v24, (char *)v44) != 0) {
                        int64_t v46 = v45 + 1; // 0x336c
                        int64_t v47 = *(int64_t *)(8 * v46 + (int64_t)&slack_codes); // 0x3377
                        int64_t v48 = v46; // 0x337e
                        v44 = v47;
                        if (v47 == 0) {
                            // 0x348f
                            v22 = 3;
                            v21 = v14;
                            if (v43 == 3) {
                                goto lab_0x3243;
                            } else {
                                if (filename == NULL) {
                                    // 0x34fa
                                    function_2420();
                                    goto lab_0x34be;
                                } else {
                                    // 0x34af
                                    quotearg_n_style_colon();
                                    goto lab_0x34be;
                                }
                            }
                        }
                        v45 = v48;
                    }
                    int64_t v49 = *(int64_t *)((0x100000000 * v45 >> 29) + (int64_t)&ls_codes); // 0x33ae
                    append_entry(0, (char *)v49, v25);
                    v22 = v43;
                    v21 = v14;
                }
            }
        }
    }
    goto lab_0x3243;
  lab_0x3425:
    // 0x3425
    append_entry(42, v24, v25);
    v22 = 1;
    v21 = v14;
    goto lab_0x3243;
  lab_0x3444_2:
    // 0x3444
    if (v1 != __readfsqword(40)) {
        // 0x3512
        return function_2450() % 2 != 0;
    }
    // 0x3458
    return result;
  lab_0x34be:
    // 0x34be
    function_2420();
    function_25e0();
    v22 = 1;
    v21 = false;
    goto lab_0x3243;
}