undefined dc_parse_stream(FILE *param_1,long param_2)

{
  byte *pbVar1;
  byte bVar2;
  ushort *puVar3;
  char cVar4;
  char cVar5;
  int iVar6;
  char *pcVar7;
  char *pcVar8;
  __ssize_t _Var9;
  ushort **ppuVar10;
  char *__ptr;
  char *__pattern;
  size_t sVar11;
  undefined8 uVar12;
  undefined8 uVar13;
  byte *pbVar14;
  long lVar15;
  byte *pbVar16;
  char *pcVar17;
  byte *pbVar18;
  long lVar19;
  long in_FS_OFFSET;
  char *local_80;
  undefined local_71;
  byte *local_50;
  size_t local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = (byte *)0x0;
  local_48 = 0;
  pcVar7 = getenv("TERM");
  if (pcVar7 == (char *)0x0) {
    local_80 = "none";
  }
  else {
    local_80 = "none";
    if (*pcVar7 != '\0') {
      local_80 = pcVar7;
    }
  }
  pcVar8 = getenv("COLORTERM");
  cVar4 = '\x03';
  local_71 = 1;
  pcVar7 = "";
  if (pcVar8 != (char *)0x0) {
    pcVar7 = pcVar8;
  }
  pcVar8 = "# Configuration file for dircolors, a utility to help you set the";
  lVar19 = 0;
LAB_00103100:
  lVar19 = lVar19 + 1;
  if (param_1 == (FILE *)0x0) goto LAB_00103260;
LAB_0010310d:
  _Var9 = __getdelim((char **)&local_50,&local_48,10,param_1);
  pbVar14 = (byte *)pcVar8;
  pbVar18 = local_50;
  if (_Var9 < 1) {
    free(local_50);
LAB_00103444:
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return local_71;
  }
  do {
    pcVar8 = (char *)pbVar14;
    ppuVar10 = __ctype_b_loc();
    while (bVar2 = *pbVar18, (*(byte *)((long)*ppuVar10 + (ulong)bVar2 * 2 + 1) & 0x20) != 0) {
      pbVar18 = pbVar18 + 1;
    }
    if ((bVar2 == 0) || (pbVar14 = pbVar18, bVar2 == 0x23)) goto LAB_00103100;
    do {
      pbVar1 = pbVar14 + 1;
      pbVar14 = pbVar14 + 1;
    } while ((*pbVar1 != 0 & ((byte)((*ppuVar10)[*pbVar1] >> 0xd) ^ 1)) != 0);
    __ptr = (char *)ximemdup0(pbVar18,(long)pbVar14 - (long)pbVar18);
    if (*pbVar14 == 0) {
LAB_001033c0:
      uVar12 = quotearg_n_style_colon(0,3,param_2);
      uVar13 = dcgettext(0,"%s:%lu: invalid line;  missing second token",5);
      error(0,0,uVar13,uVar12);
      free(__ptr);
      local_71 = 0;
      goto LAB_00103100;
    }
    puVar3 = *ppuVar10;
    do {
      pbVar18 = pbVar14;
      bVar2 = pbVar18[1];
      pbVar14 = pbVar18 + 1;
    } while ((*(byte *)((long)puVar3 + (ulong)bVar2 * 2 + 1) & 0x20) != 0);
    if ((bVar2 == 0) || (pbVar1 = pbVar14, bVar2 == 0x23)) goto LAB_001033c0;
    do {
      pbVar16 = pbVar1;
      pbVar1 = pbVar16 + 1;
    } while (pbVar16[1] != 0 && pbVar16[1] != 0x23);
    bVar2 = *(byte *)((long)puVar3 + (ulong)*pbVar16 * 2 + 1);
    while ((bVar2 & 0x20) != 0) {
      pbVar1 = pbVar16 + -1;
      pbVar16 = pbVar16 + -1;
      bVar2 = *(byte *)((long)puVar3 + (ulong)*pbVar1 * 2 + 1);
    }
    __pattern = (char *)ximemdup0(pbVar14,(long)pbVar16 - (long)pbVar18);
    iVar6 = c_strcasecmp(__ptr,"TERM");
    pcVar17 = local_80;
    if ((iVar6 == 0) || (iVar6 = c_strcasecmp(__ptr,"COLORTERM"), pcVar17 = pcVar7, iVar6 == 0)) {
      if (cVar4 != '\x02') {
        iVar6 = fnmatch(__pattern,pcVar17,0);
        cVar4 = (iVar6 == 0) * '\x02';
      }
    }
    else if (cVar4 == '\x02') {
      cVar5 = *__ptr;
      cVar4 = '\x01';
      if (cVar5 != '.') goto LAB_00103302;
LAB_00103425:
      append_entry(0x2a,__ptr,__pattern);
    }
    else if (cVar4 != '\0') {
      cVar5 = *__ptr;
      if (cVar5 == '.') goto LAB_00103425;
LAB_00103302:
      if (cVar5 == '*') {
        append_entry(0,__ptr,__pattern);
      }
      else {
        iVar6 = c_strcasecmp(__ptr,"OPTIONS");
        if (((iVar6 != 0) && (iVar6 = c_strcasecmp(__ptr,"COLOR"), iVar6 != 0)) &&
           (iVar6 = c_strcasecmp(__ptr,"EIGHTBIT"), iVar6 != 0)) {
          lVar15 = 0;
          pcVar17 = "NORMAL";
          do {
            iVar6 = c_strcasecmp(__ptr,pcVar17);
            if (iVar6 == 0) {
              append_entry(0,*(undefined8 *)(ls_codes + (long)(int)lVar15 * 8),__pattern);
              goto LAB_00103243;
            }
            lVar15 = lVar15 + 1;
            pcVar17 = *(char **)(slack_codes + lVar15 * 8);
          } while (pcVar17 != (char *)0x0);
          if (cVar4 != '\x03') {
            if (param_2 == 0) {
              uVar12 = dcgettext(0,"<internal>",5);
            }
            else {
              uVar12 = quotearg_n_style_colon(0,3);
            }
            uVar13 = dcgettext(0,"%s:%lu: unrecognized keyword %s",5);
            error(0,0,uVar13,uVar12,lVar19,__ptr);
            local_71 = 0;
            cVar4 = '\x01';
          }
        }
      }
    }
LAB_00103243:
    lVar19 = lVar19 + 1;
    free(__ptr);
    free(__pattern);
    if (param_1 != (FILE *)0x0) goto LAB_0010310d;
LAB_00103260:
    if (pcVar8 == "9.1.17-a351f") goto LAB_00103444;
    sVar11 = strlen(pcVar8);
    pbVar14 = (byte *)pcVar8 + sVar11 + 1;
    pbVar18 = (byte *)pcVar8;
  } while( true );
}