uint32_t dc_parse_stream(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, ...) {
    struct s0* r14_6;
    struct s0* v7;
    struct s0* rax8;
    struct s0* v9;
    struct s0* rax10;
    struct s0* v11;
    struct s0* rax12;
    struct s0* rax13;
    void* rsp14;
    uint32_t v15;
    struct s0* rax16;
    unsigned char v17;
    struct s0* rbx18;
    struct s0* r13_19;
    struct s0* v20;
    void* rax21;
    struct s0* rdi22;
    struct s0* r12_23;
    struct s0* rax24;
    void* rsp25;
    struct s0** rax26;
    struct s0** r15_27;
    struct s0* rbp28;
    int64_t rax29;
    int64_t rdx30;
    uint32_t eax31;
    uint32_t eax32;
    struct s0* rax33;
    void* rsp34;
    struct s0* r12_35;
    struct s0* r9_36;
    int64_t rax37;
    struct s0* rax38;
    struct s0* rax39;
    int64_t rdx40;
    struct s0* rsi41;
    uint32_t eax42;
    int64_t rax43;
    int64_t rax44;
    struct s0* rax45;
    struct s0* rbp46;
    int32_t eax47;
    void* rsp48;
    int32_t eax49;
    uint32_t eax50;
    int32_t eax51;
    int32_t eax52;
    int32_t eax53;
    struct s0* v54;
    struct s0* r14_55;
    int64_t rbp56;
    struct s0* r15_57;
    int32_t eax58;
    struct s0* rax59;
    void* rsp60;
    struct s0* r15_61;
    struct s0* rax62;
    struct s0* rax63;
    int32_t eax64;
    uint32_t eax65;

    r14_6 = rdi;
    v7 = rsi;
    rax8 = g28;
    v9 = rax8;
    rax10 = fun_2370("TERM", rsi, rdx, rcx, r8);
    if (!rax10) {
        v11 = reinterpret_cast<struct s0*>("none");
    } else {
        rax12 = reinterpret_cast<struct s0*>("none");
        if (*reinterpret_cast<struct s0**>(&rax10->f0)) {
            rax12 = rax10;
        }
        v11 = rax12;
    }
    rax13 = fun_2370("COLORTERM", rsi, rdx, rcx, r8);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8 - 8 + 8);
    v15 = 3;
    rax16 = reinterpret_cast<struct s0*>(0x8130);
    v17 = 1;
    if (rax13) {
        rax16 = rax13;
    }
    rbx18 = reinterpret_cast<struct s0*>("# Configuration file for dircolors, a utility to help you set the");
    *reinterpret_cast<int32_t*>(&r13_19) = 0;
    *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
    v20 = rax16;
    goto addr_3100_9;
    addr_343a_10:
    fun_2340(0, 0);
    addr_3444_11:
    rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax21) {
        fun_2450();
    } else {
        return static_cast<uint32_t>(v17);
    }
    while (rbx18 != "9.1.17-a351f") {
        rdi22 = rbx18;
        r12_23 = rbx18;
        rax24 = fun_2440(rdi22, rdi22);
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
        rbx18 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<unsigned char>(rax24) + 1);
        while (1) {
            rax26 = fun_2670(rdi22, rsi, rdx, rcx, rdi22, rsi, rdx, rcx);
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
            rcx = *rax26;
            r15_27 = rax26;
            while (*reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r12_23->f0)), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rdx) * 2 + 1) & 32)) {
                r12_23 = reinterpret_cast<struct s0*>(&r12_23->f1);
            }
            if (!*reinterpret_cast<signed char*>(&rdx) || *reinterpret_cast<signed char*>(&rdx) == 35) {
                addr_3100_9:
                r13_19 = reinterpret_cast<struct s0*>(&r13_19->f1);
                if (!r14_6) 
                    break;
            } else {
                rbp28 = r12_23;
                do {
                    *reinterpret_cast<uint32_t*>(&rax29) = rbp28->f1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
                    rbp28 = reinterpret_cast<struct s0*>(&rbp28->f1);
                    rdx30 = rax29;
                    eax31 = reinterpret_cast<uint16_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rax29 * 2)));
                    *reinterpret_cast<uint16_t*>(&eax31) = reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&eax31) >> 13);
                    eax32 = eax31 ^ 1;
                } while (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&rdx30))) & *reinterpret_cast<unsigned char*>(&eax32));
                rax33 = ximemdup0(r12_23, r12_23);
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
                r12_35 = rax33;
                if (!*reinterpret_cast<struct s0**>(&rbp28->f0)) 
                    goto addr_33c0_24; else 
                    goto addr_319a_25;
            }
            addr_310d_26:
            rsi = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 64);
            rdi22 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 56);
            rcx = r14_6;
            *reinterpret_cast<uint32_t*>(&rdx) = 10;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rax37 = fun_2520(rdi22, rsi, 10, rcx, r8, r9_36);
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
            if (reinterpret_cast<uint1_t>(rax37 < 0) | reinterpret_cast<uint1_t>(rax37 == 0)) 
                goto addr_343a_10;
            r12_23 = reinterpret_cast<struct s0*>(0);
            continue;
            addr_33c0_24:
            rax38 = quotearg_n_style_colon();
            rax39 = fun_2420();
            r8 = r13_19;
            rcx = rax38;
            *reinterpret_cast<int32_t*>(&rsi) = 0;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rdx = rax39;
            fun_25e0();
            fun_2340(r12_35);
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            v17 = 0;
            goto addr_3100_9;
            addr_319a_25:
            rcx = *r15_27;
            do {
                *reinterpret_cast<uint32_t*>(&rdx40) = rbp28->f1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx40) + 4) = 0;
                rbp28 = reinterpret_cast<struct s0*>(&rbp28->f1);
            } while (*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rdx40 * 2) + 1) & 32);
            if (!*reinterpret_cast<signed char*>(&rdx40)) 
                goto addr_33c0_24;
            if (*reinterpret_cast<signed char*>(&rdx40) == 35) 
                goto addr_33c0_24;
            rdx = rbp28;
            do {
                rsi41 = rdx;
                rdx = reinterpret_cast<struct s0*>(&rdx->f1);
                eax42 = rsi41->f1;
                *reinterpret_cast<unsigned char*>(&r8) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&eax42));
            } while (*reinterpret_cast<unsigned char*>(&r8) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax42) != 35)));
            *reinterpret_cast<uint32_t*>(&rax43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
            if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rax43 * 2) + 1) & 32)) 
                goto addr_31fa_34;
            while (*reinterpret_cast<uint32_t*>(&rax44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi41) + 0xffffffffffffffff), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0, rsi41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsi41) - 1), !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rax44 * 2) + 1) & 32)) {
            }
            addr_31fa_34:
            rax45 = ximemdup0(rbp28);
            rsi = reinterpret_cast<struct s0*>("TERM");
            rbp46 = rax45;
            eax47 = c_strcasecmp(r12_35, "TERM");
            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
            if (eax47) {
                rsi = reinterpret_cast<struct s0*>("COLORTERM");
                eax49 = c_strcasecmp(r12_35, "COLORTERM");
                rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                if (eax49) {
                    if (v15 != 2) {
                        if (!v15) 
                            goto addr_3243_40;
                        eax50 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r12_35->f0));
                        if (*reinterpret_cast<signed char*>(&eax50) == 46) 
                            goto addr_3425_42; else 
                            goto addr_3302_43;
                    }
                    eax50 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r12_35->f0));
                    v15 = 1;
                    if (*reinterpret_cast<signed char*>(&eax50) != 46) {
                        addr_3302_43:
                        if (*reinterpret_cast<signed char*>(&eax50) == 42) {
                            rdx = rbp46;
                            rsi = r12_35;
                            append_entry(0, rsi, rdx);
                            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                            goto addr_3243_40;
                        }
                    } else {
                        addr_3425_42:
                        rdx = rbp46;
                        rsi = r12_35;
                        append_entry(42, rsi, rdx);
                        rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                        goto addr_3243_40;
                    }
                    rsi = reinterpret_cast<struct s0*>("OPTIONS");
                    eax51 = c_strcasecmp(r12_35, "OPTIONS");
                    rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    if (!eax51 || ((rsi = reinterpret_cast<struct s0*>("COLOR"), eax52 = c_strcasecmp(r12_35, "COLOR"), rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8), eax52 == 0) || (rsi = reinterpret_cast<struct s0*>("EIGHTBIT"), eax53 = c_strcasecmp(r12_35, "EIGHTBIT"), rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8), eax53 == 0))) {
                        addr_3243_40:
                        r13_19 = reinterpret_cast<struct s0*>(&r13_19->f1);
                        fun_2340(r12_35, r12_35);
                        fun_2340(rbp46, rbp46);
                        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8 - 8 + 8);
                        if (r14_6) 
                            goto addr_310d_26; else 
                            break;
                    } else {
                        v54 = rbx18;
                        r14_55 = rbp46;
                        rsi = reinterpret_cast<struct s0*>("NORMAL");
                        rbp56 = 0;
                        r15_57 = r14_6;
                        do {
                            eax58 = c_strcasecmp(r12_35, rsi);
                            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                            if (!eax58) 
                                break;
                            ++rbp56;
                            rsi = *reinterpret_cast<struct s0**>(0xcaa0 + rbp56 * 8);
                        } while (rsi);
                        goto addr_348f_50;
                    }
                } else {
                    if (v15 == 2) 
                        goto addr_3243_40;
                    rsi = v20;
                    *reinterpret_cast<uint32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    goto addr_322d_53;
                }
            } else {
                if (v15 == 2) 
                    goto addr_3243_40;
                rsi = v11;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                goto addr_322d_53;
            }
            rbp46 = r14_55;
            r14_6 = r15_57;
            rdx = rbp46;
            rbx18 = v54;
            rsi = *reinterpret_cast<struct s0**>(0xc960 + *reinterpret_cast<int32_t*>(&rbp56) * 8);
            append_entry(0, rsi, rdx);
            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
            goto addr_3243_40;
            addr_348f_50:
            rbp46 = r14_55;
            rbx18 = v54;
            r14_6 = r15_57;
            if (v15 != 3) {
                if (!v7) {
                    rax59 = fun_2420();
                    rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    r15_61 = rax59;
                } else {
                    rax62 = quotearg_n_style_colon();
                    rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    r15_61 = rax62;
                }
                rax63 = fun_2420();
                r9_36 = r12_35;
                r8 = r13_19;
                rcx = r15_61;
                rdx = rax63;
                *reinterpret_cast<int32_t*>(&rsi) = 0;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_25e0();
                rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8 - 8 + 8);
                v17 = 0;
                v15 = 1;
                goto addr_3243_40;
            }
            addr_322d_53:
            eax64 = fun_24b0(rbp46, rsi);
            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
            eax65 = reinterpret_cast<uint1_t>(eax64 == 0);
            v15 = eax65 + eax65;
            goto addr_3243_40;
        }
    }
    goto addr_3444_11;
}