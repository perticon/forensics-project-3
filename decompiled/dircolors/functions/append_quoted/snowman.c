uint32_t append_quoted(struct s0* rdi, struct s0* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t ebp5;
    struct s0* rbx6;
    int1_t zf7;
    signed char* rax8;
    int1_t zf9;
    uint32_t edx10;
    signed char* rsi11;
    signed char* rcx12;
    uint32_t edx13;
    signed char* rax14;
    int1_t zf15;
    signed char* rax16;
    int1_t zf17;
    int1_t zf18;

    edx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0));
    if (!*reinterpret_cast<signed char*>(&edx3)) {
        return eax4;
    } else {
        ebp5 = 1;
        rbx6 = rdi;
        while (1) {
            zf7 = print_ls_colors == 0;
            rax8 = gd138;
            if (!zf7) {
                addr_2d40_5:
                zf9 = gd140 == rax8;
                if (zf9) {
                    addr_2dc0_6:
                    _obstack_newchunk(0xd120, 1);
                    rax8 = gd138;
                    goto addr_2d49_7;
                } else {
                    addr_2d49_7:
                    rbx6 = reinterpret_cast<struct s0*>(&rbx6->f1);
                    gd138 = rax8 + 1;
                    edx10 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx6) + 0xffffffffffffffff);
                    *rax8 = *reinterpret_cast<signed char*>(&edx10);
                    edx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx6->f0));
                    if (!*reinterpret_cast<signed char*>(&edx3)) 
                        break; else 
                        continue;
                }
            } else {
                rsi11 = gd140;
                rcx12 = rax8;
                if (*reinterpret_cast<signed char*>(&edx3) == 61) 
                    goto addr_2d98_9;
                if (*reinterpret_cast<signed char*>(&edx3) <= 61) 
                    goto addr_2d8a_11;
            }
            edx13 = edx3 & 0xfffffffd;
            ebp5 = ebp5 ^ 1;
            if (*reinterpret_cast<signed char*>(&edx13) != 92) {
                ebp5 = 1;
            }
            goto addr_2d40_5;
            addr_2d8a_11:
            if (*reinterpret_cast<signed char*>(&edx3) == 39) {
                if (rsi11 == rax8) {
                    _obstack_newchunk(0xd120, 1);
                    rcx12 = gd138;
                }
                gd138 = rcx12 + 1;
                *rcx12 = 39;
                rax14 = gd138;
                zf15 = gd140 == rax14;
                if (zf15) {
                    _obstack_newchunk(0xd120, 1);
                    rax14 = gd138;
                }
                gd138 = rax14 + 1;
                *rax14 = 92;
                rax16 = gd138;
                zf17 = gd140 == rax16;
                if (zf17) {
                    _obstack_newchunk(0xd120, 1);
                    rax16 = gd138;
                }
                ebp5 = 1;
                gd138 = rax16 + 1;
                *rax16 = 39;
                rax8 = gd138;
                goto addr_2d40_5;
            } else {
                if (*reinterpret_cast<signed char*>(&edx3) != 58) {
                    ebp5 = 1;
                    goto addr_2d40_5;
                } else {
                    addr_2d98_9:
                    if (*reinterpret_cast<signed char*>(&ebp5)) {
                        if (rsi11 == rax8) {
                            _obstack_newchunk(0xd120, 1);
                            rcx12 = gd138;
                        }
                        gd138 = rcx12 + 1;
                        *rcx12 = 92;
                        rax8 = gd138;
                        zf18 = gd140 == rax8;
                        if (!zf18) 
                            goto addr_2d49_7; else 
                            goto addr_2dc0_6;
                    }
                }
            }
        }
        return *reinterpret_cast<uint32_t*>(&rax8);
    }
}