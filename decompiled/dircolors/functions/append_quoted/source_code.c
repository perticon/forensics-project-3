append_quoted (char const *str)
{
  bool need_backslash = true;

  while (*str != '\0')
    {
      if (! print_ls_colors)
        switch (*str)
          {
          case '\'':
            APPEND_CHAR ('\'');
            APPEND_CHAR ('\\');
            APPEND_CHAR ('\'');
            need_backslash = true;
            break;

          case '\\':
          case '^':
            need_backslash = !need_backslash;
            break;

          case ':':
          case '=':
            if (need_backslash)
              APPEND_CHAR ('\\');
            FALLTHROUGH;

          default:
            need_backslash = true;
            break;
          }

      APPEND_CHAR (*str);
      ++str;
    }
}