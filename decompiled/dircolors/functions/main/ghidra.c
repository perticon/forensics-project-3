undefined  [16] main(int param_1,char **param_2)

{
  bool bVar1;
  void *__ptr;
  byte bVar2;
  int iVar3;
  long lVar4;
  void *pvVar5;
  int *piVar6;
  undefined8 uVar7;
  char *__s;
  undefined *puVar8;
  char *pcVar9;
  size_t sVar10;
  char cVar11;
  undefined8 uStack56;
  
  bVar1 = false;
  cVar11 = '\x02';
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar8 = &DAT_0010816b;
  textdomain("coreutils");
  atexit(close_stdout);
  do {
    while( true ) {
      iVar3 = getopt_long(param_1,param_2,puVar8,long_options,0);
      if (iVar3 == -1) break;
      if (iVar3 == 99) {
        cVar11 = '\x01';
      }
      else if (iVar3 < 100) {
        if (iVar3 == -0x82) {
          usage(0);
LAB_00102ac4:
          if (param_1 < 2) goto LAB_00102890;
          goto LAB_00102acd;
        }
        if (iVar3 != 0x62) {
          if (iVar3 == -0x83) {
            version_etc(stdout,"dircolors","GNU coreutils",Version,"H. Peter Anvin",0);
                    /* WARNING: Subroutine does not return */
            exit(0);
          }
          goto LAB_00102797;
        }
        cVar11 = '\0';
      }
      else {
        if (iVar3 != 0x70) goto LAB_001027a8;
        bVar1 = true;
      }
    }
    param_1 = param_1 - optind;
    param_2 = param_2 + optind;
    if ((print_ls_colors != '\0') || (bVar1)) {
      if (cVar11 == '\x02') {
        if (!bVar1) goto LAB_00102ac4;
        pcVar9 = "options --print-database and --print-ls-colors are mutually exclusive";
        if (print_ls_colors == '\0') {
          if (0 < param_1) goto LAB_00102b4c;
          for (pcVar9 = "# Configuration file for dircolors, a utility to help you set the";
              pcVar9 + -0x108800 < (char *)0x12c8; pcVar9 = pcVar9 + sVar10 + 1) {
            puts(pcVar9);
            sVar10 = strlen(pcVar9);
          }
LAB_00102a3e:
          bVar2 = 1;
LAB_00102a43:
          return CONCAT88(uStack56,(ulong)(bVar2 ^ 1));
        }
      }
      else {
        pcVar9 = 
        "the options to output non shell syntax,\nand to select a shell syntax are mutually exclusive"
        ;
      }
      uVar7 = dcgettext(0,pcVar9,5);
      error(0,0,uVar7);
    }
    else {
      if (param_1 < 2) {
        if (cVar11 == '\x02') {
          pcVar9 = getenv("SHELL");
          if ((pcVar9 == (char *)0x0) || (*pcVar9 == '\0')) {
            uVar7 = dcgettext(0,"no SHELL environment variable, and no shell type option given",5);
            error(1,0,uVar7);
LAB_00102b4c:
            puVar8 = (undefined *)quote(*param_2);
            uVar7 = dcgettext(0,"extra operand %s",5);
            error(0,0,uVar7,puVar8);
            uVar7 = dcgettext(0,"file operands cannot be combined with --print-database (-p)",5);
            __fprintf_chk(stderr,1,"%s\n",uVar7);
            goto LAB_00102797;
          }
          pcVar9 = (char *)last_component();
          iVar3 = strcmp(pcVar9,"csh");
          if (iVar3 == 0) {
            cVar11 = '\x01';
          }
          else {
            iVar3 = strcmp(pcVar9,"tcsh");
            cVar11 = iVar3 == 0;
          }
        }
LAB_00102890:
        _obstack_begin(lsc_obstack,0,0,PTR_malloc_0010cfe8,PTR_free_0010cfc8);
        if (param_1 != 0) {
          pcVar9 = *param_2;
          iVar3 = strcmp(pcVar9,"-");
          if (iVar3 == 0) {
LAB_001028f5:
            bVar2 = dc_parse_stream(stdin,pcVar9);
            iVar3 = rpl_fclose(stdin);
            if (iVar3 == 0) goto LAB_00102913;
          }
          else {
            lVar4 = freopen_safer(pcVar9,"r",stdin);
            if (lVar4 != 0) goto LAB_001028f5;
          }
          bVar2 = 0;
          uVar7 = quotearg_n_style_colon(0,3,pcVar9);
          piVar6 = __errno_location();
          error(0,*piVar6,"%s",uVar7);
          goto LAB_00102a43;
        }
        bVar2 = dc_parse_stream(0,0);
LAB_00102913:
        __ptr = lsc_obstack._16_8_;
        if (bVar2 == 0) goto LAB_00102a43;
        sVar10 = (long)lsc_obstack._24_8_ - (long)lsc_obstack._16_8_;
        if (lsc_obstack._24_8_ == lsc_obstack._16_8_) {
          lsc_obstack[80] = lsc_obstack[80] | 2;
        }
        pcVar9 = "\';\nexport LS_COLORS\n";
        pvVar5 = (void *)((long)lsc_obstack._24_8_ + lsc_obstack._48_8_ & ~lsc_obstack._48_8_);
        lsc_obstack._16_8_ = lsc_obstack._32_8_;
        if ((ulong)((long)pvVar5 - lsc_obstack._8_8_) <=
            (ulong)((long)lsc_obstack._32_8_ - lsc_obstack._8_8_)) {
          lsc_obstack._16_8_ = pvVar5;
        }
        if (cVar11 != '\0') {
          pcVar9 = "\'\n";
        }
        __s = "setenv LS_COLORS \'";
        if (cVar11 == '\0') {
          __s = "LS_COLORS=\'";
        }
        lsc_obstack._24_8_ = lsc_obstack._16_8_;
        if (print_ls_colors == '\0') {
          fputs_unlocked(__s,stdout);
        }
        fwrite_unlocked(__ptr,1,sVar10,stdout);
        if (print_ls_colors != '\0') goto LAB_00102a3e;
        fputs_unlocked(pcVar9,stdout);
        goto LAB_00102a43;
      }
LAB_00102acd:
      puVar8 = (undefined *)quote(param_2[1]);
      uVar7 = dcgettext(0,"extra operand %s",5);
      error(0,0,uVar7,puVar8);
    }
LAB_00102797:
    do {
      iVar3 = usage();
LAB_001027a8:
    } while (iVar3 != 0x80);
    print_ls_colors = '\x01';
  } while( true );
}