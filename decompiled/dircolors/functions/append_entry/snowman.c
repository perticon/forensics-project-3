void append_entry(int32_t edi, struct s0* rsi, struct s0* rdx) {
    struct s0* r13_4;
    int32_t ebx5;
    int1_t zf6;
    signed char* rax7;
    int1_t zf8;
    signed char* rax9;
    int1_t zf10;
    uint32_t eax11;
    signed char* rdx12;
    int1_t zf13;
    uint1_t cf14;
    uint32_t eax15;
    int1_t zf16;
    signed char* rax17;
    int1_t zf18;
    uint1_t cf19;
    int1_t zf20;

    r13_4 = rsi;
    ebx5 = edi;
    zf6 = print_ls_colors == 0;
    if (!zf6) {
        append_quoted(0x8004, rsi);
        append_quoted(rdx, rsi);
        rax7 = gd138;
        zf8 = gd140 == rax7;
        if (zf8) {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            _obstack_newchunk(0xd120, 1);
            rax7 = gd138;
        }
        gd138 = rax7 + 1;
        *rax7 = 0x6d;
    }
    if (*reinterpret_cast<signed char*>(&ebx5)) {
        rax9 = gd138;
        zf10 = gd140 == rax9;
        if (zf10) {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            _obstack_newchunk(0xd120, 1);
            rax9 = gd138;
        }
        gd138 = rax9 + 1;
        *rax9 = *reinterpret_cast<signed char*>(&ebx5);
    }
    eax11 = append_quoted(r13_4, rsi);
    rdx12 = gd138;
    zf13 = gd140 == rdx12;
    if (zf13) {
        *reinterpret_cast<int32_t*>(&rsi) = 1;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax11 = _obstack_newchunk(0xd120, 1);
        rdx12 = gd138;
    }
    cf14 = reinterpret_cast<uint1_t>(print_ls_colors < 1);
    gd138 = rdx12 + 1;
    eax15 = (eax11 - (eax11 + reinterpret_cast<uint1_t>(eax11 < eax11 + cf14)) & 52) + 9;
    *rdx12 = *reinterpret_cast<signed char*>(&eax15);
    append_quoted(rdx, rsi);
    zf16 = print_ls_colors == 0;
    if (!zf16) {
        append_quoted(0x8007, rsi);
        rax17 = gd138;
        zf18 = gd140 == rax17;
        if (zf18) {
            addr_2fe0_13:
            _obstack_newchunk(0xd120, 1);
            rax17 = gd138;
            goto addr_2fc4_14;
        } else {
            addr_2fc4_14:
            cf19 = reinterpret_cast<uint1_t>(print_ls_colors < 1);
            *reinterpret_cast<uint32_t*>(&rdx12) = (*reinterpret_cast<uint32_t*>(&rdx12) - (*reinterpret_cast<uint32_t*>(&rdx12) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx12) < *reinterpret_cast<uint32_t*>(&rdx12) + cf19)) & 48) + 10;
        }
    } else {
        rax17 = gd138;
        zf20 = gd140 == rax17;
        *reinterpret_cast<uint32_t*>(&rdx12) = 58;
        if (zf20) 
            goto addr_2fe0_13;
    }
    gd138 = rax17 + 1;
    *rax17 = *reinterpret_cast<signed char*>(&rdx12);
    return;
}