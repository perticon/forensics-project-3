void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002640(fn0000000000002420(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000357E;
	}
	fn00000000000025D0(fn0000000000002420(0x05, "Usage: %s [OPTION]... [FILE]\n", null), 0x01);
	fn0000000000002500(stdout, fn0000000000002420(0x05, "Output commands to set the LS_COLORS environment variable.\n\nDetermine format of output:\n  -b, --sh, --bourne-shell    output Bourne shell code to set LS_COLORS\n  -c, --csh, --c-shell        output C shell code to set LS_COLORS\n  -p, --print-database        output defaults\n      --print-ls-colors       output fully escaped colors for display\n", null));
	fn0000000000002500(stdout, fn0000000000002420(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002500(stdout, fn0000000000002420(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002500(stdout, fn0000000000002420(0x05, "\nIf FILE is specified, read it to determine which colors to use for which\nfile types and extensions.  Otherwise, a precompiled database is used.\nFor details on the format of these files, run 'dircolors --print-database'.\n", null));
	struct Eq_1645 * rbx_152 = fp - 0xB8 + 16;
	do
	{
		char * rsi_154 = rbx_152->qw0000;
		++rbx_152;
	} while (rsi_154 != null && fn0000000000002530(rsi_154, 0x8044) != 0x00);
	ptr64 r13_167 = rbx_152->qw0008;
	if (r13_167 != 0x00)
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_38 rax_254 = fn00000000000025C0(null, 0x05);
		if (rax_254 == 0x00 || fn00000000000023A0(0x03, "en_", rax_254) == 0x00)
			goto l0000000000003756;
	}
	else
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_38 rax_196 = fn00000000000025C0(null, 0x05);
		if (rax_196 == 0x00 || fn00000000000023A0(0x03, "en_", rax_196) == 0x00)
		{
			fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003793:
			fn00000000000025D0(fn0000000000002420(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000357E:
			fn0000000000002620(edi);
		}
		r13_167 = 0x8044;
	}
	fn0000000000002500(stdout, fn0000000000002420(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003756:
	fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003793;
}