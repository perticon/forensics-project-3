dc_parse_file (char const *filename)
{
  bool ok;

  if (! STREQ (filename, "-") && freopen (filename, "r", stdin) == NULL)
    {
      error (0, errno, "%s", quotef (filename));
      return false;
    }

  ok = dc_parse_stream (stdin, filename);

  if (fclose (stdin) != 0)
    {
      error (0, errno, "%s", quotef (filename));
      return false;
    }

  return ok;
}