
struct s0 {
    struct s0* f0;
    unsigned char f1;
    signed char f2;
    signed char[5] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    struct s0* f18;
    signed char[7] pad32;
    struct s0* f20;
    signed char[31] pad64;
    int64_t f40;
    struct s0* f48;
    signed char[7] pad80;
    unsigned char f50;
};

struct s0* g28;

struct s0* fun_2370(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

void fun_2340(struct s0* rdi, ...);

void fun_2450();

struct s0* fun_2440(struct s0* rdi, ...);

struct s0** fun_2670(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, ...);

struct s0* ximemdup0(struct s0* rdi, ...);

int64_t fun_2520(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

struct s0* quotearg_n_style_colon();

struct s0* fun_2420();

void fun_25e0();

int32_t c_strcasecmp(struct s0* rdi, struct s0* rsi);

void append_entry(int32_t edi, struct s0* rsi, struct s0* rdx);

int32_t fun_24b0(struct s0* rdi, struct s0* rsi);

uint32_t dc_parse_stream(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, ...) {
    struct s0* r14_6;
    struct s0* v7;
    struct s0* rax8;
    struct s0* v9;
    struct s0* rax10;
    struct s0* v11;
    struct s0* rax12;
    struct s0* rax13;
    void* rsp14;
    uint32_t v15;
    struct s0* rax16;
    unsigned char v17;
    struct s0* rbx18;
    struct s0* r13_19;
    struct s0* v20;
    void* rax21;
    struct s0* rdi22;
    struct s0* r12_23;
    struct s0* rax24;
    void* rsp25;
    struct s0** rax26;
    struct s0** r15_27;
    struct s0* rbp28;
    int64_t rax29;
    int64_t rdx30;
    uint32_t eax31;
    uint32_t eax32;
    struct s0* rax33;
    void* rsp34;
    struct s0* r12_35;
    struct s0* r9_36;
    int64_t rax37;
    struct s0* rax38;
    struct s0* rax39;
    int64_t rdx40;
    struct s0* rsi41;
    uint32_t eax42;
    int64_t rax43;
    int64_t rax44;
    struct s0* rax45;
    struct s0* rbp46;
    int32_t eax47;
    void* rsp48;
    int32_t eax49;
    uint32_t eax50;
    int32_t eax51;
    int32_t eax52;
    int32_t eax53;
    struct s0* v54;
    struct s0* r14_55;
    int64_t rbp56;
    struct s0* r15_57;
    int32_t eax58;
    struct s0* rax59;
    void* rsp60;
    struct s0* r15_61;
    struct s0* rax62;
    struct s0* rax63;
    int32_t eax64;
    uint32_t eax65;

    r14_6 = rdi;
    v7 = rsi;
    rax8 = g28;
    v9 = rax8;
    rax10 = fun_2370("TERM", rsi, rdx, rcx, r8);
    if (!rax10) {
        v11 = reinterpret_cast<struct s0*>("none");
    } else {
        rax12 = reinterpret_cast<struct s0*>("none");
        if (*reinterpret_cast<struct s0**>(&rax10->f0)) {
            rax12 = rax10;
        }
        v11 = rax12;
    }
    rax13 = fun_2370("COLORTERM", rsi, rdx, rcx, r8);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8 - 8 + 8);
    v15 = 3;
    rax16 = reinterpret_cast<struct s0*>(0x8130);
    v17 = 1;
    if (rax13) {
        rax16 = rax13;
    }
    rbx18 = reinterpret_cast<struct s0*>("# Configuration file for dircolors, a utility to help you set the");
    *reinterpret_cast<int32_t*>(&r13_19) = 0;
    *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
    v20 = rax16;
    goto addr_3100_9;
    addr_343a_10:
    fun_2340(0, 0);
    addr_3444_11:
    rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax21) {
        fun_2450();
    } else {
        return static_cast<uint32_t>(v17);
    }
    while (rbx18 != "9.1.17-a351f") {
        rdi22 = rbx18;
        r12_23 = rbx18;
        rax24 = fun_2440(rdi22, rdi22);
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
        rbx18 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<unsigned char>(rax24) + 1);
        while (1) {
            rax26 = fun_2670(rdi22, rsi, rdx, rcx, rdi22, rsi, rdx, rcx);
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
            rcx = *rax26;
            r15_27 = rax26;
            while (*reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r12_23->f0)), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rdx) * 2 + 1) & 32)) {
                r12_23 = reinterpret_cast<struct s0*>(&r12_23->f1);
            }
            if (!*reinterpret_cast<signed char*>(&rdx) || *reinterpret_cast<signed char*>(&rdx) == 35) {
                addr_3100_9:
                r13_19 = reinterpret_cast<struct s0*>(&r13_19->f1);
                if (!r14_6) 
                    break;
            } else {
                rbp28 = r12_23;
                do {
                    *reinterpret_cast<uint32_t*>(&rax29) = rbp28->f1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
                    rbp28 = reinterpret_cast<struct s0*>(&rbp28->f1);
                    rdx30 = rax29;
                    eax31 = reinterpret_cast<uint16_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rax29 * 2)));
                    *reinterpret_cast<uint16_t*>(&eax31) = reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&eax31) >> 13);
                    eax32 = eax31 ^ 1;
                } while (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&rdx30))) & *reinterpret_cast<unsigned char*>(&eax32));
                rax33 = ximemdup0(r12_23, r12_23);
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
                r12_35 = rax33;
                if (!*reinterpret_cast<struct s0**>(&rbp28->f0)) 
                    goto addr_33c0_24; else 
                    goto addr_319a_25;
            }
            addr_310d_26:
            rsi = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 64);
            rdi22 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 56);
            rcx = r14_6;
            *reinterpret_cast<uint32_t*>(&rdx) = 10;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rax37 = fun_2520(rdi22, rsi, 10, rcx, r8, r9_36);
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
            if (reinterpret_cast<uint1_t>(rax37 < 0) | reinterpret_cast<uint1_t>(rax37 == 0)) 
                goto addr_343a_10;
            r12_23 = reinterpret_cast<struct s0*>(0);
            continue;
            addr_33c0_24:
            rax38 = quotearg_n_style_colon();
            rax39 = fun_2420();
            r8 = r13_19;
            rcx = rax38;
            *reinterpret_cast<int32_t*>(&rsi) = 0;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rdx = rax39;
            fun_25e0();
            fun_2340(r12_35);
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            v17 = 0;
            goto addr_3100_9;
            addr_319a_25:
            rcx = *r15_27;
            do {
                *reinterpret_cast<uint32_t*>(&rdx40) = rbp28->f1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx40) + 4) = 0;
                rbp28 = reinterpret_cast<struct s0*>(&rbp28->f1);
            } while (*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rdx40 * 2) + 1) & 32);
            if (!*reinterpret_cast<signed char*>(&rdx40)) 
                goto addr_33c0_24;
            if (*reinterpret_cast<signed char*>(&rdx40) == 35) 
                goto addr_33c0_24;
            rdx = rbp28;
            do {
                rsi41 = rdx;
                rdx = reinterpret_cast<struct s0*>(&rdx->f1);
                eax42 = rsi41->f1;
                *reinterpret_cast<unsigned char*>(&r8) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&eax42));
            } while (*reinterpret_cast<unsigned char*>(&r8) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax42) != 35)));
            *reinterpret_cast<uint32_t*>(&rax43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
            if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rax43 * 2) + 1) & 32)) 
                goto addr_31fa_34;
            while (*reinterpret_cast<uint32_t*>(&rax44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi41) + 0xffffffffffffffff), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0, rsi41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsi41) - 1), !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(rax44 * 2) + 1) & 32)) {
            }
            addr_31fa_34:
            rax45 = ximemdup0(rbp28);
            rsi = reinterpret_cast<struct s0*>("TERM");
            rbp46 = rax45;
            eax47 = c_strcasecmp(r12_35, "TERM");
            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
            if (eax47) {
                rsi = reinterpret_cast<struct s0*>("COLORTERM");
                eax49 = c_strcasecmp(r12_35, "COLORTERM");
                rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                if (eax49) {
                    if (v15 != 2) {
                        if (!v15) 
                            goto addr_3243_40;
                        eax50 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r12_35->f0));
                        if (*reinterpret_cast<signed char*>(&eax50) == 46) 
                            goto addr_3425_42; else 
                            goto addr_3302_43;
                    }
                    eax50 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r12_35->f0));
                    v15 = 1;
                    if (*reinterpret_cast<signed char*>(&eax50) != 46) {
                        addr_3302_43:
                        if (*reinterpret_cast<signed char*>(&eax50) == 42) {
                            rdx = rbp46;
                            rsi = r12_35;
                            append_entry(0, rsi, rdx);
                            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                            goto addr_3243_40;
                        }
                    } else {
                        addr_3425_42:
                        rdx = rbp46;
                        rsi = r12_35;
                        append_entry(42, rsi, rdx);
                        rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                        goto addr_3243_40;
                    }
                    rsi = reinterpret_cast<struct s0*>("OPTIONS");
                    eax51 = c_strcasecmp(r12_35, "OPTIONS");
                    rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    if (!eax51 || ((rsi = reinterpret_cast<struct s0*>("COLOR"), eax52 = c_strcasecmp(r12_35, "COLOR"), rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8), eax52 == 0) || (rsi = reinterpret_cast<struct s0*>("EIGHTBIT"), eax53 = c_strcasecmp(r12_35, "EIGHTBIT"), rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8), eax53 == 0))) {
                        addr_3243_40:
                        r13_19 = reinterpret_cast<struct s0*>(&r13_19->f1);
                        fun_2340(r12_35, r12_35);
                        fun_2340(rbp46, rbp46);
                        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8 - 8 + 8);
                        if (r14_6) 
                            goto addr_310d_26; else 
                            break;
                    } else {
                        v54 = rbx18;
                        r14_55 = rbp46;
                        rsi = reinterpret_cast<struct s0*>("NORMAL");
                        rbp56 = 0;
                        r15_57 = r14_6;
                        do {
                            eax58 = c_strcasecmp(r12_35, rsi);
                            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                            if (!eax58) 
                                break;
                            ++rbp56;
                            rsi = *reinterpret_cast<struct s0**>(0xcaa0 + rbp56 * 8);
                        } while (rsi);
                        goto addr_348f_50;
                    }
                } else {
                    if (v15 == 2) 
                        goto addr_3243_40;
                    rsi = v20;
                    *reinterpret_cast<uint32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    goto addr_322d_53;
                }
            } else {
                if (v15 == 2) 
                    goto addr_3243_40;
                rsi = v11;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                goto addr_322d_53;
            }
            rbp46 = r14_55;
            r14_6 = r15_57;
            rdx = rbp46;
            rbx18 = v54;
            rsi = *reinterpret_cast<struct s0**>(0xc960 + *reinterpret_cast<int32_t*>(&rbp56) * 8);
            append_entry(0, rsi, rdx);
            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
            goto addr_3243_40;
            addr_348f_50:
            rbp46 = r14_55;
            rbx18 = v54;
            r14_6 = r15_57;
            if (v15 != 3) {
                if (!v7) {
                    rax59 = fun_2420();
                    rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    r15_61 = rax59;
                } else {
                    rax62 = quotearg_n_style_colon();
                    rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    r15_61 = rax62;
                }
                rax63 = fun_2420();
                r9_36 = r12_35;
                r8 = r13_19;
                rcx = r15_61;
                rdx = rax63;
                *reinterpret_cast<int32_t*>(&rsi) = 0;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_25e0();
                rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8 - 8 + 8);
                v17 = 0;
                v15 = 1;
                goto addr_3243_40;
            }
            addr_322d_53:
            eax64 = fun_24b0(rbp46, rsi);
            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
            eax65 = reinterpret_cast<uint1_t>(eax64 == 0);
            v15 = eax65 + eax65;
            goto addr_3243_40;
        }
    }
    goto addr_3444_11;
}

unsigned char print_ls_colors = 0;

signed char* gd138 = reinterpret_cast<signed char*>(0);

signed char* gd140 = reinterpret_cast<signed char*>(0);

uint32_t _obstack_newchunk(int64_t rdi, struct s0* rsi);

uint32_t append_quoted(struct s0* rdi, struct s0* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t ebp5;
    struct s0* rbx6;
    int1_t zf7;
    signed char* rax8;
    int1_t zf9;
    uint32_t edx10;
    signed char* rsi11;
    signed char* rcx12;
    uint32_t edx13;
    signed char* rax14;
    int1_t zf15;
    signed char* rax16;
    int1_t zf17;
    int1_t zf18;

    edx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0));
    if (!*reinterpret_cast<signed char*>(&edx3)) {
        return eax4;
    } else {
        ebp5 = 1;
        rbx6 = rdi;
        while (1) {
            zf7 = print_ls_colors == 0;
            rax8 = gd138;
            if (!zf7) {
                addr_2d40_5:
                zf9 = gd140 == rax8;
                if (zf9) {
                    addr_2dc0_6:
                    _obstack_newchunk(0xd120, 1);
                    rax8 = gd138;
                    goto addr_2d49_7;
                } else {
                    addr_2d49_7:
                    rbx6 = reinterpret_cast<struct s0*>(&rbx6->f1);
                    gd138 = rax8 + 1;
                    edx10 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx6) + 0xffffffffffffffff);
                    *rax8 = *reinterpret_cast<signed char*>(&edx10);
                    edx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx6->f0));
                    if (!*reinterpret_cast<signed char*>(&edx3)) 
                        break; else 
                        continue;
                }
            } else {
                rsi11 = gd140;
                rcx12 = rax8;
                if (*reinterpret_cast<signed char*>(&edx3) == 61) 
                    goto addr_2d98_9;
                if (*reinterpret_cast<signed char*>(&edx3) <= 61) 
                    goto addr_2d8a_11;
            }
            edx13 = edx3 & 0xfffffffd;
            ebp5 = ebp5 ^ 1;
            if (*reinterpret_cast<signed char*>(&edx13) != 92) {
                ebp5 = 1;
            }
            goto addr_2d40_5;
            addr_2d8a_11:
            if (*reinterpret_cast<signed char*>(&edx3) == 39) {
                if (rsi11 == rax8) {
                    _obstack_newchunk(0xd120, 1);
                    rcx12 = gd138;
                }
                gd138 = rcx12 + 1;
                *rcx12 = 39;
                rax14 = gd138;
                zf15 = gd140 == rax14;
                if (zf15) {
                    _obstack_newchunk(0xd120, 1);
                    rax14 = gd138;
                }
                gd138 = rax14 + 1;
                *rax14 = 92;
                rax16 = gd138;
                zf17 = gd140 == rax16;
                if (zf17) {
                    _obstack_newchunk(0xd120, 1);
                    rax16 = gd138;
                }
                ebp5 = 1;
                gd138 = rax16 + 1;
                *rax16 = 39;
                rax8 = gd138;
                goto addr_2d40_5;
            } else {
                if (*reinterpret_cast<signed char*>(&edx3) != 58) {
                    ebp5 = 1;
                    goto addr_2d40_5;
                } else {
                    addr_2d98_9:
                    if (*reinterpret_cast<signed char*>(&ebp5)) {
                        if (rsi11 == rax8) {
                            _obstack_newchunk(0xd120, 1);
                            rcx12 = gd138;
                        }
                        gd138 = rcx12 + 1;
                        *rcx12 = 92;
                        rax8 = gd138;
                        zf18 = gd140 == rax8;
                        if (!zf18) 
                            goto addr_2d49_7; else 
                            goto addr_2dc0_6;
                    }
                }
            }
        }
        return *reinterpret_cast<uint32_t*>(&rax8);
    }
}

void append_entry(int32_t edi, struct s0* rsi, struct s0* rdx) {
    struct s0* r13_4;
    int32_t ebx5;
    int1_t zf6;
    signed char* rax7;
    int1_t zf8;
    signed char* rax9;
    int1_t zf10;
    uint32_t eax11;
    signed char* rdx12;
    int1_t zf13;
    uint1_t cf14;
    uint32_t eax15;
    int1_t zf16;
    signed char* rax17;
    int1_t zf18;
    uint1_t cf19;
    int1_t zf20;

    r13_4 = rsi;
    ebx5 = edi;
    zf6 = print_ls_colors == 0;
    if (!zf6) {
        append_quoted(0x8004, rsi);
        append_quoted(rdx, rsi);
        rax7 = gd138;
        zf8 = gd140 == rax7;
        if (zf8) {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            _obstack_newchunk(0xd120, 1);
            rax7 = gd138;
        }
        gd138 = rax7 + 1;
        *rax7 = 0x6d;
    }
    if (*reinterpret_cast<signed char*>(&ebx5)) {
        rax9 = gd138;
        zf10 = gd140 == rax9;
        if (zf10) {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            _obstack_newchunk(0xd120, 1);
            rax9 = gd138;
        }
        gd138 = rax9 + 1;
        *rax9 = *reinterpret_cast<signed char*>(&ebx5);
    }
    eax11 = append_quoted(r13_4, rsi);
    rdx12 = gd138;
    zf13 = gd140 == rdx12;
    if (zf13) {
        *reinterpret_cast<int32_t*>(&rsi) = 1;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax11 = _obstack_newchunk(0xd120, 1);
        rdx12 = gd138;
    }
    cf14 = reinterpret_cast<uint1_t>(print_ls_colors < 1);
    gd138 = rdx12 + 1;
    eax15 = (eax11 - (eax11 + reinterpret_cast<uint1_t>(eax11 < eax11 + cf14)) & 52) + 9;
    *rdx12 = *reinterpret_cast<signed char*>(&eax15);
    append_quoted(rdx, rsi);
    zf16 = print_ls_colors == 0;
    if (!zf16) {
        append_quoted(0x8007, rsi);
        rax17 = gd138;
        zf18 = gd140 == rax17;
        if (zf18) {
            addr_2fe0_13:
            _obstack_newchunk(0xd120, 1);
            rax17 = gd138;
            goto addr_2fc4_14;
        } else {
            addr_2fc4_14:
            cf19 = reinterpret_cast<uint1_t>(print_ls_colors < 1);
            *reinterpret_cast<uint32_t*>(&rdx12) = (*reinterpret_cast<uint32_t*>(&rdx12) - (*reinterpret_cast<uint32_t*>(&rdx12) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx12) < *reinterpret_cast<uint32_t*>(&rdx12) + cf19)) & 48) + 10;
        }
    } else {
        rax17 = gd138;
        zf20 = gd140 == rax17;
        *reinterpret_cast<uint32_t*>(&rdx12) = 58;
        if (zf20) 
            goto addr_2fe0_13;
    }
    gd138 = rax17 + 1;
    *rax17 = *reinterpret_cast<signed char*>(&rdx12);
    return;
}

int64_t fun_2430();

int64_t fun_2380(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2430();
    if (r8d > 10) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9bc0 + rax11 * 4) + 0x9bc0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

int32_t* fun_2390();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x90);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_24c0();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

struct s0* xcharalloc(struct s0* rdi, ...);

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x57bf;
    rax8 = fun_2390();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xd090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7731]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x584b;
            fun_24c0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xd1a0) {
                fun_2340(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x58da);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2450();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xd0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x9b51);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0x9b4c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x9b55);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0x9b48);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gce38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gce38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

struct s0* free = reinterpret_cast<struct s0*>(0);

void fun_2343() {
    __asm__("cli ");
    goto free;
}

struct s0* malloc = reinterpret_cast<struct s0*>(0);

void fun_2353() {
    __asm__("cli ");
    goto malloc;
}

int64_t __cxa_finalize = 0;

void fun_2363() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2373() {
    __asm__("cli ");
    goto getenv;
}

int64_t abort = 0x2040;

void fun_2383() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2393() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_23a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_23b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_23c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t puts = 0x2090;

void fun_23d3() {
    __asm__("cli ");
    goto puts;
}

int64_t reallocarray = 0x20a0;

void fun_23e3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20b0;

void fun_23f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_2403() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_2413() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20e0;

void fun_2423() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20f0;

void fun_2433() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2100;

void fun_2443() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2110;

void fun_2453() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2120;

void fun_2463() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2130;

void fun_2473() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x2140;

void fun_2483() {
    __asm__("cli ");
    goto dup2;
}

int64_t strrchr = 0x2150;

void fun_2493() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_24a3() {
    __asm__("cli ");
    goto lseek;
}

int64_t fnmatch = 0x2170;

void fun_24b3() {
    __asm__("cli ");
    goto fnmatch;
}

int64_t memset = 0x2180;

void fun_24c3() {
    __asm__("cli ");
    goto memset;
}

int64_t freopen = 0x2190;

void fun_24d3() {
    __asm__("cli ");
    goto freopen;
}

int64_t close = 0x21a0;

void fun_24e3() {
    __asm__("cli ");
    goto close;
}

int64_t memcmp = 0x21b0;

void fun_24f3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21c0;

void fun_2503() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21d0;

void fun_2513() {
    __asm__("cli ");
    goto calloc;
}

int64_t __getdelim = 0x21e0;

void fun_2523() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x21f0;

void fun_2533() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2200;

void fun_2543() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2210;

void fun_2553() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2220;

void fun_2563() {
    __asm__("cli ");
    goto fileno;
}

int64_t fflush = 0x2230;

void fun_2573() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2240;

void fun_2583() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2250;

void fun_2593() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2260;

void fun_25a3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2270;

void fun_25b3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2280;

void fun_25c3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2290;

void fun_25d3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22a0;

void fun_25e3() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x22b0;

void fun_25f3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x22c0;

void fun_2603() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x22d0;

void fun_2613() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22e0;

void fun_2623() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22f0;

void fun_2633() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2300;

void fun_2643() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2310;

void fun_2653() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2320;

void fun_2663() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2330;

void fun_2673() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_25c0(int64_t rdi, ...);

void fun_2410(int64_t rdi, int64_t rsi);

void fun_23f0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

struct s0* stdout = reinterpret_cast<struct s0*>(0);

void fun_2500(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

void fun_23d0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

int32_t usage();

int32_t fun_2460(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t optind = 0;

struct s0* quote(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

void fun_2640(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, ...);

struct s0* Version = reinterpret_cast<struct s0*>(0xc8);

void version_etc(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

void fun_2620();

void _obstack_begin(int64_t rdi);

int32_t fun_2530(struct s0* rdi, int64_t rsi, ...);

struct s0* stdin = reinterpret_cast<struct s0*>(0);

int64_t freopen_safer(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

int64_t rpl_fclose(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

signed char* gd130 = reinterpret_cast<signed char*>(0);

unsigned char gd170 = 0;

void* gd150 = reinterpret_cast<void*>(0);

uint64_t gd128 = 0;

void fun_25a0(signed char* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s0* last_component(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

int64_t fun_26d3(int32_t edi, struct s0* rsi) {
    int32_t r15d3;
    int32_t r14d4;
    int32_t ebp5;
    struct s0* rbx6;
    struct s0* rdi7;
    struct s0* r12_8;
    uint32_t ebx9;
    struct s0* rsi10;
    struct s0* r12_11;
    struct s0* rdx12;
    struct s0* rcx13;
    struct s0* r8_14;
    struct s0* rbx15;
    struct s0* rcx16;
    struct s0* r8_17;
    struct s0* rax18;
    int32_t eax19;
    struct s0* rdx20;
    struct s0* rsi21;
    int64_t rdi22;
    int64_t rax23;
    uint32_t eax24;
    struct s0* rdi25;
    struct s0* rax26;
    struct s0* rax27;
    struct s0* rax28;
    struct s0* rsi29;
    struct s0* rdx30;
    struct s0* rdi31;
    struct s0* rax32;
    struct s0* rax33;
    struct s0* rdi34;
    struct s0* rdi35;
    struct s0* rcx36;
    struct s0* rdx37;
    uint32_t eax38;
    struct s0* r12_39;
    int32_t eax40;
    struct s0* rdi41;
    int64_t rax42;
    uint32_t eax43;
    struct s0* rdi44;
    int64_t rax45;
    uint32_t ebx46;
    int64_t rax47;
    signed char* rax48;
    signed char* rbp49;
    void* rdx50;
    signed char* rax51;
    signed char* rdx52;
    struct s0* rsi53;
    struct s0* rcx54;
    int1_t zf55;
    struct s0* rdx56;
    int1_t zf57;
    struct s0* rsi58;
    int1_t zf59;
    struct s0* rax60;
    int32_t eax61;
    int32_t eax62;

    __asm__("cli ");
    r15d3 = 0;
    r14d4 = 2;
    ebp5 = edi;
    rbx6 = rsi;
    rdi7 = *reinterpret_cast<struct s0**>(&rsi->f0);
    set_program_name(rdi7);
    fun_25c0(6, 6);
    fun_2410("coreutils", "/usr/local/share/locale");
    r12_8 = reinterpret_cast<struct s0*>("bcp");
    fun_23f0("coreutils", "/usr/local/share/locale");
    atexit(0x39a0, "/usr/local/share/locale");
    goto addr_2748_2;
    addr_2a3e_3:
    ebx9 = 1;
    goto addr_2a43_4;
    addr_29cf_5:
    rsi10 = stdout;
    fun_2500(r12_11, rsi10, rdx12, rcx13, r8_14);
    goto addr_2a43_4;
    addr_2a0f_6:
    rbx15 = reinterpret_cast<struct s0*>("# Configuration file for dircolors, a utility to help you set the");
    while (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx15) - reinterpret_cast<unsigned char>("# Configuration file for dircolors, a utility to help you set the")) <= 0x12c7) {
        fun_23d0(rbx15, "options --print-database and --print-ls-colors are mutually exclusive", 5, rcx16, r8_17);
        rax18 = fun_2440(rbx15, rbx15);
        rbx15 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx15) + reinterpret_cast<unsigned char>(rax18) + 1);
    }
    goto addr_2a3e_3;
    while (1) {
        addr_2b02_9:
        while (1) {
            fun_2420();
            fun_25e0();
            while (1) {
                while (1) {
                    eax19 = usage();
                    while (1) {
                        if (eax19 != 0x80) 
                            break;
                        print_ls_colors = 1;
                        while (1) {
                            addr_2748_2:
                            *reinterpret_cast<int32_t*>(&r8_17) = 0;
                            *reinterpret_cast<int32_t*>(&r8_17 + 4) = 0;
                            rcx16 = reinterpret_cast<struct s0*>(0xc840);
                            rdx20 = r12_8;
                            rsi21 = rbx6;
                            *reinterpret_cast<int32_t*>(&rdi22) = ebp5;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
                            eax19 = fun_2460(rdi22, rsi21, rdx20, 0xc840);
                            if (eax19 == -1) 
                                goto addr_2804_14;
                            if (eax19 == 99) {
                                r14d4 = 1;
                            } else {
                                if (eax19 > 99) {
                                    if (eax19 != 0x70) 
                                        break;
                                    r15d3 = 1;
                                } else {
                                    if (eax19 == 0xffffff7e) 
                                        goto addr_2abd_21;
                                    if (eax19 != 98) 
                                        goto addr_2790_23;
                                    r14d4 = 0;
                                }
                            }
                        }
                    }
                }
                addr_2804_14:
                rax23 = optind;
                ebp5 = ebp5 - *reinterpret_cast<int32_t*>(&rax23);
                rbx6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx6) + rax23 * 8);
                eax24 = print_ls_colors;
                if (*reinterpret_cast<signed char*>(&eax24) || *reinterpret_cast<signed char*>(&r15d3)) {
                    if (r14d4 != 2) 
                        goto addr_2b02_9;
                    if (*reinterpret_cast<signed char*>(&r15d3)) 
                        goto addr_29f3_27;
                } else {
                    if (ebp5 > 1) {
                        addr_2acd_29:
                        rdi25 = rbx6->f8;
                        rax26 = quote(rdi25, rsi21, rdx20, rcx16, r8_17);
                        r12_8 = rax26;
                        fun_2420();
                        fun_25e0();
                        continue;
                    } else {
                        if (r14d4 != 2) 
                            goto addr_2890_31;
                        rax27 = fun_2370("SHELL", rsi21, rdx20, rcx16, r8_17);
                        if (!rax27) 
                            goto addr_2b28_33;
                        if (*reinterpret_cast<struct s0**>(&rax27->f0)) 
                            goto addr_2859_35;
                        addr_2b28_33:
                        rax28 = fun_2420();
                        *reinterpret_cast<int32_t*>(&rsi29) = 0;
                        *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
                        rdx30 = rax28;
                        fun_25e0();
                        goto addr_2b4c_36;
                    }
                }
                addr_2ac4_37:
                if (ebp5 <= 1) 
                    goto addr_2890_31; else 
                    goto addr_2acd_29;
                addr_29f3_27:
                *reinterpret_cast<int32_t*>(&rdx30) = 5;
                *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
                rsi29 = reinterpret_cast<struct s0*>("options --print-database and --print-ls-colors are mutually exclusive");
                if (*reinterpret_cast<signed char*>(&eax24)) 
                    break;
                if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(ebp5 < 0) | reinterpret_cast<uint1_t>(ebp5 == 0))) 
                    goto addr_2a0f_6;
                addr_2b4c_36:
                rdi31 = *reinterpret_cast<struct s0**>(&rbx6->f0);
                rax32 = quote(rdi31, rsi29, rdx30, rcx16, r8_17);
                r12_8 = rax32;
                fun_2420();
                fun_25e0();
                rax33 = fun_2420();
                rdi34 = stderr;
                fun_2640(rdi34, 1, "%s\n", rax33, r8_17);
                continue;
                addr_2abd_21:
                usage();
                goto addr_2ac4_37;
                addr_2790_23:
                if (eax19 != 0xffffff7d) 
                    continue;
                rdi35 = stdout;
                rcx16 = Version;
                r8_17 = reinterpret_cast<struct s0*>("H. Peter Anvin");
                rdx20 = reinterpret_cast<struct s0*>("GNU coreutils");
                rsi21 = reinterpret_cast<struct s0*>("dircolors");
                version_etc(rdi35, "dircolors", "GNU coreutils", rcx16, "H. Peter Anvin");
                fun_2620();
                goto addr_2804_14;
            }
        }
    }
    addr_2890_31:
    r8_14 = free;
    rcx36 = malloc;
    *reinterpret_cast<int32_t*>(&rdx37) = 0;
    *reinterpret_cast<int32_t*>(&rdx37 + 4) = 0;
    _obstack_begin(0xd120);
    if (!ebp5) {
        eax38 = dc_parse_stream(0, 0, 0, rcx36, r8_14);
        ebx9 = eax38;
    } else {
        r12_39 = *reinterpret_cast<struct s0**>(&rbx6->f0);
        eax40 = fun_2530(r12_39, "-");
        rdi41 = stdin;
        if (eax40 && (rdx37 = rdi41, rax42 = freopen_safer(r12_39, "r", rdx37, rcx36, r8_14), rdi41 = stdin, rax42 == 0) || (eax43 = dc_parse_stream(rdi41, r12_39, rdx37, rcx36, r8_14, rdi41, r12_39), rdi44 = stdin, ebx9 = eax43, rax45 = rpl_fclose(rdi44, r12_39, rdx37, rcx36, r8_14), !!*reinterpret_cast<int32_t*>(&rax45))) {
            ebx9 = 0;
            quotearg_n_style_colon();
            fun_2390();
            fun_25e0();
            goto addr_2a43_4;
        }
    }
    if (!*reinterpret_cast<signed char*>(&ebx9)) {
        addr_2a43_4:
        ebx46 = ebx9 ^ 1;
        *reinterpret_cast<uint32_t*>(&rax47) = *reinterpret_cast<unsigned char*>(&ebx46);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax47) + 4) = 0;
        return rax47;
    } else {
        rax48 = gd138;
        rbp49 = gd130;
        if (rax48 == rbp49) {
            gd170 = reinterpret_cast<unsigned char>(gd170 | 2);
        }
        rdx50 = gd150;
        r12_11 = reinterpret_cast<struct s0*>("';\nexport LS_COLORS\n");
        rax51 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax48) + reinterpret_cast<uint64_t>(rdx50) & ~reinterpret_cast<uint64_t>(rdx50));
        rdx52 = gd140;
        rsi53 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax51) - gd128);
        rcx54 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdx52) - gd128);
        if (reinterpret_cast<unsigned char>(rsi53) <= reinterpret_cast<unsigned char>(rcx54)) {
            rdx52 = rax51;
        }
        zf55 = r14d4 == 0;
        if (!zf55) {
            r12_11 = reinterpret_cast<struct s0*>("'\n");
        }
        gd138 = rdx52;
        gd130 = rdx52;
        rdx56 = reinterpret_cast<struct s0*>("setenv LS_COLORS '");
        if (zf55) {
            rdx56 = reinterpret_cast<struct s0*>("LS_COLORS='");
        }
        zf57 = print_ls_colors == 0;
        if (zf57) {
            rsi58 = stdout;
            fun_2500(rdx56, rsi58, rdx56, rcx54, r8_14);
        }
        rcx13 = stdout;
        rdx12 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax48) - reinterpret_cast<uint64_t>(rbp49));
        fun_25a0(rbp49, 1, rdx12, rcx13, r8_14);
        zf59 = print_ls_colors == 0;
        if (!zf59) 
            goto addr_2a3e_3; else 
            goto addr_29cf_5;
    }
    addr_2859_35:
    rax60 = last_component(rax27, rsi21, rdx20, rcx16, r8_17);
    eax61 = fun_2530(rax60, "csh", rax60, "csh");
    if (!eax61) {
        r14d4 = 1;
        goto addr_2890_31;
    } else {
        r14d4 = 0;
        eax62 = fun_2530(rax60, "tcsh", rax60, "tcsh");
        *reinterpret_cast<unsigned char*>(&r14d4) = reinterpret_cast<uint1_t>(eax62 == 0);
        goto addr_2890_31;
    }
}

int64_t __libc_start_main = 0;

void fun_2bb3() {
    __asm__("cli ");
    __libc_start_main(0x26d0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xd008;

void fun_2360(int64_t rdi);

int64_t fun_2c53() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2360(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2c93() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_25d0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_23a0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

void fun_3523(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    struct s0* r12_7;
    struct s0* rax8;
    struct s0* r8_9;
    struct s0* r12_10;
    struct s0* rax11;
    struct s0* r8_12;
    struct s0* r12_13;
    struct s0* rax14;
    struct s0* r8_15;
    struct s0* r12_16;
    struct s0* rax17;
    struct s0* r8_18;
    int32_t eax19;
    struct s0* r13_20;
    struct s0* rax21;
    struct s0* rax22;
    int32_t eax23;
    struct s0* rax24;
    struct s0* rax25;
    struct s0* rax26;
    int32_t eax27;
    struct s0* rax28;
    struct s0* r15_29;
    struct s0* rax30;
    struct s0* r8_31;
    struct s0* rax32;
    struct s0* rax33;
    struct s0* rdi34;
    struct s0* r8_35;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2420();
            fun_25d0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2420();
            fun_2500(rax8, r12_7, 5, rcx6, r8_9);
            r12_10 = stdout;
            rax11 = fun_2420();
            fun_2500(rax11, r12_10, 5, rcx6, r8_12);
            r12_13 = stdout;
            rax14 = fun_2420();
            fun_2500(rax14, r12_13, 5, rcx6, r8_15);
            r12_16 = stdout;
            rax17 = fun_2420();
            fun_2500(rax17, r12_16, 5, rcx6, r8_18);
            do {
                if (1) 
                    break;
                eax19 = fun_2530("dircolors", 0, "dircolors", 0);
            } while (eax19);
            r13_20 = v4;
            if (!r13_20) {
                rax21 = fun_2420();
                fun_25d0(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax22 = fun_25c0(5);
                if (!rax22 || (eax23 = fun_23a0(rax22, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax23)) {
                    rax24 = fun_2420();
                    r13_20 = reinterpret_cast<struct s0*>("dircolors");
                    fun_25d0(1, rax24, "https://www.gnu.org/software/coreutils/", "dircolors");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_20 = reinterpret_cast<struct s0*>("dircolors");
                    goto addr_3850_9;
                }
            } else {
                rax25 = fun_2420();
                fun_25d0(1, rax25, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax26 = fun_25c0(5);
                if (!rax26 || (eax27 = fun_23a0(rax26, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax27)) {
                    addr_3756_11:
                    rax28 = fun_2420();
                    fun_25d0(1, rax28, "https://www.gnu.org/software/coreutils/", "dircolors");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_20 == "dircolors")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x8130);
                    }
                } else {
                    addr_3850_9:
                    r15_29 = stdout;
                    rax30 = fun_2420();
                    fun_2500(rax30, r15_29, 5, "https://www.gnu.org/software/coreutils/", r8_31);
                    goto addr_3756_11;
                }
            }
            rax32 = fun_2420();
            rcx6 = r12_2;
            fun_25d0(1, rax32, r13_20, rcx6);
            addr_357e_14:
            fun_2620();
        }
    } else {
        rax33 = fun_2420();
        rdi34 = stderr;
        rcx6 = r12_2;
        fun_2640(rdi34, 1, rax33, rcx6, r8_35);
        goto addr_357e_14;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
};

struct s4* fun_3883(struct s4* rdi) {
    uint32_t edx2;
    struct s4* rax3;
    struct s4* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s4*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s4*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s4*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_38e3(struct s0* rdi) {
    struct s0* rbx2;
    struct s0* rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2440(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        rax3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff);
    }
    return;
}

int64_t fun_3913(int64_t rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;
    int64_t r9_5;
    uint32_t r8d6;
    uint32_t ecx7;
    uint32_t ebx8;
    uint32_t r10d9;
    int64_t rax10;

    __asm__("cli ");
    if (rdi == rsi) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rdx3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax4) = *reinterpret_cast<unsigned char*>(rdi + rdx3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r9_5) = *reinterpret_cast<unsigned char*>(rsi + rdx3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_5) + 4) = 0;
            r8d6 = *reinterpret_cast<uint32_t*>(&rax4);
            ecx7 = *reinterpret_cast<uint32_t*>(&r9_5);
            ebx8 = *reinterpret_cast<uint32_t*>(&r9_5);
            r10d9 = static_cast<uint32_t>(r9_5 - 65);
            if (static_cast<uint32_t>(rax4 - 65) <= 25) {
                *reinterpret_cast<uint32_t*>(&rax4) = *reinterpret_cast<uint32_t*>(&rax4) + 32;
                r8d6 = r8d6 + 32;
                if (r10d9 > 25) {
                    addr_3938_6:
                    ++rdx3;
                    if (*reinterpret_cast<signed char*>(&r8d6) != *reinterpret_cast<signed char*>(&ecx7)) 
                        break;
                } else {
                    addr_392d_7:
                    ebx8 = static_cast<uint32_t>(r9_5 + 32);
                    ecx7 = ecx7 + 32;
                    if (!*reinterpret_cast<uint32_t*>(&rax4)) 
                        break; else 
                        goto addr_3938_6;
                }
            } else {
                if (r10d9 <= 25) 
                    goto addr_392d_7;
                if (*reinterpret_cast<uint32_t*>(&rax4)) 
                    goto addr_3938_6; else 
                    break;
            }
        }
        *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<uint32_t*>(&rax4) - ebx8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

int64_t file_name = 0;

void fun_3983(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3993(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_23b0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_39a3() {
    struct s0* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s0* rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2390(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2420();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3a33_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25e0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3a33_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25e0();
    }
}

struct s5 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2560(struct s5* rdi);

int32_t fun_2590(struct s5* rdi);

int64_t fun_24a0(int64_t rdi, ...);

int32_t rpl_fflush(struct s5* rdi);

int64_t fun_2400(struct s5* rdi);

int64_t fun_3a53(struct s5* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2560(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2590(rdi);
        if (!(eax3 && (eax4 = fun_2560(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24a0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2390();
            r12d9 = *rax8;
            rax10 = fun_2400(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2400;
}

void rpl_fseeko(struct s5* rdi);

void fun_3ae3(struct s5* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2590(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int32_t fun_2480();

int32_t fun_25f0(int64_t rdi);

int64_t fun_24d0(int64_t rdi, int64_t rsi, struct s5* rdx);

void fun_24e0();

int64_t fun_3b33(int64_t rdi, int64_t rsi, struct s5* rdx) {
    int32_t eax4;
    int32_t eax5;
    int32_t* rax6;
    int32_t* rbx7;
    int32_t eax8;
    int32_t r14d9;
    int32_t r15d10;
    signed char v11;
    int32_t eax12;
    int32_t eax13;
    int32_t eax14;
    int32_t* rax15;
    int64_t rax16;
    int64_t r12_17;
    int32_t* rax18;
    int32_t ebp19;
    int32_t eax20;
    int32_t eax21;

    __asm__("cli ");
    eax4 = fun_2560(rdx);
    if (eax4 == 1) {
        eax5 = fun_2480();
        rax6 = fun_2390();
        rbx7 = rax6;
        if (eax5) {
            eax8 = fun_25f0("/dev/null");
            if (eax8) {
                r14d9 = 0;
                r15d10 = 0;
                goto addr_3c78_5;
            } else {
                v11 = 1;
                r14d9 = 0;
                r15d10 = 0;
            }
        } else {
            addr_3cad_7:
            v11 = 0;
            r14d9 = 0;
            r15d10 = 0;
        }
    } else {
        if (eax4 == 2) {
            r14d9 = 0;
        } else {
            if (!eax4) 
                goto addr_3cad_7;
            eax12 = fun_2480();
            *reinterpret_cast<unsigned char*>(&r14d9) = reinterpret_cast<uint1_t>(eax12 != 2);
        }
        eax13 = fun_2480();
        *reinterpret_cast<unsigned char*>(&r15d10) = reinterpret_cast<uint1_t>(eax13 != 1);
        eax14 = fun_2480();
        rax15 = fun_2390();
        rbx7 = rax15;
        if (eax14) 
            goto addr_3c60_13; else 
            goto addr_3bbe_14;
    }
    rax16 = fun_24d0(rdi, rsi, rdx);
    r12_17 = rax16;
    rax18 = fun_2390();
    ebp19 = *rax18;
    rbx7 = rax18;
    addr_3cd3_16:
    if (*reinterpret_cast<unsigned char*>(&r14d9)) {
        addr_3c1c_17:
        fun_24e0();
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) {
            addr_3ce5_18:
            if (v11) {
                addr_3c45_19:
                fun_24e0();
                if (r12_17) {
                    addr_3cf9_20:
                    return r12_17;
                } else {
                    addr_3c55_21:
                    *rbx7 = ebp19;
                    goto addr_3cf9_20;
                }
            } else {
                addr_3cf0_22:
                if (!r12_17) 
                    goto addr_3c55_21; else 
                    goto addr_3cf9_20;
            }
        }
    } else {
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) 
            goto addr_3ce5_18;
    }
    addr_3c30_25:
    fun_24e0();
    if (!v11) 
        goto addr_3cf0_22; else 
        goto addr_3c45_19;
    addr_3c60_13:
    eax8 = fun_25f0("/dev/null");
    if (!eax8) {
        v11 = 1;
        if (eax13 != 1) {
            addr_3bce_27:
            eax20 = fun_25f0("/dev/null");
            if (eax20 != 1) {
                if (eax20 >= 0) {
                    ebp19 = 9;
                    fun_24e0();
                    *rbx7 = 9;
                } else {
                    ebp19 = *rbx7;
                }
                *reinterpret_cast<int32_t*>(&r12_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&r14d9)) {
                    fun_24e0();
                    goto addr_3c30_25;
                }
            } else {
                r15d10 = 1;
            }
        } else {
            addr_3d80_34:
            r15d10 = 0;
        }
    } else {
        addr_3c78_5:
        if (eax8 >= 0) {
            ebp19 = 9;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            fun_24e0();
            *rbx7 = 9;
            v11 = 1;
            goto addr_3cd3_16;
        } else {
            v11 = 1;
            ebp19 = *rbx7;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            goto addr_3cd3_16;
        }
    }
    if (*reinterpret_cast<unsigned char*>(&r14d9) && (eax21 = fun_25f0("/dev/null"), eax21 != 2)) {
        if (eax21 >= 0) {
            ebp19 = 9;
            fun_24e0();
            *rbx7 = 9;
        } else {
            ebp19 = *rbx7;
        }
        *reinterpret_cast<int32_t*>(&r12_17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
        goto addr_3c1c_17;
    }
    addr_3bbe_14:
    v11 = 0;
    if (eax13 == 1) 
        goto addr_3d80_34; else 
        goto addr_3bce_27;
}

int64_t fun_3e03(struct s5* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2560(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24a0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

struct s7 {
    int64_t f0;
    int64_t f8;
};

struct s6 {
    int64_t f0;
    struct s7* f8;
    uint64_t f10;
    uint64_t f18;
    int64_t f20;
    signed char[8] pad48;
    unsigned char* f30;
    int64_t f38;
    signed char[8] pad72;
    int64_t f48;
    unsigned char f50;
};

int64_t obstack_alloc_failed_handler = 0x3e80;

void fun_3e83() {
    struct s0* rax1;
    struct s0* rdi2;
    int64_t rsi3;
    struct s0* r8_4;
    struct s6* rdi5;
    unsigned char* r12_6;
    struct s0* rbp7;
    int64_t rax8;
    int64_t rdi9;
    struct s7* rax10;
    uint64_t rdx11;
    int64_t rdx12;
    int64_t rax13;

    __asm__("cli ");
    rax1 = fun_2420();
    rdi2 = stderr;
    *reinterpret_cast<int32_t*>(&rsi3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi3) + 4) = 0;
    fun_2640(rdi2, 1, "%s\n", rax1, r8_4);
    *reinterpret_cast<int32_t*>(&rdi5) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
    fun_2620();
    if (0) {
        *reinterpret_cast<int32_t*>(&r12_6) = 15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_6) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp7) = 16;
        *reinterpret_cast<int32_t*>(&rbp7 + 4) = 0;
    } else {
        rbp7 = reinterpret_cast<struct s0*>("%s\n");
        r12_6 = reinterpret_cast<unsigned char*>(" %s\n");
    }
    rdi5->f30 = r12_6;
    if (0) {
        rsi3 = 0xfe0;
    }
    rax8 = rdi5->f38;
    rdi5->f0 = rsi3;
    if (!(rdi5->f50 & 1)) {
        rdi9 = rsi3;
        rax10 = reinterpret_cast<struct s7*>(rax8(rdi9));
    } else {
        rdi9 = rdi5->f48;
        rax10 = reinterpret_cast<struct s7*>(rax8(rdi9));
    }
    rdi5->f8 = rax10;
    if (!rax10) {
        obstack_alloc_failed_handler(rdi9);
    } else {
        rdx11 = reinterpret_cast<int64_t>(rax10) + reinterpret_cast<uint64_t>(r12_6) + 16 & reinterpret_cast<uint64_t>(-reinterpret_cast<unsigned char>(rbp7));
        rdi5->f10 = rdx11;
        rdi5->f18 = rdx11;
        rdx12 = rdi5->f0 + reinterpret_cast<int64_t>(rax10);
        rax10->f0 = rdx12;
        rdi5->f20 = rdx12;
        rax10->f8 = 0;
        rdi5->f50 = reinterpret_cast<unsigned char>(rdi5->f50 & 0xf9);
        goto rax13;
    }
}

struct s8 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    signed char[8] pad80;
    unsigned char f50;
};

void fun_3f73(struct s8* rdi) {
    int64_t rcx2;
    int64_t r8_3;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 & 0xfe);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    goto 0x3ed0;
}

struct s9 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    int64_t f48;
    unsigned char f50;
};

void fun_3f93(struct s9* rdi) {
    int64_t rcx2;
    int64_t r8_3;
    int64_t r9_4;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 | 1);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    rdi->f48 = r9_4;
    goto 0x3ed0;
}

struct s11 {
    void* f0;
    struct s11* f8;
};

struct s10 {
    struct s10* f0;
    struct s11* f8;
    struct s0* f10;
    signed char[7] pad24;
    void* f18;
    void* f20;
    signed char[8] pad48;
    void* f30;
    int64_t f38;
    int64_t f40;
    struct s10* f48;
    unsigned char f50;
};

void fun_2550(struct s0* rdi, struct s0* rsi, unsigned char* rdx);

void fun_3fb3(struct s10* rdi, struct s10* rsi) {
    int64_t rcx3;
    struct s10* rbx4;
    unsigned char* r13_5;
    struct s10* tmp64_6;
    struct s11* rbp7;
    struct s10* tmp64_8;
    struct s10* rsi9;
    struct s10* rax10;
    int64_t rax11;
    struct s11* rax12;
    struct s11* r12_13;
    struct s11* rax14;
    void* rax15;
    struct s0* rsi16;
    struct s0* r14_17;
    uint32_t edx18;
    int64_t rax19;
    struct s10* rdi20;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rcx3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    rbx4 = rdi;
    r13_5 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi->f18) - reinterpret_cast<unsigned char>(rdi->f10));
    tmp64_6 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(rsi) + reinterpret_cast<uint64_t>(r13_5));
    rbp7 = rdi->f8;
    *reinterpret_cast<unsigned char*>(&rcx3) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_6) < reinterpret_cast<uint64_t>(rsi));
    tmp64_8 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(tmp64_6) + reinterpret_cast<int64_t>(rdi->f30));
    rsi9 = tmp64_8;
    *reinterpret_cast<unsigned char*>(&rdi) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_8) < reinterpret_cast<uint64_t>(tmp64_6));
    rax10 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(rsi9) + (reinterpret_cast<uint64_t>(r13_5) >> 3) + 100);
    if (reinterpret_cast<uint64_t>(rsi9) < reinterpret_cast<uint64_t>(rbx4->f0)) {
        rsi9 = rbx4->f0;
    }
    if (reinterpret_cast<uint64_t>(rax10) >= reinterpret_cast<uint64_t>(rsi9)) {
        rsi9 = rax10;
    }
    if (!rcx3 && (*reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<unsigned char*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0, !rdi)) {
        rax11 = rbx4->f38;
        if (rbx4->f50 & 1) {
            rdi = rbx4->f48;
            rax12 = reinterpret_cast<struct s11*>(rax11(rdi));
            r12_13 = rax12;
        } else {
            rdi = rsi9;
            rax14 = reinterpret_cast<struct s11*>(rax11(rdi));
            r12_13 = rax14;
        }
        if (r12_13) {
            rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<uint64_t>(rsi9));
            rbx4->f8 = r12_13;
            rsi16 = rbx4->f10;
            r12_13->f8 = rbp7;
            rbx4->f20 = rax15;
            r12_13->f0 = rax15;
            r14_17 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<int64_t>(rbx4->f30) + 16) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)));
            fun_2550(r14_17, rsi16, r13_5);
            edx18 = rbx4->f50;
            if (!(*reinterpret_cast<unsigned char*>(&edx18) & 2) && rbx4->f10 == (reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)) & reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp7) + reinterpret_cast<int64_t>(rbx4->f30) + 16))) {
                r12_13->f8 = rbp7->f8;
                rax19 = rbx4->f40;
                if (!(edx18 & 1)) {
                    rax19(rbp7, rsi16);
                } else {
                    rdi20 = rbx4->f48;
                    rax19(rdi20, rbp7);
                }
            }
            rbx4->f10 = r14_17;
            rbx4->f18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_17) + reinterpret_cast<uint64_t>(r13_5));
            rbx4->f50 = reinterpret_cast<unsigned char>(rbx4->f50 & 0xfd);
            return;
        }
    }
    obstack_alloc_failed_handler(rdi);
}

struct s12 {
    struct s12* f0;
    struct s12* f8;
};

struct s13 {
    signed char[8] pad8;
    struct s12* f8;
};

struct s12* fun_40e3(struct s13* rdi, struct s12* rsi) {
    struct s12* rax3;

    __asm__("cli ");
    rax3 = rdi->f8;
    if (!rax3) {
        return rax3;
    }
    do {
        if (reinterpret_cast<uint64_t>(rsi) <= reinterpret_cast<uint64_t>(rax3)) 
            continue;
        if (reinterpret_cast<uint64_t>(rax3->f0) >= reinterpret_cast<uint64_t>(rsi)) 
            break;
        rax3 = rax3->f8;
    } while (rax3);
    goto addr_4103_7;
    return 1;
    addr_4103_7:
    return 0;
}

void fun_4123(struct s0* rdi, struct s0* rsi) {
    struct s0* r12_3;
    struct s0* rsi4;
    struct s0* rbx5;
    struct s0* rax6;
    struct s0* rbp7;
    int64_t rax8;

    __asm__("cli ");
    r12_3 = rsi;
    rsi4 = rdi->f8;
    rbx5 = rdi;
    if (rsi4) {
        while (reinterpret_cast<unsigned char>(rsi4) >= reinterpret_cast<unsigned char>(r12_3) || (rax6 = *reinterpret_cast<struct s0**>(&rsi4->f0), reinterpret_cast<unsigned char>(rax6) < reinterpret_cast<unsigned char>(r12_3))) {
            rbp7 = rsi4->f8;
            rax8 = rbx5->f40;
            if (rbx5->f50 & 1) {
                rdi = rbx5->f48;
                rax8(rdi);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_417b_5;
            } else {
                rdi = rsi4;
                rax8(rdi);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_417b_5;
            }
            rsi4 = rbp7;
        }
    } else {
        goto addr_417b_5;
    }
    rbx5->f18 = r12_3;
    rbx5->f10 = r12_3;
    rbx5->f20 = rax6;
    rbx5->f8 = rsi4;
    return;
    addr_417b_5:
    if (r12_3) {
        fun_2380(rdi);
    } else {
        return;
    }
}

struct s15 {
    int64_t f0;
    struct s15* f8;
};

struct s14 {
    signed char[8] pad8;
    struct s15* f8;
};

int64_t fun_41b3(struct s14* rdi) {
    struct s15* rax2;
    int64_t r8_3;
    int64_t rdx4;

    __asm__("cli ");
    rax2 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_3) + 4) = 0;
    if (rax2) {
        do {
            rdx4 = rax2->f0 - reinterpret_cast<int64_t>(rax2);
            rax2 = rax2->f8;
            r8_3 = r8_3 + rdx4;
        } while (rax2);
    }
    return r8_3;
}

void fun_2630(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s16 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s16* fun_2490();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_41e3(struct s0* rdi) {
    struct s0* rcx2;
    struct s0* rbx3;
    struct s16* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2630("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2380("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2490();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5983(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2390();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xd2a0;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_59c3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd2a0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_59e3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd2a0);
    }
    *rdi = esi;
    return 0xd2a0;
}

int64_t fun_5a03(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xd2a0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s17 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5a43(struct s17* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s17*>(0xd2a0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s18 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s18* fun_5a63(struct s18* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s18*>(0xd2a0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x268f;
    if (!rdx) 
        goto 0x268f;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xd2a0;
}

struct s19 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5aa3(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s19* r8) {
    struct s19* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s19*>(0xd2a0);
    }
    rax7 = fun_2390();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5ad6);
    *rax7 = r15d8;
    return rax13;
}

struct s20 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5b23(int64_t rdi, int64_t rsi, struct s0** rdx, struct s20* rcx) {
    struct s20* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s20*>(0xd2a0);
    }
    rax6 = fun_2390();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5b51);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5bac);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5c13() {
    __asm__("cli ");
}

struct s0* gd098 = reinterpret_cast<struct s0*>(0xa0);

int64_t slotvec0 = 0x100;

void fun_5c23() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2340(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xd1a0) {
        fun_2340(rdi7);
        gd098 = reinterpret_cast<struct s0*>(0xd1a0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xd090) {
        fun_2340(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xd090);
    }
    nslots = 1;
    return;
}

void fun_5cc3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5ce3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5cf3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5d13(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_5d33(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2695;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_5dc3(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x269a;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_5e53(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x269f;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2450();
    } else {
        return rax5;
    }
}

struct s0* fun_5ee3(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26a4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_5f73(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7320]");
    __asm__("movdqa xmm1, [rip+0x7328]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x7311]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2450();
    } else {
        return rax10;
    }
}

struct s0* fun_6013(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7280]");
    __asm__("movdqa xmm1, [rip+0x7288]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x7271]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s0* fun_60b3(int64_t rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x71e0]");
    __asm__("movdqa xmm1, [rip+0x71e8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x71c9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2450();
    } else {
        return rax3;
    }
}

struct s0* fun_6143(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7150]");
    __asm__("movdqa xmm1, [rip+0x7158]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x7146]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2450();
    } else {
        return rax4;
    }
}

struct s0* fun_61d3(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26a9;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_6273(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x701a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x7012]");
    __asm__("movdqa xmm2, [rip+0x701a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ae;
    if (!rdx) 
        goto 0x26ae;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_6313(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6f7a]");
    __asm__("movdqa xmm1, [rip+0x6f82]");
    __asm__("movdqa xmm2, [rip+0x6f8a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26b3;
    if (!rdx) 
        goto 0x26b3;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s0* fun_63c3(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6eca]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6ec2]");
    __asm__("movdqa xmm2, [rip+0x6eca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b8;
    if (!rsi) 
        goto 0x26b8;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_6463(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6e2a]");
    __asm__("movdqa xmm1, [rip+0x6e32]");
    __asm__("movdqa xmm2, [rip+0x6e3a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26bd;
    if (!rsi) 
        goto 0x26bd;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void fun_6503() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6513(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6533() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6553(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s21 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_2540(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_6573(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s21* r8, struct s0* r9) {
    struct s0* r12_7;
    struct s0* rax8;
    struct s0* rax9;
    struct s0* r13_10;
    struct s0* r12_11;
    struct s0* rax12;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2640(rdi, 1, "%s %s\n", rdx, rcx);
    } else {
        r9 = rcx;
        fun_2640(rdi, 1, "%s (%s) %s\n", rsi, rdx, rdi, 1, "%s (%s) %s\n", rsi, rdx);
    }
    rax8 = fun_2420();
    fun_2640(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6);
    fun_2540(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2420();
    fun_2640(rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6);
    fun_2540(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r13_10 = r8->f8;
        r12_11 = r8->f0;
        rax12 = fun_2420();
        fun_2640(rdi, 1, rax12, r12_11, r13_10, rdi, 1, rax12, r12_11, r13_10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xa228 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xa228;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_69e3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s22 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6a03(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s22* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s22* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2450();
    } else {
        return;
    }
}

void fun_6aa3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6b46_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6b50_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2450();
    } else {
        return;
    }
    addr_6b46_5:
    goto addr_6b50_7;
}

void fun_6b83() {
    struct s0* rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2540(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2420();
    fun_25d0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2420();
    fun_25d0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2420();
    goto fun_25d0;
}

int64_t fun_23e0();

void xalloc_die();

void fun_6c23(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2350(unsigned char* rdi);

void fun_6c63(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2350(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c83(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2350(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6ca3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2350(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25b0();

void fun_6cc3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25b0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6cf3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6d23(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6d63() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6da3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6dd3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6e23(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23e0();
            if (rax5) 
                break;
            addr_6e6d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_6e6d_5;
        rax8 = fun_23e0();
        if (rax8) 
            goto addr_6e56_9;
        if (rbx4) 
            goto addr_6e6d_5;
        addr_6e56_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6eb3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23e0();
            if (rax8) 
                break;
            addr_6efa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_6efa_5;
        rax11 = fun_23e0();
        if (rax11) 
            goto addr_6ee2_9;
        if (!rbx6) 
            goto addr_6ee2_9;
        if (r12_4) 
            goto addr_6efa_5;
        addr_6ee2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6f43(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_6fed_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7000_10:
                *r12_8 = 0;
            }
            addr_6fa0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6fc6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7014_14;
            if (rcx10 <= rsi9) 
                goto addr_6fbd_16;
            if (rsi9 >= 0) 
                goto addr_7014_14;
            addr_6fbd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7014_14;
            addr_6fc6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25b0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7014_14;
            if (!rbp13) 
                break;
            addr_7014_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_6fed_9;
        } else {
            if (!r13_6) 
                goto addr_7000_10;
            goto addr_6fa0_11;
        }
    }
}

int64_t fun_2510();

void fun_7043() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2510();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7073() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2510();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2510();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2510();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70e3(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2350(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

void fun_7123(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2350(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

struct s23 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_7163(int64_t rdi, struct s23* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2350(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2550;
    }
}

void fun_71a3(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2440(rdi);
    rax3 = fun_2350(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

void fun_71e3() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2420();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_25e0();
    fun_2380(rdi1);
}

int64_t fun_23c0();

int64_t fun_7223(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    int64_t rax6;
    uint32_t ebx7;
    int64_t rax8;
    int32_t* rax9;
    int32_t* rax10;

    __asm__("cli ");
    rax6 = fun_23c0();
    ebx7 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0)) & 32;
    rax8 = rpl_fclose(rdi, rsi, rdx, rcx, r8);
    if (ebx7) {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            addr_727e_3:
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        } else {
            rax9 = fun_2390();
            *rax9 = 0;
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            if (rax6) 
                goto addr_727e_3;
            rax10 = fun_2390();
            *reinterpret_cast<int32_t*>(&rax8) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax10 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    }
    return rax8;
}

signed char* fun_2580(int64_t rdi);

signed char* fun_7293() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2580(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2470(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_72d3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2470(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2450();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_7363() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax3;
    }
}

int64_t fun_73e3(int64_t rdi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25c0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<struct s0**>(&rsi->f0) = reinterpret_cast<struct s0*>(0);
        }
    } else {
        rax6 = fun_2440(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2550(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2550(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7493() {
    __asm__("cli ");
    goto fun_25c0;
}

void fun_74a3() {
    __asm__("cli ");
}

void fun_74b7() {
    __asm__("cli ");
    return;
}

uint32_t fun_24f0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_2660(int64_t rdi, struct s0* rsi);

uint32_t fun_2650(struct s0* rdi, struct s0* rsi);

void fun_4415() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    struct s0** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2420();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2420();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2440(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4713_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4713_22; else 
                            goto addr_4b0d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4bcd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4f20_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4710_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4710_30; else 
                                goto addr_4f39_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2440(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4f20_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24f0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4f20_28; else 
                            goto addr_45bc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5080_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4f00_40:
                        if (r11_27 == 1) {
                            addr_4a8d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5048_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_46c7_44;
                            }
                        } else {
                            goto addr_4f10_46;
                        }
                    } else {
                        addr_508f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_4a8d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4713_22:
                                if (v47 != 1) {
                                    addr_4c69_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2440(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4cb4_54;
                                    }
                                } else {
                                    goto addr_4720_56;
                                }
                            } else {
                                addr_46c5_57:
                                ebp36 = 0;
                                goto addr_46c7_44;
                            }
                        } else {
                            addr_4ef4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_508f_47; else 
                                goto addr_4efe_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4a8d_41;
                        if (v47 == 1) 
                            goto addr_4720_56; else 
                            goto addr_4c69_52;
                    }
                }
                addr_4781_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4618_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_463d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4940_65;
                    } else {
                        addr_47a9_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4ff8_67;
                    }
                } else {
                    goto addr_47a0_69;
                }
                addr_4651_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_469c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4ff8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_469c_81;
                }
                addr_47a0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_463d_64; else 
                    goto addr_47a9_66;
                addr_46c7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_477f_91;
                if (v22) 
                    goto addr_46df_93;
                addr_477f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4781_62;
                addr_4cb4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_543b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_54ab_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_52af_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2660(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2650(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_4dae_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_476c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4db8_112;
                    }
                } else {
                    addr_4db8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4e89_114;
                }
                addr_4778_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_477f_91;
                while (1) {
                    addr_4e89_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_5397_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4df6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_53a5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4e77_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4e77_128;
                        }
                    }
                    addr_4e25_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4e77_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_4df6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4e25_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_469c_81;
                addr_53a5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4ff8_67;
                addr_543b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_4dae_109;
                addr_54ab_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_4dae_109;
                addr_4720_56:
                rax93 = fun_2670(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_476c_110;
                addr_4efe_59:
                goto addr_4f00_40;
                addr_4bcd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4713_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4778_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_46c5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4713_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4c12_160;
                if (!v22) 
                    goto addr_4fe7_162; else 
                    goto addr_51f3_163;
                addr_4c12_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4fe7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4ff8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4abb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4923_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4651_70; else 
                    goto addr_4937_169;
                addr_4abb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4618_63;
                goto addr_47a0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4ef4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_502f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4710_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4608_178; else 
                        goto addr_4fb2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4ef4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4713_22;
                }
                addr_502f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4710_30:
                    r8d42 = 0;
                    goto addr_4713_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4781_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5048_42;
                    }
                }
                addr_4608_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4618_63;
                addr_4fb2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4f10_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4781_62;
                } else {
                    addr_4fc2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4713_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5772_188;
                if (v28) 
                    goto addr_4fe7_162;
                addr_5772_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4923_168;
                addr_45bc_37:
                if (v22) 
                    goto addr_55b3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_45d3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5080_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_510b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4713_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4608_178; else 
                        goto addr_50e7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4ef4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4713_22;
                }
                addr_510b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4713_22;
                }
                addr_50e7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4f10_46;
                goto addr_4fc2_186;
                addr_45d3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4713_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4713_22; else 
                    goto addr_45e4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_56be_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5544_210;
            if (1) 
                goto addr_5542_212;
            if (!v29) 
                goto addr_517e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_56b1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4940_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_46fb_219; else 
            goto addr_495a_220;
        addr_46df_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_46f3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_495a_220; else 
            goto addr_46fb_219;
        addr_52af_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_495a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_52cd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_5740_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_51a6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_5397_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_46f3_221;
        addr_51f3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_46f3_221;
        addr_4937_169:
        goto addr_4940_65;
        addr_56be_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_495a_220;
        goto addr_52cd_222;
        addr_5544_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_559e_236;
        fun_2450();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5740_225;
        addr_5542_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5544_210;
        addr_517e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5544_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_51a6_226;
        }
        addr_56b1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4b0d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9cec + rax113 * 4) + 0x9cec;
    addr_4f39_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9dec + rax114 * 4) + 0x9dec;
    addr_55b3_190:
    addr_46fb_219:
    goto 0x43e0;
    addr_45e4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9bec + rax115 * 4) + 0x9bec;
    addr_559e_236:
    goto v116;
}

void fun_4600() {
}

void fun_47b8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x44b2;
}

void fun_4811() {
    goto 0x44b2;
}

void fun_48fe() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4781;
    }
    if (v2) 
        goto 0x51f3;
    if (!r10_3) 
        goto addr_535e_5;
    if (!v4) 
        goto addr_522e_7;
    addr_535e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_522e_7:
    goto 0x4634;
}

void fun_491c() {
}

void fun_49c7() {
    signed char v1;

    if (v1) {
        goto 0x494f;
    } else {
        goto 0x468a;
    }
}

void fun_49e1() {
    signed char v1;

    if (!v1) 
        goto 0x49da; else 
        goto "???";
}

void fun_4a08() {
    goto 0x4923;
}

void fun_4a88() {
}

void fun_4aa0() {
}

void fun_4acf() {
    goto 0x4923;
}

void fun_4b21() {
    goto 0x4ab0;
}

void fun_4b50() {
    goto 0x4ab0;
}

void fun_4b83() {
    goto 0x4ab0;
}

void fun_4f50() {
    goto 0x4608;
}

void fun_524e() {
    signed char v1;

    if (v1) 
        goto 0x51f3;
    goto 0x4634;
}

void fun_52f5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4634;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4618;
        goto 0x4634;
    }
}

void fun_5712() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4980;
    } else {
        goto 0x44b2;
    }
}

void fun_6648() {
    fun_2420();
}

void fun_483e() {
    goto 0x44b2;
}

void fun_4a14() {
    goto 0x49cc;
}

void fun_4adb() {
    goto 0x4608;
}

void fun_4b2d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4ab0;
    goto 0x46df;
}

void fun_4b5f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4abb;
        goto 0x44e0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x495a;
        goto 0x46fb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x52f8;
    if (r10_8 > r15_9) 
        goto addr_4a45_9;
    addr_4a4a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5303;
    goto 0x4634;
    addr_4a45_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4a4a_10;
}

void fun_4b92() {
    goto 0x46c7;
}

void fun_4f60() {
    goto 0x46c7;
}

void fun_56ff() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x481c;
    } else {
        goto 0x4980;
    }
}

void fun_6700() {
}

void fun_4b9c() {
    goto 0x4b37;
}

void fun_4f6a() {
    goto 0x4a8d;
}

void fun_6760() {
    fun_2420();
    goto fun_2640;
}

void fun_486d() {
    goto 0x44b2;
}

void fun_4ba8() {
    goto 0x4b37;
}

void fun_4f77() {
    goto 0x4ade;
}

void fun_67a0() {
    fun_2420();
    goto fun_2640;
}

void fun_489a() {
    goto 0x44b2;
}

void fun_4bb4() {
    goto 0x4ab0;
}

void fun_67e0() {
    fun_2420();
    goto fun_2640;
}

void fun_48bc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5250;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4781;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4781;
    }
    if (v11) 
        goto 0x55b3;
    if (r10_12 > r15_13) 
        goto addr_5603_8;
    addr_5608_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5341;
    addr_5603_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5608_9;
}

struct s24 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_6830() {
    struct s0* r13_1;
    struct s24* rbx2;
    struct s0* r12_3;
    struct s0** rbx4;
    struct s0* rax5;
    struct s0* rbp6;
    int64_t v7;

    r13_1 = rbx2->f8;
    r12_3 = *rbx4;
    rax5 = fun_2420();
    fun_2640(rbp6, 1, rax5, r12_3, r13_1, rbp6, 1, rax5, r12_3, r13_1);
    goto v7;
}

void fun_6888() {
    fun_2420();
    goto 0x6859;
}

struct s25 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_68c0() {
    struct s0* r13_1;
    struct s25* rbx2;
    struct s0* r12_3;
    struct s0** rbx4;
    struct s0* rax5;
    struct s0* rbp6;
    int64_t v7;

    r13_1 = rbx2->f8;
    r12_3 = *rbx4;
    rax5 = fun_2420();
    fun_2640(rbp6, 1, rax5, r12_3, r13_1, rbp6, 1, rax5, r12_3, r13_1);
    goto v7;
}

void fun_6938() {
    fun_2420();
    goto 0x68fb;
}
