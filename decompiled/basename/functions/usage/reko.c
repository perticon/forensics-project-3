void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002560(fn0000000000002390(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002A0E;
	}
	fn0000000000002500(fn0000000000002390(0x05, "Usage: %s NAME [SUFFIX]\n  or:  %s OPTION... NAME...\n", null), 0x01);
	fn0000000000002440(stdout, fn0000000000002390(0x05, "Print NAME with any leading directory components removed.\nIf specified, also remove a trailing SUFFIX.\n", null));
	fn0000000000002440(stdout, fn0000000000002390(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002440(stdout, fn0000000000002390(0x05, "  -a, --multiple       support multiple arguments and treat each as a NAME\n  -s, --suffix=SUFFIX  remove a trailing SUFFIX; implies -a\n  -z, --zero           end each output line with NUL, not newline\n", null));
	fn0000000000002440(stdout, fn0000000000002390(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002440(stdout, fn0000000000002390(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002500(fn0000000000002390(0x05, "\nExamples:\n  %s /usr/bin/sort          -> \"sort\"\n  %s include/stdio.h .h     -> \"stdio\"\n  %s -s .h include/stdio.h  -> \"stdio\"\n  %s -a any/str1 any/str2   -> \"str1\" followed by \"str2\"\n", null), 0x01);
	struct Eq_740 * rbx_189 = fp - 0xB8 + 16;
	do
	{
		char * rsi_191 = rbx_189->qw0000;
		++rbx_189;
	} while (rsi_191 != null && fn0000000000002460(rsi_191, "basename") != 0x00);
	ptr64 r13_204 = rbx_189->qw0008;
	if (r13_204 != 0x00)
	{
		fn0000000000002500(fn0000000000002390(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_11 rax_291 = fn00000000000024F0(null, 0x05);
		if (rax_291 == 0x00 || fn0000000000002320(0x03, "en_", rax_291) == 0x00)
			goto l0000000000002C46;
	}
	else
	{
		fn0000000000002500(fn0000000000002390(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_11 rax_233 = fn00000000000024F0(null, 0x05);
		if (rax_233 == 0x00 || fn0000000000002320(0x03, "en_", rax_233) == 0x00)
		{
			fn0000000000002500(fn0000000000002390(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002C83:
			fn0000000000002500(fn0000000000002390(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002A0E:
			fn0000000000002540(edi);
		}
		r13_204 = 0x7004;
	}
	fn0000000000002440(stdout, fn0000000000002390(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002C46:
	fn0000000000002500(fn0000000000002390(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002C83;
}