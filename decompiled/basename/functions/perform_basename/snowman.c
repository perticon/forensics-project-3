void perform_basename(int64_t rdi, struct s0* rsi, int64_t rdx) {
    struct s0* r13_4;
    int32_t r12d5;
    struct s0* rax6;
    struct s0* rbp7;
    struct s1* rsi8;
    struct s1* rdi9;
    signed char* rax10;
    uint32_t esi11;
    struct s0* rax12;
    struct s0* rbx13;
    struct s0* rax14;
    struct s0* rax15;
    uint32_t ecx16;

    r13_4 = rsi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx);
    rax6 = base_name();
    rbp7 = rax6;
    strip_trailing_slashes(rax6);
    if (!r13_4 || *reinterpret_cast<unsigned char*>(&rbp7->f0) == 47) {
        addr_28fb_2:
        rsi8 = stdout;
        fun_2440(rbp7, rsi8, rdx);
        rdi9 = stdout;
        rax10 = rdi9->f28;
        esi11 = *reinterpret_cast<uint32_t*>(&rsi8) - (*reinterpret_cast<uint32_t*>(&rsi8) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rsi8) < *reinterpret_cast<uint32_t*>(&rsi8) + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&r12d5) < 1))) & 10;
        if (reinterpret_cast<uint64_t>(rax10) >= reinterpret_cast<uint64_t>(rdi9->f30)) {
            fun_23f0();
        } else {
            rdi9->f28 = rax10 + 1;
            *rax10 = *reinterpret_cast<signed char*>(&esi11);
        }
    } else {
        rax12 = fun_23b0(rbp7);
        rbx13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(rax12));
        rax14 = fun_23b0(r13_4);
        rax15 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<unsigned char>(r13_4));
        do {
            if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(rbx13)) 
                goto addr_28fb_2;
            if (reinterpret_cast<unsigned char>(r13_4) >= reinterpret_cast<unsigned char>(rax15)) 
                goto addr_2990_8;
            rbx13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx13) - 1);
            rax15 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax15) - 1);
            ecx16 = *reinterpret_cast<unsigned char*>(&rax15->f0);
        } while (*reinterpret_cast<unsigned char*>(&rbx13->f0) == *reinterpret_cast<unsigned char*>(&ecx16));
        goto addr_2979_10;
    }
    addr_2990_8:
    if (reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(rbx13)) {
        *reinterpret_cast<unsigned char*>(&rbx13->f0) = 0;
        goto addr_28fb_2;
    }
    addr_2979_10:
    goto addr_28fb_2;
}