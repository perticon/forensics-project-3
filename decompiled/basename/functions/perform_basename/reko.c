void perform_basename(uint32 edx, Eq_11 rsi, Eq_11 rdi)
{
	Eq_11 rax_24 = base_name(rdi);
	strip_trailing_slashes(rax_24);
	cu8 r12b_81 = (byte) edx;
	if (rsi != 0x00 && *rax_24 != 0x2F)
	{
		word64 rbx_52 = rax_24 + fn00000000000023B0(rax_24);
		word64 rax_54 = fn00000000000023B0(rsi) + rsi;
		while (rax_24 < rbx_52)
		{
			if (rsi >= rax_54)
			{
				if (rax_24 < rbx_52)
					*rbx_52 = 0x00;
				break;
			}
			--rax_54;
			--rbx_52;
			if (*rbx_52 != *rax_54)
				break;
		}
	}
	fn0000000000002440(stdout, rax_24);
	FILE * rdi_84 = stdout;
	uint64 rsi_91 = (uint64) (0x00 - (word32) (r12b_81 < 0x01) & 0x0A);
	byte * rax_88 = rdi_84->ptr0028;
	byte sil_97 = (byte) rsi_91;
	if (rax_88 < rdi_84->ptr0030)
	{
		rdi_84->ptr0028 = rax_88 + 1;
		*rax_88 = sil_97;
	}
	else
		fn00000000000023F0((word32) rsi_91, rdi_84);
	fn00000000000022F0(rax_24);
}