void perform_basename(char * string, char * suffix, bool use_nuls) {
    char * v1 = base_name(string); // 0x28e0
    strip_trailing_slashes(v1);
    if (suffix != NULL) {
        // 0x28f5
        if (*v1 != 47) {
            uint64_t v2 = (int64_t)suffix;
            uint64_t v3 = (int64_t)v1; // 0x28e0
            int64_t v4 = function_23b0(); // 0x294b
            int64_t v5 = function_23b0() + v2; // 0x295d
            uint64_t v6 = v4 + v3;
            while (v6 > v3) {
                // 0x2965
                if (v5 <= v2) {
                    // 0x2999
                    *(char *)v6 = 0;
                    goto lab_0x28fb;
                }
                int64_t v7 = v6 - 1; // 0x296a
                v5--;
                if (*(char *)v7 != *(char *)v5) {
                    // break -> 0x28fb
                    break;
                }
                v6 = v7;
            }
        }
    }
    goto lab_0x28fb;
  lab_0x28fb:
    // 0x28fb
    function_2440();
    int64_t v8 = (int64_t)g18; // 0x290e
    int64_t * v9 = (int64_t *)(v8 + 40); // 0x2917
    uint64_t v10 = *v9; // 0x2917
    if (v10 >= *(int64_t *)(v8 + 48)) {
        // 0x2980
        function_23f0();
    } else {
        // 0x2924
        *v9 = v10 + 1;
        *(char *)v10 = !use_nuls ? 10 : 0;
    }
    // 0x292f
    function_22f0();
}