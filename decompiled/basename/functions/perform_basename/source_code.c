perform_basename (char const *string, char const *suffix, bool use_nuls)
{
  char *name = base_name (string);
  strip_trailing_slashes (name);

  /* Per POSIX, 'basename // /' must return '//' on platforms with
     distinct //.  On platforms with drive letters, this generalizes
     to making 'basename c: :' return 'c:'.  This rule is captured by
     skipping suffix stripping if base_name returned an absolute path
     or a drive letter (only possible if name is a file-system
     root).  */
  if (suffix && IS_RELATIVE_FILE_NAME (name) && ! FILE_SYSTEM_PREFIX_LEN (name))
    remove_suffix (name, suffix);

  fputs (name, stdout);
  putchar (use_nuls ? '\0' : '\n');
  free (name);
}