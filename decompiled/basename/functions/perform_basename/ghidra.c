void perform_basename(undefined8 param_1,char *param_2,char param_3)

{
  char *__s;
  size_t sVar1;
  char *pcVar2;
  char *pcVar3;
  uint uVar4;
  
  __s = (char *)base_name();
  strip_trailing_slashes(__s);
  if ((param_2 != (char *)0x0) && (*__s != '/')) {
    sVar1 = strlen(__s);
    pcVar3 = __s + sVar1;
    sVar1 = strlen(param_2);
    pcVar2 = param_2 + sVar1;
    while (__s < pcVar3) {
      if (pcVar2 <= param_2) {
        if (__s < pcVar3) {
          *pcVar3 = '\0';
        }
        break;
      }
      pcVar3 = pcVar3 + -1;
      pcVar2 = pcVar2 + -1;
      if (*pcVar3 != *pcVar2) break;
    }
  }
  fputs_unlocked(__s,stdout);
  pcVar3 = stdout->_IO_write_ptr;
  uVar4 = -(uint)(param_3 == '\0') & 10;
  if (pcVar3 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar3 + 1;
    *pcVar3 = (char)uVar4;
  }
  else {
    __overflow(stdout,uVar4);
  }
  free(__s);
  return;
}