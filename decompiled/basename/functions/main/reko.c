void main(struct Eq_97 * rsi, int32 edi, struct Eq_357 * fs)
{
	set_program_name(rsi->a0000[0]);
	fn00000000000024F0("", 0x06);
	fn0000000000002380("/usr/local/share/locale", "coreutils");
	fn0000000000002360("coreutils");
	atexit(&g_t2D90);
	byte r15b_153 = 0x00;
	byte r12b_193 = 0x00;
	Eq_11 qwLoc40_314 = 0x00;
	while (true)
	{
		int32 eax_80 = fn00000000000023D0(&g_tAB60, "+as:z", rsi, edi, null);
		if (eax_80 == ~0x00)
			break;
		if (eax_80 != 0x61)
		{
			if (eax_80 <= 0x61)
			{
				if (eax_80 != ~0x82)
				{
					if (eax_80 != ~0x81)
						goto l0000000000002793;
					usage(0x00);
				}
				else
				{
					version_etc(0x7004, stdout, fs);
					fn0000000000002540(0x00);
				}
			}
			if (eax_80 != 115)
			{
				if (eax_80 != 122)
					goto l0000000000002793;
				r12b_193 = 0x01;
				continue;
			}
			qwLoc40_314 = optarg;
		}
		r15b_153 = 0x01;
	}
	int32 eax_131 = g_dwB090;
	if (eax_131 >= edi)
	{
		fn0000000000002510(fn0000000000002390(0x05, "missing operand", null), 0x00, 0x00);
l0000000000002793:
		usage(0x01);
	}
	else
	{
		uint32 r12d_303 = (uint32) r12b_193;
		if (r15b_153 != 0x00)
		{
			do
			{
				perform_basename(r12d_303, qwLoc40_314, rsi[(int64) eax_131 * 0x08 /64 16]);
				eax_131 = g_dwB090 + 0x01;
				g_dwB090 = eax_131;
			} while (eax_131 < edi);
		}
		else
		{
			int64 rcx_187 = (int64) eax_131;
			if (eax_131 + 0x02 < edi)
			{
				quote(rsi->a0010[rcx_187], fs);
				fn0000000000002510(fn0000000000002390(0x05, "extra operand %s", null), 0x00, 0x00);
				usage(0x01);
			}
			else
			{
				uint32 edx_206 = (word32) r12b_193;
				Eq_11 rsi_197 = 0x00;
				if (eax_131 + 0x02 == edi)
					rsi_197 = rsi->a0008[rcx_187];
				perform_basename(edx_206, rsi_197, rsi[rcx_187 * 0x08 /64 16]);
			}
		}
	}
}