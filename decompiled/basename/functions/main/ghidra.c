undefined8 main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  int iVar2;
  long lVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  undefined8 local_40;
  
  bVar1 = false;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  uVar5 = 0;
  textdomain("coreutils");
  atexit(close_stdout);
  local_40 = 0;
LAB_00102660:
  do {
    iVar2 = getopt_long(param_1,param_2,"+as:z",longopts,0);
    if (iVar2 == -1) {
      if (optind < param_1) {
        if (bVar1) {
          do {
            perform_basename(param_2[optind],local_40,uVar5);
            optind = optind + 1;
          } while (optind < param_1);
        }
        else {
          lVar3 = (long)optind;
          if (optind + 2 < param_1) {
            uVar5 = quote(param_2[lVar3 + 2]);
            uVar4 = dcgettext(0,"extra operand %s",5);
            error(0,0,uVar4,uVar5);
                    /* WARNING: Subroutine does not return */
            usage(1);
          }
          uVar4 = 0;
          if (optind + 2 == param_1) {
            uVar4 = param_2[lVar3 + 1];
          }
          perform_basename(param_2[lVar3],uVar4,uVar5);
        }
        return 0;
      }
      uVar5 = dcgettext(0,"missing operand",5);
      error(0,0,uVar5);
LAB_00102793:
                    /* WARNING: Subroutine does not return */
      usage(1);
    }
    if (iVar2 != 0x61) {
      if (iVar2 < 0x62) {
        if (iVar2 == -0x83) {
          version_etc(stdout,"basename","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar2 == -0x82) {
                    /* WARNING: Subroutine does not return */
          usage(0);
        }
        goto LAB_00102793;
      }
      if (iVar2 != 0x73) {
        if (iVar2 != 0x7a) goto LAB_00102793;
        uVar5 = 1;
        goto LAB_00102660;
      }
      local_40 = optarg;
    }
    bVar1 = true;
  } while( true );
}