main (int argc, char **argv)
{
  bool multiple_names = false;
  bool use_nuls = false;
  char const *suffix = NULL;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  while (true)
    {
      int c = getopt_long (argc, argv, "+as:z", longopts, NULL);

      if (c == -1)
        break;

      switch (c)
        {
        case 's':
          suffix = optarg;
          /* -s implies -a, so...  */
          FALLTHROUGH;

        case 'a':
          multiple_names = true;
          break;

        case 'z':
          use_nuls = true;
          break;

        case_GETOPT_HELP_CHAR;
        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);

        default:
          usage (EXIT_FAILURE);
        }
    }

  if (argc < optind + 1)
    {
      error (0, 0, _("missing operand"));
      usage (EXIT_FAILURE);
    }

  if (!multiple_names && optind + 2 < argc)
    {
      error (0, 0, _("extra operand %s"), quote (argv[optind + 2]));
      usage (EXIT_FAILURE);
    }

  if (multiple_names)
    {
      for (; optind < argc; optind++)
        perform_basename (argv[optind], suffix, use_nuls);
    }
  else
    perform_basename (argv[optind],
                      optind + 2 == argc ? argv[optind + 1] : NULL, use_nuls);

  return EXIT_SUCCESS;
}