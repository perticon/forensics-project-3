void main(int param_1,undefined8 *param_2)

{
  int iVar1;
  undefined8 uVar2;
  char *pcVar3;
  undefined *puVar4;
  undefined auVar5 [16];
  undefined8 uStack56;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar4 = &DAT_001087c4;
  textdomain("coreutils");
  hard_LC_COLLATE = hard_locale(3);
  atexit(close_stdout);
  only_file_1 = 1;
  only_file_2 = 1;
  both = 1;
  seen_unpairable = 0;
  issued_disorder_warning = 0;
  check_input_order = 0;
  total_option = 0;
LAB_00102776:
  uVar2 = getopt_long(param_1,param_2,&DAT_00108745,long_options,0);
  iVar1 = (int)uVar2;
  if (iVar1 == -1) {
LAB_001027f9:
    if (col_sep_len == 0) {
      col_sep_len = 1;
    }
    if (param_1 - optind < 2) {
      if (param_1 <= optind) {
        uVar2 = dcgettext(0,"missing operand",5);
        error(0,0,uVar2);
        goto LAB_0010285c;
      }
      puVar4 = (undefined *)quote(param_2[(long)param_1 + -1]);
      pcVar3 = "missing operand after %s";
    }
    else {
      if (param_1 - optind == 2) {
        compare_files(param_2 + optind);
        uVar2 = dcgettext(0,"multiple output delimiters specified",5);
        auVar5 = error(1,0,uVar2);
        uVar2 = uStack56;
        uStack56 = SUB168(auVar5,0);
        (*(code *)PTR___libc_start_main_0010bfd0)
                  (main,uVar2,&stack0xffffffffffffffd0,0,0,SUB168(auVar5 >> 0x40,0),&uStack56);
        do {
                    /* WARNING: Do nothing block with infinite loop */
        } while( true );
      }
      puVar4 = (undefined *)quote(param_2[(long)optind + 2]);
      pcVar3 = "extra operand %s";
    }
    uVar2 = dcgettext(0,pcVar3,5);
    error(0,0,uVar2,puVar4);
  }
  else {
    if (0x83 < iVar1) goto LAB_0010285c;
    if (0x79 < iVar1) {
      if (iVar1 - 0x7aU < 10) {
                    /* WARNING: Could not recover jumptable at 0x001027db. Too many branches */
                    /* WARNING: Treating indirect jump as call */
        (*(code *)(puVar4 + *(int *)(puVar4 + (ulong)(iVar1 - 0x7aU) * 4)))();
        return;
      }
      goto LAB_0010285c;
    }
    if (iVar1 == 0x31) {
      only_file_1 = 0;
      goto LAB_00102776;
    }
    if (0x31 < iVar1) {
      if (iVar1 == 0x32) {
        only_file_2 = 0;
      }
      else {
        if (iVar1 != 0x33) goto LAB_0010285c;
        both = 0;
      }
      goto LAB_00102776;
    }
    if (iVar1 == -0x83) {
      version_etc(stdout,&DAT_00108661,"GNU coreutils",Version,"Richard M. Stallman",
                  "David MacKenzie",0,uVar2);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar1 == -0x82) {
      usage(0);
      goto LAB_001027f9;
    }
  }
LAB_0010285c:
  usage();
  total_option = 1;
  goto LAB_00102776;
}