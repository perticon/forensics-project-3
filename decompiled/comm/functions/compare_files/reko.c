void compare_files(struct Eq_327 * (* rdi)[], struct Eq_409 * fs)
{
	Eq_819 fp;
	struct Eq_685 * qwLoc01D8;
	struct Eq_685 * qwLoc01D0;
	FILE * qwLoc01C8;
	FILE * qwLoc01C0;
	struct Eq_743 * rsi;
	Eq_825 rbp_148 = 0x00;
	struct Eq_827 * rbx_152 = fp - 0xF8;
	union Eq_830 * qwLoc0238_843 = fp - 0x0198;
	struct Eq_833 * r12_151 = fp - 440;
	do
	{
		union Eq_830 * r13_46 = qwLoc0238_843;
		struct Eq_827 * r14_48 = rbx_152 - 96;
		do
		{
			initbuffer(r14_48);
			r13_46 = (union Eq_830 *) ((char *) r13_46 + 8);
			*((char *) r13_46 - 8) = (union Eq_830 *) r14_48;
			r14_48 = &r14_48->qw0010 + 1;
		} while (rbx_152 != r14_48);
		FILE * r13_73;
		r12_151->dw0000 = 0x00;
		r12_151->dw0004 = 0x00;
		word64 rdi_68 = Mem65[rdi + rbp_148:word64];
		if (rdi_68->b0000 != 0x2D)
		{
l0000000000002CA8:
			r13_73 = fopen_safer("r", rdi_68, fs);
			goto l0000000000002CB3;
		}
		r13_73 = stdin;
		if (rdi_68->b0001 != 0x00)
			goto l0000000000002CA8;
l0000000000002CB3:
		Mem100[fp - 0x01C8 + rbp_148:word64] = r13_73;
		if (r13_73 == null)
			goto l0000000000003177;
		fadvise(r13_73);
		Mem138[fp - 0x01D8 + rbp_148:word64] = readlinebuffer_delim(g_bC010, r13_73, *qwLoc0238_843);
		if ((r13_73->t0000 & 0x20) != 0x00)
			goto l0000000000003106;
		rbp_148 = (word64) rbp_148 + 8;
		qwLoc0238_843 = (union Eq_830 *) ((char *) qwLoc0238_843 + 32);
		++r12_151;
		rbx_152 = &rbx_152->qw0010 + 0x0A;
	} while (rbp_148 != 0x10);
	uint64 qwLoc0200_1026 = 0x00;
	uint64 qwLoc0210_1036 = 0x00;
	uint64 qwLoc0208_1044 = 0x00;
l0000000000002D50:
	byte bLoc01D9_1063;
	byte al_451;
	FILE * rsi_291;
	if (qwLoc01D8 == null)
	{
		if (qwLoc01D0 == null)
		{
			if (rpl_fclose(qwLoc01C8) != 0x00)
				goto l000000000000313D;
			if (rpl_fclose(qwLoc01C0) == 0x00)
			{
				if (g_bC0FC != 0x00)
				{
					fn0000000000002420(0x05, "total", null);
					umaxtostr(fp - 88, qwLoc0200_1026);
					umaxtostr(fp - 0x78, qwLoc0210_1036);
					umaxtostr(fp - 0x98, qwLoc0208_1044);
					fn00000000000025D0(34355, 0x01);
				}
				if (g_aC0FD[0] == 0x00 && g_bC0FE == 0x00)
					fn0000000000002620(0x00);
				goto l00000000000031A6;
			}
l000000000000313D:
			quotearg_n_style_colon(0x03, fs);
			fn00000000000025E0(0x8809, *fn0000000000002390(), 0x01);
l0000000000003177:
			quotearg_n_style_colon(0x03, fs);
			fn00000000000025E0(0x8809, *fn0000000000002390(), 0x01);
l00000000000031A6:
			fn00000000000025E0(fn0000000000002420(0x05, "input is not in sorted order", null), 0x00, 0x01);
			usage(0x01);
		}
		g_bC0FF = 0x01;
		rsi_291 = stdout;
l0000000000002F0F:
		writeline(0x02, rsi_291, qwLoc01D0);
		++qwLoc0210_1036;
		bLoc01D9_1063 = 0x01;
		al_451 = 0x00;
l0000000000002DEC:
		Eq_1038 r15_443 = 0x01;
		Eq_1022 r14_448 = fp - (Eq_1156 (*)[]) 440;
		word32 ebx_479 = 0x00;
		if (al_451 == 0x00)
		{
l0000000000002DFF:
			do
			{
				r14_448 = (word64) r14_448 + 0x0C;
				if (r15_443 == 0x02)
					goto l0000000000002D50;
				r15_443.u0 = 0x02;
				ebx_479 = 0x01;
			} while (bLoc01D9_1063 == 0x00);
		}
		int64 r13_478 = (int64) *r14_448;
		word32 eax_487 = *((word64) r14_448 + 4);
		int32 edx_486 = (int32) g_bC010;
		FILE * rbp_495 = *((word64) (fp - 464) + r15_443 * 0x08);
		*((word64) r14_448 + 4) = (word32) r13_478;
		ui32 eax_498 = (word32) r13_478 + 0x01 & 0x03;
		*r14_448 = eax_498;
		int64 rbx_482 = (int64) ebx_479;
		word64 rax_538 = readlinebuffer_delim((byte) edx_486, rbp_495, *((word64) (fp - 0x0198) + ((int64) eax_498 + rbx_482 * 0x04) * 0x08));
		*((word64) (fp - 0x01E0) + r15_443 * 0x08) = rax_538;
		word32 r15d_570 = (word32) r15_443;
		if (rax_538 != 0x00)
		{
			word32 eax_595 = g_dwC0F8;
			if (eax_595 != 0x02 && (eax_595 == 0x01 || g_bC0FF != 0x00))
				check_order.part.0(r15d_570, rsi, *((word64) (fp - 0x0198) + (r13_478 + rbx_482 * 0x04) * 0x08));
		}
		else
		{
			struct Eq_744 * rdi_554 = *((word64) (fp - 0x0198) + (rbx_482 * 0x04 + (int64) eax_487) * 0x08);
			if (rdi_554->t0010 != 0x00)
			{
				word32 eax_559 = g_dwC0F8;
				if (eax_559 != 0x02 && (eax_559 == 0x01 || g_bC0FF != 0x00))
					check_order.part.0(r15d_570, *((word64) (fp - 0x0198) + (r13_478 + rbx_482 * 0x04) * 0x08), rdi_554);
			}
		}
		if ((rbp_495->t0000 & 0x20) == 0x00)
		{
			Mem822[fp - 0x01DB + r15_443:byte] = 0x00;
			goto l0000000000002DFF;
		}
		quotearg_n_style_colon(0x03, fs);
		fn00000000000025E0(0x8809, *fn0000000000002390(), 0x01);
l0000000000003106:
		quotearg_n_style_colon(0x03, fs);
		fn00000000000025E0(0x8809, *fn0000000000002390(), 0x01);
		goto l0000000000003135;
	}
	bLoc01D9_1063 = 0x00;
	if (qwLoc01D0 == null)
	{
		g_bC0FF = 0x01;
		rsi_291 = stdout;
		goto l0000000000002EDB;
	}
	Eq_786 eax_331;
	Eq_32 r14_299 = qwLoc01D0->t0008;
	Eq_32 r15_300 = qwLoc01D8->t0008;
	Eq_32 rsi_301 = qwLoc01D0->t0010;
	Eq_32 rdi_302 = qwLoc01D8->t0010;
	if (g_bC103 == 0x00)
	{
		Eq_32 rdx_323 = r14_299;
		if (r15_300 <= r14_299)
			rdx_323 = r15_300;
		eax_331 = fn00000000000024D0(rdx_323 - 0x01, rsi_301, rdi_302);
		rsi_291 = stdout;
		if (eax_331 != 0x00)
		{
l0000000000002ED0:
			g_bC0FF = 0x01;
			if (eax_331 <= 0x00)
			{
l0000000000002EDB:
				writeline(0x01, rsi_291, qwLoc01D8);
				++qwLoc0208_1044;
l0000000000002DE7:
				al_451 = 0x01;
				goto l0000000000002DEC;
			}
			goto l0000000000002F0F;
		}
		eax_331 = (uint32) (int8) (r15_300 > r14_299) - (word32) (r15_300 < r14_299);
	}
	else
		eax_331 = xmemcoll(r14_299 - 1, rsi_301, r15_300 - 0x01, rdi_302);
	rsi_291 = stdout;
	if (eax_331 == 0x00)
	{
		writeline(0x03, rsi_291, qwLoc01D0);
		++qwLoc0200_1026;
		bLoc01D9_1063 = 0x01;
		goto l0000000000002DE7;
	}
	goto l0000000000002ED0;
}