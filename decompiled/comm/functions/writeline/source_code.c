writeline (struct linebuffer const *line, FILE *stream, int class)
{
  switch (class)
    {
    case 1:
      if (!only_file_1)
        return;
      break;

    case 2:
      if (!only_file_2)
        return;
      if (only_file_1)
        fwrite (col_sep, 1, col_sep_len, stream);
      break;

    case 3:
      if (!both)
        return;
      if (only_file_1)
        fwrite (col_sep, 1, col_sep_len, stream);
      if (only_file_2)
        fwrite (col_sep, 1, col_sep_len, stream);
      break;
    }

  fwrite (line->buffer, sizeof (char), line->length, stream);
}