void writeline(long param_1,FILE *param_2,int param_3)

{
  if (param_3 == 2) {
    if (only_file_2 == '\0') {
      return;
    }
    if (only_file_1 == '\0') goto LAB_00102afd;
  }
  else {
    if (param_3 != 3) {
      if (only_file_1 == '\0') {
        return;
      }
      goto LAB_00102afd;
    }
    if (both == '\0') {
      return;
    }
    if (only_file_1 != '\0') {
      fwrite_unlocked(col_sep,1,col_sep_len,param_2);
    }
    if (only_file_2 == '\0') goto LAB_00102afd;
  }
  fwrite_unlocked(col_sep,1,col_sep_len,param_2);
LAB_00102afd:
  fwrite_unlocked(*(void **)(param_1 + 0x10),1,*(size_t *)(param_1 + 8),param_2);
  return;
}