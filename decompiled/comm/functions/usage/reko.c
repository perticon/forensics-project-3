void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002640(fn0000000000002420(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000322E;
	}
	fn00000000000025D0(fn0000000000002420(0x05, "Usage: %s [OPTION]... FILE1 FILE2\n", null), 0x01);
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "Compare sorted files FILE1 and FILE2 line by line.\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "\nWith no options, produce three-column output.  Column one contains\nlines unique to FILE1, column two contains lines unique to FILE2,\nand column three contains lines common to both files.\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "\n  -1                      suppress column 1 (lines unique to FILE1)\n  -2                      suppress column 2 (lines unique to FILE2)\n  -3                      suppress column 3 (lines that appear in both files)\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "\n      --check-order       check that the input is correctly sorted, even\n                            if all input lines are pairable\n      --nocheck-order     do not check that the input is correctly sorted\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "      --output-delimiter=STR  separate columns with STR\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "      --total             output a summary\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "  -z, --zero-terminated   line delimiter is NUL, not newline\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "      --version     output version information and exit\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "\nNote, comparisons honor the rules specified by 'LC_COLLATE'.\n", null));
	fn00000000000025D0(fn0000000000002420(0x05, "\nExamples:\n  %s -12 file1 file2  Print only lines present in both file1 and file2.\n  %s -3 file1 file2  Print lines in file1 not in file2, and vice versa.\n", null), 0x01);
	struct Eq_1398 * rbx_264 = fp - 0xB8 + 16;
	do
	{
		char * rsi_266 = rbx_264->qw0000;
		++rbx_264;
	} while (rsi_266 != null && fn0000000000002500(rsi_266, "comm") != 0x00);
	ptr64 r13_279 = rbx_264->qw0008;
	if (r13_279 != 0x00)
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_366 = fn00000000000025C0(null, 0x05);
		if (rax_366 == 0x00 || fn00000000000023A0(0x03, "en_", rax_366) == 0x00)
			goto l000000000000353E;
	}
	else
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_308 = fn00000000000025C0(null, 0x05);
		if (rax_308 == 0x00 || fn00000000000023A0(0x03, "en_", rax_308) == 0x00)
		{
			fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000357B:
			fn00000000000025D0(fn0000000000002420(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000322E:
			fn0000000000002620(edi);
		}
		r13_279 = 0x8661;
	}
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l000000000000353E:
	fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000357B;
}