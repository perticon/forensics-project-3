
struct s1 {
    unsigned char f0;
    signed char f1;
    signed char f2;
};

struct s0 {
    signed char[8] pad8;
    void* f8;
    struct s1* f10;
};

struct s2 {
    unsigned char f0;
    signed char[7] pad8;
    void* f8;
    struct s1* f10;
};

signed char only_file_2 = 0;

signed char only_file_1 = 0;

struct s1* col_sep_len = reinterpret_cast<struct s1*>(0);

struct s1* col_sep = reinterpret_cast<struct s1*>(0xc0);

void fun_2590(struct s1* rdi, int64_t rsi, struct s1* rdx, struct s2* rcx);

signed char both = 0;

void writeline(struct s0* rdi, struct s2* rsi, int32_t edx, struct s1* rcx) {
    int1_t zf5;
    int1_t zf6;
    struct s1* rdx7;
    struct s1* rdi8;
    int1_t zf9;
    int1_t zf10;
    int1_t zf11;
    struct s1* rdx12;
    struct s1* rdi13;
    int1_t zf14;

    if (edx == 2) {
        zf5 = only_file_2 == 0;
        if (!zf5) {
            zf6 = only_file_1 == 0;
            if (zf6) {
                addr_2afd_4:
            } else {
                addr_2b51_5:
                rdx7 = col_sep_len;
                rdi8 = col_sep;
                fun_2590(rdi8, 1, rdx7, rsi);
                goto addr_2afd_4;
            }
        }
    } else {
        if (edx != 3) {
            zf9 = only_file_1 == 0;
            if (!zf9) 
                goto addr_2afd_4;
        } else {
            zf10 = both == 0;
            if (!zf10) {
                zf11 = only_file_1 == 0;
                if (!zf11) {
                    rdx12 = col_sep_len;
                    rdi13 = col_sep;
                    fun_2590(rdi13, 1, rdx12, rsi);
                }
                zf14 = only_file_2 == 0;
                if (!zf14) 
                    goto addr_2b51_5; else 
                    goto addr_2afd_4;
            }
        }
    }
    return;
}

struct s3 {
    signed char[8] pad8;
    void* f8;
    struct s1* f10;
};

signed char hard_LC_COLLATE = 0;

uint32_t xmemcoll(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx);

uint32_t memcmp2(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx);

struct s1* fun_2420();

void fun_25e0();

/* check_order.part.0 */
void check_order_part_0(struct s3* rdi, struct s2* rsi, void* rdx) {
    int64_t rbx4;
    int1_t zf5;
    struct s1* rdx6;
    struct s1* rcx7;
    struct s1* rdi8;
    struct s1* rsi9;
    uint32_t eax10;

    rbx4 = reinterpret_cast<int64_t>(rdx) - 1;
    if (!*reinterpret_cast<signed char*>(0xc0fd + rbx4)) {
        zf5 = hard_LC_COLLATE == 0;
        rdx6 = rsi->f10;
        rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rsi->f8) + 0xffffffffffffffff);
        rdi8 = rdi->f10;
        rsi9 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rdi->f8) + 0xffffffffffffffff);
        if (!zf5) {
            eax10 = xmemcoll(rdi8, rsi9, rdx6, rcx7);
        } else {
            eax10 = memcmp2(rdi8, rsi9, rdx6, rcx7);
        }
        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax10) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax10 == 0))) {
            fun_2420();
            fun_25e0();
            *reinterpret_cast<signed char*>(0xc0fd + rbx4) = 1;
        }
    }
    return;
}

int64_t fun_2430();

int64_t fun_2380(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2430();
    if (r8d > 10) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x88c0 + rax11 * 4) + 0x88c0;
    }
}

struct s4 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s1* g28;

int32_t* fun_2390();

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_24a0();

struct s5 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2370(void** rdi);

void** xcharalloc(void** rdi, ...);

void fun_2450();

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s4* rcx, ...) {
    int64_t rbx5;
    struct s1* rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s5* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x51ff;
    rax8 = fun_2390();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6cf1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x528b;
            fun_24a0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s5*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xc120) {
                fun_2370(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x531a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2450();
        } else {
            return r14_19;
        }
    }
}

uint32_t fun_2570(struct s1* rdi, struct s1* rsi);

struct s1* fun_2440(struct s1* rdi, ...);

uint32_t strcoll_loop(struct s1* rdi, signed char* rsi, struct s1* rdx, signed char* rcx) {
    struct s1* r15_5;
    signed char* r13_6;
    signed char* r12_7;
    struct s1* rbp8;
    int32_t* rax9;
    int32_t* r14_10;
    uint32_t eax11;
    struct s1* rax12;
    signed char* rbx13;
    struct s1* rax14;
    signed char* rax15;

    r15_5 = rdi;
    r13_6 = rsi;
    r12_7 = rcx;
    rbp8 = rdx;
    rax9 = fun_2390();
    r14_10 = rax9;
    do {
        *r14_10 = 0;
        eax11 = fun_2570(r15_5, rbp8);
        if (eax11) 
            break;
        rax12 = fun_2440(r15_5, r15_5);
        rbx13 = &rax12->f1;
        rax14 = fun_2440(rbp8, rbp8);
        r15_5 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r15_5) + reinterpret_cast<uint64_t>(rbx13));
        rax15 = &rax14->f1;
        rbp8 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<uint64_t>(rax15));
        r12_7 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r12_7) - reinterpret_cast<uint64_t>(rax15));
        r13_6 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r13_6) - reinterpret_cast<uint64_t>(rbx13));
        if (!r13_6) 
            goto addr_7300_4;
    } while (r12_7);
    goto addr_7318_6;
    return eax11;
    addr_7300_4:
    return *reinterpret_cast<uint32_t*>(&rax15) - (*reinterpret_cast<uint32_t*>(&rax15) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax15) < *reinterpret_cast<uint32_t*>(&rax15) + reinterpret_cast<uint1_t>(!!r12_7)));
    addr_7318_6:
    return 1;
}

void initbuffer(void* rdi, struct s2* rsi);

struct s6 {
    signed char f0;
    signed char f1;
};

struct s2* stdin = reinterpret_cast<struct s2*>(0);

struct s2* fopen_safer();

void fadvise(struct s2* rdi, int64_t rsi);

unsigned char delim = 10;

struct s2* readlinebuffer_delim(void* rdi, struct s2* rsi, void* rdx);

int64_t quotearg_n_style_colon();

int32_t check_input_order = 0;

unsigned char seen_unpairable = 0;

struct s2* stdout = reinterpret_cast<struct s2*>(0);

struct s1* fun_24d0(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx);

struct s7 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int64_t rpl_fclose(struct s7* rdi, struct s2* rsi, void* rdx, struct s1* rcx);

signed char total_option = 0;

struct s1* umaxtostr(int64_t rdi, void* rsi, int64_t rdx);

void fun_25d0(int64_t rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx, struct s1* r8, struct s1* r9, struct s1* a7, struct s1* a8, struct s1* a9, int64_t a10, struct s1* a11, struct s1* a12, struct s1* a13, void* a14, void* a15, int64_t a16, int64_t a17, int64_t a18, unsigned char* a19, int64_t a20);

int16_t issued_disorder_warning = 0;

void fun_2620();

void compare_files(unsigned char* rdi, struct s2* rsi) {
    void* rbp3;
    int64_t* rsp4;
    unsigned char* v5;
    void* rbx6;
    struct s1* v7;
    void* r14_8;
    void* v9;
    struct s2* r13_10;
    struct s2* rax11;
    void* rdx12;
    struct s2* rax13;
    void* rdx14;
    uint32_t eax15;
    int64_t v16;
    void* v17;
    int64_t v18;
    int64_t v19;
    int64_t v20;
    void* v21;
    int64_t rbx22;
    int32_t ebx23;
    struct s1* rcx24;
    int64_t r8_25;
    struct s2* rbp26;
    void* r15_27;
    void* rdi28;
    struct s2* rax29;
    struct s3* rdi30;
    int64_t v31;
    int32_t eax32;
    int1_t zf33;
    int32_t eax34;
    int1_t zf35;
    struct s3* rdi36;
    uint32_t eax37;
    unsigned char v38;
    struct s0* v39;
    struct s0* v40;
    int1_t zf41;
    void* r14_42;
    void* r15_43;
    struct s1* rsi44;
    struct s1* rdi45;
    struct s1* rax46;
    int32_t eax47;
    void* rdx48;
    int32_t eax49;
    struct s7* v50;
    int64_t rax51;
    struct s7* v52;
    int64_t rax53;
    void* rsp54;
    int1_t zf55;
    int64_t r15_56;
    struct s1* rax57;
    void* rsp58;
    struct s1* rcx59;
    struct s1* rax60;
    struct s1** rsp61;
    struct s1* r9_62;
    struct s1* rax63;
    void* rsp64;
    struct s1* r13_65;
    struct s1* rax66;
    int1_t zf67;
    int1_t zf68;

    *reinterpret_cast<int32_t*>(&rbp3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    rsp4 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x208);
    v5 = rdi;
    rbx6 = reinterpret_cast<void*>(rsp4 + 40);
    v7 = reinterpret_cast<struct s1*>(rsp4 + 14);
    do {
        r14_8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbx6) - 96);
        do {
            initbuffer(r14_8, rsi);
            rsp4 = rsp4 - 1 + 1;
            v9 = r14_8;
            r14_8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r14_8) + 24);
        } while (rbx6 != r14_8);
        if ((*reinterpret_cast<struct s6**>(reinterpret_cast<int64_t>(v5) + reinterpret_cast<uint64_t>(rbp3)))->f0 == 45) {
            r13_10 = stdin;
            if (!(*reinterpret_cast<struct s6**>(reinterpret_cast<int64_t>(v5) + reinterpret_cast<uint64_t>(rbp3)))->f1) {
                addr_2cb3_6:
                *reinterpret_cast<struct s2**>(reinterpret_cast<unsigned char>(v7) + reinterpret_cast<uint64_t>(rbp3)) = r13_10;
                if (!r13_10) 
                    break;
            } else {
                goto addr_2ca8_8;
            }
        } else {
            addr_2ca8_8:
            rax11 = fopen_safer();
            rsp4 = rsp4 - 1 + 1;
            r13_10 = rax11;
            goto addr_2cb3_6;
        }
        fadvise(r13_10, 2);
        *reinterpret_cast<int32_t*>(&rdx12) = reinterpret_cast<signed char>(delim);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
        rsi = r13_10;
        rax13 = readlinebuffer_delim(v9, rsi, rdx12);
        rsp4 = rsp4 - 1 + 1 - 1 + 1;
        rdx14 = reinterpret_cast<void*>(rsp4 + 12);
        *reinterpret_cast<struct s2**>(reinterpret_cast<int64_t>(rsp4) + reinterpret_cast<uint64_t>(rbp3) + 96) = rax13;
        eax15 = r13_10->f0 & 32;
        if (eax15) 
            goto addr_3106_10;
        rbp3 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp3) + 8);
        rbx6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbx6) + 96);
    } while (!reinterpret_cast<int1_t>(rbp3 == 16));
    goto addr_2d1a_12;
    addr_3177_13:
    quotearg_n_style_colon();
    fun_2390();
    fun_25e0();
    addr_31a6_14:
    fun_2420();
    fun_25e0();
    addr_3106_10:
    quotearg_n_style_colon();
    fun_2390();
    fun_25e0();
    goto addr_3135_16;
    addr_2d1a_12:
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v16) + 4) = eax15;
    v17 = rdx14;
    v18 = 0;
    v19 = 0;
    v20 = 0;
    v21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) + 93);
    goto addr_2d50_17;
    addr_30d1_18:
    quotearg_n_style_colon();
    fun_2390();
    fun_25e0();
    goto addr_3106_10;
    while (1) {
        rbx22 = ebx23;
        rcx24 = v7;
        r8_25 = rbx22 * 4;
        *reinterpret_cast<int32_t*>(&rdx14) = reinterpret_cast<signed char>(delim);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
        *reinterpret_cast<int32_t*>(&v16) = 0;
        rbp26 = *reinterpret_cast<struct s2**>(reinterpret_cast<unsigned char>(rcx24) + reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r15_27) * 8) - 8);
        rdi28 = *reinterpret_cast<void**>(rsp4 + (1 + r8_25) + 20);
        rax29 = readlinebuffer_delim(rdi28, rbp26, rdx14);
        rsp4 = rsp4 - 1 + 1;
        rsi = rax29;
        *reinterpret_cast<struct s2**>(reinterpret_cast<int64_t>(v17) + reinterpret_cast<int64_t>(r15_27) * 8 - 8) = rsi;
        if (!rsi) {
            rdi30 = *reinterpret_cast<struct s3**>(rsp4 + r8_25 + 20);
            if (v31 && ((eax32 = check_input_order, eax32 != 2) && (eax32 == 1 || (zf33 = seen_unpairable == 0, !zf33)))) {
                *reinterpret_cast<int32_t*>(&rdx14) = *reinterpret_cast<int32_t*>(&r15_27);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
                rsi = *reinterpret_cast<struct s2**>(rsp4 + rbx22 * 4 + 20);
                check_order_part_0(rdi30, rsi, rdx14);
                rsp4 = rsp4 - 1 + 1;
            }
        } else {
            eax34 = check_input_order;
            if (eax34 != 2 && (eax34 == 1 || (zf35 = seen_unpairable == 0, !zf35))) {
                *reinterpret_cast<int32_t*>(&rdx14) = *reinterpret_cast<int32_t*>(&r15_27);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
                rdi36 = *reinterpret_cast<struct s3**>(rsp4 + rbx22 * 4 + 20);
                check_order_part_0(rdi36, rsi, rdx14);
                rsp4 = rsp4 - 1 + 1;
            }
        }
        if (rbp26->f0 & 32) 
            goto addr_30d1_18;
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v21) + reinterpret_cast<int64_t>(r15_27)) = 0;
        while (1) {
            do {
                if (r15_27 == 2) 
                    break;
                eax37 = v38;
                *reinterpret_cast<int32_t*>(&r15_27) = 2;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_27) + 4) = 0;
                ebx23 = 1;
            } while (!*reinterpret_cast<signed char*>(&eax37));
            break;
            addr_2d50_17:
            if (!v39) {
                if (!v40) 
                    goto addr_2fd6_29;
                seen_unpairable = 1;
                rsi = stdout;
                goto addr_2f0f_31;
            }
            v38 = 0;
            if (!v40) {
                seen_unpairable = 1;
                rsi = stdout;
                goto addr_2edb_34;
            }
            zf41 = hard_LC_COLLATE == 0;
            r14_42 = v40->f8;
            r15_43 = v39->f8;
            rsi44 = v40->f10;
            rdi45 = v39->f10;
            if (zf41) 
                goto addr_2d8e_36;
            rcx24 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(r14_42) + 0xffffffffffffffff);
            *reinterpret_cast<uint32_t*>(&rax46) = xmemcoll(rdi45, reinterpret_cast<int64_t>(r15_43) - 1, rsi44, rcx24);
            rsp4 = rsp4 - 1 + 1;
            addr_2dc0_38:
            rsi = stdout;
            if (*reinterpret_cast<uint32_t*>(&rax46)) {
                addr_2ed0_39:
                seen_unpairable = 1;
                if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax46) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax46) == 0))) {
                    addr_2f0f_31:
                    *reinterpret_cast<int32_t*>(&rdx14) = 2;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
                    ++v19;
                    writeline(v40, rsi, 2, rcx24);
                    rsp4 = rsp4 - 1 + 1;
                    v38 = 1;
                    eax47 = 0;
                } else {
                    addr_2edb_34:
                    *reinterpret_cast<int32_t*>(&rdx14) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
                    ++v20;
                    writeline(v39, rsi, 1, rcx24);
                    rsp4 = rsp4 - 1 + 1;
                    goto addr_2de7_40;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rdx14) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
                ++v18;
                writeline(v40, rsi, 3, rcx24);
                rsp4 = rsp4 - 1 + 1;
                v38 = 1;
                goto addr_2de7_40;
            }
            addr_2dec_42:
            *reinterpret_cast<int32_t*>(&r15_27) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_27) + 4) = 0;
            ebx23 = 0;
            if (*reinterpret_cast<signed char*>(&eax47)) 
                break; else 
                continue;
            addr_2de7_40:
            eax47 = 1;
            goto addr_2dec_42;
            addr_2d8e_36:
            rdx48 = r14_42;
            if (reinterpret_cast<int64_t>(r15_43) <= reinterpret_cast<int64_t>(r14_42)) {
                rdx48 = r15_43;
            }
            rax46 = fun_24d0(rdi45, rsi44, reinterpret_cast<int64_t>(rdx48) - 1, rcx24);
            rsp4 = rsp4 - 1 + 1;
            rsi = stdout;
            if (*reinterpret_cast<uint32_t*>(&rax46)) 
                goto addr_2ed0_39;
            eax49 = 0;
            *reinterpret_cast<unsigned char*>(&eax49) = reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(r15_43) > reinterpret_cast<int64_t>(r14_42));
            *reinterpret_cast<uint32_t*>(&rax46) = eax49 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(r15_43) < reinterpret_cast<int64_t>(r14_42)));
            goto addr_2dc0_38;
        }
    }
    addr_2fd6_29:
    rax51 = rpl_fclose(v50, rsi, rdx14, rcx24);
    if (*reinterpret_cast<int32_t*>(&rax51)) {
        addr_313d_46:
        quotearg_n_style_colon();
        fun_2390();
        fun_25e0();
        goto addr_3177_13;
    } else {
        rax53 = rpl_fclose(v52, rsi, rdx14, rcx24);
        rsp54 = reinterpret_cast<void*>(rsp4 - 1 + 1 - 1 + 1);
        if (*reinterpret_cast<int32_t*>(&rax53)) {
            addr_3135_16:
            goto addr_313d_46;
        } else {
            zf55 = total_option == 0;
            if (zf55) 
                goto addr_3003_49;
            while (1) {
                *reinterpret_cast<uint32_t*>(&r15_56) = delim;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_56) + 4) = 0;
                rax57 = fun_2420();
                rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                rcx59 = col_sep;
                rax60 = umaxtostr(v18, reinterpret_cast<int64_t>(rsp58) + 0x1e0, 5);
                rsp61 = reinterpret_cast<struct s1**>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
                r9_62 = col_sep;
                rax63 = umaxtostr(v19, rsp61 + 0x1c0, 5);
                rsp64 = reinterpret_cast<void*>(rsp61 - 8 + 8);
                r13_65 = col_sep;
                rax66 = umaxtostr(v20, reinterpret_cast<int64_t>(rsp64) + 0x1a0, 5);
                fun_25d0(1, "%s%s%s%s%s%s%s%c", rax66, r13_65, rax63, r9_62, rax60, rcx59, rax57, r15_56, r9_62, rcx59, v7, v17, v21, v19, v20, v18, v5, v16);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
                addr_3003_49:
                zf67 = *reinterpret_cast<signed char*>(&issued_disorder_warning) == 0;
                if (!zf67) 
                    goto addr_31a6_14;
                zf68 = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&issued_disorder_warning) + 1) == 0;
                if (!zf68) 
                    goto addr_31a6_14;
                fun_2620();
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            }
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s8 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s8* locale_charset();

/* gettext_quote.part.0 */
struct s1* gettext_quote_part_0(struct s1* rdi, int32_t esi, struct s1* rdx) {
    struct s8* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s1* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s1* rax10;
    struct s1* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s1*>(0x8863);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax7 = reinterpret_cast<struct s1*>(0x885c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s1*>(0x8867);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax10 = reinterpret_cast<struct s1*>(0x8858);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s1*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s1*>("'");
    }
    return rax11;
}

int64_t quotearg_n_style_mem();

void collate_error(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    fun_2420();
    fun_25e0();
    fun_2420();
    fun_25e0();
    quotearg_n_style_mem();
    quotearg_n_style_mem();
    fun_2420();
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbe38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbe38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2353() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_2363() {
    __asm__("cli ");
    goto __uflow;
}

int64_t free = 0x2040;

void fun_2373() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2383() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2393() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_23d3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20b0;

void fun_23e3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20c0;

void fun_23f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2403() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2413() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2423() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2433() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2443() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2453() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2463() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2473() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strrchr = 0x2150;

void fun_2483() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_2493() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2170;

void fun_24a3() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x2180;

void fun_24b3() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x2190;

void fun_24c3() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x21a0;

void fun_24d3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21b0;

void fun_24e3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21c0;

void fun_24f3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21d0;

void fun_2503() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21e0;

void fun_2513() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x21f0;

void fun_2523() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2200;

void fun_2533() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2210;

void fun_2543() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2220;

void fun_2553() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2230;

void fun_2563() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t strcoll = 0x2240;

void fun_2573() {
    __asm__("cli ");
    goto strcoll;
}

int64_t __freading = 0x2250;

void fun_2583() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2260;

void fun_2593() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2270;

void fun_25a3() {
    __asm__("cli ");
    goto realloc;
}

int64_t fdopen = 0x2280;

void fun_25b3() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x2290;

void fun_25c3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22a0;

void fun_25d3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22b0;

void fun_25e3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22c0;

void fun_25f3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x22d0;

void fun_2603() {
    __asm__("cli ");
    goto fopen;
}

int64_t __cxa_atexit = 0x22e0;

void fun_2613() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22f0;

void fun_2623() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2300;

void fun_2633() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2310;

void fun_2643() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2320;

void fun_2653() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2330;

void fun_2663() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2340;

void fun_2673() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(unsigned char rdi);

struct s1* fun_25c0(int64_t rdi, ...);

void fun_2410(int64_t rdi, int64_t rsi);

void fun_23f0(int64_t rdi, int64_t rsi);

signed char hard_locale();

void atexit(int64_t rdi, int64_t rsi);

int64_t fun_2460(int64_t rdi, struct s2* rsi, int64_t rdx, int64_t rcx);

int32_t optind = 0;

int32_t* quote(int64_t rdi, struct s2* rsi);

int64_t Version = 0x87ec;

void version_etc(struct s2* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

void usage();

void fun_26c3(int32_t edi, struct s2* rsi) {
    int64_t rbp3;
    struct s2* rbx4;
    unsigned char rdi5;
    int32_t* r12_6;
    signed char al7;
    int64_t rdi8;
    int64_t rax9;
    int1_t zf10;
    int64_t rax11;
    int32_t edx12;
    int64_t rdi13;
    int32_t* rax14;
    int64_t rdi15;
    int32_t* rax16;
    int64_t rax17;
    struct s2* rdi18;
    int64_t rcx19;

    __asm__("cli ");
    rbp3 = edi;
    rbx4 = rsi;
    rdi5 = rsi->f0;
    set_program_name(rdi5);
    fun_25c0(6, 6);
    fun_2410("coreutils", "/usr/local/share/locale");
    r12_6 = reinterpret_cast<int32_t*>(0x87c4);
    fun_23f0("coreutils", "/usr/local/share/locale");
    al7 = hard_locale();
    hard_LC_COLLATE = al7;
    atexit(0x3690, "/usr/local/share/locale");
    only_file_1 = 1;
    only_file_2 = 1;
    both = 1;
    seen_unpairable = 0;
    issued_disorder_warning = 0;
    check_input_order = 0;
    total_option = 0;
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi8) = *reinterpret_cast<int32_t*>(&rbp3);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        rax9 = fun_2460(rdi8, rbx4, "123z", 0xbae0);
        if (*reinterpret_cast<int32_t*>(&rax9) == -1) {
            addr_27f9_3:
            zf10 = col_sep_len == 0;
            if (zf10) {
                col_sep_len = reinterpret_cast<struct s1*>(1);
            }
        } else {
            if (*reinterpret_cast<int32_t*>(&rax9) > 0x83) 
                goto addr_285c_6;
            if (*reinterpret_cast<int32_t*>(&rax9) > 0x79) 
                goto addr_27c8_8; else 
                goto addr_279e_9;
        }
        rax11 = optind;
        edx12 = *reinterpret_cast<int32_t*>(&rbp3) - *reinterpret_cast<int32_t*>(&rax11);
        if (edx12 <= 1) {
            if (*reinterpret_cast<int32_t*>(&rax11) < *reinterpret_cast<int32_t*>(&rbp3)) {
                rdi13 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rbx4) + rbp3 * 8 - 8);
                rax14 = quote(rdi13, rbx4);
                r12_6 = rax14;
            } else {
                fun_2420();
                fun_25e0();
                goto addr_285c_6;
            }
        } else {
            if (edx12 == 2) 
                break;
            rdi15 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rbx4) + rax11 * 8 + 16);
            rax16 = quote(rdi15, rbx4);
            r12_6 = rax16;
        }
        fun_2420();
        fun_25e0();
        goto addr_285c_6;
        addr_27c8_8:
        *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<int32_t*>(&rax9) - 0x7a;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rax17) > 9) 
            goto addr_285c_6; else 
            goto addr_27d4_17;
        addr_279e_9:
        if (*reinterpret_cast<int32_t*>(&rax9) == 49) {
            only_file_1 = 0;
            continue;
        }
        if (*reinterpret_cast<int32_t*>(&rax9) > 49) 
            goto addr_27a9_20;
        if (*reinterpret_cast<int32_t*>(&rax9) == 0xffffff7d) {
            rdi18 = stdout;
            rcx19 = Version;
            version_etc(rdi18, "comm", "GNU coreutils", rcx19, "Richard M. Stallman", "David MacKenzie", 0, rax9);
            fun_2620();
        } else {
            if (*reinterpret_cast<int32_t*>(&rax9) != 0xffffff7e) {
                addr_285c_6:
                usage();
                total_option = 1;
                continue;
            } else {
                usage();
                goto addr_27f9_3;
            }
        }
        only_file_2 = 0;
        continue;
        addr_27a9_20:
        if (*reinterpret_cast<int32_t*>(&rax9) != 50) {
            if (*reinterpret_cast<int32_t*>(&rax9) == 51) {
                both = 0;
            }
        }
    }
    compare_files(reinterpret_cast<int64_t>(rbx4) + rax11 * 8, rbx4);
    fun_2420();
    fun_25e0();
    addr_27d4_17:
    goto r12_6[rax17] + reinterpret_cast<int64_t>(r12_6);
}

int64_t __libc_start_main = 0;

void fun_29c3() {
    __asm__("cli ");
    __libc_start_main(0x26c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2350(int64_t rdi);

int64_t fun_2a63() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2350(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2aa3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s1* program_name = reinterpret_cast<struct s1*>(0);

void fun_24e0(struct s1* rdi, struct s2* rsi, int64_t rdx, struct s1* rcx);

int32_t fun_2500(struct s1* rdi, struct s1* rsi, struct s1* rdx);

int32_t fun_23a0(struct s1* rdi, int64_t rsi, int64_t rdx, struct s1* rcx);

struct s2* stderr = reinterpret_cast<struct s2*>(0);

void fun_2640(struct s2* rdi, int64_t rsi, struct s1* rdx, struct s1* rcx, struct s1* r8, struct s1* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_31d3(int32_t edi) {
    struct s1* r12_2;
    struct s1* rax3;
    struct s1* v4;
    struct s1* rax5;
    struct s1* rcx6;
    struct s1* r8_7;
    struct s1* r9_8;
    struct s2* r12_9;
    struct s1* rax10;
    struct s2* r12_11;
    struct s1* rax12;
    struct s2* r12_13;
    struct s1* rax14;
    struct s2* r12_15;
    struct s1* rax16;
    struct s2* r12_17;
    struct s1* rax18;
    struct s2* r12_19;
    struct s1* rax20;
    struct s2* r12_21;
    struct s1* rax22;
    struct s2* r12_23;
    struct s1* rax24;
    struct s2* r12_25;
    struct s1* rax26;
    struct s2* r12_27;
    struct s1* rax28;
    struct s2* r12_29;
    struct s1* rax30;
    struct s1* r12_31;
    struct s1* rax32;
    struct s1* rdx33;
    struct s1* r8_34;
    struct s1* r9_35;
    int32_t eax36;
    struct s1* r13_37;
    struct s1* rax38;
    struct s1* r8_39;
    struct s1* r9_40;
    struct s1* rax41;
    int32_t eax42;
    struct s1* rax43;
    struct s1* r8_44;
    struct s1* r9_45;
    struct s1* rax46;
    struct s1* r8_47;
    struct s1* r9_48;
    struct s1* rax49;
    int32_t eax50;
    struct s1* rax51;
    struct s1* r8_52;
    struct s1* r9_53;
    struct s2* r15_54;
    struct s1* rax55;
    struct s1* rax56;
    struct s1* r8_57;
    struct s1* r9_58;
    struct s1* rax59;
    struct s2* rdi60;
    struct s1* r8_61;
    struct s1* r9_62;
    int64_t v63;
    int64_t v64;
    int64_t v65;
    int64_t v66;
    int64_t v67;
    int64_t v68;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2420();
            fun_25d0(1, rax5, r12_2, rcx6, r8_7, r9_8, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_9 = stdout;
            rax10 = fun_2420();
            fun_24e0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2420();
            fun_24e0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2420();
            fun_24e0(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2420();
            fun_24e0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2420();
            fun_24e0(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2420();
            fun_24e0(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_2420();
            fun_24e0(rax22, r12_21, 5, rcx6);
            r12_23 = stdout;
            rax24 = fun_2420();
            fun_24e0(rax24, r12_23, 5, rcx6);
            r12_25 = stdout;
            rax26 = fun_2420();
            fun_24e0(rax26, r12_25, 5, rcx6);
            r12_27 = stdout;
            rax28 = fun_2420();
            fun_24e0(rax28, r12_27, 5, rcx6);
            r12_29 = stdout;
            rax30 = fun_2420();
            fun_24e0(rax30, r12_29, 5, rcx6);
            r12_31 = program_name;
            rax32 = fun_2420();
            rdx33 = r12_31;
            fun_25d0(1, rax32, rdx33, r12_31, r8_34, r9_35, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            do {
                if (1) 
                    break;
                eax36 = fun_2500("comm", 0, rdx33);
            } while (eax36);
            r13_37 = v4;
            if (!r13_37) {
                rax38 = fun_2420();
                fun_25d0(1, rax38, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_39, r9_40, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax41 = fun_25c0(5);
                if (!rax41 || (eax42 = fun_23a0(rax41, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax42)) {
                    rax43 = fun_2420();
                    r13_37 = reinterpret_cast<struct s1*>("comm");
                    fun_25d0(1, rax43, "https://www.gnu.org/software/coreutils/", "comm", r8_44, r9_45, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s1*>(" invocation");
                } else {
                    r13_37 = reinterpret_cast<struct s1*>("comm");
                    goto addr_3638_9;
                }
            } else {
                rax46 = fun_2420();
                fun_25d0(1, rax46, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_47, r9_48, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax49 = fun_25c0(5);
                if (!rax49 || (eax50 = fun_23a0(rax49, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax50)) {
                    addr_353e_11:
                    rax51 = fun_2420();
                    fun_25d0(1, rax51, "https://www.gnu.org/software/coreutils/", "comm", r8_52, r9_53, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s1*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_37 == "comm")) {
                        r12_2 = reinterpret_cast<struct s1*>(0x87c1);
                    }
                } else {
                    addr_3638_9:
                    r15_54 = stdout;
                    rax55 = fun_2420();
                    fun_24e0(rax55, r15_54, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_353e_11;
                }
            }
            rax56 = fun_2420();
            rcx6 = r12_2;
            fun_25d0(1, rax56, r13_37, rcx6, r8_57, r9_58, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            addr_322e_14:
            fun_2620();
        }
    } else {
        rax59 = fun_2420();
        rdi60 = stderr;
        rcx6 = r12_2;
        fun_2640(rdi60, 1, rax59, rcx6, r8_61, r9_62, v63, v64, v65, v66, v67, v68);
        goto addr_322e_14;
    }
}

int64_t file_name = 0;

void fun_3673(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3683(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s2* rdi);

struct s1* quotearg_colon();

int32_t exit_failure = 1;

struct s1* fun_23b0(int64_t rdi, int64_t rsi, int64_t rdx, struct s1* rcx, struct s1* r8);

void fun_3693() {
    struct s2* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s2* rdi6;
    int32_t eax7;
    struct s1* rax8;
    int64_t rdi9;
    struct s1* rax10;
    int64_t rsi11;
    struct s1* r8_12;
    struct s1* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2390(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2420();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3723_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25e0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3723_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25e0();
    }
}

void fun_3743() {
    __asm__("cli ");
}

uint32_t fun_2530(struct s7* rdi);

void fun_3753(struct s7* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2530(rdi);
        goto 0x24c0;
    }
}

int32_t fun_2580(struct s7* rdi);

int64_t fun_2490(int64_t rdi, ...);

int32_t rpl_fflush(struct s7* rdi);

int64_t fun_2400(struct s7* rdi);

int64_t fun_3783(struct s7* rdi) {
    uint32_t eax2;
    int32_t eax3;
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2530(rdi);
    if (reinterpret_cast<int32_t>(eax2) >= reinterpret_cast<int32_t>(0)) {
        eax3 = fun_2580(rdi);
        if (!(eax3 && (eax4 = fun_2530(rdi), *reinterpret_cast<uint32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2490(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2390();
            r12d9 = *rax8;
            rax10 = fun_2400(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2400;
}

void rpl_fseeko(struct s7* rdi);

void fun_3813(struct s7* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2580(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

struct s7* fun_2600();

int32_t dup_safer(int64_t rdi);

struct s7* fun_25b0(int64_t rdi, int64_t rsi);

void fun_24b0(int64_t rdi, int64_t rsi, int64_t rdx);

struct s7* fun_3863() {
    struct s7* rax1;
    struct s7* r12_2;
    uint32_t eax3;
    int64_t rdi4;
    int32_t eax5;
    int32_t* rax6;
    struct s7* rdi7;
    int32_t r13d8;
    struct s2* rsi9;
    void* rdx10;
    struct s1* rcx11;
    struct s2* rsi12;
    void* rdx13;
    struct s1* rcx14;
    int64_t rax15;
    int64_t rsi16;
    int64_t rsi17;
    int64_t rdi18;
    struct s7* rax19;
    int32_t* rax20;
    int64_t rdi21;
    int32_t r12d22;
    int64_t rdx23;

    __asm__("cli ");
    rax1 = fun_2600();
    r12_2 = rax1;
    if (!rax1) 
        goto addr_3886_2;
    eax3 = fun_2530(rax1);
    if (eax3 > 2) 
        goto addr_3886_2;
    *reinterpret_cast<uint32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    eax5 = dup_safer(rdi4);
    if (eax5 < 0) {
        rax6 = fun_2390();
        rdi7 = r12_2;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_2) + 4) = 0;
        r13d8 = *rax6;
        rpl_fclose(rdi7, rsi9, rdx10, rcx11);
        *rax6 = r13d8;
        goto addr_3886_2;
    } else {
        rax15 = rpl_fclose(r12_2, rsi12, rdx13, rcx14);
        if (!*reinterpret_cast<int32_t*>(&rax15)) {
            rsi16 = rsi17;
            *reinterpret_cast<int32_t*>(&rdi18) = eax5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0;
            rax19 = fun_25b0(rdi18, rsi16);
            r12_2 = rax19;
            if (rax19) {
                addr_3886_2:
                return r12_2;
            }
        }
        rax20 = fun_2390();
        *reinterpret_cast<int32_t*>(&rdi21) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
        r12d22 = *rax20;
        fun_24b0(rdi21, rsi16, rdx23);
        *rax20 = r12d22;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_2) + 4) = 0;
        goto addr_3886_2;
    }
}

int64_t fun_3903(struct s7* rdi, int64_t rsi, int32_t edx) {
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2530(rdi);
        *reinterpret_cast<uint32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2490(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t setlocale_null_r();

int64_t fun_3983() {
    struct s1* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax3;
    }
}

struct s9 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_3a03(uint64_t rdi, struct s9* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

struct s10 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_3a63(struct s10* rdi) {
    __asm__("cli ");
    __asm__("pxor xmm0, xmm0");
    rdi->f10 = 0;
    __asm__("movups [rdi], xmm0");
    return;
}

struct s11 {
    void* f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

uint32_t fun_2360(struct s11* rdi, struct s11* rsi, void** rdx);

struct s11* fun_3a83(struct s11* rdi, struct s11* rsi, int32_t edx) {
    void** rdx3;
    uint32_t r13d4;
    void** r15_5;
    void* rcx6;
    int32_t v7;
    unsigned char v8;
    void** rcx9;
    struct s11* r14_10;
    struct s11* rbp11;
    void** r12_12;
    void** rax13;
    uint32_t ebx14;
    uint32_t eax15;
    uint32_t eax16;
    uint32_t r11d17;
    void* r12_18;
    void** rax19;
    void* rcx20;
    void** rax21;
    void** rax22;
    uint32_t esi23;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    __asm__("cli ");
    r13d4 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&rdx3)));
    r15_5 = rdi->f10;
    rcx6 = rdi->f0;
    v7 = *reinterpret_cast<int32_t*>(&rdx3);
    v8 = *reinterpret_cast<unsigned char*>(&r13d4);
    rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx6) + reinterpret_cast<unsigned char>(r15_5));
    if (reinterpret_cast<unsigned char>(rsi->f0) & 16) {
        addr_3b70_2:
        return 0;
    } else {
        r14_10 = rdi;
        rbp11 = rsi;
        r12_12 = r15_5;
        do {
            rax13 = rbp11->f8;
            if (reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rbp11->f10)) {
                rdx3 = rax13 + 1;
                rbp11->f8 = rdx3;
                ebx14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13));
            } else {
                eax15 = fun_2360(rbp11, rsi, rdx3);
                rcx9 = rcx9;
                ebx14 = eax15;
                if (eax15 == 0xffffffff) {
                    if (r15_5 == r12_12) 
                        goto addr_3b70_2;
                    if (reinterpret_cast<unsigned char>(rbp11->f0) & 32) 
                        goto addr_3b70_2;
                    eax16 = v8;
                    if (*reinterpret_cast<signed char*>(r12_12 - 1) == *reinterpret_cast<signed char*>(&eax16)) 
                        break;
                    ebx14 = r13d4;
                    if (r12_12 != rcx9) 
                        goto addr_3b88_11;
                    r11d17 = v8;
                    goto addr_3b30_13;
                }
            }
            r11d17 = ebx14;
            if (r12_12 == rcx9) {
                addr_3b30_13:
                rsi = r14_10;
                r12_18 = r14_10->f0;
                *reinterpret_cast<int32_t*>(&rdx3) = 1;
                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                rax19 = xpalloc();
                rcx20 = r14_10->f0;
                r11d17 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r11d17));
                r15_5 = rax19;
                rax21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax19) + reinterpret_cast<unsigned char>(r12_18));
                r14_10->f10 = r15_5;
                rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx20) + reinterpret_cast<unsigned char>(r15_5));
            } else {
                rax21 = r12_12;
            }
            *reinterpret_cast<void***>(rax21) = *reinterpret_cast<void***>(&r11d17);
            r12_12 = rax21 + 1;
        } while (r13d4 != ebx14);
    }
    rax22 = r12_12;
    addr_3b96_18:
    r14_10->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax22) - reinterpret_cast<unsigned char>(r15_5));
    return r14_10;
    addr_3b88_11:
    esi23 = *reinterpret_cast<unsigned char*>(&v7);
    rax22 = r12_12 + 1;
    *reinterpret_cast<void***>(r12_12) = *reinterpret_cast<void***>(&esi23);
    goto addr_3b96_18;
}

void fun_3bc3() {
    __asm__("cli ");
    goto readlinebuffer_delim;
}

void fun_3bd3(int64_t rdi) {
    __asm__("cli ");
    goto fun_2370;
}

struct s1* fun_3be3(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx) {
    struct s1* rdx5;
    struct s1* rax6;
    uint32_t eax7;

    __asm__("cli ");
    rdx5 = rcx;
    if (reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(rcx)) {
        rdx5 = rsi;
    }
    rax6 = fun_24d0(rdi, rdx, rdx5, rcx);
    if (!*reinterpret_cast<uint32_t*>(&rax6)) {
        eax7 = 0;
        *reinterpret_cast<unsigned char*>(&eax7) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(rcx));
        *reinterpret_cast<uint32_t*>(&rax6) = eax7 - reinterpret_cast<uint1_t>(eax7 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi) < reinterpret_cast<unsigned char>(rcx))));
        *reinterpret_cast<int32_t*>(&rax6 + 4) = 0;
    }
    return rax6;
}

void fun_2630(void** rdi, int64_t rsi, int64_t rdx, struct s2* rcx);

struct s12 {
    signed char[1] pad1;
    unsigned char f1;
    signed char[2] pad4;
    unsigned char f4;
};

struct s12* fun_2480();

struct s1* __progname = reinterpret_cast<struct s1*>(0);

struct s1* __progname_full = reinterpret_cast<struct s1*>(0);

void fun_3c23(struct s1* rdi) {
    struct s2* rcx2;
    struct s1* rbx3;
    struct s12* rax4;
    struct s1* r12_5;
    struct s1* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2630("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2380("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2480();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s1*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (*reinterpret_cast<unsigned char*>(&rax4->f1) != 0x6c || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s1*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_53c3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2390();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xc220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5403(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5423(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc220);
    }
    *rdi = esi;
    return 0xc220;
}

int64_t fun_5443(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s13 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5483(struct s13* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s13*>(0xc220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s14 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s14* fun_54a3(struct s14* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s14*>(0xc220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x268a;
    if (!rdx) 
        goto 0x268a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc220;
}

struct s15 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_54e3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s15* r8) {
    struct s15* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s15*>(0xc220);
    }
    rax7 = fun_2390();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5516);
    *rax7 = r15d8;
    return rax13;
}

struct s16 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5563(int64_t rdi, int64_t rsi, void*** rdx, struct s16* rcx) {
    struct s16* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s16*>(0xc220);
    }
    rax6 = fun_2390();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5591);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x55ec);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5653() {
    __asm__("cli ");
}

void** gc098 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_5663() {
    uint32_t eax1;
    void** r12_2;
    int64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4)) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2370(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xc120) {
        fun_2370(rdi7);
        gc098 = reinterpret_cast<void**>(0xc120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc090) {
        fun_2370(r12_2);
        slotvec = reinterpret_cast<void**>(0xc090);
    }
    nslots = 1;
    return;
}

void fun_5703() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5723() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5733(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5753(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5773(void** rdi, int32_t esi, int64_t rdx) {
    struct s1* rdx4;
    struct s4* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2690;
    rcx5 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

void** fun_5803(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s1* rcx5;
    struct s4* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2695;
    rcx6 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void** fun_5893(int32_t edi, int64_t rsi) {
    struct s1* rax3;
    struct s4* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x269a;
    rcx4 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2450();
    } else {
        return rax5;
    }
}

void** fun_5923(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s1* rax4;
    struct s4* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x269f;
    rcx5 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

void** fun_59b3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s4* rsp4;
    struct s1* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6860]");
    __asm__("movdqa xmm1, [rip+0x6868]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6851]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2450();
    } else {
        return rax10;
    }
}

void** fun_5a53(int64_t rdi, uint32_t esi) {
    struct s4* rsp3;
    struct s1* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x67c0]");
    __asm__("movdqa xmm1, [rip+0x67c8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x67b1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

void** fun_5af3(int64_t rdi) {
    struct s1* rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6720]");
    __asm__("movdqa xmm1, [rip+0x6728]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6709]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2450();
    } else {
        return rax3;
    }
}

void** fun_5b83(int64_t rdi, int64_t rsi) {
    struct s1* rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6690]");
    __asm__("movdqa xmm1, [rip+0x6698]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6686]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2450();
    } else {
        return rax4;
    }
}

void** fun_5c13(void** rdi, int32_t esi, int64_t rdx) {
    struct s1* rdx4;
    struct s4* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26a4;
    rcx5 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

void** fun_5cb3(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s1* rcx5;
    struct s4* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x655a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6552]");
    __asm__("movdqa xmm2, [rip+0x655a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26a9;
    if (!rdx) 
        goto 0x26a9;
    rcx6 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void** fun_5d53(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s1* rcx6;
    struct s4* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x64ba]");
    __asm__("movdqa xmm1, [rip+0x64c2]");
    __asm__("movdqa xmm2, [rip+0x64ca]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ae;
    if (!rdx) 
        goto 0x26ae;
    rcx7 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

void** fun_5e03(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s1* rdx4;
    struct s4* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x640a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6402]");
    __asm__("movdqa xmm2, [rip+0x640a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b3;
    if (!rsi) 
        goto 0x26b3;
    rcx5 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

void** fun_5ea3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s1* rcx5;
    struct s4* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x636a]");
    __asm__("movdqa xmm1, [rip+0x6372]");
    __asm__("movdqa xmm2, [rip+0x637a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b8;
    if (!rsi) 
        goto 0x26b8;
    rcx6 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void fun_5f43() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f53(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f73() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f93(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_2520(signed char* rdi, struct s1* rsi, signed char* rdx);

int64_t fun_5fb3(int64_t rdi, signed char* rsi, struct s1* rdx) {
    struct s1* rax4;
    int32_t r13d5;
    struct s1* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25c0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2440(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2520(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2520(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6063() {
    __asm__("cli ");
    goto fun_25c0;
}

void fun_6073() {
    __asm__("cli ");
}

struct s17 {
    struct s1* f0;
    signed char[7] pad8;
    struct s1* f8;
    signed char[7] pad16;
    struct s1* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2510(int64_t rdi, struct s2* rsi, struct s1* rdx, struct s1* rcx, struct s1* r8, struct s1* r9);

void fun_6093(struct s2* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx, struct s17* r8, struct s1* r9) {
    struct s1* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s1* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s1* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s1* r14_40;
    struct s1* r13_41;
    struct s1* r12_42;
    struct s1* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2640(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2640(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2420();
    fun_2640(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2510(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2420();
    fun_2640(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2510(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2420();
        fun_2640(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8f28 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x8f28;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6503() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s18 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6523(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s18* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s18* rcx8;
    struct s1* rax9;
    struct s1* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2450();
    } else {
        return;
    }
}

void fun_65c3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s1* rax15;
    struct s1* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6666_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6670_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2450();
    } else {
        return;
    }
    addr_6666_5:
    goto addr_6670_7;
}

void fun_66a3() {
    struct s2* rsi1;
    struct s1* rdx2;
    struct s1* rcx3;
    struct s1* r8_4;
    struct s1* r9_5;
    struct s1* rax6;
    struct s1* rcx7;
    struct s1* r8_8;
    struct s1* r9_9;
    struct s1* v10;
    struct s1* v11;
    int64_t v12;
    struct s1* v13;
    struct s1* v14;
    struct s1* v15;
    void* v16;
    void* v17;
    int64_t v18;
    int64_t v19;
    int64_t v20;
    unsigned char* v21;
    int64_t v22;
    struct s1* rax23;
    struct s1* r8_24;
    struct s1* r9_25;
    struct s1* v26;
    struct s1* v27;
    int64_t v28;
    struct s1* v29;
    struct s1* v30;
    struct s1* v31;
    void* v32;
    void* v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    unsigned char* v37;
    int64_t v38;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2510(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2420();
    fun_25d0(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22);
    rax23 = fun_2420();
    fun_25d0(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_24, r9_25, v26, __return_address(), v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    fun_2420();
    goto fun_25d0;
}

int64_t fun_23d0();

void xalloc_die();

void fun_6743(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2540(signed char* rdi);

void fun_6783(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_67a3(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_67c3(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25a0();

void fun_67e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25a0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6813() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25a0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6843(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6883() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_68c3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_68f3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6943(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23d0();
            if (rax5) 
                break;
            addr_698d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_698d_5;
        rax8 = fun_23d0();
        if (rax8) 
            goto addr_6976_9;
        if (rbx4) 
            goto addr_698d_5;
        addr_6976_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_69d3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23d0();
            if (rax8) 
                break;
            addr_6a1a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_6a1a_5;
        rax11 = fun_23d0();
        if (rax11) 
            goto addr_6a02_9;
        if (!rbx6) 
            goto addr_6a02_9;
        if (r12_4) 
            goto addr_6a1a_5;
        addr_6a02_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6a63(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_6b0d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_6b20_10:
                *r12_8 = 0;
            }
            addr_6ac0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6ae6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6b34_14;
            if (rcx10 <= rsi9) 
                goto addr_6add_16;
            if (rsi9 >= 0) 
                goto addr_6b34_14;
            addr_6add_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6b34_14;
            addr_6ae6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25a0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6b34_14;
            if (!rbp13) 
                break;
            addr_6b34_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_6b0d_9;
        } else {
            if (!r13_6) 
                goto addr_6b20_10;
            goto addr_6ac0_11;
        }
    }
}

int64_t fun_24f0();

void fun_6b63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6b93() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6bc3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6be3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c03(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

void fun_6c43(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

struct s19 {
    signed char[1] pad1;
    signed char f1;
};

void fun_6c83(int64_t rdi, struct s19* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2540(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2520;
    }
}

void fun_6cc3(struct s1* rdi) {
    struct s1* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2440(rdi);
    rax3 = fun_2540(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

void fun_6d03() {
    void** rdi1;

    __asm__("cli ");
    fun_2420();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_25e0();
    fun_2380(rdi1);
}

int32_t memcoll();

int64_t fun_6e13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int32_t eax5;
    int32_t* rax6;
    int32_t edi7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax5 = memcoll();
    rax6 = fun_2390();
    edi7 = *rax6;
    if (edi7) {
        collate_error(edi7, rdi, rsi, rdx, rcx);
        *reinterpret_cast<int32_t*>(&rax8) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

int32_t memcoll0();

int64_t fun_6e73(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int32_t eax5;
    int32_t* rax6;
    int32_t edi7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax5 = memcoll0();
    rax6 = fun_2390();
    edi7 = *rax6;
    if (edi7) {
        collate_error(edi7, rdi, rsi - 1, rdx, rcx - 1);
        *reinterpret_cast<int32_t*>(&rax8) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

int64_t fun_23c0();

int64_t fun_6ed3(struct s7* rdi, struct s2* rsi, void* rdx, struct s1* rcx) {
    int64_t rax5;
    uint32_t ebx6;
    int64_t rax7;
    int32_t* rax8;
    int32_t* rax9;

    __asm__("cli ");
    rax5 = fun_23c0();
    ebx6 = rdi->f0 & 32;
    rax7 = rpl_fclose(rdi, rsi, rdx, rcx);
    if (ebx6) {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            addr_6f2e_3:
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rax8 = fun_2390();
            *rax8 = 0;
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            if (rax5) 
                goto addr_6f2e_3;
            rax9 = fun_2390();
            *reinterpret_cast<int32_t*>(&rax7) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax9 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    }
    return rax7;
}

uint32_t fun_23e0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_6f43(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t v8;
    struct s1* rax9;
    uint32_t eax10;
    uint32_t r12d11;
    int32_t eax12;
    int64_t rdx13;
    uint32_t eax14;
    int1_t zf15;
    void* rax16;
    int64_t rax17;
    int64_t rsi18;
    int64_t rdi19;
    uint32_t eax20;
    int64_t rdi21;
    uint32_t eax22;
    int32_t* rax23;
    int64_t rdi24;
    int32_t r13d25;
    uint32_t eax26;
    int32_t* rax27;
    int64_t rdi28;
    uint32_t eax29;
    uint32_t ecx30;
    int64_t rax31;
    uint32_t eax32;
    uint32_t eax33;
    uint32_t eax34;
    int32_t ecx35;
    int64_t rax36;

    __asm__("cli ");
    v8 = rdx;
    rax9 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax10 = fun_23e0(rdi);
        r12d11 = eax10;
        goto addr_7044_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax12 = have_dupfd_cloexec_0;
        *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
        if (eax12 < 0) {
            eax14 = fun_23e0(rdi);
            r12d11 = eax14;
            if (reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0) || (zf15 = have_dupfd_cloexec_0 == -1, !zf15)) {
                addr_7044_3:
                rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(g28));
                if (rax16) {
                    fun_2450();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax17) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    return rax17;
                }
            } else {
                addr_70f9_9:
                *reinterpret_cast<int32_t*>(&rsi18) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi19) = r12d11;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                eax20 = fun_23e0(rdi19, rdi19);
                if (reinterpret_cast<int32_t>(eax20) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi18) = 2, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi21) = r12d11, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx13) = eax20 | 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0, eax22 = fun_23e0(rdi21, rdi21), eax22 == 0xffffffff)) {
                    rax23 = fun_2390();
                    *reinterpret_cast<uint32_t*>(&rdi24) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
                    r12d11 = 0xffffffff;
                    r13d25 = *rax23;
                    fun_24b0(rdi24, rsi18, rdx13);
                    *rax23 = r13d25;
                    goto addr_7044_3;
                }
            }
        } else {
            eax26 = fun_23e0(rdi, rdi);
            r12d11 = eax26;
            if (reinterpret_cast<int32_t>(eax26) >= reinterpret_cast<int32_t>(0) || (rax27 = fun_2390(), *reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, *rax27 != 22)) {
                have_dupfd_cloexec_0 = 1;
                goto addr_7044_3;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
                eax29 = fun_23e0(rdi28);
                r12d11 = eax29;
                if (reinterpret_cast<int32_t>(eax29) < reinterpret_cast<int32_t>(0)) 
                    goto addr_7044_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_70f9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_6fa9_16;
    ecx30 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx30 > 10) 
        goto addr_6fad_18;
    rax31 = 1 << *reinterpret_cast<unsigned char*>(&ecx30);
    if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x502)) {
            addr_6fad_18:
            if (0) {
            }
        } else {
            addr_6ff5_23:
            eax32 = fun_23e0(rdi);
            r12d11 = eax32;
            goto addr_7044_3;
        }
        eax33 = fun_23e0(rdi);
        r12d11 = eax33;
        goto addr_7044_3;
    }
    if (0) {
    }
    eax34 = fun_23e0(rdi);
    r12d11 = eax34;
    goto addr_7044_3;
    addr_6fa9_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_6fad_18;
    ecx35 = *reinterpret_cast<int32_t*>(&rsi);
    rax36 = 1 << *reinterpret_cast<unsigned char*>(&ecx35);
    if (!(*reinterpret_cast<uint32_t*>(&rax36) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax36) & 0xa0a) 
            goto addr_6ff5_23;
        goto addr_6fad_18;
    }
}

signed char* fun_2560(int64_t rdi);

signed char* fun_71b3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2560(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2470(uint32_t* rdi);

uint64_t fun_71f3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s1* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2470(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2450();
    } else {
        return r12_7;
    }
}

int64_t fun_7333(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx) {
    struct s1* rax5;
    unsigned char* r15_6;
    unsigned char* r14_7;
    uint32_t r9d8;
    uint32_t r8d9;
    uint32_t eax10;
    uint32_t r9d11;
    uint32_t r8d12;
    uint32_t r10d13;
    int32_t* rax14;
    int64_t rax15;

    __asm__("cli ");
    if (rsi != rcx || (rax5 = fun_24d0(rdi, rdx, rsi, rcx), !!*reinterpret_cast<uint32_t*>(&rax5))) {
        r15_6 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
        r14_7 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<unsigned char>(rcx));
        r9d8 = *r15_6;
        r8d9 = *r14_7;
        *r15_6 = 0;
        *r14_7 = 0;
        eax10 = strcoll_loop(rdi, &rsi->f1, rdx, &rcx->f1);
        r9d11 = *reinterpret_cast<unsigned char*>(&r9d8);
        r8d12 = *reinterpret_cast<unsigned char*>(&r8d9);
        r10d13 = eax10;
        *r15_6 = *reinterpret_cast<unsigned char*>(&r9d11);
        *r14_7 = *reinterpret_cast<unsigned char*>(&r8d12);
    } else {
        rax14 = fun_2390();
        r10d13 = *reinterpret_cast<uint32_t*>(&rax5);
        *rax14 = 0;
    }
    *reinterpret_cast<uint32_t*>(&rax15) = r10d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    return rax15;
}

int64_t fun_73e3(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx) {
    struct s1* rax5;
    int32_t* rax6;

    __asm__("cli ");
    if (rsi != rcx || (rax5 = fun_24d0(rdi, rdx, rsi, rcx), !!*reinterpret_cast<int32_t*>(&rax5))) {
        goto strcoll_loop;
    } else {
        rax6 = fun_2390();
        *rax6 = 0;
        return 0;
    }
}

void fun_7453() {
    __asm__("cli ");
}

void fun_7467() {
    __asm__("cli ");
    return;
}

struct s1* rpl_mbrtowc(void* rdi, struct s1* rsi);

int32_t fun_2660(int64_t rdi, struct s1* rsi);

uint32_t fun_2650(struct s1* rdi, struct s1* rsi);

void** fun_2670(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx);

void fun_3e55() {
    struct s1** rsp1;
    int32_t ebp2;
    struct s1* rax3;
    struct s1** rsp4;
    struct s1* r11_5;
    struct s1* r11_6;
    struct s1* v7;
    int32_t ebp8;
    struct s1* rax9;
    struct s1* rdx10;
    struct s1* rax11;
    struct s1* r11_12;
    struct s1* v13;
    int32_t ebp14;
    struct s1* rax15;
    struct s1* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s1* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s1* v22;
    int32_t ebx23;
    struct s1* rax24;
    struct s1** rsp25;
    struct s1* v26;
    struct s1* r11_27;
    struct s1* v28;
    struct s1* v29;
    struct s1* rsi30;
    struct s1* v31;
    struct s1* v32;
    struct s1* r10_33;
    struct s1* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s1* r9_37;
    struct s1* v38;
    struct s1* rdi39;
    struct s1* v40;
    struct s1* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s1* rcx44;
    unsigned char al45;
    struct s1* v46;
    int64_t v47;
    struct s1* v48;
    struct s1* v49;
    struct s1* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s1* v58;
    unsigned char v59;
    struct s1* v60;
    struct s1* v61;
    struct s1* v62;
    signed char* v63;
    struct s1* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s1* r13_69;
    struct s1* rsi70;
    void* v71;
    struct s1* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s1* v107;
    struct s1* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s1**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2420();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2420();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *reinterpret_cast<unsigned char*>(&rdx10->f0), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s1*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2440(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s1*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s1*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s1**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s1*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4153_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4153_22; else 
                            goto addr_454d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_460d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4960_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4150_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4150_30; else 
                                goto addr_4979_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2440(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4960_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        rax24 = fun_24d0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4960_28; else 
                            goto addr_3ffc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4ac0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4940_40:
                        if (r11_27 == 1) {
                            addr_44cd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4a88_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4107_44;
                            }
                        } else {
                            goto addr_4950_46;
                        }
                    } else {
                        addr_4acf_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_44cd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4153_22:
                                if (v47 != 1) {
                                    addr_46a9_52:
                                    v48 = reinterpret_cast<struct s1*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2440(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_46f4_54;
                                    }
                                } else {
                                    goto addr_4160_56;
                                }
                            } else {
                                addr_4105_57:
                                ebp36 = 0;
                                goto addr_4107_44;
                            }
                        } else {
                            addr_4934_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_4acf_47; else 
                                goto addr_493e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_44cd_41;
                        if (v47 == 1) 
                            goto addr_4160_56; else 
                            goto addr_46a9_52;
                    }
                }
                addr_41c1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s1*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4058_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_407d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4380_65;
                    } else {
                        addr_41e9_66:
                        r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4a38_67;
                    }
                } else {
                    goto addr_41e0_69;
                }
                addr_4091_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s1*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                addr_40dc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4a38_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s1*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_40dc_81;
                }
                addr_41e0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_407d_64; else 
                    goto addr_41e9_66;
                addr_4107_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_41bf_91;
                if (v22) 
                    goto addr_411f_93;
                addr_41bf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_41c1_62;
                addr_46f4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4e7b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4eeb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *reinterpret_cast<unsigned char*>(&rdx10->f0) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_4cef_103;
                            rdx10 = reinterpret_cast<struct s1*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2660(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2650(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_47ee_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_41ac_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_47f8_112;
                    }
                } else {
                    addr_47f8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_48c9_114;
                }
                addr_41b8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_41bf_91;
                while (1) {
                    addr_48c9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4dd7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4836_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4de5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_48b7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s1*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_48b7_128;
                        }
                    }
                    addr_4865_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_48b7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                    continue;
                    addr_4836_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4865_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_40dc_81;
                addr_4de5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4a38_67;
                addr_4e7b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_47ee_109;
                addr_4eeb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_47ee_109;
                addr_4160_56:
                rax93 = fun_2670(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_41ac_110;
                addr_493e_59:
                goto addr_4940_40;
                addr_460d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4153_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s1*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s1*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_41b8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4105_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4153_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4652_160;
                if (!v22) 
                    goto addr_4a27_162; else 
                    goto addr_4c33_163;
                addr_4652_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4a27_162:
                    r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4a38_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_44fb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4363_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4091_70; else 
                    goto addr_4377_169;
                addr_44fb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4058_63;
                goto addr_41e0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4934_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4a6f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4150_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s1*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s1*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4048_178; else 
                        goto addr_49f2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4934_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4153_22;
                }
                addr_4a6f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4150_30:
                    r8d42 = 0;
                    goto addr_4153_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_41c1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4a88_42;
                    }
                }
                addr_4048_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4058_63;
                addr_49f2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4950_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_41c1_62;
                } else {
                    addr_4a02_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4153_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_51b2_188;
                if (v28) 
                    goto addr_4a27_162;
                addr_51b2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4363_168;
                addr_3ffc_37:
                if (v22) 
                    goto addr_4ff3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4013_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4ac0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4b4b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4153_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s1*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s1*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4048_178; else 
                        goto addr_4b27_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4934_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4153_22;
                }
                addr_4b4b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4153_22;
                }
                addr_4b27_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4950_46;
                goto addr_4a02_186;
                addr_4013_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4153_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4153_22; else 
                    goto addr_4024_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s1*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_50fe_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4f84_210;
            if (1) 
                goto addr_4f82_212;
            if (!v29) 
                goto addr_4bbe_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s1*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s1*>("\"");
            if (!0) 
                goto addr_50f1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s1*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s1*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4380_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_413b_219; else 
            goto addr_439a_220;
        addr_411f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4133_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_439a_220; else 
            goto addr_413b_219;
        addr_4cef_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_439a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_4d0d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s1*>("'");
        v29 = reinterpret_cast<struct s1*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s1*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s1*>(1);
        v22 = reinterpret_cast<struct s1*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s1*>(0);
            continue;
        }
        addr_5180_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4be6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s1*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s1*>(0);
        v22 = reinterpret_cast<struct s1*>(0);
        v28 = reinterpret_cast<struct s1*>(1);
        v26 = reinterpret_cast<struct s1*>("'");
        continue;
        addr_4dd7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4133_221;
        addr_4c33_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4133_221;
        addr_4377_169:
        goto addr_4380_65;
        addr_50fe_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_439a_220;
        goto addr_4d0d_222;
        addr_4f84_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(&v26->f0), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s1*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_4fde_236;
        fun_2450();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5180_225;
        addr_4f82_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4f84_210;
        addr_4bbe_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4f84_210;
        } else {
            rdx10 = reinterpret_cast<struct s1*>(0);
            goto addr_4be6_226;
        }
        addr_50f1_216:
        r13_34 = reinterpret_cast<struct s1*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s1*>("\"");
        v29 = reinterpret_cast<struct s1*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s1*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s1*>(1);
        v22 = reinterpret_cast<struct s1*>(0);
        v31 = reinterpret_cast<struct s1*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_454d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x89ec + rax113 * 4) + 0x89ec;
    addr_4979_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8aec + rax114 * 4) + 0x8aec;
    addr_4ff3_190:
    addr_413b_219:
    goto 0x3e20;
    addr_4024_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x88ec + rax115 * 4) + 0x88ec;
    addr_4fde_236:
    goto v116;
}

void fun_4040() {
}

void fun_41f8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3ef2;
}

void fun_4251() {
    goto 0x3ef2;
}

void fun_433e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x41c1;
    }
    if (v2) 
        goto 0x4c33;
    if (!r10_3) 
        goto addr_4d9e_5;
    if (!v4) 
        goto addr_4c6e_7;
    addr_4d9e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_4c6e_7:
    goto 0x4074;
}

void fun_435c() {
}

void fun_4407() {
    signed char v1;

    if (v1) {
        goto 0x438f;
    } else {
        goto 0x40ca;
    }
}

void fun_4421() {
    signed char v1;

    if (!v1) 
        goto 0x441a; else 
        goto "???";
}

void fun_4448() {
    goto 0x4363;
}

void fun_44c8() {
}

void fun_44e0() {
}

void fun_450f() {
    goto 0x4363;
}

void fun_4561() {
    goto 0x44f0;
}

void fun_4590() {
    goto 0x44f0;
}

void fun_45c3() {
    goto 0x44f0;
}

void fun_4990() {
    goto 0x4048;
}

void fun_4c8e() {
    signed char v1;

    if (v1) 
        goto 0x4c33;
    goto 0x4074;
}

void fun_4d35() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4074;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4058;
        goto 0x4074;
    }
}

void fun_5152() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x43c0;
    } else {
        goto 0x3ef2;
    }
}

void fun_6168() {
    fun_2420();
}

struct s1* optarg = reinterpret_cast<struct s1*>(0);

void fun_2872() {
    int1_t zf1;
    struct s1* r15_2;
    struct s1* rdi3;
    struct s1* rdx4;
    int32_t eax5;
    int1_t zf6;
    struct s1* rax7;

    zf1 = col_sep_len == 0;
    r15_2 = optarg;
    if (!zf1) {
        rdi3 = col_sep;
        eax5 = fun_2500(rdi3, r15_2, rdx4);
        if (eax5) 
            goto 0x298e;
    }
    zf6 = *reinterpret_cast<unsigned char*>(&r15_2->f0) == 0;
    col_sep = r15_2;
    *reinterpret_cast<int32_t*>(&rax7) = 1;
    *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
    if (!zf6) {
        rax7 = fun_2440(r15_2, r15_2);
    }
    col_sep_len = rax7;
    goto 0x2776;
}

void fun_28c0() {
    check_input_order = 2;
    goto 0x2776;
}

void fun_427e() {
    goto 0x3ef2;
}

void fun_4454() {
    goto 0x440c;
}

void fun_451b() {
    goto 0x4048;
}

void fun_456d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x44f0;
    goto 0x411f;
}

void fun_459f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x44fb;
        goto 0x3f20;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x439a;
        goto 0x413b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4d38;
    if (r10_8 > r15_9) 
        goto addr_4485_9;
    addr_448a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4d43;
    goto 0x4074;
    addr_4485_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_448a_10;
}

void fun_45d2() {
    goto 0x4107;
}

void fun_49a0() {
    goto 0x4107;
}

void fun_513f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x425c;
    } else {
        goto 0x43c0;
    }
}

void fun_6220() {
}

void fun_28cf() {
    delim = 0;
    goto 0x2776;
}

void fun_45dc() {
    goto 0x4577;
}

void fun_49aa() {
    goto 0x44cd;
}

void fun_6280() {
    fun_2420();
    goto fun_2640;
}

void fun_28db() {
    check_input_order = 1;
    goto 0x2776;
}

void fun_42ad() {
    goto 0x3ef2;
}

void fun_45e8() {
    goto 0x4577;
}

void fun_49b7() {
    goto 0x451e;
}

void fun_62c0() {
    fun_2420();
    goto fun_2640;
}

void fun_42da() {
    goto 0x3ef2;
}

void fun_45f4() {
    goto 0x44f0;
}

void fun_6300() {
    fun_2420();
    goto fun_2640;
}

void fun_42fc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4c90;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x41c1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x41c1;
    }
    if (v11) 
        goto 0x4ff3;
    if (r10_12 > r15_13) 
        goto addr_5043_8;
    addr_5048_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4d81;
    addr_5043_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5048_9;
}

struct s20 {
    signed char[24] pad24;
    int64_t f18;
};

struct s21 {
    signed char[16] pad16;
    struct s1* f10;
};

struct s22 {
    signed char[8] pad8;
    struct s1* f8;
};

void fun_6350() {
    int64_t r15_1;
    struct s20* rbx2;
    struct s1* r14_3;
    struct s21* rbx4;
    struct s1* r13_5;
    struct s22* rbx6;
    struct s1* r12_7;
    struct s1** rbx8;
    struct s1* rax9;
    struct s2* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2420();
    fun_2640(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6372, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_63a8() {
    fun_2420();
    goto 0x6379;
}

struct s23 {
    signed char[32] pad32;
    int64_t f20;
};

struct s24 {
    signed char[24] pad24;
    int64_t f18;
};

struct s25 {
    signed char[16] pad16;
    struct s1* f10;
};

struct s26 {
    signed char[8] pad8;
    struct s1* f8;
};

struct s27 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_63e0() {
    int64_t rcx1;
    struct s23* rbx2;
    int64_t r15_3;
    struct s24* rbx4;
    struct s1* r14_5;
    struct s25* rbx6;
    struct s1* r13_7;
    struct s26* rbx8;
    struct s1* r12_9;
    struct s1** rbx10;
    int64_t v11;
    struct s27* rbx12;
    struct s1* rax13;
    struct s2* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2420();
    fun_2640(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6414, __return_address(), rcx1);
    goto v15;
}

void fun_6458() {
    fun_2420();
    goto 0x641b;
}
