strict_posix2 (void)
{
  int posix_ver = posix2_version ();
  return 200112 <= posix_ver && posix_ver < 200809;
}