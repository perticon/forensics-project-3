void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002700(fn0000000000002490(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000035AE;
	}
	fn0000000000002680(fn0000000000002490(0x05, "Usage: %s [OPTION]... [INPUT [OUTPUT]]\n", null), 0x01);
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "Filter adjacent matching lines from INPUT (or standard input),\nwriting to OUTPUT (or standard output).\n\nWith no options, matching lines are merged to the first occurrence.\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "  -c, --count           prefix lines by the number of occurrences\n  -d, --repeated        only print duplicate lines, one for each group\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "  -D                    print all duplicate lines\n      --all-repeated[=METHOD]  like -D, but allow separating groups\n                                 with an empty line;\n                                 METHOD={none(default),prepend,separate}\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "  -f, --skip-fields=N   avoid comparing the first N fields\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "      --group[=METHOD]  show all items, separating groups with an empty line;\n                          METHOD={separate(default),prepend,append,both}\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "  -i, --ignore-case     ignore differences in case when comparing\n  -s, --skip-chars=N    avoid comparing the first N characters\n  -u, --unique          only print unique lines\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "  -z, --zero-terminated     line delimiter is NUL, not newline\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "  -w, --check-chars=N   compare no more than N characters in lines\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "      --help        display this help and exit\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "      --version     output version information and exit\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "\nA field is a run of blanks (usually spaces and/or TABs), then non-blank\ncharacters.  Fields are skipped before chars.\n", null));
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "\nNote: 'uniq' does not detect repeated lines unless they are adjacent.\nYou may want to sort the input first, or use 'sort -u' without 'uniq'.\n", null));
	struct Eq_1773 * rbx_269 = fp - 0xB8 + 16;
	do
	{
		Eq_16 rsi_271 = rbx_269->qw0000;
		++rbx_269;
	} while (rsi_271 != 0x00 && fn00000000000025C0(rsi_271, 0x8011) != 0x00);
	ptr64 r13_284 = rbx_269->qw0008;
	if (r13_284 != 0x00)
	{
		fn0000000000002680(fn0000000000002490(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_16 rax_371 = fn0000000000002670(null, 0x05);
		if (rax_371 == 0x00 || fn0000000000002420(0x03, 32922, rax_371) == 0x00)
			goto l00000000000038D6;
	}
	else
	{
		fn0000000000002680(fn0000000000002490(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_16 rax_313 = fn0000000000002670(null, 0x05);
		if (rax_313 == 0x00 || fn0000000000002420(0x03, 32922, rax_313) == 0x00)
		{
			fn0000000000002680(fn0000000000002490(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003913:
			fn0000000000002680(fn0000000000002490(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000035AE:
			fn00000000000026E0(edi);
		}
		r13_284 = 0x8011;
	}
	fn00000000000025A0(stdout, fn0000000000002490(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000038D6:
	fn0000000000002680(fn0000000000002490(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003913;
}