int64_t writeline(int64_t a1, int64_t a2, int64_t a3) {
    char * v1 = (char *)&output_unique; // 0x33f7
    if (a3 != 0) {
        // 0x3438
        v1 = (char)a2 != 0 ? (char *)&output_later_repeated : (char *)&output_first_repeated;
    }
    unsigned char result = *v1 ^ 1;
    if (result != 0) {
        // 0x3430
        return result;
    }
    // 0x3407
    if (countmode == 0) {
        // 0x3460
        function_2680();
    }
    // 0x3411
    return function_2650();
}