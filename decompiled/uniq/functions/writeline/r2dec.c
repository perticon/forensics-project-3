uint32_t writeline (int64_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    rbx = rdi;
    if (rdx != 0) {
        goto label_2;
    }
    eax = *(obj.output_unique);
    eax ^= 1;
    do {
label_0:
        if (al == 0) {
            ecx = countmode;
            if (ecx == 0) {
                goto label_3;
            }
label_1:
            rdx = *((rbx + 8));
            rdi = *((rbx + 0x10));
            esi = 1;
            rcx = stdout;
            void (*0x2650)() ();
        }
        return eax;
label_2:
        if (sil != 0) {
            goto label_4;
        }
        eax = *(obj.output_first_repeated);
        eax ^= 1;
    } while (1);
label_4:
    eax = *(obj.output_later_repeated);
    eax ^= 1;
    goto label_0;
label_3:
    rdx++;
    rsi = "%7lu ";
    edi = 1;
    printf_chk ();
    goto label_1;
}