void writeline(void** rdi, signed char sil, void** rdx, ...) {
    uint32_t eax4;
    uint32_t eax5;
    uint32_t eax6;
    uint32_t eax7;
    int32_t ecx8;

    if (rdx) {
        if (sil) {
            eax4 = output_later_repeated;
            eax5 = eax4 ^ 1;
        } else {
            eax6 = output_first_repeated;
            eax5 = eax6 ^ 1;
        }
    } else {
        eax7 = output_unique;
        eax5 = eax7 ^ 1;
    }
    if (*reinterpret_cast<signed char*>(&eax5)) {
        return;
    } else {
        ecx8 = countmode;
        if (!ecx8) {
            fun_2680(1, "%7lu ", rdx + 1);
        }
    }
}