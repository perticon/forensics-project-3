void writeline(long param_1,char param_2,long param_3)

{
  byte bVar1;
  
  if (param_3 == 0) {
    bVar1 = output_unique ^ 1;
  }
  else if (param_2 == '\0') {
    bVar1 = output_first_repeated ^ 1;
  }
  else {
    bVar1 = output_later_repeated ^ 1;
  }
  if (bVar1 == 0) {
    if (countmode == 0) {
      __printf_chk(1,"%7lu ",param_3 + 1);
    }
    fwrite_unlocked(*(void **)(param_1 + 0x10),1,*(size_t *)(param_1 + 8),stdout);
    return;
  }
  return;
}