bool different(char * old, char * new, uint64_t oldlen, uint64_t newlen) {
    uint64_t v1 = check_chars; // 0x32e4
    unsigned char v2 = *(char *)&ignore_case; // 0x32eb
    int64_t v3 = v1 > oldlen ? oldlen : v1; // 0x32f6
    int64_t v4 = v1 > newlen ? newlen : v1; // 0x32fd
    int64_t v5; // 0x32e0
    if (v2 == 0) {
        // 0x3318
        v5 = 1;
        if (v3 == v4) {
            // 0x3323
            return (int32_t)function_2590() != 0;
        }
    } else {
        // 0x3306
        v5 = v2;
        if (v3 == v4) {
            // 0x3340
            return memcasecmp((int32_t *)old, (int32_t *)new, v3) != 0;
        }
    }
    // 0x330b
    return v5 % 2 != 0;
}