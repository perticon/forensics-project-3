ulong different(void *param_1,void *param_2,ulong param_3,ulong param_4)

{
  int iVar1;
  ulong uVar2;
  ulong uVar3;
  
  uVar3 = (ulong)ignore_case;
  if (check_chars <= param_3) {
    param_3 = check_chars;
  }
  uVar2 = check_chars;
  if (param_4 < check_chars) {
    uVar2 = param_4;
  }
  if (ignore_case == 0) {
    uVar3 = 1;
    if (param_3 == uVar2) {
      iVar1 = memcmp(param_1,param_2,param_3);
      return uVar3 & 0xffffff00 | (ulong)(iVar1 != 0);
    }
  }
  else if (param_3 == uVar2) {
    iVar1 = memcasecmp();
    return uVar3 & 0xffffff00 | (ulong)(iVar1 != 0);
  }
  return uVar3;
}