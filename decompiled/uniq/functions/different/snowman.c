uint32_t different(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    uint32_t r8d8;
    uint32_t eax9;
    int32_t eax10;

    rax7 = check_chars;
    r8d8 = ignore_case;
    if (reinterpret_cast<unsigned char>(rax7) <= reinterpret_cast<unsigned char>(rdx)) {
        rdx = rax7;
    }
    if (reinterpret_cast<unsigned char>(rax7) > reinterpret_cast<unsigned char>(rcx)) {
        rax7 = rcx;
    }
    if (!*reinterpret_cast<unsigned char*>(&r8d8)) {
        r8d8 = 1;
        if (rdx == rax7) {
            eax9 = fun_2590(rdi, rsi, rdx);
            *reinterpret_cast<unsigned char*>(&r8d8) = reinterpret_cast<uint1_t>(!!eax9);
            return r8d8;
        }
    } else {
        if (rdx == rax7) {
            eax10 = memcasecmp();
            *reinterpret_cast<unsigned char*>(&r8d8) = reinterpret_cast<uint1_t>(!!eax10);
            return r8d8;
        }
    }
    return r8d8;
}