find_field (struct linebuffer const *line)
{
  size_t count;
  char const *lp = line->buffer;
  size_t size = line->length - 1;
  size_t i = 0;

  for (count = 0; count < skip_fields && i < size; count++)
    {
      while (i < size && field_sep (lp[i]))
        i++;
      while (i < size && !field_sep (lp[i]))
        i++;
    }

  i += MIN (skip_chars, size - i);

  return line->buffer + i;
}