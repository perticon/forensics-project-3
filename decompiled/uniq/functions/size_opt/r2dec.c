int64_t size_opt (int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, 0xa, rsp, 0x00009081);
    if (eax <= 1) {
        rax = *(rsp);
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
    }
    edx = 5;
    rax = dcgettext (0, rbp);
    rcx = r12;
    r8 = rax;
    eax = 0;
    error (1, 0, "%s: %s");
label_0:
    return stack_chk_fail ();
}