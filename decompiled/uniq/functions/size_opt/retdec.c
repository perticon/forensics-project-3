int64_t size_opt(int64_t a1, char * a2) {
    int64_t v1 = __readfsqword(40); // 0x337b
    int64_t result; // bp-40, 0x3360
    if (xstrtoumax((char *)a1, NULL, 10, &result, (char *)&g16) >= 2) {
        // 0x33b4
        function_2490();
        function_2690();
        // 0x33de
        return function_24c0();
    }
    // 0x3398
    if (v1 == __readfsqword(40)) {
        // 0x33ac
        return result;
    }
    // 0x33de
    return function_24c0();
}