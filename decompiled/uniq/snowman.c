
void** skip_fields = reinterpret_cast<void**>(0);

uint16_t** fun_2730(void** rdi, void** rsi, void** rdx, void** rcx);

void** skip_chars = reinterpret_cast<void**>(0);

/* find_field.isra.0 */
void** find_field_isra_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void** r12_7;
    void** rbp8;
    void** rbx9;
    void** rdx10;
    void** rax11;
    uint16_t** rax12;
    void** rdi13;
    uint16_t* rsi14;
    int64_t rax15;
    uint32_t ecx16;
    int64_t rdx17;
    int64_t rcx18;
    int64_t rcx19;
    void** rcx20;

    r12_7 = skip_fields;
    rbp8 = rsi;
    rbx9 = rdi + 0xffffffffffffffff;
    rdx10 = rbx9;
    if (!r12_7 || !rbx9) {
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        rax12 = fun_2730(rdi, rsi, rdx10, rcx);
        *reinterpret_cast<int32_t*>(&rdi13) = 0;
        *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
        rsi14 = *rax12;
        *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        ecx16 = rsi14[rax15];
        rdx17 = rax15;
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&rdx17) == 10 || *reinterpret_cast<unsigned char*>(&ecx16) & 1) {
                ++rax11;
                if (reinterpret_cast<unsigned char>(rbx9) <= reinterpret_cast<unsigned char>(rax11)) 
                    goto addr_34f0_6;
                *reinterpret_cast<uint32_t*>(&rcx18) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<unsigned char>(rax11));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx18) + 4) = 0;
                rdx17 = rcx18;
                ecx16 = rsi14[rcx18];
            } else {
                if (reinterpret_cast<unsigned char>(rbx9) <= reinterpret_cast<unsigned char>(rax11)) 
                    goto addr_34f0_6;
                while (*reinterpret_cast<signed char*>(&rdx17) != 10 && !(*reinterpret_cast<unsigned char*>(&ecx16) & 1)) {
                    ++rax11;
                    if (rax11 == rbx9) 
                        goto addr_34f0_6;
                    *reinterpret_cast<uint32_t*>(&rcx19) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<unsigned char>(rax11));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0;
                    rdx17 = rcx19;
                    ecx16 = rsi14[rcx19];
                }
                ++rdi13;
                if (reinterpret_cast<unsigned char>(rbx9) <= reinterpret_cast<unsigned char>(rax11)) 
                    goto addr_34f0_6;
                if (reinterpret_cast<unsigned char>(rdi13) >= reinterpret_cast<unsigned char>(r12_7)) 
                    goto addr_34e7_14;
            }
        }
    }
    addr_34f6_15:
    rcx20 = skip_chars;
    if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(rcx20)) {
        rdx10 = rcx20;
    }
    return reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax11) + reinterpret_cast<unsigned char>(rdx10)) + reinterpret_cast<unsigned char>(rbp8);
    addr_34f0_6:
    rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx9) - reinterpret_cast<unsigned char>(rax11));
    goto addr_34f6_15;
    addr_34e7_14:
    goto addr_34f0_6;
}

unsigned char output_later_repeated = 0;

unsigned char output_first_repeated = 0;

unsigned char output_unique = 0;

int32_t countmode = 0;

void fun_2680(int64_t rdi, void** rsi, void** rdx, ...);

void writeline(void** rdi, signed char sil, void** rdx, ...) {
    uint32_t eax4;
    uint32_t eax5;
    uint32_t eax6;
    uint32_t eax7;
    int32_t ecx8;

    if (rdx) {
        if (sil) {
            eax4 = output_later_repeated;
            eax5 = eax4 ^ 1;
        } else {
            eax6 = output_first_repeated;
            eax5 = eax6 ^ 1;
        }
    } else {
        eax7 = output_unique;
        eax5 = eax7 ^ 1;
    }
    if (*reinterpret_cast<signed char*>(&eax5)) {
        return;
    } else {
        ecx8 = countmode;
        if (!ecx8) {
            fun_2680(1, "%7lu ", rdx + 1);
        }
    }
}

void** check_chars = reinterpret_cast<void**>(0);

unsigned char ignore_case = 0;

uint32_t fun_2590(void** rdi, void** rsi, void** rdx, ...);

int32_t memcasecmp();

uint32_t different(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    uint32_t r8d8;
    uint32_t eax9;
    int32_t eax10;

    rax7 = check_chars;
    r8d8 = ignore_case;
    if (reinterpret_cast<unsigned char>(rax7) <= reinterpret_cast<unsigned char>(rdx)) {
        rdx = rax7;
    }
    if (reinterpret_cast<unsigned char>(rax7) > reinterpret_cast<unsigned char>(rcx)) {
        rax7 = rcx;
    }
    if (!*reinterpret_cast<unsigned char*>(&r8d8)) {
        r8d8 = 1;
        if (rdx == rax7) {
            eax9 = fun_2590(rdi, rsi, rdx);
            *reinterpret_cast<unsigned char*>(&r8d8) = reinterpret_cast<uint1_t>(!!eax9);
            return r8d8;
        }
    } else {
        if (rdx == rax7) {
            eax10 = memcasecmp();
            *reinterpret_cast<unsigned char*>(&r8d8) = reinterpret_cast<uint1_t>(!!eax10);
            return r8d8;
        }
    }
    return r8d8;
}

int64_t fun_24a0();

int64_t fun_2400(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24a0();
    if (r8d > 10) {
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8cc0 + rax11 * 4) + 0x8cc0;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

int32_t* fun_2410();

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2550();

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_23f0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** xcharalloc(void** rdi, ...);

void fun_24c0();

void** quotearg_n_options(void** rdi, void** rsi, void** rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    void** r8_15;
    struct s1* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** r9_23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x5bef;
    rax8 = fun_2410();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
        fun_2400(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xd090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7301]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5c7b;
            fun_2550();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0;
        rbx16 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        *reinterpret_cast<uint32_t*>(&r9_23) = r15d22;
        *reinterpret_cast<int32_t*>(&r9_23 + 4) = 0;
        rax24 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), *reinterpret_cast<uint32_t*>(&r9_23), &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx16->f0 = rsi25;
            if (r14_19 != 0xd140) {
                fun_23f0(r14_19, rsi25, rsi, rdx, r8_15, r9_23);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx16->f8 = rax26;
            v28 = rcx->f30;
            r14_19 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d22, rsi25, v29, v28, 0x5d0a);
        }
        *rax8 = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_24c0();
        } else {
            return r14_19;
        }
    }
}

uint32_t xstrtoumax();

void** fun_2490();

struct s2 {
    signed char[1] pad1;
    void** f1;
};

void fun_2690();

void** size_opt(void** rdi, int64_t rsi) {
    void** rax3;
    uint32_t eax4;
    struct s2* rdx5;
    void** v6;
    uint32_t eax7;
    uint32_t eax8;
    uint32_t eax9;
    uint32_t eax10;
    int64_t v11;
    int32_t ecx12;

    rax3 = g28;
    eax4 = xstrtoumax();
    if (eax4 > 1) {
        fun_2490();
        rdx5 = reinterpret_cast<struct s2*>("%s: %s");
        fun_2690();
    } else {
        rdx5 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
        if (!rdx5) {
            return v6;
        }
    }
    fun_24c0();
    if (rdx5) {
        if (0) {
            eax7 = output_later_repeated;
            eax8 = eax7 ^ 1;
        } else {
            eax9 = output_first_repeated;
            eax8 = eax9 ^ 1;
        }
    } else {
        eax10 = output_unique;
        eax8 = eax10 ^ 1;
    }
    if (*reinterpret_cast<signed char*>(&eax8)) {
        goto v11;
    } else {
        ecx12 = countmode;
        if (!ecx12) {
            fun_2680(1, "%7lu ", &rdx5->f1);
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xd0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x8c4b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x8c44);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x8c4f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x8c40);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gcdf8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gcdf8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23b3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_23c3() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t __uflow = 0x2040;

void fun_23d3() {
    __asm__("cli ");
    goto __uflow;
}

int64_t getenv = 0x2050;

void fun_23e3() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2060;

void fun_23f3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2070;

void fun_2403() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2080;

void fun_2413() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2090;

void fun_2423() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20a0;

void fun_2433() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20b0;

void fun_2443() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20c0;

void fun_2453() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20d0;

void fun_2463() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_2473() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20f0;

void fun_2483() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_2493() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_24a3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_24b3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2130;

void fun_24c3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2140;

void fun_24d3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2150;

void fun_24e3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x2160;

void fun_24f3() {
    __asm__("cli ");
    goto dup2;
}

int64_t strchr = 0x2170;

void fun_2503() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2180;

void fun_2513() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2190;

void fun_2523() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21a0;

void fun_2533() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21b0;

void fun_2543() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21c0;

void fun_2553() {
    __asm__("cli ");
    goto memset;
}

int64_t freopen = 0x21d0;

void fun_2563() {
    __asm__("cli ");
    goto freopen;
}

int64_t close = 0x21e0;

void fun_2573() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21f0;

void fun_2583() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x2200;

void fun_2593() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2210;

void fun_25a3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2220;

void fun_25b3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_25c3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_25d3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t strtol = 0x2250;

void fun_25e3() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x2260;

void fun_25f3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2270;

void fun_2603() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2280;

void fun_2613() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2290;

void fun_2623() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22a0;

void fun_2633() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22b0;

void fun_2643() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x22c0;

void fun_2653() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x22d0;

void fun_2663() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22e0;

void fun_2673() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22f0;

void fun_2683() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2300;

void fun_2693() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x2310;

void fun_26a3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2320;

void fun_26b3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x2330;

void fun_26c3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2340;

void fun_26d3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2350;

void fun_26e3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2360;

void fun_26f3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2370;

void fun_2703() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2380;

void fun_2713() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2390;

void fun_2723() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x23a0;

void fun_2733() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

signed char* fun_23e0(int64_t rdi);

void set_program_name(void** rdi);

void** fun_2670(int64_t rdi, ...);

void fun_2480(int64_t rdi, void** rsi);

void fun_2460(int64_t rdi, void** rsi);

void atexit(int64_t rdi, void** rsi);

uint32_t delimit_groups = 0;

void** quotearg_n_style_colon();

void** quote(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void usage();

void fun_2510(void** rdi, void** rsi, void** rdx, ...);

void** stdout = reinterpret_cast<void**>(0);

void fun_2650(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t grouping = 0;

int32_t fun_25c0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void** a8, void** a9, void** a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18, int64_t a19, int64_t a20);

void** stdin = reinterpret_cast<void**>(0);

int64_t freopen_safer(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void fadvise(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void initbuffer(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

int64_t readlinebuffer_delim(void** rdi, void** rsi, void** rdx, ...);

void** optarg = reinterpret_cast<void**>(0);

int64_t fun_24d0(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t optind = 0;

void** Version = reinterpret_cast<void**>(0x6c);

void version_etc(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8);

void fun_26e0();

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** quotearg_style(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t posix2_version(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int64_t fun_2783(int32_t edi, void** rsi) {
    void** r14_3;
    void** r12_4;
    void** rax5;
    void** v6;
    signed char* rax7;
    void** rdi8;
    void** r13_9;
    void** rbx10;
    void** rsi11;
    void** rbp12;
    void* rsp13;
    void** v14;
    int64_t v15;
    void** v16;
    int64_t rdx17;
    uint32_t eax18;
    void** rdi19;
    void** rdx20;
    void** rcx21;
    void** r8_22;
    void** r9_23;
    void** rax24;
    void** r12_25;
    void** rax26;
    void** rdi27;
    void* rsp28;
    void** rsi29;
    void** r15_30;
    void** rdi31;
    uint32_t eax32;
    void** v33;
    void** rax34;
    void** rax35;
    int1_t zf36;
    int1_t zf37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;
    int64_t v46;
    int32_t eax47;
    void* rsp48;
    int64_t rax49;
    void** rax50;
    int32_t* rax51;
    int1_t zf52;
    int1_t zf53;
    void** rax54;
    int64_t v55;
    int64_t v56;
    int64_t v57;
    int64_t v58;
    int64_t v59;
    int64_t v60;
    int64_t v61;
    int64_t v62;
    int64_t v63;
    int32_t eax64;
    void* rsp65;
    int64_t rax66;
    void** rdi67;
    int1_t zf68;
    uint32_t eax69;
    int1_t zf70;
    void** rsi71;
    int64_t rax72;
    void** v73;
    void** v74;
    void** rax75;
    void** rsi76;
    int64_t rax77;
    void** v78;
    void** v79;
    void** rax80;
    uint32_t eax81;
    uint32_t edx82;
    void* rdx83;
    void** rdx84;
    void** rdi85;
    void** rax86;
    uint32_t esi87;
    void** rax88;
    int1_t zf89;
    uint32_t esi90;
    void** rax91;
    int64_t rax92;
    void** r14_93;
    void** rdi94;
    signed char* r12_95;
    void** rax96;
    uint32_t eax97;
    int32_t eax98;
    void** rdi99;
    void** rax100;
    int64_t rax101;
    int64_t rax102;
    int64_t rdi103;
    int32_t eax104;
    int64_t rax105;
    void** rdi106;
    void** rax107;
    void** rdi108;
    int64_t rax109;
    void** v110;
    void** v111;
    void* rax112;
    void** rax113;
    void** rax114;
    int32_t eax115;
    uint32_t eax116;
    void** v117;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r14_3) = edi;
    r12_4 = rsi;
    rax5 = g28;
    v6 = rax5;
    rax7 = fun_23e0("POSIXLY_CORRECT");
    rdi8 = *reinterpret_cast<void***>(r12_4);
    *reinterpret_cast<unsigned char*>(&r13_9) = reinterpret_cast<uint1_t>(!!rax7);
    *reinterpret_cast<int32_t*>(&rbx10) = 0;
    set_program_name(rdi8);
    fun_2670(6, 6);
    rsi11 = reinterpret_cast<void**>("/usr/local/share/locale");
    fun_2480("coreutils", "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&rbp12) = 0;
    *reinterpret_cast<int32_t*>(&rbp12 + 4) = 0;
    fun_2460("coreutils", "/usr/local/share/locale");
    atexit(0x3e90, "/usr/local/share/locale");
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    output_first_repeated = 1;
    skip_chars = reinterpret_cast<void**>(0);
    skip_fields = reinterpret_cast<void**>(0);
    check_chars = reinterpret_cast<void**>(0xffffffffffffffff);
    output_unique = 1;
    output_later_repeated = 0;
    countmode = 1;
    delimit_groups = 0;
    *reinterpret_cast<unsigned char*>(&v14) = 0;
    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v15) + 7) = 10;
    *reinterpret_cast<int32_t*>(&v16) = 0;
    goto addr_28b7_2;
    addr_2901_3:
    *reinterpret_cast<uint32_t*>(&rdx17) = eax18;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x8a00) + reinterpret_cast<uint64_t>(rdx17 * 4)))) + reinterpret_cast<unsigned char>(0x8a00);
    addr_2c11_4:
    return 0;
    addr_31bc_5:
    quotearg_n_style_colon();
    fun_2410();
    fun_2690();
    while (1) {
        addr_313a_6:
        rax24 = quote(rdi19, rsi11, rdx20, rcx21, r8_22, r9_23);
        r12_25 = rax24;
        rax26 = fun_2490();
        rdx20 = rax26;
        fun_2690();
        *reinterpret_cast<int32_t*>(&rdi27) = 1;
        *reinterpret_cast<int32_t*>(&rdi27 + 4) = 0;
        usage();
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        while (1) {
            *reinterpret_cast<int32_t*>(&rsi29) = *reinterpret_cast<int32_t*>(&r15_30);
            *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
            fun_2510(rdi27, rsi29, rdx20, rdi27, rsi29, rdx20);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            while (1) {
                addr_2fa8_8:
                rdx20 = *reinterpret_cast<void***>(r13_9 + 8);
                rdi31 = *reinterpret_cast<void***>(r13_9 + 16);
                rbx10 = rbp12;
                rcx21 = stdout;
                fun_2650(rdi31, 1, rdx20, rcx21, r8_22, r9_23);
                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                eax32 = *reinterpret_cast<unsigned char*>(&v14);
                v33 = r12_25;
                *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6) = *reinterpret_cast<void***>(&eax32);
                rax34 = r13_9;
                r13_9 = v16;
                v16 = rax34;
                goto addr_2f0f_9;
                addr_2f7a_10:
                if (!*reinterpret_cast<int32_t*>(&r14_3)) 
                    continue;
                if (*reinterpret_cast<int32_t*>(&r14_3) == 1) 
                    goto addr_309a_12;
                if (*reinterpret_cast<int32_t*>(&r14_3) == 4) 
                    goto addr_309a_12;
                if (!*reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6)) 
                    continue;
                *reinterpret_cast<uint32_t*>(&r8_22) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(r14_3 + 0xfffffffffffffffe));
                *reinterpret_cast<int32_t*>(&r8_22 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&r8_22) > 1) 
                    continue;
                addr_309a_12:
                rdi27 = stdout;
                rax35 = *reinterpret_cast<void***>(rdi27 + 40);
                if (reinterpret_cast<unsigned char>(rax35) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi27 + 48))) 
                    break;
                *reinterpret_cast<void***>(rdi27 + 40) = rax35 + 1;
                *reinterpret_cast<void***>(rax35) = reinterpret_cast<void**>(10);
                continue;
                while (1) {
                    addr_2965_17:
                    zf36 = grouping == 0;
                    if (!zf36) {
                        if (*reinterpret_cast<unsigned char*>(&v14)) {
                            addr_3184_19:
                        } else {
                            zf37 = countmode == 1;
                            *reinterpret_cast<int32_t*>(&rdx20) = 5;
                            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                            if (zf37) {
                                addr_2994_21:
                                *reinterpret_cast<int32_t*>(&r15_30) = 10;
                                eax47 = fun_25c0("-", "-", rdx20, rcx21, r8_22, r9_23, v38, v14, v16, v33, v15, "-", v39, v40, v41, v42, v43, v44, v45, v46);
                                rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                                if (eax47 && (rdx20 = stdin, rax49 = freopen_safer("-", "r", rdx20, rcx21, r8_22, r9_23), rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8), !rax49)) {
                                    rax50 = quotearg_n_style_colon();
                                    r12_4 = rax50;
                                    rax51 = fun_2410();
                                    rcx21 = r12_4;
                                    rdx20 = reinterpret_cast<void**>("%s");
                                    *reinterpret_cast<int32_t*>(&rsi11) = *rax51;
                                    *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                    fun_2690();
                                    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8 - 8 + 8 - 8 + 8);
                                    grouping = 3;
                                    goto addr_28bc_24;
                                }
                            }
                        }
                    } else {
                        zf52 = countmode == 0;
                        if (!zf52) 
                            goto addr_2994_21;
                        zf53 = output_later_repeated == 0;
                        *reinterpret_cast<int32_t*>(&rdx20) = 5;
                        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                        if (zf53) 
                            goto addr_2994_21;
                    }
                    rax54 = fun_2490();
                    rdx20 = rax54;
                    fun_2690();
                    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8);
                    addr_295b_29:
                    usage();
                    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                    continue;
                    eax64 = fun_25c0("-", "-", rdx20, rcx21, r8_22, r9_23, v55, v14, v16, v33, v15, "-", v56, v57, v58, v59, v60, v61, v62, v63);
                    rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    if (!eax64) 
                        goto addr_29f2_31;
                    rdx20 = stdout;
                    rax66 = freopen_safer("-", "w", rdx20, rcx21, r8_22, r9_23);
                    rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8);
                    if (!rax66) 
                        goto addr_31bc_5;
                    addr_29f2_31:
                    rbp12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp65) + 48);
                    r13_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp65) + 80);
                    rdi67 = stdin;
                    fadvise(rdi67, 2, rdx20, rcx21, r8_22, r9_23);
                    initbuffer(rbp12, 2, rdx20, rcx21, r8_22, r9_23);
                    initbuffer(r13_9, 2, rdx20, rcx21, r8_22, r9_23);
                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8 - 8 + 8 - 8 + 8);
                    zf68 = output_unique == 0;
                    if (zf68) 
                        goto addr_2a42_33;
                    eax69 = output_first_repeated;
                    *reinterpret_cast<unsigned char*>(&v14) = *reinterpret_cast<unsigned char*>(&eax69);
                    if (!*reinterpret_cast<unsigned char*>(&eax69)) 
                        goto addr_2a42_33;
                    zf70 = countmode == 1;
                    if (zf70) 
                        goto addr_2f00_36;
                    addr_2a42_33:
                    rsi71 = stdin;
                    *reinterpret_cast<int32_t*>(&rdx20) = 10;
                    *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                    rax72 = readlinebuffer_delim(r13_9, rsi71, 10, r13_9, rsi71, 10);
                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (!rax72) 
                        goto addr_2bc9_37;
                    rax75 = find_field_isra_0(v73, v74, 10, rcx21, r8_22, r9_23, v73, v74, 10);
                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    rsi76 = stdin;
                    rcx21 = rax75;
                    r14_3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v73 - 1) - (reinterpret_cast<unsigned char>(rax75) - reinterpret_cast<unsigned char>(v74)));
                    v16 = r14_3;
                    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi76)) & 16)) 
                        goto addr_2a94_39;
                    *reinterpret_cast<int32_t*>(&r12_4) = 0;
                    *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
                    goto addr_2bbc_41;
                    addr_2a94_39:
                    *reinterpret_cast<int32_t*>(&v33) = 10;
                    *reinterpret_cast<int32_t*>(&r12_4) = 0;
                    *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
                    r15_30 = rcx21;
                    *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6) = reinterpret_cast<void**>(1);
                    while (*reinterpret_cast<int32_t*>(&rdx20) = 10, *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0, rax77 = readlinebuffer_delim(rbp12, rsi76, 10), rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8), !!rax77) {
                        v14 = v78;
                        rax80 = find_field_isra_0(v79, v78, v78, rcx21, r8_22, r9_23);
                        r14_3 = rax80;
                        rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v79 - 1) - (reinterpret_cast<unsigned char>(rax80) - reinterpret_cast<unsigned char>(v14)));
                        eax81 = different(r14_3, r15_30, rbx10, v16, r8_22, r9_23);
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8);
                        *reinterpret_cast<uint32_t*>(&rcx21) = delimit_groups;
                        *reinterpret_cast<int32_t*>(&rcx21 + 4) = 0;
                        edx82 = eax81 ^ 1;
                        *reinterpret_cast<uint32_t*>(&r9_23) = *reinterpret_cast<unsigned char*>(&edx82);
                        *reinterpret_cast<int32_t*>(&r9_23 + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rdx83) = *reinterpret_cast<unsigned char*>(&edx82);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx83) + 4) = 0;
                        rdx84 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx83) + reinterpret_cast<unsigned char>(r12_4));
                        r12_4 = reinterpret_cast<void**>(0xfffffffffffffffe);
                        if (reinterpret_cast<unsigned char>(rdx84) <= reinterpret_cast<unsigned char>(0xfffffffffffffffe)) {
                            r12_4 = rdx84;
                        }
                        if (*reinterpret_cast<uint32_t*>(&rcx21)) {
                            if (!*reinterpret_cast<signed char*>(&eax81)) {
                                if (reinterpret_cast<int1_t>(rdx84 == 1) && (*reinterpret_cast<uint32_t*>(&rcx21) == 1 || *reinterpret_cast<uint32_t*>(&rcx21) == 2 && !*reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6))) {
                                    rdi85 = stdout;
                                    rax86 = *reinterpret_cast<void***>(rdi85 + 40);
                                    if (reinterpret_cast<unsigned char>(rax86) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi85 + 48))) {
                                        *reinterpret_cast<uint32_t*>(&v14) = *reinterpret_cast<uint32_t*>(&r9_23);
                                        fun_2510(rdi85, 10, rdx84);
                                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                        *reinterpret_cast<uint32_t*>(&r9_23) = *reinterpret_cast<uint32_t*>(&v14);
                                        *reinterpret_cast<int32_t*>(&r9_23 + 4) = 0;
                                        goto addr_2b80_50;
                                    } else {
                                        *reinterpret_cast<uint32_t*>(&rcx21) = 10;
                                        *reinterpret_cast<int32_t*>(&rcx21 + 4) = 0;
                                        *reinterpret_cast<void***>(rdi85 + 40) = rax86 + 1;
                                        *reinterpret_cast<void***>(rax86) = reinterpret_cast<void**>(10);
                                        goto addr_2b80_50;
                                    }
                                }
                            } else {
                                *reinterpret_cast<uint32_t*>(&rcx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6));
                                *reinterpret_cast<int32_t*>(&rcx21 + 4) = 0;
                                if (rdx84) {
                                    *reinterpret_cast<uint32_t*>(&rcx21) = 0;
                                    *reinterpret_cast<int32_t*>(&rcx21 + 4) = 0;
                                }
                                *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6) = rcx21;
                                goto addr_2ac9_55;
                            }
                        } else {
                            if (*reinterpret_cast<signed char*>(&eax81)) {
                                addr_2ac9_55:
                                esi87 = *reinterpret_cast<uint32_t*>(&r9_23);
                                r15_30 = r14_3;
                                writeline(r13_9, *reinterpret_cast<signed char*>(&esi87), r12_4);
                                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                v16 = rbx10;
                                rax88 = rbp12;
                                *reinterpret_cast<int32_t*>(&r12_4) = 0;
                                *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
                                rbp12 = r13_9;
                                r13_9 = rax88;
                            } else {
                                addr_2b80_50:
                                zf89 = output_later_repeated == 0;
                                if (!zf89) {
                                    esi90 = *reinterpret_cast<uint32_t*>(&r9_23);
                                    r15_30 = r14_3;
                                    writeline(r13_9, *reinterpret_cast<signed char*>(&esi90), r12_4);
                                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                    rsi76 = stdin;
                                    rax91 = rbp12;
                                    v16 = rbx10;
                                    rbp12 = r13_9;
                                    r13_9 = rax91;
                                    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi76)) & 16)) 
                                        continue; else 
                                        goto addr_2bbc_41;
                                }
                            }
                            rsi76 = stdin;
                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi76)) & 16) 
                                goto addr_2bbc_41;
                        }
                    }
                    goto addr_2e6f_59;
                    addr_2f00_36:
                    *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6) = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rbx10) = 0;
                    *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0;
                    v16 = r13_9;
                    r13_9 = rbp12;
                    addr_2f0f_9:
                    while ((rsi71 = stdin, !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi71)) & 16)) && (*reinterpret_cast<int32_t*>(&rdx20) = *reinterpret_cast<int32_t*>(&r15_30), *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0, rax92 = readlinebuffer_delim(r13_9, rsi71, rdx20, r13_9, rsi71, rdx20), rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8), !!rax92)) {
                        r14_93 = *reinterpret_cast<void***>(r13_9 + 16);
                        rdi94 = *reinterpret_cast<void***>(r13_9 + 8);
                        r12_95 = reinterpret_cast<signed char*>(*reinterpret_cast<void***>(r13_9 + 8) - 1);
                        rax96 = find_field_isra_0(rdi94, r14_93, rdx20, rcx21, r8_22, r9_23, rdi94, r14_93, rdx20);
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                        rbp12 = rax96;
                        *reinterpret_cast<int32_t*>(&r14_3) = grouping;
                        *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
                        r12_25 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r12_95) - (reinterpret_cast<unsigned char>(rax96) - reinterpret_cast<unsigned char>(r14_93)));
                        if (!rbx10) 
                            goto addr_2f7a_10;
                        rcx21 = v33;
                        rdx20 = r12_25;
                        eax97 = different(rbp12, rbx10, rdx20, rcx21, r8_22, r9_23);
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                        if (*reinterpret_cast<signed char*>(&eax97)) 
                            goto addr_2f7a_10;
                        if (*reinterpret_cast<int32_t*>(&r14_3)) 
                            goto addr_2fa8_8;
                    }
                    eax98 = grouping;
                    if (eax98 - 2 & 0xfffffffd) 
                        goto addr_2bc9_37;
                    if (!*reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v15) + 6)) 
                        goto addr_2bc9_37;
                    rdi99 = stdout;
                    rax100 = *reinterpret_cast<void***>(rdi99 + 40);
                    if (reinterpret_cast<unsigned char>(rax100) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi99 + 48))) 
                        goto addr_31af_67;
                    *reinterpret_cast<uint32_t*>(&rcx21) = 10;
                    *reinterpret_cast<int32_t*>(&rcx21 + 4) = 0;
                    rdx20 = rax100 + 1;
                    *reinterpret_cast<void***>(rdi99 + 40) = rdx20;
                    *reinterpret_cast<void***>(rax100) = reinterpret_cast<void**>(10);
                    goto addr_2bc9_37;
                    addr_317d_69:
                    usage();
                    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                    goto addr_3184_19;
                    addr_28f9_70:
                    eax18 = static_cast<uint32_t>(rax101 - 48);
                    if (eax18 > 80) 
                        goto addr_295b_29; else 
                        goto addr_2901_3;
                    while (1) {
                        addr_2ef4_71:
                        rdi19 = optarg;
                        while (1) {
                            if (*reinterpret_cast<int32_t*>(&rbx10) == 2) 
                                goto addr_313a_6;
                            *reinterpret_cast<int32_t*>(&rax102) = *reinterpret_cast<int32_t*>(&rbx10);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax102) + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rbx10) = *reinterpret_cast<int32_t*>(&rbx10) + 1;
                            *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp13) + rax102 * 8 + 0x70) = rdi19;
                            while (1) {
                                while (1) {
                                    addr_28bc_24:
                                    r15_30 = reinterpret_cast<void**>(0x8a00);
                                    if (!*reinterpret_cast<int32_t*>(&rbx10)) 
                                        goto addr_28c7_75;
                                    if (*reinterpret_cast<unsigned char*>(&r13_9)) 
                                        goto addr_2885_77;
                                    addr_28c7_75:
                                    *reinterpret_cast<uint32_t*>(&r8_22) = 0;
                                    *reinterpret_cast<int32_t*>(&r8_22 + 4) = 0;
                                    rcx21 = reinterpret_cast<void**>(0xc9a0);
                                    rsi11 = r12_4;
                                    *reinterpret_cast<int32_t*>(&rdi103) = *reinterpret_cast<int32_t*>(&r14_3);
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi103) + 4) = 0;
                                    rdx20 = reinterpret_cast<void**>("-0123456789Dcdf:is:uw:z");
                                    rax101 = fun_24d0(rdi103, rsi11, "-0123456789Dcdf:is:uw:z", 0xc9a0);
                                    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                                    *reinterpret_cast<int32_t*>(&rbp12) = *reinterpret_cast<int32_t*>(&rax101);
                                    *reinterpret_cast<int32_t*>(&rbp12 + 4) = 0;
                                    if (*reinterpret_cast<int32_t*>(&rax101) != -1) 
                                        break;
                                    do {
                                        addr_2885_77:
                                        eax104 = optind;
                                        if (eax104 >= *reinterpret_cast<int32_t*>(&r14_3)) 
                                            goto addr_2965_17;
                                        rdx20 = reinterpret_cast<void**>(static_cast<int64_t>(eax104));
                                        rdi19 = *reinterpret_cast<void***>(r12_4 + reinterpret_cast<unsigned char>(rdx20) * 8);
                                        if (*reinterpret_cast<int32_t*>(&rbx10) == 2) 
                                            goto addr_313a_6;
                                        optind = eax104 + 1;
                                        *reinterpret_cast<int32_t*>(&rax105) = *reinterpret_cast<int32_t*>(&rbx10);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
                                        *reinterpret_cast<int32_t*>(&rbx10) = *reinterpret_cast<int32_t*>(&rbx10) + 1;
                                        *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp13) + rax105 * 8 + 0x70) = rdi19;
                                        addr_28b7_2:
                                    } while (*reinterpret_cast<int32_t*>(&rbp12) == -1);
                                }
                                if (*reinterpret_cast<int32_t*>(&rax101) > 0x80) 
                                    goto addr_295b_29;
                                if (*reinterpret_cast<int32_t*>(&rax101) > 47) 
                                    goto addr_28f9_70;
                                if (*reinterpret_cast<int32_t*>(&rax101) == 0xffffff7e) 
                                    goto addr_317d_69;
                                if (*reinterpret_cast<int32_t*>(&rax101) != 1) {
                                    if (*reinterpret_cast<int32_t*>(&rax101) != 0xffffff7d) 
                                        goto addr_295b_29;
                                    rdi106 = stdout;
                                    r9_23 = reinterpret_cast<void**>("David MacKenzie");
                                    rcx21 = Version;
                                    r8_22 = reinterpret_cast<void**>("Richard M. Stallman");
                                    rdx20 = reinterpret_cast<void**>("GNU coreutils");
                                    version_etc(rdi106, "uniq", "GNU coreutils", rcx21, "Richard M. Stallman", "David MacKenzie", 0, rax101);
                                    fun_26e0();
                                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 - 8 - 8 + 8 - 8 + 8);
                                    addr_2e6f_59:
                                    rax107 = stdin;
                                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax107)) & 32) 
                                        goto addr_2e7f_86;
                                } else {
                                    rdi19 = optarg;
                                    if (*reinterpret_cast<void***>(rdi19) == 43) 
                                        goto addr_2eb8_88; else 
                                        break;
                                }
                                addr_2bbc_41:
                                rdx20 = r12_4;
                                *reinterpret_cast<int32_t*>(&rsi71) = 0;
                                *reinterpret_cast<int32_t*>(&rsi71 + 4) = 0;
                                writeline(r13_9, 0, rdx20, r13_9, 0, rdx20);
                                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                addr_2bc9_37:
                                while ((rdi108 = stdin, !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi108)) & 32)) && (rax109 = rpl_fclose(rdi108, rsi71, rdx20, rcx21, r8_22, r9_23), rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8), !*reinterpret_cast<int32_t*>(&rax109))) {
                                    fun_23f0(v110, rsi71, rdx20, rcx21, r8_22, r9_23);
                                    rdi99 = v111;
                                    fun_23f0(rdi99, rsi71, rdx20, rcx21, r8_22, r9_23);
                                    rax112 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
                                    if (!rax112) 
                                        goto addr_2c11_4;
                                    fun_24c0();
                                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8 - 8 + 8);
                                    addr_31af_67:
                                    *reinterpret_cast<int32_t*>(&rsi71) = *reinterpret_cast<int32_t*>(&r15_30);
                                    *reinterpret_cast<int32_t*>(&rsi71 + 4) = 0;
                                    fun_2510(rdi99, rsi71, rdx20, rdi99, rsi71, rdx20);
                                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                }
                                addr_2e7f_86:
                                rax113 = quotearg_style(4, "-", rdx20, rcx21, r8_22, r9_23);
                                r12_4 = rax113;
                                rax114 = fun_2490();
                                rcx21 = r12_4;
                                *reinterpret_cast<int32_t*>(&rsi11) = 0;
                                *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdi19) = 1;
                                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                rdx20 = rax114;
                                fun_2690();
                                rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8 - 8 + 8);
                                addr_2eb8_88:
                                eax115 = posix2_version(rdi19, rsi11, rdx20, rcx21, r8_22, r9_23);
                                rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                                rdi19 = optarg;
                                if (reinterpret_cast<uint32_t>(eax115 - 0x30db0) <= 0x2b8) 
                                    break;
                                *reinterpret_cast<int32_t*>(&rsi11) = 0;
                                *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                rcx21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 80);
                                *reinterpret_cast<int32_t*>(&rdx20) = 10;
                                *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                                r8_22 = reinterpret_cast<void**>(0x9081);
                                eax116 = xstrtoumax();
                                rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                                if (eax116) 
                                    goto addr_2ef4_71;
                                skip_chars = v117;
                            }
                        }
                    }
                }
            }
        }
    }
}

int64_t __libc_start_main = 0;

void fun_31f3() {
    __asm__("cli ");
    __libc_start_main(0x2780, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xd008;

void fun_23b0(int64_t rdi);

int64_t fun_3293() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23b0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_32d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_25a0(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2420(void** rdi, void** rsi, void** rdx, ...);

void** stderr = reinterpret_cast<void**>(0);

void fun_2700(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void*** a8, int64_t a9, int64_t a10, int64_t a11, void*** a12);

void fun_3553(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r12_6;
    void** rax7;
    void** rcx8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    void** r12_21;
    void** rax22;
    void** r12_23;
    void** rax24;
    void** r12_25;
    void** rax26;
    void** r12_27;
    void** rax28;
    void** r12_29;
    void** rax30;
    void** r12_31;
    void** rax32;
    void** r8_33;
    void** r9_34;
    int32_t eax35;
    void** r13_36;
    void** rax37;
    void** rax38;
    int32_t eax39;
    void** rax40;
    void** rax41;
    void** rax42;
    int32_t eax43;
    void** rax44;
    void** r15_45;
    void** rax46;
    void** rax47;
    void** rax48;
    void** rdi49;
    void** r8_50;
    void** r9_51;
    int64_t v52;
    void*** v53;
    int64_t v54;
    int64_t v55;
    int64_t v56;
    void*** v57;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2490();
            fun_2680(1, rax5, r12_2, 1, rax5, r12_2);
            r12_6 = stdout;
            rax7 = fun_2490();
            fun_25a0(rax7, r12_6, 5, rcx8);
            r12_9 = stdout;
            rax10 = fun_2490();
            fun_25a0(rax10, r12_9, 5, rcx8);
            r12_11 = stdout;
            rax12 = fun_2490();
            fun_25a0(rax12, r12_11, 5, rcx8);
            r12_13 = stdout;
            rax14 = fun_2490();
            fun_25a0(rax14, r12_13, 5, rcx8);
            r12_15 = stdout;
            rax16 = fun_2490();
            fun_25a0(rax16, r12_15, 5, rcx8);
            r12_17 = stdout;
            rax18 = fun_2490();
            fun_25a0(rax18, r12_17, 5, rcx8);
            r12_19 = stdout;
            rax20 = fun_2490();
            fun_25a0(rax20, r12_19, 5, rcx8);
            r12_21 = stdout;
            rax22 = fun_2490();
            fun_25a0(rax22, r12_21, 5, rcx8);
            r12_23 = stdout;
            rax24 = fun_2490();
            fun_25a0(rax24, r12_23, 5, rcx8);
            r12_25 = stdout;
            rax26 = fun_2490();
            fun_25a0(rax26, r12_25, 5, rcx8);
            r12_27 = stdout;
            rax28 = fun_2490();
            fun_25a0(rax28, r12_27, 5, rcx8);
            r12_29 = stdout;
            rax30 = fun_2490();
            fun_25a0(rax30, r12_29, 5, rcx8);
            r12_31 = stdout;
            rax32 = fun_2490();
            fun_25a0(rax32, r12_31, 5, rcx8);
            do {
                if (1) 
                    break;
                eax35 = fun_25c0("uniq", 0, 5, "sha512sum", r8_33, r9_34, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            } while (eax35);
            r13_36 = v4;
            if (!r13_36) {
                rax37 = fun_2490();
                fun_2680(1, rax37, "GNU coreutils", 1, rax37, "GNU coreutils");
                rax38 = fun_2670(5);
                if (!rax38 || (eax39 = fun_2420(rax38, "en_", 3, rax38, "en_", 3), !eax39)) {
                    rax40 = fun_2490();
                    r13_36 = reinterpret_cast<void**>("uniq");
                    fun_2680(1, rax40, "https://www.gnu.org/software/coreutils/", 1, rax40, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_36 = reinterpret_cast<void**>("uniq");
                    goto addr_39d0_9;
                }
            } else {
                rax41 = fun_2490();
                fun_2680(1, rax41, "GNU coreutils", 1, rax41, "GNU coreutils");
                rax42 = fun_2670(5);
                if (!rax42 || (eax43 = fun_2420(rax42, "en_", 3, rax42, "en_", 3), !eax43)) {
                    addr_38d6_11:
                    rax44 = fun_2490();
                    fun_2680(1, rax44, "https://www.gnu.org/software/coreutils/", 1, rax44, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_36 == "uniq")) {
                        r12_2 = reinterpret_cast<void**>(0x9081);
                    }
                } else {
                    addr_39d0_9:
                    r15_45 = stdout;
                    rax46 = fun_2490();
                    fun_25a0(rax46, r15_45, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_38d6_11;
                }
            }
            rax47 = fun_2490();
            rcx8 = r12_2;
            fun_2680(1, rax47, r13_36, 1, rax47, r13_36);
            addr_35ae_14:
            fun_26e0();
        }
    } else {
        rax48 = fun_2490();
        rdi49 = stderr;
        rcx8 = r12_2;
        fun_2700(rdi49, 1, rax48, rcx8, r8_50, r9_51, v52, v53, v54, v55, v56, v57);
        goto addr_35ae_14;
    }
}

void fun_3a03() {
    __asm__("cli ");
    goto usage;
}

void** fun_24b0(void** rdi, ...);

int64_t fun_3a13(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_24b0(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2420(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_3a93_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_3ae0_6; else 
                    continue;
            } else {
                rax18 = fun_24b0(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_3b10_8;
                if (v16 == -1) 
                    goto addr_3ace_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_3a93_5;
            } else {
                eax19 = fun_2590(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_3a93_5;
            }
            addr_3ace_10:
            v16 = rbx15;
            goto addr_3a93_5;
        }
    }
    addr_3af5_16:
    return v12;
    addr_3ae0_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_3af5_16;
    addr_3b10_8:
    v12 = rbx15;
    goto addr_3af5_16;
}

int64_t fun_3b23(int64_t rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16) {
    void** v17;
    void** v18;
    void** r12_19;
    int64_t r12_20;
    void** v21;
    void** rbp22;
    int64_t v23;
    int64_t rbx24;
    void** rdi25;
    void*** rbp26;
    int64_t rbx27;
    int32_t eax28;

    v17 = reinterpret_cast<void**>(__return_address());
    __asm__("cli ");
    v18 = r12_19;
    r12_20 = rdi;
    v21 = rbp22;
    v23 = rbx24;
    rdi25 = *rsi;
    if (!rdi25) {
        addr_3b68_2:
        return -1;
    } else {
        rbp26 = rsi;
        *reinterpret_cast<int32_t*>(&rbx27) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx27) + 4) = 0;
        do {
            eax28 = fun_25c0(rdi25, r12_20, rdx, rcx, r8, r9, v23, v21, v18, v17, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16);
            if (!eax28) 
                break;
            ++rbx27;
            rdi25 = rbp26[rbx27 * 8];
        } while (rdi25);
        goto addr_3b68_2;
    }
    return rbx27;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_3b83(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2490();
    } else {
        fun_2490();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_2690;
}

void fun_3c13(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void*** v8;
    void*** r12_9;
    void** r12_10;
    void** rdx11;
    int64_t v12;
    int64_t rbp13;
    void** rbp14;
    int64_t v15;
    int64_t rbx16;
    void** r14_17;
    void*** v18;
    void** rax19;
    void** rsi20;
    void** r15_21;
    int64_t rbx22;
    uint32_t eax23;
    void** rax24;
    void** rdi25;
    int64_t v26;
    int64_t v27;
    void** rax28;
    void** rdi29;
    int64_t v30;
    int64_t v31;
    void** rdi32;
    void** rax33;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    *reinterpret_cast<int32_t*>(&rdx11) = 5;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    v12 = rbp13;
    rbp14 = rsi;
    v15 = rbx16;
    r14_17 = stderr;
    v18 = rdi;
    rax19 = fun_2490();
    rsi20 = r14_17;
    fun_25a0(rax19, rsi20, 5, rcx);
    r15_21 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx22) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx22) + 4) = 0;
    if (r15_21) {
        do {
            if (!rbx22 || (rdx11 = r12_10, rsi20 = rbp14, eax23 = fun_2590(r13_7, rsi20, rdx11, r13_7, rsi20, rdx11), !!eax23)) {
                r13_7 = rbp14;
                rax24 = quote(r15_21, rsi20, rdx11, rcx, r8, r9);
                rdi25 = stderr;
                rdx11 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rcx = rax24;
                fun_2700(rdi25, 1, "\n  - %s", rcx, r8, r9, v26, v18, v27, v15, v12, v8);
            } else {
                rax28 = quote(r15_21, rsi20, rdx11, rcx, r8, r9);
                rdi29 = stderr;
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rdx11 = reinterpret_cast<void**>(", %s");
                rcx = rax28;
                fun_2700(rdi29, 1, ", %s", rcx, r8, r9, v30, v18, v31, v15, v12, v8);
            }
            ++rbx22;
            rbp14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp14) + reinterpret_cast<unsigned char>(r12_10));
            r15_21 = v18[rbx22 * 8];
        } while (r15_21);
    }
    rdi32 = stderr;
    rax33 = *reinterpret_cast<void***>(rdi32 + 40);
    if (reinterpret_cast<unsigned char>(rax33) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi32 + 48))) {
        goto fun_2510;
    } else {
        *reinterpret_cast<void***>(rdi32 + 40) = rax33 + 1;
        *reinterpret_cast<void***>(rax33) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(int64_t rdi, void** rsi, void** rdx, void** rcx);

void argmatch_invalid(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

void argmatch_valid(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_3d43(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t v11;
    int64_t v12;
    int64_t r15_13;
    int64_t r15_14;
    int64_t v15;
    int64_t r14_16;
    int64_t r14_17;
    int64_t v18;
    int64_t r13_19;
    void** r13_20;
    int64_t v21;
    int64_t r12_22;
    void** r12_23;
    int64_t v24;
    int64_t rbp25;
    void** rbp26;
    void** v27;
    void** rbx28;
    void** v29;
    int64_t rax30;
    void** rdi31;
    int64_t rbx32;
    int64_t v33;
    void** v34;
    int32_t eax35;

    v11 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v12 = r15_13;
    r15_14 = rdi;
    v15 = r14_16;
    r14_17 = rsi;
    v18 = r13_19;
    r13_20 = r8;
    v21 = r12_22;
    r12_23 = rcx;
    v24 = rbp25;
    rbp26 = rdx;
    v27 = rbx28;
    v29 = r9;
    if (*reinterpret_cast<signed char*>(&a7)) {
        rcx = r8;
        rax30 = argmatch(r14_17, rbp26, r12_23, rcx);
        if (rax30 < 0) {
            addr_3d87_3:
            argmatch_invalid(r15_14, r14_17, rax30, rcx);
            argmatch_valid(rbp26, r12_23, r13_20, rcx);
            v29(rbp26, r12_23, r13_20, rcx);
            rax30 = -1;
            goto addr_3dfe_4;
        } else {
            addr_3dfe_4:
            return rax30;
        }
    }
    rdi31 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx32) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx32) + 4) = 0;
    if (rdi31) {
        do {
            eax35 = fun_25c0(rdi31, r14_17, rdx, rcx, r8, r9, v33, v29, v34, v27, v24, v21, v18, v15, v12, v11, a7, a8, a9, a10);
            if (!eax35) 
                break;
            ++rbx32;
            rdi31 = *reinterpret_cast<void***>(rbp26 + rbx32 * 8);
        } while (rdi31);
        goto addr_3d80_8;
    } else {
        goto addr_3d80_8;
    }
    return rbx32;
    addr_3d80_8:
    rax30 = -1;
    goto addr_3d87_3;
}

struct s4 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_3e13(void** rdi, struct s4* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2590(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_3e73(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3e83(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2430(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3e93() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2410(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2490();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3f23_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2690();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2430(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3f23_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2690();
    }
}

void fun_3f43() {
    __asm__("cli ");
}

struct s5 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2600(struct s5* rdi);

void fun_3f53(struct s5* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2600(rdi);
        goto 0x2580;
    }
}

int32_t fun_2640(struct s5* rdi);

int64_t fun_2530(int64_t rdi, ...);

int32_t rpl_fflush(struct s5* rdi);

int64_t fun_2470(struct s5* rdi);

int64_t fun_3f83(struct s5* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2600(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2640(rdi);
        if (!(eax3 && (eax4 = fun_2600(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2530(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2410();
            r12d9 = *rax8;
            rax10 = fun_2470(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2470;
}

void rpl_fseeko(struct s5* rdi);

void fun_4013(struct s5* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2640(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int32_t fun_24f0();

int32_t fun_26a0(int64_t rdi);

int64_t fun_2560(int64_t rdi, int64_t rsi, struct s5* rdx);

void fun_2570();

int64_t fun_4063(int64_t rdi, int64_t rsi, struct s5* rdx) {
    int32_t eax4;
    int32_t eax5;
    int32_t* rax6;
    int32_t* rbx7;
    int32_t eax8;
    int32_t r14d9;
    int32_t r15d10;
    signed char v11;
    int32_t eax12;
    int32_t eax13;
    int32_t eax14;
    int32_t* rax15;
    int64_t rax16;
    int64_t r12_17;
    int32_t* rax18;
    int32_t ebp19;
    int32_t eax20;
    int32_t eax21;

    __asm__("cli ");
    eax4 = fun_2600(rdx);
    if (eax4 == 1) {
        eax5 = fun_24f0();
        rax6 = fun_2410();
        rbx7 = rax6;
        if (eax5) {
            eax8 = fun_26a0("/dev/null");
            if (eax8) {
                r14d9 = 0;
                r15d10 = 0;
                goto addr_41a8_5;
            } else {
                v11 = 1;
                r14d9 = 0;
                r15d10 = 0;
            }
        } else {
            addr_41dd_7:
            v11 = 0;
            r14d9 = 0;
            r15d10 = 0;
        }
    } else {
        if (eax4 == 2) {
            r14d9 = 0;
        } else {
            if (!eax4) 
                goto addr_41dd_7;
            eax12 = fun_24f0();
            *reinterpret_cast<unsigned char*>(&r14d9) = reinterpret_cast<uint1_t>(eax12 != 2);
        }
        eax13 = fun_24f0();
        *reinterpret_cast<unsigned char*>(&r15d10) = reinterpret_cast<uint1_t>(eax13 != 1);
        eax14 = fun_24f0();
        rax15 = fun_2410();
        rbx7 = rax15;
        if (eax14) 
            goto addr_4190_13; else 
            goto addr_40ee_14;
    }
    rax16 = fun_2560(rdi, rsi, rdx);
    r12_17 = rax16;
    rax18 = fun_2410();
    ebp19 = *rax18;
    rbx7 = rax18;
    addr_4203_16:
    if (*reinterpret_cast<unsigned char*>(&r14d9)) {
        addr_414c_17:
        fun_2570();
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) {
            addr_4215_18:
            if (v11) {
                addr_4175_19:
                fun_2570();
                if (r12_17) {
                    addr_4229_20:
                    return r12_17;
                } else {
                    addr_4185_21:
                    *rbx7 = ebp19;
                    goto addr_4229_20;
                }
            } else {
                addr_4220_22:
                if (!r12_17) 
                    goto addr_4185_21; else 
                    goto addr_4229_20;
            }
        }
    } else {
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) 
            goto addr_4215_18;
    }
    addr_4160_25:
    fun_2570();
    if (!v11) 
        goto addr_4220_22; else 
        goto addr_4175_19;
    addr_4190_13:
    eax8 = fun_26a0("/dev/null");
    if (!eax8) {
        v11 = 1;
        if (eax13 != 1) {
            addr_40fe_27:
            eax20 = fun_26a0("/dev/null");
            if (eax20 != 1) {
                if (eax20 >= 0) {
                    ebp19 = 9;
                    fun_2570();
                    *rbx7 = 9;
                } else {
                    ebp19 = *rbx7;
                }
                *reinterpret_cast<int32_t*>(&r12_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&r14d9)) {
                    fun_2570();
                    goto addr_4160_25;
                }
            } else {
                r15d10 = 1;
            }
        } else {
            addr_42b0_34:
            r15d10 = 0;
        }
    } else {
        addr_41a8_5:
        if (eax8 >= 0) {
            ebp19 = 9;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            fun_2570();
            *rbx7 = 9;
            v11 = 1;
            goto addr_4203_16;
        } else {
            v11 = 1;
            ebp19 = *rbx7;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            goto addr_4203_16;
        }
    }
    if (*reinterpret_cast<unsigned char*>(&r14d9) && (eax21 = fun_26a0("/dev/null"), eax21 != 2)) {
        if (eax21 >= 0) {
            ebp19 = 9;
            fun_2570();
            *rbx7 = 9;
        } else {
            ebp19 = *rbx7;
        }
        *reinterpret_cast<int32_t*>(&r12_17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
        goto addr_414c_17;
    }
    addr_40ee_14:
    v11 = 0;
    if (eax13 == 1) 
        goto addr_42b0_34; else 
        goto addr_40fe_27;
}

int64_t fun_4333(struct s5* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2600(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2530(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

struct s6 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_43b3(struct s6* rdi) {
    __asm__("cli ");
    __asm__("pxor xmm0, xmm0");
    rdi->f10 = 0;
    __asm__("movups [rdi], xmm0");
    return;
}

struct s7 {
    void* f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

uint32_t fun_23d0(struct s7* rdi, struct s7* rsi, void** rdx);

struct s7* fun_43d3(struct s7* rdi, struct s7* rsi, int32_t edx) {
    void** rdx3;
    uint32_t r13d4;
    void** r15_5;
    void* rcx6;
    int32_t v7;
    unsigned char v8;
    void** rcx9;
    struct s7* r14_10;
    struct s7* rbp11;
    void** r12_12;
    void** rax13;
    uint32_t ebx14;
    uint32_t eax15;
    uint32_t eax16;
    uint32_t r11d17;
    void* r12_18;
    void** rax19;
    void* rcx20;
    void** rax21;
    void** rax22;
    uint32_t esi23;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    __asm__("cli ");
    r13d4 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&rdx3)));
    r15_5 = rdi->f10;
    rcx6 = rdi->f0;
    v7 = *reinterpret_cast<int32_t*>(&rdx3);
    v8 = *reinterpret_cast<unsigned char*>(&r13d4);
    rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx6) + reinterpret_cast<unsigned char>(r15_5));
    if (reinterpret_cast<unsigned char>(rsi->f0) & 16) {
        addr_44c0_2:
        return 0;
    } else {
        r14_10 = rdi;
        rbp11 = rsi;
        r12_12 = r15_5;
        do {
            rax13 = rbp11->f8;
            if (reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rbp11->f10)) {
                rdx3 = rax13 + 1;
                rbp11->f8 = rdx3;
                ebx14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13));
            } else {
                eax15 = fun_23d0(rbp11, rsi, rdx3);
                rcx9 = rcx9;
                ebx14 = eax15;
                if (eax15 == 0xffffffff) {
                    if (r15_5 == r12_12) 
                        goto addr_44c0_2;
                    if (reinterpret_cast<unsigned char>(rbp11->f0) & 32) 
                        goto addr_44c0_2;
                    eax16 = v8;
                    if (*reinterpret_cast<signed char*>(r12_12 + 0xffffffffffffffff) == *reinterpret_cast<signed char*>(&eax16)) 
                        break;
                    ebx14 = r13d4;
                    if (r12_12 != rcx9) 
                        goto addr_44d8_11;
                    r11d17 = v8;
                    goto addr_4480_13;
                }
            }
            r11d17 = ebx14;
            if (r12_12 == rcx9) {
                addr_4480_13:
                rsi = r14_10;
                r12_18 = r14_10->f0;
                *reinterpret_cast<int32_t*>(&rdx3) = 1;
                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                rax19 = xpalloc();
                rcx20 = r14_10->f0;
                r11d17 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r11d17));
                r15_5 = rax19;
                rax21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax19) + reinterpret_cast<unsigned char>(r12_18));
                r14_10->f10 = r15_5;
                rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx20) + reinterpret_cast<unsigned char>(r15_5));
            } else {
                rax21 = r12_12;
            }
            *reinterpret_cast<void***>(rax21) = *reinterpret_cast<void***>(&r11d17);
            r12_12 = rax21 + 1;
        } while (r13d4 != ebx14);
    }
    rax22 = r12_12;
    addr_44e6_18:
    r14_10->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax22) - reinterpret_cast<unsigned char>(r15_5));
    return r14_10;
    addr_44d8_11:
    esi23 = *reinterpret_cast<unsigned char*>(&v7);
    rax22 = r12_12 + 1;
    *reinterpret_cast<void***>(r12_12) = *reinterpret_cast<void***>(&esi23);
    goto addr_44e6_18;
}

void fun_4513() {
    __asm__("cli ");
    goto readlinebuffer_delim;
}

void fun_4523(int64_t rdi) {
    __asm__("cli ");
    goto fun_23f0;
}

int32_t** fun_23c0();

int64_t fun_4533(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t r12_4;
    int64_t rbp5;
    int64_t rbx6;
    int32_t** rax7;
    int64_t rcx8;
    int32_t* r8_9;
    int64_t rax10;
    int64_t r9_11;
    int64_t rax12;

    __asm__("cli ");
    if (!rdx) {
        return 0;
    } else {
        r12_4 = rsi;
        rbp5 = rdi;
        rbx6 = rdx;
        rax7 = fun_23c0();
        *reinterpret_cast<int32_t*>(&rcx8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
        r8_9 = *rax7;
        do {
            *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<unsigned char*>(rbp5 + rcx8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r9_11) = *reinterpret_cast<unsigned char*>(r12_4 + rcx8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rax12) = r8_9[rax10] - r8_9[r9_11];
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&rax12)) 
                break;
            ++rcx8;
        } while (rcx8 != rbx6);
        return rax12;
    }
}

int64_t fun_25e0(signed char* rdi, signed char** rsi, int64_t rdx);

int64_t fun_4583() {
    int64_t r12_1;
    void** rax2;
    signed char* rax3;
    int64_t rax4;
    signed char* v5;
    void* rax6;
    int64_t rax7;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_1) = 0x31069;
    rax2 = g28;
    rax3 = fun_23e0("_POSIX2_VERSION");
    if (rax3 && (*rax3 && (rax4 = fun_25e0(rax3, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 + 8, 10), !*v5))) {
        if (rax4 < 0xffffffff80000000) {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x80000000;
        } else {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x7fffffff;
            if (rax4 <= 0x7fffffff) {
                r12_1 = rax4;
            }
        }
    }
    rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax6) {
        fun_24c0();
    } else {
        *reinterpret_cast<int32_t*>(&rax7) = *reinterpret_cast<int32_t*>(&r12_1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

void fun_26f0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s8 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s8* fun_2520();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4613(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s8* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_26f0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2400("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2520();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_2420(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5db3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2410();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xd240;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5df3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd240);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5e13(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd240);
    }
    *rdi = esi;
    return 0xd240;
}

int64_t fun_5e33(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xd240);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s9 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5e73(struct s9* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xd240);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s10 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s10* fun_5e93(struct s10* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s10*>(0xd240);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x274a;
    if (!rdx) 
        goto 0x274a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xd240;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5ed3(void** rdi, void** rsi, void** rdx, void** rcx, struct s11* r8) {
    struct s11* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s11*>(0xd240);
    }
    rax7 = fun_2410();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5f06);
    *rax7 = r15d8;
    return rax13;
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5f53(void** rdi, void** rsi, void*** rdx, struct s12* rcx) {
    struct s12* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s12*>(0xd240);
    }
    rax6 = fun_2410();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5f81);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5fdc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6043() {
    __asm__("cli ");
}

void** gd098 = reinterpret_cast<void**>(64);

int64_t slotvec0 = 0x100;

void fun_6053() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rcx9;
    void** r8_10;
    void** r9_11;
    void** rdi12;
    void** rsi13;
    void** rdx14;
    void** rcx15;
    void** r8_16;
    void** r9_17;
    void** rsi18;
    void** rdx19;
    void** rcx20;
    void** r8_21;
    void** r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23f0(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi12 != 0xd140) {
        fun_23f0(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        gd098 = reinterpret_cast<void**>(0xd140);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xd090) {
        fun_23f0(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<void**>(0xd090);
    }
    nslots = 1;
    return;
}

void fun_60f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6113() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6123(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6143(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_6163(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2750;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_61f3(void** rdi, int32_t esi, void** rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2755;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void** fun_6283(int32_t edi, void** rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x275a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_24c0();
    } else {
        return rax5;
    }
}

void** fun_6313(int32_t edi, void** rsi, void** rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x275f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_63a3(void** rdi, void** rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6e90]");
    __asm__("movdqa xmm1, [rip+0x6e98]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6e81]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_24c0();
    } else {
        return rax10;
    }
}

void** fun_6443(void** rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6df0]");
    __asm__("movdqa xmm1, [rip+0x6df8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6de1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

void** fun_64e3(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6d50]");
    __asm__("movdqa xmm1, [rip+0x6d58]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6d39]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_24c0();
    } else {
        return rax3;
    }
}

void** fun_6573(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6cc0]");
    __asm__("movdqa xmm1, [rip+0x6cc8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6cb6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_24c0();
    } else {
        return rax4;
    }
}

void** fun_6603(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2764;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_66a3(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6b8a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6b82]");
    __asm__("movdqa xmm2, [rip+0x6b8a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2769;
    if (!rdx) 
        goto 0x2769;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void** fun_6743(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, void** r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6aea]");
    __asm__("movdqa xmm1, [rip+0x6af2]");
    __asm__("movdqa xmm2, [rip+0x6afa]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x276e;
    if (!rdx) 
        goto 0x276e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

void** fun_67f3(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6a3a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6a32]");
    __asm__("movdqa xmm2, [rip+0x6a3a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2773;
    if (!rsi) 
        goto 0x2773;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_6893(int64_t rdi, int64_t rsi, void** rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x699a]");
    __asm__("movdqa xmm1, [rip+0x69a2]");
    __asm__("movdqa xmm2, [rip+0x69aa]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2778;
    if (!rsi) 
        goto 0x2778;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void fun_6933() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6943(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6963() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6983(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s13 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    void*** f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    void*** f40;
};

void fun_25d0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_69a3(void** rdi, void** rsi, void** rdx, void** rcx, struct s13* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    void*** v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;
    void*** v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    void*** v19;
    void** rax20;
    int64_t v21;
    void*** v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    void*** v26;
    void** rax27;
    int64_t v28;
    void*** v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    void*** v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    void*** rcx37;
    int64_t r15_38;
    void*** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2700(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2700(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2490();
    fun_2700(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_25d0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2490();
    fun_2700(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_25d0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2490();
        fun_2700(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x9328 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x9328;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6e13() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6e33(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s14* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s14* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_24c0();
    } else {
        return;
    }
}

void fun_6ed3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6f76_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6f80_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_24c0();
    } else {
        return;
    }
    addr_6f76_5:
    goto addr_6f80_7;
}

void fun_6fb3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25d0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2490();
    fun_2680(1, rax6, "bug-coreutils@gnu.org");
    rax7 = fun_2490();
    fun_2680(1, rax7, "GNU coreutils", 1, rax7, "GNU coreutils");
    fun_2490();
    goto fun_2680;
}

int64_t fun_2450();

void xalloc_die();

void fun_7053(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2450();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2610(void** rdi);

void fun_7093(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70b3(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70d3(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2660();

void fun_70f3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2660();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7123() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2660();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7153(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2450();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7193() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2450();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_71d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2450();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7203(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2450();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7253(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2450();
            if (rax5) 
                break;
            addr_729d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_729d_5;
        rax8 = fun_2450();
        if (rax8) 
            goto addr_7286_9;
        if (rbx4) 
            goto addr_729d_5;
        addr_7286_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_72e3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2450();
            if (rax8) 
                break;
            addr_732a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_732a_5;
        rax11 = fun_2450();
        if (rax11) 
            goto addr_7312_9;
        if (!rbx6) 
            goto addr_7312_9;
        if (r12_4) 
            goto addr_732a_5;
        addr_7312_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7373(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_741d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7430_10:
                *r12_8 = 0;
            }
            addr_73d0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_73f6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7444_14;
            if (rcx10 <= rsi9) 
                goto addr_73ed_16;
            if (rsi9 >= 0) 
                goto addr_7444_14;
            addr_73ed_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7444_14;
            addr_73f6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2660();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7444_14;
            if (!rbp13) 
                break;
            addr_7444_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_741d_9;
        } else {
            if (!r13_6) 
                goto addr_7430_10;
            goto addr_73d0_11;
        }
    }
}

int64_t fun_25b0();

void fun_7473() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_74a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_74d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_74f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_25f0(signed char* rdi, void** rsi, void** rdx);

void fun_7513(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2610(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_7553(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2610(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

struct s15 {
    signed char[1] pad1;
    void** f1;
};

void fun_7593(int64_t rdi, struct s15* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2610(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_25f0;
    }
}

void fun_75d3(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_24b0(rdi);
    rax3 = fun_2610(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_7613() {
    void** rdi1;

    __asm__("cli ");
    fun_2490();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2690();
    fun_2400(rdi1);
}

void fun_2540(int64_t rdi);

void** fun_26c0(void** rdi);

int64_t fun_2500(void** rdi);

int64_t fun_7653(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    uint16_t* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    uint16_t** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<uint16_t*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_2540("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_24c0();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_79c4_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_79c4_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_770d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_7715_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2410();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_2730(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9 + rdx23) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_774b_21;
    rsi = r15_14;
    rax24 = fun_26c0(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2500(r13_18), r8 = r8, rax26 == 0))) {
            addr_774b_21:
            r12d11 = 4;
            goto addr_7715_13;
        } else {
            addr_7789_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2500(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<uint16_t*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x93d8 + rbp33 * 4) + 0x93d8;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_774b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_770d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_770d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2500(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_7789_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_7715_13;
}

int64_t fun_2440();

int64_t fun_7a83(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int64_t rax7;
    uint32_t ebx8;
    int64_t rax9;
    int32_t* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax7 = fun_2440();
    ebx8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax9 = rpl_fclose(rdi, rsi, rdx, rcx, r8, r9);
    if (ebx8) {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            addr_7ade_3:
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        } else {
            rax10 = fun_2410();
            *rax10 = 0;
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            if (rax7) 
                goto addr_7ade_3;
            rax11 = fun_2410();
            *reinterpret_cast<int32_t*>(&rax9) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax11 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    }
    return rax9;
}

signed char* fun_2630(int64_t rdi);

signed char* fun_7af3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2630(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24e0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_7b33(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24e0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_24c0();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_7bc3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax3;
    }
}

int64_t fun_7c43(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2670(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_24b0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_25f0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_25f0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7cf3() {
    __asm__("cli ");
    goto fun_2670;
}

void fun_7d03() {
    __asm__("cli ");
}

void fun_7d17() {
    __asm__("cli ");
    return;
}

void fun_2c25() {
    int32_t v1;
    void** rax2;
    int32_t eax3;
    void** rdx4;
    int32_t eax5;

    if (v1 == 2) {
        rax2 = reinterpret_cast<void**>(static_cast<int64_t>(eax3));
    } else {
        rdx4 = skip_fields;
        if (reinterpret_cast<unsigned char>(rdx4) > reinterpret_cast<unsigned char>(0x1999999999999999) || (rax2 = reinterpret_cast<void**>(eax5 + reinterpret_cast<unsigned char>(rdx4) * 10), reinterpret_cast<unsigned char>(rdx4) > reinterpret_cast<unsigned char>(rax2))) {
            skip_fields = reinterpret_cast<void**>(0xffffffffffffffff);
            goto 0x28b7;
        }
    }
    skip_fields = rax2;
    goto 0x28b7;
}

int64_t __xargmatch_internal(int64_t rdi);

void fun_2c8b(int64_t rdi) {
    void** rsi2;
    unsigned char r10b3;
    int64_t rax4;
    uint32_t eax5;

    rsi2 = optarg;
    output_unique = r10b3;
    output_later_repeated = 1;
    if (!rsi2) {
        delimit_groups = 0;
        goto 0x28bc;
    } else {
        rax4 = __xargmatch_internal("--all-repeated");
        eax5 = *reinterpret_cast<uint32_t*>(0x8b60 + rax4 * 4);
        delimit_groups = eax5;
        goto 0x28bc;
    }
}

void fun_2cee() {
    unsigned char r10b1;

    output_first_repeated = r10b1;
    goto 0x28bc;
}

void fun_2d9f() {
    void** rdi1;
    void** rax2;

    rdi1 = optarg;
    rax2 = size_opt(rdi1, "invalid number of bytes to compare");
    check_chars = rax2;
    goto 0x28bc;
}

void fun_2fe4() {
    goto 0x28c3;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2720(int64_t rdi, void** rsi);

uint32_t fun_2710(void** rdi, void** rsi);

void fun_4845() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    uint16_t** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2490();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2490();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24b0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4b43_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4b43_22; else 
                            goto addr_4f3d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4ffd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5350_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4b40_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4b40_30; else 
                                goto addr_5369_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24b0(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5350_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2590(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5350_28; else 
                            goto addr_49ec_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_54b0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5330_40:
                        if (r11_27 == 1) {
                            addr_4ebd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5478_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4af7_44;
                            }
                        } else {
                            goto addr_5340_46;
                        }
                    } else {
                        addr_54bf_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_4ebd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4b43_22:
                                if (v47 != 1) {
                                    addr_5099_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_24b0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_50e4_54;
                                    }
                                } else {
                                    goto addr_4b50_56;
                                }
                            } else {
                                addr_4af5_57:
                                ebp36 = 0;
                                goto addr_4af7_44;
                            }
                        } else {
                            addr_5324_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_54bf_47; else 
                                goto addr_532e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4ebd_41;
                        if (v47 == 1) 
                            goto addr_4b50_56; else 
                            goto addr_5099_52;
                    }
                }
                addr_4bb1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4a48_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_4a6d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4d70_65;
                    } else {
                        addr_4bd9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5428_67;
                    }
                } else {
                    goto addr_4bd0_69;
                }
                addr_4a81_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_4acc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5428_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4acc_81;
                }
                addr_4bd0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_4a6d_64; else 
                    goto addr_4bd9_66;
                addr_4af7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_4baf_91;
                if (v22) 
                    goto addr_4b0f_93;
                addr_4baf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4bb1_62;
                addr_50e4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_586b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_58db_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_56df_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2720(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2710(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_51de_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_4b9c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_51e8_112;
                    }
                } else {
                    addr_51e8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_52b9_114;
                }
                addr_4ba8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4baf_91;
                while (1) {
                    addr_52b9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_57c7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_5226_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_57d5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_52a7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_52a7_128;
                        }
                    }
                    addr_5255_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_52a7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_5226_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5255_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4acc_81;
                addr_57d5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5428_67;
                addr_586b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_51de_109;
                addr_58db_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_51de_109;
                addr_4b50_56:
                rax93 = fun_2730(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93 + reinterpret_cast<unsigned char>(rax24)) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_4b9c_110;
                addr_532e_59:
                goto addr_5330_40;
                addr_4ffd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4b43_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4ba8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4af5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4b43_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5042_160;
                if (!v22) 
                    goto addr_5417_162; else 
                    goto addr_5623_163;
                addr_5042_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5417_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5428_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4eeb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4d53_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4a81_70; else 
                    goto addr_4d67_169;
                addr_4eeb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4a48_63;
                goto addr_4bd0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5324_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_545f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4b40_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4a38_178; else 
                        goto addr_53e2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5324_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4b43_22;
                }
                addr_545f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4b40_30:
                    r8d42 = 0;
                    goto addr_4b43_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4bb1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5478_42;
                    }
                }
                addr_4a38_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4a48_63;
                addr_53e2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5340_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4bb1_62;
                } else {
                    addr_53f2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4b43_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5ba2_188;
                if (v28) 
                    goto addr_5417_162;
                addr_5ba2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4d53_168;
                addr_49ec_37:
                if (v22) 
                    goto addr_59e3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4a03_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_54b0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_553b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4b43_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4a38_178; else 
                        goto addr_5517_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5324_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4b43_22;
                }
                addr_553b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4b43_22;
                }
                addr_5517_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5340_46;
                goto addr_53f2_186;
                addr_4a03_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4b43_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4b43_22; else 
                    goto addr_4a14_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_5aee_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5974_210;
            if (1) 
                goto addr_5972_212;
            if (!v29) 
                goto addr_55ae_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_5ae1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4d70_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_4b2b_219; else 
            goto addr_4d8a_220;
        addr_4b0f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4b23_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_4d8a_220; else 
            goto addr_4b2b_219;
        addr_56df_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_4d8a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_56fd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_5b70_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_55d6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_57c7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4b23_221;
        addr_5623_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4b23_221;
        addr_4d67_169:
        goto addr_4d70_65;
        addr_5aee_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_4d8a_220;
        goto addr_56fd_222;
        addr_5974_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_59ce_236;
        fun_24c0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5b70_225;
        addr_5972_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5974_210;
        addr_55ae_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5974_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_55d6_226;
        }
        addr_5ae1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4f3d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8dec + rax113 * 4) + 0x8dec;
    addr_5369_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8eec + rax114 * 4) + 0x8eec;
    addr_59e3_190:
    addr_4b2b_219:
    goto 0x4810;
    addr_4a14_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8cec + rax115 * 4) + 0x8cec;
    addr_59ce_236:
    goto v116;
}

void fun_4a30() {
}

void fun_4be8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x48e2;
}

void fun_4c41() {
    goto 0x48e2;
}

void fun_4d2e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4bb1;
    }
    if (v2) 
        goto 0x5623;
    if (!r10_3) 
        goto addr_578e_5;
    if (!v4) 
        goto addr_565e_7;
    addr_578e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_565e_7:
    goto 0x4a64;
}

void fun_4d4c() {
}

void fun_4df7() {
    signed char v1;

    if (v1) {
        goto 0x4d7f;
    } else {
        goto 0x4aba;
    }
}

void fun_4e11() {
    signed char v1;

    if (!v1) 
        goto 0x4e0a; else 
        goto "???";
}

void fun_4e38() {
    goto 0x4d53;
}

void fun_4eb8() {
}

void fun_4ed0() {
}

void fun_4eff() {
    goto 0x4d53;
}

void fun_4f51() {
    goto 0x4ee0;
}

void fun_4f80() {
    goto 0x4ee0;
}

void fun_4fb3() {
    goto 0x4ee0;
}

void fun_5380() {
    goto 0x4a38;
}

void fun_567e() {
    signed char v1;

    if (v1) 
        goto 0x5623;
    goto 0x4a64;
}

void fun_5725() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4a64;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4a48;
        goto 0x4a64;
    }
}

void fun_5b42() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4db0;
    } else {
        goto 0x48e2;
    }
}

void fun_6a78() {
    fun_2490();
}

void fun_77fc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x780b;
}

void fun_78cc() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x78d9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x780b;
}

void fun_78f0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_791c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x780b;
}

void fun_793d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x780b;
}

void fun_7961() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x780b;
}

void fun_7985() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7914;
}

void fun_79a9() {
}

void fun_79c9() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7914;
}

void fun_79e5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7914;
}

void fun_2cff() {
    void** rdi1;
    void** rax2;

    rdi1 = optarg;
    rax2 = size_opt(rdi1, "invalid number of bytes to skip");
    skip_chars = rax2;
    goto 0x28bc;
}

void fun_2dbe() {
    unsigned char r10b1;

    output_unique = r10b1;
    goto 0x28bc;
}

void fun_4c6e() {
    goto 0x48e2;
}

void fun_4e44() {
    goto 0x4dfc;
}

void fun_4f0b() {
    goto 0x4a38;
}

void fun_4f5d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4ee0;
    goto 0x4b0f;
}

void fun_4f8f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4eeb;
        goto 0x4910;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4d8a;
        goto 0x4b2b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5728;
    if (r10_8 > r15_9) 
        goto addr_4e75_9;
    addr_4e7a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5733;
    goto 0x4a64;
    addr_4e75_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4e7a_10;
}

void fun_4fc2() {
    goto 0x4af7;
}

void fun_5390() {
    goto 0x4af7;
}

void fun_5b2f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4c4c;
    } else {
        goto 0x4db0;
    }
}

void fun_6b30() {
}

void fun_789f() {
    if (__intrinsic()) 
        goto 0x78d9; else 
        goto "???";
}

void fun_2d1e() {
    ignore_case = 1;
    goto 0x28bc;
}

void fun_2dcf() {
    countmode = 0;
    goto 0x28bc;
}

void fun_4fcc() {
    goto 0x4f67;
}

void fun_539a() {
    goto 0x4ebd;
}

void fun_6b90() {
    fun_2490();
    goto fun_2700;
}

void fun_2d2a() {
    void** rdi1;
    void** rax2;

    rdi1 = optarg;
    rax2 = size_opt(rdi1, "invalid number of fields to skip");
    skip_fields = rax2;
    goto 0x28bc;
}

void fun_4c9d() {
    goto 0x48e2;
}

void fun_4fd8() {
    goto 0x4f67;
}

void fun_53a7() {
    goto 0x4f0e;
}

void fun_6bd0() {
    fun_2490();
    goto fun_2700;
}

void fun_2d51() {
    void** rsi1;
    int64_t rax2;

    rsi1 = optarg;
    if (!rsi1) 
        goto 0x3061;
    rax2 = __xargmatch_internal("--group");
    grouping = *reinterpret_cast<int32_t*>(0x8b50 + rax2 * 4);
    goto 0x28bc;
}

void fun_4cca() {
    goto 0x48e2;
}

void fun_4fe4() {
    goto 0x4ee0;
}

void fun_6c10() {
    fun_2490();
    goto fun_2700;
}

void fun_4cec() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5680;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4bb1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4bb1;
    }
    if (v11) 
        goto 0x59e3;
    if (r10_12 > r15_13) 
        goto addr_5a33_8;
    addr_5a38_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5771;
    addr_5a33_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5a38_9;
}

struct s16 {
    signed char[24] pad24;
    int64_t f18;
};

struct s17 {
    signed char[16] pad16;
    void** f10;
};

struct s18 {
    signed char[8] pad8;
    void** f8;
};

void fun_6c60() {
    int64_t r15_1;
    struct s16* rbx2;
    void** r14_3;
    struct s17* rbx4;
    void** r13_5;
    struct s18* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2490();
    fun_2700(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6c82, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6cb8() {
    fun_2490();
    goto 0x6c89;
}

struct s19 {
    signed char[32] pad32;
    void*** f20;
};

struct s20 {
    signed char[24] pad24;
    int64_t f18;
};

struct s21 {
    signed char[16] pad16;
    void** f10;
};

struct s22 {
    signed char[8] pad8;
    void** f8;
};

struct s23 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6cf0() {
    void*** rcx1;
    struct s19* rbx2;
    int64_t r15_3;
    struct s20* rbx4;
    void** r14_5;
    struct s21* rbx6;
    void** r13_7;
    struct s22* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s23* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2490();
    fun_2700(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6d24, __return_address(), rcx1);
    goto v15;
}

void fun_6d68() {
    fun_2490();
    goto 0x6d2b;
}
