
void** xstrdup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void*** a7, void** a8, void* a9);

void** quote(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void alloc_field(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7);

struct s0 {
    signed char f0;
    void** f1;
};

struct s0* fun_37b0(void** rdi, ...);

int64_t fun_38c0(void** rdi, void** rsi, void** rdx, ...);

void** fun_3730();

void** fun_3a30();

void usage();

void decode_output_arg(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    void** r12_8;
    void*** v9;
    void** v10;
    void* v11;
    void** rax12;
    void** r13_13;
    int64_t rax14;
    int32_t ebp15;
    void** r15_16;
    void** rsi17;
    void** rax18;
    int64_t v19;
    void** rbx20;
    int64_t v21;
    int64_t v22;
    struct s0* rax23;
    void*** r14_24;
    int64_t rax25;
    void** rax26;
    void** rax27;

    r12_8 = reinterpret_cast<void**>(0x18020);
    rax12 = xstrdup(rdi, rsi, rdx, rcx, r8, r9, v9, v10, v11);
    r13_13 = rax12;
    goto addr_6718_2;
    addr_67ee_3:
    while (1) {
        addr_67a0_5:
        *reinterpret_cast<int32_t*>(&rax14) = ebp15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_8) + (reinterpret_cast<uint64_t>(rax14 + rax14 * 2) << 4) + 44)) {
            rax18 = quote(r15_16, rsi17, rdx, rcx, r8, r9);
            r12_8 = rax18;
        } else {
            if (ebp15 == 2) {
                alloc_field(2, "Size", rdx, rcx, r8, r9, v19);
                if (!rbx20) 
                    goto addr_67ee_3;
            } else {
                if (ebp15 == 4) {
                    alloc_field(4, "Avail", rdx, rcx, r8, r9, v21);
                } else {
                    alloc_field(ebp15, 0, rdx, rcx, r8, r9, v22);
                }
                if (!rbx20) 
                    goto addr_67ee_3;
            }
            r13_13 = rbx20;
            addr_6718_2:
            *reinterpret_cast<int32_t*>(&rbx20) = 0;
            *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
            rax23 = fun_37b0(r13_13, r13_13);
            if (rax23) {
                rax23->f0 = 0;
                rbx20 = reinterpret_cast<void**>(&rax23->f1);
            }
            r14_24 = reinterpret_cast<void***>(0x18028);
            ebp15 = 0;
            do {
                r15_16 = *r14_24;
                rsi17 = r13_13;
                rax25 = fun_38c0(r15_16, rsi17, rdx, r15_16, rsi17, rdx);
                if (!*reinterpret_cast<int32_t*>(&rax25)) 
                    goto addr_67a0_5;
                ++ebp15;
                r14_24 = r14_24 + 48;
            } while (ebp15 != 12);
            rax26 = quote(r13_13, rsi17, rdx, rcx, r8, r9);
            r12_8 = rax26;
        }
        rax27 = fun_3730();
        rcx = r12_8;
        *reinterpret_cast<int32_t*>(&rsi17) = 0;
        *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
        rdx = rax27;
        fun_3a30();
        usage();
    }
}

uint64_t ncolumns = 0;

void** columns = reinterpret_cast<void**>(0);

void** xreallocarray(void** rdi);

struct s1 {
    signed char[24] pad24;
    void** f18;
};

struct s2 {
    signed char[44] pad44;
    signed char f2c;
};

uint64_t fun_3800(int64_t rdi, void** rsi, ...);

void** nrows = reinterpret_cast<void**>(0);

void** table = reinterpret_cast<void**>(0);

void** xnmalloc(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void alloc_field(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    uint64_t rax8;
    void** rdi9;
    void** rax10;
    int64_t rdi11;
    uint64_t rsi12;
    struct s1* rdx13;
    struct s2* rcx14;
    void** rax15;
    void** rdi16;
    void** rax17;
    void** rdx18;
    uint64_t rdi19;
    void** rax20;
    int64_t v21;

    rax8 = ncolumns;
    rdi9 = columns;
    ncolumns = rax8 + 1;
    rax10 = xreallocarray(rdi9);
    rdi11 = edi;
    rsi12 = ncolumns;
    columns = rax10;
    rdx13 = reinterpret_cast<struct s1*>((rdi11 + rdi11 * 2 << 4) + 0x18020);
    *reinterpret_cast<struct s1**>(reinterpret_cast<uint64_t>(rax10 + rsi12 * 8) - 8) = rdx13;
    if (rsi) {
        rdx13->f18 = rsi;
    }
    rcx14 = reinterpret_cast<struct s2*>(0x18020 + (rdi11 + rdi11 * 2 << 4));
    if (rcx14->f2c) {
        fun_3800("!\"field used\"", "src/df.c", "!\"field used\"", "src/df.c");
        rax15 = nrows;
        rdi16 = table;
        nrows = rax15 + 1;
        rax17 = xreallocarray(rdi16);
        rdx18 = nrows;
        rdi19 = ncolumns;
        table = rax17;
        rax20 = xnmalloc(rdi19, 8, rdx18, "alloc_field", r8, r9);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rax17 + reinterpret_cast<unsigned char>(rdx18) * 8) - 8) = rax20;
        goto v21;
    } else {
        rcx14->f2c = 1;
        return;
    }
}

int64_t g28;

void alloc_table_row(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

uint32_t header_mode = 0;

uint32_t human_output_opts = 0;

uint64_t output_block_size = 0;

void** fun_3ae0();

void** umaxtostr(uint64_t rdi, void* rsi);

void** human_readable(uint64_t rdi, void** rsi, ...);

int32_t rpl_asprintf();

/* tty_out.1 */
int32_t tty_out_1 = -1;

int32_t fun_3670(int64_t rdi);

int32_t gnu_mbswidth(void** rdi);

void* fun_3780();

struct s3 {
    signed char[48] pad48;
    uint64_t f30;
};

unsigned char show_local_fs = 0;

unsigned char show_all_fs = 0;

signed char show_listed_fs = 0;

void** fs_select_list = reinterpret_cast<void**>(0);

void** fs_exclude_list = reinterpret_cast<void**>(0);

int32_t get_fs_usage(void** rdi, void** rsi, void** rdx);

void** fun_3630();

void** quotearg_n_style_colon();

int32_t exit_status = 0;

int32_t fun_38f0(void** rdi, void** rsi, ...);

void** devlist_table = reinterpret_cast<void**>(0);

struct s5 {
    signed char[8] pad8;
    void** f8;
};

struct s4 {
    signed char[24] pad24;
    struct s5* f18;
};

struct s4* hash_lookup();

signed char file_systems_processed = 0;

void** fun_3750(void** rdi, ...);

void* fun_3850(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t canonicalize_filename_mode(void** rdi);

void fun_35f0(void** rdi, ...);

signed char print_grand_total = 0;

int64_t g183c8 = 0;

uint64_t g183d0 = 0;

int64_t g183a8 = 0;

int64_t g183b0 = 0;

unsigned char g183c0 = 0;

uint64_t g183b8 = 0;

void xalloc_die();

void get_header(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    int64_t rax8;
    int64_t v9;
    void* rsp10;
    int1_t zf11;
    void** rdx12;
    int64_t rax13;
    uint64_t rbx14;
    void* r15_15;
    void** v16;
    void*** r14_17;
    void** rax18;
    void* rsp19;
    uint32_t eax20;
    uint64_t rdi21;
    uint32_t r8d22;
    uint64_t rsi23;
    uint64_t rcx24;
    void** rax25;
    void* rsp26;
    void** r14_27;
    uint64_t rdi28;
    void** v29;
    void** rax30;
    void** r14_31;
    void** rax32;
    void* rsp33;
    uint64_t rax34;
    uint64_t rdx35;
    uint64_t rdx36;
    uint64_t rax37;
    uint32_t eax38;
    unsigned char cl39;
    unsigned char dl40;
    void** rax41;
    void** rax42;
    void** rax43;
    int32_t eax44;
    int32_t eax45;
    int64_t rax46;
    void** rdx47;
    void** rax48;
    int32_t eax49;
    void** rax50;
    int1_t below_or_equal51;
    void** r14_52;
    void** r13_53;
    void** rbp54;
    void* rsp55;
    void** v56;
    void** v57;
    int32_t r15d58;
    int32_t v59;
    struct s3* rbx60;
    struct s3* v61;
    int64_t rax62;
    int64_t v63;
    int1_t zf64;
    int1_t zf65;
    int1_t zf66;
    void** r12_67;
    void** r12_68;
    unsigned char v69;
    uint32_t eax70;
    void** rax71;
    void** rax72;
    int32_t eax73;
    void** rax74;
    int1_t zf75;
    int64_t rax76;
    int64_t v77;
    uint64_t v78;
    int1_t zf79;
    int1_t zf80;
    int32_t eax81;
    struct s4* rax82;
    void** r12_83;
    int64_t rax84;
    int64_t v85;
    int1_t zf86;
    void** rax87;
    void*** v88;
    void** rax89;
    void** rax90;
    void* rax91;
    int64_t rax92;
    uint64_t rdx93;
    uint64_t r9_94;
    int1_t zf95;
    int64_t tmp64_96;
    uint64_t tmp64_97;
    int64_t tmp64_98;
    int64_t tmp64_99;
    uint32_t ecx100;
    uint64_t rax101;
    void** rdx102;
    int1_t zf103;
    void** rdx104;
    int64_t rax105;
    int64_t rax106;
    int64_t v107;
    int1_t zf108;
    int1_t zf109;
    int64_t rax110;
    int64_t rax111;

    rax8 = g28;
    v9 = rax8;
    alloc_table_row(rdi, rsi, rdx, rcx, r8, r9);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x2c8 - 8 + 8);
    zf11 = ncolumns == 0;
    rdx12 = columns;
    if (zf11) {
        addr_5670_2:
        rax13 = v9 - g28;
        if (!rax13) {
            return;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rbx14) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx14) + 4) = 0;
        do {
            r15_15 = reinterpret_cast<void*>(rbx14 * 8);
            v16 = reinterpret_cast<void**>(0);
            r14_17 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx12) + reinterpret_cast<uint64_t>(r15_15));
            *reinterpret_cast<int32_t*>(&rdx12) = 5;
            *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
            rsi = *reinterpret_cast<void***>(*r14_17 + 24);
            rax18 = fun_3730();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            r9 = rax18;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(*r14_17) == 2)) 
                goto addr_5430_6;
            eax20 = header_mode;
            *reinterpret_cast<uint32_t*>(&r8) = human_output_opts;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            if (!eax20) {
                addr_55a0_8:
                rdi21 = output_block_size;
                r8d22 = *reinterpret_cast<uint32_t*>(&r8) & 0x124;
                rsi23 = rdi21;
                rcx24 = rdi21;
            } else {
                if (eax20 == 4) {
                    *reinterpret_cast<uint32_t*>(&r8) = human_output_opts;
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&r8) & 16) {
                        addr_5430_6:
                        rdi = r9;
                        rax25 = fun_3ae0();
                        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                        v16 = rax25;
                        r14_27 = rax25;
                        goto addr_5440_11;
                    } else {
                        goto addr_55a0_8;
                    }
                } else {
                    if (eax20 != 3) 
                        goto addr_5430_6;
                    rdi28 = output_block_size;
                    v29 = r9;
                    rax30 = umaxtostr(rdi28, reinterpret_cast<int64_t>(rsp19) + 32);
                    r14_31 = rax30;
                    rax32 = fun_3730();
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
                    rcx = v29;
                    rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp33) + 24);
                    rsi = rax32;
                    goto addr_5548_15;
                }
            }
            do {
                r9 = reinterpret_cast<void**>((__intrinsic() >> 4) * 0x3e8);
                rax34 = rcx24;
                rdx35 = rsi23;
                rcx24 = __intrinsic() >> 4;
                *reinterpret_cast<uint32_t*>(&rdx36) = *reinterpret_cast<uint32_t*>(&rdx35) & 0x3ff;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx36) + 4) = 0;
                rsi23 = rsi23 >> 10;
                rax37 = rax34 - reinterpret_cast<unsigned char>(r9);
            } while (!(rax37 | rdx36));
            eax38 = r8d22;
            cl39 = reinterpret_cast<uint1_t>(rax37 == 0);
            dl40 = reinterpret_cast<uint1_t>(rdx36 == 0);
            *reinterpret_cast<unsigned char*>(&eax38) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax38) | 0x98);
            *reinterpret_cast<unsigned char*>(&r8d22) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d22) | 0xb8);
            if (cl39 < dl40) {
                eax38 = r8d22;
            }
            if (cl39 <= dl40) {
                if (*reinterpret_cast<unsigned char*>(&eax38) & 32) {
                    addr_561e_21:
                    *reinterpret_cast<uint32_t*>(&r8) = 1;
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    rax41 = human_readable(rdi21, reinterpret_cast<int64_t>(rsp19) + 32);
                    r14_31 = rax41;
                    rax42 = fun_3730();
                    v29 = rax42;
                    rax43 = fun_3730();
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                    rcx = v29;
                    rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp33) + 24);
                    rsi = rax43;
                } else {
                    goto addr_561b_23;
                }
            } else {
                goto addr_561b_23;
            }
            addr_5548_15:
            rdx12 = r14_31;
            eax44 = rpl_asprintf();
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
            if (eax44 == -1) {
                v16 = reinterpret_cast<void**>(0);
            }
            r14_27 = reinterpret_cast<void**>(0);
            addr_5440_11:
            if (!r14_27) 
                goto addr_56a5_27;
            eax45 = tty_out_1;
            if (eax45 < 0) {
                eax45 = fun_3670(1);
                rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                tty_out_1 = eax45;
            }
            rax46 = 0x5190;
            if (eax45) {
                rax46 = 0x52e0;
            }
            rax46(r14_27);
            rdx47 = nrows;
            rdi = v16;
            rax48 = table;
            *reinterpret_cast<void***>(reinterpret_cast<int64_t>(*reinterpret_cast<void****>(reinterpret_cast<uint64_t>(rax48 + reinterpret_cast<unsigned char>(rdx47) * 8) - 8)) + reinterpret_cast<uint64_t>(r15_15)) = rdi;
            eax49 = gnu_mbswidth(rdi);
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
            rdx12 = columns;
            rax50 = reinterpret_cast<void**>(static_cast<int64_t>(eax49));
            rcx = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx12) + reinterpret_cast<uint64_t>(r15_15));
            rsi = *reinterpret_cast<void***>(rcx + 32);
            if (reinterpret_cast<unsigned char>(rax50) < reinterpret_cast<unsigned char>(rsi)) {
                rax50 = rsi;
                continue;
            }
            addr_561b_23:
            goto addr_561e_21;
            ++rbx14;
            below_or_equal51 = ncolumns <= rbx14;
            *reinterpret_cast<void***>(rcx + 32) = rax50;
        } while (!below_or_equal51);
        goto addr_5670_2;
    }
    addr_56aa_35:
    fun_3780();
    r14_52 = rsi;
    r13_53 = r8;
    rbp54 = rdi;
    rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x428);
    v56 = rdx12;
    v57 = rcx;
    r15d58 = *reinterpret_cast<int32_t*>(&v29);
    v59 = *reinterpret_cast<int32_t*>(&v16);
    rbx60 = v61;
    rax62 = g28;
    v63 = rax62;
    if (!*reinterpret_cast<signed char*>(&r15d58)) 
        goto addr_5710_37;
    zf64 = show_local_fs == 0;
    if (!zf64) 
        goto addr_574e_39;
    addr_5710_37:
    if (*reinterpret_cast<signed char*>(&r9) && (zf65 = show_all_fs == 0, zf65)) {
        zf66 = show_listed_fs == 0;
        if (zf66) 
            goto addr_574e_39;
        r12_67 = fs_select_list;
        if (r12_67) 
            goto addr_5721_42;
        goto addr_57a0_44;
    }
    r12_67 = fs_select_list;
    if (!r12_67) {
        addr_57a0_44:
        r12_68 = fs_exclude_list;
        if (!r12_68 || !r13_53) {
            addr_57d2_46:
            v69 = reinterpret_cast<uint1_t>(rbx60 == 0);
            eax70 = v69;
            if (!r14_52 || !*reinterpret_cast<signed char*>(&eax70)) {
                if (!v57) {
                    rax71 = rbp54;
                    if (r14_52) {
                        rax71 = r14_52;
                    }
                    v57 = rax71;
                }
                if (rbx60) 
                    goto addr_5a7d_52;
            } else {
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r14_52) == 47)) 
                    goto addr_574e_39;
                rax72 = v57;
                if (!rax72) {
                    rax72 = r14_52;
                }
                v57 = rax72;
            }
        } else {
            goto addr_57b8_58;
        }
    } else {
        addr_5721_42:
        if (!r13_53) 
            goto addr_57d2_46; else 
            goto addr_572a_59;
    }
    rdi = v57;
    rdx12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp55) + 48);
    rsi = rbp54;
    eax73 = get_fs_usage(rdi, rsi, rdx12);
    rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
    if (eax73) {
        rax74 = fun_3630();
        if (!*reinterpret_cast<signed char*>(&v59) || *reinterpret_cast<void***>(rax74) != 13 && *reinterpret_cast<void***>(rax74) != 2) {
            quotearg_n_style_colon();
            fun_3a30();
            exit_status = 1;
            goto addr_574e_39;
        }
        zf75 = show_all_fs == 0;
        if (zf75) {
            addr_574e_39:
            rax76 = v63 - g28;
            if (!rax76) {
                goto v77;
            }
        } else {
            r13_53 = reinterpret_cast<void**>("-");
            v78 = 0xffffffffffffffff;
            goto addr_5857_66;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&v59)) {
            addr_616c_68:
            if (1) 
                goto addr_5857_66;
            zf79 = show_all_fs == 0;
            if (!zf79) 
                goto addr_5857_66; else 
                goto addr_6185_70;
        } else {
            zf80 = show_all_fs == 0;
            if (!zf80) {
                rdi = v57;
                rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp55) + 0xf0);
                eax81 = fun_38f0(rdi, rsi, rdi, rsi);
                rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
                if (!eax81 && ((rdi = devlist_table, !!rdi) && ((rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp55) + 0xb0), rax82 = hash_lookup(), !!rax82) && (rax82->f18 && ((r12_83 = rax82->f18->f8, !!r12_83) && ((rdi = *reinterpret_cast<void***>(r12_83), rsi = rbp54, rax84 = fun_38c0(rdi, rsi, rdx12), !!*reinterpret_cast<int32_t*>(&rax84)) && (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_83 + 40)) & 2) || !*reinterpret_cast<signed char*>(&r15d58)))))))) {
                    r13_53 = reinterpret_cast<void**>("-");
                    v78 = 0xffffffffffffffff;
                    goto addr_616c_68;
                }
            } else {
                if (v85) 
                    goto addr_5857_66; else 
                    goto addr_5845_75;
            }
        }
    }
    fun_3780();
    addr_6185_70:
    addr_5845_75:
    zf86 = show_listed_fs == 0;
    if (zf86) 
        goto addr_574e_39;
    if (rbx60) {
        addr_585e_79:
        alloc_table_row(rdi, rsi, rdx12, rcx, r8, r9);
        rax87 = reinterpret_cast<void**>("-");
        if (!rbp54) {
            rbp54 = reinterpret_cast<void**>("-");
        }
    } else {
        addr_5857_66:
        file_systems_processed = 1;
        goto addr_585e_79;
    }
    if (v56) {
        rax87 = v56;
    }
    rax89 = xstrdup(rbp54, rsi, rdx12, rcx, r8, r9, v88, rax87, *reinterpret_cast<void**>(&v57));
    if (*reinterpret_cast<signed char*>(&v59) && (rax90 = fun_3750(rax89, rax89), reinterpret_cast<unsigned char>(rax90) > reinterpret_cast<unsigned char>(36))) {
        rax91 = fun_3850(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<unsigned char>(rax90) + 0xffffffffffffffdc, "-0123456789abcdefABCDEF", rdx12, rcx, r8);
        if (rax91 == 36) {
            rax92 = canonicalize_filename_mode(rax89);
            if (rax92) {
                fun_35f0(rax89, rax89);
            }
        }
    }
    if (!r13_53) {
    }
    if (v78 <= 0xfffffffffffffffd && !1) {
    }
    rdx93 = output_block_size;
    r9_94 = 0xffffffffffffffff;
    *reinterpret_cast<unsigned char*>(&rdx93) = 0;
    if (!1 && !1) {
    }
    zf95 = print_grand_total == 0;
    if (!zf95 && v69) {
        if (!1) {
            tmp64_96 = g183c8 - 1;
            g183c8 = tmp64_96;
        }
        if (v78 <= 0xfffffffffffffffd) {
            tmp64_97 = g183d0 + v78;
            g183d0 = tmp64_97;
        }
        if (!1) {
            tmp64_98 = g183a8 + 1;
            g183a8 = tmp64_98;
        }
        if (!1) {
            tmp64_99 = g183b0 + 1;
            g183b0 = tmp64_99;
        }
        if (!1) {
            ecx100 = g183c0;
            r9_94 = 1;
            rax101 = g183b8;
            if (!*reinterpret_cast<unsigned char*>(&ecx100)) {
                r9_94 = 1 + rax101;
                g183b8 = r9_94;
            } else {
                if (*reinterpret_cast<unsigned char*>(&ecx100)) {
                    rax101 = -rax101;
                }
                if (0) {
                    r9_94 = reinterpret_cast<uint64_t>(-1);
                }
                if (r9_94 >= rax101) 
                    goto addr_621f_111;
                g183b8 = rax101 - r9_94;
                goto addr_6092_113;
            }
        }
    }
    while (rdx102 = columns, zf103 = ncolumns == 0, !zf103) {
        rdx104 = *reinterpret_cast<void***>(rdx102);
        if (*reinterpret_cast<void***>(rdx104 + 16) != 1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx104 + 16) == 2)) {
                if (*reinterpret_cast<void***>(rdx104 + 16)) {
                    *reinterpret_cast<int32_t*>(&rdx104) = 0x480;
                    *reinterpret_cast<int32_t*>(&rdx104 + 4) = 0;
                    fun_3800("!\"bad field_type\"", "src/df.c", "!\"bad field_type\"", "src/df.c");
                }
            }
        }
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx104)) <= reinterpret_cast<unsigned char>(11)) 
            goto addr_5a3c_124;
        *reinterpret_cast<uint32_t*>(&rdx93) = 0x4e6;
        rax101 = fun_3800("!\"unhandled field\"", "src/df.c", "!\"unhandled field\"", "src/df.c");
        addr_621f_111:
        r9_94 = r9_94 - rax101;
        g183c0 = *reinterpret_cast<unsigned char*>(&rdx93);
        ecx100 = *reinterpret_cast<uint32_t*>(&rdx93);
        g183b8 = r9_94;
        addr_6092_113:
        if (!*reinterpret_cast<unsigned char*>(&ecx100)) 
            continue;
        g183b8 = -g183b8;
    }
    rax105 = v63 - g28;
    if (!rax105) {
    }
    addr_5a3c_124:
    *reinterpret_cast<void***>(&rax106) = *reinterpret_cast<void***>(rdx104);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x122d8 + rax106 * 4) + 0x122d8;
    addr_5a7d_52:
    __asm__("movdqa xmm5, [rbx]");
    __asm__("movdqa xmm6, [rbx+0x10]");
    __asm__("movdqa xmm7, [rbx+0x20]");
    __asm__("movaps [rsp+0x30], xmm5");
    v78 = rbx60->f30;
    __asm__("movaps [rsp+0x40], xmm6");
    __asm__("movaps [rsp+0x50], xmm7");
    if (!v107 && (zf108 = show_all_fs == 0, zf108)) {
        zf109 = show_listed_fs == 0;
        if (zf109) 
            goto addr_574e_39;
        goto addr_585e_79;
    }
    do {
        addr_57b8_58:
        rsi = *reinterpret_cast<void***>(r12_68);
        rdi = r13_53;
        rax110 = fun_38c0(rdi, rsi, rdx12);
        rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax110)) 
            goto addr_574e_39;
        r12_68 = *reinterpret_cast<void***>(r12_68 + 8);
    } while (r12_68);
    goto addr_57d2_46;
    addr_572a_59:
    do {
        rsi = *reinterpret_cast<void***>(r12_67);
        rdi = r13_53;
        rax111 = fun_38c0(rdi, rsi, rdx12);
        rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax111)) 
            break;
        r12_67 = *reinterpret_cast<void***>(r12_67 + 8);
    } while (r12_67);
    goto addr_574e_39;
    r12_68 = fs_exclude_list;
    if (r12_68) 
        goto addr_57b8_58;
    goto addr_57d2_46;
    addr_56a5_27:
    xalloc_die();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
    goto addr_56aa_35;
}

struct s6 {
    signed char[48] pad48;
    uint64_t f30;
};

void get_dev(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int32_t a7, struct s6* a8, int32_t a9, void** a10, int64_t a11, ...) {
    void** r14_12;
    void** r13_13;
    void** rbp14;
    void* rsp15;
    void** v16;
    void** v17;
    int32_t r15d18;
    int32_t v19;
    struct s6* rbx20;
    int64_t rax21;
    int64_t v22;
    int1_t zf23;
    int1_t zf24;
    int1_t zf25;
    void** r12_26;
    void** r12_27;
    unsigned char v28;
    uint32_t eax29;
    void** rax30;
    void** rax31;
    int32_t eax32;
    void** rax33;
    int1_t zf34;
    int64_t rax35;
    uint64_t v36;
    int1_t zf37;
    int1_t zf38;
    int32_t eax39;
    struct s4* rax40;
    void** r12_41;
    int64_t rax42;
    int64_t v43;
    int1_t zf44;
    void** rax45;
    void*** v46;
    void** rax47;
    void** rax48;
    void* rax49;
    int64_t rax50;
    uint64_t rdx51;
    uint64_t r9_52;
    int1_t zf53;
    int64_t tmp64_54;
    uint64_t tmp64_55;
    int64_t tmp64_56;
    int64_t tmp64_57;
    uint32_t ecx58;
    uint64_t rax59;
    void** rdx60;
    int1_t zf61;
    void** rdx62;
    int64_t rax63;
    int64_t rax64;
    int64_t v65;
    int1_t zf66;
    int1_t zf67;
    int64_t rax68;
    int64_t rax69;

    r14_12 = rsi;
    r13_13 = r8;
    rbp14 = rdi;
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x428);
    v16 = rdx;
    v17 = rcx;
    r15d18 = a7;
    v19 = a9;
    rbx20 = a8;
    rax21 = g28;
    v22 = rax21;
    if (!*reinterpret_cast<signed char*>(&r15d18)) 
        goto addr_5710_2;
    zf23 = show_local_fs == 0;
    if (!zf23) 
        goto addr_574e_4;
    addr_5710_2:
    if (*reinterpret_cast<signed char*>(&r9) && (zf24 = show_all_fs == 0, zf24)) {
        zf25 = show_listed_fs == 0;
        if (zf25) 
            goto addr_574e_4;
        r12_26 = fs_select_list;
        if (r12_26) 
            goto addr_5721_7;
        goto addr_57a0_9;
    }
    r12_26 = fs_select_list;
    if (!r12_26) {
        addr_57a0_9:
        r12_27 = fs_exclude_list;
        if (!r12_27 || !r13_13) {
            addr_57d2_11:
            v28 = reinterpret_cast<uint1_t>(rbx20 == 0);
            eax29 = v28;
            if (!r14_12 || !*reinterpret_cast<signed char*>(&eax29)) {
                if (!v17) {
                    rax30 = rbp14;
                    if (r14_12) {
                        rax30 = r14_12;
                    }
                    v17 = rax30;
                }
                if (rbx20) 
                    goto addr_5a7d_17;
            } else {
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r14_12) == 47)) 
                    goto addr_574e_4;
                rax31 = v17;
                if (!rax31) {
                    rax31 = r14_12;
                }
                v17 = rax31;
            }
        } else {
            goto addr_57b8_23;
        }
    } else {
        addr_5721_7:
        if (!r13_13) 
            goto addr_57d2_11; else 
            goto addr_572a_24;
    }
    rdi = v17;
    rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp15) + 48);
    rsi = rbp14;
    eax32 = get_fs_usage(rdi, rsi, rdx);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
    if (eax32) {
        rax33 = fun_3630();
        if (!*reinterpret_cast<signed char*>(&v19) || *reinterpret_cast<void***>(rax33) != 13 && *reinterpret_cast<void***>(rax33) != 2) {
            quotearg_n_style_colon();
            fun_3a30();
            exit_status = 1;
            goto addr_574e_4;
        }
        zf34 = show_all_fs == 0;
        if (zf34) {
            addr_574e_4:
            rax35 = v22 - g28;
            if (!rax35) {
                return;
            }
        } else {
            r13_13 = reinterpret_cast<void**>("-");
            v36 = 0xffffffffffffffff;
            goto addr_5857_31;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&v19)) {
            addr_616c_33:
            if (1) 
                goto addr_5857_31;
            zf37 = show_all_fs == 0;
            if (!zf37) 
                goto addr_5857_31; else 
                goto addr_6185_35;
        } else {
            zf38 = show_all_fs == 0;
            if (!zf38) {
                rdi = v17;
                rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp15) + 0xf0);
                eax39 = fun_38f0(rdi, rsi, rdi, rsi);
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                if (!eax39 && ((rdi = devlist_table, !!rdi) && ((rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp15) + 0xb0), rax40 = hash_lookup(), !!rax40) && (rax40->f18 && ((r12_41 = rax40->f18->f8, !!r12_41) && ((rdi = *reinterpret_cast<void***>(r12_41), rsi = rbp14, rax42 = fun_38c0(rdi, rsi, rdx), !!*reinterpret_cast<int32_t*>(&rax42)) && (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_41 + 40)) & 2) || !*reinterpret_cast<signed char*>(&r15d18)))))))) {
                    r13_13 = reinterpret_cast<void**>("-");
                    v36 = 0xffffffffffffffff;
                    goto addr_616c_33;
                }
            } else {
                if (v43) 
                    goto addr_5857_31; else 
                    goto addr_5845_40;
            }
        }
    }
    fun_3780();
    addr_6185_35:
    addr_5845_40:
    zf44 = show_listed_fs == 0;
    if (zf44) 
        goto addr_574e_4;
    if (rbx20) {
        addr_585e_44:
        alloc_table_row(rdi, rsi, rdx, rcx, r8, r9);
        rax45 = reinterpret_cast<void**>("-");
        if (!rbp14) {
            rbp14 = reinterpret_cast<void**>("-");
        }
    } else {
        addr_5857_31:
        file_systems_processed = 1;
        goto addr_585e_44;
    }
    if (v16) {
        rax45 = v16;
    }
    rax47 = xstrdup(rbp14, rsi, rdx, rcx, r8, r9, v46, rax45, *reinterpret_cast<void**>(&v17));
    if (*reinterpret_cast<signed char*>(&v19) && (rax48 = fun_3750(rax47, rax47), reinterpret_cast<unsigned char>(rax48) > reinterpret_cast<unsigned char>(36))) {
        rax49 = fun_3850(reinterpret_cast<unsigned char>(rax47) + reinterpret_cast<unsigned char>(rax48) + 0xffffffffffffffdc, "-0123456789abcdefABCDEF", rdx, rcx, r8);
        if (rax49 == 36) {
            rax50 = canonicalize_filename_mode(rax47);
            if (rax50) {
                fun_35f0(rax47, rax47);
            }
        }
    }
    if (!r13_13) {
    }
    if (v36 <= 0xfffffffffffffffd && !1) {
    }
    rdx51 = output_block_size;
    r9_52 = 0xffffffffffffffff;
    *reinterpret_cast<unsigned char*>(&rdx51) = 0;
    if (!1 && !1) {
    }
    zf53 = print_grand_total == 0;
    if (!zf53 && v28) {
        if (!1) {
            tmp64_54 = g183c8 - 1;
            g183c8 = tmp64_54;
        }
        if (v36 <= 0xfffffffffffffffd) {
            tmp64_55 = g183d0 + v36;
            g183d0 = tmp64_55;
        }
        if (!1) {
            tmp64_56 = g183a8 + 1;
            g183a8 = tmp64_56;
        }
        if (!1) {
            tmp64_57 = g183b0 + 1;
            g183b0 = tmp64_57;
        }
        if (!1) {
            ecx58 = g183c0;
            r9_52 = 1;
            rax59 = g183b8;
            if (!*reinterpret_cast<unsigned char*>(&ecx58)) {
                r9_52 = 1 + rax59;
                g183b8 = r9_52;
            } else {
                if (*reinterpret_cast<unsigned char*>(&ecx58)) {
                    rax59 = -rax59;
                }
                if (0) {
                    r9_52 = reinterpret_cast<uint64_t>(-1);
                }
                if (r9_52 >= rax59) 
                    goto addr_621f_76;
                g183b8 = rax59 - r9_52;
                goto addr_6092_78;
            }
        }
    }
    while (rdx60 = columns, zf61 = ncolumns == 0, !zf61) {
        rdx62 = *reinterpret_cast<void***>(rdx60);
        if (*reinterpret_cast<void***>(rdx62 + 16) != 1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx62 + 16) == 2)) {
                if (*reinterpret_cast<void***>(rdx62 + 16)) {
                    *reinterpret_cast<int32_t*>(&rdx62) = 0x480;
                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                    fun_3800("!\"bad field_type\"", "src/df.c", "!\"bad field_type\"", "src/df.c");
                }
            }
        }
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx62)) <= reinterpret_cast<unsigned char>(11)) 
            goto addr_5a3c_89;
        *reinterpret_cast<uint32_t*>(&rdx51) = 0x4e6;
        rax59 = fun_3800("!\"unhandled field\"", "src/df.c", "!\"unhandled field\"", "src/df.c");
        addr_621f_76:
        r9_52 = r9_52 - rax59;
        g183c0 = *reinterpret_cast<unsigned char*>(&rdx51);
        ecx58 = *reinterpret_cast<uint32_t*>(&rdx51);
        g183b8 = r9_52;
        addr_6092_78:
        if (!*reinterpret_cast<unsigned char*>(&ecx58)) 
            continue;
        g183b8 = -g183b8;
    }
    rax63 = v22 - g28;
    if (!rax63) {
    }
    addr_5a3c_89:
    *reinterpret_cast<void***>(&rax64) = *reinterpret_cast<void***>(rdx62);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax64) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x122d8 + rax64 * 4) + 0x122d8;
    addr_5a7d_17:
    __asm__("movdqa xmm5, [rbx]");
    __asm__("movdqa xmm6, [rbx+0x10]");
    __asm__("movdqa xmm7, [rbx+0x20]");
    __asm__("movaps [rsp+0x30], xmm5");
    v36 = rbx20->f30;
    __asm__("movaps [rsp+0x40], xmm6");
    __asm__("movaps [rsp+0x50], xmm7");
    if (!v65 && (zf66 = show_all_fs == 0, zf66)) {
        zf67 = show_listed_fs == 0;
        if (zf67) 
            goto addr_574e_4;
        goto addr_585e_44;
    }
    do {
        addr_57b8_23:
        rsi = *reinterpret_cast<void***>(r12_27);
        rdi = r13_13;
        rax68 = fun_38c0(rdi, rsi, rdx);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax68)) 
            goto addr_574e_4;
        r12_27 = *reinterpret_cast<void***>(r12_27 + 8);
    } while (r12_27);
    goto addr_57d2_11;
    addr_572a_24:
    do {
        rsi = *reinterpret_cast<void***>(r12_26);
        rdi = r13_13;
        rax69 = fun_38c0(rdi, rsi, rdx);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax69)) 
            break;
        r12_26 = *reinterpret_cast<void***>(r12_26 + 8);
    } while (r12_26);
    goto addr_574e_4;
    r12_27 = fs_exclude_list;
    if (r12_27) 
        goto addr_57b8_23;
    goto addr_57d2_11;
}

signed char gl_scratch_buffer_grow_preserve(void** rdi, void** rsi);

void** fun_3a10(void** rdi, void** rsi, void** rdx);

void** fun_36e0(void** rdi, void** rsi);

signed char gl_scratch_buffer_grow(void** rdi, void** rsi);

void** hash_initialize(int64_t rdi, ...);

signed char seen_file(void** rdi, void** rsi);

void fun_3a20(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_3900(void** rdi, void** rsi, void** rdx, ...);

void record_file(void** rdi, void** rsi);

int32_t fun_36c0(int64_t rdi, void** rsi);

uint16_t dir_suffix = 47;

void** gl_scratch_buffer_dupfree(void** rdi);

void hash_free(void** rdi, void** rsi, ...);

uint64_t fun_3820();

void** fun_3890(void** rdi);

void** canonicalize_filename_mode_stk(void** rdi, uint32_t esi, void** rdx) {
    int64_t r14_4;
    void*** rsp5;
    int64_t rax6;
    int64_t v7;
    void** rbp8;
    void** rax9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** rbx13;
    void** r12_14;
    void** v15;
    void** rsi16;
    void** v17;
    void** rax18;
    void** v19;
    void** v20;
    void** rax21;
    int1_t zf22;
    void** r13_23;
    void** v24;
    void** r15_25;
    uint32_t eax26;
    void** v27;
    uint32_t v28;
    signed char v29;
    int32_t v30;
    void** v31;
    void** v32;
    void** rcx33;
    void** v34;
    void** v35;
    void** rcx36;
    void** r13_37;
    signed char al38;
    void** rax39;
    void** rcx40;
    void** v41;
    void** r13_42;
    void** v43;
    void** v44;
    void** rax45;
    signed char al46;
    void** r10_47;
    void** r9_48;
    void** rax49;
    void** rdx50;
    void** rsi51;
    void** rax52;
    void** r9_53;
    void** r8_54;
    void* v55;
    void** v56;
    void** r10_57;
    void** rax58;
    void** rax59;
    void** r11_60;
    void** rdi61;
    int32_t eax62;
    void** r11_63;
    uint32_t eax64;
    void** r10_65;
    void** r9_66;
    void** rax67;
    signed char al68;
    void** v69;
    void** v70;
    void** v71;
    signed char al72;
    void** rax73;
    signed char v74;
    int32_t eax75;
    void** rax76;
    void* rax77;
    uint32_t edx78;
    uint32_t eax79;
    uint64_t rax80;
    void** rax81;
    uint64_t rax82;
    void* rsp83;
    void** rax84;
    signed char al85;
    int64_t rdx86;
    void** rdi87;
    void** rax88;

    *reinterpret_cast<uint32_t*>(&r14_4) = esi & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_4) + 4) = 0;
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x928);
    rax6 = g28;
    v7 = rax6;
    if (static_cast<uint32_t>(r14_4 - 1) & *reinterpret_cast<uint32_t*>(&r14_4) || (rbp8 = rdi, rdi == 0)) {
        rax9 = fun_3630();
        *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax10) = 0;
        *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    } else {
        if (!*reinterpret_cast<void***>(rdi)) {
            rax11 = fun_3630();
            *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(2);
            *reinterpret_cast<int32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
        } else {
            rax12 = reinterpret_cast<void**>(rsp5 + 0x100);
            *reinterpret_cast<uint32_t*>(&rbx13) = esi;
            *reinterpret_cast<void***>(rdx + 8) = reinterpret_cast<void**>(0x400);
            r12_14 = rdx;
            v15 = rax12;
            *reinterpret_cast<int32_t*>(&rsi16) = 0x400;
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            v17 = rax12;
            rax18 = reinterpret_cast<void**>(rsp5 + 0x510);
            v19 = rax18;
            v20 = rax18;
            rax21 = rdx + 16;
            *reinterpret_cast<void***>(rdx) = rax21;
            zf22 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 47);
            r13_23 = rax21;
            v24 = rax21;
            if (zf22) {
                while (1) {
                    *reinterpret_cast<void***>(rdx + 16) = reinterpret_cast<void**>(47);
                    r15_25 = v24;
                    r13_23 = rdx + 17;
                    addr_6e5d_7:
                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                    if (!*reinterpret_cast<signed char*>(&eax26)) {
                        if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25 + 1) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47)) {
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        } else {
                            --r13_23;
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        }
                    } else {
                        v27 = reinterpret_cast<void**>(0);
                        v28 = *reinterpret_cast<uint32_t*>(&rbx13) & 4;
                        v29 = 0;
                        v30 = 0;
                        v31 = reinterpret_cast<void**>(rsp5 + 0x500);
                        while (1) {
                            if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                v32 = rbp8;
                                *reinterpret_cast<uint32_t*>(&rdx) = eax26;
                                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            } else {
                                do {
                                    *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                    ++rbp8;
                                } while (*reinterpret_cast<signed char*>(&rdx) == 47);
                                if (!*reinterpret_cast<signed char*>(&rdx)) 
                                    goto addr_6f40_17;
                                v32 = rbp8;
                            }
                            do {
                                rbx13 = rbp8;
                                eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                ++rbp8;
                                if (!*reinterpret_cast<signed char*>(&eax26)) 
                                    break;
                            } while (*reinterpret_cast<signed char*>(&eax26) != 47);
                            rcx33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v32));
                            v34 = rcx33;
                            if (!rcx33) 
                                goto addr_6f40_17;
                            if (rcx33 != 1) {
                                if (!reinterpret_cast<int1_t>(v34 == 2)) 
                                    goto addr_6dd9_24;
                                if (*reinterpret_cast<signed char*>(&rdx) != 46) 
                                    goto addr_6dd9_24;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v32 + 1) == 46)) 
                                    goto addr_6dd9_24;
                                rdx = r15_25 + 1;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(rdx)) 
                                    goto addr_6f38_28;
                                --r13_23;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25)) 
                                    goto addr_6f38_28;
                                do {
                                    if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47) 
                                        goto addr_6f38_28;
                                    --r13_23;
                                } while (r13_23 != r15_25);
                                goto addr_6f38_28;
                            }
                            if (*reinterpret_cast<signed char*>(&rdx) == 46) {
                                addr_6f38_28:
                                if (*reinterpret_cast<signed char*>(&eax26)) 
                                    continue; else 
                                    goto addr_6f40_17;
                            } else {
                                addr_6dd9_24:
                                if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) != 47) {
                                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(47);
                                    ++r13_23;
                                }
                            }
                            rsi16 = v34 + 2;
                            if (reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_14 + 8)) + reinterpret_cast<unsigned char>(r15_25)) - reinterpret_cast<unsigned char>(r13_23)) < reinterpret_cast<unsigned char>(rsi16)) {
                                v35 = rbx13;
                                rcx36 = r13_23;
                                rbx13 = rsi16;
                                r13_37 = r12_14;
                                r12_14 = rbp8;
                                do {
                                    rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx36) - reinterpret_cast<unsigned char>(r15_25));
                                    al38 = gl_scratch_buffer_grow_preserve(r13_37, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (!al38) 
                                        goto addr_6e4a_38;
                                    r15_25 = *reinterpret_cast<void***>(r13_37);
                                    rcx36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_25) + reinterpret_cast<unsigned char>(rbp8));
                                } while (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_37 + 8)) - reinterpret_cast<unsigned char>(rbp8)) < reinterpret_cast<unsigned char>(rbx13));
                                rbx13 = v35;
                                rbp8 = r12_14;
                                r12_14 = r13_37;
                                r13_23 = rcx36;
                            }
                            rdx = v34;
                            rsi16 = v32;
                            rax39 = fun_3a10(r13_23, rsi16, rdx);
                            rsp5 = rsp5 - 8 + 8;
                            *reinterpret_cast<uint32_t*>(&rcx40) = v28;
                            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                            *reinterpret_cast<void***>(rax39) = reinterpret_cast<void**>(0);
                            r13_23 = rax39;
                            if (!*reinterpret_cast<uint32_t*>(&rcx40)) {
                                v41 = rax39;
                                r13_42 = v31;
                                v43 = rbx13;
                                v44 = rbp8;
                                do {
                                    rbx13 = v20;
                                    rbp8 = reinterpret_cast<void**>(0x3ff);
                                    rsi16 = rbx13;
                                    rdx = reinterpret_cast<void**>(0x3ff);
                                    rax45 = fun_36e0(r15_25, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (reinterpret_cast<signed char>(0x3ff) > reinterpret_cast<signed char>(rax45)) 
                                        break;
                                    al46 = gl_scratch_buffer_grow(r13_42, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                } while (al46);
                                goto addr_7143_45;
                                r10_47 = rbx13;
                                r13_23 = v41;
                                rbx13 = v43;
                                r9_48 = rax45;
                                rbp8 = v44;
                                if (reinterpret_cast<signed char>(rax45) < reinterpret_cast<signed char>(0)) 
                                    goto addr_6fe2_47;
                            } else {
                                addr_6fe2_47:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) {
                                    addr_7060_48:
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1));
                                    if (*reinterpret_cast<signed char*>(&eax26)) 
                                        continue; else 
                                        goto addr_706c_49;
                                } else {
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                                    if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                        addr_70a0_51:
                                        *reinterpret_cast<uint32_t*>(&rdx) = v28;
                                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                        if (*reinterpret_cast<uint32_t*>(&rdx)) {
                                            if (*reinterpret_cast<signed char*>(&eax26)) 
                                                continue; else 
                                                goto addr_7044_53;
                                        } else {
                                            rax49 = fun_3630();
                                            rsp5 = rsp5 - 8 + 8;
                                            if (*reinterpret_cast<void***>(rax49) == 22) 
                                                goto addr_7060_48; else 
                                                goto addr_70b2_55;
                                        }
                                    } else {
                                        rdx50 = rbp8;
                                        while (1) {
                                            rsi51 = rdx50;
                                            *reinterpret_cast<uint32_t*>(&rcx40) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx50 + 1));
                                            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                                            ++rdx50;
                                            if (*reinterpret_cast<signed char*>(&rcx40) == 47) 
                                                continue;
                                            rsi16 = rsi51 + 2;
                                            if (!*reinterpret_cast<signed char*>(&rcx40)) 
                                                goto addr_7148_59;
                                            if (*reinterpret_cast<signed char*>(&rcx40) != 46) 
                                                goto addr_70a0_51;
                                            *reinterpret_cast<uint32_t*>(&rcx40) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx50 + 1));
                                            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                                            if (!*reinterpret_cast<signed char*>(&rcx40)) 
                                                goto addr_7148_59;
                                            if (*reinterpret_cast<signed char*>(&rcx40) == 46) 
                                                goto addr_7084_63;
                                            if (*reinterpret_cast<signed char*>(&rcx40) != 47) 
                                                goto addr_70a0_51;
                                            rdx50 = rsi16;
                                        }
                                    }
                                }
                            }
                            if (v30 <= 19) {
                                ++v30;
                                goto addr_725c_68;
                            }
                            if (!*reinterpret_cast<void***>(v32)) {
                                addr_725c_68:
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_47) + reinterpret_cast<unsigned char>(r9_48)) = 0;
                                if (!v29) {
                                    rax52 = fun_3750(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_53 = r9_48;
                                    r8_54 = v17;
                                    v55 = reinterpret_cast<void*>(0);
                                    v56 = rax52;
                                    r10_57 = r10_47;
                                    rax58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax52) + reinterpret_cast<unsigned char>(r9_53));
                                    if (reinterpret_cast<unsigned char>(rax58) < reinterpret_cast<unsigned char>(0x400)) 
                                        goto addr_7382_71;
                                } else {
                                    v55 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v17));
                                    rax59 = fun_3750(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_53 = r9_48;
                                    r8_54 = v17;
                                    v56 = rax59;
                                    r10_57 = r10_47;
                                    rax58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax59) + reinterpret_cast<unsigned char>(r9_53));
                                    if (reinterpret_cast<unsigned char>(0x400) > reinterpret_cast<unsigned char>(rax58)) 
                                        goto addr_7427_73;
                                }
                            } else {
                                r11_60 = reinterpret_cast<void**>(rsp5 + 96);
                                rdi61 = reinterpret_cast<void**>(".");
                                rsi16 = r11_60;
                                rdx = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v32) - reinterpret_cast<unsigned char>(rbp8)) + reinterpret_cast<unsigned char>(r13_23));
                                *reinterpret_cast<void***>(rdx) = reinterpret_cast<void**>(0);
                                if (*reinterpret_cast<void***>(r15_25)) {
                                    rdi61 = r15_25;
                                }
                                eax62 = fun_38f0(rdi61, rsi16, rdi61, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (eax62) 
                                    goto addr_70d8_77;
                                r11_63 = r11_60;
                                eax64 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v32));
                                r10_65 = r10_47;
                                r9_66 = r9_48;
                                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax64);
                                if (v27) 
                                    goto addr_71fe_79;
                                r8_54 = reinterpret_cast<void**>(0x8b40);
                                rcx40 = reinterpret_cast<void**>(0x8b00);
                                *reinterpret_cast<int32_t*>(&rsi16) = 0;
                                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                                rdx = reinterpret_cast<void**>(0x8ad0);
                                rax67 = hash_initialize(7, 7);
                                rsp5 = rsp5 - 8 + 8;
                                r10_65 = r10_65;
                                r9_66 = r9_66;
                                v27 = rax67;
                                r11_63 = r11_63;
                                if (!rax67) 
                                    goto addr_7481_81;
                                addr_71fe_79:
                                rsi16 = v32;
                                rdx = r11_63;
                                al68 = seen_file(v27, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (al68) 
                                    goto addr_731c_82; else 
                                    goto addr_7236_83;
                            }
                            v69 = rbp8;
                            v70 = r10_57;
                            rbp8 = reinterpret_cast<void**>(rsp5 + 0xf0);
                            rbx13 = rax58;
                            v71 = r9_53;
                            do {
                                al72 = gl_scratch_buffer_grow_preserve(rbp8, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (!al72) 
                                    goto addr_72fc_86;
                                r8_54 = v17;
                            } while (reinterpret_cast<unsigned char>(0x400) <= reinterpret_cast<unsigned char>(rbx13));
                            r10_57 = v70;
                            r9_53 = v71;
                            rbp8 = v69;
                            if (!v29) {
                                addr_7382_71:
                                fun_3a20(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<unsigned char>(r9_53), rbp8, v56 + 1, rcx40);
                                rsi16 = r10_57;
                                rax73 = fun_3900(r8_54, rsi16, r9_53, r8_54, rsi16, r9_53);
                                rsp5 = rsp5 - 8 + 8 - 8 + 8;
                                rdx = r15_25 + 1;
                                rbp8 = rax73;
                                if (v74 == 47) {
                                    *reinterpret_cast<void***>(r15_25) = reinterpret_cast<void**>(47);
                                    r13_23 = rdx;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax73));
                                    v29 = 1;
                                    goto addr_6f38_28;
                                } else {
                                    v29 = 1;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax73));
                                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(rdx)) {
                                        do {
                                            --r13_23;
                                            if (r13_23 == r15_25) 
                                                break;
                                        } while (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47));
                                        v29 = 1;
                                        goto addr_6f38_28;
                                    }
                                }
                            } else {
                                addr_7427_73:
                                rbp8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v55) + reinterpret_cast<unsigned char>(r8_54));
                                goto addr_7382_71;
                            }
                            addr_731c_82:
                            if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) 
                                goto addr_7060_48; else 
                                goto addr_7326_94;
                            addr_7236_83:
                            rsi16 = v32;
                            rdx = r11_63;
                            record_file(v27, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            r10_47 = r10_65;
                            r9_48 = r9_66;
                            goto addr_725c_68;
                            addr_7044_53:
                            *reinterpret_cast<uint32_t*>(&rdx) = 0;
                            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rcx40) = 0x200;
                            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                            rsi16 = r15_25;
                            eax75 = fun_36c0(0xffffff9c, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            if (eax75) {
                                addr_70b2_55:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) != 1) 
                                    goto addr_70d8_77;
                                rax76 = fun_3630();
                                rsp5 = rsp5 - 8 + 8;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax76) == 2)) 
                                    goto addr_70d8_77;
                                rsi16 = reinterpret_cast<void**>("/");
                                rax77 = fun_3850(rbp8, "/", rdx, rcx40, r8_54);
                                rsp5 = rsp5 - 8 + 8;
                                if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<uint64_t>(rax77))) 
                                    goto addr_7060_48; else 
                                    goto addr_70d8_77;
                            } else {
                                goto addr_7060_48;
                            }
                            addr_7084_63:
                            edx78 = *reinterpret_cast<unsigned char*>(rdx50 + 2);
                            if (!*reinterpret_cast<signed char*>(&edx78) || *reinterpret_cast<signed char*>(&edx78) == 47) {
                                addr_7148_59:
                                eax79 = dir_suffix;
                                *reinterpret_cast<void***>(r13_23) = *reinterpret_cast<void***>(&eax79);
                                goto addr_7044_53;
                            } else {
                                goto addr_70a0_51;
                            }
                        }
                    }
                    addr_6d2a_99:
                    if (v17 != v15) {
                        fun_35f0(v17, v17);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (v20 != v19) {
                        fun_35f0(v20, v20);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx13)) 
                        goto addr_6f70_104;
                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(0);
                    rsi16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23 + 1) - reinterpret_cast<unsigned char>(r15_25));
                    rax10 = gl_scratch_buffer_dupfree(r12_14);
                    rsp5 = rsp5 - 8 + 8;
                    if (rax10) 
                        break;
                    addr_6e4a_38:
                    xalloc_die();
                    rsp5 = rsp5 - 8 + 8;
                    continue;
                    addr_6f40_17:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(r15_25 + 1)) {
                        *reinterpret_cast<int32_t*>(&rax80) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax80) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax80) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47);
                        r13_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23) - rax80);
                    }
                    addr_6f58_107:
                    if (v27) {
                        hash_free(v27, rsi16, v27, rsi16);
                        rsp5 = rsp5 - 8 + 8;
                        goto addr_6d2a_99;
                    }
                    addr_7143_45:
                    goto addr_6e4a_38;
                    addr_706c_49:
                    goto addr_6f40_17;
                    addr_70d8_77:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
                    goto addr_6f58_107;
                    addr_72fc_86:
                    goto addr_6e4a_38;
                    addr_7481_81:
                    goto addr_6e4a_38;
                    addr_7326_94:
                    rax81 = fun_3630();
                    rsp5 = rsp5 - 8 + 8;
                    *reinterpret_cast<void***>(rax81) = reinterpret_cast<void**>(40);
                    goto addr_70d8_77;
                }
            } else {
                while (rax82 = fun_3820(), rsp83 = reinterpret_cast<void*>(rsp5 - 8 + 8), !rax82) {
                    rax84 = fun_3630();
                    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
                    if (*reinterpret_cast<void***>(rax84) == 12) 
                        goto addr_6e4a_38;
                    if (*reinterpret_cast<void***>(rax84) != 34) 
                        goto addr_6d22_112;
                    al85 = gl_scratch_buffer_grow(r12_14, rsi16);
                    rsp5 = rsp5 - 8 + 8;
                    if (!al85) 
                        goto addr_6e4a_38;
                    r13_23 = *reinterpret_cast<void***>(r12_14);
                    rsi16 = *reinterpret_cast<void***>(r12_14 + 8);
                }
                goto addr_70e2_115;
            }
        }
    }
    addr_6d76_116:
    rdx86 = v7 - g28;
    if (rdx86) {
        fun_3780();
    } else {
        return rax10;
    }
    addr_6f70_104:
    rdi87 = *reinterpret_cast<void***>(r12_14);
    *reinterpret_cast<int32_t*>(&rax10) = 0;
    *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    if (v24 != rdi87) {
        fun_35f0(rdi87, rdi87);
        rax10 = reinterpret_cast<void**>(0);
        goto addr_6d76_116;
    }
    addr_70e2_115:
    *reinterpret_cast<int32_t*>(&rsi16) = 0;
    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
    r15_25 = r13_23;
    rax88 = fun_3890(r13_23);
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
    r13_23 = rax88;
    goto addr_6e5d_7;
    addr_6d22_112:
    r15_25 = r13_23;
    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
    goto addr_6d2a_99;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0x130f0);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0x130f0);
    if (*reinterpret_cast<void***>(rdi + 40) != 0x130f0) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0xb7b8]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0x130f0);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0xb61f]");
        if (1) 
            goto addr_7bdc_6;
        __asm__("comiss xmm1, [rip+0xb616]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0xb608]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_7bbc_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_7bb2_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_7bbc_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_7bb2_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_7bb2_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_7bbc_13:
        return 0;
    } else {
        addr_7bdc_6:
        return r8_3;
    }
}

struct s8 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s7 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    signed char[8] pad40;
    struct s8* f28;
    int64_t f30;
    signed char[16] pad72;
    void** f48;
};

struct s9 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s10 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_3950(void** rdi, void** rsi, ...);

int64_t fun_3620();

int32_t transfer_entries(struct s7* rdi, struct s7* rsi, int32_t edx) {
    struct s7* r14_4;
    int32_t r12d5;
    struct s7* rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rax12;
    struct s9* rax13;
    void** rdx14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s10* r13_18;
    void** rax19;
    void** rdx20;

    r14_4 = rdi;
    r12d5 = edx;
    rbp6 = rsi;
    rbx7 = rsi->f0;
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rsi->f8)) {
        do {
            addr_7c46_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_7c38_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(rbp6->f8) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_7c46_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = r14_4->f10;
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rax12 = reinterpret_cast<void**>(r14_4->f30(r15_11, rsi10)), rsi10 = r14_4->f10, reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax13 = reinterpret_cast<struct s9*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
                        rdx14 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax13->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax13->f8;
                            rax13->f8 = r13_9;
                            if (!rdx14) 
                                goto addr_7cbe_9;
                        } else {
                            rax13->f0 = r15_11;
                            rax15 = r14_4->f48;
                            r14_4->f18 = r14_4->f18 + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            r14_4->f48 = r13_9;
                            if (!rdx14) 
                                goto addr_7cbe_9;
                        }
                        r13_9 = rdx14;
                    }
                    goto addr_3b65_12;
                    addr_7cbe_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_7c38_3;
            }
            rsi16 = r14_4->f10;
            rax17 = reinterpret_cast<void**>(r14_4->f30(r15_8, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(r14_4->f10)) 
                goto addr_3b65_12;
            r13_18 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
            if (r13_18->f0) 
                goto addr_7cf8_16;
            r13_18->f0 = r15_8;
            r14_4->f18 = r14_4->f18 + 1;
            continue;
            addr_7cf8_16:
            rax19 = r14_4->f48;
            if (!rax19) {
                rax19 = fun_3950(16, rsi16);
                if (!rax19) 
                    goto addr_7d6a_19;
            } else {
                r14_4->f48 = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx20 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx20;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            rbp6->f18 = rbp6->f18 - 1;
        } while (reinterpret_cast<unsigned char>(rbp6->f8) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_3b65_12:
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    fun_3620();
    addr_7d6a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void*** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *rdx = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_7a6f_25:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_7a14_28;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_7a80_32;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_7a80_32;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_7a6f_25;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_7a14_28;
            }
        }
    }
    addr_7a71_36:
    return rax11;
    addr_7a14_28:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_7a71_36;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_7a80_32:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

void unescape_tab(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rcx7;
    void** rdx8;
    void** rax9;
    uint32_t r8d10;
    void** rsi11;
    void** r9_12;
    int64_t rdi13;
    int64_t r11_14;
    int64_t r10_15;
    int64_t r11_16;
    int64_t rdx17;
    int32_t edx18;

    rbx5 = rdi;
    rax6 = fun_3750(rdi);
    rcx7 = rbx5;
    *reinterpret_cast<int32_t*>(&rdx8) = 0;
    *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
    rax9 = rax6 + 1;
    while (1) {
        r8d10 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rdx8));
        rsi11 = rdx8 + 1;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&r8d10) == 92) || ((r9_12 = rdx8 + 4, reinterpret_cast<unsigned char>(r9_12) >= reinterpret_cast<unsigned char>(rax9)) || ((*reinterpret_cast<uint32_t*>(&rdi13) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rsi11)) - 48, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0, *reinterpret_cast<unsigned char*>(&rdi13) > 3) || ((*reinterpret_cast<uint32_t*>(&r11_14) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rdx8) + 2), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_14) + 4) = 0, *reinterpret_cast<int32_t*>(&r10_15) = static_cast<int32_t>(r11_14 - 48), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_15) + 4) = 0, *reinterpret_cast<unsigned char*>(&r10_15) > 7) || (*reinterpret_cast<uint32_t*>(&r11_16) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rdx8) + 3) - 48, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_16) + 4) = 0, *reinterpret_cast<unsigned char*>(&r11_16) > 7))))) {
            *reinterpret_cast<void***>(rcx7) = *reinterpret_cast<void***>(&r8d10);
            ++rcx7;
            if (reinterpret_cast<unsigned char>(rax9) <= reinterpret_cast<unsigned char>(rsi11)) 
                break;
        } else {
            *reinterpret_cast<int32_t*>(&rdx17) = static_cast<int32_t>(r10_15 + rdi13 * 8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
            ++rcx7;
            rsi11 = r9_12;
            edx18 = static_cast<int32_t>(r11_16 + rdx17 * 8);
            *reinterpret_cast<void***>(rcx7 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&edx18);
        }
        rdx8 = rsi11;
    }
    return;
}

uint64_t fun_3740();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_3740();
    if (r8d > 10) {
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x13300 + rax11 * 4) + 0x13300;
    }
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0xd0);

uint32_t nslots = 1;

void** xpalloc();

void fun_3810();

struct s12 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s11* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s12* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0xc26f;
    rax8 = fun_3630();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
        fun_3620();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x182d0) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xbec1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0xc2fb;
            fun_3810();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s12*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x18440) {
                fun_35f0(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0xc38a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = rax6 - g28;
        if (rax29) {
            fun_3780();
        } else {
            return r14_19;
        }
    }
}

/* cdb_free.part.0 */
void cdb_free_part_0(int64_t rdi, void** rsi, void** rdx, void** rcx) {
    fun_3800("! close_fail", "lib/chdir-long.c", "! close_fail", "lib/chdir-long.c");
}

void alloc_table_row(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rdi8;
    void** rax9;
    void** rdx10;
    uint64_t rdi11;
    void** rax12;

    rax7 = nrows;
    rdi8 = table;
    nrows = rax7 + 1;
    rax9 = xreallocarray(rdi8);
    rdx10 = nrows;
    rdi11 = ncolumns;
    table = rax9;
    rax12 = xnmalloc(rdi11, 8, rdx10, rcx, r8, r9);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rax9 + reinterpret_cast<unsigned char>(rdx10) * 8) - 8) = rax12;
    return;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x182e8;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s13 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s13* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s13* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x132a7);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x132a4);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x132ab);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x132a0);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_3003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g17cf8 = 0;

void fun_3033() {
    __asm__("cli ");
    goto g17cf8;
}

void fun_3043() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3053() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3063() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3073() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3083() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3093() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3103() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3113() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3123() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3133() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3143() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3153() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3163() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3173() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3183() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3193() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3203() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3213() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3223() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3233() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3243() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3253() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3263() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3273() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3283() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3293() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3303() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3313() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3323() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3333() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3343() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3353() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3363() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3373() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3383() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3393() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3403() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3413() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3423() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3433() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3443() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3453() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3463() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3473() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3483() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3493() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3503() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3513() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3523() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3533() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3543() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3553() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3563() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3573() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3583() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3593() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35b3() {
    __asm__("cli ");
    goto 0x3020;
}

int64_t __cxa_finalize = 0;

void fun_35c3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x3030;

void fun_35d3() {
    __asm__("cli ");
    goto getenv;
}

int64_t __snprintf_chk = 0x3040;

void fun_35e3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x3050;

void fun_35f3() {
    __asm__("cli ");
    goto free;
}

int64_t endmntent = 0x3060;

void fun_3603() {
    __asm__("cli ");
    goto endmntent;
}

int64_t strverscmp = 0x3070;

void fun_3613() {
    __asm__("cli ");
    goto strverscmp;
}

int64_t abort = 0x3080;

void fun_3623() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x3090;

void fun_3633() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x30a0;

void fun_3643() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x30b0;

void fun_3653() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x30c0;

void fun_3663() {
    __asm__("cli ");
    goto __fpending;
}

int64_t isatty = 0x30d0;

void fun_3673() {
    __asm__("cli ");
    goto isatty;
}

int64_t iswcntrl = 0x30e0;

void fun_3683() {
    __asm__("cli ");
    goto iswcntrl;
}

int64_t reallocarray = 0x30f0;

void fun_3693() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t wcswidth = 0x3100;

void fun_36a3() {
    __asm__("cli ");
    goto wcswidth;
}

int64_t localeconv = 0x3110;

void fun_36b3() {
    __asm__("cli ");
    goto localeconv;
}

int64_t faccessat = 0x3120;

void fun_36c3() {
    __asm__("cli ");
    goto faccessat;
}

int64_t mbstowcs = 0x3130;

void fun_36d3() {
    __asm__("cli ");
    goto mbstowcs;
}

int64_t readlink = 0x3140;

void fun_36e3() {
    __asm__("cli ");
    goto readlink;
}

int64_t fcntl = 0x3150;

void fun_36f3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x3160;

void fun_3703() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x3170;

void fun_3713() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x3180;

void fun_3723() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x3190;

void fun_3733() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x31a0;

void fun_3743() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x31b0;

void fun_3753() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x31c0;

void fun_3763() {
    __asm__("cli ");
    goto openat;
}

int64_t chdir = 0x31d0;

void fun_3773() {
    __asm__("cli ");
    goto chdir;
}

int64_t __stack_chk_fail = 0x31e0;

void fun_3783() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x31f0;

void fun_3793() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x3200;

void fun_37a3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x3210;

void fun_37b3() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x3220;

void fun_37c3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x3230;

void fun_37d3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t uname = 0x3240;

void fun_37e3() {
    __asm__("cli ");
    goto uname;
}

int64_t lseek = 0x3250;

void fun_37f3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x3260;

void fun_3803() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x3270;

void fun_3813() {
    __asm__("cli ");
    goto memset;
}

int64_t getcwd = 0x3280;

void fun_3823() {
    __asm__("cli ");
    goto getcwd;
}

int64_t canonicalize_file_name = 0x3290;

void fun_3833() {
    __asm__("cli ");
    goto canonicalize_file_name;
}

int64_t close = 0x32a0;

void fun_3843() {
    __asm__("cli ");
    goto close;
}

int64_t strspn = 0x32b0;

void fun_3853() {
    __asm__("cli ");
    goto strspn;
}

int64_t memchr = 0x32c0;

void fun_3863() {
    __asm__("cli ");
    goto memchr;
}

int64_t memcmp = 0x32d0;

void fun_3873() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x32e0;

void fun_3883() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x32f0;

void fun_3893() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x3300;

void fun_38a3() {
    __asm__("cli ");
    goto calloc;
}

int64_t __getdelim = 0x3310;

void fun_38b3() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x3320;

void fun_38c3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x3330;

void fun_38d3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t __memcpy_chk = 0x3340;

void fun_38e3() {
    __asm__("cli ");
    goto __memcpy_chk;
}

int64_t stat = 0x3350;

void fun_38f3() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x3360;

void fun_3903() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x3370;

void fun_3913() {
    __asm__("cli ");
    goto fileno;
}

int64_t statfs = 0x3380;

void fun_3923() {
    __asm__("cli ");
    goto statfs;
}

int64_t sync = 0x3390;

void fun_3933() {
    __asm__("cli ");
    goto sync;
}

int64_t wcwidth = 0x33a0;

void fun_3943() {
    __asm__("cli ");
    goto wcwidth;
}

int64_t malloc = 0x33b0;

void fun_3953() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x33c0;

void fun_3963() {
    __asm__("cli ");
    goto fflush;
}

int64_t getmntent = 0x33d0;

void fun_3973() {
    __asm__("cli ");
    goto getmntent;
}

int64_t setmntent = 0x33e0;

void fun_3983() {
    __asm__("cli ");
    goto setmntent;
}

int64_t nl_langinfo = 0x33f0;

void fun_3993() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __isoc99_sscanf = 0x3400;

void fun_39a3() {
    __asm__("cli ");
    goto __isoc99_sscanf;
}

int64_t __freading = 0x3410;

void fun_39b3() {
    __asm__("cli ");
    goto __freading;
}

int64_t statvfs = 0x3420;

void fun_39c3() {
    __asm__("cli ");
    goto statvfs;
}

int64_t fchdir = 0x3430;

void fun_39d3() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x3440;

void fun_39e3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x3450;

void fun_39f3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x3460;

void fun_3a03() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x3470;

void fun_3a13() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x3480;

void fun_3a23() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x3490;

void fun_3a33() {
    __asm__("cli ");
    goto error;
}

int64_t memrchr = 0x34a0;

void fun_3a43() {
    __asm__("cli ");
    goto memrchr;
}

int64_t open = 0x34b0;

void fun_3a53() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x34c0;

void fun_3a63() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x34d0;

void fun_3a73() {
    __asm__("cli ");
    goto fopen;
}

int64_t strtoumax = 0x34e0;

void fun_3a83() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x34f0;

void fun_3a93() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t wcstombs = 0x3500;

void fun_3aa3() {
    __asm__("cli ");
    goto wcstombs;
}

int64_t exit = 0x3510;

void fun_3ab3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x3520;

void fun_3ac3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x3530;

void fun_3ad3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t strdup = 0x3540;

void fun_3ae3() {
    __asm__("cli ");
    goto strdup;
}

int64_t mbsinit = 0x3550;

void fun_3af3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x3560;

void fun_3b03() {
    __asm__("cli ");
    goto iswprint;
}

int64_t hasmntopt = 0x3570;

void fun_3b13() {
    __asm__("cli ");
    goto hasmntopt;
}

int64_t fstat = 0x3580;

void fun_3b23() {
    __asm__("cli ");
    goto fstat;
}

int64_t strstr = 0x3590;

void fun_3b33() {
    __asm__("cli ");
    goto strstr;
}

int64_t __ctype_b_loc = 0x35a0;

void fun_3b43() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x35b0;

void fun_3b53() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_39f0(int64_t rdi, ...);

void fun_3720(int64_t rdi, int64_t rsi);

void fun_3700(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

signed char print_type = 0;

int64_t grand_fsu = 0;

int32_t fun_3790(int64_t rdi, void** rsi, void** rdx);

void** stdout = reinterpret_cast<void**>(0);

void** Version = reinterpret_cast<void**>(86);

void version_etc(void** rdi, void** rsi, void** rdx);

int32_t fun_3ab0();

void** fun_35d0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t human_options(void** rdi, void** rsi, void** rdx);

int32_t optind = 0;

int32_t fun_3a50(void** rdi, void** rsi, void** rdx);

void** fun_3b20(int64_t rdi, void** rsi, void** rdx);

int32_t fun_3840();

signed char g1807c = 0;

void** read_file_system_list(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** mount_list = reinterpret_cast<void**>(0);

signed char require_sync = 0;

void fun_3930(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

signed char g1804c = 0;

void fun_37c0();

void** ambsalign(void** rdi, void** rsi, void** rdx);

void fun_3880(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void** a8, void** a9, ...);

void** find_mount_point(void** rdi, void** rsi, void** rdx);

void** fun_3830(void** rdi, void** rsi, void** rdx, ...);

int32_t fun_3640(void** rdi, void** rsi, void** rdx, ...);

void* quotearg_style(int64_t rdi, void** rsi, void** rdx);

int64_t fun_3bd3(int32_t edi, void** rsi) {
    void** rbp3;
    void** v4;
    int64_t rax5;
    int64_t v6;
    void** rdi7;
    void* rsp8;
    void** v9;
    void** r8_10;
    void** rsi11;
    void** rcx12;
    void** rdx13;
    int64_t rdi14;
    int32_t eax15;
    void* rsp16;
    void** rdi17;
    void** r9_18;
    int64_t rax19;
    int1_t zf20;
    void** rax21;
    void** rax22;
    uint64_t rax23;
    int64_t rax24;
    uint32_t eax25;
    int1_t zf26;
    void** rbx27;
    int32_t eax28;
    void** rax29;
    int64_t rbx30;
    void** r12_31;
    void** v32;
    void** r15_33;
    int32_t eax34;
    void** r14_35;
    int32_t eax36;
    int64_t rdi37;
    void** rax38;
    void** r13d39;
    int32_t eax40;
    void** eax41;
    void** rax42;
    void** rax43;
    int32_t r12d44;
    void** r14_45;
    void** r15_46;
    int64_t rax47;
    void** rax48;
    void** rax49;
    int1_t zf50;
    int64_t rdi51;
    int1_t zf52;
    int1_t zf53;
    int1_t zf54;
    void** rax55;
    int1_t less56;
    uint32_t r13d57;
    void** r12_58;
    int1_t zf59;
    int1_t zf60;
    int1_t zf61;
    int1_t zf62;
    void** rax63;
    void** rax64;
    void** rax65;
    int1_t zf66;
    int1_t below_or_equal67;
    int64_t rax68;
    int64_t rax69;
    void* rsp70;
    void** rax71;
    void** rdx72;
    void** rax73;
    int1_t zf74;
    int1_t zf75;
    void** rsi76;
    int64_t v77;
    int64_t v78;
    void** r12_79;
    int1_t cf80;
    void* r13_81;
    uint64_t rbx82;
    int1_t cf83;
    void** rax84;
    void* r14_85;
    void** rbp86;
    void** rdi87;
    void** rax88;
    uint64_t rax89;
    uint32_t ecx90;
    void** rax91;
    void** rax92;
    void** rax93;
    void** rsi94;
    void** rdi95;
    void** rax96;
    int64_t v97;
    int64_t v98;
    void** rdi99;
    void** rax100;
    void* rsp101;
    void* rsp102;
    void** rsi103;
    void** v104;
    void** v105;
    void** rax106;
    int64_t v107;
    int64_t v108;
    void** v109;
    void** v110;
    int32_t eax111;
    void** rax112;
    void*** v113;
    void** eax114;
    void** rax115;
    void* rsp116;
    void** r15_117;
    void** rdi118;
    void** rax119;
    void** v120;
    void** r13_121;
    void** r12_122;
    void** rbx123;
    void** r14_124;
    void** rdi125;
    int64_t rax126;
    void** r15_127;
    void** rax128;
    int32_t eax129;
    int1_t zf130;
    void** rsi131;
    int32_t eax132;
    void** v133;
    void** rax134;
    void* rsp135;
    void** v136;
    void** v137;
    void** rbx138;
    void** r12_139;
    void** rax140;
    int32_t eax141;
    void** rax142;
    void** rax143;
    void** v144;
    int64_t rax145;
    int32_t eax146;
    void** v147;
    uint32_t r9d148;
    uint32_t eax149;
    void** rdi150;
    int64_t rax151;
    int64_t v152;
    int64_t v153;
    int64_t v154;
    void** v155;
    uint32_t r15d156;
    void** v157;
    unsigned char v158;
    void** r12_159;
    void** rax160;
    int64_t rax161;
    void* rsp162;
    void** r14_163;
    void** r13_164;
    void** rax165;
    void** r15d166;
    int32_t eax167;
    void** r15_168;
    void** rdi169;
    int64_t rax170;
    void** rdi171;
    void** rax172;
    void* rsp173;
    void** rdi174;
    void*** v175;
    void** rax176;
    int64_t rax177;
    uint32_t r9d178;
    uint32_t eax179;
    void** rdi180;
    int64_t rax181;
    int64_t v182;
    void** r10_183;
    int64_t v184;
    void** rax185;
    int1_t zf186;
    int1_t zf187;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp3) = edi;
    *reinterpret_cast<int32_t*>(&rbp3 + 4) = 0;
    v4 = rsi;
    rax5 = g28;
    v6 = rax5;
    rdi7 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi7);
    fun_39f0(6, 6);
    fun_3720("coreutils", "/usr/local/share/locale");
    fun_3700("coreutils", "/usr/local/share/locale");
    atexit(0x7510, "/usr/local/share/locale");
    fs_select_list = reinterpret_cast<void**>(0);
    fs_exclude_list = reinterpret_cast<void**>(0);
    show_all_fs = 0;
    show_listed_fs = 0;
    human_output_opts = 0xffffffff;
    print_type = 0;
    file_systems_processed = 0;
    exit_status = 0;
    print_grand_total = 0;
    grand_fsu = 1;
    fun_3730();
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x138 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x70);
    r8_10 = v9;
    rsi11 = v4;
    rcx12 = reinterpret_cast<void**>(0x17860);
    rdx13 = reinterpret_cast<void**>("aB:iF:hHklmPTt:vx:");
    *reinterpret_cast<int32_t*>(&rdi14) = *reinterpret_cast<int32_t*>(&rbp3);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    eax15 = fun_3790(rdi14, rsi11, "aB:iF:hHklmPTt:vx:");
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
    if (eax15 != -1) {
        if (eax15 > 0x83) {
            addr_4287_4:
            usage();
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
            goto addr_4291_5;
        } else {
            if (eax15 <= 65) {
                if (eax15 == 0xffffff7d) {
                    rdi17 = stdout;
                    r9_18 = reinterpret_cast<void**>("David MacKenzie");
                    rcx12 = Version;
                    r8_10 = reinterpret_cast<void**>("Torbjorn Granlund");
                    rdx13 = reinterpret_cast<void**>("GNU coreutils");
                    rsi11 = reinterpret_cast<void**>("df");
                    version_etc(rdi17, "df", "GNU coreutils");
                    eax15 = fun_3ab0();
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 - 8 - 8 + 8 - 8 + 8);
                }
                if (eax15 != 0xffffff7e) 
                    goto addr_4287_4;
            } else {
                *reinterpret_cast<uint32_t*>(&rax19) = eax15 - 66;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax19) <= 65) {
                    goto *reinterpret_cast<int32_t*>(0x12308 + rax19 * 4) + 0x12308;
                }
            }
        }
        usage();
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
    }
    zf20 = human_output_opts == 0xffffffff;
    if (zf20) {
        if (1) {
            addr_4291_5:
            rax21 = fun_35d0("DF_BLOCK_SIZE", rsi11, rdx13, rcx12, r8_10, "David MacKenzie");
            rdx13 = reinterpret_cast<void**>(0x18400);
            rsi11 = reinterpret_cast<void**>(0x18408);
            human_options(rax21, 0x18408, 0x18400);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
        } else {
            human_output_opts = 0;
            rax22 = fun_35d0("POSIXLY_CORRECT", rsi11, rdx13, rcx12, r8_10, "David MacKenzie");
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
            rax23 = reinterpret_cast<unsigned char>(rax22) - (reinterpret_cast<unsigned char>(rax22) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax22) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(1)))))));
            *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<uint32_t*>(&rax23) & 0x200;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
            output_block_size = rax24 + 0x200;
        }
    }
    eax25 = header_mode;
    if (eax25 != 1 && eax25 != 4) {
        zf26 = (*reinterpret_cast<unsigned char*>(&human_output_opts) & 16) == 0;
        if (zf26) {
            if (!1) {
                header_mode = 3;
            }
        } else {
            header_mode = 2;
        }
    }
    rbx27 = fs_select_list;
    if (!rbx27) {
        addr_4037_22:
        eax28 = optind;
        if (eax28 < *reinterpret_cast<int32_t*>(&rbp3)) {
            rsi11 = reinterpret_cast<void**>(0x90);
            *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
            rax29 = xnmalloc(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp3) - eax28), 0x90, rdx13, rcx12, r8_10, "David MacKenzie");
            rbx30 = optind;
            r12_31 = v4;
            v32 = rax29;
            while (*reinterpret_cast<int32_t*>(&rbp3) > *reinterpret_cast<int32_t*>(&rbx30)) {
                rcx12 = v32;
                r15_33 = *reinterpret_cast<void***>(r12_31 + rbx30 * 8);
                rsi11 = reinterpret_cast<void**>(0x900);
                *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                eax34 = *reinterpret_cast<int32_t*>(&rbx30) - optind;
                r14_35 = rcx12 + eax34 * 0x90;
                eax36 = fun_3a50(r15_33, 0x900, rdx13);
                *reinterpret_cast<int32_t*>(&rdi37) = eax36;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0;
                if (eax36 < 0) {
                    rax38 = fun_3630();
                    r13d39 = *reinterpret_cast<void***>(rax38);
                    if (r13d39 != 2 && r13d39 != 20) {
                        rsi11 = r14_35;
                        eax40 = fun_38f0(r15_33, rsi11, r15_33, rsi11);
                        if (!eax40) {
                            addr_41c5_28:
                            ++rbx30;
                            continue;
                        } else {
                            rdx13 = rax38;
                            r13d39 = *reinterpret_cast<void***>(rdx13);
                        }
                    }
                } else {
                    rsi11 = r14_35;
                    eax41 = fun_3b20(rdi37, rsi11, rdx13);
                    r13d39 = eax41;
                    if (eax41) {
                        rax42 = fun_3630();
                        r13d39 = *reinterpret_cast<void***>(rax42);
                    }
                    fun_3840();
                }
                if (r13d39) {
                    rax43 = quotearg_n_style_colon();
                    rdx13 = reinterpret_cast<void**>("%s");
                    rsi11 = r13d39;
                    *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                    rcx12 = rax43;
                    fun_3a30();
                    *reinterpret_cast<void***>(r12_31 + rbx30 * 8) = reinterpret_cast<void**>(0);
                    exit_status = 1;
                    goto addr_41c5_28;
                }
            }
        }
    } else {
        r12d44 = 0;
        while (1) {
            r14_45 = fs_exclude_list;
            if (!r14_45) {
                addr_4029_37:
                rbx27 = *reinterpret_cast<void***>(rbx27 + 8);
                if (rbx27) 
                    continue; else 
                    break;
            } else {
                r15_46 = *reinterpret_cast<void***>(rbx27);
                do {
                    rsi11 = *reinterpret_cast<void***>(r14_45);
                    rax47 = fun_38c0(r15_46, rsi11, rdx13, r15_46, rsi11, rdx13);
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    if (!*reinterpret_cast<int32_t*>(&rax47)) 
                        break;
                    r14_45 = *reinterpret_cast<void***>(r14_45 + 8);
                } while (r14_45);
                goto addr_4029_37;
            }
            rax48 = quote(r15_46, rsi11, rdx13, rcx12, r8_10, "David MacKenzie");
            rax49 = fun_3730();
            rcx12 = rax48;
            rsi11 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
            rdx13 = rax49;
            r12d44 = 1;
            fun_3a30();
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_4029_37;
        }
        if (*reinterpret_cast<signed char*>(&r12d44)) 
            goto addr_40a7_43; else 
            goto addr_4037_22;
    }
    zf50 = fs_select_list == 0;
    *reinterpret_cast<uint32_t*>(&rdi51) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi51) + 4) = 0;
    if (zf50 && ((zf52 = fs_exclude_list == 0, zf52) && ((zf53 = print_type == 0, zf53) && (zf54 = g1807c == 0, zf54)))) {
        *reinterpret_cast<uint32_t*>(&rdi51) = show_local_fs;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi51) + 4) = 0;
    }
    rax55 = read_file_system_list(rdi51, rsi11, rdx13, rcx12, r8_10, "David MacKenzie");
    mount_list = rax55;
    if (rax55) 
        goto addr_4076_47;
    while (1) {
        less56 = optind < *reinterpret_cast<int32_t*>(&rbp3);
        r13d57 = 1;
        r12_58 = reinterpret_cast<void**>(0x136c1);
        if (less56 && ((zf59 = show_all_fs == 0, zf59) && ((zf60 = show_local_fs == 0, zf60) && ((zf61 = fs_select_list == 0, zf61) && (zf62 = fs_exclude_list == 0, zf62))))) {
            r13d57 = 0;
            rax63 = fun_3730();
            r12_58 = rax63;
        }
        rax64 = fun_3730();
        rax65 = fun_3630();
        r8_10 = rax64;
        rcx12 = r12_58;
        *reinterpret_cast<uint32_t*>(&rdi51) = r13d57;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi51) + 4) = 0;
        rsi11 = *reinterpret_cast<void***>(rax65);
        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
        rdx13 = reinterpret_cast<void**>("%s%s");
        fun_3a30();
        addr_4076_47:
        zf66 = require_sync == 0;
        if (!zf66) {
            fun_3930(rdi51, rsi11, rdx13, rcx12, r8_10, "David MacKenzie");
        }
        below_or_equal67 = header_mode <= 4;
        if (below_or_equal67) 
            break;
        fun_3800("!\"invalid header_mode\"", "src/df.c", "!\"invalid header_mode\"", "src/df.c");
    }
    *reinterpret_cast<uint32_t*>(&rax68) = header_mode;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax68) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x12410 + rax68 * 4) + 0x12410;
    addr_40a7_43:
    rax69 = v6 - g28;
    if (rax69) {
        fun_3780();
        rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
        while (1) {
            rax71 = fun_3730();
            rdx72 = rax71;
            rax73 = fun_3a30();
            rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8 - 8 + 8);
            while (1) {
                zf74 = print_grand_total == 0;
                if (!zf74) {
                    zf75 = g1804c == 0;
                    rsi76 = reinterpret_cast<void**>("-");
                    if (zf75) {
                        rsi76 = reinterpret_cast<void**>("total");
                    }
                    *reinterpret_cast<uint32_t*>(&r9_18) = 0;
                    *reinterpret_cast<int32_t*>(&r9_18 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r8_10) = 0;
                    *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx12) = 0;
                    *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                    rdx72 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rdx72 + 4) = 0;
                    get_dev("total", rsi76, 0, 0, 0, 0, 0, 0x183a0, 0, rax73, v77, "total", rsi76, 0, 0, 0, 0, 0, 0x183a0, 0, rax73, v78);
                    rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
                }
                *reinterpret_cast<int32_t*>(&r12_79) = 0;
                *reinterpret_cast<int32_t*>(&r12_79 + 4) = 0;
                while (cf80 = reinterpret_cast<unsigned char>(r12_79) < reinterpret_cast<unsigned char>(nrows), cf80) {
                    r13_81 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_79) * 8);
                    *reinterpret_cast<int32_t*>(&rbx82) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx82) + 4) = 0;
                    while (cf83 = rbx82 < ncolumns, cf83) {
                        rax84 = table;
                        r14_85 = reinterpret_cast<void*>(rbx82 * 8);
                        rbp86 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax84) + reinterpret_cast<uint64_t>(r13_81)) + rbx82 * 8);
                        if (rbx82) {
                            rdi87 = stdout;
                            rax88 = *reinterpret_cast<void***>(rdi87 + 40);
                            if (reinterpret_cast<unsigned char>(rax88) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi87 + 48))) {
                                fun_37c0();
                                rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8);
                            } else {
                                *reinterpret_cast<void***>(rdi87 + 40) = rax88 + 1;
                                *reinterpret_cast<void***>(rax88) = reinterpret_cast<void**>(32);
                            }
                        }
                        rax89 = ncolumns;
                        ecx90 = 0;
                        rax91 = columns;
                        *reinterpret_cast<unsigned char*>(&ecx90) = reinterpret_cast<uint1_t>(rax89 - 1 == rbx82);
                        rax92 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax91) + reinterpret_cast<uint64_t>(r14_85));
                        *reinterpret_cast<uint32_t*>(&rcx12) = ecx90 << 3;
                        *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                        rdx72 = *reinterpret_cast<void***>(rax92 + 40);
                        *reinterpret_cast<int32_t*>(&rdx72 + 4) = 0;
                        rax93 = ambsalign(rbp86, v9, rdx72);
                        rsi94 = stdout;
                        rbp3 = rax93;
                        rdi95 = rax93;
                        if (!rax93) {
                            rax96 = table;
                            rdi95 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax96) + reinterpret_cast<uint64_t>(r13_81))) + reinterpret_cast<uint64_t>(r14_85));
                        }
                        fun_3880(rdi95, rsi94, rdx72, rcx12, r8_10, r9_18, v97, v4, v9, rdi95, rsi94, rdx72, rcx12, r8_10, r9_18, v98, v4, v9);
                        ++rbx82;
                        fun_35f0(rbp3, rbp3);
                        rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8 - 8 + 8 - 8 + 8);
                    }
                    rdi99 = stdout;
                    rax100 = *reinterpret_cast<void***>(rdi99 + 40);
                    if (reinterpret_cast<unsigned char>(rax100) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi99 + 48))) {
                        fun_37c0();
                        rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8);
                    } else {
                        rdx72 = rax100 + 1;
                        *reinterpret_cast<void***>(rdi99 + 40) = rdx72;
                        *reinterpret_cast<void***>(rax100) = reinterpret_cast<void**>(10);
                    }
                    ++r12_79;
                }
                do {
                    fun_3ab0();
                    rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8);
                    while (1) {
                        addr_495e_77:
                        fun_35f0(rbp3, rbp3);
                        rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8);
                        while (1) {
                            addr_4966_78:
                            rsi103 = v104;
                            rax106 = find_mount_point(v105, rsi103, rdx72);
                            rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 + 8);
                            rbp3 = rax106;
                            if (rax106) {
                                *reinterpret_cast<uint32_t*>(&r9_18) = 0;
                                *reinterpret_cast<int32_t*>(&r9_18 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&r8_10) = 0;
                                *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
                                rdx72 = v105;
                                rsi103 = rax106;
                                get_dev(0, rsi103, rdx72, 0, 0, 0, 0, 0, 0, rcx12, v107, 0, rsi103, rdx72, 0, 0, 0, 0, 0, 0, rcx12, v108);
                                fun_35f0(rbp3, rbp3);
                                rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
                            }
                            while (rcx12 = v109, rax73 = rcx12, v110 != rcx12) {
                                v109 = rcx12 + 1;
                                rcx12 = *reinterpret_cast<void***>(v4 + reinterpret_cast<unsigned char>(rax73) * 8);
                                v105 = rcx12;
                                if (!rcx12) 
                                    continue;
                                eax111 = *reinterpret_cast<int32_t*>(&rax73) - optind;
                                rax112 = reinterpret_cast<void**>(v113 + eax111 * 0x90);
                                v104 = rax112;
                                eax114 = *reinterpret_cast<void***>(rax112 + 24);
                                if ((reinterpret_cast<unsigned char>(eax114) & 0xb000) != 0x2000) {
                                    addr_44ff_84:
                                    rax115 = fun_3830(v105, rsi103, rdx72, v105, rsi103, rdx72);
                                    rsp116 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8);
                                    r15_117 = mount_list;
                                    rbp3 = rax115;
                                    rdi118 = rax115;
                                    if (!rax115 || *reinterpret_cast<void***>(rax115) != 47) {
                                        fun_35f0(rdi118, rdi118);
                                        rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp116) - 8 + 8);
                                        if (!r15_117) 
                                            goto addr_4966_78;
                                    } else {
                                        rax119 = fun_3750(rdi118, rdi118);
                                        rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp116) - 8 + 8);
                                        if (!r15_117) 
                                            goto addr_495e_77;
                                        v120 = r15_117;
                                        r13_121 = r15_117;
                                        *reinterpret_cast<int32_t*>(&r12_122) = 0;
                                        *reinterpret_cast<int32_t*>(&r12_122 + 4) = 0;
                                        *reinterpret_cast<int32_t*>(&rbx123) = 0;
                                        *reinterpret_cast<int32_t*>(&rbx123 + 4) = 0;
                                        r14_124 = rax119;
                                        do {
                                            rdi125 = *reinterpret_cast<void***>(r13_121 + 24);
                                            rax126 = fun_38c0(rdi125, "lofs", rdx72, rdi125, "lofs", rdx72);
                                            rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8);
                                            if (*reinterpret_cast<int32_t*>(&rax126) && ((!rbx123 || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx123 + 40)) & 1 || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_121 + 40)) & 1))) && ((r15_127 = *reinterpret_cast<void***>(r13_121 + 8), rax128 = fun_3750(r15_127, r15_127), rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8), rdx72 = rax128, reinterpret_cast<unsigned char>(rax128) >= reinterpret_cast<unsigned char>(r12_122)) && reinterpret_cast<unsigned char>(r14_124) >= reinterpret_cast<unsigned char>(rax128)))) {
                                                if (rax128 == 1) {
                                                    *reinterpret_cast<int32_t*>(&r12_122) = 1;
                                                    *reinterpret_cast<int32_t*>(&r12_122 + 4) = 0;
                                                    rbx123 = r13_121;
                                                } else {
                                                    if (r14_124 == rax128 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp3) + reinterpret_cast<unsigned char>(rax128)) == 47) {
                                                        eax129 = fun_3640(r15_127, rbp3, rdx72);
                                                        rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8);
                                                        rdx72 = rdx72;
                                                        zf130 = eax129 == 0;
                                                        if (zf130) {
                                                            r12_122 = rdx72;
                                                        }
                                                        if (zf130) {
                                                            rbx123 = r13_121;
                                                        }
                                                    }
                                                }
                                            }
                                            r13_121 = *reinterpret_cast<void***>(r13_121 + 48);
                                        } while (r13_121);
                                        rdi118 = rbp3;
                                        r15_117 = v120;
                                        fun_35f0(rdi118, rdi118);
                                        rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8);
                                        if (!rbx123) 
                                            goto addr_4532_100;
                                        rdi118 = *reinterpret_cast<void***>(rbx123 + 8);
                                        rsi131 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp102) + 0x90);
                                        eax132 = fun_38f0(rdi118, rsi131, rdi118, rsi131);
                                        rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 + 8);
                                        if (eax132) 
                                            goto addr_4532_100;
                                        if (v133 == *reinterpret_cast<void***>(v104)) 
                                            goto addr_489e_103;
                                    }
                                } else {
                                    rax134 = fun_3830(rcx12, rsi103, rdx72, rcx12, rsi103, rdx72);
                                    rsp135 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8);
                                    v136 = rax134;
                                    if (!rax134) {
                                        v137 = v105;
                                    } else {
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax134) == 47)) {
                                            rax134 = rcx12;
                                        }
                                        v137 = rax134;
                                    }
                                    rbx138 = mount_list;
                                    if (!rbx138) 
                                        goto addr_4d86_110; else 
                                        goto addr_43cc_111;
                                }
                                addr_4532_100:
                                r12_139 = v104;
                                *reinterpret_cast<int32_t*>(&rbx123) = 0;
                                *reinterpret_cast<int32_t*>(&rbx123 + 4) = 0;
                                rbp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp102) + 0x90);
                                do {
                                    rax140 = *reinterpret_cast<void***>(r15_117 + 32);
                                    if (rax140 == 0xffffffffffffffff) {
                                        rdi118 = *reinterpret_cast<void***>(r15_117 + 8);
                                        eax141 = fun_38f0(rdi118, rbp3, rdi118, rbp3);
                                        rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 + 8);
                                        if (eax141) {
                                            rax142 = fun_3630();
                                            rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 + 8);
                                            if (*reinterpret_cast<void***>(rax142) == 5) {
                                                rax143 = quotearg_n_style_colon();
                                                rdx72 = reinterpret_cast<void**>("%s");
                                                *reinterpret_cast<int32_t*>(&rdi118) = 0;
                                                *reinterpret_cast<int32_t*>(&rdi118 + 4) = 0;
                                                rcx12 = rax143;
                                                fun_3a30();
                                                rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 + 8 - 8 + 8);
                                                exit_status = 1;
                                            }
                                            *reinterpret_cast<void***>(r15_117 + 32) = reinterpret_cast<void**>(0xfffffffffffffffe);
                                            rax140 = reinterpret_cast<void**>(0xfffffffffffffffe);
                                        } else {
                                            rax140 = v144;
                                            *reinterpret_cast<void***>(r15_117 + 32) = rax140;
                                        }
                                    }
                                    if (*reinterpret_cast<void***>(r12_139) == rax140 && ((rdi118 = *reinterpret_cast<void***>(r15_117 + 24), rax145 = fun_38c0(rdi118, "lofs", rdx72, rdi118, "lofs", rdx72), rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 + 8), !!*reinterpret_cast<int32_t*>(&rax145)) && (!rbx123 || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx123 + 40)) & 1 || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_117 + 40)) & 1))))) {
                                        rdi118 = *reinterpret_cast<void***>(r15_117 + 8);
                                        eax146 = fun_38f0(rdi118, rbp3, rdi118, rbp3);
                                        rsp102 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 + 8);
                                        if (eax146 || v147 != *reinterpret_cast<void***>(r15_117 + 32)) {
                                            *reinterpret_cast<void***>(r15_117 + 32) = reinterpret_cast<void**>(0xfffffffffffffffe);
                                        } else {
                                            rbx123 = r15_117;
                                        }
                                    }
                                    r15_117 = *reinterpret_cast<void***>(r15_117 + 48);
                                } while (r15_117);
                                if (!rbx123) 
                                    goto addr_4966_78;
                                addr_46c6_124:
                                r9d148 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx123 + 40));
                                r8_10 = *reinterpret_cast<void***>(rbx123 + 24);
                                rsi103 = *reinterpret_cast<void***>(rbx123 + 8);
                                eax149 = r9d148;
                                rdi150 = *reinterpret_cast<void***>(rbx123);
                                *reinterpret_cast<uint32_t*>(&r9_18) = r9d148 & 1;
                                *reinterpret_cast<int32_t*>(&r9_18 + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&eax149) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax149) >> 1);
                                *reinterpret_cast<uint32_t*>(&rax151) = eax149 & 1;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax151) + 4) = 0;
                                v152 = rax151;
                                rdx72 = v105;
                                get_dev(rdi150, rsi103, rdx72, v105, r8_10, r9_18, *reinterpret_cast<int32_t*>(&v152), 0, 0, rdi118, v153, rdi150, rsi103, rdx72, v105, r8_10, r9_18, *reinterpret_cast<int32_t*>(&v152), 0, 0, rdi118, v154);
                                rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp102) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
                                continue;
                                addr_489e_103:
                                goto addr_46c6_124;
                                addr_4d86_110:
                                fun_35f0(v136, v136);
                                rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp135) - 8 + 8);
                                goto addr_44ff_84;
                                addr_43cc_111:
                                v155 = reinterpret_cast<void**>(0);
                                r15d156 = 0;
                                v157 = reinterpret_cast<void**>(0xffffffffffffffff);
                                v158 = 0;
                                while (1) {
                                    r12_159 = *reinterpret_cast<void***>(rbx138);
                                    rax160 = fun_3830(r12_159, rsi103, rdx72);
                                    rbp3 = rax160;
                                    if (rax160 && *reinterpret_cast<void***>(rax160) == 47) {
                                        r12_159 = rax160;
                                    }
                                    rsi103 = r12_159;
                                    rax161 = fun_38c0(v137, rsi103, rdx72, v137, rsi103, rdx72);
                                    rsp162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp135) - 8 + 8 - 8 + 8);
                                    if (*reinterpret_cast<int32_t*>(&rax161)) {
                                        addr_44cb_128:
                                        fun_35f0(rbp3, rbp3);
                                        rsp135 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8);
                                        rbx138 = *reinterpret_cast<void***>(rbx138 + 48);
                                        if (rbx138) 
                                            continue; else 
                                            break;
                                    } else {
                                        r14_163 = mount_list;
                                        r13_164 = *reinterpret_cast<void***>(rbx138 + 8);
                                        if (!r14_163) {
                                            addr_4497_130:
                                            rax165 = fun_3750(r13_164, r13_164);
                                            rsp162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8);
                                            rdx72 = reinterpret_cast<void**>(static_cast<uint32_t>(v158) ^ 1);
                                            *reinterpret_cast<int32_t*>(&rdx72 + 4) = 0;
                                            *reinterpret_cast<unsigned char*>(&rcx12) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax165) < reinterpret_cast<unsigned char>(v157));
                                            r15d166 = rdx72;
                                            *reinterpret_cast<unsigned char*>(&r15d156) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r15d166) | *reinterpret_cast<unsigned char*>(&rcx12));
                                            if (*reinterpret_cast<unsigned char*>(&r15d156)) {
                                                rsi103 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp162) + 0x90);
                                                eax167 = fun_38f0(r13_164, rsi103, r13_164, rsi103);
                                                rsp162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8);
                                                if (!eax167) {
                                                    addr_4764_132:
                                                    if (rax165 == 1) 
                                                        goto addr_49bc_133;
                                                } else {
                                                    rdx72 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rdx72)));
                                                    *reinterpret_cast<int32_t*>(&rdx72 + 4) = 0;
                                                    if (!*reinterpret_cast<unsigned char*>(&rdx72) || (*reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<unsigned char*>(&rcx12), *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0, *reinterpret_cast<unsigned char*>(&rcx12) == 0)) {
                                                        r15d156 = v158;
                                                        goto addr_4778_136;
                                                    } else {
                                                        r15d156 = 0;
                                                        goto addr_4764_132;
                                                    }
                                                }
                                            } else {
                                                v158 = 1;
                                                goto addr_44c3_139;
                                            }
                                        } else {
                                            *reinterpret_cast<int32_t*>(&r15_168) = 0;
                                            *reinterpret_cast<int32_t*>(&r15_168 + 4) = 0;
                                            do {
                                                rdi169 = *reinterpret_cast<void***>(r14_163 + 8);
                                                rsi103 = r13_164;
                                                rax170 = fun_38c0(rdi169, rsi103, rdx72, rdi169, rsi103, rdx72);
                                                rsp162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8);
                                                if (!*reinterpret_cast<int32_t*>(&rax170)) {
                                                    r15_168 = r14_163;
                                                }
                                                r14_163 = *reinterpret_cast<void***>(r14_163 + 48);
                                            } while (r14_163);
                                            if (!r15_168) 
                                                goto addr_4497_130; else 
                                                goto addr_4458_145;
                                        }
                                    }
                                    v157 = rax165;
                                    v155 = rbx138;
                                    addr_4778_136:
                                    v158 = *reinterpret_cast<unsigned char*>(&r15d156);
                                    r15d156 = 0;
                                    addr_44c3_139:
                                    fun_35f0(r14_163, r14_163);
                                    rsp162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8);
                                    goto addr_44cb_128;
                                    addr_4458_145:
                                    rdi171 = *reinterpret_cast<void***>(r15_168);
                                    rax172 = fun_3830(rdi171, rsi103, rdx72);
                                    rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8);
                                    r14_163 = rax172;
                                    if (!rax172 || *reinterpret_cast<void***>(rax172) != 47) {
                                        fun_35f0(r14_163, r14_163);
                                        rdi174 = *reinterpret_cast<void***>(r15_168);
                                        rax176 = xstrdup(rdi174, rsi103, rdx72, rcx12, r8_10, r9_18, v175, v4, *reinterpret_cast<void**>(&v9));
                                        rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8 - 8 + 8);
                                        r14_163 = rax176;
                                    }
                                    rsi103 = r12_159;
                                    rax177 = fun_38c0(r14_163, rsi103, rdx72, r14_163, rsi103, rdx72);
                                    rsp162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8);
                                    if (*reinterpret_cast<int32_t*>(&rax177)) {
                                        r15d156 = 1;
                                        goto addr_44c3_139;
                                    } else {
                                        r13_164 = *reinterpret_cast<void***>(rbx138 + 8);
                                        goto addr_4497_130;
                                    }
                                }
                                fun_35f0(v136, v136);
                                rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp135) - 8 + 8);
                                if (v155) {
                                    addr_49db_152:
                                    r9d178 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v155 + 40));
                                    r8_10 = *reinterpret_cast<void***>(v155 + 24);
                                    rsi103 = *reinterpret_cast<void***>(v155 + 8);
                                    eax179 = r9d178;
                                    rdi180 = *reinterpret_cast<void***>(v155);
                                    *reinterpret_cast<uint32_t*>(&r9_18) = r9d178 & 1;
                                    *reinterpret_cast<int32_t*>(&r9_18 + 4) = 0;
                                    *reinterpret_cast<unsigned char*>(&eax179) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax179) >> 1);
                                    *reinterpret_cast<uint32_t*>(&rax181) = eax179 & 1;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax181) + 4) = 0;
                                    v182 = rax181;
                                    rdx72 = v105;
                                    get_dev(rdi180, rsi103, rdx72, 0, r8_10, r9_18, *reinterpret_cast<int32_t*>(&v182), 0, 0, r10_183, v184);
                                    rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
                                    continue;
                                } else {
                                    if (*reinterpret_cast<unsigned char*>(&r15d156)) {
                                        quotearg_style(4, v105, rdx72);
                                        rax185 = fun_3730();
                                        *reinterpret_cast<int32_t*>(&rsi103) = 0;
                                        *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
                                        rdx72 = rax185;
                                        fun_3a30();
                                        rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8 - 8 + 8 - 8 + 8);
                                        exit_status = 1;
                                        continue;
                                    }
                                }
                                addr_49bc_133:
                                fun_35f0(r14_163, r14_163);
                                fun_35f0(rbp3, rbp3);
                                fun_35f0(v136, v136);
                                rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8 - 8 + 8 - 8 + 8);
                                v155 = rbx138;
                                goto addr_49db_152;
                            }
                            goto addr_4939_155;
                        }
                    }
                    addr_4939_155:
                    zf186 = file_systems_processed == 0;
                    if (!zf186) 
                        break;
                    zf187 = exit_status == 0;
                } while (!zf187);
                break;
            }
        }
    } else {
        return 1;
    }
}

int64_t __libc_start_main = 0;

void fun_50a3() {
    __asm__("cli ");
    __libc_start_main(0x3bd0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x18008;

void fun_35c0(int64_t rdi);

int64_t fun_5143() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_35c0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_5183() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_5193() {
    uint32_t eax1;
    unsigned char* rdi2;

    __asm__("cli ");
    while (eax1 = *rdi2, !!*reinterpret_cast<signed char*>(&eax1)) {
        if (*reinterpret_cast<signed char*>(&eax1) <= 31) {
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax1) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax1) == 0))) {
                addr_51b7_5:
                *rdi2 = 63;
                goto addr_51a4_6;
            } else {
                addr_51a4_6:
                ++rdi2;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&eax1) != 0x7f) 
                goto addr_51a4_6; else 
                goto addr_51b7_5;
        }
    }
    return;
}

uint64_t fun_51d3(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_51e3(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

void** rpl_mbrtowc(void* rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_3680(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_52e3(void** rdi) {
    void** rbp2;
    int64_t rax3;
    int64_t v4;
    void** rax5;
    void* rsp6;
    void** r12_7;
    void** r15_8;
    void** r13_9;
    void* v10;
    void** r14_11;
    void** rax12;
    void** rbx13;
    int64_t rdi14;
    int32_t v15;
    int32_t eax16;
    void** rsi17;
    int64_t rax18;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = g28;
    v4 = rax3;
    rax5 = fun_3750(rdi);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    r12_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp2) + reinterpret_cast<unsigned char>(rax5));
    if (rbp2 != r12_7) {
        r15_8 = rbp2;
        r13_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
        v10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 28);
        do {
            addr_535e_3:
            r14_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_7) - reinterpret_cast<unsigned char>(r15_8));
            rax12 = rpl_mbrtowc(v10, r15_8, r14_11, r13_9);
            rbx13 = rax12;
            if (reinterpret_cast<unsigned char>(r14_11) >= reinterpret_cast<unsigned char>(rax12)) {
                *reinterpret_cast<int32_t*>(&rdi14) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                eax16 = fun_3680(rdi14, r15_8, r14_11, r13_9);
                if (!eax16) {
                    rsi17 = r15_8;
                    r15_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_8) + reinterpret_cast<unsigned char>(rbx13));
                    fun_3a20(rbp2, rsi17, rbx13, r13_9);
                    rbp2 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp2) + reinterpret_cast<unsigned char>(rbx13));
                    if (r12_7 == r15_8) 
                        break; else 
                        goto addr_535e_3;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rbx13) = 1;
                *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
            }
            r15_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_8) + reinterpret_cast<unsigned char>(rbx13));
            *reinterpret_cast<void***>(rbp2) = reinterpret_cast<void**>(63);
            ++rbp2;
        } while (r12_7 != r15_8);
    }
    *reinterpret_cast<void***>(rbp2) = reinterpret_cast<void**>(0);
    rax18 = v4 - g28;
    if (rax18) {
        fun_3780();
    } else {
        return;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_3a00(int64_t rdi, void** rsi, void** rdx, void** rcx);

unsigned char __cxa_finalize;

unsigned char g1;

signed char g2;

void** stderr = reinterpret_cast<void**>(0);

void fun_3ad0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void** a8, int64_t a9, int64_t a10, int64_t a11, void** a12);

void fun_6253(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** rcx4;
    void** r12_5;
    void** rax6;
    void** r8_7;
    void** r9_8;
    int64_t r8_9;
    int64_t r9_10;
    void** r12_11;
    void** rax12;
    void** r8_13;
    void** r9_14;
    int64_t r8_15;
    int64_t r9_16;
    void** r12_17;
    void** rax18;
    void** r8_19;
    void** r9_20;
    int64_t r8_21;
    int64_t r9_22;
    void** r12_23;
    void** rax24;
    void** r8_25;
    void** r9_26;
    int64_t r8_27;
    int64_t r9_28;
    void** r12_29;
    void** rax30;
    void** r8_31;
    void** r9_32;
    int64_t r8_33;
    int64_t r9_34;
    void** r12_35;
    void** rax36;
    void** r8_37;
    void** r9_38;
    int64_t r8_39;
    int64_t r9_40;
    void** r12_41;
    void** rax42;
    void** r8_43;
    void** r9_44;
    int64_t r8_45;
    int64_t r9_46;
    void** r12_47;
    void** rax48;
    void** r8_49;
    void** r9_50;
    int64_t r8_51;
    int64_t r9_52;
    void** r12_53;
    void** rax54;
    void** r8_55;
    void** r9_56;
    int64_t r8_57;
    int64_t r9_58;
    void** rax59;
    void** r12_60;
    void** rax61;
    void** r8_62;
    void** r9_63;
    int64_t r8_64;
    int64_t r9_65;
    void** r12_66;
    void** rax67;
    void** r8_68;
    void** r9_69;
    int64_t r8_70;
    int64_t r9_71;
    uint32_t ecx72;
    uint32_t ecx73;
    int1_t zf74;
    void** rax75;
    void** rax76;
    int32_t eax77;
    void** rax78;
    void** r13_79;
    void** rax80;
    void** rax81;
    int32_t eax82;
    void** rax83;
    void** r14_84;
    void** rax85;
    void** r8_86;
    void** r9_87;
    int64_t r8_88;
    int64_t r9_89;
    void** rax90;
    void** rax91;
    void** rdi92;
    void** r8_93;
    void** r9_94;
    int64_t v95;
    void** v96;
    int64_t v97;
    int64_t v98;
    int64_t v99;
    void** v100;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_3730();
            fun_3a00(1, rax3, r12_2, rcx4);
            r12_5 = stdout;
            rax6 = fun_3730();
            fun_3880(rax6, r12_5, 5, rcx4, r8_7, r9_8, "[", "test invocation", "coreutils", rax6, r12_5, 5, rcx4, r8_9, r9_10, "[", "test invocation", "coreutils");
            r12_11 = stdout;
            rax12 = fun_3730();
            fun_3880(rax12, r12_11, 5, rcx4, r8_13, r9_14, "[", "test invocation", "coreutils", rax12, r12_11, 5, rcx4, r8_15, r9_16, "[", "test invocation", "coreutils");
            r12_17 = stdout;
            rax18 = fun_3730();
            fun_3880(rax18, r12_17, 5, rcx4, r8_19, r9_20, "[", "test invocation", "coreutils", rax18, r12_17, 5, rcx4, r8_21, r9_22, "[", "test invocation", "coreutils");
            r12_23 = stdout;
            rax24 = fun_3730();
            fun_3880(rax24, r12_23, 5, rcx4, r8_25, r9_26, "[", "test invocation", "coreutils", rax24, r12_23, 5, rcx4, r8_27, r9_28, "[", "test invocation", "coreutils");
            r12_29 = stdout;
            rax30 = fun_3730();
            fun_3880(rax30, r12_29, 5, rcx4, r8_31, r9_32, "[", "test invocation", "coreutils", rax30, r12_29, 5, rcx4, r8_33, r9_34, "[", "test invocation", "coreutils");
            r12_35 = stdout;
            rax36 = fun_3730();
            fun_3880(rax36, r12_35, 5, rcx4, r8_37, r9_38, "[", "test invocation", "coreutils", rax36, r12_35, 5, rcx4, r8_39, r9_40, "[", "test invocation", "coreutils");
            r12_41 = stdout;
            rax42 = fun_3730();
            fun_3880(rax42, r12_41, 5, rcx4, r8_43, r9_44, "[", "test invocation", "coreutils", rax42, r12_41, 5, rcx4, r8_45, r9_46, "[", "test invocation", "coreutils");
            r12_47 = stdout;
            rax48 = fun_3730();
            fun_3880(rax48, r12_47, 5, rcx4, r8_49, r9_50, "[", "test invocation", "coreutils", rax48, r12_47, 5, rcx4, r8_51, r9_52, "[", "test invocation", "coreutils");
            r12_53 = stdout;
            rax54 = fun_3730();
            fun_3880(rax54, r12_53, 5, rcx4, r8_55, r9_56, "[", "test invocation", "coreutils", rax54, r12_53, 5, rcx4, r8_57, r9_58, "[", "test invocation", "coreutils");
            rax59 = fun_3730();
            fun_3a00(1, rax59, "DF", rcx4);
            r12_60 = stdout;
            rax61 = fun_3730();
            fun_3880(rax61, r12_60, 5, rcx4, r8_62, r9_63, "[", "test invocation", "coreutils", rax61, r12_60, 5, rcx4, r8_64, r9_65, "[", "test invocation", "coreutils");
            r12_66 = stdout;
            rax67 = fun_3730();
            fun_3880(rax67, r12_66, 5, rcx4, r8_68, r9_69, "[", "test invocation", "coreutils", rax67, r12_66, 5, rcx4, r8_70, r9_71, "[", "test invocation", "coreutils");
            do {
                if (1) 
                    break;
                ecx72 = __cxa_finalize;
            } while (100 != ecx72 || ((ecx73 = g1, 0x66 != ecx73) || (zf74 = g2 == 0, !zf74)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax75 = fun_3730();
                fun_3a00(1, rax75, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax76 = fun_39f0(5);
                if (!rax76 || (eax77 = fun_3640(rax76, "en_", 3, rax76, "en_", 3), !eax77)) {
                    rax78 = fun_3730();
                    r12_2 = reinterpret_cast<void**>("df");
                    r13_79 = reinterpret_cast<void**>(" invocation");
                    fun_3a00(1, rax78, "https://www.gnu.org/software/coreutils/", "df");
                } else {
                    r12_2 = reinterpret_cast<void**>("df");
                    goto addr_66c3_9;
                }
            } else {
                rax80 = fun_3730();
                fun_3a00(1, rax80, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax81 = fun_39f0(5);
                if (!rax81 || (eax82 = fun_3640(rax81, "en_", 3, rax81, "en_", 3), !eax82)) {
                    addr_65c6_11:
                    rax83 = fun_3730();
                    r13_79 = reinterpret_cast<void**>(" invocation");
                    fun_3a00(1, rax83, "https://www.gnu.org/software/coreutils/", "df");
                    if (!reinterpret_cast<int1_t>(r12_2 == "df")) {
                        r13_79 = reinterpret_cast<void**>(0x136c1);
                    }
                } else {
                    addr_66c3_9:
                    r14_84 = stdout;
                    rax85 = fun_3730();
                    fun_3880(rax85, r14_84, 5, "https://www.gnu.org/software/coreutils/", r8_86, r9_87, "[", "test invocation", "coreutils", rax85, r14_84, 5, "https://www.gnu.org/software/coreutils/", r8_88, r9_89, "[", "test invocation", "coreutils");
                    goto addr_65c6_11;
                }
            }
            rax90 = fun_3730();
            rcx4 = r13_79;
            fun_3a00(1, rax90, r12_2, rcx4);
            addr_62a9_14:
            fun_3ab0();
        }
    } else {
        rax91 = fun_3730();
        rdi92 = stderr;
        rcx4 = r12_2;
        fun_3ad0(rdi92, 1, rax91, rcx4, r8_93, r9_94, v95, v96, v97, v98, v99, v100);
        goto addr_62a9_14;
    }
}

int32_t save_cwd(void* rdi);

int32_t fun_3770(void** rdi, void** rsi, void** rdx);

void** dir_name(void** rdi);

int32_t restore_cwd(void* rdi, void** rsi, void** rdx, void* rcx);

void free_cwd(void* rdi, void** rsi, void** rdx, void* rcx);

int64_t xgetcwd(void** rdi, void** rsi, void** rdx);

int64_t fun_6843(void** rdi, void** rsi, void** rdx) {
    void* rsp4;
    void* rbp5;
    void* r14_6;
    int64_t rax7;
    int64_t v8;
    int32_t eax9;
    void** rax10;
    void* rsp11;
    void** r13_12;
    int64_t r12_13;
    void* rsp14;
    int32_t eax15;
    void* rsp16;
    void** rsi17;
    void** rbx18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void* rsp22;
    void** r8_23;
    void*** rax24;
    void* rcx25;
    uint64_t rdx26;
    void* rdx27;
    void* rsp28;
    int64_t* rsp29;
    void** rax30;
    int64_t* rsp31;
    int64_t* rsp32;
    int32_t eax33;
    int64_t rax34;
    int64_t* rsp35;
    void* rsp36;
    int64_t* rsp37;
    int64_t* rsp38;
    int64_t* rsp39;
    void** rsi40;
    int64_t* rsp41;
    int32_t eax42;
    void* rsp43;
    int64_t v44;
    int64_t v45;
    int64_t v46;
    int64_t v47;
    int64_t* rsp48;
    int32_t eax49;
    int64_t* rsp50;
    void* rax51;
    void* rsp52;
    void* r12_53;
    int64_t* rsp54;
    void** rax55;
    int64_t* rsp56;
    void* rsp57;
    void** ebx58;
    int64_t* rsp59;
    int32_t eax60;
    int64_t* rsp61;
    int64_t* rsp62;
    int64_t* rsp63;
    int64_t rax64;
    int64_t* rsp65;
    void* rax66;
    void** rsi67;
    int64_t* rsp68;
    int32_t eax69;
    int64_t* rsp70;
    void* rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;

    __asm__("cli ");
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp5 = rsp4;
    r14_6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp5) - 0x170);
    rax7 = g28;
    v8 = rax7;
    eax9 = save_cwd(r14_6);
    rax10 = fun_3630();
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8);
    r13_12 = rax10;
    if (eax9) {
        *reinterpret_cast<int32_t*>(&r12_13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
        fun_3730();
        fun_3a30();
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
    } else {
        if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 24)) & 0xf000) == 0x4000) {
            __asm__("movdqu xmm1, [rbx]");
            __asm__("movdqu xmm2, [rbx+0x10]");
            __asm__("movdqu xmm3, [rbx+0x20]");
            __asm__("movdqu xmm4, [rbx+0x30]");
            __asm__("movdqu xmm5, [rbx+0x40]");
            __asm__("movdqu xmm6, [rbx+0x50]");
            __asm__("movaps [rbp-0x160], xmm1");
            __asm__("movdqu xmm7, [rbx+0x60]");
            __asm__("movdqu xmm1, [rbx+0x70]");
            __asm__("movaps [rbp-0x150], xmm2");
            __asm__("movdqu xmm2, [rbx+0x80]");
            __asm__("movaps [rbp-0x140], xmm3");
            __asm__("movaps [rbp-0x130], xmm4");
            __asm__("movaps [rbp-0x120], xmm5");
            __asm__("movaps [rbp-0x110], xmm6");
            __asm__("movaps [rbp-0x100], xmm7");
            __asm__("movaps [rbp-0xf0], xmm1");
            __asm__("movaps [rbp-0xe0], xmm2");
            eax15 = fun_3770(rdi, rsi, rdx);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
            if (eax15 < 0) {
                rsi17 = rdi;
                goto addr_6bd3_6;
            } else {
                addr_6a0e_7:
                rbx18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp5) + 0xffffffffffffff30);
                goto addr_6adb_8;
            }
        } else {
            rax19 = dir_name(rdi);
            r12_20 = rax19;
            rax21 = fun_3750(rax19);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
            r8_23 = rax21 + 1;
            rax24 = reinterpret_cast<void***>(rax21 + 24);
            rcx25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - (reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffff000));
            rdx26 = reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffffff0;
            if (rsp22 != rcx25) {
                do {
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - reinterpret_cast<int64_t>("oc"));
                } while (rsp22 != rcx25);
            }
            *reinterpret_cast<uint32_t*>(&rdx27) = *reinterpret_cast<uint32_t*>(&rdx26) & reinterpret_cast<uint32_t>("loc");
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - reinterpret_cast<int64_t>(rdx27));
            if (rdx27) {
                *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<int64_t>(rdx27) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<int64_t>(rdx27) - 8);
            }
            rdx = r8_23;
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp28) - 8);
            *rsp29 = 0x6913;
            rax30 = fun_3900(reinterpret_cast<uint64_t>(rsp28) + 15 & 0xfffffffffffffff0, r12_20, rdx);
            rsp31 = rsp29 + 1 - 1;
            *rsp31 = 0x691e;
            fun_35f0(r12_20, r12_20);
            rsp32 = rsp31 + 1 - 1;
            *rsp32 = 0x6926;
            eax33 = fun_3770(rax30, r12_20, rdx);
            rsp16 = reinterpret_cast<void*>(rsp32 + 1);
            if (eax33 < 0) 
                goto addr_6bd0_14; else 
                goto addr_692e_15;
        }
    }
    addr_6b58_16:
    rax34 = v8 - g28;
    if (rax34) {
        rsp35 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp14) - 8);
        *rsp35 = 0x6c18;
        fun_3780();
        rsp36 = reinterpret_cast<void*>(rsp35 + 1);
        goto addr_6c18_18;
    } else {
        return r12_13;
    }
    addr_6bd3_6:
    rsp37 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
    *rsp37 = 0x6bdd;
    quotearg_style(4, rsi17, rdx);
    rsp38 = rsp37 + 1 - 1;
    *rsp38 = 0x6bf3;
    fun_3730();
    *reinterpret_cast<int32_t*>(&r12_13) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
    rsp39 = rsp38 + 1 - 1;
    *rsp39 = 0x6c09;
    fun_3a30();
    rsp14 = reinterpret_cast<void*>(rsp39 + 1);
    goto addr_6b58_16;
    addr_6adb_8:
    while (rsi40 = rbx18, rsp41 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp41 = 0x6ae6, eax42 = fun_38f0("..", rsi40, "..", rsi40), rsp43 = reinterpret_cast<void*>(rsp41 + 1), eax42 >= 0) {
        if (v44 != v45) 
            goto addr_6b30_21;
        if (v46 == v47) 
            goto addr_6b30_21;
        rsp48 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp43) - 8);
        *rsp48 = 0x6a58;
        eax49 = fun_3770("..", rsi40, rdx);
        rsp16 = reinterpret_cast<void*>(rsp48 + 1);
        if (eax49 < 0) 
            goto addr_6b80_24;
        __asm__("movdqa xmm0, [rbp-0xd0]");
        __asm__("movdqa xmm1, [rbp-0xc0]");
        __asm__("movdqa xmm2, [rbp-0xb0]");
        __asm__("movdqa xmm3, [rbp-0xa0]");
        __asm__("movdqa xmm4, [rbp-0x90]");
        __asm__("movdqa xmm5, [rbp-0x80]");
        __asm__("movaps [rbp-0x160], xmm0");
        __asm__("movdqa xmm6, [rbp-0x70]");
        __asm__("movdqa xmm7, [rbp-0x60]");
        __asm__("movaps [rbp-0x150], xmm1");
        __asm__("movdqa xmm0, [rbp-0x50]");
        __asm__("movaps [rbp-0x140], xmm2");
        __asm__("movaps [rbp-0x130], xmm3");
        __asm__("movaps [rbp-0x120], xmm4");
        __asm__("movaps [rbp-0x110], xmm5");
        __asm__("movaps [rbp-0x100], xmm6");
        __asm__("movaps [rbp-0xf0], xmm7");
        __asm__("movaps [rbp-0xe0], xmm0");
    }
    rsp50 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp43) - 8);
    *rsp50 = 0x6afb;
    rax51 = quotearg_style(4, "..", rdx);
    rsp52 = reinterpret_cast<void*>(rsp50 + 1);
    r12_53 = rax51;
    addr_6b0a_27:
    rsp54 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp52) - 8);
    *rsp54 = 0x6b11;
    rax55 = fun_3730();
    rsi40 = *reinterpret_cast<void***>(r13_12);
    *reinterpret_cast<int32_t*>(&rsi40 + 4) = 0;
    rcx25 = r12_53;
    rdx = rax55;
    *reinterpret_cast<int32_t*>(&r12_13) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
    rsp56 = rsp54 + 1 - 1;
    *rsp56 = 0x6b27;
    fun_3a30();
    rsp57 = reinterpret_cast<void*>(rsp56 + 1);
    addr_6b38_28:
    ebx58 = *reinterpret_cast<void***>(r13_12);
    rsp59 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp57) - 8);
    *rsp59 = 0x6b44;
    eax60 = restore_cwd(r14_6, rsi40, rdx, rcx25);
    rsp36 = reinterpret_cast<void*>(rsp59 + 1);
    if (eax60) {
        addr_6c18_18:
        rsp61 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp36) - 8);
        *rsp61 = 0x6c2b;
        fun_3730();
        *(rsp61 + 1 - 1) = 0x6c3e;
        fun_3a30();
    } else {
        rsp62 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp36) - 8);
        *rsp62 = 0x6b54;
        free_cwd(r14_6, rsi40, rdx, rcx25);
        rsp14 = reinterpret_cast<void*>(rsp62 + 1);
        *reinterpret_cast<void***>(r13_12) = ebx58;
        goto addr_6b58_16;
    }
    addr_6b30_21:
    rsp63 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp43) - 8);
    *rsp63 = 0x6b35;
    rax64 = xgetcwd("..", rsi40, rdx);
    rsp57 = reinterpret_cast<void*>(rsp63 + 1);
    r12_13 = rax64;
    goto addr_6b38_28;
    addr_6b80_24:
    rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
    *rsp65 = 0x6b8d;
    rax66 = quotearg_style(4, "..", rdx);
    rsp52 = reinterpret_cast<void*>(rsp65 + 1);
    r12_53 = rax66;
    goto addr_6b0a_27;
    addr_6bd0_14:
    rsi17 = rax30;
    goto addr_6bd3_6;
    addr_692e_15:
    rsi67 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp5) + 0xfffffffffffffea0);
    rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
    *rsp68 = 0x6941;
    eax69 = fun_38f0(".", rsi67, ".", rsi67);
    rsp16 = reinterpret_cast<void*>(rsp68 + 1);
    if (eax69 >= 0) 
        goto addr_6a0e_7;
    rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
    *rsp70 = 0x6956;
    rax71 = quotearg_style(4, rax30, rdx);
    rsp72 = rsp70 + 1 - 1;
    *rsp72 = 0x696c;
    rax73 = fun_3730();
    rsi40 = *reinterpret_cast<void***>(r13_12);
    *reinterpret_cast<int32_t*>(&rsi40 + 4) = 0;
    rcx25 = rax71;
    rdx = rax73;
    *reinterpret_cast<int32_t*>(&r12_13) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
    rsp74 = rsp72 + 1 - 1;
    *rsp74 = 0x6982;
    fun_3a30();
    rsp57 = reinterpret_cast<void*>(rsp74 + 1);
    goto addr_6b38_28;
}

void** fun_74a3(void** rdi, uint32_t esi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    rax3 = g28;
    rax4 = canonicalize_filename_mode_stk(rdi, esi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x428);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_3780();
    } else {
        return rax4;
    }
}

int64_t file_name = 0;

void fun_74f3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_7503(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

uint32_t exit_failure = 1;

void** fun_3650(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_7513() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_3630(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_3730();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_75a3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_3a30();
    }
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_3650(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_75a3_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_3a30();
    }
}

int64_t mdir_name();

void fun_75c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mdir_name();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void* last_component();

void fun_75e3(signed char* rdi) {
    void* rbp2;
    signed char* rbx3;
    void* rax4;
    void* rax5;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp2) + 4) = 0;
    rbx3 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp2) = reinterpret_cast<uint1_t>(*rdi == 47);
    rax4 = last_component();
    rax5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<uint64_t>(rbx3));
    while (reinterpret_cast<uint64_t>(rax5) > reinterpret_cast<uint64_t>(rbp2) && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbx3) + reinterpret_cast<uint64_t>(rax5) - 1) == 47) {
        rax5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - 1);
    }
    return;
}

void** fun_7623(void** rdi, void** rsi) {
    void** rbp3;
    void** rbx4;
    void* rax5;
    void** r12_6;
    void* rax7;
    uint32_t ebx8;
    void** rax9;
    void** r8_10;
    void** rax11;
    void** rax12;
    void** rax13;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx4) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax5 = last_component();
    r12_6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<unsigned char>(rbp3));
    while (reinterpret_cast<unsigned char>(rbx4) < reinterpret_cast<unsigned char>(r12_6)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp3) + reinterpret_cast<unsigned char>(r12_6) + 0xffffffffffffffff) != 47) 
            goto addr_76a0_4;
        --r12_6;
    }
    rax7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_6) ^ 1);
    ebx8 = *reinterpret_cast<uint32_t*>(&rax7) & 1;
    rax9 = fun_3950(reinterpret_cast<unsigned char>(r12_6) + reinterpret_cast<uint64_t>(rax7) + 1, rsi);
    if (!rax9) {
        addr_76cd_7:
        *reinterpret_cast<int32_t*>(&r8_10) = 0;
        *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
    } else {
        rax11 = fun_3900(rax9, rbp3, r12_6);
        r8_10 = rax11;
        if (!*reinterpret_cast<signed char*>(&ebx8)) {
            *reinterpret_cast<int32_t*>(&r12_6) = 1;
            *reinterpret_cast<int32_t*>(&r12_6 + 4) = 0;
            goto addr_768e_10;
        } else {
            *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(46);
            *reinterpret_cast<int32_t*>(&r12_6) = 1;
            *reinterpret_cast<int32_t*>(&r12_6 + 4) = 0;
            goto addr_768e_10;
        }
    }
    addr_7693_12:
    return r8_10;
    addr_768e_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_10) + reinterpret_cast<unsigned char>(r12_6)) = 0;
    goto addr_7693_12;
    addr_76a0_4:
    rax12 = fun_3950(r12_6 + 1, rsi);
    if (!rax12) 
        goto addr_76cd_7;
    rax13 = fun_3900(rax12, rbp3, r12_6);
    r8_10 = rax13;
    goto addr_768e_10;
}

void** xmalloc(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

void** hash_insert(void** rdi, void** rsi);

void fun_76e3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int64_t v7;
    int64_t rbp8;
    void** rax9;
    void*** v10;
    void** rbx11;
    void** rax12;
    void** rax13;

    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        v7 = rbp8;
        rax9 = xmalloc(24, rsi, rdx, rcx);
        rax12 = xstrdup(rsi, rsi, rdx, rcx, r8, r9, v10, rbx11, *reinterpret_cast<void**>(&v7));
        *reinterpret_cast<void***>(rax9) = rax12;
        *reinterpret_cast<void***>(rax9 + 8) = *reinterpret_cast<void***>(rdx + 8);
        *reinterpret_cast<void***>(rax9 + 16) = *reinterpret_cast<void***>(rdx);
        rax13 = hash_insert(rdi, rax9);
        if (!rax13) {
            xalloc_die();
        } else {
            if (rax9 == rax13) {
                return;
            }
        }
    }
}

struct s4* fun_7773(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s4* rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax4 = g28;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (rdi) {
        rax5 = hash_lookup();
        *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(!!rax5);
    }
    rdx6 = rax4 - g28;
    if (rdx6) {
        fun_3780();
    } else {
        return rax5;
    }
}

/* statvfs_works_cache.0 */
int32_t statvfs_works_cache_0 = -1;

int32_t fun_37e0(void* rdi);

int32_t fun_3610(void* rdi, int64_t rsi);

int32_t fun_3920(int64_t rdi, void* rsi);

int32_t fun_39c0(int64_t rdi, void* rsi);

struct s14 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
    uint64_t f18;
    signed char f20;
    signed char[7] pad40;
    int64_t f28;
    int64_t f30;
};

int64_t fun_77d3(int64_t rdi) {
    void* rsp2;
    int64_t rax3;
    int32_t eax4;
    void* r12_5;
    int32_t eax6;
    int32_t eax7;
    int32_t eax8;
    int64_t rax9;
    int64_t rax10;
    int64_t v11;
    int32_t eax12;
    int64_t rdx13;
    int64_t v14;
    int64_t v15;
    struct s14* rdx16;
    int64_t v17;
    int64_t v18;
    uint64_t v19;
    uint64_t rax20;
    int64_t v21;
    int64_t v22;

    __asm__("cli ");
    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x190);
    rax3 = g28;
    eax4 = statvfs_works_cache_0;
    r12_5 = rsp2;
    if (eax4 < 0) {
        eax6 = fun_37e0(r12_5);
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        if (eax6 || (eax7 = fun_3610(reinterpret_cast<int64_t>(rsp2) + 0x82, "2.6.36"), eax7 < 0)) {
            statvfs_works_cache_0 = 0;
            goto addr_784a_4;
        } else {
            statvfs_works_cache_0 = 1;
            goto addr_7807_6;
        }
    } else {
        if (!eax4) {
            addr_784a_4:
            eax8 = fun_3920(rdi, r12_5);
            if (eax8 < 0) {
                *reinterpret_cast<int32_t*>(&rax9) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                goto addr_789b_9;
            } else {
                rax10 = v11;
            }
        } else {
            addr_7807_6:
            eax12 = fun_39c0(rdi, r12_5);
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
            if (eax12 < 0) {
                addr_789b_9:
                rdx13 = rax3 - g28;
                if (rdx13) {
                    fun_3780();
                } else {
                    return rax9;
                }
            } else {
                rax10 = v14;
                if (!rax10) {
                    rax10 = v15;
                }
            }
        }
        rdx16->f0 = rax10;
        rdx16->f8 = v17;
        rdx16->f10 = v18;
        rdx16->f18 = v19;
        rax20 = v19 >> 63;
        rdx16->f20 = *reinterpret_cast<signed char*>(&rax20);
        rdx16->f28 = v21;
        rdx16->f30 = v22;
        *reinterpret_cast<int32_t*>(&rax9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        goto addr_789b_9;
    }
}

uint64_t fun_7903(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_7923(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s15 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_7d83(struct s15* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s16 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_7d93(struct s16* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s17 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_7da3(struct s17* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s20 {
    signed char[8] pad8;
    struct s20* f8;
};

struct s19 {
    int64_t f0;
    struct s20* f8;
};

struct s18 {
    struct s19* f0;
    struct s19* f8;
};

uint64_t fun_7db3(struct s18* rdi) {
    struct s19* rcx2;
    struct s19* rsi3;
    uint64_t r8_4;
    struct s20* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s23 {
    signed char[8] pad8;
    struct s23* f8;
};

struct s22 {
    int64_t f0;
    struct s23* f8;
};

struct s21 {
    struct s22* f0;
    struct s22* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_7e13(struct s21* rdi) {
    struct s22* rcx2;
    struct s22* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s23* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s26 {
    signed char[8] pad8;
    struct s26* f8;
};

struct s25 {
    int64_t f0;
    struct s26* f8;
};

struct s24 {
    struct s25* f0;
    struct s25* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_7e83(struct s24* rdi, void** rsi) {
    void** v3;
    int64_t v4;
    int64_t r13_5;
    int64_t v6;
    int64_t r12_7;
    uint64_t r12_8;
    int64_t v9;
    int64_t rbp10;
    void** rbp11;
    void** v12;
    void** rbx13;
    struct s25* rcx14;
    struct s25* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s26* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<void**>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_3ad0(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_3ad0(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0xafec]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_7f3a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_7fb9_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_3ad0(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_3ad0;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0xb06b]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_7fb9_14; else 
            goto addr_7f3a_13;
    }
}

struct s27 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s28 {
    int64_t f0;
    struct s28* f8;
};

int64_t fun_7fe3(struct s27* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s27* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s28* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x3b6a;
    rbx7 = reinterpret_cast<struct s28*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_8048_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_803b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_803b_7;
    }
    addr_804b_10:
    return r12_3;
    addr_8048_5:
    r12_3 = rbx7->f0;
    goto addr_804b_10;
    addr_803b_7:
    return 0;
}

struct s29 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_8063(struct s29* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x3b6f;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_809f_7;
    return *rax2;
    addr_809f_7:
    goto 0x3b6f;
}

struct s31 {
    int64_t f0;
    struct s31* f8;
};

struct s30 {
    int64_t* f0;
    struct s31* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_80b3(struct s30* rdi, int64_t rsi) {
    struct s30* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s31* rax7;
    struct s31* rdx8;
    struct s31* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x3b75;
    rax7 = reinterpret_cast<struct s31*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_80fe_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_80fe_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_811c_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_811c_10:
    return r8_10;
}

struct s33 {
    int64_t f0;
    struct s33* f8;
};

struct s32 {
    struct s33* f0;
    struct s33* f8;
};

void fun_8143(struct s32* rdi, int64_t rsi, uint64_t rdx) {
    struct s33* r9_4;
    uint64_t rax5;
    struct s33* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_8182_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_8182_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s35 {
    int64_t f0;
    struct s35* f8;
};

struct s34 {
    struct s35* f0;
    struct s35* f8;
};

int64_t fun_8193(struct s34* rdi, int64_t rsi, int64_t rdx) {
    struct s35* r14_4;
    int64_t r12_5;
    struct s34* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s35* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_81bf_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_8201_10;
            }
            addr_81bf_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_81c9_11:
    return r12_5;
    addr_8201_10:
    goto addr_81c9_11;
}

uint64_t fun_8213(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s36 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_8253(struct s36* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_38a0(void** rdi, int64_t rsi);

void** fun_8283(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    uint32_t esi12;
    void** rax13;
    void** rax14;
    void** rdi15;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x7900);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0x7920);
    }
    rax9 = fun_3950(80, rsi);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0x130f0);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((esi12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), rax13 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&esi12)), *reinterpret_cast<void***>(r12_10 + 16) = rax13, rax13 == 0) || (rax14 = fun_38a0(rax13, 16), *reinterpret_cast<void***>(r12_10) = rax14, rax14 == 0))) {
            rdi15 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_35f0(rdi15, rdi15);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s39 {
    int64_t f0;
    struct s39* f8;
};

struct s38 {
    int64_t f0;
    struct s39* f8;
};

struct s37 {
    struct s38* f0;
    struct s38* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s39* f48;
};

void fun_8383(struct s37* rdi) {
    struct s37* rbp2;
    struct s38* r12_3;
    struct s39* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s39* rax7;
    struct s39* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s40 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_8433(struct s40* rdi) {
    struct s40* r12_2;
    void** r13_3;
    void** rax4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** rdi8;
    void** rbx9;
    void** rbx10;
    void** rdi11;
    void** rdi12;

    __asm__("cli ");
    r12_2 = rdi;
    r13_3 = rdi->f0;
    rax4 = rdi->f8;
    rbp5 = r13_3;
    if (!rdi->f40 || !rdi->f20) {
        addr_84a3_2:
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp5)) {
            do {
                rbx6 = *reinterpret_cast<void***>(rbp5 + 8);
                if (rbx6) {
                    do {
                        rdi7 = rbx6;
                        rbx6 = *reinterpret_cast<void***>(rbx6 + 8);
                        fun_35f0(rdi7);
                    } while (rbx6);
                }
                rbp5 = rbp5 + 16;
            } while (reinterpret_cast<unsigned char>(r12_2->f8) > reinterpret_cast<unsigned char>(rbp5));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_3) < reinterpret_cast<unsigned char>(rax4)) {
            while (1) {
                rdi8 = *reinterpret_cast<void***>(r13_3);
                if (!rdi8) {
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                } else {
                    rbx9 = r13_3;
                    while (r12_2->f40(rdi8), rbx9 = *reinterpret_cast<void***>(rbx9 + 8), !!rbx9) {
                        rdi8 = *reinterpret_cast<void***>(rbx9);
                    }
                    rax4 = r12_2->f8;
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                }
            }
            rbp5 = r12_2->f0;
            goto addr_84a3_2;
        }
    }
    rbx10 = r12_2->f48;
    if (rbx10) {
        do {
            rdi11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
            fun_35f0(rdi11);
        } while (rbx10);
    }
    rdi12 = r12_2->f0;
    fun_35f0(rdi12);
    goto fun_35f0;
}

int64_t fun_8523(struct s7* rdi, uint64_t rsi) {
    struct s8* r12_3;
    int64_t rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    struct s7* r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    int64_t rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = rdi->f28;
    rax4 = g28;
    esi5 = r12_3->f10;
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_8660_2;
    if (rdi->f10 == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_38a0(rax6, 16);
        if (!rax8) {
            addr_8660_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8 - 8 + 8);
            v10 = rdi->f48;
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = rdi->f0;
                fun_35f0(rdi12, rdi12);
                rdi->f0 = rax8;
                rdi->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                rdi->f10 = rax6;
                rdi->f18 = 0;
                rdi->f48 = v10;
            } else {
                rdi->f48 = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x3b7a;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x3b7a;
                fun_35f0(rax8, rax8);
            }
        }
    }
    rax15 = rax4 - g28;
    if (rax15) {
        fun_3780();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s41 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_86b3(void** rdi, void** rsi, void*** rdx) {
    int64_t rax4;
    void*** r12_5;
    void** rbp6;
    void** rax7;
    void** rax8;
    uint1_t below_or_equal9;
    uint64_t rax10;
    int1_t cf11;
    signed char al12;
    void** rax13;
    struct s41* v14;
    int32_t r8d15;
    void** rax16;
    int64_t rax17;
    int64_t rax18;
    void** rdx19;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x3b7f;
    r12_5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rbp6 = rsi;
    rax7 = hash_find_entry(rdi, rsi, r12_5, 0);
    if (!rax7) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 24)) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax8 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal9 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax8 == 0)));
            if (reinterpret_cast<signed char>(rax8) < reinterpret_cast<signed char>(0)) 
                goto addr_87ce_5; else 
                goto addr_873f_6;
        }
        __asm__("pxor xmm5, xmm5");
        rax8 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal9 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax8 == 0)));
        if (reinterpret_cast<signed char>(rax8) >= reinterpret_cast<signed char>(0)) {
            addr_873f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_87ce_5:
            *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<uint32_t*>(&rax8) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            below_or_equal9 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax8) >> 1) | rax10) == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal9 && (check_tuning(rdi), !below_or_equal9)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0xa8f1]");
            if (!cf11) 
                goto addr_8825_12;
            __asm__("comiss xmm4, [rip+0xa8a1]");
            if (!cf11) {
                __asm__("subss xmm4, [rip+0xa860]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al12 = hash_rehash(rdi);
            if (!al12) 
                goto addr_8825_12;
            rsi = rbp6;
            rax13 = hash_find_entry(rdi, rsi, r12_5, 0);
            if (rax13) {
                goto 0x3b7f;
            }
        }
        if (!v14->f0) {
            v14->f0 = rbp6;
            r8d15 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax16 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax16) {
                rax16 = fun_3950(16, rsi, 16, rsi);
                if (!rax16) {
                    addr_8825_12:
                    r8d15 = -1;
                } else {
                    goto addr_8782_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax16 + 8);
                goto addr_8782_24;
            }
        }
    } else {
        r8d15 = 0;
        if (rdx) {
            *rdx = rax7;
        }
    }
    addr_86fe_28:
    rax17 = rax4 - g28;
    if (rax17) {
        fun_3780();
    } else {
        *reinterpret_cast<int32_t*>(&rax18) = r8d15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
        return rax18;
    }
    addr_8782_24:
    rdx19 = v14->f8;
    *reinterpret_cast<void***>(rax16) = rbp6;
    r8d15 = 1;
    *reinterpret_cast<void***>(rax16 + 8) = rdx19;
    v14->f8 = rax16;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_86fe_28;
}

int32_t hash_insert_if_absent();

int64_t fun_88d3() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    int64_t rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = rax1 - g28;
    if (rdx6) {
        fun_3780();
    } else {
        return rax3;
    }
}

void** fun_8933(void** rdi, void** rsi) {
    void** rbx3;
    int64_t rax4;
    int64_t v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    int64_t rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_89c0_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_8a76_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0xa70e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0xa678]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_35f0(rdi15, rdi15);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_8a76_5; else 
                goto addr_89c0_4;
        }
    }
    rax16 = v5 - g28;
    if (rax16) {
        fun_3780();
    } else {
        return r12_7;
    }
}

void fun_8ac3() {
    __asm__("cli ");
    goto 0x8930;
}

struct s42 {
    int64_t f0;
    uint64_t f8;
};

uint64_t hash_pjw(int64_t rdi);

uint64_t fun_8ad3(struct s42* rdi, int64_t rsi) {
    int64_t rdi3;
    uint64_t rax4;

    __asm__("cli ");
    rdi3 = rdi->f0;
    rax4 = hash_pjw(rdi3);
    return (rax4 ^ rdi->f8) % rsi;
}

struct s43 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
};

struct s44 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
};

int64_t fun_8b03(struct s43* rdi, struct s44* rsi) {
    void** rdx3;
    void** rsi4;
    void** rdi5;
    int64_t rax6;

    __asm__("cli ");
    rdx3 = rsi->f8;
    if (rdi->f8 != rdx3 || rdi->f10 != rsi->f10) {
        return 0;
    } else {
        rsi4 = rsi->f0;
        rdi5 = rdi->f0;
        rax6 = fun_38c0(rdi5, rsi4, rdx3);
        *reinterpret_cast<unsigned char*>(&rax6) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax6) == 0);
        return rax6;
    }
}

void fun_8b43(void*** rdi) {
    void** rdi2;

    __asm__("cli ");
    rdi2 = *rdi;
    fun_35f0(rdi2);
    goto fun_35f0;
}

struct s45 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char* f10;
};

struct s45* fun_36b0();

void fun_3b50(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void fun_38e0(void** rdi, void** rsi, void** rdx, int64_t rcx);

void** fun_8b63(uint64_t rdi, void** rsi, uint32_t edx, uint64_t rcx, uint64_t r8) {
    void** v6;
    uint64_t v7;
    uint32_t v8;
    int64_t rax9;
    int64_t v10;
    uint32_t edx11;
    uint32_t v12;
    uint32_t eax13;
    uint32_t v14;
    uint32_t v15;
    struct s45* rax16;
    void** r15_17;
    void** rax18;
    void** r14_19;
    void** rbp20;
    int1_t cf21;
    unsigned char* v22;
    void** rax23;
    void*** rsp24;
    void** v25;
    uint64_t r8_26;
    uint64_t rdx27;
    uint64_t rcx28;
    uint64_t rax29;
    uint64_t rax30;
    uint64_t rdx31;
    uint64_t rax32;
    uint64_t rdx33;
    int64_t rdi34;
    uint32_t esi35;
    uint32_t r10d36;
    uint64_t v37;
    int64_t rbx38;
    uint64_t rax39;
    int1_t zf40;
    void** rcx41;
    void** rax42;
    void*** rsp43;
    void** rdx44;
    void** r12_45;
    void** rbp46;
    void** r8_47;
    void** r13_48;
    void** rax49;
    void* rsp50;
    void** r12_51;
    void** rax52;
    void** v53;
    void* rsp54;
    uint32_t v55;
    void** rbp56;
    void** rbx57;
    unsigned char* r12_58;
    void** rax59;
    void** rsi60;
    uint64_t rcx61;
    uint64_t rdx62;
    uint64_t rax63;
    uint32_t eax64;
    void** rdx65;
    uint32_t ecx66;
    int64_t rax67;
    void* rax68;
    uint32_t tmp32_69;
    int1_t cf70;
    void** rbp71;
    void** rax72;
    uint64_t rax73;
    int1_t zf74;
    void** rax75;
    int1_t cf76;
    int1_t below_or_equal77;
    uint64_t rax78;
    void** rax79;
    uint64_t rax80;
    int1_t zf81;
    uint64_t r8_82;
    uint64_t r11_83;
    uint64_t rdx84;
    int64_t rcx85;
    uint64_t r9_86;
    int64_t rax87;
    int32_t eax88;
    int64_t rdx89;
    int64_t rax90;
    uint32_t edx91;
    uint32_t esi92;
    uint64_t rax93;
    int64_t rax94;
    uint32_t esi95;
    int64_t rdx96;
    uint32_t eax97;
    uint64_t rax98;
    uint64_t rdx99;
    uint64_t rdi100;
    uint64_t rax101;
    int32_t eax102;
    uint64_t rax103;
    void* rcx104;
    void* rax105;
    void* rax106;
    uint32_t eax107;
    uint32_t eax108;
    uint32_t edx109;
    void* rsi110;
    uint32_t edx111;
    uint32_t edx112;
    void* rax113;
    void* rsi114;
    void* rax115;
    void* rax116;
    void* rdi117;
    uint32_t eax118;
    uint32_t r9d119;
    uint32_t eax120;
    void* rdx121;
    uint32_t edx122;
    uint32_t edx123;

    __asm__("cli ");
    v6 = rsi;
    v7 = r8;
    v8 = edx;
    rax9 = g28;
    v10 = rax9;
    edx11 = edx & 32;
    v12 = edx11;
    eax13 = edx & 3;
    v14 = eax13;
    v15 = (eax13 - (eax13 + reinterpret_cast<uint1_t>(eax13 < eax13 + reinterpret_cast<uint1_t>(edx11 < 1))) & 0xffffffe8) + 0x400;
    rax16 = fun_36b0();
    r15_17 = rax16->f0;
    rax18 = fun_3750(r15_17);
    r14_19 = rax16->f8;
    rbp20 = rax18;
    cf21 = reinterpret_cast<unsigned char>(rax18 - 1) < reinterpret_cast<unsigned char>(16);
    v22 = rax16->f10;
    if (!cf21) {
        rbp20 = reinterpret_cast<void**>(1);
    }
    if (!cf21) {
        r15_17 = reinterpret_cast<void**>(".");
    }
    rax23 = fun_3750(r14_19);
    rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8 - 8 + 8);
    if (reinterpret_cast<unsigned char>(rax23) > reinterpret_cast<unsigned char>(16)) {
        r14_19 = reinterpret_cast<void**>(0x136c1);
    }
    v25 = v6 + 0x287;
    if (r8 > rcx) {
        if (!rcx || (r8_26 = v7 / rcx, !!(v7 % rcx))) {
            addr_8c44_9:
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
                __asm__("fadd dword [rip+0xa1a6]");
            }
        } else {
            rdx27 = rdi % r8_26;
            rcx28 = rdi / r8_26;
            rax29 = rdx27 + rdx27 * 4;
            rax30 = rax29 + rax29;
            rdx31 = rax30 % r8_26;
            rax32 = rax30 / r8_26;
            rdx33 = rdx31 + rdx31;
            *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            if (r8_26 <= rdx33) {
                esi35 = 2;
                if (r8_26 < rdx33) {
                    esi35 = 3;
                }
            } else {
                esi35 = 0;
                *reinterpret_cast<unsigned char*>(&esi35) = reinterpret_cast<uint1_t>(!!rdx33);
            }
            r10d36 = v8 & 16;
            if (!r10d36) 
                goto addr_91eb_17; else 
                goto addr_8ddc_18;
        }
    } else {
        if (rcx % r8) 
            goto addr_8c44_9;
        rcx28 = rcx / r8 * rdi;
        if (__intrinsic()) 
            goto addr_8c44_9;
        esi35 = 0;
        *reinterpret_cast<uint32_t*>(&rdi34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        r10d36 = v8 & 16;
        if (r10d36) 
            goto addr_8ddc_18; else 
            goto addr_91eb_17;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(v7) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xa1c6]");
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) >= reinterpret_cast<int64_t>(0)) {
            addr_8c81_24:
            __asm__("fmulp st1, st0");
            if (!(*reinterpret_cast<unsigned char*>(&v8) & 16)) {
                addr_8f78_25:
                if (v14 == 1) 
                    goto addr_900a_26;
                __asm__("fld tword [rip+0xa1d7]");
                __asm__("fcomip st0, st1");
                if (v14 <= 1) 
                    goto addr_900a_26;
            } else {
                addr_8c8e_28:
                __asm__("fild dword [rsp+0x34]");
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                __asm__("fld st0");
                goto addr_8ca4_29;
            }
        } else {
            goto addr_8f30_31;
        }
    } else {
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) 
            goto addr_8f30_31; else 
            goto addr_8c81_24;
    }
    __asm__("fld dword [rip+0xa185]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax39) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xa13d]");
    }
    zf40 = v14 == 0;
    if (zf40) 
        goto addr_8fe1_39;
    __asm__("fstp st1");
    goto addr_900a_26;
    addr_8fe1_39:
    __asm__("fxch st0, st1");
    __asm__("fucomip st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf40) {
            addr_900a_26:
            rcx41 = reinterpret_cast<void**>("%.0Lf");
            __asm__("fstp tword [rsp]");
            fun_3b50(v6, 1, -1, "%.0Lf");
            *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
            rax42 = fun_3750(v6, v6);
            rsp43 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            rdx44 = rax42;
            r12_45 = rax42;
            goto addr_9048_43;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax39 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x9be7]");
        goto addr_900a_26;
    } else {
        goto addr_900a_26;
    }
    addr_9048_43:
    rbp46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(rdx44));
    fun_3a20(rbp46, v6, rdx44, rcx41);
    rsp24 = rsp43 - 8 + 8;
    r8_47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp46) + reinterpret_cast<unsigned char>(r12_45));
    while (1) {
        if (*reinterpret_cast<unsigned char*>(&v8) & 4) {
            addr_8e70_49:
            r13_48 = reinterpret_cast<void**>(0xffffffffffffffff);
            rax49 = fun_3750(r14_19, r14_19);
            rsp50 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            r12_51 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_47) - reinterpret_cast<unsigned char>(rbp46));
            r15_17 = rax49;
            rax52 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp50) + 80);
            v53 = rax52;
            fun_38e0(rax52, rbp46, r12_51, 41);
            rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp50) - 8 + 8);
            v55 = *reinterpret_cast<uint32_t*>(&rbx38);
            rbp56 = r8_47;
            rbx57 = r12_51;
            r12_58 = v22;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rax59) = *r12_58;
                *reinterpret_cast<int32_t*>(&rax59 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&rax59)) {
                    if (reinterpret_cast<unsigned char>(r13_48) > reinterpret_cast<unsigned char>(rbx57)) {
                        r13_48 = rbx57;
                    }
                    rbx57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx57) - reinterpret_cast<unsigned char>(r13_48));
                    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v53) + reinterpret_cast<unsigned char>(rbx57));
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rax59) > 0x7e) {
                        r13_48 = rbx57;
                        rsi60 = v53;
                        *reinterpret_cast<int32_t*>(&rbx57) = 0;
                        *reinterpret_cast<int32_t*>(&rbx57 + 4) = 0;
                    } else {
                        if (reinterpret_cast<unsigned char>(rax59) > reinterpret_cast<unsigned char>(rbx57)) {
                            rax59 = rbx57;
                        }
                        rbx57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx57) - reinterpret_cast<unsigned char>(rax59));
                        r13_48 = rax59;
                        rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v53) + reinterpret_cast<unsigned char>(rbx57));
                    }
                    ++r12_58;
                }
                rbp46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp56) - reinterpret_cast<unsigned char>(r13_48));
                fun_3900(rbp46, rsi60, r13_48, rbp46, rsi60, r13_48);
                rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                if (!rbx57) 
                    break;
                rbp56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp46) - reinterpret_cast<unsigned char>(r15_17));
                fun_3900(rbp56, r14_19, r15_17, rbp56, r14_19, r15_17);
                rsp54 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            }
            *reinterpret_cast<uint32_t*>(&rbx38) = v55;
        }
        addr_906c_63:
        if (!(*reinterpret_cast<unsigned char*>(&v8) & 0x80)) 
            goto addr_908f_64;
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 0xffffffff) {
            rcx61 = v7;
            if (rcx61 <= 1) {
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                goto addr_907c_68;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx62) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx62) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx38) = 1;
                *reinterpret_cast<int32_t*>(&rax63) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax63) + 4) = 0;
                do {
                    rax63 = rax63 * rdx62;
                    if (rcx61 <= rax63) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
                } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
                eax64 = v8 & 0x100;
                if (!(v8 & 64)) 
                    goto addr_9258_73;
            }
        } else {
            addr_907c_68:
            eax64 = v8 & 0x100;
            if (eax64 | *reinterpret_cast<uint32_t*>(&rbx38)) {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 64)) {
                    addr_9250_75:
                    if (!*reinterpret_cast<uint32_t*>(&rbx38)) {
                        rdx65 = v25;
                        if (eax64) {
                            addr_929a_77:
                            *reinterpret_cast<void***>(rdx65) = reinterpret_cast<void**>(66);
                            v25 = rdx65 + 1;
                            goto addr_908f_64;
                        } else {
                            goto addr_908f_64;
                        }
                    } else {
                        addr_9258_73:
                        rdx65 = v25 + 1;
                        if (v12 || *reinterpret_cast<uint32_t*>(&rbx38) != 1) {
                            rbx38 = *reinterpret_cast<int32_t*>(&rbx38);
                            ecx66 = *reinterpret_cast<unsigned char*>(0x13150 + rbx38);
                            *reinterpret_cast<void***>(v25) = *reinterpret_cast<void***>(&ecx66);
                            if (eax64) {
                                *reinterpret_cast<uint32_t*>(&r8_47) = v12;
                                *reinterpret_cast<int32_t*>(&r8_47 + 4) = 0;
                                if (*reinterpret_cast<uint32_t*>(&r8_47)) {
                                    *reinterpret_cast<void***>(v25 + 1) = reinterpret_cast<void**>(0x69);
                                    rdx65 = v25 + 2;
                                    goto addr_929a_77;
                                }
                            }
                        } else {
                            *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0x6b);
                            if (eax64) 
                                goto addr_929a_77; else 
                                goto addr_954b_83;
                        }
                    }
                }
            } else {
                addr_908f_64:
                *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0);
                rax67 = v10 - g28;
                if (rax67) 
                    goto addr_972b_85; else 
                    break;
            }
        }
        *reinterpret_cast<signed char*>(v6 + 0x287) = 32;
        v25 = v6 + 0x288;
        goto addr_9250_75;
        addr_954b_83:
        v25 = rdx65;
        goto addr_908f_64;
        addr_972b_85:
        rax68 = fun_3780();
        rsp24 = rsp24 - 8 + 8;
        addr_9730_87:
        *reinterpret_cast<void***>(r8_47) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_47) + reinterpret_cast<uint64_t>(rax68) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax68) - 4);
        addr_9519_88:
        *reinterpret_cast<void***>(r8_47 + 0xffffffffffffffff) = reinterpret_cast<void**>(49);
        rbp46 = r8_47 + 0xffffffffffffffff;
    }
    return rbp46;
    addr_8ca4_29:
    while (tmp32_69 = *reinterpret_cast<uint32_t*>(&rbx38) + 1, cf70 = tmp32_69 < *reinterpret_cast<uint32_t*>(&rbx38), *reinterpret_cast<uint32_t*>(&rbx38) = tmp32_69, !cf70) {
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) 
            goto addr_8cb6_91;
        __asm__("fstp st1");
        __asm__("fxch st0, st2");
    }
    __asm__("fstp st2");
    __asm__("fstp st2");
    addr_8cc4_94:
    r15_17 = rbp20 + 1;
    __asm__("fdivrp st1, st0");
    rbp71 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp20 + 2) + reinterpret_cast<uint1_t>(v12 < 1));
    if (v14 == 1) {
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        rcx41 = reinterpret_cast<void**>("%.1Lf");
        __asm__("fstp tword [rsp+0x30]");
        fun_3b50(v6, 1, -1, "%.1Lf");
        rax72 = fun_3750(v6, v6);
        rsp43 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        __asm__("fld tword [rsp+0x20]");
        rdx44 = rax72;
        if (reinterpret_cast<unsigned char>(rax72) > reinterpret_cast<unsigned char>(rbp71)) {
            __asm__("fld dword [rip+0x9e5d]");
            __asm__("fmul st1, st0");
            goto addr_9301_97;
        }
    }
    __asm__("fld tword [rip+0xa47c]");
    __asm__("fcomip st0, st1");
    if (v14 <= 1) {
        __asm__("fld st0");
        goto addr_9160_100;
    }
    __asm__("fld dword [rip+0xa426]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax73 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax73 = v37;
    }
    v37 = rax73;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax73) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xa3de]");
    }
    zf74 = v14 == 0;
    if (zf74) 
        goto addr_8d42_107;
    __asm__("fxch st0, st1");
    goto addr_9160_100;
    addr_8d42_107:
    __asm__("fxch st0, st1");
    __asm__("fucomi st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st1");
    } else {
        if (zf74) {
            addr_9160_100:
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fstp tword [rsp]");
            fun_3b50(v6, 1, -1, "%.1Lf");
            rax75 = fun_3750(v6, v6);
            rdx44 = rax75;
            rcx41 = reinterpret_cast<void**>(0x8c04);
            rsp43 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            __asm__("fld tword [rsp+0x20]");
            cf76 = reinterpret_cast<unsigned char>(rdx44) < reinterpret_cast<unsigned char>(rbp71);
            below_or_equal77 = reinterpret_cast<unsigned char>(rdx44) <= reinterpret_cast<unsigned char>(rbp71);
            if (!below_or_equal77) {
                __asm__("fld dword [rip+0x9df6]");
                __asm__("fmul st1, st0");
                goto addr_9368_112;
            } else {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 8)) {
                    __asm__("fstp st0");
                    goto addr_91ca_115;
                } else {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v6) + reinterpret_cast<unsigned char>(rdx44) + 0xffffffffffffffff) == 48) {
                        __asm__("fld dword [rip+0x9cd6]");
                        cf76 = v14 < 1;
                        below_or_equal77 = v14 <= 1;
                        __asm__("fmul st1, st0");
                        if (v14 == 1) {
                            goto addr_9301_97;
                        }
                    } else {
                        __asm__("fstp st0");
                        goto addr_91ca_115;
                    }
                }
            }
        } else {
            __asm__("fstp st1");
        }
    }
    rax78 = rax73 + 1;
    v37 = rax78;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax78) >= reinterpret_cast<int64_t>(0)) {
        __asm__("fxch st0, st1");
        goto addr_9160_100;
    } else {
        __asm__("fadd dword [rip+0xa3a3]");
        __asm__("fxch st0, st1");
        goto addr_9160_100;
    }
    addr_9368_112:
    __asm__("fld tword [rip+0x9df2]");
    __asm__("fcomip st0, st2");
    if (below_or_equal77) {
        addr_9301_97:
        __asm__("fdivp st1, st0");
        rcx41 = reinterpret_cast<void**>("%.0Lf");
        __asm__("fstp tword [rsp]");
        fun_3b50(v6, 1, -1, "%.0Lf");
        rax79 = fun_3750(v6, v6);
        r15_17 = reinterpret_cast<void**>(0x8c04);
        rsp43 = rsp43 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        rdx44 = rax79;
        r12_45 = rax79;
        goto addr_9048_43;
    } else {
        __asm__("fld dword [rip+0x9da0]");
        __asm__("fxch st0, st2");
        __asm__("fcomi st0, st2");
        if (!cf76) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fsubr st2, st0");
            __asm__("fxch st0, st2");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fxch st0, st1");
            rax80 = v37;
            __asm__("btc rax, 0x3f");
        } else {
            __asm__("fstp st2");
            __asm__("fxch st0, st1");
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld st0");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            rax80 = v37;
        }
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rax80) < reinterpret_cast<int64_t>(0)) {
            __asm__("fadd dword [rip+0x9d56]");
        }
        zf81 = v14 == 0;
        if (zf81) 
            goto addr_93c6_130;
    }
    __asm__("fstp st1");
    goto addr_93f2_132;
    addr_93c6_130:
    __asm__("fucomi st0, st1");
    __asm__("fstp st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf81) {
            addr_93f2_132:
            __asm__("fxch st0, st1");
            goto addr_9301_97;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax80 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x9bdc]");
        __asm__("fxch st0, st1");
        goto addr_9301_97;
    } else {
        goto addr_93f2_132;
    }
    addr_91ca_115:
    r12_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx44) - reinterpret_cast<unsigned char>(r15_17));
    goto addr_9048_43;
    addr_8cb6_91:
    __asm__("fstp st2");
    __asm__("fstp st2");
    goto addr_8cc4_94;
    addr_8f30_31:
    __asm__("fadd dword [rip+0xa1de]");
    __asm__("fmulp st1, st0");
    if (*reinterpret_cast<unsigned char*>(&v8) & 16) 
        goto addr_8c8e_28;
    goto addr_8f78_25;
    addr_91eb_17:
    *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
    goto addr_8def_140;
    addr_8ddc_18:
    *reinterpret_cast<uint32_t*>(&r8_82) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_82) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbx38) = 0;
    r11_83 = r8_82;
    if (r8_82 > rcx28) 
        goto addr_8def_140;
    do {
        rdx84 = rcx28 % r8_82;
        *reinterpret_cast<int32_t*>(&rcx85) = reinterpret_cast<int32_t>(esi35) >> 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx85) + 4) = 0;
        r9_86 = rcx28 / r8_82;
        *reinterpret_cast<int32_t*>(&rax87) = static_cast<int32_t>(rdx84 + rdx84 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
        eax88 = static_cast<int32_t>(rdi34 + rax87 * 2);
        *reinterpret_cast<uint32_t*>(&rdx89) = eax88 % *reinterpret_cast<uint32_t*>(&r11_83);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx89) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax90) = eax88 / *reinterpret_cast<uint32_t*>(&r11_83);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax90) + 4) = 0;
        edx91 = static_cast<uint32_t>(rcx85 + rdx89 * 2);
        *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax90);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        rcx28 = r9_86;
        esi92 = esi35 + edx91;
        if (*reinterpret_cast<uint32_t*>(&r11_83) > edx91) {
            esi35 = reinterpret_cast<uint1_t>(!!esi92);
        } else {
            esi35 = static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r11_83) < esi92)) + 2;
        }
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (r8_82 > r9_86) 
            break;
    } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
    goto addr_8def_140;
    if (r9_86 > 9) {
        addr_8def_140:
        r8_47 = v25;
        if (v14 == 1) {
            rax93 = rcx28;
            *reinterpret_cast<uint32_t*>(&rax94) = *reinterpret_cast<uint32_t*>(&rax93) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax94) + 4) = 0;
            if (reinterpret_cast<int32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!(rax94 + esi35))) + *reinterpret_cast<uint32_t*>(&rdi34)) <= reinterpret_cast<int32_t>(5)) {
                goto addr_8e28_149;
            }
        } else {
            if (v14) 
                goto addr_8e28_149;
            esi95 = esi35 + *reinterpret_cast<uint32_t*>(&rdi34);
            if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(esi95) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(esi95 == 0)) 
                goto addr_8e28_149;
        }
    } else {
        if (v14 != 1) {
            if (v14) 
                goto addr_95ec_154;
            if (!esi35) 
                goto addr_95ec_154; else 
                goto addr_9577_156;
        }
        if (reinterpret_cast<int32_t>((*reinterpret_cast<uint32_t*>(&rax90) & 1) + esi35) > reinterpret_cast<int32_t>(2)) {
            addr_9577_156:
            *reinterpret_cast<int32_t*>(&rdx96) = static_cast<int32_t>(rax90 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx96) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax90) == 9) {
                rcx28 = r9_86 + 1;
                if (r9_86 == 9) {
                    r8_47 = v25;
                    goto addr_95ba_160;
                } else {
                    esi35 = 0;
                    goto addr_95f4_162;
                }
            } else {
                eax97 = static_cast<uint32_t>(rdx96 + 48);
                goto addr_9586_164;
            }
        } else {
            addr_95ec_154:
            if (*reinterpret_cast<uint32_t*>(&rax90)) {
                eax97 = *reinterpret_cast<uint32_t*>(&rax90) + 48;
                goto addr_9586_164;
            } else {
                addr_95f4_162:
                r8_47 = v25;
                if (*reinterpret_cast<unsigned char*>(&v8) & 8) {
                    addr_95bc_166:
                    *reinterpret_cast<uint32_t*>(&rdi34) = 0;
                    if (v14 == 1) {
                        goto addr_8e28_149;
                    }
                } else {
                    eax97 = 48;
                    goto addr_9586_164;
                }
            }
        }
    }
    ++rcx28;
    if (!r10d36) 
        goto addr_8e28_149;
    *reinterpret_cast<uint32_t*>(&rax98) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax98) + 4) = 0;
    if (rax98 != rcx28) {
        goto addr_8e28_149;
    }
    if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) {
        addr_8e28_149:
        rbp46 = r8_47;
    } else {
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (*reinterpret_cast<unsigned char*>(&v8) & 8) 
            goto addr_9519_88;
        *reinterpret_cast<void***>(r8_47 + 0xffffffffffffffff) = reinterpret_cast<void**>(48);
        r8_47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_47) + reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rbp20)));
        *reinterpret_cast<uint32_t*>(&rax68) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax68) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) 
            goto addr_96c0_175; else 
            goto addr_968e_176;
    }
    do {
        --rbp46;
        rdx99 = __intrinsic() >> 3;
        rdi100 = rdx99 + rdx99 * 4;
        rax101 = rcx28 - (rdi100 + rdi100);
        eax102 = *reinterpret_cast<int32_t*>(&rax101) + 48;
        *reinterpret_cast<void***>(rbp46) = *reinterpret_cast<void***>(&eax102);
        rax103 = rcx28;
        rcx28 = rdx99;
    } while (rax103 > 9);
    if (!(*reinterpret_cast<unsigned char*>(&v8) & 4)) 
        goto addr_906c_63; else 
        goto addr_8e70_49;
    addr_96c0_175:
    rcx104 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_47 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(r8_47) = *reinterpret_cast<void***>(r15_17);
    *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_47) + reinterpret_cast<uint64_t>(rax105) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax105) - 8);
    rax106 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_47) - reinterpret_cast<uint64_t>(rcx104));
    r15_17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax106));
    eax107 = *reinterpret_cast<int32_t*>(&rax106) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
    if (eax107 < 8) 
        goto addr_9519_88;
    eax108 = eax107 & 0xfffffff8;
    edx109 = 0;
    do {
        *reinterpret_cast<uint32_t*>(&rsi110) = edx109;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi110) + 4) = 0;
        edx109 = edx109 + 8;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx104) + reinterpret_cast<uint64_t>(rsi110)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rsi110));
    } while (edx109 < eax108);
    goto addr_9519_88;
    addr_968e_176:
    if (*reinterpret_cast<uint32_t*>(&rbp20) & 4) 
        goto addr_9730_87;
    if (!*reinterpret_cast<uint32_t*>(&rax68)) 
        goto addr_9519_88;
    edx111 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17));
    *reinterpret_cast<void***>(r8_47) = *reinterpret_cast<void***>(&edx111);
    if (!(*reinterpret_cast<unsigned char*>(&rax68) & 2)) 
        goto addr_9519_88;
    edx112 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax68) - 2);
    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_47) + reinterpret_cast<uint64_t>(rax68) - 2) = *reinterpret_cast<int16_t*>(&edx112);
    goto addr_9519_88;
    addr_95ba_160:
    esi35 = 0;
    goto addr_95bc_166;
    addr_9586_164:
    *reinterpret_cast<signed char*>(v6 + 0x286) = *reinterpret_cast<signed char*>(&eax97);
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    r8_47 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v6 + 0x286) - reinterpret_cast<unsigned char>(rbp20));
    if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) {
        rsi114 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_47 + 8) & 0xfffffffffffffff8);
        *reinterpret_cast<void***>(r8_47) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
        *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_47) + reinterpret_cast<uint64_t>(rax115) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax115) - 8);
        rax116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_47) - reinterpret_cast<uint64_t>(rsi114));
        rdi117 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax116));
        eax118 = *reinterpret_cast<int32_t*>(&rax116) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
        if (eax118 >= 8) {
            r9d119 = eax118 & 0xfffffff8;
            eax120 = 0;
            do {
                *reinterpret_cast<uint32_t*>(&rdx121) = eax120;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx121) + 4) = 0;
                eax120 = eax120 + 8;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsi114) + reinterpret_cast<int64_t>(rdx121)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi117) + reinterpret_cast<int64_t>(rdx121));
            } while (eax120 < r9d119);
            goto addr_95ba_160;
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&rbp20) & 4) {
            *reinterpret_cast<void***>(r8_47) = *reinterpret_cast<void***>(r15_17);
            *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_47) + reinterpret_cast<uint64_t>(rax113) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax113) - 4);
            goto addr_95ba_160;
        } else {
            if (*reinterpret_cast<uint32_t*>(&rax113) && (edx122 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17)), *reinterpret_cast<void***>(r8_47) = *reinterpret_cast<void***>(&edx122), !!(*reinterpret_cast<unsigned char*>(&rax113) & 2))) {
                edx123 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax113) - 2);
                *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_47) + reinterpret_cast<uint64_t>(rax113) - 2) = *reinterpret_cast<int16_t*>(&edx123);
                goto addr_95ba_160;
            }
        }
    }
}

int64_t argmatch(void** rdi, void** rsi, int64_t rdx, int64_t rcx);

int64_t xstrtoumax(void** rdi, void** rsi);

struct s46 {
    signed char[512] pad512;
    void** f200;
};

int64_t fun_9763(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** rbp8;
    void** rbx9;
    void* rsp10;
    int64_t rax11;
    int64_t v12;
    void** rax13;
    void** rax14;
    void** r12d15;
    int64_t rax16;
    void** rax17;
    int64_t rax18;
    void** rsi19;
    void** rcx20;
    void** rdx21;
    int64_t rcx22;
    int32_t edx23;
    void** v24;
    int64_t rdi25;
    int32_t edx26;
    void** rax27;
    uint64_t rax28;
    struct s46* rax29;
    int64_t rdx30;

    __asm__("cli ");
    r13_7 = rsi;
    rbp8 = rdx;
    rbx9 = rdi;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16);
    rax11 = g28;
    v12 = rax11;
    if (rdi || ((rax13 = fun_35d0("BLOCK_SIZE", rsi, rdx, rcx, r8, r9), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), rbx9 = rax13, !!rax13) || (rax14 = fun_35d0("BLOCKSIZE", rsi, rdx, rcx, r8, r9), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), rbx9 = rax14, !!rax14))) {
        r12d15 = reinterpret_cast<void**>(0);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx9) == 39)) {
            ++rbx9;
            r12d15 = reinterpret_cast<void**>(4);
        }
        rax16 = argmatch(rbx9, 0x17a80, 0x13148, 4);
        if (*reinterpret_cast<int32_t*>(&rax16) >= 0) 
            goto addr_97c6_5;
    } else {
        rax17 = fun_35d0("POSIXLY_CORRECT", rsi, rdx, rcx, r8, r9);
        if (!rax17) {
            *reinterpret_cast<void***>(rbp8) = reinterpret_cast<void**>(0x400);
            *reinterpret_cast<int32_t*>(&rax18) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
            *reinterpret_cast<void***>(r13_7) = reinterpret_cast<void**>(0);
            goto addr_97da_8;
        } else {
            *reinterpret_cast<void***>(rbp8) = reinterpret_cast<void**>(0x200);
            *reinterpret_cast<int32_t*>(&rax18) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
            *reinterpret_cast<void***>(r13_7) = reinterpret_cast<void**>(0);
            goto addr_97da_8;
        }
    }
    rsi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
    rcx20 = rbp8;
    rax18 = xstrtoumax(rbx9, rsi19);
    if (*reinterpret_cast<int32_t*>(&rax18)) {
        *reinterpret_cast<void***>(r13_7) = reinterpret_cast<void**>(0);
        rdx21 = *reinterpret_cast<void***>(rbp8);
    } else {
        *reinterpret_cast<uint32_t*>(&rcx22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx9));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
        edx23 = static_cast<int32_t>(rcx22 - 48);
        rcx20 = v24;
        if (*reinterpret_cast<unsigned char*>(&edx23) <= 9) {
            goto addr_9857_14;
        }
        do {
            if (rcx20 == rbx9) 
                break;
            *reinterpret_cast<uint32_t*>(&rdi25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx9 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
            ++rbx9;
            edx26 = static_cast<int32_t>(rdi25 - 48);
        } while (*reinterpret_cast<unsigned char*>(&edx26) > 9);
        goto addr_9857_14;
        if (*reinterpret_cast<void***>(rcx20 + 0xffffffffffffffff) == 66) 
            goto addr_9910_18; else 
            goto addr_984f_19;
    }
    addr_9874_20:
    if (!rdx21) {
        rax27 = fun_35d0("POSIXLY_CORRECT", rsi19, rdx21, rcx20, "eEgGkKmMpPtTyYzZ0", r9);
        rax28 = reinterpret_cast<unsigned char>(rax27) - (reinterpret_cast<unsigned char>(rax27) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax27) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax27) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax27) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax29) = *reinterpret_cast<uint32_t*>(&rax28) & 0x200;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        *reinterpret_cast<void***>(rbp8) = reinterpret_cast<void**>(&rax29->f200);
        *reinterpret_cast<int32_t*>(&rax18) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    }
    addr_97da_8:
    rdx30 = v12 - g28;
    if (rdx30) {
        fun_3780();
    } else {
        return rax18;
    }
    addr_9910_18:
    r12d15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d15) | 0x180);
    if (*reinterpret_cast<signed char*>(rcx20 + 0xfffffffffffffffe) != 0x69) {
        addr_9857_14:
        rdx21 = *reinterpret_cast<void***>(rbp8);
        *reinterpret_cast<void***>(r13_7) = r12d15;
        goto addr_9874_20;
    }
    addr_9853_25:
    r12d15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d15) | 32);
    goto addr_9857_14;
    addr_984f_19:
    *reinterpret_cast<unsigned char*>(&r12d15) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d15) | 0x80);
    goto addr_9853_25;
    addr_97c6_5:
    *reinterpret_cast<void***>(rbp8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<int32_t*>(&rax18) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    *reinterpret_cast<void***>(r13_7) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d15) | *reinterpret_cast<uint32_t*>(0x13148 + *reinterpret_cast<int32_t*>(&rax16) * 4));
    goto addr_97da_8;
}

struct s47 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_9953(uint64_t rdi, struct s47* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

uint64_t fun_37a0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_99b3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_37a0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_3780();
    } else {
        return r12_7;
    }
}

struct s48 {
    signed char[1] pad1;
    void** f1;
};

struct s48* fun_36d0();

int32_t fun_36a0(void** rdi, void** rsi, void** rdx);

int32_t fun_3940();

void** fun_3aa0();

int32_t fun_3b00();

void** fun_9a43(void** rdi, void** rsi, void* rdx, void*** rcx, int32_t r8d, uint32_t r9d) {
    void** r13_7;
    uint32_t ebp8;
    void** rbx9;
    void* v10;
    void*** v11;
    int32_t v12;
    void** rax13;
    void** v14;
    void** r9_15;
    uint64_t rax16;
    void** r12_17;
    void** r15_18;
    void** r14_19;
    struct s48* rax20;
    void** r8_21;
    void** rax22;
    void** rax23;
    void** r8_24;
    void** rdx25;
    struct s48* rax26;
    void** r8_27;
    void** rsi28;
    int32_t eax29;
    void** rax30;
    void** v31;
    void** rax32;
    void* rdx33;
    int32_t eax34;
    void** edi35;
    void** r13_36;
    void** v37;
    int32_t eax38;
    void** rax39;
    void* r12_40;
    void* rdx41;
    void** rbp42;
    void** rdi43;
    void** rdx44;
    void** rax45;
    void** rdx46;
    void** rax47;
    void** r12_48;
    int32_t eax49;
    int32_t eax50;
    void** rax51;

    __asm__("cli ");
    r13_7 = rdi;
    ebp8 = r9d;
    rbx9 = rsi;
    v10 = rdx;
    v11 = rcx;
    v12 = r8d;
    rax13 = fun_3750(rdi);
    v14 = rax13;
    r9_15 = rax13;
    if (*reinterpret_cast<unsigned char*>(&ebp8) & 2 || (rax16 = fun_3740(), r9_15 = rax13, rax16 <= 1)) {
        addr_9a81_2:
        r12_17 = r9_15;
        *reinterpret_cast<int32_t*>(&r15_18) = 0;
        *reinterpret_cast<int32_t*>(&r15_18 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r14_19) = 0;
        *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
        goto addr_9a8a_3;
    } else {
        rax20 = fun_36d0();
        r9_15 = rax13;
        if (!reinterpret_cast<int1_t>(rax20 == -1)) {
            r8_21 = reinterpret_cast<void**>(&rax20->f1);
            rax22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_21) * 4);
            rax23 = fun_3950(rax22, r13_7);
            r9_15 = r9_15;
            r15_18 = rax23;
            if (!rax23) {
                if (!(*reinterpret_cast<unsigned char*>(&ebp8) & 1)) {
                    addr_9c1a_7:
                    *reinterpret_cast<int32_t*>(&r15_18) = 0;
                    *reinterpret_cast<int32_t*>(&r15_18 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r14_19) = 0;
                    *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
                    r8_24 = reinterpret_cast<void**>(0xffffffffffffffff);
                } else {
                    r12_17 = r9_15;
                    *reinterpret_cast<int32_t*>(&r14_19) = 0;
                    *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
                    goto addr_9a8a_3;
                }
            } else {
                rdx25 = r8_21;
                *reinterpret_cast<int32_t*>(&r14_19) = 0;
                *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
                rax26 = fun_36d0();
                r9_15 = r9_15;
                r8_27 = r8_21;
                r12_17 = r9_15;
                if (!rax26) 
                    goto addr_9a8a_3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rax22) + 0xfffffffffffffffc) = 0;
                if (!*reinterpret_cast<void***>(r15_18)) 
                    goto addr_9ddb_11; else 
                    goto addr_9cb3_12;
            }
        } else {
            if (*reinterpret_cast<unsigned char*>(&ebp8) & 1) 
                goto addr_9a81_2; else 
                goto addr_9c1a_7;
        }
    }
    addr_9b6b_14:
    fun_35f0(r15_18, r15_18);
    fun_35f0(r14_19, r14_19);
    return r8_24;
    addr_9ddb_11:
    rsi28 = r8_27;
    eax29 = fun_36a0(r15_18, rsi28, rdx25);
    r9_15 = r9_15;
    r12_17 = reinterpret_cast<void**>(static_cast<int64_t>(eax29));
    addr_9df3_15:
    rax30 = *v11;
    if (reinterpret_cast<unsigned char>(rax30) >= reinterpret_cast<unsigned char>(r12_17)) {
        *reinterpret_cast<int32_t*>(&r14_19) = 0;
        *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
        goto addr_9b9b_17;
    } else {
        v31 = v14 + 1;
    }
    addr_9d38_19:
    rax32 = fun_3950(v31, rsi28, v31, rsi28);
    r9_15 = r9_15;
    r14_19 = rax32;
    if (!rax32) {
        r8_24 = reinterpret_cast<void**>(0xffffffffffffffff);
        if (*reinterpret_cast<unsigned char*>(&ebp8) & 1) {
            addr_9a8a_3:
            rax30 = *v11;
            if (reinterpret_cast<unsigned char>(rax30) >= reinterpret_cast<unsigned char>(r12_17)) {
                addr_9b9b_17:
                if (reinterpret_cast<unsigned char>(r12_17) >= reinterpret_cast<unsigned char>(rax30)) {
                    rax30 = r12_17;
                    *reinterpret_cast<uint32_t*>(&rdx33) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
                } else {
                    rdx33 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax30) - reinterpret_cast<unsigned char>(r12_17));
                    *v11 = r12_17;
                    eax34 = v12;
                    if (eax34) 
                        goto addr_9ab4_23; else 
                        goto addr_9bc1_24;
                }
            } else {
                r9_15 = rax30;
                *reinterpret_cast<uint32_t*>(&rdx33) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
            }
        } else {
            goto addr_9b6b_14;
        }
    } else {
        edi35 = *reinterpret_cast<void***>(r15_18);
        r13_36 = r15_18;
        *reinterpret_cast<int32_t*>(&r12_17) = 0;
        *reinterpret_cast<int32_t*>(&r12_17 + 4) = 0;
        v37 = *v11;
        if (edi35) {
            do {
                eax38 = fun_3940();
                if (eax38 != -1) {
                    rax39 = reinterpret_cast<void**>(static_cast<int64_t>(eax38) + reinterpret_cast<unsigned char>(r12_17));
                    if (reinterpret_cast<unsigned char>(v37) < reinterpret_cast<unsigned char>(rax39)) 
                        goto addr_9db8_30;
                } else {
                    *reinterpret_cast<void***>(r13_36) = reinterpret_cast<void**>("D");
                    rax39 = r12_17 + 1;
                    if (reinterpret_cast<unsigned char>(v37) < reinterpret_cast<unsigned char>(rax39)) 
                        goto addr_9db4_32;
                }
                r13_36 = r13_36 + 4;
                r12_17 = rax39;
            } while (*reinterpret_cast<int32_t*>(r13_36 + 4));
            goto addr_9db8_30;
        } else {
            goto addr_9db8_30;
        }
    }
    *v11 = rax30;
    eax34 = v12;
    if (!eax34) {
        addr_9bc1_24:
        r12_40 = rdx33;
        *reinterpret_cast<uint32_t*>(&rdx33) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
    } else {
        addr_9ab4_23:
        *reinterpret_cast<int32_t*>(&r12_40) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_40) + 4) = 0;
        if (eax34 != 1) {
            *reinterpret_cast<uint32_t*>(&rdx41) = *reinterpret_cast<uint32_t*>(&rdx33) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx41) + 4) = 0;
            r12_40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx33) >> 1);
            rdx33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx41) + reinterpret_cast<uint64_t>(r12_40));
        }
    }
    r8_24 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx33) + reinterpret_cast<unsigned char>(r9_15));
    if (*reinterpret_cast<unsigned char*>(&ebp8) & 4) {
        r8_24 = r9_15;
        *reinterpret_cast<uint32_t*>(&rdx33) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
    }
    if (ebp8 & 8) {
        *reinterpret_cast<int32_t*>(&r12_40) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_40) + 4) = 0;
    } else {
        r8_24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_24) + reinterpret_cast<uint64_t>(r12_40));
    }
    if (v10) {
        rbp42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx9) + reinterpret_cast<uint64_t>(v10) + 0xffffffffffffffff);
        rdi43 = rbx9;
        if (reinterpret_cast<unsigned char>(rbx9) < reinterpret_cast<unsigned char>(rbp42)) {
            if (rdx33) {
                do {
                    ++rdi43;
                    *reinterpret_cast<void***>(rdi43 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
                    if (!(reinterpret_cast<unsigned char>(rbx9) - reinterpret_cast<unsigned char>(rdi43) + reinterpret_cast<uint64_t>(rdx33))) 
                        break;
                } while (reinterpret_cast<unsigned char>(rbp42) > reinterpret_cast<unsigned char>(rdi43));
            }
        }
        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp42) - reinterpret_cast<unsigned char>(rdi43));
        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r9_15)) {
            rdx44 = r9_15;
        }
        rax45 = fun_3a10(rdi43, r13_7, rdx44);
        r8_24 = r8_24;
        rdx46 = rax45;
        if (reinterpret_cast<unsigned char>(rbp42) > reinterpret_cast<unsigned char>(rax45)) {
            if (r12_40) {
                do {
                    ++rdx46;
                    *reinterpret_cast<void***>(rdx46 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
                    if (!(reinterpret_cast<uint64_t>(r12_40) - reinterpret_cast<unsigned char>(rdx46) + reinterpret_cast<unsigned char>(rax45))) 
                        break;
                } while (reinterpret_cast<unsigned char>(rbp42) > reinterpret_cast<unsigned char>(rdx46));
            }
        }
        *reinterpret_cast<void***>(rdx46) = reinterpret_cast<void**>(0);
        goto addr_9b6b_14;
    }
    addr_9db8_30:
    *reinterpret_cast<void***>(r13_36) = reinterpret_cast<void**>(0);
    r13_7 = r14_19;
    rax47 = fun_3aa0();
    r9_15 = rax47;
    goto addr_9a8a_3;
    addr_9db4_32:
    goto addr_9db8_30;
    addr_9cb3_12:
    r12_48 = r15_18;
    do {
        eax49 = fun_3b00();
        r9_15 = r9_15;
        r8_27 = r8_27;
        if (!eax49) {
            *reinterpret_cast<void***>(r12_48) = reinterpret_cast<void**>("D");
            *reinterpret_cast<int32_t*>(&r14_19) = 1;
        }
        r12_48 = r12_48 + 4;
    } while (*reinterpret_cast<int32_t*>(r12_48 + 4));
    rsi28 = r8_27;
    eax50 = fun_36a0(r15_18, rsi28, rdx25);
    r9_15 = r9_15;
    r12_17 = reinterpret_cast<void**>(static_cast<int64_t>(eax50));
    if (!*reinterpret_cast<signed char*>(&r14_19)) 
        goto addr_9df3_15;
    rsi28 = r15_18;
    rax51 = fun_3aa0();
    r9_15 = r9_15;
    v31 = rax51 + 1;
    goto addr_9d38_19;
}

void** fun_39e0(void** rdi, void** rsi, ...);

void** mbsalign(int64_t rdi, void** rsi, void** rdx, void*** rcx, int64_t r8, int64_t r9);

void** fun_9e53(int64_t rdi, void*** rsi, int32_t edx, int32_t ecx) {
    int64_t r14_5;
    int32_t r13d6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    int32_t v10;
    void** v11;
    void** rbp12;
    void** r15_13;
    void** rax14;
    int64_t r9_15;
    int64_t r8_16;
    void** rdi17;

    __asm__("cli ");
    r14_5 = rdi;
    r13d6 = edx;
    *reinterpret_cast<int32_t*>(&r12_7) = 0;
    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
    rbx8 = rsi;
    rax9 = *rsi;
    v10 = ecx;
    v11 = rax9;
    do {
        rbp12 = rax9 + 1;
        r15_13 = r12_7;
        rax14 = fun_39e0(r12_7, rbp12, r12_7, rbp12);
        r12_7 = rax14;
        if (!rax14) 
            break;
        *reinterpret_cast<int32_t*>(&r9_15) = v10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_15) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_16) = r13d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_16) + 4) = 0;
        *rbx8 = v11;
        rax9 = mbsalign(r14_5, r12_7, rbp12, rbx8, r8_16, r9_15);
        if (rax9 == 0xffffffffffffffff) 
            goto addr_9ee0_4;
    } while (reinterpret_cast<unsigned char>(rbp12) <= reinterpret_cast<unsigned char>(rax9));
    goto addr_9ec5_6;
    fun_35f0(r15_13, r15_13);
    addr_9ec5_6:
    return r12_7;
    addr_9ee0_4:
    rdi17 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_7) = 0;
    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
    fun_35f0(rdi17, rdi17);
    goto addr_9ec5_6;
}

uint16_t** fun_3b40(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t fun_3af0(void** rdi, void** rsi);

int64_t fun_9f03(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** rbp6;
    uint32_t v7;
    int64_t rax8;
    int64_t v9;
    uint64_t rax10;
    void* rsp11;
    int32_t r12d12;
    int64_t rax13;
    int64_t rax14;
    uint16_t** rax15;
    uint16_t* rdx16;
    uint32_t ecx17;
    int64_t rax18;
    uint32_t eax19;
    void** r13_20;
    void* r14_21;
    uint32_t v22;
    uint32_t eax23;
    uint32_t eax24;
    void** rsi25;
    void** rdx26;
    void** rax27;
    void** rbx28;
    int32_t eax29;
    int64_t rdi30;
    int32_t v31;
    int32_t eax32;
    uint32_t eax33;
    uint32_t eax34;

    __asm__("cli ");
    r15_5 = rdi;
    rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
    v7 = *reinterpret_cast<uint32_t*>(&rdx);
    rax8 = g28;
    v9 = rax8;
    rax10 = fun_3740();
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    if (rax10 <= 1) {
        r12d12 = 0;
        if (reinterpret_cast<unsigned char>(r15_5) >= reinterpret_cast<unsigned char>(rbp6)) {
            addr_a048_3:
            rax13 = v9 - g28;
            if (rax13) {
                fun_3780();
            } else {
                *reinterpret_cast<int32_t*>(&rax14) = r12d12;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                return rax14;
            }
        } else {
            rax15 = fun_3b40(rdi, rsi, rdx, rcx);
            r12d12 = 0;
            rdx16 = *rax15;
            ecx17 = v7 & 2;
            do {
                *reinterpret_cast<uint32_t*>(&rax18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_5));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
                ++r15_5;
                eax19 = rdx16[rax18];
                if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax19) + 1) & 64) {
                    addr_a0b9_8:
                    if (r12d12 == 0x7fffffff) 
                        goto addr_a048_3;
                } else {
                    if (ecx17) 
                        goto addr_a0e8_10;
                    if (*reinterpret_cast<unsigned char*>(&eax19) & 2) 
                        continue; else 
                        goto addr_a0b9_8;
                }
                ++r12d12;
            } while (rbp6 != r15_5);
        }
    } else {
        r12d12 = 0;
        if (reinterpret_cast<unsigned char>(r15_5) >= reinterpret_cast<unsigned char>(rbp6)) 
            goto addr_a048_3;
        r13_20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp11) + 32);
        r14_21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 28);
        v22 = v7 & 2;
        do {
            eax23 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_5));
            if (*reinterpret_cast<signed char*>(&eax23) > 95) {
                eax24 = eax23 - 97;
                if (*reinterpret_cast<unsigned char*>(&eax24) <= 29) {
                    addr_a033_18:
                    ++r15_5;
                    ++r12d12;
                    continue;
                }
            } else {
                if (*reinterpret_cast<signed char*>(&eax23) > 64) 
                    goto addr_a033_18;
                if (*reinterpret_cast<signed char*>(&eax23) > 35) 
                    goto addr_a028_22; else 
                    goto addr_9f7c_23;
            }
            addr_9f84_24:
            do {
                rsi25 = r15_5;
                rdx26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) - reinterpret_cast<unsigned char>(r15_5));
                rax27 = rpl_mbrtowc(r14_21, rsi25, rdx26, r13_20);
                if (rax27 == 0xffffffffffffffff) 
                    break;
                if (rax27 == 0xfffffffffffffffe) 
                    goto addr_a0f8_27;
                *reinterpret_cast<int32_t*>(&rbx28) = 1;
                *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
                if (rax27) {
                    rbx28 = rax27;
                }
                eax29 = fun_3940();
                if (eax29 >= 0) {
                    if (0x7fffffff - r12d12 < eax29) 
                        goto addr_a110_32;
                    r12d12 = r12d12 + eax29;
                    continue;
                } else {
                    if (v22) 
                        goto addr_a0e8_10;
                    *reinterpret_cast<int32_t*>(&rdi30) = v31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
                    eax32 = fun_3680(rdi30, rsi25, rdx26, r13_20);
                    if (eax32) 
                        continue;
                }
                if (r12d12 == 0x7fffffff) 
                    goto addr_a110_32;
                ++r12d12;
                r15_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_5) + reinterpret_cast<unsigned char>(rbx28));
                eax33 = fun_3af0(r13_20, rsi25);
            } while (!eax33);
            continue;
            if (!(*reinterpret_cast<unsigned char*>(&v7) & 1)) 
                goto addr_a033_18; else 
                goto addr_a0e3_40;
            addr_a0f8_27:
            if (*reinterpret_cast<unsigned char*>(&v7) & 1) 
                goto addr_a0e8_10;
            ++r12d12;
            r15_5 = rbp6;
            continue;
            addr_a028_22:
            eax34 = eax23 - 37;
            if (*reinterpret_cast<unsigned char*>(&eax34) > 26) 
                goto addr_9f84_24; else 
                goto addr_a033_18;
            addr_9f7c_23:
            if (*reinterpret_cast<signed char*>(&eax23) > 31) 
                goto addr_a033_18; else 
                goto addr_9f84_24;
        } while (reinterpret_cast<unsigned char>(r15_5) < reinterpret_cast<unsigned char>(rbp6));
        goto addr_a044_43;
    }
    goto addr_a048_3;
    addr_a0e8_10:
    r12d12 = -1;
    goto addr_a048_3;
    addr_a044_43:
    goto addr_a048_3;
    addr_a110_32:
    r12d12 = 0x7fffffff;
    goto addr_a048_3;
    addr_a0e3_40:
    goto addr_a0e8_10;
}

void fun_a123(void** rdi, int32_t esi) {
    __asm__("cli ");
    fun_3750(rdi, rdi);
}

void** fun_3a70(int64_t rdi, int64_t rsi);

void** fun_3980(int64_t rdi, void** rsi);

int64_t fun_38b0(void** rdi, void* rsi, void** rdx, void** rcx, void** r8);

int32_t fun_39a0(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

struct s49 {
    signed char[3] pad3;
    void** f3;
};

struct s49* fun_3b30(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

int64_t rpl_fclose(void** rdi, void* rsi, void** rdx, void** rcx, void** r8);

void** fun_3970(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t fun_3b10(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_3600(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fun_a1e3() {
    int64_t rax1;
    int64_t v2;
    void** rax3;
    void**** rsp4;
    void** rsi5;
    void** rax6;
    void** r14_7;
    void** r15_8;
    void*** v9;
    void** r12_10;
    void* r14_11;
    void** rbx12;
    void** rcx13;
    void** r8_14;
    int64_t rax15;
    void* rsp16;
    void** rcx17;
    void** rdx18;
    int32_t eax19;
    void** r13_20;
    int32_t v21;
    struct s0* rax22;
    void** rbp23;
    struct s0* rax24;
    struct s49* rax25;
    void** rax26;
    void** v27;
    struct s0* rax28;
    void** v29;
    struct s0* rax30;
    void** rax31;
    void** r9_32;
    void** rax33;
    void** r9_34;
    void** rax35;
    uint32_t r13d36;
    void** r9_37;
    void** rax38;
    void** r9_39;
    void** rax40;
    uint64_t rcx41;
    int32_t v42;
    uint64_t rax43;
    int32_t v44;
    uint64_t rsi45;
    uint64_t rsi46;
    uint64_t rsi47;
    int64_t rax48;
    void* rsp49;
    void** rdx50;
    int64_t rax51;
    int64_t rax52;
    int64_t rax53;
    int64_t rax54;
    int64_t rax55;
    int64_t rax56;
    int64_t rax57;
    int64_t rax58;
    int64_t rax59;
    int64_t rax60;
    int64_t rax61;
    int64_t rax62;
    int64_t rax63;
    uint32_t eax64;
    uint32_t eax65;
    struct s0* rax66;
    void** rdx67;
    int32_t ecx68;
    int64_t rax69;
    int64_t rax70;
    int64_t rax71;
    uint32_t eax72;
    int64_t rax73;
    int64_t rax74;
    int64_t rax75;
    int64_t rax76;
    int64_t rax77;
    int64_t rax78;
    int64_t rax79;
    int64_t rax80;
    int64_t rax81;
    int64_t rax82;
    void** rax83;
    void** r14d84;
    int64_t rax85;
    int64_t rax86;
    int64_t rax87;
    int64_t rax88;
    int64_t rax89;
    int64_t rax90;
    int64_t rax91;
    int64_t rax92;
    int64_t rax93;
    int64_t rax94;
    int64_t rax95;
    uint32_t eax96;
    uint32_t eax97;
    void** rax98;
    uint32_t r12d99;
    int64_t rax100;
    void** rax101;
    void** rdi102;
    void** r9_103;
    void** rax104;
    void** rdi105;
    void** r9_106;
    void** rax107;
    void** rdi108;
    void** r9_109;
    void** rax110;
    int64_t rax111;
    int64_t rax112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t rax116;
    int64_t rax117;
    int64_t rax118;
    int64_t rax119;
    int64_t rax120;
    int64_t rax121;
    int64_t rax122;
    int64_t rax123;
    int64_t rax124;
    uint32_t eax125;
    struct s0* rax126;
    int32_t eax127;
    void** v128;
    void** rax129;
    int64_t rax130;
    int64_t rax131;
    int64_t rax132;
    int64_t rax133;
    void** rdi134;
    void** rdi135;
    void** rdi136;
    void** rdi137;

    __asm__("cli ");
    rax1 = g28;
    v2 = rax1;
    rax3 = fun_3a70("/proc/self/mountinfo", "re");
    rsp4 = reinterpret_cast<void****>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8);
    if (!rax3) {
        rsi5 = reinterpret_cast<void**>("r");
        rax6 = fun_3980("/etc/mtab", "r");
        r14_7 = rax6;
        if (!rax6) 
            goto addr_a4b6_3;
        r15_8 = reinterpret_cast<void**>("autofs");
        v9 = reinterpret_cast<void***>(rsp4 - 1 + 1 + 6);
        goto addr_a923_5;
    }
    r12_10 = rax3;
    r14_11 = reinterpret_cast<void*>(rsp4 + 8);
    v9 = reinterpret_cast<void***>(rsp4 + 6);
    rbx12 = reinterpret_cast<void**>(rsp4 + 7);
    r15_8 = reinterpret_cast<void**>("%*u %*u %u:%u %n");
    while (rcx13 = r12_10, rax15 = fun_38b0(rbx12, r14_11, 10, rcx13, r8_14), rsp16 = reinterpret_cast<void*>(rsp4 - 1 + 1), rax15 != -1) {
        rcx17 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 40);
        rdx18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 36);
        r8_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 44);
        eax19 = fun_39a0(0, "%*u %*u %u:%u %n", rdx18, rcx17, r8_14);
        rsp4 = reinterpret_cast<void****>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
        if (reinterpret_cast<uint32_t>(eax19 - 2) > 1) 
            continue;
        r13_20 = reinterpret_cast<void**>(static_cast<uint64_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(v21))));
        rax22 = fun_37b0(r13_20, r13_20);
        rsp4 = rsp4 - 1 + 1;
        if (!rax22) 
            continue;
        rax22->f0 = 0;
        rbp23 = reinterpret_cast<void**>(&rax22->f1);
        rax24 = fun_37b0(rbp23, rbp23);
        rsp4 = rsp4 - 1 + 1;
        if (!rax24) 
            continue;
        rax24->f0 = 0;
        rax25 = fun_3b30(&rax24->f1, " - ", rdx18, rcx17, r8_14);
        rsp4 = rsp4 - 1 + 1;
        if (!rax25) 
            continue;
        rax26 = reinterpret_cast<void**>(&rax25->f3);
        v27 = rax26;
        rax28 = fun_37b0(rax26, rax26);
        rsp4 = rsp4 - 1 + 1;
        if (!rax28) 
            continue;
        rax28->f0 = 0;
        r8_14 = reinterpret_cast<void**>(&rax28->f1);
        v29 = r8_14;
        rax30 = fun_37b0(r8_14, r8_14);
        rsp4 = rsp4 - 1 + 1;
        if (!rax30) 
            continue;
        r8_14 = v29;
        rax30->f0 = 0;
        unescape_tab(r8_14, 32, rdx18, rcx17);
        unescape_tab(rbp23, 32, rdx18, rcx17);
        unescape_tab(r13_20, 32, rdx18, rcx17);
        unescape_tab(v27, 32, rdx18, rcx17);
        rax31 = xmalloc(56, 32, rdx18, rcx17);
        v29 = rax31;
        rax33 = xstrdup(r8_14, 32, rdx18, rcx17, r8_14, r9_32, v9, v27, *reinterpret_cast<void**>(&v29));
        *reinterpret_cast<void***>(v29) = rax33;
        rax35 = xstrdup(rbp23, 32, v29, rcx17, r8_14, r9_34, v9, v27, *reinterpret_cast<void**>(&v29));
        r13d36 = 1;
        *reinterpret_cast<void***>(v29 + 8) = rax35;
        rax38 = xstrdup(r13_20, 32, v29, rcx17, r8_14, r9_37, v9, v27, *reinterpret_cast<void**>(&v29));
        *reinterpret_cast<void***>(v29 + 16) = rax38;
        rax40 = xstrdup(v27, 32, v29, rcx17, r8_14, r9_39, v9, v27, *reinterpret_cast<void**>(&v29));
        *reinterpret_cast<int32_t*>(&rcx41) = v42;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx41) + 4) = 0;
        rbp23 = rax40;
        *reinterpret_cast<void***>(v29 + 24) = rax40;
        *reinterpret_cast<int32_t*>(&rax43) = v44;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
        rsi45 = rcx41 << 8;
        *reinterpret_cast<void***>(v29 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v29 + 40)) | 4);
        *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<uint32_t*>(&rsi45) & 0xfff00;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rsi47) = *reinterpret_cast<unsigned char*>(&rax43);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi47) + 4) = 0;
        *reinterpret_cast<void***>(v29 + 32) = reinterpret_cast<void**>(rax43 << 12 & 0xffffff00000 | (rcx41 << 32 & 0xfffff00000000000 | rsi46 | rsi47));
        rax48 = fun_38c0(rbp23, "autofs", v29);
        rsp49 = reinterpret_cast<void*>(rsp4 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1);
        rdx50 = v29;
        if (*reinterpret_cast<int32_t*>(&rax48) && ((rax51 = fun_38c0(rbp23, "proc", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax51)) && ((rax52 = fun_38c0(rbp23, "subfs", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax52)) && ((rax53 = fun_38c0(rbp23, "debugfs", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax53)) && ((rax54 = fun_38c0(rbp23, "devpts", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax54)) && ((rax55 = fun_38c0(rbp23, "fusectl", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax55)) && ((rax56 = fun_38c0(rbp23, "fuse.portal", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax56)) && ((rax57 = fun_38c0(rbp23, "mqueue", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax57)) && ((rax58 = fun_38c0(rbp23, "rpc_pipefs", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax58)) && ((rax59 = fun_38c0(rbp23, "sysfs", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax59)) && ((rax60 = fun_38c0(rbp23, "devfs", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax60)) && ((rax61 = fun_38c0(rbp23, "kernfs", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax61)) && (rax62 = fun_38c0(rbp23, "ignore", rdx50), rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8), rdx50 = v29, !!*reinterpret_cast<int32_t*>(&rax62)))))))))))))) {
            rax63 = fun_38c0(rbp23, "none", rdx50);
            rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
            rdx50 = v29;
            *reinterpret_cast<unsigned char*>(&r13d36) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax63) == 0);
        }
        eax64 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx50 + 40));
        v27 = rdx50;
        eax65 = eax64 & 0xfffffffe | r13d36;
        r13_20 = *reinterpret_cast<void***>(rdx50);
        *reinterpret_cast<void***>(rdx50 + 40) = *reinterpret_cast<void***>(&eax65);
        rax66 = fun_37b0(r13_20, r13_20);
        rsp4 = reinterpret_cast<void****>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
        rdx67 = v27;
        ecx68 = 1;
        if (rax66) 
            goto addr_a454_17;
        if (*reinterpret_cast<void***>(r13_20) == 47 && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_20 + 1) == 47)) {
            *reinterpret_cast<void**>(&v29) = reinterpret_cast<void*>(1);
            rax69 = fun_38c0(rbp23, "smbfs", rdx67);
            rsp4 = rsp4 - 1 + 1;
            rdx67 = v27;
            ecx68 = 1;
            if (!*reinterpret_cast<int32_t*>(&rax69) || ((rax70 = fun_38c0(rbp23, "smb3", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, *reinterpret_cast<int32_t*>(&rax70) == 0) || (rax71 = fun_38c0(rbp23, "cifs", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, *reinterpret_cast<int32_t*>(&rax71) == 0))) {
                addr_a454_17:
                eax72 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx67 + 40))) & 0xfffffffd | reinterpret_cast<uint32_t>(ecx68 + ecx68);
                *reinterpret_cast<void***>(rdx67 + 40) = *reinterpret_cast<void***>(&eax72);
                *v9 = rdx67;
                v9 = reinterpret_cast<void***>(rdx67 + 48);
                continue;
            }
        }
        v27 = rdx67;
        rax73 = fun_38c0(rbp23, "acfs", rdx67);
        rsp4 = rsp4 - 1 + 1;
        rdx67 = v27;
        ecx68 = 1;
        if (*reinterpret_cast<int32_t*>(&rax73) && ((*reinterpret_cast<void**>(&v29) = reinterpret_cast<void*>(1), rax74 = fun_38c0(rbp23, "afs", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax74)) && ((rax75 = fun_38c0(rbp23, "coda", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax75)) && ((rax76 = fun_38c0(rbp23, "auristorfs", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax76)) && ((rax77 = fun_38c0(rbp23, "fhgfs", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax77)) && ((rax78 = fun_38c0(rbp23, "gpfs", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax78)) && ((rax79 = fun_38c0(rbp23, "ibrix", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax79)) && ((rax80 = fun_38c0(rbp23, "ocfs2", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax80)) && (rax81 = fun_38c0(rbp23, "vxfs", rdx67), rsp4 = rsp4 - 1 + 1, rdx67 = v27, ecx68 = 1, !!*reinterpret_cast<int32_t*>(&rax81)))))))))) {
            rax82 = fun_38c0("-hosts", r13_20, rdx67);
            rsp4 = rsp4 - 1 + 1;
            rdx67 = v27;
            *reinterpret_cast<void**>(&ecx68) = reinterpret_cast<void*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax82) == 0)));
            goto addr_a454_17;
        }
    }
    fun_35f0(0, 0);
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_10)) & 32)) 
        goto addr_a495_24;
    rax83 = fun_3630();
    r14d84 = *reinterpret_cast<void***>(rax83);
    r13_20 = rax83;
    rpl_fclose(r12_10, r14_11, 10, rcx13, r8_14);
    *reinterpret_cast<void***>(r13_20) = r14d84;
    goto addr_a7c7_26;
    addr_a495_24:
    rax85 = rpl_fclose(r12_10, r14_11, 10, rcx13, r8_14);
    if (*reinterpret_cast<int32_t*>(&rax85) == -1) 
        goto addr_a844_27; else 
        goto addr_a4a6_28;
    addr_a4ca_29:
    return r14_7;
    while (1) {
        addr_ac39_30:
        while (1) {
            rsi5 = reinterpret_cast<void**>("acfs");
            *reinterpret_cast<uint32_t*>(&r12_10) = 1;
            rax86 = fun_38c0(rbp23, "acfs", 10, rbp23, "acfs", 10);
            if (*reinterpret_cast<int32_t*>(&rax86) && ((rsi5 = reinterpret_cast<void**>("afs"), rax87 = fun_38c0(rbp23, "afs", 10, rbp23, "afs", 10), !!*reinterpret_cast<int32_t*>(&rax87)) && ((rsi5 = reinterpret_cast<void**>("coda"), rax88 = fun_38c0(rbp23, "coda", 10, rbp23, "coda", 10), !!*reinterpret_cast<int32_t*>(&rax88)) && ((rsi5 = reinterpret_cast<void**>("auristorfs"), rax89 = fun_38c0(rbp23, "auristorfs", 10, rbp23, "auristorfs", 10), !!*reinterpret_cast<int32_t*>(&rax89)) && ((rsi5 = reinterpret_cast<void**>("fhgfs"), rax90 = fun_38c0(rbp23, "fhgfs", 10, rbp23, "fhgfs", 10), !!*reinterpret_cast<int32_t*>(&rax90)) && ((rsi5 = reinterpret_cast<void**>("gpfs"), rax91 = fun_38c0(rbp23, "gpfs", 10, rbp23, "gpfs", 10), !!*reinterpret_cast<int32_t*>(&rax91)) && ((rsi5 = reinterpret_cast<void**>("ibrix"), rax92 = fun_38c0(rbp23, "ibrix", 10, rbp23, "ibrix", 10), !!*reinterpret_cast<int32_t*>(&rax92)) && ((rsi5 = reinterpret_cast<void**>("ocfs2"), rax93 = fun_38c0(rbp23, "ocfs2", 10, rbp23, "ocfs2", 10), !!*reinterpret_cast<int32_t*>(&rax93)) && (rsi5 = reinterpret_cast<void**>("vxfs"), rax94 = fun_38c0(rbp23, "vxfs", 10, rbp23, "vxfs", 10), !!*reinterpret_cast<int32_t*>(&rax94)))))))))) {
                rsi5 = r13_20;
                rax95 = fun_38c0("-hosts", rsi5, 10, "-hosts", rsi5, 10);
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax95) == 0);
            }
            while (1) {
                do {
                    eax96 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx12 + 40));
                    *reinterpret_cast<uint32_t*>(&r12_10) = *reinterpret_cast<uint32_t*>(&r12_10) + *reinterpret_cast<uint32_t*>(&r12_10);
                    *reinterpret_cast<void***>(rbx12 + 32) = reinterpret_cast<void**>(0xffffffffffffffff);
                    eax97 = eax96 & 0xfffffffd | *reinterpret_cast<uint32_t*>(&r12_10);
                    *reinterpret_cast<void***>(rbx12 + 40) = *reinterpret_cast<void***>(&eax97);
                    *v9 = rbx12;
                    v9 = reinterpret_cast<void***>(rbx12 + 48);
                    addr_a923_5:
                    rax98 = fun_3970(r14_7, rsi5, 10, rcx13, r8_14);
                    rbp23 = rax98;
                    if (!rax98) 
                        break;
                    r12d99 = 1;
                    rax100 = fun_3b10(rbp23, "bind", 10, rcx13, r8_14);
                    rax101 = xmalloc(56, "bind", 10, rcx13, 56, "bind", 10, rcx13);
                    rdi102 = *reinterpret_cast<void***>(rbp23);
                    rbx12 = rax101;
                    rax104 = xstrdup(rdi102, "bind", 10, rcx13, r8_14, r9_103, v9, v27, *reinterpret_cast<void**>(&v29));
                    rdi105 = *reinterpret_cast<void***>(rbp23 + 8);
                    *reinterpret_cast<void***>(rbx12) = rax104;
                    rax107 = xstrdup(rdi105, "bind", 10, rcx13, r8_14, r9_106, v9, v27, *reinterpret_cast<void**>(&v29));
                    *reinterpret_cast<void***>(rbx12 + 16) = reinterpret_cast<void**>(0);
                    rdi108 = *reinterpret_cast<void***>(rbp23 + 16);
                    *reinterpret_cast<void***>(rbx12 + 8) = rax107;
                    rax110 = xstrdup(rdi108, "bind", 10, rcx13, r8_14, r9_109, v9, v27, *reinterpret_cast<void**>(&v29));
                    *reinterpret_cast<void***>(rbx12 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx12 + 40)) | 4);
                    *reinterpret_cast<void***>(rbx12 + 24) = rax110;
                    rbp23 = rax110;
                    rax111 = fun_38c0(rax110, r15_8, 10, rax110, r15_8, 10);
                    if (*reinterpret_cast<int32_t*>(&rax111) && ((rax112 = fun_38c0(rbp23, "proc", 10, rbp23, "proc", 10), !!*reinterpret_cast<int32_t*>(&rax112)) && ((rax113 = fun_38c0(rbp23, "subfs", 10, rbp23, "subfs", 10), !!*reinterpret_cast<int32_t*>(&rax113)) && ((rax114 = fun_38c0(rbp23, "debugfs", 10, rbp23, "debugfs", 10), !!*reinterpret_cast<int32_t*>(&rax114)) && ((rax115 = fun_38c0(rbp23, "devpts", 10, rbp23, "devpts", 10), !!*reinterpret_cast<int32_t*>(&rax115)) && ((rax116 = fun_38c0(rbp23, "fusectl", 10, rbp23, "fusectl", 10), !!*reinterpret_cast<int32_t*>(&rax116)) && ((rax117 = fun_38c0(rbp23, "fuse.portal", 10, rbp23, "fuse.portal", 10), !!*reinterpret_cast<int32_t*>(&rax117)) && ((rax118 = fun_38c0(rbp23, "mqueue", 10, rbp23, "mqueue", 10), !!*reinterpret_cast<int32_t*>(&rax118)) && ((rax119 = fun_38c0(rbp23, "rpc_pipefs", 10, rbp23, "rpc_pipefs", 10), !!*reinterpret_cast<int32_t*>(&rax119)) && ((rax120 = fun_38c0(rbp23, "sysfs", 10, rbp23, "sysfs", 10), !!*reinterpret_cast<int32_t*>(&rax120)) && ((rax121 = fun_38c0(rbp23, "devfs", 10, rbp23, "devfs", 10), !!*reinterpret_cast<int32_t*>(&rax121)) && ((rax122 = fun_38c0(rbp23, "kernfs", 10, rbp23, "kernfs", 10), !!*reinterpret_cast<int32_t*>(&rax122)) && (rax123 = fun_38c0(rbp23, "ignore", 10, rbp23, "ignore", 10), !!*reinterpret_cast<int32_t*>(&rax123)))))))))))))) {
                        rax124 = fun_38c0(rbp23, "none", 10, rbp23, "none", 10);
                        *reinterpret_cast<unsigned char*>(&r12d99) = reinterpret_cast<uint1_t>(rax100 == 0);
                        *reinterpret_cast<unsigned char*>(&rax124) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax124) == 0);
                        r12d99 = r12d99 & *reinterpret_cast<uint32_t*>(&rax124);
                    }
                    r13_20 = *reinterpret_cast<void***>(rbx12);
                    *reinterpret_cast<int32_t*>(&rsi5) = 58;
                    *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
                    eax125 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx12 + 40))) & 0xfffffffe | r12d99;
                    *reinterpret_cast<uint32_t*>(&r12_10) = 1;
                    *reinterpret_cast<void***>(rbx12 + 40) = *reinterpret_cast<void***>(&eax125);
                    rax126 = fun_37b0(r13_20, r13_20);
                } while (rax126);
                goto addr_aaea_37;
                eax127 = fun_3600(r14_7, rsi5, 10, rcx13, r8_14);
                if (eax127) {
                    addr_a4a6_28:
                    *v9 = reinterpret_cast<void**>(0);
                    r14_7 = v128;
                } else {
                    addr_a844_27:
                    rax129 = fun_3630();
                    r14d84 = *reinterpret_cast<void***>(rax129);
                    r13_20 = rax129;
                    addr_a7c7_26:
                    *v9 = reinterpret_cast<void**>(0);
                    rbx12 = v128;
                    if (rbx12) 
                        goto addr_a7f2_39; else 
                        goto addr_a7dc_40;
                }
                addr_a4b6_3:
                rax130 = v2 - g28;
                if (!rax130) 
                    goto addr_a4ca_29;
                fun_3780();
                addr_abe9_42:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_20 + 1) == 47)) 
                    break;
                rsi5 = reinterpret_cast<void**>("smbfs");
                rax131 = fun_38c0(rbp23, "smbfs", 10, rbp23, "smbfs", 10);
                if (!*reinterpret_cast<int32_t*>(&rax131)) 
                    continue;
                rsi5 = reinterpret_cast<void**>("smb3");
                rax132 = fun_38c0(rbp23, "smb3", 10, rbp23, "smb3", 10);
                if (!*reinterpret_cast<int32_t*>(&rax132)) 
                    continue;
                rsi5 = reinterpret_cast<void**>("cifs");
                rax133 = fun_38c0(rbp23, "cifs", 10, rbp23, "cifs", 10);
                if (!*reinterpret_cast<int32_t*>(&rax133)) 
                    continue; else 
                    goto addr_ac39_30;
                do {
                    addr_a7f2_39:
                    rbp23 = rbx12;
                    rbx12 = *reinterpret_cast<void***>(rbx12 + 48);
                    rdi134 = *reinterpret_cast<void***>(rbp23);
                    fun_35f0(rdi134, rdi134);
                    rdi135 = *reinterpret_cast<void***>(rbp23 + 8);
                    fun_35f0(rdi135, rdi135);
                    rdi136 = *reinterpret_cast<void***>(rbp23 + 16);
                    fun_35f0(rdi136, rdi136);
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp23 + 40)) & 4) {
                        rdi137 = *reinterpret_cast<void***>(rbp23 + 24);
                        fun_35f0(rdi137, rdi137);
                    }
                    fun_35f0(rbp23, rbp23);
                    v128 = rbx12;
                } while (rbx12);
                addr_a828_48:
                *reinterpret_cast<void***>(r13_20) = r14d84;
                *reinterpret_cast<int32_t*>(&r14_7) = 0;
                *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
                goto addr_a4b6_3;
                addr_a7dc_40:
                goto addr_a828_48;
                addr_aaea_37:
                if (*reinterpret_cast<void***>(r13_20) == 47) 
                    goto addr_abe9_42; else 
                    break;
            }
        }
    }
}

struct s50 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[15] pad40;
    unsigned char f28;
};

void fun_ac43(struct s50* rdi) {
    void** rdi2;
    void** rdi3;
    void** rdi4;
    void** rdi5;

    __asm__("cli ");
    rdi2 = rdi->f0;
    fun_35f0(rdi2);
    rdi3 = rdi->f8;
    fun_35f0(rdi3);
    rdi4 = rdi->f10;
    fun_35f0(rdi4);
    if (rdi->f28 & 4) {
        rdi5 = rdi->f18;
        fun_35f0(rdi5);
        goto fun_35f0;
    } else {
        goto fun_35f0;
    }
}

void fun_3ac0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s51 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s51* fun_37d0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_ac93(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s51* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_3ac0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_3620();
    } else {
        rbx3 = rdi;
        rax4 = fun_37d0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_3640(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_c433(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_3630();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x18540;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_c473(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x18540);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_c493(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x18540);
    }
    *rdi = esi;
    return 0x18540;
}

int64_t fun_c4b3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x18540);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s52 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_c4f3(struct s52* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s52*>(0x18540);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s53 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s53* fun_c513(struct s53* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s53*>(0x18540);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x3b8e;
    if (!rdx) 
        goto 0x3b8e;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x18540;
}

struct s54 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_c553(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s54* r8) {
    struct s54* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s54*>(0x18540);
    }
    rax7 = fun_3630();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0xc586);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s55 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_c5d3(int64_t rdi, int64_t rsi, void*** rdx, struct s55* rcx) {
    struct s55* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s55*>(0x18540);
    }
    rax6 = fun_3630();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0xc601);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0xc65c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_c6c3() {
    __asm__("cli ");
}

void** g182d8 = reinterpret_cast<void**>(64);

int64_t slotvec0 = 0x100;

void fun_c6d3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_35f0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x18440) {
        fun_35f0(rdi7);
        g182d8 = reinterpret_cast<void**>(0x18440);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x182d0) {
        fun_35f0(r12_2);
        slotvec = reinterpret_cast<void**>(0x182d0);
    }
    nslots = 1;
    return;
}

void fun_c773() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_c793() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_c7a3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_c7c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_c7e3(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s11* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3b94;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_3780();
    } else {
        return rax6;
    }
}

void** fun_c873(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s11* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x3b99;
    rcx6 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_3780();
    } else {
        return rax7;
    }
}

void** fun_c903(int32_t edi, int64_t rsi) {
    int64_t rax3;
    struct s11* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x3b9e;
    rcx4 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_3780();
    } else {
        return rax5;
    }
}

void** fun_c993(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s11* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x3ba3;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_3780();
    } else {
        return rax6;
    }
}

void** fun_ca23(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s11* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xbb10]");
    __asm__("movdqa xmm1, [rip+0xbb18]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xbb01]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_3780();
    } else {
        return rax10;
    }
}

void** fun_cac3(int64_t rdi, uint32_t esi) {
    struct s11* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xba70]");
    __asm__("movdqa xmm1, [rip+0xba78]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xba61]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_3780();
    } else {
        return rax9;
    }
}

void** fun_cb63(int64_t rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xb9d0]");
    __asm__("movdqa xmm1, [rip+0xb9d8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xb9b9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_3780();
    } else {
        return rax3;
    }
}

void** fun_cbf3(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xb940]");
    __asm__("movdqa xmm1, [rip+0xb948]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xb936]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_3780();
    } else {
        return rax4;
    }
}

void** fun_cc83(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s11* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3ba8;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_3780();
    } else {
        return rax6;
    }
}

void** fun_cd23(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s11* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xb80a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xb802]");
    __asm__("movdqa xmm2, [rip+0xb80a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3bad;
    if (!rdx) 
        goto 0x3bad;
    rcx6 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_3780();
    } else {
        return rax7;
    }
}

void** fun_cdc3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rcx6;
    struct s11* rcx7;
    void** rax8;
    int64_t rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xb76a]");
    __asm__("movdqa xmm1, [rip+0xb772]");
    __asm__("movdqa xmm2, [rip+0xb77a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3bb2;
    if (!rdx) 
        goto 0x3bb2;
    rcx7 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = rcx6 - g28;
    if (rdx9) {
        fun_3780();
    } else {
        return rax8;
    }
}

void** fun_ce73(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    struct s11* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xb6ba]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xb6b2]");
    __asm__("movdqa xmm2, [rip+0xb6ba]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3bb7;
    if (!rsi) 
        goto 0x3bb7;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_3780();
    } else {
        return rax6;
    }
}

void** fun_cf13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s11* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xb61a]");
    __asm__("movdqa xmm1, [rip+0xb622]");
    __asm__("movdqa xmm2, [rip+0xb62a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3bbc;
    if (!rsi) 
        goto 0x3bbc;
    rcx6 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_3780();
    } else {
        return rax7;
    }
}

void fun_cfb3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_cfc3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_cfe3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_d003(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s56 {
    int32_t f0;
    signed char[4] pad8;
    uint64_t f8;
};

int32_t open_safer(int64_t rdi, int64_t rsi);

int64_t fun_d023(struct s56* rdi) {
    int32_t eax2;
    uint64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    rdi->f8 = 0;
    eax2 = open_safer(".", 0x80000);
    rdi->f0 = eax2;
    if (eax2 < 0) {
        rax3 = fun_3820();
        rdi->f8 = rax3;
        *reinterpret_cast<uint32_t*>(&rax4) = -static_cast<uint32_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax3 < 1))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    } else {
        return 0;
    }
}

int32_t fun_39d0(int64_t rdi, void** rsi, void** rdx);

void fun_d083(int32_t* rdi) {
    __asm__("cli ");
    if (*rdi < 0) {
        goto 0xeac0;
    } else {
        goto fun_39d0;
    }
}

void fun_d0b3(int32_t* rdi) {
    __asm__("cli ");
    if (*rdi >= 0) {
        fun_3840();
    }
    goto fun_35f0;
}

struct s57 {
    signed char[16] pad16;
    void** f10;
};

void** fun_d0d3(struct s57* rdi, void** rsi) {
    void** rdi3;
    void** r12_4;
    void** rax5;

    __asm__("cli ");
    rdi3 = reinterpret_cast<void**>(&rdi->f10);
    r12_4 = *reinterpret_cast<void***>(rdi3 + 0xfffffffffffffff0);
    if (r12_4 == rdi3) {
        rax5 = fun_3950(rsi, rsi);
        if (rax5) {
            goto fun_3900;
        }
    } else {
        rax5 = fun_39e0(r12_4, rsi);
        if (!rax5) {
            rax5 = r12_4;
        }
    }
    return rax5;
}

struct s58 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_d133(struct s58* rdi, void** rsi) {
    void** rax3;
    void** rdi4;
    void** r12_5;
    void** rbp6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    rax3 = rdi->f8;
    rdi4 = rdi->f0;
    r12_5 = reinterpret_cast<void**>(&rdi->f10);
    rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rax3));
    if (rdi4 != r12_5) {
        fun_35f0(rdi4);
        rax3 = rdi->f8;
    }
    if (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(rbp6)) {
        rax7 = fun_3630();
        *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(12);
    } else {
        rax8 = fun_3950(rbp6, rsi);
        if (rax8) {
            rdi->f0 = rax8;
            rdi->f8 = rbp6;
            return 1;
        }
    }
    rdi->f0 = r12_5;
    rdi->f8 = reinterpret_cast<void**>(0x400);
    return 0;
}

struct s59 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_d1b3(struct s59* rdi, void** rsi) {
    void** r14_3;
    void** r13_4;
    void** r12_5;
    void** rbp6;
    void** rax7;
    int64_t rax8;
    void** rax9;
    void** rcx10;
    void** rax11;
    void** rax12;

    __asm__("cli ");
    r14_3 = reinterpret_cast<void**>(&rdi->f10);
    r13_4 = rdi->f8;
    r12_5 = rdi->f0;
    rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_4) + reinterpret_cast<unsigned char>(r13_4));
    if (r12_5 == r14_3) {
        rax7 = fun_3950(rbp6, rsi);
        if (!rax7) {
            *reinterpret_cast<int32_t*>(&rax8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        } else {
            rax9 = fun_3900(rax7, r12_5, r13_4);
            rcx10 = rax9;
            goto addr_d1ec_5;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(rbp6)) {
            rax11 = fun_3630();
            *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(12);
            goto addr_d23b_8;
        } else {
            rax12 = fun_39e0(r12_5, rbp6);
            rcx10 = rax12;
            if (!rax12) {
                r12_5 = rdi->f0;
                goto addr_d23b_8;
            } else {
                addr_d1ec_5:
                rdi->f0 = rcx10;
                *reinterpret_cast<int32_t*>(&rax8) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
                rdi->f8 = rbp6;
            }
        }
    }
    addr_d1f8_11:
    return rax8;
    addr_d23b_8:
    fun_35f0(r12_5, r12_5);
    rdi->f0 = r14_3;
    *reinterpret_cast<int32_t*>(&rax8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    rdi->f8 = reinterpret_cast<void**>(0x400);
    goto addr_d1f8_11;
}

struct s60 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    void** f40;
};

void fun_38d0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_d273(void** rdi, void** rsi, void** rdx, void** rcx, struct s60* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    void** v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    void** v13;
    int64_t v14;
    void** v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    void** v19;
    void** rax20;
    int64_t v21;
    void** v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    void** v26;
    void** rax27;
    int64_t v28;
    void** v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    void** v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    void** rcx37;
    int64_t r15_38;
    void** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_3ad0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_3ad0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_3730();
    fun_3ad0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_38d0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_3730();
    fun_3ad0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_38d0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_3730();
        fun_3ad0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x13968 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x13968;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_d6e3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s61 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_d703(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s61* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s61* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_3780();
    } else {
        return;
    }
}

void fun_d7a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_d846_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_d850_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_3780();
    } else {
        return;
    }
    addr_d846_5:
    goto addr_d850_7;
}

void fun_d883() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_38d0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_3730();
    fun_3a00(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_3730();
    fun_3a00(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_3730();
    goto fun_3a00;
}

int64_t fun_3690();

void fun_d923(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3690();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_d963(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3950(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d983(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3950(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d9a3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3950(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d9c3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_39e0(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_d9f3(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_39e0(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_da23(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3690();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_da63() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_3690();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_daa3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_3690();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_dad3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_3690();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_db23(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_3690();
            if (rax5) 
                break;
            addr_db6d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_db6d_5;
        rax8 = fun_3690();
        if (rax8) 
            goto addr_db56_9;
        if (rbx4) 
            goto addr_db6d_5;
        addr_db56_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_dbb3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_3690();
            if (rax8) 
                break;
            addr_dbfa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_dbfa_5;
        rax11 = fun_3690();
        if (rax11) 
            goto addr_dbe2_9;
        if (!rbx6) 
            goto addr_dbe2_9;
        if (r12_4) 
            goto addr_dbfa_5;
        addr_dbe2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_dc43(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_dced_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_dd00_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_dca0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_dcc6_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_dd14_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_dcbd_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_dd14_14;
            addr_dcbd_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_dd14_14;
            addr_dcc6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_39e0(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_dd14_14;
            if (!rbp13) 
                break;
            addr_dd14_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_dced_9;
        } else {
            if (!r13_6) 
                goto addr_dd00_10;
            goto addr_dca0_11;
        }
    }
}

void fun_dd43(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_38a0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_dd73(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_38a0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_dda3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_38a0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ddc3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_38a0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_dde3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3950(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_3900;
    }
}

void fun_de23(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3950(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_3900;
    }
}

void fun_de63(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3950(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_3900;
    }
}

void fun_dea3(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_3750(rdi, rdi);
    rax4 = fun_3950(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3900;
    }
}

void fun_dee3() {
    __asm__("cli ");
    fun_3730();
    fun_3a30();
    fun_3620();
}

uint64_t fun_df23() {
    uint64_t rax1;
    void** rax2;

    __asm__("cli ");
    rax1 = fun_3820();
    if (rax1 || (rax2 = fun_3630(), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax2) == 12))) {
        return rax1;
    } else {
        xalloc_die();
    }
}

void fun_df53(uint32_t edi, int32_t esi) {
    uint32_t ebp3;

    __asm__("cli ");
    ebp3 = exit_failure;
    if (edi > 3) {
        while (1) {
            if (edi != 4) 
                goto 0x3bc1;
            if (esi >= 0) {
                addr_df99_4:
            } else {
                addr_dfef_5:
            }
            fun_3730();
            esi = 0;
            edi = ebp3;
            fun_3a30();
            fun_3620();
        }
    } else {
        if (edi <= 1) {
            if (edi != 1) {
                goto 0x3bc1;
            }
        }
        if (esi < 0) 
            goto addr_dfef_5; else 
            goto addr_df99_4;
    }
}

void** fun_3a80(void** rdi);

int64_t fun_e023(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    int64_t rax7;
    int64_t v8;
    uint16_t* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    int64_t rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    uint16_t** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    struct s0* rax26;
    int64_t rax27;
    struct s0* rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    struct s0* rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<uint16_t*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_3800("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", "0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c");
        do {
            fun_3780();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_e394_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_e394_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_e0dd_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_e0e5_13:
            rax15 = v8 - g28;
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_3630();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_3b40(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9 + rdx23) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_e11b_21;
    rsi = r15_14;
    rax24 = fun_3a80(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_37b0(r13_18), r8 = r8, rax26 == 0))) {
            addr_e11b_21:
            r12d11 = 4;
            goto addr_e0e5_13;
        } else {
            addr_e159_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_37b0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<uint16_t*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x13a80 + rbp33 * 4) + 0x13a80;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_e11b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_e0dd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_e0dd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_37b0(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_e159_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_e0e5_13;
}

void rpl_vasprintf();

void fun_e453() {
    signed char al1;
    int64_t rax2;
    int64_t rdx3;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    rpl_vasprintf();
    rdx3 = rax2 - g28;
    if (rdx3) {
        fun_3780();
    } else {
        return;
    }
}

void** vasnprintf();

uint64_t fun_e513(void*** rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    void** rax5;
    uint64_t rax6;
    uint64_t v7;
    void** rax8;
    int64_t rdx9;

    __asm__("cli ");
    rax4 = g28;
    rax5 = vasnprintf();
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rax6 = v7;
        if (rax6 > 0x7fffffff) {
            fun_35f0(rax5, rax5);
            rax8 = fun_3630();
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(75);
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            *rdi = rax5;
        }
    }
    rdx9 = rax4 - g28;
    if (rdx9) {
        fun_3780();
    } else {
        return rax6;
    }
}

void fun_e593() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_3870(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_e5a3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_3750(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_3640(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_e623_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_e670_6; else 
                    continue;
            } else {
                rax18 = fun_3750(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_e6a0_8;
                if (v16 == -1) 
                    goto addr_e65e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_e623_5;
            } else {
                eax19 = fun_3870(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_e623_5;
            }
            addr_e65e_10:
            v16 = rbx15;
            goto addr_e623_5;
        }
    }
    addr_e685_16:
    return v12;
    addr_e670_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_e685_16;
    addr_e6a0_8:
    v12 = rbx15;
    goto addr_e685_16;
}

int64_t fun_e6b3(void** rdi, void*** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void*** rbp6;
    int64_t rbx7;
    int64_t rax8;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *rsi;
    if (!rdi5) {
        addr_e6f8_2:
        return -1;
    } else {
        rbp6 = rsi;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
        do {
            rax8 = fun_38c0(rdi5, r12_4, rdx);
            if (!*reinterpret_cast<int32_t*>(&rax8)) 
                break;
            ++rbx7;
            rdi5 = rbp6[rbx7 * 8];
        } while (rdi5);
        goto addr_e6f8_2;
    }
    return rbx7;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_e713(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_3730();
    } else {
        fun_3730();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_3a30;
}

void fun_e7a3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** v8;
    void** r12_9;
    void** r12_10;
    void** rdx11;
    int64_t v12;
    int64_t rbp13;
    void** rbp14;
    int64_t v15;
    int64_t rbx16;
    void** r14_17;
    void** v18;
    void** rax19;
    void** rsi20;
    int64_t v21;
    void** v22;
    void** r15_23;
    int64_t rbx24;
    uint32_t eax25;
    void** rax26;
    void** rdi27;
    int64_t v28;
    int64_t v29;
    void** rax30;
    void** rdi31;
    int64_t v32;
    int64_t v33;
    void** rdi34;
    void** rax35;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    *reinterpret_cast<int32_t*>(&rdx11) = 5;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    v12 = rbp13;
    rbp14 = rsi;
    v15 = rbx16;
    r14_17 = stderr;
    v18 = rdi;
    rax19 = fun_3730();
    rsi20 = r14_17;
    fun_3880(rax19, rsi20, 5, rcx, r8, r9, v21, v18, v22);
    r15_23 = *reinterpret_cast<void***>(rdi);
    *reinterpret_cast<int32_t*>(&rbx24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx24) + 4) = 0;
    if (r15_23) {
        do {
            if (!rbx24 || (rdx11 = r12_10, rsi20 = rbp14, eax25 = fun_3870(r13_7, rsi20, rdx11, r13_7, rsi20, rdx11), !!eax25)) {
                r13_7 = rbp14;
                rax26 = quote(r15_23, rsi20, rdx11, rcx, r8, r9);
                rdi27 = stderr;
                rdx11 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rcx = rax26;
                fun_3ad0(rdi27, 1, "\n  - %s", rcx, r8, r9, v28, v18, v29, v15, v12, v8);
            } else {
                rax30 = quote(r15_23, rsi20, rdx11, rcx, r8, r9);
                rdi31 = stderr;
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rdx11 = reinterpret_cast<void**>(", %s");
                rcx = rax30;
                fun_3ad0(rdi31, 1, ", %s", rcx, r8, r9, v32, v18, v33, v15, v12, v8);
            }
            ++rbx24;
            rbp14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp14) + reinterpret_cast<unsigned char>(r12_10));
            r15_23 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<uint64_t>(rbx24 * 8));
        } while (r15_23);
    }
    rdi34 = stderr;
    rax35 = *reinterpret_cast<void***>(rdi34 + 40);
    if (reinterpret_cast<unsigned char>(rax35) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi34 + 48))) {
        goto fun_37c0;
    } else {
        *reinterpret_cast<void***>(rdi34 + 40) = rax35 + 1;
        *reinterpret_cast<void***>(rax35) = reinterpret_cast<void**>(10);
        return;
    }
}

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_e8d3(int64_t rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int64_t rax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_e917_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_e98e_4;
        } else {
            addr_e98e_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            rax17 = fun_38c0(rdi15, r14_9, rdx);
            if (!*reinterpret_cast<int32_t*>(&rax17)) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<void***>(rbp12 + rbx16 * 8);
        } while (rdi15);
        goto addr_e910_8;
    } else {
        goto addr_e910_8;
    }
    return rbx16;
    addr_e910_8:
    rax14 = -1;
    goto addr_e917_3;
}

struct s62 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_e9a3(void** rdi, struct s62* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_3870(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

struct s63 {
    unsigned char f0;
    unsigned char f1;
};

struct s63* fun_ea03(struct s63* rdi) {
    uint32_t edx2;
    struct s63* rax3;
    struct s63* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s63*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s63*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s63*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_ea63(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_3750(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

struct s64 {
    signed char f0;
    void** f1;
};

struct s65 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

struct s64* fun_3860(struct s65* rdi, int64_t rsi, uint64_t rdx);

int32_t fun_3760(int64_t rdi, void** rsi, void** rdx);

void** fun_3a40(void** rdi, int64_t rsi, int64_t rdx);

int64_t fun_eac3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int32_t eax6;
    int32_t r12d7;
    void** rax8;
    int1_t zf9;
    void** v10;
    int64_t rax11;
    void** rax12;
    void** rbx13;
    void** rsi14;
    void* rax15;
    void* r15_16;
    struct s64* rax17;
    int32_t eax18;
    int32_t r13d19;
    void** r14_20;
    void* rax21;
    void** r14_22;
    int32_t eax23;
    void** rbp24;
    void** rax25;
    int64_t rdi26;
    int32_t eax27;
    int64_t rdi28;
    int32_t eax29;
    void* rax30;
    int64_t rdi31;
    int32_t eax32;
    int32_t eax33;
    int64_t rdi34;
    int32_t eax35;
    int32_t eax36;
    int64_t rdi37;
    int32_t eax38;
    int32_t eax39;

    __asm__("cli ");
    eax6 = fun_3770(rdi, rsi, rdx);
    r12d7 = eax6;
    if (!eax6 || (rax8 = fun_3630(), zf9 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax8) == 36), v10 = rax8, !zf9)) {
        addr_ec30_2:
        *reinterpret_cast<int32_t*>(&rax11) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        return rax11;
    } else {
        rax12 = fun_3750(rdi);
        rbx13 = rax12;
        if (!rax12) {
            fun_3800("0 < len", "lib/chdir-long.c", "0 < len", "lib/chdir-long.c");
            goto addr_ee02_5;
        }
        if (reinterpret_cast<unsigned char>(rax12) <= reinterpret_cast<unsigned char>("loc")) {
            addr_ee02_5:
            fun_3800("4096 <= len", "lib/chdir-long.c", "4096 <= len", "lib/chdir-long.c");
            goto addr_ee21_7;
        } else {
            rsi14 = reinterpret_cast<void**>("/");
            rax15 = fun_3850(rdi, "/", rdx, rcx, r8);
            r15_16 = rax15;
            if (rax15 == 2) {
                rax17 = fun_3860(rdi + 3, 47, rbx13 + 0xfffffffffffffffd);
                if (!rax17) {
                    r12d7 = -1;
                    goto addr_ec30_2;
                } else {
                    rax17->f0 = 0;
                    *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    eax18 = fun_3760(0xffffff9c, rdi, 0x10900);
                    rax17->f0 = 47;
                    r13d19 = eax18;
                    if (eax18 < 0) {
                        addr_edb0_12:
                        rbx13 = *reinterpret_cast<void***>(v10);
                    } else {
                        r14_20 = reinterpret_cast<void**>(&rax17->f1);
                        rsi14 = reinterpret_cast<void**>("/");
                        rax21 = fun_3850(r14_20, "/", 0x10900, rcx, r8);
                        r14_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<uint64_t>(rax21));
                        goto addr_eb49_14;
                    }
                }
            } else {
                r14_22 = rdi;
                r13d19 = -100;
                if (rax15) {
                    *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rsi14 = reinterpret_cast<void**>("/");
                    eax23 = fun_3760(0xffffff9c, "/", 0x10900);
                    r13d19 = eax23;
                    if (eax23 < 0) 
                        goto addr_edb0_12;
                    r14_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(r15_16));
                    goto addr_eb49_14;
                }
            }
        }
    }
    addr_ec23_18:
    r12d7 = -1;
    *reinterpret_cast<void***>(v10) = rbx13;
    goto addr_ec30_2;
    addr_eb49_14:
    if (*reinterpret_cast<void***>(r14_22) == 47) {
        addr_ee21_7:
        fun_3800("*dir != '/'", "lib/chdir-long.c", "*dir != '/'", "lib/chdir-long.c");
        goto addr_ee40_19;
    } else {
        rbp24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rbx13));
        if (reinterpret_cast<unsigned char>(r14_22) > reinterpret_cast<unsigned char>(rbp24)) {
            addr_ee40_19:
            fun_3800("dir <= dir_end", "lib/chdir-long.c", "dir <= dir_end", "lib/chdir-long.c");
        } else {
            if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rbp24) - reinterpret_cast<unsigned char>(r14_22)) > reinterpret_cast<int64_t>("loc")) {
                while (rax25 = fun_3a40(r14_22, 47, "oc"), rbx13 = rax25, !!rax25) {
                    *reinterpret_cast<void***>(rax25) = reinterpret_cast<void**>(0);
                    if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rax25) - reinterpret_cast<unsigned char>(r14_22)) > reinterpret_cast<int64_t>("loc")) 
                        goto addr_edbc_24;
                    *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rsi14 = r14_22;
                    *reinterpret_cast<int32_t*>(&rdi26) = r13d19;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                    eax27 = fun_3760(rdi26, rsi14, 0x10900);
                    *reinterpret_cast<int32_t*>(&r15_16) = eax27;
                    if (eax27 < 0) 
                        goto addr_ec10_26;
                    if (r13d19 < 0) 
                        goto addr_eb78_28;
                    *reinterpret_cast<int32_t*>(&rdi28) = r13d19;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
                    eax29 = fun_3840();
                    if (eax29) 
                        goto addr_ec01_30;
                    addr_eb78_28:
                    *reinterpret_cast<void***>(rbx13) = reinterpret_cast<void**>(47);
                    ++rbx13;
                    rsi14 = reinterpret_cast<void**>("/");
                    rax30 = fun_3850(rbx13, "/", 0x10900, rcx, r8);
                    r14_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<uint64_t>(rax30));
                    if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rbp24) - reinterpret_cast<unsigned char>(r14_22)) <= reinterpret_cast<int64_t>("loc")) 
                        goto addr_ece0_31;
                    r13d19 = *reinterpret_cast<int32_t*>(&r15_16);
                }
            } else {
                *reinterpret_cast<int32_t*>(&r15_16) = r13d19;
                goto addr_ece0_31;
            }
        }
    }
    r12d7 = -1;
    *reinterpret_cast<void***>(v10) = reinterpret_cast<void**>(36);
    goto addr_ec30_2;
    addr_edbc_24:
    rcx = reinterpret_cast<void**>("chdir_long");
    *reinterpret_cast<int32_t*>(&rdx) = 0xb3;
    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
    rsi14 = reinterpret_cast<void**>("lib/chdir-long.c");
    fun_3800("slash - dir < 4096", "lib/chdir-long.c", "slash - dir < 4096", "lib/chdir-long.c");
    goto addr_eddb_36;
    addr_ece0_31:
    if (reinterpret_cast<unsigned char>(rbp24) <= reinterpret_cast<unsigned char>(r14_22)) {
        *reinterpret_cast<int32_t*>(&rdi31) = *reinterpret_cast<int32_t*>(&r15_16);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi31) + 4) = 0;
        r13d19 = *reinterpret_cast<int32_t*>(&r15_16);
        eax32 = fun_39d0(rdi31, "/", rdx);
        if (eax32) {
            addr_ec13_38:
            while (rbx13 = *reinterpret_cast<void***>(v10), *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0, r13d19 >= 0) {
                addr_ed77_39:
                *reinterpret_cast<int32_t*>(&rdi28) = r13d19;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
                eax33 = fun_3840();
                if (!eax33) 
                    break;
                addr_ec01_30:
                cdb_free_part_0(rdi28, rsi14, rdx, rcx);
                addr_ec10_26:
                *reinterpret_cast<void***>(rbx13) = reinterpret_cast<void**>(47);
            }
            goto addr_ec23_18;
        } else {
            if (*reinterpret_cast<int32_t*>(&r15_16) < 0) {
                addr_ed27_42:
                r12d7 = 0;
                goto addr_ec30_2;
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi14 = r14_22;
        *reinterpret_cast<int32_t*>(&rdi34) = *reinterpret_cast<int32_t*>(&r15_16);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        eax35 = fun_3760(rdi34, rsi14, 0x10900);
        r13d19 = eax35;
        if (eax35 < 0) {
            addr_eddb_36:
            r13d19 = *reinterpret_cast<int32_t*>(&r15_16);
            goto addr_ec13_38;
        } else {
            if (*reinterpret_cast<int32_t*>(&r15_16) < 0 || (*reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&r15_16), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, eax36 = fun_3840(), eax36 == 0)) {
                *reinterpret_cast<int32_t*>(&rdi37) = r13d19;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0;
                eax38 = fun_39d0(rdi37, rsi14, 0x10900);
                if (eax38) {
                    rbx13 = *reinterpret_cast<void***>(v10);
                    *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
                    goto addr_ed77_39;
                }
            } else {
                goto addr_ec01_30;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi28) = r13d19;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
    eax39 = fun_3840();
    if (eax39) 
        goto addr_ec01_30; else 
        goto addr_ed27_42;
}

int64_t fun_3660();

int64_t fun_ee63(void** rdi, void* rsi, void** rdx, void** rcx, void** r8) {
    int64_t rax6;
    uint32_t ebx7;
    int64_t rax8;
    void** rax9;
    void** rax10;

    __asm__("cli ");
    rax6 = fun_3660();
    ebx7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax8 = rpl_fclose(rdi, rsi, rdx, rcx, r8);
    if (ebx7) {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            addr_eebe_3:
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        } else {
            rax9 = fun_3630();
            *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            if (rax6) 
                goto addr_eebe_3;
            rax10 = fun_3630();
            *reinterpret_cast<int32_t*>(&rax8) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax10) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    }
    return rax8;
}

struct s66 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_3910(struct s66* rdi);

int32_t fun_39b0(struct s66* rdi);

int64_t fun_37f0(int64_t rdi, ...);

int32_t rpl_fflush(struct s66* rdi);

int64_t fun_3710(struct s66* rdi);

int64_t fun_eed3(struct s66* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_3910(rdi);
    if (eax2 >= 0) {
        eax3 = fun_39b0(rdi);
        if (!(eax3 && (eax4 = fun_3910(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_37f0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_3630();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_3710(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_3710;
}

void fd_safer(int64_t rdi);

void fun_ef63(void** rdi, void** rsi, int64_t rdx) {
    int64_t v4;
    void** rdx5;
    int64_t rax6;
    int32_t eax7;
    int64_t rdi8;
    int64_t rdx9;

    __asm__("cli ");
    v4 = rdx;
    *reinterpret_cast<int32_t*>(&rdx5) = 0;
    *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
    rax6 = g28;
    if (*reinterpret_cast<unsigned char*>(&rsi) & 64) {
        *reinterpret_cast<int32_t*>(&rdx5) = *reinterpret_cast<int32_t*>(&v4);
        *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
    }
    eax7 = fun_3a50(rdi, rsi, rdx5);
    *reinterpret_cast<int32_t*>(&rdi8) = eax7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    fd_safer(rdi8);
    rdx9 = rax6 - g28;
    if (rdx9) {
        fun_3780();
    } else {
        return;
    }
}

void rpl_fseeko(struct s66* rdi);

void fun_efe3(struct s66* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_39b0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_f033(struct s66* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_3910(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_37f0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t setlocale_null_r();

int64_t fun_f0b3() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_3780();
    } else {
        return rax3;
    }
}

uint64_t fun_f133(signed char* rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;

    __asm__("cli ");
    rdx3 = *rdi;
    if (!*reinterpret_cast<signed char*>(&rdx3)) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        do {
            __asm__("rol rax, 0x9");
            ++rdi;
            rax4 = rax4 + rdx3;
            rdx3 = *rdi;
        } while (*reinterpret_cast<signed char*>(&rdx3));
        return rax4 % reinterpret_cast<uint64_t>(rsi);
    }
}

signed char* fun_3990(int64_t rdi);

signed char* fun_f173() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_3990(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

int64_t fun_f1b3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_39f0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_3750(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_3900(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_3900(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_f263() {
    __asm__("cli ");
    goto fun_39f0;
}

int32_t dup_safer();

int64_t fun_f273(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_3630();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_3840();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s67 {
    signed char[7] pad7;
    void** f7;
};

struct s68 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s69 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_f2d3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    int64_t rax12;
    int64_t v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    int64_t rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    void** rax30;
    void** r15_31;
    void** v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    void** rax41;
    void** rax42;
    struct s67* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    void** rax55;
    struct s68* r14_56;
    struct s68* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s69* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    void** rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    void** rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    void** rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = v13 - g28, !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x1020a;
                fun_3780();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_1020a_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_10214_6;
                addr_100fe_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x10123, rax21 = fun_39e0(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x10149;
                    fun_35f0(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x1016f;
                    fun_35f0(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x10195;
                    fun_35f0(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_10214_6:
            addr_fdf8_16:
            v28 = r10_16;
            addr_fdff_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0xfe04;
            rax30 = fun_3630();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_fe12_18:
            *reinterpret_cast<void***>(v32) = reinterpret_cast<void**>(12);
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0xf9b1;
                fun_35f0(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0xf9c9;
                fun_35f0(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_f608_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0xf620;
                fun_35f0(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0xf638;
            fun_35f0(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_3630();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *reinterpret_cast<void***>(rax41) = reinterpret_cast<void**>(22);
        goto addr_f608_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_f5fd_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_f5fd_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>("_memcpy_chk")) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>("oc"));
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & reinterpret_cast<uint32_t>("loc");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_3950(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_f5fd_33:
            rax55 = fun_3630();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(12);
            goto addr_f608_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_f3fc_46;
    while (1) {
        addr_fd54_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_f4bf_50;
            if (r14_56->f50 != -1) 
                goto 0x3bc6;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_fd2f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_fdf8_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_fdf8_16;
                if (r10_16 == v10) 
                    goto addr_10044_64; else 
                    goto addr_fd03_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s68*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_fd54_47;
            addr_f3fc_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_feb0_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_feb0_73;
                if (r15_31 == v10) 
                    goto addr_fe40_79; else 
                    goto addr_f457_80;
            }
            addr_f47c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0xf492;
            fun_3900(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_fe40_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0xfe48;
            rax67 = fun_3950(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_feb0_73;
            if (!r9_58) 
                goto addr_f47c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0xfe87;
            rax69 = fun_3900(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_f47c_81;
            addr_f457_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0xf462;
            rax71 = fun_39e0(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_feb0_73; else 
                goto addr_f47c_81;
            addr_10044_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x1005a;
            rax73 = fun_3950(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_10219_84;
            if (r12_9) 
                goto addr_1007a_86;
            r10_16 = rax73;
            goto addr_fd2f_55;
            addr_1007a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x1008f;
            rax75 = fun_3900(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_fd2f_55;
            addr_fd03_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0xfd1c;
            rax77 = fun_39e0(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_fdff_17;
            r10_16 = rax77;
            goto addr_fd2f_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_1020a_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_100fe_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_fdf8_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_101bd_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_101bd_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_fdf8_16; else 
                goto addr_101c7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_100d3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x101de;
        rax80 = fun_3950(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_100fe_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x101fd;
                rax82 = fun_3900(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_100fe_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x100f2;
        rax84 = fun_39e0(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_fdff_17; else 
            goto addr_100fe_7;
    }
    addr_101c7_96:
    rbx19 = r13_8;
    goto addr_100d3_98;
    addr_f4bf_50:
    if (r14_56->f50 == -1) 
        goto 0x3bc6;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x3bcb;
        goto *reinterpret_cast<int32_t*>(0x13cbc + r13_87 * 4) + 0x13cbc;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0xf58f;
        fun_3900(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0xf5c9;
        fun_3900(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0x13c4c + rax95 * 4) + 0x13c4c;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x3bc6;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s69*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x3bc6;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_f702_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_fdf8_16;
    }
    addr_f702_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_fdf8_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_f723_142; else 
                goto addr_ffb2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_ffb2_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_fdf8_16; else 
                    goto addr_ffbc_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_f723_142;
            }
        }
    }
    addr_f755_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0xf75f;
    rax102 = fun_3630();
    *reinterpret_cast<void***>(rax102) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x3bcb;
    goto *reinterpret_cast<int32_t*>(0x13c74 + rax103 * 4) + 0x13c74;
    addr_f723_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x10018;
        rax105 = fun_3950(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_10219_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x1021e;
            rax107 = fun_3630();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_fe12_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x1003f;
                fun_3900(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_f755_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0xf742;
        rax110 = fun_39e0(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_fdf8_16; else 
            goto addr_f755_147;
    }
    addr_ffbc_145:
    rbx19 = tmp64_100;
    goto addr_f723_142;
    addr_feb0_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0xfeb5;
    rax112 = fun_3630();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_fe12_18;
}

void fun_10253() {
    __asm__("cli ");
}

struct s70 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_10273(int64_t rdi, struct s70* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_102a9_5;
    return 0xffffffff;
    addr_102a9_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x13ce0 + rdx3 * 4) + 0x13ce0;
}

struct s71 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s72 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s73 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_104a3(void** rdi, struct s71* rsi, struct s72* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s72* r15_7;
    struct s71* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    struct s73* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s73* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    struct s73* rbx65;
    void* rsi66;
    int32_t eax67;
    struct s73* rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rdx88;
    void** rax89;
    void** rax90;
    void** rax91;
    void*** rax92;
    void** rdi93;
    void** rax94;
    struct s65* rbx95;
    void* rdi96;
    int32_t eax97;
    struct s65* rcx98;
    void* rax99;
    void* rdx100;
    void* rdx101;
    void* tmp64_102;
    int32_t eax103;
    int32_t eax104;
    int32_t eax105;
    int64_t rax106;
    int64_t rax107;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_10558_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_10545_7:
    return rax15;
    addr_10558_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_105c9_11;
    } else {
        addr_105c9_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_16 + 16)) | 1);
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_105e0_14;
        } else {
            goto addr_105e0_14;
        }
    }
    rax23 = reinterpret_cast<struct s73*>(rax5 + 2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s73*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_10aa8_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s73*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s73*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_10aa8_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<void**>(&rcx26->f2);
    goto addr_105c9_11;
    addr_105e0_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x13d5c + rax33 * 4) + 0x13d5c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_10727_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_10e29_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_10e2e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_10e24_42;
            }
        } else {
            addr_1060d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_10828_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_10617_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<unsigned char*>(rbx14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_11014_56; else 
                        goto addr_10865_57;
                }
            } else {
                addr_10617_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_10638_60;
                } else {
                    goto addr_10638_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_10bab_65;
    addr_10727_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_10aa8_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_1074c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_107ca_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_110cb_75; else 
            goto addr_10773_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_10aac_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_10617_52;
        goto addr_10828_44;
    }
    addr_10e2e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_1060d_43;
    addr_110cb_75:
    if (v11 != rdx53) {
        fun_35f0(rdx53, rdx53);
        r10_4 = r10_4;
        goto addr_10eda_84;
    } else {
        goto addr_10eda_84;
    }
    addr_10773_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_3950(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_39e0(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_110cb_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_3900(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_107ca_68;
    addr_10bab_65:
    rbx65 = reinterpret_cast<struct s73*>(rbx14 + 2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s73*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_10aa8_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s73*>(&rbx65->pad2);
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s73*>(&rbx65->pad2);
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_10aa8_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = reinterpret_cast<void**>(&rdx68->f2);
    goto addr_1074c_67;
    label_41:
    addr_10e24_42:
    goto addr_10e29_36;
    addr_11014_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_1103a_104;
    addr_10865_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_10aa8_22:
            r8_54 = r15_7->f8;
            goto addr_10aac_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_10874_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_10f22_111;
    } else {
        addr_10881_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_108b7_116;
        }
    }
    rdx53 = r8_54;
    goto addr_110cb_75;
    addr_10f22_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_3950(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_10eda_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_35f0(rdi86, rdi86);
            }
        } else {
            addr_11160_120:
            rdx87 = r15_7->f0;
            rdx88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx87) << 5);
            rax89 = fun_3900(rdi84, r8_85, rdx88, rdi84, r8_85, rdx88);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax89;
            goto addr_10f7f_121;
        }
    } else {
        rax90 = fun_39e0(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax90;
        if (!rax90) {
            rdx53 = r15_7->f8;
            goto addr_110cb_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_11160_120;
            }
        }
    }
    rax91 = fun_3630();
    *reinterpret_cast<void***>(rax91) = reinterpret_cast<void**>(12);
    return 0xffffffff;
    addr_10f7f_121:
    r15_7->f8 = r8_54;
    goto addr_10881_112;
    addr_108b7_116:
    rax92 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax92) {
        if (!reinterpret_cast<int1_t>(*rax92 == 5)) {
            addr_10aac_80:
            if (v11 != r8_54) {
                fun_35f0(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_10617_52;
        }
    } else {
        *rax92 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_10617_52;
    }
    rdi93 = r14_8->f8;
    if (r10_4 != rdi93) {
        fun_35f0(rdi93, rdi93);
    }
    rax94 = fun_3630();
    *reinterpret_cast<void***>(rax94) = reinterpret_cast<void**>(22);
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_10545_7;
    addr_1103a_104:
    rbx95 = reinterpret_cast<struct s65*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi96) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
    while (1) {
        eax97 = static_cast<int32_t>(rsi46 - 48);
        rcx98 = reinterpret_cast<struct s65*>(reinterpret_cast<uint64_t>(rbx95) + 0xffffffffffffffff);
        rax99 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax97)));
        if (reinterpret_cast<uint64_t>(rdi96) > 0x1999999999999999) {
            rdx100 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi96) + reinterpret_cast<uint64_t>(rdi96) * 4);
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx101) + reinterpret_cast<uint64_t>(rdx101));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx95->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_102 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rax99)), rdi96 = tmp64_102, eax103 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_102) < reinterpret_cast<uint64_t>(rdx100)) {
            if (*reinterpret_cast<unsigned char*>(&eax103) > 9) 
                goto addr_10aa8_22;
            rcx98 = rbx95;
            rax99 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax103)));
            rbx95 = reinterpret_cast<struct s65*>(&rbx95->pad2);
            rdx100 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax103) > 9) 
            break;
        rbx95 = reinterpret_cast<struct s65*>(&rbx95->pad2);
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_102) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_10aa8_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx98->f2);
    goto addr_10874_107;
    addr_10638_60:
    eax104 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax104) > 46) {
        eax105 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax105) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax105);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x13ec0 + rax106 * 4) + 0x13ec0;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax107) = *reinterpret_cast<unsigned char*>(&eax104);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax107) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x13e04 + rax107 * 4) + 0x13e04;
    }
}

uint32_t fun_36f0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_11233(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    int64_t rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_36f0(rdi);
        r12d10 = eax9;
        goto addr_11334_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_36f0(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_11334_3:
                rax14 = rax8 - g28;
                if (rax14) {
                    fun_3780();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_113e9_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_36f0(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_36f0(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_3630();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_3840();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_11334_3;
                }
            }
        } else {
            eax22 = fun_36f0(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_3630(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_11334_3;
            } else {
                eax25 = fun_36f0(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_11334_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_113e9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_11299_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_1129d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_1129d_18:
            if (0) {
            }
        } else {
            addr_112e5_23:
            eax28 = fun_36f0(rdi);
            r12d10 = eax28;
            goto addr_11334_3;
        }
        eax29 = fun_36f0(rdi);
        r12d10 = eax29;
        goto addr_11334_3;
    }
    if (0) {
    }
    eax30 = fun_36f0(rdi);
    r12d10 = eax30;
    goto addr_11334_3;
    addr_11299_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_1129d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_112e5_23;
        goto addr_1129d_18;
    }
}

void fun_114a3() {
    __asm__("cli ");
}

void fun_114b7() {
    __asm__("cli ");
    return;
}

void** optarg = reinterpret_cast<void**>(0);

void fun_3d38() {
    void** rdx1;
    void** rsi2;
    void** rcx3;
    void** rax4;
    void** rdx5;

    rdx1 = optarg;
    rax4 = xmalloc(16, rsi2, rdx1, rcx3);
    *reinterpret_cast<void***>(rax4) = rdx1;
    rdx5 = fs_select_list;
    fs_select_list = rax4;
    *reinterpret_cast<void***>(rax4 + 8) = rdx5;
    goto 0x3ce0;
}

void fun_3df5() {
    print_grand_total = 1;
    goto 0x3ce0;
}

void fun_3eaa() {
    human_output_opts = 0xb0;
    output_block_size = 1;
    goto 0x3ce0;
}

void fun_3ee9() {
    int1_t zf1;

    zf1 = header_mode == 4;
    if (zf1) {
        fun_3a30();
    } else {
        goto 0x3ce0;
    }
}

void fun_3f01() {
    human_output_opts = 0x90;
    output_block_size = 1;
    goto 0x3ce0;
}

void free_mount_entry(void** rdi, void** rsi);

void fun_45bb() {
    int64_t v1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void* rsp6;
    int1_t zf7;
    void** rdx8;
    void** rcx9;
    void** r8_10;
    void** r9_11;
    void** rdx12;
    void** rcx13;
    void** r8_14;
    void** r9_15;
    void** rdx16;
    void** rcx17;
    void** r8_18;
    void** r9_19;
    void** rdx20;
    void** rcx21;
    void** r8_22;
    void** r9_23;
    void** rdx24;
    void** rcx25;
    void** r8_26;
    void** r9_27;
    void** rdx28;
    void** rcx29;
    void** r8_30;
    void** r9_31;
    void** rdx32;
    void** rcx33;
    void** r8_34;
    void** r9_35;
    void* rsp36;
    int64_t v37;
    int32_t edx38;
    int32_t ebp39;
    uint32_t eax40;
    void** rbp41;
    signed char v42;
    int32_t eax43;
    void** rsi44;
    void** rax45;
    void** rdx46;
    void** rbx47;
    void** v48;
    uint32_t eax49;
    int1_t zf50;
    int1_t zf51;
    int1_t zf52;
    void** r12_53;
    void** r13_54;
    void** r12_55;
    void** rdi56;
    int32_t eax57;
    void** v58;
    void** rdi59;
    void** v60;
    struct s4* rax61;
    struct s5* rax62;
    void** r12_63;
    void** r15_64;
    void** rax65;
    void** r14_66;
    void** rax67;
    void** rdi68;
    void** r13_69;
    uint32_t v70;
    void** rax71;
    void** rax72;
    int1_t zf73;
    void** r13_74;
    void** rdi75;
    int64_t rax76;
    struct s0* rax77;
    void** rdi78;
    struct s0* rax79;
    void** rdi80;
    int64_t rax81;
    int64_t rax82;
    int64_t rax83;
    void** rax84;
    void** rdi85;
    void** rax86;
    int64_t rax87;
    void** rdi88;
    void** rax89;
    void** rbx90;
    void** rdi91;
    void** rbx92;
    uint32_t r9d93;
    void** r8_94;
    void** rsi95;
    void** v96;
    uint32_t eax97;
    void** rdi98;
    void** r9_99;
    int64_t rax100;
    int64_t v101;

    v1 = reinterpret_cast<int64_t>(__return_address());
    alloc_field(0, 0, rdx2, rcx3, r8_4, r9_5, v1);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8);
    zf7 = print_type == 0;
    if (!zf7) {
        alloc_field(1, 0, rdx8, rcx9, r8_10, r9_11, v1);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
    }
    alloc_field(2, "Size", rdx12, rcx13, r8_14, r9_15, v1);
    alloc_field(3, 0, rdx16, rcx17, r8_18, r9_19, v1);
    alloc_field(4, "Avail", rdx20, rcx21, r8_22, r9_23, v1);
    alloc_field(5, 0, rdx24, rcx25, r8_26, r9_27, v1);
    alloc_field(10, 0, rdx28, rcx29, r8_30, r9_31, v1);
    get_header(10, 0, rdx32, rcx33, r8_34, r9_35, v1);
    rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (v37) {
        edx38 = optind;
        show_listed_fs = 1;
        if (ebp39 <= edx38) 
            goto 0x4939;
    }
    eax40 = show_all_fs;
    rbp41 = mount_list;
    v42 = *reinterpret_cast<signed char*>(&eax40);
    eax43 = 0;
    while (rbp41) {
        rbp41 = *reinterpret_cast<void***>(rbp41 + 48);
        ++eax43;
    }
    *reinterpret_cast<int32_t*>(&rsi44) = 0;
    *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
    rax45 = hash_initialize(static_cast<int64_t>(eax43));
    rdx46 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp36) - 8 + 8 + 0x90);
    rbx47 = mount_list;
    devlist_table = rax45;
    v48 = rdx46;
    if (rax45) 
        goto addr_4b2e_12;
    addr_4e11_13:
    xalloc_die();
    addr_4b2e_12:
    while (rbx47) {
        eax49 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx47 + 40));
        if (!(*reinterpret_cast<unsigned char*>(&eax49) & 2)) 
            goto addr_4b48_15;
        zf50 = show_local_fs == 0;
        if (!zf50) 
            goto addr_4b9e_17;
        addr_4b48_15:
        if (!(*reinterpret_cast<unsigned char*>(&eax49) & 1)) 
            goto addr_4b5e_18;
        zf51 = show_all_fs == 0;
        if (!zf51) 
            goto addr_4b5e_18;
        zf52 = show_listed_fs == 0;
        if (zf52) 
            goto addr_4b9e_17;
        addr_4b5e_18:
        r12_53 = fs_select_list;
        r13_54 = *reinterpret_cast<void***>(rbx47 + 24);
        if (!r12_53) {
            r12_55 = fs_exclude_list;
            if (!r12_55 || !r13_54) {
                addr_4c22_22:
                rdi56 = *reinterpret_cast<void***>(rbx47 + 8);
                rsi44 = v48;
                eax57 = fun_38f0(rdi56, rsi44, rdi56, rsi44);
                if (!(eax57 + 1)) {
                    addr_4b9e_17:
                    v58 = *reinterpret_cast<void***>(rbx47 + 32);
                } else {
                    rdi59 = devlist_table;
                    if (rdi59 && ((rsi44 = v60, rax61 = hash_lookup(), !!rax61) && (rax62 = rax61->f18, !!rax62))) {
                        r12_63 = rax62->f8;
                        r15_64 = *reinterpret_cast<void***>(r12_63 + 8);
                        rax65 = fun_3750(r15_64, r15_64);
                        r14_66 = *reinterpret_cast<void***>(rbx47 + 8);
                        rax67 = fun_3750(r14_66, r14_66);
                        rdi68 = *reinterpret_cast<void***>(r12_63 + 16);
                        if (!rdi68 || (r13_69 = *reinterpret_cast<void***>(rbx47 + 16), r13_69 == 0)) {
                            v70 = 0;
                        } else {
                            rax71 = fun_3750(rdi68, rdi68);
                            rax72 = fun_3750(r13_69, r13_69);
                            v70 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax71) < reinterpret_cast<unsigned char>(rax72));
                        }
                        zf73 = print_grand_total == 0;
                        r13_74 = *reinterpret_cast<void***>(rbx47);
                        if (!zf73 || (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx47 + 40)) & 2) || (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_63 + 40)) & 2) || (rdi75 = *reinterpret_cast<void***>(r12_63), rsi44 = r13_74, rax76 = fun_38c0(rdi75, rsi44, rdx46, rdi75, rsi44, rdx46), !*reinterpret_cast<int32_t*>(&rax76))))) {
                            *reinterpret_cast<int32_t*>(&rsi44) = 47;
                            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
                            rax77 = fun_37b0(r13_74, r13_74);
                            if (rax77 && (rdi78 = *reinterpret_cast<void***>(r12_63), *reinterpret_cast<int32_t*>(&rsi44) = 47, *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0, rax79 = fun_37b0(rdi78, rdi78), rax79 == 0) || ((rdx46 = rax67, reinterpret_cast<unsigned char>(rax65) > reinterpret_cast<unsigned char>(rdx46)) && !v70 || (rdi80 = *reinterpret_cast<void***>(r12_63), rsi44 = r13_74, rax81 = fun_38c0(rdi80, rsi44, rdx46, rdi80, rsi44, rdx46), !!*reinterpret_cast<int32_t*>(&rax81)) && (rsi44 = r15_64, rax82 = fun_38c0(r14_66, rsi44, rdx46, r14_66, rsi44, rdx46), *reinterpret_cast<int32_t*>(&rax82) == 0))) {
                                rax62->f8 = rbx47;
                            } else {
                                r12_63 = rbx47;
                            }
                            rbx47 = *reinterpret_cast<void***>(rbx47 + 48);
                            if (v42) 
                                continue;
                            free_mount_entry(r12_63, rsi44);
                            continue;
                        }
                    }
                }
            } else {
                goto addr_4c08_34;
            }
        } else {
            if (!r13_54) 
                goto addr_4c22_22;
            do {
                rsi44 = *reinterpret_cast<void***>(r12_53);
                rax83 = fun_38c0(r13_54, rsi44, rdx46, r13_54, rsi44, rdx46);
                if (!*reinterpret_cast<int32_t*>(&rax83)) 
                    break;
                r12_53 = *reinterpret_cast<void***>(r12_53 + 8);
            } while (r12_53);
            goto addr_4b9e_17;
            r12_55 = fs_exclude_list;
            if (r12_55) 
                goto addr_4c08_34; else 
                goto addr_4d81_40;
        }
        rax84 = xmalloc(32, rsi44, rdx46, 0x51e0);
        rdi85 = devlist_table;
        *reinterpret_cast<void***>(rax84 + 8) = rbx47;
        *reinterpret_cast<void***>(rax84 + 16) = rbp41;
        rsi44 = rax84;
        *reinterpret_cast<void***>(rax84) = v58;
        rax86 = hash_insert(rdi85, rsi44);
        if (!rax86) 
            goto addr_4e11_13;
        *reinterpret_cast<void***>(rax86 + 24) = rax84;
        rbx47 = *reinterpret_cast<void***>(rbx47 + 48);
        rbp41 = rax84;
        continue;
        do {
            addr_4c08_34:
            rsi44 = *reinterpret_cast<void***>(r12_55);
            rax87 = fun_38c0(r13_54, rsi44, rdx46, r13_54, rsi44, rdx46);
            if (!*reinterpret_cast<int32_t*>(&rax87)) 
                goto addr_4b9e_17;
            r12_55 = *reinterpret_cast<void***>(r12_55 + 8);
        } while (r12_55);
        goto addr_4c22_22;
        addr_4d81_40:
        goto addr_4c22_22;
    }
    if (!v42) {
        mount_list = reinterpret_cast<void**>(0);
        rdi88 = rbp41;
        while (rdi88) {
            rax89 = *reinterpret_cast<void***>(rdi88 + 8);
            rdx46 = mount_list;
            rbx90 = *reinterpret_cast<void***>(rdi88 + 16);
            *reinterpret_cast<void***>(rax89 + 48) = rdx46;
            mount_list = rax89;
            fun_35f0(rdi88, rdi88);
            rdi88 = rbx90;
        }
        rdi91 = devlist_table;
        hash_free(rdi91, rsi44);
        devlist_table = reinterpret_cast<void**>(0);
    }
    rbx92 = mount_list;
    while (1) {
        if (!rbx92) 
            goto "???";
        r9d93 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx92 + 40));
        r8_94 = *reinterpret_cast<void***>(rbx92 + 24);
        rsi95 = *reinterpret_cast<void***>(rbx92 + 8);
        v96 = rdx46;
        *reinterpret_cast<int32_t*>(&rdx46) = 0;
        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
        eax97 = r9d93;
        rdi98 = *reinterpret_cast<void***>(rbx92);
        *reinterpret_cast<uint32_t*>(&r9_99) = r9d93 & 1;
        *reinterpret_cast<int32_t*>(&r9_99 + 4) = 0;
        *reinterpret_cast<unsigned char*>(&eax97) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax97) >> 1);
        *reinterpret_cast<uint32_t*>(&rax100) = eax97 & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax100) + 4) = 0;
        v101 = rax100;
        get_dev(rdi98, rsi95, 0, 0, r8_94, r9_99, *reinterpret_cast<int32_t*>(&v101), 0, 1, v96, v1);
        rbx92 = *reinterpret_cast<void***>(rbx92 + 48);
    }
}

void fun_4606() {
    void** rdx1;
    void** rcx2;
    void** r8_3;
    void** r9_4;
    int1_t zf5;
    void** rdx6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** rdx10;
    void** rcx11;
    void** r8_12;
    void** r9_13;
    void** rdx14;
    void** rcx15;
    void** r8_16;
    void** r9_17;
    void** rdx18;
    void** rcx19;
    void** r8_20;
    void** r9_21;
    void** rdx22;
    void** rcx23;
    void** r8_24;
    void** r9_25;
    void** rdx26;
    void** rcx27;
    void** r8_28;
    void** r9_29;

    alloc_field(0, 0, rdx1, rcx2, r8_3, r9_4, __return_address());
    zf5 = print_type == 0;
    if (!zf5) {
        alloc_field(1, 0, rdx6, rcx7, r8_8, r9_9, __return_address());
    }
    alloc_field(6, 0, rdx10, rcx11, r8_12, r9_13, __return_address());
    alloc_field(7, 0, rdx14, rcx15, r8_16, r9_17, __return_address());
    alloc_field(8, 0, rdx18, rcx19, r8_20, r9_21, __return_address());
    alloc_field(9, 0, rdx22, rcx23, r8_24, r9_25, __return_address());
    alloc_field(10, 0, rdx26, rcx27, r8_28, r9_29, __return_address());
    goto 0x430f;
}

void fun_465d() {
    void** rdx1;
    void** rcx2;
    void** r8_3;
    void** r9_4;
    int1_t zf5;
    void** rdx6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** rdx10;
    void** rcx11;
    void** r8_12;
    void** r9_13;
    void** rdx14;
    void** rcx15;
    void** r8_16;
    void** r9_17;
    void** rdx18;
    void** rcx19;
    void** r8_20;
    void** r9_21;

    alloc_field(0, 0, rdx1, rcx2, r8_3, r9_4, __return_address());
    zf5 = print_type == 0;
    if (!zf5) {
        alloc_field(1, 0, rdx6, rcx7, r8_8, r9_9, __return_address());
    }
    alloc_field(2, 0, rdx10, rcx11, r8_12, r9_13, __return_address());
    alloc_field(3, 0, rdx14, rcx15, r8_16, r9_17, __return_address());
    alloc_field(4, 0, rdx18, rcx19, r8_20, r9_21, __return_address());
    goto 0x42f9;
}

void fun_469e() {
    int1_t zf1;
    void** rsi2;
    void** rdx3;
    void** rcx4;
    void** r8_5;
    void** r9_6;

    zf1 = ncolumns == 0;
    if (!zf1) 
        goto 0x430f;
    decode_output_arg("source,fstype,itotal,iused,iavail,ipcent,size,used,avail,pcent,file,target", rsi2, rdx3, rcx4, r8_5, r9_6, __return_address());
    goto 0x430f;
}

struct s74 {
    signed char[48] pad48;
    uint64_t f30;
};

struct s75 {
    signed char[24] pad24;
    uint64_t f18;
};

struct s76 {
    signed char[32] pad32;
    unsigned char f20;
};

struct s77 {
    signed char[56] pad56;
    signed char f38;
};

struct s78 {
    signed char[32] pad32;
    uint64_t f20;
};

void fun_5ad0() {
    void* rsp1;
    struct s74* rcx2;
    uint64_t rdx3;
    struct s75* rcx4;
    int32_t* rsi5;
    struct s76* rcx6;
    struct s77* rcx7;
    int32_t* rdi8;
    uint64_t tmp64_9;
    uint64_t rax10;
    uint64_t rax11;
    uint64_t rax12;
    uint64_t rax13;
    uint1_t cf14;
    uint64_t rax15;
    int1_t pf16;
    uint1_t zf17;
    uint1_t below_or_equal18;
    uint64_t rax19;
    void** rax20;
    void* rsp21;
    void** v22;
    void** r15_23;
    uint64_t rax24;
    int32_t eax25;
    int32_t eax26;
    int64_t rax27;
    int32_t eax28;
    void** rdx29;
    uint64_t rax30;
    void* rbp31;
    int1_t below_or_equal32;
    int64_t rbx33;
    void** rcx34;
    void** rax35;
    int64_t rbp36;

    rsp1 = __zero_stack_offset();
    if (rcx2->f30 <= 0xfffffffffffffffd && (rdx3 = rcx4->f18, rdx3 <= 0xfffffffffffffffd)) {
        *reinterpret_cast<uint32_t*>(&rsi5) = rcx6->f20;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi5) + 4) = 0;
        if (rcx7->f38) {
            if (__intrinsic()) {
                __asm__("pxor xmm0, xmm0");
                __asm__("cvtsi2sd xmm0, rcx");
                __asm__("addsd xmm0, xmm0");
            } else {
                __asm__("pxor xmm0, xmm0");
                __asm__("cvtsi2sd xmm0, rax");
            }
            __asm__("xorpd xmm0, [rip+0xd0f2]");
        } else {
            if (rcx2->f30 > 0x28f5c28f5c28f5c || ((*reinterpret_cast<int32_t*>(&rdi8) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0, tmp64_9 = rcx2->f30 + rdx3, *reinterpret_cast<unsigned char*>(&rdi8) = reinterpret_cast<uint1_t>(tmp64_9 < rcx2->f30), !tmp64_9) || *reinterpret_cast<unsigned char*>(&rsi5) != *reinterpret_cast<unsigned char*>(&rdi8))) {
                if (reinterpret_cast<int64_t>(rcx2->f30) < reinterpret_cast<int64_t>(0)) {
                    __asm__("pxor xmm0, xmm0");
                    __asm__("cvtsi2sd xmm0, rcx");
                    __asm__("addsd xmm0, xmm0");
                } else {
                    __asm__("pxor xmm0, xmm0");
                    __asm__("cvtsi2sd xmm0, rax");
                }
            } else {
                rax10 = rcx2->f30 + rcx2->f30 * 4;
                rax11 = rax10 + rax10 * 4 << 2;
                rdx3 = rax11 % tmp64_9;
                rax12 = rax11 / tmp64_9;
                rax13 = rax12 - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(rax12 < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(rdx3 < 1)))));
                cf14 = 0;
                if (reinterpret_cast<int64_t>(rax13) < reinterpret_cast<int64_t>(0)) {
                    __asm__("pxor xmm0, xmm0");
                    __asm__("pxor xmm2, xmm2");
                    *reinterpret_cast<uint32_t*>(&rax15) = *reinterpret_cast<uint32_t*>(&rax13) & 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    rdx3 = rax13 >> 1 | rax15;
                    cf14 = 0;
                    __asm__("cvtsi2sd xmm0, rdx");
                    __asm__("addsd xmm0, xmm0");
                    goto addr_5e27_13;
                } else {
                    __asm__("pxor xmm0, xmm0");
                    __asm__("pxor xmm2, xmm2");
                    __asm__("cvtsi2sd xmm0, rax");
                    goto addr_5e27_13;
                }
            }
        }
        if (!*reinterpret_cast<unsigned char*>(&rsi5)) 
            goto addr_5e70_16;
        cf14 = reinterpret_cast<uint1_t>(!!rdx3);
        rdx3 = -rdx3;
        pf16 = __intrinsic();
        zf17 = reinterpret_cast<uint1_t>(rdx3 == 0);
        below_or_equal18 = reinterpret_cast<uint1_t>(cf14 | zf17);
        if (__intrinsic()) {
            rax19 = rdx3;
            *reinterpret_cast<uint32_t*>(&rdx3) = *reinterpret_cast<uint32_t*>(&rdx3) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            cf14 = 0;
            pf16 = __intrinsic();
            zf17 = reinterpret_cast<uint1_t>((rax19 >> 1 | rdx3) == 0);
            below_or_equal18 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf17));
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rdx");
        }
        __asm__("xorpd xmm1, [rip+0xd1bb]");
        goto addr_5dd5_21;
    }
    while (1) {
        addr_5ade_22:
        rdi8 = reinterpret_cast<int32_t*>("-");
        rax20 = fun_3ae0();
        rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8);
        v22 = rax20;
        r15_23 = rax20;
        while (!r15_23) {
            while (1) {
                xalloc_die();
                rsp1 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                addr_5e70_16:
                cf14 = 0;
                pf16 = __intrinsic();
                zf17 = reinterpret_cast<uint1_t>(rdx3 == 0);
                below_or_equal18 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf17));
                if (reinterpret_cast<int64_t>(rdx3) < reinterpret_cast<int64_t>(0)) {
                    rax24 = rdx3;
                    *reinterpret_cast<uint32_t*>(&rdx3) = *reinterpret_cast<uint32_t*>(&rdx3) & 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
                    __asm__("pxor xmm1, xmm1");
                    cf14 = 0;
                    pf16 = __intrinsic();
                    zf17 = reinterpret_cast<uint1_t>((rax24 >> 1 | rdx3) == 0);
                    below_or_equal18 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf17));
                    __asm__("cvtsi2sd xmm1, rax");
                    __asm__("addsd xmm1, xmm1");
                } else {
                    __asm__("pxor xmm1, xmm1");
                    __asm__("cvtsi2sd xmm1, rdx");
                }
                addr_5dd5_21:
                __asm__("addsd xmm1, xmm0");
                __asm__("pxor xmm2, xmm2");
                __asm__("ucomisd xmm1, xmm2");
                if (pf16) 
                    goto addr_5de9_27;
                if (zf17) 
                    goto addr_5ade_22;
                addr_5de9_27:
                __asm__("mulsd xmm0, [rip+0xd1af]");
                __asm__("pxor xmm3, xmm3");
                __asm__("divsd xmm0, xmm1");
                *rdi8 = *rsi5;
                ++rsi5;
                __asm__("cvttsd2si rax, xmm0");
                __asm__("cvtsi2sd xmm3, rax");
                __asm__("movapd xmm4, xmm3");
                __asm__("subsd xmm4, xmm1");
                __asm__("comisd xmm0, xmm4");
                if (!below_or_equal18 && !cf14) {
                    __asm__("comisd xmm0, xmm3");
                    if (!below_or_equal18) {
                        __asm__("movapd xmm0, xmm1");
                    } else {
                        __asm__("addsd xmm3, xmm2");
                        __asm__("movapd xmm0, xmm3");
                    }
                }
                addr_5e27_13:
                __asm__("comisd xmm0, xmm2");
                if (cf14) 
                    goto addr_5ade_22;
                rdi8 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp1) + 40);
                rsi5 = reinterpret_cast<int32_t*>("%.0f%%");
                eax25 = rpl_asprintf();
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8);
                if (eax25 != -1) 
                    break;
                v22 = reinterpret_cast<void**>(0);
            }
            r15_23 = v22;
        }
        break;
    }
    eax26 = tty_out_1;
    if (eax26 >= 0) 
        goto addr_5b09_39;
    eax26 = fun_3670(1);
    tty_out_1 = eax26;
    addr_5b09_39:
    rax27 = 0x52e0;
    if (!eax26) {
        rax27 = 0x5190;
    }
    rax27(r15_23);
    eax28 = gnu_mbswidth(v22);
    rdx29 = columns;
    rax30 = reinterpret_cast<uint64_t>(static_cast<int64_t>(eax28));
    if (rax30 < (*reinterpret_cast<struct s78**>(reinterpret_cast<unsigned char>(rdx29) + reinterpret_cast<uint64_t>(rbp31)))->f20) {
        rax30 = (*reinterpret_cast<struct s78**>(reinterpret_cast<unsigned char>(rdx29) + reinterpret_cast<uint64_t>(rbp31)))->f20;
    }
    below_or_equal32 = ncolumns <= rbx33 + 1;
    (*reinterpret_cast<struct s78**>(reinterpret_cast<unsigned char>(rdx29) + reinterpret_cast<uint64_t>(rbp31)))->f20 = rax30;
    rcx34 = nrows;
    rax35 = table;
    *reinterpret_cast<void***>(*reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rax35 + reinterpret_cast<unsigned char>(rcx34) * 8) - 8) + rbp36) = v22;
    if (!below_or_equal32) 
        goto 0x5a10; else 
        goto "???";
}

struct s79 {
    signed char[24] pad24;
    uint64_t f18;
};

struct s80 {
    signed char[8] pad8;
    void** f8;
};

struct s81 {
    signed char[32] pad32;
    unsigned char f20;
};

void fun_5bb0() {
    uint64_t rdi1;
    struct s79* rcx2;
    void** r8_3;
    struct s80* rcx4;
    void** r9_5;
    void*** rcx6;
    uint32_t eax7;
    struct s81* rcx8;
    void* rcx9;
    void** rdx10;
    void** rsi11;
    uint64_t rdi12;
    void** rcx13;
    void** rax14;
    void** rdi15;
    void** rax16;
    void** v17;
    void* v18;
    int32_t eax19;

    rdi1 = rcx2->f18;
    r8_3 = rcx4->f8;
    r9_5 = *rcx6;
    eax7 = rcx8->f20;
    if (rdi1 <= 0xfffffffffffffffd) {
        *reinterpret_cast<uint32_t*>(&rcx9) = *reinterpret_cast<unsigned char*>(&eax7);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx10) = human_output_opts;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rsi11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + reinterpret_cast<int64_t>(rcx9) + 0x180);
        if (*reinterpret_cast<unsigned char*>(&eax7)) {
            addr_5d3e_3:
            rdi12 = -rdi1;
            rcx13 = r9_5;
            rax14 = human_readable(rdi12, rsi11, rdi12, rsi11);
            *reinterpret_cast<void***>(rax14 + 0xffffffffffffffff) = reinterpret_cast<void**>(45);
            rdi15 = rax14 + 0xffffffffffffffff;
        } else {
            rcx13 = r9_5;
            rax16 = human_readable(rdi1, rsi11, rdi1, rsi11);
            rdi15 = rax16;
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&eax7)) {
            *reinterpret_cast<uint32_t*>(&rdx10) = human_output_opts;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rsi11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x181);
            goto addr_5d3e_3;
        } else {
            rdi15 = reinterpret_cast<void**>("-");
        }
    }
    xstrdup(rdi15, rsi11, rdx10, rcx13, r8_3, r9_5, __return_address(), v17, v18);
    eax19 = tty_out_1;
    if (eax19 >= 0) 
        goto 0x5b09; else 
        goto "???";
}

struct s82 {
    signed char[48] pad48;
    uint64_t f30;
};

void fun_5c10() {
    struct s82* rcx1;

    if (rcx1->f30 > 0xfffffffffffffffd) 
        goto 0x5bc5; else 
        goto "???";
}

struct s83 {
    signed char[16] pad16;
    uint64_t f10;
};

void fun_5c50() {
    struct s83* rcx1;
    uint64_t rdi2;
    void** rsi3;

    if (rcx1->f10 > 0xfffffffffffffffd) 
        goto 0x5bd8;
    rdi2 = rcx1->f10;
    rsi3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x180);
    human_readable(rdi2, rsi3, rdi2, rsi3);
    goto 0x5bd8;
}

void fun_5c90() {
    void** r13_1;
    void** rsi2;
    void** rdx3;
    void** rcx4;
    void** r8_5;
    void** r9_6;
    void** v7;
    void* v8;

    xstrdup(r13_1, rsi2, rdx3, rcx4, r8_5, r9_6, __return_address(), v7, v8);
    goto 0x5afb;
}

void fun_aec5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    uint64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int32_t eax75;
    void* rdi76;
    unsigned char v77;
    void* rdi78;
    void* v79;
    uint32_t esi80;
    uint32_t ebp81;
    uint32_t eax82;
    uint32_t eax83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    void* rdx88;
    void* rcx89;
    void* v90;
    uint16_t** rax91;
    uint1_t zf92;
    int32_t ecx93;
    uint32_t ecx94;
    uint32_t edi95;
    int32_t ecx96;
    uint32_t edi97;
    uint32_t edi98;
    uint64_t rax99;
    uint32_t eax100;
    uint32_t r12d101;
    uint64_t rax102;
    uint64_t rax103;
    uint32_t r12d104;
    void** v105;
    void** rdx106;
    void* rax107;
    void* v108;
    int64_t rax109;
    int64_t v110;
    int64_t rax111;
    int64_t rax112;
    int64_t rax113;
    int64_t v114;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_3730();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_3730();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_3750(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_b1c3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_b1c3_22; else 
                            goto addr_b5bd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_b67d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_b9d0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_b1c0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_b1c0_30; else 
                                goto addr_b9e9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_3750(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_b9d0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_3870(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_b9d0_28; else 
                            goto addr_b06c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_bb30_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_b9b0_40:
                        if (r11_27 == 1) {
                            addr_b53d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_baf8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_b177_44;
                            }
                        } else {
                            goto addr_b9c0_46;
                        }
                    } else {
                        addr_bb3f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_b53d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_b1c3_22:
                                if (v47 != 1) {
                                    addr_b719_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_3750(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_b764_54;
                                    }
                                } else {
                                    goto addr_b1d0_56;
                                }
                            } else {
                                addr_b175_57:
                                ebp36 = 0;
                                goto addr_b177_44;
                            }
                        } else {
                            addr_b9a4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_bb3f_47; else 
                                goto addr_b9ae_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_b53d_41;
                        if (v47 == 1) 
                            goto addr_b1d0_56; else 
                            goto addr_b719_52;
                    }
                }
                addr_b231_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_b0c8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_b0ed_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_b3f0_65;
                    } else {
                        addr_b259_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_baa8_67;
                    }
                } else {
                    goto addr_b250_69;
                }
                addr_b101_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_b14c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_baa8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_b14c_81;
                }
                addr_b250_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_b0ed_64; else 
                    goto addr_b259_66;
                addr_b177_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_b22f_91;
                if (v22) 
                    goto addr_b18f_93;
                addr_b22f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_b231_62;
                addr_b764_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70, reinterpret_cast<unsigned char>(v62) - reinterpret_cast<unsigned char>(r13_69), rcx44);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_beeb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_bf5b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_bd5f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    eax75 = fun_3b00();
                    if (!eax75) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_3af0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi76 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_b85e_109:
                if (reinterpret_cast<uint64_t>(rdi76) <= 1) {
                    addr_b21c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_b868_112;
                    }
                } else {
                    addr_b868_112:
                    v77 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi78 = v79;
                    esi80 = 0;
                    ebp81 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi76) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_b939_114;
                }
                addr_b228_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_b22f_91;
                while (1) {
                    addr_b939_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi80) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax82 = esi80;
                        if (*reinterpret_cast<signed char*>(&ebp81)) 
                            goto addr_be47_117;
                        eax83 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax83) & *reinterpret_cast<unsigned char*>(&esi80));
                        if (*reinterpret_cast<unsigned char*>(&eax83)) 
                            goto addr_b8a6_119;
                    } else {
                        eax54 = (esi80 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_be55_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_b927_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_b927_128;
                        }
                    }
                    addr_b8d5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax84 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax84) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax84) >> 6);
                        eax85 = eax84 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax85);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 3);
                        eax87 = (eax86 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi80 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_b927_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_b8a6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax83;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_b8d5_134;
                }
                ebp36 = v77;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_b14c_81;
                addr_be55_125:
                ebp36 = v77;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_baa8_67;
                addr_beeb_96:
                rdi76 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_b85e_109;
                addr_bf5b_98:
                r11_27 = v62;
                rdi76 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx88 = rdi76;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx89 = v90;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx89) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx88 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx88) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx88));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi76 = rdx88;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_b85e_109;
                addr_b1d0_56:
                rax91 = fun_3b40(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi76) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi76) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf92 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax91 + reinterpret_cast<unsigned char>(rax24)) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf92);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf92) & reinterpret_cast<unsigned char>(v32));
                goto addr_b21c_110;
                addr_b9ae_59:
                goto addr_b9b0_40;
                addr_b67d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_b1c3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_b228_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_b175_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_b1c3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_b6c2_160;
                if (!v22) 
                    goto addr_ba97_162; else 
                    goto addr_bca3_163;
                addr_b6c2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_ba97_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_baa8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_b56b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_b3d3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_b101_70; else 
                    goto addr_b3e7_169;
                addr_b56b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_b0c8_63;
                goto addr_b250_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_b9a4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_badf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_b1c0_30;
                    ecx93 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx93));
                    ecx94 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_b0b8_178; else 
                        goto addr_ba62_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_b9a4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_b1c3_22;
                }
                addr_badf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_b1c0_30:
                    r8d42 = 0;
                    goto addr_b1c3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_b231_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_baf8_42;
                    }
                }
                addr_b0b8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx94;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_b0c8_63;
                addr_ba62_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_b9c0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_b231_62;
                } else {
                    addr_ba72_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_b1c3_22;
                }
                edi95 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi95))) 
                    goto addr_c222_188;
                if (v28) 
                    goto addr_ba97_162;
                addr_c222_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_b3d3_168;
                addr_b06c_37:
                if (v22) 
                    goto addr_c063_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_b083_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_bb30_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_bbbb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_b1c3_22;
                    ecx96 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx96));
                    ecx94 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_b0b8_178; else 
                        goto addr_bb97_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_b9a4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_b1c3_22;
                }
                addr_bbbb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_b1c3_22;
                }
                addr_bb97_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_b9c0_46;
                goto addr_ba72_186;
                addr_b083_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_b1c3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_b1c3_22; else 
                    goto addr_b094_206;
            }
            edi97 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi97 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_c16e_208;
            edi98 = edi97 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi98;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi98));
            if (!rax24) 
                goto addr_bff4_210;
            if (1) 
                goto addr_bff2_212;
            if (!v29) 
                goto addr_bc2e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax99 = fun_3740();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax99;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_c161_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_b3f0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax100 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax100)) 
            goto addr_b1ab_219; else 
            goto addr_b40a_220;
        addr_b18f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax82 = reinterpret_cast<unsigned char>(v32);
        addr_b1a3_221:
        if (*reinterpret_cast<signed char*>(&eax82)) 
            goto addr_b40a_220; else 
            goto addr_b1ab_219;
        addr_bd5f_103:
        r12d101 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d101)) {
            addr_b40a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax102 = fun_3740();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax102;
        } else {
            addr_bd7d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax103 = fun_3740();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax103;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_c1f0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_bc56_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_be47_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_b1a3_221;
        addr_bca3_163:
        eax82 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_b1a3_221;
        addr_b3e7_169:
        goto addr_b3f0_65;
        addr_c16e_208:
        r14_35 = r12_21;
        r12d104 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d104)) 
            goto addr_b40a_220;
        goto addr_bd7d_222;
        addr_bff4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v105;
            rdx106 = r15_16;
            rax107 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx106)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx106)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx106;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax107) + reinterpret_cast<unsigned char>(rdx106));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx106;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v108) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax109 = v110 - g28;
        if (!rax109) 
            goto addr_c04e_236;
        fun_3780();
        rsp25 = rsp25 - 8 + 8;
        goto addr_c1f0_225;
        addr_bff2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_bff4_210;
        addr_bc2e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_bff4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_bc56_226;
        }
        addr_c161_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_b5bd_24:
    *reinterpret_cast<uint32_t*>(&rax111) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax111) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1342c + rax111 * 4) + 0x1342c;
    addr_b9e9_32:
    *reinterpret_cast<uint32_t*>(&rax112) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax112) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1352c + rax112 * 4) + 0x1352c;
    addr_c063_190:
    addr_b1ab_219:
    goto 0xae90;
    addr_b094_206:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1332c + rax113 * 4) + 0x1332c;
    addr_c04e_236:
    goto v114;
}

void fun_b0b0() {
}

void fun_b268() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0xaf62;
}

void fun_b2c1() {
    goto 0xaf62;
}

void fun_b3ae() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0xb231;
    }
    if (v2) 
        goto 0xbca3;
    if (!r10_3) 
        goto addr_be0e_5;
    if (!v4) 
        goto addr_bcde_7;
    addr_be0e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_bcde_7:
    goto 0xb0e4;
}

void fun_b3cc() {
}

void fun_b477() {
    signed char v1;

    if (v1) {
        goto 0xb3ff;
    } else {
        goto 0xb13a;
    }
}

void fun_b491() {
    signed char v1;

    if (!v1) 
        goto 0xb48a; else 
        goto "???";
}

void fun_b4b8() {
    goto 0xb3d3;
}

void fun_b538() {
}

void fun_b550() {
}

void fun_b57f() {
    goto 0xb3d3;
}

void fun_b5d1() {
    goto 0xb560;
}

void fun_b600() {
    goto 0xb560;
}

void fun_b633() {
    goto 0xb560;
}

void fun_ba00() {
    goto 0xb0b8;
}

void fun_bcfe() {
    signed char v1;

    if (v1) 
        goto 0xbca3;
    goto 0xb0e4;
}

void fun_bda5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0xb0e4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0xb0c8;
        goto 0xb0e4;
    }
}

void fun_c1c2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0xb430;
    } else {
        goto 0xaf62;
    }
}

void fun_d348() {
    fun_3730();
}

void fun_e1cc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0xe1db;
}

void fun_e29c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0xe2a9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0xe1db;
}

void fun_e2c0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_e2ec() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xe1db;
}

void fun_e30d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xe1db;
}

void fun_e331() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xe1db;
}

void fun_e355() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xe2e4;
}

void fun_e379() {
}

void fun_e399() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xe2e4;
}

void fun_e3b5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xe2e4;
}

int32_t fun_35e0(int64_t rdi);

struct s84 {
    signed char[1] pad1;
    signed char f1;
};

struct s85 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_f7c8() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    void** rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    void** rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    void** rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    void** rax30;
    void** rcx31;
    void* rbx32;
    void* rbx33;
    void** tmp64_34;
    void* r12_35;
    void** rax36;
    void** rbx37;
    int64_t r15_38;
    int64_t rbp39;
    void** r15_40;
    void** rax41;
    void** rax42;
    int64_t r12_43;
    void** r15_44;
    void** r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s85* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_35e0(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<void***>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_f953_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_35e0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_35e0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_f953_5;
    }
    rcx19 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x3bc6;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_f85d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0xfd37;
        }
    } else {
        addr_f855_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_f85d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0xf9a0;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0xf778;
        goto 0xfe12;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0xfe12;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_f896_26;
    rax36 = rcx31;
    addr_f896_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0xf778;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0xfe12;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_39e0(r15_40, rax36);
        if (!rax41) 
            goto 0xfe12;
        goto 0xf778;
    }
    rax42 = fun_3950(rax36, rsi10);
    if (!rax42) 
        goto 0xfe12;
    if (r12_43) 
        goto addr_fc5a_36;
    goto 0xf778;
    addr_fc5a_36:
    fun_3900(rax42, r15_44, r12_45, rax42, r15_44, r12_45);
    goto 0xf778;
    addr_f953_5:
    if ((*reinterpret_cast<struct s84**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s84**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0xf778;
    }
    if (eax7 >= 0) 
        goto addr_f855_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0xf9a0;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_f997_42;
    eax50 = 84;
    addr_f997_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_f8e0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_f9d0() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0xfb15;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_35e0(r15_4 + r12_5);
            goto 0xf82d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0xf803;
        }
    }
}

void fun_fa18() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_35e0(rdi5);
            goto 0xf82d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_35e0(rdi5);
    goto 0xf82d;
}

void fun_fa80() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xf906;
}

void fun_fad0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0xfab0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0xf90f;
}

void fun_fb80() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xf906;
    goto 0xfab0;
}

void fun_fc13() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0xf682;
}

void fun_fdb0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0xfd37;
}

struct s86 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s87 {
    signed char[8] pad8;
    int64_t f8;
};

struct s88 {
    signed char[16] pad16;
    int64_t f10;
};

struct s89 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_102b8() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s86* rcx3;
    struct s87* rcx4;
    int64_t r11_5;
    struct s88* rcx6;
    uint32_t* rcx7;
    struct s89* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x102a0; else 
        goto "???";
}

struct s90 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s91 {
    signed char[8] pad8;
    int64_t f8;
};

struct s92 {
    signed char[16] pad16;
    int64_t f10;
};

struct s93 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_102f0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s90* rcx3;
    struct s91* rcx4;
    int64_t r11_5;
    struct s92* rcx6;
    uint32_t* rcx7;
    struct s93* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x102d6;
}

struct s94 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s95 {
    signed char[8] pad8;
    int64_t f8;
};

struct s96 {
    signed char[16] pad16;
    int64_t f10;
};

struct s97 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_10310() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s94* rcx3;
    struct s95* rcx4;
    int64_t r11_5;
    struct s96* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s97* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x102d6;
}

struct s98 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s99 {
    signed char[8] pad8;
    int64_t f8;
};

struct s100 {
    signed char[16] pad16;
    int64_t f10;
};

struct s101 {
    signed char[16] pad16;
    signed char f10;
};

void fun_10330() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s98* rcx3;
    struct s99* rcx4;
    int64_t r11_5;
    struct s100* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s101* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x102d6;
}

struct s102 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s103 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_103b0() {
    struct s102* rcx1;
    struct s103* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x102d6;
}

struct s104 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s105 {
    signed char[8] pad8;
    int64_t f8;
};

struct s106 {
    signed char[16] pad16;
    int64_t f10;
};

struct s107 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_10400() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s104* rcx3;
    struct s105* rcx4;
    int64_t r11_5;
    struct s106* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s107* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x102d6;
}

void fun_1064c() {
}

void fun_1067b() {
    goto 0x10658;
}

void fun_106d0() {
}

void fun_108e0() {
    goto 0x106d3;
}

struct s108 {
    signed char[8] pad8;
    void** f8;
};

struct s109 {
    signed char[8] pad8;
    int64_t f8;
};

struct s110 {
    signed char[8] pad8;
    void** f8;
};

struct s111 {
    signed char[8] pad8;
    void** f8;
};

void fun_10988() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s108* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s109* r15_14;
    void** rax15;
    struct s110* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s111* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x110c7;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x110c7;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_3950(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x10ee8; else 
                goto "???";
        }
    } else {
        rax15 = fun_39e0(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x110c7;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_1121a_9; else 
            goto addr_109fe_10;
    }
    addr_10b54_11:
    rax19 = fun_3900(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_109fe_10:
    r14_20->f8 = rcx12;
    goto 0x10519;
    addr_1121a_9:
    r13_18 = *r14_21;
    goto addr_10b54_11;
}

struct s112 {
    signed char[11] pad11;
    void** fb;
};

struct s113 {
    signed char[80] pad80;
    int64_t f50;
};

struct s114 {
    signed char[80] pad80;
    int64_t f50;
};

struct s115 {
    signed char[8] pad8;
    void** f8;
};

struct s116 {
    signed char[8] pad8;
    void** f8;
};

struct s117 {
    signed char[8] pad8;
    void** f8;
};

struct s118 {
    signed char[72] pad72;
    signed char f48;
};

struct s119 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_10c11() {
    void** ecx1;
    int32_t edx2;
    struct s112* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s113* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s114* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s115* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rdx32;
    void** rax33;
    struct s116* r15_34;
    void** rax35;
    uint64_t r11_36;
    void** v37;
    struct s117* r15_38;
    void*** r13_39;
    struct s118* r12_40;
    signed char bpl41;
    int64_t rax42;
    int64_t* r14_43;
    struct s119* r12_44;
    int64_t rbx45;
    uint64_t r13_46;
    uint64_t* r14_47;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s112*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x10aa8;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x111fc;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_10d6d_12;
    } else {
        addr_10914_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_1094f_17;
        }
    }
    rax25 = fun_3950(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x10eda;
    addr_10e80_19:
    rdx30 = *r15_31;
    rdx32 = reinterpret_cast<void**>(rdx30 << 5);
    rax33 = fun_3900(rdi28, r8_29, rdx32, rdi28, r8_29, rdx32);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax33;
    addr_10db6_20:
    r15_34->f8 = r8_12;
    goto addr_10914_13;
    addr_10d6d_12:
    rax35 = fun_39e0(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_36;
    r8_12 = rax35;
    if (!rax35) 
        goto 0x110c7;
    if (v37 != r15_38->f8) 
        goto addr_10db6_20;
    rdi28 = r8_12;
    r8_29 = v37;
    goto addr_10e80_19;
    addr_1094f_17:
    r13_39 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_39) {
        if (*r13_39 != ecx1) {
            goto 0x10aac;
        }
    } else {
        *r13_39 = ecx1;
    }
    r12_40->f48 = bpl41;
    rax42 = *r14_43;
    r12_44->f8 = rbx45;
    r13_46 = reinterpret_cast<uint64_t>(rax42 + 1);
    *r14_47 = r13_46;
    if (r11_26 <= r13_46) 
        goto 0x10990;
    goto 0x10519;
}

void fun_10c2f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x108f8;
    if (dl2 & 4) 
        goto 0x108f8;
    if (edx3 > 7) 
        goto 0x108f8;
    if (dl4 & 2) 
        goto 0x108f8;
    goto 0x108f8;
}

void fun_10c78() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x108f8;
    if (dl2 & 4) 
        goto 0x108f8;
    if (edx3 > 7) 
        goto 0x108f8;
    if (dl4 & 2) 
        goto 0x108f8;
    goto 0x108f8;
}

void fun_10cc0() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x108f8;
    if (dl2 & 4) 
        goto 0x108f8;
    if (edx3 > 7) 
        goto 0x108f8;
    if (dl4 & 2) 
        goto 0x108f8;
    goto 0x108f8;
}

void fun_10d08() {
    goto 0x108f8;
}

void fun_42b8() {
    void** rdx1;
    void** rcx2;
    void** r8_3;
    void** r9_4;
    int1_t zf5;
    void** rdx6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** rdx10;
    void** rcx11;
    void** r8_12;
    void** r9_13;
    void** rdx14;
    void** rcx15;
    void** r8_16;
    void** r9_17;
    void** rdx18;
    void** rcx19;
    void** r8_20;
    void** r9_21;

    alloc_field(0, 0, rdx1, rcx2, r8_3, r9_4, __return_address());
    zf5 = print_type == 0;
    if (!zf5) {
        alloc_field(1, 0, rdx6, rcx7, r8_8, r9_9, __return_address());
    }
    alloc_field(2, 0, rdx10, rcx11, r8_12, r9_13, __return_address());
    alloc_field(3, 0, rdx14, rcx15, r8_16, r9_17, __return_address());
    alloc_field(4, 0, rdx18, rcx19, r8_20, r9_21, __return_address());
}

void xstrtol_fatal(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void fun_3d6d() {
    void** rdi1;
    void** r15_2;
    int32_t eax3;
    void** r8_4;
    void** rsi5;
    int32_t v6;
    int64_t rdi7;
    void** r12_8;
    uint32_t eax9;
    signed char r14b10;
    int1_t zf11;
    void** rdi12;
    void** r9_13;

    rdi1 = optarg;
    eax3 = human_options(rdi1, 0x18408, r15_2);
    if (!eax3) 
        goto 0x3ce0;
    r8_4 = optarg;
    *reinterpret_cast<int32_t*>(&rsi5) = v6;
    *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi7) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
    xstrtol_fatal(rdi7, rsi5, 66, r12_8, r8_4);
    eax9 = header_mode;
    if (eax9 == 1) {
        goto 0x4277;
    } else {
        if (eax9 || !r14b10) {
            zf11 = print_type == 0;
            if (zf11) {
                rdi12 = optarg;
                header_mode = 4;
                if (!rdi12) 
                    goto 0x3ce0;
                decode_output_arg(rdi12, rsi5, 66, r12_8, r8_4, r9_13, __return_address());
                goto 0x3ce0;
            }
        } else {
            fun_3a30();
            usage();
        }
        goto 0x4277;
    }
}

void fun_3e01() {
    require_sync = 1;
    goto 0x3ce0;
}

void fun_3ec4() {
    show_all_fs = 1;
    goto 0x3ce0;
}

void fun_5ca8() {
    void** v1;
    void** rsi2;
    void** rdx3;
    void** rcx4;
    void** r8_5;
    void** r9_6;
    void** v7;
    void* v8;

    xstrdup(v1, rsi2, rdx3, rcx4, r8_5, r9_6, __return_address(), v7, v8);
    goto 0x5afb;
}

void fun_b2ee() {
    goto 0xaf62;
}

void fun_b4c4() {
    goto 0xb47c;
}

void fun_b58b() {
    goto 0xb0b8;
}

void fun_b5dd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0xb560;
    goto 0xb18f;
}

void fun_b60f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0xb56b;
        goto 0xaf90;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0xb40a;
        goto 0xb1ab;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0xbda8;
    if (r10_8 > r15_9) 
        goto addr_b4f5_9;
    addr_b4fa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0xbdb3;
    goto 0xb0e4;
    addr_b4f5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_b4fa_10;
}

void fun_b642() {
    goto 0xb177;
}

void fun_ba10() {
    goto 0xb177;
}

void fun_c1af() {
    int32_t ebx1;

    if (ebx1) {
        goto 0xb2cc;
    } else {
        goto 0xb430;
    }
}

void fun_d400() {
}

void fun_e26f() {
    if (__intrinsic()) 
        goto 0xe2a9; else 
        goto "???";
}

struct s120 {
    signed char[1] pad1;
    signed char f1;
};

void fun_f670() {
    signed char* r13_1;
    struct s120* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_fdbe() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0xfd37;
}

struct s121 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s122 {
    signed char[8] pad8;
    int64_t f8;
};

struct s123 {
    signed char[16] pad16;
    int64_t f10;
};

struct s124 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_103d0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s121* rcx3;
    struct s122* rcx4;
    int64_t r11_5;
    struct s123* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s124* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x102d6;
}

void fun_10685() {
    goto 0x10658;
}

void fun_10fd4() {
    goto 0x108f8;
}

void fun_108e8() {
}

void fun_10d18() {
    goto 0x108f8;
}

void fun_3e0d() {
    require_sync = 0;
    goto 0x3ce0;
}

void fun_3ed0() {
    int1_t zf1;

    zf1 = header_mode == 4;
    if (zf1) 
        goto 0x508d;
    print_type = 1;
    goto 0x3ce0;
}

void fun_5cc0() {
    goto 0x5bd8;
}

void fun_b64c() {
    goto 0xb5e7;
}

void fun_ba1a() {
    goto 0xb53d;
}

void fun_d460() {
    fun_3730();
    goto fun_3ad0;
}

void fun_fb50() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xf906;
    goto 0xfab0;
}

void fun_fdcc() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0xfd37;
}

struct s125 {
    int32_t f0;
    int32_t f4;
};

struct s126 {
    int32_t f0;
    int32_t f4;
};

struct s127 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s128 {
    signed char[8] pad8;
    int64_t f8;
};

struct s129 {
    signed char[8] pad8;
    int64_t f8;
};

struct s130 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_10380(struct s125* rdi, struct s126* rsi) {
    struct s127* rcx3;
    struct s128* rcx4;
    struct s129* rcx5;
    struct s130* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x102d6;
}

void fun_1068f() {
    goto 0x10658;
}

void fun_10fde() {
    goto 0x108f8;
}

void fun_3e19() {
    void** rdx1;
    void** rsi2;
    void** rcx3;
    void** rax4;
    void** rdx5;

    rdx1 = optarg;
    rax4 = xmalloc(16, rsi2, rdx1, rcx3);
    *reinterpret_cast<void***>(rax4) = rdx1;
    rdx5 = fs_exclude_list;
    fs_exclude_list = rax4;
    *reinterpret_cast<void***>(rax4 + 8) = rdx5;
    goto 0x3ce0;
}

void fun_5cd0() {
    void** r14_1;
    void** rsi2;
    void** rdx3;
    void** rcx4;
    void** r8_5;
    void** r9_6;
    void** v7;
    void* v8;

    xstrdup(r14_1, rsi2, rdx3, rcx4, r8_5, r9_6, __return_address(), v7, v8);
    goto 0x5afb;
}

void fun_b31d() {
    goto 0xaf62;
}

void fun_b658() {
    goto 0xb5e7;
}

void fun_ba27() {
    goto 0xb58e;
}

void fun_d4a0() {
    fun_3730();
    goto fun_3ad0;
}

void fun_fddb() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0xfd37;
}

void fun_10699() {
    goto 0x10658;
}

void fun_3e4e() {
    human_output_opts = 0;
    output_block_size = 0x100000;
    goto 0x3ce0;
}

void fun_b34a() {
    goto 0xaf62;
}

void fun_b664() {
    goto 0xb560;
}

void fun_d4e0() {
    fun_3730();
    goto fun_3ad0;
}

void fun_106a3() {
    goto 0x10658;
}

void fun_3e68() {
    show_local_fs = 1;
    goto 0x3ce0;
}

void fun_b36c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0xbd00;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0xb231;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0xb231;
    }
    if (v11) 
        goto 0xc063;
    if (r10_12 > r15_13) 
        goto addr_c0b3_8;
    addr_c0b8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0xbdf1;
    addr_c0b3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_c0b8_9;
}

struct s131 {
    signed char[24] pad24;
    int64_t f18;
};

struct s132 {
    signed char[16] pad16;
    void** f10;
};

struct s133 {
    signed char[8] pad8;
    void** f8;
};

void fun_d530() {
    int64_t r15_1;
    struct s131* rbx2;
    void** r14_3;
    struct s132* rbx4;
    void** r13_5;
    struct s133* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    void** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_3730();
    fun_3ad0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xd552, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_3e74() {
    human_output_opts = 0;
    output_block_size = 0x400;
    goto 0x3ce0;
}

void fun_d588() {
    fun_3730();
    goto 0xd559;
}

void fun_3e8e() {
    int1_t zf1;

    zf1 = header_mode == 4;
    if (zf1) 
        goto 0x5054;
    header_mode = 1;
    goto 0x3ce0;
}

struct s134 {
    signed char[32] pad32;
    void** f20;
};

struct s135 {
    signed char[24] pad24;
    int64_t f18;
};

struct s136 {
    signed char[16] pad16;
    void** f10;
};

struct s137 {
    signed char[8] pad8;
    void** f8;
};

struct s138 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_d5c0() {
    void** rcx1;
    struct s134* rbx2;
    int64_t r15_3;
    struct s135* rbx4;
    void** r14_5;
    struct s136* rbx6;
    void** r13_7;
    struct s137* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s138* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_3730();
    fun_3ad0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0xd5f4, __return_address(), rcx1);
    goto v15;
}

void fun_d638() {
    fun_3730();
    goto 0xd5fb;
}
