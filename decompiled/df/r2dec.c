#include <stdint.h>

/* /tmp/tmp6wcn29hf @ 0x50a0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp6wcn29hf @ 0x7900 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_raw_hasher ( const * data, size_t n) {
    rdi = data;
    rsi = n;
    /* size_t raw_hasher( const * data,size_t n); */
    rax = rdi;
    edx = 0;
    rax = rotate_right64 (rax, 3);
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x7920 */
 
int64_t dbg_raw_comparator ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    /* _Bool raw_comparator( const * a, const * b); */
    *(rcx) ^= al;
    *(rax) += al;
    *(rax) += al;
    al += dh;
}

/* /tmp/tmp6wcn29hf @ 0x7930 */
 
int64_t dbg_check_tuning (Hash_table * table) {
    rdi = table;
    /* _Bool check_tuning(Hash_table * table); */
    *(rcx) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += bh;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    if (*(rax) < 0) {
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rsi + rsi*4)) += ch;
    *(rax) += al;
    if (*(rax) >= 0) {
        __asm ("addss xmm1, dword [0x00013104]");
        xmm2 = *((rax + 4));
        __asm ("comiss xmm2, xmm1");
        if (*(rax) <= 0) {
            goto label_0;
        }
        xmm3 = *(0x00013110);
        __asm ("comiss xmm3, xmm2");
        if (*(rax) < 0) {
            goto label_0;
        }
        __asm ("comiss xmm0, xmm1");
        eax = 1;
        if (*(rax) > 0) {
            goto label_1;
        }
    }
label_0:
    *((rdi + 0x28)) = rdx;
    eax = 0;
    return rax;
    eax = 1;
label_1:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x79c0 */
 
uint64_t hash_find_entry (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg_48h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    r14 = rdx;
    r13 = rsi;
    r12d = ecx;
    rsi = *((rdi + 0x10));
    rdi = r13;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13);
    if (rax >= *((rbp + 0x10))) {
        void (*0x3b60)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    *(r14) = rbx;
    rsi = *(rbx);
    if (rsi == 0) {
        goto label_2;
    }
    if (rsi == r13) {
        goto label_3;
    }
    rdi = r13;
    al = uint64_t (*rbp + 0x38)() ();
    if (al == 0) {
        goto label_4;
    }
    rax = *(rbx);
label_1:
    if (r12b == 0) {
        goto label_0;
    }
    rdx = *((rbx + 8));
    if (rdx == 0) {
        goto label_5;
    }
    __asm ("movdqu xmm0, xmmword [rdx]");
    __asm ("movups xmmword [rbx], xmm0");
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
    do {
        rsi = *(rax);
        if (rsi == r13) {
            goto label_6;
        }
        rdi = r13;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_6;
        }
        rbx = *((rbx + 8));
label_4:
        rax = *((rbx + 8));
    } while (rax != 0);
label_2:
    eax = 0;
    do {
label_0:
        return rax;
label_6:
        rdx = *((rbx + 8));
        rax = *(rdx);
    } while (r12b == 0);
    rcx = *((rdx + 8));
    *((rbx + 8)) = rcx;
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
label_5:
    *(rbx) = 0;
    goto label_0;
label_3:
    rax = rsi;
    goto label_1;
}

/* /tmp/tmp6wcn29hf @ 0x3b60 */
 
void hash_find_entry_cold (void) {
    /* [16] -r-x section size 55634 named .text */
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x7ad0 */
 
int64_t compute_bucket_size_isra_0 (uint32_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    if (sil == 0) {
        if (rdi < 0) {
            goto label_5;
        }
        xmm1 = 0;
        __asm ("cvtsi2ss xmm1, rdi");
label_2:
        __asm ("divss xmm1, xmm0");
        r8d = 0;
        __asm ("comiss xmm1, dword [0x00013114]");
        if (rdi >= 0) {
            goto label_6;
        }
        __asm ("comiss xmm1, dword [0x00013118]");
        if (rdi < 0) {
            goto label_7;
        }
        __asm ("subss xmm1, dword [0x00013118]");
        __asm ("cvttss2si rdi, xmm1");
        __asm ("btc rdi, 0x3f");
    }
label_4:
    r9 = 0xaaaaaaaaaaaaaaab;
    eax = 0xa;
    if (rdi >= rax) {
        rax = rdi;
    }
    r8 = rax;
    r8 |= 1;
    if (r8 == -1) {
        goto label_1;
    }
label_0:
    rax = r8;
    rdx:rax = rax * r9;
    rax = rdx;
    rdx &= 0xfffffffffffffffe;
    rax >>= 1;
    rdx += rax;
    rax = r8;
    rax -= rdx;
    if (r8 <= 9) {
        goto label_8;
    }
    if (rax == 0) {
        goto label_9;
    }
    edi = 0x10;
    esi = 9;
    ecx = 3;
    while (r8 > rsi) {
        rdi += 8;
        if (rdx == 0) {
            goto label_9;
        }
        rcx += 2;
        rax = r8;
        edx = 0;
        rsi += rdi;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    }
label_3:
    rax = r8;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rdx != 0) {
        goto label_10;
    }
label_9:
    r8 += 2;
    if (r8 != -1) {
        goto label_0;
    }
    do {
label_1:
        r8d = 0;
        rax = r8;
        return rax;
label_10:
        rax = r8;
        rax >>= 0x3d;
        al = (rax != 0) ? 1 : 0;
        eax = (int32_t) al;
    } while (((r8 >> 0x3c) & 1) < 0);
    if (rax != 0) {
        goto label_1;
    }
label_6:
    rax = r8;
    return rax;
label_5:
    rax = rdi;
    edi &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rdi;
    __asm ("cvtsi2ss xmm1, rax");
    __asm ("addss xmm1, xmm1");
    goto label_2;
label_8:
    ecx = 3;
    goto label_3;
label_7:
    __asm ("cvttss2si rdi, xmm1");
    goto label_4;
}

/* /tmp/tmp6wcn29hf @ 0x7c10 */
 
uint64_t transfer_entries (uint32_t arg_8h, int64_t arg_18h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r14 = rdi;
    r12d = edx;
    rbx = *(rsi);
    if (rbx < *((rsi + 8))) {
        goto label_3;
    }
    goto label_6;
    do {
label_2:
        rbx += 0x10;
        if (*((rbp + 8)) <= rbx) {
            goto label_6;
        }
label_3:
        r15 = *(rbx);
    } while (r15 == 0);
    r13 = *((rbx + 8));
    if (r13 == 0) {
        goto label_7;
    }
    rsi = *((r14 + 0x10));
    goto label_8;
label_0:
    rcx = *((rax + 8));
    *((r13 + 8)) = rcx;
    *((rax + 8)) = r13;
    if (rdx == 0) {
        goto label_9;
    }
label_1:
    r13 = rdx;
label_8:
    r15 = *(r13);
    rdi = *(r13);
    rax = uint64_t (*r14 + 0x30)() ();
    rsi = *((r14 + 0x10));
    if (rax >= rsi) {
        void (*0x3b65)() ();
    }
    rax <<= 4;
    rax += *(r14);
    rdx = *((r13 + 8));
    if (*(rax) != 0) {
        goto label_0;
    }
    *(rax) = r15;
    rax = *((r14 + 0x48));
    *((r14 + 0x18))++;
    *(r13) = 0;
    *((r13 + 8)) = rax;
    *((r14 + 0x48)) = r13;
    if (rdx != 0) {
        goto label_1;
    }
label_9:
    r15 = *(rbx);
label_7:
    *((rbx + 8)) = 0;
    if (r12b != 0) {
        goto label_2;
    }
    rsi = *((r14 + 0x10));
    rdi = r15;
    rax = uint64_t (*r14 + 0x30)() ();
    r13 = rax;
    if (rax >= *((r14 + 0x10))) {
        void (*0x3b65)() ();
    }
    r13 <<= 4;
    r13 += *(r14);
    if (*(r13) == 0) {
        goto label_10;
    }
    rax = *((r14 + 0x48));
    if (rax == 0) {
        goto label_11;
    }
    rdx = *((rax + 8));
    *((r14 + 0x48)) = rdx;
label_5:
    rdx = *((r13 + 8));
    *(rax) = r15;
    *((rax + 8)) = rdx;
    *((r13 + 8)) = rax;
label_4:
    *(rbx) = 0;
    rbx += 0x10;
    *((rbp + 0x18))--;
    if (*((rbp + 8)) > rbx) {
        goto label_3;
    }
label_6:
    eax = 1;
    return rax;
label_10:
    *(r13) = r15;
    *((r14 + 0x18))++;
    goto label_4;
label_11:
    rax = malloc (0x10);
    if (rax != 0) {
        goto label_5;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x3b65 */
 
void transfer_entries_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b6a */
 
void hash_lookup_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3620 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmp6wcn29hf @ 0x3b6f */
 
void hash_get_first_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b75 */
 
void hash_get_next_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b7a */
 
void hash_rehash_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b7f */
 
void hash_insert_if_absent_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0xad40 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x000132ab;
        rdx = 0x000132a0;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x00012019;
        rdx = 0x00013bf9;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x000132a7;
    rdx = 0x000132a4;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xf170 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x3990 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmp6wcn29hf @ 0xae20 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x3b84)() ();
    }
    rdx = 0x00013300;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x13300 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x000132af;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x00013bf9;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0001332c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x1332c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x00012019;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x00013bf9;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x00012019;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x00013bf9;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0001342c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1342c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0001352c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1352c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x00013bf9;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x00012019;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x00012019;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x00013bf9;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmp6wcn29hf @ 0x3b84 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0xc240 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x3b89)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x3b89 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b8e */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b94 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b99 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3b9e */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3ba3 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3ba8 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3bad */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3bb2 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3bb7 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3bbc */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3bc1 */
 
void xstrtol_fatal_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3bc6 */
 
void vasnprintf_cold (void) {
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x5190 */
 
uint32_t dbg_replace_control_chars (int64_t arg1) {
    rdi = arg1;
    /* void replace_control_chars(char * cell); */
    while (al <= 0x1f) {
        if (al > 0) {
            goto label_1;
        }
label_0:
        rdi++;
        eax = *(rdi);
        if (al == 0) {
            goto label_2;
        }
    }
    if (al != 0x7f) {
        goto label_0;
    }
label_1:
    *(rdi) = 0x3f;
    goto label_0;
label_2:
    return eax;
}

/* /tmp/tmp6wcn29hf @ 0x51d0 */
 
int64_t dbg_devlist_hash (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t devlist_hash( const * x,size_t table_size); */
    rax = *(rdi);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x51e0 */
 
int64_t dbg_devlist_compare ( const * x,  const * y) {
    rdi = x;
    rsi = y;
    /* _Bool devlist_compare( const * x, const * y); */
    rax = *(rsi);
    al = (*(rdi) == rax) ? 1 : 0;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x51f0 */
 
int64_t dbg_alloc_field (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void alloc_field(int f,char const * c); */
    edx = 8;
    ebx = edi;
    rax = ncolumns;
    rdi = columns;
    rsi = rax + 1;
    *(obj.ncolumns) = rsi;
    rax = xreallocarray ();
    rdi = (int64_t) ebx;
    rsi = ncolumns;
    rcx = obj_field_data;
    rdx = rdi * 3;
    *(obj.columns) = rax;
    rdx <<= 4;
    rdx += rcx;
    *((rax + rsi*8 - 8)) = rdx;
    if (rbp != 0) {
        *((rdx + 0x18)) = rbp;
    }
    rax = rdi * 3;
    rax <<= 4;
    rcx += rax;
    if (*((rcx + 0x2c)) == 0) {
        *((rcx + 0x2c)) = 1;
        return rax;
    }
    return assert_fail ("!\"field used\", "src/df.c", 0x1a9, "alloc_field");
}

/* /tmp/tmp6wcn29hf @ 0x5290 */
 
int64_t dbg_alloc_table_row (void) {
    /* void alloc_table_row(); */
    rax = nrows;
    edx = 8;
    rdi = table;
    rsi = rax + 1;
    *(obj.nrows) = rsi;
    rax = xreallocarray ();
    rdx = nrows;
    *(obj.table) = rax;
    rbx = rax + rdx*8 - 8;
    rax = xnmalloc (*(obj.ncolumns), 8);
    *(rbx) = rax;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xda20 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xdaa0 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x52e0 */
 
int64_t dbg_replace_invalid_chars (int64_t arg1) {
    mbstate_t mbstate;
    int64_t var_8h;
    wint_t wc;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    /* void replace_invalid_chars(char * cell); */
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    strlen (rdi);
    *((rsp + 0x20)) = 0;
    r12 = rbp + rax;
    if (rbp == r12) {
        goto label_1;
    }
    rax = rsp + 0x1c;
    r15 = rbp;
    r13 = rsp + 0x20;
    *((rsp + 8)) = rax;
    while (r14 >= rax) {
        eax = iswcntrl (*((rsp + 0x1c)));
        if (eax != 0) {
            goto label_2;
        }
        r15 += rbx;
        memmove (rbp, r15, rbx);
        rbp += rbx;
        if (r12 == r15) {
            goto label_1;
        }
label_0:
        r14 = r12;
        r14 -= r15;
        rax = rpl_mbrtowc (*((rsp + 8)), r15, r14, r13);
        rbx = rax;
    }
    ebx = 1;
label_2:
    r15 += rbx;
    *(rbp) = 0x3f;
    rbp++;
    *(r13) = 0;
    if (r12 != r15) {
        goto label_0;
    }
label_1:
    *(rbp) = 0;
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x53d0 */
 
int64_t dbg_get_header (void) {
    char * cell;
    char[21] buf;
    int64_t var_8h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_2b8h;
    /* void get_header(); */
    rax = *(fs:0x28);
    *((rsp + 0x2b8)) = rax;
    eax = 0;
    alloc_table_row ();
    rdx = columns;
    if (*(obj.ncolumns) == 0) {
        goto label_5;
    }
    ebx = 0;
    r13 = "%s-%s";
    r12 = dbg_replace_invalid_chars;
    while (*(rax) != 2) {
label_0:
        rax = strdup (r9);
        *((rsp + 0x18)) = rax;
        r14 = rax;
label_1:
        if (r14 == 0) {
            goto label_6;
        }
        eax = tty_out.1;
        if (eax < 0) {
            goto label_7;
        }
label_2:
        rax = dbg_replace_control_chars;
        rdi = r14;
        if (eax != 0) {
            rax = r12;
        }
        void (*rax)() ();
        rdx = nrows;
        rax = table;
        rax = *((rax + rdx*8 - 8));
        *((rax + r15)) = rdi;
        rax = gnu_mbswidth (*((rsp + 0x18)), 0);
        rdx = columns;
        rax = (int64_t) eax;
        rcx = *((rdx + r15));
        rsi = *((rcx + 0x20));
        if (rax < rsi) {
            rax = rsi;
        }
        rbx++;
        *((rcx + 0x20)) = rax;
        if (*(obj.ncolumns) <= rbx) {
            goto label_5;
        }
        r15 = rbx*8;
        *((rsp + 0x18)) = 0;
        r14 = rdx + r15;
        edx = 5;
        rax = *(r14);
        rax = dcgettext (0, *((rax + 0x18)));
        r9 = rax;
        rax = *(r14);
    }
    eax = header_mode;
    r8d = human_output_opts;
    if (eax == 0) {
        goto label_8;
    }
    if (eax == 4) {
        goto label_9;
    }
    if (eax != 3) {
        goto label_0;
    }
    *((rsp + 8)) = r9;
    rax = umaxtostr (*(obj.output_block_size), rsp + 0x20, rdx);
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, r13);
label_3:
    eax = 0;
    eax = rpl_asprintf (rsp + 0x18, rax, r14, *((rsp + 8)), r8, r9);
    if (eax == 0xffffffff) {
        *((rsp + 0x18)) = 0;
    }
    r14 = *((rsp + 0x18));
    goto label_1;
label_7:
    eax = isatty (1);
    *(obj.tty_out.1) = eax;
    goto label_2;
label_9:
    r8d = human_output_opts;
    if ((r8b & 0x10) != 0) {
        goto label_0;
    }
label_8:
    rdi = output_block_size;
    r8d &= 0x124;
    rsi = rdi;
    rcx = rdi;
    do {
        rdx = rcx;
        rdx >>= 3;
        rax = rdx;
        rdx:rax = rax * rbp;
        rax = rdx;
        rax >>= 4;
        r9 = rax * 0x3e8;
        rax = rcx;
        rcx = rdx;
        rdx = rsi;
        rcx >>= 4;
        edx &= 0x3ff;
        rsi >>= 0xa;
        rax -= r9;
        r10 = rax;
        r10 |= rdx;
    } while (r10 == 0);
    eax = r8d;
    cl = (rax == 0) ? 1 : 0;
    dl = (rdx == 0) ? 1 : 0;
    al |= 0x98;
    r8b |= 0xb8;
    if (cl < dl) {
        eax = r8d;
    }
    edx = eax;
    if (cl <= dl) {
        goto label_10;
    }
    edx &= 0xffffffdf;
label_4:
    dh |= 1;
    do {
        ecx = 1;
        rax = human_readable (rdi, rsp + 0x20, rdx, ecx, 1, r9);
        edx = 5;
        r14 = rax;
        rax = dcgettext (0, "blocks");
        edx = 5;
        *((rsp + 8)) = rax;
        rax = dcgettext (0, r13);
        rcx = *((rsp + 8));
        rdi = rsp + 0x18;
        rsi = rax;
        goto label_3;
label_5:
        rax = *((rsp + 0x2b8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_11;
        }
        return rax;
label_10:
    } while ((al & 0x20) != 0);
    goto label_4;
label_6:
    xalloc_die ();
label_11:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x56b0 */
 
int64_t dbg_get_dev (int64_t arg_460h, int64_t arg_468h, int64_t arg_470h, int64_t arg1, int64_t arg2, int64_t arg3, void * arg4, int64_t arg5) {
    char * cell;
    fs_usage fsu;
    field_values_t block_values;
    devlist dev_entry;
    stat sb;
    char[653] buf;
    int64_t var_8h;
    void * ptr;
    uint32_t var_18h;
    uint32_t var_1fh;
    char * var_28h;
    int64_t var_30h;
    uint32_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    uint32_t var_a8h;
    int64_t var_b0h;
    int64_t var_b8h;
    int64_t var_c0h;
    int64_t var_c8h;
    int64_t var_d0h;
    int64_t var_d8h;
    int64_t var_e0h;
    int64_t var_e8h;
    int64_t var_f0h;
    int64_t var_180h;
    int64_t var_181h;
    int64_t var_418h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void get_dev(char const * device,char const * mount_point,char const * file,char const * stat_file,char const * fstype,_Bool me_dummy,_Bool me_remote,fs_usage const * force_fsu,_Bool process_all); */
    r14 = rsi;
    r13 = r8;
    eax = *((rsp + 0x470));
    *((rsp + 8)) = rdx;
    *((rsp + 0x10)) = rcx;
    r15d = *((rsp + 0x460));
    *((rsp + 0x18)) = eax;
    rbx = *((rsp + 0x468));
    rax = *(fs:0x28);
    *((rsp + 0x418)) = rax;
    eax = 0;
    if (r15b != 0) {
        if (*(obj.show_local_fs) != 0) {
            goto label_1;
        }
    }
    if (r9b != 0) {
        goto label_26;
    }
label_0:
    r12 = fs_select_list;
    if (r12 == 0) {
        goto label_27;
    }
label_2:
    if (r13 == 0) {
        goto label_4;
    }
    do {
        eax = strcmp (r13, *(r12));
        if (eax == 0) {
            goto label_28;
        }
        r12 = *((r12 + 8));
    } while (r12 != 0);
label_1:
    rax = *((rsp + 0x418));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_29;
    }
    return rax;
label_26:
    if (*(obj.show_all_fs) != 0) {
        goto label_0;
    }
    if (*(obj.show_listed_fs) == 0) {
        goto label_1;
    }
    r12 = fs_select_list;
    if (r12 != 0) {
        goto label_2;
    }
label_27:
    r12 = fs_exclude_list;
    if (r12 == 0) {
        goto label_4;
    }
    if (r13 == 0) {
        goto label_4;
    }
label_3:
    eax = strcmp (r13, *(r12));
    if (eax == 0) {
        goto label_1;
    }
    r12 = *((r12 + 8));
    if (r12 != 0) {
        goto label_3;
    }
label_4:
    rsp + 0x1f = (rbx == 0) ? 1 : 0;
    eax = *((rsp + 0x1f));
    if (r14 == 0) {
        goto label_30;
    }
    if (al == 0) {
        goto label_30;
    }
    if (*(r14) != 0x2f) {
        goto label_1;
    }
    rax = *((rsp + 0x10));
    if (rax == 0) {
        rax = r14;
    }
    *((rsp + 0x10)) = rax;
label_5:
    rdx = rsp + 0x30;
    eax = get_fs_usage (*((rsp + 0x10)), rbp);
    if (eax != 0) {
        goto label_31;
    }
    if (*((rsp + 0x18)) == 0) {
        goto label_32;
    }
    if (*(obj.show_all_fs) != 0) {
        goto label_33;
    }
    if (*((rsp + 0x38)) != 0) {
        goto label_23;
    }
label_24:
    if (*(obj.show_listed_fs) == 0) {
        goto label_1;
    }
    if (rbx == 0) {
label_23:
        *(obj.file_systems_processed) = 1;
    }
label_6:
    alloc_table_row ();
    rbx = *((rsp + 8));
    rax = 0x00013a19;
    if (rbp == 0) {
    }
    if (rbx != 0) {
        rax = rbx;
    }
    *((rsp + 8)) = rax;
    rax = xstrdup (rbp);
    *((rsp + 0x10)) = rax;
    rbx = rax;
    if (*((rsp + 0x18)) != 0) {
        rax = strlen (rax);
        if (rax <= 0x24) {
            goto label_20;
        }
        rax = strspn (rbx + rax - 0x24, "-0123456789abcdefABCDEF");
        if (rax == 0x24) {
            goto label_34;
        }
    }
label_20:
    rax = 0x00013a19;
    rdi = *((rsp + 0x58));
    *((rsp + 0xb8)) = 1;
    if (r13 == 0) {
        r13 = rax;
    }
    rax = *((rsp + 0x60));
    *((rsp + 0xb0)) = 1;
    *((rsp + 0xc0)) = rdi;
    *((rsp + 0xd8)) = rax;
    *((rsp + 0xc8)) = rax;
    *((rsp + 0xd0)) = 0;
    *((rsp + 0xe0)) = 0xffffffffffffffff;
    *((rsp + 0xe8)) = 0;
    if (rax <= 0xfffffffffffffffd) {
        if (rdi > 0xfffffffffffffffd) {
            goto label_35;
        }
        rdx = rdi;
        rdx -= rax;
        *((rsp + 0xe0)) = rdx;
        rsp + 0xe8 = (rdi < rax) ? 1 : 0;
    }
label_35:
    r8 = *((rsp + 0x48));
    rdx = output_block_size;
    *((rsp + 0xa0)) = 0xffffffffffffffff;
    r9 = *((rsp + 0x30));
    rsi = *((rsp + 0x38));
    *((rsp + 0xa8)) = 0;
    rcx = *((rsp + 0x40));
    *((rsp + 0x78)) = rdx;
    dl = (r8 <= 0xfffffffffffffffd) ? 1 : 0;
    dl &= *((rsp + 0x50));
    *((rsp + 0x70)) = r9;
    *((rsp + 0x80)) = rsi;
    *((rsp + 0x88)) = r8;
    *((rsp + 0x98)) = rcx;
    *((rsp + 0x90)) = dl;
    if (rcx <= 0xfffffffffffffffd) {
        if (rsi > 0xfffffffffffffffd) {
            goto label_36;
        }
        r10 = rsi;
        r10 -= rcx;
        *((rsp + 0xa0)) = r10;
        rsp + 0xa8 = (rsi < rcx) ? 1 : 0;
    }
label_36:
    if (*(obj.print_grand_total) != 0) {
        if (*((rsp + 0x1f)) != 0) {
            goto label_37;
        }
    }
label_22:
    ebx = 0;
    rdx = columns;
    r12 = 0x000122d8;
    if (*(obj.ncolumns) == 0) {
        goto label_38;
    }
label_7:
    rdx = *((rdx + rbx*8));
    rbp = rbx*8;
    eax = *((rdx + 0x10));
    if (eax == 1) {
        goto label_39;
    }
    if (eax != 2) {
        goto label_40;
    }
    ecx = 0;
label_12:
    if (*(rdx) > 0xb) {
        goto label_41;
    }
    eax = *(rdx);
    rax = *((r12 + rax*4));
    rax += r12;
    /* switch table (12 cases) at 0x122d8 */
    void (*rax)() ();
label_28:
    r12 = fs_exclude_list;
    if (r12 != 0) {
        goto label_3;
    }
    goto label_4;
label_30:
    if (*((rsp + 0x10)) == 0) {
        goto label_42;
    }
label_21:
    if (rbx == 0) {
        goto label_5;
    }
    __asm ("movdqa xmm5, xmmword [rbx]");
    __asm ("movdqa xmm6, xmmword [rbx + 0x10]");
    __asm ("movdqa xmm7, xmmword [rbx + 0x20]");
    rax = *((rbx + 0x30));
    *((rsp + 0x30)) = xmm5;
    *((rsp + 0x60)) = rax;
    *((rsp + 0x40)) = xmm6;
    *((rsp + 0x50)) = xmm7;
    if (*((rsp + 0x38)) != 0) {
        goto label_6;
    }
    if (*(obj.show_all_fs) != 0) {
        goto label_6;
    }
    if (*(obj.show_listed_fs) == 0) {
        goto label_1;
    }
    goto label_6;
    rax = *((rcx + 0x30));
    if (rax <= 0xfffffffffffffffd) {
        goto label_43;
    }
label_13:
    rax = strdup (0x00013a19);
    *((rsp + 0x28)) = rax;
    r15 = rax;
label_14:
    if (r15 == 0) {
        goto label_44;
    }
label_11:
    eax = tty_out.1;
    if (eax < 0) {
        goto label_45;
    }
label_8:
    rdx = dbg_replace_control_chars;
    rax = dbg_replace_invalid_chars;
    rdi = r15;
    if (eax == 0) {
        rax = rdx;
    }
    void (*rax)() ();
    rax = gnu_mbswidth (*((rsp + 0x28)), 0);
    rdx = columns;
    rax = (int64_t) eax;
    rcx = *((rdx + rbp));
    rsi = *((rcx + 0x20));
    if (rax < rsi) {
        rax = rsi;
    }
    rbx++;
    *((rcx + 0x20)) = rax;
    rcx = nrows;
    rax = table;
    rax = *((rax + rcx*8 - 8));
    rcx = *((rsp + 0x28));
    *((rax + rbp)) = rcx;
    if (*(obj.ncolumns) > rbx) {
        goto label_7;
    }
label_38:
    rax = *((rsp + 0x418));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_29;
    }
    rdi = *((rsp + 0x10));
    void (*0x35f0)() ();
    rdi = *((rcx + 0x18));
    r8 = *((rcx + 8));
    r9 = *(rcx);
    eax = *((rcx + 0x20));
    if (rdi <= 0xfffffffffffffffd) {
        goto label_46;
    }
label_9:
    if (al != 0) {
        goto label_47;
    }
label_10:
    rax = xstrdup (0x00013a19);
    *((rsp + 0x28)) = rax;
    r15 = rax;
    eax = tty_out.1;
    if (eax >= 0) {
        goto label_8;
    }
label_45:
    eax = isatty (1);
    *(obj.tty_out.1) = eax;
    goto label_8;
    rdi = *((rcx + 0x30));
    r8 = *((rcx + 8));
    r9 = *(rcx);
    eax = *((rcx + 0x38));
    if (rdi > 0xfffffffffffffffd) {
        goto label_9;
    }
label_46:
    ecx = (int32_t) al;
    edx = human_output_opts;
    rsi = rsp + rcx + 0x180;
    if (al != 0) {
        goto label_48;
    }
    rax = human_readable (rdi, rsi, rdx, r9, r8, r9);
    rdi = rax;
    goto label_10;
    r9 = *((rcx + 0x10));
    rdi = 0x00013a19;
    if (r9 > 0xfffffffffffffffd) {
        goto label_10;
    }
    rax = human_readable (r9, rsp + 0x180, *(obj.human_output_opts), *(rcx), *((rcx + 8)), r9);
    rdi = rax;
    goto label_10;
    rax = xstrdup (r13);
    *((rsp + 0x28)) = rax;
    r15 = rax;
    goto label_11;
    rax = xstrdup (*((rsp + 0x10)));
    *((rsp + 0x28)) = rax;
    r15 = rax;
    goto label_11;
    rdi = *((rsp + 8));
    goto label_10;
    rax = xstrdup (r14);
    *((rsp + 0x28)) = rax;
    r15 = rax;
    goto label_11;
label_40:
    if (eax != 0) {
        assert_fail ("!\"bad field_type\", "src/df.c", 0x480, "get_dev");
    }
    rcx = rsp + 0x70;
    goto label_12;
label_39:
    rcx = rsp + 0xb0;
    goto label_12;
label_47:
label_48:
    rdi = -rdi;
    rax = human_readable (rdi, rsp + 0x181, *(obj.human_output_opts), r9, r8, r9);
    *((rax - 1)) = 0x2d;
    rdi = rax - 1;
    goto label_10;
label_43:
    rdx = *((rcx + 0x18));
    if (rdx > 0xfffffffffffffffd) {
        goto label_13;
    }
    esi = *((rcx + 0x20));
    if (*((rcx + 0x38)) != 0) {
        goto label_49;
    }
    rcx = 0x28f5c28f5c28f5c;
    if (rax <= rcx) {
        rcx = rax;
        edi = 0;
        rcx += rdx;
        dil = (rcx < 0) ? 1 : 0;
        if (rcx != 0) {
            goto label_50;
        }
    }
label_18:
    if (rax < 0) {
        goto label_51;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, rax");
label_16:
    if (sil == 0) {
        goto label_52;
    }
    rdx = -rdx;
    if (sil < 0) {
        goto label_53;
    }
    xmm1 = 0;
    __asm ("cvtsi2sd xmm1, rdx");
label_17:
    __asm ("xorpd xmm1, xmmword [0x00012f90]");
label_15:
    __asm ("addsd xmm1, xmm0");
    xmm2 = 0;
    __asm ("ucomisd xmm1, xmm2");
    if (sil == 0) {
        goto label_54;
    }
    if (sil == 0) {
        goto label_13;
    }
label_54:
    __asm ("mulsd xmm0, qword [0x00012fa0]");
    xmm3 = 0;
    __asm ("divsd xmm0, xmm1");
    xmm1 = *(0x00012fa8);
    __asm ("cvttsd2si rax, xmm0");
    __asm ("cvtsi2sd xmm3, rax");
    __asm ("movapd xmm4, xmm3");
    __asm ("subsd xmm4, xmm1");
    __asm ("comisd xmm0, xmm4");
    if (sil > 0) {
        __asm ("addsd xmm1, xmm3");
        __asm ("comisd xmm1, xmm0");
        if (sil >= 0) {
            goto label_55;
        }
    }
label_19:
    __asm ("comisd xmm0, xmm2");
    if (sil < 0) {
        goto label_13;
    }
    eax = 1;
    eax = rpl_asprintf (rsp + 0x28, "%.0f%%", rdx, rcx, r8, r9);
    if (eax != 0xffffffff) {
        r15 = *((rsp + 0x28));
        goto label_14;
    }
    *((rsp + 0x28)) = 0;
label_44:
    rax = xalloc_die ();
label_52:
    if (rdx < 0) {
        goto label_56;
    }
    xmm1 = 0;
    __asm ("cvtsi2sd xmm1, rdx");
    goto label_15;
label_49:
    rax = -rax;
    if (rdx < 0) {
        goto label_57;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, rax");
    do {
        __asm ("xorpd xmm0, xmmword [0x00012f90]");
        goto label_16;
label_53:
        rax = rdx;
        edx &= 1;
        xmm1 = 0;
        rax >>= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm1, rax");
        __asm ("addsd xmm1, xmm1");
        goto label_17;
label_51:
        rcx = rax;
        eax &= 1;
        xmm0 = 0;
        rcx >>= 1;
        rcx |= rax;
        __asm ("cvtsi2sd xmm0, rcx");
        __asm ("addsd xmm0, xmm0");
        goto label_16;
label_56:
        rax = rdx;
        edx &= 1;
        xmm1 = 0;
        rax >>= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm1, rax");
        __asm ("addsd xmm1, xmm1");
        goto label_15;
label_57:
        rcx = rax;
        eax &= 1;
        xmm0 = 0;
        rcx >>= 1;
        rcx |= rax;
        __asm ("cvtsi2sd xmm0, rcx");
        __asm ("addsd xmm0, xmm0");
    } while (1);
label_50:
    if (sil != dil) {
        goto label_18;
    }
    rax *= 5;
    edx = 0;
    rax *= 5;
    rax <<= 2;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    rax -= 0xffffffffffffffff;
    if (rax < 0) {
        goto label_58;
    }
    xmm0 = 0;
    xmm2 = 0;
    __asm ("cvtsi2sd xmm0, rax");
    goto label_19;
label_55:
    __asm ("comisd xmm0, xmm3");
    if (rax > 0) {
        goto label_59;
    }
    __asm ("addsd xmm3, xmm2");
    __asm ("movapd xmm0, xmm3");
    goto label_19;
label_31:
    rax = errno_location ();
    rbx = rax;
    if (*((rsp + 0x18)) != 0) {
        eax = *(rax);
        if (eax == 0xd) {
            goto label_60;
        }
        if (eax == 2) {
            goto label_60;
        }
    }
    rdx = *((rsp + 0x10));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    rcx = rax;
    eax = 0;
    error (0, *(rbx), 0x00013bba);
    *(obj.exit_status) = 1;
    goto label_1;
label_34:
    rdi = *((rsp + 0x10));
    esi = 0;
    rax = canonicalize_filename_mode ();
    rbx = rax;
    if (rax == 0) {
        goto label_20;
    }
    free (*((rsp + 0x10)));
    *((rsp + 0x10)) = rbx;
    goto label_20;
label_42:
    rax = rbp;
    if (r14 != 0) {
        rax = r14;
    }
    *((rsp + 0x10)) = rax;
    goto label_21;
label_37:
    if (rdi <= 0xfffffffffffffffd) {
        *(0x000183c8) += rdi;
    }
    if (rax <= 0xfffffffffffffffd) {
        *(0x000183d0) += rax;
    }
    if (rsi <= 0xfffffffffffffffd) {
        rsi *= r9;
        *(0x000183a8) += rsi;
    }
    if (rcx <= 0xfffffffffffffffd) {
        rcx *= r9;
        *(0x000183b0) += rcx;
    }
    if (r8 > 0xfffffffffffffffd) {
        goto label_22;
    }
    ecx = *(0x000183c0);
    r9 *= r8;
    rax = *(0x000183b8);
    if (cl == dl) {
        goto label_61;
    }
    rsi = rax;
    rsi = -rsi;
    if (cl != 0) {
        rax = rsi;
    }
    rsi = r9;
    rsi = -rsi;
    if (dl != 0) {
        r9 = rsi;
    }
    if (r9 >= rax) {
        goto label_62;
    }
    rax -= r9;
    *(0x000183b8) = rax;
label_25:
    if (cl == 0) {
        goto label_22;
    }
    0x000183b8 = -0x000183b8;
    goto label_22;
label_59:
    __asm ("movapd xmm0, xmm1");
    goto label_19;
label_33:
    rdi = *((rsp + 0x10));
    rsi = rsp + 0xf0;
    eax = stat ();
    if (eax == 0) {
        rdi = devlist_table;
        rax = *((rsp + 0xf0));
        if (rdi == 0) {
            goto label_32;
        }
        rsi = rsp + 0xb0;
        *((rsp + 0xb0)) = rax;
        rax = hash_lookup ();
        if (rax == 0) {
            goto label_32;
        }
        rax = *((rax + 0x18));
        if (rax == 0) {
            goto label_32;
        }
        r12 = *((rax + 8));
        if (r12 == 0) {
            goto label_32;
        }
        eax = strcmp (*(r12), rbp);
        if (eax == 0) {
            goto label_32;
        }
        if ((*((r12 + 0x28)) & 2) != 0) {
            if (r15b != 0) {
                goto label_32;
            }
        }
        *((rsp + 0x50)) = 0;
        r13 = 0x00013a19;
        *((rsp + 0x60)) = 0xffffffffffffffff;
        *((rsp + 0x58)) = 0xffffffffffffffff;
        *((rsp + 0x48)) = 0xffffffffffffffff;
        *((rsp + 0x40)) = 0xffffffffffffffff;
        *((rsp + 0x38)) = 0xffffffffffffffff;
        *((rsp + 0x30)) = 0xffffffffffffffff;
    }
label_32:
    if (*((rsp + 0x38)) != 0) {
        goto label_23;
    }
    if (*(obj.show_all_fs) != 0) {
        goto label_23;
    }
    goto label_24;
label_60:
    if (*(obj.show_all_fs) == 0) {
        goto label_1;
    }
    *((rsp + 0x50)) = 0;
    r13 = 0x00013a19;
    *((rsp + 0x60)) = 0xffffffffffffffff;
    *((rsp + 0x58)) = 0xffffffffffffffff;
    *((rsp + 0x48)) = 0xffffffffffffffff;
    *((rsp + 0x40)) = 0xffffffffffffffff;
    *((rsp + 0x38)) = 0xffffffffffffffff;
    *((rsp + 0x30)) = 0xffffffffffffffff;
    goto label_23;
label_58:
    rdx = rax;
    xmm0 = 0;
    xmm2 = 0;
    eax &= 1;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2sd xmm0, rdx");
    __asm ("addsd xmm0, xmm0");
    goto label_19;
label_41:
    rax = assert_fail ("!\"unhandled field\", "src/df.c", 0x4e6, "get_dev");
label_62:
    r9 -= rax;
    *(0x000183c0) = dl;
    ecx = edx;
    *(0x000183b8) = r9;
    goto label_25;
label_61:
    r9 += rax;
    *(0x000183b8) = r9;
    goto label_22;
label_29:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x66f0 */
 
uint64_t dbg_decode_output_arg (void) {
    int64_t var_8h;
    /* void decode_output_arg(char const * arg); */
    r12 = obj_field_data;
    rax = xstrdup (rdi);
    *((rsp + 8)) = rax;
    r13 = rax;
label_0:
    ebx = 0;
    rax = strchr (r13, 0x2c);
    if (rax != 0) {
        *(rax) = 0;
        rbx = rax + 1;
    }
    r14 = str_source;
    ebp = 0;
    do {
        r15 = *(r14);
        eax = strcmp (r15, r13);
        if (eax == 0) {
            goto label_3;
        }
        ebp++;
        r14 += 0x30;
    } while (ebp != 0xc);
    rax = quote (r13, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
label_2:
    rax = dcgettext (0, "option --output: field %s unknown");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    usage (1);
label_3:
    eax = ebp;
    rax *= 3;
    rax <<= 4;
    if (*((r12 + rax + 0x2c)) != 0) {
        goto label_4;
    }
    if (ebp == 2) {
        goto label_5;
    }
    if (ebp == 4) {
        goto label_6;
    }
    alloc_field (ebp, 0);
label_1:
    if (rbx == 0) {
        goto label_7;
    }
    do {
        r13 = rbx;
        goto label_0;
label_5:
        alloc_field (2, "Size");
    } while (rbx != 0);
label_7:
    rdi = *((rsp + 8));
    void (*0x35f0)() ();
label_6:
    alloc_field (4, "Avail");
    goto label_1;
label_4:
    rax = quote (r15, rsi, rdx, rcx, r8);
    edx = 5;
    rsi = "option --output: field %s used more than once";
    r12 = rax;
    goto label_2;
}

/* /tmp/tmp6wcn29hf @ 0x50d0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = loc__edata;
    rax = loc__edata;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x5100 */
 
int64_t register_tm_clones (void) {
    rdi = loc__edata;
    rsi = loc__edata;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x5140 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_000035c0 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp6wcn29hf @ 0x35c0 */
 
void fcn_000035c0 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp6wcn29hf @ 0x5180 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp6wcn29hf @ 0x6c40 */
 
int64_t dbg_canonicalize_filename_mode_stk (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_1h;
    stat st;
    scratch_buffer extra_buffer;
    scratch_buffer link_buffer;
    uint32_t var_8h;
    uint32_t var_10h;
    size_t n;
    size_t * s1;
    uint32_t var_28h;
    size_t * s2;
    int64_t var_3ch;
    int64_t var_40h;
    uint32_t var_4bh;
    signed int64_t var_4ch;
    size_t * var_50h;
    size_t * var_58h;
    int64_t var_60h;
    int64_t var_f0h;
    uint32_t var_f8h;
    int64_t var_100h;
    int64_t var_500h;
    int64_t var_508h;
    int64_t var_510h;
    int64_t var_918h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * canonicalize_filename_mode_stk(char const * name,canonicalize_mode_t can_mode,scratch_buffer * rname_buf); */
    r14d = esi;
    r14d &= 3;
    rax = *(fs:0x28);
    *((rsp + 0x918)) = rax;
    eax = 0;
    eax = r14 - 1;
    if ((eax & r14d) != 0) {
        goto label_21;
    }
    if (rdi == 0) {
        goto label_21;
    }
    if (*(rdi) == 0) {
        goto label_22;
    }
    rax = rsp + 0x100;
    ebx = esi;
    *((rdx + 8)) = 0x400;
    r12 = rdx;
    *((rsp + 8)) = rax;
    esi = 0x400;
    *((rsp + 0xf0)) = rax;
    rax = rsp + 0x510;
    *(rsp) = rax;
    *((rsp + 0x500)) = rax;
    rax = rdx + 0x10;
    *(rdx) = rax;
    r13 = rax;
    *((rsp + 0xf8)) = 0x400;
    *((rsp + 0x508)) = 0x400;
    *((rsp + 0x10)) = rax;
    if (*(rdi) == 0x2f) {
        goto label_23;
    }
    do {
        rdi = r13;
        rax = getcwd ();
        if (rax != 0) {
            goto label_24;
        }
        rax = errno_location ();
        eax = *(rax);
        if (eax == 0xc) {
            goto label_12;
        }
        if (eax != 0x22) {
            r15 = r13;
            ebx = 1;
label_4:
            rdi = *((rsp + 0xf0));
            if (rdi != *((rsp + 8))) {
                free (rdi);
            }
            rdi = *((rsp + 0x500));
            if (rdi != *(rsp)) {
                free (rdi);
            }
            if (bl != 0) {
                goto label_25;
            }
            *(r13) = 0;
            rsi -= r15;
            rax = gl_scratch_buffer_dupfree (r12, r13 + 1);
            if (rax == 0) {
                goto label_12;
            }
label_0:
            rdx = *((rsp + 0x918));
            rdx -= *(fs:0x28);
            if (rdx != 0) {
                goto label_26;
            }
            return rax;
        }
        al = gl_scratch_buffer_grow (r12);
        if (al == 0) {
            goto label_12;
        }
        r13 = *(r12);
        rsi = *((r12 + 8));
    } while (1);
label_21:
    errno_location ();
    *(rax) = 0x16;
    eax = 0;
    goto label_0;
label_1:
    if (dl == 0x2e) {
        goto label_17;
    }
label_2:
    if (*((r13 - 1)) != 0x2f) {
        *(r13) = 0x2f;
        r13++;
    }
    rax = *((rsp + 0x18));
    rsi = rax + 2;
    rax = *((r12 + 8));
    rax += r15;
    rax -= r13;
    if (rax >= rsi) {
        goto label_27;
    }
    *((rsp + 0x30)) = rbx;
    rcx = r13;
    rbx = rsi;
    r13 = r12;
    r12 = rbp;
    while (al != 0) {
        rax = *((r13 + 8));
        r15 = *(r13);
        rax -= rbp;
        rcx = r15 + rbp;
        if (rax >= rbx) {
            goto label_28;
        }
        rcx -= r15;
        al = gl_scratch_buffer_grow_preserve (r13);
    }
label_12:
    xalloc_die ();
label_23:
    *((rdx + 0x10)) = 0x2f;
    r15 = *((rsp + 0x10));
    r13 = rdx + 0x11;
label_11:
    eax = *(rbp);
    if (al == 0) {
        goto label_29;
    }
    ebx &= 4;
    rcx = rsp + 0x500;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x3c)) = ebx;
    *((rsp + 0x4b)) = 0;
    *((rsp + 0x4c)) = 0;
    *((rsp + 0x40)) = rcx;
label_3:
    if (al != 0x2f) {
        goto label_30;
    }
    do {
        edx = *((rbp + 1));
        rbp++;
    } while (dl == 0x2f);
    if (dl == 0) {
        goto label_6;
    }
    *((rsp + 0x20)) = rbp;
    do {
label_7:
        rbx = rbp;
        eax = *((rbp + 1));
        rbp++;
        if (al == 0) {
            goto label_31;
        }
    } while (al != 0x2f);
label_31:
    rcx = rbp;
    rcx -= *((rsp + 0x20));
    *((rsp + 0x18)) = rcx;
    if (rcx == 0) {
        goto label_6;
    }
    if (rcx == 1) {
        goto label_1;
    }
    if (*((rsp + 0x18)) != 2) {
        goto label_2;
    }
    if (dl != 0x2e) {
        goto label_2;
    }
    rcx = *((rsp + 0x20));
    if (*((rcx + 1)) != 0x2e) {
        goto label_2;
    }
    rdx = r15 + 1;
    if (r13 <= rdx) {
        goto label_17;
    }
    r13--;
    if (r13 <= r15) {
        goto label_17;
    }
    do {
        if (*((r13 - 1)) == 0x2f) {
            goto label_17;
        }
        r13--;
    } while (r13 != r15);
label_17:
    if (al != 0) {
        goto label_3;
    }
label_6:
    rax = r15 + 1;
    ebx = 0;
    if (r13 > rax) {
        eax = 0;
        al = (*((r13 - 1)) == 0x2f) ? 1 : 0;
        r13 -= rax;
    }
label_10:
    rdi = *((rsp + 0x28));
    if (rdi == 0) {
        goto label_4;
    }
    eax = hash_free (rdi, rsi);
    goto label_4;
label_25:
    rdi = *(r12);
    eax = 0;
    if (*((rsp + 0x10)) == rdi) {
        goto label_0;
    }
    *(rsp) = rax;
    free (rdi);
    rax = *(rsp);
    goto label_0;
label_22:
    errno_location ();
    *(rax) = 2;
    eax = 0;
    goto label_0;
label_28:
    rbx = *((rsp + 0x30));
    r12 = r13;
    r13 = rcx;
label_27:
    rdx = *((rsp + 0x18));
    rsi = *((rsp + 0x20));
    rdi = r13;
    mempcpy ();
    ecx = *((rsp + 0x3c));
    *(rax) = 0;
    r13 = rax;
    if (ecx == 0) {
        goto label_32;
    }
label_14:
    if (r14d == 2) {
        goto label_9;
    }
    eax = *(rbp);
    if (al != 0x2f) {
        goto label_33;
    }
    rdx = rbp;
    do {
label_5:
        rsi = rdx;
        ecx = *((rdx + 1));
        rdx++;
    } while (cl == 0x2f);
    rsi += 2;
    if (cl == 0) {
        goto label_34;
    }
    if (cl != 0x2e) {
        goto label_33;
    }
    ecx = *((rdx + 1));
    if (cl == 0) {
        goto label_34;
    }
    if (cl == 0x2e) {
        goto label_35;
    }
    if (cl != 0x2f) {
        goto label_33;
    }
    rdx = rsi;
    goto label_5;
label_8:
    if (al != 0) {
        goto label_3;
    }
label_13:
    edx = 0;
    ecx = 0x200;
    rsi = r15;
    edi = 0xffffff9c;
    eax = faccessat ();
    if (eax != 0) {
        goto label_36;
    }
label_9:
    eax = *((rbx + 1));
    if (al != 0) {
        goto label_3;
    }
    goto label_6;
label_30:
    *((rsp + 0x20)) = rbp;
    edx = eax;
    goto label_7;
label_35:
    edx = *((rdx + 2));
    if (dl == 0) {
        goto label_34;
    }
    if (dl == 0x2f) {
        goto label_34;
    }
label_33:
    edx = *((rsp + 0x3c));
    if (edx != 0) {
        goto label_8;
    }
    rax = errno_location ();
    if (*(rax) == 0x16) {
        goto label_9;
    }
label_36:
    if (r14d != 1) {
        goto label_15;
    }
    rax = errno_location ();
    if (*(rax) != 2) {
        goto label_15;
    }
    strspn (rbp, 0x0001371b);
    if (*((rbp + rax)) == 0) {
        goto label_9;
    }
label_15:
    ebx = 1;
    goto label_10;
label_24:
    rdi = r13;
    esi = 0;
    r15 = r13;
    rax = rawmemchr ();
    r13 = rax;
    goto label_11;
label_32:
    *((rsp + 0x18)) = rax;
    r13 = *((rsp + 0x40));
    *((rsp + 0x30)) = rbx;
    *((rsp + 0x50)) = rbp;
    do {
        rax = *((rsp + 0x508));
        rbx = *((rsp + 0x500));
        rdi = r15;
        rbp = rax - 1;
        rsi = rbx;
        rdx = rbp;
        rax = readlink ();
        if (rbp > rax) {
            goto label_37;
        }
        al = gl_scratch_buffer_grow (r13);
    } while (al != 0);
    goto label_12;
label_34:
    eax = *(obj.dir_suffix);
    *(r13) = ax;
    goto label_13;
label_37:
    r10 = rbx;
    r13 = *((rsp + 0x18));
    rbx = *((rsp + 0x30));
    r9 = rax;
    rbp = *((rsp + 0x50));
    if (rax < 0) {
        goto label_14;
    }
    if (*((rsp + 0x4c)) <= 0x13) {
        goto label_38;
    }
    rax = *((rsp + 0x20));
    if (*(rax) == 0) {
        goto label_18;
    }
    rdx = rax;
    r11 = rsp + 0x60;
    *((rsp + 0x58)) = r9;
    rdi = 0x00012fec;
    rdx -= rbp;
    rsi = r11;
    *((rsp + 0x50)) = r10;
    rdx += r13;
    *((rsp + 0x18)) = r11;
    *(rdx) = 0;
    if (*(r15) != 0) {
        rdi = r15;
    }
    *((rsp + 0x30)) = rdx;
    eax = stat ();
    if (eax != 0) {
        goto label_15;
    }
    rax = *((rsp + 0x20));
    rdx = *((rsp + 0x30));
    r11 = *((rsp + 0x18));
    eax = *(rax);
    r10 = *((rsp + 0x50));
    r9 = *((rsp + 0x58));
    *(rdx) = al;
    if (*((rsp + 0x28)) == 0) {
        goto label_39;
    }
label_20:
    rdx = r11;
    *((rsp + 0x50)) = r9;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x18)) = r11;
    al = seen_file (*((rsp + 0x28)), *((rsp + 0x20)), rdx, rcx, r8, r9);
    r11 = *((rsp + 0x18));
    r10 = *((rsp + 0x30));
    r9 = *((rsp + 0x50));
    if (al != 0) {
        goto label_40;
    }
    *((rsp + 0x30)) = r9;
    *((rsp + 0x18)) = r10;
    record_file (*((rsp + 0x28)), *((rsp + 0x20)), r11, rcx, r8, r9);
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x30));
label_18:
    *((r10 + r9)) = 0;
    r8 = *((rsp + 0xf0));
    if (*((rsp + 0x4b)) == 0) {
        goto label_41;
    }
    rax = rbp;
    *((rsp + 0x58)) = r9;
    rax -= r8;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x30)) = r8;
    *((rsp + 0x20)) = rax;
    rax = strlen (rbp);
    r9 = *((rsp + 0x58));
    r8 = *((rsp + 0x30));
    *((rsp + 0x18)) = rax;
    r10 = *((rsp + 0x50));
    rax += r9;
    if (*((rsp + 0xf8)) > rax) {
        goto label_42;
    }
label_16:
    rbx = rsp + 0xf0;
    *((rsp + 0x58)) = rbp;
    *((rsp + 0x30)) = r10;
    rbx = rax;
    *((rsp + 0x50)) = r9;
    while (al != 0) {
        r8 = *((rsp + 0xf0));
        if (*((rsp + 0xf8)) > rbx) {
            goto label_43;
        }
        al = gl_scratch_buffer_grow_preserve (rbp);
    }
    goto label_12;
label_29:
    rax = r15 + 1;
    if (r13 <= rax) {
        goto label_44;
    }
    if (*((r13 - 1)) != 0x2f) {
        goto label_44;
    }
    r13--;
    ebx = 0;
    goto label_4;
label_40:
    if (r14d == 2) {
        goto label_9;
    }
    errno_location ();
    *(rax) = 0x28;
    goto label_15;
label_44:
    ebx = 0;
    goto label_4;
label_41:
    *((rsp + 0x20)) = r9;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x30)) = r8;
    rax = strlen (rbp);
    r9 = *((rsp + 0x20));
    r8 = *((rsp + 0x30));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x18)) = rax;
    r10 = *((rsp + 0x50));
    rax += r9;
    if (rax >= *((rsp + 0xf8))) {
        goto label_16;
    }
label_19:
    *((rsp + 0x30)) = r10;
    *((rsp + 0x18)) = r9;
    rdx++;
    *((rsp + 0x20)) = r8;
    memmove (r8 + r9, rbp, *((rsp + 0x18)));
    r10 = *((rsp + 0x30));
    rsi = r10;
    *((rsp + 0x18)) = r10;
    rax = memcpy (*((rsp + 0x20)), rsi, *((rsp + 0x18)));
    r10 = *((rsp + 0x18));
    rdx = r15 + 1;
    if (*(r10) == 0x2f) {
        goto label_45;
    }
    *((rsp + 0x4b)) = 1;
    eax = *(rax);
    if (r13 <= rdx) {
        goto label_17;
    }
    do {
        r13--;
        if (r13 == r15) {
            goto label_46;
        }
    } while (*((r13 - 1)) != 0x2f);
label_46:
    *((rsp + 0x4b)) = 1;
    goto label_17;
label_38:
    goto label_18;
label_43:
    r10 = *((rsp + 0x30));
    r9 = *((rsp + 0x50));
    rbp = *((rsp + 0x58));
    if (*((rsp + 0x4b)) == 0) {
        goto label_19;
    }
label_42:
    rbp = *((rsp + 0x20));
    rbp += r8;
    goto label_19;
label_39:
    *((rsp + 0x50)) = r11;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x18)) = r10;
    rax = hash_initialize (7, 0, dbg.triple_hash, dbg.triple_compare_ino_str, dbg.triple_free);
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x30));
    *((rsp + 0x28)) = rax;
    r11 = *((rsp + 0x50));
    if (rax != 0) {
        goto label_20;
    }
    goto label_12;
label_45:
    *(r15) = 0x2f;
    r13 = rdx;
    eax = *(rax);
    *((rsp + 0x4b)) = 1;
    goto label_17;
label_26:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xa150 */
 
uint64_t dbg_unescape_tab (int64_t arg1) {
    rdi = arg1;
    /* void unescape_tab(char * str); */
    rbx = rdi;
    rax = strlen (rdi);
    rcx = rbx;
    edx = 0;
    rax++;
    while (r8b != 0x5c) {
label_0:
        *(rcx) = r8b;
        rcx++;
        if (rax <= rsi) {
            goto label_2;
        }
label_1:
        rdx = rsi;
        r8d = *((rbx + rdx));
        rsi = rdx + 1;
    }
    r9 = rdx + 4;
    if (r9 >= rax) {
        goto label_0;
    }
    edi = *((rbx + rsi));
    edi -= 0x30;
    if (dil > 3) {
        goto label_0;
    }
    r11d = *((rbx + rdx + 2));
    r10d = r11 - 0x30;
    if (r10b > 7) {
        goto label_0;
    }
    r11d = *((rbx + rdx + 3));
    r11d -= 0x30;
    if (r11b > 7) {
        goto label_0;
    }
    edx = r10 + rdi*8;
    rcx++;
    rsi = r9;
    edx = r11 + rdx*8;
    *((rcx - 1)) = dl;
    goto label_1;
label_2:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xe590 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x6250)() ();
}

/* /tmp/tmp6wcn29hf @ 0x3730 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmp6wcn29hf @ 0x3ad0 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp6wcn29hf @ 0x3ab0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp6wcn29hf @ 0x3a00 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp6wcn29hf @ 0x3880 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmp6wcn29hf @ 0x39f0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmp6wcn29hf @ 0x3640 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmp6wcn29hf @ 0xea90 */
 
void cdb_free_part_0 (void) {
    return assert_fail ("! close_fail", "lib/chdir-long.c", 0x40, "cdb_free");
}

/* /tmp/tmp6wcn29hf @ 0x3800 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmp6wcn29hf @ 0x114a0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmp6wcn29hf @ 0xc790 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0xea60 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmp6wcn29hf @ 0xcac0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x3780 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmp6wcn29hf @ 0xd7a0 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xc5d0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x3630 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp6wcn29hf @ 0xd9a0 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x3950 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp6wcn29hf @ 0xdee0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x00013bba);
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x3a30 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmp6wcn29hf @ 0x39e0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmp6wcn29hf @ 0xc4f0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x99b0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xc7e0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x3b94)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x9e50 */
 
int64_t dbg_ambsalign (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * ambsalign(char const * src,size_t * width,mbs_align_t align,int flags); */
    r14 = rdi;
    r13d = edx;
    r12d = 0;
    rbx = rsi;
    rax = *(rsi);
    *((rsp + 0xc)) = ecx;
    *(rsp) = rax;
    do {
        rbp = rax + 1;
        rdi = r12;
        r15 = r12;
        rax = realloc (rdi, rbp);
        r12 = rax;
        if (rax == 0) {
            goto label_1;
        }
        rax = *(rsp);
        *(rbx) = rax;
        rax = mbsalign (r14, r12, rbp, rbx, r13d, *((rsp + 0xc)));
        if (rax == -1) {
            goto label_2;
        }
    } while (rbp <= rax);
    do {
label_0:
        rax = r12;
        return rax;
label_2:
        r12d = 0;
        free (r12);
    } while (1);
label_1:
    free (r15);
    goto label_0;
}

/* /tmp/tmp6wcn29hf @ 0xc550 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xeac0 */
 
uint64_t dbg_chdir_long (void * s, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* int chdir_long(char * dir); */
    eax = chdir ();
    r12d = eax;
    if (eax == 0) {
        goto label_2;
    }
    rax = errno_location ();
    *((rsp + 8)) = rax;
    if (*(rax) != 0x24) {
        goto label_2;
    }
    rax = strlen (rbp);
    rbx = rax;
    if (rax == 0) {
        goto label_9;
    }
    if (rax <= 0xfff) {
        goto label_10;
    }
    r12 = 0x0001371b;
    rax = strspn (rbp, r12);
    r15 = rax;
    if (rax == 2) {
        goto label_11;
    }
    r14 = rbp;
    r13d = 0xffffff9c;
    if (rax != 0) {
        goto label_12;
    }
label_1:
    if (*(r14) == 0x2f) {
        goto label_13;
    }
    rbp += rbx;
    if (r14 > rbp) {
        goto label_14;
    }
    rax = rbp;
    rax -= r14;
    if (rax > 0xfff) {
        goto label_15;
    }
    goto label_16;
    do {
label_0:
        *(rbx) = 0x2f;
        rbx++;
        strspn (rbx, r12);
        r14 = rbx + rax;
        rax = rbp;
        rax -= r14;
        if (rax <= 0xfff) {
            goto label_17;
        }
        r13d = r15d;
label_15:
        edx = 0x1000;
        esi = 0x2f;
        rdi = r14;
        rax = memrchr ();
        rbx = rax;
        if (rax == 0) {
            goto label_18;
        }
        *(rax) = 0;
        rax -= r14;
        if (rax > 0xfff) {
            goto label_19;
        }
        eax = 0;
        eax = openat (r13d, r14, 0x10900);
        r15d = eax;
        if (eax < 0) {
            goto label_20;
        }
    } while (r13d < 0);
    eax = close (r13d);
    if (eax == 0) {
        goto label_0;
    }
    do {
label_7:
        cdb_free_part_0 ();
label_20:
        *(rbx) = 0x2f;
label_3:
        rax = *((rsp + 8));
        ebx = *(rax);
        if (r13d >= 0) {
            goto label_21;
        }
label_6:
        rax = *((rsp + 8));
        r12d = 0xffffffff;
        *(rax) = ebx;
label_2:
        eax = r12d;
        return rax;
label_12:
        eax = 0;
        eax = openat (0xffffff9c, r12, 0x10900);
        r13d = eax;
        if (eax < 0) {
            goto label_22;
        }
        r14 = rbp + r15;
        goto label_1;
label_11:
        rax = memchr (rbp + 3, 0x2f, rbx - 3);
        r14 = rax;
        if (rax == 0) {
            goto label_23;
        }
        *(rax) = 0;
        eax = 0;
        eax = openat (0xffffff9c, rbp, 0x10900);
        *(r14) = 0x2f;
        r13d = eax;
        if (eax < 0) {
            goto label_22;
        }
        r14++;
        rax = strspn (r14, r12);
        r14 += rax;
        goto label_1;
label_16:
        r15d = r13d;
label_17:
        if (rbp <= r14) {
            goto label_24;
        }
        eax = 0;
        eax = openat (r15d, r14, 0x10900);
        r13d = eax;
        if (eax < 0) {
            goto label_25;
        }
        if (r15d >= 0) {
            goto label_26;
        }
label_8:
        edi = r13d;
        eax = fchdir ();
        if (eax != 0) {
            goto label_27;
        }
label_5:
        eax = close (r13d);
    } while (eax != 0);
label_4:
    r12d = 0;
    goto label_2;
label_18:
    rax = *((rsp + 8));
    r12d = 0xffffffff;
    *(rax) = 0x24;
    goto label_2;
label_24:
    edi = r15d;
    r13d = r15d;
    eax = fchdir ();
    if (eax != 0) {
        goto label_3;
    }
    if (r15d < 0) {
        goto label_4;
    }
    goto label_5;
label_27:
    rax = *((rsp + 8));
    ebx = *(rax);
label_21:
    eax = close (r13d);
    if (eax == 0) {
        goto label_6;
    }
    goto label_7;
label_23:
    r12d = 0xffffffff;
    goto label_2;
label_26:
    eax = close (r15d);
    if (eax == 0) {
        goto label_8;
    }
    goto label_7;
label_22:
    rax = *((rsp + 8));
    ebx = *(rax);
    goto label_6;
label_19:
    assert_fail ("slash - dir < 4096", "lib/chdir-long.c", 0xb3, "chdir_long");
label_25:
    r13d = r15d;
    goto label_3;
label_9:
    assert_fail ("0 < len", "lib/chdir-long.c", 0x7e, "chdir_long");
label_10:
    assert_fail ("4096 <= len", "lib/chdir-long.c", 0x7f, "chdir_long");
label_13:
    assert_fail ("*dir != '/', "lib/chdir-long.c", 0xa2, "chdir_long");
label_14:
    return assert_fail ("dir <= dir_end", "lib/chdir-long.c", 0xa3, "chdir_long");
}

/* /tmp/tmp6wcn29hf @ 0x7e80 */
 
int64_t dbg_hash_print_statistics (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void hash_print_statistics(Hash_table const * table,FILE * stream); */
    r12d = 0;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8 = *((rdi + 0x20));
    rbx = *((rdi + 0x10));
    r13 = *((rdi + 0x18));
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_2;
    do {
        rcx += 0x10;
        if (rsi <= rcx) {
            goto label_2;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_3;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_3:
    if (r12 < rdx) {
        r12 = rdx;
    }
    rcx += 0x10;
    if (rsi > rcx) {
        goto label_0;
    }
label_2:
    rcx = r8;
    rdx = "# entries:         %lu\n";
    rdi = rbp;
    eax = 0;
    esi = 1;
    eax = fprintf_chk ();
    eax = 0;
    rcx = rbx;
    esi = 1;
    rdx = "# buckets:         %lu\n";
    rdi = rbp;
    fprintf_chk ();
    if (r13 < 0) {
        goto label_4;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, r13");
    __asm ("mulsd xmm0, qword [0x00012fa0]");
    if (rbx < 0) {
        goto label_5;
    }
    do {
        xmm1 = 0;
        __asm ("cvtsi2sd xmm1, rbx");
label_1:
        __asm ("divsd xmm0, xmm1");
        rcx = r13;
        rdi = rbp;
        esi = 1;
        rdx = "# buckets used:    %lu (%.2f%%)\n";
        eax = 1;
        eax = fprintf_chk ();
        rcx = r12;
        rdi = rbp;
        rdx = "max bucket length: %lu\n";
        esi = 1;
        eax = 0;
        void (*0x3ad0)() ();
label_4:
        rax = r13;
        rdx = r13;
        xmm0 = 0;
        rax >>= 1;
        edx &= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm0, rax");
        __asm ("addsd xmm0, xmm0");
        __asm ("mulsd xmm0, qword [0x00012fa0]");
    } while (rbx >= 0);
label_5:
    rax = rbx;
    ebx &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rbx;
    __asm ("cvtsi2sd xmm1, rax");
    __asm ("addsd xmm1, xmm1");
    goto label_1;
}

/* /tmp/tmp6wcn29hf @ 0xac90 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmp6wcn29hf @ 0x37d0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmp6wcn29hf @ 0x3ac0 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp6wcn29hf @ 0xe710 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x3a30)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmp6wcn29hf @ 0xcfe0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0xcd20 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x3bad)() ();
    }
    if (rdx == 0) {
        void (*0x3bad)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x88d0 */
 
int64_t dbg_hash_insert (int64_t arg2) {
     const * matched_ent;
    int64_t var_8h;
    rsi = arg2;
    /* void * hash_insert(Hash_table * table, const * entry); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    eax = hash_insert_if_absent ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    rax = rbx;
    rax = *(rsp);
    while (1) {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        eax = 0;
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xd080 */
 
void dbg_restore_cwd (int64_t arg_3h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* int restore_cwd(saved_cwd const * cwd); */
    r8d = *(rdi);
    if (r8d >= 0) {
        edi = r8d;
        void (*0x39d0)() ();
    }
    rdi = *((rdi + 8));
    return void (*0xeac0)() ();
}

/* /tmp/tmp6wcn29hf @ 0xcdc0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3bb2)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x3bb2)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xc900 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x3b9e)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x7620 */
 
uint64_t dbg_mdir_name (uint32_t arg1) {
    rdi = arg1;
    /* char * mdir_name(char const * file); */
    ebx = 0;
    bl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbp;
    r12 = rax;
    while (rbx < r12) {
        rax = r12 - 1;
        if (*((rbp + r12 - 1)) != 0x2f) {
            goto label_2;
        }
        r12 = rax;
    }
    rax = r12;
    rax ^= 1;
    ebx = eax;
    ebx &= 1;
    rax = malloc (r12 + rax + 1);
    if (rax == 0) {
        goto label_3;
    }
    rax = memcpy (rax, rbp, r12);
    r8 = rax;
    if (bl == 0) {
        goto label_4;
    }
    *(rax) = 0x2e;
    r12d = 1;
    do {
label_0:
        *((r8 + r12)) = 0;
label_1:
        rax = r8;
        return rax;
label_2:
        rax = malloc (r12 + 1);
        r8 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = memcpy (r8, rbp, r12);
        r8 = rax;
    } while (1);
label_4:
    r12d = 1;
    goto label_0;
label_3:
    r8d = 0;
    goto label_1;
}

/* /tmp/tmp6wcn29hf @ 0x8b40 */
 
void dbg_triple_free (void ** ptr) {
    rdi = ptr;
    /* void triple_free(void * x); */
    free (*(rdi));
    rdi = rbp;
    return free ();
}

/* /tmp/tmp6wcn29hf @ 0x35f0 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmp6wcn29hf @ 0xcc80 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, uint32_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x3ba8)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x114b4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp6wcn29hf @ 0x8430 */
 
int64_t dbg_hash_free (int64_t arg_8h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_free(Hash_table * table); */
    r12 = rdi;
    r13 = *(rdi);
    rax = *((rdi + 8));
    if (*((rdi + 0x40)) == 0) {
        goto label_2;
    }
    if (*((rdi + 0x20)) == 0) {
        goto label_2;
    }
    if (r13 < rax) {
        goto label_0;
    }
    goto label_3;
    do {
        r13 += 0x10;
        if (rax <= r13) {
            goto label_4;
        }
label_0:
        rdi = *(r13);
    } while (rdi == 0);
    rbx = r13;
    while (rbx != 0) {
        rdi = *(rbx);
        uint64_t (*r12 + 0x40)() ();
        rbx = *((rbx + 8));
    }
    rax = *((r12 + 8));
    r13 += 0x10;
    if (rax > r13) {
        goto label_0;
    }
label_4:
    rbp = *(r12);
label_2:
    if (rax <= rbp) {
        goto label_3;
    }
label_1:
    rbx = *((rbp + 8));
    if (rbx == 0) {
        goto label_5;
    }
    do {
        rbx = *((rbx + 8));
        free (rbx);
    } while (rbx != 0);
label_5:
    rbp += 0x10;
    if (*((r12 + 8)) > rbp) {
        goto label_1;
    }
label_3:
    rbx = *((r12 + 0x48));
    if (rbx == 0) {
        goto label_6;
    }
    do {
        rbx = *((rbx + 8));
        free (rbx);
    } while (rbx != 0);
label_6:
    free (*(r12));
    rdi = r12;
    return free ();
}

/* /tmp/tmp6wcn29hf @ 0xd020 */
 
uint64_t dbg_save_cwd (int64_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    /* int save_cwd(saved_cwd * cwd); */
    rbx = rdi;
    eax = 0;
    *((rdi + 8)) = 0;
    eax = open_safer (0x00012fec, 0x80000, rdx, rcx);
    r8d = 0;
    *(rbx) = eax;
    if (eax >= 0) {
        eax = r8d;
        return eax;
    }
    esi = 0;
    edi = 0;
    rax = getcwd ();
    *((rbx + 8)) = rax;
    r8d -= r8d;
    eax = r8d;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xef60 */
 
int64_t dbg_open_safer (int64_t arg_60h, int64_t arg3, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    rdx = arg3;
    rsi = oflag;
    rdi = path;
    /* int open_safer(char const * file,int flags,va_args ...); */
    *((rsp + 0x30)) = rdx;
    edx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = open (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x10;
        edx = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x3820 */
 
void getcwd (void) {
    __asm ("bnd jmp qword [reloc.getcwd]");
}

/* /tmp/tmp6wcn29hf @ 0xdf50 */
 
int64_t xstrtol_fatal (uint32_t arg1, char * arg2, int64_t arg4, int64_t arg5) {
    int64_t var_6h;
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    r8 = arg5;
    rbx = r8;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    ebp = exit_failure;
    if (edi <= 3) {
        if (edi > 1) {
            goto label_3;
        }
        if (edi != 1) {
            goto label_4;
        }
        r9 = "%s%s argument '%s' too large";
label_2:
        rax = (int64_t) esi;
        if (esi < 0) {
            goto label_5;
        }
label_0:
        rax <<= 5;
        r12 = 0x00013a18;
        r13 = *((rcx + rax));
label_1:
        edx = 5;
        rax = dcgettext (0, r9);
        r9 = rbx;
        r8 = r13;
        rcx = r12;
        eax = 0;
        error (ebp, 0, rax);
        abort ();
    }
    if (edi != 4) {
        void (*0x3bc1)() ();
    }
    r9 = "invalid %s%s argument '%s';
    rax = (int64_t) esi;
    if (esi >= 0) {
        goto label_0;
    }
label_5:
    r12 = 0x00013a18;
    *((rsp + 6)) = dl;
    r13 = rsp + 6;
    *((rsp + 7)) = 0;
    r12 -= rax;
    goto label_1;
label_3:
    r9 = "invalid suffix in %s%s argument '%s';
    goto label_2;
label_4:
    return xstrtol_fatal_cold ();
}

/* /tmp/tmp6wcn29hf @ 0xdb20 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x76e0 */
 
uint64_t dbg_record_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg1, uint32_t arg2, int64_t arg3) {
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void record_file(Hash_table * ht,char const * file,stat const * stats); */
    if (rdi != 0) {
        r13 = rsi;
        r12 = rdi;
        rbx = rdx;
        rax = xmalloc (0x18);
        rax = xstrdup (r13);
        rsi = rbp;
        *(rbp) = rax;
        rax = *((rbx + 8));
        *((rbp + 8)) = rax;
        rax = *(rbx);
        *((rbp + 0x10)) = rax;
        rax = hash_insert (r12);
        if (rax == 0) {
            goto label_0;
        }
        if (rbp != rax) {
            rdi = rbp;
            void (*0x8b40)() ();
        }
        return rax;
    }
    return rax;
label_0:
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x77d0 */
 
int64_t dbg_get_fs_usage (int64_t arg1, int64_t arg3) {
    utsname name;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_48h;
    int64_t var_82h;
    int64_t var_188h;
    rdi = arg1;
    rdx = arg3;
    /* int get_fs_usage(char const * file,char const * disk,fs_usage * fsp); */
    rbx = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x188)) = rax;
    eax = 0;
    eax = statvfs_works_cache.0;
    r12 = rsp;
    if (eax >= 0) {
        if (eax == 0) {
            goto label_2;
        }
label_0:
        rsi = r12;
        rdi = rbp;
        eax = statvfs ();
        r8d = eax;
        eax = 0xffffffff;
        if (r8d < 0) {
            goto label_1;
        }
        rax = *((rsp + 8));
        if (rax == 0) {
            rax = *(rsp);
        }
        goto label_3;
    }
    rdi = r12;
    eax = uname ();
    while (eax < 0) {
        *(obj.statvfs_works_cache.0) = 0;
label_2:
        rsi = r12;
        rdi = rbp;
        eax = statfs ();
        if (eax < 0) {
            goto label_4;
        }
        rax = *((rsp + 0x48));
label_3:
        *(rbx) = rax;
        rax = *((rsp + 0x10));
        *((rbx + 8)) = rax;
        rax = *((rsp + 0x18));
        *((rbx + 0x10)) = rax;
        rax = *((rsp + 0x20));
        *((rbx + 0x18)) = rax;
        rax >>= 0x3f;
        *((rbx + 0x20)) = al;
        rax = *((rsp + 0x28));
        *((rbx + 0x28)) = rax;
        rax = *((rsp + 0x30));
        *((rbx + 0x30)) = rax;
        eax = 0;
label_1:
        rdx = *((rsp + 0x188));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_5;
        }
        return rax;
        rdi = rsp + 0x82;
        rsi = "2.6.36";
        eax = strverscmp ();
    }
    *(obj.statvfs_works_cache.0) = 1;
    goto label_0;
label_4:
    eax = 0xffffffff;
    goto label_1;
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xd9c0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x75c0 */
 
uint64_t dbg_dir_name (void) {
    /* char * dir_name(char const * file); */
    rax = mdir_name (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xea00 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xefe0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x3960)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmp6wcn29hf @ 0x8210 */
 
int64_t dbg_hash_string (int64_t arg1, size_t n_buckets) {
    rdi = arg1;
    rsi = n_buckets;
    /* size_t hash_string(char const * string,size_t n_buckets); */
    ecx = *(rdi);
    edx = 0;
    if (cl == 0) {
        goto label_0;
    }
    do {
        rax = rdx;
        rdi++;
        rax <<= 5;
        rax -= rdx;
        edx = 0;
        rax += rcx;
        ecx = *(rdi);
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
    } while (cl != 0);
label_0:
    rax = rdx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xd960 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xc7c0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0xd0b0 */
 
void dbg_free_cwd (int64_t arg1) {
    rdi = arg1;
    /* void free_cwd(saved_cwd * cwd); */
    rbx = rdi;
    edi = *(rdi);
    if (edi >= 0) {
        close (rdi);
    }
    rdi = *((rbx + 8));
    return free ();
}

/* /tmp/tmp6wcn29hf @ 0x3840 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmp6wcn29hf @ 0xdea0 */
 
uint64_t dbg_xstrdup (uint32_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3900)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x3750 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmp6wcn29hf @ 0xd980 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xe5a0 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmp6wcn29hf @ 0xd6e0 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0xd270)() ();
}

/* /tmp/tmp6wcn29hf @ 0x7510 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x00013bba);
    } while (1);
}

/* /tmp/tmp6wcn29hf @ 0x9950 */
 
int64_t dbg_umaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * umaxtostr(uintmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    rcx = rdi;
    r8 = rsi + 0x14;
    rdi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rdi;
        rax = rcx;
        rdx >>= 3;
        rsi = rdx * 5;
        rsi += rsi;
        rax -= rsi;
        eax += 0x30;
        *(r8) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    rax = r8;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xf1b0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmp6wcn29hf @ 0xd920 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x7e10 */
 
int64_t dbg_hash_table_ok (Hash_table const * table) {
    rdi = table;
    /* _Bool hash_table_ok(Hash_table const * table); */
    rcx = *(rdi);
    rsi = *((rdi + 8));
    edx = 0;
    r8d = 0;
    if (rcx < rsi) {
        goto label_1;
    }
    goto label_2;
    do {
label_0:
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_2;
        }
label_1:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    r8++;
    rdx++;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_1;
    }
label_2:
    eax = 0;
    if (*((rdi + 0x18)) != r8) {
        return rax;
    }
    al = (*((rdi + 0x20)) == rdx) ? 1 : 0;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x6840 */
 
int64_t dbg_find_mount_point (int64_t arg1, int64_t arg2) {
    int64_t var_1h;
    saved_cwd cwd;
    stat last_stat;
    int64_t var_110h;
    int64_t var_100h;
    int64_t var_f0h;
    int64_t var_e0h;
    stat st;
    int64_t var_80h;
    int64_t var_70h;
    int64_t var_bp_60h;
    int64_t var_bp_50h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_sp_28h;
    int64_t var_30h;
    int64_t var_3ch;
    int64_t var_40h;
    int64_t var_4bh;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_sp_f0h;
    int64_t var_f8h;
    int64_t var_sp_100h;
    int64_t var_500h;
    int64_t var_508h;
    int64_t var_510h;
    int64_t var_ff8h;
    void * s1;
    rdi = arg1;
    rsi = arg2;
    /* char * find_mount_point(char const * file,stat const * file_stat); */
    r14 = cwd_desc;
    r12 = rdi;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = save_cwd (r14, rsi);
    r15d = eax;
    rax = errno_location ();
    r13 = rax;
    if (r15d != 0) {
        goto label_1;
    }
    eax = *((rbx + 0x18));
    eax &= 0xf000;
    if (eax == 0x4000) {
        goto label_2;
    }
    rdi = r12;
    rax = dir_name ();
    rdi = rax;
    r12 = rax;
    rax = strlen (rdi);
    rcx = rsp;
    r8 = rax + 1;
    rax += 0x18;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_3;
    }
    do {
    } while (rsp != rcx);
label_3:
    edx &= 0xfff;
    if (rdx != 0) {
    }
    rdi &= 0xfffffffffffffff0;
    rax = memcpy (rsp + 0xf, r12, r8);
    r15 = rax;
    free (r12);
    rdi = r15;
    eax = chdir ();
    if (eax < 0) {
        goto label_4;
    }
    rsi = last_stat_st_dev;
    rdi = 0x00012fec;
    eax = stat ();
    if (eax < 0) {
        rsi = r15;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "cannot stat current directory (now %s)");
        rcx = r12;
        eax = 0;
        r12d = 0;
        error (0, *(r13), rax);
        goto label_5;
label_2:
        __asm ("movdqu xmm1, xmmword [rbx]");
        __asm ("movdqu xmm2, xmmword [rbx + 0x10]");
        rdi = r12;
        __asm ("movdqu xmm3, xmmword [rbx + 0x20]");
        __asm ("movdqu xmm4, xmmword [rbx + 0x30]");
        __asm ("movdqu xmm5, xmmword [rbx + 0x40]");
        __asm ("movdqu xmm6, xmmword [rbx + 0x50]");
        *(last_stat.st_dev) = xmm1;
        __asm ("movdqu xmm7, xmmword [rbx + 0x60]");
        __asm ("movdqu xmm1, xmmword [rbx + 0x70]");
        *(last_stat.st_nlink) = xmm2;
        __asm ("movdqu xmm2, xmmword [rbx + 0x80]");
        *(last_stat.st_gid) = xmm3;
        *(last_stat.st_size) = xmm4;
        *(last_stat.st_blocks) = xmm5;
        *((rbp - 0x110)) = xmm6;
        *((rbp - 0x100)) = xmm7;
        *((rbp - 0xf0)) = xmm1;
        *((rbp - 0xe0)) = xmm2;
        eax = chdir ();
        if (eax < 0) {
            goto label_6;
        }
    }
    rbx = st_st_dev;
    r12 = 0x00012feb;
    while (eax >= 0) {
        rax = *(last_stat.st_dev);
        if (*(st.st_dev) != rax) {
            goto label_7;
        }
        rax = *(last_stat.st_ino);
        if (*(st.st_ino) == rax) {
            goto label_7;
        }
        rdi = r12;
        eax = chdir ();
        if (eax < 0) {
            goto label_8;
        }
        __asm ("movdqa xmm0, xmmword [st.st_dev]");
        __asm ("movdqa xmm1, xmmword [st.st_nlink]");
        __asm ("movdqa xmm2, xmmword [st.st_gid]");
        __asm ("movdqa xmm3, xmmword [st.st_size]");
        __asm ("movdqa xmm4, xmmword [st.st_blocks]");
        __asm ("movdqa xmm5, xmmword [rbp - 0x80]");
        *(last_stat.st_dev) = xmm0;
        __asm ("movdqa xmm6, xmmword [rbp - 0x70]");
        __asm ("movdqa xmm7, xmmword [rbp - 0x60]");
        *(last_stat.st_nlink) = xmm1;
        __asm ("movdqa xmm0, xmmword [rbp - 0x50]");
        *(last_stat.st_gid) = xmm2;
        *(last_stat.st_size) = xmm3;
        *(last_stat.st_blocks) = xmm4;
        *((rbp - 0x110)) = xmm5;
        *((rbp - 0x100)) = xmm6;
        *((rbp - 0xf0)) = xmm7;
        *((rbp - 0xe0)) = xmm0;
        rsi = rbx;
        rdi = r12;
        eax = stat ();
    }
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    do {
        rax = dcgettext (0, "cannot stat %s");
        rcx = r12;
        eax = 0;
        r12d = 0;
        error (0, *(r13), rax);
        goto label_5;
label_7:
        rax = xgetcwd ();
        r12 = rax;
label_5:
        ebx = *(r13);
        eax = restore_cwd (r14, rsi);
        if (eax != 0) {
            goto label_9;
        }
        free_cwd (r14);
        *(r13) = ebx;
label_0:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_10;
        }
        rsp = rbp - 0x28;
        rax = r12;
        return rax;
label_8:
        rsi = r12;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        rsi = "cannot change to directory %s";
        r12 = rax;
    } while (1);
label_1:
    edx = 5;
    r12d = 0;
    rax = dcgettext (0, 0x00012fb0);
    eax = 0;
    error (0, *(r13), rax);
    goto label_0;
label_4:
    rsi = r15;
    do {
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "cannot change to directory %s");
        rcx = r12;
        eax = 0;
        r12d = 0;
        error (0, *(r13), rax);
        goto label_0;
label_6:
        rsi = r12;
    } while (1);
label_10:
    stack_chk_fail ();
label_9:
    edx = 5;
    rax = dcgettext (0, "failed to return to initial working directory");
    eax = 0;
    error (1, *(r13), rax);
}

/* /tmp/tmp6wcn29hf @ 0xf0b0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xe8d0 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xce70 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3bb7)() ();
    }
    if (rax == 0) {
        void (*0x3bb7)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x86b0 */
 
int64_t hash_insert_if_absent (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rsi == 0) {
        void (*0x3b7f)() ();
    }
    r12 = rsp;
    r13 = rdx;
    ecx = 0;
    rbx = rdi;
    rdx = r12;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_8;
    }
    r8d = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r13) = rax;
    do {
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        eax = r8d;
        return rax;
label_8:
        rax = *((rbx + 0x18));
        if (rax < 0) {
            goto label_10;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_11;
        }
label_0:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_1:
        rax = *((rbx + 0x28));
        xmm0 = *((rax + 8));
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm5, xmm0");
        if (rax > 0) {
            goto label_12;
        }
label_2:
        r12 = *(rsp);
        if (*(r12) == 0) {
            goto label_13;
        }
        rax = *((rbx + 0x48));
        if (rax == 0) {
            goto label_14;
        }
        rdx = *((rax + 8));
        *((rbx + 0x48)) = rdx;
label_4:
        rdx = *((r12 + 8));
        *(rax) = rbp;
        r8d = 1;
        *((rax + 8)) = rdx;
        *((r12 + 8)) = rax;
        *((rbx + 0x20))++;
    } while (1);
label_10:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_0;
    }
label_11:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_1;
label_12:
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm1 = xmm4;
    xmm0 = *((rax + 8));
    __asm ("mulss xmm1, xmm0");
    __asm ("comiss xmm5, xmm1");
    if (rdx <= 0) {
        goto label_2;
    }
    __asm ("mulss xmm4, dword [rax + 0xc]");
    if (*((rax + 0x10)) == 0) {
        goto label_15;
    }
label_5:
    __asm ("comiss xmm4, dword [0x00013114]");
    if (*((rax + 0x10)) < 0) {
        goto label_16;
    }
    do {
label_6:
        r8d = 0xffffffff;
        goto label_3;
label_13:
        *(r12) = rbp;
        r8d = 1;
        *((rbx + 0x20))++;
        *((rbx + 0x18))++;
        goto label_3;
label_14:
        rax = malloc (0x10);
    } while (rax == 0);
    goto label_4;
label_15:
    __asm ("mulss xmm4, xmm0");
    goto label_5;
label_16:
    __asm ("comiss xmm4, dword [0x00013118]");
    if (rax >= 0) {
        goto label_17;
    }
    __asm ("cvttss2si rsi, xmm4");
label_7:
    rdi = rbx;
    al = hash_rehash ();
    if (al == 0) {
        goto label_6;
    }
    ecx = 0;
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_2;
    }
    void (*0x3b7f)() ();
label_17:
    __asm ("subss xmm4, dword [0x00013118]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_7;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x7d90 */
 
int64_t hash_get_n_buckets_used (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x18));
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xcbf0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xdd70 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x38a0 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmp6wcn29hf @ 0xc490 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x8b60 */
 
void dbg_human_readable (int64_t arg_48h, int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_1h;
    char[41] buf;
    int64_t var_10h_2;
    void ** s1;
    uint32_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    void ** var_30h;
    int64_t var_34h;
    int64_t var_38h;
    int64_t var_40h;
    uint32_t var_48h;
    int64_t var_4ch;
    int64_t var_4eh;
    int64_t var_50h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* char * human_readable(uintmax_t n,char * buf,int opts,uintmax_t from_block_size,uintmax_t to_block_size); */
    r13 = r8;
    r12 = rcx;
    rbx = rdi;
    *(rsp) = rsi;
    *((rsp + 0x18)) = r8;
    *((rsp + 0x48)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = edx;
    edx &= 0x20;
    *((rsp + 0x30)) = edx;
    eax &= 3;
    *((rsp + 0x10)) = eax;
    eax -= eax;
    eax &= 0xffffffe8;
    eax += 0x400;
    *((rsp + 0x34)) = eax;
    rax = localeconv ();
    r15 = *(rax);
    r14 = rax;
    rax = strlen (r15);
    rcx = *((r14 + 0x10));
    r14 = *((r14 + 8));
    rax--;
    eax = 1;
    rdi = r14;
    *((rsp + 0x38)) = rcx;
    if (rax >= 0x10) {
    }
    rax = 0x00012fec;
    if (rax >= 0x10) {
        r15 = rax;
    }
    rax = strlen (rdi);
    rax = 0x000136c1;
    if (rax > 0x10) {
        r14 = rax;
    }
    rax = *(rsp);
    rax += 0x287;
    *((rsp + 8)) = rax;
    if (r13 > r12) {
        goto label_36;
    }
    rax = r12;
    edx = 0;
    rax = rdx:rax / r13;
    rdx = rdx:rax % r13;
    if (rdx == 0) {
        rdx:rax = rax * rbx;
        rcx = rax;
        if (rdx !overflow 0) {
            goto label_37;
        }
    }
label_0:
    *((rsp + 0x20)) = r12;
    *(fp_stack--) = *((rsp + 0x20));
    if (r12 < 0) {
        goto label_38;
    }
label_5:
    rax = *((rsp + 0x18));
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        goto label_39;
    }
    *((rsp + 0x20)) = rbx;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = *((rsp + 0x20));
    if (rbx < 0) {
        goto label_4;
    }
label_3:
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    if ((*((rsp + 0x48)) & 0x10) == 0) {
        goto label_40;
    }
label_2:
    *(fp_stack--) = *((rsp + 0x34));
    ebx = 0;
    *(fp_stack--) = fp_stack[0];
    while (ebx != 8) {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        fp_tmp_0 = fp_stack[2];
        fp_stack[2] = fp_stack[0];
        fp_stack[0] = fp_tmp_0;
        *(fp_stack--) = fp_stack[0];
        ebx++;
        fp_stack[0] *= fp_stack[2];
        fp_tmp_1 = fp_stack[3];
        fp_stack[3] = fp_stack[0];
        fp_stack[0] = fp_tmp_1;
        if (fp_stack[0] < fp_stack[3]) {
            goto label_41;
        }
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    goto label_42;
label_41:
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_stack[2] = fp_stack[0];
    fp_stack--;
label_42:
    r15 = rbp + 1;
    fp_stack[1] /= fp_stack[0];
    fp_stack++;
    rbp += 2;
    if (*((rsp + 0x10)) == 1) {
        goto label_43;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (*((rsp + 0x10)) <= 1) {
        goto label_44;
    }
    *(fp_stack--) = *(0x00013118);
    fp_tmp_2 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_2;
    if (fp_stack[0] >= fp_stack[1]) {
        goto label_45;
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
label_21:
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        fp_stack[0] += *(0x00013114);
    }
    ecx = *((rsp + 0x10));
    if (ecx != 0) {
        goto label_46;
    }
    fp_tmp_3 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_3;
    if (fp_stack[0] != fp_stack[1]) {
        if (fp_stack[0] == fp_stack[1]) {
            goto label_47;
        }
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    } else {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
    rax++;
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax >= 0) {
        goto label_48;
    }
    fp_stack[0] += *(0x00013114);
    fp_tmp_4 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_4;
    goto label_47;
label_36:
    if (r12 == 0) {
        goto label_0;
    }
    rax = *((rsp + 0x18));
    edx = 0;
    rax = rdx:rax / r12;
    rdx = rdx:rax % r12;
    r8 = rax;
    if (rdx != 0) {
        goto label_0;
    }
    rax = rbx;
    edx = 0;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    rcx = rax;
    rax = rdx * 5;
    edx = 0;
    rax += rax;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    rdx += rdx;
    edi = eax;
    if (r8 <= rdx) {
        goto label_49;
    }
    esi = 0;
    sil = (rdx != 0) ? 1 : 0;
label_20:
    r10d = *((rsp + 0x48));
    r10d &= 0x10;
    if (r10d == 0) {
        goto label_50;
    }
label_12:
    r8d = *((rsp + 0x34));
    ebx = 0;
    r11 = r8;
    if (r8 <= rcx) {
        goto label_51;
    }
label_8:
    r8 = *((rsp + 8));
    if (*((rsp + 0x10)) == 1) {
        goto label_52;
    }
label_28:
    r11d = *((rsp + 0x10));
    if (r11d == 0) {
        esi += edi;
        if (esi <= 0) {
            goto label_19;
        }
label_18:
        rcx++;
        if (r10d == 0) {
            goto label_19;
        }
        eax = *((rsp + 0x34));
        if (rax == rcx) {
            goto label_53;
        }
    }
label_19:
    rsi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        rbp--;
        rdx:rax = rax * rsi;
        rax = rcx;
        rdx >>= 3;
        rdi = rdx * 5;
        rdi += rdi;
        rax -= rdi;
        eax += 0x30;
        *(rbp) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    if ((*((rsp + 0x48)) & 4) == 0) {
        goto label_54;
    }
label_6:
    r12 = r8;
    *((rsp + 0x40)) = r8;
    r13 = 0xffffffffffffffff;
    rax = strlen (r14);
    r12 -= rbp;
    rsi = rbp;
    ecx = 0x29;
    r15 = rax;
    rax = rsp + 0x50;
    rdx = r12;
    rdi = rax;
    *((rsp + 0x10)) = rax;
    memcpy_chk ();
    *((rsp + 0x20)) = ebx;
    rbp = *((rsp + 0x40));
    rbx = r12;
    r12 = *((rsp + 0x38));
    while (al == 0) {
        rax = *((rsp + 0x10));
        if (r13 > rbx) {
            r13 = rbx;
        }
        rbx -= r13;
label_1:
        rbp -= r13;
        memcpy (rbp, rax + rbx, r13);
        if (rbx == 0) {
            goto label_55;
        }
        rbp -= r15;
        memcpy (rbp, r14, r15);
        eax = *(r12);
    }
    if (al > 0x7e) {
        goto label_56;
    }
    if (rax > rbx) {
        rax = rbx;
    }
    rbx -= rax;
    r13 = rax;
    rax = *((rsp + 0x10));
    rsi = rax + rbx;
label_7:
    r12++;
    goto label_1;
label_4:
    fp_stack[0] += *(0x00013114);
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    if ((*((rsp + 0x48)) & 0x10) != 0) {
        goto label_2;
    }
    goto label_40;
label_39:
    fp_stack[0] += *(0x00013114);
    *((rsp + 0x20)) = rbx;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = *((rsp + 0x20));
    if (rbx >= 0) {
        goto label_3;
    }
    goto label_4;
label_38:
    fp_stack[0] += *(0x00013114);
    goto label_5;
label_40:
    if (*((rsp + 0x10)) != 1) {
        *(fp_stack--) = fp_stack[?];
        __asm ("fcompi st(1)");
        if (*((rsp + 0x10)) <= 1) {
            goto label_27;
        }
        *(fp_stack--) = *(0x00013118);
        fp_tmp_5 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_5;
        if (fp_stack[0] >= fp_stack[1]) {
            goto label_57;
        }
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        eax = *((rsp + 0x4e));
        ah |= 0xc;
        *((rsp + 0x4c)) = ax;
        *(fp_stack--) = fp_stack[0];
        *((rsp + 0x20)) = fp_stack[0];
        fp_stack--;
        rax = *((rsp + 0x20));
label_24:
        *((rsp + 0x20)) = rax;
        *(fp_stack--) = *((rsp + 0x20));
        if (rax < 0) {
            fp_stack[0] += *(0x00013114);
        }
        r8d = *((rsp + 0x10));
        if (r8d == 0) {
            fp_tmp_6 = fp_stack[1];
            fp_stack[1] = fp_stack[0];
            fp_stack[0] = fp_tmp_6;
            __asm ("fucompi st(1)");
            if (r8d != 0) {
                if (r8d == 0) {
                    goto label_27;
                }
                fp_stack++;
            } else {
                fp_stack++;
            }
            rax++;
            *((rsp + 0x10)) = rax;
            *(fp_stack--) = *((rsp + 0x10));
            if (rax < 0) {
                goto label_58;
            }
        } else {
            fp_stack[1] = fp_stack[0];
            fp_stack--;
        }
    }
label_27:
    rdx = 0xffffffffffffffff;
    esi = 1;
    eax = 0;
    rbx = *((rsp + 0x10));
    rcx = 0x0001311c;
    ? = fp_stack[0];
    fp_stack--;
    rdi = rbx;
    sprintf_chk ();
    ebx = 0xffffffff;
    rax = strlen (rbx);
    rdx = rax;
    r12 = rax;
label_11:
    rbp = *((rsp + 8));
    rbp -= rdx;
    memmove (rbp, *(rsp), rdx);
    r8 = rbp + r12;
label_26:
    if ((*((rsp + 0x48)) & 4) != 0) {
        goto label_6;
    }
    do {
label_54:
        if ((*((rsp + 0x48)) & 0x80) != 0) {
            if (ebx == 0xffffffff) {
                goto label_59;
            }
label_33:
            eax = *((rsp + 0x48));
            eax &= 0x100;
            esi = eax;
            esi |= ebx;
            if (esi != 0) {
                goto label_60;
            }
        }
label_13:
        rax = *((rsp + 8));
        *(rax) = 0;
        rax = *((rsp + 0x88));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_61;
        }
        rax = rbp;
        return rax;
label_56:
        r13 = rbx;
        rsi = *((rsp + 0x10));
        ebx = 0;
        goto label_7;
label_55:
        ebx = *((rsp + 0x20));
    } while (1);
label_9:
    sil = (esi != 0) ? 1 : 0;
    esi = (int32_t) sil;
label_10:
    ebx++;
    if (r8 > r9) {
        goto label_62;
    }
    if (ebx == 8) {
        goto label_8;
    }
label_51:
    rax = rcx;
    edx = 0;
    ecx = esi;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    ecx >>= 1;
    r9 = rax;
    eax = rdx * 5;
    edx = 0;
    eax = rdi + rax*2;
    eax = edx:eax / r11d;
    edx = edx:eax % r11d;
    edx = rcx + rdx*2;
    edi = eax;
    rcx = r9;
    esi += edx;
    if (r11d > edx) {
        goto label_9;
    }
    sil = (r11d < esi) ? 1 : 0;
    esi = (int32_t) sil;
    esi += 2;
    goto label_10;
label_44:
    *(fp_stack--) = fp_stack[0];
    goto label_47;
label_46:
    fp_tmp_7 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_7;
    goto label_47;
label_48:
    fp_tmp_8 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_8;
label_47:
    ? = fp_stack[0];
    fp_stack--;
    esi = 1;
    eax = 0;
    r12 = *((rsp + 0x10));
    rcx = "%.1Lf";
    rdx = 0xffffffffffffffff;
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r12);
    rdx = rax;
    *(fp_stack--) = fp_stack[?];
    if (rdx > rbp) {
        goto label_63;
    }
label_14:
    if ((*((rsp + 0x48)) & 8) != 0) {
        rax = *(rsp);
        if (*((rax + rdx - 1)) == 0x30) {
            goto label_64;
        }
        fp_stack++;
    } else {
        fp_stack++;
    }
    r12 = rdx;
    r12 -= r15;
    goto label_11;
label_37:
    r10d = *((rsp + 0x48));
    esi = 0;
    edi = 0;
    r10d &= 0x10;
    if (r10d != 0) {
        goto label_12;
    }
label_50:
    ebx = 0xffffffff;
    goto label_8;
label_59:
    rcx = *((rsp + 0x18));
    if (rcx <= 1) {
        goto label_65;
    }
    edx = *((rsp + 0x34));
    ebx = 1;
    eax = 1;
    do {
        rax *= rdx;
        if (rcx <= rax) {
            goto label_66;
        }
        ebx++;
    } while (ebx != 8);
label_66:
    esi = *((rsp + 0x48));
    eax = *((rsp + 0x48));
    eax &= 0x100;
    esi &= 0x40;
    if (esi != 0) {
label_16:
        rsi = *(rsp);
        rdi = rsi + 0x288;
        *((rsi + 0x287)) = 0x20;
        *((rsp + 8)) = rdi;
label_15:
        if (ebx == 0) {
            goto label_67;
        }
    }
    rsi = *((rsp + 8));
    r9d = *((rsp + 0x30));
    rdx = rsi + 1;
    if (r9d == 0) {
        if (ebx == 1) {
            goto label_68;
        }
    }
    rbx = (int64_t) ebx;
    rcx = obj_power_letter;
    ecx = *((rcx + rbx));
    *(rsi) = cl;
    if (eax == 0) {
        goto label_69;
    }
    r8d = *((rsp + 0x30));
    if (r8d != 0) {
        goto label_70;
    }
label_22:
    rax = rdx + 1;
    *(rdx) = 0x42;
    *((rsp + 8)) = rax;
    goto label_13;
label_43:
    rdx = 0xffffffffffffffff;
    esi = 1;
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    r12 = *((rsp + 0x10));
    rcx = "%.1Lf";
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r12);
    *(fp_stack--) = fp_stack[?];
    rdx = rax;
    if (rax <= rbp) {
        goto label_14;
    }
    *(fp_stack--) = *(0x0001315c);
    fp_stack[1] *= fp_stack[0];
label_17:
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    rdx = 0xffffffffffffffff;
    eax = 0;
    r15 = *((rsp + 0x10));
    rcx = 0x0001311c;
    esi = 1;
    rdi = r15;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r15);
    rdx = rax;
    r12 = rax;
    goto label_11;
label_60:
    if ((*((rsp + 0x48)) & 0x40) == 0) {
        goto label_15;
    }
    goto label_16;
label_63:
    *(fp_stack--) = *(0x0001315c);
    fp_stack[1] *= fp_stack[0];
label_23:
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(2)");
    if ((*((rsp + 0x48)) & 0x40) <= 0) {
        goto label_17;
    }
    *(fp_stack--) = *(0x00013118);
    fp_tmp_9 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_9;
    if (fp_stack[0] >= fp_stack[2]) {
        goto label_71;
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_tmp_10 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_10;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
label_25:
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        fp_stack[0] += *(0x00013114);
    }
    edx = *((rsp + 0x10));
    if (edx == 0) {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        if (fp_stack[0] != fp_stack[1]) {
            if (fp_stack[0] == fp_stack[1]) {
                goto label_72;
            }
            fp_stack++;
        } else {
            fp_stack++;
        }
        rax++;
        *((rsp + 0x10)) = rax;
        *(fp_stack--) = *((rsp + 0x10));
        if (rax < 0) {
            goto label_73;
        }
    } else {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
label_72:
    fp_tmp_11 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_11;
    goto label_17;
label_52:
    rax = rcx;
    rsi = (int64_t) esi;
    eax &= 1;
    rax += rsi;
    al = (rax != 0) ? 1 : 0;
    eax = (int32_t) al;
    eax += edi;
    if (eax > 5) {
        goto label_18;
    }
    goto label_19;
label_49:
    esi = 2;
    eax = 3;
    if (eax < 5) {
        esi = eax;
    }
    goto label_20;
label_45:
    fp_stack[0] -= fp_stack[1];
    fp_tmp_12 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_12;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_21;
label_70:
    *((rsi + 1)) = 0x69;
    rdx = rsi + 2;
    goto label_22;
label_64:
    *(fp_stack--) = *(0x0001315c);
    fp_stack[1] *= fp_stack[0];
    if (*((rsp + 0x10)) != 1) {
        goto label_23;
    }
    goto label_17;
label_57:
    fp_stack[0] -= fp_stack[1];
    fp_tmp_13 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_13;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_24;
label_71:
    fp_stack[0] -= fp_stack[2];
    fp_tmp_14 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_14;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    fp_tmp_15 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_15;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_25;
label_53:
    if (ebx == 8) {
        goto label_19;
    }
    ebx++;
    if ((*((rsp + 0x48)) & 8) == 0) {
        goto label_74;
    }
label_35:
    *((r8 - 1)) = 0x31;
    rbp = r8 - 1;
    goto label_26;
label_58:
    fp_stack[0] += *(0x00013114);
    goto label_27;
label_73:
    fp_stack[0] += *(0x00013114);
    fp_tmp_16 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_16;
    goto label_17;
label_68:
    *(rsi) = 0x6b;
    if (eax != 0) {
        goto label_22;
    }
label_69:
    *((rsp + 8)) = rdx;
    goto label_13;
label_62:
    if (r9 > 9) {
        goto label_8;
    }
    if (*((rsp + 0x10)) == 1) {
        goto label_75;
    }
    r12d = *((rsp + 0x10));
    if (r12d != 0) {
        goto label_76;
    }
    if (esi == 0) {
        goto label_76;
    }
label_30:
    edx = rax + 1;
    if (eax == 9) {
        goto label_77;
    }
    eax = rdx + 0x30;
label_32:
    rdi = *(rsp);
    r8 = rdi + 0x286;
    *((rdi + 0x286)) = al;
    eax = ebp;
    r8 -= rbp;
    if (ebp >= 8) {
        goto label_78;
    }
    if ((bpl & 4) != 0) {
        goto label_79;
    }
    if (eax != 0) {
        edx = *(r15);
        *(r8) = dl;
        if ((al & 2) != 0) {
            goto label_80;
        }
    }
label_29:
    esi = 0;
label_31:
    edi = 0;
    if (*((rsp + 0x10)) != 1) {
        goto label_28;
    }
    goto label_19;
label_80:
    edx = *((r15 + rax - 2));
    *((r8 + rax - 2)) = dx;
    goto label_29;
label_75:
    edx = eax;
    edx &= 1;
    edx += esi;
    if (edx > 2) {
        goto label_30;
    }
label_76:
    if (eax != 0) {
        goto label_81;
    }
label_34:
    r8 = *((rsp + 8));
    if ((*((rsp + 0x48)) & 8) != 0) {
        goto label_31;
    }
    eax = 0x30;
    goto label_32;
label_65:
    ebx = 0;
    goto label_33;
label_78:
    rax = *(r15);
    rsi = r8 + 8;
    rdi = r15;
    rsi &= 0xfffffffffffffff8;
    *(r8) = rax;
    eax = ebp;
    rdx = *((r15 + rax - 8));
    *((r8 + rax - 8)) = rdx;
    rax = r8;
    rax -= rsi;
    rdi -= rax;
    eax += ebp;
    eax &= 0xfffffff8;
    if (eax < 8) {
        goto label_29;
    }
    eax &= 0xfffffff8;
    r9d = eax;
    eax = 0;
    do {
        edx = eax;
        eax += 8;
        r11 = *((rdi + rdx));
        *((rsi + rdx)) = r11;
    } while (eax < r9d);
    goto label_29;
label_77:
    rcx = r9 + 1;
    if (r9 == 9) {
        goto label_82;
    }
    esi = 0;
    goto label_34;
label_74:
    rax = rbp;
    *((r8 - 1)) = 0x30;
    rax = ~rax;
    r8 += rax;
    eax = ebp;
    if (ebp >= 8) {
        goto label_83;
    }
    ebp &= 4;
    if (ebp != 0) {
        goto label_84;
    }
    if (eax == 0) {
        goto label_35;
    }
    edx = *(r15);
    *(r8) = dl;
    if ((al & 2) == 0) {
        goto label_35;
    }
    edx = *((r15 + rax - 2));
    *((r8 + rax - 2)) = dx;
    goto label_35;
label_83:
    rax = *(r15);
    rcx = r8 + 8;
    rcx &= 0xfffffffffffffff8;
    *(r8) = rax;
    eax = ebp;
    rdx = *((r15 + rax - 8));
    *((r8 + rax - 8)) = rdx;
    rax = r8;
    rax -= rcx;
    r15 -= rax;
    eax += ebp;
    eax &= 0xfffffff8;
    if (eax < 8) {
        goto label_35;
    }
    eax &= 0xfffffff8;
    edx = 0;
    do {
        esi = edx;
        edx += 8;
        rdi = *((r15 + rsi));
        *((rcx + rsi)) = rdi;
    } while (edx < eax);
    goto label_35;
label_82:
    r8 = *((rsp + 8));
    goto label_29;
label_79:
    edx = *(r15);
    *(r8) = edx;
    edx = *((r15 + rax - 4));
    *((r8 + rax - 4)) = edx;
    goto label_29;
label_61:
    eax = stack_chk_fail ();
label_84:
    edx = *(r15);
    *(r8) = edx;
    edx = *((r15 + rax - 4));
    *((r8 + rax - 4)) = edx;
    goto label_35;
label_67:
    rdx = *((rsp + 8));
    if (eax != 0) {
        goto label_22;
    }
    goto label_13;
label_81:
    eax += 0x30;
    goto label_32;
}

/* /tmp/tmp6wcn29hf @ 0x8280 */
 
uint64_t dbg_hash_initialize (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* Hash_table * hash_initialize(size_t candidate,Hash_tuning const * tuning,Hash_hasher hasher,Hash_comparator comparator,Hash_data_freer data_freer); */
    rax = dbg_raw_hasher;
    r15 = rsi;
    r14 = r8;
    r13 = rdi;
    edi = 0x50;
    rbx = rcx;
    if (rdx == 0) {
    }
    rax = dbg_raw_comparator;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = malloc (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = obj_default_tuning;
    rdi = r12;
    if (r15 == 0) {
        r15 = rax;
    }
    *((r12 + 0x28)) = r15;
    al = check_tuning ();
    if (al == 0) {
        goto label_1;
    }
    esi = *((r15 + 0x10));
    xmm0 = *((r15 + 8));
    rdi = r13;
    rax = compute_bucket_size_isra_0 ();
    *((r12 + 0x10)) = rax;
    r13 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = calloc (rax, 0x10);
    *(r12) = rax;
    if (rax == 0) {
        goto label_1;
    }
    r13 <<= 4;
    *((r12 + 0x30)) = rbp;
    rax += r13;
    *((r12 + 0x38)) = rbx;
    *((r12 + 8)) = rax;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x40)) = r14;
    *((r12 + 0x48)) = 0;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        r12d = 0;
        free (r12);
    } while (1);
}

/* /tmp/tmp6wcn29hf @ 0xcfb0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0xc430 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xdde0 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3900)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xc6d0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x7fe0 */
 
uint64_t hash_lookup (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 0x10));
    rdi = r12;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t) (rbx, rbp);
    if (rax >= *((rbp + 0x10))) {
        void (*0x3b6a)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    rsi = *(rbx);
    if (rsi != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rsi = *(rbx);
label_0:
        if (rsi == r12) {
            goto label_2;
        }
        rdi = r12;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_3;
        }
        rbx = *((rbx + 8));
    } while (rbx != 0);
label_1:
    eax = 0;
    return rax;
label_3:
    r12 = *(rbx);
label_2:
    rax = *(rbx);
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x8ad0 */
 
uint64_t dbg_triple_hash (int64_t arg_8h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* size_t triple_hash( const * x,size_t table_size); */
    rbx = rsi;
    rax = hash_pjw (*(rdi));
    edx = 0;
    rax ^= *((rbp + 8));
    rax = rdx:rax / rbx;
    rdx = rdx:rax % rbx;
    rax = rdx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xf130 */
 
uint64_t rotate_left64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
int64_t dbg_hash_pjw (int64_t arg1, size_t tablesize) {
    rdi = arg1;
    rsi = tablesize;
    /* size_t hash_pjw( const * x,size_t tablesize); */
    rdx = *(rdi);
    if (dl == 0) {
        goto label_0;
    }
    eax = 0;
    do {
        rax = rotate_left64 (rax, 9);
        rdi++;
        rax += rdx;
        rdx = *(rdi);
    } while (dl != 0);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    r8 = rdx;
    rax = rdx;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x8380 */
 
int64_t dbg_hash_clear (uint32_t arg_8h, int64_t arg_18h, int64_t arg_20h, int64_t arg_40h, int64_t arg_48h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_clear(Hash_table * table); */
    r12 = *(rdi);
    if (r12 < *((rdi + 8))) {
        goto label_0;
    }
    goto label_1;
    do {
        r12 += 0x10;
        if (*((rbp + 8)) <= r12) {
            goto label_1;
        }
label_0:
    } while (*(r12) == 0);
    rbx = *((r12 + 8));
    rdx = *((rbp + 0x40));
    if (rbx != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        rbx = rax;
label_2:
        if (rdx != 0) {
            rdi = *(rbx);
            void (*rdx)() ();
            rdx = *((rbp + 0x40));
        }
        rax = *((rbx + 8));
        rcx = *((rbp + 0x48));
        *(rbx) = 0;
        *((rbx + 8)) = rcx;
        *((rbp + 0x48)) = rbx;
    } while (rax != 0);
label_3:
    if (rdx != 0) {
        rdi = *(r12);
        void (*rdx)() ();
    }
    *(r12) = 0;
    r12 += 0x10;
    *((r12 - 8)) = 0;
    if (*((rbp + 8)) > r12) {
        goto label_0;
    }
label_1:
    *((rbp + 0x18)) = 0;
    *((rbp + 0x20)) = 0;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x8250 */
 
int64_t dbg_hash_reset_tuning (Hash_tuning * tuning) {
    rdi = tuning;
    /* void hash_reset_tuning(Hash_tuning * tuning); */
    rax = 0x3f80000000000000;
    *((rdi + 0x10)) = 0;
    *(rdi) = rax;
    rax = 0x3fb4fdf43f4ccccd;
    *((rdi + 8)) = rax;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x8b00 */
 
int32_t dbg_triple_compare_ino_str (char ** s1, char ** s2) {
    rdi = s1;
    rsi = s2;
    /* _Bool triple_compare_ino_str( const * x, const * y); */
    rdx = *((rsi + 8));
    eax = 0;
    while (*((rdi + 0x10)) != rcx) {
        return eax;
        rcx = *((rsi + 0x10));
    }
    eax = strcmp (*(rdi), *(rsi));
    al = (eax == 0) ? 1 : 0;
    return eax;
}

/* /tmp/tmp6wcn29hf @ 0x8140 */
 
int64_t dbg_hash_get_entries (void ** buffer, size_t buffer_size, Hash_table const * table) {
    rsi = buffer;
    rdx = buffer_size;
    rdi = table;
    /* size_t hash_get_entries(Hash_table const * table,void ** buffer,size_t buffer_size); */
    r9 = *(rdi);
    eax = 0;
    if (r9 >= *((rdi + 8))) {
        goto label_2;
    }
    do {
        if (*(r9) != 0) {
            goto label_3;
        }
label_0:
        r9 += 0x10;
    } while (*((rdi + 8)) > r9);
    return eax;
label_3:
    rcx = r9;
    goto label_4;
label_1:
    r8 = *(rcx);
    rax++;
    *((rsi + rax*8 - 8)) = r8;
    rcx = *((rcx + 8));
    if (rcx == 0) {
        goto label_0;
    }
label_4:
    if (rdx > rax) {
        goto label_1;
    }
label_2:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x7500 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmp6wcn29hf @ 0xd9f0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xdbb0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xe6b0 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x38c0 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmp6wcn29hf @ 0x8520 */
 
int64_t hash_rehash (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t canary;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdi = rsi;
    r12 = *((rbp + 0x28));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    esi = *((r12 + 0x10));
    xmm0 = *((r12 + 8));
    rax = compute_bucket_size_isra_0 ();
    if (rax == 0) {
        goto label_1;
    }
    rbx = rax;
    if (*((rbp + 0x10)) == rax) {
        goto label_2;
    }
    rax = calloc (rax, 0x10);
    *(rsp) = rax;
    if (rax == 0) {
        goto label_1;
    }
    *((rsp + 0x10)) = rbx;
    rbx <<= 4;
    r13 = rsp;
    edx = 0;
    rax += rbx;
    rsi = rbp;
    rdi = r13;
    *((rsp + 0x28)) = r12;
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x30));
    *((rsp + 0x18)) = 0;
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 0x38));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x38)) = rax;
    rax = *((rbp + 0x40));
    *((rsp + 0x40)) = rax;
    rax = *((rbp + 0x48));
    *((rsp + 0x48)) = rax;
    eax = transfer_entries ();
    r12d = eax;
    if (al != 0) {
        goto label_3;
    }
    rax = *((rsp + 0x48));
    edx = 1;
    rsi = r13;
    rdi = rbp;
    *((rbp + 0x48)) = rax;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x3b7a)() ();
    }
    edx = 0;
    rsi = r13;
    rdi = rbp;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x3b7a)() ();
    }
    free (*(rsp));
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        eax = r12d;
        return rax;
label_2:
        r12d = 1;
    } while (1);
label_1:
    r12d = 0;
    goto label_0;
label_3:
    free (*(rbp));
    rax = *(rsp);
    *(rbp) = rax;
    rax = *((rsp + 8));
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x10));
    *((rbp + 0x10)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    rax = *((rsp + 0x48));
    *((rbp + 0x48)) = rax;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xeed0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x3710)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp6wcn29hf @ 0xdad0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xd0d0 */
 
uint64_t dbg_gl_scratch_buffer_dupfree (uint32_t arg1, size_t size) {
    size_t var_8h;
    rdi = arg1;
    rsi = size;
    /* void * gl_scratch_buffer_dupfree(scratch_buffer * buffer,size_t size); */
    rdi += 0x10;
    r12 = *((rdi - 0x10));
    if (r12 == rdi) {
        goto label_1;
    }
    rax = realloc (r12, rsi);
    if (rax == 0) {
        goto label_2;
    }
    do {
label_0:
        return rax;
label_1:
        rdi = rsi;
        *((rsp + 8)) = rsi;
        rax = malloc (rdi);
        rdi = rax;
    } while (rax == 0);
    rdx = *((rsp + 8));
    rsi = r12;
    void (*0x3900)() ();
label_2:
    rax = r12;
    goto label_0;
}

/* /tmp/tmp6wcn29hf @ 0xcfc0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0xcf10 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3bbc)() ();
    }
    if (rax == 0) {
        void (*0x3bbc)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x7770 */
 
int64_t dbg_seen_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg1, uint32_t arg2, int64_t arg3) {
    F_triple new_ent;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool seen_file(Hash_table const * ht,char const * file,stat const * stats); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (rdi != 0) {
        rax = *((rdx + 8));
        *(rsp) = rsi;
        rsi = rsp;
        *((rsp + 8)) = rax;
        rax = *(rdx);
        *((rsp + 0x10)) = rax;
        rax = hash_lookup ();
        al = (rax != 0) ? 1 : 0;
    }
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x7db0 */
 
int64_t hash_get_max_bucket_length (int64_t arg1) {
    rdi = arg1;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8d = 0;
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_1;
    do {
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_1;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_2;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_2:
    if (r8 < rdx) {
        r8 = rdx;
    }
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_0;
    }
label_1:
    rax = r8;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xa1e0 */
 
int64_t dbg_read_file_system_list (void * arg_8h, void * arg_10h, void * arg_18h, int64_t arg_28h) {
    uint32_t devmaj;
    uint32_t devmin;
    int32_t mntroot_s;
    mount_entry * mount_list;
    char * line;
    size_t buf_size;
    void * var_8h;
    void * var_10h;
    void ** var_18h;
    char * s2;
    int64_t var_28h;
    int64_t var_2ch;
    int64_t var_30h;
    char * ptr;
    char * var_40h;
    int64_t var_48h;
    /* mount_entry * read_file_system_list(_Bool need_fs_type); */
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    rax = fopen ("/proc/self/mountinfo", 0x000131f8);
    if (rax == 0) {
        goto label_11;
    }
    r12 = rax;
    rax = rsp + 0x30;
    *((rsp + 0x38)) = 0;
    r14 = rsp + 0x40;
    *(rsp) = rax;
    rbx = rsp + 0x38;
    r15 = "%*u %*u %u:%u %n";
    *((rsp + 0x40)) = 0;
    do {
label_0:
        rcx = r12;
        edx = 0xa;
        rsi = r14;
        rdi = rbx;
        rax = getdelim ();
        if (rax == -1) {
            goto label_12;
        }
        rdi = *((rsp + 0x38));
        rcx = rsp + 0x28;
        rsi = r15;
        eax = 0;
        rdx = rsp + 0x24;
        r8 = rsp + 0x2c;
        eax = isoc99_sscanf ();
        eax -= 2;
    } while (eax > 1);
    r13 = *((rsp + 0x2c));
    r13 += *((rsp + 0x38));
    rax = strchr (r13, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    *(rax) = 0;
    rbp = rax + 1;
    rax = strchr (rbp, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    *(rax) = 0;
    rax = strstr (rax + 1, 0x00013196);
    if (rax == 0) {
        goto label_0;
    }
    rax += 3;
    rdi = rax;
    *((rsp + 8)) = rax;
    rax = strchr (rdi, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    *(rax) = 0;
    r8 = rax + 1;
    rdi = r8;
    *((rsp + 0x10)) = r8;
    rax = strchr (rdi, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    r8 = *((rsp + 0x10));
    *(rax) = 0;
    rdi = r8;
    *((rsp + 0x18)) = r8;
    unescape_tab (rdi);
    unescape_tab (rbp);
    unescape_tab (r13);
    unescape_tab (*((rsp + 8)));
    rax = xmalloc (0x38);
    *((rsp + 0x10)) = rax;
    rax = xstrdup (*((rsp + 0x18)));
    rdx = *((rsp + 0x10));
    *(rdx) = rax;
    rax = xstrdup (rbp);
    rdx = *((rsp + 0x10));
    r13d = 1;
    *((rdx + 8)) = rax;
    rax = xstrdup (r13);
    rdx = *((rsp + 0x10));
    *((rdx + 0x10)) = rax;
    rax = xstrdup (*((rsp + 8)));
    ecx = *((rsp + 0x24));
    rdx = *((rsp + 0x10));
    rdi = 0xfffff00000000000;
    rsi = rcx;
    *((rdx + 0x18)) = rax;
    rcx <<= 0x20;
    eax = *((rsp + 0x28));
    rsi <<= 8;
    rcx &= rdi;
    *((rdx + 0x28)) |= 4;
    esi &= 0xfff00;
    *((rsp + 8)) = rdx;
    rcx |= rsi;
    esi = (int32_t) al;
    rax <<= 0xc;
    rcx |= rsi;
    rsi = 0xffffff00000;
    rax &= rsi;
    rax |= rcx;
    *((rdx + 0x20)) = rax;
    eax = strcmp (rbp, "autofs");
    rdx = *((rsp + 8));
    while (eax == 0) {
label_1:
        eax = *((rdx + 0x28));
        *((rsp + 8)) = rdx;
        eax &= 0xfffffffe;
        eax |= r13d;
        r13 = *(rdx);
        *((rdx + 0x28)) = al;
        rax = strchr (r13, 0x3a);
        rdx = *((rsp + 8));
        ecx = 1;
        if (rax == 0) {
            goto label_13;
        }
label_2:
        eax = *((rdx + 0x28));
        ecx += ecx;
        eax &= 0xfffffffd;
        eax |= ecx;
        *((rdx + 0x28)) = al;
        rax = *(rsp);
        *(rax) = rdx;
        rax = rdx + 0x30;
        *(rsp) = rax;
        goto label_0;
label_12:
        free (*((rsp + 0x38)));
        if ((*(r12) & 0x20) != 0) {
            goto label_14;
        }
        eax = rpl_fclose (r12);
        if (eax == 0xffffffff) {
            goto label_15;
        }
label_5:
        rax = *(rsp);
        *(rax) = 0;
        r14 = *((rsp + 0x30));
label_4:
        rax = *((rsp + 0x48));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_16;
        }
        rax = r14;
        return rax;
        eax = strcmp (rbp, "proc");
        rdx = *((rsp + 8));
    }
    eax = strcmp (rbp, "subfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "debugfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "devpts");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "fusectl");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "fuse.portal");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "mqueue");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "rpc_pipefs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "sysfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "devfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "kernfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "ignore");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "none");
    rdx = *((rsp + 8));
    r13b = (eax == 0) ? 1 : 0;
    goto label_1;
label_13:
    if (*(r13) == 0x2f) {
        goto label_17;
    }
label_7:
    *((rsp + 8)) = rdx;
    eax = strcmp (rbp, "acfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    *((rsp + 0x10)) = cl;
    eax = strcmp (rbp, 0x00013215);
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "coda");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "auristorfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "fhgfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "gpfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "ibrix");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "ocfs2");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "vxfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp ("-hosts", r13);
    rdx = *((rsp + 8));
    cl = (eax == 0) ? 1 : 0;
    goto label_2;
label_14:
    rax = errno_location ();
    r14d = *(rax);
    r13 = rax;
    rpl_fclose (r12);
    *(r13) = r14d;
label_6:
    rax = *(rsp);
    *(rax) = 0;
    rbx = *((rsp + 0x30));
    if (rbx != 0) {
        goto label_18;
    }
    goto label_19;
    do {
label_3:
        free (rbp);
        *((rsp + 0x30)) = rbx;
        if (rbx == 0) {
            goto label_19;
        }
label_18:
        rbx = *((rbx + 0x30));
        free (*(rbp));
        free (*((rbp + 8)));
        free (*((rbp + 0x10)));
    } while ((*((rbp + 0x28)) & 4) == 0);
    free (*((rbp + 0x18)));
    goto label_3;
label_19:
    *(r13) = r14d;
    r14d = 0;
    goto label_4;
label_8:
    rdi = r14;
    eax = endmntent ();
    if (eax != 0) {
        goto label_5;
    }
label_15:
    rax = errno_location ();
    r14d = *(rax);
    r13 = rax;
    goto label_6;
label_17:
    if (*((r13 + 1)) != 0x2f) {
        goto label_7;
    }
    *((rsp + 0x10)) = cl;
    eax = strcmp (rbp, "smbfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "smb3");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "cifs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    goto label_7;
label_11:
    rsi = 0x0001306f;
    rdi = "/etc/mtab";
    rax = setmntent ();
    r14 = rax;
    if (rax == 0) {
        goto label_4;
    }
    rax = rsp + 0x30;
    r15 = "autofs";
    *(rsp) = rax;
    goto label_20;
label_9:
    eax = *((rbx + 0x28));
    r12d += r12d;
    *((rbx + 0x20)) = 0xffffffffffffffff;
    eax &= 0xfffffffd;
    eax |= r12d;
    *((rbx + 0x28)) = al;
    rax = *(rsp);
    *(rax) = rbx;
    rax = rbx + 0x30;
    *(rsp) = rax;
label_20:
    rdi = r14;
    rax = getmntent ();
    if (rax == 0) {
        goto label_8;
    }
    rsi = "bind";
    rdi = rbp;
    r12d = 1;
    rax = hasmntopt ();
    r13 = rax;
    rax = xmalloc (0x38);
    rbx = rax;
    rax = xstrdup (*(rbp));
    *(rbx) = rax;
    rax = xstrdup (*((rbp + 8)));
    *((rbx + 0x10)) = 0;
    *((rbx + 8)) = rax;
    rax = xstrdup (*((rbp + 0x10)));
    *((rbx + 0x28)) |= 4;
    *((rbx + 0x18)) = rax;
    rdi = rax;
    eax = strcmp (rdi, r15);
    if (eax != 0) {
        eax = strcmp (rbp, "proc");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "subfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "debugfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "devpts");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "fusectl");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "fuse.portal");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "mqueue");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "rpc_pipefs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "sysfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "devfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "kernfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "ignore");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "none");
        r12b = (r13 == 0) ? 1 : 0;
        al = (eax == 0) ? 1 : 0;
        r12d &= eax;
    }
label_21:
    eax = *((rbx + 0x28));
    r13 = *(rbx);
    eax &= 0xfffffffe;
    eax |= r12d;
    r12d = 1;
    *((rbx + 0x28)) = al;
    rax = strchr (r13, 0x3a);
    if (rax != 0) {
        goto label_9;
    }
    if (*(r13) == 0x2f) {
        goto label_22;
    }
label_10:
    r12d = 1;
    eax = strcmp (rbp, "acfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, 0x00013215);
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "coda");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "auristorfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "fhgfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "gpfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "ibrix");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "ocfs2");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "vxfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp ("-hosts", r13);
    r12b = (eax == 0) ? 1 : 0;
    goto label_9;
label_16:
    stack_chk_fail ();
label_22:
    if (*((r13 + 1)) != 0x2f) {
        goto label_10;
    }
    eax = strcmp (rbp, "smbfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "smb3");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "cifs");
    if (eax == 0) {
        goto label_9;
    }
    goto label_10;
}

/* /tmp/tmp6wcn29hf @ 0xe7a0 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmp6wcn29hf @ 0xde20 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3900)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x75e0 */
 
uint64_t dir_len (uint32_t arg1) {
    rdi = arg1;
    ebp = 0;
    rbx = rdi;
    bpl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbx;
    while (rax > rbp) {
        rdx = rax - 1;
        if (*((rbx + rax - 1)) != 0x2f) {
            goto label_0;
        }
        rax = rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x10250 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0x11230)() ();
}

/* /tmp/tmp6wcn29hf @ 0x8930 */
 
int64_t dbg_hash_remove (int64_t arg_8h, int64_t arg1) {
    hash_entry * bucket;
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_remove(Hash_table * table, const * entry); */
    ecx = 1;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    rax = hash_find_entry ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = *(rsp);
    *((rbx + 0x20))--;
    while (rax <= 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
        rax = *((rbx + 0x18));
        rax--;
        *((rbx + 0x18)) = rax;
        if (rax < 0) {
            goto label_5;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_6;
        }
label_1:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_2:
        rax = *((rbx + 0x28));
        xmm0 = *(rax);
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm0, xmm5");
    }
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm0 = *(rax);
    __asm ("mulss xmm0, xmm4");
    __asm ("comiss xmm0, xmm5");
    if (rax <= 0) {
        goto label_0;
    }
    __asm ("mulss xmm4, dword [rax + 4]");
    if (*((rax + 0x10)) == 0) {
        __asm ("mulss xmm4, dword [rax + 8]");
    }
    __asm ("comiss xmm4, dword [0x00013118]");
    if (*((rax + 0x10)) >= 0) {
        goto label_7;
    }
    __asm ("cvttss2si rsi, xmm4");
label_3:
    rdi = rbx;
    al = hash_rehash ();
    if (al != 0) {
        goto label_0;
    }
    rbp = *((rbx + 0x48));
    if (rbp == 0) {
        goto label_8;
    }
    do {
        rbp = *((rbp + 8));
        rax = free (rbp);
    } while (rbp != 0);
label_8:
    *((rbx + 0x48)) = 0;
    goto label_0;
label_5:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_1;
    }
label_6:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_2;
label_7:
    __asm ("subss xmm4, dword [0x00013118]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_3;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xc6c0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0xc5d0)() ();
}

/* /tmp/tmp6wcn29hf @ 0x9a40 */
 
uint64_t dbg_mbsalign (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_24h;
    size_t size;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* size_t mbsalign(char const * src,char * dest,size_t dest_size,size_t * width,mbs_align_t align,int flags); */
    r13 = rdi;
    rbx = rsi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rcx;
    *((rsp + 0x24)) = r8d;
    rax = strlen (rdi);
    *((rsp + 8)) = rax;
    r9 = rax;
    if ((bpl & 2) == 0) {
        goto label_11;
    }
label_3:
    r12 = r9;
    r15d = 0;
    r14d = 0;
label_5:
    rax = *((rsp + 0x10));
    rax = *(rax);
    if (rax >= r12) {
        goto label_12;
    }
    r9 = rax;
    edx = 0;
label_9:
    rsi = *((rsp + 0x10));
    *(rsi) = rax;
    eax = *((rsp + 0x24));
    if (eax == 0) {
        goto label_13;
    }
label_0:
    r12d = 0;
    if (eax != 1) {
        r12 = rdx;
        edx &= 1;
        r12 >>= 1;
        rdx += r12;
    }
label_1:
    r8 = rdx + r9;
    if ((bpl & 4) != 0) {
        r8 = r9;
        edx = 0;
    }
    ebp &= 8;
    if (ebp != 0) {
        goto label_14;
    }
    r8 += r12;
label_2:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_4;
    }
    rbp = rbx + rax - 1;
    rdi = rbx;
    if (rbx >= rbp) {
        goto label_15;
    }
    if (rdx != 0) {
        goto label_16;
    }
    goto label_15;
    do {
        if (rbp <= rdi) {
            goto label_15;
        }
label_16:
        rdi++;
        rax = rbx;
        *((rdi - 1)) = 0x20;
        rax -= rdi;
        rax += rdx;
    } while (rax != 0);
label_15:
    rdx = rbp;
    rsi = r13;
    *((rsp + 8)) = r8;
    rdx -= rdi;
    if (rdx > r9) {
        rdx = r9;
    }
    rax = mempcpy ();
    r8 = *((rsp + 8));
    rdx = rax;
    if (rbp <= rax) {
        goto label_17;
    }
    if (r12 != 0) {
        goto label_18;
    }
    goto label_17;
    do {
        if (rbp <= rdx) {
            goto label_17;
        }
label_18:
        rdx++;
        rcx = r12;
        *((rdx - 1)) = 0x20;
        rcx -= rdx;
        rcx += rax;
    } while (rcx != 0);
label_17:
    *(rdx) = 0;
label_4:
    *((rsp + 8)) = r8;
    free (r15);
    free (r14);
    rax = *((rsp + 8));
    return rax;
label_7:
    r14d = 0;
label_12:
    if (r12 >= rax) {
        goto label_19;
    }
    rax -= r12;
    rsi = *((rsp + 0x10));
    rdx = rax;
    rax = r12;
    *(rsi) = rax;
    eax = *((rsp + 0x24));
    if (eax != 0) {
        goto label_0;
    }
label_13:
    r12 = rdx;
    edx = 0;
    goto label_1;
label_14:
    r12d = 0;
    goto label_2;
label_11:
    *((rsp + 0x28)) = rax;
    rax = ctype_get_mb_cur_max ();
    r9 = *((rsp + 0x28));
    if (rax <= 1) {
        goto label_3;
    }
    rax = mbstowcs (0, r13, 0);
    r9 = *((rsp + 0x28));
    if (rax != -1) {
        goto label_20;
    }
    if ((bpl & 1) != 0) {
        goto label_3;
    }
label_10:
    r15d = 0;
    r14d = 0;
    r8 = 0xffffffffffffffff;
    goto label_4;
label_20:
    r8 = rax + 1;
    *((rsp + 0x30)) = r9;
    rax = r8*4;
    *((rsp + 0x28)) = r8;
    rdi = rax;
    *((rsp + 0x38)) = rax;
    rax = malloc (rdi);
    r8 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    r15 = rax;
    if (rax == 0) {
        goto label_21;
    }
    rdx = r8;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r8;
    r14d = 0;
    rax = mbstowcs (rax, r13, rdx);
    r9 = *((rsp + 0x30));
    r8 = *((rsp + 0x28));
    r12 = r9;
    if (rax == 0) {
        goto label_5;
    }
    rax = *((rsp + 0x38));
    *((r15 + rax - 4)) = 0;
    edi = *(r15);
    if (edi == 0) {
        goto label_22;
    }
    r12 = r15;
    do {
        *((rsp + 0x30)) = r8;
        *((rsp + 0x28)) = r9;
        eax = iswprint (rdi);
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x30));
        if (eax == 0) {
            *(r12) = 0xfffd;
            r14d = 1;
        }
        edi = *((r12 + 4));
        r12 += 4;
    } while (edi != 0);
    rsi = r8;
    rdi = r15;
    *((rsp + 0x28)) = r9;
    eax = wcswidth ();
    r9 = *((rsp + 0x28));
    r12 = (int64_t) eax;
    if (r14b == 0) {
        goto label_23;
    }
    *((rsp + 8)) = r9;
    rax = wcstombs (0, r15, 0);
    r9 = *((rsp + 8));
    rax++;
    *((rsp + 0x28)) = rax;
label_8:
    *((rsp + 8)) = r9;
    rax = malloc (*((rsp + 0x28)));
    r9 = *((rsp + 8));
    r14 = rax;
    if (rax == 0) {
        goto label_24;
    }
    rax = *((rsp + 0x10));
    edi = *(r15);
    r13 = r15;
    r12d = 0;
    rax = *(rax);
    *((rsp + 8)) = rax;
    if (edi != 0) {
        goto label_25;
    }
    goto label_26;
    do {
        rax = (int64_t) eax;
        rax += r12;
        if (*((rsp + 8)) < rax) {
            goto label_26;
        }
label_6:
        edi = *((r13 + 4));
        r13 += 4;
        r12 = rax;
        if (edi == 0) {
            goto label_26;
        }
label_25:
        eax = wcwidth ();
    } while (eax != 0xffffffff);
    eax = 1;
    *(r13) = 0xfffd;
    rax += r12;
    if (*((rsp + 8)) >= rax) {
        goto label_6;
    }
label_26:
    *(r13) = 0;
    rdi = r14;
    r13 = r14;
    rax = wcstombs (rdi, r15, *((rsp + 0x28)));
    r9 = rax;
    goto label_5;
label_22:
    rsi = r8;
    rdi = r15;
    *((rsp + 0x28)) = r9;
    eax = wcswidth ();
    r9 = *((rsp + 0x28));
    r12 = (int64_t) eax;
label_23:
    rax = *((rsp + 0x10));
    rax = *(rax);
    if (rax >= r12) {
        goto label_7;
    }
    rax = *((rsp + 8));
    rax++;
    *((rsp + 0x28)) = rax;
    goto label_8;
label_24:
    r8 |= 0xffffffffffffffff;
    if ((bpl & 1) != 0) {
        goto label_5;
    }
    goto label_4;
label_19:
    rax = r12;
    edx = 0;
    goto label_9;
label_21:
    if ((bpl & 1) == 0) {
        goto label_10;
    }
    r12 = r9;
    r14d = 0;
    goto label_5;
}

/* /tmp/tmp6wcn29hf @ 0xf2d0 */
 
int64_t vasnprintf (void * arg1, void ** arg2, int64_t arg3, int64_t arg4) {
    int64_t var_418h;
    int64_t var_40ch;
    int64_t var_408h;
    void ** var_400h;
    int64_t var_3f8h;
    int64_t var_3f0h;
    void * s2;
    void * var_3e0h;
    int64_t var_3d8h;
    void ** var_3d0h;
    void ** var_3c8h;
    int64_t var_3bch;
    int64_t var_3b8h;
    int64_t var_3b4h;
    int64_t var_3b0h;
    void ** var_3a8h;
    int64_t var_3a0h;
    uint32_t var_2c0h;
    void ** var_2b8h;
    int64_t var_2b0h;
    int64_t var_2a8h;
    int64_t var_2a0h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    int64_t var_fh;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
label_0:
    abort ();
    r14 = section__gnu_hash;
    r13 = rdx;
    r12 = rcx;
    *((rbp - 0x3e8)) = rdi;
    *((rbp - 0x400)) = rsi;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = printf_parse (r13, rbp - 0x2c0, r14);
    if (eax < 0) {
        goto label_34;
    }
    eax = printf_fetchargs (r12, r14);
    if (eax < 0) {
        goto label_35;
    }
    rdx = *((rbp - 0x2b0));
    rax = rdx + 7;
    rdx = 0xffffffffffffffff;
    if (rdx >= 0xfffffffffffffff9) {
        rax = rdx;
    }
    rax += *((rbp - 0x2a8));
    if (rax < 0) {
        goto label_36;
    }
    rdi = rax;
    r8d = 0;
    rdi += 6;
    r8b = (rdi < 0) ? 1 : 0;
    if (rdi < 0) {
        goto label_36;
    }
    if (rdi <= 0xf9f) {
        goto label_37;
    }
    *((rbp - 0x3c8)) = r8;
    if (rdi == -1) {
        goto label_36;
    }
    rax = malloc (rdi);
    *((rbp - 0x3e0)) = rax;
    if (rax == 0) {
        goto label_36;
    }
    *((rbp - 0x408)) = rax;
    r8 = *((rbp - 0x3c8));
label_11:
    ebx = 0;
    if (*((rbp - 0x3e8)) != 0) {
        rax = *((rbp - 0x400));
        rbx = *(rax);
    }
    r14 = *((rbp - 0x2b8));
    r9 = r8;
    r8 = r13;
    *((rbp - section..dynsym)) = 0;
    r15 = *((rbp - 0x3e8));
    r13 = *(r14);
    if (r13 == r8) {
        goto label_38;
    }
label_15:
    r13 -= r8;
    r12 = r9;
    rax = 0xffffffffffffffff;
    r12 += r13;
    if (r12 < 0) {
        r12 = rax;
    }
    if (rbx >= r12) {
        goto label_39;
    }
    if (rbx == 0) {
        goto label_40;
    }
    if (rbx < 0) {
        goto label_41;
    }
    rbx += rbx;
label_17:
    if (rbx < r12) {
        rbx = r12;
    }
    if (rbx == -1) {
        goto label_41;
    }
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    if (r15 == *((rbp - 0x3e8))) {
        goto label_42;
    }
    rax = realloc (r15, rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
label_18:
    *((rbp - 0x3c8)) = r10;
    memcpy (r10 + r9, r8, r13);
    r10 = *((rbp - 0x3c8));
label_16:
    rax = *((rbp - section..dynsym));
    if (*((rbp - 0x2c0)) == rax) {
        goto label_43;
    }
    edx = *((r14 + 0x48));
    rax = *((r14 + 0x50));
    if (dl == 0x25) {
        goto label_44;
    }
    if (rax == -1) {
        void (*0x3bc6)() ();
    }
    r15 = *((rbp - 0x3a8));
    rax <<= 5;
    rax += r15;
    ecx = *(rax);
    *((rbp - 0x3c8)) = ecx;
    if (dl == 0x6e) {
        goto label_45;
    }
    rcx = *((rbp - 0x3e0));
    eax = *((r14 + 0x10));
    *((rbp - 0x3b8)) = 0;
    *(rcx) = 0x25;
    r13 = rcx + 1;
    if ((al & 1) != 0) {
        rdx = *((rbp - 0x3e0));
        *((rdx + 1)) = 0x27;
        r13 = rdx + 2;
    }
    if ((al & 2) != 0) {
        *(r13) = 0x2d;
        r13++;
    }
    if ((al & 4) != 0) {
        *(r13) = 0x2b;
        r13++;
    }
    if ((al & 8) != 0) {
        *(r13) = 0x20;
        r13++;
    }
    if ((al & 0x10) != 0) {
        *(r13) = 0x23;
        r13++;
    }
    if ((al & 0x40) != 0) {
        *(r13) = 0x49;
        r13++;
    }
    if ((al & 0x20) != 0) {
        *(r13) = 0x30;
        r13++;
    }
    rsi = *((r14 + 0x18));
    rax = *((r14 + 0x20));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    rsi = *((r14 + 0x30));
    rax = *((r14 + 0x38));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    eax = *((rbp - 0x3c8));
    eax -= 7;
    if (eax > 9) {
        goto label_12;
    }
    rcx = 0x00013c4c;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (10 cases) at 0x13c4c */
    void (*rax)() ();
label_36:
    errno_location ();
    *(rax) = 0xc;
    do {
label_4:
        rdi = *((rbp - 0x2b8));
        rax = rbp - 0x2a0;
        if (rdi != rax) {
            free (rdi);
        }
        rdi = *((rbp - 0x3a8));
        rax = rbp - 0x3a0;
        if (rdi != rax) {
            free (rdi);
        }
label_34:
        r10d = 0;
label_31:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_46;
        }
        rsp = rbp - 0x28;
        rax = r10;
        return rax;
label_35:
        errno_location ();
        *(rax) = 0x16;
    } while (1);
    *(r13) = 0x6c;
    r13++;
    *(r13) = 0x6c;
    r13++;
label_12:
    eax = *((r14 + 0x48));
    *((r13 + 1)) = 0;
    *(r13) = al;
    rax = *((r14 + 0x28));
    if (rax == -1) {
        goto label_47;
    }
    rax <<= 5;
    rax += r15;
    if (*(rax) != 5) {
        void (*0x3bc6)() ();
    }
    *((rbp - 0x3d8)) = 1;
    eax = *((rax + 0x10));
    *((rbp - 0x3b8)) = eax;
label_21:
    rax = *((r14 + 0x40));
    if (rax == -1) {
        goto label_48;
    }
    rax <<= 5;
    rcx = r15 + rax;
    if (*(rcx) != 5) {
        void (*0x3bc6)() ();
    }
    eax = *((rbp - 0x3d8));
    edx = *((rcx + 0x10));
    *((rbp + rax*4 - 0x3b8)) = edx;
    eax = rax + 1;
    *((rbp - 0x3d8)) = eax;
label_48:
    rax = r12;
    rax += 2;
    if (rax < 0) {
        goto label_49;
    }
    if (rbx >= rax) {
        goto label_50;
    }
    if (rbx != 0) {
        goto label_51;
    }
    if (rax > 0xc) {
        goto label_52;
    }
    ebx = 0xc;
label_26:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_53;
    }
    rdi = r10;
    *((rbp - 0x3d0)) = r10;
    rax = realloc (rdi, rbx);
    r10 = *((rbp - 0x3d0));
    r15 = rax;
    if (rax == 0) {
        goto label_25;
    }
label_22:
    *((r15 + r12)) = 0;
    rax = errno_location ();
    *((rbp - 0x3f0)) = r13;
    *((rbp - 0x3d0)) = rax;
    eax = *(rax);
    *((rbp - 0x40c)) = eax;
label_1:
    rax = *((rbp - 0x3d0));
    r13 = rbx;
    esi = 0x7fffffff;
    *((rbp - 0x3bc)) = 0xffffffff;
    r13 -= r12;
    *(rax) = 0;
    eax = *((rbp - 0x3c8));
    if (r13 <= rsi) {
        rsi = r13;
    }
    if (eax > 0x11) {
        goto label_0;
    }
    rdi = 0x00013c74;
    rax = *((rdi + rax*4));
    rax += rdi;
    /* switch table (18 cases) at 0x13c74 */
    void (*rax)() ();
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_54;
    }
    if (eax == 2) {
        goto label_55;
    }
    rax = rbp - 0x3bc;
label_5:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    rsi = *((rbp - 0x418));
label_6:
    edx = *((rbp - 0x3bc));
    if (edx < 0) {
        goto label_56;
    }
label_2:
    rcx = (int64_t) edx;
    if (rcx >= rsi) {
        goto label_57;
    }
    rcx += r15;
    if (*((rcx + r12)) != 0) {
        void (*0x3bc6)() ();
    }
label_57:
    if (edx < eax) {
label_3:
        *((rbp - 0x3bc)) = eax;
        edx = eax;
    }
    eax = rdx + 1;
    if (rax < rsi) {
        goto label_58;
    }
    if (r13 > 0x7ffffffe) {
        goto label_59;
    }
    if (rbx < 0) {
        goto label_60;
    }
    eax = rdx + 2;
    rcx = rbx + rbx;
    rax += r12;
    if (rax < 0) {
        goto label_23;
    }
    if (rax < rcx) {
        rax = rcx;
    }
    if (rbx >= rax) {
        goto label_1;
    }
    if (rcx >= rax) {
        rax = rcx;
    }
    rbx = rax;
    if (rax == -1) {
        goto label_23;
    }
    if (r15 == *((rbp - 0x3e8))) {
        goto label_61;
    }
    rax = realloc (r15, rax);
    if (rax == 0) {
        goto label_23;
    }
    r15 = rax;
    goto label_1;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_10;
    }
label_7:
    if (eax == 2) {
        goto label_62;
    }
label_9:
    rax = rbp - 0x3bc;
label_8:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    edx = *((rbp - 0x3bc));
    rsi = *((rbp - 0x418));
    if (edx >= 0) {
        goto label_2;
    }
label_56:
    rcx = *((rbp - 0x3f0));
    if (*((rcx + 1)) != 0) {
        goto label_63;
    }
    if (eax >= 0) {
        goto label_3;
    }
    rax = *((rbp - 0x3d0));
    eax = *(rax);
    if (eax == 0) {
        eax = *((r14 + 0x48));
        edx = 0x54;
        rbx = *((rbp - 0x3d0));
        eax &= 0xffffffef;
        eax = 0x16;
        if (al == 0x63) {
            eax = edx;
        }
        *(rbx) = eax;
    }
label_20:
    if (r15 != *((rbp - 0x3e8))) {
        free (r15);
    }
    rax = *((rbp - 0x408));
    if (rax == 0) {
        goto label_4;
    }
    free (rax);
    goto label_4;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    *(fp_stack--) = fp_stack[?];
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_64;
    }
    if (eax == 2) {
        goto label_65;
    }
    r9 = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_5;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    xmm0 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_66;
    }
    if (eax == 2) {
        goto label_67;
    }
    r8 = *((rbp - 0x3e0));
    edx = 1;
    eax = 1;
    r9 = rbp - 0x3bc;
    rcx = 0xffffffffffffffff;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    do {
label_10:
        rax = rbp - 0x3bc;
label_14:
        r9d = *((rbp - 0x3b8));
        goto label_8;
        rax = *((r14 + 0x50));
        rdi = r15 + r12;
        rax <<= 5;
        rax += *((rbp - 0x3a8));
        r9d = *((rax + 0x10));
        eax = *((rbp - 0x3d8));
    } while (eax == 1);
    if (eax != 2) {
        goto label_9;
    }
label_62:
    rax = rbp - 0x3bc;
label_13:
    eax = *((rbp - 0x3b4));
label_24:
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    eax = 0;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
label_37:
    rax += 0x1d;
    rcx = rsp;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_68;
    }
    do {
    } while (rsp != rcx);
label_68:
    edx &= 0xfff;
    if (rdx != 0) {
        goto label_69;
    }
label_29:
    *((rbp - 0x408)) = 0;
    rax = rsp + 0xf;
    rax &= 0xfffffffffffffff0;
    *((rbp - 0x3e0)) = rax;
    goto label_11;
    *(r13) = 0x4c;
    r13++;
    goto label_12;
label_60:
    if (rbx == -1) {
        goto label_1;
    }
    goto label_23;
label_61:
    rax = malloc (rax);
    if (rax == 0) {
        goto label_23;
    }
    if (r12 == 0) {
        goto label_70;
    }
    rax = memcpy (rax, r15, r12);
    r15 = rax;
    goto label_1;
label_55:
    rax = rbp - 0x3bc;
    goto label_13;
label_54:
    rax = rbp - 0x3bc;
    goto label_14;
label_63:
    *((rcx + 1)) = 0;
    goto label_1;
label_44:
    if (rax != -1) {
        void (*0x3bc6)() ();
    }
    r15 = r12 + 1;
    r9 = 0xffffffffffffffff;
    if (r12 < -1) {
        r9 = r15;
    }
    if (rbx < r9) {
        if (rbx == 0) {
            goto label_71;
        }
        if (rbx < 0) {
            goto label_25;
        }
        rbx += rbx;
label_27:
        if (rbx < r9) {
            rbx = r9;
        }
        if (rbx == -1) {
            goto label_25;
        }
        if (r10 == *((rbp - 0x3e8))) {
            goto label_72;
        }
        rdi = r10;
        *((rbp - 0x3d0)) = r9;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, rbx);
        r9 = *((rbp - 0x3d0));
        if (rax == 0) {
            goto label_30;
        }
        r10 = rax;
    }
label_28:
    *((r10 + r12)) = 0x25;
    r15 = r10;
label_19:
    r8 = *((r14 + 8));
    r13 = *((r14 + 0x58));
    r14 += 0x58;
    *((rbp - section..dynsym))++;
    if (r13 != r8) {
        goto label_15;
    }
label_38:
    r12 = r9;
    r10 = r15;
    goto label_16;
label_40:
    ebx = 0xc;
    goto label_17;
label_39:
    r10 = r15;
    goto label_18;
label_45:
    r13d = ecx;
    r13d -= 0x12;
    if (r13d > 4) {
        goto label_0;
    }
    rcx = 0x00013cbc;
    rax = *((rax + 0x10));
    rdx = *((rcx + r13*4));
    rdx += rcx;
    /* switch table (5 cases) at 0x13cbc */
    void (*rdx)() ();
    *(rax) = r12;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12d;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12w;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12b;
    r9 = r12;
    r15 = r10;
    goto label_19;
label_49:
    if (rbx == -1) {
        goto label_50;
    }
label_25:
    *((rbp - 0x3c8)) = r10;
label_30:
    rax = errno_location ();
    r15 = *((rbp - 0x3c8));
    *((rbp - 0x3d0)) = rax;
label_23:
    rax = *((rbp - 0x3d0));
    *(rax) = 0xc;
    goto label_20;
label_47:
    *((rbp - 0x3d8)) = 0;
    goto label_21;
label_42:
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
    if (r9 == 0) {
        goto label_18;
    }
    rdx = r9;
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, *((rbp - 0x3e8)), rdx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    goto label_18;
label_50:
    r15 = r10;
    goto label_22;
label_41:
    rax = errno_location ();
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_65:
    rax = rbp - 0x3bc;
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    eax = *((rbp - 0x3b4));
    rcx = 0xffffffffffffffff;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
label_64:
    rax = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_24;
label_67:
    rax = rbp - 0x3bc;
    eax = *((rbp - 0x3b4));
    do {
        r9d = *((rbp - 0x3b8));
        edx = 1;
        eax = 1;
        r8 = *((rbp - 0x3e0));
        rcx = 0xffffffffffffffff;
        *((rbp - 0x418)) = rsi;
        snprintf_chk ();
        rsi = *((rbp - 0x418));
        goto label_6;
label_66:
        rax = rbp - 0x3bc;
    } while (1);
    if (r9 < 0) {
label_51:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= rax) {
        goto label_26;
    }
label_52:
    if (rax == -1) {
        goto label_25;
    }
    rbx = rax;
    goto label_26;
label_58:
    rax = (int64_t) edx;
    edx = *((rbp - 0x40c));
    r9 = rax + r12;
    rax = *((rbp - 0x3d0));
    *(rax) = edx;
    goto label_19;
label_59:
    rax = *((rbp - 0x3d0));
    *(rax) = 0x4b;
    goto label_20;
label_71:
    ebx = 0xc;
    goto label_27;
label_53:
    rax = malloc (rbx);
    r15 = rax;
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_22;
    }
    memcpy (rax, *((rbp - 0x3e8)), r12);
    goto label_22;
label_72:
    *((rbp - 0x3d0)) = r10;
    *((rbp - 0x3c8)) = r9;
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r10 = *((rbp - 0x3d0));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_74;
    }
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, r10, r12);
    r9 = *((rbp - 0x3c8));
    r10 = rax;
    goto label_28;
label_69:
    goto label_29;
label_43:
    r13 = r12;
    r13++;
    if (r13 < 0) {
        goto label_75;
    }
    if (rbx >= r13) {
        goto label_33;
    }
    if (rbx != 0) {
        goto label_76;
    }
    if (r13 > 0xc) {
        goto label_77;
    }
    ebx = 0xc;
label_32:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_78;
    }
    rdi = r10;
    *((rbp - 0x3c8)) = r10;
    rax = realloc (rdi, rbx);
    r10 = rax;
    if (rax == 0) {
        goto label_30;
    }
label_33:
    *((r10 + r12)) = 0;
    if (rbx > r13) {
        if (r10 == *((rbp - 0x3e8))) {
            goto label_79;
        }
        rdi = r10;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, r13);
        r10 = *((rbp - 0x3c8));
        if (rax == 0) {
            r10 = rax;
            goto label_79;
        }
    }
label_79:
    rdi = *((rbp - 0x408));
    if (rdi != 0) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x2b8));
    rax = rbp - 0x2a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x3a8));
    rax = rbp - 0x3a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rax = *((rbp - 0x400));
    *(rax) = r12;
    goto label_31;
    if (rdi < rax) {
label_76:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= r13) {
        goto label_32;
    }
label_77:
    if (r13 == -1) {
        goto label_25;
    }
    rbx = r13;
    goto label_32;
label_78:
    *((rbp - 0x3c8)) = r10;
    rax = malloc (rbx);
    r10 = *((rbp - 0x3c8));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_80;
    }
    rax = memcpy (rax, r10, r12);
    r10 = rax;
    goto label_33;
label_46:
    stack_chk_fail ();
label_75:
    if (rbx == -1) {
        goto label_33;
    }
    goto label_25;
label_73:
    rax = errno_location ();
    r15 = *((rbp - 0x3e8));
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_70:
    r15 = rax;
    goto label_1;
label_74:
    r10 = rax;
    goto label_28;
label_80:
    r10 = rax;
    goto label_33;
}

/* /tmp/tmp6wcn29hf @ 0x10270 */
 
int64_t dbg_printf_fetchargs (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int printf_fetchargs(__va_list_tag * args,arguments * a); */
    r8 = *(rsi);
    rax = *((rsi + 8));
    rcx = rdi;
    if (r8 == 0) {
        goto label_6;
    }
    esi = 0;
    rdi = 0x00013ce0;
    r10 = "(NULL)";
    r9 = "(NULL)";
    do {
        if (*(rax) > 0x16) {
            goto label_7;
        }
        edx = *(rax);
        rdx = *((rdi + rdx*4));
        rdx += rdi;
        /* switch table (23 cases) at 0x13ce0 */
        rax = void (*rdx)() ();
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_8;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_1:
        rdx = *(r11);
        *((rax + 0x10)) = rdx;
label_0:
        rsi++;
        rax += 0x20;
    } while (rsi != r8);
label_6:
    eax = 0;
    return rax;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_9;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        edx = *(r11);
        *((rax + 0x10)) = edx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_10;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_2:
        edx = *(r11);
        *((rax + 0x10)) = dx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_11;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_3:
        edx = *(r11);
        *((rax + 0x10)) = dl;
        goto label_0;
label_8:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_1;
label_9:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
    edx = *((rcx + 4));
    if (edx > 0xaf) {
        goto label_12;
    }
    r11d = edx;
    edx += 0x10;
    r11 += *((rcx + 0x10));
    *((rcx + 4)) = edx;
label_4:
    xmm0 = *(r11);
    *((rax + 0x10)) = xmm0;
    goto label_0;
    rdx = *((rcx + 8));
    rdx += 0xf;
    rdx &= 0xfffffffffffffff0;
    r11 = rdx + 0x10;
    *((rcx + 8)) = r11;
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_13;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
label_5:
    rdx = *(r11);
    if (rdx == 0) {
        rdx = r10;
    }
    *((rax + 0x10)) = rdx;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_14;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        rdx = *(r11);
        if (rdx == 0) {
            rdx = r9;
        }
        *((rax + 0x10)) = rdx;
        goto label_0;
label_10:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_2;
label_11:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_3;
label_14:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
label_12:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_4;
label_13:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_5;
label_7:
    eax |= 0xffffffff;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x7d80 */
 
int64_t hash_get_n_buckets (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x10));
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xc7a0 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0xac40 */
 
void free_mount_entry (void * arg_8h, void * arg_10h, void * arg_18h, int64_t arg_28h, void ** ptr) {
    rdi = ptr;
    free (*(rdi));
    free (*((rbp + 8)));
    free (*((rbp + 0x10)));
    if ((*((rbp + 0x28)) & 4) == 0) {
        rdi = rbp;
        void (*0x35f0)() ();
    }
    free (*((rbp + 0x18)));
    rdi = rbp;
    return free ();
}

/* /tmp/tmp6wcn29hf @ 0x80b0 */
 
uint64_t hash_get_next (int64_t arg_8h, uint32_t arg_10h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rsi = *((rdi + 0x10));
    rdi = rbx;
    rax = uint64_t (*rbp + 0x30)(uint64_t) (rbx);
    if (rax >= *((rbp + 0x10))) {
        void (*0x3b75)() ();
    }
    rax <<= 4;
    rax += *(rbp);
    rdx = rax;
    while (rcx != rbx) {
        if (rdx == 0) {
            goto label_0;
        }
        rcx = *(rdx);
        rdx = *((rdx + 8));
    }
    if (rdx != 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 8));
    while (rdx > rax) {
        r8 = *(rax);
        if (r8 != 0) {
            goto label_2;
        }
        rax += 0x10;
    }
    r8d = 0;
label_2:
    rax = r8;
    return rax;
label_1:
    r8 = *(rdx);
    rax = r8;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x3bd0 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    size_t width;
    stat buf;
    size_t var_8h;
    int64_t var_10h;
    char * s1;
    char * format;
    uint32_t var_28h;
    size_t var_30h;
    size_t var_38h;
    uint32_t var_40h;
    uint32_t var_48h;
    int64_t var_50h;
    uint32_t var_58h;
    int64_t var_66h;
    int64_t var_67h;
    void * ptr;
    int64_t var_70h;
    uint32_t var_90h;
    int64_t var_128h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15 = obj_output_block_size;
    r14d = 0;
    r13 = "aB:iF:hHklmPTt:vx:";
    r12 = 0x000120ec;
    rbx = 0x00012308;
    *((rsp + 8)) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x128)) = rax;
    rax = rsi;
    set_program_name (*(rax), rsi, rdx);
    setlocale (6, 0x000136c1);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = obj_long_options;
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    edx = 5;
    *(obj.fs_select_list) = 0;
    *(obj.fs_exclude_list) = 0;
    *(obj.show_all_fs) = 0;
    *(obj.show_listed_fs) = 0;
    *(obj.human_output_opts) = 0xffffffff;
    *(obj.print_type) = 0;
    *(obj.file_systems_processed) = 0;
    *(obj.exit_status) = 0;
    *(obj.print_grand_total) = 0;
    *(obj.grand_fsu) = 1;
    rax = dcgettext (0, "options %s and %s are mutually exclusive");
    *((rsp + 0x20)) = rax;
    rax = rsp + 0x70;
    *((rsp + 0x10)) = rax;
    do {
label_0:
        r8 = *((rsp + 0x10));
        rsi = *((rsp + 8));
        rcx = r12;
        rdx = r13;
        edi = ebp;
        *((rsp + 0x70)) = 0xffffffff;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_44;
        }
        if (eax > 0x83) {
            goto label_45;
        }
        if (eax <= 0x41) {
            goto label_46;
        }
        eax -= 0x42;
        if (eax > 0x41) {
            goto label_45;
        }
        rax = *((rbx + rax*4));
        rax += rbx;
        /* switch table (66 cases) at 0x12308 */
        void (*rax)() ();
        rdx = optarg;
        *((rsp + 0x18)) = rdx;
        xmalloc (0x10);
        rdx = *((rsp + 0x18));
        *(rax) = rdx;
        rdx = fs_select_list;
        *(obj.fs_select_list) = rax;
        *((rax + 8)) = rdx;
    } while (1);
    eax = human_options (*(obj.optarg), obj.human_output_opts, r15);
    if (eax == 0) {
        goto label_0;
    }
    r8 = optarg;
    esi = *((rsp + 0x70));
    rcx = r12;
    edi = eax;
    edx = 0x42;
    rax = xstrtol_fatal ();
    eax = header_mode;
    if (eax == 1) {
        goto label_47;
    }
    if (eax == 0) {
        if (r14b != 0) {
            goto label_48;
        }
    }
    if (*(obj.print_type) != 0) {
        goto label_49;
    }
    rdi = optarg;
    *(obj.header_mode) = 4;
    if (rdi == 0) {
        goto label_0;
    }
    decode_output_arg ();
    goto label_0;
    *(obj.print_grand_total) = 1;
    goto label_0;
    *(obj.require_sync) = 1;
    goto label_0;
    *(obj.require_sync) = 0;
    goto label_0;
    rdx = optarg;
    *((rsp + 0x18)) = rdx;
    xmalloc (0x10);
    rdx = *((rsp + 0x18));
    *(rax) = rdx;
    rdx = fs_exclude_list;
    *(obj.fs_exclude_list) = rax;
    *((rax + 8)) = rdx;
    goto label_0;
    *(obj.human_output_opts) = 0;
    *(obj.output_block_size) = 0x100000;
    goto label_0;
    *(obj.show_local_fs) = 1;
    goto label_0;
    *(obj.human_output_opts) = 0;
    *(obj.output_block_size) = 0x400;
    goto label_0;
    if (*(obj.header_mode) == 4) {
        goto label_47;
    }
    *(obj.header_mode) = 1;
    goto label_0;
    *(obj.human_output_opts) = 0xb0;
    *(obj.output_block_size) = 1;
    goto label_0;
    *(obj.show_all_fs) = 1;
    goto label_0;
    if (*(obj.header_mode) == 4) {
        goto label_49;
    }
    *(obj.print_type) = 1;
    goto label_0;
    if (*(obj.header_mode) == 4) {
        goto label_50;
    }
    r14d = 1;
    goto label_0;
    *(obj.human_output_opts) = 0x90;
    *(obj.output_block_size) = 1;
    goto label_0;
label_46:
    if (eax == 0xffffff7d) {
        rax = "Paul Eggert";
        eax = 0;
        version_etc (*(obj.stdout), 0x0001206c, "GNU coreutils", *(obj.Version), "Torbjorn Granlund", "David MacKenzie");
        eax = exit (0);
    }
    if (eax != 0xffffff7e) {
        goto label_45;
    }
    usage (0);
label_44:
    if (*(obj.human_output_opts) == 0xffffffff) {
        goto label_51;
    }
label_2:
    eax = header_mode;
    if (eax != 1) {
        if (eax == 4) {
            goto label_3;
        }
        if ((*(obj.human_output_opts) & 0x10) == 0) {
            goto label_52;
        }
        *(obj.header_mode) = 2;
    }
label_3:
    rbx = fs_select_list;
    if (rbx == 0) {
        goto label_53;
    }
    r12d = 0;
    r13 = "file system type %s both selected and excluded";
label_1:
    r14 = fs_exclude_list;
    if (r14 == 0) {
        goto label_54;
    }
    r15 = *(rbx);
    while (eax != 0) {
        r14 = *((r14 + 8));
        if (r14 == 0) {
            goto label_54;
        }
        eax = strcmp (r15, *(r14));
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, r13);
    rcx = r12;
    eax = 0;
    r12d = 1;
    error (0, 0, rax);
label_54:
    rbx = *((rbx + 8));
    if (rbx != 0) {
        goto label_1;
    }
    if (r12b == 0) {
label_53:
        *((rsp + 0x40)) = 0;
        eax = optind;
        if (eax < ebp) {
            goto label_55;
        }
label_5:
        edi = 1;
        if (*(obj.fs_select_list) == 0) {
            goto label_56;
        }
label_8:
        rax = read_file_system_list (rdi, rsi, rdx, rcx);
        *(obj.mount_list) = rax;
        if (rax == 0) {
            goto label_57;
        }
label_40:
        if (*(obj.require_sync) != 0) {
            goto label_58;
        }
label_4:
        if (*(obj.header_mode) > 4) {
            goto label_59;
        }
        eax = header_mode;
        rdx = 0x00012410;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (5 cases) at 0x12410 */
        void (*rax)() ();
    }
    rax = *((rsp + 0x128));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_60;
    }
    eax = 1;
    return rax;
label_51:
    if (r14b == 0) {
        goto label_61;
    }
    *(obj.human_output_opts) = 0;
    rax = getenv ("POSIXLY_CORRECT");
    rax -= rax;
    eax &= 0x200;
    rax += 0x200;
    *(obj.output_block_size) = rax;
    goto label_2;
label_52:
    if (r14b == 0) {
        goto label_3;
    }
    *(obj.header_mode) = 3;
    goto label_3;
label_58:
    eax = sync ();
    goto label_4;
label_55:
    edi -= eax;
    rdi = (int64_t) edi;
    rax = xnmalloc (ebp, 0x90);
    rbx = *(obj.optind);
    r12 = *((rsp + 8));
    *((rsp + 0x40)) = rax;
    goto label_62;
label_6:
    rax = errno_location ();
    r13d = *(rax);
    *((rsp + 0x18)) = rax;
    if (r13d != 2) {
        if (r13d == 0x14) {
            goto label_7;
        }
        rsi = r14;
        rdi = r15;
        eax = stat ();
        if (eax == 0) {
            goto label_63;
        }
        rdx = *((rsp + 0x18));
        r13d = *(rdx);
    }
label_7:
    if (r13d != 0) {
        rdx = *((r12 + rbx*8));
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        rcx = rax;
        eax = 0;
        error (0, r13d, 0x00013bba);
        *((r12 + rbx*8)) = 0;
        *(obj.exit_status) = 1;
    }
label_63:
    rbx++;
label_62:
    if (ebp <= ebx) {
        goto label_5;
    }
    eax = ebx;
    rcx = *((rsp + 0x40));
    r15 = *((r12 + rbx*8));
    eax -= *(obj.optind);
    rax = (int64_t) eax;
    rax *= 0x90;
    r14 = rcx + rax;
    eax = 0;
    eax = open (r15, 0x900, rdx);
    edi = eax;
    if (eax < 0) {
        goto label_6;
    }
    *((rsp + 0x18)) = eax;
    eax = fstat (rdi, r14);
    edi = *((rsp + 0x18));
    r13d = eax;
    if (eax != 0) {
        rax = errno_location ();
        r13d = *(rax);
    }
    eax = close (*((rsp + 0x18)));
    goto label_7;
label_56:
    if (*(obj.fs_exclude_list) != 0) {
        goto label_8;
    }
    if (*(obj.print_type) != 0) {
        goto label_8;
    }
    if (*(0x0001807c) != 0) {
        goto label_8;
    }
    edi = *(obj.show_local_fs);
    goto label_8;
label_50:
    r8 = "--output";
    rcx = 0x0001215a;
label_43:
    eax = 0;
    error (0, 0, *((rsp + 0x20)));
label_45:
    usage (1);
label_61:
    rax = getenv ("DF_BLOCK_SIZE");
    human_options (rax, obj.human_output_opts, obj.output_block_size);
    goto label_2;
    alloc_field (0, 0);
    if (*(obj.print_type) != 0) {
        goto label_64;
    }
label_30:
    alloc_field (2, 0);
    alloc_field (3, 0);
    alloc_field (4, 0);
label_11:
    alloc_field (5, "Capacity");
    alloc_field (0xa, 0);
label_12:
    get_header ();
    if (*((rsp + 0x40)) == 0) {
        goto label_65;
    }
    edx = optind;
    *(obj.show_listed_fs) = 1;
    if (ebp <= edx) {
        goto label_66;
    }
    rax = (int64_t) edx;
    ebp -= edx;
    rcx = rax + 1;
    edx = rbp - 1;
    rdx += rcx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x48)) = rdx;
label_13:
    rcx = *((rsp + 8));
    rcx = *((rcx + rax*8));
    *((rsp + 0x30)) = rcx;
    rbx = rcx;
    if (rcx == 0) {
        goto label_18;
    }
    eax -= *(obj.optind);
    rax = (int64_t) eax;
    rax *= 0x90;
    rax += *((rsp + 0x40));
    *((rsp + 0x50)) = rax;
    eax = *((rax + 0x18));
    *((rsp + 0x18)) = eax;
    eax &= 0xb000;
    if (eax != 0x2000) {
        goto label_28;
    }
    rdi = rcx;
    rax = canonicalize_file_name ();
    *((rsp + 0x68)) = rax;
    if (rax == 0) {
        goto label_67;
    }
    if (*(rax) != 0x2f) {
        rax = rbx;
    }
    *((rsp + 0x18)) = rax;
label_42:
    rbx = mount_list;
    if (rbx == 0) {
        goto label_68;
    }
    *((rsp + 0x58)) = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0xffffffffffffffff;
    *((rsp + 0x20)) = 0;
label_9:
    r12 = *(rbx);
    rdi = *(rbx);
    rax = canonicalize_file_name ();
    if (rax != 0) {
        if (*(rax) != 0x2f) {
            r12 = rax;
            goto label_69;
        }
    }
label_69:
    eax = strcmp (*((rsp + 0x18)), r12);
    if (eax != 0) {
        goto label_70;
    }
    r14 = mount_list;
    r13 = *((rbx + 8));
    if (r14 == 0) {
        goto label_71;
    }
    r15d = 0;
    do {
        eax = strcmp (*((r14 + 8)), r13);
        if (eax == 0) {
            r15 = r14;
        }
        r14 = *((r14 + 0x30));
    } while (r14 != 0);
    if (r15 != 0) {
        rdi = *(r15);
        rax = canonicalize_file_name ();
        r14 = rax;
        if (rax != 0) {
            if (*(rax) == 0x2f) {
                goto label_72;
            }
        }
        free (r14);
        rax = xstrdup (*(r15));
        r14 = rax;
label_72:
        eax = strcmp (r14, r12);
        if (eax != 0) {
            goto label_73;
        }
        r13 = *((rbx + 8));
    }
label_71:
    rax = strlen (*((rbx + 8)));
    edx = *((rsp + 0x20));
    r12 = rax;
    edx ^= 1;
    cl = (rax < *((rsp + 0x28))) ? 1 : 0;
    r15d = edx;
    r15b |= cl;
    if (r15b != 0) {
        goto label_74;
    }
    *((rsp + 0x20)) = 1;
label_14:
    free (r14);
label_70:
    free (rbp);
    rbx = *((rbx + 0x30));
    if (rbx != 0) {
        goto label_9;
    }
    free (*((rsp + 0x68)));
    if (*((rsp + 0x58)) != 0) {
        goto label_75;
    }
    if (r15b != 0) {
        goto label_76;
    }
label_28:
    rdi = *((rsp + 0x30));
    rax = canonicalize_file_name ();
    r15 = mount_list;
    rdi = rax;
    if (rax != 0) {
        if (*(rax) == 0x2f) {
            goto label_77;
        }
    }
    free (rdi);
    if (r15 == 0) {
        goto label_78;
    }
label_16:
    r12 = *((rsp + 0x50));
    ebx = 0;
    rbp = rsp + 0x90;
    while (*(r12) != rax) {
label_10:
        r15 = *((r15 + 0x30));
        if (r15 == 0) {
            goto label_79;
        }
        rax = *((r15 + 0x20));
        if (rax == -1) {
            goto label_80;
        }
label_15:
    }
    eax = strcmp (*((r15 + 0x18)), "lofs");
    if (eax == 0) {
        goto label_10;
    }
    if (rbx == 0) {
        goto label_81;
    }
    if ((*((rbx + 0x28)) & 1) != 0) {
        goto label_81;
    }
    if ((*((r15 + 0x28)) & 1) != 0) {
        goto label_10;
    }
label_81:
    rdi = *((r15 + 8));
    rsi = rbp;
    eax = stat ();
    if (eax == 0) {
        rax = *((r15 + 0x20));
        if (*((rsp + 0x90)) == rax) {
            goto label_82;
        }
    }
    *((r15 + 0x20)) = 0xfffffffffffffffe;
    goto label_10;
    alloc_field (0, 0);
    if (*(obj.print_type) != 0) {
        goto label_83;
    }
label_29:
    alloc_field (2, "Size");
    alloc_field (3, 0);
    alloc_field (4, "Avail");
    esi = 0;
    goto label_11;
    alloc_field (0, 0);
    if (*(obj.print_type) != 0) {
        goto label_84;
    }
label_31:
    alloc_field (6, 0);
    alloc_field (7, 0);
    alloc_field (8, 0);
    alloc_field (9, 0);
    alloc_field (0xa, 0);
    goto label_12;
    alloc_field (0, 0);
    if (*(obj.print_type) != 0) {
        goto label_85;
    }
label_32:
    alloc_field (2, 0);
    alloc_field (3, 0);
    alloc_field (4, 0);
    esi = 0;
    goto label_11;
    if (*(obj.ncolumns) != 0) {
        goto label_12;
    }
    rdi = "source,fstype,itotal,iused,iavail,ipcent,size,used,avail,pcent,file,target";
    decode_output_arg ();
    goto label_12;
label_79:
    if (rbx == 0) {
        goto label_78;
    }
label_17:
    r9d = *((rbx + 0x28));
    eax = r9d;
    r9d &= 1;
    al >>= 1;
    eax &= 1;
    rcx = *((rsp + 0x50));
    get_dev (*(rbx), *((rbx + 8)), *((rsp + 0x50)), *(rcx), *((rbx + 0x18)), r9);
label_18:
    rcx = *((rsp + 0x38));
    rax = *((rsp + 0x38));
    if (*((rsp + 0x48)) == rcx) {
        goto label_66;
    }
    rcx++;
    *((rsp + 0x38)) = rcx;
    goto label_13;
label_73:
    r15d = 1;
    goto label_14;
label_74:
    rsi = rsp + 0x90;
    rdi = r13;
    *((rsp + 0x67)) = cl;
    *((rsp + 0x66)) = dl;
    eax = stat ();
    if (eax != 0) {
        edx = *((rsp + 0x66));
        if (dl == 0) {
            goto label_86;
        }
        ecx = *((rsp + 0x67));
        if (cl == 0) {
            goto label_86;
        }
        r15d = 0;
    }
    if (r12 == 1) {
        goto label_87;
    }
    *((rsp + 0x28)) = r12;
    *((rsp + 0x58)) = rbx;
label_19:
    *((rsp + 0x20)) = r15b;
    r15d = 0;
    goto label_14;
label_80:
    rdi = *((r15 + 8));
    rsi = rbp;
    eax = stat ();
    if (eax == 0) {
        rax = *((rsp + 0x90));
        *((r15 + 0x20)) = rax;
        goto label_15;
    }
    rax = errno_location ();
    r13 = rax;
    if (*(rax) == 5) {
        goto label_88;
    }
label_21:
    *((r15 + 0x20)) = 0xfffffffffffffffe;
    rax = 0xfffffffffffffffe;
    goto label_15;
label_77:
    rax = strlen (rdi);
    if (r15 == 0) {
        goto label_89;
    }
    *((rsp + 0x18)) = r15;
    r13 = r15;
    r12d = 0;
    ebx = 0;
    r14 = rax;
    do {
        eax = strcmp (*((r13 + 0x18)), "lofs");
        if (eax != 0) {
            if (rbx != 0) {
                if ((*((rbx + 0x28)) & 1) != 0) {
                    goto label_90;
                }
                if ((*((r13 + 0x28)) & 1) != 0) {
                    goto label_20;
                }
            }
label_90:
            r15 = *((r13 + 8));
            rax = strlen (*((r13 + 8)));
            rdx = rax;
            if (rax < r12) {
                goto label_20;
            }
            if (r14 < rax) {
                goto label_20;
            }
            if (rax == 1) {
                goto label_91;
            }
            if (r14 == rax) {
                goto label_92;
            }
            if (*((rbp + rax)) == 0x2f) {
                goto label_92;
            }
        }
label_20:
        r13 = *((r13 + 0x30));
    } while (r13 != 0);
    r15 = *((rsp + 0x18));
    free (rbp);
    if (rbx == 0) {
        goto label_16;
    }
    rdi = *((rbx + 8));
    rsi = rsp + 0x90;
    eax = stat ();
    if (eax != 0) {
        goto label_16;
    }
    rax = *((rsp + 0x50));
    rax = *(rax);
    if (*((rsp + 0x90)) != rax) {
        goto label_16;
    }
    goto label_17;
label_22:
    if (*((rsp + 0x18)) != 0) {
        goto label_93;
    }
    *(obj.mount_list) = 0;
    rdi = rbp;
    while (rdi != 0) {
        rax = *((rdi + 8));
        rdx = mount_list;
        rbx = *((rdi + 0x10));
        *((rax + 0x30)) = rdx;
        *(obj.mount_list) = rax;
        free (rdi);
        rdi = rbx;
    }
    hash_free (*(obj.devlist_table), rsi);
    *(obj.devlist_table) = 0;
label_93:
    rbx = mount_list;
    while (rbx != 0) {
        r9d = *((rbx + 0x28));
        eax = r9d;
        r9d &= 1;
        al >>= 1;
        eax &= 1;
        get_dev (*(rbx), *((rbx + 8)), 0, 0, *((rbx + 0x18)), r9);
        rbx = *((rbx + 0x30));
    }
label_66:
    if (*(obj.file_systems_processed) != 0) {
        goto label_94;
    }
    if (*(obj.exit_status) == 0) {
        goto label_95;
    }
label_36:
    exit (*(obj.exit_status));
label_89:
    free (rbp);
label_78:
    rbx = *((rsp + 0x30));
    rax = find_mount_point (rbx, *((rsp + 0x50)));
    if (rax == 0) {
        goto label_18;
    }
    get_dev (0, rax, rbx, 0, 0, 0);
    free (rbp);
    goto label_18;
label_86:
    r15d = *((rsp + 0x20));
    goto label_19;
label_87:
    free (r14);
    free (rbp);
    free (*((rsp + 0x68)));
    *((rsp + 0x58)) = rbx;
label_75:
    rbx = *((rsp + 0x58));
    r9d = *((rbx + 0x28));
    eax = r9d;
    r9d &= 1;
    al >>= 1;
    eax &= 1;
    get_dev (*(rbx), *((rbx + 8)), *((rsp + 0x50)), 0, *((rbx + 0x18)), r9);
    goto label_18;
label_92:
    *((rsp + 0x20)) = rdx;
    eax = strncmp (r15, rbp, rdx);
    rdx = *((rsp + 0x20));
    if (eax == 0) {
        r12 = rdx;
    }
    if (eax == 0) {
        rbx = r13;
    }
    goto label_20;
label_91:
    r12d = 1;
    rbx = r13;
    goto label_20;
label_82:
    rbx = r15;
    goto label_10;
label_88:
    rdx = *((r15 + 8));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    rcx = rax;
    eax = 0;
    error (0, *(r13), 0x00013bba);
    *(obj.exit_status) = 1;
    goto label_21;
label_76:
    rsi = *((rsp + 0x30));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot access %s: over-mounted by another device");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    *(obj.exit_status) = 1;
    goto label_18;
label_65:
    eax = *(obj.show_all_fs);
    rbp = mount_list;
    *((rsp + 0x18)) = al;
    eax = 0;
    while (rbp != 0) {
        rbp = *((rbp + 0x30));
        eax++;
    }
    rdi = (int64_t) eax;
    rax = hash_initialize (rdi, 0, dbg.devlist_hash, dbg.devlist_compare, 0);
    rdx = rsp + 0x90;
    rbx = mount_list;
    *(obj.devlist_table) = rax;
    *((rsp + 0x20)) = rdx;
    if (rax == 0) {
        goto label_96;
    }
label_23:
    if (rbx == 0) {
        goto label_22;
    }
    eax = *((rbx + 0x28));
    if ((al & 2) != 0) {
        if (*(obj.show_local_fs) != 0) {
            goto label_25;
        }
    }
    if ((al & 1) != 0) {
        if (*(obj.show_all_fs) != 0) {
            goto label_97;
        }
        if (*(obj.show_listed_fs) == 0) {
            goto label_25;
        }
    }
label_97:
    r12 = fs_select_list;
    r13 = *((rbx + 0x18));
    if (r12 == 0) {
        goto label_98;
    }
    if (r13 == 0) {
        goto label_27;
    }
    do {
        eax = strcmp (r13, *(r12));
        if (eax == 0) {
            goto label_99;
        }
        r12 = *((r12 + 8));
    } while (r12 != 0);
    do {
label_25:
        rax = *((rbx + 0x20));
        *((rsp + 0x90)) = rax;
label_26:
        rax = xmalloc (0x20);
        r12 = rax;
        *((rax + 8)) = rbx;
        rax = *((rsp + 0x90));
        *((r12 + 0x10)) = rbp;
        rsi = r12;
        *(r12) = rax;
        rax = hash_insert (*(obj.devlist_table));
        if (rax == 0) {
            goto label_96;
        }
        *((rax + 0x18)) = r12;
        rbx = *((rbx + 0x30));
        goto label_23;
label_98:
        r12 = fs_exclude_list;
        if (r12 == 0) {
            goto label_27;
        }
        if (r13 == 0) {
            goto label_27;
        }
label_24:
        eax = strcmp (r13, *(r12));
    } while (eax == 0);
    r12 = *((r12 + 8));
    if (r12 != 0) {
        goto label_24;
    }
label_27:
    rdi = *((rbx + 8));
    rsi = *((rsp + 0x20));
    eax = stat ();
    eax++;
    if (eax == 0) {
        goto label_25;
    }
    rdi = devlist_table;
    rax = *((rsp + 0x90));
    if (rdi == 0) {
        goto label_26;
    }
    rsi = *((rsp + 0x10));
    *((rsp + 0x70)) = rax;
    rax = hash_lookup ();
    if (rax == 0) {
        goto label_26;
    }
    rax = *((rax + 0x18));
    *((rsp + 0x28)) = rax;
    if (rax == 0) {
        goto label_26;
    }
    r12 = *((rax + 8));
    r15 = *((r12 + 8));
    rax = strlen (*((r12 + 8)));
    r14 = *((rbx + 8));
    *((rsp + 0x30)) = rax;
    rax = strlen (r14);
    rdi = *((r12 + 0x10));
    *((rsp + 0x38)) = rax;
    if (rdi == 0) {
        goto label_100;
    }
    r13 = *((rbx + 0x10));
    if (r13 == 0) {
        goto label_100;
    }
    rax = strlen (rdi);
    *((rsp + 8)) = rax;
    rax = strlen (r13);
    al = (*((rsp + 8)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    *((rsp + 8)) = eax;
label_33:
    r13 = *(rbx);
    if (*(obj.print_grand_total) != 0) {
        goto label_101;
    }
    if ((*((rbx + 0x28)) & 2) == 0) {
        goto label_101;
    }
    if ((*((r12 + 0x28)) & 2) == 0) {
        goto label_101;
    }
    eax = strcmp (*(r12), r13);
    if (eax != 0) {
        goto label_26;
    }
label_101:
    rax = strchr (r13, 0x2f);
    if (rax != 0) {
        rax = strchr (*(r12), 0x2f);
        if (rax == 0) {
            goto label_34;
        }
    }
    rdx = *((rsp + 0x38));
    if (*((rsp + 0x30)) <= rdx) {
        goto label_102;
    }
    if (*((rsp + 8)) != 0) {
        goto label_102;
    }
label_34:
    rax = *((rsp + 0x28));
    *((rax + 8)) = rbx;
label_35:
    rbx = *((rbx + 0x30));
    if (*((rsp + 0x18)) != 0) {
        goto label_23;
    }
    rdi = r12;
    free_mount_entry ();
    goto label_23;
label_99:
    r12 = fs_exclude_list;
    if (r12 != 0) {
        goto label_24;
    }
    goto label_27;
label_68:
    free (*((rsp + 0x68)));
    goto label_28;
label_83:
    alloc_field (1, 0);
    goto label_29;
label_64:
    alloc_field (1, 0);
    goto label_30;
label_84:
    alloc_field (1, 0);
    goto label_31;
label_85:
    alloc_field (1, 0);
    goto label_32;
label_100:
    *((rsp + 8)) = 0;
    goto label_33;
label_102:
    eax = strcmp (*(r12), r13);
    if (eax == 0) {
        goto label_103;
    }
    eax = strcmp (r14, r15);
    if (eax == 0) {
        goto label_34;
    }
label_103:
    r12 = rbx;
    goto label_35;
label_96:
    xalloc_die ();
label_60:
    stack_chk_fail ();
label_95:
    edx = 5;
    rax = dcgettext (0, "no file systems processed");
    eax = 0;
    rax = error (1, 0, rax);
label_94:
    if (*(obj.print_grand_total) != 0) {
        rax = obj_grand_fsu;
        rdi = 0x00012287;
        rsi = 0x00013a19;
        if (*(0x0001804c) == 0) {
            rsi = rdi;
        }
        get_dev (rdi, rsi, 0, 0, 0, 0);
    }
    r12d = 0;
label_37:
    if (r12 >= *(obj.nrows)) {
        goto label_36;
    }
    r13 = r12*8;
    ebx = 0;
    while (rbx < *(obj.ncolumns)) {
        rax = table;
        r14 = rbx*8;
        rax = *((rax + r13));
        rbp = *((rax + rbx*8));
        if (rbx != 0) {
            rdi = stdout;
            rax = *((rdi + 0x28));
            if (rax >= *((rdi + 0x30))) {
                goto label_104;
            }
            rdx = rax + 1;
            *((rdi + 0x28)) = rdx;
            *(rax) = 0x20;
        }
label_38:
        rax = ncolumns;
        rax--;
        rax = columns;
        cl = (rax == rbx) ? 1 : 0;
        rax = *((rax + r14));
        ecx <<= 3;
        rdx = *((rax + 0x20));
        *((rsp + 0x70)) = rdx;
        rax = ambsalign (rbp, *((rsp + 0x10)), *((rax + 0x28)), 0);
        rsi = stdout;
        rdi = rax;
        if (rax == 0) {
            goto label_105;
        }
label_39:
        fputs_unlocked ();
        rbx++;
        free (rbp);
    }
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_106;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0xa;
    do {
        r12++;
        goto label_37;
label_104:
        esi = 0x20;
        overflow ();
        goto label_38;
label_105:
        rax = table;
        rax = *((rax + r13));
        rdi = *((rax + r14));
        goto label_39;
label_106:
        esi = 0xa;
        overflow ();
    } while (1);
label_59:
    assert_fail ("!\"invalid header_mode\", "src/df.c", 0x235, "get_field_list");
label_57:
    r13d = 1;
    r12 = 0x000136c1;
    if (*(obj.optind) >= ebp) {
        goto label_41;
    }
    if (*(obj.show_all_fs) != 0) {
        goto label_41;
    }
    if (*(obj.show_local_fs) != 0) {
        goto label_41;
    }
    while (*(obj.fs_exclude_list) != 0) {
label_41:
        edx = 5;
        rax = dcgettext (0, "cannot read table of mounted file systems");
        rbx = rax;
        rax = errno_location ();
        r8 = rbx;
        rcx = r12;
        eax = 0;
        error (r13d, *(rax), "%s%s");
        goto label_40;
    }
    edx = 5;
    r13d = 0;
    rax = dcgettext (0, "Warning: ");
    r12 = rax;
    goto label_41;
label_67:
    rax = *((rsp + 0x30));
    *((rsp + 0x18)) = rax;
    goto label_42;
label_47:
    r8 = "--output";
    rcx = 0x00012154;
    goto label_43;
label_48:
    r8 = "--output";
    rcx = 0x0001215a;
    error (0, 0, *((rsp + 0x20)));
    usage (1);
label_49:
    r8 = "--output";
    rcx = 0x00012157;
    goto label_43;
}

/* /tmp/tmp6wcn29hf @ 0x6250 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Show information about the file system on which each FILE resides,\nor all file systems by default.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -a, --all             include pseudo, duplicate, inaccessible file systems\n  -B, --block-size=SIZE  scale sizes by SIZE before printing them; e.g.,\n                           '-BM' prints sizes in units of 1,048,576 bytes;\n                           see SIZE format below\n  -h, --human-readable  print sizes in powers of 1024 (e.g., 1023M)\n  -H, --si              print sizes in powers of 1000 (e.g., 1.1G)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -i, --inodes          list inode information instead of block usage\n  -k                    like --block-size=1K\n  -l, --local           limit listing to local file systems\n      --no-sync         do not invoke sync before getting usage info (default)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --output[=FIELD_LIST]  use the output format defined by FIELD_LIST,\n                               or print all fields if FIELD_LIST is omitted.\n  -P, --portability     use the POSIX output format\n      --sync            invoke sync before getting usage info\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --total           elide all entries insignificant to available space,\n                          and produce a grand total\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -t, --type=TYPE       limit listing to file systems of type TYPE\n  -T, --print-type      print file system type\n  -x, --exclude-type=TYPE   limit listing to file systems not of type TYPE\n  -v                    (ignored)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    rax = dcgettext (0, "\nDisplay values are in units of the first available SIZE from --block-size,\nand the %s_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment variables.\nOtherwise, units default to 1024 bytes (or 512 if POSIXLY_CORRECT is set).\n");
    rdx = 0x0001206f;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "\nFIELD_LIST is a comma-separated list of columns to be included.  Valid\nfield names are: 'source', 'fstype', 'itotal', 'iused', 'iavail', 'ipcent',\n'size', 'used', 'avail', 'pcent', 'file' and 'target' (see info page).\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x00012072;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x64;
    *((rsp + 8)) = rax;
    rax = 0x000120ec;
    edi = 0x66;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x0001206c;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000120f6, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x0001208e;
    printf_chk ();
    rax = 0x000136c1;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000120f6, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x0001206c;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x0001208e;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x0001206c;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmp6wcn29hf @ 0xc470 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xcb60 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x11230 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = rsi - 0x400;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xc990 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x3ba3)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x8060 */
 
int64_t hash_get_first (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rax = *(rdi);
    rdx = *((rdi + 8));
    if (rax < rdx) {
        goto label_1;
    }
    void (*0x3b6f)() ();
    do {
        rax += 0x10;
        if (rax >= rdx) {
            goto label_2;
        }
label_1:
        r8 = *(rax);
    } while (r8 == 0);
    rax = r8;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
label_2:
    return hash_get_first_cold ();
}

/* /tmp/tmp6wcn29hf @ 0xc870 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x3b99)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xe510 */
 
int64_t dbg_rpl_vasprintf (int64_t arg1, int64_t arg2, int64_t arg3) {
    size_t length;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_vasprintf(char ** resultp,char const * format,__va_list_tag * args); */
    r8 = rsi;
    rbx = rdi;
    rcx = rdx;
    edi = 0;
    rdx = r8;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rsi = rsp;
    rax = vasnprintf ();
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = *(rsp);
    if (rax > 0x7fffffff) {
        goto label_2;
    }
    *(rbx) = rdi;
    do {
label_0:
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_3;
        }
        return rax;
label_2:
        free (rdi);
        errno_location ();
        *(rax) = 0x4b;
        eax = 0xffffffff;
    } while (1);
label_1:
    eax = 0xffffffff;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xe450 */
 
int64_t dbg_rpl_asprintf (int64_t arg_e0h, char * arg1, int64_t arg10, int64_t arg11, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list args;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* int rpl_asprintf(char ** resultp,char const * format,va_args ...); */
    *((rsp + 0x30)) = rdx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x40)) = r8;
    *((rsp + 0x48)) = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = rsp + 0xe0;
    *(rsp) = 0x10;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    rpl_vasprintf (rdi, rsi, rsp);
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xd000 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0x8ac0 */
 
void dbg_hash_delete (int64_t arg_8h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_delete(Hash_table * table, const * entry); */
    return void (*0x8930)() ();
}

/* /tmp/tmp6wcn29hf @ 0xe020 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x00013a80;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0x13a80 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmp6wcn29hf @ 0xc510 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x3b8e)() ();
    }
    if (rdx == 0) {
        void (*0x3b8e)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xca20 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00018550]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00018560]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xdd40 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xddc0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x9f00 */
 
int64_t dbg_mbsnwidth (int64_t arg1, int64_t arg3) {
    mbstate_t mbstate;
    int64_t var_8h;
    int64_t var_ch;
    wint_t wc;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rdx = arg3;
    /* int mbsnwidth(char const * string,size_t nbytes,int flags); */
    r15 = rdi;
    rbp = rdi + rsi;
    *((rsp + 0xc)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = ctype_get_mb_cur_max ();
    if (rax <= 1) {
        goto label_7;
    }
    r12d = 0;
    if (r15 >= rbp) {
        goto label_4;
    }
    eax = *((rsp + 0xc));
    r13 = rsp + 0x20;
    r14 = rsp + 0x1c;
    eax &= 2;
    *((rsp + 8)) = eax;
label_2:
    eax = *(r15);
    if (al > 0x5f) {
        goto label_8;
    }
    if (al > 0x40) {
        goto label_3;
    }
    if (al > 0x23) {
        goto label_9;
    }
    if (al > 0x1f) {
        goto label_3;
    }
label_1:
    *(r13) = 0;
    while (eax >= 0) {
        edx = 0x7fffffff;
        edx -= r12d;
        if (edx < eax) {
            goto label_10;
        }
        r12d += eax;
label_0:
        r15 += rbx;
        eax = mbsinit (r13);
        if (eax != 0) {
            goto label_6;
        }
        rdx -= r15;
        rax = rpl_mbrtowc (r14, r15, rbp, r13);
        if (rax == -1) {
            goto label_11;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_12;
        }
        edi = *((rsp + 0x1c));
        ebx = 1;
        if (rax != 0) {
            rbx = rax;
        }
        eax = wcwidth ();
    }
    eax = *((rsp + 8));
    if (eax != 0) {
        goto label_13;
    }
    eax = iswcntrl (*((rsp + 0x1c)));
    if (eax != 0) {
        goto label_0;
    }
    if (r12d == 0x7fffffff) {
        goto label_10;
    }
    r12d++;
    goto label_0;
label_9:
    eax -= 0x25;
    if (al > 0x1a) {
        goto label_1;
    }
label_3:
    r15++;
    r12d++;
label_6:
    if (r15 < rbp) {
        goto label_2;
    }
label_4:
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_14;
    }
    eax = r12d;
    return rax;
label_8:
    eax -= 0x61;
    if (al <= 0x1d) {
        goto label_3;
    }
    goto label_1;
label_7:
    r12d = 0;
    if (r15 >= rbp) {
        goto label_4;
    }
    rax = ctype_b_loc ();
    ecx = *((rsp + 0xc));
    r12d = 0;
    rdx = *(rax);
    ecx &= 2;
label_5:
    eax = *(r15);
    r15++;
    eax = *((rdx + rax*2));
    if ((ah & 0x40) == 0) {
        if (ecx != 0) {
            goto label_13;
        }
        if ((al & 2) != 0) {
            goto label_15;
        }
    }
    if (r12d == 0x7fffffff) {
        goto label_4;
    }
    r12d++;
label_15:
    if (rbp != r15) {
        goto label_5;
    }
    goto label_4;
label_11:
    if ((*((rsp + 0xc)) & 1) == 0) {
        goto label_3;
    }
    do {
label_13:
        r12d = 0xffffffff;
        goto label_4;
label_12:
    } while ((*((rsp + 0xc)) & 1) != 0);
    r12d++;
    r15 = rbp;
    goto label_6;
label_10:
    r12d = 0x7fffffff;
    goto label_4;
label_14:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xc770 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp6wcn29hf @ 0xda60 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x8190 */
 
int64_t dbg_hash_do_for_each (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t hash_do_for_each(Hash_table const * table,Hash_processor processor,void * processor_data); */
    r14 = *(rdi);
    if (r14 >= *((rdi + 8))) {
        goto label_3;
    }
    r15 = rdi;
    r13 = rdx;
    r12d = 0;
    do {
        rdi = *(r14);
        if (rdi != 0) {
            goto label_4;
        }
label_0:
        r14 += 0x10;
    } while (*((r15 + 8)) > r14);
label_2:
    rax = r12;
    return rax;
label_4:
    rbx = r14;
    goto label_5;
label_1:
    rbx = *((rbx + 8));
    r12++;
    if (rbx == 0) {
        goto label_0;
    }
    rdi = *(rbx);
label_5:
    rsi = r13;
    al = void (*rbp)() ();
    if (al != 0) {
        goto label_1;
    }
    goto label_2;
label_3:
    r12d = 0;
    goto label_2;
}

/* /tmp/tmp6wcn29hf @ 0xa120 */
 
uint64_t dbg_gnu_mbswidth (char * arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_ch;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* int gnu_mbswidth(char const * string,int flags); */
    r12d = esi;
    rax = strlen (rdi);
    edx = r12d;
    rdi = rbp;
    rsi = rax;
    return void (*0x9f00)() ();
}

/* /tmp/tmp6wcn29hf @ 0xf030 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x3a60)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmp6wcn29hf @ 0xc4b0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xdc40 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmp6wcn29hf @ 0x74a0 */
 
int64_t dbg_canonicalize_filename_mode (void) {
    scratch_buffer rname_buffer;
    int64_t var_418h;
    /* char * canonicalize_filename_mode(char const * name,canonicalize_mode_t can_mode); */
    rax = *(fs:0x28);
    *((rsp + 0x418)) = rax;
    eax = 0;
    canonicalize_filename_mode_stk (rdi, rsi, rsp);
    rdx = *((rsp + 0x418));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x9760 */
 
int64_t dbg_human_options (int64_t arg1, int64_t arg2, int64_t arg3) {
    char * ptr;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* strtol_error human_options(char const * spec,int * opts,uintmax_t * block_size); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_4;
    }
label_1:
    r12d = 0;
    if (*(rbx) == 0x27) {
        rbx++;
        r12d = 4;
    }
    r14 = obj_block_size_opts;
    eax = argmatch (rbx, obj.block_size_args, r14, 4);
    if (eax >= 0) {
        rax = (int64_t) eax;
        *(rbp) = 1;
        r12d |= *((r14 + rax*4));
        eax = 0;
        *(r13) = r12d;
label_0:
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_5;
        }
        return rax;
    }
    eax = xstrtoumax (rbx, rsp, 0, rbp, "eEgGkKmMpPtTyYzZ0");
    if (eax != 0) {
        goto label_6;
    }
    ecx = *(rbx);
    edx = rcx - 0x30;
    rcx = *(rsp);
    if (dl > 9) {
        goto label_7;
    }
    goto label_2;
    do {
        edi = *((rbx + 1));
        rbx++;
        edx = rdi - 0x30;
        if (dl <= 9) {
            goto label_2;
        }
label_7:
    } while (rcx != rbx);
    if (*((rcx - 1)) == 0x42) {
        goto label_8;
    }
    r12b |= 0x80;
label_3:
    r12d |= 0x20;
label_2:
    rdx = *(rbp);
    *(r13) = r12d;
    goto label_9;
label_6:
    *(r13) = 0;
    rdx = *(rbp);
label_9:
    if (rdx != 0) {
        goto label_0;
    }
    rax = getenv ("POSIXLY_CORRECT");
    rax -= rax;
    eax &= 0x200;
    rax += 0x200;
    *(rbp) = rax;
    eax = 4;
    goto label_0;
label_4:
    rax = getenv (0x0001219e);
    rbx = rax;
    if (rax != 0) {
        goto label_1;
    }
    rax = getenv ("BLOCKSIZE");
    rbx = rax;
    if (rax != 0) {
        goto label_1;
    }
    rax = getenv ("POSIXLY_CORRECT");
    if (rax == 0) {
        goto label_10;
    }
    *(rbp) = 0x200;
    eax = 0;
    *(r13) = 0;
    goto label_0;
label_8:
    r12d |= 0x180;
    if (*((rcx - 2)) != 0x69) {
        goto label_2;
    }
    goto label_3;
label_10:
    *(rbp) = 0x400;
    eax = 0;
    *(r13) = 0;
    goto label_0;
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0xd880 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmp6wcn29hf @ 0x38d0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmp6wcn29hf @ 0x74f0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmp6wcn29hf @ 0xd1b0 */
 
uint64_t dbg_gl_scratch_buffer_grow_preserve (int64_t arg1) {
    rdi = arg1;
    /* _Bool gl_scratch_buffer_grow_preserve(scratch_buffer * buffer); */
    r14 = rdi + 0x10;
    r13 = *((rdi + 8));
    rbx = rdi;
    r12 = *(rdi);
    rbp = r13 + r13;
    if (r12 == r14) {
        goto label_1;
    }
    if (r13 > rbp) {
        goto label_2;
    }
    rax = realloc (r12, rbp);
    rcx = rax;
    if (rax == 0) {
        goto label_3;
    }
    do {
        *(rbx) = rcx;
        eax = 1;
        *((rbx + 8)) = rbp;
label_0:
        return rax;
label_1:
        rax = malloc (rbp);
        if (rax == 0) {
            goto label_4;
        }
        rax = memcpy (rax, r12, r13);
        rcx = rax;
    } while (1);
label_2:
    errno_location ();
    *(rax) = 0xc;
    do {
        eax = free (r12);
        *(rbx) = r14;
        eax = 0;
        *((rbx + 8)) = 0x400;
        goto label_0;
label_3:
        r12 = *(rbx);
    } while (1);
label_4:
    eax = 0;
    goto label_0;
}

/* /tmp/tmp6wcn29hf @ 0xee60 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp6wcn29hf @ 0x3660 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmp6wcn29hf @ 0xd130 */
 
int64_t dbg_gl_scratch_buffer_grow (uint32_t arg1) {
    rdi = arg1;
    /* _Bool gl_scratch_buffer_grow(scratch_buffer * buffer); */
    rbx = rdi;
    rax = *((rdi + 8));
    rdi = *(rdi);
    r12 = rbx + 0x10;
    rbp = rax + rax;
    if (rdi != r12) {
        free (rdi);
        rax = *((rbx + 8));
    }
    if (rax <= rbp) {
        rax = malloc (rbp);
        if (rax == 0) {
            goto label_0;
        }
        r8d = 1;
        *(rbx) = rax;
        *((rbx + 8)) = rbp;
        eax = r8d;
        return rax;
    }
    errno_location ();
    *(rax) = 0xc;
label_0:
    rax = r12;
    r8d = 0;
    *(rbx) = rax;
    eax = r8d;
    *((rbx + 8)) = rbp;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xd270 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x00013668;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0001367b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x00013968;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x13968 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x3ad0)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x3ad0)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x3ad0)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmp6wcn29hf @ 0x104a0 */
 
int64_t dbg_printf_parse (int64_t arg1, int64_t arg2, int64_t arg3, size_t sum) {
    int64_t var_1h;
    int64_t var_4ch;
    int64_t var_30h;
    int64_t var_25h;
    int64_t var_bp_20h;
    int64_t var_8h;
    int64_t var_10h;
    void * s2;
    int64_t var_20h;
    void ** var_28h;
    void ** var_sp_30h;
    void ** var_38h;
    void ** var_40h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r9 = sum;
    /* int printf_parse(char const * format,char_directives * d,arguments * a); */
    r10 = rsi + 0x20;
    rax = rdi;
    rdi = rdx + 0x10;
    r15 = rdx;
    r14 = rsi;
    rcx = r10;
    r9d = 7;
    r13d = 0;
    r11d = 7;
    *(rsi) = 0;
    *((rsi + 8)) = r10;
    *((rsp + 0x18)) = rdi;
    *(rdx) = 0;
    *((rdx + 8)) = rdi;
    *((rsp + 0x10)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x20)) = 0;
    while (dl != 0) {
        rbx = rax + 1;
        if (dl == 0x25) {
            goto label_34;
        }
label_4:
        rax = rbx;
        edx = *(rax);
    }
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    *((rcx + rdx*8)) = rax;
    rax = *((rsp + 8));
    *((r14 + 0x10)) = rax;
    rax = *((rsp + 0x10));
    *((r14 + 0x18)) = rax;
    eax = 0;
label_7:
    return rax;
label_34:
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    r13 = 0xffffffffffffffff;
    r12 = rcx + rdx*8;
    *(r12) = rax;
    *((r12 + 0x10)) = 0;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x28)) = 0xffffffffffffffff;
    *((r12 + 0x30)) = 0;
    *((r12 + 0x38)) = 0;
    *((r12 + 0x40)) = 0xffffffffffffffff;
    *((r12 + 0x50)) = 0xffffffffffffffff;
    ebp = *((rax + 1));
    edx = rbp - 0x30;
    if (dl <= 9) {
        goto label_35;
    }
label_5:
    rcx = 0x00013d5c;
    rdx = rbx + 1;
    if (bpl == 0x27) {
        goto label_36;
    }
    do {
        eax = rbp - 0x20;
        if (al <= 0x29) {
            eax = (int32_t) al;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (42 cases) at 0x13d5c */
            void (*rax)() ();
        }
        if (bpl == 0x2a) {
            goto label_37;
        }
        eax = rbp - 0x30;
        if (al <= 9) {
            goto label_38;
        }
label_17:
        if (bpl == 0x2e) {
            goto label_39;
        }
label_3:
        edx = 0;
        rsi = 0x00013e04;
        edi = 1;
        rbx++;
        if (bpl == 0x68) {
            goto label_40;
        }
label_1:
        eax = rbp - 0x4c;
        if (al > 0x2e) {
            goto label_41;
        }
        eax = (int32_t) al;
        rax = *((rsi + rax*4));
        rax += rsi;
        /* switch table (47 cases) at 0x13e04 */
        void (*rax)() ();
        eax = *((r12 + 0x10));
        eax |= 0x40;
label_0:
        *((r12 + 0x10)) = eax;
        ebp = *(rdx);
        rbx = rdx;
        rdx = rbx + 1;
    } while (bpl != 0x27);
label_36:
    eax = *((r12 + 0x10));
    eax |= 1;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x20;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 2;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 4;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x10;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 8;
    goto label_0;
label_41:
    eax = rbp - 0x25;
    if (al > 0x53) {
        goto label_10;
    }
    rcx = 0x00013ec0;
    eax = (int32_t) al;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (84 cases) at 0x13ec0 */
    void (*rax)() ();
    edx += 8;
label_2:
    ebp = *(rbx);
    rbx++;
    if (bpl != 0x68) {
        goto label_1;
    }
label_40:
    ecx = edx;
    eax = edi;
    ecx &= 1;
    eax <<= cl;
    edx |= eax;
    goto label_2;
label_37:
    rdi = *((rsp + 8));
    eax = 1;
    *((r12 + 0x18)) = rbx;
    *((r12 + 0x20)) = rdx;
    ecx = *((rbx + 1));
    if (rdi != 0) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
    eax = rcx - 0x30;
    if (al <= 9) {
        goto label_42;
    }
label_9:
    rdi = *((rsp + 0x20));
    *((r12 + 0x28)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbp = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    rbx = rdx;
label_29:
    rdx = *((r15 + 8));
    r8 = *((r15 + 8));
    if (r9 <= rbp) {
        r9 += r9;
        rax = rbp + 1;
        if (r9 <= rbp) {
            r9 = rax;
        }
        rax = r9;
        rax >>= 0x3b;
        if (rax != 0) {
            goto label_30;
        }
        rsi = r9;
        rsi <<= 5;
        if (*((rsp + 0x18)) == rdx) {
            goto label_43;
        }
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r11;
        *((rsp + 0x28)) = r10;
        rax = realloc (rdx, rsi);
        rdx = *((r15 + 8));
        r10 = *((rsp + 0x28));
        r11 = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r8 = rax;
label_20:
        if (r8 == 0) {
            goto label_30;
        }
        if (*((rsp + 0x18)) == rdx) {
            goto label_44;
        }
label_26:
        *((r15 + 8)) = r8;
    }
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbp) {
        goto label_45;
    }
    do {
        rdx++;
        *(rax) = 0;
        rcx = rax;
        rax += 0x20;
    } while (rdx <= rbp);
    *(r15) = rdx;
    *(rcx) = 0;
label_45:
    rbp <<= 5;
    rbp += r8;
    eax = *(rbp);
    if (eax != 0) {
        goto label_46;
    }
    *(rbp) = 5;
    ebp = *(rbx);
    if (bpl != 0x2e) {
        goto label_3;
    }
label_39:
    if (*((rbx + 1)) != 0x2a) {
        goto label_47;
    }
    rdi = *((rsp + 0x10));
    eax = 2;
    rcx = rbx + 2;
    *((r12 + 0x30)) = rbx;
    *((r12 + 0x38)) = rcx;
    esi = *((rbx + 2));
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    eax = rsi - 0x30;
    if (al <= 9) {
        goto label_48;
    }
label_22:
    rbx = *((r12 + 0x40));
    if (rbx == -1) {
        goto label_49;
    }
label_21:
    r8 = *((r15 + 8));
    if (r9 <= rbx) {
        goto label_50;
    }
label_19:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbx) {
        goto label_51;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= rbx);
    *(r15) = rdx;
    *(rsi) = 0;
label_51:
    rbx <<= 5;
    rax = r8 + rbx;
    edx = *(rax);
    if (edx != 0) {
        goto label_52;
    }
    *(rax) = 5;
    rbx = rcx;
    ebp = *(rcx);
    goto label_3;
    edx |= 4;
    goto label_2;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xf;
label_12:
    if (r13 == -1) {
        goto label_53;
    }
    *((r12 + 0x50)) = r13;
label_16:
    r8 = *((r15 + 8));
    if (r9 <= r13) {
        goto label_54;
    }
label_15:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > r13) {
        goto label_55;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= r13);
    *(r15) = rdx;
    *(rsi) = 0;
label_55:
    r13 <<= 5;
    r13 += r8;
    eax = *(r13);
    if (eax != 0) {
        goto label_56;
    }
    *(r13) = ecx;
label_13:
    *((r12 + 0x48)) = bpl;
    rax = *(r14);
    *((r12 + 8)) = rbx;
    r13 = rax + 1;
    *(r14) = r13;
    if (r11 > r13) {
        rcx = *((r14 + 8));
        goto label_4;
    }
    if (r11 < 0) {
        goto label_57;
    }
    rax = 0x2e8ba2e8ba2e8ba;
    r12 = r11 + r11;
    if (r12 > rax) {
        goto label_57;
    }
    rax = r11 * 5;
    rbp = *((r14 + 8));
    *((rsp + 0x30)) = r9;
    rsi = r11 + rax*2;
    *((rsp + 0x28)) = r10;
    rsi <<= 4;
    if (r10 == rbp) {
        goto label_58;
    }
    rax = realloc (rbp, rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_57;
    }
    rbp = *((r14 + 8));
    if (r10 == rbp) {
        goto label_59;
    }
label_8:
    *((r14 + 8)) = rcx;
    r13 = *(r14);
    r11 = r12;
    goto label_4;
label_35:
    rdx = rbx;
    do {
        ecx = *((rdx + 1));
        rdx++;
        esi = rcx - 0x30;
    } while (sil <= 9);
    r13 = 0xffffffffffffffff;
    if (cl != 0x24) {
        goto label_5;
    }
    rax += 2;
    edi = 0;
    while (rsi >= 0) {
        if (dl > 9) {
            goto label_60;
        }
        rax++;
        edx = rbp - 0x30;
        rcx = rax - 1;
        rsi = 0x1999999999999999;
        rdx = (int64_t) dl;
        if (rdi > rsi) {
            goto label_61;
        }
        rsi = rdi * 5;
        rsi += rsi;
label_6:
        ebp = *(rax);
        rsi += rdx;
        rdi = rsi;
        edx = rbp - 0x30;
    }
    if (dl <= 9) {
        rcx = rax;
        rdx = (int64_t) dl;
        rax++;
        rsi = 0xffffffffffffffff;
        goto label_6;
    }
label_10:
    r8 = *((r15 + 8));
label_14:
    if (*((rsp + 0x18)) != r8) {
        *((rsp + 8)) = r10;
        free (r8);
        r10 = *((rsp + 8));
    }
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
    errno_location ();
    *(rax) = 0x16;
    eax = 0xffffffff;
    goto label_7;
label_47:
    *((r12 + 0x30)) = rbx;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    eax -= 0x30;
    if (al > 9) {
        goto label_62;
    }
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
label_28:
    rdi = *((rsp + 0x10));
    *((r12 + 0x38)) = rdx;
    ebp = *(rdx);
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    goto label_3;
label_58:
    rax = malloc (rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_63;
    }
label_33:
    rax = r13 * 5;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r10;
    rdx <<= 3;
    rax = memcpy (rcx, rbp, r13 + rax*2);
    r9 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    rcx = rax;
    goto label_8;
label_42:
    rax = rdx;
    do {
        esi = *((rax + 1));
        rax++;
        edi = rsi - 0x30;
    } while (dil <= 9);
    if (sil != 0x24) {
        goto label_9;
    }
    rbx += 2;
    esi = 0;
    while (rcx >= 0) {
        if (al > 9) {
            goto label_64;
        }
        rbx++;
        eax = rcx - 0x30;
        rdx = rbx - 1;
        rdi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rsi > rdi) {
            goto label_65;
        }
        rcx = rsi * 5;
        rcx += rcx;
label_11:
        rcx += rax;
        rsi = rcx;
        ecx = *(rbx);
        eax = rcx - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rdx = rbx;
    rax = (int64_t) al;
    rbx++;
    rcx = 0xffffffffffffffff;
    goto label_11;
    ecx = 0xc;
    if (edx > 0xf) {
        goto label_12;
    }
    ecx = 0;
    edx &= 4;
    cl = (edx != 0) ? 1 : 0;
    ecx += 0xb;
    goto label_12;
    ecx = 0xa;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 8;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 2;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 4;
    goto label_12;
    ecx = 9;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 7;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 1;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 3;
    goto label_12;
    ecx = 0x16;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 0x15;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 0x12;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx = 0x14;
    ecx -= edx;
    goto label_12;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xd;
    goto label_12;
    ecx = 0xe;
    goto label_12;
label_56:
    if (eax == ecx) {
        goto label_13;
    }
    goto label_14;
label_54:
    r9 += r9;
    rax = r13 + 1;
    if (r9 <= r13) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_67;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = realloc (r8, rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_68;
    }
label_18:
    *((r15 + 8)) = r8;
    goto label_15;
label_53:
    rdi = *((rsp + 0x20));
    *((r12 + 0x50)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    r13 = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_16;
label_38:
    *((r12 + 0x18)) = rbx;
    eax = *(rbx);
    eax -= 0x30;
    if (al > 9) {
        goto label_69;
    }
    rdx = rbx;
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rdi = *((rsp + 8));
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
label_69:
    *((r12 + 0x20)) = rbx;
    do {
        ebp = *(rbx);
        goto label_17;
label_46:
    } while (eax == 5);
    goto label_14;
label_67:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = malloc (rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax != 0) {
label_31:
        *((rsp + 0x40)) = r9;
        *((rsp + 0x38)) = r11;
        rdx <<= 5;
        *((rsp + 0x30)) = r10;
        *((rsp + 0x28)) = ecx;
        rax = memcpy (rdi, r8, *(r15));
        r9 = *((rsp + 0x40));
        r11 = *((rsp + 0x38));
        r10 = *((rsp + 0x30));
        ecx = *((rsp + 0x28));
        r8 = rax;
        goto label_18;
label_63:
        rdx = *((r15 + 8));
        if (*((rsp + 0x18)) == rdx) {
            goto label_70;
        }
label_24:
        *((rsp + 8)) = r10;
        free (rdx);
        r10 = *((rsp + 8));
    }
label_25:
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
label_70:
    errno_location ();
    *(rax) = 0xc;
    eax = 0xffffffff;
    return rax;
label_50:
    r9 += r9;
    rax = rbx + 1;
    if (r9 <= rbx) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_71;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = realloc (r8, rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_72;
    }
label_27:
    *((r15 + 8)) = r8;
    goto label_19;
label_52:
    if (edx != 5) {
        goto label_14;
    }
    ebp = *(rcx);
    rbx = rcx;
    goto label_3;
label_43:
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x28)) = r10;
    rax = malloc (rsi);
    rdx = *((rsp + 0x40));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_20;
    ecx = 0x11;
    goto label_12;
    ecx = 0x10;
    goto label_12;
label_49:
    rdi = *((rsp + 0x20));
    *((r12 + 0x40)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbx = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_21;
label_48:
    rax = rcx;
    do {
        edx = *((rax + 1));
        rax++;
        edi = rdx - 0x30;
    } while (dil <= 9);
    if (dl != 0x24) {
        goto label_22;
    }
    rbx += 3;
    edi = 0;
    while (rdx >= 0) {
        if (al > 9) {
            goto label_73;
        }
        rbx++;
        eax = rsi - 0x30;
        rcx = rbx - 1;
        rsi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rdi > rsi) {
            goto label_74;
        }
        rdx = rdi * 5;
        rdx += rdx;
label_23:
        esi = *(rbx);
        rdx += rax;
        rdi = rdx;
        eax = rsi - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rcx = rbx;
    rax = (int64_t) al;
    rbx++;
    rdx = 0xffffffffffffffff;
    goto label_23;
label_61:
    rsi = 0xffffffffffffffff;
    goto label_6;
label_60:
    r13 = rsi;
    r13--;
    if (r13 > 0xfffffffffffffffd) {
        goto label_10;
    }
    ebp = *((rcx + 2));
    rbx = rcx + 2;
    goto label_5;
label_57:
    rdx = *((r15 + 8));
label_30:
    if (*((rsp + 0x18)) != rdx) {
        goto label_24;
    }
    goto label_25;
label_44:
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    rdx <<= 5;
    *((rsp + 0x28)) = r10;
    rax = memcpy (r8, *((rsp + 0x18)), *(r15));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_26;
label_71:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = malloc (rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax == 0) {
        goto label_25;
    }
label_32:
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    rdx <<= 5;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = memcpy (rdi, r8, *(r15));
    r9 = *((rsp + 0x40));
    r11 = *((rsp + 0x38));
    r10 = *((rsp + 0x30));
    rcx = *((rsp + 0x28));
    r8 = rax;
    goto label_27;
label_65:
    rcx = 0xffffffffffffffff;
    goto label_11;
label_62:
    rbx = rdx;
    eax = 1;
    goto label_28;
label_64:
    rbp--;
    if (rbp > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x28)) = rbp;
    rbx = rdx + 2;
    goto label_29;
label_74:
    rdx = 0xffffffffffffffff;
    goto label_23;
label_73:
    rbx = rdx - 1;
    if (rbx > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x40)) = rbx;
    rcx += 2;
    goto label_21;
label_66:
    rdx = r8;
    goto label_30;
label_68:
    rdi = r8;
    r8 = rax;
    goto label_31;
label_72:
    rdi = r8;
    r8 = rax;
    goto label_32;
label_59:
    r13 = *(r14);
    goto label_33;
}

/* /tmp/tmp6wcn29hf @ 0xf270 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xd700 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp6wcn29hf @ 0x3000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xdda0 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0x7da0 */
 
int64_t hash_get_n_entries (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x20));
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0xf260 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmp6wcn29hf @ 0xdf20 */
 
uint64_t dbg_xgetcwd (void) {
    int64_t var_6h;
    int64_t var_7h;
    int64_t var_8h;
    /* char * xgetcwd(); */
    esi = 0;
    edi = 0;
    rax = getcwd ();
    r12 = rax;
    while (*(rax) != 0xc) {
        rax = r12;
        return rax;
        rax = errno_location ();
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xde60 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3900)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp6wcn29hf @ 0xe9a0 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmp6wcn29hf @ 0x35d0 */
 
void getenv (void) {
    /* [15] -r-x section size 1424 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp6wcn29hf @ 0x35e0 */
 
void snprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__snprintf_chk]");
}

/* /tmp/tmp6wcn29hf @ 0x3600 */
 
void endmntent (void) {
    __asm ("bnd jmp qword [reloc.endmntent]");
}

/* /tmp/tmp6wcn29hf @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    al = uint64_t (*rdi)() ();
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rdi + 0x21)) += bh;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    eax |= 0xfffffe00;
    *(rax)++;
    ebp = *(rcx) * 0;
    *(rax) += al;
    *(rbx)--;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(0x0000005d) += cl;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    *(rax)++;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    *(rbx)++;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *((rax + 0x17)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al &= *(rax);
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x22)) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    cl += al;
    __asm ("in al, 0");
    *(rax) += al;
    *(rax) += al;
    cl += al;
    __asm ("in al, 0");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += ah;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rdi) += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) += ch;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *((rcx + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rsi) += al;
    *(rax) += al;
    al += bh;
    *(rax) += al;
    *(rax) += al;
    al += bh;
    if (al != 0) {
        *(rax) += al;
    }
    *(rax) += al;
    *(rax) += al;
    __asm ("clc");
    if (*(rax) != 0) {
        *(rax) += al;
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    al += dh;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += cl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + 3)) += ch;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += r8b;
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("in eax, 0x74");
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("in eax, 0x74");
    al += 0;
    *(rax) += al;
    *((rax + 1)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += eax;
    *(rax) += al;
    al ^= *(rax);
    *((rax + 1)) += al;
    *(rax) += al;
    *(rax) += al;
    ah += bh;
    eax += 0;
    *(rax) += al;
    eax += 0;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("in eax, 0x74");
}

/* /tmp/tmp6wcn29hf @ 0x3610 */
 
void strverscmp (void) {
    __asm ("bnd jmp qword [reloc.strverscmp]");
}

/* /tmp/tmp6wcn29hf @ 0x3650 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmp6wcn29hf @ 0x3670 */
 
void isatty (void) {
    __asm ("bnd jmp qword [reloc.isatty]");
}

/* /tmp/tmp6wcn29hf @ 0x3680 */
 
void iswcntrl (void) {
    __asm ("bnd jmp qword [reloc.iswcntrl]");
}

/* /tmp/tmp6wcn29hf @ 0x3690 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmp6wcn29hf @ 0x36a0 */
 
void wcswidth (void) {
    __asm ("bnd jmp qword [reloc.wcswidth]");
}

/* /tmp/tmp6wcn29hf @ 0x36b0 */
 
void localeconv (void) {
    __asm ("bnd jmp qword [reloc.localeconv]");
}

/* /tmp/tmp6wcn29hf @ 0x36c0 */
 
void faccessat (void) {
    __asm ("bnd jmp qword [reloc.faccessat]");
}

/* /tmp/tmp6wcn29hf @ 0x36d0 */
 
void mbstowcs (void) {
    __asm ("bnd jmp qword [reloc.mbstowcs]");
}

/* /tmp/tmp6wcn29hf @ 0x36e0 */
 
void readlink (void) {
    __asm ("bnd jmp qword [reloc.readlink]");
}

/* /tmp/tmp6wcn29hf @ 0x36f0 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmp6wcn29hf @ 0x3700 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmp6wcn29hf @ 0x3710 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp6wcn29hf @ 0x3720 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmp6wcn29hf @ 0x3740 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmp6wcn29hf @ 0x3760 */
 
void openat (void) {
    __asm ("bnd jmp qword [reloc.openat]");
}

/* /tmp/tmp6wcn29hf @ 0x3770 */
 
void chdir (void) {
    __asm ("bnd jmp qword [reloc.chdir]");
}

/* /tmp/tmp6wcn29hf @ 0x3790 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmp6wcn29hf @ 0x37a0 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmp6wcn29hf @ 0x37b0 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmp6wcn29hf @ 0x37c0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmp6wcn29hf @ 0x37e0 */
 
void uname (void) {
    __asm ("bnd jmp qword [reloc.uname]");
}

/* /tmp/tmp6wcn29hf @ 0x37f0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmp6wcn29hf @ 0x3810 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp6wcn29hf @ 0x3830 */
 
void canonicalize_file_name (void) {
    __asm ("bnd jmp qword [reloc.canonicalize_file_name]");
}

/* /tmp/tmp6wcn29hf @ 0x3850 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmp6wcn29hf @ 0x3860 */
 
void memchr (void) {
    __asm ("bnd jmp qword [reloc.memchr]");
}

/* /tmp/tmp6wcn29hf @ 0x3870 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmp6wcn29hf @ 0x3890 */
 
void rawmemchr (void) {
    __asm ("bnd jmp qword [reloc.rawmemchr]");
}

/* /tmp/tmp6wcn29hf @ 0x38b0 */
 
void getdelim (void) {
    __asm ("bnd jmp qword [reloc.__getdelim]");
}

/* /tmp/tmp6wcn29hf @ 0x38e0 */
 
void memcpy_chk (void) {
    __asm ("bnd jmp qword [reloc.__memcpy_chk]");
}

/* /tmp/tmp6wcn29hf @ 0x38f0 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmp6wcn29hf @ 0x3900 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmp6wcn29hf @ 0x3910 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmp6wcn29hf @ 0x3920 */
 
void statfs (void) {
    __asm ("bnd jmp qword [reloc.statfs]");
}

/* /tmp/tmp6wcn29hf @ 0x3930 */
 
void sync (void) {
    __asm ("bnd jmp qword [reloc.sync]");
}

/* /tmp/tmp6wcn29hf @ 0x3940 */
 
void wcwidth (void) {
    __asm ("bnd jmp qword [reloc.wcwidth]");
}

/* /tmp/tmp6wcn29hf @ 0x3960 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmp6wcn29hf @ 0x3970 */
 
void getmntent (void) {
    __asm ("bnd jmp qword [reloc.getmntent]");
}

/* /tmp/tmp6wcn29hf @ 0x3980 */
 
void setmntent (void) {
    __asm ("bnd jmp qword [reloc.setmntent]");
}

/* /tmp/tmp6wcn29hf @ 0x39a0 */
 
void isoc99_sscanf (void) {
    __asm ("bnd jmp qword [reloc.__isoc99_sscanf]");
}

/* /tmp/tmp6wcn29hf @ 0x39b0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmp6wcn29hf @ 0x39c0 */
 
void statvfs (void) {
    __asm ("bnd jmp qword [reloc.statvfs]");
}

/* /tmp/tmp6wcn29hf @ 0x39d0 */
 
void fchdir (void) {
    __asm ("bnd jmp qword [reloc.fchdir]");
}

/* /tmp/tmp6wcn29hf @ 0x3a10 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmp6wcn29hf @ 0x3a20 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmp6wcn29hf @ 0x3a40 */
 
void memrchr (void) {
    __asm ("bnd jmp qword [reloc.memrchr]");
}

/* /tmp/tmp6wcn29hf @ 0x3a50 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmp6wcn29hf @ 0x3a60 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmp6wcn29hf @ 0x3a70 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmp6wcn29hf @ 0x3a80 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmp6wcn29hf @ 0x3a90 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmp6wcn29hf @ 0x3aa0 */
 
void wcstombs (void) {
    __asm ("bnd jmp qword [reloc.wcstombs]");
}

/* /tmp/tmp6wcn29hf @ 0x3ae0 */
 
void strdup (void) {
    __asm ("bnd jmp qword [reloc.strdup]");
}

/* /tmp/tmp6wcn29hf @ 0x3af0 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmp6wcn29hf @ 0x3b00 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmp6wcn29hf @ 0x3b10 */
 
void hasmntopt (void) {
    __asm ("bnd jmp qword [reloc.hasmntopt]");
}

/* /tmp/tmp6wcn29hf @ 0x3b20 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmp6wcn29hf @ 0x3b30 */
 
void strstr (void) {
    __asm ("bnd jmp qword [reloc.strstr]");
}

/* /tmp/tmp6wcn29hf @ 0x3b40 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmp6wcn29hf @ 0x3b50 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmp6wcn29hf @ 0x3030 */
 
void fcn_00003030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1440 named .plt */
    __asm ("bnd jmp qword [0x00017cf8]");
}

/* /tmp/tmp6wcn29hf @ 0x3040 */
 
void fcn_00003040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3050 */
 
void fcn_00003050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3060 */
 
void fcn_00003060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3070 */
 
void fcn_00003070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3080 */
 
void fcn_00003080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3090 */
 
void fcn_00003090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x30a0 */
 
void fcn_000030a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x30b0 */
 
void fcn_000030b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x30c0 */
 
void fcn_000030c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x30d0 */
 
void fcn_000030d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x30e0 */
 
void fcn_000030e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x30f0 */
 
void fcn_000030f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3100 */
 
void fcn_00003100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3110 */
 
void fcn_00003110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3120 */
 
void fcn_00003120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3130 */
 
void fcn_00003130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3140 */
 
void fcn_00003140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3150 */
 
void fcn_00003150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3160 */
 
void fcn_00003160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3170 */
 
void fcn_00003170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3180 */
 
void fcn_00003180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3190 */
 
void fcn_00003190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x31a0 */
 
void fcn_000031a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x31b0 */
 
void fcn_000031b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x31c0 */
 
void fcn_000031c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x31d0 */
 
void fcn_000031d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x31e0 */
 
void fcn_000031e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x31f0 */
 
void fcn_000031f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3200 */
 
void fcn_00003200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3210 */
 
void fcn_00003210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3220 */
 
void fcn_00003220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3230 */
 
void fcn_00003230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3240 */
 
void fcn_00003240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3250 */
 
void fcn_00003250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3260 */
 
void fcn_00003260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3270 */
 
void fcn_00003270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3280 */
 
void fcn_00003280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3290 */
 
void fcn_00003290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x32a0 */
 
void fcn_000032a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x32b0 */
 
void fcn_000032b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x32c0 */
 
void fcn_000032c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x32d0 */
 
void fcn_000032d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x32e0 */
 
void fcn_000032e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x32f0 */
 
void fcn_000032f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3300 */
 
void fcn_00003300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3310 */
 
void fcn_00003310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3320 */
 
void fcn_00003320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3330 */
 
void fcn_00003330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3340 */
 
void fcn_00003340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3350 */
 
void fcn_00003350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3360 */
 
void fcn_00003360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3370 */
 
void fcn_00003370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3380 */
 
void fcn_00003380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3390 */
 
void fcn_00003390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x33a0 */
 
void fcn_000033a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x33b0 */
 
void fcn_000033b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x33c0 */
 
void fcn_000033c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x33d0 */
 
void fcn_000033d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x33e0 */
 
void fcn_000033e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x33f0 */
 
void fcn_000033f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3400 */
 
void fcn_00003400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3410 */
 
void fcn_00003410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3420 */
 
void fcn_00003420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3430 */
 
void fcn_00003430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3440 */
 
void fcn_00003440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3450 */
 
void fcn_00003450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3460 */
 
void fcn_00003460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3470 */
 
void fcn_00003470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3480 */
 
void fcn_00003480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3490 */
 
void fcn_00003490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x34a0 */
 
void fcn_000034a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x34b0 */
 
void fcn_000034b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x34c0 */
 
void fcn_000034c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x34d0 */
 
void fcn_000034d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x34e0 */
 
void fcn_000034e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x34f0 */
 
void fcn_000034f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3500 */
 
void fcn_00003500 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3510 */
 
void fcn_00003510 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3520 */
 
void fcn_00003520 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3530 */
 
void fcn_00003530 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3540 */
 
void fcn_00003540 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3550 */
 
void fcn_00003550 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3560 */
 
void fcn_00003560 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3570 */
 
void fcn_00003570 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3580 */
 
void fcn_00003580 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x3590 */
 
void fcn_00003590 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x35a0 */
 
void fcn_000035a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp6wcn29hf @ 0x35b0 */
 
void fcn_000035b0 (void) {
    return __asm ("bnd jmp section..plt");
}
