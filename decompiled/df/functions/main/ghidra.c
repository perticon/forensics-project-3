undefined8 main(int param_1,undefined8 *param_2)

{
  stat *__buf;
  char **ppcVar1;
  bool bVar2;
  char cVar3;
  int iVar4;
  int iVar5;
  uint uVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  char *pcVar9;
  long lVar10;
  char **ppcVar11;
  char *pcVar12;
  char *__ptr;
  char **ppcVar13;
  size_t sVar14;
  char *pcVar15;
  size_t sVar16;
  void *__ptr_00;
  size_t sVar17;
  size_t sVar18;
  char *pcVar19;
  int *piVar20;
  undefined *puVar21;
  size_t extraout_RDX;
  size_t extraout_RDX_00;
  size_t extraout_RDX_01;
  long lVar22;
  char **ppcVar23;
  char **ppcVar24;
  ulong uVar25;
  undefined8 in_R10;
  char **ppcVar26;
  ulong uVar27;
  char *__s2;
  long in_FS_OFFSET;
  bool bVar28;
  bool bVar29;
  undefined auVar30 [16];
  char *local_150;
  size_t local_140;
  long local_130;
  long local_128;
  char **local_110;
  undefined8 local_f8;
  stat local_d8;
  long local_40;
  
  bVar29 = false;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  fs_select_list = (char **)0x0;
  fs_exclude_list = (char **)0x0;
  show_all_fs = '\0';
  show_listed_fs = '\0';
  human_output_opts = 0xffffffff;
  print_type = '\0';
  file_systems_processed = '\0';
  exit_status = 0;
  print_grand_total = '\0';
  grand_fsu._0_8_ = 1;
  uVar7 = dcgettext(0,"options %s and %s are mutually exclusive",5);
switchD_00103d2f_caseD_76:
  local_f8 = (char *)CONCAT44(local_f8._4_4_,0xffffffff);
  iVar4 = getopt_long(param_1,param_2,"aB:iF:hHklmPTt:vx:",long_options,&local_f8);
  pcVar9 = optarg;
  if (iVar4 == -1) {
LAB_00103f7b:
    if (human_output_opts != 0xffffffff) goto LAB_00103f88;
    if (!bVar29) goto LAB_00104291;
    human_output_opts = 0;
    pcVar9 = getenv("POSIXLY_CORRECT");
    output_block_size = (ulong)(-(uint)(pcVar9 == (char *)0x0) & 0x200) + 0x200;
    goto LAB_00103f88;
  }
  if (0x83 < iVar4) goto switchD_00103d2f_caseD_43;
  if (iVar4 < 0x42) {
    if (iVar4 == -0x83) {
      version_etc(stdout,&DAT_0011206c,"GNU coreutils",Version,"Torbjorn Granlund","David MacKenzie"
                  ,"Paul Eggert",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar4 == -0x82) {
      usage(0);
      goto LAB_00103f7b;
    }
    goto switchD_00103d2f_caseD_43;
  }
  switch(iVar4) {
  case 0x42:
    iVar4 = human_options(optarg,&human_output_opts,&output_block_size);
    if (iVar4 == 0) goto switchD_00103d2f_caseD_76;
    xstrtol_fatal(iVar4,(ulong)local_f8 & 0xffffffff,0x42,long_options,optarg);
  case 0x83:
    if (header_mode == 1) {
LAB_00105054:
      puVar21 = &DAT_00112154;
    }
    else {
      if ((header_mode == 0) && (bVar29)) {
        error(0,0,uVar7,&DAT_0011215a,"--output");
        usage(1);
      }
      else if (print_type == '\0') {
        header_mode = 4;
        if (optarg != (char *)0x0) {
          decode_output_arg();
        }
        goto switchD_00103d2f_caseD_76;
      }
LAB_0010508d:
      puVar21 = &DAT_00112157;
    }
LAB_00104277:
    error(0,0,uVar7,puVar21,"--output");
  default:
    break;
  case 0x46:
  case 0x74:
    ppcVar11 = (char **)xmalloc();
    *ppcVar11 = pcVar9;
    ppcVar11[1] = (char *)fs_select_list;
    fs_select_list = ppcVar11;
    goto switchD_00103d2f_caseD_76;
  case 0x48:
    human_output_opts = 0x90;
    output_block_size = 1;
    goto switchD_00103d2f_caseD_76;
  case 0x50:
    if (header_mode == 4) {
      puVar21 = &DAT_0011215a;
      goto LAB_00104277;
    }
    bVar29 = true;
    goto switchD_00103d2f_caseD_76;
  case 0x54:
    if (header_mode == 4) goto LAB_0010508d;
    print_type = '\x01';
    goto switchD_00103d2f_caseD_76;
  case 0x61:
    show_all_fs = '\x01';
    goto switchD_00103d2f_caseD_76;
  case 0x68:
    human_output_opts = 0xb0;
    output_block_size = 1;
    goto switchD_00103d2f_caseD_76;
  case 0x69:
    if (header_mode == 4) goto LAB_00105054;
    header_mode = 1;
    goto switchD_00103d2f_caseD_76;
  case 0x6b:
    human_output_opts = 0;
    output_block_size = 0x400;
    goto switchD_00103d2f_caseD_76;
  case 0x6c:
    show_local_fs = '\x01';
    goto switchD_00103d2f_caseD_76;
  case 0x6d:
    human_output_opts = 0;
    output_block_size = 0x100000;
  case 0x76:
    goto switchD_00103d2f_caseD_76;
  case 0x78:
    ppcVar11 = (char **)xmalloc();
    *ppcVar11 = pcVar9;
    ppcVar11[1] = (char *)fs_exclude_list;
    fs_exclude_list = ppcVar11;
    goto switchD_00103d2f_caseD_76;
  case 0x80:
    require_sync = '\0';
    goto switchD_00103d2f_caseD_76;
  case 0x81:
    require_sync = '\x01';
    goto switchD_00103d2f_caseD_76;
  case 0x82:
    print_grand_total = '\x01';
    goto switchD_00103d2f_caseD_76;
  }
switchD_00103d2f_caseD_43:
  usage(1);
LAB_00104291:
  pcVar9 = getenv("DF_BLOCK_SIZE");
  human_options(pcVar9,&human_output_opts,&output_block_size);
LAB_00103f88:
  if ((header_mode != 1) && (header_mode != 4)) {
    if ((human_output_opts & 0x10) == 0) {
      if (bVar29) {
        header_mode = 3;
      }
    }
    else {
      header_mode = 2;
    }
  }
  if (fs_select_list != (char **)0x0) {
    bVar29 = false;
    ppcVar11 = fs_select_list;
    do {
      if (fs_exclude_list != (char **)0x0) {
        pcVar9 = *ppcVar11;
        ppcVar24 = fs_exclude_list;
        do {
          iVar4 = strcmp(pcVar9,*ppcVar24);
          if (iVar4 == 0) {
            uVar7 = quote(pcVar9);
            uVar8 = dcgettext(0,"file system type %s both selected and excluded",5);
            bVar29 = true;
            error(0,0,uVar8,uVar7);
            break;
          }
          ppcVar24 = (char **)ppcVar24[1];
        } while (ppcVar24 != (char **)0x0);
      }
      ppcVar11 = (char **)ppcVar11[1];
    } while (ppcVar11 != (char **)0x0);
    if (bVar29) {
      if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
        __stack_chk_fail();
      }
      return 1;
    }
  }
  local_128 = 0;
  if (optind < param_1) {
    local_128 = xnmalloc((long)(param_1 - optind),0x90);
    for (lVar22 = (long)optind; (int)lVar22 < param_1; lVar22 = lVar22 + 1) {
      pcVar9 = (char *)param_2[lVar22];
      __buf = (stat *)(local_128 + (long)((int)lVar22 - optind) * 0x90);
      iVar4 = open(pcVar9,0x900);
      if (iVar4 < 0) {
        piVar20 = __errno_location();
        iVar5 = *piVar20;
        if ((iVar5 == 2) || (iVar5 == 0x14)) goto LAB_00104188;
        iVar4 = stat(pcVar9,__buf);
        if (iVar4 != 0) {
          iVar5 = *piVar20;
          goto LAB_00104188;
        }
      }
      else {
        iVar5 = fstat(iVar4,__buf);
        if (iVar5 != 0) {
          piVar20 = __errno_location();
          iVar5 = *piVar20;
        }
        close(iVar4);
LAB_00104188:
        if (iVar5 != 0) {
          uVar7 = quotearg_n_style_colon(0,3,param_2[lVar22]);
          error(0,iVar5,&DAT_00113bba,uVar7);
          param_2[lVar22] = 0;
          exit_status = 1;
        }
      }
    }
  }
  mount_list = (char **)read_file_system_list();
  if (mount_list == (char **)0x0) {
    uVar7 = 1;
    pcVar9 = "";
    if ((((optind < param_1) && (show_all_fs == '\0')) && (show_local_fs == '\0')) &&
       ((fs_select_list == (char **)0x0 && (fs_exclude_list == (char **)0x0)))) {
      uVar7 = 0;
      pcVar9 = (char *)dcgettext(0,"Warning: ",5);
    }
    uVar8 = dcgettext(0,"cannot read table of mounted file systems",5);
    piVar20 = __errno_location();
    error(uVar7,*piVar20,&DAT_001121b3,pcVar9,uVar8);
  }
  if (require_sync != '\0') {
    sync();
  }
  switch(header_mode) {
  case 0:
    alloc_field(0,0);
    if (print_type != '\0') {
      alloc_field(1,0);
    }
    alloc_field(2,0);
    alloc_field(3,0);
    alloc_field(4,0);
    pcVar9 = (char *)0x0;
    break;
  case 1:
    alloc_field(0,0);
    if (print_type != '\0') {
      alloc_field(1,0);
    }
    alloc_field(6,0);
    alloc_field(7,0);
    alloc_field(8,0);
    alloc_field(9,0);
    alloc_field(10,0);
    goto LAB_0010430f;
  case 2:
    alloc_field(0,0);
    if (print_type != '\0') {
      alloc_field(1,0);
    }
    alloc_field(2,&DAT_00112115);
    alloc_field(3,0);
    alloc_field(4,"Avail");
    pcVar9 = (char *)0x0;
    break;
  case 3:
    alloc_field(0,0);
    if (print_type != '\0') {
      alloc_field(1,0);
    }
    alloc_field(2,0);
    alloc_field(3,0);
    alloc_field(4,0);
    pcVar9 = "Capacity";
    break;
  case 4:
    if (ncolumns == 0) {
      decode_output_arg();
    }
    goto LAB_0010430f;
  default:
                    /* WARNING: Subroutine does not return */
    __assert_fail("!\"invalid header_mode\"","src/df.c",0x235,"get_field_list");
  }
  alloc_field(5,pcVar9);
  alloc_field(10,0);
LAB_0010430f:
  local_130 = get_header();
  cVar3 = show_all_fs;
  if (local_128 == 0) {
    iVar4 = 0;
    for (ppcVar11 = mount_list; ppcVar11 != (char **)0x0; ppcVar11 = (char **)ppcVar11[6]) {
      iVar4 = iVar4 + 1;
    }
    devlist_table = hash_initialize((long)iVar4,0,devlist_hash,devlist_compare,0);
    auVar30 = CONCAT88(&local_d8,devlist_table);
    ppcVar24 = mount_list;
    if (devlist_table == 0) {
LAB_00104e11:
                    /* WARNING: Subroutine does not return */
      xalloc_die();
    }
LAB_00104b2e:
    if (ppcVar24 != (char **)0x0) {
      if ((((*(byte *)(ppcVar24 + 5) & 2) == 0) || (show_local_fs == '\0')) &&
         (((*(byte *)(ppcVar24 + 5) & 1) == 0 || ((show_all_fs != '\0' || (show_listed_fs != '\0')))
          ))) {
        ppcVar13 = (char **)ppcVar24[3];
        if (fs_select_list == (char **)0x0) {
          ppcVar23 = fs_exclude_list;
          ppcVar1 = ppcVar13;
          if (fs_exclude_list != (char **)0x0) {
joined_r0x00104d7b:
            do {
              if (ppcVar1 == (char **)0x0) break;
              iVar4 = strcmp((char *)ppcVar13,*ppcVar23);
              if (iVar4 == 0) goto LAB_00104b9e;
              ppcVar1 = ppcVar23 + 1;
              ppcVar23 = (char **)*ppcVar1;
              ppcVar1 = (char **)*ppcVar1;
            } while( true );
          }
        }
        else {
          ppcVar26 = fs_select_list;
          if (ppcVar13 != (char **)0x0) {
            do {
              iVar4 = strcmp((char *)ppcVar13,*ppcVar26);
              ppcVar23 = fs_exclude_list;
              ppcVar1 = fs_exclude_list;
              if (iVar4 == 0) goto joined_r0x00104d7b;
              ppcVar23 = ppcVar26 + 1;
              ppcVar26 = (char **)*ppcVar23;
            } while ((char **)*ppcVar23 != (char **)0x0);
            goto LAB_00104b9e;
          }
        }
        iVar4 = stat(ppcVar24[1],&local_d8);
        if (iVar4 == -1) goto LAB_00104b9e;
        if (devlist_table != 0) {
          local_f8 = (char *)local_d8.st_dev;
          lVar22 = hash_lookup(devlist_table,&local_f8);
          if ((lVar22 != 0) && (uVar27 = *(ulong *)(lVar22 + 0x18), uVar27 != 0)) {
            ppcVar13 = *(char ***)(uVar27 + 8);
            pcVar9 = ppcVar13[1];
            sVar14 = strlen(pcVar9);
            pcVar19 = ppcVar24[1];
            sVar16 = strlen(pcVar19);
            if ((ppcVar13[2] == (char *)0x0) || (pcVar12 = ppcVar24[2], pcVar12 == (char *)0x0)) {
              bVar29 = false;
            }
            else {
              sVar17 = strlen(ppcVar13[2]);
              sVar18 = strlen(pcVar12);
              bVar29 = sVar17 < sVar18;
            }
            pcVar12 = *ppcVar24;
            if ((((print_grand_total != '\0') || ((*(byte *)(ppcVar24 + 5) & 2) == 0)) ||
                ((*(byte *)(ppcVar13 + 5) & 2) == 0)) ||
               (iVar4 = strcmp(*ppcVar13,pcVar12), iVar4 == 0)) {
              pcVar15 = strchr(pcVar12,0x2f);
              if ((((pcVar15 == (char *)0x0) ||
                   (pcVar15 = strchr(*ppcVar13,0x2f), sVar17 = extraout_RDX, pcVar15 != (char *)0x0)
                   ) && ((sVar14 <= sVar16 || (sVar17 = sVar16, bVar29)))) &&
                 ((uVar6 = strcmp(*ppcVar13,pcVar12), sVar17 = extraout_RDX_00, uVar6 == 0 ||
                  (uVar6 = strcmp(pcVar19,pcVar9), sVar17 = extraout_RDX_01, uVar6 != 0)))) {
                uVar27 = (ulong)uVar6;
              }
              else {
                *(char ***)(uVar27 + 8) = ppcVar24;
              }
              auVar30 = CONCAT88(sVar17,uVar27);
              ppcVar24 = (char **)ppcVar24[6];
              if (cVar3 == '\0') {
                auVar30 = free_mount_entry();
              }
              goto LAB_00104b2e;
            }
          }
        }
      }
      else {
LAB_00104b9e:
        local_d8.st_dev = (__dev_t)ppcVar24[4];
      }
      ppcVar13 = (char **)xmalloc(0x20);
      lVar22 = devlist_table;
      ppcVar13[1] = (char *)ppcVar24;
      ppcVar13[2] = (char *)ppcVar11;
      *ppcVar13 = (char *)local_d8.st_dev;
      auVar30 = hash_insert(lVar22,ppcVar13);
      if (SUB168(auVar30,0) == 0) goto LAB_00104e11;
      *(char ***)(SUB168(auVar30,0) + 0x18) = ppcVar13;
      ppcVar24 = (char **)ppcVar24[6];
      ppcVar11 = ppcVar13;
      goto LAB_00104b2e;
    }
    ppcVar24 = mount_list;
    if (cVar3 == '\0') {
      mount_list = (char **)0x0;
      while (ppcVar11 != (char **)0x0) {
        ppcVar24 = (char **)ppcVar11[1];
        ppcVar13 = (char **)ppcVar11[2];
        ppcVar24[6] = (char *)mount_list;
        mount_list = ppcVar24;
        free(ppcVar11);
        ppcVar11 = ppcVar13;
      }
      auVar30 = hash_free();
      devlist_table = 0;
      ppcVar24 = mount_list;
    }
    while( true ) {
      local_130 = SUB168(auVar30,0);
      if (ppcVar24 == (char **)0x0) break;
      auVar30 = get_dev(*ppcVar24,ppcVar24[1],0,0,ppcVar24[3],*(byte *)(ppcVar24 + 5) & 1,
                        *(byte *)(ppcVar24 + 5) >> 1 & 1,0,1,SUB168(auVar30 >> 0x40,0));
      ppcVar24 = (char **)ppcVar24[6];
    }
  }
  else {
    show_listed_fs = '\x01';
    if (optind < param_1) {
      param_1 = param_1 - optind;
      lVar22 = (long)optind + 1;
      lVar10 = (long)optind;
      local_130 = lVar22;
      do {
        pcVar9 = (char *)param_2[lVar10];
        if (pcVar9 == (char *)0x0) goto LAB_001046f9;
        ppcVar11 = (char **)((long)((int)lVar10 - optind) * 0x90 + local_128);
        pcVar19 = pcVar9;
        if ((*(uint *)(ppcVar11 + 3) & 0xb000) == 0x2000) {
          pcVar12 = canonicalize_file_name(pcVar9);
          local_150 = pcVar9;
          if ((pcVar12 != (char *)0x0) && (local_150 = pcVar12, *pcVar12 != '/')) {
            local_150 = pcVar9;
          }
          if (mount_list == (char **)0x0) {
            free(pcVar12);
            goto LAB_001044ff;
          }
          local_110 = (char **)0x0;
          bVar29 = false;
          local_140 = 0xffffffffffffffff;
          bVar2 = false;
          ppcVar24 = mount_list;
          do {
            pcVar15 = *ppcVar24;
            __ptr = canonicalize_file_name(pcVar15);
            if ((__ptr != (char *)0x0) && (*__ptr == '/')) {
              pcVar15 = __ptr;
            }
            iVar4 = strcmp(local_150,pcVar15);
            if (iVar4 == 0) {
              __s2 = ppcVar24[1];
              ppcVar13 = mount_list;
              if (mount_list == (char **)0x0) {
LAB_00104497:
                sVar14 = strlen(__s2);
                bVar28 = sVar14 < local_140;
                pcVar19 = (char *)((ulong)pcVar19 & 0xffffffffffffff00 | (ulong)bVar28);
                bVar29 = (bool)(bVar2 ^ 1U | bVar28);
                if (bVar29) {
                  iVar4 = stat(__s2,&local_d8);
                  if (iVar4 == 0) {
LAB_00104764:
                    local_140 = sVar14;
                    local_110 = ppcVar24;
                    bVar2 = bVar29;
                    if (sVar14 == 1) {
                      free(ppcVar13);
                      free(__ptr);
                      free(pcVar12);
                      goto LAB_001049db;
                    }
                  }
                  else if (((bool)(bVar2 ^ 1U)) && (pcVar19 = (char *)(ulong)bVar28, bVar28)) {
                    bVar29 = false;
                    goto LAB_00104764;
                  }
                  bVar29 = false;
                }
                else {
                  bVar2 = true;
                }
              }
              else {
                ppcVar23 = (char **)0x0;
                do {
                  iVar4 = strcmp(ppcVar13[1],__s2);
                  if (iVar4 == 0) {
                    ppcVar23 = ppcVar13;
                  }
                  ppcVar13 = (char **)ppcVar13[6];
                } while (ppcVar13 != (char **)0x0);
                if (ppcVar23 == (char **)0x0) goto LAB_00104497;
                ppcVar13 = (char **)canonicalize_file_name(*ppcVar23);
                if ((ppcVar13 == (char **)0x0) || (*(char *)ppcVar13 != '/')) {
                  free(ppcVar13);
                  ppcVar13 = (char **)xstrdup(*ppcVar23);
                }
                iVar4 = strcmp((char *)ppcVar13,pcVar15);
                if (iVar4 == 0) {
                  __s2 = ppcVar24[1];
                  goto LAB_00104497;
                }
                bVar29 = true;
              }
              free(ppcVar13);
            }
            free(__ptr);
            ppcVar24 = (char **)ppcVar24[6];
          } while (ppcVar24 != (char **)0x0);
          free(pcVar12);
          if (local_110 == (char **)0x0) {
            if (!bVar29) goto LAB_001044ff;
            uVar7 = quotearg_style(4,pcVar9);
            uVar8 = dcgettext(0,"cannot access %s: over-mounted by another device",5);
            error(0,0,uVar8,uVar7);
            exit_status = 1;
          }
          else {
LAB_001049db:
            get_dev(*local_110,local_110[1],pcVar9,0,local_110[3],*(byte *)(local_110 + 5) & 1,
                    *(byte *)(local_110 + 5) >> 1 & 1,0,0,in_R10);
          }
        }
        else {
LAB_001044ff:
          pcVar12 = canonicalize_file_name(pcVar9);
          ppcVar24 = mount_list;
          if ((pcVar12 == (char *)0x0) || (*pcVar12 != '/')) {
            free(pcVar12);
            if (ppcVar24 != (char **)0x0) goto LAB_00104532;
          }
          else {
            sVar14 = strlen(pcVar12);
            if (ppcVar24 != (char **)0x0) {
              uVar27 = 0;
              ppcVar23 = (char **)0x0;
              ppcVar13 = ppcVar24;
              do {
                iVar4 = strcmp(ppcVar13[3],"lofs");
                if ((iVar4 != 0) &&
                   (((ppcVar23 == (char **)0x0 || ((*(byte *)(ppcVar23 + 5) & 1) != 0)) ||
                    ((*(byte *)(ppcVar13 + 5) & 1) == 0)))) {
                  pcVar15 = ppcVar13[1];
                  sVar16 = strlen(pcVar15);
                  if ((uVar27 <= sVar16) && (sVar16 <= sVar14)) {
                    if (sVar16 == 1) {
                      ppcVar23 = ppcVar13;
                      uVar27 = 1;
                    }
                    else if (((sVar14 == sVar16) || (pcVar12[sVar16] == '/')) &&
                            (iVar4 = strncmp(pcVar15,pcVar12,sVar16), iVar4 == 0)) {
                      ppcVar23 = ppcVar13;
                      uVar27 = sVar16;
                    }
                  }
                }
                ppcVar13 = (char **)ppcVar13[6];
              } while (ppcVar13 != (char **)0x0);
              free(pcVar12);
              if (ppcVar23 == (char **)0x0) {
LAB_00104532:
                ppcVar13 = (char **)0x0;
                do {
                  pcVar15 = ppcVar24[4];
                  if (ppcVar24[4] == (char *)0xffffffffffffffff) {
                    pcVar12 = ppcVar24[1];
                    iVar4 = stat(pcVar12,&local_d8);
                    if (iVar4 == 0) {
                      ppcVar24[4] = (char *)local_d8.st_dev;
                      pcVar15 = (char *)local_d8.st_dev;
                    }
                    else {
                      piVar20 = __errno_location();
                      if (*piVar20 == 5) {
                        pcVar19 = (char *)quotearg_n_style_colon(0,3,ppcVar24[1]);
                        pcVar12 = (char *)0x0;
                        error(0,*piVar20,&DAT_00113bba);
                        exit_status = 1;
                      }
                      ppcVar24[4] = (char *)0xfffffffffffffffe;
                      pcVar15 = (char *)0xfffffffffffffffe;
                    }
                  }
                  ppcVar23 = ppcVar13;
                  if (*ppcVar11 == pcVar15) {
                    pcVar12 = ppcVar24[3];
                    iVar4 = strcmp(pcVar12,"lofs");
                    if ((iVar4 != 0) &&
                       (((ppcVar13 == (char **)0x0 || ((*(byte *)(ppcVar13 + 5) & 1) != 0)) ||
                        ((*(byte *)(ppcVar24 + 5) & 1) == 0)))) {
                      pcVar12 = ppcVar24[1];
                      iVar4 = stat(pcVar12,&local_d8);
                      if ((iVar4 != 0) ||
                         (ppcVar23 = ppcVar24, (char *)local_d8.st_dev != ppcVar24[4])) {
                        ppcVar24[4] = (char *)0xfffffffffffffffe;
                        ppcVar23 = ppcVar13;
                      }
                    }
                  }
                  ppcVar24 = (char **)ppcVar24[6];
                  ppcVar13 = ppcVar23;
                } while (ppcVar24 != (char **)0x0);
                if (ppcVar23 == (char **)0x0) goto LAB_00104966;
              }
              else {
                pcVar12 = ppcVar23[1];
                iVar4 = stat(pcVar12,&local_d8);
                if ((iVar4 != 0) || ((char *)local_d8.st_dev != *ppcVar11)) goto LAB_00104532;
              }
              get_dev(*ppcVar23,ppcVar23[1],pcVar9,pcVar9,ppcVar23[3],*(byte *)(ppcVar23 + 5) & 1,
                      *(byte *)(ppcVar23 + 5) >> 1 & 1,0,0,pcVar12);
              goto LAB_001046f9;
            }
            free(pcVar12);
          }
LAB_00104966:
          __ptr_00 = (void *)find_mount_point(pcVar9,ppcVar11);
          if (__ptr_00 != (void *)0x0) {
            get_dev(0,__ptr_00,pcVar9,0,0,0,0,0,0,pcVar19);
            free(__ptr_00);
          }
        }
LAB_001046f9:
        if ((ulong)(param_1 - 1) + lVar22 == local_130) break;
        lVar10 = local_130;
        local_130 = local_130 + 1;
      } while( true );
    }
  }
  if (file_systems_processed == '\0') {
    if (exit_status != 0) goto LAB_00104953;
    uVar7 = dcgettext(0,"no file systems processed",5);
    local_130 = error(1,0,uVar7);
  }
  if (print_grand_total != '\0') {
    pcVar9 = "-";
    if (field_data[44] == '\0') {
      pcVar9 = "total";
    }
    get_dev("total",pcVar9,0,0,0,0,0,grand_fsu,0,local_130);
  }
  for (uVar27 = 0; uVar27 < nrows; uVar27 = uVar27 + 1) {
    for (uVar25 = 0; uVar25 < ncolumns; uVar25 = uVar25 + 1) {
      uVar7 = *(undefined8 *)(*(long *)(table + uVar27 * 8) + uVar25 * 8);
      if (uVar25 != 0) {
        pcVar9 = stdout->_IO_write_ptr;
        if (pcVar9 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar9 + 1;
          *pcVar9 = ' ';
        }
        else {
          __overflow(stdout,0x20);
        }
      }
      lVar22 = *(long *)(columns + uVar25 * 8);
      local_f8 = *(char **)(lVar22 + 0x20);
      pcVar19 = (char *)ambsalign(uVar7,&local_f8,*(undefined4 *)(lVar22 + 0x28),
                                  (ulong)(ncolumns - 1 == uVar25) << 3);
      pcVar9 = pcVar19;
      if (pcVar19 == (char *)0x0) {
        pcVar9 = *(char **)(*(long *)(table + uVar27 * 8) + uVar25 * 8);
      }
      fputs_unlocked(pcVar9,stdout);
      free(pcVar19);
    }
    pcVar9 = stdout->_IO_write_ptr;
    if (pcVar9 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar9 + 1;
      *pcVar9 = '\n';
    }
    else {
      __overflow(stdout,10);
    }
  }
LAB_00104953:
                    /* WARNING: Subroutine does not return */
  exit(exit_status);
}