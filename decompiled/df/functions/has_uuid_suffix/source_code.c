has_uuid_suffix (char const *s)
{
  size_t len = strlen (s);
  return (36 < len
          && strspn (s + len - 36, "-0123456789abcdefABCDEF") == 36);
}