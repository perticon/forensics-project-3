devlist_for_dev (dev_t dev)
{
  if (devlist_table == NULL)
    return NULL;
  struct devlist dev_entry;
  dev_entry.dev_num = dev;

  struct devlist *found = hash_lookup (devlist_table, &dev_entry);
  if (found == NULL)
    return NULL;

  /* Return the last devlist entry we have seen with this dev_num */
  return found->seen_last;
}