void decode_output_arg(void)

{
  int iVar1;
  char *__ptr;
  char *pcVar2;
  undefined8 uVar3;
  char *pcVar4;
  uint uVar5;
  ulong uVar6;
  char *pcVar7;
  undefined1 *puVar8;
  char **ppcVar9;
  
  puVar8 = field_data;
  __ptr = (char *)xstrdup();
  pcVar7 = __ptr;
  do {
    pcVar4 = (char *)0x0;
    pcVar2 = strchr(pcVar7,0x2c);
    if (pcVar2 != (char *)0x0) {
      *pcVar2 = '\0';
      pcVar4 = pcVar2 + 1;
    }
    ppcVar9 = (char **)(field_data + 8);
    uVar6 = 0;
    do {
      iVar1 = strcmp(*ppcVar9,pcVar7);
      if (iVar1 == 0) goto LAB_001067a0;
      uVar5 = (int)uVar6 + 1;
      uVar6 = (ulong)uVar5;
      ppcVar9 = ppcVar9 + 6;
    } while (uVar5 != 0xc);
    puVar8 = (undefined1 *)quote(pcVar7);
    pcVar7 = "option --output: field %s unknown";
    while( true ) {
      uVar3 = dcgettext(0,pcVar7,5);
      error(0,0,uVar3,puVar8);
      usage(1);
LAB_001067a0:
      if (puVar8[uVar6 * 0x30 + 0x2c] == '\0') break;
      puVar8 = (undefined1 *)quote();
      pcVar7 = "option --output: field %s used more than once";
    }
    if ((int)uVar6 == 2) {
      alloc_field(2,&DAT_00112115);
    }
    else if ((int)uVar6 == 4) {
      alloc_field(4,"Avail");
    }
    else {
      alloc_field(uVar6,0);
    }
    pcVar7 = pcVar4;
  } while (pcVar4 != (char *)0x0);
  free(__ptr);
  return;
}