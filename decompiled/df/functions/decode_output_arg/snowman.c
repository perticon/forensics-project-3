void decode_output_arg(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    void** r12_8;
    void*** v9;
    void** v10;
    void* v11;
    void** rax12;
    void** r13_13;
    int64_t rax14;
    int32_t ebp15;
    void** r15_16;
    void** rsi17;
    void** rax18;
    int64_t v19;
    void** rbx20;
    int64_t v21;
    int64_t v22;
    struct s0* rax23;
    void*** r14_24;
    int64_t rax25;
    void** rax26;
    void** rax27;

    r12_8 = reinterpret_cast<void**>(0x18020);
    rax12 = xstrdup(rdi, rsi, rdx, rcx, r8, r9, v9, v10, v11);
    r13_13 = rax12;
    goto addr_6718_2;
    addr_67ee_3:
    while (1) {
        addr_67a0_5:
        *reinterpret_cast<int32_t*>(&rax14) = ebp15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_8) + (reinterpret_cast<uint64_t>(rax14 + rax14 * 2) << 4) + 44)) {
            rax18 = quote(r15_16, rsi17, rdx, rcx, r8, r9);
            r12_8 = rax18;
        } else {
            if (ebp15 == 2) {
                alloc_field(2, "Size", rdx, rcx, r8, r9, v19);
                if (!rbx20) 
                    goto addr_67ee_3;
            } else {
                if (ebp15 == 4) {
                    alloc_field(4, "Avail", rdx, rcx, r8, r9, v21);
                } else {
                    alloc_field(ebp15, 0, rdx, rcx, r8, r9, v22);
                }
                if (!rbx20) 
                    goto addr_67ee_3;
            }
            r13_13 = rbx20;
            addr_6718_2:
            *reinterpret_cast<int32_t*>(&rbx20) = 0;
            *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
            rax23 = fun_37b0(r13_13, r13_13);
            if (rax23) {
                rax23->f0 = 0;
                rbx20 = reinterpret_cast<void**>(&rax23->f1);
            }
            r14_24 = reinterpret_cast<void***>(0x18028);
            ebp15 = 0;
            do {
                r15_16 = *r14_24;
                rsi17 = r13_13;
                rax25 = fun_38c0(r15_16, rsi17, rdx, r15_16, rsi17, rdx);
                if (!*reinterpret_cast<int32_t*>(&rax25)) 
                    goto addr_67a0_5;
                ++ebp15;
                r14_24 = r14_24 + 48;
            } while (ebp15 != 12);
            rax26 = quote(r13_13, rsi17, rdx, rcx, r8, r9);
            r12_8 = rax26;
        }
        rax27 = fun_3730();
        rcx = r12_8;
        *reinterpret_cast<int32_t*>(&rsi17) = 0;
        *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
        rdx = rax27;
        fun_3a30();
        usage();
    }
}