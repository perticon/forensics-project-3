decode_output_arg (char const *arg)
{
  char *arg_writable = xstrdup (arg);
  char *s = arg_writable;
  do
    {
      /* find next comma */
      char *comma = strchr (s, ',');

      /* If we found a comma, put a NUL in its place and advance.  */
      if (comma)
        *comma++ = 0;

      /* process S.  */
      display_field_t field = INVALID_FIELD;
      for (unsigned int i = 0; i < ARRAY_CARDINALITY (field_data); i++)
        {
          if (STREQ (field_data[i].arg, s))
            {
              field = i;
              break;
            }
        }
      if (field == INVALID_FIELD)
        {
          error (0, 0, _("option --output: field %s unknown"), quote (s));
          usage (EXIT_FAILURE);
        }

      if (field_data[field].used)
        {
          /* Prevent the fields from being used more than once.  */
          error (0, 0, _("option --output: field %s used more than once"),
                 quote (field_data[field].arg));
          usage (EXIT_FAILURE);
        }

      switch (field)
        {
        case SOURCE_FIELD:
        case FSTYPE_FIELD:
        case USED_FIELD:
        case PCENT_FIELD:
        case ITOTAL_FIELD:
        case IUSED_FIELD:
        case IAVAIL_FIELD:
        case IPCENT_FIELD:
        case TARGET_FIELD:
        case FILE_FIELD:
          alloc_field (field, NULL);
          break;

        case SIZE_FIELD:
          alloc_field (field, N_("Size"));
          break;

        case AVAIL_FIELD:
          alloc_field (field, N_("Avail"));
          break;

        default:
          assert (!"invalid field");
        }
      s = comma;
    }
  while (s);

  free (arg_writable);
}