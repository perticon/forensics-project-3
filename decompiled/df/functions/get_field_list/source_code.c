get_field_list (void)
{
  switch (header_mode)
    {
    case DEFAULT_MODE:
      alloc_field (SOURCE_FIELD, NULL);
      if (print_type)
        alloc_field (FSTYPE_FIELD, NULL);
      alloc_field (SIZE_FIELD,   NULL);
      alloc_field (USED_FIELD,   NULL);
      alloc_field (AVAIL_FIELD,  NULL);
      alloc_field (PCENT_FIELD,  NULL);
      alloc_field (TARGET_FIELD, NULL);
      break;

    case HUMAN_MODE:
      alloc_field (SOURCE_FIELD, NULL);
      if (print_type)
        alloc_field (FSTYPE_FIELD, NULL);

      alloc_field (SIZE_FIELD,   N_("Size"));
      alloc_field (USED_FIELD,   NULL);
      alloc_field (AVAIL_FIELD,  N_("Avail"));
      alloc_field (PCENT_FIELD,  NULL);
      alloc_field (TARGET_FIELD, NULL);
      break;

    case INODES_MODE:
      alloc_field (SOURCE_FIELD, NULL);
      if (print_type)
        alloc_field (FSTYPE_FIELD, NULL);
      alloc_field (ITOTAL_FIELD,  NULL);
      alloc_field (IUSED_FIELD,   NULL);
      alloc_field (IAVAIL_FIELD,  NULL);
      alloc_field (IPCENT_FIELD,  NULL);
      alloc_field (TARGET_FIELD,  NULL);
      break;

    case POSIX_MODE:
      alloc_field (SOURCE_FIELD, NULL);
      if (print_type)
        alloc_field (FSTYPE_FIELD, NULL);
      alloc_field (SIZE_FIELD,   NULL);
      alloc_field (USED_FIELD,   NULL);
      alloc_field (AVAIL_FIELD,  NULL);
      alloc_field (PCENT_FIELD,  N_("Capacity"));
      alloc_field (TARGET_FIELD, NULL);
      break;

    case OUTPUT_MODE:
      if (!ncolumns)
        {
          /* Add all fields if --output was given without a field list.  */
          decode_output_arg (all_args_string);
        }
      break;

    default:
      assert (!"invalid header_mode");
    }
}