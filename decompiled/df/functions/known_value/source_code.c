known_value (uintmax_t n)
{
  return n < UINTMAX_MAX - 1;
}