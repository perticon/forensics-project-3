void alloc_table_row(void) {
    // 0x5290
    nrows++;
    int64_t v1 = xreallocarray(); // 0x52af
    *(int64_t *)&table = v1;
    char * v2 = xnmalloc(ncolumns, 8); // 0x52d3
    *(int64_t *)(v1 - 8 + 8 * nrows) = (int64_t)v2;
}