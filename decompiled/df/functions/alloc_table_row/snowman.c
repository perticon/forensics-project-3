void alloc_table_row(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rdi8;
    void** rax9;
    void** rdx10;
    uint64_t rdi11;
    void** rax12;

    rax7 = nrows;
    rdi8 = table;
    nrows = rax7 + 1;
    rax9 = xreallocarray(rdi8);
    rdx10 = nrows;
    rdi11 = ncolumns;
    table = rax9;
    rax12 = xnmalloc(rdi11, 8, rdx10, rcx, r8, r9);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rax9 + reinterpret_cast<unsigned char>(rdx10) * 8) - 8) = rax12;
    return;
}