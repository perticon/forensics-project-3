replace_control_chars (char *cell)
{
  char *p = cell;
  while (*p)
    {
      if (c_iscntrl (to_uchar (*p)))
        *p = '?';
      p++;
    }
}