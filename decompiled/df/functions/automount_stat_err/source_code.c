automount_stat_err (char const *file, struct stat *st)
{
  int fd = open (file, O_RDONLY | O_NOCTTY | O_NONBLOCK);
  if (fd < 0)
    {
      if (errno == ENOENT || errno == ENOTDIR)
        return errno;
      return stat (file, st) == 0 ? 0 : errno;
    }
  else
    {
      int err = fstat (fd, st) == 0 ? 0 : errno;
      close (fd);
      return err;
    }
}