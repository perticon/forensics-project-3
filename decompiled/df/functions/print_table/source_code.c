print_table (void)
{
  size_t row;

  for (row = 0; row < nrows; row++)
    {
      size_t col;
      for (col = 0; col < ncolumns; col++)
        {
          char *cell = table[row][col];

          /* Note the SOURCE_FIELD used to be displayed on it's own line
             if (!posix_format && mbswidth (cell) > 20), but that
             functionality was probably more problematic than helpful,
             hence changed in commit v8.10-40-g99679ff.  */
          if (col != 0)
            putchar (' ');

          int flags = 0;
          if (col == ncolumns - 1) /* The last one.  */
            flags = MBA_NO_RIGHT_PAD;

          size_t width = columns[col]->width;
          cell = ambsalign (cell, &width, columns[col]->align, flags);
          /* When ambsalign fails, output unaligned data.  */
          fputs (cell ? cell : table[row][col], stdout);
          free (cell);
        }
      putchar ('\n');
    }
}