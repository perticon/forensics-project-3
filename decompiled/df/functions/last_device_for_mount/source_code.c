last_device_for_mount (char const *mount)
{
  struct mount_entry const *me;
  struct mount_entry const *le = NULL;

  for (me = mount_list; me; me = me->me_next)
    {
      if (STREQ (me->me_mountdir, mount))
        le = me;
    }

  if (le)
    {
      char *devname = le->me_devname;
      char *canon_dev = canonicalize_file_name (devname);
      if (canon_dev && IS_ABSOLUTE_FILE_NAME (canon_dev))
        return canon_dev;
      free (canon_dev);
      return xstrdup (le->me_devname);
    }
  else
    return NULL;
}