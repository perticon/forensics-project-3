void replace_invalid_chars(char *param_1)

{
  char *pcVar1;
  int iVar2;
  size_t sVar3;
  ulong __n;
  char *pcVar4;
  char *__src;
  long in_FS_OFFSET;
  wint_t local_4c;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  sVar3 = strlen(param_1);
  local_48 = 0;
  pcVar1 = param_1 + sVar3;
  if (param_1 != pcVar1) {
    __src = param_1;
    do {
      while( true ) {
        __n = rpl_mbrtowc(&local_4c,__src,(long)pcVar1 - (long)__src,&local_48);
        if (__n <= (ulong)((long)pcVar1 - (long)__src)) break;
        __n = 1;
LAB_00105384:
        __src = __src + __n;
        *param_1 = '?';
        param_1 = param_1 + 1;
        local_48 = 0;
        if (pcVar1 == __src) goto LAB_0010539c;
      }
      iVar2 = iswcntrl(local_4c);
      if (iVar2 != 0) goto LAB_00105384;
      pcVar4 = __src + __n;
      memmove(param_1,__src,__n);
      param_1 = param_1 + __n;
      __src = pcVar4;
    } while (pcVar1 != pcVar4);
  }
LAB_0010539c:
  *param_1 = '\0';
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}