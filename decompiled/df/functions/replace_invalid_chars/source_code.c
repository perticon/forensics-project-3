replace_invalid_chars (char *cell)
{
  char *srcend = cell + strlen (cell);
  char *dst = cell;
  mbstate_t mbstate = { 0, };
  size_t n;

  for (char *src = cell; src != srcend; src += n)
    {
      wchar_t wc;
      size_t srcbytes = srcend - src;
      n = mbrtowc (&wc, src, srcbytes, &mbstate);
      bool ok = n <= srcbytes;

      if (ok)
        ok = !iswcntrl (wc);
      else
        n = 1;

      if (ok)
        {
          memmove (dst, src, n);
          dst += n;
        }
      else
        {
          *dst++ = '?';
          memset (&mbstate, 0, sizeof mbstate);
        }
    }

  *dst = '\0';
}