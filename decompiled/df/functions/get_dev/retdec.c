void get_dev(char * device, char * mount_point, char * file, char * stat_file, char * fstype, bool me_dummy, bool me_remote, int32_t * force_fsu, bool process_all) {
    int64_t v1 = __readfsqword(40); // 0x56ef
    if (!me_remote) {
        goto lab_0x5710;
    } else {
        // 0x5707
        if (*(char *)&show_local_fs != 0) {
            goto lab_0x574e;
        } else {
            goto lab_0x5710;
        }
    }
  lab_0x57d2:;
    int64_t v2 = (int64_t)mount_point;
    int64_t v3; // 0x56b0
    int64_t v4; // 0x56b0
    char v5; // 0x56b0
    int64_t v6; // 0x56b0
    int64_t v7; // 0x56b0
    int64_t v8; // bp-1064, 0x56b0
    char * v9; // 0x56b0
    int128_t v10; // 0x56b0
    if (mount_point == NULL || force_fsu != NULL) {
        int64_t v11 = mount_point != NULL ? v2 : (int64_t)device;
        v9 = stat_file == NULL ? (char *)v11 : stat_file;
        if (force_fsu == NULL) {
            goto lab_0x580b;
        } else {
            int64_t v12 = (int64_t)force_fsu;
            int128_t v13 = __asm_movdqa(*(int128_t *)force_fsu); // 0x5a7d
            int128_t v14 = __asm_movdqa(*(int128_t *)(v12 + 16)); // 0x5a81
            int128_t v15 = __asm_movdqa(*(int128_t *)(v12 + 32)); // 0x5a86
            int64_t v16 = *(int64_t *)(v12 + 48); // 0x5a8b
            v8 = __asm_movaps(v13);
            int64_t v17 = v10; // 0x5a94
            int64_t v18 = __asm_movaps(v14); // 0x5a9f
            char v19 = __asm_movaps(v15); // 0x5aa4
            int64_t v20 = v10;
            v3 = v17;
            v4 = v16;
            v6 = v20;
            v7 = v18;
            v5 = v19;
            if (v17 != 0) {
                goto lab_0x585e;
            } else {
                // 0x5aaf
                v3 = v17;
                v4 = v16;
                v6 = v20;
                v7 = v18;
                v5 = v19;
                if ((*(char *)&show_listed_fs | *(char *)&show_all_fs) == 0) {
                    goto lab_0x574e;
                } else {
                    goto lab_0x585e;
                }
            }
        }
    } else {
        int64_t v21; // 0x56b0
        if ((char)v21 != 47) {
            goto lab_0x574e;
        } else {
            int64_t v22 = stat_file == NULL ? v2 : (int64_t)stat_file; // 0x5802
            v9 = (char *)v22;
            goto lab_0x580b;
        }
    }
  lab_0x574e:
    // 0x574e
    if (v1 != __readfsqword(40)) {
        // 0x6245
        function_3780();
        return;
    }
  lab_0x5710:
    if (me_dummy) {
        // 0x5780
        if (*(char *)&show_all_fs != 0) {
            goto lab_0x5715;
        } else {
            // 0x5789
            if (*(char *)&show_listed_fs == 0) {
                goto lab_0x574e;
            } else {
                // 0x5792
                if (fs_select_list != NULL) {
                    goto lab_0x5721;
                } else {
                    goto lab_0x57a0;
                }
            }
        }
    } else {
        goto lab_0x5715;
    }
  lab_0x5715:
    // 0x5715
    if (fs_select_list == NULL) {
        goto lab_0x57a0;
    } else {
        goto lab_0x5721;
    }
  lab_0x57a0:
    // 0x57a0
    if (fstype == NULL || fs_exclude_list == NULL) {
        goto lab_0x57d2;
    } else {
        goto lab_0x57b8;
    }
  lab_0x5721:
    if (fstype == NULL) {
        goto lab_0x57d2;
    } else {
        int64_t v23 = (int64_t)fs_select_list; // 0x5744
        while ((int32_t)function_38c0() != 0) {
            // 0x5744
            v23 += 8;
            if (v23 == 0) {
                goto lab_0x574e;
            }
        }
        // 0x5a50
        if (fs_exclude_list != NULL) {
            goto lab_0x57b8;
        } else {
            goto lab_0x57d2;
        }
    }
  lab_0x57b8:;
    int64_t v24 = (int64_t)fs_exclude_list; // 0x56b0
    while ((int32_t)function_38c0() != 0) {
        int64_t v25 = *(int64_t *)(v24 + 8); // 0x57c8
        v24 = v25;
        if (v25 == 0) {
            goto lab_0x57d2;
        }
    }
    goto lab_0x574e;
  lab_0x580b:;
    // 0x580b
    int64_t v26; // 0x56b0
    int64_t v27; // 0x56b0
    int64_t v28; // 0x56b0
    int64_t v29; // 0x56b0
    int64_t v30; // 0x56b0
    char v31; // 0x56b0
    int64_t v32; // 0x56b0
    int64_t v33; // 0x56b0
    int64_t v34; // 0x56b0
    if (get_fs_usage(v9, device, (int32_t *)&v8) != 0) {
        int64_t v35 = function_3630(); // 0x5f6f
        if (!process_all) {
            goto lab_0x5f92;
        } else {
            // 0x5f7e
            switch (*(int32_t *)v35) {
                case 13: {
                    goto lab_0x618a;
                }
                case 2: {
                    goto lab_0x618a;
                }
                default: {
                    goto lab_0x5f92;
                }
            }
        }
    } else {
        // 0x5825
        v26 = v10;
        v27 = v10;
        v28 = v10;
        if (!process_all) {
            goto lab_0x616c;
        } else {
            // 0x5830
            if (*(char *)&show_all_fs != 0) {
                // 0x60af
                if (devlist_table == NULL || (int32_t)function_38f0() != 0) {
                    goto lab_0x616c;
                } else {
                    int64_t v36 = hash_lookup(); // 0x60f1
                    if (v36 == 0) {
                        goto lab_0x616c;
                    } else {
                        int64_t v37 = *(int64_t *)(v36 + 24); // 0x60fb
                        if (v37 == 0) {
                            goto lab_0x616c;
                        } else {
                            int64_t v38 = *(int64_t *)(v37 + 8); // 0x6104
                            if (v38 == 0) {
                                goto lab_0x616c;
                            } else {
                                // 0x610d
                                if ((int32_t)function_38c0() == 0) {
                                    goto lab_0x616c;
                                } else {
                                    // 0x611d
                                    if (me_remote == ((*(char *)(v38 + 40) & 2) != 0)) {
                                        goto lab_0x616c;
                                    } else {
                                        // 0x616c
                                        v8 = -1;
                                        v34 = -1;
                                        v30 = -1;
                                        v29 = -1;
                                        v32 = -1;
                                        v33 = -1;
                                        v31 = 0;
                                        goto lab_0x5857;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                // 0x583d
                v34 = v26;
                v32 = v27;
                v33 = v28;
                if (v26 != 0) {
                    goto lab_0x5857;
                } else {
                    goto lab_0x5845;
                }
            }
        }
    }
  lab_0x585e:
    // 0x585e
    alloc_table_row();
    char * v39 = xstrdup(device == NULL ? (char *)&g18 : device); // 0x5885
    if (process_all) {
        // 0x5899
        if (function_3750() >= 37) {
            // 0x58a7
            if (function_3850() == 36) {
                // 0x5fc7
                if (canonicalize_filename_mode(v39, 0) != NULL) {
                    // 0x5fdf
                    function_35f0();
                }
            }
        }
    }
    int64_t v40 = v8; // 0x59e0
    int64_t v41; // 0x56b0
    int64_t v42; // 0x56b0
    int64_t v43; // 0x56b0
    int64_t v44; // 0x56b0
    int64_t v45; // 0x56b0
    int64_t v46; // 0x56b0
    int64_t v47; // 0x56b0
    if (force_fsu == NULL == (*(char *)&print_grand_total != 0)) {
        if (v46 < 0xfffffffffffffffe) {
            // 0x600d
            g44 += v46;
        }
        if (v4 < 0xfffffffffffffffe) {
            // 0x601a
            g45 += v4;
        }
        if (v3 < 0xfffffffffffffffe) {
            // 0x6027
            g40 += v8 * v3;
        }
        if (v7 < 0xfffffffffffffffe) {
            // 0x6038
            g41 += v8 * v7;
        }
        // 0x6043
        v40 = v8;
        if (v6 < 0xfffffffffffffffe) {
            unsigned char v48 = v5 & (char)(v6 < 0xfffffffffffffffe); // 0x598c
            unsigned char v49 = g43; // 0x604d
            int64_t v50 = v8 * v6; // 0x6054
            int64_t v51 = g42; // 0x6058
            if (v49 == v48) {
                int64_t v52 = v51 + v50; // 0x6236
                g42 = v52;
                v40 = v52;
                goto lab_0x59ed;
            } else {
                int64_t v53 = v49 != 0 ? -v51 : v51; // 0x606f
                int64_t v54 = v48 != 0 ? -v50 : v50; // 0x607b
                v43 = v53;
                v45 = v48;
                v42 = v54;
                if (v53 <= v54) {
                    goto lab_0x621f;
                } else {
                    // 0x6088
                    v44 = v49;
                    v41 = v54;
                    v47 = v53 - v54;
                    goto lab_0x6092;
                }
            }
        } else {
            goto lab_0x59ed;
        }
    } else {
        goto lab_0x59ed;
    }
  lab_0x5f92:
    // 0x5f92
    quotearg_n_style_colon();
    function_3a30();
    exit_status = 1;
    goto lab_0x574e;
  lab_0x616c:
    // 0x616c
    if (v26 != 0) {
        goto lab_0x5857;
    } else {
        // 0x6178
        v34 = v26;
        v32 = v27;
        v33 = v28;
        if (*(char *)&show_all_fs != 0) {
            goto lab_0x5857;
        } else {
            goto lab_0x5845;
        }
    }
  lab_0x59ed:
    // 0x59ed
    if (ncolumns == 0) {
        // 0x5b77
        if (v1 != __readfsqword(40)) {
            // 0x6245
            function_3780();
            return;
        }
        // 0x5b8e
        function_35f0();
        return;
    }
    int64_t v55 = (int64_t)*columns;
    int64_t v56 = v55; // 0x56b0
    if (*(int32_t *)(v55 + 16) >= 3) {
        // 0x5cec
        function_3800();
        v56 = (int64_t)&g54;
    }
    // 0x5a33
    if (*(int32_t *)v56 < 12) {
        // 0x5765
        return;
    }
    // 0x6200
    v43 = function_3800();
    v45 = &g55;
    v42 = v40;
    goto lab_0x621f;
  lab_0x618a:
    // 0x618a
    if (*(char *)&show_all_fs == 0) {
        goto lab_0x574e;
    } else {
        // 0x6197
        v8 = -1;
        v34 = -1;
        v30 = -1;
        v29 = -1;
        v32 = -1;
        v33 = -1;
        v31 = 0;
        goto lab_0x5857;
    }
  lab_0x5857:
    // 0x5857
    *(char *)&file_systems_processed = 1;
    v3 = v34;
    v46 = v30;
    v4 = v29;
    v6 = v32;
    v7 = v33;
    v5 = v31;
    goto lab_0x585e;
  lab_0x621f:;
    int64_t v57 = v42 - v43; // 0x621f
    g43 = v45;
    v44 = v45 & 0xffffffff;
    v41 = v57;
    v47 = v57;
    goto lab_0x6092;
  lab_0x5845:
    // 0x5845
    v34 = v26;
    v32 = v27;
    v33 = v28;
    if (*(char *)&show_listed_fs == 0) {
        goto lab_0x574e;
    } else {
        goto lab_0x5857;
    }
  lab_0x6092:;
    int64_t v58 = v47;
    int64_t v59 = v41;
    g42 = v58;
    v40 = v59;
    if ((char)v44 != 0) {
        // 0x609a
        g42 = -v58;
        v40 = v59;
    }
    goto lab_0x59ed;
}