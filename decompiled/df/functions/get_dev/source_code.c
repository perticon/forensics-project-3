get_dev (char const *device, char const *mount_point, char const *file,
         char const *stat_file, char const *fstype,
         bool me_dummy, bool me_remote,
         const struct fs_usage *force_fsu,
         bool process_all)
{
  if (me_remote && show_local_fs)
    return;

  if (me_dummy && !show_all_fs && !show_listed_fs)
    return;

  if (!selected_fstype (fstype) || excluded_fstype (fstype))
    return;

  /* Ignore relative MOUNT_POINTs, which are present for example
     in /proc/mounts on Linux with network namespaces.  */
  if (!force_fsu && mount_point && ! IS_ABSOLUTE_FILE_NAME (mount_point))
    return;

  /* If MOUNT_POINT is NULL, then the file system is not mounted, and this
     program reports on the file system that the special file is on.
     It would be better to report on the unmounted file system,
     but statfs doesn't do that on most systems.  */
  if (!stat_file)
    stat_file = mount_point ? mount_point : device;

  struct fs_usage fsu;
  if (force_fsu)
    fsu = *force_fsu;
  else if (get_fs_usage (stat_file, device, &fsu))
    {
      /* If we can't access a system provided entry due
         to it not being present (now), or due to permissions,
         just output placeholder values rather than failing.  */
      if (process_all && (errno == EACCES || errno == ENOENT))
        {
          if (! show_all_fs)
            return;

          fstype = "-";
          fsu.fsu_bavail_top_bit_set = false;
          fsu.fsu_blocksize = fsu.fsu_blocks = fsu.fsu_bfree =
          fsu.fsu_bavail = fsu.fsu_files = fsu.fsu_ffree = UINTMAX_MAX;
        }
      else
        {
          error (0, errno, "%s", quotef (stat_file));
          exit_status = EXIT_FAILURE;
          return;
        }
    }
  else if (process_all && show_all_fs)
    {
      /* Ensure we don't output incorrect stats for over-mounted directories.
         Discard stats when the device name doesn't match.  Though don't
         discard when used and current mount entries are both remote due
         to the possibility of aliased host names or exports.  */
      struct stat sb;
      if (stat (stat_file, &sb) == 0)
        {
          struct mount_entry const * dev_me = me_for_dev (sb.st_dev);
          if (dev_me && ! STREQ (dev_me->me_devname, device)
              && (! dev_me->me_remote || ! me_remote))
            {
              fstype = "-";
              fsu.fsu_bavail_top_bit_set = false;
              fsu.fsu_blocksize = fsu.fsu_blocks = fsu.fsu_bfree =
              fsu.fsu_bavail = fsu.fsu_files = fsu.fsu_ffree = UINTMAX_MAX;
            }
        }
    }

  if (fsu.fsu_blocks == 0 && !show_all_fs && !show_listed_fs)
    return;

  if (! force_fsu)
    file_systems_processed = true;

  alloc_table_row ();

  if (! device)
    device = "-";		/* unknown */

  if (! file)
    file = "-";			/* unspecified */

  char *dev_name = xstrdup (device);
  char *resolved_dev;

  /* On some systems, dev_name is a long-named symlink like
     /dev/disk/by-uuid/828fc648-9f30-43d8-a0b1-f7196a2edb66 pointing to a
     much shorter and more useful name like /dev/sda1.  It may also look
     like /dev/mapper/luks-828fc648-9f30-43d8-a0b1-f7196a2edb66 and point to
     /dev/dm-0.  When process_all is true and dev_name is a symlink whose
     name ends with a UUID use the resolved name instead.  */
  if (process_all
      && has_uuid_suffix (dev_name)
      && (resolved_dev = canonicalize_filename_mode (dev_name, CAN_EXISTING)))
    {
      free (dev_name);
      dev_name = resolved_dev;
    }

  if (! fstype)
    fstype = "-";		/* unknown */

  struct field_values_t block_values;
  struct field_values_t inode_values;
  get_field_values (&block_values, &inode_values, &fsu);

  /* Add to grand total unless processing grand total line.  */
  if (print_grand_total && ! force_fsu)
    add_to_grand_total (&block_values, &inode_values);

  size_t col;
  for (col = 0; col < ncolumns; col++)
    {
      char buf[LONGEST_HUMAN_READABLE + 2];
      char *cell;

      struct field_values_t *v;
      switch (columns[col]->field_type)
        {
        case BLOCK_FLD:
          v = &block_values;
          break;
        case INODE_FLD:
          v = &inode_values;
          break;
        case OTHER_FLD:
          v = NULL;
          break;
        default:
          v = NULL; /* Avoid warnings where assert() is not __noreturn__.  */
          assert (!"bad field_type");
        }

      switch (columns[col]->field)
        {
        case SOURCE_FIELD:
          cell = xstrdup (dev_name);
          break;

        case FSTYPE_FIELD:
          cell = xstrdup (fstype);
          break;

        case SIZE_FIELD:
        case ITOTAL_FIELD:
          cell = xstrdup (df_readable (false, v->total, buf,
                                       v->input_units, v->output_units));
          break;

        case USED_FIELD:
        case IUSED_FIELD:
          cell = xstrdup (df_readable (v->negate_used, v->used, buf,
                                       v->input_units, v->output_units));
          break;

        case AVAIL_FIELD:
        case IAVAIL_FIELD:
          cell = xstrdup (df_readable (v->negate_available, v->available, buf,
                                       v->input_units, v->output_units));
          break;

        case PCENT_FIELD:
        case IPCENT_FIELD:
          {
            double pct = -1;
            if (! known_value (v->used) || ! known_value (v->available))
              ;
            else if (!v->negate_used
                     && v->used <= TYPE_MAXIMUM (uintmax_t) / 100
                     && v->used + v->available != 0
                     && (v->used + v->available < v->used)
                     == v->negate_available)
              {
                uintmax_t u100 = v->used * 100;
                uintmax_t nonroot_total = v->used + v->available;
                pct = u100 / nonroot_total + (u100 % nonroot_total != 0);
              }
            else
              {
                /* The calculation cannot be done easily with integer
                   arithmetic.  Fall back on floating point.  This can suffer
                   from minor rounding errors, but doing it exactly requires
                   multiple precision arithmetic, and it's not worth the
                   aggravation.  */
                double u = v->negate_used ? - (double) - v->used : v->used;
                double a = v->negate_available
                           ? - (double) - v->available : v->available;
                double nonroot_total = u + a;
                if (nonroot_total)
                  {
                    long int lipct = pct = u * 100 / nonroot_total;
                    double ipct = lipct;

                    /* Like 'pct = ceil (dpct);', but avoid ceil so that
                       the math library needn't be linked.  */
                    if (ipct - 1 < pct && pct <= ipct + 1)
                      pct = ipct + (ipct < pct);
                  }
              }

            if (0 <= pct)
              {
                if (asprintf (&cell, "%.0f%%", pct) == -1)
                  cell = NULL;
              }
            else
              cell = strdup ("-");

            if (!cell)
              xalloc_die ();

            break;
          }

        case FILE_FIELD:
          cell = xstrdup (file);
          break;

        case TARGET_FIELD:
#ifdef HIDE_AUTOMOUNT_PREFIX
          /* Don't print the first directory name in MOUNT_POINT if it's an
             artifact of an automounter.  This is a bit too aggressive to be
             the default.  */
          if (STRNCMP_LIT (mount_point, "/auto/") == 0)
            mount_point += 5;
          else if (STRNCMP_LIT (mount_point, "/tmp_mnt/") == 0)
            mount_point += 8;
#endif
          cell = xstrdup (mount_point);
          break;

        default:
          assert (!"unhandled field");
        }

      if (!cell)
        assert (!"empty cell");

      replace_problematic_chars (cell);
      size_t cell_width = mbswidth (cell, 0);
      columns[col]->width = MAX (columns[col]->width, cell_width);
      table[nrows - 1][col] = cell;
    }
  free (dev_name);
}