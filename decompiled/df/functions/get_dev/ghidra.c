void get_dev(char *param_1,char *param_2,undefined *param_3,char *param_4,char *param_5,char param_6
            ,char param_7,__dev_t *param_8,char param_9)

{
  long lVar1;
  char **ppcVar2;
  uint uVar3;
  uint *puVar4;
  long lVar5;
  ulong uVar6;
  byte bVar7;
  int iVar8;
  undefined *puVar9;
  size_t sVar10;
  code *pcVar11;
  int *piVar12;
  undefined8 uVar13;
  char *pcVar14;
  long lVar15;
  __dev_t *p_Var16;
  ulong uVar17;
  ulong uVar18;
  undefined *puVar19;
  __dev_t _Var20;
  __dev_t _Var21;
  ulong uVar22;
  char **ppcVar23;
  long in_FS_OFFSET;
  double dVar24;
  double dVar25;
  char *local_448;
  char *local_430;
  __dev_t local_428;
  ulong uStack1056;
  ulong local_418;
  ulong uStack1040;
  uint local_408;
  undefined4 uStack1028;
  ulong uStack1024;
  ulong local_3f8;
  __dev_t local_3e8;
  undefined8 local_3e0;
  ulong local_3d8;
  ulong local_3d0;
  byte local_3c8 [8];
  ulong local_3c0;
  long local_3b8;
  undefined local_3b0 [8];
  __dev_t local_3a8;
  undefined8 local_3a0;
  ulong local_398;
  ulong local_390;
  undefined local_388;
  ulong local_380;
  long local_378;
  undefined local_370;
  stat local_368;
  undefined local_2d8 [664];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((param_7 == '\0') || (show_local_fs == '\0')) {
    if ((param_6 == '\0') || (show_all_fs != '\0')) {
      if (fs_select_list != (char **)0x0) goto LAB_00105721;
LAB_001057a0:
      if ((fs_exclude_list != (char **)0x0) && (ppcVar23 = fs_exclude_list, param_5 != (char *)0x0))
      {
LAB_001057b8:
        do {
          iVar8 = strcmp(param_5,*ppcVar23);
          if (iVar8 == 0) goto LAB_0010574e;
          ppcVar2 = ppcVar23 + 1;
          ppcVar23 = (char **)*ppcVar2;
        } while ((char **)*ppcVar2 != (char **)0x0);
      }
LAB_001057d2:
      local_448 = param_4;
      if ((param_2 == (char *)0x0) || (param_8 != (__dev_t *)0x0)) {
        if ((param_4 == (char *)0x0) && (local_448 = param_1, param_2 != (char *)0x0)) {
          local_448 = param_2;
        }
        if (param_8 == (__dev_t *)0x0) goto LAB_0010580b;
        local_428 = *param_8;
        uStack1056 = param_8[1];
        local_418 = param_8[2];
        uStack1040 = param_8[3];
        local_408 = *(uint *)(param_8 + 4);
        uStack1028 = *(undefined4 *)((long)param_8 + 0x24);
        uStack1024 = param_8[5];
        local_3f8 = param_8[6];
        if (((uStack1056 == 0) && (show_all_fs == '\0')) && (show_listed_fs == '\0'))
        goto LAB_0010574e;
      }
      else {
        if (*param_2 != '/') goto LAB_0010574e;
        if (param_4 == (char *)0x0) {
          local_448 = param_2;
        }
LAB_0010580b:
        iVar8 = get_fs_usage(local_448,param_1,&local_428);
        if (iVar8 != 0) {
          piVar12 = __errno_location();
          if ((param_9 == '\0') || ((*piVar12 != 0xd && (*piVar12 != 2)))) {
            uVar13 = quotearg_n_style_colon(0,3,local_448);
            error(0,*piVar12,&DAT_00113bba,uVar13);
            exit_status = 1;
          }
          else if (show_all_fs != '\0') {
            local_408 = local_408 & 0xffffff00;
            param_5 = "-";
            local_3f8 = 0xffffffffffffffff;
            uStack1024 = 0xffffffffffffffff;
            uStack1040 = 0xffffffffffffffff;
            local_418 = 0xffffffffffffffff;
            uStack1056 = 0xffffffffffffffff;
            local_428 = 0xffffffffffffffff;
            goto LAB_00105857;
          }
          goto LAB_0010574e;
        }
        if (param_9 == '\0') {
LAB_0010616c:
          if ((uStack1056 == 0) && (show_all_fs == '\0')) goto LAB_00105845;
        }
        else {
          if (show_all_fs != '\0') {
            iVar8 = stat(local_448,&local_368);
            if ((iVar8 == 0) && (devlist_table != 0)) {
              local_3a8 = local_368.st_dev;
              lVar15 = hash_lookup(devlist_table,&local_3a8);
              if (((lVar15 != 0) &&
                  (((*(long *)(lVar15 + 0x18) != 0 &&
                    (ppcVar23 = *(char ***)(*(long *)(lVar15 + 0x18) + 8), ppcVar23 != (char **)0x0)
                    ) && (iVar8 = strcmp(*ppcVar23,param_1), iVar8 != 0)))) &&
                 (((*(byte *)(ppcVar23 + 5) & 2) == 0 || (param_7 == '\0')))) {
                local_408 = local_408 & 0xffffff00;
                param_5 = "-";
                local_3f8 = 0xffffffffffffffff;
                uStack1024 = 0xffffffffffffffff;
                uStack1040 = 0xffffffffffffffff;
                local_418 = 0xffffffffffffffff;
                uStack1056 = 0xffffffffffffffff;
                local_428 = 0xffffffffffffffff;
              }
            }
            goto LAB_0010616c;
          }
          if (uStack1056 != 0) goto LAB_00105857;
LAB_00105845:
          if (show_listed_fs == '\0') goto LAB_0010574e;
          if (param_8 != (__dev_t *)0x0) goto LAB_0010585e;
        }
LAB_00105857:
        file_systems_processed = 1;
      }
LAB_0010585e:
      alloc_table_row();
      if (param_1 == (char *)0x0) {
        param_1 = "-";
      }
      puVar9 = &DAT_00113a19;
      if (param_3 != (undefined *)0x0) {
        puVar9 = param_3;
      }
      local_448 = (char *)xstrdup(param_1);
      if ((((param_9 != '\0') && (sVar10 = strlen(local_448), 0x24 < sVar10)) &&
          (sVar10 = strspn(local_448 + (sVar10 - 0x24),"-0123456789abcdefABCDEF"), sVar10 == 0x24))
         && (pcVar14 = (char *)canonicalize_filename_mode(local_448,0), pcVar14 != (char *)0x0)) {
        free(local_448);
        local_448 = pcVar14;
      }
      local_3a0 = 1;
      if (param_5 == (char *)0x0) {
        param_5 = "-";
      }
      local_3a8 = 1;
      local_398 = uStack1024;
      local_380 = local_3f8;
      local_390 = local_3f8;
      local_388 = 0;
      local_378 = -1;
      local_370 = false;
      if ((local_3f8 < 0xfffffffffffffffe) && (uStack1024 < 0xfffffffffffffffe)) {
        local_378 = uStack1024 - local_3f8;
        local_370 = uStack1024 < local_3f8;
      }
      local_3b8 = -1;
      local_3b0[0] = false;
      local_3e0 = output_block_size;
      local_3c8[0] = uStack1040 < 0xfffffffffffffffe & (byte)local_408;
      local_3e8 = local_428;
      local_3d8 = uStack1056;
      local_3d0 = uStack1040;
      local_3c0 = local_418;
      if ((local_418 < 0xfffffffffffffffe) && (uStack1056 < 0xfffffffffffffffe)) {
        local_3b8 = uStack1056 - local_418;
        local_3b0[0] = uStack1056 < local_418;
      }
      if ((print_grand_total != '\0') && (param_8 == (__dev_t *)0x0)) {
        if (uStack1024 < 0xfffffffffffffffe) {
          grand_fsu._40_8_ = grand_fsu._40_8_ + uStack1024;
        }
        if (local_3f8 < 0xfffffffffffffffe) {
          grand_fsu._48_8_ = grand_fsu._48_8_ + local_3f8;
        }
        if (uStack1056 < 0xfffffffffffffffe) {
          grand_fsu._8_8_ = grand_fsu._8_8_ + uStack1056 * local_428;
        }
        if (local_418 < 0xfffffffffffffffe) {
          grand_fsu._16_8_ = grand_fsu._16_8_ + local_418 * local_428;
        }
        if (uStack1040 < 0xfffffffffffffffe) {
          uVar22 = local_428 * uStack1040;
          if (grand_fsu[32] == local_3c8[0]) {
            grand_fsu._24_8_ = uVar22 + grand_fsu._24_8_;
          }
          else {
            if (grand_fsu[32] != 0) {
              grand_fsu._24_8_ = -grand_fsu._24_8_;
            }
            if (local_3c8[0] != 0) {
              uVar22 = -uVar22;
            }
            if (uVar22 < grand_fsu._24_8_) {
              grand_fsu._24_8_ = grand_fsu._24_8_ - uVar22;
            }
            else {
              grand_fsu._24_8_ = uVar22 - grand_fsu._24_8_;
              grand_fsu[32] = local_3c8[0];
            }
            if (grand_fsu[32] != 0) {
              grand_fsu._24_8_ = -grand_fsu._24_8_;
            }
          }
        }
      }
      uVar22 = 0;
      lVar15 = columns;
      if (ncolumns != 0) {
        do {
          puVar4 = *(uint **)(lVar15 + uVar22 * 8);
          lVar1 = uVar22 * 8;
          uVar3 = puVar4[4];
          if (uVar3 == 1) {
            p_Var16 = &local_3a8;
          }
          else if (uVar3 == 2) {
            p_Var16 = (__dev_t *)0x0;
          }
          else {
            if (uVar3 != 0) {
                    /* WARNING: Subroutine does not return */
              __assert_fail("!\"bad field_type\"","src/df.c",0x480,"get_dev");
            }
            p_Var16 = &local_3e8;
          }
          if (0xb < *puVar4) {
                    /* WARNING: Subroutine does not return */
            __assert_fail("!\"unhandled field\"","src/df.c",0x4e6,"get_dev");
          }
          switch(*puVar4) {
          case 0:
            pcVar14 = (char *)xstrdup(local_448);
            break;
          case 1:
            pcVar14 = (char *)xstrdup(param_5);
            break;
          default:
            puVar19 = &DAT_00113a19;
            if (p_Var16[2] < 0xfffffffffffffffe) {
              puVar19 = (undefined *)
                        human_readable(p_Var16[2],local_2d8,human_output_opts,*p_Var16,p_Var16[1]);
            }
            goto LAB_00105bd8;
          case 3:
          case 7:
            uVar18 = p_Var16[6];
            _Var20 = p_Var16[1];
            _Var21 = *p_Var16;
            bVar7 = *(byte *)(p_Var16 + 7);
            if (0xfffffffffffffffd < uVar18) goto LAB_00105bc5;
LAB_00105c25:
            puVar19 = local_2d8 + bVar7;
            if (bVar7 == 0) {
              puVar19 = (undefined *)human_readable(uVar18,puVar19,human_output_opts,_Var21,_Var20);
              goto LAB_00105bd8;
            }
LAB_00105d3e:
            lVar15 = human_readable(-uVar18,puVar19,human_output_opts,_Var21,_Var20);
            *(undefined *)(lVar15 + -1) = 0x2d;
            puVar19 = (undefined *)(lVar15 + -1);
            goto LAB_00105bd8;
          case 4:
          case 8:
            uVar18 = p_Var16[3];
            _Var20 = p_Var16[1];
            _Var21 = *p_Var16;
            bVar7 = *(byte *)(p_Var16 + 4);
            if (uVar18 < 0xfffffffffffffffe) goto LAB_00105c25;
LAB_00105bc5:
            if (bVar7 != 0) {
              puVar19 = local_2d8 + 1;
              goto LAB_00105d3e;
            }
            puVar19 = &DAT_00113a19;
            goto LAB_00105bd8;
          case 5:
          case 9:
            uVar18 = p_Var16[6];
            if ((uVar18 < 0xfffffffffffffffe) && (uVar6 = p_Var16[3], uVar6 < 0xfffffffffffffffe)) {
              if (*(char *)(p_Var16 + 7) == '\0') {
                if (0x28f5c28f5c28f5c < uVar18) {
LAB_00105da0:
                  if ((long)uVar18 < 0) {
                    dVar24 = (double)(uVar18 >> 1 | (ulong)((uint)uVar18 & 1));
                    dVar24 = dVar24 + dVar24;
                  }
                  else {
                    dVar24 = (double)uVar18;
                  }
                  goto LAB_00105db2;
                }
                uVar17 = uVar18 + uVar6;
                if ((uVar17 == 0) || ((bool)*(char *)(p_Var16 + 4) != CARRY8(uVar18,uVar6)))
                goto LAB_00105da0;
                uVar18 = ((uVar18 * 100) / uVar17 + 1) - (ulong)((uVar18 * 100) % uVar17 == 0);
                if ((long)uVar18 < 0) {
                  dVar24 = (double)(uVar18 >> 1 | (ulong)((uint)uVar18 & 1));
                  dVar24 = dVar24 + dVar24;
                }
                else {
                  dVar24 = (double)uVar18;
                }
              }
              else {
                uVar17 = -uVar18;
                if ((long)uVar18 < 1) {
                  dVar24 = (double)uVar17;
                }
                else {
                  dVar24 = (double)(uVar17 >> 1 | (ulong)((uint)uVar17 & 1));
                  dVar24 = dVar24 + dVar24;
                }
                dVar24 = (double)((ulong)dVar24 ^ _DAT_00112f90);
LAB_00105db2:
                if (*(char *)(p_Var16 + 4) == '\0') {
                  if ((long)uVar6 < 0) {
                    dVar25 = (double)(uVar6 >> 1 | (ulong)((uint)uVar6 & 1));
                    dVar25 = dVar25 + dVar25;
                  }
                  else {
                    dVar25 = (double)uVar6;
                  }
                }
                else {
                  uVar18 = -uVar6;
                  if ((long)uVar6 < 1) {
                    dVar25 = (double)uVar18;
                  }
                  else {
                    dVar25 = (double)(uVar18 >> 1 | (ulong)((uint)uVar18 & 1));
                    dVar25 = dVar25 + dVar25;
                  }
                  dVar25 = (double)((ulong)dVar25 ^ _DAT_00112f90);
                }
                if (dVar25 + dVar24 == 0.0) goto LAB_00105ade;
                dVar24 = (dVar24 * _DAT_00112fa0) / (dVar25 + dVar24);
                dVar25 = (double)(long)dVar24;
                if (((dVar25 - _DAT_00112fa8 < dVar24) && (dVar24 <= _DAT_00112fa8 + dVar25)) &&
                   (dVar24 = _DAT_00112fa8 + dVar25, dVar24 <= dVar25)) {
                  dVar24 = dVar25 + 0.0;
                }
              }
              if (dVar24 < 0.0) goto LAB_00105ade;
              iVar8 = rpl_asprintf(&local_430,"%.0f%%");
              if (iVar8 == -1) {
                local_430 = (char *)0x0;
                goto LAB_00105e69;
              }
            }
            else {
LAB_00105ade:
              local_430 = strdup("-");
            }
            pcVar14 = local_430;
            if (local_430 == (char *)0x0) {
LAB_00105e69:
                    /* WARNING: Subroutine does not return */
              xalloc_die();
            }
            break;
          case 10:
            pcVar14 = (char *)xstrdup(param_2);
            break;
          case 0xb:
            puVar19 = puVar9;
LAB_00105bd8:
            pcVar14 = (char *)xstrdup(puVar19);
          }
          local_430 = pcVar14;
          if (tty_out_1 < 0) {
            tty_out_1 = isatty(1);
          }
          pcVar11 = replace_invalid_chars;
          if (tty_out_1 == 0) {
            pcVar11 = replace_control_chars;
          }
          (*pcVar11)(pcVar14);
          iVar8 = gnu_mbswidth(local_430,0);
          lVar15 = columns;
          uVar6 = ncolumns;
          lVar5 = *(long *)(columns + lVar1);
          uVar18 = *(ulong *)(lVar5 + 0x20);
          uVar17 = (long)iVar8;
          if ((ulong)(long)iVar8 < uVar18) {
            uVar17 = uVar18;
          }
          uVar22 = uVar22 + 1;
          *(ulong *)(lVar5 + 0x20) = uVar17;
          *(char **)(*(long *)(table + -8 + nrows * 8) + lVar1) = local_430;
        } while (uVar22 < uVar6);
      }
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        free(local_448);
        return;
      }
      goto LAB_00106245;
    }
    if (show_listed_fs != '\0') {
      if (fs_select_list == (char **)0x0) goto LAB_001057a0;
LAB_00105721:
      ppcVar23 = fs_select_list;
      if (param_5 == (char *)0x0) goto LAB_001057d2;
      do {
        iVar8 = strcmp(param_5,*ppcVar23);
        if (iVar8 == 0) {
          ppcVar23 = fs_exclude_list;
          if (fs_exclude_list == (char **)0x0) goto LAB_001057d2;
          goto LAB_001057b8;
        }
        ppcVar2 = ppcVar23 + 1;
        ppcVar23 = (char **)*ppcVar2;
      } while ((char **)*ppcVar2 != (char **)0x0);
    }
  }
LAB_0010574e:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_00106245:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}