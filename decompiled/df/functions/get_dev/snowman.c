void get_dev(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int32_t a7, struct s6* a8, int32_t a9, void** a10, int64_t a11, ...) {
    void** r14_12;
    void** r13_13;
    void** rbp14;
    void* rsp15;
    void** v16;
    void** v17;
    int32_t r15d18;
    int32_t v19;
    struct s6* rbx20;
    int64_t rax21;
    int64_t v22;
    int1_t zf23;
    int1_t zf24;
    int1_t zf25;
    void** r12_26;
    void** r12_27;
    unsigned char v28;
    uint32_t eax29;
    void** rax30;
    void** rax31;
    int32_t eax32;
    void** rax33;
    int1_t zf34;
    int64_t rax35;
    uint64_t v36;
    int1_t zf37;
    int1_t zf38;
    int32_t eax39;
    struct s4* rax40;
    void** r12_41;
    int64_t rax42;
    int64_t v43;
    int1_t zf44;
    void** rax45;
    void*** v46;
    void** rax47;
    void** rax48;
    void* rax49;
    int64_t rax50;
    uint64_t rdx51;
    uint64_t r9_52;
    int1_t zf53;
    int64_t tmp64_54;
    uint64_t tmp64_55;
    int64_t tmp64_56;
    int64_t tmp64_57;
    uint32_t ecx58;
    uint64_t rax59;
    void** rdx60;
    int1_t zf61;
    void** rdx62;
    int64_t rax63;
    int64_t rax64;
    int64_t v65;
    int1_t zf66;
    int1_t zf67;
    int64_t rax68;
    int64_t rax69;

    r14_12 = rsi;
    r13_13 = r8;
    rbp14 = rdi;
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x428);
    v16 = rdx;
    v17 = rcx;
    r15d18 = a7;
    v19 = a9;
    rbx20 = a8;
    rax21 = g28;
    v22 = rax21;
    if (!*reinterpret_cast<signed char*>(&r15d18)) 
        goto addr_5710_2;
    zf23 = show_local_fs == 0;
    if (!zf23) 
        goto addr_574e_4;
    addr_5710_2:
    if (*reinterpret_cast<signed char*>(&r9) && (zf24 = show_all_fs == 0, zf24)) {
        zf25 = show_listed_fs == 0;
        if (zf25) 
            goto addr_574e_4;
        r12_26 = fs_select_list;
        if (r12_26) 
            goto addr_5721_7;
        goto addr_57a0_9;
    }
    r12_26 = fs_select_list;
    if (!r12_26) {
        addr_57a0_9:
        r12_27 = fs_exclude_list;
        if (!r12_27 || !r13_13) {
            addr_57d2_11:
            v28 = reinterpret_cast<uint1_t>(rbx20 == 0);
            eax29 = v28;
            if (!r14_12 || !*reinterpret_cast<signed char*>(&eax29)) {
                if (!v17) {
                    rax30 = rbp14;
                    if (r14_12) {
                        rax30 = r14_12;
                    }
                    v17 = rax30;
                }
                if (rbx20) 
                    goto addr_5a7d_17;
            } else {
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r14_12) == 47)) 
                    goto addr_574e_4;
                rax31 = v17;
                if (!rax31) {
                    rax31 = r14_12;
                }
                v17 = rax31;
            }
        } else {
            goto addr_57b8_23;
        }
    } else {
        addr_5721_7:
        if (!r13_13) 
            goto addr_57d2_11; else 
            goto addr_572a_24;
    }
    rdi = v17;
    rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp15) + 48);
    rsi = rbp14;
    eax32 = get_fs_usage(rdi, rsi, rdx);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
    if (eax32) {
        rax33 = fun_3630();
        if (!*reinterpret_cast<signed char*>(&v19) || *reinterpret_cast<void***>(rax33) != 13 && *reinterpret_cast<void***>(rax33) != 2) {
            quotearg_n_style_colon();
            fun_3a30();
            exit_status = 1;
            goto addr_574e_4;
        }
        zf34 = show_all_fs == 0;
        if (zf34) {
            addr_574e_4:
            rax35 = v22 - g28;
            if (!rax35) {
                return;
            }
        } else {
            r13_13 = reinterpret_cast<void**>("-");
            v36 = 0xffffffffffffffff;
            goto addr_5857_31;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&v19)) {
            addr_616c_33:
            if (1) 
                goto addr_5857_31;
            zf37 = show_all_fs == 0;
            if (!zf37) 
                goto addr_5857_31; else 
                goto addr_6185_35;
        } else {
            zf38 = show_all_fs == 0;
            if (!zf38) {
                rdi = v17;
                rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp15) + 0xf0);
                eax39 = fun_38f0(rdi, rsi, rdi, rsi);
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                if (!eax39 && ((rdi = devlist_table, !!rdi) && ((rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp15) + 0xb0), rax40 = hash_lookup(), !!rax40) && (rax40->f18 && ((r12_41 = rax40->f18->f8, !!r12_41) && ((rdi = *reinterpret_cast<void***>(r12_41), rsi = rbp14, rax42 = fun_38c0(rdi, rsi, rdx), !!*reinterpret_cast<int32_t*>(&rax42)) && (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_41 + 40)) & 2) || !*reinterpret_cast<signed char*>(&r15d18)))))))) {
                    r13_13 = reinterpret_cast<void**>("-");
                    v36 = 0xffffffffffffffff;
                    goto addr_616c_33;
                }
            } else {
                if (v43) 
                    goto addr_5857_31; else 
                    goto addr_5845_40;
            }
        }
    }
    fun_3780();
    addr_6185_35:
    addr_5845_40:
    zf44 = show_listed_fs == 0;
    if (zf44) 
        goto addr_574e_4;
    if (rbx20) {
        addr_585e_44:
        alloc_table_row(rdi, rsi, rdx, rcx, r8, r9);
        rax45 = reinterpret_cast<void**>("-");
        if (!rbp14) {
            rbp14 = reinterpret_cast<void**>("-");
        }
    } else {
        addr_5857_31:
        file_systems_processed = 1;
        goto addr_585e_44;
    }
    if (v16) {
        rax45 = v16;
    }
    rax47 = xstrdup(rbp14, rsi, rdx, rcx, r8, r9, v46, rax45, *reinterpret_cast<void**>(&v17));
    if (*reinterpret_cast<signed char*>(&v19) && (rax48 = fun_3750(rax47, rax47), reinterpret_cast<unsigned char>(rax48) > reinterpret_cast<unsigned char>(36))) {
        rax49 = fun_3850(reinterpret_cast<unsigned char>(rax47) + reinterpret_cast<unsigned char>(rax48) + 0xffffffffffffffdc, "-0123456789abcdefABCDEF", rdx, rcx, r8);
        if (rax49 == 36) {
            rax50 = canonicalize_filename_mode(rax47);
            if (rax50) {
                fun_35f0(rax47, rax47);
            }
        }
    }
    if (!r13_13) {
    }
    if (v36 <= 0xfffffffffffffffd && !1) {
    }
    rdx51 = output_block_size;
    r9_52 = 0xffffffffffffffff;
    *reinterpret_cast<unsigned char*>(&rdx51) = 0;
    if (!1 && !1) {
    }
    zf53 = print_grand_total == 0;
    if (!zf53 && v28) {
        if (!1) {
            tmp64_54 = g183c8 - 1;
            g183c8 = tmp64_54;
        }
        if (v36 <= 0xfffffffffffffffd) {
            tmp64_55 = g183d0 + v36;
            g183d0 = tmp64_55;
        }
        if (!1) {
            tmp64_56 = g183a8 + 1;
            g183a8 = tmp64_56;
        }
        if (!1) {
            tmp64_57 = g183b0 + 1;
            g183b0 = tmp64_57;
        }
        if (!1) {
            ecx58 = g183c0;
            r9_52 = 1;
            rax59 = g183b8;
            if (!*reinterpret_cast<unsigned char*>(&ecx58)) {
                r9_52 = 1 + rax59;
                g183b8 = r9_52;
            } else {
                if (*reinterpret_cast<unsigned char*>(&ecx58)) {
                    rax59 = -rax59;
                }
                if (0) {
                    r9_52 = reinterpret_cast<uint64_t>(-1);
                }
                if (r9_52 >= rax59) 
                    goto addr_621f_76;
                g183b8 = rax59 - r9_52;
                goto addr_6092_78;
            }
        }
    }
    while (rdx60 = columns, zf61 = ncolumns == 0, !zf61) {
        rdx62 = *reinterpret_cast<void***>(rdx60);
        if (*reinterpret_cast<void***>(rdx62 + 16) != 1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx62 + 16) == 2)) {
                if (*reinterpret_cast<void***>(rdx62 + 16)) {
                    *reinterpret_cast<int32_t*>(&rdx62) = 0x480;
                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                    fun_3800("!\"bad field_type\"", "src/df.c", "!\"bad field_type\"", "src/df.c");
                }
            }
        }
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx62)) <= reinterpret_cast<unsigned char>(11)) 
            goto addr_5a3c_89;
        *reinterpret_cast<uint32_t*>(&rdx51) = 0x4e6;
        rax59 = fun_3800("!\"unhandled field\"", "src/df.c", "!\"unhandled field\"", "src/df.c");
        addr_621f_76:
        r9_52 = r9_52 - rax59;
        g183c0 = *reinterpret_cast<unsigned char*>(&rdx51);
        ecx58 = *reinterpret_cast<uint32_t*>(&rdx51);
        g183b8 = r9_52;
        addr_6092_78:
        if (!*reinterpret_cast<unsigned char*>(&ecx58)) 
            continue;
        g183b8 = -g183b8;
    }
    rax63 = v22 - g28;
    if (!rax63) {
    }
    addr_5a3c_89:
    *reinterpret_cast<void***>(&rax64) = *reinterpret_cast<void***>(rdx62);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax64) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x122d8 + rax64 * 4) + 0x122d8;
    addr_5a7d_17:
    __asm__("movdqa xmm5, [rbx]");
    __asm__("movdqa xmm6, [rbx+0x10]");
    __asm__("movdqa xmm7, [rbx+0x20]");
    __asm__("movaps [rsp+0x30], xmm5");
    v36 = rbx20->f30;
    __asm__("movaps [rsp+0x40], xmm6");
    __asm__("movaps [rsp+0x50], xmm7");
    if (!v65 && (zf66 = show_all_fs == 0, zf66)) {
        zf67 = show_listed_fs == 0;
        if (zf67) 
            goto addr_574e_4;
        goto addr_585e_44;
    }
    do {
        addr_57b8_23:
        rsi = *reinterpret_cast<void***>(r12_27);
        rdi = r13_13;
        rax68 = fun_38c0(rdi, rsi, rdx);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax68)) 
            goto addr_574e_4;
        r12_27 = *reinterpret_cast<void***>(r12_27 + 8);
    } while (r12_27);
    goto addr_57d2_11;
    addr_572a_24:
    do {
        rsi = *reinterpret_cast<void***>(r12_26);
        rdi = r13_13;
        rax69 = fun_38c0(rdi, rsi, rdx);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax69)) 
            break;
        r12_26 = *reinterpret_cast<void***>(r12_26 + 8);
    } while (r12_26);
    goto addr_574e_4;
    r12_27 = fs_exclude_list;
    if (r12_27) 
        goto addr_57b8_23;
    goto addr_57d2_11;
}