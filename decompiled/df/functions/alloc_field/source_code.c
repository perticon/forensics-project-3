alloc_field (int f, char const *c)
{
  ncolumns++;
  columns = xnrealloc (columns, ncolumns, sizeof (struct field_data_t *));
  columns[ncolumns - 1] = &field_data[f];
  if (c != NULL)
    columns[ncolumns - 1]->caption = c;

  if (field_data[f].used)
    assert (!"field used");

  /* Mark field as used.  */
  field_data[f].used = true;
}