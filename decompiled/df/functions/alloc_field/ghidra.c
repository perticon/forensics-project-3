void alloc_field(int param_1,long param_2)

{
  long lVar1;
  
  ncolumns = ncolumns + 1;
  columns = xreallocarray(columns,ncolumns,8);
  lVar1 = (long)param_1 * 0x30;
  *(undefined1 **)(columns + -8 + ncolumns * 8) = field_data + lVar1;
  if (param_2 != 0) {
    *(long *)(field_data + lVar1 + 0x18) = param_2;
  }
  lVar1 = (long)param_1 * 0x30;
  if (field_data[lVar1 + 0x2c] == '\0') {
    field_data[lVar1 + 0x2c] = 1;
    return;
  }
                    /* WARNING: Subroutine does not return */
  __assert_fail("!\"field used\"","src/df.c",0x1a9,"alloc_field");
}