void alloc_field(int32_t f, char * c) {
    // 0x51f0
    ncolumns++;
    int64_t v1 = xreallocarray(); // 0x5219
    *(int64_t *)&columns = v1;
    int64_t v2 = 48 * (int64_t)f; // 0x523a
    *(int64_t *)(v1 - 8 + 8 * ncolumns) = v2 + (int64_t)&field_data;
    if (c != NULL) {
        // 0x524b
        *(int64_t *)(v2 + (int64_t)&field_data + 24) = (int64_t)c;
    }
    char * v3 = (char *)(v2 + (int64_t)&field_data + 44); // 0x525a
    if (*v3 != 0) {
        // 0x526b
        function_3800();
        return;
    }
    // 0x5260
    *v3 = 1;
}