void alloc_field(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    uint64_t rax8;
    void** rdi9;
    void** rax10;
    int64_t rdi11;
    uint64_t rsi12;
    struct s1* rdx13;
    struct s2* rcx14;
    void** rax15;
    void** rdi16;
    void** rax17;
    void** rdx18;
    uint64_t rdi19;
    void** rax20;
    int64_t v21;

    rax8 = ncolumns;
    rdi9 = columns;
    ncolumns = rax8 + 1;
    rax10 = xreallocarray(rdi9);
    rdi11 = edi;
    rsi12 = ncolumns;
    columns = rax10;
    rdx13 = reinterpret_cast<struct s1*>((rdi11 + rdi11 * 2 << 4) + 0x18020);
    *reinterpret_cast<struct s1**>(reinterpret_cast<uint64_t>(rax10 + rsi12 * 8) - 8) = rdx13;
    if (rsi) {
        rdx13->f18 = rsi;
    }
    rcx14 = reinterpret_cast<struct s2*>(0x18020 + (rdi11 + rdi11 * 2 << 4));
    if (rcx14->f2c) {
        fun_3800("!\"field used\"", "src/df.c", "!\"field used\"", "src/df.c");
        rax15 = nrows;
        rdi16 = table;
        nrows = rax15 + 1;
        rax17 = xreallocarray(rdi16);
        rdx18 = nrows;
        rdi19 = ncolumns;
        table = rax17;
        rax20 = xnmalloc(rdi19, 8, rdx18, "alloc_field", r8, r9);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rax17 + reinterpret_cast<unsigned char>(rdx18) * 8) - 8) = rax20;
        goto v21;
    } else {
        rcx14->f2c = 1;
        return;
    }
}