replace_problematic_chars (char *cell)
{
  static int tty_out = -1;
  if (tty_out < 0)
    tty_out = isatty (STDOUT_FILENO);

  (tty_out ? replace_invalid_chars : replace_control_chars) (cell) ;
}