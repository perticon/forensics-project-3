me_for_dev (dev_t dev)
{
  struct devlist *dl = devlist_for_dev (dev);
  if (dl)
        return dl->me;

  return NULL;
}