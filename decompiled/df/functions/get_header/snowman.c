void get_header(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    int64_t rax8;
    int64_t v9;
    void* rsp10;
    int1_t zf11;
    void** rdx12;
    int64_t rax13;
    uint64_t rbx14;
    void* r15_15;
    void** v16;
    void*** r14_17;
    void** rax18;
    void* rsp19;
    uint32_t eax20;
    uint64_t rdi21;
    uint32_t r8d22;
    uint64_t rsi23;
    uint64_t rcx24;
    void** rax25;
    void* rsp26;
    void** r14_27;
    uint64_t rdi28;
    void** v29;
    void** rax30;
    void** r14_31;
    void** rax32;
    void* rsp33;
    uint64_t rax34;
    uint64_t rdx35;
    uint64_t rdx36;
    uint64_t rax37;
    uint32_t eax38;
    unsigned char cl39;
    unsigned char dl40;
    void** rax41;
    void** rax42;
    void** rax43;
    int32_t eax44;
    int32_t eax45;
    int64_t rax46;
    void** rdx47;
    void** rax48;
    int32_t eax49;
    void** rax50;
    int1_t below_or_equal51;
    void** r14_52;
    void** r13_53;
    void** rbp54;
    void* rsp55;
    void** v56;
    void** v57;
    int32_t r15d58;
    int32_t v59;
    struct s3* rbx60;
    struct s3* v61;
    int64_t rax62;
    int64_t v63;
    int1_t zf64;
    int1_t zf65;
    int1_t zf66;
    void** r12_67;
    void** r12_68;
    unsigned char v69;
    uint32_t eax70;
    void** rax71;
    void** rax72;
    int32_t eax73;
    void** rax74;
    int1_t zf75;
    int64_t rax76;
    int64_t v77;
    uint64_t v78;
    int1_t zf79;
    int1_t zf80;
    int32_t eax81;
    struct s4* rax82;
    void** r12_83;
    int64_t rax84;
    int64_t v85;
    int1_t zf86;
    void** rax87;
    void*** v88;
    void** rax89;
    void** rax90;
    void* rax91;
    int64_t rax92;
    uint64_t rdx93;
    uint64_t r9_94;
    int1_t zf95;
    int64_t tmp64_96;
    uint64_t tmp64_97;
    int64_t tmp64_98;
    int64_t tmp64_99;
    uint32_t ecx100;
    uint64_t rax101;
    void** rdx102;
    int1_t zf103;
    void** rdx104;
    int64_t rax105;
    int64_t rax106;
    int64_t v107;
    int1_t zf108;
    int1_t zf109;
    int64_t rax110;
    int64_t rax111;

    rax8 = g28;
    v9 = rax8;
    alloc_table_row(rdi, rsi, rdx, rcx, r8, r9);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x2c8 - 8 + 8);
    zf11 = ncolumns == 0;
    rdx12 = columns;
    if (zf11) {
        addr_5670_2:
        rax13 = v9 - g28;
        if (!rax13) {
            return;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rbx14) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx14) + 4) = 0;
        do {
            r15_15 = reinterpret_cast<void*>(rbx14 * 8);
            v16 = reinterpret_cast<void**>(0);
            r14_17 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx12) + reinterpret_cast<uint64_t>(r15_15));
            *reinterpret_cast<int32_t*>(&rdx12) = 5;
            *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
            rsi = *reinterpret_cast<void***>(*r14_17 + 24);
            rax18 = fun_3730();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            r9 = rax18;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(*r14_17) == 2)) 
                goto addr_5430_6;
            eax20 = header_mode;
            *reinterpret_cast<uint32_t*>(&r8) = human_output_opts;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            if (!eax20) {
                addr_55a0_8:
                rdi21 = output_block_size;
                r8d22 = *reinterpret_cast<uint32_t*>(&r8) & 0x124;
                rsi23 = rdi21;
                rcx24 = rdi21;
            } else {
                if (eax20 == 4) {
                    *reinterpret_cast<uint32_t*>(&r8) = human_output_opts;
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&r8) & 16) {
                        addr_5430_6:
                        rdi = r9;
                        rax25 = fun_3ae0();
                        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                        v16 = rax25;
                        r14_27 = rax25;
                        goto addr_5440_11;
                    } else {
                        goto addr_55a0_8;
                    }
                } else {
                    if (eax20 != 3) 
                        goto addr_5430_6;
                    rdi28 = output_block_size;
                    v29 = r9;
                    rax30 = umaxtostr(rdi28, reinterpret_cast<int64_t>(rsp19) + 32);
                    r14_31 = rax30;
                    rax32 = fun_3730();
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
                    rcx = v29;
                    rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp33) + 24);
                    rsi = rax32;
                    goto addr_5548_15;
                }
            }
            do {
                r9 = reinterpret_cast<void**>((__intrinsic() >> 4) * 0x3e8);
                rax34 = rcx24;
                rdx35 = rsi23;
                rcx24 = __intrinsic() >> 4;
                *reinterpret_cast<uint32_t*>(&rdx36) = *reinterpret_cast<uint32_t*>(&rdx35) & 0x3ff;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx36) + 4) = 0;
                rsi23 = rsi23 >> 10;
                rax37 = rax34 - reinterpret_cast<unsigned char>(r9);
            } while (!(rax37 | rdx36));
            eax38 = r8d22;
            cl39 = reinterpret_cast<uint1_t>(rax37 == 0);
            dl40 = reinterpret_cast<uint1_t>(rdx36 == 0);
            *reinterpret_cast<unsigned char*>(&eax38) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax38) | 0x98);
            *reinterpret_cast<unsigned char*>(&r8d22) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d22) | 0xb8);
            if (cl39 < dl40) {
                eax38 = r8d22;
            }
            if (cl39 <= dl40) {
                if (*reinterpret_cast<unsigned char*>(&eax38) & 32) {
                    addr_561e_21:
                    *reinterpret_cast<uint32_t*>(&r8) = 1;
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                    rax41 = human_readable(rdi21, reinterpret_cast<int64_t>(rsp19) + 32);
                    r14_31 = rax41;
                    rax42 = fun_3730();
                    v29 = rax42;
                    rax43 = fun_3730();
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                    rcx = v29;
                    rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp33) + 24);
                    rsi = rax43;
                } else {
                    goto addr_561b_23;
                }
            } else {
                goto addr_561b_23;
            }
            addr_5548_15:
            rdx12 = r14_31;
            eax44 = rpl_asprintf();
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
            if (eax44 == -1) {
                v16 = reinterpret_cast<void**>(0);
            }
            r14_27 = reinterpret_cast<void**>(0);
            addr_5440_11:
            if (!r14_27) 
                goto addr_56a5_27;
            eax45 = tty_out_1;
            if (eax45 < 0) {
                eax45 = fun_3670(1);
                rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                tty_out_1 = eax45;
            }
            rax46 = 0x5190;
            if (eax45) {
                rax46 = 0x52e0;
            }
            rax46(r14_27);
            rdx47 = nrows;
            rdi = v16;
            rax48 = table;
            *reinterpret_cast<void***>(reinterpret_cast<int64_t>(*reinterpret_cast<void****>(reinterpret_cast<uint64_t>(rax48 + reinterpret_cast<unsigned char>(rdx47) * 8) - 8)) + reinterpret_cast<uint64_t>(r15_15)) = rdi;
            eax49 = gnu_mbswidth(rdi);
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
            rdx12 = columns;
            rax50 = reinterpret_cast<void**>(static_cast<int64_t>(eax49));
            rcx = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx12) + reinterpret_cast<uint64_t>(r15_15));
            rsi = *reinterpret_cast<void***>(rcx + 32);
            if (reinterpret_cast<unsigned char>(rax50) < reinterpret_cast<unsigned char>(rsi)) {
                rax50 = rsi;
                continue;
            }
            addr_561b_23:
            goto addr_561e_21;
            ++rbx14;
            below_or_equal51 = ncolumns <= rbx14;
            *reinterpret_cast<void***>(rcx + 32) = rax50;
        } while (!below_or_equal51);
        goto addr_5670_2;
    }
    addr_56aa_35:
    fun_3780();
    r14_52 = rsi;
    r13_53 = r8;
    rbp54 = rdi;
    rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x428);
    v56 = rdx12;
    v57 = rcx;
    r15d58 = *reinterpret_cast<int32_t*>(&v29);
    v59 = *reinterpret_cast<int32_t*>(&v16);
    rbx60 = v61;
    rax62 = g28;
    v63 = rax62;
    if (!*reinterpret_cast<signed char*>(&r15d58)) 
        goto addr_5710_37;
    zf64 = show_local_fs == 0;
    if (!zf64) 
        goto addr_574e_39;
    addr_5710_37:
    if (*reinterpret_cast<signed char*>(&r9) && (zf65 = show_all_fs == 0, zf65)) {
        zf66 = show_listed_fs == 0;
        if (zf66) 
            goto addr_574e_39;
        r12_67 = fs_select_list;
        if (r12_67) 
            goto addr_5721_42;
        goto addr_57a0_44;
    }
    r12_67 = fs_select_list;
    if (!r12_67) {
        addr_57a0_44:
        r12_68 = fs_exclude_list;
        if (!r12_68 || !r13_53) {
            addr_57d2_46:
            v69 = reinterpret_cast<uint1_t>(rbx60 == 0);
            eax70 = v69;
            if (!r14_52 || !*reinterpret_cast<signed char*>(&eax70)) {
                if (!v57) {
                    rax71 = rbp54;
                    if (r14_52) {
                        rax71 = r14_52;
                    }
                    v57 = rax71;
                }
                if (rbx60) 
                    goto addr_5a7d_52;
            } else {
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r14_52) == 47)) 
                    goto addr_574e_39;
                rax72 = v57;
                if (!rax72) {
                    rax72 = r14_52;
                }
                v57 = rax72;
            }
        } else {
            goto addr_57b8_58;
        }
    } else {
        addr_5721_42:
        if (!r13_53) 
            goto addr_57d2_46; else 
            goto addr_572a_59;
    }
    rdi = v57;
    rdx12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp55) + 48);
    rsi = rbp54;
    eax73 = get_fs_usage(rdi, rsi, rdx12);
    rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
    if (eax73) {
        rax74 = fun_3630();
        if (!*reinterpret_cast<signed char*>(&v59) || *reinterpret_cast<void***>(rax74) != 13 && *reinterpret_cast<void***>(rax74) != 2) {
            quotearg_n_style_colon();
            fun_3a30();
            exit_status = 1;
            goto addr_574e_39;
        }
        zf75 = show_all_fs == 0;
        if (zf75) {
            addr_574e_39:
            rax76 = v63 - g28;
            if (!rax76) {
                goto v77;
            }
        } else {
            r13_53 = reinterpret_cast<void**>("-");
            v78 = 0xffffffffffffffff;
            goto addr_5857_66;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&v59)) {
            addr_616c_68:
            if (1) 
                goto addr_5857_66;
            zf79 = show_all_fs == 0;
            if (!zf79) 
                goto addr_5857_66; else 
                goto addr_6185_70;
        } else {
            zf80 = show_all_fs == 0;
            if (!zf80) {
                rdi = v57;
                rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp55) + 0xf0);
                eax81 = fun_38f0(rdi, rsi, rdi, rsi);
                rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
                if (!eax81 && ((rdi = devlist_table, !!rdi) && ((rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp55) + 0xb0), rax82 = hash_lookup(), !!rax82) && (rax82->f18 && ((r12_83 = rax82->f18->f8, !!r12_83) && ((rdi = *reinterpret_cast<void***>(r12_83), rsi = rbp54, rax84 = fun_38c0(rdi, rsi, rdx12), !!*reinterpret_cast<int32_t*>(&rax84)) && (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_83 + 40)) & 2) || !*reinterpret_cast<signed char*>(&r15d58)))))))) {
                    r13_53 = reinterpret_cast<void**>("-");
                    v78 = 0xffffffffffffffff;
                    goto addr_616c_68;
                }
            } else {
                if (v85) 
                    goto addr_5857_66; else 
                    goto addr_5845_75;
            }
        }
    }
    fun_3780();
    addr_6185_70:
    addr_5845_75:
    zf86 = show_listed_fs == 0;
    if (zf86) 
        goto addr_574e_39;
    if (rbx60) {
        addr_585e_79:
        alloc_table_row(rdi, rsi, rdx12, rcx, r8, r9);
        rax87 = reinterpret_cast<void**>("-");
        if (!rbp54) {
            rbp54 = reinterpret_cast<void**>("-");
        }
    } else {
        addr_5857_66:
        file_systems_processed = 1;
        goto addr_585e_79;
    }
    if (v56) {
        rax87 = v56;
    }
    rax89 = xstrdup(rbp54, rsi, rdx12, rcx, r8, r9, v88, rax87, *reinterpret_cast<void**>(&v57));
    if (*reinterpret_cast<signed char*>(&v59) && (rax90 = fun_3750(rax89, rax89), reinterpret_cast<unsigned char>(rax90) > reinterpret_cast<unsigned char>(36))) {
        rax91 = fun_3850(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<unsigned char>(rax90) + 0xffffffffffffffdc, "-0123456789abcdefABCDEF", rdx12, rcx, r8);
        if (rax91 == 36) {
            rax92 = canonicalize_filename_mode(rax89);
            if (rax92) {
                fun_35f0(rax89, rax89);
            }
        }
    }
    if (!r13_53) {
    }
    if (v78 <= 0xfffffffffffffffd && !1) {
    }
    rdx93 = output_block_size;
    r9_94 = 0xffffffffffffffff;
    *reinterpret_cast<unsigned char*>(&rdx93) = 0;
    if (!1 && !1) {
    }
    zf95 = print_grand_total == 0;
    if (!zf95 && v69) {
        if (!1) {
            tmp64_96 = g183c8 - 1;
            g183c8 = tmp64_96;
        }
        if (v78 <= 0xfffffffffffffffd) {
            tmp64_97 = g183d0 + v78;
            g183d0 = tmp64_97;
        }
        if (!1) {
            tmp64_98 = g183a8 + 1;
            g183a8 = tmp64_98;
        }
        if (!1) {
            tmp64_99 = g183b0 + 1;
            g183b0 = tmp64_99;
        }
        if (!1) {
            ecx100 = g183c0;
            r9_94 = 1;
            rax101 = g183b8;
            if (!*reinterpret_cast<unsigned char*>(&ecx100)) {
                r9_94 = 1 + rax101;
                g183b8 = r9_94;
            } else {
                if (*reinterpret_cast<unsigned char*>(&ecx100)) {
                    rax101 = -rax101;
                }
                if (0) {
                    r9_94 = reinterpret_cast<uint64_t>(-1);
                }
                if (r9_94 >= rax101) 
                    goto addr_621f_111;
                g183b8 = rax101 - r9_94;
                goto addr_6092_113;
            }
        }
    }
    while (rdx102 = columns, zf103 = ncolumns == 0, !zf103) {
        rdx104 = *reinterpret_cast<void***>(rdx102);
        if (*reinterpret_cast<void***>(rdx104 + 16) != 1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx104 + 16) == 2)) {
                if (*reinterpret_cast<void***>(rdx104 + 16)) {
                    *reinterpret_cast<int32_t*>(&rdx104) = 0x480;
                    *reinterpret_cast<int32_t*>(&rdx104 + 4) = 0;
                    fun_3800("!\"bad field_type\"", "src/df.c", "!\"bad field_type\"", "src/df.c");
                }
            }
        }
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx104)) <= reinterpret_cast<unsigned char>(11)) 
            goto addr_5a3c_124;
        *reinterpret_cast<uint32_t*>(&rdx93) = 0x4e6;
        rax101 = fun_3800("!\"unhandled field\"", "src/df.c", "!\"unhandled field\"", "src/df.c");
        addr_621f_111:
        r9_94 = r9_94 - rax101;
        g183c0 = *reinterpret_cast<unsigned char*>(&rdx93);
        ecx100 = *reinterpret_cast<uint32_t*>(&rdx93);
        g183b8 = r9_94;
        addr_6092_113:
        if (!*reinterpret_cast<unsigned char*>(&ecx100)) 
            continue;
        g183b8 = -g183b8;
    }
    rax105 = v63 - g28;
    if (!rax105) {
    }
    addr_5a3c_124:
    *reinterpret_cast<void***>(&rax106) = *reinterpret_cast<void***>(rdx104);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x122d8 + rax106 * 4) + 0x122d8;
    addr_5a7d_52:
    __asm__("movdqa xmm5, [rbx]");
    __asm__("movdqa xmm6, [rbx+0x10]");
    __asm__("movdqa xmm7, [rbx+0x20]");
    __asm__("movaps [rsp+0x30], xmm5");
    v78 = rbx60->f30;
    __asm__("movaps [rsp+0x40], xmm6");
    __asm__("movaps [rsp+0x50], xmm7");
    if (!v107 && (zf108 = show_all_fs == 0, zf108)) {
        zf109 = show_listed_fs == 0;
        if (zf109) 
            goto addr_574e_39;
        goto addr_585e_79;
    }
    do {
        addr_57b8_58:
        rsi = *reinterpret_cast<void***>(r12_68);
        rdi = r13_53;
        rax110 = fun_38c0(rdi, rsi, rdx12);
        rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax110)) 
            goto addr_574e_39;
        r12_68 = *reinterpret_cast<void***>(r12_68 + 8);
    } while (r12_68);
    goto addr_57d2_46;
    addr_572a_59:
    do {
        rsi = *reinterpret_cast<void***>(r12_67);
        rdi = r13_53;
        rax111 = fun_38c0(rdi, rsi, rdx12);
        rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax111)) 
            break;
        r12_67 = *reinterpret_cast<void***>(r12_67 + 8);
    } while (r12_67);
    goto addr_574e_39;
    r12_68 = fs_exclude_list;
    if (r12_68) 
        goto addr_57b8_58;
    goto addr_57d2_46;
    addr_56a5_27:
    xalloc_die();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
    goto addr_56aa_35;
}