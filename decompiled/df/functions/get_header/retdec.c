void get_header(void) {
    int64_t v1 = __readfsqword(40); // 0x53e1
    alloc_table_row();
    if (ncolumns == 0) {
      lab_0x5670:
        // 0x5670
        if (v1 == __readfsqword(40)) {
            // 0x5683
            return;
        }
        // 0x56aa
        function_3780();
        return;
    }
    int64_t v2 = (int64_t)columns; // 0x5428
    int64_t v3 = 0; // 0x5428
    int64_t v4; // bp-728, 0x53d0
    char * v5; // bp-736, 0x53d0
    int64_t v6; // 0x53d0
    int64_t v7; // 0x54b9
    while (true) {
      lab_0x54b9:
        // 0x54b9
        v7 = 8 * v3;
        v5 = NULL;
        function_3730();
        if (*(int32_t *)*(int64_t *)(v7 + v2) != 2) {
            goto lab_0x5430;
        } else {
            // 0x54f0
            switch (header_mode) {
                case 0: {
                    goto lab_0x55a0;
                }
                case 4: {
                    if ((human_output_opts & 16) != 0) {
                        goto lab_0x5430;
                    } else {
                        goto lab_0x55a0;
                    }
                }
                default: {
                    if (header_mode != 3) {
                        goto lab_0x5430;
                    } else {
                        // 0x5513
                        umaxtostr(output_block_size, (char *)&v4);
                        v6 = function_3730();
                        goto lab_0x5548;
                    }
                }
            }
        }
    }
  lab_0x56a5:
    // 0x56a5
    xalloc_die();
    // 0x56aa
    function_3780();
  lab_0x5430:;
    int64_t v8 = function_3ae0(); // 0x5433
    char * v9 = (char *)v8; // 0x5438
    v5 = v9;
    char * v10 = v9; // 0x543d
    int64_t v11 = v8; // 0x543d
    goto lab_0x5440;
  lab_0x5440:
    // 0x5440
    if (v11 == 0) {
        // break -> 0x56a5
        goto lab_0x56a5;
    }
    char * v12 = v10; // 0x5451
    if (g26 < 0) {
        // 0x5570
        g26 = function_3670();
        v12 = v5;
    }
    int64_t v13 = *(int64_t *)(8 * nrows - 8 + (int64_t)table); // 0x547e
    *(int64_t *)(v13 + v7) = (int64_t)v12;
    int32_t v14 = gnu_mbswidth(v12, 0); // 0x5487
    v2 = (int64_t)columns;
    uint64_t v15 = (int64_t)v14; // 0x5493
    int64_t * v16 = (int64_t *)(*(int64_t *)(v7 + v2) + 32); // 0x5499
    uint64_t v17 = *v16; // 0x5499
    v3++;
    *v16 = v17 > v15 ? v17 : v15;
    if (ncolumns <= v3) {
        goto lab_0x5670;
    }
    goto lab_0x54b9;
  lab_0x55a0:;
    int64_t v18 = output_block_size; // 0x55b4
    int64_t v19 = v18 & (int64_t)(int32_t)&g51; // 0x55e0
    v18 /= 1024;
    while ((output_block_size || v19) == 0) {
        // 0x55b8
        v19 = v18 & (int64_t)(int32_t)&g51;
        v18 /= 1024;
    }
    int64_t v20 = human_output_opts & 292; // 0x55a7
    int64_t v21 = (output_block_size == 0) < (v19 == 0) ? v20 & 4 | 184 : v20 & 36 | 152;
    int64_t v22 = v21 | v20 & 256;
    int64_t v23; // 0x53d0
    if ((output_block_size == 0) > (v19 == 0)) {
        // 0x561b
        v23 = v22 & 412 | 256;
        goto lab_0x561e;
    } else {
        int64_t v24 = v22; // 0x569a
        v23 = v22;
        if ((v21 & 32) != 0) {
            goto lab_0x561e;
        } else {
            // 0x561b
            v23 = v24 | 256;
            goto lab_0x561e;
        }
    }
  lab_0x5548:;
    // 0x5548
    char * v25; // 0x53d0
    if (rpl_asprintf(&v5, (char *)v6) != -1) {
        // 0x5548
        v25 = v5;
    } else {
        // 0x5557
        v5 = NULL;
        v25 = NULL;
    }
    // 0x5560
    v10 = v25;
    v11 = (int64_t)v25;
    goto lab_0x5440;
  lab_0x561e:
    // 0x561e
    human_readable(output_block_size, (char *)&v4, (int32_t)v23, 1, 1);
    function_3730();
    v6 = function_3730();
    goto lab_0x5548;
}