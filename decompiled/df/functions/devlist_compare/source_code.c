devlist_compare (void const *x, void const *y)
{
  struct devlist const *a = x;
  struct devlist const *b = y;
  return a->dev_num == b->dev_num;
}