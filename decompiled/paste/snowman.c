
struct s0 {
    unsigned char f0;
    signed char f1;
    signed char f2;
};

struct s0* fun_2400();

uint32_t* fun_2370();

void fun_25a0();

void** write_error(void** rdi, void** rsi, ...) {
    fun_2400();
    fun_2370();
    fun_25a0();
}

int64_t fun_2410();

int64_t fun_2360(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2410();
    if (r8d > 10) {
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x75e0 + rax11 * 4) + 0x75e0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2490();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2350(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** xcharalloc(void** rdi, ...);

void fun_2430();

void** quotearg_n_options(void** rdi, void** rsi, void** rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    uint32_t* rax8;
    void** r15_9;
    uint32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x4f8f;
    rax8 = fun_2370();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x5f61]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x501b;
            fun_2490();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb120) {
                fun_2350(r14_19, rsi24, rsi, rdx, r14_19, rsi24, rsi, rdx);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x50aa);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2430();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x7583);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax7 = reinterpret_cast<struct s0*>(0x757c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x7587);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax10 = reinterpret_cast<struct s0*>(0x7578);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2333() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_2343() {
    __asm__("cli ");
    goto __uflow;
}

int64_t free = 0x2040;

void fun_2353() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2363() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2373() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2383() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2393() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23a3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_23b3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clearerr_unlocked = 0x20b0;

void fun_23c3() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x20c0;

void fun_23d3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_23e3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_23f3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2403() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2413() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2423() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2433() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2443() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2453() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2150;

void fun_2463() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2160;

void fun_2473() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_2483() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2180;

void fun_2493() {
    __asm__("cli ");
    goto memset;
}

int64_t posix_fadvise = 0x2190;

void fun_24a3() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x21a0;

void fun_24b3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21b0;

void fun_24c3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21c0;

void fun_24d3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21d0;

void fun_24e3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21e0;

void fun_24f3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x21f0;

void fun_2503() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2200;

void fun_2513() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2210;

void fun_2523() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2220;

void fun_2533() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2230;

void fun_2543() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2240;

void fun_2553() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2250;

void fun_2563() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2260;

void fun_2573() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2270;

void fun_2583() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2280;

void fun_2593() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2290;

void fun_25a3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22a0;

void fun_25b3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x22b0;

void fun_25c3() {
    __asm__("cli ");
    goto fopen;
}

int64_t __cxa_atexit = 0x22c0;

void fun_25d3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22d0;

void fun_25e3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22e0;

void fun_25f3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x22f0;

void fun_2603() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2300;

void fun_2613() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2310;

void fun_2623() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2320;

void fun_2633() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

struct s0* fun_2580(int64_t rdi, ...);

void fun_23f0(int64_t rdi, int64_t rsi);

void fun_23d0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

signed char have_read_stdin = 0;

signed char serial_merge = 0;

int32_t fun_2440(int64_t rdi, void*** rsi, int64_t rdx, void** rcx);

void** optarg = reinterpret_cast<void**>(0);

void usage();

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0x7520;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_25e0();

unsigned char line_delim = 10;

int32_t optind = 0;

void** xstrdup(void** rdi, void*** rsi, int64_t rdx, void** rcx);

void** delims = reinterpret_cast<void**>(0);

void** delim_end = reinterpret_cast<void**>(0);

void** stdin = reinterpret_cast<void**>(0);

int64_t rpl_fclose(void** rdi, void** rsi, ...);

void** quotearg_n_style_colon();

int64_t fun_2683(int32_t edi, void*** rsi) {
    int32_t ebp3;
    void*** rbx4;
    void** rdi5;
    void** r12_6;
    void** rcx7;
    int64_t rdi8;
    int32_t eax9;
    void** rdi10;
    int64_t rcx11;
    int64_t rax12;
    int32_t ebp13;
    void** rax14;
    uint32_t edx15;
    void** rsi16;
    int1_t zf17;
    int64_t rax18;
    void** rax19;
    int64_t rdx20;
    int32_t esi21;
    void** rdx22;
    void** rsi23;
    uint32_t eax24;
    void** rdi25;
    int1_t zf26;
    void** rdi27;
    int64_t rax28;
    uint32_t ebx29;
    int64_t rax30;
    int64_t rsi31;

    __asm__("cli ");
    ebp3 = edi;
    rbx4 = rsi;
    rdi5 = *rsi;
    set_program_name(rdi5);
    fun_2580(6, 6);
    fun_23f0("coreutils", "/usr/local/share/locale");
    r12_6 = reinterpret_cast<void**>("\t");
    fun_23d0("coreutils", "/usr/local/share/locale");
    atexit(0x3760, "/usr/local/share/locale");
    have_read_stdin = 0;
    serial_merge = 0;
    while (rcx7 = reinterpret_cast<void**>(0xab20), *reinterpret_cast<int32_t*>(&rdi8) = ebp3, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0, eax9 = fun_2440(rdi8, rbx4, "d:sz", 0xab20), eax9 != -1) {
        if (eax9 == 100) {
            addr_27b0_4:
            r12_6 = optarg;
            if (!*reinterpret_cast<void***>(r12_6)) {
                r12_6 = reinterpret_cast<void**>("\\0");
            }
        } else {
            if (eax9 <= 100) {
                if (eax9 != 0xffffff7d) {
                    if (eax9 != 0xffffff7e) 
                        goto addr_298d_9;
                    usage();
                    goto addr_27b0_4;
                } else {
                    rdi10 = stdout;
                    rcx11 = Version;
                    version_etc(rdi10, "paste", "GNU coreutils", rcx11, "David M. Ihnat", "David MacKenzie", 0, 0x2717);
                    eax9 = fun_25e0();
                    goto addr_2788_12;
                }
            } else {
                if (eax9 == 0x73) {
                    serial_merge = 1;
                    continue;
                }
            }
        }
        continue;
        addr_2788_12:
        if (eax9 != 0x7a) 
            goto addr_298d_9;
        line_delim = 0;
    }
    rax12 = optind;
    ebp13 = ebp3 - *reinterpret_cast<int32_t*>(&rax12);
    if (!ebp13) {
        rcx7 = reinterpret_cast<void**>("-");
        ebp13 = 1;
        rbx4[rax12 * 8] = reinterpret_cast<void**>("-");
    }
    rax14 = xstrdup(r12_6, rbx4, "d:sz", rcx7);
    edx15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_6));
    delims = rax14;
    rsi16 = rax14;
    if (!*reinterpret_cast<signed char*>(&edx15)) {
        addr_2873_20:
        zf17 = serial_merge == 0;
        delim_end = rsi16;
        rax18 = 0x2fd0;
        if (zf17) {
            rax18 = 0x2ac0;
        }
    } else {
        rax19 = rax14 + 1;
        rcx7 = r12_6;
        do {
            if (*reinterpret_cast<signed char*>(&edx15) != 92) {
                *reinterpret_cast<signed char*>(rax19 + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&edx15);
                ++rcx7;
                rsi16 = rax19;
                continue;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx7 + 1));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rdx20) > 0x76) {
                    addr_2853_26:
                    rsi16 = rax19;
                } else {
                    if (*reinterpret_cast<signed char*>(&rdx20) <= 91) {
                        if (!*reinterpret_cast<signed char*>(&rdx20)) 
                            goto addr_291e_29;
                        if (*reinterpret_cast<signed char*>(&rdx20) != 48) 
                            goto addr_2853_26; else 
                            goto addr_286c_31;
                    } else {
                        esi21 = static_cast<int32_t>(rdx20 - 92);
                        if (*reinterpret_cast<unsigned char*>(&esi21) > 26) 
                            goto addr_2853_26; else 
                            goto addr_2845_33;
                    }
                }
            }
            addr_2856_34:
            *reinterpret_cast<signed char*>(rax19 + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&rdx20);
            rcx7 = rcx7 + 2;
            continue;
            addr_286c_31:
            rsi16 = rax19;
            *reinterpret_cast<uint32_t*>(&rdx20) = 0;
            goto addr_2856_34;
            edx15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx7));
            ++rax19;
        } while (*reinterpret_cast<signed char*>(&edx15));
        goto addr_2873_20;
    }
    rdx22 = reinterpret_cast<void**>(static_cast<int64_t>(optind));
    rsi23 = reinterpret_cast<void**>(rbx4 + reinterpret_cast<unsigned char>(rdx22) * 8);
    eax24 = reinterpret_cast<uint32_t>(rax18(static_cast<int64_t>(ebp13), rsi23));
    rdi25 = delims;
    *reinterpret_cast<uint32_t*>(&rbx4) = eax24;
    fun_2350(rdi25, rsi23, rdx22, rcx7, rdi25, rsi23, rdx22, rcx7);
    zf26 = have_read_stdin == 0;
    if (zf26) 
        goto addr_28be_37;
    addr_295e_38:
    rdi27 = stdin;
    rax28 = rpl_fclose(rdi27, rsi23, rdi27, rsi23);
    if (*reinterpret_cast<int32_t*>(&rax28) + 1) {
        addr_28be_37:
        ebx29 = *reinterpret_cast<uint32_t*>(&rbx4) ^ 1;
        *reinterpret_cast<uint32_t*>(&rax30) = *reinterpret_cast<unsigned char*>(&ebx29);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
        return rax30;
    } else {
        fun_2370();
        fun_25a0();
    }
    addr_298d_9:
    usage();
    addr_291e_29:
    delim_end = rax19 + 0xffffffffffffffff;
    quotearg_n_style_colon();
    fun_2400();
    *reinterpret_cast<int32_t*>(&rsi23) = 0;
    *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
    fun_25a0();
    goto addr_295e_38;
    addr_2845_33:
    *reinterpret_cast<uint32_t*>(&rsi31) = *reinterpret_cast<unsigned char*>(&esi21);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi31) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x74b4 + rsi31 * 4) + 0x74b4;
}

int64_t __libc_start_main = 0;

void fun_29a3() {
    __asm__("cli ");
    __libc_start_main(0x2680, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_2330(int64_t rdi);

int64_t fun_2a43() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2330(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2a83() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s4 {
    signed char f0;
    signed char f1;
};

void** xmalloc();

void** xnmalloc(uint64_t rdi, void** rsi);

void** fun_25c0();

int32_t fun_2510(void** rdi, int64_t rsi);

void fadvise(void** rdi, void** rsi, void** rdx, ...);

int32_t fun_2460(void** rdi, void** rsi, ...);

void** fun_2560(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t fun_2340(void** rdi, void** rsi, ...);

void fun_23c0(void** rdi, void** rsi, ...);

int64_t fun_2ac3(uint64_t rdi, struct s4** rsi, void** rdx) {
    struct s4** r13_4;
    uint64_t rbx5;
    void** rax6;
    void** rsi7;
    void** v8;
    void** rax9;
    void** v10;
    unsigned char v11;
    uint64_t r12_12;
    void** r14_13;
    void** rax14;
    void** rax15;
    int32_t eax16;
    void** rcx17;
    int64_t rax18;
    uint64_t r13_19;
    int1_t zf20;
    uint32_t eax21;
    uint32_t r9d22;
    uint64_t r15_23;
    void** rdi24;
    int32_t eax25;
    void** rax26;
    void** r14_27;
    void** rbp28;
    signed char v29;
    void** rbx30;
    void** rax31;
    void** rax32;
    uint32_t eax33;
    void** rax34;
    int32_t eax35;
    uint32_t eax36;
    int32_t eax37;
    int1_t zf38;
    void** rax39;
    uint32_t eax40;
    uint32_t* rax41;
    uint32_t v42;
    void** rax43;
    int64_t rax44;
    void** rax45;
    int64_t rax46;
    uint32_t eax47;
    int1_t zf48;
    void** rax49;
    int32_t eax50;

    __asm__("cli ");
    r13_4 = rsi;
    rbx5 = rdi;
    rax6 = xmalloc();
    *reinterpret_cast<uint32_t*>(&rsi7) = 8;
    *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
    v8 = rax6;
    rax9 = xnmalloc(rbx5 + 1, 8);
    v10 = rax9;
    if (!rbx5) {
        v11 = 1;
    } else {
        v11 = 0;
        *reinterpret_cast<int32_t*>(&r12_12) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_12) + 4) = 0;
        r14_13 = rax9;
        do {
            addr_2b67_4:
            if ((*r13_4)->f0 == 45 && !(*r13_4)->f1) {
                rax14 = stdin;
                have_read_stdin = 1;
                ++r13_4;
                *reinterpret_cast<void***>(r14_13 + r12_12 * 8) = rax14;
                ++r12_12;
                if (rbx5 != r12_12) 
                    goto addr_2b67_4; else 
                    goto addr_2b95_6;
            }
            rax15 = fun_25c0();
            *reinterpret_cast<void***>(r14_13 + r12_12 * 8) = rax15;
            if (!rax15) 
                goto addr_2fa0_8;
            eax16 = fun_2510(rax15, "r");
            *reinterpret_cast<uint32_t*>(&rcx17) = v11;
            *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi7) = 2;
            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
            if (eax16) 
                continue;
            *reinterpret_cast<uint32_t*>(&rcx17) = 1;
            *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
            ++r12_12;
            ++r13_4;
            v11 = *reinterpret_cast<unsigned char*>(&rcx17);
            fadvise(rax15, 2, rdx);
        } while (rbx5 != r12_12);
        goto addr_2b95_6;
    }
    addr_2db5_12:
    fun_2350(v10, rsi7, rdx, rcx17);
    fun_2350(v8, rsi7, rdx, rcx17);
    *reinterpret_cast<uint32_t*>(&rax18) = v11;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    return rax18;
    addr_2fa0_8:
    quotearg_n_style_colon();
    fun_2370();
    fun_25a0();
    addr_2b95_6:
    if (!v11) {
        v11 = 1;
        r13_19 = r12_12;
        goto addr_2bb0_14;
    } else {
        zf20 = have_read_stdin == 0;
        if (!zf20) {
            fun_2400();
            fun_25a0();
            goto addr_2fa0_8;
        } else {
            r13_19 = r12_12;
            goto addr_2bb0_14;
        }
    }
    while (1) {
        addr_2c51_18:
        eax21 = line_delim;
        if (eax21 == r9d22) {
            if (r12_12 != r15_23) 
                goto addr_2ca1_20; else 
                goto addr_2d29_21;
        } else {
            rdi24 = stdout;
            rdx = *reinterpret_cast<void***>(rdi24 + 40);
            if (reinterpret_cast<unsigned char>(rdx) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi24 + 48))) {
                *reinterpret_cast<void***>(rdi24 + 40) = rdx + 1;
                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&r9d22);
            } else {
                *reinterpret_cast<uint32_t*>(&rsi7) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d22));
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                eax25 = fun_2460(rdi24, rsi7, rdi24, rsi7);
                if (eax25 < 0) 
                    goto addr_2c7f_25;
            }
            rax26 = *reinterpret_cast<void***>(r14_27 + 8);
            if (reinterpret_cast<unsigned char>(rax26) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_27 + 16))) 
                goto addr_2c88_27;
            rdx = rax26 + 1;
            *reinterpret_cast<void***>(r14_27 + 8) = rdx;
            r9d22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax26));
            continue;
        }
        addr_2c27_29:
        *reinterpret_cast<int32_t*>(&rbp28) = 0;
        *reinterpret_cast<int32_t*>(&rbp28 + 4) = 0;
        continue;
        while (1) {
            addr_2dac_30:
            if (!r13_19) 
                goto addr_2db5_12;
            addr_2bb0_14:
            *reinterpret_cast<int32_t*>(&r15_23) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_23) + 4) = 0;
            v29 = 0;
            *reinterpret_cast<int32_t*>(&rbp28) = 0;
            *reinterpret_cast<int32_t*>(&rbp28 + 4) = 0;
            rbx30 = delims;
            r14_27 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(v10 + 8) - 8);
            if (!r14_27) 
                goto addr_2ce8_31;
            goto addr_2be0_33;
            addr_2da2_34:
            rsi7 = rax31 + 1;
            *reinterpret_cast<void***>(rcx17 + 40) = rsi7;
            *reinterpret_cast<void***>(rax31) = rdx;
            continue;
            while (1) {
                addr_2d58_35:
                if (!v29) 
                    break;
                rcx17 = stdout;
                if (rbp28) {
                    rdi24 = v8;
                    rdx = rbp28;
                    *reinterpret_cast<uint32_t*>(&rsi7) = 1;
                    *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                    rax32 = fun_2560(rdi24, 1, rdx, rcx17);
                    if (rax32 != rbp28) {
                        while (1) {
                            while (1) {
                                addr_2c7f_25:
                                write_error(rdi24, rsi7, rdi24, rsi7);
                                addr_2c88_27:
                                eax33 = fun_2340(r14_27, rsi7, r14_27, rsi7);
                                r9d22 = eax33;
                                if (eax33 != 0xffffffff) 
                                    goto addr_2c51_18;
                                if (r12_12 != r15_23) 
                                    break;
                                r9d22 = line_delim;
                                addr_2d29_21:
                                rdi24 = stdout;
                                rax34 = *reinterpret_cast<void***>(rdi24 + 40);
                                if (reinterpret_cast<unsigned char>(rax34) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi24 + 48))) 
                                    goto addr_2d3e_40;
                                *reinterpret_cast<uint32_t*>(&rsi7) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r9d22));
                                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                                eax35 = fun_2460(rdi24, rsi7, rdi24, rsi7);
                                if (eax35 >= 0) 
                                    goto addr_2d49_42;
                            }
                            addr_2ca1_20:
                            eax36 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx30));
                            if (*reinterpret_cast<void***>(&eax36)) {
                                rdi24 = stdout;
                                rdx = *reinterpret_cast<void***>(rdi24 + 40);
                                if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi24 + 48))) {
                                    *reinterpret_cast<uint32_t*>(&rsi7) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax36));
                                    *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                                    eax37 = fun_2460(rdi24, rsi7, rdi24, rsi7);
                                    if (eax37 < 0) {
                                        continue;
                                    }
                                } else {
                                    rcx17 = rdx + 1;
                                    *reinterpret_cast<void***>(rdi24 + 40) = rcx17;
                                    *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax36);
                                }
                            }
                            ++rbx30;
                            zf38 = delim_end == rbx30;
                            v29 = 1;
                            if (!zf38) 
                                goto addr_2cbe_49;
                            while (1) {
                                rbx30 = delims;
                                do {
                                    addr_2cbe_49:
                                    rdx = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_12 > r15_23)));
                                    ++r15_23;
                                    if (!(reinterpret_cast<unsigned char>(rdx) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!r13_19)))) 
                                        goto addr_2dac_30;
                                    r14_27 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(v10 + r15_23 * 8) - 8);
                                    if (r14_27) {
                                        addr_2be0_33:
                                        rax39 = *reinterpret_cast<void***>(r14_27 + 8);
                                        if (reinterpret_cast<unsigned char>(rax39) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_27 + 16))) 
                                            goto addr_2bee_52;
                                        eax40 = fun_2340(r14_27, rsi7, r14_27, rsi7);
                                        rax41 = fun_2370();
                                        r9d22 = eax40;
                                        rdx = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rbp28)));
                                        v42 = *rax41;
                                        if (reinterpret_cast<unsigned char>(rdx) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r9d22 != 0xffffffff))) 
                                            goto addr_2bff_54;
                                        if (r9d22 != 0xffffffff) 
                                            goto addr_2c51_18;
                                        rax43 = stdin;
                                        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_27)) & 32)) 
                                            goto addr_2e55_57;
                                    } else {
                                        addr_2ce8_31:
                                        if (r12_12 == r15_23) 
                                            goto addr_2d58_35; else 
                                            goto addr_2ced_58;
                                    }
                                    if (r14_27 == rax43) {
                                        fun_23c0(r14_27, rsi7, r14_27, rsi7);
                                        goto addr_2e9a_61;
                                    }
                                    rax44 = rpl_fclose(r14_27, rsi7, r14_27, rsi7);
                                    if (*reinterpret_cast<int32_t*>(&rax44) == -1) {
                                        *reinterpret_cast<uint32_t*>(&rdx) = v42;
                                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                        if (*reinterpret_cast<uint32_t*>(&rdx)) 
                                            goto addr_2ea2_64;
                                    } else {
                                        addr_2e9a_61:
                                        if (!v42) {
                                            addr_2e6f_65:
                                            --r13_19;
                                            *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(v10 + r15_23 * 8) - 8) = 0;
                                            goto addr_2ce8_31;
                                        } else {
                                            addr_2ea2_64:
                                            rax45 = quotearg_n_style_colon();
                                            *reinterpret_cast<uint32_t*>(&rsi7) = v42;
                                            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                                            rdx = reinterpret_cast<void**>("%s");
                                            rcx17 = rax45;
                                            fun_25a0();
                                            v11 = 0;
                                            goto addr_2e6f_65;
                                        }
                                    }
                                    addr_2f06_66:
                                    v42 = *rax41;
                                    goto addr_2e9a_61;
                                    addr_2e55_57:
                                    if (r14_27 == rax43) {
                                        fun_23c0(r14_27, rsi7, r14_27, rsi7);
                                        goto addr_2e6f_65;
                                    } else {
                                        rax46 = rpl_fclose(r14_27, rsi7, r14_27, rsi7);
                                        if (*reinterpret_cast<int32_t*>(&rax46) == -1) 
                                            goto addr_2f06_66; else 
                                            goto addr_2e6f_65;
                                    }
                                    addr_2ced_58:
                                    eax47 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx30));
                                    if (*reinterpret_cast<signed char*>(&eax47)) {
                                        rcx17 = v8;
                                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx17) + reinterpret_cast<unsigned char>(rbp28)) = *reinterpret_cast<signed char*>(&eax47);
                                        ++rbp28;
                                    }
                                    ++rbx30;
                                    zf48 = delim_end == rbx30;
                                } while (!zf48);
                            }
                            addr_2bee_52:
                            rdx = rax39 + 1;
                            *reinterpret_cast<void***>(r14_27 + 8) = rdx;
                            r9d22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax39));
                            if (!rbp28) 
                                goto addr_2c51_18;
                            addr_2bff_54:
                            rcx17 = stdout;
                            rdi24 = v8;
                            rdx = rbp28;
                            *reinterpret_cast<uint32_t*>(&rsi7) = 1;
                            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                            rax49 = fun_2560(rdi24, 1, rdx, rcx17);
                            r9d22 = r9d22;
                            if (rax49 != rbp28) 
                                continue; else 
                                goto addr_2c27_29;
                            addr_2d3e_40:
                            rdx = rax34 + 1;
                            *reinterpret_cast<void***>(rdi24 + 40) = rdx;
                            *reinterpret_cast<void***>(rax34) = *reinterpret_cast<void***>(&r9d22);
                            addr_2d49_42:
                            v29 = 1;
                            goto addr_2cbe_49;
                        }
                    } else {
                        rcx17 = stdout;
                    }
                }
                *reinterpret_cast<uint32_t*>(&rdx) = line_delim;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                rax31 = *reinterpret_cast<void***>(rcx17 + 40);
                if (reinterpret_cast<unsigned char>(rax31) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx17 + 48))) 
                    goto addr_2da2_34;
                *reinterpret_cast<uint32_t*>(&rsi7) = reinterpret_cast<unsigned char>(rdx);
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                rdi24 = rcx17;
                eax50 = fun_2460(rdi24, rsi7);
                if (eax50 >= 0) 
                    break;
                goto addr_2c7f_25;
            }
        }
    }
}

int64_t fun_2fd3(int64_t rdi, void** rsi) {
    unsigned char v3;
    int64_t r12_4;
    void** rbp5;
    uint32_t* rax6;
    uint32_t* v7;
    int64_t rax8;
    uint32_t eax9;
    void** rdi10;
    int32_t eax11;
    void** r14_12;
    uint32_t ebx13;
    int64_t rax14;
    uint32_t r15d15;
    int64_t rax16;
    void** rax17;
    void** rdx18;
    void** rdx19;
    void** rcx20;
    void** r13_21;
    uint32_t eax22;
    void** rax23;
    uint32_t eax24;
    uint32_t eax25;
    uint32_t eax26;
    void** rdx27;
    int1_t zf28;
    int1_t zf29;
    void** rax30;
    int32_t eax31;
    void** rax32;
    int32_t eax33;
    uint32_t ecx34;
    int32_t eax35;

    __asm__("cli ");
    if (!rdi) {
        v3 = 1;
    } else {
        r12_4 = rdi;
        rbp5 = rsi;
        rax6 = fun_2370();
        v3 = 1;
        v7 = rax6;
        goto addr_3000_4;
    }
    addr_3138_5:
    *reinterpret_cast<uint32_t*>(&rax8) = v3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
    while (1) {
        addr_32c0_6:
        *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax9));
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax11 = fun_2460(rdi10, rsi);
        if (eax11 >= 0) 
            goto addr_306d_7;
        addr_32b8_9:
        *reinterpret_cast<void***>(&eax9) = write_error(rdi10, rsi);
        continue;
        addr_32e8_10:
        goto addr_32b8_9;
        addr_3308_11:
        goto addr_32b8_9;
        while (1) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_12)) & 32) {
                if (ebx13) {
                    rax14 = rpl_fclose(r14_12, rsi);
                    if (*reinterpret_cast<int32_t*>(&rax14)) {
                        if (r15d15) 
                            goto addr_31c6_16;
                        r15d15 = *v7;
                    }
                } else {
                    fun_23c0(r14_12, rsi);
                }
            } else {
                if (ebx13) {
                    rax16 = rpl_fclose(r14_12, rsi);
                    if (!*reinterpret_cast<int32_t*>(&rax16)) 
                        goto addr_312a_21;
                    r15d15 = *v7;
                } else {
                    fun_23c0(r14_12, rsi);
                    goto addr_312a_21;
                }
            }
            if (!r15d15) {
                while (1) {
                    addr_312a_21:
                    rbp5 = rbp5 + 8;
                    --r12_4;
                    if (!r12_4) 
                        goto addr_3138_5;
                    addr_3000_4:
                    ebx13 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp5)) - 45);
                    if (ebx13) 
                        goto addr_3010_25;
                    ebx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp5) + 1));
                    if (!ebx13) 
                        break;
                    addr_3010_25:
                    rax17 = fun_25c0();
                    r14_12 = rax17;
                    if (rax17) 
                        goto addr_3028_27;
                    quotearg_n_style_colon();
                    rdx18 = reinterpret_cast<void**>("%s");
                    *reinterpret_cast<uint32_t*>(&rsi) = *v7;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    fun_25a0();
                    v3 = 0;
                }
            } else {
                addr_31c6_16:
                quotearg_n_style_colon();
                rdx18 = reinterpret_cast<void**>("%s");
                *reinterpret_cast<uint32_t*>(&rsi) = r15d15;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_25a0();
                v3 = 0;
                goto addr_312a_21;
            }
            have_read_stdin = 1;
            r14_12 = stdin;
            addr_3035_30:
            rdx19 = *reinterpret_cast<void***>(r14_12 + 8);
            rcx20 = *reinterpret_cast<void***>(r14_12 + 16);
            r13_21 = delims;
            if (reinterpret_cast<unsigned char>(rdx19) >= reinterpret_cast<unsigned char>(rcx20)) {
                eax22 = fun_2340(r14_12, rsi);
                *reinterpret_cast<uint32_t*>(&rdx18) = eax22;
                r15d15 = *v7;
                if (*reinterpret_cast<uint32_t*>(&rdx18) != 0xffffffff) {
                    rax23 = *reinterpret_cast<void***>(r14_12 + 8);
                    rcx20 = *reinterpret_cast<void***>(r14_12 + 16);
                } else {
                    eax24 = line_delim;
                    goto addr_30f1_34;
                }
            } else {
                rax23 = rdx19 + 1;
                *reinterpret_cast<void***>(r14_12 + 8) = rax23;
                *reinterpret_cast<uint32_t*>(&rdx18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19));
            }
            while (1) {
                if (reinterpret_cast<unsigned char>(rax23) >= reinterpret_cast<unsigned char>(rcx20)) {
                    eax25 = fun_2340(r14_12, rsi);
                    *reinterpret_cast<uint32_t*>(&rdx18) = *reinterpret_cast<uint32_t*>(&rdx18);
                    *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
                    r15d15 = eax25;
                    if (eax25 == 0xffffffff) 
                        break;
                } else {
                    *reinterpret_cast<void***>(r14_12 + 8) = rax23 + 1;
                    r15d15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax23));
                }
                eax26 = line_delim;
                if (eax26 == *reinterpret_cast<uint32_t*>(&rdx18)) {
                    eax9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_21));
                    if (*reinterpret_cast<void***>(&eax9)) {
                        rdi10 = stdout;
                        rdx27 = *reinterpret_cast<void***>(rdi10 + 40);
                        if (reinterpret_cast<unsigned char>(rdx27) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi10 + 48))) 
                            goto addr_32c0_6;
                        ++r13_21;
                        zf28 = delim_end == r13_21;
                        *reinterpret_cast<void***>(rdi10 + 40) = rdx27 + 1;
                        *reinterpret_cast<void***>(rdx27) = *reinterpret_cast<void***>(&eax9);
                        if (!zf28) 
                            goto addr_307e_43; else 
                            goto addr_3180_44;
                    } else {
                        addr_306d_7:
                        ++r13_21;
                        zf29 = delim_end == r13_21;
                        if (zf29) {
                            addr_3180_44:
                            r13_21 = delims;
                            goto addr_307e_43;
                        } else {
                            addr_307e_43:
                            rax23 = *reinterpret_cast<void***>(r14_12 + 8);
                            rcx20 = *reinterpret_cast<void***>(r14_12 + 16);
                            *reinterpret_cast<uint32_t*>(&rdx18) = r15d15;
                        }
                    }
                } else {
                    rdi10 = stdout;
                    rax30 = *reinterpret_cast<void***>(rdi10 + 40);
                    if (reinterpret_cast<unsigned char>(rax30) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi10 + 48))) {
                        *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(rdx18);
                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                        eax31 = fun_2460(rdi10, rsi);
                        if (eax31 >= 0) 
                            goto addr_307e_43; else 
                            goto addr_32b8_9;
                    } else {
                        *reinterpret_cast<void***>(rdi10 + 40) = rax30 + 1;
                        *reinterpret_cast<void***>(rax30) = rdx18;
                        goto addr_307e_43;
                    }
                }
            }
            rdi10 = stdout;
            r15d15 = *v7;
            rax32 = *reinterpret_cast<void***>(rdi10 + 40);
            if (reinterpret_cast<unsigned char>(rax32) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi10 + 48))) {
                *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(rdx18);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                eax33 = fun_2460(rdi10, rsi);
                *reinterpret_cast<uint32_t*>(&rdx18) = *reinterpret_cast<uint32_t*>(&rdx18);
                *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
                if (eax33 < 0) 
                    goto addr_3308_11;
            } else {
                *reinterpret_cast<void***>(rdi10 + 40) = rax32 + 1;
                *reinterpret_cast<void***>(rax32) = rdx18;
            }
            ecx34 = line_delim;
            eax24 = ecx34;
            if (ecx34 == *reinterpret_cast<uint32_t*>(&rdx18)) 
                continue;
            addr_30f1_34:
            rdi10 = stdout;
            rdx18 = *reinterpret_cast<void***>(rdi10 + 40);
            if (reinterpret_cast<unsigned char>(rdx18) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi10 + 48))) {
                *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax24));
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                eax35 = fun_2460(rdi10, rsi);
                if (eax35 >= 0) 
                    continue; else 
                    goto addr_32e8_10;
            } else {
                *reinterpret_cast<void***>(rdi10 + 40) = rdx18 + 1;
                *reinterpret_cast<void***>(rdx18) = *reinterpret_cast<void***>(&eax24);
                continue;
            }
            addr_3028_27:
            *reinterpret_cast<uint32_t*>(&rsi) = 2;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            fadvise(rax17, 2, rdx18, rax17, 2, rdx18);
            goto addr_3035_30;
        }
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_2590(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_24c0(struct s0* rdi, void** rsi, int64_t rdx, struct s0* rcx);

int32_t fun_24e0(int64_t rdi);

int32_t fun_2380(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2600(void** rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3373(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    void** r12_7;
    struct s0* rax8;
    void** r12_9;
    struct s0* rax10;
    void** r12_11;
    struct s0* rax12;
    void** r12_13;
    struct s0* rax14;
    void** r12_15;
    struct s0* rax16;
    void** r12_17;
    struct s0* rax18;
    void** r12_19;
    struct s0* rax20;
    int32_t eax21;
    struct s0* r13_22;
    struct s0* rax23;
    struct s0* rax24;
    int32_t eax25;
    struct s0* rax26;
    struct s0* rax27;
    struct s0* rax28;
    int32_t eax29;
    struct s0* rax30;
    void** r15_31;
    struct s0* rax32;
    struct s0* rax33;
    struct s0* rax34;
    void** rdi35;
    struct s0* r8_36;
    struct s0* r9_37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2400();
            fun_2590(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2400();
            fun_24c0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2400();
            fun_24c0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2400();
            fun_24c0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2400();
            fun_24c0(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2400();
            fun_24c0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2400();
            fun_24c0(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2400();
            fun_24c0(rax20, r12_19, 5, rcx6);
            do {
                if (1) 
                    break;
                eax21 = fun_24e0("paste");
            } while (eax21);
            r13_22 = v4;
            if (!r13_22) {
                rax23 = fun_2400();
                fun_2590(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax24 = fun_2580(5);
                if (!rax24 || (eax25 = fun_2380(rax24, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax25)) {
                    rax26 = fun_2400();
                    r13_22 = reinterpret_cast<struct s0*>("paste");
                    fun_2590(1, rax26, "https://www.gnu.org/software/coreutils/", "paste");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_22 = reinterpret_cast<struct s0*>("paste");
                    goto addr_3710_9;
                }
            } else {
                rax27 = fun_2400();
                fun_2590(1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax28 = fun_2580(5);
                if (!rax28 || (eax29 = fun_2380(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    addr_3616_11:
                    rax30 = fun_2400();
                    fun_2590(1, rax30, "https://www.gnu.org/software/coreutils/", "paste");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_22 == "paste")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x70d6);
                    }
                } else {
                    addr_3710_9:
                    r15_31 = stdout;
                    rax32 = fun_2400();
                    fun_24c0(rax32, r15_31, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3616_11;
                }
            }
            rax33 = fun_2400();
            rcx6 = r12_2;
            fun_2590(1, rax33, r13_22, rcx6);
            addr_33ce_14:
            fun_25e0();
        }
    } else {
        rax34 = fun_2400();
        rdi35 = stderr;
        rcx6 = r12_2;
        fun_2600(rdi35, 1, rax34, rcx6, r8_36, r9_37, v38, v39, v40, v41, v42, v43);
        goto addr_33ce_14;
    }
}

int64_t file_name = 0;

void fun_3743(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3753(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_2390(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_3763() {
    void** rdi1;
    int32_t eax2;
    uint32_t* rax3;
    int1_t zf4;
    uint32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2370(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2400();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_37f3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25a0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2390(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_37f3_5:
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25a0();
    }
}

void fun_3813() {
    __asm__("cli ");
}

void fun_3823(void** rdi, int64_t rsi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2510(rdi, rsi);
        goto 0x24a0;
    }
}

int32_t fun_2550(void** rdi);

int64_t fun_2480(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_23e0(void** rdi);

int64_t fun_3853(void** rdi, int64_t rsi) {
    int32_t eax3;
    int32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t rax7;
    int32_t eax8;
    uint32_t* rax9;
    uint32_t r12d10;
    int64_t rax11;

    __asm__("cli ");
    eax3 = fun_2510(rdi, rsi);
    if (eax3 >= 0) {
        eax4 = fun_2550(rdi);
        if (!(eax4 && (eax5 = fun_2510(rdi, rsi), *reinterpret_cast<int32_t*>(&rdi6) = eax5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0, rax7 = fun_2480(rdi6), rax7 == -1) || (eax8 = rpl_fflush(rdi), eax8 == 0))) {
            rax9 = fun_2370();
            r12d10 = *rax9;
            rax11 = fun_23e0(rdi);
            if (r12d10) {
                *rax9 = r12d10;
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
            return rax11;
        }
    }
    goto fun_23e0;
}

void rpl_fseeko(void** rdi);

void fun_38e3(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2550(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_3933(void** rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax4 = fun_2510(rdi, rsi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2480(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_25f0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s5 {
    signed char[1] pad1;
    unsigned char f1;
    signed char[2] pad4;
    unsigned char f4;
};

struct s5* fun_2470();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_39b3(struct s0* rdi) {
    void** rcx2;
    struct s0* rbx3;
    struct s5* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_25f0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2360("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2470();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2380(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (*reinterpret_cast<unsigned char*>(&rax4->f1) != 0x6c || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5153(int64_t rdi) {
    int64_t rbp2;
    uint32_t* rax3;
    uint32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2370();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xb220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5193(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_51b3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb220);
    }
    *rdi = esi;
    return 0xb220;
}

int64_t fun_51d3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5213(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xb220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_5233(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xb220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x264a;
    if (!rdx) 
        goto 0x264a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb220;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5273(void** rdi, void** rsi, void** rdx, void** rcx, struct s8* r8) {
    struct s8* rbx6;
    uint32_t* rax7;
    uint32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xb220);
    }
    rax7 = fun_2370();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x52a6);
    *rax7 = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_52f3(void** rdi, void** rsi, void*** rdx, struct s9* rcx) {
    struct s9* rbx5;
    uint32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    uint32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xb220);
    }
    rax6 = fun_2370();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5321);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x537c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_53e3() {
    __asm__("cli ");
}

void** gb098 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_53f3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rcx9;
    void** rdi10;
    void** rsi11;
    void** rdx12;
    void** rcx13;
    void** rsi14;
    void** rdx15;
    void** rcx16;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2350(rdi6, rsi7, rdx8, rcx9);
        } while (rbx4 != rbp5);
    }
    rdi10 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi10 != 0xb120) {
        fun_2350(rdi10, rsi11, rdx12, rcx13);
        gb098 = reinterpret_cast<void**>(0xb120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb090) {
        fun_2350(r12_2, rsi14, rdx15, rcx16);
        slotvec = reinterpret_cast<void**>(0xb090);
    }
    nslots = 1;
    return;
}

void fun_5493() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_54b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_54c3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_54e3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5503(void** rdi, int32_t esi, void** rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2650;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2430();
    } else {
        return rax6;
    }
}

void** fun_5593(void** rdi, int32_t esi, void** rdx, void** rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2655;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2430();
    } else {
        return rax7;
    }
}

void** fun_5623(int32_t edi, void** rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x265a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2430();
    } else {
        return rax5;
    }
}

void** fun_56b3(int32_t edi, void** rsi, void** rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x265f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2430();
    } else {
        return rax6;
    }
}

void** fun_5743(void** rdi, void** rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5ad0]");
    __asm__("movdqa xmm1, [rip+0x5ad8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5ac1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2430();
    } else {
        return rax10;
    }
}

void** fun_57e3(void** rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5a30]");
    __asm__("movdqa xmm1, [rip+0x5a38]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5a21]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2430();
    } else {
        return rax9;
    }
}

void** fun_5883(void** rdi) {
    struct s0* rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5990]");
    __asm__("movdqa xmm1, [rip+0x5998]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x5979]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2430();
    } else {
        return rax3;
    }
}

void** fun_5913(void** rdi, void** rsi) {
    struct s0* rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5900]");
    __asm__("movdqa xmm1, [rip+0x5908]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x58f6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2430();
    } else {
        return rax4;
    }
}

void** fun_59a3(void** rdi, int32_t esi, void** rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2664;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2430();
    } else {
        return rax6;
    }
}

void** fun_5a43(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x57ca]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x57c2]");
    __asm__("movdqa xmm2, [rip+0x57ca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2669;
    if (!rdx) 
        goto 0x2669;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2430();
    } else {
        return rax7;
    }
}

void** fun_5ae3(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, void** r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x572a]");
    __asm__("movdqa xmm1, [rip+0x5732]");
    __asm__("movdqa xmm2, [rip+0x573a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x266e;
    if (!rdx) 
        goto 0x266e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2430();
    } else {
        return rax9;
    }
}

void** fun_5b93(int64_t rdi, int64_t rsi, void** rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x567a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5672]");
    __asm__("movdqa xmm2, [rip+0x567a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2673;
    if (!rsi) 
        goto 0x2673;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2430();
    } else {
        return rax6;
    }
}

void** fun_5c33(int64_t rdi, int64_t rsi, void** rdx, void** rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x55da]");
    __asm__("movdqa xmm1, [rip+0x55e2]");
    __asm__("movdqa xmm2, [rip+0x55ea]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2678;
    if (!rsi) 
        goto 0x2678;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2430();
    } else {
        return rax7;
    }
}

void fun_5cd3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5ce3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5d03() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5d23(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s10 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_24f0(int64_t rdi, void** rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_5d43(void** rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s10* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2600(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2600(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2400();
    fun_2600(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_24f0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2400();
    fun_2600(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_24f0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2400();
        fun_2600(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7c48 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7c48;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_61b3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_61d3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2430();
    } else {
        return;
    }
}

void fun_6273(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6316_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6320_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2430();
    } else {
        return;
    }
    addr_6316_5:
    goto addr_6320_7;
}

void fun_6353() {
    void** rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_24f0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2400();
    fun_2590(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2400();
    fun_2590(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2400();
    goto fun_2590;
}

int64_t fun_23b0();

void xalloc_die();

void fun_63f3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23b0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2520(signed char* rdi);

void fun_6433(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6453(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6473(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2570();

void fun_6493(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2570();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_64c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2570();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_64f3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23b0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6533() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6573(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_65a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_65f3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23b0();
            if (rax5) 
                break;
            addr_663d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_663d_5;
        rax8 = fun_23b0();
        if (rax8) 
            goto addr_6626_9;
        if (rbx4) 
            goto addr_663d_5;
        addr_6626_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6683(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23b0();
            if (rax8) 
                break;
            addr_66ca_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_66ca_5;
        rax11 = fun_23b0();
        if (rax11) 
            goto addr_66b2_9;
        if (!rbx6) 
            goto addr_66b2_9;
        if (r12_4) 
            goto addr_66ca_5;
        addr_66b2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6713(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_67bd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_67d0_10:
                *r12_8 = 0;
            }
            addr_6770_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6796_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_67e4_14;
            if (rcx10 <= rsi9) 
                goto addr_678d_16;
            if (rsi9 >= 0) 
                goto addr_67e4_14;
            addr_678d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_67e4_14;
            addr_6796_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2570();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_67e4_14;
            if (!rbp13) 
                break;
            addr_67e4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_67bd_9;
        } else {
            if (!r13_6) 
                goto addr_67d0_10;
            goto addr_6770_11;
        }
    }
}

int64_t fun_24d0();

void fun_6813() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6843() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6873() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6893() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2500(signed char* rdi, struct s0* rsi, signed char* rdx);

void fun_68b3(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2520(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2500;
    }
}

void fun_68f3(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2520(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2500;
    }
}

struct s12 {
    signed char[1] pad1;
    signed char f1;
};

void fun_6933(int64_t rdi, struct s12* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2520(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2500;
    }
}

struct s0* fun_2420(struct s0* rdi, ...);

void fun_6973(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2420(rdi);
    rax3 = fun_2520(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2500;
    }
}

void fun_69b3() {
    void** rdi1;

    __asm__("cli ");
    fun_2400();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_25a0();
    fun_2360(rdi1);
}

int64_t fun_23a0();

int64_t fun_69f3(void** rdi, void** rsi) {
    int64_t rax3;
    uint32_t ebx4;
    int64_t rax5;
    uint32_t* rax6;
    uint32_t* rax7;

    __asm__("cli ");
    rax3 = fun_23a0();
    ebx4 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax5 = rpl_fclose(rdi, rsi);
    if (ebx4) {
        if (*reinterpret_cast<int32_t*>(&rax5)) {
            addr_6a4e_3:
            *reinterpret_cast<int32_t*>(&rax5) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        } else {
            rax6 = fun_2370();
            *rax6 = 0;
            *reinterpret_cast<int32_t*>(&rax5) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax5)) {
            if (rax3) 
                goto addr_6a4e_3;
            rax7 = fun_2370();
            *reinterpret_cast<int32_t*>(&rax5) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax7 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
    }
    return rax5;
}

signed char* fun_2540(int64_t rdi);

signed char* fun_6a63() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2540(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2450(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6aa3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2450(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2430();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6b33() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2430();
    } else {
        return rax3;
    }
}

int64_t fun_6bb3(int64_t rdi, signed char* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2580(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2420(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2500(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2500(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6c63() {
    __asm__("cli ");
    goto fun_2580;
}

void fun_6c73() {
    __asm__("cli ");
}

void fun_6c87() {
    __asm__("cli ");
    return;
}

void fun_28d0() {
    goto 0x2856;
}

uint32_t fun_24b0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_2620(int64_t rdi, struct s0* rsi);

uint32_t fun_2610(struct s0* rdi, struct s0* rsi);

void** fun_2630(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_3be5() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2400();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2400();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *reinterpret_cast<unsigned char*>(&rdx10->f0), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2420(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3ee3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3ee3_22; else 
                            goto addr_42dd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_439d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_46f0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3ee0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3ee0_30; else 
                                goto addr_4709_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2420(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_46f0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24b0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_46f0_28; else 
                            goto addr_3d8c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4850_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_46d0_40:
                        if (r11_27 == 1) {
                            addr_425d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4818_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3e97_44;
                            }
                        } else {
                            goto addr_46e0_46;
                        }
                    } else {
                        addr_485f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_425d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3ee3_22:
                                if (v47 != 1) {
                                    addr_4439_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2420(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4484_54;
                                    }
                                } else {
                                    goto addr_3ef0_56;
                                }
                            } else {
                                addr_3e95_57:
                                ebp36 = 0;
                                goto addr_3e97_44;
                            }
                        } else {
                            addr_46c4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_485f_47; else 
                                goto addr_46ce_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_425d_41;
                        if (v47 == 1) 
                            goto addr_3ef0_56; else 
                            goto addr_4439_52;
                    }
                }
                addr_3f51_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3de8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3e0d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4110_65;
                    } else {
                        addr_3f79_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_47c8_67;
                    }
                } else {
                    goto addr_3f70_69;
                }
                addr_3e21_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_3e6c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_47c8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3e6c_81;
                }
                addr_3f70_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3e0d_64; else 
                    goto addr_3f79_66;
                addr_3e97_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_3f4f_91;
                if (v22) 
                    goto addr_3eaf_93;
                addr_3f4f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3f51_62;
                addr_4484_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4c0b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4c7b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *reinterpret_cast<unsigned char*>(&rdx10->f0) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_4a7f_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2620(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2610(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_457e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_3f3c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4588_112;
                    }
                } else {
                    addr_4588_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4659_114;
                }
                addr_3f48_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_3f4f_91;
                while (1) {
                    addr_4659_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4b67_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_45c6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4b75_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4647_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4647_128;
                        }
                    }
                    addr_45f5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4647_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_45c6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_45f5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3e6c_81;
                addr_4b75_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_47c8_67;
                addr_4c0b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_457e_109;
                addr_4c7b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_457e_109;
                addr_3ef0_56:
                rax93 = fun_2630(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_3f3c_110;
                addr_46ce_59:
                goto addr_46d0_40;
                addr_439d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3ee3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3f48_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3e95_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3ee3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_43e2_160;
                if (!v22) 
                    goto addr_47b7_162; else 
                    goto addr_49c3_163;
                addr_43e2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_47b7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_47c8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_428b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_40f3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3e21_70; else 
                    goto addr_4107_169;
                addr_428b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3de8_63;
                goto addr_3f70_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_46c4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_47ff_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3ee0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3dd8_178; else 
                        goto addr_4782_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_46c4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3ee3_22;
                }
                addr_47ff_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3ee0_30:
                    r8d42 = 0;
                    goto addr_3ee3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3f51_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4818_42;
                    }
                }
                addr_3dd8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3de8_63;
                addr_4782_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_46e0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3f51_62;
                } else {
                    addr_4792_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3ee3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4f42_188;
                if (v28) 
                    goto addr_47b7_162;
                addr_4f42_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_40f3_168;
                addr_3d8c_37:
                if (v22) 
                    goto addr_4d83_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3da3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4850_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_48db_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3ee3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3dd8_178; else 
                        goto addr_48b7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_46c4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3ee3_22;
                }
                addr_48db_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3ee3_22;
                }
                addr_48b7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_46e0_46;
                goto addr_4792_186;
                addr_3da3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3ee3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3ee3_22; else 
                    goto addr_3db4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_4e8e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4d14_210;
            if (1) 
                goto addr_4d12_212;
            if (!v29) 
                goto addr_494e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2410();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_4e81_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4110_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_3ecb_219; else 
            goto addr_412a_220;
        addr_3eaf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3ec3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_412a_220; else 
            goto addr_3ecb_219;
        addr_4a7f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_412a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2410();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_4a9d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2410();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_4f10_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4976_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_4b67_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3ec3_221;
        addr_49c3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3ec3_221;
        addr_4107_169:
        goto addr_4110_65;
        addr_4e8e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_412a_220;
        goto addr_4a9d_222;
        addr_4d14_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(&v26->f0), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_4d6e_236;
        fun_2430();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4f10_225;
        addr_4d12_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4d14_210;
        addr_494e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4d14_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_4976_226;
        }
        addr_4e81_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_42dd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x770c + rax113 * 4) + 0x770c;
    addr_4709_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x780c + rax114 * 4) + 0x780c;
    addr_4d83_190:
    addr_3ecb_219:
    goto 0x3bb0;
    addr_3db4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x760c + rax115 * 4) + 0x760c;
    addr_4d6e_236:
    goto v116;
}

void fun_3dd0() {
}

void fun_3f88() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3c82;
}

void fun_3fe1() {
    goto 0x3c82;
}

void fun_40ce() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3f51;
    }
    if (v2) 
        goto 0x49c3;
    if (!r10_3) 
        goto addr_4b2e_5;
    if (!v4) 
        goto addr_49fe_7;
    addr_4b2e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_49fe_7:
    goto 0x3e04;
}

void fun_40ec() {
}

void fun_4197() {
    signed char v1;

    if (v1) {
        goto 0x411f;
    } else {
        goto 0x3e5a;
    }
}

void fun_41b1() {
    signed char v1;

    if (!v1) 
        goto 0x41aa; else 
        goto "???";
}

void fun_41d8() {
    goto 0x40f3;
}

void fun_4258() {
}

void fun_4270() {
}

void fun_429f() {
    goto 0x40f3;
}

void fun_42f1() {
    goto 0x4280;
}

void fun_4320() {
    goto 0x4280;
}

void fun_4353() {
    goto 0x4280;
}

void fun_4720() {
    goto 0x3dd8;
}

void fun_4a1e() {
    signed char v1;

    if (v1) 
        goto 0x49c3;
    goto 0x3e04;
}

void fun_4ac5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3e04;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3de8;
        goto 0x3e04;
    }
}

void fun_4ee2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4150;
    } else {
        goto 0x3c82;
    }
}

void fun_5e18() {
    fun_2400();
}

void fun_28dd() {
    goto 0x2856;
}

void fun_400e() {
    goto 0x3c82;
}

void fun_41e4() {
    goto 0x419c;
}

void fun_42ab() {
    goto 0x3dd8;
}

void fun_42fd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4280;
    goto 0x3eaf;
}

void fun_432f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x428b;
        goto 0x3cb0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x412a;
        goto 0x3ecb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4ac8;
    if (r10_8 > r15_9) 
        goto addr_4215_9;
    addr_421a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4ad3;
    goto 0x3e04;
    addr_4215_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_421a_10;
}

void fun_4362() {
    goto 0x3e97;
}

void fun_4730() {
    goto 0x3e97;
}

void fun_4ecf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x3fec;
    } else {
        goto 0x4150;
    }
}

void fun_5ed0() {
}

void fun_28ea() {
    goto 0x2856;
}

void fun_436c() {
    goto 0x4307;
}

void fun_473a() {
    goto 0x425d;
}

void fun_5f30() {
    fun_2400();
    goto fun_2600;
}

void fun_28f7() {
    goto 0x2856;
}

void fun_403d() {
    goto 0x3c82;
}

void fun_4378() {
    goto 0x4307;
}

void fun_4747() {
    goto 0x42ae;
}

void fun_5f70() {
    fun_2400();
    goto fun_2600;
}

void fun_2904() {
    goto 0x2856;
}

void fun_406a() {
    goto 0x3c82;
}

void fun_4384() {
    goto 0x4280;
}

void fun_5fb0() {
    fun_2400();
    goto fun_2600;
}

void fun_2911() {
    goto 0x2856;
}

void fun_408c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4a20;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3f51;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3f51;
    }
    if (v11) 
        goto 0x4d83;
    if (r10_12 > r15_13) 
        goto addr_4dd3_8;
    addr_4dd8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4b11;
    addr_4dd3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4dd8_9;
}

struct s13 {
    signed char[24] pad24;
    int64_t f18;
};

struct s14 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s15 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_6000() {
    int64_t r15_1;
    struct s13* rbx2;
    struct s0* r14_3;
    struct s14* rbx4;
    struct s0* r13_5;
    struct s15* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2400();
    fun_2600(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6022, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6058() {
    fun_2400();
    goto 0x6029;
}

struct s16 {
    signed char[32] pad32;
    int64_t f20;
};

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s20 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6090() {
    int64_t rcx1;
    struct s16* rbx2;
    int64_t r15_3;
    struct s17* rbx4;
    struct s0* r14_5;
    struct s18* rbx6;
    struct s0* r13_7;
    struct s19* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s20* rbx12;
    struct s0* rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2400();
    fun_2600(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x60c4, __return_address(), rcx1);
    goto v15;
}

void fun_6108() {
    fun_2400();
    goto 0x60cb;
}
