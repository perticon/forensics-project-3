undefined  [16] main(int param_1,undefined8 *param_2)

{
  int iVar1;
  uint uVar2;
  code *pcVar3;
  undefined8 uVar4;
  int *piVar5;
  char *pcVar6;
  char cVar7;
  char *pcVar8;
  undefined8 uVar9;
  undefined8 uStack56;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  have_read_stdin = '\0';
  serial_merge = '\0';
  pcVar8 = "\t";
  while( true ) {
    uVar9 = 0x102717;
    iVar1 = getopt_long(param_1,param_2,&DAT_0010710e,longopts,0);
    if (iVar1 == -1) break;
    if (iVar1 == 100) {
      pcVar8 = optarg;
      if (*optarg == '\0') {
        pcVar8 = "\\0";
      }
    }
    else {
      if (iVar1 < 0x65) {
        if (iVar1 == -0x83) {
          version_etc(stdout,"paste","GNU coreutils",Version,"David M. Ihnat","David MacKenzie",0,
                      uVar9);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar1 == -0x82) {
                    /* WARNING: Subroutine does not return */
          usage(0);
        }
        goto LAB_0010298d;
      }
      if (iVar1 == 0x73) {
        serial_merge = '\x01';
      }
      else {
        if (iVar1 != 0x7a) goto LAB_0010298d;
        line_delim = 0;
      }
    }
  }
  param_1 = param_1 - optind;
  if (param_1 == 0) {
    param_1 = 1;
    param_2[optind] = &DAT_00107113;
  }
  delims = (char *)xstrdup(pcVar8);
  cVar7 = *pcVar8;
  delim_end = delims;
  pcVar6 = pcVar8;
  do {
    if (cVar7 == '\0') {
      pcVar3 = paste_serial;
      if (serial_merge == '\0') {
        pcVar3 = paste_parallel;
      }
      uVar2 = (*pcVar3)((long)param_1,param_2 + optind);
      param_2 = (undefined8 *)(ulong)uVar2;
      free(delims);
      if (have_read_stdin != '\0') {
LAB_0010295e:
        uVar2 = (uint)param_2;
        iVar1 = rpl_fclose(stdin);
        if (iVar1 == -1) {
          piVar5 = __errno_location();
          error(1,*piVar5,&DAT_00107113);
LAB_0010298d:
                    /* WARNING: Subroutine does not return */
          usage(1);
        }
      }
      return CONCAT88(uStack56,(ulong)((uVar2 ^ 1) & 0xff));
    }
    if (cVar7 == '\\') {
      cVar7 = pcVar6[1];
      if (cVar7 < 'w') {
        if (cVar7 < '\\') {
          if (cVar7 == '\0') {
            uVar9 = quotearg_n_style_colon(0,6,pcVar8);
            uVar4 = dcgettext(0,"delimiter list ends with an unescaped backslash: %s",5);
            error(1,0,uVar4,uVar9);
            goto LAB_0010295e;
          }
          if (cVar7 == '0') {
            cVar7 = '\0';
          }
        }
        else {
          switch(cVar7) {
          case 'b':
            cVar7 = '\b';
            break;
          case 'f':
            cVar7 = '\f';
            break;
          case 'n':
            cVar7 = '\n';
            break;
          case 'r':
            cVar7 = '\r';
            break;
          case 't':
            cVar7 = '\t';
            break;
          case 'v':
            cVar7 = '\v';
          }
        }
      }
      *delim_end = cVar7;
      pcVar6 = pcVar6 + 2;
    }
    else {
      *delim_end = cVar7;
      pcVar6 = pcVar6 + 1;
    }
    cVar7 = *pcVar6;
    delim_end = delim_end + 1;
  } while( true );
}