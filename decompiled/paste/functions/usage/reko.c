void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002600(fn0000000000002400(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000033CE;
	}
	fn0000000000002590(fn0000000000002400(0x05, "Usage: %s [OPTION]... [FILE]...\n", null), 0x01);
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "Write lines consisting of the sequentially corresponding lines from\neach FILE, separated by TABs, to standard output.\n", null));
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "  -d, --delimiters=LIST   reuse characters from LIST instead of TABs\n  -s, --serial            paste one file at a time instead of in parallel\n", null));
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "  -z, --zero-terminated    line delimiter is NUL, not newline\n", null));
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "      --version     output version information and exit\n", null));
	struct Eq_1593 * rbx_191 = fp - 0xB8 + 16;
	do
	{
		char * rsi_193 = rbx_191->qw0000;
		++rbx_191;
	} while (rsi_193 != null && fn00000000000024E0(rsi_193, "paste") != 0x00);
	ptr64 r13_206 = rbx_191->qw0008;
	if (r13_206 != 0x00)
	{
		fn0000000000002590(fn0000000000002400(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_293 = fn0000000000002580(null, 0x05);
		if (rax_293 == 0x00 || fn0000000000002380(0x03, "en_", rax_293) == 0x00)
			goto l0000000000003616;
	}
	else
	{
		fn0000000000002590(fn0000000000002400(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_235 = fn0000000000002580(null, 0x05);
		if (rax_235 == 0x00 || fn0000000000002380(0x03, "en_", rax_235) == 0x00)
		{
			fn0000000000002590(fn0000000000002400(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003653:
			fn0000000000002590(fn0000000000002400(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000033CE:
			fn00000000000025E0(edi);
		}
		r13_206 = 0x7029;
	}
	fn00000000000024C0(stdout, fn0000000000002400(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003616:
	fn0000000000002590(fn0000000000002400(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003653;
}