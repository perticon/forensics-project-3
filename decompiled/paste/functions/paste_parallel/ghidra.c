char paste_parallel(long param_1,char **param_2)

{
  char *pcVar1;
  char **ppcVar2;
  byte bVar3;
  int iVar4;
  uint uVar5;
  uint uVar6;
  uint uVar7;
  void *__ptr;
  _IO_FILE **__ptr_00;
  size_t sVar8;
  int *piVar9;
  undefined8 uVar10;
  uint *puVar11;
  FILE *pFVar12;
  byte *pbVar13;
  uint uVar14;
  byte *pbVar15;
  byte *pbVar16;
  size_t __n;
  byte **ppbVar17;
  long lVar18;
  _IO_FILE *p_Var19;
  long lVar20;
  ulong uVar21;
  ulong uVar22;
  char **ppcVar23;
  byte *pbVar24;
  ulong uVar25;
  ulong uVar26;
  bool bVar27;
  char cStack161;
  int local_54;
  char local_4d;
  
  __ptr = (void *)xmalloc(param_1 + 2);
  __ptr_00 = (_IO_FILE **)xnmalloc(param_1 + 1);
  if (param_1 == 0) {
    local_4d = '\x01';
LAB_00102db5:
    free(__ptr_00);
    free(__ptr);
    return local_4d;
  }
  local_4d = '\0';
  lVar18 = 0;
  ppcVar2 = param_2;
  do {
    while ((ppcVar23 = ppcVar2, lVar20 = lVar18, pcVar1 = *ppcVar23, *pcVar1 != '-' ||
           (pcVar1[1] != '\0'))) {
      pFVar12 = fopen(pcVar1,"r");
      __ptr_00[lVar20] = pFVar12;
      if (pFVar12 == (FILE *)0x0) goto LAB_00102fa0;
      iVar4 = fileno(pFVar12);
      if (iVar4 == 0) {
        local_4d = '\x01';
      }
      fadvise(pFVar12);
      lVar18 = lVar20 + 1;
      ppcVar2 = ppcVar23 + 1;
      if (param_1 == lVar20 + 1) goto LAB_00102b95;
    }
    have_read_stdin = '\x01';
    __ptr_00[lVar20] = stdin;
    lVar18 = lVar20 + 1;
    ppcVar2 = ppcVar23 + 1;
  } while (param_1 != lVar20 + 1);
LAB_00102b95:
  ppcVar23 = ppcVar23 + 1;
  uVar21 = lVar20 + 1;
  uVar22 = uVar21;
  if (local_4d == '\0') {
    local_4d = '\x01';
  }
  else if (have_read_stdin != '\0') {
    uVar10 = dcgettext(0,"standard input is closed",5);
    error(1,0,uVar10);
LAB_00102fa0:
    uVar10 = quotearg_n_style_colon(0,3,*ppcVar23);
    puVar11 = (uint *)__errno_location();
    lVar18 = 1;
    ppbVar17 = (byte **)(ulong)*puVar11;
    error(1,ppbVar17,"%s",uVar10);
    if (lVar18 == 0) {
      cStack161 = '\x01';
    }
    else {
      puVar11 = (uint *)__errno_location();
      cStack161 = '\x01';
      do {
        pbVar16 = *ppbVar17;
        uVar6 = *pbVar16 - 0x2d;
        if ((uVar6 != 0) || (uVar6 = (uint)pbVar16[1], pbVar16[1] != 0)) {
          pFVar12 = fopen((char *)pbVar16,"r");
          if (pFVar12 != (FILE *)0x0) {
            fadvise(pFVar12);
            goto LAB_00103035;
          }
          uVar10 = quotearg_n_style_colon(0,3,*ppbVar17);
          error(0,*puVar11,"%s",uVar10);
          cStack161 = '\0';
          goto LAB_0010312a;
        }
        have_read_stdin = '\x01';
        pFVar12 = stdin;
LAB_00103035:
        pbVar24 = delims;
        pbVar16 = (byte *)pFVar12->_IO_read_ptr;
        pbVar15 = (byte *)pFVar12->_IO_read_end;
        if (pbVar16 < pbVar15) {
          pbVar13 = pbVar16 + 1;
          pFVar12->_IO_read_ptr = (char *)pbVar13;
          uVar7 = (uint)*pbVar16;
        }
        else {
          uVar7 = __uflow(pFVar12);
          uVar5 = *puVar11;
          if (uVar7 == 0xffffffff) {
            uVar14 = (uint)line_delim;
            goto LAB_001030f1;
          }
          pbVar13 = (byte *)pFVar12->_IO_read_ptr;
          pbVar15 = (byte *)pFVar12->_IO_read_end;
        }
LAB_00103089:
        if (pbVar13 < pbVar15) {
          pFVar12->_IO_read_ptr = (char *)(pbVar13 + 1);
          uVar5 = (uint)*pbVar13;
LAB_0010309e:
          p_Var19 = stdout;
          if (line_delim == uVar7) {
            bVar3 = *pbVar24;
            if (bVar3 == 0) goto LAB_0010306d;
            pbVar16 = (byte *)stdout->_IO_write_ptr;
            if (stdout->_IO_write_end <= pbVar16) goto LAB_001032c0;
            pbVar24 = pbVar24 + 1;
            bVar27 = delim_end != pbVar24;
            stdout->_IO_write_ptr = (char *)(pbVar16 + 1);
            *pbVar16 = bVar3;
            if (bVar27) goto LAB_0010307e;
            goto LAB_00103180;
          }
          pcVar1 = stdout->_IO_write_ptr;
          if (pcVar1 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar1 + 1;
            *pcVar1 = (char)uVar7;
          }
          else {
            iVar4 = __overflow(stdout,uVar7 & 0xff);
            if (iVar4 < 0) goto LAB_001032b8;
          }
          goto LAB_0010307e;
        }
        uVar5 = __uflow(pFVar12);
        if (uVar5 != 0xffffffff) goto LAB_0010309e;
        uVar5 = *puVar11;
        pcVar1 = stdout->_IO_write_ptr;
        if (stdout->_IO_write_end <= pcVar1) {
          p_Var19 = stdout;
          iVar4 = __overflow(stdout,uVar7 & 0xff);
          if (-1 < iVar4) goto LAB_00103242;
LAB_001032b8:
          do {
            bVar3 = write_error();
LAB_001032c0:
            iVar4 = __overflow(p_Var19,(uint)bVar3);
          } while (iVar4 < 0);
LAB_0010306d:
          pbVar24 = pbVar24 + 1;
          if (delim_end == pbVar24) {
LAB_00103180:
            pbVar24 = delims;
          }
LAB_0010307e:
          pbVar13 = (byte *)pFVar12->_IO_read_ptr;
          pbVar15 = (byte *)pFVar12->_IO_read_end;
          uVar7 = uVar5;
          goto LAB_00103089;
        }
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar7;
LAB_00103242:
        uVar14 = (uint)line_delim;
        if (uVar14 == uVar7) goto LAB_00103110;
LAB_001030f1:
        pcVar1 = stdout->_IO_write_ptr;
        if (stdout->_IO_write_end <= pcVar1) {
          p_Var19 = stdout;
          iVar4 = __overflow(stdout,uVar14);
          if (-1 < iVar4) goto LAB_00103110;
          goto LAB_001032b8;
        }
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar14;
LAB_00103110:
        if ((*(byte *)&pFVar12->_flags & 0x20) == 0) {
          if (uVar6 == 0) {
            clearerr_unlocked(pFVar12);
          }
          else {
            iVar4 = rpl_fclose();
            if (iVar4 != 0) {
              uVar5 = *puVar11;
              goto LAB_001031bd;
            }
          }
        }
        else {
          if (uVar6 == 0) {
            clearerr_unlocked(pFVar12);
LAB_001031bd:
            if (uVar5 == 0) goto LAB_0010312a;
          }
          else {
            iVar4 = rpl_fclose();
            if (iVar4 == 0) goto LAB_001031bd;
            if (uVar5 == 0) {
              uVar5 = *puVar11;
              goto LAB_001031bd;
            }
          }
          uVar10 = quotearg_n_style_colon(0,3,*ppbVar17);
          error(0,uVar5,"%s",uVar10);
          cStack161 = '\0';
        }
LAB_0010312a:
        ppbVar17 = ppbVar17 + 1;
        lVar18 = lVar18 + -1;
      } while (lVar18 != 0);
    }
    return cStack161;
  }
LAB_00102bb0:
  uVar25 = 1;
  bVar27 = false;
  __n = 0;
  p_Var19 = *__ptr_00;
  pbVar16 = delims;
  if (p_Var19 == (_IO_FILE *)0x0) goto LAB_00102ce8;
LAB_00102be0:
  pbVar15 = (byte *)p_Var19->_IO_read_ptr;
  if (pbVar15 < p_Var19->_IO_read_end) {
    p_Var19->_IO_read_ptr = (char *)(pbVar15 + 1);
    uVar6 = (uint)*pbVar15;
    uVar5 = (uint)*pbVar15;
    if (__n == 0) goto LAB_00102c51;
  }
  else {
    uVar6 = __uflow(p_Var19);
    piVar9 = __errno_location();
    local_54 = *piVar9;
    if (__n == 0 || uVar6 == 0xffffffff) {
      uVar5 = uVar6;
      if (uVar6 != 0xffffffff) goto LAB_00102c51;
      if ((*(byte *)&p_Var19->_flags & 0x20) == 0) {
        if (p_Var19 == stdin) {
          clearerr_unlocked(p_Var19);
          goto LAB_00102e6f;
        }
        iVar4 = rpl_fclose();
        if (iVar4 != -1) goto LAB_00102e6f;
LAB_00102f06:
        local_54 = *piVar9;
LAB_00102e9a:
        if (local_54 == 0) goto LAB_00102e6f;
      }
      else {
        if (p_Var19 == stdin) {
          clearerr_unlocked(p_Var19);
          goto LAB_00102e9a;
        }
        iVar4 = rpl_fclose();
        if (iVar4 != -1) goto LAB_00102e9a;
        if (local_54 == 0) goto LAB_00102f06;
      }
      uVar10 = quotearg_n_style_colon(0,3,param_2[uVar25 - 1]);
      error(0,local_54,"%s",uVar10);
      local_4d = '\0';
LAB_00102e6f:
      uVar22 = uVar22 - 1;
      __ptr_00[uVar25 - 1] = (_IO_FILE *)0x0;
      goto LAB_00102ce8;
    }
  }
  sVar8 = fwrite_unlocked(__ptr,1,__n,stdout);
  if (sVar8 != __n) goto LAB_00102c7f;
  __n = 0;
  uVar5 = uVar6;
LAB_00102c51:
  do {
    while( true ) {
      if (line_delim == uVar5) {
        if (uVar21 != uVar25) goto LAB_00102ca1;
        goto LAB_00102d29;
      }
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar5;
      }
      else {
        iVar4 = __overflow(stdout,uVar5 & 0xff);
        if (iVar4 < 0) goto LAB_00102c7f;
      }
      pbVar15 = (byte *)p_Var19->_IO_read_ptr;
      if (p_Var19->_IO_read_end <= pbVar15) break;
      p_Var19->_IO_read_ptr = (char *)(pbVar15 + 1);
      uVar5 = (uint)*pbVar15;
    }
    while (uVar5 = __uflow(p_Var19), uVar5 == 0xffffffff) {
      if (uVar21 != uVar25) {
LAB_00102ca1:
        bVar3 = *pbVar16;
        if (bVar3 != 0) {
          pbVar15 = (byte *)stdout->_IO_write_ptr;
          if (pbVar15 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = (char *)(pbVar15 + 1);
            *pbVar15 = bVar3;
          }
          else {
            iVar4 = __overflow(stdout,(uint)bVar3);
            if (iVar4 < 0) goto LAB_00102c7f;
          }
        }
        pbVar16 = pbVar16 + 1;
        bVar27 = true;
        uVar26 = uVar25;
        if (delim_end == pbVar16) goto LAB_00102d0d;
LAB_00102cbe:
        while( true ) {
          uVar25 = uVar26 + 1;
          if (uVar21 <= uVar26 || uVar22 == 0) goto LAB_00102dac;
          p_Var19 = __ptr_00[uVar26];
          if (p_Var19 != (_IO_FILE *)0x0) goto LAB_00102be0;
LAB_00102ce8:
          if (uVar21 == uVar25) break;
          if (*pbVar16 != 0) {
            *(byte *)((long)__ptr + __n) = *pbVar16;
            __n = __n + 1;
          }
          pbVar16 = pbVar16 + 1;
          uVar26 = uVar25;
          if (delim_end == pbVar16) {
LAB_00102d0d:
            pbVar16 = delims;
            uVar26 = uVar25;
          }
        }
        if (bVar27) {
          if ((__n != 0) && (sVar8 = fwrite_unlocked(__ptr,1,__n,stdout), sVar8 != __n))
          goto LAB_00102c7f;
          bVar3 = line_delim;
          pbVar15 = (byte *)stdout->_IO_write_ptr;
          if (pbVar15 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = (char *)(pbVar15 + 1);
            *pbVar15 = bVar3;
          }
          else {
            iVar4 = __overflow(stdout,(uint)line_delim);
            if (iVar4 < 0) goto LAB_00102c7f;
          }
        }
LAB_00102dac:
        if (uVar22 != 0) goto LAB_00102bb0;
        goto LAB_00102db5;
      }
      uVar5 = (uint)line_delim;
LAB_00102d29:
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar5;
LAB_00102d49:
        bVar27 = true;
        uVar26 = uVar25;
        goto LAB_00102cbe;
      }
      iVar4 = __overflow(stdout,uVar5 & 0xff);
      if (-1 < iVar4) goto LAB_00102d49;
LAB_00102c7f:
      write_error();
    }
  } while( true );
}