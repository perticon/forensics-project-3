char write_error(void)

{
  char *pcVar1;
  long lVar2;
  char **ppcVar3;
  byte bVar4;
  int iVar5;
  uint uVar6;
  uint uVar7;
  uint uVar8;
  undefined8 uVar9;
  uint *puVar10;
  void *__ptr;
  _IO_FILE **__ptr_00;
  size_t sVar11;
  int *piVar12;
  FILE *pFVar13;
  byte *pbVar14;
  uint uVar15;
  byte *pbVar16;
  byte *pbVar17;
  size_t __n;
  byte **ppbVar18;
  char **ppcVar19;
  long lVar20;
  _IO_FILE *p_Var21;
  long lVar22;
  ulong uVar23;
  ulong uVar24;
  char **ppcVar25;
  byte *pbVar26;
  ulong uVar27;
  ulong uVar28;
  bool bVar29;
  char cStack169;
  int iStack92;
  char cStack85;
  
  uVar9 = dcgettext(0,"write error",5);
  puVar10 = (uint *)__errno_location();
  lVar20 = 1;
  ppcVar19 = (char **)(ulong)*puVar10;
  error(1,ppcVar19,uVar9);
  __ptr = (void *)xmalloc(lVar20 + 2);
  __ptr_00 = (_IO_FILE **)xnmalloc(lVar20 + 1);
  if (lVar20 == 0) {
    cStack85 = '\x01';
LAB_00102db5:
    free(__ptr_00);
    free(__ptr);
    return cStack85;
  }
  cStack85 = '\0';
  lVar2 = 0;
  ppcVar3 = ppcVar19;
  do {
    while ((ppcVar25 = ppcVar3, lVar22 = lVar2, pcVar1 = *ppcVar25, *pcVar1 != '-' ||
           (pcVar1[1] != '\0'))) {
      pFVar13 = fopen(pcVar1,"r");
      __ptr_00[lVar22] = pFVar13;
      if (pFVar13 == (FILE *)0x0) goto LAB_00102fa0;
      iVar5 = fileno(pFVar13);
      if (iVar5 == 0) {
        cStack85 = '\x01';
      }
      fadvise(pFVar13);
      lVar2 = lVar22 + 1;
      ppcVar3 = ppcVar25 + 1;
      if (lVar20 == lVar22 + 1) goto LAB_00102b95;
    }
    have_read_stdin = '\x01';
    __ptr_00[lVar22] = stdin;
    lVar2 = lVar22 + 1;
    ppcVar3 = ppcVar25 + 1;
  } while (lVar20 != lVar22 + 1);
LAB_00102b95:
  ppcVar25 = ppcVar25 + 1;
  uVar23 = lVar22 + 1;
  uVar24 = uVar23;
  if (cStack85 == '\0') {
    cStack85 = '\x01';
  }
  else if (have_read_stdin != '\0') {
    uVar9 = dcgettext(0,"standard input is closed",5);
    error(1,0,uVar9);
LAB_00102fa0:
    uVar9 = quotearg_n_style_colon(0,3,*ppcVar25);
    puVar10 = (uint *)__errno_location();
    lVar20 = 1;
    ppbVar18 = (byte **)(ulong)*puVar10;
    error(1,ppbVar18,"%s",uVar9);
    if (lVar20 == 0) {
      cStack169 = '\x01';
    }
    else {
      puVar10 = (uint *)__errno_location();
      cStack169 = '\x01';
      do {
        pbVar17 = *ppbVar18;
        uVar7 = *pbVar17 - 0x2d;
        if ((uVar7 != 0) || (uVar7 = (uint)pbVar17[1], pbVar17[1] != 0)) {
          pFVar13 = fopen((char *)pbVar17,"r");
          if (pFVar13 != (FILE *)0x0) {
            fadvise(pFVar13);
            goto LAB_00103035;
          }
          uVar9 = quotearg_n_style_colon(0,3,*ppbVar18);
          error(0,*puVar10,"%s",uVar9);
          cStack169 = '\0';
          goto LAB_0010312a;
        }
        have_read_stdin = '\x01';
        pFVar13 = stdin;
LAB_00103035:
        pbVar26 = delims;
        pbVar17 = (byte *)pFVar13->_IO_read_ptr;
        pbVar16 = (byte *)pFVar13->_IO_read_end;
        if (pbVar17 < pbVar16) {
          pbVar14 = pbVar17 + 1;
          pFVar13->_IO_read_ptr = (char *)pbVar14;
          uVar8 = (uint)*pbVar17;
        }
        else {
          uVar8 = __uflow(pFVar13);
          uVar6 = *puVar10;
          if (uVar8 == 0xffffffff) {
            uVar15 = (uint)line_delim;
            goto LAB_001030f1;
          }
          pbVar14 = (byte *)pFVar13->_IO_read_ptr;
          pbVar16 = (byte *)pFVar13->_IO_read_end;
        }
LAB_00103089:
        if (pbVar14 < pbVar16) {
          pFVar13->_IO_read_ptr = (char *)(pbVar14 + 1);
          uVar6 = (uint)*pbVar14;
LAB_0010309e:
          p_Var21 = stdout;
          if (line_delim == uVar8) {
            bVar4 = *pbVar26;
            if (bVar4 == 0) goto LAB_0010306d;
            pbVar17 = (byte *)stdout->_IO_write_ptr;
            if (stdout->_IO_write_end <= pbVar17) goto LAB_001032c0;
            pbVar26 = pbVar26 + 1;
            bVar29 = delim_end != pbVar26;
            stdout->_IO_write_ptr = (char *)(pbVar17 + 1);
            *pbVar17 = bVar4;
            if (bVar29) goto LAB_0010307e;
            goto LAB_00103180;
          }
          pcVar1 = stdout->_IO_write_ptr;
          if (pcVar1 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar1 + 1;
            *pcVar1 = (char)uVar8;
          }
          else {
            iVar5 = __overflow(stdout,uVar8 & 0xff);
            if (iVar5 < 0) goto LAB_001032b8;
          }
          goto LAB_0010307e;
        }
        uVar6 = __uflow(pFVar13);
        if (uVar6 != 0xffffffff) goto LAB_0010309e;
        uVar6 = *puVar10;
        pcVar1 = stdout->_IO_write_ptr;
        if (stdout->_IO_write_end <= pcVar1) {
          p_Var21 = stdout;
          iVar5 = __overflow(stdout,uVar8 & 0xff);
          if (-1 < iVar5) goto LAB_00103242;
LAB_001032b8:
          do {
            bVar4 = write_error();
LAB_001032c0:
            iVar5 = __overflow(p_Var21,(uint)bVar4);
          } while (iVar5 < 0);
LAB_0010306d:
          pbVar26 = pbVar26 + 1;
          if (delim_end == pbVar26) {
LAB_00103180:
            pbVar26 = delims;
          }
LAB_0010307e:
          pbVar14 = (byte *)pFVar13->_IO_read_ptr;
          pbVar16 = (byte *)pFVar13->_IO_read_end;
          uVar8 = uVar6;
          goto LAB_00103089;
        }
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar8;
LAB_00103242:
        uVar15 = (uint)line_delim;
        if (uVar15 == uVar8) goto LAB_00103110;
LAB_001030f1:
        pcVar1 = stdout->_IO_write_ptr;
        if (stdout->_IO_write_end <= pcVar1) {
          p_Var21 = stdout;
          iVar5 = __overflow(stdout,uVar15);
          if (-1 < iVar5) goto LAB_00103110;
          goto LAB_001032b8;
        }
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar15;
LAB_00103110:
        if ((*(byte *)&pFVar13->_flags & 0x20) == 0) {
          if (uVar7 == 0) {
            clearerr_unlocked(pFVar13);
          }
          else {
            iVar5 = rpl_fclose();
            if (iVar5 != 0) {
              uVar6 = *puVar10;
              goto LAB_001031bd;
            }
          }
        }
        else {
          if (uVar7 == 0) {
            clearerr_unlocked(pFVar13);
LAB_001031bd:
            if (uVar6 == 0) goto LAB_0010312a;
          }
          else {
            iVar5 = rpl_fclose();
            if (iVar5 == 0) goto LAB_001031bd;
            if (uVar6 == 0) {
              uVar6 = *puVar10;
              goto LAB_001031bd;
            }
          }
          uVar9 = quotearg_n_style_colon(0,3,*ppbVar18);
          error(0,uVar6,"%s",uVar9);
          cStack169 = '\0';
        }
LAB_0010312a:
        ppbVar18 = ppbVar18 + 1;
        lVar20 = lVar20 + -1;
      } while (lVar20 != 0);
    }
    return cStack169;
  }
LAB_00102bb0:
  uVar27 = 1;
  bVar29 = false;
  __n = 0;
  p_Var21 = *__ptr_00;
  pbVar17 = delims;
  if (p_Var21 == (_IO_FILE *)0x0) goto LAB_00102ce8;
LAB_00102be0:
  pbVar16 = (byte *)p_Var21->_IO_read_ptr;
  if (pbVar16 < p_Var21->_IO_read_end) {
    p_Var21->_IO_read_ptr = (char *)(pbVar16 + 1);
    uVar7 = (uint)*pbVar16;
    uVar6 = (uint)*pbVar16;
    if (__n == 0) goto LAB_00102c51;
  }
  else {
    uVar7 = __uflow(p_Var21);
    piVar12 = __errno_location();
    iStack92 = *piVar12;
    if (__n == 0 || uVar7 == 0xffffffff) {
      uVar6 = uVar7;
      if (uVar7 != 0xffffffff) goto LAB_00102c51;
      if ((*(byte *)&p_Var21->_flags & 0x20) == 0) {
        if (p_Var21 == stdin) {
          clearerr_unlocked(p_Var21);
          goto LAB_00102e6f;
        }
        iVar5 = rpl_fclose();
        if (iVar5 != -1) goto LAB_00102e6f;
LAB_00102f06:
        iStack92 = *piVar12;
LAB_00102e9a:
        if (iStack92 == 0) goto LAB_00102e6f;
      }
      else {
        if (p_Var21 == stdin) {
          clearerr_unlocked(p_Var21);
          goto LAB_00102e9a;
        }
        iVar5 = rpl_fclose();
        if (iVar5 != -1) goto LAB_00102e9a;
        if (iStack92 == 0) goto LAB_00102f06;
      }
      uVar9 = quotearg_n_style_colon(0,3,ppcVar19[uVar27 - 1]);
      error(0,iStack92,"%s",uVar9);
      cStack85 = '\0';
LAB_00102e6f:
      uVar24 = uVar24 - 1;
      __ptr_00[uVar27 - 1] = (_IO_FILE *)0x0;
      goto LAB_00102ce8;
    }
  }
  sVar11 = fwrite_unlocked(__ptr,1,__n,stdout);
  if (sVar11 != __n) goto LAB_00102c7f;
  __n = 0;
  uVar6 = uVar7;
LAB_00102c51:
  do {
    while( true ) {
      if (line_delim == uVar6) {
        if (uVar23 != uVar27) goto LAB_00102ca1;
        goto LAB_00102d29;
      }
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar6;
      }
      else {
        iVar5 = __overflow(stdout,uVar6 & 0xff);
        if (iVar5 < 0) goto LAB_00102c7f;
      }
      pbVar16 = (byte *)p_Var21->_IO_read_ptr;
      if (p_Var21->_IO_read_end <= pbVar16) break;
      p_Var21->_IO_read_ptr = (char *)(pbVar16 + 1);
      uVar6 = (uint)*pbVar16;
    }
    while (uVar6 = __uflow(p_Var21), uVar6 == 0xffffffff) {
      if (uVar23 != uVar27) {
LAB_00102ca1:
        bVar4 = *pbVar17;
        if (bVar4 != 0) {
          pbVar16 = (byte *)stdout->_IO_write_ptr;
          if (pbVar16 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = (char *)(pbVar16 + 1);
            *pbVar16 = bVar4;
          }
          else {
            iVar5 = __overflow(stdout,(uint)bVar4);
            if (iVar5 < 0) goto LAB_00102c7f;
          }
        }
        pbVar17 = pbVar17 + 1;
        bVar29 = true;
        uVar28 = uVar27;
        if (delim_end == pbVar17) goto LAB_00102d0d;
LAB_00102cbe:
        while( true ) {
          uVar27 = uVar28 + 1;
          if (uVar23 <= uVar28 || uVar24 == 0) goto LAB_00102dac;
          p_Var21 = __ptr_00[uVar28];
          if (p_Var21 != (_IO_FILE *)0x0) goto LAB_00102be0;
LAB_00102ce8:
          if (uVar23 == uVar27) break;
          if (*pbVar17 != 0) {
            *(byte *)((long)__ptr + __n) = *pbVar17;
            __n = __n + 1;
          }
          pbVar17 = pbVar17 + 1;
          uVar28 = uVar27;
          if (delim_end == pbVar17) {
LAB_00102d0d:
            pbVar17 = delims;
            uVar28 = uVar27;
          }
        }
        if (bVar29) {
          if ((__n != 0) && (sVar11 = fwrite_unlocked(__ptr,1,__n,stdout), sVar11 != __n))
          goto LAB_00102c7f;
          bVar4 = line_delim;
          pbVar16 = (byte *)stdout->_IO_write_ptr;
          if (pbVar16 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = (char *)(pbVar16 + 1);
            *pbVar16 = bVar4;
          }
          else {
            iVar5 = __overflow(stdout,(uint)line_delim);
            if (iVar5 < 0) goto LAB_00102c7f;
          }
        }
LAB_00102dac:
        if (uVar24 != 0) goto LAB_00102bb0;
        goto LAB_00102db5;
      }
      uVar6 = (uint)line_delim;
LAB_00102d29:
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar6;
LAB_00102d49:
        bVar29 = true;
        uVar28 = uVar27;
        goto LAB_00102cbe;
      }
      iVar5 = __overflow(stdout,uVar6 & 0xff);
      if (-1 < iVar5) goto LAB_00102d49;
LAB_00102c7f:
      write_error();
    }
  } while( true );
}