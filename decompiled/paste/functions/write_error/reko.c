byte write_error(struct Eq_708 & rdiOut)
{
	word64 * rsi;
	uint64 rdi;
	fn00000000000025A0(fn0000000000002400(0x05, "write error", null), *fn0000000000002370(), 0x01);
	struct Eq_708 * rdi_30;
	uint64 rax_31 = (uint64) paste_parallel(rsi, rdi, out rdi_30);
	rdiOut = rdi_30;
	return (byte) rax_31;
}