byte main(int param_1,stat *param_2)

{
  int iVar1;
  int iVar2;
  byte bVar3;
  char cVar4;
  int iVar5;
  int iVar6;
  long lVar7;
  undefined1 *puVar8;
  int *piVar9;
  tm *ptVar10;
  char *pcVar11;
  stat *__buf;
  ulong *__file;
  byte bVar12;
  uint *puVar13;
  uint uVar14;
  long in_FS_OFFSET;
  undefined8 uVar15;
  ulong *local_118;
  ulong local_108;
  long local_100;
  ulong local_f8;
  long local_f0;
  ulong local_e8;
  long local_e0;
  stat local_d8;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(param_2->st_dev);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar13 = &switchD_00103a2b::switchdataD_00112900;
  textdomain("coreutils");
  atexit(close_stdout);
  use_ref = '\0';
  change_times = 0;
  no_create = 0;
  local_118 = (ulong *)0x0;
  cVar4 = '\0';
switchD_00103a2b_caseD_66:
  uVar15 = 0x1039fb;
  __buf = param_2;
  iVar5 = getopt_long(param_1,param_2,"acd:fhmr:t:");
  if (iVar5 == -1) {
LAB_00103b9f:
    puVar8 = (undefined1 *)ref_file;
    if (change_times == 0) {
      change_times = 3;
    }
    if (cVar4 != '\0') {
      if ((local_118 == (ulong *)0x0) && (puVar8 = (undefined1 *)puVar13, use_ref == '\0'))
      goto LAB_00103bd7;
      pcVar11 = "cannot specify times from more than one source";
      puVar8 = (undefined1 *)puVar13;
      goto LAB_0010407f;
    }
    if (use_ref != '\0') {
      __buf = &local_d8;
      __file = ref_file;
      if (no_dereference != '\0') goto LAB_0010409e;
      iVar5 = stat((char *)ref_file,__buf);
      if (iVar5 != 0) goto LAB_001040ab;
      goto LAB_00103ff7;
    }
    puVar8 = (undefined1 *)puVar13;
    if (local_118 != (ulong *)0x0) {
      puVar8 = (undefined1 *)&local_108;
      gettime(puVar8);
      get_reldate(newtime,local_118,puVar8);
      newtime._16_8_ = newtime._0_8_;
      newtime._24_8_ = newtime._8_8_;
      if (change_times != 3) goto LAB_00103bd7;
      if (newtime._0_8_ != local_108) goto LAB_00103bd7;
      if (newtime._8_8_ != local_100) goto LAB_00103bd7;
      local_f8 = local_108 ^ 1;
      local_f0 = local_100;
      get_reldate(&local_e8,local_118,&local_f8);
      if (local_e8 != local_f8) goto LAB_00103bd7;
      if (local_e0 != local_f0) goto LAB_00103bd7;
    }
    if ((param_1 - optind < 2) || (iVar5 = posix2_version(), 0x30daf < iVar5)) {
LAB_00103fa6:
      if (change_times == 3) goto LAB_001040ea;
      newtime._8_8_ = 0x3fffffff;
      newtime._24_8_ = 0x3fffffff;
      goto LAB_00103bd7;
    }
    puVar8 = newtime;
    cVar4 = posixtime(newtime,param_2->__unused[(long)optind + -0xf],9);
    if (cVar4 == '\0') goto LAB_00103fa6;
    newtime._8_8_ = 0;
    newtime._16_8_ = newtime._0_8_;
    newtime._24_8_ = 0;
    pcVar11 = getenv("POSIXLY_CORRECT");
    if (pcVar11 != (char *)0x0) goto LAB_0010416f;
    goto LAB_001041b1;
  }
  if (iVar5 < 0x81) {
    if (0x60 < iVar5) goto code_r0x00103a24;
    if (iVar5 == -0x83) {
      version_etc(stdout,"touch","GNU coreutils",Version,"Paul Rubin","Arnold Robbins","Jim Kingdon"
                  ,"David MacKenzie","Randy Smith",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar5 == -0x82) {
      usage();
      goto LAB_00103b9f;
    }
  }
  goto switchD_00103a2b_caseD_62;
code_r0x00103a24:
  switch(iVar5) {
  case 0x61:
    change_times = change_times | 1;
    goto switchD_00103a2b_caseD_66;
  default:
    break;
  case 99:
    no_create = 1;
    goto switchD_00103a2b_caseD_66;
  case 100:
    local_118 = optarg;
  case 0x66:
    goto switchD_00103a2b_caseD_66;
  case 0x68:
    no_dereference = '\x01';
    goto switchD_00103a2b_caseD_66;
  case 0x6d:
    change_times = change_times | 2;
    goto switchD_00103a2b_caseD_66;
  case 0x72:
    ref_file = optarg;
    use_ref = '\x01';
    goto switchD_00103a2b_caseD_66;
  case 0x74:
    cVar4 = posixtime(newtime,optarg,6);
    if (cVar4 == '\0') {
      puVar8 = (undefined1 *)quote(optarg);
      uVar15 = dcgettext(0,"invalid date format %s",5);
      error(1,0,uVar15);
LAB_001041b1:
      ptVar10 = localtime((time_t *)puVar8);
      if (ptVar10 != (tm *)0x0) {
        iVar5 = ptVar10->tm_mon;
        iVar6 = ptVar10->tm_sec;
        iVar1 = ptVar10->tm_min;
        uVar14 = ptVar10->tm_hour;
        iVar2 = ptVar10->tm_mday;
        puVar8 = (undefined1 *)((long)ptVar10->tm_year + 0x76c);
        local_118 = (ulong *)((ulong)local_118 & 0xffffffff00000000 | (ulong)uVar14);
        lVar7 = param_2->__unused[(long)optind + -0xf];
        uVar15 = dcgettext(0,
                           "warning: \'touch %s\' is obsolete; use \'touch -t %04ld%02d%02d%02d%02d.%02d\'"
                           ,5);
        error(0,0,uVar15,lVar7,puVar8,iVar5 + 1,iVar2,(ulong)uVar14,iVar1,iVar6);
      }
LAB_0010416f:
      optind = optind + 1;
      goto LAB_00103bd7;
    }
    newtime._8_8_ = 0;
    newtime._16_8_ = newtime._0_8_;
    newtime._24_8_ = 0;
    goto switchD_00103a2b_caseD_66;
  case 0x80:
    lVar7 = __xargmatch_internal("--time",optarg,time_args,time_masks,4,argmatch_die,1,uVar15);
    change_times = change_times | *(uint *)(time_masks + lVar7 * 4);
    goto switchD_00103a2b_caseD_66;
  }
switchD_00103a2b_caseD_62:
  while( true ) {
    __file = (ulong *)0x1;
    usage();
    puVar8 = (undefined1 *)puVar13;
LAB_0010409e:
    iVar5 = lstat((char *)__file,__buf);
    if (iVar5 == 0) {
LAB_00103ff7:
      newtime._0_8_ = local_d8.st_atim.tv_sec;
      newtime._8_8_ = local_d8.st_atim.tv_nsec;
      newtime._16_8_ = local_d8.st_mtim.tv_sec;
      newtime._24_8_ = local_d8.st_mtim.tv_nsec;
      if (local_118 != (ulong *)0x0) {
        if ((change_times & 1) != 0) {
          get_reldate(newtime,local_118,newtime);
        }
        if ((change_times & 2) != 0) {
          get_reldate(0x119130,local_118,0x119130);
        }
      }
    }
    else {
LAB_001040ab:
      uVar15 = quotearg_style(4,puVar8);
      puVar8 = (undefined1 *)dcgettext(0,"failed to get attributes of %s",5);
      piVar9 = __errno_location();
      error(1,*piVar9,puVar8,uVar15);
LAB_001040ea:
      amtime_now = '\x01';
    }
LAB_00103bd7:
    if (optind != param_1) break;
    pcVar11 = "missing file operand";
LAB_0010407f:
    uVar15 = dcgettext(0,pcVar11,5);
    __buf = (stat *)0x0;
    error(0,0,uVar15);
    puVar13 = (uint *)puVar8;
  }
  bVar12 = 1;
  do {
    if (param_1 <= optind) {
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        return bVar12 ^ 1;
      }
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    pcVar11 = (char *)param_2->__unused[(long)optind + -0xf];
    if (*pcVar11 == '-') {
      uVar14 = (uint)(byte)pcVar11[1];
      iVar5 = 1;
      if (pcVar11[1] != 0) goto LAB_00103cbd;
    }
    else {
LAB_00103cbd:
      if ((no_create == 0) && (no_dereference == '\0')) {
        iVar5 = fd_reopen(0,pcVar11,0x941,0x1b6);
        uVar14 = 0;
        if (iVar5 < 0) {
          puVar13 = (uint *)__errno_location();
          uVar14 = *puVar13;
        }
      }
      else {
        iVar5 = -1;
        uVar14 = 0;
      }
    }
    if (change_times != 3) {
      if (change_times == 2) {
        newtime._8_8_ = 0x3ffffffe;
      }
      else {
        if (change_times != 1) {
                    /* WARNING: Subroutine does not return */
          __assert_fail("change_times == CH_ATIME","src/touch.c",0x93,"touch");
        }
        newtime._24_8_ = 0x3ffffffe;
      }
    }
    puVar8 = (undefined1 *)0x0;
    if (amtime_now == '\0') {
      puVar8 = newtime;
    }
    if (iVar5 == 1) {
      iVar5 = fdutimensat(1,0xffffff9c,0,puVar8);
      if (iVar5 == 0) {
LAB_00103c8e:
        bVar3 = 1;
      }
      else {
        piVar9 = __errno_location();
        iVar6 = *piVar9;
        if (iVar6 != 9) goto LAB_00103d06;
        if (no_create != 0) goto LAB_00103c8e;
        if ((uVar14 != 0) && (uVar14 != 0x15)) goto LAB_00103d1e;
LAB_00103e0b:
        quotearg_style(4,pcVar11);
        uVar15 = dcgettext(0,"setting times of %s",5);
        error(0,iVar6,uVar15);
        bVar3 = 0;
      }
    }
    else {
      iVar6 = fdutimensat(iVar5,0xffffff9c,pcVar11,puVar8);
      if (iVar6 == 0) {
        if ((iVar5 != 0) || (iVar5 = close(0), iVar5 == 0)) goto LAB_00103c8e;
      }
      else {
        piVar9 = __errno_location();
        iVar6 = *piVar9;
        if ((iVar5 != 0) || (iVar5 = close(0), iVar5 == 0)) {
LAB_00103d06:
          if (iVar6 == 0) goto LAB_00103c8e;
          if ((uVar14 != 0) && (uVar14 != 0x15)) {
LAB_00103d1e:
            if ((uVar14 != 0x16) ||
               ((iVar5 = stat(pcVar11,&local_d8), iVar5 != 0 ||
                ((local_d8.st_mode & 0xf000) != 0x4000)))) {
              quotearg_style(4,pcVar11);
              uVar15 = dcgettext(0,"cannot touch %s",5);
              error(0,uVar14,uVar15);
              bVar3 = 0;
              goto LAB_00103c93;
            }
          }
          bVar3 = iVar6 == 2 & no_create;
          if (bVar3 == 0) goto LAB_00103e0b;
          goto LAB_00103c93;
        }
      }
      quotearg_style(4,pcVar11);
      uVar15 = dcgettext(0,"failed to close %s",5);
      piVar9 = __errno_location();
      error(0,*piVar9,uVar15);
      bVar3 = 0;
    }
LAB_00103c93:
    bVar12 = bVar12 & bVar3;
    optind = optind + 1;
  } while( true );
}