void get_reldate(undefined8 param_1,undefined8 param_2)

{
  FILE *pFVar1;
  undefined **ppuVar2;
  char cVar3;
  int iVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  char *pcVar7;
  char *pcVar8;
  undefined *puVar9;
  int __status;
  long in_FS_OFFSET;
  undefined *puStack208;
  char *pcStack200;
  char *apcStack192 [5];
  char *pcStack152;
  char *pcStack144;
  char *pcStack136;
  char *pcStack128;
  char *pcStack120;
  undefined8 uStack112;
  undefined8 uStack104;
  undefined8 uStack88;
  
  cVar3 = parse_datetime();
  if (cVar3 != '\0') {
    return;
  }
  uVar5 = quote(param_2);
  uVar6 = dcgettext(0,"invalid date format %s",5);
  __status = 1;
  error(1,0,uVar6,uVar5);
  uVar5 = program_name;
  uStack88 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  if (__status != 0) {
    uVar6 = dcgettext(0,"Try \'%s --help\' for more information.\n",5);
    __fprintf_chk(stderr,1,uVar6,uVar5);
    goto LAB_0010441e;
  }
  uVar6 = dcgettext(0,"Usage: %s [OPTION]... FILE...\n",5);
  __printf_chk(1,uVar6,uVar5);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,
                             "Update the access and modification times of each FILE to the current time.\n\nA FILE argument that does not exist is created empty, unless -c or -h\nis supplied.\n\nA FILE argument string of - is handled specially and causes touch to\nchange the times of the file associated with standard output.\n"
                             ,5);
  fputs_unlocked(pcVar7,pFVar1);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,
                             "\nMandatory arguments to long options are mandatory for short options too.\n"
                             ,5);
  fputs_unlocked(pcVar7,pFVar1);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,
                             "  -a                     change only the access time\n  -c, --no-create        do not create any files\n  -d, --date=STRING      parse STRING and use it instead of current time\n  -f                     (ignored)\n"
                             ,5);
  fputs_unlocked(pcVar7,pFVar1);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,
                             "  -h, --no-dereference   affect each symbolic link instead of any referenced\n                         file (useful only on systems that can change the\n                         timestamps of a symlink)\n  -m                     change only the modification time\n"
                             ,5);
  fputs_unlocked(pcVar7,pFVar1);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,
                             "  -r, --reference=FILE   use this file\'s times instead of current time\n  -t STAMP               use [[CC]YY]MMDDhhmm[.ss] instead of current time\n      --time=WORD        change the specified time:\n                           WORD is access, atime, or use: equivalent to -a\n                           WORD is modify or mtime: equivalent to -m\n"
                             ,5);
  fputs_unlocked(pcVar7,pFVar1);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,"      --help        display this help and exit\n",5);
  fputs_unlocked(pcVar7,pFVar1);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,"      --version     output version information and exit\n",5);
  fputs_unlocked(pcVar7,pFVar1);
  pFVar1 = stdout;
  pcVar7 = (char *)dcgettext(0,
                             "\nNote that the -d and -t options accept different time-date formats.\n"
                             ,5);
  fputs_unlocked(pcVar7,pFVar1);
  uStack112 = 0;
  puStack208 = &DAT_00112021;
  pcStack200 = "test invocation";
  apcStack192[0] = "coreutils";
  apcStack192[1] = "Multi-call invocation";
  apcStack192[4] = "sha256sum";
  apcStack192[2] = "sha224sum";
  pcStack144 = "sha384sum";
  apcStack192[3] = "sha2 utilities";
  pcStack152 = "sha2 utilities";
  pcStack136 = "sha2 utilities";
  pcStack128 = "sha512sum";
  pcStack120 = "sha2 utilities";
  uStack104 = 0;
  ppuVar2 = &puStack208;
  do {
    puVar9 = (undefined *)ppuVar2;
    if (*(char **)(puVar9 + 0x10) == (char *)0x0) break;
    iVar4 = strcmp("touch",*(char **)(puVar9 + 0x10));
    ppuVar2 = (undefined **)(puVar9 + 0x10);
  } while (iVar4 != 0);
  pcVar7 = *(char **)(puVar9 + 0x18);
  if (pcVar7 == (char *)0x0) {
    uVar5 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar5,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar7 = setlocale(5,(char *)0x0);
    if (pcVar7 != (char *)0x0) {
      iVar4 = strncmp(pcVar7,"en_",3);
      if (iVar4 != 0) {
        pcVar7 = "touch";
        goto LAB_00104788;
      }
    }
    uVar5 = dcgettext(0,"Full documentation <%s%s>\n",5);
    pcVar7 = "touch";
    __printf_chk(1,uVar5,"https://www.gnu.org/software/coreutils/","touch");
    pcVar8 = " invocation";
  }
  else {
    uVar5 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar5,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar8 = setlocale(5,(char *)0x0);
    if (pcVar8 != (char *)0x0) {
      iVar4 = strncmp(pcVar8,"en_",3);
      if (iVar4 != 0) {
LAB_00104788:
        pFVar1 = stdout;
        pcVar8 = (char *)dcgettext(0,
                                   "Report any translation bugs to <https://translationproject.org/team/>\n"
                                   ,5);
        fputs_unlocked(pcVar8,pFVar1);
      }
    }
    uVar5 = dcgettext(0,"Full documentation <%s%s>\n",5);
    __printf_chk(1,uVar5,"https://www.gnu.org/software/coreutils/","touch");
    pcVar8 = " invocation";
    if (pcVar7 != "touch") {
      pcVar8 = "";
    }
  }
  uVar5 = dcgettext(0,"or available locally via: info \'(coreutils) %s%s\'\n",5);
  __printf_chk(1,uVar5,pcVar7,pcVar8);
LAB_0010441e:
                    /* WARNING: Subroutine does not return */
  exit(__status);
}