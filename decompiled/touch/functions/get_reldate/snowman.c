void get_reldate(void* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    signed char al7;

    al7 = parse_datetime();
    if (!al7) {
        quote(rsi, rsi, rdx, rcx);
        fun_35b0();
        fun_3800();
    } else {
        return;
    }
}