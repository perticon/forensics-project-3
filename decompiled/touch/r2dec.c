#include <stdint.h>

/* /tmp/tmpyosj1dz2 @ 0x4280 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpyosj1dz2 @ 0x9f50 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x00013ef5;
        rdx = 0x00013ee8;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x00012bb1;
        rdx = 0x00013eef;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x00013ef1;
    rdx = 0x00013eec;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xe050 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x37a0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpyosj1dz2 @ 0xa030 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x38e0)() ();
    }
    rdx = 0x00013f60;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x13f60 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x00013ef9;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x00013eef;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x00013f8c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x13f8c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x00012bb1;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x00013eef;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x00012bb1;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x00013eef;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0001408c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1408c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0001418c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1418c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x00013eef;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x00012bb1;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x00012bb1;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x00013eef;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpyosj1dz2 @ 0x38e0 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 58466 named .text */
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0xb450 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x38e5)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x38e5 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x38ea */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x3500 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpyosj1dz2 @ 0x38f0 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x38f5 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x38fa */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x38ff */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x3904 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x3909 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x390e */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x3913 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x3918 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x391d */
 
void vasnprintf_cold (void) {
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x4370 */
 
uint64_t get_reldate (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg2) {
    int64_t var_78h;
    rsi = arg2;
    al = parse_datetime (rdi, rsi, rdx);
    if (al != 0) {
        return al;
    }
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid date format %s");
    rcx = r12;
    eax = 0;
    rax = error (1, 0, rax);
}

/* /tmp/tmpyosj1dz2 @ 0x9a10 */
 
uint64_t dbg_parse_datetime (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool parse_datetime(timespec * result,char const * p,timespec const * now); */
    r15 = rdx;
    r14 = rsi;
    r13 = rdi;
    r12d = 0;
    rax = getenv (0x00012e3e);
    rdi = rax;
    rbx = rax;
    rax = tzalloc (rdi);
    if (rax != 0) {
        eax = parse_datetime_body (r13, r14, r15, 0, rax, rbx);
        rdi = rbp;
        r12d = eax;
        tzfree ();
    }
    eax = r12d;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x34b0 */
 
void getenv (void) {
    __asm ("bnd jmp qword [reloc.getenv]");
}

/* /tmp/tmpyosj1dz2 @ 0xc2c0 */
 
int64_t dbg_tzalloc (uint32_t arg1) {
    rdi = arg1;
    /* timezone_t tzalloc(char const * name); */
    if (rdi == 0) {
        goto label_0;
    }
    strlen (rdi);
    rbx = rax + 1;
    eax = 0x76;
    if (rbx >= rax) {
        rax = rbx;
    }
    rdi += 0x11;
    rdi &= 0xfffffffffffffff8;
    rax = malloc (rax);
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    *(rax) = 0;
    eax = 1;
    *((r12 + 8)) = ax;
    memcpy (r12 + 9, rbp, rbx);
    *((r12 + rbx + 9)) = 0;
    do {
label_1:
        rax = r12;
        return rax;
label_0:
        rax = malloc (0x80);
        r12 = rax;
    } while (rax == 0);
    edx = 0;
    *(r12) = 0;
    rax = r12;
    *((r12 + 8)) = dx;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x35d0 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpyosj1dz2 @ 0x3780 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpyosj1dz2 @ 0x3740 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpyosj1dz2 @ 0x7970 */
 
int64_t dbg_parse_datetime_body (int64_t arg1, int64_t arg3, uint32_t arg4, int64_t arg5, char * arg6, char ** s) {
    int64_t var_ch;
    int64_t var_76ch;
    timespec gettime_buffer;
    tm tm;
    tm tm0;
    tm gmt;
    tm lmt;
    parser_control pc;
    char[13] tm_year_buf;
    char[27] time_zone_buf;
    char[100] dbg_tm;
    char[100] tz1buf;
    char[100] tmp;
    int64_t var_1h;
    char * s1;
    uint32_t var_10h;
    size_t var_18h;
    int64_t var_20h;
    size_t var_28h;
    size_t var_30h;
    uint32_t var_38h;
    uint32_t var_40h;
    uint32_t var_48h;
    int64_t var_50h;
    int64_t var_54h;
    int64_t var_58h;
    int64_t var_5ch;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_74h;
    uint32_t var_78h;
    uint32_t var_7ch;
    uint32_t var_80h;
    uint32_t var_84h;
    size_t var_88h;
    int64_t var_8ch;
    uint32_t var_90h;
    int64_t var_98h;
    int64_t var_b0h;
    int64_t var_b4h;
    int64_t var_b8h;
    int64_t var_bch;
    int64_t var_c0h;
    int64_t var_c4h;
    int64_t var_d0h;
    int64_t var_f0h;
    int64_t var_f4h;
    int64_t var_f8h;
    int64_t var_fch;
    int64_t var_100h;
    int64_t var_104h;
    int64_t var_110h;
    int64_t var_120h;
    int64_t var_130h;
    int64_t var_170h;
    int64_t var_190h;
    int64_t var_198h;
    int64_t var_1a0h;
    int64_t var_1b0h;
    int64_t var_1b8h;
    int64_t var_1c0h;
    int64_t var_1c4h;
    int64_t var_1c8h;
    int64_t var_1cch;
    int64_t var_1d8h;
    uint32_t var_1e0h;
    int64_t var_1e8h;
    int64_t var_1f0h;
    int64_t var_1f8h;
    int64_t var_200h;
    int64_t var_208h;
    int64_t var_210h;
    int64_t var_218h;
    uint32_t var_220h;
    uint32_t var_228h;
    int64_t var_230h;
    int64_t var_238h;
    int64_t var_240h;
    int64_t var_248h;
    uint32_t var_250h;
    uint32_t var_251h;
    uint32_t var_258h;
    int32_t var_260h;
    int64_t var_268h;
    uint32_t var_270h;
    signed int64_t var_278h;
    uint32_t var_280h;
    uint32_t var_288h;
    int64_t var_290h;
    uint32_t var_291h;
    int64_t var_292h;
    int64_t var_294h;
    int64_t var_298h;
    int64_t var_2a0h;
    int64_t var_2a8h;
    uint32_t var_2ach;
    int64_t var_2b0h;
    int64_t var_2b8h;
    int64_t var_2bch;
    int64_t var_2c0h;
    int64_t var_2d3h;
    int64_t var_2e0h;
    int64_t var_300h;
    int64_t var_370h;
    int64_t var_3e0h;
    int64_t var_3e2h;
    int64_t var_3e3h;
    int64_t var_448h;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    rsi = s;
    /* _Bool parse_datetime_body(timespec * result,char const * p,timespec const * now,unsigned int flags,timezone_t tzdefault,char const * tzstring); */
    r13 = rdx;
    r12 = r8;
    *((rsp + 0x20)) = rdi;
    *((rsp + 0x10)) = ecx;
    *((rsp + 8)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x448)) = rax;
    eax = 0;
    rax = strlen (rsi);
    *((rsp + 0x28)) = rax;
    if (r13 == 0) {
        goto label_59;
    }
label_10:
    rax = *((r13 + 8));
    rbx = *(r13);
    r14 = rbp;
    *((rsp + 0x18)) = rax;
label_2:
    eax = *(r14);
    if (al > 0xd) {
        goto label_60;
    }
    if (al > 8) {
        goto label_61;
    }
label_1:
    eax = strncmp (r14, "TZ=\", 4);
    if (eax != 0) {
        goto label_62;
    }
    r15d = *((r14 + 4));
    r8 = r14 + 4;
    if (r15b == 0) {
        goto label_62;
    }
    rdx = r8;
    eax = r15d;
    edi = 1;
    while (al != 0x5c) {
        if (al == 0x22) {
            goto label_63;
        }
        rax = rdx;
label_0:
        rdx = rax + 1;
        eax = *((rax + 1));
        rdi++;
        if (al == 0) {
            goto label_62;
        }
    }
    rax = rdx + 1;
    edx = *((rdx + 1));
    if (dl == 0x5c) {
        goto label_0;
    }
    if (dl == 0x22) {
        goto label_0;
    }
label_62:
    rax = localtime_rz (r12, r13, rsp + 0xf0);
    if (rax == 0) {
        goto label_64;
    }
    *(rsp) = 0;
    r15 = r12;
label_4:
    rax = 0x00012c26;
    *((rsp + 0x1e0)) = 0;
    *((rsp + 0x1cc)) = 2;
    if (*(r14) == 0) {
        r14 = rax;
    }
    eax = *((rsp + 0x10));
    r11d = 0;
    *((rsp + 0x218)) = 0;
    r13d = 0;
    *((rsp + 0x291)) = al;
    rax = *((rsp + 0x104));
    *((rsp + 0x1b0)) = r14;
    r14 = *((rsp + 0x18));
    rax += 0x76c;
    *((rsp + 0x1d8)) = rax;
    eax = *((rsp + 0x100));
    *((rsp + 0x210)) = r14;
    r14 = rsp + 0x170;
    *((rsp + 0x220)) = 0;
    eax++;
    *((rsp + 0x228)) = 0;
    rax = (int64_t) eax;
    *((rsp + 0x1e8)) = rax;
    rax = *((rsp + 0xfc));
    *((rsp + 0x230)) = 0;
    *((rsp + 0x1f0)) = rax;
    rax = *((rsp + 0xf8));
    *((rsp + 0x238)) = 0;
    *((rsp + 0x1f8)) = rax;
    rax = *((rsp + 0xf4));
    *((rsp + 0x240)) = 0;
    *((rsp + 0x200)) = rax;
    rax = *((rsp + 0xf0));
    *((rsp + 0x248)) = 0;
    *((rsp + 0x208)) = rax;
    eax = *((rsp + 0x110));
    *((rsp + 0x250)) = r11w;
    *((rsp + 0x90)) = eax;
    *((rsp + 0x258)) = 0;
    rdx = *((rsp + 0x120));
    *((rsp + 0x260)) = 0;
    *((rsp + 0x280)) = 0;
    *((rsp + 0x268)) = 0;
    *((rsp + 0x270)) = 0;
    *((rsp + 0x278)) = 0;
    *((rsp + 0x288)) = 0;
    *((rsp + 0x290)) = 0;
    *((rsp + 0x294)) = 0;
    *((rsp + 0x298)) = 0;
    *((rsp + 0x2a0)) = rdx;
    *((rsp + 0x2a8)) = 0x10d;
    *((rsp + 0x2ac)) = eax;
    *((rsp + 0x2b0)) = 0;
    *((rsp + 0x292)) = r13w;
    r13d = 0x76a700;
    do {
        rax = (int64_t) r13d;
        rax += rbx;
        *((rsp + 0x130)) = rax;
        if (rax overflow 0) {
            goto label_26;
        }
        rax = localtime_rz (r15, rsp + 0x130, r14);
        if (rax != 0) {
            rax = *((rsp + 0x1a0));
            if (rax == 0) {
                goto label_65;
            }
            edx = *((rsp + 0x190));
            if (edx != *((rsp + 0x2ac))) {
                goto label_66;
            }
        }
label_65:
        r13d += 0x76a700;
    } while (r13d != 0x1da9c00);
label_26:
    rdi = *((rsp + 0x2a0));
    if (rdi != 0) {
        rsi = *((rsp + 0x2b0));
        if (rsi == 0) {
            goto label_6;
        }
        eax = strcmp (rdi, rsi);
        if (eax == 0) {
            goto label_67;
        }
    }
label_6:
    r14 = rsp + 0x1b0;
    eax = yyparse (r14, rsi, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_68;
    }
    if (*((rsp + 0x291)) != 0) {
        goto label_69;
    }
    r13d = *((rsp + 0x250));
    if (r13b != 0) {
        goto label_70;
    }
    rax = *((rsp + 0x280));
    rdx = *((rsp + 0x270));
    rax |= *((rsp + 0x258));
    rdx += *((rsp + 0x268));
    rax |= *((rsp + 0x260));
    rdx += *((rsp + 0x288));
    rax |= *((rsp + 0x278));
    rax |= rdx;
    if (rax > 1) {
        goto label_5;
    }
label_14:
    rbp = *((rsp + 0x1d8));
    if (rbp < 0) {
        goto label_71;
    }
    if (*((rsp + 0x1e0)) == 2) {
        goto label_72;
    }
label_24:
    edx = 0;
    rax = rbp - 0x76c;
    if (rbp >= 0x76c) {
        goto label_73;
    }
    if (rax >= 0) {
        goto label_74;
    }
label_21:
    rcx = (int64_t) eax;
    ecx = 1;
    if (rax != rcx) {
        edx = ecx;
    }
    ecx = edx;
    ecx &= 1;
    *((rsp + 0x10)) = cl;
label_25:
    *((rsp + 0x84)) = eax;
    if (*((rsp + 0x10)) != 0) {
        goto label_75;
    }
    rax = *((rsp + 0x1e8));
    edx = 0;
    esi = *((rsp + 0x291));
    rax += 0xffffffffffffffff;
    rcx = (int64_t) eax;
    __asm ("seto dl");
    *((rsp + 0x80)) = eax;
    ecx = 1;
    if (rax != rcx) {
        edx = ecx;
    }
    ecx = eax;
    if (edx != 0) {
        goto label_76;
    }
    rax = *((rsp + 0x1f0));
    rdi = (int64_t) eax;
    *((rsp + 0x7c)) = eax;
    edx = eax;
    if (rax != rdi) {
        goto label_76;
    }
    rax = *((rsp + 0x280));
    if (rax != 0) {
        goto label_77;
    }
    if (*((rsp + 0x251)) == 0) {
        goto label_78;
    }
    if (*((rsp + 0x258)) != 0) {
        goto label_78;
    }
    if (*((rsp + 0x260)) == 0) {
        goto label_77;
    }
    *((rsp + 0x70)) = 0;
    *((rsp + 0x78)) = 0;
    *((rsp + 0x210)) = 0;
    if (sil != 0) {
        goto label_79;
    }
    r9d = 0;
    r11d = 0;
    edi = 0;
label_29:
    *((rsp + 0x90)) = 0xffffffff;
label_28:
    if (*((rsp + 0x270)) != 0) {
        goto label_80;
    }
    eax = *((rsp + 0x90));
label_30:
    *((rsp + 0xbc)) = edx;
    r13 = rsp + 0x70;
    rbx = rsp + 0xb0;
    *((rsp + 0xb0)) = edi;
    *((rsp + 0xb4)) = r11d;
    *((rsp + 0xb8)) = r9d;
    *((rsp + 0xc0)) = ecx;
    *((rsp + 0xc4)) = edx;
    *((rsp + 0xd0)) = eax;
    *((rsp + 0x88)) = 0xffffffff;
    rax = mktime_z (r15, r13, *((rsp + 0x84)), rcx, r8, r9);
    rsi = r13;
    al = mktime_ok (rbx);
    if (al != 0) {
        goto label_81;
    }
    rax = *((rsp + 0x288));
    *((rsp + 0x48)) = rax;
    if (rax != 0) {
        goto label_82;
    }
label_27:
    eax = *((rsp + 0xb8));
    r9d = *((rsp + 0xb0));
    r8d = *((rsp + 0x70));
    ecx = *((rsp + 0xb4));
    *((rsp + 0x28)) = eax;
    eax = *((rsp + 0x78));
    r11d = *((rsp + 0x74));
    *((rsp + 0x30)) = eax;
    eax = *((rsp + 0xbc));
    *((rsp + 8)) = eax;
    eax = *((rsp + 0x7c));
    *((rsp + 0x10)) = eax;
    eax = *((rsp + 0xc0));
    *((rsp + 0x18)) = eax;
    eax = *((rsp + 0x80));
    *((rsp + 0x20)) = eax;
    eax = *((rsp + 0xc4));
    *((rsp + 0x38)) = eax;
    eax = *((rsp + 0x84));
    *((rsp + 0x40)) = eax;
    eax = *((rsp + 0x291));
    if (r9d != r8d) {
        goto label_83;
    }
    if (ecx != r11d) {
        goto label_83;
    }
    esi = *((rsp + 0x10));
    if (*((rsp + 8)) != esi) {
        goto label_84;
    }
    esi = *((rsp + 0x30));
    if (*((rsp + 0x28)) == esi) {
        goto label_84;
    }
    esi = *((rsp + 0x20));
    if (*((rsp + 0x18)) != esi) {
        goto label_84;
    }
    esi = *((rsp + 0x40));
    if (*((rsp + 0x38)) != esi) {
        goto label_84;
    }
    if (al == 0) {
        goto label_5;
    }
    edx = 5;
    rax = dcgettext (0, "error: invalid date/time value:\n");
    rbp = rsp + 0x3e0;
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    rdi = rbx;
    rdx = rbp;
    rsi = r14;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "    user provided time: '%s'\n");
    eax = 0;
    dbg_printf (rax, rbx, rdx, rcx, r8, r9);
    rdi = r13;
    rdx = rbp;
    rsi = r14;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "       normalized time: '%s'\n");
    r13d = 1;
    eax = 0;
    dbg_printf (rax, r13, rdx, rcx, r8, r9);
    rdx = 0x00012ce0;
    rsi = 0x00012bab;
    rax = rdx;
label_52:
    rcx = 0x00012ce0;
label_51:
    r11d = *((rsp + 0x20));
    rdi = 0x00012ce0;
    r8 = 0x00012bab;
    r11d = *((rsp + 0x40));
    if (*((rsp + 0x18)) == r11d) {
        r8 = rdi;
    }
    r9 = "----";
    if (*((rsp + 0x38)) == r11d) {
        r9 = rdi;
    }
    rdi = rbp;
    eax = 0;
    edx = 1;
    esi = 0x64;
    ecx = 0x64;
    r8 = "                                 %4s %2s %2s %2s %2s %2s";
    eax = snprintf_chk ();
    if (eax < 0) {
        goto label_85;
    }
    edx = 0x63;
    if (eax > edx) {
        eax = edx;
    }
    rax = (int64_t) eax;
    while (rax != 0) {
        rax--;
        if (*((rbp + rax)) != 0x20) {
            goto label_86;
        }
        rdx = (int64_t) eax;
    }
label_86:
    *((rsp + rdx + 0x3e0)) = 0;
label_85:
    eax = 0;
    dbg_printf (0x00012d42, rbp, 0, rcx, r8, r9);
    edx = 5;
    rax = dcgettext (0, "     possible reasons:\n");
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    if (r13d != 0) {
        goto label_87;
    }
label_56:
    ecx = *((rsp + 0x10));
    if (*((rsp + 8)) != ecx) {
        ecx = *((rsp + 0x20));
        if (*((rsp + 0x18)) == ecx) {
            goto label_88;
        }
        edx = 5;
        rax = dcgettext (0, "       invalid day/month combination;\n");
        eax = 0;
        dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    }
label_88:
    edx = 5;
    rax = dcgettext (0, "       numeric values overflow;\n");
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    edx = 5;
    if (*((rsp + 0x48)) == 0) {
        goto label_89;
    }
    rax = dcgettext (0, "incorrect timezone");
label_54:
    eax = 0;
    rax = dbg_printf ("       %s\n", rax, rdx, rcx, r8, r9);
label_5:
    r13d = 0;
label_3:
    if (r15 != r12) {
        rdi = r15;
        tzfree ();
    }
label_11:
    free (*(rsp));
    rax = *((rsp + 0x448));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_90;
    }
    eax = r13d;
    return rax;
label_60:
    if (al != 0x20) {
        goto label_1;
    }
label_61:
    r14++;
    goto label_2;
label_68:
    r13d = *((rsp + 0x291));
    if (r13b == 0) {
        goto label_3;
    }
    r13 = *((rsp + 0x1b0));
    rbp += *((rsp + 0x28));
    edx = 5;
    if (r13 < rbp) {
        goto label_91;
    }
    rax = dcgettext (0, "error: parsing failed\n");
label_15:
    eax = 0;
    r13d = 0;
    dbg_printf (rax, r13, rdx, rcx, r8, r9);
    goto label_3;
label_63:
    rax = rsp + 0x370;
    *(rsp) = 0;
    *((rsp + 8)) = rax;
    if (rdi > 0x64) {
        goto label_92;
    }
label_7:
    rdx = *((rsp + 8));
    if (r15b == 0x22) {
        goto label_93;
    }
    do {
        eax = 0;
        al = (r15b == 0x5c) ? 1 : 0;
        rdx++;
        rax += r8;
        ecx = *(rax);
        r15d = *((rax + 1));
        r8 = rax + 1;
        *((rdx - 1)) = cl;
    } while (r15b != 0x22);
label_93:
    *(rdx) = 0;
    *((rsp + 0x30)) = r8;
    rax = tzalloc (*((rsp + 8)));
    r15 = rax;
    if (rax == 0) {
        goto label_94;
    }
    r8 = *((rsp + 0x30));
    r14 = r8 + 1;
label_9:
    eax = *(r14);
    if (al > 0xd) {
        goto label_95;
    }
    if (al > 8) {
        goto label_96;
    }
label_8:
    rax = localtime_rz (r15, r13, rsp + 0xf0);
    if (rax != 0) {
        goto label_4;
    }
    goto label_5;
label_67:
    *((rsp + 0x2ac)) = 0xffffffff;
    *((rsp + 0x2b0)) = 0;
    goto label_6;
label_70:
    __asm ("movdqu xmm0, xmmword [rsp + 0x208]");
    rax = *((rsp + 0x20));
    __asm ("movups xmmword [rax], xmm0");
    goto label_3;
label_69:
    edx = 5;
    rax = dcgettext (0, "input timezone: ");
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    edx = 5;
    rsi = '@timespec' - always UTC";
    if (*((rsp + 0x250)) != 0) {
        goto label_22;
    }
    edx = 5;
    rsi = "parsed date/time string";
    if (*((rsp + 0x288)) != 0) {
        goto label_22;
    }
    rbx = *((rsp + 8));
    if (rbx == 0) {
        goto label_97;
    }
    if (r12 == r15) {
        goto label_98;
    }
    edx = 5;
    rax = dcgettext (0, "TZ=\"%s\" in date string");
    rdi = stderr;
    rcx = rbx;
    esi = 1;
    rdx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_23;
label_92:
    *((rsp + 8)) = r8;
    rax = malloc (rdi);
    r8 = *((rsp + 8));
    *(rsp) = rax;
    if (rax == 0) {
        goto label_64;
    }
    rax = *(rsp);
    *((rsp + 8)) = rax;
    goto label_7;
label_95:
    if (al != 0x20) {
        goto label_8;
    }
label_96:
    r14++;
    goto label_9;
label_59:
    r13 = rsp + 0x60;
    rdi = r13;
    rax = gettime ();
    goto label_10;
label_64:
    *(rsp) = 0;
    r13d = 0;
    goto label_11;
label_97:
    edx = 5;
label_22:
    rax = dcgettext (0, "system default");
    rdi = stderr;
    esi = 1;
    rdx = rax;
    eax = 0;
    fprintf_chk ();
label_23:
    rax = *((rsp + 0x288));
    if (*((rsp + 0x270)) == 0) {
        goto label_99;
    }
    if (rax == 0) {
        goto label_100;
    }
label_13:
    rax = time_zone_str (*((rsp + 0x1c8)), rsp + 0x2e0);
    rdi = stderr;
    esi = 1;
    rdx = " (%s)";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
label_12:
    fputc (0xa, *(obj.stderr));
    r13d = *((rsp + 0x291));
    if (*((rsp + 0x250)) == 0) {
        goto label_101;
    }
    __asm ("movdqu xmm1, xmmword [rsp + 0x208]");
    rax = *((rsp + 0x20));
    __asm ("movups xmmword [rax], xmm1");
label_32:
    if (r13b == 0) {
        goto label_102;
    }
    edx = 5;
    rsi = "timezone: system default\n";
    if (*((rsp + 8)) != 0) {
        eax = strcmp (*((rsp + 8)), "UTC0");
        edx = 5;
        if (eax != 0) {
            goto label_103;
        }
    }
    rax = dcgettext (0, "timezone: Universal Time\n");
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
label_16:
    rbx = *((rsp + 0x20));
    edx = 5;
    rbp = *(rbx);
    r14d = *((rbx + 8));
    rax = dcgettext (0, "final: %ld.%09d (epoch-seconds)\n");
    eax = 0;
    dbg_printf (rax, rbp, r14d, rcx, r8, r9);
    rbp = rsp + 0x130;
    rdi = rbx;
    rsi = rbp;
    rax = fcn_00003480 ();
    if (rax != 0) {
        rdi = rbp;
        rdx = rsp + 0x300;
        esi = 0;
        rax = debug_strfdatetime_constprop_0 ();
        edx = 5;
        rax = dcgettext (0, "final: %s (UTC)\n");
        eax = 0;
        dbg_printf (rax, rbp, rdx, rcx, r8, r9);
    }
    rbp = rsp + 0x170;
    rax = localtime_rz (r15, *((rsp + 0x20)), rbp);
    if (rax == 0) {
        goto label_3;
    }
    rax = time_zone_str (*((rsp + 0x198)), rsp + 0x2e0);
    rdx = rsp + 0x300;
    esi = 0;
    rdi = rbp;
    r14 = rax;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    rax = dcgettext (0, "final: %s (UTC%s)\n");
    eax = 0;
    dbg_printf (rax, rbp, r14, rcx, r8, r9);
    goto label_3;
label_100:
    r10d = *((rsp + 0x1c4));
    rsi = stderr;
    if (r10d <= 0) {
        goto label_12;
    }
    rax = fwrite (", dst", 1, 5, rsi);
label_99:
    if (*((rsp + 0x288)) != 0) {
        goto label_13;
    }
    rsi = stderr;
    goto label_12;
label_101:
    rsi = *((rsp + 0x280));
    rcx = *((rsp + 0x258));
    rdx = *((rsp + 0x270));
    rdx += *((rsp + 0x268));
    rax = rsi;
    rdx += *((rsp + 0x288));
    rax |= rcx;
    rax |= *((rsp + 0x260));
    rax |= *((rsp + 0x278));
    rax |= rdx;
    if (rax <= 1) {
        goto label_14;
    }
    if (r13b == 0) {
        goto label_5;
    }
    if (rsi > 1) {
        goto label_104;
    }
label_20:
    if (rcx > 1) {
        goto label_105;
    }
label_19:
    if (*((rsp + 0x260)) > 1) {
        goto label_106;
    }
label_18:
    if (*((rsp + 0x278)) > 1) {
        goto label_107;
    }
label_17:
    rax = *((rsp + 0x270));
    rax += *((rsp + 0x268));
    rax += *((rsp + 0x288));
    if (rax <= 1) {
        goto label_5;
    }
    eax = 0;
    dbg_printf ("error: seen multiple time-zone parts\n", rsi, rdx, rcx, r8, r9);
    goto label_5;
label_91:
    rax = dcgettext (0, "error: parsing failed, stopped at '%s'\n");
    rdi = rax;
    goto label_15;
label_103:
    rax = dcgettext (0, "timezone: TZ=\"%s\" environment value\n");
    eax = 0;
    eax = dbg_printf (rax, *((rsp + 8)), rdx, rcx, r8, r9);
    goto label_16;
label_94:
    r13d = 0;
    goto label_11;
label_107:
    eax = 0;
    eax = dbg_printf ("error: seen multiple daylight-saving parts\n", rsi, rdx, rcx, r8, r9);
    goto label_17;
label_106:
    eax = 0;
    eax = dbg_printf ("error: seen multiple days parts\n", rsi, rdx, rcx, r8, r9);
    goto label_18;
label_105:
    eax = 0;
    eax = dbg_printf ("error: seen multiple date parts\n", rsi, rdx, rcx, r8, r9);
    goto label_19;
label_104:
    eax = 0;
    rax = dbg_printf ("error: seen multiple time parts\n", rsi, rdx, rcx, r8, r9);
    rcx = *((rsp + 0x258));
    goto label_20;
label_73:
    if (rax >= 0) {
        goto label_21;
    }
label_74:
    edx = 1;
    goto label_21;
label_98:
    eax = strcmp (*((rsp + 8)), "UTC0");
    edx = 5;
    rsi = "TZ=\"UTC0\" environment value or -u";
    if (eax == 0) {
        goto label_22;
    }
    rax = dcgettext (0, "TZ=\"%s\" environment value");
    rcx = *((rsp + 8));
    esi = 1;
    rdi = stderr;
    rdx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_23;
label_72:
    ebx = 0x7d0;
    eax = 0x76c;
    if (rbp >= 0x45) {
        rbx = rax;
    }
    rbx += rbp;
    if (r13b != 0) {
        goto label_108;
    }
    goto label_24;
label_75:
    if (r13b != 0) {
        edx = 5;
        rax = dcgettext (0, "error: out-of-range year %ld\n");
        eax = 0;
        dbg_printf (rax, rbp, rdx, rcx, r8, r9);
    }
label_76:
    edx = 5;
    rsi = "error: year, month, or day overflow\n";
    if (*((rsp + 0x291)) == 0) {
        goto label_5;
    }
label_45:
    rax = dcgettext (0, rsi);
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    goto label_5;
label_71:
    rax = 0xfffffffffffff894;
    ecx = 0;
    rax -= rbp;
    rdx = (int64_t) eax;
    __asm ("seto cl");
    edx = 1;
    if (rax == rdx) {
        edx = ecx;
    }
    *((rsp + 0x10)) = edx;
    goto label_25;
label_66:
    *((rsp + 0x2b0)) = rax;
    *((rsp + 0x2b8)) = 0x10d;
    *((rsp + 0x2bc)) = edx;
    *((rsp + 0x2c0)) = 0;
    goto label_26;
label_82:
    edi = 0x5858;
    rbp = rsp + 0x3e0;
    *((rsp + 0x3e2)) = 0x58;
    *((rsp + 0x3e0)) = di;
    time_zone_str (*((rsp + 0x1c8)), rsp + 0x3e3);
    rax = tzalloc (rbp);
    r8 = rax;
    if (rax == 0) {
        goto label_109;
    }
    rax = *((rsp + 0xb0));
    rdi = r8;
    *((rsp + 0x28)) = r8;
    *((rsp + 0x88)) = 0xffffffff;
    *((rsp + 0x70)) = rax;
    rax = *((rsp + 0xb8));
    *((rsp + 0x78)) = rax;
    rax = *((rsp + 0xc0));
    *((rsp + 0x80)) = rax;
    eax = *((rsp + 0xd0));
    *((rsp + 0x90)) = eax;
    rax = mktime_z (rdi, r13, rdx, rcx, r8, r9);
    rsi = r13;
    al = mktime_ok (rbx);
    rdi = *((rsp + 0x28));
    *((rsp + 0x18)) = al;
    tzfree ();
    eax = *((rsp + 0x18));
    if (al == 0) {
        goto label_27;
    }
label_81:
    if (*((rsp + 0x260)) == 0) {
        goto label_110;
    }
    if (*((rsp + 0x258)) != 0) {
        goto label_111;
    }
    rcx = *((rsp + 0x1b8));
    *((rsp + 0x8c)) = 0xffffffff;
    if (rcx > 0) {
        eax = *((rsp + 0x1c0));
        al = (*((rsp + 0x88)) != eax) ? 1 : 0;
        eax = (int32_t) al;
        rcx -= rax;
    }
    rcx *= 7;
    if (rcx !overflow 0) {
        eax = *((rsp + 0x1c0));
        eax -= *((rsp + 0x88));
        eax += 7;
        rdx = (int64_t) eax;
        esi = eax;
        rdx *= 0xffffffff92492493;
        esi >>= 0x1f;
        rdx >>= 0x20;
        edx += eax;
        edx >>= 2;
        edx -= esi;
        esi = rdx*8;
        esi -= edx;
        eax -= esi;
        rax = (int64_t) eax;
        rax += rcx;
        if (rax overflow 0) {
            goto label_31;
        }
        rdx = *((rsp + 0x7c));
        rax += rdx;
        __asm ("seto dl");
        rcx = (int64_t) eax;
        *((rsp + 0x7c)) = eax;
        edx = (int32_t) dl;
        ecx = 1;
        if (rax != rcx) {
            edx = ecx;
        }
        if (edx == 0) {
            goto label_112;
        }
    }
label_31:
    if (*((rsp + 0x291)) == 0) {
        goto label_5;
    }
    rdx = rsp + 0x300;
    rsi = r14;
    rdi = r13;
    rax = debug_strfdatetime_constprop_0 ();
    r9 = *((rsp + 0x1b8));
    rdi = r14;
    rsi = rsp + 0x3e0;
    r13d = *((rsp + 0x1c0));
    rbx = rax;
    *((rsp + 8)) = r9;
    rax = str_days_constprop_0 ();
    edx = 5;
    rax = dcgettext (0, "error: day '%s' (day ordinal=%ld number=%d) resulted in an invalid date: '%s'\n");
    eax = 0;
    rax = dbg_printf (rax, rbp, *((rsp + 8)), r13d, rbx, r9);
    goto label_5;
label_102:
    r13d = 1;
    goto label_3;
label_77:
    r9d = *((rsp + 0x1cc));
    rbp = *((rsp + 0x1f8));
    if (r9d == 0) {
        goto label_113;
    }
    if (r9d == 1) {
        goto label_114;
    }
    if (rbp > 0x17) {
        goto label_115;
    }
label_39:
    r9d = ebp;
label_38:
    r8 = *((rsp + 0x200));
    r10 = *((rsp + 0x208));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x74)) = r8d;
    r11d = r8d;
    edi = r10d;
    *((rsp + 0x70)) = r10d;
    if (sil != 0) {
        goto label_116;
    }
label_33:
    rsi = *((rsp + 0x258));
    rsi |= *((rsp + 0x260));
    rsi |= rax;
    if (rsi == 0) {
        goto label_28;
    }
    goto label_29;
label_80:
    eax = *((rsp + 0x1c4));
    *((rsp + 0x90)) = eax;
    goto label_30;
label_112:
    *((rsp + 0x90)) = 0xffffffff;
    rax = mktime_z (r15, r13, rdx, rcx, r8, r9);
    esi = *((rsp + 0x8c));
    if (esi < 0) {
        goto label_31;
    }
    if (*((rsp + 0x291)) == 0) {
        goto label_41;
    }
    rbx = rsp + 0x300;
    rsi = r14;
    rdi = r13;
    rdx = rbx;
    rax = debug_strfdatetime_constprop_0 ();
    rsi = rsp + 0x3e0;
    rdi = r14;
    rbx = rax;
    rax = str_days_constprop_0 ();
    edx = 5;
    *((rsp + 0x18)) = rax;
    rax = dcgettext (0, "new start date: '%s' is '%s'\n");
    eax = 0;
    dbg_printf (rax, *((rsp + 0x18)), rbx, rcx, r8, r9);
label_110:
    if (*((rsp + 0x291)) == 0) {
        goto label_41;
    }
    rax = *((rsp + 0x260));
    if (*((rsp + 0x258)) != 0) {
        goto label_117;
    }
    rbx = rsp + 0x300;
    if (rax == 0) {
        goto label_118;
    }
label_42:
    rdx = rbx;
    rsi = r14;
    rdi = r13;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "starting date/time: '%s'\n");
    eax = 0;
    dbg_printf (rax, rbx, rdx, rcx, r8, r9);
    rcx = *((rsp + 0x218));
    rdx = *((rsp + 0x220));
    rsi = *((rsp + 0x228));
    eax = *((rsp + 0x291));
    rdi = rcx;
    rdi |= rdx;
    rbx = rdi;
    rbx |= rsi;
    if (rbx == 0) {
        goto label_119;
    }
    if (al == 0) {
        goto label_120;
    }
    if (rdi == 0) {
        goto label_121;
    }
    if (*((rsp + 0x7c)) != 0xf) {
        edx = 5;
        rax = dcgettext (0, "warning: when adding relative months/years, it is recommended to specify the 15th of the months\n");
        eax = 0;
        dbg_printf (rax, rsi, rdx, rcx, r8, r9);
        rsi = *((rsp + 0x228));
    }
    if (rsi != 0) {
        goto label_121;
    }
label_48:
    edx = 0;
    rax = *((rsp + 0x84));
    rax += *((rsp + 0x218));
    rcx = (int64_t) eax;
    __asm ("seto dl");
    if (rax != rcx) {
        goto label_122;
    }
    if (edx != 0) {
        goto label_122;
    }
    *((rsp + 0x18)) = eax;
    rdx = *((rsp + 0x220));
label_34:
    rax = *((rsp + 0x80));
    rax += rdx;
    rcx = rax;
    *((rsp + 0x28)) = rax;
    __asm ("seto al");
    rdx = (int64_t) ecx;
    eax = (int32_t) al;
    if (rcx != rdx) {
        goto label_122;
    }
    if (eax != 0) {
        goto label_122;
    }
    rax = *((rsp + 0x7c));
    rax += *((rsp + 0x228));
    rbx = rax;
    __asm ("seto al");
    rdx = (int64_t) ebx;
    eax = (int32_t) al;
    if (rbx != rdx) {
        goto label_122;
    }
    if (eax != 0) {
        goto label_122;
    }
    eax = *((rsp + 0x18));
    *((rsp + 0x30)) = ebx;
    *((rsp + 0x7c)) = ebx;
    *((rsp + 0x84)) = eax;
    eax = *((rsp + 0x28));
    *((rsp + 0x88)) = 0xffffffff;
    *((rsp + 0x80)) = eax;
    rax = *((rsp + 0xb0));
    *((rsp + 0x70)) = rax;
    eax = *((rsp + 0xb8));
    *((rsp + 0x78)) = eax;
    eax = *((rsp + 0xd0));
    *((rsp + 0x90)) = eax;
    rax = mktime_z (r15, r13, rdx, rcx, r8, r9);
    eax = *((rsp + 0x88));
    if (eax < 0) {
        goto label_123;
    }
    eax = *((rsp + 0x291));
    if (al != 0) {
        goto label_124;
    }
    if (*((rsp + 0x288)) == 0) {
        goto label_43;
    }
    edi = 0;
    rdx = *((rsp + 0x1c8));
    rcx = rbp;
    rdx -= *((rsp + 0x98));
    __asm ("seto dil");
    esi = 0;
    rcx -= rdx;
    __asm ("seto sil");
    rdx = rcx;
    rdi |= rsi;
    if (rdi != 0) {
        goto label_5;
    }
label_37:
label_36:
    if (al == 0) {
        goto label_43;
    }
    rsi = r14;
    rdi = r13;
    rdx = rsp + 0x300;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, '%s' = %ld epoch-seconds\n");
    eax = 0;
    dbg_printf (rax, r13, rbp, rcx, r8, r9);
    r14 = *((rsp + 0x230));
    r9 = *((rsp + 0x210));
    ebx = *((rsp + 0x248));
    r8 = r14 * 0xe10;
    if (al overflow 0) {
        goto label_125;
    }
    rcx = 0x112e0be826d694b3;
    rdi = (int64_t) ebx;
    r11 = 0x44b82fa09b5a53;
    r9 += rdi;
    rax = r9;
    r10 = r9;
    rdx:rax = rax * rcx;
    rax = rdx;
    rdx = r9;
    rdx >>= 0x3f;
    rax >>= 0x1a;
    rax -= rdx;
    rax *= 0x3b9aca00;
    r10 -= rax;
    r10 += 0x3b9aca00;
    rdx = r10;
    rdx >>= 9;
    rax = rdx;
    rdx:rax = rax * r11;
    rsi = rdx;
    rsi >>= 0xb;
    rax = rsi * 0x3b9aca00;
    rsi = r10;
    rsi -= rax;
    r9 -= rsi;
    rax = r9;
    r9 >>= 0x3f;
    rdx:rax = rax * rcx;
    ecx = *((rsp + 0x291));
    *((rsp + 0x10)) = cl;
    rcx = rbp;
    rax = rdx;
    rax >>= 0x1a;
    eax -= r9d;
    rcx += r8;
    if (rcx overflow 0) {
        goto label_126;
    }
    r13 = *((rsp + 0x238));
    rdx = r13 * 0x3c;
    if (rcx overflow 0) {
        goto label_126;
    }
label_35:
    rcx += rdx;
    if (rcx overflow 0) {
        goto label_126;
    }
    rcx = *((rsp + 0x240));
    rbp += rcx;
    if (rbp overflow 0) {
        goto label_126;
    }
    rax = (int64_t) eax;
    rbp += rax;
    if (rbp overflow 0) {
        goto label_126;
    }
    rax = *((rsp + 0x20));
    *(rax) = rbp;
    *((rax + 8)) = rsi;
    if (*((rsp + 0x10)) != 0) {
        rax = rcx;
        rax |= r13;
        rax |= r14;
        rax |= rdi;
        if (rax != 0) {
            goto label_127;
        }
    }
label_57:
    r13d = *((rsp + 0x291));
    goto label_32;
label_78:
    *((rsp + 0x70)) = 0;
    *((rsp + 0x78)) = 0;
    *((rsp + 0x210)) = 0;
    if (sil != 0) {
        goto label_79;
    }
    r9d = 0;
    r11d = 0;
    edi = 0;
    goto label_33;
label_41:
    rdx = *((rsp + 0x220));
    rcx = *((rsp + 0x218));
    rax = rdx;
    rax |= rcx;
    rax |= *((rsp + 0x228));
    if (rax == 0) {
        goto label_128;
    }
label_120:
    rax = *((rsp + 0x84));
    rax += rcx;
    __asm ("seto cl");
    rsi = (int64_t) eax;
    ecx = (int32_t) cl;
    if (rax != rsi) {
        goto label_5;
    }
    if (ecx != 0) {
        goto label_5;
    }
    *((rsp + 0x18)) = eax;
    goto label_34;
label_43:
    r14 = *((rsp + 0x230));
    r9 = *((rsp + 0x210));
    ebx = *((rsp + 0x248));
    r8 = r14 * 0xe10;
    if (ecx overflow 0) {
        goto label_5;
    }
    rcx = 0x112e0be826d694b3;
    rdi = (int64_t) ebx;
    r11 = 0x44b82fa09b5a53;
    r9 += rdi;
    rax = r9;
    r10 = r9;
    rdx:rax = rax * rcx;
    rax = rdx;
    rdx = r9;
    rdx >>= 0x3f;
    rax >>= 0x1a;
    rax -= rdx;
    rax *= 0x3b9aca00;
    r10 -= rax;
    r10 += 0x3b9aca00;
    rdx = r10;
    rdx >>= 9;
    rax = rdx;
    rdx:rax = rax * r11;
    rsi = rdx;
    rsi >>= 0xb;
    rax = rsi * 0x3b9aca00;
    rsi = r10;
    rsi -= rax;
    r9 -= rsi;
    rax = r9;
    r9 >>= 0x3f;
    rdx:rax = rax * rcx;
    rcx = rbp;
    rax = rdx;
    rax >>= 0x1a;
    eax -= r9d;
    rcx += r8;
    if (rcx overflow 0) {
        goto label_5;
    }
    r13 = *((rsp + 0x238));
    rdx = r13 * 0x3c;
    if (rcx !overflow 0) {
        goto label_35;
    }
    goto label_5;
label_122:
    if (*((rsp + 0x291)) == 0) {
        goto label_5;
    }
    edx = 5;
    rax = dcgettext (0, "error: %s:%d\n");
    eax = 0;
    dbg_printf (rax, "parse-datetime.y", 0x863, rcx, r8, r9);
    goto label_5;
label_108:
    edx = 5;
    rax = dcgettext (0, "warning: adjusting year value %ld to %ld\n");
    rdx = rbx;
    eax = 0;
    dbg_printf (rax, rbp, rdx, rcx, r8, r9);
    goto label_24;
label_124:
    r9 = *((rsp + 0x220));
    r8 = *((rsp + 0x218));
    edx = 5;
    rcx = *((rsp + 0x228));
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r8;
    *((rsp + 0x48)) = rcx;
    rax = dcgettext (0, "after date adjustment (%+ld years, %+ld months, %+ld days),\n");
    eax = 0;
    dbg_printf (rax, *((rsp + 0x38)), *((rsp + 0x40)), *((rsp + 0x48)), r8, r9);
    rdx = rsp + 0x300;
    rsi = r14;
    rdi = r13;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    *((rsp + 0x38)) = rax;
    rax = dcgettext (0, "    new date/time = '%s'\n");
    eax = 0;
    dbg_printf (rax, *((rsp + 0x38)), rdx, rcx, r8, r9);
    eax = *((rsp + 0xd0));
    if (eax != 0xffffffff) {
        if (eax == *((rsp + 0x90))) {
            goto label_129;
        }
        edx = 5;
        rax = dcgettext (0, "warning: daylight saving time changed after date adjustment\n");
        eax = 0;
        dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    }
label_129:
    if (*((rsp + 0x228)) == 0) {
        if (ebx == *((rsp + 0x7c))) {
            if (*((rsp + 0x220)) != 0) {
                goto label_130;
            }
            eax = *((rsp + 0x28));
            if (eax == *((rsp + 0x80))) {
                goto label_130;
            }
        }
        edx = 5;
        rax = dcgettext (0, "warning: month/year adjustment resulted in shifted dates:\n");
        eax = 0;
        dbg_printf (rax, rsi, rdx, rcx, r8, r9);
        r8 = rsp + 0x2d3;
        edi = *((rsp + 0x18));
        rsi = r8;
        *((rsp + 0x38)) = r8;
        rax = tm_year_str ();
        edx = 5;
        rbx = rax;
        rax = dcgettext (0, "     adjusted Y M D: %s %02d %02d\n");
        eax = 0;
        edx++;
        dbg_printf (rax, rbx, *((rsp + 0x28)), *((rsp + 0x30)), r8, r9);
        ecx = *((rsp + 0x7c));
        eax = *((rsp + 0x80));
        rsi = *((rsp + 0x38));
        edi = *((rsp + 0x84));
        *((rsp + 0x28)) = ecx;
        ebx = rax + 1;
        rax = tm_year_str ();
        edx = 5;
        *((rsp + 0x18)) = rax;
        rax = dcgettext (0, "   normalized Y M D: %s %02d %02d\n");
        eax = 0;
        dbg_printf (rax, *((rsp + 0x18)), ebx, *((rsp + 0x28)), r8, r9);
    }
label_130:
    eax = *((rsp + 0x291));
label_119:
    if (*((rsp + 0x288)) == 0) {
        goto label_36;
    }
label_44:
    rdx = *((rsp + 0x1c8));
    edi = 0;
    rcx = rbp;
    rbx = rdx;
    rdx -= *((rsp + 0x98));
    __asm ("seto dil");
    esi = 0;
    rcx -= rdx;
    __asm ("seto sil");
    rdx = rcx;
    rdi |= rsi;
    if (rdi == 0) {
        goto label_37;
    }
    if (al == 0) {
        goto label_5;
    }
    edx = 5;
    rax = dcgettext (0, "error: timezone %d caused time_t overflow\n");
    eax = 0;
    dbg_printf (rax, ebx, rdx, rcx, r8, r9);
    goto label_5;
label_114:
    rdi = rbp - 1;
    r9d = rbp + 0xc;
    if (rdi <= 0xa) {
        goto label_38;
    }
    r9d = 0xc;
    r13 = 0x00012abb;
    if (rbp == 0xc) {
        goto label_38;
    }
label_40:
    *((rsp + 0x78)) = 0xffffffff;
    if (sil == 0) {
        goto label_5;
    }
    edx = 5;
    rax = dcgettext (0, "error: invalid hour %ld%s\n");
    eax = 0;
    dbg_printf (rax, rbp, r13, rcx, r8, r9);
    goto label_5;
label_113:
    rdi = rbp - 1;
    if (rdi <= 0xa) {
        goto label_39;
    }
    r13 = 0x00012ba6;
    if (rbp == 0xc) {
        goto label_38;
    }
    goto label_40;
label_111:
    if (*((rsp + 0x291)) == 0) {
        goto label_41;
    }
label_46:
    rbx = rsp + 0x300;
label_49:
    rsi = rsp + 0x3e0;
    rdi = r14;
    rax = str_days_constprop_0 ();
    edx = 5;
    *((rsp + 0x18)) = rax;
    rax = dcgettext (0, "warning: day (%s) ignored when explicit dates are given\n");
    eax = 0;
    eax = dbg_printf (rax, *((rsp + 0x18)), rdx, rcx, r8, r9);
    goto label_42;
label_128:
    if (*((rsp + 0x288)) == 0) {
        goto label_43;
    }
    eax = 0;
    goto label_44;
label_125:
    eax = *((rsp + 0x291));
    *((rsp + 0x10)) = al;
label_126:
    if (*((rsp + 0x10)) == 0) {
        goto label_5;
    }
    edx = 5;
    rsi = "error: adding relative time caused an overflow\n";
    goto label_45;
label_79:
    eax = 0;
    dbg_printf ("warning: using midnight as starting time: 00:00:00\n", rsi, rdx, rcx, r8, r9);
label_47:
    rax = *((rsp + 0x280));
    edi = *((rsp + 0x70));
    r11d = *((rsp + 0x74));
    r9d = *((rsp + 0x78));
    edx = *((rsp + 0x7c));
    ecx = *((rsp + 0x80));
    goto label_33;
label_117:
    if (rax != 0) {
        goto label_46;
    }
    rbx = rsp + 0x300;
    goto label_42;
label_116:
    rbx = rsp + 0x300;
    edx = 1;
    eax = 0;
    ecx = 0x64;
    esi = 0x64;
    rdi = rbx;
    r8 = "%02d:%02d:%02d";
    snprintf_chk ();
    edx = 5;
    if (*((rsp + 0x280)) == 0) {
        goto label_131;
    }
    rax = dcgettext (0, "using specified time as starting value: '%s'\n");
label_50:
    eax = 0;
    dbg_printf (rax, rbx, rdx, rcx, r8, r9);
    goto label_47;
label_121:
    if (*((rsp + 0x78)) == 0xc) {
        goto label_48;
    }
    edx = 5;
    rax = dcgettext (0, "warning: when adding relative days, it is recommended to specify noon\n");
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    goto label_48;
label_118:
    edx = *((rsp + 0x84));
    eax = *((rsp + 0x80));
    rdi = rsp + 0x2d3;
    r11d = *((rsp + 0x7c));
    *((rsp + 0x18)) = rdi;
    r10d = rax + 1;
    rax = (int64_t) edx;
    ecx = edx;
    esi = edx;
    rax *= 0x51eb851f;
    ecx >>= 0x1f;
    *((rsp + 0x30)) = r11d;
    *((rsp + 0x28)) = r10d;
    rax >>= 0x25;
    eax -= ecx;
    ecx = eax * 0x64;
    eax += 0x13;
    r8d = eax;
    esi -= ecx;
    ecx = 0;
    cl = (edx >= 0xfffff894) ? 1 : 0;
    rdx = "-%02d%02d";
    r9d = esi;
    rcx += rdx;
    r9d = -r9d;
    edx = 0xd;
    __asm ("cmovs r9d, esi");
    r8d = -r8d;
    esi = 1;
    __asm ("cmovs r8d, eax");
    eax = 0;
    eax = sprintf_chk ();
    r11d = *((rsp + 0x30));
    rdi = rbx;
    eax = 0;
    ecx = 0x64;
    edx = 1;
    esi = 0x64;
    r10d = *((rsp + 0x30));
    r8 = "(Y-M-D) %s-%02d-%02d";
    r9 = *((rsp + 0x28));
    snprintf_chk ();
    edx = 5;
    rax = dcgettext (0, "using current date as starting value: '%s'\n");
    eax = 0;
    dbg_printf (rax, rbx, rdx, rcx, r8, r9);
    if (*((rsp + 0x260)) == 0) {
        goto label_42;
    }
    if (*((rsp + 0x258)) != 0) {
        goto label_49;
    }
    goto label_42;
label_131:
    rax = dcgettext (0, "using current time as starting value: '%s'\n");
    rdi = rax;
    goto label_50;
label_123:
    if (*((rsp + 0x291)) == 0) {
        goto label_5;
    }
    rdx = rsp + 0x300;
    rsi = r14;
    rdi = r13;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
label_55:
    rax = dcgettext (0, "error: adding relative date resulted in an invalid date: '%s'\n");
    eax = 0;
    al = dbg_printf (rax, rbp, rdx, rcx, r8, r9);
    goto label_5;
label_83:
    *((rsp + 0x5c)) = r11d;
    *((rsp + 0x58)) = ecx;
    *((rsp + 0x54)) = r8d;
    *((rsp + 0x50)) = r9d;
    if (al == 0) {
        goto label_5;
    }
    edx = 5;
    rax = dcgettext (0, "error: invalid date/time value:\n");
    rbp = rsp + 0x3e0;
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    rdi = rbx;
    rdx = rbp;
    rsi = r14;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "    user provided time: '%s'\n");
    eax = 0;
    dbg_printf (rax, rbx, rdx, rcx, r8, r9);
    rdi = r13;
    rdx = rbp;
    rsi = r14;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "       normalized time: '%s'\n");
    eax = 0;
    dbg_printf (rax, r13, rdx, rcx, r8, r9);
    r9d = *((rsp + 0x50));
    r8d = *((rsp + 0x54));
    if (r9d == r8d) {
        goto label_132;
    }
    ecx = *((rsp + 0x58));
    r11d = *((rsp + 0x5c));
    rax = 0x00012bab;
    if (ecx == r11d) {
        goto label_133;
    }
label_53:
    rdx = rax;
label_58:
    esi = *((rsp + 0x30));
    rcx = 0x00012bab;
    rsi = 0x00012ce0;
    if (*((rsp + 0x28)) != esi) {
        rsi = rcx;
    }
    edi = *((rsp + 0x10));
    r13d = 0;
    if (*((rsp + 8)) != edi) {
        goto label_51;
    }
    goto label_52;
label_84:
    if (al == 0) {
        goto label_5;
    }
    edx = 5;
    rax = dcgettext (0, "error: invalid date/time value:\n");
    rbp = rsp + 0x3e0;
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    rdi = rbx;
    rdx = rbp;
    rsi = r14;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "    user provided time: '%s'\n");
    eax = 0;
    dbg_printf (rax, rbx, rdx, rcx, r8, r9);
    rdi = r13;
    rdx = rbp;
    rsi = r14;
    rax = debug_strfdatetime_constprop_0 ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "       normalized time: '%s'\n");
    eax = 0;
    dbg_printf (rax, r13, rdx, rcx, r8, r9);
    rax = 0x00012ce0;
    goto label_53;
label_89:
    rax = dcgettext (0, "missing timezone");
    rsi = rax;
    goto label_54;
label_109:
    edx = 5;
    rsi = "error: tzalloc (\"%s\") failed\n";
    if (*((rsp + 0x291)) == 0) {
        goto label_5;
    }
    goto label_55;
label_115:
    r13 = 0x00012ce0;
    goto label_40;
label_87:
    edx = 5;
    rax = dcgettext (0, "       non-existing due to daylight-saving time;\n");
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    goto label_56;
label_90:
    stack_chk_fail ();
label_127:
    edx = 5;
    *((rsp + 0x10)) = rcx;
    rax = dcgettext (0, "after time adjustment (%+ld hours, %+ld minutes, %+ld seconds, %+d ns),\n");
    eax = 0;
    dbg_printf (rax, r14, r13, *((rsp + 0x10)), ebx, r9);
    edx = 5;
    rax = dcgettext (0, "    new time = %ld epoch-seconds\n");
    eax = 0;
    dbg_printf (rax, rbp, rdx, rcx, r8, r9);
    if (*((rsp + 0x90)) == 0xffffffff) {
        goto label_57;
    }
    rax = localtime_rz (r15, *((rsp + 0x20)), rsp + 0x170);
    if (rax == 0) {
        goto label_57;
    }
    eax = *((rsp + 0x190));
    if (*((rsp + 0x90)) == eax) {
        goto label_57;
    }
    edx = 5;
    rax = dcgettext (0, "warning: daylight saving time changed after time adjustment\n");
    eax = 0;
    dbg_printf (rax, rsi, rdx, rcx, r8, r9);
    goto label_57;
label_133:
    rdx = 0x00012ce0;
    goto label_58;
label_132:
    rax = 0x00012ce0;
    rdx = 0x00012bab;
    goto label_58;
}

/* /tmp/tmpyosj1dz2 @ 0xc590 */
 
void tzfree (size_t arg1) {
    rdi = arg1;
    if (rdi == 1) {
        goto label_0;
    }
    rbx = rdi;
    if (rdi == 0) {
        goto label_1;
    }
    do {
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
label_1:
    return;
label_0:
}

/* /tmp/tmpyosj1dz2 @ 0xc210 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0x35b0 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpyosj1dz2 @ 0x3800 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpyosj1dz2 @ 0x3880 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpyosj1dz2 @ 0x3860 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpyosj1dz2 @ 0x37e0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpyosj1dz2 @ 0x36e0 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpyosj1dz2 @ 0x3700 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpyosj1dz2 @ 0x37d0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpyosj1dz2 @ 0x3520 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpyosj1dz2 @ 0x42b0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x42e0 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x4320 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00003490 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpyosj1dz2 @ 0x3490 */
 
void fcn_00003490 (void) {
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpyosj1dz2 @ 0x4360 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpyosj1dz2 @ 0x47c0 */
 
void dbg_argmatch_die (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h) {
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x43c0)() ();
}

/* /tmp/tmpyosj1dz2 @ 0x4e80 */
 
int64_t dbg_digits_to_date_time (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg1, int64_t arg3, int64_t arg7) {
    rdi = arg1;
    rdx = arg3;
    xmm0 = arg7;
    /* void digits_to_date_time(parser_control * pc,textint text_int); */
    rax = *((rdi + 0xa8));
    r8 = *((rsp + 0x10));
    rcx = rdi;
    r9 = *((rsp + 0x18));
    if (rax != 0) {
        if (*((rdi + 0x30)) != 0) {
            goto label_0;
        }
        if (*((rdi + 0xa1)) != 0) {
            goto label_0;
        }
        if (*((rdi + 0xd0)) == 0) {
            if (r9 <= 2) {
                goto label_1;
            }
        }
        __asm ("movdqu xmm0, xmmword [rsp + 8]");
        rax = *((rsp + 0x18));
        *((rcx + 0xe0)) = 1;
        *((rcx + 0x30)) = rax;
        __asm ("movups xmmword [rcx + 0x20], xmm0");
        return rax;
    }
label_0:
    if (r9 > 4) {
        rax++;
        r10 = r8;
        r9 -= 4;
        rdi = 0xa3d70a3d70a3d70b;
        *((rcx + 0xa8)) = rax;
        rax = r8;
        r10 >>= 0x3f;
        rdx:rax = rax * rdi;
        *((rcx + 0x30)) = r9;
        rsi = rdx + r8;
        rdx = r8;
        rsi >>= 6;
        rsi -= r10;
        rax = rsi * 5;
        rax *= 5;
        rax <<= 2;
        rdx -= rax;
        rax = rsi;
        *((rcx + 0x40)) = rdx;
        rdx:rax = rax * rdi;
        rax = rsi;
        rax >>= 0x3f;
        rdi = rdx + rsi;
        rdx = 0x346dc5d63886594b;
        rdi >>= 6;
        rdi -= rax;
        rax = rdi * 5;
        rax *= 5;
        rax <<= 2;
        rsi -= rax;
        rax = r8;
        rdx:rax = rax * rdx;
        *((rcx + 0x38)) = rsi;
        rdx >>= 0xb;
        rdx -= r10;
        *((rcx + 0x28)) = rdx;
        return rax;
    }
    *((rcx + 0xd0))++;
    if (r9 <= 2) {
        goto label_2;
    }
    rdx = 0xa3d70a3d70a3d70b;
    rax = r8;
    rdx:rax = rax * rdx;
    rax = r8;
    rax >>= 0x3f;
    rdx += r8;
    rdx >>= 6;
    rdx -= rax;
    rax = rdx * 5;
    rsi = rax * 5;
    rax = r8;
    r8 = rdx;
    rsi <<= 2;
    rax -= rsi;
    do {
        *((rcx + 0x48)) = r8;
        *((rcx + 0x50)) = rax;
        *((rcx + 0x58)) = 0;
        *((rcx + 0x60)) = 0;
        *((rcx + 0x1c)) = 2;
        return rax;
label_1:
        *((rdi + 0xd0)) = 1;
label_2:
        eax = 0;
    } while (1);
}

/* /tmp/tmpyosj1dz2 @ 0x5000 */
 
int32_t dbg_mktime_ok (int64_t arg2, tm const * tm0) {
    rsi = arg2;
    rdi = tm0;
    /* _Bool mktime_ok(tm const * tm0,tm const * tm1); */
    rdx = rsi;
    esi = *((rsi + 0x18));
    eax = 0;
    if (esi >= 0) {
        eax = *(rdi);
        esi = *((rdi + 4));
        eax ^= *(rdx);
        esi ^= *((rdx + 4));
        eax |= esi;
        esi = *((rdi + 8));
        esi ^= *((rdx + 8));
        eax |= esi;
        esi = *((rdi + 0xc));
        esi ^= *((rdx + 0xc));
        eax |= esi;
        esi = *((rdi + 0x10));
        esi ^= *((rdx + 0x10));
        eax |= esi;
        ecx = *((rdi + 0x14));
        ecx ^= *((rdx + 0x14));
        eax |= ecx;
        al = (eax == 0) ? 1 : 0;
    }
    return eax;
}

/* /tmp/tmpyosj1dz2 @ 0x5040 */
 
uint64_t dbg_time_zone_str (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* char const * time_zone_str(int time_zone,char * time_zone_buf); */
    r12 = rsi;
    rcx = "%c%02d";
    esi = 1;
    rbx = (int64_t) edi;
    rdi = r12;
    eax = ebx;
    rbx *= 0xffffffff91a2b3c5;
    edx = ebp;
    eax >>= 0x1f;
    edx >>= 0x1f;
    eax &= 2;
    r8d = rax + 0x2b;
    rbx >>= 0x20;
    ebx += ebp;
    ebx >>= 0xb;
    ebx -= edx;
    rdx = 0xffffffffffffffff;
    r9d = ebx;
    r9d = -r9d;
    __asm ("cmovs r9d, ebx");
    ebx *= 0xe10;
    eax = 0;
    rax = sprintf_chk ();
    ebp -= ebx;
    if (ebp == 0) {
        goto label_0;
    }
    rax = (int64_t) eax;
    ecx = ebp;
    esi = 0x88888889;
    r8d = 0xcccccccd;
    rax += r12;
    ecx = -ecx;
    __asm ("cmovs ecx, ebp");
    *(rax) = 0x3a;
    edx = ecx;
    rdx *= rsi;
    rdx >>= 0x25;
    esi = edx;
    edi = edx * 0x3c;
    rsi *= r8;
    rsi >>= 0x23;
    r9d = rsi + 0x30;
    esi = rsi * 5;
    esi += esi;
    *((rax + 1)) = r9b;
    edx -= esi;
    rsi = rax + 3;
    edx += 0x30;
    ecx -= edi;
    *((rax + 2)) = dl;
    while (1) {
        *(rsi) = 0;
label_0:
        rax = r12;
        return rax;
        edx = ecx;
        *((rax + 3)) = 0x3a;
        rdx *= r8;
        rdx >>= 0x23;
        esi = rdx + 0x30;
        edx = rdx * 5;
        edx += edx;
        *((rax + 4)) = sil;
        rsi = rax + 6;
        ecx -= edx;
        ecx += 0x30;
        *((rax + 5)) = cl;
    }
}

/* /tmp/tmpyosj1dz2 @ 0x5130 */
 
int64_t tm_year_str (uint32_t arg1, uint32_t arg2, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    rax = (int64_t) edi;
    edx = edi;
    r12 = rsi;
    rax *= 0x51eb851f;
    edx >>= 0x1f;
    rsi = "-%02d%02d";
    rax >>= 0x25;
    eax -= edx;
    edx = edi;
    ecx = eax * 0x64;
    eax += 0x13;
    r8d = eax;
    edx -= ecx;
    ecx = 0;
    rdi = r12;
    cl = (edi >= 0xfffff894) ? 1 : 0;
    r9d = edx;
    rcx += rsi;
    r9d = -r9d;
    esi = 1;
    __asm ("cmovs r9d, edx");
    r8d = -r8d;
    rdx = 0xffffffffffffffff;
    __asm ("cmovs r8d, eax");
    eax = 0;
    sprintf_chk ();
    rax = r12;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x38d0 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmpyosj1dz2 @ 0x51a0 */
 
int64_t dbg_dbg_printf (int64_t arg_e0h, int64_t arg1, int64_t arg10, int64_t arg11, char * arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list args;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    char * var_28h;
    char * var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* void dbg_printf(char const * msg,va_args ...); */
    *((rsp + 0x28)) = rsi;
    *((rsp + 0x30)) = rdx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x40)) = r8;
    *((rsp + 0x48)) = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    fwrite ("date: ", 1, 6, *(obj.stderr));
    rax = rsp + 0xe0;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *(rsp) = 8;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    rpl_vfprintf (*(obj.stderr), rbp, rsp);
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x5290 */
 
uint64_t lookup_zone (int64_t arg_f0h, int64_t arg1, char * arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = obj_universal_time_zone_table;
    rbx = rsi;
    rsi = 0x00012a36;
    while (eax != 0) {
        rsi = *((r12 + 0x10));
        r12 += 0x10;
        if (rsi == 0) {
            goto label_3;
        }
        eax = strcmp (rbx, rsi);
    }
label_0:
    rax = r12;
    return rax;
label_3:
    rsi = *((rbp + 0xf0));
    r12 = rbp + 0xf0;
    if (rsi != 0) {
        goto label_4;
    }
label_1:
    r12 = obj_time_zone_table;
    rsi = 0x00012a3a;
    while (eax != 0) {
        rsi = *((r12 + 0x10));
        r12 += 0x10;
        if (rsi == 0) {
            goto label_5;
        }
        eax = strcmp (rbx, rsi);
    }
    goto label_0;
label_2:
    rsi = *((r12 + 0x10));
    r12 += 0x10;
    if (rsi == 0) {
        goto label_1;
    }
label_4:
    eax = strcmp (rbx, rsi);
    if (eax != 0) {
        goto label_2;
    }
    rax = r12;
    return rax;
label_5:
    r12d = 0;
    rax = r12;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x5360 */
 
int64_t debug_strfdatetime_constprop_0 (int64_t arg_18h, uint32_t arg_d8h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rcx = rdi;
    r9d = 0;
    r8d = 0;
    rdx = "(Y-M-D) %Y-%m-%d %H:%M:%S";
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = nstrftime (r12, 0x64);
    if (rbp == 0) {
        goto label_0;
    }
    rbx = rax;
    if (eax > 0x63) {
        goto label_0;
    }
    while (1) {
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        rax = time_zone_str (*((rbp + 0x18)), rsp);
        esi = 0x64;
        rdi = (int64_t) ebx;
        r8 = " TZ=%s";
        esi -= ebx;
        r9 = rax;
        rdi += r12;
        rcx = 0xffffffffffffffff;
        rsi = (int64_t) esi;
        edx = 1;
        eax = 0;
        snprintf_chk ();
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x5410 */
 
uint64_t str_days_constprop_0 (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rbx = rdi;
    if (*((rdi + 0xe8)) == 0) {
        goto label_1;
    }
    r9 = *((rdi + 8));
    rax = r9 + 1;
    if (rax <= 0xd) {
        goto label_2;
    }
    edx = 1;
    esi = 0x64;
    rdi = r12;
    eax = 0;
    r8 = 0x00012add;
    rcx = 0xffffffffffffffff;
    eax = snprintf_chk ();
label_0:
    rdx = *((rbx + 0x10));
    if (edx > 6) {
        goto label_3;
    }
    if (eax <= 0x63) {
        goto label_4;
    }
    do {
label_3:
        rax = r12;
        return rax;
label_1:
        *(rsi) = 0;
        rdx = *((rdi + 0x10));
    } while (edx > 6);
    rdi = rsi;
    ecx = 1;
    esi = 0x64;
    do {
        rax = obj_days_values_0;
        r9 = rax + rdx*4;
        rax = 0x00012a07;
        edx = 1;
        r8 = rax + rcx;
        rcx = 0xffffffffffffffff;
        eax = 0;
        snprintf_chk ();
        rax = r12;
        return rax;
label_2:
        rdx = rax * 5;
        rax = "last";
        rcx += rax;
        eax = 0;
        eax = snprintf (r12, 0x64, 0x00012a08, rax + rdx*2);
        goto label_0;
label_4:
        ecx = 0;
        esi = 0x64;
        cl = (eax == 0) ? 1 : 0;
        esi -= eax;
        rax = (int64_t) eax;
        rsi = (int64_t) esi;
        rdi = r12 + rax;
    } while (1);
}

/* /tmp/tmpyosj1dz2 @ 0x5520 */
 
int64_t debug_print_current_time_part_0 (int64_t arg1, int64_t arg2) {
    int64_t var_68h;
    rdi = arg1;
    rsi = arg2;
    edx = 5;
    r12 = rdi;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x68)) = rax;
    eax = 0;
    rax = dcgettext (0, "parsed %s part: ");
    eax = 0;
    dbg_printf (rax, r12, rdx, rcx, r8, r9);
    if (*((rbx + 0xa8)) != 0) {
        if (*((rbx + 0xe2)) == 0) {
            goto label_9;
        }
    }
    eax = *((rbx + 0xe7));
    edx = 0;
    if (*((rbx + 0xe0)) != al) {
label_1:
        r12 = *((rbx + 0x28));
        edx = 5;
        rax = dcgettext (0, "year: %04ld");
        rdi = stderr;
        esi = 1;
        rdx = rax;
        rcx = r12;
        eax = 0;
        fprintf_chk ();
        eax = *((rbx + 0xe0));
        edx = 1;
        *((rbx + 0xe7)) = al;
    }
label_0:
    if (*((rbx + 0xd0)) != 0) {
        if (*((rbx + 0xe5)) == 0) {
            goto label_10;
        }
    }
    if (*((rbx + 0xb0)) != 0) {
        if (*((rbx + 0xe3)) != 0) {
            goto label_2;
        }
        if (dl != 0) {
            goto label_11;
        }
label_3:
        rsi = rsp;
        rdi = rbx;
        r13d = *((rbx + 0x10));
        rbp = *((rbx + 8));
        rax = str_days_constprop_0 ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "%s (day ordinal=%ld number=%d)");
        r9d = r13d;
        r8 = rbp;
        rcx = r12;
        rdi = stderr;
        rdx = rax;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        *((rbx + 0xe3)) = 1;
        edx = 1;
    }
label_2:
    if (*((rbx + 0xc0)) == 0) {
        goto label_12;
    }
    if (*((rbx + 0xe4)) != 0) {
        goto label_12;
    }
    rax = 0x00012ce0;
    ecx = *((rbx + 0x14));
    r8 = " DST";
    if (*((rbx + 0xc8)) == 0) {
        r8 = rax;
    }
    edx ^= 1;
    rax = " isdst=%d%s";
    rdi = stderr;
    edx = (int32_t) dl;
    esi = 1;
    rdx += rax;
    eax = 0;
    fprintf_chk ();
    *((rbx + 0xe4)) = 1;
    if (*((rbx + 0xd8)) != 0) {
        if (*((rbx + 0xe6)) == 0) {
            goto label_13;
        }
    }
    if (*((rbx + 0xa0)) == 0) {
        goto label_4;
    }
label_5:
    r12 = *((rbx + 0x58));
    do {
        fputc (0x20, *(obj.stderr));
        goto label_14;
label_12:
        if (*((rbx + 0xd8)) != 0) {
            if (*((rbx + 0xe6)) == 0) {
                goto label_15;
            }
        }
        if (*((rbx + 0xa0)) == 0) {
            goto label_4;
        }
        r12 = *((rbx + 0x58));
    } while (dl != 0);
label_14:
    edx = 5;
    rax = dcgettext (0, "number of seconds: %ld");
    rdi = stderr;
    rcx = r12;
    esi = 1;
    rdx = rax;
    eax = 0;
    fprintf_chk ();
label_4:
    rax = *((rsp + 0x68));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_16;
    }
    rsi = stderr;
    edi = 0xa;
    void (*0x36b0)() ();
label_9:
    rcx = *((rbx + 0x28));
    r9 = *((rbx + 0x40));
    esi = 1;
    eax = 0;
    r8 = *((rbx + 0x38));
    rdi = stderr;
    rdx = "(Y-M-D) %04ld-%02ld-%02ld";
    fprintf_chk ();
    *((rbx + 0xe2)) = 1;
    edx = 1;
    eax = *((rbx + 0xe7));
    if (*((rbx + 0xe0)) == al) {
        goto label_0;
    }
    fputc (0x20, *(obj.stderr));
    goto label_1;
label_10:
    edx ^= 1;
    rcx = *((rbx + 0x48));
    r9 = *((rbx + 0x58));
    esi = 1;
    rax = " %02ld:%02ld:%02ld";
    r8 = *((rbx + 0x50));
    rdi = stderr;
    edx = (int32_t) dl;
    rdx += rax;
    eax = 0;
    fprintf_chk ();
    rcx = *((rbx + 0x60));
    if (rcx != 0) {
        goto label_17;
    }
label_6:
    if (*((rbx + 0x1c)) == 1) {
        goto label_18;
    }
label_8:
    *((rbx + 0xe5)) = 1;
    edx = 1;
    if (*((rbx + 0xb0)) == 0) {
        goto label_2;
    }
    edx = *((rbx + 0xe3));
    if (dl != 0) {
        goto label_2;
    }
label_11:
    fputc (0x20, *(obj.stderr));
    goto label_3;
label_15:
    edx ^= 1;
    rax = " UTC%s";
    r12d = (int32_t) dl;
    r12 += rax;
label_7:
    rax = time_zone_str (*((rbx + 0x18)), rsp);
    rdi = stderr;
    rdx = r12;
    esi = 1;
    rcx = rax;
    eax = 0;
    eax = fprintf_chk ();
    *((rbx + 0xe6)) = 1;
    if (*((rbx + 0xa0)) == 0) {
        goto label_4;
    }
    goto label_5;
label_17:
    rdi = stderr;
    rdx = ".%09d";
    esi = 1;
    eax = 0;
    fprintf_chk ();
    goto label_6;
label_13:
    r12 = " UTC%s";
    goto label_7;
label_18:
    fwrite (0x00012abb, 1, 2, *(obj.stderr));
    goto label_8;
label_16:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x5910 */
 
int64_t time_zone_hhmm_isra_0 (int64_t arg1, int64_t arg3, signed int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = rdi;
    rdi = rdx;
    if (rcx > 2) {
        goto label_2;
    }
    if (r8 < 0) {
        goto label_3;
    }
    do {
        edx = 0;
        rdi *= 0x3c;
        __asm ("seto dl");
        if (sil != 0) {
            goto label_4;
        }
        ecx = 0;
        rdi += r8;
        __asm ("seto cl");
label_0:
        eax = 0;
        edx |= ecx;
        if (edx == 0) {
label_1:
            rdx = rdi + 0x5a0;
            eax = 0;
            if (rdx > 0xb40) {
                goto label_5;
            }
            edi *= 0x3c;
            eax = 1;
            *((r9 + 0x18)) = edi;
        }
label_5:
        return eax;
label_2:
    } while (r8 >= 0);
    rdx = 0xa3d70a3d70a3d70b;
    rax = rdi;
    rcx = rdi;
    rdx:rax = rax * rdx;
    rax = rdi;
    rax >>= 0x3f;
    rdx += rdi;
    rdx >>= 6;
    rdx -= rax;
    rax = rdx * 5;
    rdi = rdx;
    rax *= 5;
    rax <<= 2;
    rcx -= rax;
    goto label_6;
label_4:
    ecx = 0;
    rdi -= r8;
    __asm ("seto cl");
    goto label_0;
label_3:
    ecx = 0;
label_6:
    rax = rdi;
    rax <<= 4;
    rax -= rdi;
    rdi = rcx + rax*4;
    goto label_1;
}

/* /tmp/tmpyosj1dz2 @ 0x59d0 */
 
uint64_t debug_print_relative_time_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    edx = 5;
    rbx = rsi;
    rax = dcgettext (0, "parsed %s part: ");
    eax = 0;
    eax = dbg_printf (rax, rbp, rdx, rcx, r8, r9);
    rcx = *((rbx + 0x68));
    if (rcx != 0) {
        goto label_3;
    }
    rcx = *((rbx + 0x70));
    rbp = stderr;
    if (rcx == 0) {
        goto label_4;
    }
    rdx = 0x00012ae2;
label_2:
    r8 = "month(s)";
    esi = 1;
    rdi = rbp;
    eax = 0;
    fprintf_chk ();
label_1:
    rcx = *((rbx + 0x78));
    rbp = stderr;
    eax = 1;
    while (rcx != 0) {
        rdi = rbp;
        rdx = " %+ld %s";
        esi = 1;
        rdx += 0;
        r8 = "day(s)";
        eax = 0;
        fprintf_chk ();
        rbp = stderr;
        eax = 1;
label_0:
        rcx = *((rbx + 0x80));
        if (rcx != 0) {
            rdi = rbp;
            rdx = " %+ld %s";
            esi = 1;
            rdx += 0;
            r8 = "hour(s)";
            eax = 0;
            fprintf_chk ();
            rbp = stderr;
            eax = 1;
        }
        rcx = *((rbx + 0x88));
        if (rcx != 0) {
            rdi = rbp;
            rdx = " %+ld %s";
            esi = 1;
            rdx += 0;
            r8 = "minutes";
            eax = 0;
            fprintf_chk ();
            rbp = stderr;
            eax = 1;
        }
        rcx = *((rbx + 0x90));
        if (rcx != 0) {
            rdi = rbp;
            rdx = " %+ld %s";
            esi = 1;
            rdx += 0;
            r8 = 0x00012b6d;
            eax = 0;
            fprintf_chk ();
            rbp = stderr;
            eax = 1;
        }
        rcx = *((rbx + 0x98));
        if (rcx != 0) {
            rdi = rbp;
            rdx = " %+ld %s";
            esi = 1;
            rdx += 0;
            r8 = "nanoseconds";
            eax = 0;
            eax = fprintf_chk ();
            rbp = stderr;
        }
        rsi = rbp;
        edi = 0xa;
        void (*0x36b0)() ();
label_4:
        rcx = *((rbx + 0x78));
        eax = 0;
    }
    if (*((rbx + 0x80)) != 0) {
        goto label_0;
    }
    if (*((rbx + 0x88)) != 0) {
        goto label_0;
    }
    if (*((rbx + 0x90)) != 0) {
        goto label_0;
    }
    edx = *((rbx + 0x98));
    if (edx != 0) {
        goto label_0;
    }
    edx = 5;
    rax = dcgettext (0, "today/this/now\n");
    rsi = rbp;
    rdi = rax;
    void (*0x3680)() ();
label_3:
    rdi = stderr;
    r8 = "year(s)";
    rdx = 0x00012ae2;
    eax = 0;
    esi = 1;
    fprintf_chk ();
    rcx = *((rbx + 0x70));
    if (rcx == 0) {
        goto label_1;
    }
    rbp = stderr;
    rdx = " %+ld %s";
    goto label_2;
}

/* /tmp/tmpyosj1dz2 @ 0x9a80 */
 
int64_t dbg_year (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4) {
    time_t now;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool year(tm * tm,int const * digit_pair,idx_t n,unsigned int syntax_bits); */
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rdx == 1) {
        goto label_1;
    }
    if (rdx == 2) {
        goto label_2;
    }
    rax = time (0);
    *(rsp) = rax;
    rax = localtime (rsp);
    if (rax == 0) {
        goto label_0;
    }
    eax = *((rax + 0x14));
    *((rbx + 0x14)) = eax;
    eax = 1;
    do {
label_0:
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_3;
        }
        return rax;
label_1:
        edx = *(rsi);
        eax = 1;
        *((rdi + 0x14)) = edx;
    } while (edx > 0x44);
    ecx &= 8;
    if (ecx == 0) {
        edx += 0x64;
        *((rdi + 0x14)) = edx;
        goto label_0;
label_2:
        ecx &= 2;
        if (ecx == 0) {
            goto label_4;
        }
        eax = *(rsi) * 0x64;
        eax += *((rsi + 4));
        eax -= 0x76c;
        *((rdi + 0x14)) = eax;
        eax = 1;
        goto label_0;
    }
label_4:
    eax = 0;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xc230 */
 
uint64_t revert_tz_part_0 (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    rax = errno_location ();
    r12d = *(rax);
    if (*((rbx + 8)) != 0) {
        goto label_2;
    }
    rdi = 0x00012e3e;
    eax = unsetenv ();
    if (eax == 0) {
        goto label_3;
    }
label_0:
    r12d = *(rbp);
    r13d = 0;
    do {
label_1:
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
    *(rbp) = r12d;
    eax = r13d;
    return rax;
label_2:
    eax = setenv (0x00012e3e, rbx + 9, 1);
    if (eax != 0) {
        goto label_0;
    }
label_3:
    tzset ();
    r13d = 1;
    goto label_1;
}

/* /tmp/tmpyosj1dz2 @ 0xc360 */
 
uint64_t dbg_save_abbr (uint32_t arg_8h, int64_t arg_9h, int64_t arg_80h, int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool save_abbr(timezone_t tz,tm * tm); */
    r12 = *((rsi + 0x30));
    if (r12 == 0) {
        goto label_4;
    }
    r13 = rsi;
    if (rsi <= r12) {
        rdx = rsi + 0x38;
        eax = 1;
        if (r12 < rdx) {
            goto label_3;
        }
    }
    rbx = rbp + 9;
    if (*(r12) == 0) {
        goto label_5;
    }
    do {
label_0:
        eax = strcmp (rbx, r12);
        if (eax == 0) {
            goto label_2;
        }
label_1:
        if (*(rbx) == 0) {
            rax = rbp + 9;
            if (rbx != rax) {
                goto label_6;
            }
            if (*((rbp + 8)) == 0) {
                goto label_6;
            }
        }
        strlen (rbx);
        rbx = rbx + rax + 1;
    } while (*(rbx) != 0);
    rax = *(rbp);
    if (rax == 0) {
        goto label_0;
    }
    rbx = rax + 9;
    eax = strcmp (rbx, r12);
    if (eax != 0) {
        goto label_1;
    }
    do {
label_2:
        *((r13 + 0x30)) = rbx;
        eax = 1;
label_3:
        return rax;
label_5:
        rbx = 0x00012ce0;
    } while (1);
label_4:
    eax = 1;
    return rax;
label_6:
    rax = strlen (r12);
    r14 = rax;
    rdx = rax + 1;
    rax = rbp + 0x80;
    rax -= rbx;
    if (rax > rdx) {
        memcpy (rbx, r12, rdx);
        *((rbx + r14 + 1)) = 0;
        goto label_2;
    }
    rax = tzalloc (r12);
    *(rbp) = rax;
    if (rax != 0) {
        *((rax + 8)) = 0;
        rbx = rax + 9;
        goto label_2;
    }
    eax = 0;
    goto label_3;
}

/* /tmp/tmpyosj1dz2 @ 0xc490 */
 
uint64_t dbg_set_tz (int64_t arg1) {
    rdi = arg1;
    /* timezone_t set_tz(timezone_t tz); */
    r13 = 0x00012e3e;
    rbx = rdi;
    rax = getenv (r13);
    if (rax == 0) {
        goto label_2;
    }
    while (eax != 0) {
label_0:
        rax = tzalloc (rbp);
        r12 = rax;
        if (rax != 0) {
            if (*((rbx + 8)) != 0) {
                goto label_3;
            }
            rdi = r13;
            eax = unsetenv ();
            if (eax != 0) {
                goto label_4;
            }
label_1:
            tzset ();
        }
        rax = r12;
        return rax;
        r12d = 1;
        eax = strcmp (rbx + 9, rax);
    }
    rax = r12;
    return rax;
label_2:
    r12d = 1;
    if (*((rbx + 8)) != 0) {
        goto label_0;
    }
    rax = r12;
    return rax;
label_3:
    eax = setenv (r13, rbx + 9, 1);
    if (eax == 0) {
        goto label_1;
    }
label_4:
    rax = errno_location ();
    ebx = *(rax);
    if (r12 == 1) {
        goto label_5;
    }
    do {
        rdi = r12;
        r12 = *(r12);
        free (rdi);
    } while (r12 != 0);
label_5:
    *(rbp) = ebx;
    r12d = 0;
    rax = r12;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xd4a0 */
 
int64_t dbg_ydhms_diff (int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg3, int64_t arg4, int64_t arg6, int32_t sec1, long_int yday1) {
    int32_t yday0;
    int32_t hour0;
    int32_t min0;
    int32_t sec0;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    r8 = sec1;
    rsi = yday1;
    /* long_int ydhms_diff(long_int year1,long_int yday1,int hour1,int min1,int sec1,int year0,int yday0,int hour0,int min0,int sec0); */
    r11 = (int64_t) ecx;
    rcx = rdi;
    rax = rdi;
    ecx &= 3;
    r10 = (int64_t) edx;
    rax >>= 2;
    rbx = (int64_t) r9d;
    r8 = (int64_t) r8d;
    rdx = rbx;
    eax -= 0xfffffe25;
    r9 = rbx;
    edx &= 3;
    r9 >>= 2;
    r9d -= 0xfffffe25;
    ebp >>= 0x1f;
    rdi -= rbx;
    ecx = rbp + rax;
    eax -= r9d;
    rdx = (int64_t) ecx;
    ecx >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x23;
    edx -= ecx;
    edx -= ebp;
    ebp >>= 0x1f;
    r12d = rbp + r9;
    r9d = edx;
    rdx = (int64_t) edx;
    rcx = (int64_t) r12d;
    r12d >>= 0x1f;
    rcx *= 0x51eb851f;
    rdx >>= 2;
    rcx >>= 0x23;
    ecx -= r12d;
    ecx -= ebp;
    r9d -= ecx;
    rcx = (int64_t) ecx;
    rcx >>= 2;
    eax -= r9d;
    edx -= ecx;
    rcx = *((rsp + 0x20));
    eax += edx;
    rdx = rdi * 9;
    rdx = rdi + rdx*8;
    rax = (int64_t) eax;
    rdx *= 5;
    rdx += rsi;
    rdx -= rcx;
    rax += rdx;
    rdx = *((rsp + 0x28));
    rax *= 3;
    rax = r10 + rax*8;
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r11 + rdx*4;
    rdx = *((rsp + 0x30));
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r8 + rdx*4;
    rdx = *((rsp + 0x38));
    r12 = rbx;
    rax -= rdx;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xd5a0 */
 
int64_t dbg_ranged_convert (int64_t arg1, signed int64_t arg2, int64_t arg3) {
    time_t x;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_24h;
    int64_t var_28h;
    int64_t var_2ch;
    int64_t canary;
    int64_t var_38h;
    int64_t var_40h;
    signed int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* tm * ranged_convert(tm * (*)() convert,long_int * t,tm * tp); */
    r15 = rsi;
    r14 = rdx;
    rbx = rdi;
    r12 = *(rsi);
    rbp = rsp + 0x50;
    rsi = rdx;
    rdi = rbp;
    rax = *(fs:0x28);
    eax = 0;
    rax = void (*rbx)(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13, r14);
    *((rsp + 0x38)) = rax;
    if (rax == 0) {
        goto label_2;
    }
    *(r15) = r12;
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = *((rsp + 0x38));
        return rax;
label_2:
        rax = errno_location ();
        *((rsp + 0x40)) = rax;
    } while (*(rax) != 0x4b);
    rcx = r12;
    rax = r12;
    ecx &= 1;
    rax >>= 1;
    r13 = rcx + rax;
    if (r12 == r13) {
        goto label_0;
    }
    if (r13 == 0) {
        goto label_0;
    }
    r15d = 0;
    rax = r14;
    *((rsp + 4)) = 0xffffffff;
    r14 = r12;
    r12 = r13;
    r13 = r15;
    r15 = rax;
    while (rax != 0) {
        eax = *(r15);
        r13 = r12;
        *((rsp + 4)) = eax;
        eax = *((r15 + 4));
        *((rsp + 0x2c)) = eax;
        eax = *((r15 + 8));
        *((rsp + 0x28)) = eax;
        eax = *((r15 + 0xc));
        *((rsp + 0x24)) = eax;
        eax = *((r15 + 0x10));
        *((rsp + 0x20)) = eax;
        eax = *((r15 + 0x14));
        *((rsp + 0xc)) = eax;
        eax = *((r15 + 0x18));
        *((rsp + 0x18)) = eax;
        eax = *((r15 + 0x1c));
        *((rsp + 0x1c)) = eax;
        eax = *((r15 + 0x20));
        *((rsp + 8)) = eax;
        rax = *((r15 + 0x28));
        *((rsp + 0x10)) = rax;
        rax = *((r15 + 0x30));
        *((rsp + 0x30)) = rax;
label_1:
        rdx = r13;
        rax = r14;
        rax >>= 1;
        rdx >>= 1;
        rdx += rax;
        rax = r13;
        rax |= r14;
        eax &= 1;
        r12 = rdx + rax;
        if (r12 == r13) {
            goto label_4;
        }
        if (r12 == r14) {
            goto label_4;
        }
        rsi = r15;
        rdi = rbp;
        rax = void (*rbx)(uint64_t) (r12);
    }
    rax = *((rsp + 0x40));
    if (*(rax) != 0x4b) {
        goto label_0;
    }
    r14 = r12;
    goto label_1;
label_4:
    eax = *((rsp + 4));
    r14 = r15;
    if (eax < 0) {
        goto label_0;
    }
    rcx = *((rsp + 0x48));
    *((rsp + 0x38)) = r14;
    *(rcx) = r13;
    *(r14) = eax;
    eax = *((rsp + 0x2c));
    *((r14 + 4)) = eax;
    eax = *((rsp + 0x28));
    *((r14 + 8)) = eax;
    eax = *((rsp + 0x24));
    *((r14 + 0xc)) = eax;
    eax = *((rsp + 0x20));
    *((r14 + 0x10)) = eax;
    eax = *((rsp + 0xc));
    *((r14 + 0x14)) = eax;
    eax = *((rsp + 0x18));
    *((r14 + 0x18)) = eax;
    eax = *((rsp + 0x1c));
    *((r14 + 0x1c)) = eax;
    eax = *((rsp + 8));
    *((r14 + 0x20)) = eax;
    rax = *((rsp + 0x10));
    *((r14 + 0x28)) = rax;
    rax = *((rsp + 0x30));
    *((r14 + 0x30)) = rax;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xe120 */
 
uint32_t rotate_right32 (uint32_t value, uint32_t count) {
    const uint32_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t strftime_internal_isra_0 (int64_t arg_500h, int64_t arg_508h, int64_t arg_518h, int64_t arg1, uint32_t arg2, int64_t arg3, tm * arg4, uint32_t arg6) {
    int64_t var_1h;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_1fh;
    tm * timeptr;
    tm * var_28h;
    int64_t var_30h;
    uint32_t var_34h;
    size_t var_38h;
    size_t var_40h;
    size_t var_50h;
    tm * var_58h;
    int64_t var_60h;
    tm * var_64h;
    tm * var_68h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_8ch;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_a8h;
    char * format;
    int64_t var_ach;
    int64_t var_adh;
    int64_t var_aeh;
    int64_t var_afh;
    char * s;
    void * s2;
    int64_t var_c7h;
    int64_t var_4b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
label_19:
    r14 = rdi;
    rbx = rcx;
    rax = *((rsp + 0x508));
    *((rsp + 8)) = rsi;
    *((rsp + 0x20)) = rcx;
    r15 = *((rsp + 0x500));
    *((rsp + 0x34)) = r9d;
    *((rsp + 0x28)) = rax;
    *((rsp + 0x1f)) = r8b;
    rax = *(fs:0x28);
    *((rsp + 0x4b8)) = rax;
    eax = 0;
    rax = errno_location ();
    r11 = *((rbx + 0x30));
    r10d = *((rbx + 8));
    *((rsp + 0x10)) = rax;
    eax = *(rax);
    *((rsp + 0x30)) = eax;
    rax = 0x00012ce0;
    if (r11 == 0) {
        r11 = rax;
    }
    if (r10d <= 0xc) {
        goto label_63;
    }
    r10d -= 0xc;
label_1:
    eax = *(rbp);
    r13d = 0;
    if (al == 0) {
        goto label_64;
    }
    *((rsp + 0x60)) = r10d;
    r12 = r11;
    do {
        if (al == 0x25) {
            goto label_65;
        }
        eax = 0;
        rdx = *((rsp + 8));
        ecx = 1;
        __asm ("cmovns rax, r15");
        if (rax != 0) {
            rcx = rax;
        }
        rdx -= r13;
        if (rcx >= rdx) {
            goto label_9;
        }
        if (r14 != 0) {
            if (r15d > 1) {
                goto label_66;
            }
label_2:
            eax = *(rbp);
            r14++;
            *((r14 - 1)) = al;
        }
        r13 += rcx;
        rbx = rbp;
label_0:
        eax = *((rbx + 1));
        rbp = rbx + 1;
        r15 = 0xffffffffffffffff;
    } while (al != 0);
label_64:
    if (r14 != 0) {
        if (*((rsp + 8)) == 0) {
            goto label_67;
        }
        *(r14) = 0;
    }
label_67:
    rax = *((rsp + 0x10));
    edi = *((rsp + 0x30));
    *(rax) = edi;
    goto label_68;
label_12:
    if (*((rsp + 8)) != r13) {
        goto label_0;
    }
    do {
label_9:
        rax = *((rsp + 0x10));
        *(rax) = 0x22;
label_62:
        r13d = 0;
label_68:
        rax = *((rsp + 0x4b8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_69;
        }
        rax = r13;
        return rax;
label_63:
        eax = 0xc;
        if (r10d == 0) {
            r10d = eax;
        }
        goto label_1;
label_66:
        rbx = rax - 1;
        *((rsp + 0x38)) = rcx;
        r14 += rbx;
        memset (r14, 0x20, rbx);
        rcx = *((rsp + 0x38));
        goto label_2;
label_65:
        eax = *((rsp + 0x1f));
        rbx = rbp;
        r8d = 0;
        r10d = 0;
        *((rsp + 0x38)) = al;
label_7:
        edx = *((rbx + 1));
        rbx++;
        ecx = rdx - 0x23;
        esi = edx;
        edi = edx;
        if (cl <= 0x3c) {
            r9 = 0x1000000000002500;
            eax = 1;
            rax <<= cl;
            if ((rax & r9) != 0) {
                goto label_70;
            }
            if (cl == 0x3b) {
                goto label_71;
            }
            eax &= 1;
            if (eax != 0) {
                goto label_72;
            }
        }
        edx -= 0x30;
        if (edx <= 9) {
            goto label_73;
        }
label_4:
        if (sil == 0x45) {
            goto label_74;
        }
        if (sil == 0x4f) {
            goto label_74;
        }
        edi = 0;
label_3:
        if (sil <= 0x7a) {
            rcx = 0x00014694;
            eax = (int32_t) sil;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (123 cases) at 0x14694 */
            rax = void (*rax)() ();
            if (edi != 0x4f) {
                goto label_75;
            }
        }
label_8:
        rcx = rbx;
        rcx -= rbp;
        r8 = rcx + 1;
        if (r15d >= 0) {
            if (r10d != 0x2d) {
                rdx = (int64_t) r15d;
                r9 = rdx;
                if (r8 >= rdx) {
                    r9 = r8;
                }
            }
        } else {
            r9 = r8;
            edx = 0;
        }
        rax = *((rsp + 8));
        rax -= r13;
    } while (rax <= r9);
    if (r14 == 0) {
        goto label_76;
    }
    if (r8 < rdx) {
        rdx -= r8;
        *((rsp + 0x58)) = r9;
        *((rsp + 0x50)) = r8;
        r15 = r14 + rdx;
        *((rsp + 0x40)) = rcx;
        if (r10d == 0x30) {
            goto label_77;
        }
        if (r10d == 0x2b) {
            goto label_77;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
        r9 = *((rsp + 0x58));
        r8 = *((rsp + 0x50));
        rcx = *((rsp + 0x40));
    }
label_37:
    if (*((rsp + 0x38)) == 0) {
        goto label_78;
    }
    r15 = rcx;
    if (r8 == 0) {
        goto label_79;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r8;
    rax = ctype_toupper_loc ();
    r8 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    do {
        ecx = *((rbp + r15));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + r15)) = dl;
        r15--;
    } while (r15 >= 0);
    goto label_79;
label_74:
    esi = *((rbx + 1));
    rbx++;
    goto label_3;
label_78:
    rdx = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r8;
    memcpy (r14, rbp, rdx);
    r9 = *((rsp + 0x40));
    r8 = *((rsp + 0x38));
label_79:
    r14 += r8;
label_76:
    r13 += r9;
    goto label_0;
label_73:
    r15d = 0;
    goto label_80;
label_5:
    eax = *(rbx);
    eax -= 0x30;
    r15d += eax;
    if (r15d overflow 0) {
        goto label_81;
    }
label_6:
    edi = *((rbx + 1));
    rbx++;
    eax = rdi - 0x30;
    esi = edi;
    if (eax > 9) {
        goto label_4;
    }
label_80:
    r15d *= 0xa;
    if (eax !overflow 9) {
        goto label_5;
    }
label_81:
    r15d = 0x7fffffff;
    goto label_6;
label_70:
    r10d = edx;
    goto label_7;
    eax = *((rsp + 0x38));
    if (r8b != 0) {
        eax = r8d;
    }
    *((rsp + 0x38)) = al;
    if (edi == 0x45) {
        goto label_8;
    }
label_75:
    ebp = 0;
label_16:
    r11d = 0x2520;
    *((rsp + 0xab)) = r11w;
    if (edi != 0) {
        goto label_82;
    }
    rax = rsp + 0xad;
label_10:
    *(rax) = sil;
    *((rax + 1)) = 0;
    *((rsp + 0x40)) = r10d;
    rax = strftime (rsp + 0xb0, section..dynsym, rsp + 0xab, *((rsp + 0x20)));
    rcx = rax;
    if (rax == 0) {
        goto label_0;
    }
    r10d = *((rsp + 0x40));
    r8 = rax - 1;
    if (r10d == 0x2d) {
        goto label_83;
    }
    if (r15d < 0) {
        goto label_83;
    }
    rdx = (int64_t) r15d;
    r15 = rdx;
    if (r8 >= rdx) {
        r15 = r8;
    }
label_31:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= r15) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_15;
    }
    if (r8 < rdx) {
        rdx -= r8;
        *((rsp + 0x58)) = r8;
        rax = r14 + rdx;
        *((rsp + 0x50)) = rcx;
        *((rsp + 0x40)) = rax;
        if (r10d == 0x30) {
            goto label_84;
        }
        if (r10d == 0x2b) {
            goto label_84;
        }
        memset (r14, 0x20, rdx);
        r14 = *((rsp + 0x40));
        r8 = *((rsp + 0x58));
        rcx = *((rsp + 0x50));
    }
label_41:
    if (bpl != 0) {
        goto label_85;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_86;
    }
    rbp = rcx - 2;
    if (r8 == 0) {
        goto label_38;
    }
    *((rsp + 0x38)) = r8;
    rax = ctype_toupper_loc ();
    r8 = *((rsp + 0x38));
    rsi = rsp + 0xb1;
    do {
        ecx = *((rsi + rbp));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
label_38:
    r14 += r8;
label_15:
    r13 += r15;
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    rdx = *((rsp + 0x20));
    ecx = *((rdx + 0x14));
    r11d = *((rdx + 0x1c));
    ebp = *((rdx + 0x18));
    eax = ecx;
    edx = r11d;
    eax >>= 0x1f;
    edx -= ebp;
    edx += 0x17e;
    eax &= 0x190;
    r9d = rcx + rax - 0x64;
    rax = (int64_t) edx;
    r8d = edx;
    rax *= 0xffffffff92492493;
    r8d >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= r8d;
    r8d = rax*8;
    r8d -= eax;
    eax = r11d;
    eax -= edx;
    r8d = rax + r8 + 3;
    if (r8d < 0) {
        goto label_87;
    }
    eax = 0x16d;
    if ((r9b & 3) == 0) {
        edx = r9d * 0xc28f5c29;
        eax = 0x16e;
        edx += 0x51eb850;
        edx = rotate_right32 (edx, 2);
        if (edx > 0x28f5c28) {
            goto label_88;
        }
        eax = r9d;
        r9d = 0x190;
        edx:eax = (int64_t) eax;
        eax = edx:eax / r9d;
        edx = edx:eax % r9d;
        edx = -edx;
        eax -= eax;
        eax += 0x16e;
    }
label_88:
    r11d -= eax;
    edx = r11d;
    edx -= ebp;
    edx += 0x17e;
    rax = (int64_t) edx;
    r9d = edx;
    r11d -= edx;
    rax *= 0xffffffff92492493;
    r9d >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= r9d;
    r9d = rax*8;
    r9d -= eax;
    eax = r11 + r9 + 3;
    __asm ("cmovns r8d, eax");
    eax >>= 0x1f;
    eax++;
label_52:
    if (sil == 0x47) {
        goto label_89;
    }
    if (sil != 0x67) {
        goto label_90;
    }
    rdx = (int64_t) ecx;
    r8d = ecx;
    rdx *= 0x51eb851f;
    r8d >>= 0x1f;
    rdx >>= 0x25;
    edx -= r8d;
    r8d = ecx;
    edx *= 0x64;
    r8d -= edx;
    r8d += eax;
    rdx = (int64_t) r8d;
    r9d = r8d;
    rdx *= 0x51eb851f;
    r9d >>= 0x1f;
    rdx >>= 0x25;
    edx -= r9d;
    r9d = edx * 0x64;
    edx = r8d;
    edx -= r9d;
    if (edx < 0) {
        goto label_91;
    }
    if (r10d != 0) {
        goto label_61;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_92;
    }
label_26:
    *((rsp + 0x58)) = 0;
    eax = 1;
    *((rsp + 0x40)) = 0;
    *((rsp + 0x50)) = 2;
label_18:
    r9d = 0;
label_13:
    if (edi != 0x4f) {
        goto label_93;
    }
    if (al == 0) {
        goto label_93;
    }
label_17:
    ecx = 0x2520;
    ebp = 0;
    *((rsp + 0xab)) = cx;
label_82:
    *((rsp + 0xad)) = dil;
    rax = rsp + 0xae;
    goto label_10;
    if (edi != 0) {
        goto label_8;
    }
    eax = *((rsp + 0x38));
    edx = 0x2520;
    *((rsp + 0xab)) = dx;
    if (r8b != 0) {
        eax = r8d;
    }
    ebp = 0;
    *((rsp + 0x38)) = al;
    rax = rsp + 0xad;
    goto label_10;
    if (edi == 0x45) {
        goto label_8;
    }
    eax = 9;
    edx = *((rsp + 0x510));
    if (r15d <= 0) {
        r15d = eax;
    }
    while (ebp > r15d) {
label_11:
        rax *= 0x66666667;
        ecx = edx;
        ebp--;
        ecx >>= 0x1f;
        rax >>= 0x22;
        eax -= ecx;
        edx = eax;
        rax = (int64_t) edx;
    }
    rcx = rax * 0x66666667;
    esi = edx;
    esi >>= 0x1f;
    rcx >>= 0x22;
    ecx -= esi;
    esi = rcx * 5;
    ecx = edx;
    esi += esi;
    ecx -= esi;
    if (ebp == 1) {
        goto label_94;
    }
    if (ecx == 0) {
        goto label_11;
    }
    rsi = (int64_t) ebp;
    rdi = rsi;
    if (ebp == 0) {
        goto label_95;
    }
label_51:
    rcx = rsp + rdi + 0xb0;
    r8d = rbp - 1;
    rdi = rsp + rdi + 0xaf;
    rdi -= r8;
    while (rcx != rdi) {
        rax = (int64_t) eax;
        rax *= 0x66666667;
        r8d = edx;
        rcx--;
        r8d >>= 0x1f;
        rax >>= 0x22;
        eax -= r8d;
        r8d = rax * 5;
        r8d += r8d;
        edx -= r8d;
        edx += 0x30;
        *(rcx) = dl;
        edx = eax;
    }
label_60:
    eax = 0x30;
    if (r10d == 0) {
        r10d = eax;
    }
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= rsi) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_96;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_97;
    }
    rdx = rsi - 1;
    *((rsp + 0x50)) = rdx;
    if (rsi == 0) {
        goto label_58;
    }
    *((rsp + 0x40)) = rsi;
    *((rsp + 0x38)) = r10d;
    rax = ctype_toupper_loc ();
    r10d = *((rsp + 0x38));
    rdx = *((rsp + 0x50));
    rdi = rsp + 0xb0;
    rsi = *((rsp + 0x40));
    rcx = rax;
    do {
        r8d = *((rdi + rdx));
        rax = *(rcx);
        eax = *((rax + r8*4));
        *((r14 + rdx)) = al;
        rdx--;
    } while (rdx >= 0);
label_58:
    r14 += rsi;
label_96:
    r13 += rsi;
    if (r10d == 0x2d) {
        goto label_12;
    }
    edx = r15d;
    edx -= ebp;
    if (edx < 0) {
        goto label_12;
    }
    rax = *((rsp + 8));
    rdx = (int64_t) edx;
    rax -= r13;
    if (rdx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_98;
    }
    if (rdx == 0) {
        goto label_0;
    }
    rbp = r14 + rdx;
    r13 += rdx;
    if (r10d == 0x30) {
        goto label_99;
    }
    if (r10d == 0x2b) {
        goto label_99;
    }
    r14 = rbp;
    memset (r14, 0x20, rdx);
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *(rax);
label_14:
    eax = edx;
    *((rsp + 0x58)) = 0;
    r9d = 0;
    eax >>= 0x1f;
    *((rsp + 0x40)) = eax;
    eax = edx;
    eax = ~eax;
    eax >>= 0x1f;
    goto label_13;
    if (edi == 0x45) {
        goto label_8;
    }
    *((rsp + 0x50)) = 2;
    edx = *((rsp + 0x60));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 4));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 8));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    r11 = *((rsp + 0x20));
    eax = *((r11 + 0x18));
    r8d = *((r11 + 0x1c));
    edx = rax + 6;
    *((rsp + 0x40)) = eax;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    eax = ecx;
    eax -= edx;
    eax = rax + r8 + 7;
label_24:
    rdx = (int64_t) eax;
    *((rsp + 0x50)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += eax;
    eax >>= 0x1f;
    edx >>= 2;
    edx -= eax;
    goto label_14;
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = r10d;
    __asm ("movdqu xmm4, xmmword [rax + 0x20]");
    __asm ("movdqu xmm0, xmmword [rax]");
    __asm ("movdqu xmm2, xmmword [rax + 0x10]");
    rax = *((rax + 0x30));
    *((rsp + 0x70)) = xmm0;
    *((rsp + 0x80)) = xmm2;
    *((rsp + 0xa0)) = rax;
    *((rsp + 0x8c)) = 0xffffffff;
    *((rsp + 0x40)) = xmm4;
    *((rsp + 0x90)) = xmm4;
    rax = mktime_z (*((rsp + 0x28)), rsp + 0x70, rdx, rcx, r8, r9);
    r9d = *((rsp + 0x8c));
    r10d = *((rsp + 0x50));
    rsi = rax;
    if (r9d < 0) {
        goto label_100;
    }
    rax >>= 0x3f;
    rcx = rsi;
    rdi = rsp + 0xc7;
    r11 = 0x6666666666666667;
    *((rsp + 0x40)) = rax;
    r8 = rdi;
    r9d = 0x30;
    do {
        rax = rcx;
        rdx:rax = rax * r11;
        rax = rcx;
        rax >>= 0x3f;
        rdx >>= 2;
        rdx -= rax;
        rbp = rdx * 5;
        rax = rdx;
        rbp += rbp;
        rcx -= rbp;
        rdx = rcx;
        rcx = rax;
        eax = r9d;
        eax -= edx;
        edx += 0x30;
        __asm ("cmovs edx, eax");
        r8--;
        *(r8) = dl;
    } while (rcx != 0);
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 1;
label_21:
    if (r10d == 0) {
        goto label_101;
    }
    al = (r10d != 0x2d) ? 1 : 0;
label_34:
    __asm ("cmovs r15d, dword [rsp + 0x50]");
    rdi -= r8;
    if (*((rsp + 0x40)) != 0) {
        goto label_102;
    }
    if (*((rsp + 0x58)) != 0) {
        goto label_103;
    }
    rcx = (int64_t) edi;
    if (r15d <= edi) {
        goto label_104;
    }
    if (al == 0) {
        goto label_104;
    }
label_33:
    rdx = (int64_t) r15d;
    r15 = rcx;
    if (rdx >= rcx) {
        r15 = rdx;
    }
label_32:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= r15) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_15;
    }
    if (rdx > rcx) {
        rdx -= rcx;
        *((rsp + 0x50)) = r8;
        *((rsp + 0x40)) = rcx;
        rbp = r14 + rdx;
        if (r10d == 0x30) {
            goto label_105;
        }
        if (r10d == 0x2b) {
            goto label_105;
        }
        r14 = rbp;
        memset (r14, 0x20, rdx);
        r8 = *((rsp + 0x50));
        rcx = *((rsp + 0x40));
    }
label_43:
    if (*((rsp + 0x38)) == 0) {
        goto label_106;
    }
    *((rsp + 0x38)) = r8;
    rbp = rcx - 1;
    if (rcx == 0) {
        goto label_36;
    }
    *((rsp + 0x40)) = rcx;
    rax = ctype_toupper_loc ();
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x38));
    do {
        esi = *((r8 + rbp));
        rdx = *(rax);
        edx = *((rdx + rsi*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
label_36:
    r14 += rcx;
    goto label_15;
    rbx--;
    goto label_8;
    rax = rbx - 1;
    if (rax == rbp) {
        goto label_107;
    }
    rbx = rax;
    goto label_8;
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_108;
    }
    if (r15d < 0) {
        goto label_108;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 != 0) {
        if (r15d > 1) {
            rdx--;
            r15 = r14 + rdx;
            if (r10d == 0x30) {
                goto label_109;
            }
            if (r10d == 0x2b) {
                goto label_109;
            }
            r14 = r15;
            memset (r14, 0x20, rdx);
        }
label_45:
        *(r14) = 9;
        r14++;
    }
label_23:
    r13 += rbp;
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    eax = *((rsp + 0x38));
    if (r8b != 0) {
        eax = r8d;
    }
    ebp = 0;
    *((rsp + 0x38)) = al;
    goto label_16;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    ecx = *((rax + 0x14));
    eax = rcx + 0x76c;
    rsp + 0x40 = (ecx < 0xfffff894) ? 1 : 0;
    eax -= eax;
    eax &= 0xffffff9d;
    eax += ecx;
    rdx = (int64_t) eax;
    eax >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x25;
    edx -= eax;
    edx += 0x13;
    al = (ecx >= 0xfffff894) ? 1 : 0;
    if (r10d != 0) {
        goto label_110;
    }
    r10d = *((rsp + 0x34));
    if (*((rsp + 0x34)) == 0x2b) {
        goto label_111;
    }
label_53:
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 2;
    goto label_18;
    if (edi != 0) {
        goto label_8;
    }
    *((rsp + 0x40)) = 0xffffffff;
    r11 = "%m/%d/%y";
label_20:
    r8d = *((rsp + 0x38));
    r9d = r10d;
    rdx = r11;
    eax = *((rsp + 0x518));
    edi = 0;
    rsi = 0xffffffffffffffff;
    eax = *((rsp + 0x58));
    rcx = *((rsp + 0x40));
    *((rsp + 0x78)) = r10d;
    *((rsp + 0x70)) = r8d;
    *((rsp + 0x58)) = r11;
    rax = _strftime_internal_isra_0 ();
    goto label_19;
    r10d = *((rsp + 0x78));
    r11 = *((rsp + 0x38));
    r8d = *((rsp + 0x50));
    if (r10d == 0x2d) {
        goto label_112;
    }
    if (r15d < 0) {
        goto label_112;
    }
    rdx = (int64_t) r15d;
    rax = rdx;
    if (rax >= rdx) {
        rax = rbp;
    }
    *((rsp + 0x38)) = rax;
label_39:
    r15 = *((rsp + 8));
    r15 -= r13;
    if (r15 <= *((rsp + 0x38))) {
        goto label_9;
    }
    if (r14 != 0) {
        if (rdx > rbp) {
            rdx -= rbp;
            *((rsp + 0x64)) = r10d;
            rcx = r14 + rdx;
            *((rsp + 0x58)) = r11;
            *((rsp + 0x68)) = rcx;
            *((rsp + 0x50)) = r8d;
            if (r10d == 0x30) {
                goto label_113;
            }
            if (r10d == 0x2b) {
                goto label_113;
            }
            memset (r14, 0x20, rdx);
            r14 = *((rsp + 0x68));
            r10d = *((rsp + 0x64));
            r11 = *((rsp + 0x58));
            r8d = *((rsp + 0x50));
        }
label_49:
        rdi = r14;
        r9d = r10d;
        rdx = r11;
        eax = *((rsp + 0x518));
        rsi = r15;
        r14 += rbp;
        eax = *((rsp + 0x58));
        rcx = *((rsp + 0x40));
        eax = _strftime_internal_isra_0 ();
        goto label_19;
    }
    r13 += *((rsp + 0x38));
    goto label_0;
    if (edi != 0) {
        goto label_8;
    }
    if (r15d < 0) {
        if (r10d == 0) {
            goto label_114;
        }
    }
    edi = r15 - 6;
    eax = 0;
    r11 = "%Y-%m-%d";
    __asm ("cmovns eax, edi");
    *((rsp + 0x40)) = eax;
    goto label_20;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 1;
    edx = *((rax + 0x18));
    goto label_14;
    if (edi == 0x4f) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    r9d = 0;
    *((rsp + 0x50)) = 1;
    eax = *((rax + 0x10));
    *((rsp + 0x40)) = 0;
    edx = rax * 5;
    edx = rax + rdx*2;
    edx >>= 5;
    edx++;
label_25:
    rdi = rsp + 0xc7;
    r8 = rdi;
label_22:
    rsi = rdi;
    if ((r9b & 1) != 0) {
        *((r8 - 1)) = 0x3a;
        rsi--;
    }
    eax = edx;
    ecx = edx;
    r9d >>= 1;
    r8 = rsi - 1;
    rax *= rbp;
    rax >>= 0x23;
    r11d = rax * 5;
    r11d += r11d;
    ecx -= r11d;
    ecx += 0x30;
    *((rsi - 1)) = cl;
    if (edx > 9) {
        goto label_115;
    }
    if (r9d == 0) {
        goto label_21;
    }
label_115:
    edx = eax;
    goto label_22;
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 1;
    eax = *((rax + 0x18));
    edx = rax + 6;
    *((rsp + 0x40)) = eax;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    edx -= ecx;
    edx++;
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    edx = *((rsp + 0x60));
label_27:
    eax = 0x5f;
    *((rsp + 0x50)) = 2;
    if (r10d == 0) {
        r10d = eax;
    }
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 2;
    eax = *((rax + 0x10));
    edx = rax + 1;
    rsp + 0x40 = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r9d = 0;
    goto label_13;
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_116;
    }
    if (r15d < 0) {
        goto label_116;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_23;
    }
    if (r15d > 1) {
        rdx--;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_117;
        }
        if (r10d == 0x2b) {
            goto label_117;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
    }
label_46:
    *(r14) = 0xa;
    r14++;
    goto label_23;
    if (edi == 0x45) {
        goto label_8;
    }
    rcx = *((rsp + 0x20));
    eax = *((rcx + 0x1c));
    eax -= *((rcx + 0x18));
    eax += 7;
    goto label_24;
    if (edi == 0x45) {
        goto label_17;
    }
    if (edi == 0x4f) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 0x14));
    rsp + 0x40 = (edx < 0xfffff894) ? 1 : 0;
    edx += 0x76c;
    if (r10d != 0) {
        goto label_55;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_118;
    }
    *((rsp + 0x58)) = 0;
    r9d = 0;
    *((rsp + 0x50)) = 4;
label_93:
    eax = edx;
    eax = -eax;
    if (*((rsp + 0x40)) != 0) {
        edx = eax;
    }
    goto label_25;
    edi = *((rsp + 0x38));
    eax = 0;
    *((rsp + 0x50)) = r10d;
    *((rsp + 0x40)) = r8b;
    if (r8b != 0) {
        edi = eax;
    }
    *((rsp + 0x38)) = dil;
    rax = strlen (r12);
    r10d = *((rsp + 0x50));
    r8d = *((rsp + 0x40));
    if (r10d == 0x2d) {
        goto label_119;
    }
    if (r15d < 0) {
        goto label_119;
    }
    r15 = (int64_t) r15d;
    rax = r15;
    if (rax >= r15) {
        rax = rbp;
    }
    *((rsp + 0x40)) = rax;
label_47:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= *((rsp + 0x40))) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_120;
    }
    if (rbp < r15) {
        rdx = r15;
        *((rsp + 0x50)) = r8b;
        rdx -= rbp;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_121;
        }
        if (r10d == 0x2b) {
            goto label_121;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
        r8d = *((rsp + 0x50));
    }
label_59:
    if (r8b != 0) {
        goto label_122;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_123;
    }
    r15 = rbp - 1;
    if (rbp == 0) {
        goto label_57;
    }
    rax = ctype_toupper_loc ();
    rsi = rax;
    do {
        edx = *((r12 + r15));
        rax = *(rsi);
        eax = *((rax + rdx*4));
        *((r14 + r15)) = al;
        r15--;
    } while (r15 >= 0);
label_57:
    r14 += rbp;
label_120:
    r13 += *((rsp + 0x40));
    goto label_0;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    rdx = *((rax + 0x14));
    rax = rdx;
    rdx *= 0x51eb851f;
    ecx = eax;
    ecx >>= 0x1f;
    rdx >>= 0x25;
    edx -= ecx;
    ecx = edx * 0x64;
    edx = eax;
    edx -= ecx;
    if (edx < 0) {
        ecx = edx;
        edx += 0x64;
        ecx = -ecx;
        if (eax > 0xfffff893) {
            edx = ecx;
            goto label_124;
        }
    }
label_124:
    if (r10d != 0) {
        goto label_61;
    }
label_50:
    eax = *((rsp + 0x34));
    if (eax == 0x2b) {
        goto label_92;
    }
    r10d = eax;
    goto label_26;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 0xc));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 0xc));
    goto label_27;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 3;
    eax = *((rax + 0x1c));
    edx = rax + 1;
    rsp + 0x40 = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r9d = 0;
    goto label_13;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 8));
    goto label_27;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    r11d = 1;
    if (al == 0x3a) {
        goto label_30;
    }
label_29:
    if (al != 0x7a) {
        goto label_8;
    }
    rbx = rdx;
label_28:
    rax = *((rsp + 0x20));
    r8d = *((rax + 0x20));
    if (r8d < 0) {
        goto label_0;
    }
    rdx = *((rax + 0x28));
    *((rsp + 0x40)) = 1;
    if (edx >= 0) {
        *((rsp + 0x40)) = 0;
        if (edx != 0) {
            goto label_125;
        }
        rsp + 0x40 = (*(r12) == 0x2d) ? 1 : 0;
    }
label_125:
    rax = (int64_t) edx;
    ecx = edx;
    r8 = rax * 0xffffffff91a2b3c5;
    ecx >>= 0x1f;
    rax *= 0xffffffff88888889;
    r8 >>= 0x20;
    rax >>= 0x20;
    r8d += edx;
    eax += edx;
    r8d >>= 0xb;
    eax >>= 5;
    r8d -= ecx;
    eax -= ecx;
    r9 = (int64_t) eax;
    ecx = eax;
    r9 *= 0xffffffff88888889;
    ecx >>= 0x1f;
    r9 >>= 0x20;
    r9d += eax;
    r9d >>= 5;
    r9d -= ecx;
    ecx = r9d * 0x3c;
    r9d = eax;
    eax *= 0x3c;
    r9d -= ecx;
    ecx = edx;
    ecx -= eax;
    if (r11 == 2) {
        goto label_126;
    }
    if (r11 > 2) {
        goto label_127;
    }
    if (r11 == 0) {
        goto label_128;
    }
label_44:
    edx = r8d * 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    *((rsp + 0x50)) = 6;
    eax ^= 1;
    edx += r9d;
    r9d = 4;
    goto label_13;
    r11d = 0;
    goto label_28;
    ebp = 0;
    do {
        ecx = *((rsp + 0x38));
        eax = 0;
        esi = 0x70;
        if (r8b != 0) {
        }
        if (r8b != 0) {
            ecx = eax;
        }
        *((rsp + 0x38)) = cl;
        goto label_16;
        *((rsp + 0x40)) = 0xffffffff;
        r11 = "%H:%M";
        goto label_20;
    } while (1);
    *((rsp + 0x40)) = 0xffffffff;
    r11 = 0x00012a4f;
    goto label_20;
label_30:
    r11++;
    eax = *((rbx + r11));
    rdx = rbx + r11;
    if (al != 0x3a) {
        goto label_29;
    }
    goto label_30;
label_83:
    r15 = r8;
    edx = 0;
    goto label_31;
label_102:
    ecx = 0x2d;
label_40:
    esi = r15 - 1;
    edx = esi;
    edx -= ebp;
    if (edx <= 0) {
        goto label_129;
    }
    if (al == 0) {
        goto label_129;
    }
label_35:
    if (r10d == 0x5f) {
        goto label_130;
    }
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
label_42:
        *(r14) = cl;
        r14++;
    }
    r13++;
    rcx = (int64_t) ebp;
    if (r10d == 0x2d) {
        goto label_131;
    }
label_48:
    r15 = rcx;
    edx = 0;
    if (esi < 0) {
        goto label_32;
    }
    r15d = esi;
    goto label_33;
label_101:
    eax = 1;
    r10d = 0x30;
    goto label_34;
label_129:
    edx = 0;
    goto label_35;
label_104:
    if (r10d != 0x2d) {
        goto label_33;
    }
label_131:
    r15 = rcx;
    edx = 0;
    goto label_32;
label_106:
    rdx = rcx;
    *((rsp + 0x38)) = rcx;
    eax = memcpy (r14, r8, rdx);
    rcx = *((rsp + 0x38));
    goto label_36;
label_71:
    *((rsp + 0x38)) = 1;
    goto label_7;
label_72:
    r8d = eax;
    goto label_7;
label_77:
    r14 = r15;
    memset (r14, 0x30, rdx);
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x50));
    r9 = *((rsp + 0x58));
    goto label_37;
label_86:
    rdx = r8;
    *((rsp + 0x38)) = r8;
    memcpy (r14, rsp + 0xb1, rdx);
    r8 = *((rsp + 0x38));
    goto label_38;
label_85:
    rbp = rcx - 2;
    if (r8 == 0) {
        goto label_38;
    }
    *((rsp + 0x38)) = r8;
    rax = ctype_tolower_loc ();
    r8 = *((rsp + 0x38));
    rsi = rsp + 0xb1;
    do {
        ecx = *((rsi + rbp));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
    goto label_38;
label_112:
    *((rsp + 0x38)) = rbp;
    edx = 0;
    goto label_39;
label_103:
    ecx = 0x2b;
    goto label_40;
label_84:
    memset (r14, 0x30, rdx);
    r14 = *((rsp + 0x40));
    rcx = *((rsp + 0x50));
    r8 = *((rsp + 0x58));
    goto label_41;
label_130:
    r9d = r15d;
    r15 = *((rsp + 8));
    r9d -= edx;
    rdx = (int64_t) edx;
    r13 += rdx;
    r15 -= r13;
    if (r14 == 0) {
        goto label_132;
    }
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x68)) = cl;
    *((rsp + 0x64)) = r9d;
    *((rsp + 0x58)) = r10d;
    *((rsp + 0x50)) = r8;
    memset (r14, 0x20, rdx);
    rdx = *((rsp + 0x40));
    r14 += rdx;
    if (r15 <= 1) {
        goto label_9;
    }
    r9d = *((rsp + 0x64));
    r8 = *((rsp + 0x50));
    r10d = *((rsp + 0x58));
    ecx = *((rsp + 0x68));
    esi = r9 - 1;
    goto label_42;
label_105:
    r14 = rbp;
    memset (r14, 0x30, rdx);
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x50));
    goto label_43;
label_127:
    if (r11 != 3) {
        goto label_8;
    }
    if (ecx != 0) {
        goto label_126;
    }
    if (r9d != 0) {
        goto label_44;
    }
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    edx = r8d;
    *((rsp + 0x50)) = 3;
    eax ^= 1;
    goto label_13;
label_108:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_45;
    }
    goto label_23;
label_116:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_46;
    }
    goto label_23;
label_119:
    *((rsp + 0x40)) = rbp;
    r15d = 0;
    goto label_47;
label_132:
    if (r15 <= 1) {
        goto label_9;
    }
    r13++;
    esi = r9 - 1;
    rcx = (int64_t) ebp;
    goto label_48;
label_113:
    memset (r14, 0x30, rdx);
    r14 = *((rsp + 0x68));
    r8d = *((rsp + 0x50));
    r11 = *((rsp + 0x58));
    r10d = *((rsp + 0x64));
    goto label_49;
label_107:
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_133;
    }
    if (r15d < 0) {
        goto label_133;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_23;
    }
    r15d--;
    if (r15d > 0) {
        rdx--;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_134;
        }
        if (r10d == 0x2b) {
            goto label_134;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
    }
label_56:
    eax = *(rbx);
    r14++;
    *((r14 - 1)) = al;
    goto label_23;
label_91:
    r8d = 0xfffff894;
    r8d -= eax;
    if (ecx >= r8d) {
        goto label_135;
    }
    edx = -edx;
    if (r10d == 0) {
        goto label_50;
    }
label_61:
    if (r10d != 0x2b) {
        goto label_26;
    }
label_92:
    *((rsp + 0x40)) = 0;
    *((rsp + 0x50)) = 2;
    do {
        eax = *((rsp + 0x40));
        r10d = 0x2b;
        rsp + 0x58 = (r15d > *((rsp + 0x50))) ? 1 : 0;
        eax ^= 1;
        goto label_18;
label_94:
        esi = 1;
        edi = 1;
        goto label_51;
label_55:
        *((rsp + 0x50)) = 4;
        eax = 0x270f;
        if (r10d != 0x2b) {
            goto label_136;
        }
label_54:
    } while (eax >= edx);
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    r10d = 0x2b;
    eax ^= 1;
    goto label_18;
label_87:
    r9d--;
    eax = 0x16d;
    if ((r9b & 3) == 0) {
        edx = r9d * 0xc28f5c29;
        eax = 0x16e;
        edx += 0x51eb850;
        edx = rotate_right32 (edx, 2);
        if (edx > 0x28f5c28) {
            goto label_137;
        }
        eax = r9d;
        r8d = 0x190;
        edx:eax = (int64_t) eax;
        eax = edx:eax / r8d;
        edx = edx:eax % r8d;
        edx = -edx;
        eax -= eax;
        eax += 0x16e;
    }
label_137:
    eax += r11d;
    r8d = eax;
    r8d -= ebp;
    r8d += 0x17e;
    rdx = (int64_t) r8d;
    r9d = r8d;
    eax -= r8d;
    rdx *= 0xffffffff92492493;
    r9d >>= 0x1f;
    rdx >>= 0x20;
    edx += r8d;
    edx >>= 2;
    edx -= r9d;
    r9d = rdx*8;
    r9d -= edx;
    r8d = rax + r9 + 3;
    eax = 0xffffffff;
    goto label_52;
label_110:
    if (r10d != 0x2b) {
        goto label_53;
    }
label_111:
    *((rsp + 0x50)) = 2;
    eax = 0x63;
    goto label_54;
label_90:
    rdx = (int64_t) r8d;
    *((rsp + 0x50)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += r8d;
    r8d >>= 0x1f;
    edx >>= 2;
    edx -= r8d;
    edx++;
    goto label_14;
label_89:
    r8d = 0xfffff894;
    edx = rcx + rax + 0x76c;
    r8d -= eax;
    rsp + 0x40 = (ecx < r8d) ? 1 : 0;
    if (r10d != 0) {
        goto label_55;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_118;
    }
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 4;
    al = (ecx >= r8d) ? 1 : 0;
    goto label_18;
label_133:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_56;
    }
    goto label_23;
label_123:
    memcpy (r14, r12, rbp);
    goto label_57;
label_122:
    r15 = rbp - 1;
    if (rbp == 0) {
        goto label_57;
    }
    rax = ctype_tolower_loc ();
    rsi = rax;
    do {
        edx = *((r12 + r15));
        rax = *(rsi);
        eax = *((rax + rdx*4));
        *((r14 + r15)) = al;
        r15--;
    } while (r15 >= 0);
    goto label_57;
label_126:
    r9d *= 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    edx = r8d * 0x2710;
    *((rsp + 0x50)) = 9;
    eax ^= 1;
    edx += r9d;
    r9d = 0x14;
    edx += ecx;
    goto label_13;
label_117:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_46;
label_97:
    rdi = rsp + 0xb0;
    if (rsi >= 8) {
        goto label_138;
    }
    if ((sil & 4) != 0) {
        goto label_139;
    }
    if (rsi == 0) {
        goto label_58;
    }
    eax = *((rsp + 0xb0));
    *(r14) = al;
    if ((sil & 2) == 0) {
        goto label_58;
    }
    eax = *((rsp + rsi + 0xae));
    *((r14 + rsi - 2)) = ax;
    goto label_58;
label_109:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_45;
label_128:
    edx = r8d * 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    *((rsp + 0x50)) = 5;
    eax ^= 1;
    edx += r9d;
    r9d = 0;
    goto label_13;
label_121:
    r14 = r15;
    memset (r14, 0x30, rdx);
    r8d = *((rsp + 0x50));
    goto label_59;
label_118:
    *((rsp + 0x50)) = 4;
    eax = 0x270f;
    goto label_54;
label_114:
    r8d = *((rsp + 0x38));
    r15 = "%Y-%m-%d";
    rsi |= 0xffffffffffffffff;
    eax = *((rsp + 0x518));
    rdx = r15;
    r9d = 0x2b;
    edi = 0;
    rcx = *((rsp + 0x40));
    *((rsp + 0x70)) = r8d;
    rax = _strftime_internal_isra_0 ();
    goto label_19;
    r11 = r15;
    edx = 0;
    r10d = 0x2b;
    r8d = *((rsp + 0x50));
    *((rsp + 0x38)) = rax;
    *((rsp + 0x40)) = 4;
    goto label_39;
label_99:
    r14 = rbp;
    memset (r14, 0x30, rdx);
    goto label_0;
label_138:
    rax = *((rsp + 0xb0));
    rdx = r14 + 8;
    rdx &= 0xfffffffffffffff8;
    *(r14) = rax;
    rax = *((rsp + rsi + 0xa8));
    *((r14 + rsi - 8)) = rax;
    rax = r14;
    rax -= rdx;
    rdi -= rax;
    rax += rsi;
    rax &= 0xfffffffffffffff8;
    if (rax < 8) {
        goto label_58;
    }
    rax &= 0xfffffffffffffff8;
    ecx = 0;
    do {
        r8 = *((rdi + rcx));
        *((rdx + rcx)) = r8;
        rcx += 8;
    } while (rcx < rax);
    goto label_58;
label_134:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_56;
label_95:
    esi = 0;
    goto label_60;
label_98:
    r13 += rdx;
    goto label_0;
label_69:
    stack_chk_fail ();
label_135:
    edx += 0x64;
    if (r10d == 0) {
        goto label_50;
    }
    goto label_61;
label_136:
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 0;
    eax ^= 1;
    goto label_18;
label_100:
    rax = *((rsp + 0x10));
    *(rax) = 0x4b;
    goto label_62;
label_139:
    eax = *((rsp + 0xb0));
    *(r14) = eax;
    eax = *((rsp + rsi + 0xac));
    *((r14 + rsi - 4)) = eax;
    goto label_58;
}

/* /tmp/tmpyosj1dz2 @ 0x11d30 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpyosj1dz2 @ 0xb9a0 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0xbcd0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x35e0 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpyosj1dz2 @ 0xe040 */
 
void dbg_fseterr (FILE * fp) {
    rdi = fp;
    /* void fseterr(FILE * fp); */
    *(rdi) |= 0x20;
}

/* /tmp/tmpyosj1dz2 @ 0xcd20 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xb7e0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x3510 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpyosj1dz2 @ 0xcf20 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xd460 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x00012a08);
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x37c0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpyosj1dz2 @ 0x3550 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpyosj1dz2 @ 0xdd80 */
 
int64_t dbg_rpl_vfprintf (int64_t arg1, int64_t arg2, int64_t arg3) {
    size_t lenbuf;
    char[2000] buf;
    int64_t var_ch;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_7f8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_vfprintf(FILE * fp,char const * format,__va_list_tag * args); */
    r8 = rsi;
    rcx = rdx;
    rdx = r8;
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x7f8)) = rax;
    eax = 0;
    r13 = rsp + 0x20;
    rsi = rsp + 0x18;
    *((rsp + 0x18)) = 0x7d0;
    rdi = r13;
    rax = vasnprintf ();
    rbx = *((rsp + 0x18));
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = fwrite (rdi, 1, rbx, r12);
    if (rax < rbx) {
        goto label_2;
    }
    if (rbp != r13) {
        free (rbp);
    }
    if (rbx > 0x7fffffff) {
        goto label_3;
    }
    eax = ebx;
    do {
label_0:
        rdx = *((rsp + 0x7f8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_4;
        }
        return rax;
label_2:
        eax = 0xffffffff;
    } while (rbp == r13);
    *((rsp + 0xc)) = eax;
    free (rbp);
    eax = *((rsp + 0xc));
    goto label_0;
label_3:
    errno_location ();
    *(rax) = 0x4b;
label_1:
    rdi = r12;
    fseterr ();
    eax = 0xffffffff;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xb700 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xe090 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xb9f0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x38f0)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xb760 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x9ea0 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpyosj1dz2 @ 0x3650 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpyosj1dz2 @ 0x3870 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpyosj1dz2 @ 0x4940 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x3800)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmpyosj1dz2 @ 0xc1f0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0xbf30 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x3909)() ();
    }
    if (rdx == 0) {
        void (*0x3909)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xbfd0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x390e)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x390e)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xbb10 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x38fa)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x5c40 */
 
int64_t dbg_yyparse (int64_t arg1, int64_t arg10, int64_t arg11, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    YYSTYPE[20] yyvsa;
    yy_state_t[20] yyssa;
    char[20] buff;
    char * var_8h;
    char * s1;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    size_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    uint32_t var_78h;
    int64_t var_7ch;
    int64_t var_80h;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_b0h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_540h;
    int64_t var_553h;
    char * var_560h;
    uint32_t var_563h;
    int64_t var_573h;
    int64_t var_578h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* int yyparse(parser_control * pc); */
    r9d = 0xfffffffe;
    r14d = 0;
    r15d = r9d;
    r9d = r14d;
    r12d = 0xfffffff2;
    ecx = (int32_t) r12b;
    rbp = obj_yypact;
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x578)) = rax;
    eax = 0;
    *((rsp + 0x540)) = 0;
    r11 = rsp + 0x540;
    rbx = rsp + 0xe0;
    *((rsp + 0x78)) = 0;
    r13 = r11;
    r14 = r11;
label_1:
    r10d = ecx;
    if (ecx != 0xffffffa5) {
        if (r15d == 0xfffffffe) {
            goto label_52;
        }
label_19:
        if (r15d <= 0) {
            goto label_12;
        }
        if (r15d == 0x100) {
            goto label_53;
        }
        if (r15d <= 0x115) {
            goto label_40;
        }
        r10d = rcx + 2;
        edx = 2;
label_2:
        if (r10d > 0x72) {
            goto label_54;
        }
        r10 = (int64_t) r10d;
        rax = obj_yycheck;
        eax = *((rax + r10));
        if (eax != edx) {
            goto label_54;
        }
        rax = "PDEFGHIfJ";
        r9d = *((rax + r10));
        eax = r9d;
        if (r9d <= 0) {
            goto label_55;
        }
        edi = *((rsp + 0x78));
        rdx = *((rsp + 0xd0));
        r15d = 0xfffffffe;
        __asm ("movdqa xmm2, xmmword [rsp + 0xc0]");
        *((rbx + 0x68)) = rdx;
        rdx = r13;
        edi += 0xffffffff;
        __asm ("movups xmmword [rbx + 0x58], xmm2");
        rbx += 0x38;
        *((rsp + 0x78)) = edi;
        rdi = *((rsp + 0x38));
        *((rsp + 0xa8)) = rdi;
        rdi = *((rsp + 0x40));
        __asm ("movdqa xmm0, xmmword [rsp + 0xa0]");
        *((rsp + 0xb0)) = rdi;
        __asm ("movdqa xmm1, xmmword [rsp + 0xb0]");
        __asm ("movups xmmword [rbx], xmm0");
        __asm ("movups xmmword [rbx + 0x10], xmm1");
        goto label_11;
    }
label_54:
    rdx = (int64_t) r9d;
    rax = obj_yydefact;
    eax = *((rax + rdx));
    if (eax != 0) {
        goto label_56;
    }
    if (*((rsp + 0x78)) == 3) {
        if (r15d > 0) {
            goto label_57;
        }
        if (r15d == 0) {
            goto label_21;
        }
    }
    ecx = *((rbp + rdx));
    while (ecx == 0xffffffa5) {
label_0:
        if (r13 == r14) {
            goto label_21;
        }
        rax = *((r13 - 1));
        r13--;
        rbx -= 0x38;
        ecx = *((rbp + rax));
label_3:
    }
    ecx++;
    if (ecx > 0x72) {
        goto label_0;
    }
    rcx = (int64_t) ecx;
    rax = obj_yycheck;
    if (*((rax + rcx)) != 1) {
        goto label_0;
    }
    rax = "PDEFGHIfJ";
    r9d = *((rax + rcx));
    eax = r9d;
    if (r9d <= 0) {
        goto label_0;
    }
    rdi = *((rsp + 0x38));
    rdx = *((rsp + 0xd0));
    rbx += 0x38;
    *((rsp + 0x78)) = 3;
    __asm ("movdqa xmm5, xmmword [rsp + 0xc0]");
    *((rsp + 0xa8)) = rdi;
    rdi = *((rsp + 0x40));
    __asm ("movdqa xmm3, xmmword [rsp + 0xa0]");
    *((rbx + 0x30)) = rdx;
    rdx = r13;
    *((rsp + 0xb0)) = rdi;
    __asm ("movdqa xmm4, xmmword [rsp + 0xb0]");
    __asm ("movups xmmword [rbx], xmm3");
    __asm ("movups xmmword [rbx + 0x10], xmm4");
    __asm ("movups xmmword [rbx + 0x20], xmm5");
    goto label_11;
label_55:
    eax = -eax;
label_56:
    r10 = (int64_t) eax;
    rdx = obj_yyr2;
    eax -= 4;
    rcx = *((rdx + r10));
    edx = 1;
    edx -= ecx;
    r9 = rcx;
    rdx = (int64_t) edx;
    rcx = rdx*8;
    rcx -= rdx;
    rdx = rbx + rcx*8;
    rdi = *((rdx + 8));
    r11 = *(rdx);
    *((rsp + 8)) = rdi;
    rdi = *((rdx + 0x10));
    *((rsp + 0x28)) = rdi;
    rdi = *((rdx + 0x18));
    *((rsp + 0x10)) = rdi;
    rdi = *((rdx + 0x20));
    *((rsp + 0x20)) = rdi;
    rdi = *((rdx + 0x28));
    *((rsp + 0x18)) = rdi;
    edi = *((rdx + 0x30));
    *((rsp + 0x30)) = edi;
    if (eax <= 0x58) {
        rdx = 0x00013840;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (89 cases) at 0x13840 */
        void (*rax)() ();
        rax = *((rbx - 0x38));
        *((rsp + 0x18)) = rax;
        eax = *((rbx - 0x30));
        *((rsp + 0x30)) = eax;
label_22:
        *((rsp + 0x20)) = 0;
        r11d = 0;
        *((rsp + 0x10)) = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 8)) = 0;
    }
label_20:
    rdx = r9*8;
    rdi = *((rsp + 8));
    rax = rbx;
    rcx = obj_yypgoto;
    rdx -= r9;
    rdx <<= 3;
    rax -= rdx;
    rdx = r13;
    *((rax + 0x40)) = rdi;
    rdi = *((rsp + 0x28));
    rbx = rax + 0x38;
    rdx -= r9;
    *((rax + 0x38)) = r11;
    *((rax + 0x48)) = rdi;
    rdi = *((rsp + 0x10));
    *((rax + 0x50)) = rdi;
    rdi = *((rsp + 0x20));
    *((rax + 0x58)) = rdi;
    rdi = *((rsp + 0x18));
    *((rax + 0x60)) = rdi;
    edi = *((rsp + 0x30));
    *((rax + 0x68)) = edi;
    rax = obj_yyr1;
    edi = *(rdx);
    eax = *((rax + r10));
    esi = edi;
    eax -= 0x1d;
    rax = (int64_t) eax;
    ecx = *((rcx + rax));
    ecx += edi;
    if (ecx <= 0x72) {
        rcx = (int64_t) ecx;
        rdi = obj_yycheck;
        if (sil == *((rdi + rcx))) {
            goto label_58;
        }
    }
    rcx = obj_yydefgoto;
    r9d = *((rcx + rax));
    eax = r9d;
label_11:
    *((rdx + 1)) = al;
    r13 = rdx + 1;
    rax = rsp + 0x553;
    if (r13 >= rax) {
        goto label_59;
    }
    if (r9d == 0xc) {
        goto label_60;
    }
    rax = (int64_t) r9d;
    ecx = *((rbp + rax));
    goto label_1;
label_12:
    edx = 0;
    r15d = 0;
    goto label_2;
    r8 = *((rbx - 0x10));
    rax = *((rbx - 0x38));
    rdi = *((rbx - 0x30));
    rsi = *((rbx - 0x28));
    rdx = *((rbx - 0x20));
    rcx = *((rbx - 0x18));
    *((rsp + 0x70)) = r8;
    r8 = *((r12 + 0x70));
    *((rsp + 0x48)) = rax;
    eax = *((rbx - 8));
    *((rsp + 0x50)) = rdi;
    *((rsp + 0x88)) = r8;
    r8 = *((r12 + 0x68));
    *((rsp + 0x58)) = rsi;
    edi = *((r12 + 0x98));
    *((rsp + 0x80)) = r8;
    r8d = *(rbx);
    *((rsp + 0x60)) = rdx;
    rsi = *((r12 + 0x90));
    *((rsp + 0x68)) = rcx;
    rdx = *((r12 + 0x80));
    *((rsp + 0x7c)) = eax;
    rcx = *((r12 + 0x88));
    rax = *((r12 + 0x78));
    if (r8d < 0) {
        goto label_61;
    }
    r8d = 0;
    edi += *((rsp + 0x7c));
    __asm ("seto r8b");
    *((rsp + 0x7c)) = r8d;
    r8d = 0;
    rsi += *((rsp + 0x70));
    __asm ("seto r8b");
    *((rsp + 0x70)) = r8;
    r8d = 0;
    rcx += *((rsp + 0x68));
    __asm ("seto r8b");
    *((rsp + 0x68)) = r8;
    r8d = 0;
    rdx += *((rsp + 0x60));
    __asm ("seto r8b");
    *((rsp + 0x60)) = rdx;
    *((rsp + 0x90)) = r8;
    r8d = 0;
    rax += *((rsp + 0x58));
    __asm ("seto r8b");
    *((rsp + 0x58)) = rax;
    eax = 0;
    *((rsp + 0x98)) = r8;
    r8 = *((rsp + 0x88));
    r8 += *((rsp + 0x50));
    __asm ("seto al");
    *((rsp + 0x50)) = r8;
    edx = 0;
    r8 = *((rsp + 0x80));
    r8 += *((rsp + 0x48));
    __asm ("seto dl");
    *((rsp + 0x48)) = rdx;
    rdx = r8;
    r8d = *((rsp + 0x70));
    r8b |= *((rsp + 0x7c));
    r8b |= *((rsp + 0x68));
    r8b |= *((rsp + 0x90));
    r8b |= *((rsp + 0x98));
    r8d |= eax;
    r8b |= *((rsp + 0x48));
label_39:
    rax = *((rsp + 0x60));
    *((r12 + 0x98)) = edi;
    *((r12 + 0x90)) = rsi;
    *((r12 + 0x80)) = rax;
    rax = *((rsp + 0x58));
    *((r12 + 0x88)) = rcx;
    *((r12 + 0x78)) = rax;
    rax = *((rsp + 0x50));
    *((r12 + 0x68)) = rdx;
    *((r12 + 0x70)) = rax;
    if (r8b == 0) {
        goto label_62;
    }
label_21:
    eax = 1;
label_37:
    rdx = *((rsp + 0x578));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_63;
    }
    return rax;
label_57:
    ecx = *((rbp + rdx));
    r15d = 0xfffffffe;
    goto label_3;
label_52:
    rsi = *(r12);
    edx = *(rsi);
label_4:
    if (dl > 0xd) {
        goto label_64;
    }
    if (dl > 8) {
        goto label_65;
    }
label_5:
    eax = rdx - 0x2b;
    edi = edx;
    eax &= 0xfffffffd;
    r8d = eax;
    eax = (int32_t) dl;
    eax -= 0x30;
    if (eax <= 9) {
        goto label_66;
    }
    if (r8b != 0) {
        goto label_67;
    }
label_8:
    eax = 0;
    al = (dl != 0x2d) ? 1 : 0;
    eax = rax + rax - 1;
    do {
label_6:
        rsi++;
        *(r12) = rsi;
        edx = *(rsi);
        if (dl > 0xd) {
            goto label_68;
        }
    } while (dl > 8);
label_7:
    edi = (int32_t) dl;
    edi -= 0x30;
    if (edi > 9) {
        goto label_4;
    }
label_9:
    r8d = 0;
    r15d = 0x30;
label_10:
    edi = 0x30;
    edi -= edx;
    edx -= 0x30;
    if (eax == 0xffffffff) {
        edx = edi;
    }
    rdx = (int64_t) edx;
    r8 += rdx;
    if (r8 overflow 0) {
        goto label_69;
    }
    edx = *((rsi + 1));
    r10 = rsi + 1;
    r11d = rdx - 0x30;
    edi = edx;
    if (r11d <= 9) {
        goto label_70;
    }
    edi &= 0xfffffffd;
    if (dil != 0x2c) {
        goto label_71;
    }
    edx = *((r10 + 1));
    edx -= 0x30;
    if (edx > 9) {
        goto label_71;
    }
    r11d = *((rsi + 3));
    r10 = rsi + 3;
    edi = 8;
    r15d = r11d;
    do {
        edx = rdx * 5;
        esi = r11 - 0x30;
        edx += edx;
        if (esi <= 9) {
            r11d = *((r10 + 1));
            edx += esi;
            r10++;
            r15d = r11d;
            esi = r11 - 0x30;
        }
        edi--;
    } while (edi != 0);
    if (eax == 0xffffffff) {
        goto label_72;
    }
label_44:
    if (esi > 9) {
        goto label_73;
    }
    do {
        esi = *((r10 + 1));
        r10++;
        esi -= 0x30;
    } while (esi <= 9);
label_73:
    if (eax >= 0) {
        goto label_74;
    }
    if (edx == 0) {
        goto label_74;
    }
label_23:
    r8 += 0xffffffffffffffff;
    if (r8 overflow 0) {
        goto label_46;
    }
    eax = 0x3b9aca00;
    *(r12) = r10;
    r15d = 0x114;
    eax -= edx;
    *((rsp + 0xa0)) = r8;
    edx = 0x15;
    rax = (int64_t) eax;
    *((rsp + 0x38)) = rax;
    goto label_15;
label_64:
    if (dl != 0x20) {
        goto label_5;
    }
label_65:
    rsi++;
    *(r12) = rsi;
    edx = *(rsi);
    goto label_4;
label_68:
    if (dl == 0x20) {
        goto label_6;
    }
    goto label_7;
label_66:
    if (r8b == 0) {
        goto label_8;
    }
    eax = 0;
    goto label_9;
    do {
label_69:
        r15d = 0x3f;
label_40:
        rax = (int64_t) r15d;
        rdx = obj_yytranslate;
        edx = *((rdx + rax));
label_15:
        r10d = rcx + rdx;
        goto label_2;
label_70:
        r8 *= 0xa;
    } while (r8b overflow 0);
    rsi = r10;
    goto label_10;
label_58:
    rax = "PDEFGHIfJ";
    r9d = *((rax + rcx));
    eax = r9d;
    goto label_11;
label_67:
    if (dl > 0x5a) {
        goto label_75;
    }
    if (dl > 0x40) {
        goto label_76;
    }
label_16:
    if (dl != 0x28) {
        goto label_77;
    }
    edx = 0;
    goto label_78;
label_13:
    al = (al == 0x29) ? 1 : 0;
    eax = (int32_t) al;
    rdx -= rax;
label_14:
    if (rdx == 0) {
        goto label_79;
    }
label_78:
    rdi = rsi;
    rsi++;
    *(r12) = rsi;
    eax = *((rsi - 1));
    if (al == 0) {
        goto label_12;
    }
    if (al != 0x28) {
        goto label_13;
    }
    rdx++;
    goto label_14;
label_71:
    rdi = r10;
    rdi -= *(r12);
    edx = eax;
    *(r12) = r10;
    *((rsp + 0x40)) = rdi;
    edi = eax;
    edx >>= 0x1f;
    edi = -edi;
    *((rsp + 0xa0)) = dl;
    edx -= edx;
    *((rsp + 0x38)) = r8;
    edx += 0x14;
    eax = -eax;
    r15d -= r15d;
    r15d += 0x113;
    goto label_15;
label_75:
    eax = rdx - 0x61;
    if (al > 0x19) {
        goto label_16;
    }
label_76:
    r11 = rsp + 0x560;
    rsi++;
    r8 = rsp + 0x573;
    rax = r11;
    while (dil <= 0x5a) {
        if (dil <= 0x40) {
label_18:
            if (dil != 0x2e) {
                goto label_80;
            }
        }
label_17:
        rsi++;
        if (rax < r8) {
            *(rax) = dil;
            rax++;
        }
        *(r12) = rsi;
        edi = *(rsi);
    }
    edx = rdi - 0x61;
    if (dl <= 0x19) {
        goto label_17;
    }
    goto label_18;
label_53:
    r15d = 0x101;
    goto label_3;
label_80:
    *(rax) = 0;
    eax = *((rsp + 0x560));
    rsi = r11;
    if (al == 0) {
        goto label_81;
    }
    do {
        edx = (int32_t) al;
        edi = rdx - 0x61;
        edx -= 0x20;
        if (edi < 0x1a) {
            eax = edx;
        }
        rsi++;
        *((rsi - 1)) = al;
        eax = *(rsi);
    } while (al != 0);
label_81:
    r15 = obj_meridian_table;
    *((rsp + 0x20)) = ecx;
    rsi = 0x00012b2e;
    *((rsp + 0x10)) = rbx;
    rbx = r15;
    r15 = r11;
    *((rsp + 8)) = r9d;
    *((rsp + 0x18)) = r10d;
    while (eax != 0) {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_82;
        }
        eax = strcmp (r15, rsi);
    }
    r15 = rbx;
    r9d = *((rsp + 8));
    r10d = *((rsp + 0x18));
    rbx = *((rsp + 0x10));
    ecx = *((rsp + 0x20));
label_34:
    rax = *((r15 + 0xc));
    r15d = *((r15 + 8));
label_38:
    *((rsp + 0xa0)) = rax;
    goto label_19;
    rax = *(rbx);
    r11d = 0;
    *((rsp + 0x30)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x28)) = rax;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 8)) = 0;
    goto label_20;
    r11 = *((rbx - 0x30));
label_26:
    *((rsp + 0x30)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 8)) = 0;
    goto label_20;
    eax = *((rbx + 0x30));
    r8d = 0;
    eax += *((r12 + 0x98));
    *((r12 + 0x98)) = eax;
    __asm ("seto r8b");
    rax = *((r12 + 0x90));
    rax += *((rbx + 0x28));
    rdx = rax;
    __asm ("seto al");
    edi = 0;
    *((r12 + 0x90)) = rdx;
    rdx = *((r12 + 0x88));
    eax = (int32_t) al;
    rdx += *((rbx + 0x20));
    __asm ("seto dil");
    *((r12 + 0x88)) = rdx;
    esi = 0;
    rdx = *((r12 + 0x80));
    rdx += *((rbx + 0x18));
    __asm ("seto sil");
    *((r12 + 0x80)) = rdx;
    ecx = 0;
    rdx = *((r12 + 0x78));
    rdx += *((rbx + 0x10));
    __asm ("seto cl");
    *((r12 + 0x78)) = rdx;
    rdx = *((r12 + 0x70));
    *((rsp + 0x48)) = rcx;
    ecx = 0;
    rdx += *((rbx + 8));
    __asm ("seto cl");
    *((r12 + 0x70)) = rdx;
    *((rsp + 0x50)) = rcx;
    rdx = *((r12 + 0x68));
    ecx = 0;
    rdx += *(rbx);
    __asm ("seto cl");
    eax |= r8d;
    *((r12 + 0x68)) = rdx;
    eax |= edi;
    eax |= esi;
    al |= *((rsp + 0x48));
    al |= *((rsp + 0x50));
    if (al != 0) {
        goto label_21;
    }
    if (rcx != 0) {
        goto label_21;
    }
label_62:
    *((r12 + 0xa1)) = 1;
    goto label_20;
    rax = *((rbx - 0x30));
label_28:
    *((rsp + 0x10)) = rax;
    r11d = 0;
    *((rsp + 0x30)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 8)) = 0;
    goto label_20;
    rax = *((rbx - 0x30));
label_30:
    *((rsp + 0x20)) = rax;
    r11d = 0;
    *((rsp + 0x30)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 8)) = 0;
    goto label_20;
    *((rsp + 8)) = 0;
    r11 = *((rbx + 8));
    goto label_20;
    rax = *((rbx - 0x30));
label_27:
    *((rsp + 0x18)) = rax;
    *((rsp + 0x30)) = 0;
    goto label_22;
    rax = *((rbx - 0x30));
label_24:
    *((rsp + 8)) = rax;
    r11d = 0;
    *((rsp + 0x30)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x28)) = 0;
    goto label_20;
    rax = *((rbx - 0x30));
label_25:
    rax *= *(rbx);
    __asm ("seto r11b");
    *((rsp + 0x28)) = rax;
    r11d = (int32_t) r11b;
    if (rcx overflow 0) {
        goto label_21;
    }
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x30)) = 0;
    goto label_20;
label_72:
    if (esi > 9) {
        goto label_83;
    }
    do {
        if (r15b != 0x30) {
            goto label_84;
        }
        esi = *((r10 + 1));
        r10++;
        r15d = esi;
        esi -= 0x30;
    } while (esi <= 9);
label_83:
    if (edx != 0) {
        goto label_23;
    }
    *(r12) = r10;
    edx = 0x15;
    r15d = 0x114;
    *((rsp + 0xa0)) = r8;
    *((rsp + 0x38)) = 0;
    goto label_15;
label_74:
    rdi = (int64_t) edx;
    *((rsp + 0xa0)) = r8;
    *((rsp + 0x38)) = rdi;
    edi = eax;
    edi = -edi;
    *(r12) = r10;
    edx -= edx;
    edx += 0x16;
    eax = -eax;
    r15d -= r15d;
    r15d += 0x115;
    goto label_15;
    eax = *((r12 + 0xe1));
    r8 = *((rbx - 0xd0));
    if (r8 <= 3) {
        goto label_85;
    }
    if (al != 0) {
        goto label_86;
    }
label_41:
    rax = *((rbx - 0xd0));
    __asm ("movdqu xmm7, xmmword [rbx - 0xe0]");
    *((r12 + 0x30)) = rax;
    __asm ("movups xmmword [r12 + 0x20], xmm7");
    rax = *((rbx - 0x68));
    *((r12 + 0x38)) = rax;
    rax = *((rbx + 8));
    *((r12 + 0x40)) = rax;
    goto label_20;
    rax = *((rbx - 0x38));
    goto label_24;
    rax = *((rbx - 0x38));
    goto label_25;
    r11 = *((rbx - 0x38));
    goto label_26;
    *((r12 + 0xd0))++;
    edx = 5;
    *((r12 + 0xa8))++;
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
label_29:
    rax = dcgettext (0, "datetime");
    r9 = *((rsp + 0x48));
    r11 = *((rsp + 0x50));
    r10 = *((rsp + 0x58));
    rdi = rax;
    if (*((r12 + 0xe1)) == 0) {
        goto label_20;
    }
    rsi = r12;
    debug_print_current_time_part_0 ();
    r9 = *((rsp + 0x48));
    r11 = *((rsp + 0x50));
    r10 = *((rsp + 0x58));
    goto label_20;
    rax = *((rbx - 0x38));
    goto label_27;
    *((rsp + 0x30)) = 0;
    r11d = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 1;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 8)) = 0;
    goto label_20;
    *((rsp + 0x30)) = 0;
    r11d = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 1;
    *((rsp + 0x28)) = 0;
    *((rsp + 8)) = 0;
    goto label_20;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x50)) = r9b;
    digits_to_date_time (r12, rsi, rdx, rcx, r8, r9);
    eax = *((rbx + 0x30));
    r8d = 0;
    eax += *((r12 + 0x98));
    *((r12 + 0x98)) = eax;
    __asm ("seto r8b");
    rax = *((r12 + 0x90));
    rax += *((rbx + 0x28));
    rdx = rax;
    __asm ("seto al");
    edi = 0;
    *((r12 + 0x90)) = rdx;
    rdx = *((r12 + 0x88));
    eax = (int32_t) al;
    rdx += *((rbx + 0x20));
    __asm ("seto dil");
    *((r12 + 0x88)) = rdx;
    esi = 0;
    rdx = *((r12 + 0x80));
    rdx += *((rbx + 0x18));
    __asm ("seto sil");
    *((r12 + 0x80)) = rdx;
    r10d = 0;
    rdx = *((r12 + 0x78));
    rdx += *((rbx + 0x10));
    __asm ("seto r10b");
    *((r12 + 0x78)) = rdx;
    r9d = 0;
    rdx = *((r12 + 0x70));
    rdx += *((rbx + 8));
    __asm ("seto r9b");
    *((r12 + 0x70)) = rdx;
    ecx = 0;
    rdx = *((r12 + 0x68));
    rdx += *(rbx);
    __asm ("seto cl");
    eax |= r8d;
    *((r12 + 0x68)) = rdx;
    eax |= edi;
    eax |= esi;
    eax |= r10d;
    al |= r9b;
    if (al != 0) {
        goto label_21;
    }
    if (rcx != 0) {
        goto label_21;
    }
    *((r12 + 0xa1)) = 1;
    r9 = *((rsp + 0x48));
    r10 = *((rsp + 0x50));
    goto label_20;
    rax = *((rbx - 0x38));
    goto label_28;
    r11 = 0xffffffffffffffff;
    goto label_20;
    *((r12 + 0xd0))++;
    edx = 5;
    rsi = 0x000120de;
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    goto label_29;
    rax = *((rbx - 0x38));
    rdx = *((rbx - 0xa0));
    *((r12 + 0x1c)) = 2;
    rcx = *((rbx - 0x110));
    *((r12 + 0x58)) = rax;
    rax = *((rbx - 0x30));
    *((r12 + 0x48)) = rcx;
    *((r12 + 0x50)) = rdx;
    *((r12 + 0x60)) = rax;
    goto label_20;
    rax = *((rbx - 0x30));
    rdx = *((rbx - 0xa0));
    *((r12 + 0x58)) = 0;
    *((r12 + 0x60)) = 0;
    *((r12 + 0x48)) = rdx;
    *((r12 + 0x50)) = rax;
    *((r12 + 0x1c)) = 2;
    goto label_20;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x50)) = r9b;
    digits_to_date_time (r12, rsi, rdx, rcx, r8, r9);
    r9 = *((rsp + 0x48));
    r10 = *((rsp + 0x50));
    goto label_20;
    rax = *((rbx - 0x38));
    goto label_30;
    *((r12 + 0xd8))++;
    edx = 5;
    rsi = 0x00012b84;
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    goto label_29;
    *((r12 + 0xb8))++;
    if (*((r12 + 0xe1)) == 0) {
        goto label_20;
    }
    rsi = r12;
    rdi = 0x00012b89;
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    debug_print_current_time_part_0 ();
    r9 = *((rsp + 0x48));
    r11 = *((rsp + 0x50));
    r10 = *((rsp + 0x58));
    goto label_20;
    *((r12 + 0xc0))++;
    edx = 5;
    rsi = "local_zone";
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    goto label_29;
    *((r12 + 0x50)) = 0;
    rax = *((rbx - 0x30));
    *((r12 + 0x58)) = 0;
    *((r12 + 0x48)) = rax;
    *((r12 + 0x60)) = 0;
    *((r12 + 0x1c)) = 2;
    goto label_20;
    __asm ("movdqu xmm6, xmmword [rbx]");
    *((rsp + 0x58)) = r10;
    edx = 5;
    rsi = "number of seconds";
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    *((r12 + 0xa0)) = 1;
    __asm ("movups xmmword [r12 + 0x58], xmm6");
    goto label_29;
    *((r12 + 0xa8))++;
    edx = 5;
    rsi = "date";
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    goto label_29;
    rax = *((rbx - 0x38));
    rdx = *((rbx - 0xa0));
    rcx = *((rbx - 0x110));
    *((r12 + 0x58)) = rax;
    rax = *((rbx - 0x30));
    *((r12 + 0x48)) = rcx;
    *((r12 + 0x60)) = rax;
    rax = *(rbx);
    *((r12 + 0x50)) = rdx;
    *((r12 + 0x1c)) = eax;
    goto label_20;
    rax = *((rbx - 0x30));
    rdx = *((rbx - 0xa0));
    *((r12 + 0x58)) = 0;
    *((r12 + 0x60)) = 0;
    *((r12 + 0x50)) = rax;
    rax = *(rbx);
    *((r12 + 0x48)) = rdx;
    *((r12 + 0x1c)) = eax;
    goto label_20;
    *((r12 + 0x50)) = 0;
    rax = *((rbx - 0x30));
    *((r12 + 0x58)) = 0;
    *((r12 + 0x48)) = rax;
    rax = *(rbx);
    *((r12 + 0x60)) = 0;
    *((r12 + 0x1c)) = eax;
    goto label_20;
    *((rsp + 0x58)) = r10;
    edx = 5;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
label_31:
    rax = dcgettext (0, "hybrid");
    r9 = *((rsp + 0x48));
    r11 = *((rsp + 0x50));
    r10 = *((rsp + 0x58));
    rdi = rax;
    if (*((r12 + 0xe1)) == 0) {
        goto label_20;
    }
    rsi = r12;
    debug_print_relative_time_part_0 ();
    r9 = *((rsp + 0x48));
    r11 = *((rsp + 0x50));
    r10 = *((rsp + 0x58));
    goto label_20;
    *((rsp + 0x58)) = r10;
    edx = 5;
    rsi = "number";
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    goto label_29;
    *((rsp + 0x58)) = r10;
    edx = 5;
    rsi = "relative";
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    goto label_31;
    *((r12 + 0xb0))++;
    edx = 5;
    rsi = 0x00012b8b;
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    goto label_29;
    *((r12 + 0xe8)) = 1;
    rax = *((rbx - 0x30));
    *((r12 + 8)) = rax;
    rax = *(rbx);
    *((r12 + 0x10)) = eax;
    goto label_20;
    *((r12 + 0xe8)) = 1;
    rax = *((rbx - 0x38));
    *((r12 + 8)) = rax;
    rax = *(rbx);
    *((r12 + 0x10)) = eax;
    goto label_20;
    *((r12 + 8)) = 0;
    rax = *((rbx - 0x38));
    *((r12 + 0x10)) = eax;
    goto label_20;
    *((r12 + 8)) = 0;
    rax = *(rbx);
    *((r12 + 0x10)) = eax;
    goto label_20;
    eax = *((rbx - 0x38));
    eax += 0xe10;
    *((r12 + 0x18)) = eax;
    goto label_20;
    eax = *(rbx);
    eax += 0xe10;
    *((r12 + 0x18)) = eax;
    goto label_20;
    rcx = *((rbx - 0x28));
    rdx = *((rbx - 0x30));
    rdi = r12;
    *((rsp + 0x48)) = r9b;
    esi = *((rbx - 0x38));
    r8 = *(rbx);
    al = time_zone_hhmm_isra_0 ();
    if (al == 0) {
        goto label_21;
    }
    rax = *((r12 + 0x18));
    edx = 0;
    rax += *((rbx - 0x70));
    rcx = (int64_t) eax;
    __asm ("seto dl");
    *((r12 + 0x18)) = eax;
    r9 = *((rsp + 0x48));
    ecx = 1;
    if (rax != rcx) {
        edx = ecx;
    }
    if (edx == 0) {
        goto label_20;
    }
    goto label_21;
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    *((r12 + 0x18)) = 0xffff9d90;
label_32:
    eax = *((rbx + 0x30));
    r8d = 0;
    eax += *((r12 + 0x98));
    *((r12 + 0x98)) = eax;
    __asm ("seto r8b");
    rax = *((r12 + 0x90));
    rax += *((rbx + 0x28));
    rdx = rax;
    __asm ("seto al");
    edi = 0;
    *((r12 + 0x90)) = rdx;
    rdx = *((r12 + 0x88));
    eax = (int32_t) al;
    rdx += *((rbx + 0x20));
    __asm ("seto dil");
    *((r12 + 0x88)) = rdx;
    esi = 0;
    rdx = *((r12 + 0x80));
    rdx += *((rbx + 0x18));
    __asm ("seto sil");
    *((r12 + 0x80)) = rdx;
    r11d = 0;
    rdx = *((r12 + 0x78));
    rdx += *((rbx + 0x10));
    __asm ("seto r11b");
    *((r12 + 0x78)) = rdx;
    r10d = 0;
    rdx = *((r12 + 0x70));
    rdx += *((rbx + 8));
    __asm ("seto r10b");
    *((r12 + 0x70)) = rdx;
    ecx = 0;
    rdx = *((r12 + 0x68));
    rdx += *(rbx);
    __asm ("seto cl");
    eax |= r8d;
    *((r12 + 0x68)) = rdx;
    eax |= edi;
    eax |= esi;
    eax |= r11d;
    al |= r10b;
    if (al != 0) {
        goto label_21;
    }
    if (rcx != 0) {
        goto label_21;
    }
    *((r12 + 0xa1)) = 1;
    edx = 5;
    rsi = "relative";
    goto label_31;
    rax = *((rbx - 0x38));
    *((rsp + 0x58)) = r10;
    *((rsp + 0x50)) = r11;
    *((rsp + 0x48)) = r9b;
    *((r12 + 0x18)) = eax;
    goto label_32;
    *((r12 + 0x18)) = 0xffff9d90;
    goto label_20;
    rax = *(rbx);
    *((r12 + 0x18)) = eax;
    goto label_20;
    *((r12 + 0xc8))++;
    *((r12 + 0x14)) = 1;
    goto label_20;
    rax = *(rbx);
    *((r12 + 0x14)) = eax;
    goto label_20;
    rcx = *((rbx - 0x28));
    rdx = *((rbx - 0x30));
    rdi = r12;
    *((rsp + 0x48)) = r9b;
    *((r12 + 0xd8))++;
    esi = *((rbx - 0x38));
    r8 = *(rbx);
    al = time_zone_hhmm_isra_0 ();
    r9 = *((rsp + 0x48));
    if (al != 0) {
        goto label_20;
    }
    goto label_21;
    rax = *((rbx - 0x60));
    __asm ("movdqu xmm7, xmmword [rbx - 0x70]");
    rdx = 0x8000000000000000;
    *((r12 + 0x30)) = rax;
    rax = *((rbx - 0x30));
    __asm ("movups xmmword [r12 + 0x20], xmm7");
    rcx = rax;
    rcx = -rcx;
    *((r12 + 0x38)) = rcx;
    if (rax == rdx) {
        goto label_21;
    }
    rax = *((rbx + 8));
    rcx = *((rbx + 8));
    rcx = -rcx;
    *((r12 + 0x40)) = rcx;
    if (rax != rdx) {
        goto label_20;
    }
    goto label_21;
    rax = *((rbx - 0x68));
    *((r12 + 0x40)) = rax;
    rax = *((rbx - 0x38));
    *((r12 + 0x38)) = rax;
    rax = *((rbx + 8));
    rdx = *((rbx + 8));
    rdx = -rdx;
    *((r12 + 0x28)) = rdx;
    rdx = 0x8000000000000000;
    if (rax == rdx) {
        goto label_21;
    }
    do {
label_33:
        rax = *((rbx + 0x10));
        *((r12 + 0x30)) = rax;
        goto label_20;
        rax = *((rbx - 0x68));
        __asm ("movdqu xmm6, xmmword [rbx]");
        *((r12 + 0x40)) = rax;
        rax = *((rbx - 0x38));
        __asm ("movups xmmword [r12 + 0x20], xmm6");
        *((r12 + 0x38)) = rax;
        rax = *((rbx + 0x10));
        *((r12 + 0x30)) = rax;
        goto label_20;
        rax = *((rbx - 0x30));
        *((r12 + 0x40)) = rax;
        rax = *(rbx);
        *((r12 + 0x38)) = rax;
        goto label_20;
        rax = *((rbx - 0xa8));
        __asm ("movdqu xmm7, xmmword [rbx]");
        *((r12 + 0x38)) = rax;
        rax = *((rbx - 0x68));
        __asm ("movups xmmword [r12 + 0x20], xmm7");
        *((r12 + 0x40)) = rax;
    } while (1);
    rax = *((rbx - 0x38));
    *((r12 + 0x38)) = rax;
    rax = *((rbx + 8));
    *((r12 + 0x40)) = rax;
    goto label_20;
    *((rsp + 0x30)) = 0;
    *((rsp + 0x18)) = 1;
    goto label_22;
    *((rsp + 0x30)) = 0;
    r11d = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 8)) = 1;
    goto label_20;
    r11 = *((rbx + 8));
    goto label_20;
    rdx = 0x8000000000000000;
    rax = *((rbx - 0x70));
    *((r12 + 0x38)) = rax;
    rax = *((rbx - 0x30));
    rcx = *((rbx - 0x30));
    rcx = -rcx;
    *((r12 + 0x40)) = rcx;
    if (rax == rdx) {
        goto label_21;
    }
    rax = *((rbx + 8));
    rcx = *((rbx + 8));
    rcx = -rcx;
    *((r12 + 0x28)) = rcx;
    if (rax != rdx) {
        goto label_33;
    }
    goto label_21;
    *((rsp + 0x30)) = 0;
    r11d = 1;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 8)) = 0;
    goto label_20;
label_82:
    r9d = *((rsp + 8));
    r10d = *((rsp + 0x18));
    rdi = r15;
    ecx = *((rsp + 0x20));
    rbx = *((rsp + 0x10));
    *((rsp + 0x10)) = r15;
    *((rsp + 0x48)) = r9d;
    *((rsp + 0x28)) = r10d;
    *((rsp + 0x18)) = ecx;
    rax = strlen (rdi);
    *((rsp + 8)) = 1;
    ecx = *((rsp + 0x18));
    r11 = r15;
    *((rsp + 0x30)) = rax;
    r10d = *((rsp + 0x28));
    r9d = *((rsp + 0x48));
    if (rax != 3) {
        *((rsp + 8)) = 0;
        if (rax != 4) {
            goto label_87;
        }
        eax = 0;
        al = (*((rsp + 0x563)) == 0x2e) ? 1 : 0;
        *((rsp + 8)) = eax;
    }
label_87:
    *((rsp + 0x10)) = r11;
    r15 = obj_month_and_day_table;
    rsi = "JANUARY";
    goto label_88;
label_35:
    eax = strncmp (*((rsp + 0x10)), rsi, 3);
label_36:
    r9d = *((rsp + 0x18));
    r10d = *((rsp + 0x20));
    dl = (eax == 0) ? 1 : 0;
    ecx = *((rsp + 0x28));
    if (dl != 0) {
        goto label_34;
    }
    rsi = *((r15 + 0x10));
    r15 += 0x10;
    if (rsi == 0) {
        goto label_89;
    }
label_88:
    r11d = *((rsp + 8));
    *((rsp + 0x28)) = ecx;
    *((rsp + 0x20)) = r10d;
    *((rsp + 0x18)) = r9d;
    if (r11d != 0) {
        goto label_35;
    }
    strcmp (*((rsp + 0x10)), rsi);
    goto label_36;
label_79:
    edx = *((rdi + 1));
    goto label_4;
label_59:
    eax = 2;
    goto label_37;
label_60:
    eax = 0;
    goto label_37;
label_89:
    r11 = *((rsp + 0x10));
    rdi = r12;
    *((rsp + 0x28)) = r9d;
    *((rsp + 0x20)) = r10d;
    rsi = r11;
    *((rsp + 0x18)) = dl;
    *((rsp + 0x10)) = ecx;
    *((rsp + 8)) = r11;
    rax = lookup_zone ();
    r11 = *((rsp + 8));
    ecx = *((rsp + 0x10));
    edx = *((rsp + 0x18));
    r10d = *((rsp + 0x20));
    rsi = rax;
    r9d = *((rsp + 0x28));
    if (rax != 0) {
label_50:
        rax = *((rsi + 0xc));
        r15d = *((rsi + 8));
        goto label_38;
label_46:
        edx = 2;
        r15d = 0x3f;
        goto label_15;
    }
    rdi = r11;
    *((rsp + 0x28)) = r9d;
    r15 = obj_time_units_table;
    *((rsp + 0x20)) = ecx;
    *((rsp + 0x18)) = dl;
    *((rsp + 0x10)) = r10d;
    *((rsp + 8)) = r11;
    eax = strcmp (rdi, 0x00012a60);
    r11 = *((rsp + 8));
    r10d = *((rsp + 0x10));
    rsi = "YEAR";
    edx = *((rsp + 0x18));
    ecx = *((rsp + 0x20));
    r9d = *((rsp + 0x28));
    if (eax == 0) {
        goto label_90;
    }
    *((rsp + 0x10)) = rbx;
    rbx = r15;
    r15d = ecx;
    *((rsp + 0x20)) = r12;
    r12 = r11;
    *((rsp + 8)) = r9d;
    *((rsp + 0x18)) = r10d;
    *((rsp + 0x28)) = dl;
    while (eax != 0) {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_91;
        }
        eax = strcmp (r12, rsi);
    }
    ecx = r15d;
    r15 = rbx;
    r9d = *((rsp + 8));
    r10d = *((rsp + 0x18));
    rax = *((r15 + 0xc));
    r12 = *((rsp + 0x20));
    rbx = *((rsp + 0x10));
    r15d = *((r15 + 8));
    goto label_38;
label_85:
    rdi = *((rbx - 0xd8));
    *((rsp + 0x48)) = rdi;
    if (al != 0) {
        goto label_92;
    }
label_42:
    rax = *((rsp + 0x48));
    __asm ("movdqu xmm6, xmmword [rbx]");
    *((r12 + 0x38)) = rax;
    rax = *((rbx - 0x68));
    __asm ("movups xmmword [r12 + 0x20], xmm6");
    *((r12 + 0x40)) = rax;
    rax = *((rbx + 0x10));
    *((r12 + 0x30)) = rax;
    goto label_20;
label_61:
    r8d = 0;
    edi -= *((rsp + 0x7c));
    __asm ("seto r8b");
    *((rsp + 0x7c)) = r8d;
    r8d = 0;
    rsi -= *((rsp + 0x70));
    __asm ("seto r8b");
    *((rsp + 0x70)) = r8;
    r8d = 0;
    rcx -= *((rsp + 0x68));
    __asm ("seto r8b");
    *((rsp + 0x68)) = r8;
    r8d = 0;
    rdx -= *((rsp + 0x60));
    __asm ("seto r8b");
    *((rsp + 0x60)) = rdx;
    *((rsp + 0x90)) = r8;
    r8d = 0;
    rax -= *((rsp + 0x58));
    __asm ("seto r8b");
    *((rsp + 0x58)) = rax;
    eax = 0;
    *((rsp + 0x98)) = r8;
    r8 = *((rsp + 0x88));
    r8 -= *((rsp + 0x50));
    __asm ("seto al");
    *((rsp + 0x50)) = r8;
    r8 = *((rsp + 0x80));
    *((rsp + 0x88)) = rax;
    eax = 0;
    r8 -= *((rsp + 0x48));
    __asm ("seto al");
    rdx = r8;
    r8d = *((rsp + 0x70));
    r8b |= *((rsp + 0x7c));
    r8b |= *((rsp + 0x68));
    r8b |= *((rsp + 0x90));
    r8b |= *((rsp + 0x98));
    r8b |= *((rsp + 0x88));
    r8d |= eax;
    goto label_39;
label_91:
    rax = *((rsp + 0x30));
    r11 = r12;
    r9d = *((rsp + 8));
    ecx = r15d;
    rbx = *((rsp + 0x10));
    r10d = *((rsp + 0x18));
    r8 = r11 + rax - 1;
    edx = *((rsp + 0x28));
    r12 = *((rsp + 0x20));
    if (*(r8) == 0x53) {
        goto label_93;
    }
label_51:
    *((rsp + 8)) = r12;
    r15 = obj_relative_time_table;
    r12 = rbx;
    ebx = r9d;
    *((rsp + 0x10)) = r14;
    rsi = "TOMORROW";
    r14d = r10d;
    while (eax != 0) {
        rsi = *((r15 + 0x10));
        r15 += 0x10;
        if (rsi == 0) {
            goto label_94;
        }
        rdi = r11;
        *((rsp + 0x28)) = ecx;
        *((rsp + 0x20)) = dl;
        *((rsp + 0x18)) = r11;
        eax = strcmp (rdi, rsi);
        r11 = *((rsp + 0x18));
        edx = *((rsp + 0x20));
        ecx = *((rsp + 0x28));
    }
label_43:
    rax = *((r15 + 0xc));
    r9d = ebx;
    r10d = r14d;
    rbx = r12;
    r14 = *((rsp + 0x10));
    r12 = *((rsp + 8));
    r15d = *((r15 + 8));
    goto label_38;
label_90:
    *((rsp + 0xa0)) = 0;
    r15d = 0x103;
    goto label_40;
label_86:
    rcx = *((rbx - 0xd8));
    edx = 5;
    *((rsp + 0x68)) = r10;
    *((rsp + 0x58)) = r11;
    *((rsp + 0x50)) = r9b;
    *((rsp + 0x60)) = r8;
    *((rsp + 0x48)) = rcx;
    rax = dcgettext (0, "warning: value %ld has %ld digits. Assuming YYYY/MM/DD\n");
    eax = 0;
    dbg_printf (rax, *((rsp + 0x48)), *((rsp + 0x60)), rcx, r8, r9);
    r10 = *((rsp + 0x68));
    r11 = *((rsp + 0x58));
    r9 = *((rsp + 0x50));
    goto label_41;
label_92:
    edx = 5;
    *((rsp + 0x60)) = r10;
    *((rsp + 0x58)) = r11;
    *((rsp + 0x50)) = r9b;
    rax = dcgettext (0, "warning: value %ld has less than 4 digits. Assuming MM/DD/YY[YY]\n");
    eax = 0;
    dbg_printf (rax, *((rsp + 0x48)), rdx, rcx, r8, r9);
    r10 = *((rsp + 0x60));
    r11 = *((rsp + 0x58));
    r9 = *((rsp + 0x50));
    goto label_42;
label_93:
    *((rsp + 8)) = r12;
    r15 = obj_time_units_table;
    r12 = rbx;
    ebx = r9d;
    *((rsp + 0x10)) = r14;
    rsi = "YEAR";
    r14d = r10d;
    *(r8) = 0;
    while (eax != 0) {
        rsi = *((r15 + 0x10));
        r15 += 0x10;
        if (rsi == 0) {
            goto label_95;
        }
        rdi = r11;
        *((rsp + 0x48)) = ecx;
        *((rsp + 0x28)) = r8;
        *((rsp + 0x20)) = dl;
        *((rsp + 0x18)) = r11;
        eax = strcmp (rdi, rsi);
        r11 = *((rsp + 0x18));
        edx = *((rsp + 0x20));
        r8 = *((rsp + 0x28));
        ecx = *((rsp + 0x48));
    }
    goto label_43;
label_77:
    rax = rsi + 1;
    edx = 0;
    *(r12) = rax;
    r15d = *(rsi);
    if (r15d != 0) {
        goto label_40;
    }
    goto label_2;
label_84:
    esi = *(r10);
    edx++;
    esi -= 0x30;
    goto label_44;
label_94:
    r9d = ebx;
    r10d = r14d;
    rbx = r12;
    r14 = *((rsp + 0x10));
    r12 = *((rsp + 8));
    eax = *((rsp + 0x560));
    if (*((rsp + 0x30)) == 1) {
        goto label_96;
    }
label_48:
    rdi = r11;
    rsi = r11;
    if (al != 0) {
        goto label_97;
    }
    goto label_47;
    do {
        rsi++;
label_45:
        eax = *((rdi + 1));
        rdi++;
        *(rsi) = al;
        if (al == 0) {
            goto label_98;
        }
label_97:
    } while (al != 0x2e);
    edx = 1;
    goto label_45;
label_47:
    if (*((r12 + 0xe1)) == 0) {
        goto label_46;
    }
    edx = 5;
    *((rsp + 0x18)) = r9d;
    *((rsp + 0x10)) = ecx;
    r15d = 0x3f;
    *((rsp + 8)) = r11;
    rax = dcgettext (0, "error: unknown word '%s'\n");
    eax = 0;
    dbg_printf (rax, *((rsp + 8)), rdx, rcx, r8, r9);
    ecx = *((rsp + 0x10));
    r9d = *((rsp + 0x18));
    edx = 2;
    goto label_15;
label_98:
    *((rsp + 0x20)) = r10d;
    if (dl == 0) {
        goto label_47;
    }
    rsi = r11;
    rdi = r12;
    *((rsp + 0x18)) = r9d;
    *((rsp + 0x10)) = ecx;
    *((rsp + 8)) = r11;
    rax = lookup_zone ();
    r11 = *((rsp + 8));
    ecx = *((rsp + 0x10));
    r9d = *((rsp + 0x18));
    rdx = rax;
    if (rax == 0) {
        goto label_47;
    }
    rax = *((rax + 0xc));
    r15d = *((rdx + 8));
    r10d = *((rsp + 0x20));
    goto label_38;
label_96:
    rsi = obj_military_table;
    rdi = 0x00012b47;
    goto label_99;
label_49:
    rdi = *((rsi + 0x10));
    rsi += 0x10;
    if (rdi == 0) {
        goto label_48;
    }
label_99:
    if (*(rdi) != al) {
        goto label_49;
    }
    goto label_50;
label_63:
    stack_chk_fail ();
label_95:
    r9d = ebx;
    r10d = r14d;
    rbx = r12;
    *(r8) = 0x53;
    r12 = *((rsp + 8));
    r14 = *((rsp + 0x10));
    goto label_51;
}

/* /tmp/tmpyosj1dz2 @ 0xbe90 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x3904)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x11d44 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpyosj1dz2 @ 0xcfa0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xd0a0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xcf40 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xdf70 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x3790)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpyosj1dz2 @ 0xcee0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xb9d0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0xd420 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3740)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xcf00 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0x47d0 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmpyosj1dz2 @ 0xcc60 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0xc7f0)() ();
}

/* /tmp/tmpyosj1dz2 @ 0x4c50 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x00012a08);
    } while (1);
}

/* /tmp/tmpyosj1dz2 @ 0x10cb0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpyosj1dz2 @ 0xcea0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0x10c30 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x4b00 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xc080 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3913)() ();
    }
    if (rax == 0) {
        void (*0x3913)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xbe00 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x9e10 */
 
int64_t dbg_posix2_version (void) {
    char * e;
    int64_t var_8h;
    /* int posix2_version(); */
    r12d = 0x31069;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = getenv ("_POSIX2_VERSION");
    if (rax == 0) {
        goto label_0;
    }
    while (*(rdx) != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        eax = r12d;
        return rax;
        rax = strtol (rax, rsp, 0xa);
        rdx = *(rsp);
    }
    if (rax >= 0xffffffff80000000) {
        r12d = 0x7fffffff;
        if (rax <= r12) {
            r12 = rax;
        }
        goto label_0;
    }
    r12d = 0x80000000;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xd2f0 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0x36f0 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpyosj1dz2 @ 0xb6a0 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xc1c0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0xb640 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xd360 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3740)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xb8e0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x4e30 */
 
int64_t dbg_current_timespec (void) {
    timespec ts;
    int64_t var_8h;
    int64_t var_18h;
    /* timespec current_timespec(); */
    edi = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rsi = rsp;
    clock_gettime ();
    rax = *(rsp);
    rdx = *((rsp + 8));
    rcx = *((rsp + 0x18));
    rcx -= *(fs:0x28);
    if (rcx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x3560 */
 
void clock_gettime (void) {
    __asm ("bnd jmp qword [reloc.clock_gettime]");
}

/* /tmp/tmpyosj1dz2 @ 0xfc90 */
 
void dbg_nstrftime (int64_t arg5, int64_t arg6) {
    r8 = arg5;
    r9 = arg6;
    /* size_t nstrftime(char * s,size_t maxsize,char const * format,tm const * tp,timezone_t tz,int ns); */
    r9d = 0;
    r8d = 0;
    _strftime_internal_isra_0 ();
}

/* /tmp/tmpyosj1dz2 @ 0x4d00 */
 
uint64_t dbg_fd_reopen (int64_t arg1, int64_t arg4, int64_t oflag, char * path) {
    rdi = arg1;
    rcx = arg4;
    rdx = oflag;
    rsi = path;
    /* int fd_reopen(int desired_fd,char const * file,int flags,mode_t mode); */
    eax = 0;
    eax = open (rsi, edx, ecx);
    r12d = eax;
    if (ebp != eax) {
        if (eax >= 0) {
            goto label_0;
        }
    }
    eax = r12d;
    return eax;
label_0:
    esi = ebp;
    edi = eax;
    eax = dup2 ();
    ebx = eax;
    rax = errno_location ();
    r12d = ebx;
    r13d = *(rax);
    close (r12d);
    eax = r12d;
    *(rbp) = r13d;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x4c40 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpyosj1dz2 @ 0xcf70 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xd130 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x48e0 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x9b30 */
 
int64_t dbg_posixtime (int64_t arg_1h, int64_t arg1, int64_t arg3, char ** s) {
    tm tm0;
    tm tm1;
    int[6] pair;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t canary;
    int64_t var_28h;
    int64_t var_2ch;
    int64_t var_30h;
    int64_t var_34h;
    int64_t var_60h;
    int64_t var_64h;
    int64_t var_68h;
    int64_t var_6ch;
    int64_t var_70h;
    int64_t var_74h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a4h;
    int64_t var_a8h;
    int64_t var_ach;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rdx = arg3;
    rsi = s;
    /* _Bool posixtime(time_t * p,char const * s,unsigned int syntax_bits); */
    r12d = edx;
    *((rsp + 0x18)) = rdi;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
    rax = strlen (rsi);
    rbx = rax;
    r14 = rax;
    if ((r12b & 4) == 0) {
        goto label_7;
    }
    rax = strchr (rbp, 0x2e);
    r13 = rax;
    if (rax == 0) {
        goto label_3;
    }
    rax -= rbp;
    rbx -= rax;
    if (rbx == 3) {
        goto label_8;
    }
    do {
label_0:
        eax = 0;
label_4:
        rdx = *((rsp + 0xb8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_9;
        }
        return rax;
label_7:
        r13d = 0;
label_3:
        rax = rbx - 8;
    } while (rax > 4);
    ebx &= 1;
    if (ebx != 0) {
        goto label_0;
    }
    edx = 0;
label_1:
    eax = *((rbp + rdx));
    eax -= 0x30;
    if (eax > 9) {
        goto label_0;
    }
    rdx++;
    if (r14 > rdx) {
        goto label_1;
    }
    r14 >>= 1;
    eax = 0;
    rbx = rsp + 0xa0;
    do {
        edx = *((rbp + rax*2));
        ecx = rdx * 5;
        edx = *((rbp + rax*2 + 1));
        edx = rdx + rcx*2 - 0x30;
        *((rbx + rax*4)) = edx;
        rax++;
    } while (r14 != rax);
    rdx = r14 - 4;
    if ((r12b & 1) == 0) {
        goto label_10;
    }
    eax = *((rsp + 0xa0));
    eax--;
    *((rsp + 0x30)) = eax;
    eax = *((rsp + 0xa4));
    *((rsp + 0x2c)) = eax;
    eax = *((rsp + 0xa8));
    *((rsp + 0x28)) = eax;
    eax = *((rsp + 0xac));
    *((rsp + 0x24)) = eax;
    al = year (rsp + 0x20, rsp + 0xb0, rdx, r12d);
    if (al == 0) {
        goto label_0;
    }
label_5:
    if (r13 == 0) {
        goto label_11;
    }
    eax = *((r13 + 1));
    eax -= 0x30;
    if (eax > 9) {
        goto label_0;
    }
    edx = *((r13 + 2));
    ecx = rdx - 0x30;
    if (ecx > 9) {
        goto label_0;
    }
    eax = rax * 5;
    r15d = rdx + rax*2 - 0x30;
label_6:
    rax = rsp + 0x60;
    *((rsp + 0x10)) = 0;
    r14d = *((rsp + 0x24));
    r13d = *((rsp + 0x28));
    r12d = *((rsp + 0x2c));
    *((rsp + 8)) = rax;
    ebp = *((rsp + 0x30));
    ebx = *((rsp + 0x34));
    goto label_12;
label_2:
    eax = *((rsp + 0x74));
    esi = *((rsp + 0x70));
    eax ^= ebx;
    esi ^= ebp;
    eax |= esi;
    esi = *((rsp + 0x6c));
    esi ^= r12d;
    eax |= esi;
    esi = *((rsp + 0x68));
    esi ^= r13d;
    eax |= esi;
    esi = *((rsp + 0x64));
    esi ^= r14d;
    eax |= esi;
    esi = *((rsp + 0x60));
    esi ^= r15d;
    eax |= esi;
    if (eax == 0) {
        goto label_13;
    }
    if (r15d != 0x3c) {
        goto label_0;
    }
    *((rsp + 0x10)) = 1;
    r15d = 0x3b;
label_12:
    *((rsp + 0x60)) = r15d;
    *((rsp + 0x64)) = r14d;
    *((rsp + 0x68)) = r13d;
    *((rsp + 0x6c)) = r12d;
    *((rsp + 0x70)) = ebp;
    *((rsp + 0x74)) = ebx;
    *((rsp + 0x78)) = 0xffffffff;
    *((rsp + 0x80)) = 0xffffffff;
    rax = rpl_mktime (*((rsp + 8)));
    rcx = rax;
    eax = *((rsp + 0x78));
    if (eax >= 0) {
        goto label_2;
    }
    goto label_0;
label_8:
    rbx = rax;
    r14 = rax;
    goto label_3;
label_13:
    rax = *((rsp + 0x10));
    eax &= 1;
    rax += rcx;
    if (rax overflow 0) {
        goto label_0;
    }
    rcx = *((rsp + 0x18));
    *(rcx) = rax;
    eax = 1;
    goto label_4;
label_10:
    al = year (rsp + 0x20, rbx, rdx, r12d);
    if (al == 0) {
        goto label_0;
    }
    rax = rbx + r14*4 - 0x10;
    edx = *(rax);
    edx--;
    *((rsp + 0x30)) = edx;
    edx = *((rax + 4));
    *((rsp + 0x2c)) = edx;
    edx = *((rax + 8));
    eax = *((rax + 0xc));
    *((rsp + 0x28)) = edx;
    *((rsp + 0x24)) = eax;
    goto label_5;
label_11:
    r15d = 0;
    goto label_6;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xdee0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x3590)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpyosj1dz2 @ 0xd050 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xc1d0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0xc120 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x3918)() ();
    }
    if (rax == 0) {
        void (*0x3918)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x49d0 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmpyosj1dz2 @ 0xd3a0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3740)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xb8d0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0xb7e0)() ();
}

/* /tmp/tmpyosj1dz2 @ 0xc5d0 */
 
uint64_t dbg_localtime_rz (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* tm * localtime_rz(timezone_t tz,time_t const * t,tm * tm); */
    r14 = rsi;
    if (rdi == 0) {
        goto label_1;
    }
    r12 = rdi;
    rax = set_tz (rdi);
    r13 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rsi = rbp;
    rdi = r14;
    rax = fcn_00003470 ();
    if (rax == 0) {
        goto label_2;
    }
    al = save_abbr (r12, rbp, rdx, rcx, r8);
    if (al == 0) {
        goto label_2;
    }
    while (al != 0) {
        rax = rbp;
        return rax;
label_2:
        if (r13 != 1) {
            rdi = r13;
            eax = revert_tz_part_0 ();
        }
label_0:
        eax = 0;
        return rax;
        rdi = r13;
        al = revert_tz_part_0 ();
    }
    goto label_0;
label_1:
    rdi = r14;
    rsi = rdx;
    return void (*0x3480)() ();
}

/* /tmp/tmpyosj1dz2 @ 0xfcb0 */
 
int64_t vasnprintf (void * arg1, void ** arg2, int64_t arg3, int64_t arg4) {
    int64_t var_418h;
    int64_t var_40ch;
    int64_t var_408h;
    void ** var_400h;
    int64_t var_3f8h;
    int64_t var_3f0h;
    void * s2;
    void * var_3e0h;
    int64_t var_3d8h;
    void ** var_3d0h;
    void ** var_3c8h;
    int64_t var_3bch;
    int64_t var_3b8h;
    int64_t var_3b4h;
    int64_t var_3b0h;
    void ** var_3a8h;
    int64_t var_3a0h;
    uint32_t var_2c0h;
    void ** var_2b8h;
    int64_t var_2b0h;
    int64_t var_2a8h;
    int64_t var_2a0h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    int64_t var_fh;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
label_0:
    abort ();
    r14 = section__gnu_hash;
    r13 = rdx;
    r12 = rcx;
    *((rbp - 0x3e8)) = rdi;
    *((rbp - section..dynsym)) = rsi;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = printf_parse (r13, rbp - 0x2c0, r14);
    if (eax < 0) {
        goto label_34;
    }
    eax = printf_fetchargs (r12, r14);
    if (eax < 0) {
        goto label_35;
    }
    rdx = *((rbp - 0x2b0));
    rax = rdx + 7;
    rdx = 0xffffffffffffffff;
    if (rdx >= 0xfffffffffffffff9) {
        rax = rdx;
    }
    rax += *((rbp - 0x2a8));
    if (rax < 0) {
        goto label_36;
    }
    rdi = rax;
    r8d = 0;
    rdi += 6;
    r8b = (rdi < 0) ? 1 : 0;
    if (rdi < 0) {
        goto label_36;
    }
    if (rdi <= 0xf9f) {
        goto label_37;
    }
    *((rbp - 0x3c8)) = r8;
    if (rdi == -1) {
        goto label_36;
    }
    rax = malloc (rdi);
    *((rbp - 0x3e0)) = rax;
    if (rax == 0) {
        goto label_36;
    }
    *((rbp - 0x408)) = rax;
    r8 = *((rbp - 0x3c8));
label_11:
    ebx = 0;
    if (*((rbp - 0x3e8)) != 0) {
        rax = *((rbp - section..dynsym));
        rbx = *(rax);
    }
    r14 = *((rbp - 0x2b8));
    r9 = r8;
    r8 = r13;
    *((rbp - 0x3f8)) = 0;
    r15 = *((rbp - 0x3e8));
    r13 = *(r14);
    if (r13 == r8) {
        goto label_38;
    }
label_15:
    r13 -= r8;
    r12 = r9;
    rax = 0xffffffffffffffff;
    r12 += r13;
    if (r12 < 0) {
        r12 = rax;
    }
    if (rbx >= r12) {
        goto label_39;
    }
    if (rbx == 0) {
        goto label_40;
    }
    if (rbx < 0) {
        goto label_41;
    }
    rbx += rbx;
label_17:
    if (rbx < r12) {
        rbx = r12;
    }
    if (rbx == -1) {
        goto label_41;
    }
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    if (r15 == *((rbp - 0x3e8))) {
        goto label_42;
    }
    rax = realloc (r15, rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
label_18:
    *((rbp - 0x3c8)) = r10;
    memcpy (r10 + r9, r8, r13);
    r10 = *((rbp - 0x3c8));
label_16:
    rax = *((rbp - 0x3f8));
    if (*((rbp - 0x2c0)) == rax) {
        goto label_43;
    }
    edx = *((r14 + 0x48));
    rax = *((r14 + 0x50));
    if (dl == 0x25) {
        goto label_44;
    }
    if (rax == -1) {
        void (*0x391d)() ();
    }
    r15 = *((rbp - 0x3a8));
    rax <<= 5;
    rax += r15;
    ecx = *(rax);
    *((rbp - 0x3c8)) = ecx;
    if (dl == 0x6e) {
        goto label_45;
    }
    rcx = *((rbp - 0x3e0));
    eax = *((r14 + 0x10));
    *((rbp - 0x3b8)) = 0;
    *(rcx) = 0x25;
    r13 = rcx + 1;
    if ((al & 1) != 0) {
        rdx = *((rbp - 0x3e0));
        *((rdx + 1)) = 0x27;
        r13 = rdx + 2;
    }
    if ((al & 2) != 0) {
        *(r13) = 0x2d;
        r13++;
    }
    if ((al & 4) != 0) {
        *(r13) = 0x2b;
        r13++;
    }
    if ((al & 8) != 0) {
        *(r13) = 0x20;
        r13++;
    }
    if ((al & 0x10) != 0) {
        *(r13) = 0x23;
        r13++;
    }
    if ((al & 0x40) != 0) {
        *(r13) = 0x49;
        r13++;
    }
    if ((al & 0x20) != 0) {
        *(r13) = 0x30;
        r13++;
    }
    rsi = *((r14 + 0x18));
    rax = *((r14 + 0x20));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    rsi = *((r14 + 0x30));
    rax = *((r14 + 0x38));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    eax = *((rbp - 0x3c8));
    eax -= 7;
    if (eax > 9) {
        goto label_12;
    }
    rcx = 0x00014880;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (10 cases) at 0x14880 */
    void (*rax)() ();
label_36:
    errno_location ();
    *(rax) = 0xc;
    do {
label_4:
        rdi = *((rbp - 0x2b8));
        rax = rbp - 0x2a0;
        if (rdi != rax) {
            free (rdi);
        }
        rdi = *((rbp - 0x3a8));
        rax = rbp - 0x3a0;
        if (rdi != rax) {
            free (rdi);
        }
label_34:
        r10d = 0;
label_31:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_46;
        }
        rsp = rbp - 0x28;
        rax = r10;
        return rax;
label_35:
        errno_location ();
        *(rax) = 0x16;
    } while (1);
    *(r13) = 0x6c;
    r13++;
    *(r13) = 0x6c;
    r13++;
label_12:
    eax = *((r14 + 0x48));
    *((r13 + 1)) = 0;
    *(r13) = al;
    rax = *((r14 + 0x28));
    if (rax == -1) {
        goto label_47;
    }
    rax <<= 5;
    rax += r15;
    if (*(rax) != 5) {
        void (*0x391d)() ();
    }
    *((rbp - 0x3d8)) = 1;
    eax = *((rax + 0x10));
    *((rbp - 0x3b8)) = eax;
label_21:
    rax = *((r14 + 0x40));
    if (rax == -1) {
        goto label_48;
    }
    rax <<= 5;
    rcx = r15 + rax;
    if (*(rcx) != 5) {
        void (*0x391d)() ();
    }
    eax = *((rbp - 0x3d8));
    edx = *((rcx + 0x10));
    *((rbp + rax*4 - 0x3b8)) = edx;
    eax = rax + 1;
    *((rbp - 0x3d8)) = eax;
label_48:
    rax = r12;
    rax += 2;
    if (rax < 0) {
        goto label_49;
    }
    if (rbx >= rax) {
        goto label_50;
    }
    if (rbx != 0) {
        goto label_51;
    }
    if (rax > 0xc) {
        goto label_52;
    }
    ebx = 0xc;
label_26:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_53;
    }
    rdi = r10;
    *((rbp - 0x3d0)) = r10;
    rax = realloc (rdi, rbx);
    r10 = *((rbp - 0x3d0));
    r15 = rax;
    if (rax == 0) {
        goto label_25;
    }
label_22:
    *((r15 + r12)) = 0;
    rax = errno_location ();
    *((rbp - 0x3f0)) = r13;
    *((rbp - 0x3d0)) = rax;
    eax = *(rax);
    *((rbp - 0x40c)) = eax;
label_1:
    rax = *((rbp - 0x3d0));
    r13 = rbx;
    esi = 0x7fffffff;
    *((rbp - 0x3bc)) = 0xffffffff;
    r13 -= r12;
    *(rax) = 0;
    eax = *((rbp - 0x3c8));
    if (r13 <= rsi) {
        rsi = r13;
    }
    if (eax > 0x11) {
        goto label_0;
    }
    rdi = 0x000148a8;
    rax = *((rdi + rax*4));
    rax += rdi;
    /* switch table (18 cases) at 0x148a8 */
    void (*rax)() ();
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_54;
    }
    if (eax == 2) {
        goto label_55;
    }
    rax = rbp - 0x3bc;
label_5:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    rsi = *((rbp - 0x418));
label_6:
    edx = *((rbp - 0x3bc));
    if (edx < 0) {
        goto label_56;
    }
label_2:
    rcx = (int64_t) edx;
    if (rcx >= rsi) {
        goto label_57;
    }
    rcx += r15;
    if (*((rcx + r12)) != 0) {
        void (*0x391d)() ();
    }
label_57:
    if (edx < eax) {
label_3:
        *((rbp - 0x3bc)) = eax;
        edx = eax;
    }
    eax = rdx + 1;
    if (rax < rsi) {
        goto label_58;
    }
    if (r13 > 0x7ffffffe) {
        goto label_59;
    }
    if (rbx < 0) {
        goto label_60;
    }
    eax = rdx + 2;
    rcx = rbx + rbx;
    rax += r12;
    if (rax < 0) {
        goto label_23;
    }
    if (rax < rcx) {
        rax = rcx;
    }
    if (rbx >= rax) {
        goto label_1;
    }
    if (rcx >= rax) {
        rax = rcx;
    }
    rbx = rax;
    if (rax == -1) {
        goto label_23;
    }
    if (r15 == *((rbp - 0x3e8))) {
        goto label_61;
    }
    rax = realloc (r15, rax);
    if (rax == 0) {
        goto label_23;
    }
    r15 = rax;
    goto label_1;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_10;
    }
label_7:
    if (eax == 2) {
        goto label_62;
    }
label_9:
    rax = rbp - 0x3bc;
label_8:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    edx = *((rbp - 0x3bc));
    rsi = *((rbp - 0x418));
    if (edx >= 0) {
        goto label_2;
    }
label_56:
    rcx = *((rbp - 0x3f0));
    if (*((rcx + 1)) != 0) {
        goto label_63;
    }
    if (eax >= 0) {
        goto label_3;
    }
    rax = *((rbp - 0x3d0));
    eax = *(rax);
    if (eax == 0) {
        eax = *((r14 + 0x48));
        edx = 0x54;
        rbx = *((rbp - 0x3d0));
        eax &= 0xffffffef;
        eax = 0x16;
        if (al == 0x63) {
            eax = edx;
        }
        *(rbx) = eax;
    }
label_20:
    if (r15 != *((rbp - 0x3e8))) {
        free (r15);
    }
    rax = *((rbp - 0x408));
    if (rax == 0) {
        goto label_4;
    }
    free (rax);
    goto label_4;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    *(fp_stack--) = fp_stack[?];
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_64;
    }
    if (eax == 2) {
        goto label_65;
    }
    r9 = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_5;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    xmm0 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_66;
    }
    if (eax == 2) {
        goto label_67;
    }
    r8 = *((rbp - 0x3e0));
    edx = 1;
    eax = 1;
    r9 = rbp - 0x3bc;
    rcx = 0xffffffffffffffff;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    do {
label_10:
        rax = rbp - 0x3bc;
label_14:
        r9d = *((rbp - 0x3b8));
        goto label_8;
        rax = *((r14 + 0x50));
        rdi = r15 + r12;
        rax <<= 5;
        rax += *((rbp - 0x3a8));
        r9d = *((rax + 0x10));
        eax = *((rbp - 0x3d8));
    } while (eax == 1);
    if (eax != 2) {
        goto label_9;
    }
label_62:
    rax = rbp - 0x3bc;
label_13:
    eax = *((rbp - 0x3b4));
label_24:
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    eax = 0;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
label_37:
    rax += 0x1d;
    rcx = rsp;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_68;
    }
    do {
    } while (rsp != rcx);
label_68:
    edx &= 0xfff;
    if (rdx != 0) {
        goto label_69;
    }
label_29:
    *((rbp - 0x408)) = 0;
    rax = rsp + 0xf;
    rax &= 0xfffffffffffffff0;
    *((rbp - 0x3e0)) = rax;
    goto label_11;
    *(r13) = 0x4c;
    r13++;
    goto label_12;
label_60:
    if (rbx == -1) {
        goto label_1;
    }
    goto label_23;
label_61:
    rax = malloc (rax);
    if (rax == 0) {
        goto label_23;
    }
    if (r12 == 0) {
        goto label_70;
    }
    rax = memcpy (rax, r15, r12);
    r15 = rax;
    goto label_1;
label_55:
    rax = rbp - 0x3bc;
    goto label_13;
label_54:
    rax = rbp - 0x3bc;
    goto label_14;
label_63:
    *((rcx + 1)) = 0;
    goto label_1;
label_44:
    if (rax != -1) {
        void (*0x391d)() ();
    }
    r15 = r12 + 1;
    r9 = 0xffffffffffffffff;
    if (r12 < -1) {
        r9 = r15;
    }
    if (rbx < r9) {
        if (rbx == 0) {
            goto label_71;
        }
        if (rbx < 0) {
            goto label_25;
        }
        rbx += rbx;
label_27:
        if (rbx < r9) {
            rbx = r9;
        }
        if (rbx == -1) {
            goto label_25;
        }
        if (r10 == *((rbp - 0x3e8))) {
            goto label_72;
        }
        rdi = r10;
        *((rbp - 0x3d0)) = r9;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, rbx);
        r9 = *((rbp - 0x3d0));
        if (rax == 0) {
            goto label_30;
        }
        r10 = rax;
    }
label_28:
    *((r10 + r12)) = 0x25;
    r15 = r10;
label_19:
    r8 = *((r14 + 8));
    r13 = *((r14 + 0x58));
    r14 += 0x58;
    *((rbp - 0x3f8))++;
    if (r13 != r8) {
        goto label_15;
    }
label_38:
    r12 = r9;
    r10 = r15;
    goto label_16;
label_40:
    ebx = 0xc;
    goto label_17;
label_39:
    r10 = r15;
    goto label_18;
label_45:
    r13d = ecx;
    r13d -= 0x12;
    if (r13d > 4) {
        goto label_0;
    }
    rcx = 0x000148f0;
    rax = *((rax + 0x10));
    rdx = *((rcx + r13*4));
    rdx += rcx;
    /* switch table (5 cases) at 0x148f0 */
    void (*rdx)() ();
    *(rax) = r12;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12d;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12w;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12b;
    r9 = r12;
    r15 = r10;
    goto label_19;
label_49:
    if (rbx == -1) {
        goto label_50;
    }
label_25:
    *((rbp - 0x3c8)) = r10;
label_30:
    rax = errno_location ();
    r15 = *((rbp - 0x3c8));
    *((rbp - 0x3d0)) = rax;
label_23:
    rax = *((rbp - 0x3d0));
    *(rax) = 0xc;
    goto label_20;
label_47:
    *((rbp - 0x3d8)) = 0;
    goto label_21;
label_42:
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
    if (r9 == 0) {
        goto label_18;
    }
    rdx = r9;
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, *((rbp - 0x3e8)), rdx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    goto label_18;
label_50:
    r15 = r10;
    goto label_22;
label_41:
    rax = errno_location ();
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_65:
    rax = rbp - 0x3bc;
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    eax = *((rbp - 0x3b4));
    rcx = 0xffffffffffffffff;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
label_64:
    rax = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_24;
label_67:
    rax = rbp - 0x3bc;
    eax = *((rbp - 0x3b4));
    do {
        r9d = *((rbp - 0x3b8));
        edx = 1;
        eax = 1;
        r8 = *((rbp - 0x3e0));
        rcx = 0xffffffffffffffff;
        *((rbp - 0x418)) = rsi;
        snprintf_chk ();
        rsi = *((rbp - 0x418));
        goto label_6;
label_66:
        rax = rbp - 0x3bc;
    } while (1);
    if (r9 < 0) {
label_51:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= rax) {
        goto label_26;
    }
label_52:
    if (rax == -1) {
        goto label_25;
    }
    rbx = rax;
    goto label_26;
label_58:
    rax = (int64_t) edx;
    edx = *((rbp - 0x40c));
    r9 = rax + r12;
    rax = *((rbp - 0x3d0));
    *(rax) = edx;
    goto label_19;
label_59:
    rax = *((rbp - 0x3d0));
    *(rax) = 0x4b;
    goto label_20;
label_71:
    ebx = 0xc;
    goto label_27;
label_53:
    rax = malloc (rbx);
    r15 = rax;
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_22;
    }
    memcpy (rax, *((rbp - 0x3e8)), r12);
    goto label_22;
label_72:
    *((rbp - 0x3d0)) = r10;
    *((rbp - 0x3c8)) = r9;
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r10 = *((rbp - 0x3d0));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_74;
    }
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, r10, r12);
    r9 = *((rbp - 0x3c8));
    r10 = rax;
    goto label_28;
label_69:
    goto label_29;
label_43:
    r13 = r12;
    r13++;
    if (r13 < 0) {
        goto label_75;
    }
    if (rbx >= r13) {
        goto label_33;
    }
    if (rbx != 0) {
        goto label_76;
    }
    if (r13 > 0xc) {
        goto label_77;
    }
    ebx = 0xc;
label_32:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_78;
    }
    rdi = r10;
    *((rbp - 0x3c8)) = r10;
    rax = realloc (rdi, rbx);
    r10 = rax;
    if (rax == 0) {
        goto label_30;
    }
label_33:
    *((r10 + r12)) = 0;
    if (rbx > r13) {
        if (r10 == *((rbp - 0x3e8))) {
            goto label_79;
        }
        rdi = r10;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, r13);
        r10 = *((rbp - 0x3c8));
        if (rax == 0) {
            r10 = rax;
            goto label_79;
        }
    }
label_79:
    rdi = *((rbp - 0x408));
    if (rdi != 0) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x2b8));
    rax = rbp - 0x2a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x3a8));
    rax = rbp - 0x3a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rax = *((rbp - section..dynsym));
    *(rax) = r12;
    goto label_31;
    if (rdi < rax) {
label_76:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= r13) {
        goto label_32;
    }
label_77:
    if (r13 == -1) {
        goto label_25;
    }
    rbx = r13;
    goto label_32;
label_78:
    *((rbp - 0x3c8)) = r10;
    rax = malloc (rbx);
    r10 = *((rbp - 0x3c8));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_80;
    }
    rax = memcpy (rax, r10, r12);
    r10 = rax;
    goto label_33;
label_46:
    stack_chk_fail ();
label_75:
    if (rbx == -1) {
        goto label_33;
    }
    goto label_25;
label_73:
    rax = errno_location ();
    r15 = *((rbp - 0x3e8));
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_70:
    r15 = rax;
    goto label_1;
label_74:
    r10 = rax;
    goto label_28;
label_80:
    r10 = rax;
    goto label_33;
}

/* /tmp/tmpyosj1dz2 @ 0x9a00 */
 
void dbg_parse_datetime2 (int64_t arg_ch, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_76ch;
    int64_t var_1h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_54h;
    int64_t var_58h;
    int64_t var_5ch;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_74h;
    int64_t var_78h;
    int64_t var_7ch;
    int64_t var_80h;
    int64_t var_84h;
    int64_t var_88h;
    int64_t var_8ch;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_b0h;
    int64_t var_b4h;
    int64_t var_b8h;
    int64_t var_bch;
    int64_t var_c0h;
    int64_t var_c4h;
    int64_t var_d0h;
    int64_t var_f0h;
    int64_t var_f4h;
    int64_t var_f8h;
    int64_t var_fch;
    int64_t var_100h;
    int64_t var_104h;
    int64_t var_110h;
    int64_t var_120h;
    int64_t var_130h;
    int64_t var_170h;
    int64_t var_190h;
    int64_t var_198h;
    int64_t var_1a0h;
    int64_t var_1b0h;
    int64_t var_1b8h;
    int64_t var_1c0h;
    int64_t var_1c4h;
    int64_t var_1c8h;
    int64_t var_1cch;
    int64_t var_1d8h;
    int64_t var_1e0h;
    int64_t var_1e8h;
    int64_t var_1f0h;
    int64_t var_1f8h;
    int64_t var_200h;
    int64_t var_208h;
    int64_t var_210h;
    int64_t var_218h;
    int64_t var_220h;
    int64_t var_228h;
    int64_t var_230h;
    int64_t var_238h;
    int64_t var_240h;
    int64_t var_248h;
    int64_t var_250h;
    int64_t var_251h;
    int64_t var_258h;
    int64_t var_260h;
    int64_t var_268h;
    int64_t var_270h;
    int64_t var_278h;
    int64_t var_280h;
    int64_t var_288h;
    int64_t var_290h;
    int64_t var_291h;
    int64_t var_292h;
    int64_t var_294h;
    int64_t var_298h;
    int64_t var_2a0h;
    int64_t var_2a8h;
    int64_t var_2ach;
    int64_t var_2b0h;
    int64_t var_2b8h;
    int64_t var_2bch;
    int64_t var_2c0h;
    int64_t var_2d3h;
    int64_t var_2e0h;
    int64_t var_300h;
    int64_t var_370h;
    int64_t var_3e0h;
    int64_t var_3e2h;
    int64_t var_3e3h;
    int64_t var_448h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* _Bool parse_datetime2(timespec * result,char const * p,timespec const * now,unsigned int flags,timezone_t tzdefault,char const * tzstring); */
    return void (*0x7970)() ();
}

/* /tmp/tmpyosj1dz2 @ 0x10d70 */
 
int64_t dbg_printf_fetchargs (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int printf_fetchargs(__va_list_tag * args,arguments * a); */
    r8 = *(rsi);
    rax = *((rsi + 8));
    rcx = rdi;
    if (r8 == 0) {
        goto label_6;
    }
    esi = 0;
    rdi = 0x00014920;
    r10 = "(NULL)";
    r9 = "(NULL)";
    do {
        if (*(rax) > 0x16) {
            goto label_7;
        }
        edx = *(rax);
        rdx = *((rdi + rdx*4));
        rdx += rdi;
        /* switch table (23 cases) at 0x14920 */
        rax = void (*rdx)() ();
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_8;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_1:
        rdx = *(r11);
        *((rax + 0x10)) = rdx;
label_0:
        rsi++;
        rax += 0x20;
    } while (rsi != r8);
label_6:
    eax = 0;
    return rax;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_9;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        edx = *(r11);
        *((rax + 0x10)) = edx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_10;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_2:
        edx = *(r11);
        *((rax + 0x10)) = dx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_11;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_3:
        edx = *(r11);
        *((rax + 0x10)) = dl;
        goto label_0;
label_8:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_1;
label_9:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
    edx = *((rcx + 4));
    if (edx > 0xaf) {
        goto label_12;
    }
    r11d = edx;
    edx += 0x10;
    r11 += *((rcx + 0x10));
    *((rcx + 4)) = edx;
label_4:
    xmm0 = *(r11);
    *((rax + 0x10)) = xmm0;
    goto label_0;
    rdx = *((rcx + 8));
    rdx += 0xf;
    rdx &= 0xfffffffffffffff0;
    r11 = rdx + 0x10;
    *((rcx + 8)) = r11;
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_13;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
label_5:
    rdx = *(r11);
    if (rdx == 0) {
        rdx = r10;
    }
    *((rax + 0x10)) = rdx;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_14;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        rdx = *(r11);
        if (rdx == 0) {
            rdx = r9;
        }
        *((rax + 0x10)) = rdx;
        goto label_0;
label_10:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_2;
label_11:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_3;
label_14:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
label_12:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_4;
label_13:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_5;
label_7:
    eax |= 0xffffffff;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xdd50 */
 
void dbg_rpl_mktime (int64_t arg1, mktime_offset_t localtime_offset) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    rdx = localtime_offset;
    /* time_t rpl_mktime(tm * tp); */
    tzset ();
    rsi = *(reloc.localtime_r);
    rdi = rbp;
    rdx = obj_localtime_offset_0;
    return void (*0xd7a0)() ();
}

/* /tmp/tmpyosj1dz2 @ 0xb9b0 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0x3930 */
 
void dbg_main (int32_t argc, char ** argv) {
    timespec now;
    timespec notnow;
    timespec notnow1;
    stat st;
    int64_t var_10h;
    uint32_t var_8h;
    tm* var_ch;
    int64_t var_10h_2;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    uint32_t var_30h;
    uint32_t var_38h;
    int64_t var_40h;
    int64_t var_58h;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_d8h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15 = obj_time_masks;
    r14 = obj_longopts;
    r13 = "acd:fhmr:t:";
    r12 = 0x0001209b;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0xd8)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x00012ce0);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = 0x00012900;
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    rax = atexit ();
    *(obj.use_ref) = 0;
    *(obj.change_times) = 0;
    *(obj.no_create) = 0;
    *(rsp) = 0;
    *((rsp + 8)) = 0;
    do {
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_14;
        }
        if (eax > 0x80) {
            goto label_15;
        }
        if (eax <= 0x60) {
            goto label_16;
        }
        eax -= 0x61;
        if (eax > 0x1f) {
            goto label_15;
        }
        rax = *((r12 + rax*4));
        rax += r12;
        /* switch table (32 cases) at 0x12900 */
        void (*rax)() ();
        _xargmatch_internal ("--time", *(obj.optarg), obj.time_args, r15, 4, *(obj.argmatch_die));
        eax = *((r15 + rax*4));
        *(obj.change_times) |= eax;
    } while (1);
    al = posixtime (obj.newtime, *(obj.optarg), 6, rcx);
    *((rsp + 8)) = al;
    if (al == 0) {
        goto label_17;
    }
    *(0x00019128) = 0;
    __asm ("movdqa xmm0, xmmword [obj.newtime]");
    *(0x00019130) = xmm0;
    goto label_0;
    rax = optarg;
    *(obj.use_ref) = 1;
    *(obj.ref_file) = rax;
    goto label_0;
    *(obj.change_times) |= 2;
    goto label_0;
    *(obj.no_dereference) = 1;
    goto label_0;
    rax = optarg;
    *(rsp) = rax;
    goto label_0;
    *(obj.no_create) = 1;
    goto label_0;
    *(obj.change_times) |= 1;
    goto label_0;
label_16:
    if (eax == 0xffffff7d) {
        rax = "Randy Smith";
        rax = "David MacKenzie";
        rax = "Jim Kingdon";
        eax = 0;
        version_etc (*(obj.stdout), "touch", "GNU coreutils", *(obj.Version), "Paul Rubin", "Arnold Robbins");
        eax = exit (0);
    }
    if (eax != 0xffffff7e) {
        goto label_15;
    }
    usage (0, rsi, rdx, rcx, r8, r9);
label_14:
    if (*(obj.change_times) == 0) {
        *(obj.change_times) = 3;
    }
    eax = *(obj.use_ref);
    if (*((rsp + 8)) == 0) {
        goto label_18;
    }
    if (*(rsp) != 0) {
        goto label_19;
    }
    if (al != 0) {
        goto label_19;
    }
label_8:
    eax = optind;
    if (eax == ebp) {
        goto label_20;
    }
    r12d = 1;
    if (eax < ebp) {
        goto label_21;
    }
    goto label_22;
    do {
        r13d = *((r14 + 1));
        r15d = 1;
        if (r13d != 0) {
            goto label_23;
        }
label_1:
        eax = change_times;
        if (eax != 3) {
            if (eax == 2) {
                goto label_24;
            }
            eax--;
            if (eax != 0) {
                goto label_25;
            }
            *(0x00019138) = 0x3ffffffe;
        }
label_5:
        r8d = *(obj.no_dereference);
        ecx = 0;
        rax = obj_newtime;
        if (*(obj.amtime_now) == 0) {
            rcx = rax;
        }
        r8d <<= 8;
        if (r15d == 1) {
            goto label_26;
        }
        eax = fdutimensat (r15d, 0xffffff9c, r14, rcx, r8);
        if (eax != 0) {
            goto label_27;
        }
        if (r15d == 0) {
            eax = close (0);
            if (eax != 0) {
                goto label_28;
            }
        }
label_2:
        eax = 1;
label_3:
        r12d &= eax;
        eax = optind;
        eax++;
        *(obj.optind) = eax;
        if (eax >= ebp) {
            goto label_22;
        }
label_21:
        rax = (int64_t) eax;
        r14 = *((rbx + rax*8));
    } while (*(r14) == 0x2d);
label_23:
    if (*(obj.no_create) == 0) {
        if (*(obj.no_dereference) == 0) {
            goto label_29;
        }
    }
    r15d |= 0xffffffff;
    r13d = 0;
    goto label_1;
label_26:
    eax = fdutimensat (1, 0xffffff9c, 0, rcx, r8);
    if (eax == 0) {
        goto label_2;
    }
    rax = errno_location ();
    r8d = *(rax);
    if (r8d == 9) {
        goto label_30;
    }
label_4:
    if (r8d == 0) {
        goto label_2;
    }
    if (r13d == 0) {
        goto label_31;
    }
    if (r13d == 0x15) {
        goto label_31;
    }
label_7:
    if (r13d == 0x16) {
        rsi = rsp + 0x40;
        rdi = r14;
        *(rsp) = r8d;
        eax = stat ();
        if (eax != 0) {
            goto label_32;
        }
        eax = *((rsp + 0x58));
        r8d = *(rsp);
        eax &= 0xf000;
        if (eax == 0x4000) {
            goto label_31;
        }
    }
label_32:
    rsi = r14;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "cannot touch %s");
    rcx = r14;
    eax = 0;
    eax = error (0, r13d, rax);
    eax = 0;
    goto label_3;
label_27:
    rax = errno_location ();
    r8d = *(rax);
    if (r15d != 0) {
        goto label_4;
    }
    *(rsp) = r8d;
    eax = close (0);
    r8d = *(rsp);
    if (eax == 0) {
        goto label_4;
    }
label_28:
    rsi = r14;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "failed to close %s");
    r13 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    eax = error (0, *(rax), r13);
    eax = 0;
    goto label_3;
label_31:
    al = (r8d == 2) ? 1 : 0;
    al &= *(obj.no_create);
    if (al != 0) {
        goto label_3;
    }
label_6:
    rsi = r14;
    edi = 4;
    *(rsp) = r8d;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "setting times of %s");
    rcx = r13;
    eax = 0;
    eax = error (0, *(rsp), rax);
    eax = 0;
    goto label_3;
label_24:
    *(0x00019128) = 0x3ffffffe;
    goto label_5;
label_22:
    r12d ^= 1;
    eax = (int32_t) r12b;
    rdx = *((rsp + 0xd8));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_33;
    }
    return;
label_29:
    eax = fd_reopen (0, r14, 0x941, 0x1b6);
    r13d = 0;
    r15d = eax;
    if (eax >= 0) {
        goto label_1;
    }
    rax = errno_location ();
    r13d = *(rax);
    goto label_1;
label_30:
    if (*(obj.no_create) != 0) {
        goto label_2;
    }
    if (r13d == 0) {
        goto label_6;
    }
    if (r13d != 0x15) {
        goto label_7;
    }
    goto label_6;
label_18:
    if (al != 0) {
        goto label_34;
    }
    r15 = *(rsp);
    if (r15 == 0) {
        goto label_35;
    }
    r12 = rsp + 0x10;
    rdi = r12;
    gettime ();
    rdx = r12;
    rsi = r15;
    rdi = obj_newtime;
    get_reldate ();
    __asm ("movdqa xmm1, xmmword [obj.newtime]");
    *(0x00019130) = xmm1;
    if (*(obj.change_times) != 3) {
        goto label_8;
    }
    rax = *((rsp + 0x10));
    if (*(obj.newtime) != rax) {
        goto label_8;
    }
    rdx = *((rsp + 0x18));
    if (*(0x00019128) != rdx) {
        goto label_8;
    }
    rsi = *(rsp);
    rax ^= 1;
    *((rsp + 0x28)) = rdx;
    rdi = rsp + 0x30;
    rdx = rsp + 0x20;
    *((rsp + 0x20)) = rax;
    get_reldate ();
    rax = *((rsp + 0x20));
    if (*((rsp + 0x30)) != rax) {
        goto label_8;
    }
    rax = *((rsp + 0x28));
    if (*((rsp + 0x38)) != rax) {
        goto label_8;
    }
label_35:
    eax = ebp;
    eax -= *(obj.optind);
    eax--;
    if (eax > 0) {
        goto label_36;
    }
label_11:
    if (*(obj.change_times) == 3) {
        goto label_37;
    }
    *(0x00019128) = 0x3fffffff;
    *(0x00019138) = 0x3fffffff;
    goto label_8;
label_34:
    r12 = ref_file;
    rsi = rsp + 0x40;
    rdi = r12;
    if (*(obj.no_dereference) != 0) {
        goto label_38;
    }
    eax = stat ();
    if (eax != 0) {
        goto label_39;
    }
label_9:
    rax = *((rsp + 0x88));
    rdi = obj_newtime;
    *(obj.newtime) = rax;
    rax = *((rsp + 0x90));
    *(0x00019128) = rax;
    rax = *((rsp + 0x98));
    *(0x00019130) = rax;
    rax = *((rsp + 0xa0));
    *(0x00019138) = rax;
    if (*(rsp) == 0) {
        goto label_8;
    }
    eax = change_times;
    if ((al & 1) != 0) {
        goto label_40;
    }
label_10:
    if ((al & 2) == 0) {
        goto label_8;
    }
    rdi = 0x00019130;
    rsi = *(rsp);
    rdx = rdi;
    get_reldate ();
    goto label_8;
label_20:
    edx = 5;
label_13:
    rax = dcgettext (0, "missing file operand");
    eax = 0;
    error (0, 0, rax);
label_15:
    usage (1, rsi, rdx, rcx, r8, r9);
label_38:
    eax = lstat (rdi, rsi);
    if (eax == 0) {
        goto label_9;
    }
label_39:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to get attributes of %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_37:
    *(obj.amtime_now) = 1;
    goto label_8;
label_40:
    rsi = *(rsp);
    rdx = rdi;
    get_reldate ();
    eax = change_times;
    goto label_10;
label_36:
    eax = posix2_version ();
    if (eax > 0x30daf) {
        goto label_11;
    }
    rax = *(obj.optind);
    r12 = obj_newtime;
    al = posixtime (r12, *((rbx + rax*8)), 9, rcx);
    if (al == 0) {
        goto label_11;
    }
    *(0x00019128) = 0;
    __asm ("movdqa xmm2, xmmword [obj.newtime]");
    *(0x00019130) = xmm2;
    rax = getenv ("POSIXLY_CORRECT");
    while (rax == 0) {
label_12:
        *(obj.optind)++;
        goto label_8;
label_17:
        rax = quote (*(obj.optarg), rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "invalid date format %s");
        rcx = r12;
        eax = 0;
        error (1, 0, rax);
        rax = localtime (r12);
    }
    edx = *((rax + 0x10));
    r9d = *(rax);
    r8d = *((rax + 4));
    ecx = *((rax + 8));
    r15d = *((rax + 0xc));
    r12 = *((rax + 0x14));
    r13d = rdx + 1;
    edx = 5;
    rax = *(obj.optind);
    *((rsp + 0xc)) = r9d;
    *((rsp + 8)) = r8d;
    r12 += 0x76c;
    *(rsp) = ecx;
    r14 = *((rbx + rax*8));
    rax = dcgettext (0, "warning: 'touch %s' is obsolete; use 'touch -t %04ld%02d%02d%02d%02d.%02d');
    r9d = *((rsp + 0xc));
    eax = 0;
    r8d = *((rsp + 0x10));
    r9d = r13d;
    ecx = *((rsp + 0x10));
    r8 = r12;
    rcx = r14;
    error (0, 0, rax);
    goto label_12;
label_33:
    stack_chk_fail ();
label_25:
    assert_fail ("change_times == CH_ATIME", "src/touch.c", 0x93, "touch");
label_19:
    edx = 5;
    rsi = "cannot specify times from more than one source";
    goto label_13;
}

/* /tmp/tmpyosj1dz2 @ 0x43c0 */
 
int64_t dbg_usage (char * arg_8h, int64_t arg_10h, char * arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, char * arg_60h, int64_t arg_68h, int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... FILE...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Update the access and modification times of each FILE to the current time.\n\nA FILE argument that does not exist is created empty, unless -c or -h\nis supplied.\n\nA FILE argument string of - is handled specially and causes touch to\nchange the times of the file associated with standard output.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -a                     change only the access time\n  -c, --no-create        do not create any files\n  -d, --date=STRING      parse STRING and use it instead of current time\n  -f                     (ignored)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -h, --no-dereference   affect each symbolic link instead of any referenced\n                         file (useful only on systems that can change the\n                         timestamps of a symlink)\n  -m                     change only the modification time\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -r, --reference=FILE   use this file's times instead of current time\n  -t STAMP               use [[CC]YY]MMDDhhmm[.ss] instead of current time\n      --time=WORD        change the specified time:\n                           WORD is access, atime, or use: equivalent to -a\n                           WORD is modify or mtime: equivalent to -m\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    eax = void (*0x2e0036e0)() ();
    tmp_0 = ecx;
    ecx = eax;
    eax = tmp_0;
    esp = stdout;
    edx = 5;
    rax = dcgettext (0, "\nNote that the -d and -t options accept different time-date formats.\n");
    rsi = r12;
    r12 = "touch";
    rdi = rax;
    fputs_unlocked ();
    rax = 0x00012021;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x0001209b;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000120a5, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x00012ce0;
    r12 = 0x0001203d;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000120a5, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "touch";
        printf_chk ();
        r12 = 0x0001203d;
    }
label_5:
    r13 = "touch";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpyosj1dz2 @ 0xd7a0 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_mktime_internal (int64_t arg2, int64_t arg3, int32_t mon) {
    long_int t;
    long_int ot;
    tm tm;
    time_t x;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    signed int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    uint32_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rsi = arg2;
    rdx = arg3;
    rdi = mon;
    /* time_t mktime_internal(tm * tp,tm * (*)() convert,mktime_offset_t * offset); */
    rbx = rdi;
    rcx = *((rdi + 0x10));
    *((rsp + 0x58)) = rdi;
    *((rsp + 0x20)) = rsi;
    rsi = *((rdi + 0xc));
    *((rsp + 0x40)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x118)) = rax;
    eax = *(rdi);
    *((rsp + 0x48)) = eax;
    eax = *((rdi + 4));
    *((rsp + 0x10)) = eax;
    eax = *((rdi + 8));
    rdi = rcx;
    rcx *= 0x2aaaaaab;
    *((rsp + 0x14)) = eax;
    eax = *((rbx + 0x20));
    rcx >>= 0x21;
    *((rsp + 0x38)) = eax;
    eax = edi;
    eax >>= 0x1f;
    ecx -= eax;
    edx = rcx * 3;
    eax = ecx;
    edx <<= 2;
    edi -= edx;
    edx = edi;
    ecx = edi;
    edx >>= 0x1f;
    eax -= edx;
    rdx = *((rbx + 0x14));
    rax = (int64_t) eax;
    r15 = rax + rdx;
    edx = 0;
    if ((r15b & 3) == 0) {
        rax = 0x8f5c28f5c28f5c29;
        rdx = 0x51eb851eb851eb8;
        rdi = 0x28f5c28f5c28f5c;
        rax *= r15;
        rax += rdx;
        edx = 1;
        rax = rotate_right64 (rax, 2);
        if (rax <= rdi) {
            goto label_6;
        }
    }
label_1:
    eax = ecx;
    r12d = 0x3b;
    r9d = 0x46;
    rdi = r15;
    eax >>= 0x1f;
    eax &= 0xc;
    eax += ecx;
    rcx = rdx * 3;
    rdx = rdx + rcx*4;
    rax = (int64_t) eax;
    rax += rdx;
    rdx = obj___mon_yday;
    eax = *((rdx + rax*2));
    eax--;
    rax = (int64_t) eax;
    rsi += rax;
    rax = *((rsp + 0x48));
    *((rsp + 0x18)) = rsi;
    if (eax <= r12d) {
        r12 = rax;
    }
    eax = 0;
    __asm ("cmovs r12, rax");
    rax = *((rsp + 0x40));
    rax = *(rax);
    *((rsp + 8)) = rax;
    eax = *((rsp + 8));
    eax = -eax;
    *((rsp + 0x4c)) = eax;
    rax = ydhms_diff (rdi, rsi, *((rsp + 0x34)), *((rsp + 0x30)), r12d, r9);
    *((rsp + 0x50)) = rax;
    r13 = rsp + 0xa0;
    r14 = rsp + 0x88;
    *((rsp + 0x88)) = rax;
    *((rsp + 8)) = rax;
    *((rsp + 0x28)) = 6;
    *((rsp + 0x30)) = 0;
    do {
        rax = ranged_convert (*((rsp + 0x20)), r14, r13);
        if (rax == 0) {
            goto label_4;
        }
        ebx = *((rsp + 0xa0));
        eax = *((rsp + 0xac));
        eax = *((rsp + 0xb8));
        eax = *((rsp + 0xd4));
        rax = ydhms_diff (r15, *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), r12d, *((rsp + 0xd4)));
        r10 = *((rsp + 0x88));
        if (rax == 0) {
            goto label_7;
        }
        if (rbp != r10) {
            if (*((rsp + 8)) == r10) {
                goto label_8;
            }
        }
label_0:
        if (*((rsp + 8)) == r10) {
            goto label_5;
        }
        edx = *((rsp + 0xc0));
        rax += r10;
        *((rsp + 8)) = rbp;
        *((rsp + 0x88)) = rax;
        eax = 0;
        al = (edx != 0) ? 1 : 0;
        *((rsp + 0x30)) = eax;
    } while (1);
label_8:
    esi = *((rsp + 0xc0));
    if (esi < 0) {
        goto label_2;
    }
    ecx = *((rsp + 0x38));
    dl = (esi != 0) ? 1 : 0;
    if (ecx < 0) {
        goto label_9;
    }
    cl = (ecx != 0) ? 1 : 0;
    if (cl == dl) {
        goto label_0;
    }
label_2:
    rax = *((rsp + 0x4c));
    rdx = r10;
    rax += *((rsp + 0x50));
    rdx -= rax;
    edi = *((rsp + 0x48));
    rax = *((rsp + 0x40));
    *(rax) = rdx;
    if (edi != ebx) {
        al = (ebx == 0x3c) ? 1 : 0;
        edx = 0;
        dl = (edi <= 0) ? 1 : 0;
        rdx &= rax;
        rax = (int64_t) edi;
        rdx -= r12;
        rax += rdx;
        rax += r10;
        *((rsp + 0x88)) = rax;
        if (rax overflow 0) {
            goto label_5;
        }
        rdi = rsp + 0xe0;
        rsi = r13;
        rax = *((rsp + 0x20));
        rax = void (*rax)(uint64_t, uint64_t) (rax, rax);
        r10 = *((rsp + 8));
        if (rax == 0) {
            goto label_4;
        }
    }
    rcx = *((rsp + 0x58));
    __asm ("movdqa xmm0, xmmword [rsp + 0xa0]");
    __asm ("movdqa xmm1, xmmword [rsp + 0xb0]");
    rax = *((rsp + 0xd0));
    __asm ("movdqa xmm2, xmmword [rsp + 0xc0]");
    __asm ("movups xmmword [rcx], xmm0");
    *((rcx + 0x30)) = rax;
    __asm ("movups xmmword [rcx + 0x10], xmm1");
    __asm ("movups xmmword [rcx + 0x20], xmm2");
    goto label_10;
label_5:
    errno_location ();
    *(rax) = 0x4b;
label_4:
    r10 = 0xffffffffffffffff;
label_10:
    rax = *((rsp + 0x118));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_11;
    }
    rax = r10;
    return rax;
label_6:
    rdx = 0xa3d70a3d70a3d70b;
    rax = r15;
    rdx:rax = rax * rdx;
    rax = r15;
    rax >>= 0x3f;
    rdx += r15;
    rdx >>= 6;
    rdx -= rax;
    edx &= 3;
    dl = (rdx == 1) ? 1 : 0;
    edx = (int32_t) dl;
    goto label_1;
label_9:
    edx = (int32_t) dl;
    if (edx < *((rsp + 0x30))) {
        goto label_0;
    }
    goto label_2;
label_7:
    esi = *((rsp + 0x38));
    eax = *((rsp + 0xc0));
    rsp + 8 = (esi == 0) ? 1 : 0;
    edi = *((rsp + 8));
    rsp + 0x7f = (eax == 0) ? 1 : 0;
    ecx = *((rsp + 0x7f));
    if (cl == dil) {
        goto label_2;
    }
    eax |= esi;
    if (eax < 0) {
        goto label_2;
    }
    rax = rsp + 0xe0;
    *((rsp + 0x78)) = r12d;
    r12 = *((rsp + 0x20));
    *((rsp + 0x28)) = rax;
    rax = rsp + 0x90;
    r14d = 0x92c70;
    *((rsp + 0x30)) = rax;
    rax = rsp + 0x98;
    *((rsp + 0x68)) = rax;
    *((rsp + 0x70)) = r15;
    *((rsp + 0x60)) = r13;
    while (r13d == 1) {
        r14d += 0x92c70;
        if (r14d == 0xdb04f20) {
            goto label_12;
        }
        ebx = r14d;
        r15d = r14 + r14;
        r13d = 2;
        ebx = -ebx;
        rax = (int64_t) ebx;
        rax += rbp;
        *((rsp + 0x90)) = rax;
        if (rax !overflow 0) {
            goto label_13;
        }
label_3:
        ebx += r15d;
    }
    rax = (int64_t) ebx;
    r13d = 1;
    rax += rbp;
    *((rsp + 0x90)) = rax;
    if (rax overflow 0) {
        goto label_3;
    }
label_13:
    rax = ranged_convert (r12, *((rsp + 0x30)), *((rsp + 0x28)));
    if (rax == 0) {
        goto label_4;
    }
    eax = *((rsp + 0x100));
    dl = (eax == 0) ? 1 : 0;
    if (*((rsp + 8)) == dl) {
        goto label_14;
    }
    if (eax >= 0) {
        goto label_3;
    }
label_14:
    eax = *((rsp + 0xe0));
    eax = *((rsp + 0xec));
    eax = *((rsp + 0xf8));
    eax = *((rsp + 0x114));
    rax = ydhms_diff (*((rsp + 0x90)), *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), *((rsp + 0x98)), *((rsp + 0x114)));
    rax += *((rsp + 0xb0));
    rsi = *((rsp + 0x60));
    rdi = *((rsp + 0x68));
    rax = void (*r12)(uint64_t) (rax);
    rdx = *((rsp + 0x38));
    if (rax != 0) {
        goto label_15;
    }
    rax = errno_location ();
    if (*(rax) == 0x4b) {
        goto label_3;
    }
    goto label_4;
label_12:
    eax = *((rsp + 8));
    edx = *((rsp + 0x7f));
    r10 = rbp;
    r13 = *((rsp + 0x60));
    r12 = *((rsp + 0x78));
    eax -= edx;
    rdi = *((rsp + 0x28));
    eax *= 0xe10;
    rsi = r13;
    rax = (int64_t) eax;
    r10 += rax;
    rax = *((rsp + 0x20));
    rax = void (*rax)(uint64_t, uint64_t) (r10, r10);
    if (rax == 0) {
        goto label_5;
    }
    ebx = *((rsp + 0xa0));
    r10 = *((rsp + 8));
    goto label_2;
label_15:
    r12 = *((rsp + 0x78));
    r13 = *((rsp + 0x60));
    r10 = rdx;
    ebx = *((rsp + 0xa0));
    goto label_2;
label_11:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xb680 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xbd70 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xbba0 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x38ff)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xba80 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x38f5)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xb720 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x38ea)() ();
    }
    if (rdx == 0) {
        void (*0x38ea)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xbc30 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00019270]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00019280]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xd2c0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xd340 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0x4d80 */
 
uint64_t dbg_fdutimensat (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int fdutimensat(int fd,int dir,char const * file,timespec const * ts,int atflag); */
    r14d = r8d;
    r13d = esi;
    r12 = rcx;
    if (edi >= 0) {
        goto label_2;
    }
    if (rdx == 0) {
        goto label_3;
    }
label_1:
    ecx = r14d;
    rdx = r12;
    rsi = rbp;
    edi = r13d;
    eax = utimensat ();
    do {
label_0:
        if (eax == 1) {
            goto label_3;
        }
        return eax;
label_2:
        rsi = rcx;
        eax = futimens ();
    } while (eax != 0xffffffff);
    if (rbp == 0) {
        goto label_0;
    }
    rax = errno_location ();
    if (*(rax) == 0x26) {
        goto label_1;
    }
    eax = 0xffffffff;
    return rax;
label_3:
    errno_location ();
    *(rax) = 9;
    eax = 0xffffffff;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xb980 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpyosj1dz2 @ 0xcfe0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xdfc0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x3820)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpyosj1dz2 @ 0xb6c0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xd1c0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpyosj1dz2 @ 0xc680 */
 
int64_t dbg_mktime_z (int64_t arg_8h, int64_t arg_10h, int64_t arg_20h, int64_t arg_30h, tm * arg1, int64_t arg2) {
    tm tm_1;
    int64_t var_114h_2;
    int64_t var_8h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_1ch;
    int64_t var_20h_2;
    int64_t var_118h;
    int64_t var_30h_2;
    int64_t var_38h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h_2;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    rdi = arg1;
    rsi = arg2;
    /* time_t mktime_z(timezone_t tz,tm * tm); */
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_2;
    }
    r14 = rdi;
    rax = set_tz (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = *(rbp);
    r15 = rsp;
    *((rsp + 0x1c)) = 0xffffffff;
    *(rsp) = rax;
    rax = *((rbp + 8));
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x10));
    *((rsp + 0x10)) = rax;
    eax = *((rbp + 0x20));
    *((rsp + 0x20)) = eax;
    rax = rpl_mktime (r15);
    r13 = rax;
    eax = *((rsp + 0x1c));
    while (al == 0) {
        if (r12 != 1) {
            rdi = r12;
            revert_tz_part_0 ();
        }
label_1:
        r13 = 0xffffffffffffffff;
label_0:
        rax = *((rsp + 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r13;
        return rax;
        al = save_abbr (r14, r15, rdx, rcx, r8);
    }
    while (al != 0) {
        __asm ("movdqa xmm0, xmmword [rsp]");
        __asm ("movdqa xmm1, xmmword [rsp + 0x10]");
        __asm ("movdqa xmm2, xmmword [rsp + 0x20]");
        rax = *((rsp + 0x30));
        __asm ("movups xmmword [rbp], xmm0");
        *((rbp + 0x30)) = rax;
        __asm ("movups xmmword [rbp + 0x10], xmm1");
        __asm ("movups xmmword [rbp + 0x20], xmm2");
        goto label_0;
        rdi = r12;
        al = revert_tz_part_0 ();
    }
    goto label_1;
label_2:
    rax = *((rsp + 0x38));
    rax -= *(fs:0x28);
    if (rax == 0) {
        rdi = rsi;
        void (*0xc7d0)() ();
    }
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0xce00 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpyosj1dz2 @ 0x3710 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpyosj1dz2 @ 0x4c30 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpyosj1dz2 @ 0xde70 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpyosj1dz2 @ 0x3540 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpyosj1dz2 @ 0xc7f0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x000142c8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x000142db);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x000145c8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x145c8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x3880)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x3880)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x3880)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpyosj1dz2 @ 0x10fa0 */
 
int64_t dbg_printf_parse (int64_t arg1, int64_t arg2, int64_t arg3, size_t sum) {
    int64_t var_1h;
    int64_t var_4ch;
    int64_t var_30h;
    int64_t var_25h;
    int64_t var_bp_20h;
    int64_t var_8h;
    int64_t var_10h;
    void * s2;
    int64_t var_20h;
    void ** var_28h;
    void ** var_sp_30h;
    void ** var_38h;
    void ** var_40h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r9 = sum;
    /* int printf_parse(char const * format,char_directives * d,arguments * a); */
    r10 = rsi + 0x20;
    rax = rdi;
    rdi = rdx + 0x10;
    r15 = rdx;
    r14 = rsi;
    rcx = r10;
    r9d = 7;
    r13d = 0;
    r11d = 7;
    *(rsi) = 0;
    *((rsi + 8)) = r10;
    *((rsp + 0x18)) = rdi;
    *(rdx) = 0;
    *((rdx + 8)) = rdi;
    *((rsp + 0x10)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x20)) = 0;
    while (dl != 0) {
        rbx = rax + 1;
        if (dl == 0x25) {
            goto label_34;
        }
label_4:
        rax = rbx;
        edx = *(rax);
    }
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    *((rcx + rdx*8)) = rax;
    rax = *((rsp + 8));
    *((r14 + 0x10)) = rax;
    rax = *((rsp + 0x10));
    *((r14 + 0x18)) = rax;
    eax = 0;
label_7:
    return rax;
label_34:
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    r13 = 0xffffffffffffffff;
    r12 = rcx + rdx*8;
    *(r12) = rax;
    *((r12 + 0x10)) = 0;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x28)) = 0xffffffffffffffff;
    *((r12 + 0x30)) = 0;
    *((r12 + 0x38)) = 0;
    *((r12 + 0x40)) = 0xffffffffffffffff;
    *((r12 + 0x50)) = 0xffffffffffffffff;
    ebp = *((rax + 1));
    edx = rbp - 0x30;
    if (dl <= 9) {
        goto label_35;
    }
label_5:
    rcx = 0x0001499c;
    rdx = rbx + 1;
    if (bpl == 0x27) {
        goto label_36;
    }
    do {
        eax = rbp - 0x20;
        if (al <= 0x29) {
            eax = (int32_t) al;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (42 cases) at 0x1499c */
            void (*rax)() ();
        }
        if (bpl == 0x2a) {
            goto label_37;
        }
        eax = rbp - 0x30;
        if (al <= 9) {
            goto label_38;
        }
label_17:
        if (bpl == 0x2e) {
            goto label_39;
        }
label_3:
        edx = 0;
        rsi = 0x00014a44;
        edi = 1;
        rbx++;
        if (bpl == 0x68) {
            goto label_40;
        }
label_1:
        eax = rbp - 0x4c;
        if (al > 0x2e) {
            goto label_41;
        }
        eax = (int32_t) al;
        rax = *((rsi + rax*4));
        rax += rsi;
        /* switch table (47 cases) at 0x14a44 */
        void (*rax)() ();
        eax = *((r12 + 0x10));
        eax |= 0x40;
label_0:
        *((r12 + 0x10)) = eax;
        ebp = *(rdx);
        rbx = rdx;
        rdx = rbx + 1;
    } while (bpl != 0x27);
label_36:
    eax = *((r12 + 0x10));
    eax |= 1;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x20;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 2;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 4;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x10;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 8;
    goto label_0;
label_41:
    eax = rbp - 0x25;
    if (al > 0x53) {
        goto label_10;
    }
    rcx = 0x00014b00;
    eax = (int32_t) al;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (84 cases) at 0x14b00 */
    void (*rax)() ();
    edx += 8;
label_2:
    ebp = *(rbx);
    rbx++;
    if (bpl != 0x68) {
        goto label_1;
    }
label_40:
    ecx = edx;
    eax = edi;
    ecx &= 1;
    eax <<= cl;
    edx |= eax;
    goto label_2;
label_37:
    rdi = *((rsp + 8));
    eax = 1;
    *((r12 + 0x18)) = rbx;
    *((r12 + 0x20)) = rdx;
    ecx = *((rbx + 1));
    if (rdi != 0) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
    eax = rcx - 0x30;
    if (al <= 9) {
        goto label_42;
    }
label_9:
    rdi = *((rsp + 0x20));
    *((r12 + 0x28)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbp = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    rbx = rdx;
label_29:
    rdx = *((r15 + 8));
    r8 = *((r15 + 8));
    if (r9 <= rbp) {
        r9 += r9;
        rax = rbp + 1;
        if (r9 <= rbp) {
            r9 = rax;
        }
        rax = r9;
        rax >>= 0x3b;
        if (rax != 0) {
            goto label_30;
        }
        rsi = r9;
        rsi <<= 5;
        if (*((rsp + 0x18)) == rdx) {
            goto label_43;
        }
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r11;
        *((rsp + 0x28)) = r10;
        rax = realloc (rdx, rsi);
        rdx = *((r15 + 8));
        r10 = *((rsp + 0x28));
        r11 = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r8 = rax;
label_20:
        if (r8 == 0) {
            goto label_30;
        }
        if (*((rsp + 0x18)) == rdx) {
            goto label_44;
        }
label_26:
        *((r15 + 8)) = r8;
    }
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbp) {
        goto label_45;
    }
    do {
        rdx++;
        *(rax) = 0;
        rcx = rax;
        rax += 0x20;
    } while (rdx <= rbp);
    *(r15) = rdx;
    *(rcx) = 0;
label_45:
    rbp <<= 5;
    rbp += r8;
    eax = *(rbp);
    if (eax != 0) {
        goto label_46;
    }
    *(rbp) = 5;
    ebp = *(rbx);
    if (bpl != 0x2e) {
        goto label_3;
    }
label_39:
    if (*((rbx + 1)) != 0x2a) {
        goto label_47;
    }
    rdi = *((rsp + 0x10));
    eax = 2;
    rcx = rbx + 2;
    *((r12 + 0x30)) = rbx;
    *((r12 + 0x38)) = rcx;
    esi = *((rbx + 2));
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    eax = rsi - 0x30;
    if (al <= 9) {
        goto label_48;
    }
label_22:
    rbx = *((r12 + 0x40));
    if (rbx == -1) {
        goto label_49;
    }
label_21:
    r8 = *((r15 + 8));
    if (r9 <= rbx) {
        goto label_50;
    }
label_19:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbx) {
        goto label_51;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= rbx);
    *(r15) = rdx;
    *(rsi) = 0;
label_51:
    rbx <<= 5;
    rax = r8 + rbx;
    edx = *(rax);
    if (edx != 0) {
        goto label_52;
    }
    *(rax) = 5;
    rbx = rcx;
    ebp = *(rcx);
    goto label_3;
    edx |= 4;
    goto label_2;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xf;
label_12:
    if (r13 == -1) {
        goto label_53;
    }
    *((r12 + 0x50)) = r13;
label_16:
    r8 = *((r15 + 8));
    if (r9 <= r13) {
        goto label_54;
    }
label_15:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > r13) {
        goto label_55;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= r13);
    *(r15) = rdx;
    *(rsi) = 0;
label_55:
    r13 <<= 5;
    r13 += r8;
    eax = *(r13);
    if (eax != 0) {
        goto label_56;
    }
    *(r13) = ecx;
label_13:
    *((r12 + 0x48)) = bpl;
    rax = *(r14);
    *((r12 + 8)) = rbx;
    r13 = rax + 1;
    *(r14) = r13;
    if (r11 > r13) {
        rcx = *((r14 + 8));
        goto label_4;
    }
    if (r11 < 0) {
        goto label_57;
    }
    rax = 0x2e8ba2e8ba2e8ba;
    r12 = r11 + r11;
    if (r12 > rax) {
        goto label_57;
    }
    rax = r11 * 5;
    rbp = *((r14 + 8));
    *((rsp + 0x30)) = r9;
    rsi = r11 + rax*2;
    *((rsp + 0x28)) = r10;
    rsi <<= 4;
    if (r10 == rbp) {
        goto label_58;
    }
    rax = realloc (rbp, rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_57;
    }
    rbp = *((r14 + 8));
    if (r10 == rbp) {
        goto label_59;
    }
label_8:
    *((r14 + 8)) = rcx;
    r13 = *(r14);
    r11 = r12;
    goto label_4;
label_35:
    rdx = rbx;
    do {
        ecx = *((rdx + 1));
        rdx++;
        esi = rcx - 0x30;
    } while (sil <= 9);
    r13 = 0xffffffffffffffff;
    if (cl != 0x24) {
        goto label_5;
    }
    rax += 2;
    edi = 0;
    while (rsi >= 0) {
        if (dl > 9) {
            goto label_60;
        }
        rax++;
        edx = rbp - 0x30;
        rcx = rax - 1;
        rsi = 0x1999999999999999;
        rdx = (int64_t) dl;
        if (rdi > rsi) {
            goto label_61;
        }
        rsi = rdi * 5;
        rsi += rsi;
label_6:
        ebp = *(rax);
        rsi += rdx;
        rdi = rsi;
        edx = rbp - 0x30;
    }
    if (dl <= 9) {
        rcx = rax;
        rdx = (int64_t) dl;
        rax++;
        rsi = 0xffffffffffffffff;
        goto label_6;
    }
label_10:
    r8 = *((r15 + 8));
label_14:
    if (*((rsp + 0x18)) != r8) {
        *((rsp + 8)) = r10;
        free (r8);
        r10 = *((rsp + 8));
    }
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
    errno_location ();
    *(rax) = 0x16;
    eax = 0xffffffff;
    goto label_7;
label_47:
    *((r12 + 0x30)) = rbx;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    eax -= 0x30;
    if (al > 9) {
        goto label_62;
    }
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
label_28:
    rdi = *((rsp + 0x10));
    *((r12 + 0x38)) = rdx;
    ebp = *(rdx);
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    goto label_3;
label_58:
    rax = malloc (rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_63;
    }
label_33:
    rax = r13 * 5;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r10;
    rdx <<= 3;
    rax = memcpy (rcx, rbp, r13 + rax*2);
    r9 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    rcx = rax;
    goto label_8;
label_42:
    rax = rdx;
    do {
        esi = *((rax + 1));
        rax++;
        edi = rsi - 0x30;
    } while (dil <= 9);
    if (sil != 0x24) {
        goto label_9;
    }
    rbx += 2;
    esi = 0;
    while (rcx >= 0) {
        if (al > 9) {
            goto label_64;
        }
        rbx++;
        eax = rcx - 0x30;
        rdx = rbx - 1;
        rdi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rsi > rdi) {
            goto label_65;
        }
        rcx = rsi * 5;
        rcx += rcx;
label_11:
        rcx += rax;
        rsi = rcx;
        ecx = *(rbx);
        eax = rcx - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rdx = rbx;
    rax = (int64_t) al;
    rbx++;
    rcx = 0xffffffffffffffff;
    goto label_11;
    ecx = 0xc;
    if (edx > 0xf) {
        goto label_12;
    }
    ecx = 0;
    edx &= 4;
    cl = (edx != 0) ? 1 : 0;
    ecx += 0xb;
    goto label_12;
    ecx = 0xa;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 8;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 2;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 4;
    goto label_12;
    ecx = 9;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 7;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 1;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 3;
    goto label_12;
    ecx = 0x16;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 0x15;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 0x12;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx = 0x14;
    ecx -= edx;
    goto label_12;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xd;
    goto label_12;
    ecx = 0xe;
    goto label_12;
label_56:
    if (eax == ecx) {
        goto label_13;
    }
    goto label_14;
label_54:
    r9 += r9;
    rax = r13 + 1;
    if (r9 <= r13) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_67;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = realloc (r8, rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_68;
    }
label_18:
    *((r15 + 8)) = r8;
    goto label_15;
label_53:
    rdi = *((rsp + 0x20));
    *((r12 + 0x50)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    r13 = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_16;
label_38:
    *((r12 + 0x18)) = rbx;
    eax = *(rbx);
    eax -= 0x30;
    if (al > 9) {
        goto label_69;
    }
    rdx = rbx;
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rdi = *((rsp + 8));
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
label_69:
    *((r12 + 0x20)) = rbx;
    do {
        ebp = *(rbx);
        goto label_17;
label_46:
    } while (eax == 5);
    goto label_14;
label_67:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = malloc (rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax != 0) {
label_31:
        *((rsp + 0x40)) = r9;
        *((rsp + 0x38)) = r11;
        rdx <<= 5;
        *((rsp + 0x30)) = r10;
        *((rsp + 0x28)) = ecx;
        rax = memcpy (rdi, r8, *(r15));
        r9 = *((rsp + 0x40));
        r11 = *((rsp + 0x38));
        r10 = *((rsp + 0x30));
        ecx = *((rsp + 0x28));
        r8 = rax;
        goto label_18;
label_63:
        rdx = *((r15 + 8));
        if (*((rsp + 0x18)) == rdx) {
            goto label_70;
        }
label_24:
        *((rsp + 8)) = r10;
        free (rdx);
        r10 = *((rsp + 8));
    }
label_25:
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
label_70:
    errno_location ();
    *(rax) = 0xc;
    eax = 0xffffffff;
    return rax;
label_50:
    r9 += r9;
    rax = rbx + 1;
    if (r9 <= rbx) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_71;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = realloc (r8, rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_72;
    }
label_27:
    *((r15 + 8)) = r8;
    goto label_19;
label_52:
    if (edx != 5) {
        goto label_14;
    }
    ebp = *(rcx);
    rbx = rcx;
    goto label_3;
label_43:
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x28)) = r10;
    rax = malloc (rsi);
    rdx = *((rsp + 0x40));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_20;
    ecx = 0x11;
    goto label_12;
    ecx = 0x10;
    goto label_12;
label_49:
    rdi = *((rsp + 0x20));
    *((r12 + 0x40)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbx = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_21;
label_48:
    rax = rcx;
    do {
        edx = *((rax + 1));
        rax++;
        edi = rdx - 0x30;
    } while (dil <= 9);
    if (dl != 0x24) {
        goto label_22;
    }
    rbx += 3;
    edi = 0;
    while (rdx >= 0) {
        if (al > 9) {
            goto label_73;
        }
        rbx++;
        eax = rsi - 0x30;
        rcx = rbx - 1;
        rsi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rdi > rsi) {
            goto label_74;
        }
        rdx = rdi * 5;
        rdx += rdx;
label_23:
        esi = *(rbx);
        rdx += rax;
        rdi = rdx;
        eax = rsi - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rcx = rbx;
    rax = (int64_t) al;
    rbx++;
    rdx = 0xffffffffffffffff;
    goto label_23;
label_61:
    rsi = 0xffffffffffffffff;
    goto label_6;
label_60:
    r13 = rsi;
    r13--;
    if (r13 > 0xfffffffffffffffd) {
        goto label_10;
    }
    ebp = *((rcx + 2));
    rbx = rcx + 2;
    goto label_5;
label_57:
    rdx = *((r15 + 8));
label_30:
    if (*((rsp + 0x18)) != rdx) {
        goto label_24;
    }
    goto label_25;
label_44:
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    rdx <<= 5;
    *((rsp + 0x28)) = r10;
    rax = memcpy (r8, *((rsp + 0x18)), *(r15));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_26;
label_71:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = malloc (rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax == 0) {
        goto label_25;
    }
label_32:
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    rdx <<= 5;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = memcpy (rdi, r8, *(r15));
    r9 = *((rsp + 0x40));
    r11 = *((rsp + 0x38));
    r10 = *((rsp + 0x30));
    rcx = *((rsp + 0x28));
    r8 = rax;
    goto label_27;
label_65:
    rcx = 0xffffffffffffffff;
    goto label_11;
label_62:
    rbx = rdx;
    eax = 1;
    goto label_28;
label_64:
    rbp--;
    if (rbp > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x28)) = rbp;
    rbx = rdx + 2;
    goto label_29;
label_74:
    rdx = 0xffffffffffffffff;
    goto label_23;
label_73:
    rbx = rdx - 1;
    if (rbx > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x40)) = rbx;
    rcx += 2;
    goto label_21;
label_66:
    rdx = r8;
    goto label_30;
label_68:
    rdi = r8;
    r8 = rax;
    goto label_31;
label_72:
    rdi = r8;
    r8 = rax;
    goto label_32;
label_59:
    r13 = *(r14);
    goto label_33;
}

/* /tmp/tmpyosj1dz2 @ 0xcc80 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpyosj1dz2 @ 0x3000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0xd320 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0x4e20 */
 
void gettime (int64_t arg1) {
    rdi = arg1;
    rsi = rdi;
    edi = 0;
    return clock_gettime ();
}

/* /tmp/tmpyosj1dz2 @ 0x10d60 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpyosj1dz2 @ 0xd020 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0xc7d0 */
 
void dbg_rpl_timegm (int64_t arg1, mktime_offset_t gmtime_offset) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    rdx = gmtime_offset;
    /* time_t rpl_timegm(tm * tmp); */
    *((rdi + 0x20)) = 0;
    rsi = *(reloc.gmtime_r);
    rdx = obj_gmtime_offset_0;
    return void (*0xd7a0)() ();
}

/* /tmp/tmpyosj1dz2 @ 0xd3e0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x3740)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpyosj1dz2 @ 0x4bd0 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmpyosj1dz2 @ 0x34a0 */
 
void ctype_toupper_loc (void) {
    /* [15] -r-x section size 1088 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpyosj1dz2 @ 0x34c0 */
 
void snprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__snprintf_chk]");
}

/* /tmp/tmpyosj1dz2 @ 0x34d0 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpyosj1dz2 @ 0x34e0 */
 
void utimensat (void) {
    __asm ("bnd jmp qword [reloc.utimensat]");
}

/* /tmp/tmpyosj1dz2 @ 0x34f0 */
 
void localtime (void) {
    __asm ("bnd jmp qword [reloc.localtime]");
}

/* /tmp/tmpyosj1dz2 @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *((rbx + 0xffffff))++;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *((rcx + 0x20)) += cl;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    if (*(rax) > 0) {
        void (*0x31)() ();
    }
    *((rbx + 0xffffff))++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rbx + 0x2fffffe)) += cl;
    *(rax) += al;
    *((rbp + 0x5a)) += cl;
    eax = (int32_t) ax;
    tmp_0 = edx;
    edx = eax;
    eax = tmp_0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    cl += ah;
    *(rax) += eax;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    al += 0x64;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    if (*(rax) >= 0) {
        *(rax) += eax;
        *(rax) += al;
        *(rax) += al;
        *(rcx) += eax;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *((rbx + 0x2ffffff))++;
        *(rax) += al;
        *((rax + rax)) += al;
        *(rax) += al;
        *(rax) += eax;
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("loop 0x101");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += edx;
    *(rcx) += eax;
    *(rcx) += eax;
    *(rax) += eax;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) ^= al;
    *(rax) += al;
    *(rax) += al;
    *((rcx - 0x13)) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("in eax, dx");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    eax = (int32_t) ax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax = (int32_t) ax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmpyosj1dz2 @ 0x3530 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpyosj1dz2 @ 0x3570 */
 
void setenv (void) {
    __asm ("bnd jmp qword [reloc.setenv]");
}

/* /tmp/tmpyosj1dz2 @ 0x3580 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpyosj1dz2 @ 0x3590 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpyosj1dz2 @ 0x35a0 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpyosj1dz2 @ 0x35c0 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpyosj1dz2 @ 0x35f0 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpyosj1dz2 @ 0x3600 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpyosj1dz2 @ 0x3610 */
 
void dup2 (void) {
    __asm ("bnd jmp qword [reloc.dup2]");
}

/* /tmp/tmpyosj1dz2 @ 0x3620 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpyosj1dz2 @ 0x3630 */
 
void snprintf (void) {
    __asm ("bnd jmp qword [reloc.snprintf]");
}

/* /tmp/tmpyosj1dz2 @ 0x3640 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmpyosj1dz2 @ 0x3660 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpyosj1dz2 @ 0x3670 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpyosj1dz2 @ 0x3680 */
 
void fputs (void) {
    __asm ("bnd jmp qword [reloc.fputs]");
}

/* /tmp/tmpyosj1dz2 @ 0x3690 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpyosj1dz2 @ 0x36a0 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmpyosj1dz2 @ 0x36b0 */
 
void fputc (void) {
    __asm ("bnd jmp qword [reloc.fputc]");
}

/* /tmp/tmpyosj1dz2 @ 0x36c0 */
 
void lstat (void) {
    __asm ("bnd jmp qword [reloc.lstat]");
}

/* /tmp/tmpyosj1dz2 @ 0x36d0 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpyosj1dz2 @ 0x3720 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmpyosj1dz2 @ 0x3730 */
 
void strtol (void) {
    __asm ("bnd jmp qword [reloc.strtol]");
}

/* /tmp/tmpyosj1dz2 @ 0x3750 */
 
void tzset (void) {
    __asm ("bnd jmp qword [reloc.tzset]");
}

/* /tmp/tmpyosj1dz2 @ 0x3760 */
 
void time (void) {
    __asm ("bnd jmp qword [reloc.time]");
}

/* /tmp/tmpyosj1dz2 @ 0x3770 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpyosj1dz2 @ 0x3790 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpyosj1dz2 @ 0x37b0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpyosj1dz2 @ 0x37f0 */
 
void strftime (void) {
    __asm ("bnd jmp qword [reloc.strftime]");
}

/* /tmp/tmpyosj1dz2 @ 0x3810 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmpyosj1dz2 @ 0x3820 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpyosj1dz2 @ 0x3830 */
 
void unsetenv (void) {
    __asm ("bnd jmp qword [reloc.unsetenv]");
}

/* /tmp/tmpyosj1dz2 @ 0x3840 */
 
void futimens (void) {
    __asm ("bnd jmp qword [reloc.futimens]");
}

/* /tmp/tmpyosj1dz2 @ 0x3850 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpyosj1dz2 @ 0x3890 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpyosj1dz2 @ 0x38a0 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpyosj1dz2 @ 0x38b0 */
 
void ctype_tolower_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_tolower_loc]");
}

/* /tmp/tmpyosj1dz2 @ 0x38c0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpyosj1dz2 @ 0x3480 */
 
void fcn_00003480 (void) {
    __asm ("bnd jmp qword [reloc.gmtime_r]");
}

/* /tmp/tmpyosj1dz2 @ 0x3470 */
 
void fcn_00003470 (void) {
    /* [14] -r-x section size 48 named .plt.got */
    __asm ("bnd jmp qword [reloc.localtime_r]");
}

/* /tmp/tmpyosj1dz2 @ 0x3030 */
 
void fcn_00003030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1104 named .plt */
    __asm ("bnd jmp qword [0x00018d98]");
}

/* /tmp/tmpyosj1dz2 @ 0x3040 */
 
void fcn_00003040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3050 */
 
void fcn_00003050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3060 */
 
void fcn_00003060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3070 */
 
void fcn_00003070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3080 */
 
void fcn_00003080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3090 */
 
void fcn_00003090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x30a0 */
 
void fcn_000030a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x30b0 */
 
void fcn_000030b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x30c0 */
 
void fcn_000030c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x30d0 */
 
void fcn_000030d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x30e0 */
 
void fcn_000030e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x30f0 */
 
void fcn_000030f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3100 */
 
void fcn_00003100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3110 */
 
void fcn_00003110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3120 */
 
void fcn_00003120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3130 */
 
void fcn_00003130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3140 */
 
void fcn_00003140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3150 */
 
void fcn_00003150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3160 */
 
void fcn_00003160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3170 */
 
void fcn_00003170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3180 */
 
void fcn_00003180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3190 */
 
void fcn_00003190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x31a0 */
 
void fcn_000031a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x31b0 */
 
void fcn_000031b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x31c0 */
 
void fcn_000031c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x31d0 */
 
void fcn_000031d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x31e0 */
 
void fcn_000031e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x31f0 */
 
void fcn_000031f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3200 */
 
void fcn_00003200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3210 */
 
void fcn_00003210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3220 */
 
void fcn_00003220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3230 */
 
void fcn_00003230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3240 */
 
void fcn_00003240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3250 */
 
void fcn_00003250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3260 */
 
void fcn_00003260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3270 */
 
void fcn_00003270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3280 */
 
void fcn_00003280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3290 */
 
void fcn_00003290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x32a0 */
 
void fcn_000032a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x32b0 */
 
void fcn_000032b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x32c0 */
 
void fcn_000032c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x32d0 */
 
void fcn_000032d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x32e0 */
 
void fcn_000032e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x32f0 */
 
void fcn_000032f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3300 */
 
void fcn_00003300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3310 */
 
void fcn_00003310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3320 */
 
void fcn_00003320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3330 */
 
void fcn_00003330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3340 */
 
void fcn_00003340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3350 */
 
void fcn_00003350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3360 */
 
void fcn_00003360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3370 */
 
void fcn_00003370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3380 */
 
void fcn_00003380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3390 */
 
void fcn_00003390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x33a0 */
 
void fcn_000033a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x33b0 */
 
void fcn_000033b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x33c0 */
 
void fcn_000033c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x33d0 */
 
void fcn_000033d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x33e0 */
 
void fcn_000033e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x33f0 */
 
void fcn_000033f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3400 */
 
void fcn_00003400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3410 */
 
void fcn_00003410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3420 */
 
void fcn_00003420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3430 */
 
void fcn_00003430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3440 */
 
void fcn_00003440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3450 */
 
void fcn_00003450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpyosj1dz2 @ 0x3460 */
 
void fcn_00003460 (void) {
    return __asm ("bnd jmp section..plt");
}
