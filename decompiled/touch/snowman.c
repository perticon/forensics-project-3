
signed char parse_datetime();

void** quote(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_35b0();

void fun_3800();

void get_reldate(void* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    signed char al7;

    al7 = parse_datetime();
    if (!al7) {
        quote(rsi, rsi, rdx, rcx);
        fun_35b0();
        fun_3800();
    } else {
        return;
    }
}

struct s0 {
    signed char[76295] pad76295;
    void** f12a07;
};

uint32_t fun_3630(void** rdi, int64_t rsi, int64_t rdx);

uint32_t fun_34c0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

/* str_days.constprop.0 */
void** str_days_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...) {
    int64_t rdx6;
    void** rdi7;
    struct s0* rcx8;
    void** rsi9;
    uint32_t eax10;
    void** r8_11;

    if (!*reinterpret_cast<signed char*>(rdi + 0xe8)) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        rdx6 = *reinterpret_cast<int32_t*>(rdi + 16);
        if (*reinterpret_cast<uint32_t*>(&rdx6) > 6) {
            addr_546c_3:
            return rsi;
        } else {
            rdi7 = rsi;
            *reinterpret_cast<int32_t*>(&rcx8) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi9) = 100;
            *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 8) + 1) <= reinterpret_cast<unsigned char>(13)) {
            eax10 = fun_3630(rsi, 100, "%s");
        } else {
            eax10 = fun_34c0(rsi, 100, 1, 0xffffffffffffffff, "%ld");
        }
        rdx6 = *reinterpret_cast<int32_t*>(rdi + 16);
        if (*reinterpret_cast<uint32_t*>(&rdx6) > 6) 
            goto addr_546c_3;
        if (eax10 > 99) 
            goto addr_546c_3;
        *reinterpret_cast<int32_t*>(&rcx8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rcx8) = reinterpret_cast<uint1_t>(eax10 == 0);
        rsi9 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(100 - eax10)));
        rdi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax10))));
    }
    r8_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(" %s") + reinterpret_cast<uint64_t>(rcx8));
    fun_34c0(rdi7, rsi9, 1, 0xffffffffffffffff, r8_11, rdi7, rsi9, 1, 0xffffffffffffffff, r8_11);
    return rsi;
}

void** g28;

void dbg_printf(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, ...);

void** stderr = reinterpret_cast<void**>(0);

void fun_3880(void** rdi, ...);

void fun_36b0(int64_t rdi, void** rsi, void** rdx);

uint64_t fun_3870(void** rdi, ...);

struct s1 {
    signed char[76388] pad76388;
    void** f12a64;
};

struct s2 {
    signed char[76478] pad76478;
    void** f12abe;
};

void** time_zone_str(void** edi, void** rsi, ...);

void fun_35e0();

/* debug_print_current_time.part.0 */
void debug_print_current_time_part_0(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    void** rax8;
    void** rax9;
    void** rsi10;
    void** rdi11;
    void** v12;
    void** rsp13;
    void** rdx14;
    uint32_t eax15;
    void** rsi16;
    uint32_t eax17;
    void** rsi18;
    void** rbp19;
    void** rax20;
    int1_t zf21;
    void** r12_22;
    uint32_t eax23;
    void** r12_24;
    void** rsi25;
    void** rax26;
    uint32_t edx27;
    struct s1* r12_28;
    void** r12_29;
    uint32_t edx30;
    struct s2* rdx31;
    int1_t zf32;
    void** edi33;
    void** rax34;
    int1_t zf35;
    void* rax36;
    void** rdi37;
    uint32_t edx38;
    void* rdi39;
    uint32_t ecx40;
    void*** rdi41;
    void** rcx42;
    void** rdx43;
    void*** rax44;
    void*** rcx45;
    int64_t v46;

    rax8 = g28;
    rax9 = fun_35b0();
    rsi10 = rdi;
    rdi11 = rax9;
    dbg_printf(rdi11, rsi10, 5, rcx, r8, r9, v12);
    rsp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x78 - 8 + 8 - 8 + 8);
    if (*reinterpret_cast<int64_t*>(rsi + 0xa8) && !*reinterpret_cast<signed char*>(rsi + 0xe2)) {
        rcx = *reinterpret_cast<void***>(rsi + 40);
        *reinterpret_cast<int32_t*>(&rsi10) = 1;
        r8 = *reinterpret_cast<void***>(rsi + 56);
        rdi11 = stderr;
        fun_3880(rdi11);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
        *reinterpret_cast<signed char*>(rsi + 0xe2) = 1;
        *reinterpret_cast<uint32_t*>(&rdx14) = 1;
        *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
        eax15 = *reinterpret_cast<unsigned char*>(rsi + 0xe7);
        if (*reinterpret_cast<unsigned char*>(rsi + 0xe0) == *reinterpret_cast<unsigned char*>(&eax15)) 
            goto addr_55ca_3;
        rsi16 = stderr;
        fun_36b0(32, rsi16, 1);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
        goto addr_5588_5;
    }
    eax17 = *reinterpret_cast<unsigned char*>(rsi + 0xe7);
    *reinterpret_cast<uint32_t*>(&rdx14) = 0;
    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
    if (*reinterpret_cast<unsigned char*>(rsi + 0xe0) == *reinterpret_cast<unsigned char*>(&eax17)) {
        addr_55ca_3:
        if (!*reinterpret_cast<int64_t*>(rsi + 0xd0) || *reinterpret_cast<signed char*>(rsi + 0xe5)) {
            if (*reinterpret_cast<int64_t*>(rsi + 0xb0) && !*reinterpret_cast<unsigned char*>(rsi + 0xe3)) {
                if (*reinterpret_cast<signed char*>(&rdx14)) {
                    addr_5847_9:
                    rsi18 = stderr;
                    fun_36b0(32, rsi18, rdx14);
                    rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
                    goto addr_55fc_10;
                } else {
                    addr_55fc_10:
                    rbp19 = *reinterpret_cast<void***>(rsi + 8);
                    rax20 = str_days_constprop_0(rsi, rsp13, rdx14, rcx, r8, rsi, rsp13, rdx14, rcx, r8);
                    fun_35b0();
                    r8 = rbp19;
                    rcx = rax20;
                    rdi11 = stderr;
                    *reinterpret_cast<int32_t*>(&rsi10) = 1;
                    fun_3880(rdi11);
                    rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8 - 8 + 8 - 8 + 8);
                    *reinterpret_cast<unsigned char*>(rsi + 0xe3) = 1;
                    *reinterpret_cast<uint32_t*>(&rdx14) = 1;
                    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                }
            }
        } else {
            *reinterpret_cast<int32_t*>(&rsi10) = 1;
            r8 = *reinterpret_cast<void***>(rsi + 80);
            rdi11 = stderr;
            fun_3880(rdi11);
            rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
            rcx = *reinterpret_cast<void***>(rsi + 96);
            if (rcx) {
                rdi11 = stderr;
                *reinterpret_cast<int32_t*>(&rsi10) = 1;
                fun_3880(rdi11);
                rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
            }
            if (*reinterpret_cast<int32_t*>(rsi + 28) == 1) {
                rcx = stderr;
                *reinterpret_cast<int32_t*>(&rsi10) = 1;
                rdi11 = reinterpret_cast<void**>("pm");
                fun_3870("pm");
                rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
            }
            zf21 = *reinterpret_cast<int64_t*>(rsi + 0xb0) == 0;
            *reinterpret_cast<signed char*>(rsi + 0xe5) = 1;
            *reinterpret_cast<uint32_t*>(&rdx14) = 1;
            *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
            if (zf21) 
                goto addr_5650_16;
            *reinterpret_cast<uint32_t*>(&rdx14) = *reinterpret_cast<unsigned char*>(rsi + 0xe3);
            *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
            if (!*reinterpret_cast<signed char*>(&rdx14)) 
                goto addr_5847_9;
        }
    } else {
        addr_5588_5:
        r12_22 = *reinterpret_cast<void***>(rsi + 40);
        fun_35b0();
        rdi11 = stderr;
        *reinterpret_cast<int32_t*>(&rsi10) = 1;
        rcx = r12_22;
        fun_3880(rdi11);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8 - 8 + 8);
        eax23 = *reinterpret_cast<unsigned char*>(rsi + 0xe0);
        *reinterpret_cast<uint32_t*>(&rdx14) = 1;
        *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
        *reinterpret_cast<unsigned char*>(rsi + 0xe7) = *reinterpret_cast<unsigned char*>(&eax23);
        goto addr_55ca_3;
    }
    addr_5650_16:
    if (!*reinterpret_cast<int64_t*>(rsi + 0xc0) || *reinterpret_cast<signed char*>(rsi + 0xe4)) {
        if (!*reinterpret_cast<int64_t*>(rsi + 0xd8) || *reinterpret_cast<signed char*>(rsi + 0xe6)) {
            if (*reinterpret_cast<signed char*>(rsi + 0xa0)) {
                r12_24 = *reinterpret_cast<void***>(rsi + 88);
                if (*reinterpret_cast<signed char*>(&rdx14)) {
                    addr_56d6_21:
                    rsi25 = stderr;
                    fun_36b0(32, rsi25, rdx14);
                    goto addr_5718_22;
                } else {
                    addr_5718_22:
                    rax26 = fun_35b0();
                    rdi11 = stderr;
                    rcx = r12_24;
                    *reinterpret_cast<int32_t*>(&rsi10) = 1;
                    rdx14 = rax26;
                    fun_3880(rdi11);
                    goto addr_5744_23;
                }
            }
        } else {
            edx27 = *reinterpret_cast<uint32_t*>(&rdx14) ^ 1;
            *reinterpret_cast<uint32_t*>(&r12_28) = *reinterpret_cast<unsigned char*>(&edx27);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_28) + 4) = 0;
            r12_29 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r12_28) + reinterpret_cast<uint64_t>(" UTC%s"));
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(rsi + 20);
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        r8 = reinterpret_cast<void**>(" DST");
        if (!*reinterpret_cast<int64_t*>(rsi + 0xc8)) {
            r8 = reinterpret_cast<void**>(0x12ce0);
        }
        edx30 = *reinterpret_cast<uint32_t*>(&rdx14) ^ 1;
        rdi11 = stderr;
        *reinterpret_cast<uint32_t*>(&rdx31) = *reinterpret_cast<unsigned char*>(&edx30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx31) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rsi10) = 1;
        rdx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx31) + reinterpret_cast<uint64_t>(" isdst=%d%s"));
        fun_3880(rdi11);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
        zf32 = *reinterpret_cast<int64_t*>(rsi + 0xd8) == 0;
        *reinterpret_cast<signed char*>(rsi + 0xe4) = 1;
        if (zf32) 
            goto addr_56c9_28;
        if (!*reinterpret_cast<signed char*>(rsi + 0xe6)) 
            goto addr_58d0_30; else 
            goto addr_56c9_28;
    }
    addr_5871_31:
    edi33 = *reinterpret_cast<void***>(rsi + 24);
    rax34 = time_zone_str(edi33, rsp13, edi33, rsp13);
    rdi11 = stderr;
    rdx14 = r12_29;
    *reinterpret_cast<int32_t*>(&rsi10) = 1;
    rcx = rax34;
    fun_3880(rdi11);
    zf35 = *reinterpret_cast<signed char*>(rsi + 0xa0) == 0;
    *reinterpret_cast<signed char*>(rsi + 0xe6) = 1;
    if (zf35) {
        addr_5744_23:
        rax36 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
        if (!rax36) {
        }
    } else {
        goto addr_56d2_35;
    }
    fun_35e0();
    rdi37 = rdx14;
    if (reinterpret_cast<signed char>(rcx) <= reinterpret_cast<signed char>(2)) 
        goto addr_591c_38;
    if (reinterpret_cast<signed char>(r8) >= reinterpret_cast<signed char>(0)) {
        addr_5925_40:
        edx38 = 0;
        rdi39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdi37) * 60);
        *reinterpret_cast<unsigned char*>(&edx38) = __intrinsic();
        if (*reinterpret_cast<signed char*>(&rsi10)) {
            ecx40 = 0;
            rdi41 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi39) - reinterpret_cast<unsigned char>(r8));
            *reinterpret_cast<unsigned char*>(&ecx40) = __intrinsic();
        } else {
            ecx40 = 0;
            rdi41 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi39) + reinterpret_cast<unsigned char>(r8));
            *reinterpret_cast<unsigned char*>(&ecx40) = __intrinsic();
        }
    } else {
        rcx42 = rdi37;
        rdx43 = reinterpret_cast<void**>((reinterpret_cast<int64_t>(__intrinsic()) + reinterpret_cast<unsigned char>(rdi37) >> 6) - reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rdi37) >> 63));
        rax44 = reinterpret_cast<void***>(rdx43 + reinterpret_cast<unsigned char>(rdx43) * 4);
        rdi37 = rdx43;
        rcx45 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rcx42) - (reinterpret_cast<uint64_t>(rax44 + reinterpret_cast<uint64_t>(rax44) * 4) << 2));
        goto addr_59b2_44;
    }
    if (edx38 | ecx40) {
        addr_595f_46:
        goto v46;
    } else {
        addr_5941_47:
        if (reinterpret_cast<uint64_t>(rdi41 + 0x5a0) <= 0xb40) {
            *reinterpret_cast<void***>(rdi11 + 24) = reinterpret_cast<void**>(*reinterpret_cast<int32_t*>(&rdi41) * 60);
            goto addr_595f_46;
        }
    }
    addr_59b2_44:
    rdi41 = rcx45 + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdi37) << 4) - reinterpret_cast<unsigned char>(rdi37)) * 4;
    goto addr_5941_47;
    addr_591c_38:
    if (reinterpret_cast<signed char>(r8) >= reinterpret_cast<signed char>(0)) 
        goto addr_5925_40;
    *reinterpret_cast<int32_t*>(&rcx45) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx45) + 4) = 0;
    goto addr_59b2_44;
    addr_56d2_35:
    r12_24 = *reinterpret_cast<void***>(rsi + 88);
    goto addr_56d6_21;
    addr_56c9_28:
    if (!*reinterpret_cast<signed char*>(rsi + 0xa0)) 
        goto addr_5744_23; else 
        goto addr_56d2_35;
    addr_58d0_30:
    r12_29 = reinterpret_cast<void**>(" UTC%s");
    goto addr_5871_31;
}

struct s3 {
    signed char[104] pad104;
    int64_t f68;
    int64_t f70;
    int64_t f78;
    int64_t f80;
    int64_t f88;
    int64_t f90;
    int32_t f98;
};

/* debug_print_relative_time.part.0 */
void debug_print_relative_time_part_0(void** rdi, struct s3* rsi, int64_t rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    void** rax8;
    void** v9;
    void** rdi10;
    void** rbp11;

    rax8 = fun_35b0();
    dbg_printf(rax8, rdi, 5, rcx, r8, r9, v9);
    if (rsi->f68) {
        rdi10 = stderr;
        fun_3880(rdi10, rdi10);
        if (!rsi->f70) {
            addr_5a3a_3:
            rbp11 = stderr;
            if (!rsi->f78) {
                addr_5a7e_4:
                if (rsi->f80) {
                    fun_3880(rbp11, rbp11);
                    rbp11 = stderr;
                }
            } else {
                addr_5a4f_6:
                fun_3880(rbp11, rbp11);
                rbp11 = stderr;
                goto addr_5a7e_4;
            }
        } else {
            rbp11 = stderr;
            goto addr_5a24_8;
        }
    } else {
        rbp11 = stderr;
        if (!rsi->f70) {
            if (rsi->f78) 
                goto addr_5a4f_6;
            if (!rsi->f80 && (!rsi->f88 && (!rsi->f90 && !rsi->f98))) {
                fun_35b0();
            }
        } else {
            goto addr_5a24_8;
        }
    }
    if (rsi->f88) {
        fun_3880(rbp11, rbp11);
        rbp11 = stderr;
    }
    if (rsi->f90) {
        fun_3880(rbp11, rbp11);
        rbp11 = stderr;
    }
    if (rsi->f98) {
        fun_3880(rbp11, rbp11);
    }
    addr_5a24_8:
    fun_3880(rbp11, rbp11);
    goto addr_5a3a_3;
}

void rpl_vfprintf();

struct s4 {
    signed char[16] pad16;
    void** f10;
};

int32_t fun_3700(void** rdi, void** rsi, ...);

struct s5 {
    void** f0;
    signed char[7] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

void dbg_printf(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, ...) {
    signed char al8;
    void** rax9;
    void** rdi10;
    void* rax11;
    struct s4* r12_12;
    void** rbp13;
    void** rbx14;
    void** rsi15;
    int32_t eax16;
    void** rsi17;
    struct s5* r12_18;
    int32_t eax19;
    void** rsi20;
    int32_t eax21;

    if (al8) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax9 = g28;
    fun_3870("date: ", "date: ");
    rdi10 = stderr;
    rpl_vfprintf();
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(g28));
    if (!rax11) {
        return;
    }
    fun_35e0();
    r12_12 = reinterpret_cast<struct s4*>(0x186e0);
    rbp13 = rdi10;
    rbx14 = rdi;
    rsi15 = reinterpret_cast<void**>("GMT");
    do {
        eax16 = fun_3700(rbx14, rsi15, rbx14, rsi15);
        if (!eax16) 
            break;
        rsi15 = r12_12->f10;
        r12_12 = reinterpret_cast<struct s4*>(&r12_12->f10);
    } while (rsi15);
    goto addr_52d8_9;
    addr_52ca_10:
    goto 0x3000000008;
    addr_52d8_9:
    rsi17 = *reinterpret_cast<void***>(rbp13 + 0xf0);
    r12_18 = reinterpret_cast<struct s5*>(rbp13 + 0xf0);
    if (rsi17) {
        do {
            eax19 = fun_3700(rbx14, rsi17, rbx14, rsi17);
            if (!eax19) 
                break;
            rsi17 = r12_18->f10;
            r12_18 = reinterpret_cast<struct s5*>(&r12_18->f10);
        } while (rsi17);
        goto addr_52f0_13;
    } else {
        goto addr_52f0_13;
    }
    goto 0x3000000008;
    addr_52f0_13:
    r12_12 = reinterpret_cast<struct s4*>(0x183e0);
    rsi20 = reinterpret_cast<void**>("WET");
    do {
        eax21 = fun_3700(rbx14, rsi20, rbx14, rsi20);
        if (!eax21) 
            break;
        rsi20 = r12_12->f10;
        r12_12 = reinterpret_cast<struct s4*>(&r12_12->f10);
    } while (rsi20);
    goto addr_5348_18;
    goto addr_52ca_10;
    addr_5348_18:
    goto 0x3000000008;
}

struct s5* lookup_zone(void** rdi, void** rsi) {
    struct s5* r12_3;
    void** rbp4;
    void** rbx5;
    void** rsi6;
    int32_t eax7;
    void** rsi8;
    struct s5* r12_9;
    int32_t eax10;
    void** rsi11;
    int32_t eax12;

    r12_3 = reinterpret_cast<struct s5*>(0x186e0);
    rbp4 = rdi;
    rbx5 = rsi;
    rsi6 = reinterpret_cast<void**>("GMT");
    do {
        eax7 = fun_3700(rbx5, rsi6);
        if (!eax7) 
            break;
        rsi6 = r12_3->f10;
        r12_3 = reinterpret_cast<struct s5*>(&r12_3->f10);
    } while (rsi6);
    goto addr_52d8_4;
    addr_52ca_5:
    return r12_3;
    addr_52d8_4:
    rsi8 = *reinterpret_cast<void***>(rbp4 + 0xf0);
    r12_9 = reinterpret_cast<struct s5*>(rbp4 + 0xf0);
    if (rsi8) {
        do {
            eax10 = fun_3700(rbx5, rsi8);
            if (!eax10) 
                break;
            rsi8 = r12_9->f10;
            r12_9 = reinterpret_cast<struct s5*>(&r12_9->f10);
        } while (rsi8);
        goto addr_52f0_8;
    } else {
        goto addr_52f0_8;
    }
    return r12_9;
    addr_52f0_8:
    r12_3 = reinterpret_cast<struct s5*>(0x183e0);
    rsi11 = reinterpret_cast<void**>("WET");
    do {
        eax12 = fun_3700(rbx5, rsi11);
        if (!eax12) 
            break;
        rsi11 = r12_3->f10;
        r12_3 = reinterpret_cast<struct s5*>(&r12_3->f10);
    } while (rsi11);
    goto addr_5348_13;
    goto addr_52ca_5;
    addr_5348_13:
    return 0;
}

unsigned char mktime_ok(void** rdi, void** rsi) {
    int32_t eax3;

    eax3 = 0;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi + 24)) >= reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<unsigned char*>(&eax3) = reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) ^ reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi)) | reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(rdi + 4)) ^ reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(rsi + 4)) | reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 8)) ^ reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8)) | *reinterpret_cast<uint32_t*>(rdi + 12) ^ *reinterpret_cast<uint32_t*>(rsi + 12) | *reinterpret_cast<uint32_t*>(rdi + 16) ^ *reinterpret_cast<uint32_t*>(rsi + 16) | *reinterpret_cast<uint32_t*>(rdi + 20) ^ *reinterpret_cast<uint32_t*>(rsi + 20)) == 0);
    }
    return *reinterpret_cast<unsigned char*>(&eax3);
}

int64_t nstrftime();

struct s6 {
    signed char[76295] pad76295;
    void** f12a07;
};

/* debug_strfdatetime.constprop.0 */
void** debug_strfdatetime_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, ...) {
    void** rsi5;
    void** rdi6;
    void** rax7;
    int64_t rax8;
    int64_t rbx9;
    void** edi10;
    void** rsi11;
    void* rax12;
    int64_t rdx13;
    int64_t v14;
    void** rdi15;
    struct s6* rcx16;
    void** rsi17;
    void** r8_18;
    int64_t v19;
    uint32_t eax20;

    *reinterpret_cast<int32_t*>(&rsi5) = 100;
    *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
    rdi6 = rdx;
    rax7 = g28;
    rax8 = nstrftime();
    if (rsi && ((rbx9 = rax8, *reinterpret_cast<int32_t*>(&rax8) <= 99) && *reinterpret_cast<int64_t*>(rsi + 0xd8))) {
        edi10 = *reinterpret_cast<void***>(rsi + 24);
        rsi11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 48 - 8 + 8);
        time_zone_str(edi10, rsi11, edi10, rsi11);
        rdi6 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx9)) + reinterpret_cast<unsigned char>(rdx));
        rsi5 = reinterpret_cast<void**>(static_cast<int64_t>(100 - *reinterpret_cast<int32_t*>(&rbx9)));
        fun_34c0(rdi6, rsi5, 1, 0xffffffffffffffff, " TZ=%s", rdi6, rsi5, 1, 0xffffffffffffffff, " TZ=%s");
    }
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (!rax12) {
        return rdx;
    }
    fun_35e0();
    if (*reinterpret_cast<signed char*>(rdi6 + 0xe8)) 
        goto addr_5426_7;
    *reinterpret_cast<void***>(rsi5) = reinterpret_cast<void**>(0);
    rdx13 = *reinterpret_cast<int32_t*>(rdi6 + 16);
    if (*reinterpret_cast<uint32_t*>(&rdx13) > 6) {
        addr_546c_9:
        goto v14;
    } else {
        rdi15 = rsi5;
        *reinterpret_cast<int32_t*>(&rcx16) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rsi17) = 100;
        *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
    }
    addr_5499_11:
    r8_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(" %s") + reinterpret_cast<uint64_t>(rcx16));
    fun_34c0(rdi15, rsi17, 1, 0xffffffffffffffff, r8_18, rdi15, rsi17, 1, 0xffffffffffffffff, r8_18);
    goto v19;
    addr_5426_7:
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 8) + 1) <= reinterpret_cast<unsigned char>(13)) {
        eax20 = fun_3630(rsi5, 100, "%s");
    } else {
        eax20 = fun_34c0(rsi5, 100, 1, 0xffffffffffffffff, "%ld");
    }
    rdx13 = *reinterpret_cast<int32_t*>(rdi6 + 16);
    if (*reinterpret_cast<uint32_t*>(&rdx13) > 6) 
        goto addr_546c_9;
    if (eax20 > 99) 
        goto addr_546c_9;
    *reinterpret_cast<int32_t*>(&rcx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx16) = reinterpret_cast<uint1_t>(eax20 == 0);
    rsi17 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(100 - eax20)));
    rdi15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi5) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax20))));
    goto addr_5499_11;
}

int32_t fun_38d0(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

struct s7 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
};

void** time_zone_str(void** edi, void** rsi, ...) {
    int64_t rbx3;
    int64_t rbp4;
    int64_t rax5;
    int64_t r8_6;
    uint64_t rbx7;
    int32_t ebx8;
    int64_t r9_9;
    int32_t eax10;
    int32_t ebp11;
    struct s7* rax12;
    int32_t ecx13;
    int64_t rdx14;
    uint64_t rdx15;
    int64_t rsi16;
    uint64_t rsi17;
    int32_t r9d18;
    int32_t esi19;
    signed char* rsi20;
    int32_t edx21;
    int32_t ecx22;
    int64_t rdx23;
    uint64_t rdx24;
    int32_t esi25;
    int32_t edx26;
    int32_t ecx27;

    rbx3 = reinterpret_cast<int32_t>(edi);
    rbp4 = rbx3;
    *reinterpret_cast<uint32_t*>(&rax5) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rbx3) >> 31) & 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_6) = static_cast<int32_t>(rax5 + 43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_6) + 4) = 0;
    rbx7 = rbx3 * 0xffffffff91a2b3c5 >> 32;
    ebx8 = (*reinterpret_cast<int32_t*>(&rbx7) + *reinterpret_cast<int32_t*>(&rbp4) >> 11) - (*reinterpret_cast<int32_t*>(&rbp4) >> 31);
    *reinterpret_cast<int32_t*>(&r9_9) = -ebx8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_9) + 4) = 0;
    if (__intrinsic()) {
        *reinterpret_cast<int32_t*>(&r9_9) = ebx8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_9) + 4) = 0;
    }
    eax10 = fun_38d0(rsi, 1, -1, "%c%02d", r8_6, r9_9);
    ebp11 = *reinterpret_cast<int32_t*>(&rbp4) - ebx8 * reinterpret_cast<int32_t>("ort");
    if (ebp11) {
        rax12 = reinterpret_cast<struct s7*>(static_cast<int64_t>(eax10) + reinterpret_cast<unsigned char>(rsi));
        ecx13 = -ebp11;
        if (__intrinsic()) {
            ecx13 = ebp11;
        }
        rax12->f0 = 58;
        *reinterpret_cast<int32_t*>(&rdx14) = ecx13;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
        rdx15 = reinterpret_cast<uint64_t>(rdx14 * 0x88888889) >> 37;
        *reinterpret_cast<int32_t*>(&rsi16) = *reinterpret_cast<int32_t*>(&rdx15);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi16) + 4) = 0;
        rsi17 = reinterpret_cast<uint64_t>(rsi16 * 0xcccccccd) >> 35;
        r9d18 = static_cast<int32_t>(rsi17 + 48);
        esi19 = static_cast<int32_t>(rsi17 + rsi17 * 4);
        rax12->f1 = *reinterpret_cast<signed char*>(&r9d18);
        rsi20 = &rax12->f3;
        edx21 = *reinterpret_cast<int32_t*>(&rdx15) - (esi19 + esi19) + 48;
        ecx22 = ecx13 - *reinterpret_cast<int32_t*>(&rdx15) * 60;
        rax12->f2 = *reinterpret_cast<signed char*>(&edx21);
        if (ecx22) {
            *reinterpret_cast<int32_t*>(&rdx23) = ecx22;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
            rax12->f3 = 58;
            rdx24 = reinterpret_cast<uint64_t>(rdx23 * 0xcccccccd) >> 35;
            esi25 = static_cast<int32_t>(rdx24 + 48);
            edx26 = static_cast<int32_t>(rdx24 + rdx24 * 4);
            rax12->f4 = *reinterpret_cast<signed char*>(&esi25);
            rsi20 = &rax12->f6;
            ecx27 = ecx22 - (edx26 + edx26) + 48;
            rax12->f5 = *reinterpret_cast<signed char*>(&ecx27);
        }
        *rsi20 = 0;
    }
    return rsi;
}

void** tm_year_str(int32_t edi, void** rsi, ...) {
    int64_t rax3;
    int32_t eax4;
    int32_t eax5;
    int32_t edx6;
    int64_t rcx7;
    int64_t r9_8;
    int64_t r8_9;

    rax3 = edi * 0x51eb851f >> 37;
    eax4 = *reinterpret_cast<int32_t*>(&rax3) - (edi >> 31);
    eax5 = eax4 + 19;
    edx6 = edi - eax4 * 100;
    *reinterpret_cast<int32_t*>(&rcx7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx7) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx7) = reinterpret_cast<uint1_t>(edi >= 0xfffff894);
    *reinterpret_cast<int32_t*>(&r9_8) = -edx6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_8) + 4) = 0;
    if (__intrinsic()) {
        *reinterpret_cast<int32_t*>(&r9_8) = edx6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_8) + 4) = 0;
    }
    *reinterpret_cast<int32_t*>(&r8_9) = -eax5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_9) + 4) = 0;
    if (__intrinsic()) {
        *reinterpret_cast<int32_t*>(&r8_9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_9) + 4) = 0;
    }
    fun_38d0(rsi, 1, -1, rcx7 + reinterpret_cast<int64_t>("-%02d%02d"), r8_9, r9_8);
    return rsi;
}

void** fun_35d0(void** rdi, ...);

void gettime(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_3520(void** rdi, void** rsi, void** rdx, ...);

int64_t localtime_rz(void** rdi, void** rsi, void** rdx, ...);

void fun_34d0(void** rdi, ...);

int64_t fun_3480(void** rdi, void** rsi, void** rdx);

void tzfree(void** rdi, void** rsi, ...);

int32_t yyparse(void** rdi, void** rsi, void** rdx);

void** mktime_z(void** rdi, void** rsi, ...);

void** tzalloc(void** rdi, void** rsi);

void** fun_3780(void** rdi, void** rsi, ...);

struct s8 {
    unsigned char f0;
    void** f1;
};

uint32_t parse_datetime_body(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rbp9;
    void** v10;
    uint32_t v11;
    void** v12;
    void** rax13;
    void** v14;
    void** rax15;
    void* rsp16;
    void** v17;
    void** rbx18;
    void** r14_19;
    void** v20;
    uint32_t eax21;
    int32_t eax22;
    void*** rsp23;
    void** r15_24;
    void** rsi25;
    int64_t rax26;
    void** v27;
    void** r13_28;
    void** rdx29;
    uint32_t eax30;
    void** rdi31;
    void** rax32;
    uint32_t edx33;
    void* rax34;
    void** rax35;
    void** rax36;
    int32_t v37;
    void** rdx38;
    int64_t rax39;
    int32_t v40;
    void** rax41;
    unsigned char v42;
    void** rsi43;
    void** rax44;
    void* rsp45;
    int32_t eax46;
    void** rax47;
    void** rbp48;
    void** rax49;
    void** rdx50;
    void* rsp51;
    void** rbp52;
    int64_t rax53;
    void* rsp54;
    void** rdx55;
    void** rax56;
    void** rax57;
    int64_t rax58;
    void** rsi59;
    void** v60;
    void** rax61;
    void* rsp62;
    void** rdx63;
    void** rax64;
    void** rax65;
    int1_t zf66;
    uint32_t eax67;
    void** v68;
    void** v69;
    int32_t v70;
    void** v71;
    void** r14_72;
    int64_t v73;
    int32_t v74;
    int64_t v75;
    int32_t v76;
    void** v77;
    int32_t v78;
    void** v79;
    int32_t v80;
    void** v81;
    int32_t v82;
    int16_t v83;
    int32_t v84;
    void** rdx85;
    void** v86;
    void** v87;
    int32_t v88;
    void** v89;
    int32_t r13d90;
    int64_t rax91;
    void** v92;
    int32_t v93;
    int32_t eax94;
    int32_t eax95;
    void** rax96;
    void* rsp97;
    void** rax98;
    void* rsp99;
    void** rdi100;
    void** rax101;
    int32_t eax102;
    void** rax103;
    void** rdi104;
    void** rdx105;
    void* rsp106;
    void** rax107;
    void** rdi108;
    void** rax109;
    void** rdi110;
    void** rsi111;
    void** v112;
    void** rax113;
    void** rdi114;
    int32_t v115;
    uint64_t rax116;
    uint32_t edx117;
    uint32_t v118;
    void* rbx119;
    void** rax120;
    void** rsi121;
    uint32_t edx122;
    int64_t v123;
    void** rax124;
    int32_t edx125;
    int64_t rax126;
    int64_t rax127;
    int64_t v128;
    void** rdx129;
    void** rax130;
    void** r13_131;
    void** r10_132;
    int64_t v133;
    uint32_t r11d134;
    uint32_t edi135;
    void** rbx136;
    void* rsp137;
    void** rax138;
    void* rsp139;
    void** rdi140;
    void** rax141;
    int32_t eax142;
    int32_t v143;
    int64_t v144;
    void** r13_145;
    void** rdx146;
    int64_t v147;
    int64_t v148;
    void** rax149;
    unsigned char al150;
    void** rsi151;
    void** v152;
    void** rax153;
    void** rax154;
    unsigned char al155;
    uint32_t eax156;
    void** r8_157;
    uint32_t v158;
    uint32_t v159;
    uint32_t eax160;
    void** rax161;
    void* rsp162;
    void** rax163;
    void** rax164;
    void** rax165;
    void** rax166;
    void* rsp167;
    void** rax168;
    void* rsp169;
    void** rax170;
    void** rax171;
    void** rax172;
    void** rax173;
    void** rax174;
    void* rsp175;
    void** rax176;
    void** rax177;
    void** rax178;
    void** rax179;
    int32_t r13d180;
    void** r9_181;
    int64_t rax182;
    uint32_t eax183;
    uint32_t eax184;
    void** rbx185;
    int64_t rax186;
    void** rdi187;
    int64_t rax188;
    int32_t eax189;
    int32_t eax190;
    uint32_t esi191;
    int64_t rcx192;
    int64_t r9_193;
    int64_t r8_194;
    void** r11_195;
    void** r10_196;
    void** rax197;
    void** rax198;
    void** rax199;
    void** rdx200;
    void** rax201;
    void** rax202;
    uint32_t eax203;
    void** r9_204;
    uint32_t ecx205;
    int64_t rax206;
    void** rdx207;
    int32_t v208;
    uint64_t rdi209;
    uint64_t v210;
    void** rdx211;
    void** rax212;
    void** rax213;
    void** rax214;
    void** rax215;
    int32_t edx216;
    void** rax217;
    int32_t v218;
    int64_t rcx219;
    int64_t v220;
    int64_t rax221;
    int32_t v222;
    int32_t eax223;
    int32_t v224;
    uint64_t rdx225;
    int64_t rdx226;
    void** rax227;
    void** rdx228;
    uint32_t eax229;
    void* rsp230;
    void** rax231;
    void* rsp232;
    void** rax233;
    void* rax234;
    void** rax235;
    void** rax236;
    void* rsp237;
    void** rax238;
    void* rsp239;
    void** rax240;
    void** rsi241;
    void** rax242;
    void** rax243;
    void** rax244;
    void** v245;
    void** rax246;
    void** rax247;
    void** rdx248;
    void** rax249;
    void** rax250;
    void* rsp251;
    void** rax252;
    void** rax253;
    void** rax254;
    void** rax255;
    void* rsp256;
    void** rax257;
    void** rax258;
    void** rcx259;
    void** rdx260;
    int64_t rax261;
    void** rax262;
    void** rax263;
    void** rdx264;
    uint64_t rdi265;
    int32_t v266;
    uint64_t v267;
    void** rdx268;
    void** rax269;
    void* rsp270;
    void** v271;
    void** rax272;
    void** rax273;
    uint32_t v274;
    uint64_t rax275;
    uint32_t edx276;
    void** rax277;
    void** rdx278;
    void** rax279;
    void* rsp280;
    void** rax281;
    void** rax282;
    void** rax283;
    void** rax284;
    void** rdx285;
    void* rax286;
    struct s8* rax287;
    void** rax288;
    uint32_t eax289;
    int64_t rax290;

    r13_7 = rdx;
    r12_8 = r8;
    rbp9 = rsi;
    v10 = rdi;
    v11 = *reinterpret_cast<uint32_t*>(&rcx);
    v12 = r9;
    rax13 = g28;
    v14 = rax13;
    rax15 = fun_35d0(rsi);
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x458 - 8 + 8);
    v17 = rax15;
    if (!r13_7) {
        r13_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 96);
        gettime(r13_7, rsi, rdx, rcx, r8, r9);
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
    }
    rbx18 = *reinterpret_cast<void***>(r13_7);
    r14_19 = rbp9;
    v20 = *reinterpret_cast<void***>(r13_7 + 8);
    while (1) {
        eax21 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_19));
        if (*reinterpret_cast<signed char*>(&eax21) > 13) {
            if (*reinterpret_cast<signed char*>(&eax21) != 32) 
                break;
        } else {
            if (*reinterpret_cast<signed char*>(&eax21) <= 8) 
                break;
        }
        ++r14_19;
    }
    eax22 = fun_3520(r14_19, "TZ=\"", 4);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
    if (eax22 || (*reinterpret_cast<uint32_t*>(&r15_24) = reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(r14_19 + 4)), *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0, r8 = r14_19 + 4, *reinterpret_cast<signed char*>(&r15_24) == 0)) {
        addr_7a58_9:
        rsi25 = r13_7;
        rax26 = localtime_rz(r12_8, rsi25, rsp23 + 0xf0);
        rsp23 = rsp23 - 8 + 8;
        if (!rax26) {
            addr_8470_10:
            v27 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        } else {
            v27 = reinterpret_cast<void**>(0);
            r15_24 = r12_8;
            goto addr_7a7f_12;
        }
    } else {
        rdx29 = r8;
        eax30 = *reinterpret_cast<uint32_t*>(&r15_24);
        *reinterpret_cast<int32_t*>(&rdi31) = 1;
        *reinterpret_cast<int32_t*>(&rdi31 + 4) = 0;
        do {
            if (*reinterpret_cast<signed char*>(&eax30) != 92) {
                if (*reinterpret_cast<signed char*>(&eax30) == 34) 
                    goto addr_8288_16;
                rax32 = rdx29;
            } else {
                rax32 = rdx29 + 1;
                edx33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx29 + 1));
                if (*reinterpret_cast<signed char*>(&edx33) == 92) 
                    continue;
                if (*reinterpret_cast<signed char*>(&edx33) != 34) 
                    goto addr_7a51_20;
            }
            rdx29 = rax32 + 1;
            eax30 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax32 + 1));
            ++rdi31;
        } while (*reinterpret_cast<signed char*>(&eax30));
        goto addr_7a58_9;
    }
    addr_81e8_22:
    while (fun_34d0(v27, v27), rax34 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28)), !!rax34) {
        fun_35e0();
        rsp23 = rsp23 - 8 + 8 - 8 + 8;
        addr_9923_24:
        rax35 = fun_35b0();
        rcx = rcx;
        *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(&rbx18);
        *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
        dbg_printf(rax35, r14_19, r13_28, rcx, r8, r9, v27, rax35, r14_19, r13_28, rcx, r8, r9, v27);
        rax36 = fun_35b0();
        rsi25 = rbp9;
        dbg_printf(rax36, rsi25, 5, rcx, r8, r9, v27, rax36, rsi25, 5, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        if (v37 != -1 && ((rsi25 = v10, rdx38 = reinterpret_cast<void**>(rsp23 + 0x170), rax39 = localtime_rz(r15_24, rsi25, rdx38, r15_24, rsi25, rdx38), rsp23 = rsp23 - 8 + 8, !!rax39) && v37 != v40)) {
            rsi25 = reinterpret_cast<void**>("warning: daylight saving time changed after time adjustment\n");
            rax41 = fun_35b0();
            dbg_printf(rax41, "warning: daylight saving time changed after time adjustment\n", 5, rcx, r8, r9, v27, rax41, "warning: daylight saving time changed after time adjustment\n", 5, rcx, r8, r9, v27);
            rsp23 = rsp23 - 8 + 8 - 8 + 8;
        }
        addr_8feb_26:
        *reinterpret_cast<uint32_t*>(&r13_28) = v42;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        addr_8537_27:
        if (!*reinterpret_cast<signed char*>(&r13_28)) {
            *reinterpret_cast<uint32_t*>(&r13_28) = 1;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        } else {
            rsi43 = reinterpret_cast<void**>("timezone: system default\n");
            if (!v12) {
                addr_8579_30:
                rax44 = fun_35b0();
                dbg_printf(rax44, rsi43, 5, rcx, r8, r9, v27, rax44, rsi43, 5, rcx, r8, r9, v27);
                rsp45 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
            } else {
                eax46 = fun_3700(v12, "UTC0", v12, "UTC0");
                rsp23 = rsp23 - 8 + 8;
                if (eax46) {
                    rax47 = fun_35b0();
                    dbg_printf(rax47, v12, 5, rcx, r8, r9, v27, rax47, v12, 5, rcx, r8, r9, v27);
                    rsp45 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
                } else {
                    rsi43 = reinterpret_cast<void**>("timezone: Universal Time\n");
                    goto addr_8579_30;
                }
            }
            rbx18 = v10;
            rbp48 = *reinterpret_cast<void***>(rbx18);
            r14_19 = *reinterpret_cast<void***>(rbx18 + 8);
            *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
            rax49 = fun_35b0();
            rdx50 = r14_19;
            *reinterpret_cast<int32_t*>(&rdx50 + 4) = 0;
            dbg_printf(rax49, rbp48, rdx50, rcx, r8, r9, v27, rax49, rbp48, rdx50, rcx, r8, r9, v27);
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8 - 8 + 8);
            rbp52 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x130);
            rax53 = fun_3480(rbx18, rbp52, rdx50);
            rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            if (rax53) {
                rdx55 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 0x300);
                rax56 = debug_strfdatetime_constprop_0(rbp52, 0, rdx55, rcx, rbp52, 0, rdx55, rcx);
                rax57 = fun_35b0();
                dbg_printf(rax57, rax56, 5, rcx, r8, r9, v27, rax57, rax56, 5, rcx, r8, r9, v27);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8 - 8 + 8 - 8 + 8);
            }
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 0x170);
            rsi25 = v10;
            rax58 = localtime_rz(r15_24, rsi25, rbp9, r15_24, rsi25, rbp9);
            rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            if (rax58) {
                rsi59 = reinterpret_cast<void**>(rsp23 + 0x2e0);
                rax61 = time_zone_str(v60, rsi59, v60, rsi59);
                rsp62 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                rdx63 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp62) + 0x300);
                r14_19 = rax61;
                rax64 = debug_strfdatetime_constprop_0(rbp9, 0, rdx63, rcx, rbp9, 0, rdx63, rcx);
                rbp9 = rax64;
                rax65 = fun_35b0();
                rsi25 = rbp9;
                dbg_printf(rax65, rsi25, r14_19, rcx, r8, r9, v27, rax65, rsi25, r14_19, rcx, r8, r9, v27);
                rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp62) - 8 + 8 - 8 + 8 - 8 + 8);
            }
        }
        addr_81db_38:
        if (r15_24 == r12_8) 
            continue;
        tzfree(r15_24, rsi25, r15_24, rsi25);
        rsp23 = rsp23 - 8 + 8;
    }
    return *reinterpret_cast<uint32_t*>(&r13_28);
    addr_7a7f_12:
    zf66 = *reinterpret_cast<void***>(r14_19) == 0;
    if (zf66) {
        r14_19 = reinterpret_cast<void**>("0");
    }
    eax67 = *reinterpret_cast<unsigned char*>(&v11);
    v68 = r14_19;
    v42 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax67) & 1);
    v69 = reinterpret_cast<void**>(v70 + 0x76c);
    v71 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v20)));
    r14_72 = reinterpret_cast<void**>(rsp23 + 0x170);
    v73 = v74 + 1;
    v75 = v76;
    v77 = reinterpret_cast<void**>(static_cast<int64_t>(v78));
    v79 = reinterpret_cast<void**>(static_cast<int64_t>(v80));
    v81 = reinterpret_cast<void**>(static_cast<int64_t>(v82));
    v83 = 0;
    v37 = v84;
    rdx85 = v86;
    v87 = rdx85;
    v88 = v84;
    v89 = reinterpret_cast<void**>(0);
    r13d90 = 0x76a700;
    do {
        if (__intrinsic()) 
            break;
        rsi25 = reinterpret_cast<void**>(rsp23 + 0x130);
        rdx85 = r14_72;
        rax91 = localtime_rz(r15_24, rsi25, rdx85, r15_24, rsi25, rdx85);
        rsp23 = rsp23 - 8 + 8;
        if (!rax91) 
            continue;
        if (!v92) 
            continue;
        *reinterpret_cast<int32_t*>(&rdx85) = v93;
        *reinterpret_cast<int32_t*>(&rdx85 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx85) != v88) 
            goto addr_8924_47;
        r13d90 = r13d90 + 0x76a700;
    } while (r13d90 != 0x1da9c00);
    addr_7cb5_49:
    if (v87 && ((rsi25 = v89, !!rsi25) && (eax94 = fun_3700(v87, rsi25, v87, rsi25), rsp23 = rsp23 - 8 + 8, eax94 == 0))) {
    }
    r14_19 = reinterpret_cast<void**>(rsp23 + 0x1b0);
    eax95 = yyparse(r14_19, rsi25, rdx85);
    rsp23 = rsp23 - 8 + 8;
    if (eax95) {
        *reinterpret_cast<uint32_t*>(&r13_28) = v42;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&r13_28)) 
            goto addr_81db_38;
        rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(v17));
        if (reinterpret_cast<unsigned char>(v68) >= reinterpret_cast<unsigned char>(rbp9)) 
            goto addr_8261_54;
    } else {
        if (v42) {
            rax96 = fun_35b0();
            dbg_printf(rax96, "input timezone: ", 5, rcx, r8, r9, v27, rax96, "input timezone: ", 5, rcx, r8, r9, v27);
            rsp97 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
            if (0) 
                goto addr_8490_57;
            if (0) 
                goto addr_8490_57;
            rbx18 = v12;
            if (!rbx18) 
                goto addr_8480_60; else 
                goto addr_83e0_61;
        } else {
            *reinterpret_cast<uint32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
            if (0) {
                __asm__("movdqu xmm0, [rsp+0x208]");
                __asm__("movups [rax], xmm0");
                goto addr_81db_38;
            }
            if (0) 
                goto addr_81d8_65; else 
                goto addr_7d59_66;
        }
    }
    rax98 = fun_35b0();
    rsp99 = reinterpret_cast<void*>(rsp23 - 8 + 8);
    rdi100 = rax98;
    addr_8272_68:
    rsi25 = v68;
    *reinterpret_cast<uint32_t*>(&r13_28) = 0;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    dbg_printf(rdi100, rsi25, 5, rcx, r8, r9, v27, rdi100, rsi25, 5, rcx, r8, r9, v27);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp99) - 8 + 8);
    goto addr_81db_38;
    addr_8261_54:
    rax101 = fun_35b0();
    rsp99 = reinterpret_cast<void*>(rsp23 - 8 + 8);
    rdi100 = rax101;
    goto addr_8272_68;
    addr_8480_60:
    goto addr_8490_57;
    addr_83e0_61:
    if (r12_8 == r15_24) {
        eax102 = fun_3700(v12, "UTC0", v12, "UTC0");
        rsp97 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8);
        if (!eax102) {
            addr_8490_57:
            rax103 = fun_35b0();
            rdi104 = stderr;
            rdx105 = rax103;
            fun_3880(rdi104, rdi104);
            rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8 - 8 + 8);
        } else {
            rax107 = fun_35b0();
            rcx = v12;
            rdi108 = stderr;
            rdx105 = rax107;
            fun_3880(rdi108, rdi108);
            rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8 - 8 + 8);
        }
    } else {
        rax109 = fun_35b0();
        rdi110 = stderr;
        rcx = rbx18;
        rdx105 = rax109;
        fun_3880(rdi110, rdi110);
        rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8 - 8 + 8);
    }
    if (1) {
        addr_86b0_73:
        if (0) {
            addr_84cd_74:
            rsi111 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp106) + 0x2e0);
            rax113 = time_zone_str(v112, rsi111, v112, rsi111);
            rdi114 = stderr;
            rdx105 = reinterpret_cast<void**>(" (%s)");
            rcx = rax113;
            fun_3880(rdi114, rdi114);
            rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8 - 8 + 8);
            rsi25 = stderr;
        } else {
            rsi25 = stderr;
        }
    } else {
        if (!1) 
            goto addr_84cd_74;
        rsi25 = stderr;
        if (!(reinterpret_cast<uint1_t>(v115 < 0) | reinterpret_cast<uint1_t>(v115 == 0))) 
            goto addr_8693_78;
    }
    fun_36b0(10, rsi25, rdx105);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
    *reinterpret_cast<uint32_t*>(&r13_28) = v42;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    if (!1) {
        __asm__("movdqu xmm1, [rsp+0x208]");
        __asm__("movups [rax], xmm1");
        goto addr_8537_27;
    }
    rsi25 = reinterpret_cast<void**>(0);
    rcx = reinterpret_cast<void**>(0);
    if (!1) {
        if (*reinterpret_cast<signed char*>(&r13_28)) {
            if (0) {
                dbg_printf("error: seen multiple time parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple time parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
                rcx = reinterpret_cast<void**>(0);
            }
            if (0) {
                dbg_printf("error: seen multiple date parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple date parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
            }
            if (0) {
                dbg_printf("error: seen multiple days parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple days parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
            }
            if (0) {
                dbg_printf("error: seen multiple daylight-saving parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple daylight-saving parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
            }
            if (!1) {
                dbg_printf("error: seen multiple time-zone parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple time-zone parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
                goto addr_81d8_65;
            }
        }
    }
    addr_7d59_66:
    rbp9 = v69;
    if (reinterpret_cast<signed char>(rbp9) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<uint32_t*>(&rcx) = 0;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rax116 = reinterpret_cast<uint64_t>(0xfffffffffffff894 - reinterpret_cast<unsigned char>(rbp9));
        *reinterpret_cast<unsigned char*>(&rcx) = __intrinsic();
        edx117 = 1;
        if (rax116 == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax116))) {
            edx117 = *reinterpret_cast<uint32_t*>(&rcx);
        }
        v118 = edx117;
    } else {
        if (0) {
            *reinterpret_cast<int32_t*>(&rbx119) = 0x7d0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx119) + 4) = 0;
            if (reinterpret_cast<signed char>(rbp9) >= reinterpret_cast<signed char>(69)) {
                rbx119 = reinterpret_cast<void*>(0x76c);
            }
            rbx18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx119) + reinterpret_cast<unsigned char>(rbp9));
            if (*reinterpret_cast<signed char*>(&r13_28)) {
                rax120 = fun_35b0();
                rsi121 = rbp9;
                rbp9 = rbx18;
                dbg_printf(rax120, rsi121, rbx18, rcx, r8, r9, v27, rax120, rsi121, rbx18, rcx, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8 - 8 + 8;
            } else {
                rbp9 = rbx18;
            }
        }
        edx122 = 0;
        rax116 = reinterpret_cast<uint64_t>(rbp9 + 0xfffffffffffff894);
        if (reinterpret_cast<unsigned char>(rbp9) >= reinterpret_cast<unsigned char>(0x76c)) 
            goto addr_8816_103; else 
            goto addr_7d8f_104;
    }
    addr_7daf_105:
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v123) + 4) = *reinterpret_cast<uint32_t*>(&rax116);
    if (*reinterpret_cast<unsigned char*>(&v118)) {
        if (*reinterpret_cast<signed char*>(&r13_28)) {
            rax124 = fun_35b0();
            dbg_printf(rax124, rbp9, 5, rcx, r8, r9, v27, rax124, rbp9, 5, rcx, r8, r9, v27);
            rsp23 = rsp23 - 8 + 8 - 8 + 8;
        }
    } else {
        edx125 = 0;
        *reinterpret_cast<uint32_t*>(&rsi25) = v42;
        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
        rax126 = v73 - 1;
        *reinterpret_cast<unsigned char*>(&edx125) = __intrinsic();
        *reinterpret_cast<uint32_t*>(&v123) = *reinterpret_cast<uint32_t*>(&rax126);
        if (rax126 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax126))) {
            edx125 = 1;
        }
        *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&rax126);
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        if (edx125) 
            goto addr_88ce_111;
        rax127 = v75;
        *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v128) + 4) = *reinterpret_cast<uint32_t*>(&rax127);
        *reinterpret_cast<uint32_t*>(&rdx129) = *reinterpret_cast<uint32_t*>(&rax127);
        *reinterpret_cast<int32_t*>(&rdx129 + 4) = 0;
        if (rax127 == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax127))) 
            goto addr_7e13_113;
    }
    addr_88ce_111:
    rsi25 = reinterpret_cast<void**>("error: year, month, or day overflow\n");
    if (!v42) {
        addr_81d8_65:
        *reinterpret_cast<uint32_t*>(&r13_28) = 0;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        goto addr_81db_38;
    } else {
        addr_88e8_114:
        rax130 = fun_35b0();
        dbg_printf(rax130, rsi25, 5, rcx, r8, r9, v27, rax130, rsi25, 5, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8;
        goto addr_81d8_65;
    }
    addr_7e13_113:
    if (0) {
        addr_8b53_115:
        *reinterpret_cast<uint32_t*>(&r9) = 2;
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        rbp9 = v77;
        if (0) {
            if (reinterpret_cast<uint64_t>(rbp9 + 0xffffffffffffffff) <= 10) {
                addr_8b80_117:
                *reinterpret_cast<uint32_t*>(&r9) = *reinterpret_cast<uint32_t*>(&rbp9);
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                goto addr_8b83_118;
            } else {
                r13_131 = reinterpret_cast<void**>("am");
                if (rbp9 == 12) {
                    addr_8b83_118:
                    r8 = v79;
                    r10_132 = v81;
                    *reinterpret_cast<uint32_t*>(&v128) = *reinterpret_cast<uint32_t*>(&r9);
                    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v133) + 4) = *reinterpret_cast<uint32_t*>(&r8);
                    r11d134 = *reinterpret_cast<uint32_t*>(&r8);
                    edi135 = *reinterpret_cast<uint32_t*>(&r10_132);
                    *reinterpret_cast<uint32_t*>(&v133) = *reinterpret_cast<uint32_t*>(&r10_132);
                    if (*reinterpret_cast<unsigned char*>(&rsi25)) {
                        rbx136 = reinterpret_cast<void**>(rsp23 + 0x300);
                        fun_34c0(rbx136, 100, 1, 100, "%02d:%02d:%02d", rbx136, 100, 1, 100, "%02d:%02d:%02d");
                        r8 = r8;
                        rsp137 = reinterpret_cast<void*>(rsp23 - 8 - 8 - 8 + 8 + 8 + 8);
                        if (1) {
                            rax138 = fun_35b0();
                            rsp139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp137) - 8 + 8);
                            rdi140 = rax138;
                        } else {
                            rax141 = fun_35b0();
                            rsp139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp137) - 8 + 8);
                            rdi140 = rax141;
                        }
                        dbg_printf(rdi140, rbx136, 5, 100, r8, r10_132, v27, rdi140, rbx136, 5, 100, r8, r10_132, v27);
                        rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp139) - 8 + 8);
                    } else {
                        addr_8bb1_124:
                        if (1) {
                            addr_7e89_125:
                            if (0) {
                                eax142 = v143;
                                v37 = eax142;
                                goto addr_7e9f_127;
                            } else {
                                eax142 = v37;
                                goto addr_7e9f_127;
                            }
                        } else {
                            goto addr_7e7e_130;
                        }
                    }
                } else {
                    goto addr_93f5_132;
                }
            }
        } else {
            if (0) {
                *reinterpret_cast<uint32_t*>(&r9) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rbp9 + 12));
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                if (reinterpret_cast<uint64_t>(rbp9 + 0xffffffffffffffff) <= 10) 
                    goto addr_8b83_118;
                *reinterpret_cast<uint32_t*>(&r9) = 12;
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                r13_131 = reinterpret_cast<void**>("pm");
                if (rbp9 == 12) 
                    goto addr_8b83_118; else 
                    goto addr_93f5_132;
            } else {
                if (reinterpret_cast<unsigned char>(rbp9) <= reinterpret_cast<unsigned char>(23)) 
                    goto addr_8b80_117;
                r13_131 = reinterpret_cast<void**>(0x12ce0);
                goto addr_93f5_132;
            }
        }
    } else {
        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&v83) + 1)) {
            v133 = 0;
            *reinterpret_cast<uint32_t*>(&v128) = 0;
            v71 = reinterpret_cast<void**>(0);
            if (!*reinterpret_cast<unsigned char*>(&rsi25)) {
                *reinterpret_cast<uint32_t*>(&r9) = 0;
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                r11d134 = 0;
                edi135 = 0;
                goto addr_8bb1_124;
            }
        }
        if (1) 
            goto addr_8b53_115; else 
            goto addr_7e50_142;
    }
    addr_94ee_143:
    edi135 = *reinterpret_cast<uint32_t*>(&v133);
    r11d134 = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v133) + 4);
    *reinterpret_cast<uint32_t*>(&r9) = *reinterpret_cast<uint32_t*>(&v128);
    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdx129) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v128) + 4);
    *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&v123);
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    goto addr_8bb1_124;
    addr_7e9f_127:
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v144) + 4) = *reinterpret_cast<uint32_t*>(&rdx129);
    r13_145 = reinterpret_cast<void**>(rsp23 + 0x70);
    *reinterpret_cast<uint32_t*>(&rdx146) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v123) + 4);
    *reinterpret_cast<int32_t*>(&rdx146 + 4) = 0;
    rbx18 = reinterpret_cast<void**>(rsp23 + 0xb0);
    *reinterpret_cast<uint32_t*>(&v147) = edi135;
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v147) + 4) = r11d134;
    *reinterpret_cast<uint32_t*>(&v144) = *reinterpret_cast<uint32_t*>(&r9);
    *reinterpret_cast<uint32_t*>(&v148) = *reinterpret_cast<uint32_t*>(&rcx);
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v148) + 4) = *reinterpret_cast<uint32_t*>(&rdx146);
    rax149 = mktime_z(r15_24, r13_145);
    rsi25 = r13_145;
    rbp9 = rax149;
    al150 = mktime_ok(rbx18, rsi25);
    rsp23 = rsp23 - 8 + 8 - 8 + 8;
    if (al150) 
        goto addr_8a10_144;
    if (!0) 
        goto addr_7f28_146;
    rsi151 = reinterpret_cast<void**>(rsp23 + 0x3e3);
    rbp9 = reinterpret_cast<void**>(rsp23 + 0x3e0);
    time_zone_str(v152, rsi151, v152, rsi151);
    rax153 = tzalloc(rbp9, rsi151);
    rsp23 = rsp23 - 8 + 8 - 8 + 8;
    r8 = rax153;
    if (!rax153) {
        rsi25 = reinterpret_cast<void**>("error: tzalloc (\"%s\") failed\n");
        if (!v42) 
            goto addr_81d8_65;
    } else {
        v133 = v147;
        v128 = v144;
        v123 = v148;
        v37 = eax142;
        rax154 = mktime_z(r8, r13_145);
        rsi25 = r13_145;
        rbp9 = rax154;
        al155 = mktime_ok(rbx18, rsi25);
        tzfree(r8, rsi25);
        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
        eax156 = al155;
        if (!*reinterpret_cast<signed char*>(&eax156)) {
            addr_7f28_146:
            *reinterpret_cast<uint32_t*>(&r9) = *reinterpret_cast<uint32_t*>(&v147);
            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r8_157) = *reinterpret_cast<uint32_t*>(&v133);
            *reinterpret_cast<int32_t*>(&r8_157 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v147) + 4);
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            *reinterpret_cast<uint32_t*>(&v12) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v144) + 4);
            v158 = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v128) + 4);
            v159 = *reinterpret_cast<uint32_t*>(&v148);
            *reinterpret_cast<uint32_t*>(&v10) = *reinterpret_cast<uint32_t*>(&v123);
            eax160 = v42;
            if (*reinterpret_cast<uint32_t*>(&r9) != *reinterpret_cast<uint32_t*>(&r8_157) || *reinterpret_cast<uint32_t*>(&rcx) != *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v133) + 4)) {
                if (!*reinterpret_cast<signed char*>(&eax160)) 
                    goto addr_81d8_65;
                rax161 = fun_35b0();
                rsp162 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp162) + 0x3e0);
                dbg_printf(rax161, "error: invalid date/time value:\n", 5, rcx, r8_157, r9, v27, rax161, "error: invalid date/time value:\n", 5, rcx, r8_157, r9, v27);
                rax163 = debug_strfdatetime_constprop_0(rbx18, r14_19, rbp9, rcx);
                rbx18 = rax163;
                rax164 = fun_35b0();
                dbg_printf(rax164, rbx18, 5, rcx, r8_157, r9, v27, rax164, rbx18, 5, rcx, r8_157, r9, v27);
                rax165 = debug_strfdatetime_constprop_0(r13_145, r14_19, rbp9, rcx);
                rax166 = fun_35b0();
                dbg_printf(rax166, rax165, 5, rcx, r8_157, r9, v27, rax166, rax165, 5, rcx, r8_157, r9, v27);
                rsp167 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp162) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                if (*reinterpret_cast<uint32_t*>(&r9) == *reinterpret_cast<uint32_t*>(&r8_157)) 
                    goto addr_99e1_153; else 
                    goto addr_97d4_154;
            } else {
                *reinterpret_cast<uint32_t*>(&rsi25) = v158;
                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&v12) != *reinterpret_cast<uint32_t*>(&rsi25) || ((*reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&v128), *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, *reinterpret_cast<uint32_t*>(&v144) == *reinterpret_cast<uint32_t*>(&rsi25)) || ((*reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&v10), *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, v159 != *reinterpret_cast<uint32_t*>(&rsi25)) || (*reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v123) + 4), *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v148) + 4) != *reinterpret_cast<uint32_t*>(&rsi25))))) {
                    if (!*reinterpret_cast<signed char*>(&eax160)) 
                        goto addr_81d8_65;
                    rax168 = fun_35b0();
                    rsp169 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                    rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp169) + 0x3e0);
                    dbg_printf(rax168, "error: invalid date/time value:\n", 5, rcx, r8_157, r9, v27, rax168, "error: invalid date/time value:\n", 5, rcx, r8_157, r9, v27);
                    rax170 = debug_strfdatetime_constprop_0(rbx18, r14_19, rbp9, rcx);
                    rbx18 = rax170;
                    rax171 = fun_35b0();
                    dbg_printf(rax171, rbx18, 5, rcx, r8_157, r9, v27, rax171, rbx18, 5, rcx, r8_157, r9, v27);
                    rax172 = debug_strfdatetime_constprop_0(r13_145, r14_19, rbp9, rcx);
                    rax173 = fun_35b0();
                    dbg_printf(rax173, rax172, 5, rcx, r8_157, r9, v27, rax173, rax172, 5, rcx, r8_157, r9, v27);
                    rsp167 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp169) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_97ed_158;
                } else {
                    if (!*reinterpret_cast<signed char*>(&eax160)) 
                        goto addr_81d8_65;
                    rax174 = fun_35b0();
                    rsp175 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                    rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp175) + 0x3e0);
                    dbg_printf(rax174, "error: invalid date/time value:\n", 5, rcx, r8_157, r9, v27, rax174, "error: invalid date/time value:\n", 5, rcx, r8_157, r9, v27);
                    rax176 = debug_strfdatetime_constprop_0(rbx18, r14_19, rbp9, rcx);
                    rbx18 = rax176;
                    rax177 = fun_35b0();
                    dbg_printf(rax177, rbx18, 5, rcx, r8_157, r9, v27, rax177, rbx18, 5, rcx, r8_157, r9, v27);
                    rax178 = debug_strfdatetime_constprop_0(r13_145, r14_19, rbp9, rcx);
                    rax179 = fun_35b0();
                    r13d180 = 1;
                    dbg_printf(rax179, rax178, 5, rcx, r8_157, r9, v27, rax179, rax178, 5, rcx, r8_157, r9, v27);
                    rsp167 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp175) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_808b_161;
                }
            }
        } else {
            addr_8a10_144:
            if (1) {
                addr_8c6b_162:
                if (!v42) {
                    addr_9033_163:
                    rcx = reinterpret_cast<void**>(0);
                    if (1) {
                        if (1) {
                            addr_9085_165:
                            r14_19 = reinterpret_cast<void**>(0);
                            r9 = v71;
                            *reinterpret_cast<uint32_t*>(&rbx18) = 0;
                            r8 = reinterpret_cast<void**>(0);
                            if (__intrinsic()) 
                                goto addr_81d8_65;
                            r9_181 = r9;
                            rsi25 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r9_181 - ((__intrinsic() >> 26) - (reinterpret_cast<signed char>(r9_181) >> 63)) * 0x3b9aca00) + 0x3b9aca00 - (__intrinsic() >> 11) * 0x3b9aca00);
                            r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r9_181) - reinterpret_cast<unsigned char>(rsi25)) >> 63);
                            rax182 = __intrinsic() >> 26;
                            eax183 = *reinterpret_cast<int32_t*>(&rax182) - *reinterpret_cast<uint32_t*>(&r9);
                            rcx = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp9)));
                            if (__intrinsic()) 
                                goto addr_81d8_65;
                        } else {
                            eax184 = 0;
                            goto addr_936f_168;
                        }
                        r13_28 = reinterpret_cast<void**>(0);
                        if (__intrinsic()) {
                            goto addr_81d8_65;
                        }
                    }
                } else {
                    if (0) {
                        if (0) {
                            addr_945e_173:
                            rbx185 = reinterpret_cast<void**>(rsp23 + 0x300);
                            goto addr_9466_174;
                        } else {
                            rbx185 = reinterpret_cast<void**>(rsp23 + 0x300);
                            goto addr_8ca1_176;
                        }
                    }
                    rbx185 = reinterpret_cast<void**>(rsp23 + 0x300);
                    if (1) {
                        *reinterpret_cast<uint32_t*>(&rax186) = *reinterpret_cast<uint32_t*>(&v123);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax186) + 4) = 0;
                        rdi187 = reinterpret_cast<void**>(rsp23 + 0x2d3);
                        rax188 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v123) + 4) * 0x51eb851f >> 37;
                        eax189 = *reinterpret_cast<int32_t*>(&rax188) - (*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v123) + 4) >> 31);
                        eax190 = eax189 + 19;
                        esi191 = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v123) + 4) - eax189 * 100;
                        *reinterpret_cast<int32_t*>(&rcx192) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx192) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rcx192) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v123) + 4) >= reinterpret_cast<int32_t>(0xfffff894));
                        *reinterpret_cast<uint32_t*>(&r9_193) = -esi191;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_193) + 4) = 0;
                        if (__intrinsic()) {
                            *reinterpret_cast<uint32_t*>(&r9_193) = esi191;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_193) + 4) = 0;
                        }
                        *reinterpret_cast<int32_t*>(&r8_194) = -eax190;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_194) + 4) = 0;
                        if (__intrinsic()) {
                            *reinterpret_cast<int32_t*>(&r8_194) = eax190;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_194) + 4) = 0;
                        }
                        fun_38d0(rdi187, 1, 13, rcx192 + reinterpret_cast<int64_t>("-%02d%02d"), r8_194, r9_193);
                        *reinterpret_cast<uint32_t*>(&r11_195) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v128) + 4);
                        *reinterpret_cast<int32_t*>(&r11_195 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r10_196) = static_cast<int32_t>(rax186 + 1);
                        *reinterpret_cast<int32_t*>(&r10_196 + 4) = 0;
                        r8 = reinterpret_cast<void**>("(Y-M-D) %s-%02d-%02d");
                        r9 = rdi187;
                        fun_34c0(rbx185, 100, 1, 100, "(Y-M-D) %s-%02d-%02d", rbx185, 100, 1, 100, "(Y-M-D) %s-%02d-%02d");
                        rax197 = fun_35b0();
                        dbg_printf(rax197, rbx185, 5, 100, "(Y-M-D) %s-%02d-%02d", r9, r10_196, rax197, rbx185, 5, 100, "(Y-M-D) %s-%02d-%02d", r9, r10_196);
                        rdx146 = r10_196;
                        rcx = r11_195;
                        rsp23 = rsp23 - 8 + 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 + 8 + 8;
                        if (1) 
                            goto addr_8ca1_176; else 
                            goto addr_96a6_183;
                    } else {
                        addr_8ca1_176:
                        rax198 = debug_strfdatetime_constprop_0(r13_145, r14_19, rbx185, rcx, r13_145, r14_19, rbx185, rcx);
                        rax199 = fun_35b0();
                        dbg_printf(rax199, rax198, 5, rcx, r8, r9, v27, rax199, rax198, 5, rcx, r8, r9, v27);
                        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
                        rcx = reinterpret_cast<void**>(0);
                        rsi25 = reinterpret_cast<void**>(0);
                        eax184 = v42;
                        rbx18 = reinterpret_cast<void**>(0);
                        if (1) {
                            addr_9360_184:
                            if (1) {
                                addr_8e92_185:
                                if (*reinterpret_cast<signed char*>(&eax184)) {
                                    rdx200 = reinterpret_cast<void**>(rsp23 + 0x300);
                                    rax201 = debug_strfdatetime_constprop_0(r13_145, r14_19, rdx200, rcx, r13_145, r14_19, rdx200, rcx);
                                    rax202 = fun_35b0();
                                    rsi25 = rax201;
                                    dbg_printf(rax202, rsi25, rbp9, rcx, r8, r9, v27, rax202, rsi25, rbp9, rcx, r8, r9, v27);
                                    rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
                                    r14_19 = reinterpret_cast<void**>(0);
                                    r9 = v71;
                                    *reinterpret_cast<uint32_t*>(&rbx18) = 0;
                                    r8 = reinterpret_cast<void**>(0);
                                    if (__intrinsic()) {
                                        eax203 = v42;
                                        *reinterpret_cast<unsigned char*>(&v118) = *reinterpret_cast<unsigned char*>(&eax203);
                                        goto addr_94c4_188;
                                    } else {
                                        r9_204 = r9;
                                        rsi25 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r9_204 - ((__intrinsic() >> 26) - (reinterpret_cast<signed char>(r9_204) >> 63)) * 0x3b9aca00) + 0x3b9aca00 - (__intrinsic() >> 11) * 0x3b9aca00);
                                        r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r9_204) - reinterpret_cast<unsigned char>(rsi25)) >> 63);
                                        ecx205 = v42;
                                        *reinterpret_cast<unsigned char*>(&v118) = *reinterpret_cast<unsigned char*>(&ecx205);
                                        rax206 = __intrinsic() >> 26;
                                        eax183 = *reinterpret_cast<int32_t*>(&rax206) - *reinterpret_cast<uint32_t*>(&r9);
                                        rcx = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp9)));
                                        if (__intrinsic() || (r13_28 = reinterpret_cast<void**>(0), __intrinsic())) {
                                            addr_94c4_188:
                                            if (!*reinterpret_cast<unsigned char*>(&v118)) 
                                                goto addr_81d8_65;
                                        } else {
                                            rcx = rcx;
                                            rbp9 = rcx;
                                            if (!__intrinsic() && ((rcx = reinterpret_cast<void**>(0), rbp9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp9))), !__intrinsic()) && (rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax183)))), !__intrinsic()))) {
                                                *reinterpret_cast<void***>(v10) = rbp9;
                                                *reinterpret_cast<void***>(v10 + 8) = rsi25;
                                                if (!*reinterpret_cast<unsigned char*>(&v118)) 
                                                    goto addr_8feb_26;
                                                if (0) 
                                                    goto addr_9923_24; else 
                                                    goto addr_8feb_26;
                                            }
                                        }
                                        rsi25 = reinterpret_cast<void**>("error: adding relative time caused an overflow\n");
                                        goto addr_88e8_114;
                                    }
                                }
                            } else {
                                addr_936f_168:
                                rdx207 = reinterpret_cast<void**>(static_cast<int64_t>(v208));
                                *reinterpret_cast<int32_t*>(&rdi209) = 0;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi209) + 4) = 0;
                                rbx18 = rdx207;
                                *reinterpret_cast<unsigned char*>(&rdi209) = __intrinsic();
                                *reinterpret_cast<uint32_t*>(&rsi25) = 0;
                                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) - (reinterpret_cast<unsigned char>(rdx207) - v210));
                                *reinterpret_cast<unsigned char*>(&rsi25) = __intrinsic();
                                rdx211 = rcx;
                                if (rdi209 | reinterpret_cast<unsigned char>(rsi25)) {
                                    if (*reinterpret_cast<signed char*>(&eax184)) {
                                        rax212 = fun_35b0();
                                        *reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&rbx18);
                                        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                        dbg_printf(rax212, rsi25, 5, rcx, r8, r9, v27, rax212, rsi25, 5, rcx, r8, r9, v27);
                                        rsp23 = rsp23 - 8 + 8 - 8 + 8;
                                        goto addr_81d8_65;
                                    }
                                }
                            }
                        } else {
                            if (!*reinterpret_cast<signed char*>(&eax184)) {
                                rax213 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v123) + 4)))));
                                rsi25 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax213)));
                                *reinterpret_cast<uint32_t*>(&rcx) = __intrinsic();
                                *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                                if (rax213 != rsi25) 
                                    goto addr_81d8_65;
                                if (*reinterpret_cast<uint32_t*>(&rcx)) 
                                    goto addr_81d8_65; else 
                                    goto addr_907c_199;
                            } else {
                                if (1) {
                                    addr_958e_201:
                                    if (*reinterpret_cast<uint32_t*>(&v128) != 12) {
                                        rsi25 = reinterpret_cast<void**>("warning: when adding relative days, it is recommended to specify noon\n");
                                        rax214 = fun_35b0();
                                        dbg_printf(rax214, "warning: when adding relative days, it is recommended to specify noon\n", 5, 0, r8, r9, v27, rax214, "warning: when adding relative days, it is recommended to specify noon\n", 5, 0, r8, r9, v27);
                                        rsp23 = rsp23 - 8 + 8 - 8 + 8;
                                    }
                                } else {
                                    if (*reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v128) + 4) != 15) {
                                        rax215 = fun_35b0();
                                        dbg_printf(rax215, "warning: when adding relative months/years, it is recommended to specify the 15th of the months\n", 5, 0, r8, r9, v27, rax215, "warning: when adding relative months/years, it is recommended to specify the 15th of the months\n", 5, 0, r8, r9, v27);
                                        rsp23 = rsp23 - 8 + 8 - 8 + 8;
                                        rsi25 = reinterpret_cast<void**>(0);
                                    }
                                    if (0) 
                                        goto addr_958e_201;
                                }
                                edx216 = 0;
                                rax217 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v123) + 4)))));
                                rcx = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax217)));
                                *reinterpret_cast<unsigned char*>(&edx216) = __intrinsic();
                                if (rax217 != rcx) 
                                    goto addr_9149_207;
                                if (edx216) 
                                    goto addr_9149_207;
                                v218 = *reinterpret_cast<int32_t*>(&rax217);
                                goto addr_8d7f_210;
                            }
                        }
                    }
                }
            } else {
                if (0) {
                    if (!v42) 
                        goto addr_9033_163; else 
                        goto addr_945e_173;
                }
                rcx219 = v220;
                if (!(reinterpret_cast<uint1_t>(rcx219 < 0) | reinterpret_cast<uint1_t>(rcx219 == 0))) {
                    *reinterpret_cast<uint32_t*>(&rax221) = reinterpret_cast<uint1_t>(-1 != v222);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax221) + 4) = 0;
                    rcx219 = rcx219 - rax221;
                }
                rcx = reinterpret_cast<void**>(rcx219 * 7);
                if (__intrinsic()) 
                    goto addr_8ac8_216;
                eax223 = v224 + 1 + 7;
                rdx225 = eax223 * 0xffffffff92492493 >> 32;
                *reinterpret_cast<uint32_t*>(&rdx226) = (*reinterpret_cast<int32_t*>(&rdx225) + eax223 >> 2) - reinterpret_cast<uint32_t>(eax223 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx226) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rsi25) = static_cast<int32_t>(rdx226 * 8) - *reinterpret_cast<uint32_t*>(&rdx226);
                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                if (__intrinsic()) 
                    goto addr_8ac8_216; else 
                    goto addr_8aa0_218;
            }
        }
    }
    addr_9706_219:
    rax227 = fun_35b0();
    rsi25 = rbp9;
    dbg_printf(rax227, rsi25, 5, rcx, r8, r9, v27, rax227, rsi25, 5, rcx, r8, r9, v27);
    rsp23 = rsp23 - 8 + 8 - 8 + 8;
    goto addr_81d8_65;
    addr_99e1_153:
    addr_97f0_220:
    if (*reinterpret_cast<uint32_t*>(&v144) != *reinterpret_cast<uint32_t*>(&v128)) {
    }
    r13d180 = 0;
    if (*reinterpret_cast<uint32_t*>(&v12) != v158) {
        addr_8092_223:
        if (v159 == *reinterpret_cast<uint32_t*>(&v10)) {
        }
    } else {
        goto addr_808b_161;
    }
    r9 = reinterpret_cast<void**>("----");
    if (*reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v148) + 4) == *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v123) + 4)) {
        r9 = reinterpret_cast<void**>(0x12ce0);
    }
    *reinterpret_cast<int32_t*>(&rdx228) = 1;
    *reinterpret_cast<int32_t*>(&rdx228 + 4) = 0;
    eax229 = fun_34c0(rbp9, 100, 1, 100, "                                 %4s %2s %2s %2s %2s %2s", rbp9, 100, 1, 100, "                                 %4s %2s %2s %2s %2s %2s");
    rsp230 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp167) - 8 - 8 - 8 - 8 - 8 - 8 - 8 + 8 + 48);
    if (reinterpret_cast<int32_t>(eax229) < reinterpret_cast<int32_t>(0)) {
        addr_8120_229:
        dbg_printf("%s\n", rbp9, rdx228, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, "%s\n", rbp9, rdx228, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
        rax231 = fun_35b0();
        dbg_printf(rax231, "     possible reasons:\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax231, "     possible reasons:\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
        rsp232 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp230) - 8 + 8 - 8 + 8 - 8 + 8);
        if (r13d180) {
            rax233 = fun_35b0();
            dbg_printf(rax233, "       non-existing due to daylight-saving time;\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax233, "       non-existing due to daylight-saving time;\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
            rsp232 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp232) - 8 + 8 - 8 + 8);
        }
    } else {
        if (reinterpret_cast<int32_t>(eax229) > reinterpret_cast<int32_t>(99)) {
            eax229 = 99;
        }
        rax234 = reinterpret_cast<void*>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax229)));
        do {
            rdx228 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax234)));
            if (!rax234) 
                goto addr_8116_235;
            rax234 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax234) - 1);
        } while (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax234)) == 32);
        goto addr_8118_237;
    }
    *reinterpret_cast<uint32_t*>(&rcx) = v158;
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&v12) != *reinterpret_cast<uint32_t*>(&rcx) && (*reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&v10), *reinterpret_cast<int32_t*>(&rcx + 4) = 0, v159 != *reinterpret_cast<uint32_t*>(&rcx))) {
        rax235 = fun_35b0();
        dbg_printf(rax235, "       invalid day/month combination;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax235, "       invalid day/month combination;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
        rsp232 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp232) - 8 + 8 - 8 + 8);
    }
    rax236 = fun_35b0();
    dbg_printf(rax236, "       numeric values overflow;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax236, "       numeric values overflow;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
    rsp237 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp232) - 8 + 8 - 8 + 8);
    if (1) {
        rax238 = fun_35b0();
        rsp239 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp237) - 8 + 8);
        rsi25 = rax238;
    } else {
        rax240 = fun_35b0();
        rsp239 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp237) - 8 + 8);
        rsi25 = rax240;
    }
    dbg_printf("       %s\n", rsi25, 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, "       %s\n", rsi25, 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp239) - 8 + 8);
    goto addr_81d8_65;
    addr_8116_235:
    *reinterpret_cast<int32_t*>(&rdx228) = 0;
    *reinterpret_cast<int32_t*>(&rdx228 + 4) = 0;
    addr_8118_237:
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp230) + reinterpret_cast<unsigned char>(rdx228) + 0x3e0) = 0;
    goto addr_8120_229;
    addr_808b_161:
    goto addr_8092_223;
    addr_97d4_154:
    if (*reinterpret_cast<uint32_t*>(&rcx) == *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v133) + 4)) {
        goto addr_97f0_220;
    } else {
        addr_97ed_158:
        goto addr_97f0_220;
    }
    addr_96a6_183:
    if (0) {
        addr_9466_174:
        rsi241 = reinterpret_cast<void**>(rsp23 + 0x3e0);
        rax242 = str_days_constprop_0(r14_19, rsi241, rdx146, rcx, r8, r14_19, rsi241, rdx146, rcx, r8);
        rax243 = fun_35b0();
        dbg_printf(rax243, rax242, 5, rcx, r8, r9, v27, rax243, rax242, 5, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_8ca1_176;
    } else {
        goto addr_8ca1_176;
    }
    addr_8e8f_246:
    rbp9 = rdx211;
    goto addr_8e92_185;
    addr_907c_199:
    v218 = *reinterpret_cast<int32_t*>(&rax213);
    addr_8d7f_210:
    rax244 = reinterpret_cast<void**>(static_cast<uint64_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v123)))));
    rcx = rax244;
    v245 = rax244;
    if (!reinterpret_cast<int1_t>(rcx == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rcx))) || (__intrinsic() || ((rbx18 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v128) + 4))))), !reinterpret_cast<int1_t>(rbx18 == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx18)))) || __intrinsic()))) {
        addr_9149_207:
        if (v42) {
            rax246 = fun_35b0();
            rsi25 = reinterpret_cast<void**>("parse-datetime.y");
            dbg_printf(rax246, "parse-datetime.y", 0x863, rcx, r8, r9, v27, rax246, "parse-datetime.y", 0x863, rcx, r8, r9, v27);
            rsp23 = rsp23 - 8 + 8 - 8 + 8;
            goto addr_81d8_65;
        }
    } else {
        rsi25 = r13_145;
        v37 = eax142;
        rax247 = mktime_z(r15_24, rsi25, r15_24, rsi25);
        rsp23 = rsp23 - 8 + 8;
        rbp9 = rax247;
        if (1) {
            if (!v42) 
                goto addr_81d8_65;
            rdx248 = reinterpret_cast<void**>(rsp23 + 0x300);
            rax249 = debug_strfdatetime_constprop_0(r13_145, r14_19, rdx248, rcx, r13_145, r14_19, rdx248, rcx);
            rsp23 = rsp23 - 8 + 8;
            rbp9 = rax249;
            goto addr_9706_219;
        } else {
            eax184 = v42;
            if (*reinterpret_cast<signed char*>(&eax184)) {
                r9 = reinterpret_cast<void**>(0);
                r8 = reinterpret_cast<void**>(0);
                rax250 = fun_35b0();
                rcx = reinterpret_cast<void**>(0);
                dbg_printf(rax250, 0, 0, 0, 0, 0, v27, rax250, 0, 0, 0, 0, 0, v27);
                rsp251 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
                rax252 = debug_strfdatetime_constprop_0(r13_145, r14_19, reinterpret_cast<int64_t>(rsp251) + 0x300, 0);
                rax253 = fun_35b0();
                rsi25 = rax252;
                dbg_printf(rax253, rsi25, 5, 0, 0, 0, v27, rax253, rsi25, 5, 0, 0, 0, v27);
                rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp251) - 8 + 8 - 8 + 8 - 8 + 8);
                if (eax142 != -1 && eax142 != v37) {
                    rsi25 = reinterpret_cast<void**>("warning: daylight saving time changed after date adjustment\n");
                    rax254 = fun_35b0();
                    dbg_printf(rax254, "warning: daylight saving time changed after date adjustment\n", 5, 0, 0, 0, v27, rax254, "warning: daylight saving time changed after date adjustment\n", 5, 0, 0, 0, v27);
                    rsp23 = rsp23 - 8 + 8 - 8 + 8;
                }
                if (!0 && (*reinterpret_cast<uint32_t*>(&rbx18) != *reinterpret_cast<uint32_t*>(&rbx18) || !0 && *reinterpret_cast<int32_t*>(&v245) != *reinterpret_cast<int32_t*>(&v245))) {
                    rax255 = fun_35b0();
                    dbg_printf(rax255, "warning: month/year adjustment resulted in shifted dates:\n", 5, 0, 0, 0, v27, rax255, "warning: month/year adjustment resulted in shifted dates:\n", 5, 0, 0, 0, v27);
                    rsp256 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
                    r8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp256) + 0x2d3);
                    rax257 = tm_year_str(v218, r8, v218, r8);
                    rax258 = fun_35b0();
                    *reinterpret_cast<uint32_t*>(&rcx259) = *reinterpret_cast<uint32_t*>(&rbx18);
                    *reinterpret_cast<int32_t*>(&rcx259 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx260) = *reinterpret_cast<int32_t*>(&v245) + 1;
                    *reinterpret_cast<int32_t*>(&rdx260 + 4) = 0;
                    dbg_printf(rax258, rax257, rdx260, rcx259, r8, 0, v27, rax258, rax257, rdx260, rcx259, r8, 0, v27);
                    *reinterpret_cast<int32_t*>(&rax261) = *reinterpret_cast<int32_t*>(&v245);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax261) + 4) = 0;
                    rax262 = tm_year_str(v218, r8);
                    rax263 = fun_35b0();
                    *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&rbx18);
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    rsi25 = rax262;
                    *reinterpret_cast<int32_t*>(&rdx264) = static_cast<int32_t>(rax261 + 1);
                    *reinterpret_cast<int32_t*>(&rdx264 + 4) = 0;
                    dbg_printf(rax263, rsi25, rdx264, rcx, r8, 0, v27, rax263, rsi25, rdx264, rcx, r8, 0, v27);
                    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp256) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                }
                eax184 = v42;
                goto addr_9360_184;
            } else {
                if (1) 
                    goto addr_9085_165;
                *reinterpret_cast<int32_t*>(&rdi265) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi265) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdi265) = __intrinsic();
                *reinterpret_cast<uint32_t*>(&rsi25) = 0;
                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) - (v266 - v267));
                *reinterpret_cast<unsigned char*>(&rsi25) = __intrinsic();
                rdx211 = rcx;
                if (rdi265 | reinterpret_cast<unsigned char>(rsi25)) 
                    goto addr_81d8_65; else 
                    goto addr_8e8f_246;
            }
        }
    }
    addr_8ac8_216:
    if (v42) {
        rdx268 = reinterpret_cast<void**>(rsp23 + 0x300);
        rax269 = debug_strfdatetime_constprop_0(r13_145, r14_19, rdx268, rcx, r13_145, r14_19, rdx268, rcx);
        rsp270 = reinterpret_cast<void*>(rsp23 - 8 + 8);
        r9 = v271;
        rbx18 = rax269;
        v12 = r9;
        rax272 = str_days_constprop_0(r14_19, reinterpret_cast<int64_t>(rsp270) + 0x3e0, rdx268, rcx, r8);
        rbp9 = rax272;
        rax273 = fun_35b0();
        *reinterpret_cast<uint32_t*>(&rcx) = v274;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rsi25 = rbp9;
        dbg_printf(rax273, rsi25, v12, rcx, rbx18, r9, v27, rax273, rsi25, v12, rcx, rbx18, r9, v27);
        rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp270) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_81d8_65;
    }
    addr_8aa0_218:
    rax275 = static_cast<int64_t>(reinterpret_cast<int32_t>(eax223 - *reinterpret_cast<uint32_t*>(&rsi25))) + reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v128) + 4)));
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v128) + 4) = *reinterpret_cast<uint32_t*>(&rax275);
    edx276 = __intrinsic();
    *reinterpret_cast<uint32_t*>(&rcx) = 1;
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    if (rax275 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax275))) {
        edx276 = 1;
    }
    if (edx276) 
        goto addr_8ac8_216;
    v37 = -1;
    rax277 = mktime_z(r15_24, r13_145);
    rsp23 = rsp23 - 8 + 8;
    *reinterpret_cast<uint32_t*>(&rsi25) = 0xffffffff;
    *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
    rbp9 = rax277;
    if (1) 
        goto addr_8ac8_216;
    if (!v42) 
        goto addr_9033_163;
    rdx278 = reinterpret_cast<void**>(rsp23 + 0x300);
    rax279 = debug_strfdatetime_constprop_0(r13_145, r14_19, rdx278, 1, r13_145, r14_19, rdx278, 1);
    rsp280 = reinterpret_cast<void*>(rsp23 - 8 + 8);
    rbx18 = rax279;
    rax281 = str_days_constprop_0(r14_19, reinterpret_cast<int64_t>(rsp280) + 0x3e0, rdx278, 1, r8);
    rax282 = fun_35b0();
    rsi25 = rax281;
    rdx146 = rbx18;
    dbg_printf(rax282, rsi25, rdx146, 1, r8, r9, v27, rax282, rsi25, rdx146, 1, r8, r9, v27);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp280) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_8c6b_162;
    addr_7e7e_130:
    v37 = -1;
    goto addr_7e89_125;
    addr_93f5_132:
    if (*reinterpret_cast<unsigned char*>(&rsi25)) {
        rax283 = fun_35b0();
        rsi25 = rbp9;
        dbg_printf(rax283, rsi25, r13_131, rcx, r8, r9, v27, rax283, rsi25, r13_131, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8;
        goto addr_81d8_65;
    }
    addr_7e50_142:
    v133 = 0;
    *reinterpret_cast<uint32_t*>(&v128) = 0;
    v71 = reinterpret_cast<void**>(0);
    if (*reinterpret_cast<unsigned char*>(&rsi25)) {
        dbg_printf("warning: using midnight as starting time: 00:00:00\n", rsi25, rdx129, rcx, r8, r9, v27, "warning: using midnight as starting time: 00:00:00\n", rsi25, rdx129, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8;
        goto addr_94ee_143;
    } else {
        *reinterpret_cast<uint32_t*>(&r9) = 0;
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        r11d134 = 0;
        edi135 = 0;
        goto addr_7e7e_130;
    }
    addr_8816_103:
    if (reinterpret_cast<int64_t>(rax116) >= reinterpret_cast<int64_t>(0)) {
        addr_7d98_268:
        if (rax116 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax116))) {
            edx122 = 1;
        }
    } else {
        addr_881f_270:
        edx122 = 1;
        goto addr_7d98_268;
    }
    *reinterpret_cast<uint32_t*>(&rcx) = edx122 & 1;
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    *reinterpret_cast<unsigned char*>(&v118) = *reinterpret_cast<unsigned char*>(&rcx);
    goto addr_7daf_105;
    addr_7d8f_104:
    if (reinterpret_cast<int64_t>(rax116) >= reinterpret_cast<int64_t>(0)) 
        goto addr_881f_270; else 
        goto addr_7d98_268;
    addr_8693_78:
    rcx = rsi25;
    *reinterpret_cast<int32_t*>(&rdx105) = 5;
    *reinterpret_cast<int32_t*>(&rdx105 + 4) = 0;
    fun_3870(", dst", ", dst");
    rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
    goto addr_86b0_73;
    addr_8924_47:
    v89 = v92;
    goto addr_7cb5_49;
    addr_8288_16:
    v27 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(rsp23 + 0x370);
    if (reinterpret_cast<signed char>(rdi31) > reinterpret_cast<signed char>(100)) {
        v12 = r8;
        rax284 = fun_3780(rdi31, "TZ=\"");
        rsp23 = rsp23 - 8 + 8;
        r8 = v12;
        v27 = rax284;
        if (!rax284) 
            goto addr_8470_10;
        v12 = v27;
    }
    rdx285 = v12;
    if (*reinterpret_cast<signed char*>(&r15_24) != 34) {
        do {
            *reinterpret_cast<int32_t*>(&rax286) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax286) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rax286) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&r15_24) == 92);
            ++rdx285;
            rax287 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(rax286) + reinterpret_cast<unsigned char>(r8));
            *reinterpret_cast<uint32_t*>(&rcx) = rax287->f0;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r15_24) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax287->f1));
            r8 = reinterpret_cast<void**>(&rax287->f1);
            *reinterpret_cast<unsigned char*>(rdx285 + 0xffffffffffffffff) = *reinterpret_cast<unsigned char*>(&rcx);
        } while (*reinterpret_cast<signed char*>(&r15_24) != 34);
    }
    *reinterpret_cast<void***>(rdx285) = reinterpret_cast<void**>(0);
    rax288 = tzalloc(v12, "TZ=\"");
    rsp23 = rsp23 - 8 + 8;
    r15_24 = rax288;
    if (!rax288) {
        *reinterpret_cast<uint32_t*>(&r13_28) = 0;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        goto addr_81e8_22;
    } else {
        r8 = r8;
        r14_19 = r8 + 1;
        while (1) {
            eax289 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_19));
            if (*reinterpret_cast<signed char*>(&eax289) > 13) {
                if (*reinterpret_cast<signed char*>(&eax289) != 32) 
                    break;
            } else {
                if (*reinterpret_cast<signed char*>(&eax289) <= 8) 
                    break;
            }
            ++r14_19;
        }
        rsi25 = r13_7;
        rax290 = localtime_rz(r15_24, rsi25, rsp23 + 0xf0);
        rsp23 = rsp23 - 8 + 8;
        if (rax290) 
            goto addr_7a7f_12;
    }
    goto addr_81d8_65;
    addr_7a51_20:
    goto addr_7a58_9;
}

struct s9 {
    signed char[20] pad20;
    void** f14;
};

struct s10 {
    signed char[8] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    void** f14;
};

void** fun_3760();

struct s10* fun_34f0(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7);

signed char year(struct s9* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** edx8;
    struct s10* rax9;
    void** rax10;
    void* rdx11;

    rax7 = g28;
    if (rdx == 1) {
        edx8 = *reinterpret_cast<void***>(rsi);
        *reinterpret_cast<int32_t*>(&rax9) = 1;
        rdi->f14 = edx8;
        if (reinterpret_cast<signed char>(edx8) <= reinterpret_cast<signed char>(68)) {
            if (*reinterpret_cast<uint32_t*>(&rcx) & 8) {
                addr_9b20_4:
                *reinterpret_cast<int32_t*>(&rax9) = 0;
            } else {
                rdi->f14 = edx8 + 100;
            }
        }
    } else {
        if (rdx == 2) {
            if (!(*reinterpret_cast<uint32_t*>(&rcx) & 2)) 
                goto addr_9b20_4;
            rdi->f14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi)) * 100 + reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(rsi + 4)) - 0x76c);
            *reinterpret_cast<int32_t*>(&rax9) = 1;
        } else {
            rax10 = fun_3760();
            rax9 = fun_34f0(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 + 8, rsi, rdx, rcx, r8, r9, rax10);
            if (rax9) {
                rdi->f14 = rax9->f14;
                *reinterpret_cast<int32_t*>(&rax9) = 1;
            }
        }
    }
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_35e0();
    } else {
        return *reinterpret_cast<signed char*>(&rax9);
    }
}

int64_t fun_35c0();

int64_t fun_3500(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_35c0();
    if (r8d > 10) {
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x13f60 + rax11 * 4) + 0x13f60;
    }
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void*** fun_3510();

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_3690(void** rdi, ...);

struct s12 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s11* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void*** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    void** rdi15;
    int64_t rax16;
    uint32_t r8d17;
    struct s12* rbx18;
    uint32_t r15d19;
    void** rsi20;
    void** r14_21;
    int64_t v22;
    int64_t v23;
    uint32_t r15d24;
    void** rax25;
    void** rsi26;
    void** rax27;
    uint32_t r8d28;
    int64_t v29;
    int64_t v30;
    void* rax31;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0xb47f;
    rax8 = fun_3510();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
        fun_3500(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x19090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xda71]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            rdi15 = reinterpret_cast<void**>((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            v7 = 0xb50b;
            fun_3690(rdi15, rdi15);
            rax16 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax16);
        }
        r8d17 = rcx->f0;
        rbx18 = reinterpret_cast<struct s12*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d19 = rcx->f4;
        rsi20 = rbx18->f0;
        r14_21 = rbx18->f8;
        v22 = rcx->f30;
        v23 = rcx->f28;
        r15d24 = r15d19 | 1;
        rax25 = quotearg_buffer_restyled(r14_21, rsi20, rsi, rdx, r8d17, r15d24, &rcx->f8, v23, v22, v7);
        if (reinterpret_cast<unsigned char>(rsi20) <= reinterpret_cast<unsigned char>(rax25)) {
            rsi26 = rax25 + 1;
            rbx18->f0 = rsi26;
            if (r14_21 != 0x19160) {
                fun_34d0(r14_21, r14_21);
                rsi26 = rsi26;
            }
            rax27 = xcharalloc(rsi26, rsi26);
            r8d28 = rcx->f0;
            rbx18->f8 = rax27;
            v29 = rcx->f30;
            r14_21 = rax27;
            v30 = rcx->f28;
            quotearg_buffer_restyled(rax27, rsi26, rsi, rdx, r8d28, r15d24, rsi26, v30, v29, 0xb59a);
        }
        *rax8 = v10;
        rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax31) {
            fun_35e0();
        } else {
            return r14_21;
        }
    }
}

void** fun_34b0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_3830(int64_t rdi, void** rsi);

int32_t fun_3570(int64_t rdi, void** rsi, void** rdx);

void fun_3750(int64_t rdi, void** rsi, void** rdx);

void** set_tz(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    int32_t eax8;
    void** rax9;
    void** r12_10;
    int32_t eax11;
    int32_t eax12;
    void*** rax13;
    void** ebx14;
    void*** rbp15;
    void** rdi16;

    rax7 = fun_34b0("TZ", rsi, rdx, rcx, r8, r9);
    if (!rax7) {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            return 1;
        }
    } else {
        if (*reinterpret_cast<void***>(rdi + 8) && (rsi = rax7, eax8 = fun_3700(rdi + 9, rsi), !eax8)) {
            return 1;
        }
    }
    rax9 = tzalloc(rax7, rsi);
    r12_10 = rax9;
    if (!rax9) {
        addr_c4e1_7:
        return r12_10;
    } else {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            eax11 = fun_3830("TZ", rsi);
            if (eax11) 
                goto addr_c54d_10; else 
                goto addr_c4dc_11;
        }
        rsi = rdi + 9;
        *reinterpret_cast<int32_t*>(&rdx) = 1;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        eax12 = fun_3570("TZ", rsi, 1);
        if (!eax12) {
            addr_c4dc_11:
            fun_3750("TZ", rsi, rdx);
            goto addr_c4e1_7;
        } else {
            addr_c54d_10:
            rax13 = fun_3510();
            ebx14 = *rax13;
            rbp15 = rax13;
            if (r12_10 != 1) {
                do {
                    rdi16 = r12_10;
                    r12_10 = *reinterpret_cast<void***>(r12_10);
                    fun_34d0(rdi16, rdi16);
                } while (r12_10);
            }
        }
    }
    *rbp15 = ebx14;
    return 0;
}

void** fun_3740(void** rdi, void** rsi, void** rdx, ...);

signed char save_abbr(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void** r13_5;
    int32_t eax6;
    void** rbx7;
    void** rsi8;
    int32_t eax9;
    void** rax10;
    int32_t eax11;
    void** rax12;
    void** rdx13;
    void** rax14;

    r12_3 = *reinterpret_cast<void***>(rsi + 48);
    if (!r12_3) {
        return 1;
    }
    rbp4 = rdi;
    r13_5 = rsi;
    if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(r12_3) || (eax6 = 1, reinterpret_cast<unsigned char>(r12_3) >= reinterpret_cast<unsigned char>(rsi + 56))) {
        rbx7 = rbp4 + 9;
        if (!*reinterpret_cast<void***>(r12_3)) {
            rbx7 = reinterpret_cast<void**>(0x12ce0);
        } else {
            while (rsi8 = r12_3, eax9 = fun_3700(rbx7, rsi8), !!eax9) {
                do {
                    if (*reinterpret_cast<void***>(rbx7)) 
                        goto addr_c3c3_9;
                    if (rbx7 != rbp4 + 9) 
                        goto addr_c430_11;
                    if (!*reinterpret_cast<void***>(rbp4 + 8)) 
                        goto addr_c430_11;
                    addr_c3c3_9:
                    rax10 = fun_35d0(rbx7, rbx7);
                    rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax10) + 1);
                    if (*reinterpret_cast<void***>(rbx7)) 
                        break;
                    if (!*reinterpret_cast<void***>(rbp4)) 
                        break;
                    rbx7 = *reinterpret_cast<void***>(rbp4) + 9;
                    rsi8 = r12_3;
                    rbp4 = *reinterpret_cast<void***>(rbp4);
                    eax11 = fun_3700(rbx7, rsi8);
                } while (eax11);
                goto addr_c3f4_15;
            }
        }
    } else {
        addr_c401_16:
        return *reinterpret_cast<signed char*>(&eax6);
    }
    addr_c3f8_17:
    *reinterpret_cast<void***>(r13_5 + 48) = rbx7;
    eax6 = 1;
    goto addr_c401_16;
    addr_c430_11:
    rax12 = fun_35d0(r12_3, r12_3);
    rdx13 = rax12 + 1;
    if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbp4 + 0x80) - reinterpret_cast<unsigned char>(rbx7)) <= reinterpret_cast<signed char>(rdx13)) {
        rax14 = tzalloc(r12_3, rsi8);
        *reinterpret_cast<void***>(rbp4) = rax14;
        if (!rax14) {
            eax6 = 0;
            goto addr_c401_16;
        } else {
            *reinterpret_cast<void***>(rax14 + 8) = reinterpret_cast<void**>(0);
            rbx7 = rax14 + 9;
            goto addr_c3f8_17;
        }
    } else {
        fun_3740(rbx7, r12_3, rdx13);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax12) + 1) = 0;
        goto addr_c3f8_17;
    }
    addr_c3f4_15:
    goto addr_c3f8_17;
}

/* revert_tz.part.0 */
signed char revert_tz_part_0(void** rdi, void** rsi) {
    void** rbx3;
    void*** rax4;
    void** r12d5;
    void*** rbp6;
    int32_t eax7;
    int32_t r13d8;
    void** rdi9;
    int32_t eax10;
    int32_t eax11;

    rbx3 = rdi;
    rax4 = fun_3510();
    r12d5 = *rax4;
    rbp6 = rax4;
    if (*reinterpret_cast<void***>(rbx3 + 8)) {
        rsi = rbx3 + 9;
        eax7 = fun_3570("TZ", rsi, 1);
        if (eax7) {
            addr_c25e_3:
            r12d5 = *rbp6;
            r13d8 = 0;
        } else {
            addr_c2a9_4:
            fun_3750("TZ", rsi, 1);
            r13d8 = 1;
        }
        do {
            rdi9 = rbx3;
            rbx3 = *reinterpret_cast<void***>(rbx3);
            fun_34d0(rdi9, rdi9);
        } while (rbx3);
        *rbp6 = r12d5;
        eax10 = r13d8;
        return *reinterpret_cast<signed char*>(&eax10);
    } else {
        eax11 = fun_3830("TZ", rsi);
        if (!eax11) 
            goto addr_c2a9_4; else 
            goto addr_c25e_3;
    }
}

uint64_t ydhms_diff(int64_t rdi, int64_t rsi, int32_t edx, uint32_t ecx, int32_t r8d, int32_t r9d, int32_t a7, int32_t a8, int32_t a9, int32_t a10) {
    int64_t rcx11;
    uint64_t rcx12;
    int64_t rax13;
    int64_t rbx14;
    int64_t rdx15;
    int64_t rax16;
    int64_t r9_17;
    int64_t r9_18;
    int64_t rbp19;
    int64_t rdi20;
    int32_t ecx21;
    int64_t rdx22;
    uint32_t edx23;
    int64_t rbp24;
    int32_t r12d25;
    int64_t rdx26;
    int64_t rcx27;
    uint32_t ecx28;
    int64_t rcx29;
    int64_t rdx30;
    int64_t rax31;
    int64_t rax32;
    int64_t rax33;

    rcx11 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx11) & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
    rax13 = rdi >> 2;
    rbx14 = r9d;
    rdx15 = rbx14;
    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<uint32_t*>(&rax13) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax13) < 0x1db - reinterpret_cast<uint1_t>(rcx12 < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    r9_17 = rbx14 >> 2;
    *reinterpret_cast<uint32_t*>(&r9_18) = *reinterpret_cast<uint32_t*>(&r9_17) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_17) < 0x1db - reinterpret_cast<uint1_t>((*reinterpret_cast<uint32_t*>(&rdx15) & 3) < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_18) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbp19) = *reinterpret_cast<uint32_t*>(&rax16) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp19) + 4) = 0;
    rdi20 = rdi - rbx14;
    ecx21 = static_cast<int32_t>(rbp19 + rax16);
    rdx22 = ecx21 * 0x51eb851f >> 35;
    edx23 = *reinterpret_cast<int32_t*>(&rdx22) - (ecx21 >> 31) - *reinterpret_cast<uint32_t*>(&rbp19);
    *reinterpret_cast<uint32_t*>(&rbp24) = *reinterpret_cast<uint32_t*>(&r9_18) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp24) + 4) = 0;
    r12d25 = static_cast<int32_t>(rbp24 + r9_18);
    rdx26 = static_cast<int64_t>(reinterpret_cast<int32_t>(edx23)) >> 2;
    rcx27 = r12d25 * 0x51eb851f >> 35;
    ecx28 = *reinterpret_cast<int32_t*>(&rcx27) - (r12d25 >> 31) - *reinterpret_cast<uint32_t*>(&rbp24);
    rcx29 = static_cast<int64_t>(reinterpret_cast<int32_t>(ecx28)) >> 2;
    rdx30 = rdi20 + (rdi20 + rdi20 * 8) * 8;
    rax31 = reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax16) - *reinterpret_cast<uint32_t*>(&r9_18) - (edx23 - ecx28) + (*reinterpret_cast<int32_t*>(&rdx26) - *reinterpret_cast<uint32_t*>(&rcx29))) + (rdx30 + rdx30 * 4 + rsi - a7);
    rax32 = edx + (rax31 + rax31 * 2) * 8 - a8;
    rax33 = reinterpret_cast<int32_t>(ecx) + ((rax32 << 4) - rax32) * 4 - a9;
    return r8d + ((rax33 << 4) - rax33) * 4 - reinterpret_cast<uint64_t>(static_cast<int64_t>(a10));
}

struct s13 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    int32_t f18;
    int32_t f1c;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
    int64_t f30;
};

struct s13* ranged_convert(int64_t rdi, uint64_t* rsi, struct s13* rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rbx7;
    uint64_t r12_8;
    uint64_t* v9;
    void* rbp10;
    void** rax11;
    void** v12;
    struct s13* rax13;
    struct s13* v14;
    void*** rax15;
    int1_t zf16;
    void*** v17;
    uint64_t rcx18;
    int64_t rcx19;
    uint64_t r13_20;
    void* rax21;
    int32_t v22;
    uint64_t r14_23;
    uint64_t r12_24;
    uint64_t r13_25;
    struct s13* r15_26;
    int64_t rax27;
    int32_t v28;
    int32_t v29;
    int32_t v30;
    int32_t v31;
    int32_t v32;
    int32_t v33;
    int32_t v34;
    int32_t v35;
    int64_t v36;
    int64_t v37;
    uint64_t rax38;
    uint64_t rax39;

    rbx7 = rdi;
    r12_8 = *rsi;
    v9 = rsi;
    rbp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68 + 80);
    rax11 = g28;
    v12 = rax11;
    rax13 = reinterpret_cast<struct s13*>(rbx7(rbp10, rdx));
    v14 = rax13;
    if (!rax13) {
        rax15 = fun_3510();
        zf16 = reinterpret_cast<int1_t>(*rax15 == 75);
        v17 = rax15;
        if (!zf16 || ((rcx18 = r12_8, *reinterpret_cast<uint32_t*>(&rcx19) = *reinterpret_cast<uint32_t*>(&rcx18) & 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0, r13_20 = rcx19 + reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_8) >> 1), r12_8 == r13_20) || !r13_20)) {
            addr_d5ee_3:
            rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
            if (rax21) {
                fun_35e0();
            } else {
                return v14;
            }
        } else {
            v22 = -1;
            r14_23 = r12_8;
            r12_24 = r13_20;
            r13_25 = 0;
            r15_26 = rdx;
            do {
                rax27 = reinterpret_cast<int64_t>(rbx7(rbp10, r15_26));
                if (rax27) {
                    r13_25 = r12_24;
                    v22 = r15_26->f0;
                    v28 = r15_26->f4;
                    v29 = r15_26->f8;
                    v30 = r15_26->fc;
                    v31 = r15_26->f10;
                    v32 = r15_26->f14;
                    v33 = r15_26->f18;
                    v34 = r15_26->f1c;
                    v35 = r15_26->f20;
                    v36 = r15_26->f28;
                    v37 = r15_26->f30;
                } else {
                    if (!reinterpret_cast<int1_t>(*v17 == 75)) 
                        goto addr_d5ee_3;
                    r14_23 = r12_24;
                }
                rax38 = r13_25 | r14_23;
                *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<uint32_t*>(&rax38) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
                r12_24 = (reinterpret_cast<int64_t>(r13_25) >> 1) + (reinterpret_cast<int64_t>(r14_23) >> 1) + rax39;
            } while (r12_24 != r13_25 && r12_24 != r14_23);
        }
        if (v22 >= 0) {
            v14 = r15_26;
            *v9 = r13_25;
            r15_26->f0 = v22;
            r15_26->f4 = v28;
            r15_26->f8 = v29;
            r15_26->fc = v30;
            r15_26->f10 = v31;
            r15_26->f14 = v32;
            r15_26->f18 = v33;
            r15_26->f1c = v34;
            r15_26->f20 = v35;
            r15_26->f28 = v36;
            r15_26->f30 = v37;
            goto addr_d5ee_3;
        }
    } else {
        *rsi = r12_8;
        goto addr_d5ee_3;
    }
}

void* fun_37f0();

uint32_t** fun_34a0();

struct s14 {
    signed char[1] pad1;
    void** f1;
};

uint32_t** fun_38b0(void** rdi, ...);

/* __strftime_internal.isra.0 */
void** __strftime_internal_isra_0(void** rdi, void** rsi, void** rdx, void** rcx, unsigned char r8b, void** r9d, int32_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11) {
    void** r14_12;
    void** rbp13;
    void** rbx14;
    void** v15;
    void** r15_16;
    void** v17;
    unsigned char v18;
    void** rax19;
    void** v20;
    void*** rax21;
    void* rsp22;
    void** r11_23;
    void** r10d24;
    void*** v25;
    void** v26;
    uint32_t eax27;
    void** r13_28;
    void* rax29;
    uint32_t edx30;
    unsigned char v31;
    void** rdi32;
    void** r8_33;
    int64_t rax34;
    uint64_t rax35;
    int32_t eax36;
    void** rbp37;
    void* rax38;
    void* rcx39;
    uint32_t ecx40;
    int64_t r9_41;
    void** rcx42;
    int64_t r9_43;
    void** rbp44;
    void** v45;
    unsigned char* rbp46;
    uint32_t** rax47;
    void** r8_48;
    int1_t cf49;
    void** rax50;
    void** rcx51;
    void** rbx52;
    uint32_t eax53;
    uint32_t eax54;
    int64_t rdx55;
    int32_t ecx56;
    uint64_t rax57;
    struct s14* rcx58;
    void** r8_59;
    void** r9_60;
    void** r15_61;
    struct s14* r15_62;
    uint32_t** rax63;
    int64_t rcx64;
    int1_t cf65;
    void** r8_66;
    void** v67;
    void* rbp68;
    uint32_t** rax69;
    int64_t rcx70;
    int1_t cf71;
    void* rbp72;
    uint32_t** rax73;
    int64_t rcx74;
    int1_t cf75;
    int64_t rax76;

    r14_12 = rdi;
    rbp13 = rdx;
    rbx14 = rcx;
    v15 = rsi;
    r15_16 = reinterpret_cast<void**>(static_cast<int64_t>(a7));
    v17 = r9d;
    v18 = r8b;
    rax19 = g28;
    v20 = rax19;
    rax21 = fun_3510();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x4c8 - 8 + 8);
    r11_23 = *reinterpret_cast<void***>(rbx14 + 48);
    r10d24 = *reinterpret_cast<void***>(rbx14 + 8);
    v25 = rax21;
    v26 = *rax21;
    if (!r11_23) {
    }
    if (reinterpret_cast<signed char>(r10d24) <= reinterpret_cast<signed char>(12)) {
        if (!r10d24) {
            r10d24 = reinterpret_cast<void**>(12);
        }
    } else {
        r10d24 = r10d24 - 12;
    }
    eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
    *reinterpret_cast<int32_t*>(&r13_28) = 0;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax27)) {
        goto addr_e1bd_10;
    }
    while (1) {
        addr_e21b_11:
        if (r14_12 && v15) {
            *reinterpret_cast<void***>(r14_12) = reinterpret_cast<void**>(0);
        }
        rdi = v26;
        *v25 = rdi;
        while (rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(g28)), !!rax29) {
            fun_35e0();
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            edx30 = *reinterpret_cast<uint32_t*>(&rdx) + 100;
            if (!r10d24) {
                if (v17 == 43) {
                    addr_f85f_18:
                } else {
                    r10d24 = v17;
                    goto addr_e78c_20;
                }
            } else {
                if (!reinterpret_cast<int1_t>(r10d24 == 43)) 
                    goto addr_e78c_20; else 
                    goto addr_f85f_18;
            }
            r10d24 = reinterpret_cast<void**>(43);
            v31 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(2));
            addr_e7a3_24:
            if (static_cast<int1_t>(!reinterpret_cast<int1_t>(rdi == 79))) {
                if (0) {
                    edx30 = -edx30;
                }
                rdi32 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xc7);
                r8_33 = rdi32;
                while (1) {
                    rsi = r8_33;
                    if (!1) {
                        --rsi;
                    }
                    *reinterpret_cast<uint32_t*>(&rax34) = edx30;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax34) + 4) = 0;
                    r8_33 = rsi + 0xffffffffffffffff;
                    rax35 = reinterpret_cast<uint64_t>(rax34 * 0xcccccccd) >> 35;
                    if (edx30 > 9) 
                        goto addr_f003_33;
                    if (1) 
                        break;
                    addr_f003_33:
                    edx30 = *reinterpret_cast<uint32_t*>(&rax35);
                }
                if (!r10d24) {
                    eax36 = 1;
                    r10d24 = reinterpret_cast<void**>(48);
                } else {
                    *reinterpret_cast<unsigned char*>(&eax36) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(r10d24 == 45));
                }
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                    r15_16 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                }
                rdi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi32) - reinterpret_cast<unsigned char>(r8_33));
                rbp37 = rdi;
                if (!0) 
                    goto addr_ebda_41;
            } else {
                rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xab);
                rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb0);
                rax38 = fun_37f0();
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                rcx39 = rax38;
                if (!rax38) 
                    goto addr_e208_45; else 
                    goto addr_e539_46;
            }
            ecx40 = 45;
            addr_f4f5_48:
            rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(r15_16 + 0xffffffffffffffff)));
            *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(rsi) - *reinterpret_cast<uint32_t*>(&rbp37);
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx) == 0) || !*reinterpret_cast<unsigned char*>(&eax36)) {
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
            }
            if (r10d24 == 95) {
                *reinterpret_cast<void**>(&r9_41) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_16) - *reinterpret_cast<uint32_t*>(&rdx));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_41) + 4) = 0;
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx)));
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rdx));
                r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                if (!r14_12) {
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_e240_53;
                    ++r13_28;
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_41 - 1));
                    rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
                    goto addr_f53a_55;
                } else {
                    rdi = r14_12;
                    fun_3690(rdi);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    rdx = rdx;
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_e240_53;
                    *reinterpret_cast<void**>(&r9_43) = *reinterpret_cast<void**>(&r9_41);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_43) + 4) = 0;
                    r8_33 = r8_33;
                    r10d24 = r10d24;
                    ecx40 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ecx40));
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_43 - 1));
                }
            } else {
                if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= 1) 
                    goto addr_e240_53;
                if (!r14_12) 
                    goto addr_f52d_60;
            }
            *reinterpret_cast<void***>(r14_12) = *reinterpret_cast<void***>(&ecx40);
            ++r14_12;
            addr_f52d_60:
            ++r13_28;
            rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
            if (r10d24 == 45) {
                addr_f56e_62:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                goto addr_ec06_63;
            } else {
                addr_f53a_55:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                if (reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) {
                    addr_ec06_63:
                    if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) {
                        addr_e240_53:
                        *v25 = reinterpret_cast<void**>(34);
                    } else {
                        if (r14_12) {
                            if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rcx42)) {
                                rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(rcx42));
                                rbp44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                                if (r10d24 == 48 || r10d24 == 43) {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_3690(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_33 = r8_33;
                                } else {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_3690(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    r8_33 = r8_33;
                                    rcx42 = rcx42;
                                }
                            }
                            if (!*reinterpret_cast<signed char*>(&v45)) {
                                rdx = rcx42;
                                rdi = r14_12;
                                v45 = rcx42;
                                fun_3740(rdi, r8_33, rdx);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                rcx42 = v45;
                            } else {
                                v45 = r8_33;
                                rbp46 = reinterpret_cast<unsigned char*>(rcx42 + 0xffffffffffffffff);
                                if (rcx42) {
                                    rax47 = fun_34a0();
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_48 = v45;
                                    do {
                                        rsi = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r8_48) + reinterpret_cast<uint64_t>(rbp46))));
                                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&rdx) = (*rax47)[reinterpret_cast<unsigned char>(rsi)];
                                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp46)) = *reinterpret_cast<signed char*>(&rdx);
                                        cf49 = reinterpret_cast<uint64_t>(rbp46) < 1;
                                        --rbp46;
                                    } while (!cf49);
                                }
                            }
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rcx42));
                            goto addr_e617_75;
                        }
                    }
                } else {
                    r15_16 = rsi;
                    goto addr_ebf9_77;
                }
            }
            *reinterpret_cast<int32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
            continue;
            addr_e617_75:
            r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r15_16));
            addr_e208_45:
            while (eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1)), rbp13 = rbx14 + 1, r15_16 = reinterpret_cast<void**>(0xffffffffffffffff), !!*reinterpret_cast<signed char*>(&eax27)) {
                addr_e1bd_10:
                if (*reinterpret_cast<signed char*>(&eax27) != 37) {
                    *reinterpret_cast<int32_t*>(&rax50) = 0;
                    *reinterpret_cast<int32_t*>(&rax50 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rcx51) = 1;
                    *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
                    if (reinterpret_cast<signed char>(r15_16) >= reinterpret_cast<signed char>(0)) {
                        rax50 = r15_16;
                    }
                    if (rax50) {
                        rcx51 = rax50;
                    }
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                    if (reinterpret_cast<unsigned char>(rcx51) >= reinterpret_cast<unsigned char>(rdx)) 
                        goto addr_e240_53;
                    if (r14_12) {
                        if (reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(1)) {
                            rbx52 = rax50 + 0xffffffffffffffff;
                            rdi = r14_12;
                            v45 = rcx51;
                            rdx = rbx52;
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rbx52));
                            fun_3690(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx51 = v45;
                        }
                        eax53 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
                        ++r14_12;
                        *reinterpret_cast<unsigned char*>(r14_12 + 0xffffffffffffffff) = *reinterpret_cast<unsigned char*>(&eax53);
                    }
                    r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rcx51));
                    rbx14 = rbp13;
                    continue;
                }
                eax54 = v18;
                rbx14 = rbp13;
                r10d24 = reinterpret_cast<void**>(0);
                *reinterpret_cast<signed char*>(&v45) = *reinterpret_cast<signed char*>(&eax54);
                while (*reinterpret_cast<void***>(&rdx55) = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1)))), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, ++rbx14, ecx56 = static_cast<int32_t>(rdx55 - 35), rsi = *reinterpret_cast<void***>(&rdx55), rdi = *reinterpret_cast<void***>(&rdx55), *reinterpret_cast<unsigned char*>(&ecx56) <= 60) {
                    rax57 = 1 << *reinterpret_cast<unsigned char*>(&ecx56);
                    if (rax57 & 0x1000000000002500) {
                        r10d24 = *reinterpret_cast<void***>(&rdx55);
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&ecx56) == 59) {
                            *reinterpret_cast<signed char*>(&v45) = 1;
                        } else {
                            if (!(*reinterpret_cast<uint32_t*>(&rax57) & 1)) 
                                break;
                        }
                    }
                }
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rdx55) - 48) > 9) 
                    goto addr_e31f_98;
                r15_16 = reinterpret_cast<void**>(0);
                do {
                    if (__intrinsic() || (r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_16) * 10 + reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48)), *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0, __intrinsic())) {
                        r15_16 = reinterpret_cast<void**>(0x7fffffff);
                        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                    }
                    rdi = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    ++rbx14;
                    rsi = rdi;
                } while (static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdi + 0xffffffffffffffd0)) <= 9);
                addr_e31f_98:
                if (*reinterpret_cast<unsigned char*>(&rsi) == 69 || *reinterpret_cast<unsigned char*>(&rsi) == 79) {
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    ++rbx14;
                } else {
                    rdi = reinterpret_cast<void**>(0);
                }
                if (*reinterpret_cast<unsigned char*>(&rsi) <= 0x7a) 
                    goto addr_e33b_106;
                rcx58 = reinterpret_cast<struct s14*>(reinterpret_cast<unsigned char>(rbx14) - reinterpret_cast<unsigned char>(rbp13));
                r8_59 = reinterpret_cast<void**>(&rcx58->f1);
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0) || r10d24 == 45) {
                    r9_60 = r8_59;
                    *reinterpret_cast<uint32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                } else {
                    rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                    r9_60 = rdx;
                    if (reinterpret_cast<unsigned char>(r8_59) >= reinterpret_cast<unsigned char>(rdx)) {
                        r9_60 = r8_59;
                    }
                }
                if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r9_60)) 
                    goto addr_e240_53;
                if (r14_12) {
                    if (reinterpret_cast<unsigned char>(r8_59) < reinterpret_cast<unsigned char>(rdx)) {
                        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_59));
                        r15_61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                        if (r10d24 == 48 || r10d24 == 43) {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_3690(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx58 = rcx58;
                            r8_59 = r8_59;
                            r9_60 = r9_60;
                        } else {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_3690(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r9_60 = r9_60;
                            r8_59 = r8_59;
                            rcx58 = rcx58;
                        }
                    }
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_59;
                        rdi = r14_12;
                        v45 = r8_59;
                        fun_3740(rdi, rbp13, rdx);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r9_60 = r9_60;
                        r8_59 = v45;
                    } else {
                        r15_62 = rcx58;
                        if (r8_59) {
                            v45 = r8_59;
                            rax63 = fun_34a0();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_59 = v45;
                            r9_60 = r9_60;
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx64) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<uint64_t>(r15_62));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx64) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax63)[rcx64];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(r15_62)) = *reinterpret_cast<signed char*>(&rdx);
                                cf65 = reinterpret_cast<uint64_t>(r15_62) < 1;
                                r15_62 = reinterpret_cast<struct s14*>(reinterpret_cast<uint64_t>(r15_62) - 1);
                            } while (!cf65);
                        }
                    }
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_59));
                }
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r9_60));
            }
            goto addr_e21b_11;
            addr_ebf9_77:
            rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
            r15_16 = rcx42;
            if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(rcx42)) {
                r15_16 = rdx;
                goto addr_ec06_63;
            }
            addr_ebda_41:
            if (v31) {
                ecx40 = 43;
                goto addr_f4f5_48;
            } else {
                rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(rdi)));
                if (reinterpret_cast<signed char>(r15_16) <= reinterpret_cast<signed char>(rdi)) 
                    goto addr_f564_129;
                if (*reinterpret_cast<unsigned char*>(&eax36)) 
                    goto addr_ebf9_77;
                addr_f564_129:
                if (!reinterpret_cast<int1_t>(r10d24 == 45)) 
                    goto addr_ebf9_77; else 
                    goto addr_f56e_62;
            }
            addr_e539_46:
            r10d24 = r10d24;
            r8_66 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax38) + 0xffffffffffffffff);
            if (r10d24 == 45 || reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                r15_16 = r8_66;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            } else {
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                r15_16 = rdx;
                if (reinterpret_cast<unsigned char>(r8_66) >= reinterpret_cast<unsigned char>(rdx)) {
                    r15_16 = r8_66;
                }
            }
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) 
                goto addr_e240_53;
            if (r14_12) {
                if (reinterpret_cast<unsigned char>(r8_66) < reinterpret_cast<unsigned char>(rdx)) {
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_66));
                    v67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (r10d24 == 48 || r10d24 == 43) {
                        rdi = r14_12;
                        fun_3690(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        rcx39 = rcx39;
                        r8_66 = r8_66;
                    } else {
                        rdi = r14_12;
                        fun_3690(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        r8_66 = r8_66;
                        rcx39 = rcx39;
                    }
                }
                if (0) {
                    rbp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx39) + 0xfffffffffffffffe);
                    if (r8_66) {
                        v45 = r8_66;
                        rax69 = fun_38b0(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx70) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rbp68));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx70) + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rdx) = (*rax69)[rcx70];
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp68)) = *reinterpret_cast<signed char*>(&rdx);
                            cf71 = reinterpret_cast<uint64_t>(rbp68) < 1;
                            rbp68 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp68) - 1);
                        } while (!cf71);
                    }
                } else {
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_66;
                        rdi = r14_12;
                        v45 = r8_66;
                        fun_3740(rdi, reinterpret_cast<int64_t>(rsp22) + 0xb1, rdx);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                    } else {
                        rbp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx39) + 0xfffffffffffffffe);
                        if (r8_66) {
                            v45 = r8_66;
                            rax73 = fun_34a0();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_66 = v45;
                            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx74) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rbp72));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx74) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax73)[rcx74];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp72)) = *reinterpret_cast<signed char*>(&rdx);
                                cf75 = reinterpret_cast<uint64_t>(rbp72) < 1;
                                rbp72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp72) - 1);
                            } while (!cf75);
                        }
                    }
                }
                r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_66));
                goto addr_e617_75;
            }
            addr_e78c_20:
            v31 = 0;
            goto addr_e7a3_24;
        }
        break;
    }
    return r13_28;
    addr_e33b_106:
    *reinterpret_cast<uint32_t*>(&rax76) = *reinterpret_cast<unsigned char*>(&rsi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax76) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x14694 + rax76 * 4) + 0x14694;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x190a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s15 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s15* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s15* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x13ef1);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x13eec);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x13ef5);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x13ee8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

struct s16 {
    signed char[28] pad28;
    int32_t f1c;
    signed char[8] pad40;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
    int64_t f48;
    int64_t f50;
    int64_t f58;
    int64_t f60;
    signed char[57] pad161;
    signed char fa1;
    signed char[6] pad168;
    int64_t fa8;
    signed char[32] pad208;
    int64_t fd0;
    signed char[8] pad224;
    signed char fe0;
};

int64_t digits_to_date_time(struct s16* rdi) {
    int64_t r8_2;
    int64_t v3;
    int64_t v4;
    int64_t r10_5;
    int64_t rsi6;
    int64_t rax7;
    int64_t rdi8;
    int64_t rax9;
    int64_t v10;
    int64_t rax11;
    int64_t rdx12;
    int64_t rax13;
    int64_t rax14;

    r8_2 = v3;
    if (!rdi->fa8 || (rdi->f30 || rdi->fa1)) {
        if (v4 > 4) {
            rdi->fa8 = rdi->fa8 + 1;
            r10_5 = r8_2 >> 63;
            rdi->f30 = v4 - 4;
            rsi6 = (__intrinsic() + r8_2 >> 6) - r10_5;
            rax7 = rsi6 + rsi6 * 4;
            rdi->f40 = r8_2 - (rax7 + rax7 * 4 << 2);
            rdi8 = (__intrinsic() + rsi6 >> 6) - (rsi6 >> 63);
            rax9 = rdi8 + rdi8 * 4;
            rdi->f38 = rsi6 - (rax9 + rax9 * 4 << 2);
            rdi->f28 = (__intrinsic() >> 11) - r10_5;
            return r8_2 * 0x346dc5d63886594b;
        }
        rdi->fd0 = rdi->fd0 + 1;
        if (v4 > 2) 
            goto addr_4f8e_5;
    } else {
        if (rdi->fd0 || v4 > 2) {
            __asm__("movdqu xmm0, [rsp+0x8]");
            rdi->fe0 = 1;
            rdi->f30 = v10;
            __asm__("movups [rcx+0x20], xmm0");
            return v10;
        } else {
            rdi->fd0 = 1;
        }
    }
    *reinterpret_cast<int32_t*>(&rax11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    addr_4fc4_10:
    rdi->f48 = r8_2;
    rdi->f50 = rax11;
    rdi->f58 = 0;
    rdi->f60 = 0;
    rdi->f1c = 2;
    return rax11;
    addr_4f8e_5:
    rdx12 = (__intrinsic() + r8_2 >> 6) - (r8_2 >> 63);
    rax13 = rdx12 + rdx12 * 4;
    rax14 = r8_2;
    r8_2 = rdx12;
    rax11 = rax14 - (rax13 + rax13 * 4 << 2);
    goto addr_4fc4_10;
}

struct s17 {
    signed char[24] pad24;
    int32_t f18;
};

/* time_zone_hhmm.isra.0 */
signed char time_zone_hhmm_isra_0(struct s17* rdi, signed char sil, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rdi6;
    uint32_t edx7;
    int64_t rdi8;
    uint32_t ecx9;
    int64_t rdi10;
    int64_t rcx11;
    int64_t rdx12;
    int64_t rax13;
    int64_t rcx14;
    int32_t eax15;

    rdi6 = rdx;
    if (rcx > 2) {
        if (r8 >= 0) {
            addr_5925_3:
            edx7 = 0;
            rdi8 = rdi6 * 60;
            *reinterpret_cast<unsigned char*>(&edx7) = __intrinsic();
            if (sil) {
                ecx9 = 0;
                rdi10 = rdi8 - r8;
                *reinterpret_cast<unsigned char*>(&ecx9) = __intrinsic();
            } else {
                ecx9 = 0;
                rdi10 = rdi8 + r8;
                *reinterpret_cast<unsigned char*>(&ecx9) = __intrinsic();
            }
        } else {
            rcx11 = rdi6;
            rdx12 = (__intrinsic() + rdi6 >> 6) - (rdi6 >> 63);
            rax13 = rdx12 + rdx12 * 4;
            rdi6 = rdx12;
            rcx14 = rcx11 - (rax13 + rax13 * 4 << 2);
            goto addr_59b2_7;
        }
    } else {
        if (r8 >= 0) 
            goto addr_5925_3;
        *reinterpret_cast<int32_t*>(&rcx14) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
        goto addr_59b2_7;
    }
    eax15 = 0;
    if (edx7 | ecx9) {
        addr_595f_11:
        return *reinterpret_cast<signed char*>(&eax15);
    } else {
        addr_5941_12:
        eax15 = 0;
        if (reinterpret_cast<uint64_t>(rdi10 + 0x5a0) <= 0xb40) {
            eax15 = 1;
            rdi->f18 = *reinterpret_cast<int32_t*>(&rdi10) * 60;
            goto addr_595f_11;
        }
    }
    addr_59b2_7:
    rdi10 = rcx14 + ((rdi6 << 4) - rdi6) * 4;
    goto addr_5941_12;
}

int64_t __gmon_start__ = 0;

void fun_3003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g18d98 = 0;

void fun_3033() {
    __asm__("cli ");
    goto g18d98;
}

void fun_3043() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3053() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3063() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3073() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3083() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3093() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3103() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3113() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3123() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3133() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3143() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3153() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3163() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3173() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3183() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3193() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3203() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3213() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3223() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3233() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3243() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3253() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3263() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3273() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3283() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3293() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3303() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3313() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3323() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3333() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3343() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3353() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3363() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3373() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3383() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3393() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3403() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3413() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3423() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3433() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3443() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3453() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3463() {
    __asm__("cli ");
    goto 0x3020;
}

int64_t localtime_r = 0;

void fun_3473() {
    __asm__("cli ");
    goto localtime_r;
}

int64_t gmtime_r = 0;

void fun_3483() {
    __asm__("cli ");
    goto gmtime_r;
}

int64_t __cxa_finalize = 0;

void fun_3493() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x3030;

void fun_34a3() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t getenv = 0x3040;

void fun_34b3() {
    __asm__("cli ");
    goto getenv;
}

int64_t __snprintf_chk = 0x3050;

void fun_34c3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x3060;

void fun_34d3() {
    __asm__("cli ");
    goto free;
}

int64_t utimensat = 0x3070;

void fun_34e3() {
    __asm__("cli ");
    goto utimensat;
}

int64_t localtime = 0x3080;

void fun_34f3() {
    __asm__("cli ");
    goto localtime;
}

int64_t abort = 0x3090;

void fun_3503() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x30a0;

void fun_3513() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x30b0;

void fun_3523() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x30c0;

void fun_3533() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x30d0;

void fun_3543() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x30e0;

void fun_3553() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clock_gettime = 0x30f0;

void fun_3563() {
    __asm__("cli ");
    goto clock_gettime;
}

int64_t setenv = 0x3100;

void fun_3573() {
    __asm__("cli ");
    goto setenv;
}

int64_t textdomain = 0x3110;

void fun_3583() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x3120;

void fun_3593() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x3130;

void fun_35a3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x3140;

void fun_35b3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x3150;

void fun_35c3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x3160;

void fun_35d3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x3170;

void fun_35e3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x3180;

void fun_35f3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x3190;

void fun_3603() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x31a0;

void fun_3613() {
    __asm__("cli ");
    goto dup2;
}

int64_t strchr = 0x31b0;

void fun_3623() {
    __asm__("cli ");
    goto strchr;
}

int64_t snprintf = 0x31c0;

void fun_3633() {
    __asm__("cli ");
    goto snprintf;
}

int64_t __overflow = 0x31d0;

void fun_3643() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x31e0;

void fun_3653() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x31f0;

void fun_3663() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x3200;

void fun_3673() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t fputs = 0x3210;

void fun_3683() {
    __asm__("cli ");
    goto fputs;
}

int64_t memset = 0x3220;

void fun_3693() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x3230;

void fun_36a3() {
    __asm__("cli ");
    goto close;
}

int64_t fputc = 0x3240;

void fun_36b3() {
    __asm__("cli ");
    goto fputc;
}

int64_t lstat = 0x3250;

void fun_36c3() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x3260;

void fun_36d3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x3270;

void fun_36e3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x3280;

void fun_36f3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x3290;

void fun_3703() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x32a0;

void fun_3713() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x32b0;

void fun_3723() {
    __asm__("cli ");
    goto stat;
}

int64_t strtol = 0x32c0;

void fun_3733() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x32d0;

void fun_3743() {
    __asm__("cli ");
    goto memcpy;
}

int64_t tzset = 0x32e0;

void fun_3753() {
    __asm__("cli ");
    goto tzset;
}

int64_t time = 0x32f0;

void fun_3763() {
    __asm__("cli ");
    goto time;
}

int64_t fileno = 0x3300;

void fun_3773() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x3310;

void fun_3783() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x3320;

void fun_3793() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x3330;

void fun_37a3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x3340;

void fun_37b3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x3350;

void fun_37c3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x3360;

void fun_37d3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x3370;

void fun_37e3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t strftime = 0x3380;

void fun_37f3() {
    __asm__("cli ");
    goto strftime;
}

int64_t error = 0x3390;

void fun_3803() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x33a0;

void fun_3813() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x33b0;

void fun_3823() {
    __asm__("cli ");
    goto fseeko;
}

int64_t unsetenv = 0x33c0;

void fun_3833() {
    __asm__("cli ");
    goto unsetenv;
}

int64_t futimens = 0x33d0;

void fun_3843() {
    __asm__("cli ");
    goto futimens;
}

int64_t __cxa_atexit = 0x33e0;

void fun_3853() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x33f0;

void fun_3863() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x3400;

void fun_3873() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x3410;

void fun_3883() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x3420;

void fun_3893() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x3430;

void fun_38a3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_tolower_loc = 0x3440;

void fun_38b3() {
    __asm__("cli ");
    goto __ctype_tolower_loc;
}

int64_t __ctype_b_loc = 0x3450;

void fun_38c3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x3460;

void fun_38d3() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_37d0(int64_t rdi, ...);

void fun_35a0(int64_t rdi, int64_t rsi);

void fun_3580(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

unsigned char use_ref = 0;

uint32_t change_times = 0;

unsigned char no_create = 0;

int32_t fun_35f0(void* rdi, void** rsi, void** rdx, void** rcx);

void** ref_file = reinterpret_cast<void**>(0);

unsigned char no_dereference = 0;

int32_t fun_36c0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

uint64_t newtime = 0;

int64_t g19128 = 0;

int64_t g19130 = 0;

int64_t g19138 = 0;

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

signed char amtime_now = 0;

int32_t optind = 0;

uint32_t fd_reopen();

int32_t fdutimensat(int64_t rdi, int64_t rsi, ...);

uint32_t fun_36a0();

int32_t fun_3720(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void usage();

void fun_3670(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8, void** r9);

int32_t posix2_version(void* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

signed char posixtime(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7);

struct s18 {
    signed char[1900] pad1900;
    void** f76c;
};

void** stdout = reinterpret_cast<void**>(0);

void** Version = reinterpret_cast<void**>(0xa4);

void version_etc(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10);

int32_t fun_3860();

int64_t fun_3933(int32_t edi, void** rsi) {
    int32_t ebp3;
    void** rbx4;
    void** rdi5;
    void** rax6;
    void** v7;
    void** r12_8;
    void** v9;
    void** r8_10;
    void** rcx11;
    void** rdx12;
    void** rsi13;
    void* rdi14;
    int32_t eax15;
    void*** rsp16;
    int1_t zf17;
    uint32_t eax18;
    int1_t zf19;
    void** rdi20;
    void** r9_21;
    int32_t eax22;
    uint64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    uint32_t eax27;
    void** rax28;
    void** rax29;
    int32_t eax30;
    void** r14_31;
    void** r13d32;
    int64_t r15_33;
    int1_t zf34;
    int1_t zf35;
    uint32_t eax36;
    void*** rax37;
    uint32_t eax38;
    uint32_t r8d39;
    void** rcx40;
    int1_t zf41;
    void** rdx42;
    int32_t eax43;
    void*** rax44;
    int1_t zf45;
    int64_t rdi46;
    int32_t eax47;
    int32_t eax48;
    int32_t eax49;
    uint32_t v50;
    uint32_t eax51;
    void** rax52;
    int64_t rax53;
    void* rdx54;
    int32_t eax55;
    int1_t zf56;
    int1_t zf57;
    uint64_t v58;
    int1_t zf59;
    int64_t v60;
    uint64_t v61;
    int64_t v62;
    int32_t eax63;
    int32_t eax64;
    int64_t rax65;
    void** rsi66;
    signed char al67;
    int1_t zf68;
    void** rax69;
    struct s10* rax70;
    int64_t rdx71;
    void** ecx72;
    struct s18* r12_73;
    int64_t rax74;
    void** r14_75;
    int32_t tmp32_76;
    void** rdi77;
    int64_t rax78;

    __asm__("cli ");
    ebp3 = edi;
    rbx4 = rsi;
    rdi5 = *reinterpret_cast<void***>(rsi);
    rax6 = g28;
    v7 = rax6;
    set_program_name(rdi5);
    fun_37d0(6, 6);
    fun_35a0("coreutils", "/usr/local/share/locale");
    r12_8 = reinterpret_cast<void**>(0x12900);
    fun_3580("coreutils", "/usr/local/share/locale");
    atexit(0x4c50, "/usr/local/share/locale");
    use_ref = 0;
    change_times = 0;
    no_create = 0;
    v9 = reinterpret_cast<void**>(0);
    r8_10 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
    rcx11 = reinterpret_cast<void**>(0x18120);
    rdx12 = reinterpret_cast<void**>("acd:fhmr:t:");
    rsi13 = rbx4;
    *reinterpret_cast<int32_t*>(&rdi14) = ebp3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    eax15 = fun_35f0(rdi14, rsi13, "acd:fhmr:t:", 0x18120);
    rsp16 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xe8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (eax15 == -1) {
        addr_3b9f_3:
        zf17 = change_times == 0;
        if (zf17) {
            change_times = 3;
        }
    } else {
        if (eax15 > 0x80) 
            goto addr_4094_6;
        if (eax15 <= 96) 
            goto addr_3b30_8; else 
            goto addr_3a18_9;
    }
    eax18 = use_ref;
    if (!1) {
        if (0) 
            goto addr_4262_12;
        if (*reinterpret_cast<signed char*>(&eax18)) 
            goto addr_4262_12; else 
            goto addr_3bd7_14;
    }
    if (*reinterpret_cast<signed char*>(&eax18)) {
        r12_8 = ref_file;
        zf19 = no_dereference == 0;
        rsi13 = reinterpret_cast<void**>(rsp16 + 64);
        rdi20 = r12_8;
        if (!zf19) {
            while (1) {
                eax22 = fun_36c0(rdi20, rsi13, rdx12, rcx11, r8_10, r9_21);
                rsp16 = rsp16 - 8 + 8;
                if (!eax22) {
                    addr_3ff7_18:
                    newtime = v23;
                    g19128 = v24;
                    g19130 = v25;
                    g19138 = v26;
                    if (v9) {
                        eax27 = change_times;
                        if (*reinterpret_cast<unsigned char*>(&eax27) & 1) {
                            get_reldate(0x19120, v9, 0x19120, rcx11, r8_10, r9_21);
                            rsp16 = rsp16 - 8 + 8;
                            eax27 = change_times;
                        }
                        if (*reinterpret_cast<unsigned char*>(&eax27) & 2) {
                            get_reldate(0x19130, v9, 0x19130, rcx11, r8_10, r9_21);
                            rsp16 = rsp16 - 8 + 8;
                        }
                    }
                } else {
                    addr_40ab_23:
                    rax28 = quotearg_style(4, r12_8, rdx12, rcx11, 4, r12_8, rdx12, rcx11);
                    rax29 = fun_35b0();
                    r12_8 = rax29;
                    fun_3510();
                    rcx11 = rax28;
                    fun_3800();
                    rsp16 = rsp16 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                    addr_40ea_24:
                    amtime_now = 1;
                }
                addr_3bd7_14:
                eax30 = optind;
                if (eax30 != ebp3) {
                    *reinterpret_cast<uint32_t*>(&r12_8) = 1;
                    *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
                    if (eax30 < ebp3) {
                        while (1) {
                            r14_31 = *reinterpret_cast<void***>(rbx4 + eax30 * 8);
                            if (*reinterpret_cast<void***>(r14_31) != 45 || (r13d32 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_31 + 1)))), *reinterpret_cast<uint32_t*>(&r15_33) = 1, !!r13d32)) {
                                zf34 = no_create == 0;
                                if (!zf34 || (zf35 = no_dereference == 0, !zf35)) {
                                    *reinterpret_cast<uint32_t*>(&r15_33) = 0xffffffff;
                                    r13d32 = reinterpret_cast<void**>(0);
                                } else {
                                    eax36 = fd_reopen();
                                    rsp16 = rsp16 - 8 + 8;
                                    r13d32 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<uint32_t*>(&r15_33) = eax36;
                                    if (reinterpret_cast<int32_t>(eax36) < reinterpret_cast<int32_t>(0)) {
                                        rax37 = fun_3510();
                                        rsp16 = rsp16 - 8 + 8;
                                        r13d32 = *rax37;
                                    }
                                }
                            }
                            eax38 = change_times;
                            if (eax38 != 3) {
                                if (eax38 == 2) {
                                    g19128 = 0x3ffffffe;
                                } else {
                                    if (eax38 - 1) 
                                        goto addr_4243_36;
                                    g19138 = 0x3ffffffe;
                                }
                            }
                            r8d39 = no_dereference;
                            *reinterpret_cast<int32_t*>(&rcx40) = 0;
                            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                            zf41 = amtime_now == 0;
                            if (zf41) {
                                rcx40 = reinterpret_cast<void**>(0x19120);
                            }
                            r8_10 = reinterpret_cast<void**>(r8d39 << 8);
                            *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
                            if (*reinterpret_cast<uint32_t*>(&r15_33) == 1) {
                                *reinterpret_cast<int32_t*>(&rdx42) = 0;
                                *reinterpret_cast<int32_t*>(&rdx42 + 4) = 0;
                                eax43 = fdutimensat(1, 0xffffff9c);
                                rsp16 = rsp16 - 8 + 8;
                                if (!eax43) 
                                    goto addr_3c8e_42;
                                rax44 = fun_3510();
                                rsp16 = rsp16 - 8 + 8;
                                r8_10 = *rax44;
                                *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
                                if (r8_10 != 9) 
                                    goto addr_3d06_44;
                                zf45 = no_create == 0;
                                if (!zf45) {
                                    addr_3c8e_42:
                                    *reinterpret_cast<uint32_t*>(&rax44) = 1;
                                    goto addr_3c93_46;
                                } else {
                                    if (!r13d32) 
                                        goto addr_3e0b_48;
                                    if (!reinterpret_cast<int1_t>(r13d32 == 21)) 
                                        goto addr_3d1e_50;
                                }
                                goto addr_3e0b_48;
                            } else {
                                rdx42 = r14_31;
                                *reinterpret_cast<uint32_t*>(&rdi46) = *reinterpret_cast<uint32_t*>(&r15_33);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi46) + 4) = 0;
                                eax47 = fdutimensat(rdi46, 0xffffff9c, rdi46, 0xffffff9c);
                                rsp16 = rsp16 - 8 + 8;
                                if (eax47) {
                                    rax44 = fun_3510();
                                    rsp16 = rsp16 - 8 + 8;
                                    r8_10 = *rax44;
                                    *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
                                    if (*reinterpret_cast<uint32_t*>(&r15_33) || (v9 = r8_10, *reinterpret_cast<uint32_t*>(&rax44) = fun_36a0(), rsp16 = rsp16 - 8 + 8, r8_10 = v9, *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0, *reinterpret_cast<uint32_t*>(&rax44) == 0)) {
                                        addr_3d06_44:
                                        if (!r8_10) 
                                            goto addr_3c8e_42;
                                    } else {
                                        addr_3db5_54:
                                        quotearg_style(4, r14_31, rdx42, rcx40);
                                        fun_35b0();
                                        fun_3510();
                                        fun_3800();
                                        rsp16 = rsp16 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                                        *reinterpret_cast<uint32_t*>(&rax44) = 0;
                                        goto addr_3c93_46;
                                    }
                                    if (!r13d32 || r13d32 == 21) {
                                        *reinterpret_cast<unsigned char*>(&rax44) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r8_10 == 2)) & no_create);
                                        if (*reinterpret_cast<unsigned char*>(&rax44)) {
                                            addr_3c93_46:
                                            *reinterpret_cast<uint32_t*>(&r12_8) = *reinterpret_cast<uint32_t*>(&r12_8) & *reinterpret_cast<uint32_t*>(&rax44);
                                            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
                                            eax48 = optind;
                                            eax30 = eax48 + 1;
                                            optind = eax30;
                                            if (eax30 >= ebp3) 
                                                goto addr_3e5b_57;
                                        } else {
                                            addr_3e0b_48:
                                            v9 = r8_10;
                                            quotearg_style(4, r14_31, rdx42, rcx40);
                                            fun_35b0();
                                            fun_3800();
                                            rsp16 = rsp16 - 8 + 8 - 8 + 8 - 8 + 8;
                                            *reinterpret_cast<uint32_t*>(&rax44) = 0;
                                            goto addr_3c93_46;
                                        }
                                    } else {
                                        addr_3d1e_50:
                                        if (!reinterpret_cast<int1_t>(r13d32 == 22) || ((v9 = r8_10, eax49 = fun_3720(r14_31, rsp16 + 64, rdx42, rcx40), rsp16 = rsp16 - 8 + 8, !!eax49) || (r8_10 = v9, *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0, *reinterpret_cast<uint32_t*>(&rax44) = v50 & 0xf000, *reinterpret_cast<uint32_t*>(&rax44) != 0x4000))) {
                                            quotearg_style(4, r14_31, rdx42, rcx40);
                                            fun_35b0();
                                            fun_3800();
                                            rsp16 = rsp16 - 8 + 8 - 8 + 8 - 8 + 8;
                                            *reinterpret_cast<uint32_t*>(&rax44) = 0;
                                            goto addr_3c93_46;
                                        }
                                    }
                                } else {
                                    if (*reinterpret_cast<uint32_t*>(&r15_33)) 
                                        goto addr_3c8e_42;
                                    eax51 = fun_36a0();
                                    rsp16 = rsp16 - 8 + 8;
                                    if (eax51) 
                                        goto addr_3db5_54; else 
                                        goto addr_3c8e_42;
                                }
                            }
                        }
                    } else {
                        goto addr_3e5b_57;
                    }
                }
                addr_407f_62:
                rax52 = fun_35b0();
                *reinterpret_cast<int32_t*>(&rsi13) = 0;
                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                rdx12 = rax52;
                fun_3800();
                rsp16 = rsp16 - 8 + 8 - 8 + 8;
                addr_4094_6:
                *reinterpret_cast<int32_t*>(&rdi20) = 1;
                *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0;
                usage();
                rsp16 = rsp16 - 8 + 8;
                continue;
                addr_3e5b_57:
                *reinterpret_cast<uint32_t*>(&r12_8) = *reinterpret_cast<uint32_t*>(&r12_8) ^ 1;
                *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax53) = *reinterpret_cast<unsigned char*>(&r12_8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax53) + 4) = 0;
                rdx54 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
                if (!rdx54) 
                    break;
                fun_35e0();
                rsp16 = rsp16 - 8 + 8;
                addr_4243_36:
                rcx11 = reinterpret_cast<void**>("touch");
                fun_3670("change_times == CH_ATIME", "src/touch.c", 0x93, "touch", r8_10, r9_21);
                rsp16 = rsp16 - 8 + 8;
                addr_4262_12:
                goto addr_407f_62;
            }
            return rax53;
        } else {
            eax55 = fun_3720(rdi20, rsi13, rdx12, rcx11, rdi20, rsi13, rdx12, rcx11);
            rsp16 = rsp16 - 8 + 8;
            if (eax55) 
                goto addr_40ab_23; else 
                goto addr_3ff7_18;
        }
    }
    if (1) 
        goto addr_3f95_67;
    r12_8 = reinterpret_cast<void**>(rsp16 + 16);
    gettime(r12_8, rsi13, rdx12, rcx11, r8_10, "Arnold Robbins");
    get_reldate(0x19120, 0, r12_8, rcx11, r8_10, "Arnold Robbins");
    rsp16 = rsp16 - 8 + 8 - 8 + 8;
    __asm__("movdqa xmm1, [rip+0x15204]");
    zf56 = change_times == 3;
    __asm__("movaps [rip+0x15206], xmm1");
    if (!zf56) 
        goto addr_3bd7_14;
    zf57 = newtime == v58;
    if (!zf57) 
        goto addr_3bd7_14;
    zf59 = g19128 == v60;
    if (!zf59) 
        goto addr_3bd7_14;
    rsi13 = reinterpret_cast<void**>(0);
    rdi14 = reinterpret_cast<void*>(rsp16 + 48);
    rdx12 = reinterpret_cast<void**>(rsp16 + 32);
    get_reldate(rdi14, 0, rdx12, rcx11, r8_10, "Arnold Robbins");
    rsp16 = rsp16 - 8 + 8;
    if (v61 != (v58 ^ 1)) 
        goto addr_3bd7_14;
    if (v62 != v60) 
        goto addr_3bd7_14;
    addr_3f95_67:
    eax63 = ebp3 - optind;
    if (eax63 <= 1) 
        goto addr_3fa6_73;
    eax64 = posix2_version(rdi14, rsi13, rdx12, rcx11, r8_10, "Arnold Robbins");
    rsp16 = rsp16 - 8 + 8;
    if (eax64 > 0x30daf) 
        goto addr_3fa6_73;
    rax65 = optind;
    r12_8 = reinterpret_cast<void**>(0x19120);
    rsi66 = *reinterpret_cast<void***>(rbx4 + rax65 * 8);
    al67 = posixtime(0x19120, rsi66, 9, rcx11, r8_10, "Arnold Robbins", 0);
    rsp16 = rsp16 - 8 + 8;
    if (al67) 
        goto addr_4144_76;
    addr_3fa6_73:
    zf68 = change_times == 3;
    if (zf68) 
        goto addr_40ea_24;
    g19128 = 0x3fffffff;
    g19138 = 0x3fffffff;
    goto addr_3bd7_14;
    addr_4144_76:
    g19128 = 0;
    __asm__("movdqa xmm2, [rip+0x14fc9]");
    __asm__("movaps [rip+0x14fcb], xmm2");
    rax69 = fun_34b0("POSIXLY_CORRECT", rsi66, 9, rcx11, r8_10, "Arnold Robbins");
    rsp16 = rsp16 - 8 + 8;
    if (!rax69 && (rax70 = fun_34f0(0x19120, rsi66, 9, rcx11, r8_10, "Arnold Robbins", 0), rsp16 = rsp16 - 8 + 8, !!rax70)) {
        *reinterpret_cast<int32_t*>(&rdx71) = rax70->f10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx71) + 4) = 0;
        ecx72 = rax70->f8;
        r12_73 = reinterpret_cast<struct s18*>(static_cast<int64_t>(reinterpret_cast<int32_t>(rax70->f14)));
        rax74 = optind;
        r12_8 = reinterpret_cast<void**>(&r12_73->f76c);
        v9 = ecx72;
        r14_75 = *reinterpret_cast<void***>(rbx4 + rax74 * 8);
        fun_35b0();
        *reinterpret_cast<int32_t*>(&r9_21) = static_cast<int32_t>(rdx71 + 1);
        *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
        r8_10 = r12_8;
        rcx11 = r14_75;
        fun_3800();
        rsp16 = rsp16 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32;
    }
    tmp32_76 = optind + 1;
    optind = tmp32_76;
    goto addr_3bd7_14;
    addr_3b30_8:
    if (eax15 == 0xffffff7d) {
        rdi77 = stdout;
        r9_21 = reinterpret_cast<void**>("Arnold Robbins");
        rcx11 = Version;
        r8_10 = reinterpret_cast<void**>("Paul Rubin");
        rdx12 = reinterpret_cast<void**>("GNU coreutils");
        rsi13 = reinterpret_cast<void**>("touch");
        version_etc(rdi77, "touch", "GNU coreutils", rcx11, "Paul Rubin", "Arnold Robbins", "Jim Kingdon", "David MacKenzie", "Randy Smith", 0);
        eax15 = fun_3860();
        rsp16 = rsp16 - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8;
    }
    if (eax15 != 0xffffff7e) 
        goto addr_4094_6;
    *reinterpret_cast<int32_t*>(&rdi14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    usage();
    rsp16 = rsp16 - 8 + 8;
    goto addr_3b9f_3;
    addr_3a18_9:
    *reinterpret_cast<uint32_t*>(&rax78) = eax15 - 97;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax78) > 31) 
        goto addr_4094_6;
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x12900) + reinterpret_cast<uint64_t>(rax78 * 4)))) + reinterpret_cast<unsigned char>(0x12900);
}

int64_t __libc_start_main = 0;

void fun_4283() {
    __asm__("cli ");
    __libc_start_main(0x3930, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x19008;

void fun_3490(int64_t rdi);

int64_t fun_4323() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_3490(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_4363() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_37e0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_36e0(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_43c3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    void** r12_21;
    void** rax22;
    int32_t eax23;
    void** r13_24;
    void** rax25;
    void** rax26;
    int32_t eax27;
    void** rax28;
    void** rax29;
    void** rax30;
    int32_t eax31;
    void** rax32;
    void** r15_33;
    void** rax34;
    void** rax35;
    void** rdi36;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_35b0();
            fun_37e0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_35b0();
            fun_36e0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_35b0();
            fun_36e0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_35b0();
            fun_36e0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_35b0();
            fun_36e0(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_35b0();
            fun_36e0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_35b0();
            fun_36e0(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_35b0();
            fun_36e0(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_35b0();
            fun_36e0(rax22, r12_21, 5, rcx6);
            do {
                if (1) 
                    break;
                eax23 = fun_3700("touch", 0, "touch", 0);
            } while (eax23);
            r13_24 = v4;
            if (!r13_24) {
                rax25 = fun_35b0();
                fun_37e0(1, rax25, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax26 = fun_37d0(5);
                if (!rax26 || (eax27 = fun_3520(rax26, "en_", 3, rax26, "en_", 3), !eax27)) {
                    rax28 = fun_35b0();
                    r13_24 = reinterpret_cast<void**>("touch");
                    fun_37e0(1, rax28, "https://www.gnu.org/software/coreutils/", "touch");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_24 = reinterpret_cast<void**>("touch");
                    goto addr_4788_9;
                }
            } else {
                rax29 = fun_35b0();
                fun_37e0(1, rax29, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax30 = fun_37d0(5);
                if (!rax30 || (eax31 = fun_3520(rax30, "en_", 3, rax30, "en_", 3), !eax31)) {
                    addr_468e_11:
                    rax32 = fun_35b0();
                    fun_37e0(1, rax32, "https://www.gnu.org/software/coreutils/", "touch");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_24 == "touch")) {
                        r12_2 = reinterpret_cast<void**>(0x12ce0);
                    }
                } else {
                    addr_4788_9:
                    r15_33 = stdout;
                    rax34 = fun_35b0();
                    fun_36e0(rax34, r15_33, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_468e_11;
                }
            }
            rax35 = fun_35b0();
            rcx6 = r12_2;
            fun_37e0(1, rax35, r13_24, rcx6);
            addr_441e_14:
            fun_3860();
        }
    } else {
        fun_35b0();
        rdi36 = stderr;
        rcx6 = r12_2;
        fun_3880(rdi36, rdi36);
        goto addr_441e_14;
    }
}

void fun_47c3() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_36d0(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_47d3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_35d0(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_3520(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_4853_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_48a0_6; else 
                    continue;
            } else {
                rax18 = fun_35d0(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_48d0_8;
                if (v16 == -1) 
                    goto addr_488e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_4853_5;
            } else {
                eax19 = fun_36d0(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_4853_5;
            }
            addr_488e_10:
            v16 = rbx15;
            goto addr_4853_5;
        }
    }
    addr_48b5_16:
    return v12;
    addr_48a0_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_48b5_16;
    addr_48d0_8:
    v12 = rbx15;
    goto addr_48b5_16;
}

int64_t fun_48e3(void** rdi, void*** rsi) {
    void** r12_3;
    void** rdi4;
    void*** rbp5;
    int64_t rbx6;
    int32_t eax7;

    __asm__("cli ");
    r12_3 = rdi;
    rdi4 = *rsi;
    if (!rdi4) {
        addr_4928_2:
        return -1;
    } else {
        rbp5 = rsi;
        *reinterpret_cast<int32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        do {
            eax7 = fun_3700(rdi4, r12_3);
            if (!eax7) 
                break;
            ++rbx6;
            rdi4 = rbp5[rbx6 * 8];
        } while (rdi4);
        goto addr_4928_2;
    }
    return rbx6;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_4943(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_35b0();
    } else {
        fun_35b0();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_3800;
}

void fun_49d3(void*** rdi, void** rsi, void** rdx, void** rcx) {
    void** r13_5;
    void** r12_6;
    void** rdx7;
    void** rbp8;
    void** r14_9;
    void*** v10;
    void** rax11;
    void** rsi12;
    void** r15_13;
    int64_t rbx14;
    uint32_t eax15;
    void** rax16;
    void** rdi17;
    void** rax18;
    void** rdi19;
    void** rdi20;
    void** rax21;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_5) = 0;
    *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
    r12_6 = rdx;
    *reinterpret_cast<int32_t*>(&rdx7) = 5;
    *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
    rbp8 = rsi;
    r14_9 = stderr;
    v10 = rdi;
    rax11 = fun_35b0();
    rsi12 = r14_9;
    fun_36e0(rax11, rsi12, 5, rcx);
    r15_13 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx14) + 4) = 0;
    if (r15_13) {
        do {
            if (!rbx14 || (rdx7 = r12_6, rsi12 = rbp8, eax15 = fun_36d0(r13_5, rsi12, rdx7, r13_5, rsi12, rdx7), !!eax15)) {
                r13_5 = rbp8;
                rax16 = quote(r15_13, rsi12, rdx7, rcx);
                rdi17 = stderr;
                rdx7 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi12) = 1;
                *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
                rcx = rax16;
                fun_3880(rdi17, rdi17);
            } else {
                rax18 = quote(r15_13, rsi12, rdx7, rcx);
                rdi19 = stderr;
                *reinterpret_cast<int32_t*>(&rsi12) = 1;
                *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
                rdx7 = reinterpret_cast<void**>(", %s");
                rcx = rax18;
                fun_3880(rdi19, rdi19);
            }
            ++rbx14;
            rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<unsigned char>(r12_6));
            r15_13 = v10[rbx14 * 8];
        } while (r15_13);
    }
    rdi20 = stderr;
    rax21 = *reinterpret_cast<void***>(rdi20 + 40);
    if (reinterpret_cast<unsigned char>(rax21) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi20 + 48))) {
        *reinterpret_cast<void***>(rdi20 + 40) = rax21 + 1;
        *reinterpret_cast<void***>(rax21) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(void** rdi, void*** rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void*** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_4b03(int64_t rdi, void** rsi, void*** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void*** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_4b47_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_4bbe_4;
        } else {
            addr_4bbe_4:
            return rax14;
        }
    }
    rdi15 = *rdx;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_3700(rdi15, r14_9);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = rbp12[rbx16 * 8];
        } while (rdi15);
        goto addr_4b40_8;
    } else {
        goto addr_4b40_8;
    }
    return rbx16;
    addr_4b40_8:
    rax14 = -1;
    goto addr_4b47_3;
}

struct s19 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_4bd3(void** rdi, struct s19* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_36d0(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_4c33(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4c43(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_3530(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4c53() {
    void** rdi1;
    int32_t eax2;
    void*** rax3;
    int1_t zf4;
    void*** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_3510(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*rax3 == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_35b0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4ce3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_3800();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_3530(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4ce3_5:
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_3800();
    }
}

int32_t fun_3810(int64_t rdi, int64_t rsi, int64_t rdx);

int32_t fun_3610(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t fun_4d03(int32_t edi, int64_t rsi, int32_t edx, int32_t ecx) {
    int64_t rsi5;
    int64_t rdx6;
    int32_t eax7;
    int64_t rax8;
    int64_t rsi9;
    int64_t rdi10;
    int32_t eax11;
    void*** rax12;
    void** r13d13;
    int64_t rax14;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi5) = edx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi5) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx6) = ecx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
    eax7 = fun_3810(rsi, rsi5, rdx6);
    if (edi == eax7 || eax7 < 0) {
        *reinterpret_cast<int32_t*>(&rax8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rsi9) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi10) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
        eax11 = fun_3610(rdi10, rsi9, rdx6);
        rax12 = fun_3510();
        r13d13 = *rax12;
        fun_36a0();
        *reinterpret_cast<int32_t*>(&rax14) = eax11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        *rax12 = r13d13;
        return rax14;
    }
}

int64_t fun_3840();

int64_t fun_34e0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_4d83(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx, int32_t r8d) {
    int64_t rax6;
    void*** rax7;
    void*** rax8;
    int64_t rcx9;
    int64_t rdi10;

    __asm__("cli ");
    if (edi >= 0) {
        rax6 = fun_3840();
        if (*reinterpret_cast<int32_t*>(&rax6) != -1 || !rdx) {
            addr_4db5_3:
            if (*reinterpret_cast<int32_t*>(&rax6) != 1) {
                return rax6;
            }
        } else {
            rax7 = fun_3510();
            if (*rax7 != 38) {
                return 0xffffffff;
            }
        }
    } else {
        if (!rdx) {
            rax8 = fun_3510();
            *rax8 = reinterpret_cast<void**>(9);
            return 0xffffffff;
        } else {
            *reinterpret_cast<int32_t*>(&rcx9) = r8d;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi10) = esi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            rax6 = fun_34e0(rdi10, rdx, rcx, rcx9);
            goto addr_4db5_3;
        }
    }
}

void fun_3560();

void fun_4e23(int64_t rdi) {
    __asm__("cli ");
    goto fun_3560;
}

int64_t fun_4e33() {
    void** rax1;
    void* rcx2;
    int64_t v3;

    __asm__("cli ");
    rax1 = g28;
    fun_3560();
    rcx2 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rcx2) {
        fun_35e0();
    } else {
        return v3;
    }
}

struct s20 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
    signed char[7] pad32;
    int64_t f20;
    int64_t f28;
    int32_t f30;
};

struct s21 {
    void** f0;
    signed char[55] pad56;
    void** f38;
    signed char[7] pad64;
    void** f40;
    signed char[7] pad72;
    int64_t f48;
    void** f50;
    signed char[7] pad88;
    int64_t f58;
    int64_t f60;
    int32_t f68;
};

struct s22 {
    signed char[80960] pad80960;
    signed char f13c40;
    signed char[31] pad80992;
    signed char f13c60;
};

struct s23 {
    signed char[8] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

struct s24 {
    signed char[8] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

struct s25 {
    signed char[8] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

int64_t fun_5c43(void** rdi) {
    uint32_t r15d2;
    void** r9_3;
    void** rcx4;
    void** r12_5;
    void* rsp6;
    void** rax7;
    void** v8;
    void** r11_9;
    void** rbx10;
    uint32_t v11;
    void** r13_12;
    void** r14_13;
    int64_t rax14;
    int64_t rax15;
    void* rdx16;
    int64_t rdx17;
    int32_t eax18;
    int64_t r10_19;
    uint64_t rcx20;
    int64_t rdx21;
    struct s20* rdx22;
    void** v23;
    int64_t v24;
    void** v25;
    int64_t v26;
    int64_t v27;
    void** v28;
    struct s21* rax29;
    void** rdx30;
    uint32_t edi31;
    uint32_t esi32;
    struct s22* rax33;
    uint32_t ecx34;
    uint32_t eax35;
    uint32_t r10d36;
    void** rsi37;
    int64_t rdx38;
    void** r8_39;
    int32_t eax40;
    int64_t rdx41;
    void** rdi42;
    uint32_t eax43;
    int64_t rax44;
    int64_t rax45;
    uint32_t eax46;
    uint64_t rdx47;
    void** v48;
    uint32_t edx49;
    void** r10_50;
    uint32_t edi51;
    int64_t rdx52;
    uint32_t edx53;
    void** r10_54;
    int32_t edi55;
    uint32_t r15d56;
    uint32_t edx57;
    uint32_t esi58;
    int64_t r10_59;
    int64_t v60;
    uint32_t esi61;
    void** rsi62;
    void** rax63;
    int64_t rdi64;
    int32_t edx65;
    uint32_t eax66;
    unsigned char v67;
    int64_t rdx68;
    uint32_t v69;
    void** rsi70;
    struct s23* rbx71;
    void** r15_72;
    uint32_t v73;
    int32_t eax74;
    struct s23* r15_75;
    uint32_t v76;
    void** rax77;
    uint32_t eax78;
    signed char v79;
    void** rsi80;
    int32_t eax81;
    unsigned char dl82;
    void** r11_83;
    struct s5* rax84;
    uint32_t edx85;
    struct s5* rsi86;
    int32_t eax87;
    uint32_t r10d88;
    void** rsi89;
    uint32_t edx90;
    struct s24* rbx91;
    uint32_t r15d92;
    void** v93;
    void** r12_94;
    uint32_t v95;
    unsigned char v96;
    int32_t eax97;
    uint32_t r9d98;
    void** rbx99;
    uint32_t r10d100;
    void** r12_101;
    struct s25* r15_102;
    void** rsi103;
    int32_t eax104;
    void** rsi105;
    int32_t eax106;
    uint32_t eax107;
    unsigned char v108;
    void** rdi109;
    void** rdi110;
    void** rsi111;
    struct s5* rax112;
    void** rax113;
    void** v114;
    uint1_t zf115;

    __asm__("cli ");
    r15d2 = 0xfffffffe;
    *reinterpret_cast<uint32_t*>(&r9_3) = 0;
    *reinterpret_cast<uint32_t*>(&rcx4) = 0xfffffff2;
    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
    r12_5 = rdi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x588);
    rax7 = g28;
    v8 = rax7;
    r11_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x540);
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0xe0);
    v11 = 0;
    r13_12 = r11_9;
    r14_13 = r11_9;
    goto addr_5cb1_2;
    addr_61cc_3:
    return rax14;
    addr_5ef4_4:
    goto *reinterpret_cast<int32_t*>(0x13840 + rax15 * 4) + 0x13840;
    while (1) {
        addr_73f4_5:
        *reinterpret_cast<int32_t*>(&rax14) = 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        while (1) {
            while (rdx16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28)), !!rdx16) {
                fun_35e0();
                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                goto addr_7952_8;
                addr_61b0_9:
                *reinterpret_cast<int32_t*>(&rax14) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                continue;
                while (1) {
                    addr_5da0_10:
                    rdx17 = *reinterpret_cast<int32_t*>(&r9_3);
                    eax18 = *reinterpret_cast<signed char*>(0x13c80 + rdx17);
                    if (eax18) {
                        while (1) {
                            r10_19 = eax18;
                            *reinterpret_cast<uint32_t*>(&rax15) = eax18 - 4;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                            rcx20 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(0x13a80 + r10_19)));
                            rdx21 = 1 - *reinterpret_cast<int32_t*>(&rcx20);
                            rdx22 = reinterpret_cast<struct s20*>(reinterpret_cast<unsigned char>(rbx10) + reinterpret_cast<uint64_t>((rdx21 * 8 - rdx21) * 8));
                            r11_9 = rdx22->f0;
                            v23 = rdx22->f8;
                            v24 = rdx22->f10;
                            v25 = rdx22->f18;
                            v26 = rdx22->f20;
                            v27 = rdx22->f28;
                            *reinterpret_cast<int32_t*>(&v28) = rdx22->f30;
                            if (*reinterpret_cast<uint32_t*>(&rax15) <= 88) 
                                goto addr_5ef4_4;
                            rax29 = reinterpret_cast<struct s21*>(reinterpret_cast<unsigned char>(rbx10) - (rcx20 * 8 - rcx20 << 3));
                            rax29->f40 = v23;
                            rbx10 = reinterpret_cast<void**>(&rax29->f38);
                            rdx30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_12) - rcx20);
                            *reinterpret_cast<void***>(&rax29->f38) = r11_9;
                            rax29->f48 = v24;
                            rax29->f50 = v25;
                            rax29->f58 = v26;
                            rax29->f60 = v27;
                            rax29->f68 = *reinterpret_cast<int32_t*>(&v28);
                            edi31 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx30))));
                            esi32 = edi31;
                            rax33 = reinterpret_cast<struct s22*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(0x13ae0 + r10_19) - 29));
                            ecx34 = rax33->f13c60 + edi31;
                            if (ecx34 > 0x72 || (rcx4 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(ecx34))), *reinterpret_cast<signed char*>(&esi32) != *reinterpret_cast<signed char*>(rcx4 + 0x13b40))) {
                                rcx4 = reinterpret_cast<void**>(0x13c40);
                                *reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x13c40) + reinterpret_cast<uint64_t>(rax33))));
                                eax35 = *reinterpret_cast<uint32_t*>(&r9_3);
                            } else {
                                *reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(rcx4 + 0x13bc0)));
                                eax35 = *reinterpret_cast<uint32_t*>(&r9_3);
                            }
                            while (1) {
                                *reinterpret_cast<void***>(rdx30 + 1) = *reinterpret_cast<void***>(&eax35);
                                r13_12 = rdx30 + 1;
                                if (reinterpret_cast<unsigned char>(r13_12) >= reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(rsp6) + 0x553)) 
                                    goto addr_73f4_5;
                                if (*reinterpret_cast<uint32_t*>(&r9_3) == 12) 
                                    goto addr_73fe_17;
                                *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x13d00 + *reinterpret_cast<int32_t*>(&r9_3))));
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                addr_5cb1_2:
                                r10d36 = *reinterpret_cast<uint32_t*>(&rcx4);
                                if (*reinterpret_cast<uint32_t*>(&rcx4) == 0xffffffa5) 
                                    goto addr_5da0_10;
                                if (r15d2 == 0xfffffffe) {
                                    rsi37 = *reinterpret_cast<void***>(r12_5);
                                    *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                    do {
                                        addr_61f7_21:
                                        if (*reinterpret_cast<signed char*>(&rdx38) <= reinterpret_cast<signed char>(13)) {
                                            if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(8)) 
                                                goto addr_6361_23; else 
                                                goto addr_6209_24;
                                        }
                                        if (*reinterpret_cast<unsigned char*>(&rdx38) != 32) {
                                            addr_6209_24:
                                            *reinterpret_cast<uint32_t*>(&r8_39) = static_cast<uint32_t>(rdx38 - 43) & 0xfffffffd;
                                            *reinterpret_cast<int32_t*>(&r8_39 + 4) = 0;
                                            if (*reinterpret_cast<unsigned char*>(&rdx38) - 48 <= 9) {
                                                if (*reinterpret_cast<signed char*>(&r8_39)) 
                                                    break;
                                            } else {
                                                if (*reinterpret_cast<signed char*>(&r8_39)) {
                                                    if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(90)) {
                                                        eax40 = static_cast<int32_t>(rdx38 - 97);
                                                        if (*reinterpret_cast<unsigned char*>(&eax40) <= 25) 
                                                            goto addr_649b_30;
                                                    } else {
                                                        if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(64)) 
                                                            goto addr_649b_30;
                                                    }
                                                    if (*reinterpret_cast<unsigned char*>(&rdx38) != 40) 
                                                        goto addr_7800_33;
                                                    *reinterpret_cast<int32_t*>(&rdx41) = 0;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx41) + 4) = 0;
                                                    do {
                                                        rdi42 = rsi37;
                                                        ++rsi37;
                                                        *reinterpret_cast<void***>(r12_5) = rsi37;
                                                        eax43 = *reinterpret_cast<unsigned char*>(rsi37 + 0xffffffffffffffff);
                                                        if (!*reinterpret_cast<signed char*>(&eax43)) 
                                                            goto addr_6018_36;
                                                        if (*reinterpret_cast<signed char*>(&eax43) != 40) {
                                                            *reinterpret_cast<uint32_t*>(&rax44) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax43) == 41);
                                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
                                                            rdx41 = rdx41 - rax44;
                                                        } else {
                                                            ++rdx41;
                                                        }
                                                    } while (rdx41);
                                                    *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi42 + 1));
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                                    goto addr_61f7_21;
                                                }
                                            }
                                        } else {
                                            addr_6361_23:
                                            ++rsi37;
                                            *reinterpret_cast<void***>(r12_5) = rsi37;
                                            *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                            goto addr_61f7_21;
                                        }
                                        *reinterpret_cast<int32_t*>(&rax45) = 0;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax45) + 4) = 0;
                                        *reinterpret_cast<unsigned char*>(&rax45) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rdx38) != 45);
                                        eax46 = static_cast<uint32_t>(rax45 + rax45 - 1);
                                        while (1) {
                                            ++rsi37;
                                            *reinterpret_cast<void***>(r12_5) = rsi37;
                                            *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                            if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(13)) {
                                                if (*reinterpret_cast<unsigned char*>(&rdx38) != 32) 
                                                    break;
                                            } else {
                                                if (*reinterpret_cast<signed char*>(&rdx38) <= reinterpret_cast<signed char>(8)) 
                                                    goto addr_6251_46;
                                            }
                                        }
                                        addr_6251_46:
                                    } while (*reinterpret_cast<unsigned char*>(&rdx38) - 48 > 9);
                                    goto addr_625c_48;
                                } else {
                                    addr_5cc7_49:
                                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r15d2) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(r15d2 == 0)) {
                                        addr_6018_36:
                                        *reinterpret_cast<uint32_t*>(&rdx47) = 0;
                                        r15d2 = 0;
                                        goto addr_5cf3_50;
                                    } else {
                                        if (r15d2 == 0x100) {
                                            r15d2 = 0x101;
                                            addr_5df3_53:
                                            while (*reinterpret_cast<uint32_t*>(&rcx4) == 0xffffffa5 || ((*reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4) + 1, *reinterpret_cast<uint32_t*>(&rcx4) > 0x72) || ((rcx4 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rcx4))), *reinterpret_cast<signed char*>(rcx4 + 0x13b40) != 1) || (*reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(rcx4 + 0x13bc0))), eax35 = *reinterpret_cast<uint32_t*>(&r9_3), reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_3) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_3) == 0))))) {
                                                if (r13_12 == r14_13) 
                                                    goto addr_61b0_9;
                                                --r13_12;
                                                rbx10 = rbx10 - 56;
                                                *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x13d00 + *reinterpret_cast<signed char*>(r13_12 + 0xffffffffffffffff))));
                                            }
                                            rbx10 = rbx10 + 56;
                                            v11 = 3;
                                            __asm__("movdqa xmm5, [rsp+0xc0]");
                                            __asm__("movdqa xmm3, [rsp+0xa0]");
                                            *reinterpret_cast<void***>(rbx10 + 48) = v48;
                                            rdx30 = r13_12;
                                            __asm__("movdqa xmm4, [rsp+0xb0]");
                                            __asm__("movups [rbx], xmm3");
                                            __asm__("movups [rbx+0x10], xmm4");
                                            __asm__("movups [rbx+0x20], xmm5");
                                            continue;
                                        }
                                        if (reinterpret_cast<int32_t>(r15d2) <= reinterpret_cast<int32_t>(0x115)) 
                                            goto addr_63b0_58; else 
                                            goto addr_5cea_59;
                                    }
                                }
                                eax46 = 0;
                                addr_625c_48:
                                *reinterpret_cast<uint32_t*>(&r8_39) = 0;
                                *reinterpret_cast<int32_t*>(&r8_39 + 4) = 0;
                                while (1) {
                                    edx49 = *reinterpret_cast<uint32_t*>(&rdx38) - 48;
                                    if (eax46 == 0xffffffff) {
                                        edx49 = 48 - *reinterpret_cast<uint32_t*>(&rdx38);
                                    }
                                    r8_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_39) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(edx49))));
                                    if (__intrinsic()) 
                                        break;
                                    *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37 + 1));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                    r10_50 = rsi37 + 1;
                                    *reinterpret_cast<uint32_t*>(&r11_9) = static_cast<uint32_t>(rdx38 - 48);
                                    *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                    if (*reinterpret_cast<uint32_t*>(&r11_9) > 9) 
                                        goto addr_6297_65;
                                    r8_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_39) * 10);
                                    if (__intrinsic()) 
                                        break;
                                    rsi37 = r10_50;
                                }
                                r15d2 = 63;
                                goto addr_63b0_58;
                                addr_6297_65:
                                edi51 = *reinterpret_cast<uint32_t*>(&rdx38) & 0xfffffffd;
                                if (*reinterpret_cast<signed char*>(&edi51) != 44 || (*reinterpret_cast<uint32_t*>(&rdx52) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r10_50 + 1) - 48), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx52) > 9)) {
                                    *reinterpret_cast<void***>(r12_5) = r10_50;
                                    edx53 = eax46 >> 31;
                                    *reinterpret_cast<uint32_t*>(&rdx47) = edx53 - (edx53 + reinterpret_cast<uint1_t>(edx53 < edx53 + reinterpret_cast<uint1_t>(!!eax46))) + 20;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                    r15d2 = 48 - (48 + reinterpret_cast<uint1_t>(48 < 48 + reinterpret_cast<uint1_t>(!!eax46))) + 0x113;
                                } else {
                                    *reinterpret_cast<uint32_t*>(&r11_9) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi37 + 3))));
                                    *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                    r10_54 = rsi37 + 3;
                                    edi55 = 8;
                                    r15d56 = *reinterpret_cast<uint32_t*>(&r11_9);
                                    do {
                                        edx57 = static_cast<uint32_t>(rdx52 + rdx52 * 4);
                                        esi58 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(r11_9 + 0xffffffffffffffd0));
                                        *reinterpret_cast<uint32_t*>(&rdx52) = edx57 + edx57;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
                                        if (esi58 <= 9) {
                                            *reinterpret_cast<uint32_t*>(&r11_9) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r10_54 + 1))));
                                            *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                            *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx52) + esi58;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
                                            ++r10_54;
                                            r15d56 = *reinterpret_cast<uint32_t*>(&r11_9);
                                            esi58 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(r11_9 + 0xffffffffffffffd0));
                                        }
                                        --edi55;
                                    } while (edi55);
                                    if (eax46 == 0xffffffff) 
                                        goto addr_6826_75; else 
                                        goto addr_62fe_76;
                                }
                                addr_63be_77:
                                r10d36 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rcx4) + rdx47);
                                addr_5cf3_50:
                                if (r10d36 > 0x72) 
                                    goto addr_5da0_10;
                                r10_59 = reinterpret_cast<int32_t>(r10d36);
                                if (static_cast<int32_t>(*reinterpret_cast<signed char*>(0x13b40 + r10_59)) != *reinterpret_cast<uint32_t*>(&rdx47)) 
                                    goto addr_5da0_10;
                                *reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x13bc0 + r10_59)));
                                eax35 = *reinterpret_cast<uint32_t*>(&r9_3);
                                if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_3) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_3) == 0)) 
                                    break;
                                r15d2 = 0xfffffffe;
                                __asm__("movdqa xmm2, [rsp+0xc0]");
                                *reinterpret_cast<int64_t*>(rbx10 + 0x68) = v60;
                                rdx30 = r13_12;
                                __asm__("movups [rbx+0x58], xmm2");
                                rbx10 = rbx10 + 56;
                                v11 = v11 - 1 + reinterpret_cast<uint1_t>(v11 < 1);
                                __asm__("movdqa xmm0, [rsp+0xa0]");
                                __asm__("movdqa xmm1, [rsp+0xb0]");
                                __asm__("movups [rbx], xmm0");
                                __asm__("movups [rbx+0x10], xmm1");
                                continue;
                                addr_6826_75:
                                if (esi58 > 9) {
                                    addr_6849_81:
                                    if (*reinterpret_cast<uint32_t*>(&rdx52)) {
                                        addr_6324_82:
                                        --r8_39;
                                        if (__intrinsic()) {
                                            addr_7459_83:
                                            *reinterpret_cast<uint32_t*>(&rdx47) = 2;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                            r15d2 = 63;
                                            goto addr_63be_77;
                                        } else {
                                            *reinterpret_cast<void***>(r12_5) = r10_54;
                                            r15d2 = 0x114;
                                            *reinterpret_cast<uint32_t*>(&rdx47) = 21;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                            goto addr_63be_77;
                                        }
                                    } else {
                                        *reinterpret_cast<void***>(r12_5) = r10_54;
                                        *reinterpret_cast<uint32_t*>(&rdx47) = 21;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                        r15d2 = 0x114;
                                        goto addr_63be_77;
                                    }
                                } else {
                                    do {
                                        if (*reinterpret_cast<signed char*>(&r15d56) != 48) 
                                            break;
                                        esi61 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r10_54 + 1))));
                                        ++r10_54;
                                        r15d56 = esi61;
                                    } while (esi61 - 48 <= 9);
                                    goto addr_6849_81;
                                }
                                *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx52) + 1;
                                esi58 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r10_54) - 48);
                                addr_62fe_76:
                                if (esi58 <= 9) {
                                    do {
                                        ++r10_54;
                                    } while (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r10_54 + 1) - 48) <= 9);
                                }
                                if (reinterpret_cast<int32_t>(eax46) >= reinterpret_cast<int32_t>(0)) 
                                    goto addr_6880_91;
                                if (*reinterpret_cast<uint32_t*>(&rdx52)) 
                                    goto addr_6324_82;
                                addr_6880_91:
                                *reinterpret_cast<void***>(r12_5) = r10_54;
                                *reinterpret_cast<uint32_t*>(&rdx47) = *reinterpret_cast<uint32_t*>(&rdx52) - (*reinterpret_cast<uint32_t*>(&rdx52) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx52) < *reinterpret_cast<uint32_t*>(&rdx52) + reinterpret_cast<uint1_t>(!!eax46))) + 22;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                r15d2 = r15d56 - (r15d56 + reinterpret_cast<uint1_t>(r15d56 < r15d56 + reinterpret_cast<uint1_t>(!!eax46))) + 0x115;
                                goto addr_63be_77;
                                addr_649b_30:
                                r11_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x560);
                                rsi62 = rsi37 + 1;
                                r8_39 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x573);
                                rax63 = r11_9;
                                while (1) {
                                    if (reinterpret_cast<unsigned char>(rax63) < reinterpret_cast<unsigned char>(r8_39)) {
                                        ++rax63;
                                    }
                                    *reinterpret_cast<void***>(r12_5) = rsi62;
                                    *reinterpret_cast<uint32_t*>(&rdi64) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi62));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi64) + 4) = 0;
                                    if (*reinterpret_cast<signed char*>(&rdi64) <= 90) {
                                        if (*reinterpret_cast<signed char*>(&rdi64) > 64) {
                                            addr_64c4_97:
                                            ++rsi62;
                                        } else {
                                            addr_64be_98:
                                            if (*reinterpret_cast<signed char*>(&rdi64) != 46) 
                                                break; else 
                                                goto addr_64c4_97;
                                        }
                                    } else {
                                        edx65 = static_cast<int32_t>(rdi64 - 97);
                                        if (*reinterpret_cast<unsigned char*>(&edx65) <= 25) 
                                            goto addr_64c4_97;
                                        goto addr_64be_98;
                                    }
                                }
                                eax66 = v67;
                                if (*reinterpret_cast<unsigned char*>(&eax66)) {
                                    do {
                                        *reinterpret_cast<uint32_t*>(&rdx68) = *reinterpret_cast<unsigned char*>(&eax66);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx68) + 4) = 0;
                                        if (static_cast<uint32_t>(rdx68 - 97) < 26) {
                                        }
                                        eax66 = *reinterpret_cast<unsigned char*>(&v8 + 6);
                                    } while (*reinterpret_cast<unsigned char*>(&eax66));
                                }
                                v69 = *reinterpret_cast<uint32_t*>(&rcx4);
                                rsi70 = reinterpret_cast<void**>("AM");
                                v25 = rbx10;
                                rbx71 = reinterpret_cast<struct s23*>(0x18ae0);
                                r15_72 = r11_9;
                                *reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&r9_3);
                                v73 = r10d36;
                                do {
                                    eax74 = fun_3700(r15_72, rsi70);
                                    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    if (!eax74) 
                                        break;
                                    rsi70 = rbx71->f10;
                                    rbx71 = reinterpret_cast<struct s23*>(&rbx71->f10);
                                } while (rsi70);
                                goto addr_7300_109;
                                r15_75 = rbx71;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&v23);
                                r10d36 = v73;
                                rbx10 = v25;
                                *reinterpret_cast<uint32_t*>(&rcx4) = v69;
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                addr_6593_111:
                                r15d2 = r15_75->f8;
                                addr_659b_112:
                                goto addr_5cc7_49;
                                addr_7300_109:
                                rbx10 = v25;
                                v76 = *reinterpret_cast<uint32_t*>(&v23);
                                rax77 = fun_35d0(r15_72);
                                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&v23) = 1;
                                *reinterpret_cast<uint32_t*>(&rcx4) = v69;
                                v28 = rax77;
                                r10d36 = v73;
                                *reinterpret_cast<uint32_t*>(&r9_3) = v76;
                                if (rax77 != 3 && (*reinterpret_cast<uint32_t*>(&v23) = 0, reinterpret_cast<int1_t>(rax77 == 4))) {
                                    eax78 = 0;
                                    *reinterpret_cast<unsigned char*>(&eax78) = reinterpret_cast<uint1_t>(v79 == 46);
                                    *reinterpret_cast<uint32_t*>(&v23) = eax78;
                                }
                                v25 = r15_72;
                                r15_75 = reinterpret_cast<struct s23*>(0x18940);
                                rsi80 = reinterpret_cast<void**>("JANUARY");
                                do {
                                    *reinterpret_cast<uint32_t*>(&r11_9) = *reinterpret_cast<uint32_t*>(&v23);
                                    *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                    if (*reinterpret_cast<uint32_t*>(&r11_9)) {
                                        eax81 = fun_3520(v25, rsi80, 3);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    } else {
                                        eax81 = fun_3700(v25, rsi80);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    }
                                    *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                    r10d36 = r10d36;
                                    dl82 = reinterpret_cast<uint1_t>(eax81 == 0);
                                    *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                    if (dl82) 
                                        goto addr_6593_111;
                                    rsi80 = r15_75->f10;
                                    r15_75 = reinterpret_cast<struct s23*>(&r15_75->f10);
                                } while (rsi80);
                                r11_83 = v25;
                                *reinterpret_cast<uint32_t*>(&v25) = *reinterpret_cast<uint32_t*>(&rcx4);
                                v23 = r11_83;
                                rax84 = lookup_zone(r12_5, r11_83);
                                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                r11_9 = v23;
                                *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&v25);
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                edx85 = dl82;
                                r10d36 = r10d36;
                                rsi86 = rax84;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                if (!rax84) {
                                    *reinterpret_cast<uint32_t*>(&v25) = r10d36;
                                    v23 = r11_9;
                                    eax87 = fun_3700(r11_9, "DST");
                                    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    r11_9 = v23;
                                    r10d88 = *reinterpret_cast<uint32_t*>(&v25);
                                    rsi89 = reinterpret_cast<void**>("YEAR");
                                    edx90 = *reinterpret_cast<unsigned char*>(&edx85);
                                    *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                    *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                    if (!eax87) {
                                        r15d2 = 0x103;
                                        goto addr_63b0_58;
                                    } else {
                                        v25 = rbx10;
                                        rbx91 = reinterpret_cast<struct s24*>(0x18880);
                                        r15d92 = *reinterpret_cast<uint32_t*>(&rcx4);
                                        v93 = r12_5;
                                        r12_94 = r11_9;
                                        *reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&r9_3);
                                        v95 = r10d88;
                                        v96 = *reinterpret_cast<unsigned char*>(&edx90);
                                        do {
                                            eax97 = fun_3700(r12_94, rsi89);
                                            rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                            if (!eax97) 
                                                break;
                                            rsi89 = rbx91->f10;
                                            rbx91 = reinterpret_cast<struct s24*>(&rbx91->f10);
                                        } while (rsi89);
                                        goto addr_762b_126;
                                    }
                                } else {
                                    addr_744c_127:
                                    r15d2 = rsi86->f8;
                                    goto addr_659b_112;
                                }
                                *reinterpret_cast<uint32_t*>(&rcx4) = r15d92;
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&v23);
                                r10d36 = v95;
                                r12_5 = v93;
                                rbx10 = v25;
                                r15d2 = rbx91->f8;
                                goto addr_659b_112;
                                addr_762b_126:
                                r11_9 = r12_94;
                                r9d98 = *reinterpret_cast<uint32_t*>(&v23);
                                *reinterpret_cast<uint32_t*>(&rcx4) = r15d92;
                                rbx99 = v25;
                                r10d100 = v95;
                                r8_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_9) + reinterpret_cast<unsigned char>(v28) + 0xffffffffffffffff);
                                *reinterpret_cast<uint32_t*>(&rdx16) = v96;
                                r12_101 = v93;
                                if (*reinterpret_cast<void***>(r8_39) == 83) {
                                    v23 = r12_101;
                                    r15_102 = reinterpret_cast<struct s25*>(0x18880);
                                    r12_5 = rbx99;
                                    *reinterpret_cast<uint32_t*>(&rbx10) = r9d98;
                                    v25 = r14_13;
                                    rsi103 = reinterpret_cast<void**>("YEAR");
                                    *reinterpret_cast<uint32_t*>(&r14_13) = r10d100;
                                    *reinterpret_cast<void***>(r8_39) = reinterpret_cast<void**>(0);
                                    do {
                                        eax104 = fun_3700(r11_9, rsi103);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                        r11_9 = r11_9;
                                        *reinterpret_cast<uint32_t*>(&rdx16) = *reinterpret_cast<unsigned char*>(&rdx16);
                                        r8_39 = r8_39;
                                        *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                        *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                        if (!eax104) 
                                            break;
                                        rsi103 = r15_102->f10;
                                        r15_102 = reinterpret_cast<struct s25*>(&r15_102->f10);
                                    } while (rsi103);
                                    goto addr_7952_8;
                                } else {
                                    addr_765e_132:
                                    v23 = r12_101;
                                    r15_102 = reinterpret_cast<struct s25*>(0x18720);
                                    r12_5 = rbx99;
                                    *reinterpret_cast<uint32_t*>(&rbx10) = r9d98;
                                    v25 = r14_13;
                                    rsi105 = reinterpret_cast<void**>("TOMORROW");
                                    *reinterpret_cast<uint32_t*>(&r14_13) = r10d100;
                                    goto addr_7692_133;
                                }
                                addr_76b9_135:
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&rbx10);
                                r10d36 = *reinterpret_cast<uint32_t*>(&r14_13);
                                rbx10 = r12_5;
                                r14_13 = v25;
                                r12_5 = v23;
                                r15d2 = r15_102->f8;
                                goto addr_659b_112;
                                addr_7952_8:
                                r9d98 = *reinterpret_cast<uint32_t*>(&rbx10);
                                r10d100 = *reinterpret_cast<uint32_t*>(&r14_13);
                                rbx99 = r12_5;
                                *reinterpret_cast<void***>(r8_39) = reinterpret_cast<void**>(83);
                                r12_101 = v23;
                                r14_13 = v25;
                                goto addr_765e_132;
                                do {
                                    addr_7692_133:
                                    eax106 = fun_3700(r11_9, rsi105);
                                    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    r11_9 = r11_9;
                                    *reinterpret_cast<uint32_t*>(&rdx16) = *reinterpret_cast<unsigned char*>(&rdx16);
                                    *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                    if (!eax106) 
                                        goto addr_76b9_135;
                                    rsi105 = r15_102->f10;
                                    r15_102 = reinterpret_cast<struct s25*>(&r15_102->f10);
                                } while (rsi105);
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&rbx10);
                                *reinterpret_cast<int32_t*>(&r9_3 + 4) = 0;
                                r10d36 = *reinterpret_cast<uint32_t*>(&r14_13);
                                rbx10 = r12_5;
                                r14_13 = v25;
                                r12_5 = v23;
                                eax107 = v108;
                                if (v28 == 1) {
                                    rsi86 = reinterpret_cast<struct s5*>(0x18220);
                                    rdi109 = reinterpret_cast<void**>("A");
                                    do {
                                        if (*reinterpret_cast<void***>(rdi109) == *reinterpret_cast<void***>(&eax107)) 
                                            break;
                                        rdi109 = rsi86->f10;
                                        rsi86 = reinterpret_cast<struct s5*>(&rsi86->f10);
                                    } while (rdi109);
                                    goto addr_7852_141;
                                } else {
                                    addr_7852_141:
                                    rdi110 = r11_9;
                                    rsi111 = r11_9;
                                    if (*reinterpret_cast<void***>(&eax107)) {
                                        do {
                                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax107) == 46)) {
                                                ++rsi111;
                                            } else {
                                                *reinterpret_cast<uint32_t*>(&rdx16) = 1;
                                            }
                                            eax107 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi110 + 1));
                                            ++rdi110;
                                            *reinterpret_cast<void***>(rsi111) = *reinterpret_cast<void***>(&eax107);
                                        } while (*reinterpret_cast<void***>(&eax107));
                                        if (!*reinterpret_cast<unsigned char*>(&rdx16)) 
                                            goto addr_787d_147;
                                        *reinterpret_cast<uint32_t*>(&v25) = *reinterpret_cast<uint32_t*>(&rcx4);
                                        v23 = r11_9;
                                        rax112 = lookup_zone(r12_5, r11_9);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                        r11_9 = v23;
                                        *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&v25);
                                        *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                        *reinterpret_cast<int32_t*>(&r9_3 + 4) = 0;
                                        if (!rax112) 
                                            goto addr_787d_147; else 
                                            goto addr_7911_149;
                                    } else {
                                        goto addr_787d_147;
                                    }
                                }
                                goto addr_744c_127;
                                addr_787d_147:
                                if (!*reinterpret_cast<signed char*>(r12_5 + 0xe1)) 
                                    goto addr_7459_83;
                                *reinterpret_cast<uint32_t*>(&v25) = *reinterpret_cast<uint32_t*>(&rcx4);
                                r15d2 = 63;
                                v23 = r11_9;
                                rax113 = fun_35b0();
                                dbg_printf(rax113, v23, 5, rcx4, r8_39, r9_3, v114);
                                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&v25);
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                *reinterpret_cast<uint32_t*>(&rdx47) = 2;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                goto addr_63be_77;
                                addr_7911_149:
                                r15d2 = rax112->f8;
                                r10d36 = r10d36;
                                goto addr_659b_112;
                                addr_7800_33:
                                *reinterpret_cast<uint32_t*>(&rdx47) = 0;
                                *reinterpret_cast<void***>(r12_5) = rsi37 + 1;
                                r15d2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                if (r15d2) {
                                    addr_63b0_58:
                                    *reinterpret_cast<uint32_t*>(&rdx47) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x13d80 + r15d2)));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                    goto addr_63be_77;
                                } else {
                                    goto addr_5cf3_50;
                                }
                                addr_5cea_59:
                                r10d36 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rcx4 + 2));
                                *reinterpret_cast<uint32_t*>(&rdx47) = 2;
                                goto addr_5cf3_50;
                            }
                            eax18 = reinterpret_cast<int32_t>(-eax35);
                        }
                    } else {
                        if (v11 == 3) {
                            zf115 = reinterpret_cast<uint1_t>(r15d2 == 0);
                            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r15d2) < reinterpret_cast<int32_t>(0)) | zf115)) {
                                *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x13d00 + rdx17)));
                                r15d2 = 0xfffffffe;
                                goto addr_5df3_53;
                            } else {
                                if (zf115) 
                                    goto addr_61b0_9;
                            }
                        }
                        *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x13d00 + rdx17)));
                        goto addr_5df3_53;
                    }
                }
            }
            goto addr_61cc_3;
            addr_73fe_17:
            *reinterpret_cast<int32_t*>(&rax14) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        }
    }
}

void fun_9a03() {
    __asm__("cli ");
    goto parse_datetime_body;
}

int64_t fun_9a13(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    uint32_t r12d7;
    void** rax8;
    void** rax9;
    uint32_t eax10;
    int64_t rax11;

    __asm__("cli ");
    r12d7 = 0;
    rax8 = fun_34b0("TZ", rsi, rdx, rcx, r8, r9);
    rax9 = tzalloc(rax8, rsi);
    if (rax9) {
        eax10 = parse_datetime_body(rdi, rsi, rdx, 0, rax9, rax8);
        r12d7 = eax10;
        tzfree(rax9, rsi, rax9, rsi);
    }
    *reinterpret_cast<uint32_t*>(&rax11) = r12d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    return rax11;
}

struct s26 {
    signed char[1] pad1;
    signed char f1;
    signed char f2;
};

struct s26* fun_3620(void** rdi, int64_t rsi);

struct s27 {
    int32_t f0;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
};

void** rpl_mktime(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_9b33(uint64_t* rdi, void** rsi, uint32_t edx) {
    uint32_t r12d4;
    void** rbp5;
    uint64_t* v6;
    void** rax7;
    void** v8;
    void** rax9;
    void* rsp10;
    void** rbx11;
    void** r14_12;
    struct s26* r13_13;
    struct s26* rax14;
    void** rax15;
    void** rdx16;
    void* r14_17;
    void* rax18;
    void** rbx19;
    int64_t rdx20;
    int64_t rcx21;
    int64_t rdx22;
    void** rdx23;
    void** rcx24;
    void** rsi25;
    void** r8_26;
    void** r9_27;
    signed char al28;
    struct s27* rax29;
    uint32_t v30;
    uint32_t v31;
    uint32_t eax32;
    uint32_t v33;
    uint32_t v34;
    int32_t v35;
    uint32_t v36;
    uint32_t v37;
    uint32_t v38;
    void** r8_39;
    void** r9_40;
    signed char al41;
    uint32_t r15d42;
    int64_t rax43;
    int64_t rax44;
    int64_t v45;
    uint32_t r14d46;
    uint32_t r13d47;
    uint32_t r12d48;
    void** v49;
    uint32_t ebp50;
    uint32_t ebx51;
    uint32_t v52;
    void** rax53;
    int64_t rax54;
    int64_t rax55;
    int64_t rax56;
    void* rdx57;

    __asm__("cli ");
    r12d4 = edx;
    rbp5 = rsi;
    v6 = rdi;
    rax7 = g28;
    v8 = rax7;
    rax9 = fun_35d0(rsi);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8 - 8 + 8);
    rbx11 = rax9;
    r14_12 = rax9;
    if (!(*reinterpret_cast<unsigned char*>(&r12d4) & 4)) {
        *reinterpret_cast<int32_t*>(&r13_13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_13) + 4) = 0;
    } else {
        rax14 = fun_3620(rbp5, 46);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        r13_13 = rax14;
        if (rax14) {
            rax15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax14) - reinterpret_cast<unsigned char>(rbp5));
            if (reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rax15) == 3) {
                rbx11 = rax15;
                r14_12 = rax15;
            } else {
                goto addr_9ba0_7;
            }
        }
    }
    if (reinterpret_cast<uint64_t>(rbx11 + 0xfffffffffffffff8) > 4) 
        goto addr_9ba0_7;
    if (*reinterpret_cast<uint32_t*>(&rbx11) & 1) 
        goto addr_9ba0_7;
    *reinterpret_cast<int32_t*>(&rdx16) = 0;
    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    do {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(rdx16)) - 48 > 9) 
            goto addr_9ba0_7;
        ++rdx16;
    } while (reinterpret_cast<signed char>(r14_12) > reinterpret_cast<signed char>(rdx16));
    r14_17 = reinterpret_cast<void*>(reinterpret_cast<signed char>(r14_12) >> 1);
    *reinterpret_cast<int32_t*>(&rax18) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    rbx19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 0xa0);
    do {
        *reinterpret_cast<int32_t*>(&rdx20) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp5 + reinterpret_cast<int64_t>(rax18) * 2));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rcx21) = static_cast<int32_t>(rdx20 + rdx20 * 4 - 0xf0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx21) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx22) = *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp5 + reinterpret_cast<int64_t>(rax18) * 2) + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
        *reinterpret_cast<void***>(rbx19 + reinterpret_cast<int64_t>(rax18) * 4) = reinterpret_cast<void**>(static_cast<uint32_t>(rdx22 + rcx21 * 2 - 48));
        rax18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax18) + 1);
    } while (r14_17 != rax18);
    rdx23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_17) + 0xfffffffffffffffc);
    if (!(*reinterpret_cast<unsigned char*>(&r12d4) & 1)) {
        *reinterpret_cast<uint32_t*>(&rcx24) = r12d4;
        *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
        rsi25 = rbx19;
        al28 = year(reinterpret_cast<int64_t>(rsp10) + 32, rsi25, rdx23, rcx24, r8_26, r9_27);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        if (!al28) 
            goto addr_9ba0_7;
        rax29 = reinterpret_cast<struct s27*>(reinterpret_cast<uint64_t>(rbx19 + reinterpret_cast<int64_t>(r14_17) * 4) - 16);
        v30 = reinterpret_cast<uint32_t>(rax29->f0 - 1);
        v31 = rax29->f4;
        *reinterpret_cast<uint32_t*>(&rdx23) = rax29->f8;
        *reinterpret_cast<int32_t*>(&rdx23 + 4) = 0;
        eax32 = rax29->fc;
        v33 = *reinterpret_cast<uint32_t*>(&rdx23);
        v34 = eax32;
    } else {
        rsi25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 0xb0);
        *reinterpret_cast<uint32_t*>(&rcx24) = r12d4;
        *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
        v30 = reinterpret_cast<uint32_t>(v35 - 1);
        v31 = v36;
        v33 = v37;
        v34 = v38;
        al41 = year(reinterpret_cast<int64_t>(rsp10) + 32, rsi25, rdx23, rcx24, r8_39, r9_40);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        if (!al41) 
            goto addr_9ba0_7;
    }
    if (!r13_13) {
        r15d42 = 0;
    } else {
        *reinterpret_cast<uint32_t*>(&rax43) = r13_13->f1 - 48;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rax43) > 9) 
            goto addr_9ba0_7;
        *reinterpret_cast<uint32_t*>(&rdx23) = reinterpret_cast<uint32_t>(static_cast<int32_t>(r13_13->f2));
        *reinterpret_cast<int32_t*>(&rdx23 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx24) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdx23 + 0xffffffffffffffd0));
        *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rcx24) > 9) 
            goto addr_9ba0_7;
        *reinterpret_cast<int32_t*>(&rax44) = static_cast<int32_t>(rax43 + rax43 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
        r15d42 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rdx23) + reinterpret_cast<uint64_t>(rax44 * 2) - 48);
    }
    *reinterpret_cast<signed char*>(&v45) = 0;
    r14d46 = v34;
    r13d47 = v33;
    r12d48 = v31;
    v49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 96);
    ebp50 = v30;
    ebx51 = v52;
    while (rax53 = rpl_mktime(v49, rsi25, rdx23, rcx24), rcx24 = rax53, 0) {
        *reinterpret_cast<uint32_t*>(&rsi25) = r15d42 ^ r15d42;
        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
        if (!(ebx51 ^ ebx51 | ebp50 ^ ebp50 | r12d48 ^ r12d48 | r13d47 ^ r13d47 | r14d46 ^ r14d46 | *reinterpret_cast<uint32_t*>(&rsi25))) 
            goto addr_9da0_27;
        if (r15d42 != 60) 
            goto addr_9ba0_7;
        *reinterpret_cast<signed char*>(&v45) = 1;
        r15d42 = 59;
    }
    goto addr_9ba0_7;
    addr_9da0_27:
    rax54 = v45;
    *reinterpret_cast<uint32_t*>(&rax55) = *reinterpret_cast<uint32_t*>(&rax54) & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax55) + 4) = 0;
    if (__intrinsic()) {
        addr_9ba0_7:
        *reinterpret_cast<int32_t*>(&rax56) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax56) + 4) = 0;
    } else {
        *v6 = reinterpret_cast<uint64_t>(rax55 + reinterpret_cast<unsigned char>(rcx24));
        *reinterpret_cast<int32_t*>(&rax56) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax56) + 4) = 0;
    }
    rdx57 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (rdx57) {
        fun_35e0();
    } else {
        return rax56;
    }
}

int64_t fun_3730(void** rdi, signed char** rsi, int64_t rdx);

int64_t fun_9e13() {
    int64_t r12_1;
    void** rax2;
    void** rsi3;
    void** rdx4;
    void** rcx5;
    void** r8_6;
    void** r9_7;
    void** rax8;
    int64_t rax9;
    signed char* v10;
    void* rax11;
    int64_t rax12;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_1) = 0x31069;
    rax2 = g28;
    rax8 = fun_34b0("_POSIX2_VERSION", rsi3, rdx4, rcx5, r8_6, r9_7);
    if (rax8 && (*reinterpret_cast<void***>(rax8) && (rax9 = fun_3730(rax8, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 + 8, 10), !*v10))) {
        if (rax9 < 0xffffffff80000000) {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x80000000;
        } else {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x7fffffff;
            if (rax9 <= 0x7fffffff) {
                r12_1 = rax9;
            }
        }
    }
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax11) {
        fun_35e0();
    } else {
        *reinterpret_cast<int32_t*>(&rax12) = *reinterpret_cast<int32_t*>(&r12_1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        return rax12;
    }
}

struct s28 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s28* fun_3650();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_9ea3(void** rdi) {
    void** rbx2;
    struct s28* rax3;
    void** r12_4;
    int32_t eax5;

    __asm__("cli ");
    if (!rdi) {
        fun_3870("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
        fun_3500("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx2 = rdi;
        rax3 = fun_3650();
        if (rax3 && ((r12_4 = reinterpret_cast<void**>(&rax3->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_4) - reinterpret_cast<unsigned char>(rbx2)) > reinterpret_cast<int64_t>(6)) && (eax5 = fun_3520(reinterpret_cast<int64_t>(rax3) + 0xfffffffffffffffa, "/.libs/", 7), !eax5))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax3->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_4 + 1) == 0x74) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_4 + 2) == 45))) {
                rbx2 = r12_4;
            } else {
                rbx2 = reinterpret_cast<void**>(&rax3->f4);
                __progname = rbx2;
            }
        }
        program_name = rbx2;
        __progname_full = rbx2;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_b643(int64_t rdi) {
    int64_t rbp2;
    void*** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_3510();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x19260;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_b683(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x19260);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_b6a3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x19260);
    }
    *rdi = esi;
    return 0x19260;
}

int64_t fun_b6c3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x19260);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s29 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_b703(struct s29* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s29*>(0x19260);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s30 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s30* fun_b723(struct s30* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s30*>(0x19260);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x38ea;
    if (!rdx) 
        goto 0x38ea;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x19260;
}

struct s31 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_b763(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s31* r8) {
    struct s31* rbx6;
    void*** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s31*>(0x19260);
    }
    rax7 = fun_3510();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0xb796);
    *rax7 = r15d8;
    return rax13;
}

struct s32 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_b7e3(int64_t rdi, int64_t rsi, void*** rdx, struct s32* rcx) {
    struct s32* rbx5;
    void*** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s32*>(0x19260);
    }
    rax6 = fun_3510();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0xb811);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0xb86c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_b8d3() {
    __asm__("cli ");
}

void** g19098 = reinterpret_cast<void**>(96);

int64_t slotvec0 = 0x100;

void fun_b8e3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_34d0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x19160) {
        fun_34d0(rdi7);
        g19098 = reinterpret_cast<void**>(0x19160);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x19090) {
        fun_34d0(r12_2);
        slotvec = reinterpret_cast<void**>(0x19090);
    }
    nslots = 1;
    return;
}

void fun_b983() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b9a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b9b3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b9d3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_b9f3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s11* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x38f0;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_35e0();
    } else {
        return rax6;
    }
}

void** fun_ba83(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s11* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x38f5;
    rcx6 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_35e0();
    } else {
        return rax7;
    }
}

void** fun_bb13(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s11* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x38fa;
    rcx4 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_35e0();
    } else {
        return rax5;
    }
}

void** fun_bba3(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s11* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x38ff;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_35e0();
    } else {
        return rax6;
    }
}

void** fun_bc33(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s11* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xd620]");
    __asm__("movdqa xmm1, [rip+0xd628]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xd611]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_35e0();
    } else {
        return rax10;
    }
}

void** fun_bcd3(int64_t rdi, uint32_t esi) {
    struct s11* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xd580]");
    __asm__("movdqa xmm1, [rip+0xd588]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xd571]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_35e0();
    } else {
        return rax9;
    }
}

void** fun_bd73(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd4e0]");
    __asm__("movdqa xmm1, [rip+0xd4e8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xd4c9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_35e0();
    } else {
        return rax3;
    }
}

void** fun_be03(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd450]");
    __asm__("movdqa xmm1, [rip+0xd458]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xd446]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_35e0();
    } else {
        return rax4;
    }
}

void** fun_be93(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s11* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3904;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_35e0();
    } else {
        return rax6;
    }
}

void** fun_bf33(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s11* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd31a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xd312]");
    __asm__("movdqa xmm2, [rip+0xd31a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3909;
    if (!rdx) 
        goto 0x3909;
    rcx6 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_35e0();
    } else {
        return rax7;
    }
}

void** fun_bfd3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s11* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd27a]");
    __asm__("movdqa xmm1, [rip+0xd282]");
    __asm__("movdqa xmm2, [rip+0xd28a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x390e;
    if (!rdx) 
        goto 0x390e;
    rcx7 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_35e0();
    } else {
        return rax9;
    }
}

void** fun_c083(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s11* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd1ca]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xd1c2]");
    __asm__("movdqa xmm2, [rip+0xd1ca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3913;
    if (!rsi) 
        goto 0x3913;
    rcx5 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_35e0();
    } else {
        return rax6;
    }
}

void** fun_c123(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s11* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd12a]");
    __asm__("movdqa xmm1, [rip+0xd132]");
    __asm__("movdqa xmm2, [rip+0xd13a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3918;
    if (!rsi) 
        goto 0x3918;
    rcx6 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_35e0();
    } else {
        return rax7;
    }
}

void fun_c1c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_c1d3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_c1f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_c213(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_c2c3(void** rdi, void** rsi) {
    void** rax3;
    void** r12_4;
    void** rax5;
    void** rbx6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    if (!rdi) {
        rax3 = fun_3780(0x80, rsi);
        r12_4 = rax3;
        if (rax3) {
            *reinterpret_cast<void***>(r12_4) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0);
            return r12_4;
        }
    } else {
        rax5 = fun_35d0(rdi);
        rbx6 = rax5 + 1;
        *reinterpret_cast<int32_t*>(&rax7) = 0x76;
        *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx6) >= reinterpret_cast<unsigned char>(0x76)) {
            rax7 = rbx6;
        }
        rax8 = fun_3780(reinterpret_cast<uint64_t>(rax7 + 17) & 0xfffffffffffffff8, rsi);
        r12_4 = rax8;
        if (rax8) {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(1);
            fun_3740(r12_4 + 9, rdi, rbx6);
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(rbx6) + 9) = 0;
        }
    }
    return r12_4;
}

void fun_c593(void** rdi) {
    void** rbx2;
    void** rdi3;

    __asm__("cli ");
    if (rdi == 1) {
        return;
    } else {
        rbx2 = rdi;
        if (rdi) {
            do {
                rdi3 = rbx2;
                rbx2 = *reinterpret_cast<void***>(rbx2);
                fun_34d0(rdi3);
            } while (rbx2);
        }
        return;
    }
}

int64_t fun_3470(void** rdi, void** rsi);

void** fun_c5d3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rsi8;
    int64_t rax9;
    signed char al10;
    signed char al11;

    __asm__("cli ");
    if (!rdi) {
        goto fun_3480;
    } else {
        rax7 = set_tz(rdi, rsi, rdx, rcx, r8, r9);
        if (rax7) {
            rsi8 = rdx;
            rax9 = fun_3470(rsi, rsi8);
            if (!rax9 || (rsi8 = rdx, al10 = save_abbr(rdi, rsi8), al10 == 0)) {
                if (rax7 != 1) {
                    revert_tz_part_0(rax7, rsi8);
                }
            } else {
                if (reinterpret_cast<int1_t>(rax7 == 1) || (al11 = revert_tz_part_0(rax7, rsi8), !!al11)) {
                    return rdx;
                }
            }
        }
        return 0;
    }
}

void** fun_c683(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void** rax8;
    void* rax9;
    void** rax10;
    void** r13_11;
    void** r15_12;
    void** rax13;
    signed char al14;
    signed char al15;
    void** v16;
    void* rax17;

    __asm__("cli ");
    rbp7 = rsi;
    rax8 = g28;
    if (!rdi) {
        rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
        if (rax9) {
            fun_35e0();
        }
    }
    rax10 = set_tz(rdi, rsi, rdx, rcx, r8, r9);
    if (!rax10) {
        addr_c710_7:
        r13_11 = reinterpret_cast<void**>(0xffffffffffffffff);
    } else {
        r15_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 64) - 8 + 8);
        rax13 = rpl_mktime(r15_12, rsi, rdx, rcx);
        r13_11 = rax13;
        if (!0 || (rsi = r15_12, al14 = save_abbr(rdi, rsi), al14 == 0)) {
            if (rax10 != 1) {
                revert_tz_part_0(rax10, rsi);
                goto addr_c710_7;
            }
        } else {
            if (reinterpret_cast<int1_t>(rax10 == 1) || (al15 = revert_tz_part_0(rax10, rsi), !!al15)) {
                __asm__("movdqa xmm0, [rsp]");
                __asm__("movdqa xmm1, [rsp+0x10]");
                __asm__("movdqa xmm2, [rsp+0x20]");
                __asm__("movups [rbp+0x0], xmm0");
                *reinterpret_cast<void***>(rbp7 + 48) = v16;
                __asm__("movups [rbp+0x10], xmm1");
                __asm__("movups [rbp+0x20], xmm2");
            } else {
                goto addr_c710_7;
            }
        }
    }
    rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
    if (!rax17) {
        return r13_11;
    }
}

struct s33 {
    signed char[32] pad32;
    int32_t f20;
};

void fun_c7d3(struct s33* rdi) {
    __asm__("cli ");
    rdi->f20 = 0;
    goto 0xd7a0;
}

void fun_3710(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8, uint64_t r9);

void fun_c7f3(void** rdi, int64_t rsi, int64_t rdx, uint64_t rcx, int64_t r8, uint64_t r9) {
    uint64_t r12_7;
    void** rax8;
    void** rax9;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_3880(rdi, rdi);
    } else {
        r9 = rcx;
        fun_3880(rdi, rdi);
    }
    rax8 = fun_35b0();
    fun_3880(rdi, rdi);
    fun_3710(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_35b0();
    fun_3880(rdi, rdi);
    fun_3710(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        fun_35b0();
        fun_3880(rdi, rdi);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x145c8 + r12_7 * 4) + 0x145c8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_cc63() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s34 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_cc83(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s34* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s34* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_35e0();
    } else {
        return;
    }
}

void fun_cd23(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_cdc6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_cdd0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_35e0();
    } else {
        return;
    }
    addr_cdc6_5:
    goto addr_cdd0_7;
}

void fun_ce03() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_3710(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_35b0();
    fun_37e0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_35b0();
    fun_37e0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_35b0();
    goto fun_37e0;
}

int64_t fun_3550();

void xalloc_die();

void fun_cea3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3550();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_cee3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3780(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_cf03(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3780(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_cf23(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3780(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_37c0(void** rdi, void** rsi, ...);

void fun_cf43(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_37c0(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_cf73(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_37c0(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_cfa3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3550();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_cfe3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_3550();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d023(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_3550();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d053(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_3550();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d0a3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_3550();
            if (rax5) 
                break;
            addr_d0ed_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_d0ed_5;
        rax8 = fun_3550();
        if (rax8) 
            goto addr_d0d6_9;
        if (rbx4) 
            goto addr_d0ed_5;
        addr_d0d6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_d133(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_3550();
            if (rax8) 
                break;
            addr_d17a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_d17a_5;
        rax11 = fun_3550();
        if (rax11) 
            goto addr_d162_9;
        if (!rbx6) 
            goto addr_d162_9;
        if (r12_4) 
            goto addr_d17a_5;
        addr_d162_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_d1c3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_d26d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_d280_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_d220_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_d246_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_d294_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_d23d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_d294_14;
            addr_d23d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_d294_14;
            addr_d246_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_37c0(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_d294_14;
            if (!rbp13) 
                break;
            addr_d294_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_d26d_9;
        } else {
            if (!r13_6) 
                goto addr_d280_10;
            goto addr_d220_11;
        }
    }
}

int64_t fun_36f0();

void fun_d2c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_36f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d2f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_36f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d323() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_36f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d343() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_36f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_d363(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3780(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_3740;
    }
}

void fun_d3a3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3780(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_3740;
    }
}

void fun_d3e3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3780(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_3740;
    }
}

void fun_d423(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_35d0(rdi);
    rax4 = fun_3780(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3740;
    }
}

void fun_d463() {
    void** rdi1;

    __asm__("cli ");
    fun_35b0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_3800();
    fun_3500(rdi1);
}

struct s35 {
    int32_t f0;
    uint32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    signed char[8] pad32;
    uint32_t f20;
    signed char[12] pad48;
    int64_t f30;
};

uint64_t fun_d7a3(struct s35* rdi, int64_t rsi, uint64_t* rdx) {
    int64_t rcx4;
    struct s35* v5;
    int64_t v6;
    int64_t rsi7;
    uint64_t* v8;
    void** rax9;
    void** v10;
    int32_t v11;
    uint32_t v12;
    int64_t rdi13;
    int32_t v14;
    int64_t rcx15;
    uint32_t v16;
    int64_t rcx17;
    uint32_t edi18;
    int64_t r15_19;
    int64_t rdx20;
    int64_t rdx21;
    int64_t rdx22;
    uint64_t r12_23;
    int64_t r9_24;
    int64_t rsi25;
    uint64_t rax26;
    int64_t v27;
    int64_t r8_28;
    uint64_t v29;
    int64_t rax30;
    int32_t v31;
    int64_t v32;
    int64_t rcx33;
    uint64_t rax34;
    void* rsp35;
    uint64_t v36;
    uint64_t rbp37;
    struct s13* r13_38;
    uint64_t* r14_39;
    uint64_t v40;
    uint64_t v41;
    int32_t v42;
    uint32_t v43;
    struct s13* rax44;
    int64_t rbx45;
    int32_t v46;
    int64_t v47;
    int64_t rax48;
    int32_t v49;
    int64_t v50;
    int64_t rax51;
    int32_t v52;
    int64_t v53;
    int64_t rax54;
    int32_t v55;
    int64_t v56;
    int32_t v57;
    uint64_t rax58;
    uint64_t r10_59;
    int32_t v60;
    unsigned char dl61;
    uint32_t eax62;
    int32_t v63;
    unsigned char v64;
    uint32_t edi65;
    unsigned char v66;
    int32_t v67;
    int64_t rcx68;
    int32_t v69;
    uint64_t rbp70;
    int64_t r12_71;
    struct s13* v72;
    int64_t r14_73;
    uint64_t* v74;
    void* v75;
    int64_t v76;
    struct s13* v77;
    uint64_t* rax78;
    uint64_t rdx79;
    int64_t rax80;
    int64_t v81;
    void* rax82;
    uint64_t rax83;
    uint64_t v84;
    int32_t v85;
    struct s13* rax86;
    int32_t v87;
    int64_t rax88;
    int32_t v89;
    int64_t v90;
    int64_t rax91;
    int32_t v92;
    int64_t v93;
    int64_t rax94;
    int32_t v95;
    int64_t v96;
    int64_t rax97;
    int32_t v98;
    int64_t v99;
    int32_t v100;
    int64_t rdx101;
    int64_t rax102;
    void*** rax103;
    int32_t ebx104;
    int32_t r15d105;
    int32_t r13d106;
    int64_t rax107;
    void*** rax108;
    int32_t v109;

    __asm__("cli ");
    rcx4 = rdi->f10;
    v5 = rdi;
    v6 = rsi;
    rsi7 = rdi->fc;
    v8 = rdx;
    rax9 = g28;
    v10 = rax9;
    v11 = rdi->f0;
    v12 = rdi->f4;
    rdi13 = rcx4;
    v14 = rdi->f8;
    rcx15 = rcx4 * 0x2aaaaaab >> 33;
    v16 = rdi->f20;
    *reinterpret_cast<int32_t*>(&rcx17) = *reinterpret_cast<int32_t*>(&rcx15) - (*reinterpret_cast<int32_t*>(&rdi13) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx17) + 4) = 0;
    edi18 = *reinterpret_cast<int32_t*>(&rdi13) - (static_cast<uint32_t>(rcx17 + rcx17 * 2) << 2);
    r15_19 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rcx17) - (edi18 >> 31)) + static_cast<int64_t>(rdi->f14);
    *reinterpret_cast<uint32_t*>(&rdx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&r15_19) & 3) && (*reinterpret_cast<uint32_t*>(&rdx20) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0, reinterpret_cast<uint64_t>(0x8f5c28f5c28f5c29 * r15_19 + 0x51eb851eb851eb8) <= 0x28f5c28f5c28f5c)) {
        rdx21 = (__intrinsic() + r15_19 >> 6) - (r15_19 >> 63);
        *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx21) & 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx20) = reinterpret_cast<uint1_t>(rdx22 == 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    }
    *reinterpret_cast<int32_t*>(&r12_23) = 59;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_23) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r9_24) = 70;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
    rsi25 = rsi7 + reinterpret_cast<int32_t>(*reinterpret_cast<uint16_t*>(0x14640 + ((reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(edi18) >> 31) & 12) + edi18 + (rdx20 + (rdx20 + rdx20 * 2) * 4)) * 2) - 1);
    rax26 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v11));
    v27 = rsi25;
    if (*reinterpret_cast<int32_t*>(&rax26) <= 59) {
        r12_23 = rax26;
    }
    if (*reinterpret_cast<int32_t*>(&r12_23) < 0) {
        r12_23 = 0;
    }
    *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
    v29 = *v8;
    *reinterpret_cast<int32_t*>(&rax30) = -*reinterpret_cast<int32_t*>(&v29);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
    v31 = *reinterpret_cast<int32_t*>(&rax30);
    v32 = rax30;
    *reinterpret_cast<uint32_t*>(&rcx33) = v12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
    rax34 = ydhms_diff(r15_19, rsi25, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), 70, 0, 0, 0, *reinterpret_cast<int32_t*>(&v32));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
    v36 = rax34;
    rbp37 = rax34;
    r13_38 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(rsp35) + 0xa0);
    r14_39 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x88);
    v40 = rax34;
    v41 = rax34;
    v42 = 6;
    v43 = 0;
    while (rax44 = ranged_convert(v6, r14_39, r13_38, rcx33, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax44) {
        *reinterpret_cast<int32_t*>(&rbx45) = v46;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx45) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
        v47 = rbx45;
        *reinterpret_cast<int32_t*>(&rax48) = v49;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
        v50 = rax48;
        *reinterpret_cast<int32_t*>(&rax51) = v52;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        v53 = rax51;
        *reinterpret_cast<int32_t*>(&rax54) = v55;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0;
        v56 = rax54;
        *reinterpret_cast<int32_t*>(&r9_24) = v57;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx33) = v12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
        rax58 = ydhms_diff(r15_19, v27, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v56), *reinterpret_cast<int32_t*>(&v53), *reinterpret_cast<int32_t*>(&v50), *reinterpret_cast<int32_t*>(&v47));
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        r10_59 = v40;
        if (!rax58) 
            goto addr_db40_10;
        if (rbp37 == r10_59 || v41 != r10_59) {
            addr_d9a0_12:
            --v42;
            if (!v42) 
                goto addr_dab0_13;
        } else {
            if (v60 < 0) 
                goto addr_da00_15;
            *reinterpret_cast<uint32_t*>(&rcx33) = v16;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
            dl61 = reinterpret_cast<uint1_t>(!!v60);
            if (*reinterpret_cast<int32_t*>(&rcx33) < reinterpret_cast<int32_t>(0)) 
                goto addr_db28_17; else 
                goto addr_d9f2_18;
        }
        v41 = rbp37;
        rbp37 = r10_59;
        v40 = rax58 + r10_59;
        eax62 = 0;
        *reinterpret_cast<unsigned char*>(&eax62) = reinterpret_cast<uint1_t>(!!v63);
        v43 = eax62;
        continue;
        addr_db28_17:
        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(dl61)) < reinterpret_cast<int32_t>(v43)) 
            goto addr_d9a0_12; else 
            goto addr_db35_20;
        addr_d9f2_18:
        *reinterpret_cast<unsigned char*>(&rcx33) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<uint32_t*>(&rcx33));
        if (*reinterpret_cast<unsigned char*>(&rcx33) == dl61) 
            goto addr_d9a0_12; else 
            goto addr_d9f9_21;
    }
    goto addr_dabb_22;
    addr_db40_10:
    v64 = reinterpret_cast<uint1_t>(v16 == 0);
    edi65 = v64;
    v66 = reinterpret_cast<uint1_t>(v67 == 0);
    *reinterpret_cast<uint32_t*>(&rcx68) = v66;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx68) != *reinterpret_cast<signed char*>(&edi65) && !__intrinsic()) {
        v69 = *reinterpret_cast<int32_t*>(&r12_23);
        rbp70 = r10_59;
        r12_71 = v6;
        v72 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(rsp35) + 0xe0);
        *reinterpret_cast<int32_t*>(&r14_73) = 0x92c70;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
        v74 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x90);
        v75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) + 0x98);
        v76 = r15_19;
        v77 = r13_38;
        goto addr_dbd4_24;
    }
    addr_da00_15:
    rax78 = v8;
    *rax78 = r10_59 - (v31 + v36);
    if (v11 == *reinterpret_cast<int32_t*>(&rbx45)) 
        goto addr_da6f_25;
    *reinterpret_cast<unsigned char*>(&rax78) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx45) == 60);
    *reinterpret_cast<int32_t*>(&rdx79) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx79) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rdx79) = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(v11 < 0) | reinterpret_cast<uint1_t>(v11 == 0));
    if (!__intrinsic()) {
        rax80 = reinterpret_cast<int64_t>(v6(reinterpret_cast<int64_t>(rsp35) + 0xe0, r13_38));
        r10_59 = v11 + ((rdx79 & reinterpret_cast<uint64_t>(rax78)) - r12_23) + r10_59;
        if (!rax80) {
            addr_dabb_22:
            r10_59 = 0xffffffffffffffff;
        } else {
            addr_da6f_25:
            __asm__("movdqa xmm0, [rsp+0xa0]");
            __asm__("movdqa xmm1, [rsp+0xb0]");
            __asm__("movdqa xmm2, [rsp+0xc0]");
            __asm__("movups [rcx], xmm0");
            v5->f30 = v81;
            __asm__("movups [rcx+0x10], xmm1");
            __asm__("movups [rcx+0x20], xmm2");
        }
        rax82 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
        if (rax82) {
            fun_35e0();
        } else {
            return r10_59;
        }
    }
    addr_db35_20:
    goto addr_da00_15;
    addr_d9f9_21:
    goto addr_da00_15;
    addr_dd32_31:
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    r13_38 = v77;
    r10_59 = rax83 + v84;
    *reinterpret_cast<int32_t*>(&rbx45) = v85;
    goto addr_da00_15;
    addr_dccb_32:
    goto addr_dabb_22;
    while (rax86 = ranged_convert(r12_71, v74, v72, rcx68, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax86) {
        if (v64 == static_cast<unsigned char>(reinterpret_cast<uint1_t>(v87 == 0)) || v87 < 0) {
            *reinterpret_cast<int32_t*>(&rax88) = v89;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax88) + 4) = 0;
            v90 = rax88;
            *reinterpret_cast<int32_t*>(&rax91) = v92;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
            v93 = rax91;
            *reinterpret_cast<int32_t*>(&rax94) = v95;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax94) + 4) = 0;
            v96 = rax94;
            *reinterpret_cast<int32_t*>(&rax97) = v98;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
            v99 = rax97;
            *reinterpret_cast<int32_t*>(&r9_24) = v100;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_28) = v69;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx68) = v12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx101) = v14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx101) + 4) = 0;
            rax83 = ydhms_diff(v76, v27, *reinterpret_cast<int32_t*>(&rdx101), *reinterpret_cast<uint32_t*>(&rcx68), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v99), *reinterpret_cast<int32_t*>(&v96), *reinterpret_cast<int32_t*>(&v93), *reinterpret_cast<int32_t*>(&v90));
            rax102 = reinterpret_cast<int64_t>(r12_71(v75, v77, rdx101, rcx68, r8_28, r9_24));
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
            if (rax102) 
                goto addr_dd32_31;
            rax103 = fun_3510();
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            if (*rax103 != 75) 
                goto addr_dccb_32;
        }
        while (1) {
            do {
                ebx104 = ebx104 + r15d105;
                if (r13d106 == 1) 
                    break;
                r13d106 = 1;
                v84 = ebx104 + rbp70;
            } while (__intrinsic());
            break;
            *reinterpret_cast<int32_t*>(&r14_73) = *reinterpret_cast<int32_t*>(&r14_73) + 0x92c70;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r14_73) == 0xdb04f20) 
                goto addr_dcd0_40;
            addr_dbd4_24:
            r15d105 = static_cast<int32_t>(r14_73 + r14_73);
            r13d106 = 2;
            ebx104 = -*reinterpret_cast<int32_t*>(&r14_73);
            v84 = ebx104 + rbp70;
            if (!__intrinsic()) 
                break;
        }
    }
    goto addr_dabb_22;
    addr_dcd0_40:
    r13_38 = v77;
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    rax107 = reinterpret_cast<int64_t>(v6(v72, r13_38));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    if (!rax107) {
        addr_dab0_13:
        rax108 = fun_3510();
        *rax108 = reinterpret_cast<void**>(75);
        goto addr_dabb_22;
    } else {
        *reinterpret_cast<int32_t*>(&rbx45) = v109;
        r10_59 = rbp70 + static_cast<int64_t>(reinterpret_cast<int32_t>((v64 - v66) * reinterpret_cast<uint32_t>("ort")));
        goto addr_da00_15;
    }
}

void fun_dd53(int64_t rdi, void** rsi, void** rdx) {
    __asm__("cli ");
    fun_3750(rdi, rsi, rdx);
    goto 0xd7a0;
}

void** vasnprintf(void** rdi, void* rsi, uint64_t rdx, int64_t rcx);

void fseterr(int64_t rdi, void* rsi, uint64_t rdx, int64_t rcx);

int64_t fun_dd83(int64_t rdi, uint64_t rsi, int64_t rdx) {
    int64_t rcx4;
    uint64_t rdx5;
    void* rsp6;
    void** rax7;
    void** r13_8;
    void* rsi9;
    void** rax10;
    int64_t rax11;
    uint64_t rax12;
    void* rdx13;
    void*** rax14;

    __asm__("cli ");
    rcx4 = rdx;
    rdx5 = rsi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x808);
    rax7 = g28;
    r13_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    rsi9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 24);
    rax10 = vasnprintf(r13_8, rsi9, rdx5, rcx4);
    if (!rax10) {
        addr_de57_2:
        fseterr(rdi, rsi9, rdx5, rcx4);
        *reinterpret_cast<int32_t*>(&rax11) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    } else {
        rcx4 = rdi;
        rdx5 = 0x7d0;
        *reinterpret_cast<int32_t*>(&rsi9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rax12 = fun_3870(rax10, rax10);
        if (rax12 < 0x7d0) {
            *reinterpret_cast<int32_t*>(&rax11) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (rax10 != r13_8) {
                fun_34d0(rax10, rax10);
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
        } else {
            if (rax10 != r13_8) {
                fun_34d0(rax10, rax10);
            }
            if (0) 
                goto addr_de4c_9; else 
                goto addr_de0a_10;
        }
    }
    addr_de0c_11:
    rdx13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx13) {
        fun_35e0();
    } else {
        return rax11;
    }
    addr_de4c_9:
    rax14 = fun_3510();
    *rax14 = reinterpret_cast<void**>(75);
    goto addr_de57_2;
    addr_de0a_10:
    *reinterpret_cast<int32_t*>(&rax11) = 0x7d0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    goto addr_de0c_11;
}

int64_t fun_3540();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_de73(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void*** rax5;
    void*** rax6;

    __asm__("cli ");
    rax2 = fun_3540();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_dece_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_3510();
            *rax5 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_dece_3;
            rax6 = fun_3510();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*rax6 == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s36 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_3770(struct s36* rdi);

int32_t fun_37b0(struct s36* rdi);

int64_t fun_3660(int64_t rdi, ...);

int32_t rpl_fflush(struct s36* rdi);

int64_t fun_3590(struct s36* rdi);

int64_t fun_dee3(struct s36* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void*** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_3770(rdi);
    if (eax2 >= 0) {
        eax3 = fun_37b0(rdi);
        if (!(eax3 && (eax4 = fun_3770(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_3660(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_3510();
            r12d9 = *rax8;
            rax10 = fun_3590(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_3590;
}

void rpl_fseeko(struct s36* rdi);

void fun_df73(struct s36* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_37b0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_dfc3(struct s36* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_3770(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_3660(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_e043(uint32_t* rdi) {
    __asm__("cli ");
    *rdi = *rdi | 32;
    return;
}

signed char* fun_37a0(int64_t rdi);

signed char* fun_e053() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_37a0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_3600(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_e093(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_3600(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_35e0();
    } else {
        return r12_7;
    }
}

void** fun_fc93(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9) {
    int64_t v7;
    int64_t v8;
    void** rax9;

    __asm__("cli ");
    rax9 = __strftime_internal_isra_0(rdi, rsi, rdx, rcx, 0, 0, 0xff, r8, r9, v7, v8);
    return rax9;
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s37 {
    signed char[7] pad7;
    void** f7;
};

struct s38 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s39 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_fcb3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    void** rax12;
    void** v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    void* rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    void*** rax30;
    void** r15_31;
    void*** v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    void*** rax41;
    void** rax42;
    struct s37* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    void*** rax55;
    struct s38* r14_56;
    struct s38* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s39* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    void*** rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    void*** rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    void*** rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x10bea;
                fun_35e0();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_10bea_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_10bf4_6;
                addr_10ade_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x10b03, rax21 = fun_37c0(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x10b29;
                    fun_34d0(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x10b4f;
                    fun_34d0(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x10b75;
                    fun_34d0(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_10bf4_6:
            addr_107d8_16:
            v28 = r10_16;
            addr_107df_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0x107e4;
            rax30 = fun_3510();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_107f2_18:
            *v32 = reinterpret_cast<void**>(12);
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0x10391;
                fun_34d0(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0x103a9;
                fun_34d0(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_ffe8_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0x10000;
                fun_34d0(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0x10018;
            fun_34d0(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_3510();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *rax41 = reinterpret_cast<void**>(22);
        goto addr_ffe8_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_ffdd_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_ffdd_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>("oneTable")) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>("\n"));
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_3780(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_ffdd_33:
            rax55 = fun_3510();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *rax55 = reinterpret_cast<void**>(12);
            goto addr_ffe8_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_fddc_46;
    while (1) {
        addr_10734_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_fe9f_50;
            if (r14_56->f50 != -1) 
                goto 0x391d;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_1070f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_107d8_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_107d8_16;
                if (r10_16 == v10) 
                    goto addr_10a24_64; else 
                    goto addr_106e3_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s38*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_10734_47;
            addr_fddc_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_10890_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_10890_73;
                if (r15_31 == v10) 
                    goto addr_10820_79; else 
                    goto addr_fe37_80;
            }
            addr_fe5c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0xfe72;
            fun_3740(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_10820_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0x10828;
            rax67 = fun_3780(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_10890_73;
            if (!r9_58) 
                goto addr_fe5c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0x10867;
            rax69 = fun_3740(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_fe5c_81;
            addr_fe37_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0xfe42;
            rax71 = fun_37c0(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_10890_73; else 
                goto addr_fe5c_81;
            addr_10a24_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x10a3a;
            rax73 = fun_3780(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_10bf9_84;
            if (r12_9) 
                goto addr_10a5a_86;
            r10_16 = rax73;
            goto addr_1070f_55;
            addr_10a5a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x10a6f;
            rax75 = fun_3740(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_1070f_55;
            addr_106e3_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0x106fc;
            rax77 = fun_37c0(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_107df_17;
            r10_16 = rax77;
            goto addr_1070f_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_10bea_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_10ade_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_107d8_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_10b9d_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_10b9d_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_107d8_16; else 
                goto addr_10ba7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_10ab3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x10bbe;
        rax80 = fun_3780(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_10ade_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x10bdd;
                rax82 = fun_3740(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_10ade_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x10ad2;
        rax84 = fun_37c0(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_107df_17; else 
            goto addr_10ade_7;
    }
    addr_10ba7_96:
    rbx19 = r13_8;
    goto addr_10ab3_98;
    addr_fe9f_50:
    if (r14_56->f50 == -1) 
        goto 0x391d;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x3922;
        goto *reinterpret_cast<int32_t*>(0x148f0 + r13_87 * 4) + 0x148f0;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0xff6f;
        fun_3740(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0xffa9;
        fun_3740(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0x14880 + rax95 * 4) + 0x14880;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x391d;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s39*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x391d;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_100e2_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_107d8_16;
    }
    addr_100e2_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_107d8_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_10103_142; else 
                goto addr_10992_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_10992_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_107d8_16; else 
                    goto addr_1099c_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_10103_142;
            }
        }
    }
    addr_10135_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0x1013f;
    rax102 = fun_3510();
    *rax102 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x3922;
    goto *reinterpret_cast<int32_t*>(0x148a8 + rax103 * 4) + 0x148a8;
    addr_10103_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x109f8;
        rax105 = fun_3780(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_10bf9_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x10bfe;
            rax107 = fun_3510();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_107f2_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x10a1f;
                fun_3740(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_10135_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0x10122;
        rax110 = fun_37c0(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_107d8_16; else 
            goto addr_10135_147;
    }
    addr_1099c_145:
    rbx19 = tmp64_100;
    goto addr_10103_142;
    addr_10890_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0x10895;
    rax112 = fun_3510();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_107f2_18;
}

int32_t setlocale_null_r();

int64_t fun_10c33() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_35e0();
    } else {
        return rax3;
    }
}

int64_t fun_10cb3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_37d0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_35d0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_3740(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_3740(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_10d63() {
    __asm__("cli ");
    goto fun_37d0;
}

struct s40 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_10d73(int64_t rdi, struct s40* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_10da9_5;
    return 0xffffffff;
    addr_10da9_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x14920 + rdx3 * 4) + 0x14920;
}

struct s41 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s42 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_10fa3(void** rdi, struct s41* rsi, struct s42* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s42* r15_7;
    struct s41* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    void** rax23;
    uint64_t rdi24;
    int32_t edx25;
    void** rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    void** rbx65;
    void* rsi66;
    int32_t eax67;
    void** rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rax88;
    void** rax89;
    void*** rax90;
    void*** rax91;
    void** rdi92;
    void*** rax93;
    void** rbx94;
    void* rdi95;
    int32_t eax96;
    void** rcx97;
    void* rax98;
    void* rdx99;
    void* rdx100;
    void* tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_11058_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_11045_7:
    return rax15;
    addr_11058_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<uint32_t*>(r12_16 + 16) = 0;
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 80) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_110c9_11;
    } else {
        addr_110c9_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<uint32_t*>(r12_16 + 16) = *reinterpret_cast<uint32_t*>(r12_16 + 16) | 1;
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_110e0_14;
        } else {
            goto addr_110e0_14;
        }
    }
    rax23 = rax5 + 2;
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = rax23 + 0xffffffffffffffff;
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax23)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_115a8_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            ++rax23;
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        ++rax23;
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_115a8_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx26 + 2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = rcx26 + 2;
    goto addr_110c9_11;
    addr_110e0_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x1499c + rax33 * 4) + 0x1499c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_11227_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_11929_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_1192e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_11924_42;
            }
        } else {
            addr_1110d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_11328_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_11117_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 2));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_11b14_56; else 
                        goto addr_11365_57;
                }
            } else {
                addr_11117_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_11138_60;
                } else {
                    goto addr_11138_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_116ab_65;
    addr_11227_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_115a8_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_1124c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_112ca_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_11bcb_75; else 
            goto addr_11273_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_115ac_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_11117_52;
        goto addr_11328_44;
    }
    addr_1192e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_1110d_43;
    addr_11bcb_75:
    if (v11 != rdx53) {
        fun_34d0(rdx53);
        r10_4 = r10_4;
        goto addr_119da_84;
    } else {
        goto addr_119da_84;
    }
    addr_11273_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_3780(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_37c0(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_11bcb_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_3740(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_112ca_68;
    addr_116ab_65:
    rbx65 = rbx14 + 2;
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = rbx65 + 0xffffffffffffffff;
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx65)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_115a8_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            ++rbx65;
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        ++rbx65;
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_115a8_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = rdx68 + 2;
    goto addr_1124c_67;
    label_41:
    addr_11924_42:
    goto addr_11929_36;
    addr_11b14_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_11b3a_104;
    addr_11365_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_115a8_22:
            r8_54 = r15_7->f8;
            goto addr_115ac_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_11374_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_11a22_111;
    } else {
        addr_11381_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_113b7_116;
        }
    }
    rdx53 = r8_54;
    goto addr_11bcb_75;
    addr_11a22_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_3780(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_119da_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_34d0(rdi86);
            }
        } else {
            addr_11c60_120:
            rdx87 = r15_7->f0;
            rax88 = fun_3740(rdi84, r8_85, reinterpret_cast<unsigned char>(rdx87) << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_11a7f_121;
        }
    } else {
        rax89 = fun_37c0(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_11bcb_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_11c60_120;
            }
        }
    }
    rax90 = fun_3510();
    *rax90 = reinterpret_cast<void**>(12);
    return 0xffffffff;
    addr_11a7f_121:
    r15_7->f8 = r8_54;
    goto addr_11381_112;
    addr_113b7_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_115ac_80:
            if (v11 != r8_54) {
                fun_34d0(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_11117_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_11117_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_34d0(rdi92, rdi92);
    }
    rax93 = fun_3510();
    *rax93 = reinterpret_cast<void**>(22);
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_11045_7;
    addr_11b3a_104:
    rbx94 = rbx14 + 3;
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = rbx94 + 0xffffffffffffffff;
        rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (reinterpret_cast<uint64_t>(rdi95) > 0x1999999999999999) {
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi95) + reinterpret_cast<uint64_t>(rdi95) * 4);
            rdx99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx94)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx99) + reinterpret_cast<uint64_t>(rax98)), rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_101) < reinterpret_cast<uint64_t>(rdx99)) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_115a8_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            ++rbx94;
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        ++rbx94;
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_101) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_115a8_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = rcx97 + 2;
    goto addr_11374_107;
    addr_11138_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x14b00 + rax105 * 4) + 0x14b00;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x14a44 + rax106 * 4) + 0x14a44;
    }
}

void fun_11d33() {
    __asm__("cli ");
}

void fun_11d47() {
    __asm__("cli ");
    return;
}

int64_t argmatch_die = 0x47c0;

void** optarg = reinterpret_cast<void**>(0);

int64_t __xargmatch_internal(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_3a30() {
    int64_t r9_1;
    void** rsi2;
    int64_t r15_3;
    int64_t rax4;
    uint32_t* r15_5;

    r9_1 = argmatch_die;
    rsi2 = optarg;
    rax4 = __xargmatch_internal("--time", rsi2, 0x180e0, r15_3, 4, r9_1);
    change_times = change_times | r15_5[rax4];
    goto 0x39e8;
}

void fun_3ac0() {
    void** rax1;

    rax1 = optarg;
    use_ref = 1;
    ref_file = rax1;
    goto 0x39e8;
}

void fun_5f05() {
}

void fun_65a8() {
    goto 0x5f40;
}

void fun_66fb() {
    goto 0x5f40;
}

void fun_6901() {
    goto 0x5f40;
}

void fun_698f() {
    goto 0x5f15;
}

void fun_6b11() {
    goto 0x66ff;
}

struct s43 {
    signed char[192] pad192;
    int64_t fc0;
};

struct s44 {
    signed char[192] pad192;
    int64_t fc0;
};

struct s45 {
    signed char[225] pad225;
    signed char fe1;
};

void fun_6c6b() {
    struct s43* r12_1;
    struct s44* r12_2;
    void** rax3;
    struct s45* r12_4;
    void** r12_5;
    void** rcx6;
    void** r8_7;
    signed char r9b8;

    r12_1->fc0 = r12_2->fc0 + 1;
    rax3 = fun_35b0();
    if (!r12_4->fe1) 
        goto 0x5f40;
    debug_print_current_time_part_0(rax3, r12_5, 5, rcx6, r8_7, static_cast<int64_t>(r9b8), __return_address());
    goto 0x5f40;
}

void fun_6e1f() {
    goto 0x6949;
}

struct s46 {
    signed char[24] pad24;
    int32_t f18;
};

struct s47 {
    signed char[152] pad152;
    int32_t f98;
};

struct s48 {
    signed char[48] pad48;
    int32_t f30;
};

struct s49 {
    signed char[152] pad152;
    int32_t f98;
};

struct s50 {
    signed char[144] pad144;
    int64_t f90;
};

struct s51 {
    signed char[144] pad144;
    int64_t f90;
};

struct s52 {
    signed char[40] pad40;
    int64_t f28;
};

struct s53 {
    signed char[136] pad136;
    int64_t f88;
};

struct s54 {
    signed char[136] pad136;
    int64_t f88;
};

struct s55 {
    signed char[32] pad32;
    int64_t f20;
};

struct s56 {
    signed char[128] pad128;
    int64_t f80;
};

struct s57 {
    signed char[128] pad128;
    int64_t f80;
};

struct s58 {
    signed char[24] pad24;
    int64_t f18;
};

struct s59 {
    signed char[120] pad120;
    int64_t f78;
};

struct s60 {
    signed char[120] pad120;
    int64_t f78;
};

struct s61 {
    signed char[16] pad16;
    int64_t f10;
};

struct s62 {
    signed char[112] pad112;
    int64_t f70;
};

struct s63 {
    signed char[112] pad112;
    int64_t f70;
};

struct s64 {
    signed char[8] pad8;
    int64_t f8;
};

struct s65 {
    signed char[104] pad104;
    int64_t f68;
};

struct s66 {
    signed char[104] pad104;
    int64_t f68;
};

struct s67 {
    signed char[161] pad161;
    signed char fa1;
};

struct s68 {
    signed char[225] pad225;
    signed char fe1;
};

void fun_6f70() {
    struct s46* r12_1;
    void** r8_2;
    struct s47* r12_3;
    struct s48* rbx4;
    struct s49* r12_5;
    uint32_t edi6;
    struct s50* r12_7;
    struct s51* r12_8;
    struct s52* rbx9;
    struct s53* r12_10;
    struct s54* r12_11;
    struct s55* rbx12;
    uint32_t esi13;
    struct s56* r12_14;
    struct s57* r12_15;
    struct s58* rbx16;
    uint32_t r11d17;
    struct s59* r12_18;
    struct s60* r12_19;
    struct s61* rbx20;
    struct s62* r12_21;
    struct s63* r12_22;
    struct s64* rbx23;
    void** rcx24;
    struct s65* r12_25;
    struct s66* r12_26;
    int64_t* rbx27;
    uint32_t eax28;
    struct s67* r12_29;
    void** rax30;
    struct s68* r12_31;
    struct s3* r12_32;
    signed char r9b33;

    r12_1->f18 = 0xffff9d90;
    *reinterpret_cast<uint32_t*>(&r8_2) = 0;
    *reinterpret_cast<int32_t*>(&r8_2 + 4) = 0;
    r12_3->f98 = rbx4->f30 + r12_5->f98;
    *reinterpret_cast<unsigned char*>(&r8_2) = __intrinsic();
    edi6 = 0;
    r12_7->f90 = r12_8->f90 + rbx9->f28;
    *reinterpret_cast<unsigned char*>(&edi6) = __intrinsic();
    r12_10->f88 = r12_11->f88 + rbx12->f20;
    esi13 = 0;
    *reinterpret_cast<unsigned char*>(&esi13) = __intrinsic();
    r12_14->f80 = r12_15->f80 + rbx16->f18;
    r11d17 = 0;
    *reinterpret_cast<unsigned char*>(&r11d17) = __intrinsic();
    r12_18->f78 = r12_19->f78 + rbx20->f10;
    r12_21->f70 = r12_22->f70 + rbx23->f8;
    *reinterpret_cast<int32_t*>(&rcx24) = 0;
    *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx24) = __intrinsic();
    r12_25->f68 = r12_26->f68 + *rbx27;
    eax28 = static_cast<uint32_t>(static_cast<unsigned char>(__intrinsic())) | *reinterpret_cast<uint32_t*>(&r8_2) | edi6 | esi13 | r11d17;
    if (*reinterpret_cast<unsigned char*>(&eax28) | static_cast<unsigned char>(__intrinsic())) 
        goto 0x61b0;
    if (rcx24) 
        goto 0x61b0;
    r12_29->fa1 = 1;
    rax30 = fun_35b0();
    if (!r12_31->fe1) 
        goto 0x5f40;
    debug_print_relative_time_part_0(rax30, r12_32, 5, rcx24, r8_2, static_cast<int64_t>(r9b33), __return_address());
    goto 0x5f40;
}

struct s69 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_7065() {
    int64_t rax1;
    int64_t rbx2;
    struct s69* r12_3;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    r12_3->f18 = *reinterpret_cast<int32_t*>(&rax1);
    goto 0x6f88;
}

struct s70 {
    signed char[48] pad48;
    int64_t f30;
};

struct s71 {
    signed char[56] pad56;
    int64_t f38;
};

struct s72 {
    signed char[8] pad8;
    int64_t f8;
};

struct s73 {
    signed char[64] pad64;
    int64_t f40;
};

void fun_7100() {
    struct s70* r12_1;
    int64_t rbx2;
    int64_t rax3;
    int64_t rbx4;
    struct s71* r12_5;
    int64_t rax6;
    struct s72* rbx7;
    struct s73* r12_8;

    __asm__("movdqu xmm7, [rbx-0x70]");
    r12_1->f30 = *reinterpret_cast<int64_t*>(rbx2 - 96);
    rax3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    __asm__("movups [r12+0x20], xmm7");
    r12_5->f38 = -rax3;
    if (rax3 == 0x8000000000000000) 
        goto 0x61b0;
    rax6 = rbx7->f8;
    r12_8->f40 = -rax6;
    if (rax6 != 0x8000000000000000) 
        goto 0x5f40;
    goto 0x61b0;
}

struct s74 {
    signed char[64] pad64;
    int64_t f40;
};

struct s75 {
    signed char[56] pad56;
    int64_t f38;
};

struct s76 {
    signed char[8] pad8;
    int64_t f8;
};

struct s77 {
    signed char[40] pad40;
    int64_t f28;
};

struct s78 {
    signed char[48] pad48;
    int64_t f30;
};

struct s79 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_7158() {
    struct s74* r12_1;
    int64_t rbx2;
    struct s75* r12_3;
    int64_t rbx4;
    int64_t rax5;
    struct s76* rbx6;
    struct s77* r12_7;
    struct s78* r12_8;
    struct s79* rbx9;

    r12_1->f40 = *reinterpret_cast<int64_t*>(rbx2 - 0x68);
    r12_3->f38 = *reinterpret_cast<int64_t*>(rbx4 - 56);
    rax5 = rbx6->f8;
    r12_7->f28 = -rax5;
    if (rax5 == 0x8000000000000000) 
        goto 0x61b0;
    r12_8->f30 = rbx9->f10;
    goto 0x5f40;
}

struct s80 {
    signed char[64] pad64;
    int64_t f40;
};

struct s81 {
    signed char[56] pad56;
    int64_t f38;
};

struct s82 {
    signed char[48] pad48;
    int64_t f30;
};

struct s83 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_719a() {
    struct s80* r12_1;
    int64_t rbx2;
    struct s81* r12_3;
    int64_t rbx4;
    struct s82* r12_5;
    struct s83* rbx6;

    __asm__("movdqu xmm6, [rbx]");
    r12_1->f40 = *reinterpret_cast<int64_t*>(rbx2 - 0x68);
    __asm__("movups [r12+0x20], xmm6");
    r12_3->f38 = *reinterpret_cast<int64_t*>(rbx4 - 56);
    r12_5->f30 = rbx6->f10;
    goto 0x5f40;
}

void fun_72c0() {
    goto 0x5f40;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_38a0(int64_t rdi, void** rsi);

uint32_t fun_3890(void** rdi, void** rsi);

void** fun_38c0(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_a0d5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_35b0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_35b0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_35d0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_a3d3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_a3d3_22; else 
                            goto addr_a7cd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_a88d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_abe0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_a3d0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_a3d0_30; else 
                                goto addr_abf9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_35d0(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_abe0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_36d0(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_abe0_28; else 
                            goto addr_a27c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_ad40_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_abc0_40:
                        if (r11_27 == 1) {
                            addr_a74d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_ad08_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_a387_44;
                            }
                        } else {
                            goto addr_abd0_46;
                        }
                    } else {
                        addr_ad4f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_a74d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_a3d3_22:
                                if (v47 != 1) {
                                    addr_a929_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_35d0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_a974_54;
                                    }
                                } else {
                                    goto addr_a3e0_56;
                                }
                            } else {
                                addr_a385_57:
                                ebp36 = 0;
                                goto addr_a387_44;
                            }
                        } else {
                            addr_abb4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_ad4f_47; else 
                                goto addr_abbe_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_a74d_41;
                        if (v47 == 1) 
                            goto addr_a3e0_56; else 
                            goto addr_a929_52;
                    }
                }
                addr_a441_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_a2d8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_a2fd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_a600_65;
                    } else {
                        addr_a469_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_acb8_67;
                    }
                } else {
                    goto addr_a460_69;
                }
                addr_a311_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_a35c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_acb8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_a35c_81;
                }
                addr_a460_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_a2fd_64; else 
                    goto addr_a469_66;
                addr_a387_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_a43f_91;
                if (v22) 
                    goto addr_a39f_93;
                addr_a43f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_a441_62;
                addr_a974_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_b0fb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_b16b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_af6f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_38a0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_3890(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_aa6e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_a42c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_aa78_112;
                    }
                } else {
                    addr_aa78_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_ab49_114;
                }
                addr_a438_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_a43f_91;
                while (1) {
                    addr_ab49_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_b057_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_aab6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_b065_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_ab37_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_ab37_128;
                        }
                    }
                    addr_aae5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_ab37_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_aab6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_aae5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_a35c_81;
                addr_b065_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_acb8_67;
                addr_b0fb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_aa6e_109;
                addr_b16b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_aa6e_109;
                addr_a3e0_56:
                rax93 = fun_38c0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_a42c_110;
                addr_abbe_59:
                goto addr_abc0_40;
                addr_a88d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_a3d3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_a438_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_a385_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_a3d3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_a8d2_160;
                if (!v22) 
                    goto addr_aca7_162; else 
                    goto addr_aeb3_163;
                addr_a8d2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_aca7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_acb8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_a77b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_a5e3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_a311_70; else 
                    goto addr_a5f7_169;
                addr_a77b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_a2d8_63;
                goto addr_a460_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_abb4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_acef_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_a3d0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_a2c8_178; else 
                        goto addr_ac72_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_abb4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_a3d3_22;
                }
                addr_acef_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_a3d0_30:
                    r8d42 = 0;
                    goto addr_a3d3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_a441_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_ad08_42;
                    }
                }
                addr_a2c8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_a2d8_63;
                addr_ac72_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_abd0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_a441_62;
                } else {
                    addr_ac82_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_a3d3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_b432_188;
                if (v28) 
                    goto addr_aca7_162;
                addr_b432_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_a5e3_168;
                addr_a27c_37:
                if (v22) 
                    goto addr_b273_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_a293_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_ad40_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_adcb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_a3d3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_a2c8_178; else 
                        goto addr_ada7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_abb4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_a3d3_22;
                }
                addr_adcb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_a3d3_22;
                }
                addr_ada7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_abd0_46;
                goto addr_ac82_186;
                addr_a293_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_a3d3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_a3d3_22; else 
                    goto addr_a2a4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_b37e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_b204_210;
            if (1) 
                goto addr_b202_212;
            if (!v29) 
                goto addr_ae3e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_35c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_b371_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_a600_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_a3bb_219; else 
            goto addr_a61a_220;
        addr_a39f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_a3b3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_a61a_220; else 
            goto addr_a3bb_219;
        addr_af6f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_a61a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_35c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_af8d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_35c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_b400_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_ae66_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_b057_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_a3b3_221;
        addr_aeb3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_a3b3_221;
        addr_a5f7_169:
        goto addr_a600_65;
        addr_b37e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_a61a_220;
        goto addr_af8d_222;
        addr_b204_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_b25e_236;
        fun_35e0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_b400_225;
        addr_b202_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_b204_210;
        addr_ae3e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_b204_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_ae66_226;
        }
        addr_b371_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_a7cd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1408c + rax113 * 4) + 0x1408c;
    addr_abf9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1418c + rax114 * 4) + 0x1418c;
    addr_b273_190:
    addr_a3bb_219:
    goto 0xa0a0;
    addr_a2a4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x13f8c + rax115 * 4) + 0x13f8c;
    addr_b25e_236:
    goto v116;
}

void fun_a2c0() {
}

void fun_a478() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0xa172;
}

void fun_a4d1() {
    goto 0xa172;
}

void fun_a5be() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0xa441;
    }
    if (v2) 
        goto 0xaeb3;
    if (!r10_3) 
        goto addr_b01e_5;
    if (!v4) 
        goto addr_aeee_7;
    addr_b01e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_aeee_7:
    goto 0xa2f4;
}

void fun_a5dc() {
}

void fun_a687() {
    signed char v1;

    if (v1) {
        goto 0xa60f;
    } else {
        goto 0xa34a;
    }
}

void fun_a6a1() {
    signed char v1;

    if (!v1) 
        goto 0xa69a; else 
        goto "???";
}

void fun_a6c8() {
    goto 0xa5e3;
}

void fun_a748() {
}

void fun_a760() {
}

void fun_a78f() {
    goto 0xa5e3;
}

void fun_a7e1() {
    goto 0xa770;
}

void fun_a810() {
    goto 0xa770;
}

void fun_a843() {
    goto 0xa770;
}

void fun_ac10() {
    goto 0xa2c8;
}

void fun_af0e() {
    signed char v1;

    if (v1) 
        goto 0xaeb3;
    goto 0xa2f4;
}

void fun_afb5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0xa2f4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0xa2d8;
        goto 0xa2f4;
    }
}

void fun_b3d2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0xa640;
    } else {
        goto 0xa172;
    }
}

void fun_c8c8() {
    fun_35b0();
}

void fun_e350(int32_t edi) {
    if (edi != 79) {
        if (edi) 
            goto 0xe7d0;
    }
}

void fun_e819(int32_t edi) {
    void* rsp2;
    int64_t rbp3;
    int32_t edx4;
    int32_t v5;
    int32_t r15d6;
    int32_t r15d7;
    int64_t rax8;
    int64_t rcx9;
    int64_t rcx10;
    int32_t esi11;
    int64_t rax12;
    void* rsi13;
    void* rdi14;
    signed char* rcx15;
    int64_t r8_16;
    signed char* rdi17;
    int32_t r10d18;
    void* v19;
    uint64_t r13_20;
    int64_t r14_21;
    signed char v22;
    void* rdx23;
    int64_t r14_24;
    int64_t* r14_25;
    int64_t v26;
    void* r14_27;
    void* rax28;
    void* r14_29;
    void* rdi30;
    uint64_t rax31;
    void* rax32;
    void* rcx33;
    int32_t* r14_34;
    int32_t v35;
    void* r14_36;
    uint32_t eax37;
    unsigned char v38;
    signed char* r14_39;
    uint32_t eax40;
    void* r14_41;
    uint32_t** rax42;
    void* rdx43;
    void* rdi44;
    uint32_t** rcx45;
    int64_t r8_46;
    uint32_t eax47;
    void* r14_48;
    int1_t cf49;
    void** r14_50;
    void* r14_51;
    uint64_t r13_52;
    int64_t r13_53;
    int32_t edx54;
    uint64_t v55;
    uint64_t rdx56;
    int64_t v57;
    int64_t rax58;
    int64_t rax59;
    int32_t r8d60;
    int32_t edx61;

    rsp2 = __zero_stack_offset();
    if (edi == 69) 
        goto 0xe360;
    *reinterpret_cast<int32_t*>(&rbp3) = 9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    edx4 = v5;
    if (reinterpret_cast<uint1_t>(r15d6 < 0) | reinterpret_cast<uint1_t>(r15d7 == 0)) {
    }
    while (1) {
        rax8 = edx4;
        if (*reinterpret_cast<int32_t*>(&rbp3) <= 9) {
            rcx9 = rax8 * 0x66666667 >> 34;
            *reinterpret_cast<int32_t*>(&rcx10) = *reinterpret_cast<int32_t*>(&rcx9) - (edx4 >> 31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            esi11 = static_cast<int32_t>(rcx10 + rcx10 * 4);
            if (*reinterpret_cast<int32_t*>(&rbp3) == 1) 
                break;
            if (edx4 - (esi11 + esi11)) 
                goto addr_e887_8;
        }
        *reinterpret_cast<int32_t*>(&rbp3) = *reinterpret_cast<int32_t*>(&rbp3) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
        rax12 = rax8 * 0x66666667 >> 34;
        edx4 = *reinterpret_cast<int32_t*>(&rax12) - (edx4 >> 31);
    }
    *reinterpret_cast<int32_t*>(&rsi13) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    goto addr_e895_11;
    addr_e887_8:
    rsi13 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp3)));
    rdi14 = rsi13;
    if (!*reinterpret_cast<int32_t*>(&rbp3)) {
        *reinterpret_cast<int32_t*>(&rsi13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    } else {
        addr_e895_11:
        rcx15 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xb0);
        *reinterpret_cast<int32_t*>(&r8_16) = static_cast<int32_t>(rbp3 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_16) + 4) = 0;
        rdi17 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xaf - r8_16);
        goto addr_e8b2_13;
    }
    addr_e8e1_14:
    if (!r10d18) {
    }
    if (reinterpret_cast<int64_t>(v19) - r13_20 <= reinterpret_cast<uint64_t>(rsi13)) 
        goto 0xe240;
    if (r14_21) {
        if (!v22) {
            if (reinterpret_cast<uint64_t>(rsi13) >= 8) {
                rdx23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_24 + 8) & 0xfffffffffffffff8);
                *r14_25 = v26;
                *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r14_27) + reinterpret_cast<uint64_t>(rsi13) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xa8);
                rax28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r14_29) - reinterpret_cast<uint64_t>(rdx23));
                rdi30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0 - reinterpret_cast<uint64_t>(rax28));
                rax31 = reinterpret_cast<uint64_t>(rax28) + reinterpret_cast<uint64_t>(rsi13) & 0xfffffffffffffff8;
                if (rax31 >= 8) {
                    rax32 = reinterpret_cast<void*>(rax31 & 0xfffffffffffffff8);
                    *reinterpret_cast<int32_t*>(&rcx33) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
                    do {
                        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdx23) + reinterpret_cast<uint64_t>(rcx33)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi30) + reinterpret_cast<uint64_t>(rcx33));
                        rcx33 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx33) + 8);
                    } while (reinterpret_cast<uint64_t>(rcx33) < reinterpret_cast<uint64_t>(rax32));
                }
            } else {
                if (*reinterpret_cast<unsigned char*>(&rsi13) & 4) {
                    *r14_34 = v35;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(r14_36) + reinterpret_cast<uint64_t>(rsi13) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xac);
                } else {
                    if (rsi13 && (eax37 = v38, *r14_39 = *reinterpret_cast<signed char*>(&eax37), !!(*reinterpret_cast<unsigned char*>(&rsi13) & 2))) {
                        eax40 = *reinterpret_cast<uint16_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xae);
                        *reinterpret_cast<int16_t*>(reinterpret_cast<int64_t>(r14_41) + reinterpret_cast<uint64_t>(rsi13) - 2) = *reinterpret_cast<int16_t*>(&eax40);
                    }
                }
            }
        } else {
            if (rsi13) {
                rax42 = fun_34a0();
                rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
                rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi13) + 0xffffffffffffffff);
                rdi44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0);
                rsi13 = rsi13;
                rcx45 = rax42;
                do {
                    *reinterpret_cast<uint32_t*>(&r8_46) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi44) + reinterpret_cast<uint64_t>(rdx43));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_46) + 4) = 0;
                    eax47 = (*rcx45)[r8_46];
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_48) + reinterpret_cast<uint64_t>(rdx43)) = *reinterpret_cast<signed char*>(&eax47);
                    cf49 = reinterpret_cast<uint64_t>(rdx43) < 1;
                    rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx43) - 1);
                } while (!cf49);
            }
        }
        r14_50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_51) + reinterpret_cast<uint64_t>(rsi13));
    }
    r13_52 = r13_53 + reinterpret_cast<uint64_t>(rsi13);
    if (edx54 = 9 - *reinterpret_cast<int32_t*>(&rbp3), edx54 < 0) {
        if (v55 != r13_52) 
            goto 0xe208; else 
            goto "???";
    }
    rdx56 = reinterpret_cast<uint64_t>(static_cast<int64_t>(edx54));
    if (rdx56 >= v57 - r13_52) 
        goto 0xe240;
    if (r14_50) 
        goto addr_e993_36;
    goto 0xe208;
    addr_e993_36:
    if (!rdx56) 
        goto 0xe208;
    if (1) 
        goto addr_fba9_39;
    if (!0) 
        goto addr_e9b7_41;
    addr_fba9_39:
    fun_3690(r14_50, r14_50);
    goto 0xe208;
    addr_e9b7_41:
    fun_3690(r14_50, r14_50);
    goto 0xe208;
    addr_e8b2_13:
    while (--rcx15, rax58 = rax8 * 0x66666667 >> 34, *reinterpret_cast<int32_t*>(&rax59) = *reinterpret_cast<int32_t*>(&rax58) - (edx4 >> 31), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0, r8d60 = static_cast<int32_t>(rax59 + rax59 * 4), edx61 = edx4 - (r8d60 + r8d60) + 48, *rcx15 = *reinterpret_cast<signed char*>(&edx61), edx4 = *reinterpret_cast<int32_t*>(&rax59), rcx15 != rdi17) {
        rax8 = *reinterpret_cast<int32_t*>(&rax59);
    }
    goto addr_e8e1_14;
}

void fun_e9d0(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe7b0;
}

void fun_ea0d(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe9f0;
}

void fun_ea24(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe9f0;
}

void fun_ea3f(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe9f0;
}

void fun_ea5a(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe9f0;
}

void fun_eac8() {
    void** v1;
    void** rax2;
    void** rsi3;
    int32_t* v4;
    void** rcx5;

    __asm__("movdqu xmm4, [rax+0x20]");
    __asm__("movdqu xmm0, [rax]");
    __asm__("movdqu xmm2, [rax+0x10]");
    __asm__("movaps [rsp+0x70], xmm0");
    __asm__("movaps [rsp+0x80], xmm2");
    __asm__("movaps [rsp+0x40], xmm4");
    __asm__("movaps [rsp+0x90], xmm4");
    rax2 = mktime_z(v1, reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x70);
    rsi3 = rax2;
    if (1) {
        *v4 = 75;
        goto 0xe24b;
    } else {
        rcx5 = rsi3;
        do {
            rcx5 = reinterpret_cast<void**>((__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rcx5) >> 63));
            if (reinterpret_cast<signed char>(rsi3) < reinterpret_cast<signed char>(0)) {
            }
        } while (rcx5);
    }
}

void fun_ecb5() {
    goto 0xe360;
}

void fun_ecd3() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    signed char* r14_11;
    int32_t r15d12;
    signed char* r15_13;
    int64_t r14_14;
    int32_t r10d15;
    int32_t r10d16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0xe240;
        if (!r14_6) 
            goto addr_f748_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0xe240;
        if (!r14_10) 
            goto addr_ed48_9; else 
            goto addr_ed0e_10;
    }
    addr_ed40_11:
    *r14_11 = 9;
    addr_ed48_9:
    goto 0xe208;
    addr_f748_4:
    goto addr_ed48_9;
    addr_ed0e_10:
    if (r15d12 > 1) {
        r15_13 = reinterpret_cast<signed char*>(r14_14 + (rdx7 - 1));
        if (r10d15 == 48 || r10d16 == 43) {
            r14_11 = r15_13;
            fun_3690(r14_17, r14_17);
            goto addr_ed40_11;
        } else {
            r14_11 = r15_13;
            fun_3690(r14_18, r14_18);
            goto addr_ed40_11;
        }
    }
}

struct s84 {
    signed char[20] pad20;
    uint32_t f14;
};

void fun_ed70(int32_t edi) {
    int64_t rcx2;
    struct s84* v3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdx6;
    int32_t r10d7;
    int32_t r10d8;
    int32_t v9;

    if (edi == 69) 
        goto 0xe7c1;
    *reinterpret_cast<uint32_t*>(&rcx2) = v3->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx2) + 4) = 0;
    eax4 = static_cast<uint32_t>(rcx2 + 0x76c);
    eax5 = (eax4 - (eax4 + reinterpret_cast<uint1_t>(eax4 < eax4 + reinterpret_cast<uint1_t>(eax4 < 0x76c))) & 0xffffff9d) + *reinterpret_cast<uint32_t*>(&rcx2);
    rdx6 = reinterpret_cast<int32_t>(eax5) * 0x51eb851f >> 37;
    if (r10d7) {
        if (r10d8 != 43) {
            addr_edd6_4:
            goto 0xe7a3;
        } else {
            addr_f962_5:
        }
        if (99 >= reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx6) - (reinterpret_cast<int32_t>(eax5) >> 31) + 19)) 
            goto 0xf86c;
        goto 0xe7a3;
    } else {
        if (v9 == 43) 
            goto addr_f962_5; else 
            goto addr_edd6_4;
    }
}

void fun_ede8(int32_t edi) {
    uint32_t r8d2;
    unsigned char v3;
    int64_t rax4;
    int32_t v5;
    void** v6;
    void** r10d7;
    int64_t v8;
    int64_t v9;
    void** rax10;
    void** r10d11;
    void** r10d12;
    uint32_t r8d13;
    int32_t r15d14;
    void** v15;
    void** rdx16;
    int32_t r15d17;
    void** rax18;
    void** r15_19;
    void* v20;
    uint64_t r13_21;
    int64_t r14_22;
    void** v23;
    void* r14_24;
    int64_t v25;
    void** r14_26;
    void** r14_27;
    void** r14_28;
    int64_t rax29;
    int32_t v30;
    void** v31;
    int64_t v32;

    if (edi) 
        goto 0xe360;
    r8d2 = v3;
    *reinterpret_cast<int32_t*>(&rax4) = v5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    rax10 = __strftime_internal_isra_0(0, 0xffffffffffffffff, "%m/%d/%y", v6, *reinterpret_cast<unsigned char*>(&r8d2), r10d7, -1, v8, rax4, v9, __return_address());
    r10d11 = r10d12;
    r8d13 = r8d2;
    if (r10d11 == 45 || r15d14 < 0) {
        v15 = rax10;
        *reinterpret_cast<int32_t*>(&rdx16) = 0;
        *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    } else {
        rdx16 = reinterpret_cast<void**>(static_cast<int64_t>(r15d17));
        rax18 = rdx16;
        if (reinterpret_cast<unsigned char>(rax10) >= reinterpret_cast<unsigned char>(rdx16)) {
            rax18 = rax10;
        }
        v15 = rax18;
    }
    r15_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v20) - r13_21);
    if (reinterpret_cast<unsigned char>(r15_19) <= reinterpret_cast<unsigned char>(v15)) 
        goto 0xe240;
    if (r14_22) {
        if (reinterpret_cast<unsigned char>(rdx16) > reinterpret_cast<unsigned char>(rax10)) {
            v23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_24) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx16) - reinterpret_cast<unsigned char>(rax10)));
            if (r10d11 == 48 || r10d11 == 43) {
                v25 = 0xf79e;
                fun_3690(r14_26, r14_26);
                r14_27 = v23;
                r8d13 = r8d13;
                r10d11 = r10d11;
            } else {
                v25 = 0xeeda;
                fun_3690(r14_28, r14_28);
                r14_27 = v23;
                r10d11 = r10d11;
                r8d13 = r8d13;
            }
        }
        *reinterpret_cast<int32_t*>(&rax29) = v30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        __strftime_internal_isra_0(r14_27, r15_19, "%m/%d/%y", v31, *reinterpret_cast<unsigned char*>(&r8d13), r10d11, -1, v32, rax29, v25, __return_address());
    }
    goto 0xe208;
}

void fun_ef5b(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe9f0;
}

void fun_ef79(int32_t edi) {
    if (edi == 79) 
        goto 0xe7c1;
}

void fun_f007() {
    goto 0xe9f0;
}

void fun_f071(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe7b0;
}

void fun_f0a5() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    int32_t r15d11;
    signed char* r15_12;
    int64_t r14_13;
    int32_t r10d14;
    int32_t r10d15;
    signed char* r14_16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0xe240;
        if (!r14_6) 
            goto addr_f765_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0xe240;
        if (!r14_10) 
            goto 0xed48;
        if (r15d11 > 1) {
            r15_12 = reinterpret_cast<signed char*>(r14_13 + (rdx7 - 1));
            if (r10d14 == 48 || r10d15 == 43) {
                r14_16 = r15_12;
                fun_3690(r14_17, r14_17);
            } else {
                r14_16 = r15_12;
                fun_3690(r14_18, r14_18);
            }
        }
    }
    *r14_16 = 10;
    goto 0xed48;
    addr_f765_4:
    goto 0xed48;
}

void fun_f13f(int32_t edi) {
    int32_t r10d2;
    int32_t r10d3;
    int32_t v4;

    if (edi == 69) 
        goto 0xe7c1;
    if (edi == 79) 
        goto 0xe360;
    if (r10d2) {
        if (r10d3 == 43) 
            goto "???";
        goto 0xe7a3;
    } else {
        if (v4 == 43) {
            goto 0xf8af;
        }
    }
}

void fun_f1a9() {
    uint32_t edi1;
    unsigned char v2;
    signed char r8b3;
    void** rdi4;
    void** r12_5;
    void** rax6;
    uint32_t r8d7;
    unsigned char r8b8;
    int32_t r10d9;
    int32_t r15d10;
    void** v11;
    void** r15_12;
    int32_t r15d13;
    void** rax14;
    void* v15;
    uint64_t r13_16;
    int64_t r14_17;
    void** r15_18;
    void* r14_19;
    void** r14_20;
    void** r14_21;
    void** r14_22;
    unsigned char* r15_23;
    uint32_t** rax24;
    uint32_t** rsi25;
    int64_t rdx26;
    void* r12_27;
    uint32_t eax28;
    int1_t cf29;
    void** r12_30;
    unsigned char* r15_31;
    uint32_t** rax32;
    uint32_t** rsi33;
    int64_t rdx34;
    void* r12_35;
    uint32_t eax36;
    int1_t cf37;

    edi1 = v2;
    if (r8b3) {
        edi1 = 0;
    }
    rdi4 = r12_5;
    rax6 = fun_35d0(rdi4);
    r8d7 = r8b8;
    if (r10d9 == 45 || r15d10 < 0) {
        v11 = rax6;
        *reinterpret_cast<int32_t*>(&r15_12) = 0;
        *reinterpret_cast<int32_t*>(&r15_12 + 4) = 0;
    } else {
        r15_12 = reinterpret_cast<void**>(static_cast<int64_t>(r15d13));
        rax14 = r15_12;
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(r15_12)) {
            rax14 = rax6;
        }
        v11 = rax14;
    }
    if (reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(v15) - r13_16) <= reinterpret_cast<unsigned char>(v11)) 
        goto 0xe240;
    if (r14_17) {
        if (reinterpret_cast<unsigned char>(rax6) < reinterpret_cast<unsigned char>(r15_12)) {
            r15_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_19) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_12) - reinterpret_cast<unsigned char>(rax6)));
            if (r10d9 == 48 || r10d9 == 43) {
                rdi4 = r14_20;
                r14_21 = r15_18;
                fun_3690(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            } else {
                rdi4 = r14_22;
                r14_21 = r15_18;
                fun_3690(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            }
        }
        if (*reinterpret_cast<unsigned char*>(&r8d7)) {
            r15_23 = reinterpret_cast<unsigned char*>(rax6 + 0xffffffffffffffff);
            if (rax6) {
                rax24 = fun_38b0(rdi4, rdi4);
                rsi25 = rax24;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx26) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_27) + reinterpret_cast<uint64_t>(r15_23));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
                    eax28 = (*rsi25)[rdx26];
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<uint64_t>(r15_23)) = *reinterpret_cast<signed char*>(&eax28);
                    cf29 = reinterpret_cast<uint64_t>(r15_23) < 1;
                    --r15_23;
                } while (!cf29);
            }
        } else {
            if (!*reinterpret_cast<signed char*>(&edi1)) {
                fun_3740(r14_21, r12_30, rax6);
            } else {
                r15_31 = reinterpret_cast<unsigned char*>(rax6 + 0xffffffffffffffff);
                if (rax6) {
                    rax32 = fun_34a0();
                    rsi33 = rax32;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx34) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_35) + reinterpret_cast<uint64_t>(r15_31));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx34) + 4) = 0;
                        eax36 = (*rsi33)[rdx34];
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<uint64_t>(r15_31)) = *reinterpret_cast<signed char*>(&eax36);
                        cf37 = reinterpret_cast<uint64_t>(r15_31) < 1;
                        --r15_31;
                    } while (!cf37);
                }
            }
        }
    }
    goto 0xe208;
}

void fun_f2fd(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe9f0;
}

void fun_f31b(int32_t edi) {
    int32_t r10d2;

    if (edi == 69) 
        goto 0xe360;
    if (!r10d2) {
    }
    goto 0xe9f0;
}

void fun_f331(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xe7b0;
}

void fun_f365(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xf058;
}

struct s85 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s86 {
    signed char[32] pad32;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
};

void fun_f37b() {
    uint32_t eax1;
    struct s85* rbx2;
    uint64_t r11_3;
    int64_t rbx4;
    struct s86* v5;
    int64_t rdx6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t r9_9;

    eax1 = rbx2->f1;
    *reinterpret_cast<int32_t*>(&r11_3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_3) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax1) == 58) {
        while (++r11_3, eax1 = *reinterpret_cast<unsigned char*>(rbx4 + r11_3), *reinterpret_cast<signed char*>(&eax1) == 58) {
        }
    }
    if (*reinterpret_cast<signed char*>(&eax1) != 0x7a) 
        goto 0xe360;
    if (v5->f20 < 0) 
        goto 0xe208;
    rdx6 = v5->f28;
    if (*reinterpret_cast<int32_t*>(&rdx6) >= 0 && !*reinterpret_cast<int32_t*>(&rdx6)) {
    }
    rax7 = *reinterpret_cast<int32_t*>(&rdx6) * 0xffffffff88888889 >> 32;
    eax8 = (*reinterpret_cast<int32_t*>(&rax7) + *reinterpret_cast<int32_t*>(&rdx6) >> 5) - (*reinterpret_cast<int32_t*>(&rdx6) >> 31);
    r9_9 = eax8 * 0xffffffff88888889 >> 32;
    if (r11_3 == 2) {
        addr_fa46_10:
        goto 0xe7b0;
    } else {
        if (r11_3 > 2) {
            if (r11_3 != 3) 
                goto 0xe360;
            if (*reinterpret_cast<int32_t*>(&rdx6) - eax8 * 60) 
                goto addr_fa46_10;
        } else {
            if (!r11_3) {
                goto 0xe7b0;
            }
        }
    }
    if (!(eax8 - ((*reinterpret_cast<int32_t*>(&r9_9) + eax8 >> 5) - (eax8 >> 31)) * 60)) {
        goto 0xe7b0;
    }
    goto 0xe7b0;
}

void fun_f469() {
    goto 0xf39c;
}

void fun_f495() {
    goto 0xedff;
}

struct s87 {
    signed char[1] pad1;
    signed char f1;
};

struct s88 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_101a8() {
    void** rdi1;
    void* r15_2;
    void* r12_3;
    int64_t rbp4;
    void** r8_5;
    int64_t rbp6;
    int64_t rbp7;
    int64_t rsi8;
    void** rsi9;
    int64_t rsi10;
    uint32_t eax11;
    int64_t rdx12;
    int64_t rbp13;
    void** rsi14;
    int64_t rbp15;
    void** r8_16;
    int64_t rbp17;
    int64_t rbp18;
    int64_t rsi19;
    void** rsi20;
    int64_t rsi21;
    int64_t rbp22;
    void** r8_23;
    int64_t rbp24;
    int64_t rbp25;
    int64_t rsi26;
    void** rsi27;
    int64_t rbp28;
    int64_t rbp29;
    void** rcx30;
    uint64_t r15_31;
    int64_t r12_32;
    void** rax33;
    int64_t rbp34;
    int64_t rbp35;
    int64_t rbp36;
    uint64_t r13_37;
    int64_t rbp38;
    int64_t rbx39;
    int64_t rbx40;
    void** rax41;
    void** rcx42;
    void* rbx43;
    void* rbx44;
    void** tmp64_45;
    void* r12_46;
    void** rax47;
    void** rbx48;
    int64_t r15_49;
    int64_t rbp50;
    void** r15_51;
    void** rax52;
    void** rax53;
    int64_t r12_54;
    void** r15_55;
    void** r12_56;
    int64_t rbp57;
    int64_t rbp58;
    uint32_t eax59;
    struct s88* r14_60;
    int32_t eax61;
    int64_t rbp62;

    rdi1 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<uint64_t>(r12_3));
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        r8_5 = *reinterpret_cast<void***>(rbp6 - 0x3e0);
        *reinterpret_cast<int64_t*>(rbp7 - 0x418) = rsi8;
        eax11 = fun_34c0(rdi1, rsi9, 1, 0xffffffffffffffff, r8_5, rdi1, rsi10, 1, 0xffffffffffffffff, r8_5);
        *reinterpret_cast<uint32_t*>(&rdx12) = *reinterpret_cast<uint32_t*>(rbp13 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
        rsi14 = *reinterpret_cast<void***>(rbp15 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx12) < reinterpret_cast<int32_t>(0)) 
            goto addr_10333_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            r8_16 = *reinterpret_cast<void***>(rbp17 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp18 - 0x418) = rsi19;
            eax11 = fun_34c0(rdi1, rsi20, 1, 0xffffffffffffffff, r8_16, rdi1, rsi21, 1, 0xffffffffffffffff, r8_16);
            rsi14 = *reinterpret_cast<void***>(rbp22 - 0x418);
        } else {
            r8_23 = *reinterpret_cast<void***>(rbp24 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp25 - 0x418) = rsi26;
            eax11 = fun_34c0(rdi1, rsi27, 1, 0xffffffffffffffff, r8_23);
            rsi14 = *reinterpret_cast<void***>(rbp28 - 0x418);
        }
        *reinterpret_cast<uint32_t*>(&rdx12) = *reinterpret_cast<uint32_t*>(rbp29 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx12) < reinterpret_cast<int32_t>(0)) 
            goto addr_10333_5;
    }
    rcx30 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx12)));
    if (reinterpret_cast<unsigned char>(rcx30) < reinterpret_cast<unsigned char>(rsi14)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx30) + r15_31 + r12_32)) 
            goto 0x391d;
    }
    if (*reinterpret_cast<int32_t*>(&rdx12) >= reinterpret_cast<int32_t>(eax11)) {
        addr_1023d_16:
        *reinterpret_cast<int32_t*>(&rax33) = static_cast<int32_t>(rdx12 + 1);
        *reinterpret_cast<int32_t*>(&rax33 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax33) < reinterpret_cast<unsigned char>(rsi14)) {
            **reinterpret_cast<int32_t**>(rbp34 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp35 - 0x40c);
            goto 0x10717;
        }
    } else {
        addr_10235_18:
        *reinterpret_cast<uint32_t*>(rbp36 - 0x3bc) = eax11;
        *reinterpret_cast<uint32_t*>(&rdx12) = eax11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
        goto addr_1023d_16;
    }
    if (r13_37 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp38 - 0x3d0) = 75;
        goto 0x10380;
    }
    if (rbx39 < 0) {
        if (rbx40 == -1) 
            goto 0x10158;
        goto 0x107f2;
    }
    *reinterpret_cast<int32_t*>(&rax41) = static_cast<int32_t>(rdx12 + 2);
    *reinterpret_cast<int32_t*>(&rax41 + 4) = 0;
    rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx43) + reinterpret_cast<uint64_t>(rbx44));
    tmp64_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax41) + reinterpret_cast<uint64_t>(r12_46));
    rax47 = tmp64_45;
    if (reinterpret_cast<unsigned char>(tmp64_45) < reinterpret_cast<unsigned char>(rax41)) 
        goto 0x107f2;
    if (reinterpret_cast<unsigned char>(rax47) >= reinterpret_cast<unsigned char>(rcx42)) 
        goto addr_10276_26;
    rax47 = rcx42;
    addr_10276_26:
    if (reinterpret_cast<unsigned char>(rbx48) >= reinterpret_cast<unsigned char>(rax47)) 
        goto 0x10158;
    if (reinterpret_cast<unsigned char>(rcx42) >= reinterpret_cast<unsigned char>(rax47)) {
        rax47 = rcx42;
    }
    if (rax47 == 0xffffffffffffffff) 
        goto 0x107f2;
    if (r15_49 != *reinterpret_cast<int64_t*>(rbp50 - 0x3e8)) {
        rax52 = fun_37c0(r15_51, rax47);
        if (!rax52) 
            goto 0x107f2;
        goto 0x10158;
    }
    rax53 = fun_3780(rax47, rsi14);
    if (!rax53) 
        goto 0x107f2;
    if (r12_54) 
        goto addr_1063a_36;
    goto 0x10158;
    addr_1063a_36:
    fun_3740(rax53, r15_55, r12_56, rax53, r15_55, r12_56);
    goto 0x10158;
    addr_10333_5:
    if ((*reinterpret_cast<struct s87**>(rbp57 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s87**>(rbp57 - 0x3f0))->f1 = 0;
        goto 0x10158;
    }
    if (reinterpret_cast<int32_t>(eax11) >= reinterpret_cast<int32_t>(0)) 
        goto addr_10235_18;
    if (**reinterpret_cast<int32_t**>(rbp58 - 0x3d0)) 
        goto 0x10380;
    eax59 = static_cast<uint32_t>(r14_60->f48) & 0xffffffef;
    eax61 = 22;
    if (*reinterpret_cast<signed char*>(&eax59) != 99) 
        goto addr_10377_42;
    eax61 = 84;
    addr_10377_42:
    **reinterpret_cast<int32_t**>(rbp62 - 0x3d0) = eax61;
}

void fun_102c0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_103b0() {
    void** rdi1;
    void* r15_2;
    void* r12_3;
    int64_t rbp4;
    void** r8_5;
    int64_t rbp6;
    int64_t rbp7;
    int64_t rsi8;
    void** rsi9;
    int64_t rsi10;

    rdi1 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<uint64_t>(r12_3));
    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x104f5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            r8_5 = *reinterpret_cast<void***>(rbp6 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp7 - 0x418) = rsi8;
            __asm__("fstp tword [rsp+0x8]");
            fun_34c0(rdi1, rsi9, 1, 0xffffffffffffffff, r8_5, rdi1, rsi10, 1, 0xffffffffffffffff, r8_5);
            goto 0x1020d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x101e3;
        }
    }
}

struct s89 {
    int32_t f0;
    void** f4;
};

struct s90 {
    signed char[4] pad4;
    void** f4;
};

void fun_103f8() {
    struct s89* rdi1;
    void* r15_2;
    void* r12_3;
    int32_t* rsi4;
    void** rdi5;
    void** rsi6;
    struct s90* rsi7;
    int64_t rbp8;
    void** r8_9;
    int64_t rbp10;
    int64_t rbp11;
    void** r8_12;
    int64_t rbp13;
    int64_t rbp14;

    rdi1 = reinterpret_cast<struct s89*>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<int64_t>(r12_3));
    rdi1->f0 = *rsi4;
    rdi5 = reinterpret_cast<void**>(&rdi1->f4);
    rsi6 = reinterpret_cast<void**>(&rsi7->f4);
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            r8_9 = *reinterpret_cast<void***>(rbp10 - 0x3e0);
            *reinterpret_cast<void***>(rbp11 - 0x418) = rsi6;
            fun_34c0(rdi5, rsi6, 1, 0xffffffffffffffff, r8_9, rdi5, rsi6, 1, 0xffffffffffffffff, r8_9);
            goto 0x1020d;
        }
    }
    r8_12 = *reinterpret_cast<void***>(rbp13 - 0x3e0);
    *reinterpret_cast<void***>(rbp14 - 0x418) = rsi6;
    fun_34c0(rdi5, rsi6, 1, 0xffffffffffffffff, r8_12, rdi5, rsi6, 1, 0xffffffffffffffff, r8_12);
    goto 0x1020d;
}

void fun_10460() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x102e6;
}

void fun_104b0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x10490;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x102ef;
}

void fun_10560() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x102e6;
    goto 0x10490;
}

void fun_105f3() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x10062;
}

void fun_10790() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x10717;
}

struct s91 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s92 {
    signed char[8] pad8;
    int64_t f8;
};

struct s93 {
    signed char[16] pad16;
    int64_t f10;
};

struct s94 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_10db8() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s91* rcx3;
    struct s92* rcx4;
    int64_t r11_5;
    struct s93* rcx6;
    uint32_t* rcx7;
    struct s94* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x10da0; else 
        goto "???";
}

struct s95 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s96 {
    signed char[8] pad8;
    int64_t f8;
};

struct s97 {
    signed char[16] pad16;
    int64_t f10;
};

struct s98 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_10df0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s95* rcx3;
    struct s96* rcx4;
    int64_t r11_5;
    struct s97* rcx6;
    uint32_t* rcx7;
    struct s98* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x10dd6;
}

struct s99 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s100 {
    signed char[8] pad8;
    int64_t f8;
};

struct s101 {
    signed char[16] pad16;
    int64_t f10;
};

struct s102 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_10e10() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s99* rcx3;
    struct s100* rcx4;
    int64_t r11_5;
    struct s101* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s102* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x10dd6;
}

struct s103 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s104 {
    signed char[8] pad8;
    int64_t f8;
};

struct s105 {
    signed char[16] pad16;
    int64_t f10;
};

struct s106 {
    signed char[16] pad16;
    signed char f10;
};

void fun_10e30() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s103* rcx3;
    struct s104* rcx4;
    int64_t r11_5;
    struct s105* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s106* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x10dd6;
}

struct s107 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s108 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_10eb0() {
    struct s107* rcx1;
    struct s108* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x10dd6;
}

struct s109 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s110 {
    signed char[8] pad8;
    int64_t f8;
};

struct s111 {
    signed char[16] pad16;
    int64_t f10;
};

struct s112 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_10f00() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s109* rcx3;
    struct s110* rcx4;
    int64_t r11_5;
    struct s111* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s112* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x10dd6;
}

void fun_1114c() {
}

void fun_1117b() {
    goto 0x11158;
}

void fun_111d0() {
}

void fun_113e0() {
    goto 0x111d3;
}

struct s113 {
    signed char[8] pad8;
    void** f8;
};

struct s114 {
    signed char[8] pad8;
    int64_t f8;
};

struct s115 {
    signed char[8] pad8;
    void** f8;
};

struct s116 {
    signed char[8] pad8;
    void** f8;
};

void fun_11488() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s113* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s114* r15_14;
    void** rax15;
    struct s115* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s116* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x11bc7;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x11bc7;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_3780(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x119e8; else 
                goto "???";
        }
    } else {
        rax15 = fun_37c0(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x11bc7;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_11d1a_9; else 
            goto addr_114fe_10;
    }
    addr_11654_11:
    rax19 = fun_3740(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_114fe_10:
    r14_20->f8 = rcx12;
    goto 0x11019;
    addr_11d1a_9:
    r13_18 = *r14_21;
    goto addr_11654_11;
}

struct s117 {
    signed char[11] pad11;
    void** fb;
};

struct s118 {
    signed char[80] pad80;
    int64_t f50;
};

struct s119 {
    signed char[80] pad80;
    int64_t f50;
};

struct s120 {
    signed char[8] pad8;
    void** f8;
};

struct s121 {
    signed char[8] pad8;
    void** f8;
};

struct s122 {
    signed char[8] pad8;
    void** f8;
};

struct s123 {
    signed char[72] pad72;
    signed char f48;
};

struct s124 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_11711() {
    void** ecx1;
    int32_t edx2;
    struct s117* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s118* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s119* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s120* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s121* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s122* r15_37;
    void*** r13_38;
    struct s123* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s124* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s117*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x115a8;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x11cfc;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_1186d_12;
    } else {
        addr_11414_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_1144f_17;
        }
    }
    rax25 = fun_3780(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x119da;
    addr_11980_19:
    rdx30 = *r15_31;
    rax32 = fun_3740(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_118b6_20:
    r15_33->f8 = r8_12;
    goto addr_11414_13;
    addr_1186d_12:
    rax34 = fun_37c0(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x11bc7;
    if (v36 != r15_37->f8) 
        goto addr_118b6_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_11980_19;
    addr_1144f_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x115ac;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x11490;
    goto 0x11019;
}

void fun_1172f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x113f8;
    if (dl2 & 4) 
        goto 0x113f8;
    if (edx3 > 7) 
        goto 0x113f8;
    if (dl4 & 2) 
        goto 0x113f8;
    goto 0x113f8;
}

void fun_11778() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x113f8;
    if (dl2 & 4) 
        goto 0x113f8;
    if (edx3 > 7) 
        goto 0x113f8;
    if (dl4 & 2) 
        goto 0x113f8;
    goto 0x113f8;
}

void fun_117c0() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x113f8;
    if (dl2 & 4) 
        goto 0x113f8;
    if (edx3 > 7) 
        goto 0x113f8;
    if (dl4 & 2) 
        goto 0x113f8;
    goto 0x113f8;
}

void fun_11808() {
    goto 0x113f8;
}

void fun_3a78() {
    void** rsi1;
    void** rcx2;
    void** r8_3;
    void** r9_4;
    signed char al5;
    void** rdi6;
    void** rcx7;

    rsi1 = optarg;
    al5 = posixtime(0x19120, rsi1, 6, rcx2, r8_3, r9_4, __return_address());
    if (!al5) {
        rdi6 = optarg;
        quote(rdi6, rsi1, 6, rcx7);
        fun_35b0();
        fun_3800();
    } else {
        g19128 = 0;
        __asm__("movdqa xmm0, [rip+0x15671]");
        __asm__("movaps [rip+0x1567a], xmm0");
        goto 0x39e8;
    }
}

void fun_3ae0() {
    change_times = change_times | 2;
    goto 0x39e8;
}

struct s125 {
    signed char[112] pad112;
    int64_t f70;
};

struct s126 {
    signed char[104] pad104;
    int64_t f68;
};

struct s127 {
    signed char[152] pad152;
    int32_t f98;
};

struct s128 {
    signed char[144] pad144;
    int64_t f90;
};

struct s129 {
    signed char[128] pad128;
    int64_t f80;
};

struct s130 {
    signed char[136] pad136;
    int64_t f88;
};

struct s131 {
    signed char[120] pad120;
    int64_t f78;
};

struct s132 {
    signed char[152] pad152;
    int32_t f98;
};

struct s133 {
    signed char[144] pad144;
    int64_t f90;
};

struct s134 {
    signed char[128] pad128;
    int64_t f80;
};

struct s135 {
    signed char[136] pad136;
    int64_t f88;
};

struct s136 {
    signed char[120] pad120;
    int64_t f78;
};

struct s137 {
    signed char[104] pad104;
    int64_t f68;
};

struct s138 {
    signed char[112] pad112;
    int64_t f70;
};

struct s139 {
    signed char[161] pad161;
    signed char fa1;
};

void fun_6022() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdi3;
    int64_t rbx4;
    int64_t rsi5;
    int64_t rbx6;
    int64_t rdx7;
    int64_t rbx8;
    int64_t rcx9;
    int64_t rbx10;
    int64_t v11;
    int64_t rbx12;
    int64_t r8_13;
    struct s125* r12_14;
    int32_t eax15;
    int64_t rbx16;
    int64_t r8_17;
    struct s126* r12_18;
    int32_t edi19;
    struct s127* r12_20;
    int32_t r8d21;
    int32_t* rbx22;
    int64_t rsi23;
    struct s128* r12_24;
    int64_t rdx25;
    struct s129* r12_26;
    int64_t rcx27;
    struct s130* r12_28;
    int64_t rax29;
    struct s131* r12_30;
    int32_t r8d31;
    int32_t edi32;
    int32_t v33;
    int64_t r8_34;
    int64_t rsi35;
    int64_t v36;
    int64_t r8_37;
    int64_t rcx38;
    int64_t v39;
    int64_t r8_40;
    int64_t v41;
    int64_t v42;
    int64_t r8_43;
    int64_t v44;
    int64_t rax45;
    int64_t v46;
    int64_t v47;
    int64_t v48;
    uint32_t eax49;
    int64_t rdx50;
    uint32_t r8d51;
    uint32_t r8d52;
    int32_t r8d53;
    int32_t v54;
    int64_t r8_55;
    int64_t v56;
    int64_t r8_57;
    int64_t v58;
    int64_t r8_59;
    int64_t v60;
    int64_t r8_61;
    uint32_t eax62;
    int64_t v63;
    int64_t rdx64;
    int64_t v65;
    uint32_t r8d66;
    uint32_t r8d67;
    struct s132* r12_68;
    struct s133* r12_69;
    struct s134* r12_70;
    struct s135* r12_71;
    struct s136* r12_72;
    struct s137* r12_73;
    struct s138* r12_74;
    struct s139* r12_75;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    rdi3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    rsi5 = *reinterpret_cast<int64_t*>(rbx6 - 40);
    rdx7 = *reinterpret_cast<int64_t*>(rbx8 - 32);
    rcx9 = *reinterpret_cast<int64_t*>(rbx10 - 24);
    v11 = *reinterpret_cast<int64_t*>(rbx12 - 16);
    r8_13 = r12_14->f70;
    eax15 = *reinterpret_cast<int32_t*>(rbx16 - 8);
    r8_17 = r12_18->f68;
    edi19 = r12_20->f98;
    r8d21 = *rbx22;
    rsi23 = r12_24->f90;
    rdx25 = r12_26->f80;
    rcx27 = r12_28->f88;
    rax29 = r12_30->f78;
    if (r8d21 < 0) {
        r8d31 = 0;
        edi32 = edi19 - eax15;
        *reinterpret_cast<unsigned char*>(&r8d31) = __intrinsic();
        v33 = r8d31;
        *reinterpret_cast<int32_t*>(&r8_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_34) + 4) = 0;
        rsi35 = rsi23 - v11;
        *reinterpret_cast<unsigned char*>(&r8_34) = __intrinsic();
        v36 = r8_34;
        *reinterpret_cast<int32_t*>(&r8_37) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_37) + 4) = 0;
        rcx38 = rcx27 - rcx9;
        *reinterpret_cast<unsigned char*>(&r8_37) = __intrinsic();
        v39 = r8_37;
        *reinterpret_cast<int32_t*>(&r8_40) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_40) = __intrinsic();
        v41 = rdx25 - rdx7;
        v42 = r8_40;
        *reinterpret_cast<int32_t*>(&r8_43) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_43) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_43) = __intrinsic();
        v44 = rax29 - rsi5;
        *reinterpret_cast<int32_t*>(&rax45) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax45) + 4) = 0;
        v46 = r8_43;
        *reinterpret_cast<unsigned char*>(&rax45) = __intrinsic();
        v47 = r8_13 - rdi3;
        v48 = rax45;
        eax49 = 0;
        *reinterpret_cast<unsigned char*>(&eax49) = __intrinsic();
        rdx50 = r8_17 - rax1;
        r8d51 = *reinterpret_cast<unsigned char*>(&v36);
        *reinterpret_cast<unsigned char*>(&r8d51) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d51) | *reinterpret_cast<unsigned char*>(&v33)) | *reinterpret_cast<unsigned char*>(&v39)) | *reinterpret_cast<unsigned char*>(&v42)) | *reinterpret_cast<unsigned char*>(&v46)) | *reinterpret_cast<unsigned char*>(&v48));
        r8d52 = r8d51 | eax49;
    } else {
        r8d53 = 0;
        edi32 = edi19 + eax15;
        *reinterpret_cast<unsigned char*>(&r8d53) = __intrinsic();
        v54 = r8d53;
        *reinterpret_cast<int32_t*>(&r8_55) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_55) + 4) = 0;
        rsi35 = rsi23 + v11;
        *reinterpret_cast<unsigned char*>(&r8_55) = __intrinsic();
        v56 = r8_55;
        *reinterpret_cast<int32_t*>(&r8_57) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_57) + 4) = 0;
        rcx38 = rcx27 + rcx9;
        *reinterpret_cast<unsigned char*>(&r8_57) = __intrinsic();
        v58 = r8_57;
        *reinterpret_cast<int32_t*>(&r8_59) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_59) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_59) = __intrinsic();
        v41 = rdx25 + rdx7;
        v60 = r8_59;
        *reinterpret_cast<int32_t*>(&r8_61) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_61) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_61) = __intrinsic();
        v44 = rax29 + rsi5;
        eax62 = 0;
        v63 = r8_61;
        *reinterpret_cast<unsigned char*>(&eax62) = __intrinsic();
        v47 = r8_13 + rdi3;
        *reinterpret_cast<int32_t*>(&rdx64) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx64) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx64) = __intrinsic();
        v65 = rdx64;
        rdx50 = r8_17 + rax1;
        r8d66 = *reinterpret_cast<unsigned char*>(&v56);
        *reinterpret_cast<unsigned char*>(&r8d66) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d66) | *reinterpret_cast<unsigned char*>(&v54)) | *reinterpret_cast<unsigned char*>(&v58)) | *reinterpret_cast<unsigned char*>(&v60)) | *reinterpret_cast<unsigned char*>(&v63));
        r8d67 = r8d66 | eax62;
        *reinterpret_cast<unsigned char*>(&r8d52) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d67) | *reinterpret_cast<unsigned char*>(&v65));
    }
    r12_68->f98 = edi32;
    r12_69->f90 = rsi35;
    r12_70->f80 = v41;
    r12_71->f88 = rcx38;
    r12_72->f78 = v44;
    r12_73->f68 = rdx50;
    r12_74->f70 = v47;
    if (!*reinterpret_cast<unsigned char*>(&r8d52)) {
        r12_75->fa1 = 1;
        goto 0x5f40;
    }
}

struct s140 {
    signed char[225] pad225;
    unsigned char fe1;
};

struct s141 {
    signed char[56] pad56;
    void** f38;
};

struct s142 {
    signed char[64] pad64;
    int64_t f40;
};

struct s143 {
    signed char[48] pad48;
    int64_t f30;
};

struct s144 {
    signed char[16] pad16;
    int64_t f10;
};

struct s145 {
    signed char[48] pad48;
    int64_t f30;
};

struct s146 {
    signed char[56] pad56;
    int64_t f38;
};

struct s147 {
    signed char[64] pad64;
    int64_t f40;
};

struct s148 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_68ae() {
    uint32_t eax1;
    struct s140* r12_2;
    void** r8_3;
    int64_t rbx4;
    void** v5;
    int64_t rbx6;
    void** rax7;
    void** rcx8;
    void** r9_9;
    struct s141* r12_10;
    struct s142* r12_11;
    int64_t rbx12;
    struct s143* r12_13;
    struct s144* rbx14;
    void** rcx15;
    int64_t rbx16;
    void** rax17;
    void** r9_18;
    struct s145* r12_19;
    int64_t rbx20;
    struct s146* r12_21;
    int64_t rbx22;
    struct s147* r12_23;
    struct s148* rbx24;

    eax1 = r12_2->fe1;
    r8_3 = *reinterpret_cast<void***>(rbx4 - 0xd0);
    if (reinterpret_cast<signed char>(r8_3) <= reinterpret_cast<signed char>(3)) {
        v5 = *reinterpret_cast<void***>(rbx6 - 0xd8);
        if (*reinterpret_cast<signed char*>(&eax1)) {
            rax7 = fun_35b0();
            dbg_printf(rax7, v5, 5, rcx8, r8_3, r9_9, __return_address());
        }
        __asm__("movdqu xmm6, [rbx]");
        r12_10->f38 = v5;
        __asm__("movups [r12+0x20], xmm6");
        r12_11->f40 = *reinterpret_cast<int64_t*>(rbx12 - 0x68);
        r12_13->f30 = rbx14->f10;
        goto 0x5f40;
    } else {
        if (*reinterpret_cast<signed char*>(&eax1)) {
            rcx15 = *reinterpret_cast<void***>(rbx16 - 0xd8);
            rax17 = fun_35b0();
            dbg_printf(rax17, rcx15, r8_3, rcx15, r8_3, r9_18, __return_address());
        }
        __asm__("movdqu xmm7, [rbx-0xe0]");
        r12_19->f30 = *reinterpret_cast<int64_t*>(rbx20 - 0xd0);
        __asm__("movups [r12+0x20], xmm7");
        r12_21->f38 = *reinterpret_cast<int64_t*>(rbx22 - 0x68);
        r12_23->f40 = rbx24->f8;
        goto 0x5f40;
    }
}

void fun_65e4() {
    goto 0x5f40;
}

void fun_6738() {
    goto 0x5f40;
}

void fun_690a() {
    if (__intrinsic()) 
        goto 0x61b0;
    goto 0x5f40;
}

void fun_6998() {
    goto 0x5f40;
}

void fun_6b1a() {
    goto 0x5f40;
}

struct s149 {
    signed char[80] pad80;
    int64_t f50;
};

struct s150 {
    signed char[88] pad88;
    int64_t f58;
};

struct s151 {
    signed char[72] pad72;
    int64_t f48;
};

struct s152 {
    signed char[96] pad96;
    int64_t f60;
};

struct s153 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_6c94() {
    struct s149* r12_1;
    int64_t rax2;
    int64_t rbx3;
    struct s150* r12_4;
    struct s151* r12_5;
    struct s152* r12_6;
    struct s153* r12_7;

    r12_1->f50 = 0;
    rax2 = *reinterpret_cast<int64_t*>(rbx3 - 48);
    r12_4->f58 = 0;
    r12_5->f48 = rax2;
    r12_6->f60 = 0;
    r12_7->f1c = 2;
    goto 0x5f40;
}

void fun_6e3f() {
    goto 0x6dd9;
}

struct s154 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_7082() {
    struct s154* r12_1;

    r12_1->f18 = 0xffff9d90;
    goto 0x5f40;
}

struct s155 {
    signed char[64] pad64;
    int64_t f40;
};

struct s156 {
    signed char[56] pad56;
    int64_t f38;
};

void fun_71c4() {
    struct s155* r12_1;
    int64_t rbx2;
    struct s156* r12_3;
    int64_t* rbx4;

    r12_1->f40 = *reinterpret_cast<int64_t*>(rbx2 - 48);
    r12_3->f38 = *rbx4;
    goto 0x5f40;
}

void fun_a4fe() {
    goto 0xa172;
}

void fun_a6d4() {
    goto 0xa68c;
}

void fun_a79b() {
    goto 0xa2c8;
}

void fun_a7ed() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0xa770;
    goto 0xa39f;
}

void fun_a81f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0xa77b;
        goto 0xa1a0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0xa61a;
        goto 0xa3bb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0xafb8;
    if (r10_8 > r15_9) 
        goto addr_a705_9;
    addr_a70a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0xafc3;
    goto 0xa2f4;
    addr_a705_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_a70a_10;
}

void fun_a852() {
    goto 0xa387;
}

void fun_ac20() {
    goto 0xa387;
}

void fun_b3bf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0xa4dc;
    } else {
        goto 0xa640;
    }
}

void fun_c980() {
}

void fun_e4c8(int32_t edi) {
    signed char r8b2;

    if (r8b2) {
    }
    if (edi == 69) 
        goto 0xe360; else 
        goto "???";
}

void fun_e7e5(int32_t edi) {
    signed char r8b2;

    if (edi) 
        goto 0xe360;
    if (r8b2) {
    }
    goto 0xe502;
}

struct s157 {
    signed char[20] pad20;
    int32_t f14;
    uint32_t f18;
    int32_t f1c;
};

void fun_e61f(int32_t edi, signed char sil) {
    int64_t rcx3;
    struct s157* v4;
    uint32_t edx5;
    int64_t rax6;
    int32_t r9d7;
    uint64_t rax8;
    int64_t rax9;
    int64_t r8_10;
    int64_t rax11;
    int32_t r9d12;
    int64_t rax13;
    uint32_t eax14;
    uint32_t eax15;
    uint32_t r11d16;
    uint32_t edx17;
    int64_t r11_18;
    uint64_t rax19;
    int64_t rax20;
    int64_t r9_21;
    int32_t eax22;
    int32_t r10d23;
    int32_t v24;
    int64_t rdx25;
    int32_t r8d26;
    int64_t rdx27;
    int32_t r10d28;
    int32_t r10d29;
    int32_t v30;

    if (edi == 69) 
        goto 0xe360;
    *reinterpret_cast<int32_t*>(&rcx3) = v4->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx5 = v4->f1c - v4->f18 + 0x17e;
    *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rcx3) >> 31) & 0x190;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    r9d7 = static_cast<int32_t>(rcx3 + rax6 - 100);
    rax8 = reinterpret_cast<int32_t>(edx5) * 0xffffffff92492493 >> 32;
    *reinterpret_cast<int32_t*>(&rax9) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax8) + edx5) >> 2) - (reinterpret_cast<int32_t>(edx5) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_10) = static_cast<int32_t>(rax9 * 8) - *reinterpret_cast<int32_t*>(&rax9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax11) = v4->f1c - edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    if (static_cast<int32_t>(rax11 + r8_10 + 3) < 0) {
        r9d12 = r9d7 - 1;
        if (!(*reinterpret_cast<unsigned char*>(&r9d12) & 3) && reinterpret_cast<uint32_t>(r9d12 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28) {
            __asm__("cdq ");
        }
        *reinterpret_cast<int32_t*>(&rax13) = -1;
    } else {
        eax14 = 0x16d;
        if (!(*reinterpret_cast<unsigned char*>(&r9d7) & 3) && (eax14 = 0x16e, reinterpret_cast<uint32_t>(r9d7 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28)) {
            __asm__("cdq ");
            eax15 = reinterpret_cast<uint32_t>(r9d7 / 0x190);
            eax14 = eax15 - (eax15 + reinterpret_cast<uint1_t>(eax15 < eax15 + reinterpret_cast<uint1_t>(!!(r9d7 % 0x190)))) + 0x16e;
        }
        r11d16 = v4->f1c - eax14;
        edx17 = r11d16 - v4->f18 + 0x17e;
        *reinterpret_cast<uint32_t*>(&r11_18) = r11d16 - edx17;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_18) + 4) = 0;
        rax19 = reinterpret_cast<int32_t>(edx17) * 0xffffffff92492493 >> 32;
        *reinterpret_cast<int32_t*>(&rax20) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax19) + edx17) >> 2) - (reinterpret_cast<int32_t>(edx17) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r9_21) = static_cast<int32_t>(rax20 * 8) - *reinterpret_cast<int32_t*>(&rax20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_21) + 4) = 0;
        eax22 = static_cast<int32_t>(r11_18 + r9_21 + 3);
        if (eax22 >= 0) {
        }
        *reinterpret_cast<int32_t*>(&rax13) = (eax22 >> 31) + 1;
    }
    if (sil == 71) {
        if (r10d23) 
            goto 0xf898;
        if (v24 == 43) 
            goto 0xfb34;
        goto 0xe7a3;
    } else {
        if (sil != 0x67) {
            goto 0xe9f0;
        } else {
            rdx25 = *reinterpret_cast<int32_t*>(&rcx3) * 0x51eb851f >> 37;
            r8d26 = *reinterpret_cast<int32_t*>(&rcx3) - (*reinterpret_cast<int32_t*>(&rdx25) - (*reinterpret_cast<int32_t*>(&rcx3) >> 31)) * 100 + *reinterpret_cast<int32_t*>(&rax13);
            rdx27 = r8d26 * 0x51eb851f >> 37;
            if (r8d26 - (*reinterpret_cast<int32_t*>(&rdx27) - (r8d26 >> 31)) * 100 < 0) {
                if (*reinterpret_cast<int32_t*>(&rcx3) >= 0xfffff894 - *reinterpret_cast<int32_t*>(&rax13)) 
                    goto 0xfc3d;
                if (!r10d28) 
                    goto 0xf2e8; else 
                    goto "???";
            } else {
                if (r10d29) 
                    goto 0xf855;
                if (v30 == 43) 
                    goto 0xf85f; else 
                    goto "???";
            }
        }
    }
}

void fun_ecbe() {
    int64_t rbx1;
    int64_t rbp2;
    uint64_t rax3;
    int64_t v4;
    uint64_t r13_5;
    int32_t r10d6;
    int32_t r15d7;
    int64_t r14_8;
    uint64_t rdx9;
    int32_t r15d10;
    uint64_t rcx11;
    int64_t r14_12;
    int32_t r15d13;
    uint64_t r15_14;
    int64_t r14_15;
    int32_t r10d16;
    int32_t r10d17;
    uint64_t r14_18;
    void** r14_19;
    void** r14_20;
    uint32_t eax21;
    unsigned char* rbx22;

    if (rbx1 - 1 != rbp2) {
        goto 0xe360;
    }
    rax3 = v4 - r13_5;
    if (r10d6 == 45 || r15d7 < 0) {
        if (rax3 <= 1) 
            goto 0xe240;
        if (!r14_8) 
            goto addr_f9ff_6;
    } else {
        rdx9 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d10));
        *reinterpret_cast<int32_t*>(&rcx11) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
        if (rdx9) {
            rcx11 = rdx9;
        }
        if (rcx11 >= rax3) 
            goto 0xe240;
        if (!r14_12) 
            goto 0xed48;
        if (r15d13 > 1) {
            r15_14 = r14_15 + (rdx9 - 1);
            if (r10d16 == 48 || r10d17 == 43) {
                r14_18 = r15_14;
                fun_3690(r14_19, r14_19);
            } else {
                r14_18 = r15_14;
                fun_3690(r14_20, r14_20);
            }
        }
    }
    eax21 = *rbx22;
    *reinterpret_cast<signed char*>(r14_18 + 1 - 1) = *reinterpret_cast<signed char*>(&eax21);
    goto 0xed48;
    addr_f9ff_6:
    goto 0xed48;
}

void fun_ed50(int32_t edi) {
    signed char r8b2;

    if (edi == 69) 
        goto 0xe360;
    if (r8b2) {
    }
    goto 0xe4e3;
}

void fun_ef2a(int32_t edi, int64_t rsi) {
    int32_t r15d3;
    int32_t r10d4;
    int64_t r15_5;
    uint32_t r8d6;
    unsigned char v7;
    int64_t rax8;
    int32_t v9;
    void** v10;
    int64_t v11;
    int64_t rax12;

    if (edi) 
        goto 0xe360;
    if (r15d3 >= 0 || r10d4) {
        if (static_cast<int32_t>(r15_5 - 6) >= 0) {
        }
        goto 0xedff;
    } else {
        r8d6 = v7;
        *reinterpret_cast<int32_t*>(&rax8) = v9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        __strftime_internal_isra_0(0, 0xffffffffffffffff, "%Y-%m-%d", v10, *reinterpret_cast<unsigned char*>(&r8d6), 43, 4, v11, rax8, rax12, __return_address());
        goto 0xee7d;
    }
}

void fun_f04b(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
}

void fun_f123(int32_t edi) {
    if (edi == 69) 
        goto 0xe360;
    goto 0xeaa3;
}

struct s158 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_f2a0(int32_t edi) {
    int64_t rdx2;
    struct s158* v3;
    int64_t rax4;
    int64_t rdx5;
    int32_t r10d6;

    if (edi == 69) 
        goto 0xe7c1;
    rdx2 = v3->f14;
    rax4 = rdx2;
    rdx5 = rdx2 * 0x51eb851f >> 37;
    if (*reinterpret_cast<int32_t*>(&rax4) - (*reinterpret_cast<int32_t*>(&rdx5) - (*reinterpret_cast<int32_t*>(&rax4) >> 31)) * 100 < 0 && *reinterpret_cast<int32_t*>(&rax4) <= 0xfffff893) {
    }
    if (r10d6) 
        goto 0xf855; else 
        goto "???";
}

void fun_f471() {
    int1_t zf1;
    signed char r8b2;

    zf1 = r8b2 == 0;
    if (!zf1) {
    }
    if (!zf1) {
    }
    goto 0xe4e3;
}

void fun_f4a9() {
    goto 0xf473;
}

struct s159 {
    signed char[1] pad1;
    signed char f1;
};

void fun_10050() {
    signed char* r13_1;
    struct s159* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_1079e() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x10717;
}

struct s160 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s161 {
    signed char[8] pad8;
    int64_t f8;
};

struct s162 {
    signed char[16] pad16;
    int64_t f10;
};

struct s163 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_10ed0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s160* rcx3;
    struct s161* rcx4;
    int64_t r11_5;
    struct s162* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s163* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x10dd6;
}

void fun_11185() {
    goto 0x11158;
}

void fun_11ad4() {
    goto 0x113f8;
}

void fun_113e8() {
}

void fun_11818() {
    goto 0x113f8;
}

void fun_3af0() {
    no_dereference = 1;
    goto 0x39e8;
}

struct s164 {
    signed char[152] pad152;
    int32_t f98;
};

struct s165 {
    signed char[48] pad48;
    int32_t f30;
};

struct s166 {
    signed char[152] pad152;
    int32_t f98;
};

struct s167 {
    signed char[144] pad144;
    int64_t f90;
};

struct s168 {
    signed char[144] pad144;
    int64_t f90;
};

struct s169 {
    signed char[40] pad40;
    int64_t f28;
};

struct s170 {
    signed char[136] pad136;
    int64_t f88;
};

struct s171 {
    signed char[136] pad136;
    int64_t f88;
};

struct s172 {
    signed char[32] pad32;
    int64_t f20;
};

struct s173 {
    signed char[128] pad128;
    int64_t f80;
};

struct s174 {
    signed char[128] pad128;
    int64_t f80;
};

struct s175 {
    signed char[24] pad24;
    int64_t f18;
};

struct s176 {
    signed char[120] pad120;
    int64_t f78;
};

struct s177 {
    signed char[120] pad120;
    int64_t f78;
};

struct s178 {
    signed char[16] pad16;
    int64_t f10;
};

struct s179 {
    signed char[112] pad112;
    int64_t f70;
};

struct s180 {
    signed char[112] pad112;
    int64_t f70;
};

struct s181 {
    signed char[8] pad8;
    int64_t f8;
};

struct s182 {
    signed char[104] pad104;
    int64_t f68;
};

struct s183 {
    signed char[104] pad104;
    int64_t f68;
};

void fun_6622() {
    uint32_t r8d1;
    struct s164* r12_2;
    struct s165* rbx3;
    struct s166* r12_4;
    uint32_t edi5;
    struct s167* r12_6;
    struct s168* r12_7;
    struct s169* rbx8;
    struct s170* r12_9;
    struct s171* r12_10;
    struct s172* rbx11;
    uint32_t esi12;
    struct s173* r12_13;
    struct s174* r12_14;
    struct s175* rbx15;
    int64_t rcx16;
    struct s176* r12_17;
    struct s177* r12_18;
    struct s178* rbx19;
    int64_t rdx20;
    struct s179* r12_21;
    int64_t v22;
    int64_t rcx23;
    struct s180* r12_24;
    struct s181* rbx25;
    int64_t v26;
    int64_t rcx27;
    struct s182* r12_28;
    struct s183* r12_29;
    int64_t* rbx30;
    uint32_t eax31;

    r8d1 = 0;
    r12_2->f98 = rbx3->f30 + r12_4->f98;
    *reinterpret_cast<unsigned char*>(&r8d1) = __intrinsic();
    edi5 = 0;
    r12_6->f90 = r12_7->f90 + rbx8->f28;
    *reinterpret_cast<unsigned char*>(&edi5) = __intrinsic();
    r12_9->f88 = r12_10->f88 + rbx11->f20;
    esi12 = 0;
    *reinterpret_cast<unsigned char*>(&esi12) = __intrinsic();
    r12_13->f80 = r12_14->f80 + rbx15->f18;
    *reinterpret_cast<int32_t*>(&rcx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx16) = __intrinsic();
    r12_17->f78 = r12_18->f78 + rbx19->f10;
    rdx20 = r12_21->f70;
    v22 = rcx16;
    *reinterpret_cast<int32_t*>(&rcx23) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx23) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx23) = __intrinsic();
    r12_24->f70 = rdx20 + rbx25->f8;
    v26 = rcx23;
    *reinterpret_cast<int32_t*>(&rcx27) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx27) = __intrinsic();
    r12_28->f68 = r12_29->f68 + *rbx30;
    eax31 = static_cast<uint32_t>(static_cast<unsigned char>(__intrinsic())) | r8d1 | edi5 | esi12;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax31) | *reinterpret_cast<unsigned char*>(&v22)) | *reinterpret_cast<unsigned char*>(&v26)) 
        goto 0x61b0;
    if (rcx27) 
        goto 0x61b0; else 
        goto "???";
}

void fun_6775() {
    goto 0x5f40;
}

void fun_67da() {
}

void fun_6913() {
    goto 0x65e8;
}

void fun_679d() {
}

void fun_69d5() {
    goto 0x5f40;
}

struct s184 {
    signed char[208] pad208;
    int64_t fd0;
};

struct s185 {
    signed char[208] pad208;
    int64_t fd0;
};

void fun_6b26() {
    struct s184* r12_1;
    struct s185* r12_2;

    r12_1->fd0 = r12_2->fd0 + 1;
    goto 0x6949;
}

struct s186 {
    signed char[160] pad160;
    signed char fa0;
};

void fun_6cc6() {
    struct s186* r12_1;

    __asm__("movdqu xmm6, [rbx]");
    r12_1->fa0 = 1;
    __asm__("movups [r12+0x58], xmm6");
    goto 0x6949;
}

struct s187 {
    signed char[176] pad176;
    int64_t fb0;
};

struct s188 {
    signed char[176] pad176;
    int64_t fb0;
};

void fun_6e5f() {
    struct s187* r12_1;
    struct s188* r12_2;

    r12_1->fb0 = r12_2->fb0 + 1;
    goto 0x6949;
}

struct s189 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_7090() {
    int64_t rax1;
    int64_t* rbx2;
    struct s189* r12_3;

    rax1 = *rbx2;
    r12_3->f18 = *reinterpret_cast<int32_t*>(&rax1);
    goto 0x5f40;
}

struct s190 {
    signed char[56] pad56;
    int64_t f38;
};

struct s191 {
    signed char[64] pad64;
    int64_t f40;
};

void fun_71da() {
    struct s190* r12_1;
    int64_t rbx2;
    struct s191* r12_3;
    int64_t rbx4;

    __asm__("movdqu xmm7, [rbx]");
    r12_1->f38 = *reinterpret_cast<int64_t*>(rbx2 - 0xa8);
    __asm__("movups [r12+0x20], xmm7");
    r12_3->f40 = *reinterpret_cast<int64_t*>(rbx4 - 0x68);
    goto 0x718c;
}

void fun_a85c() {
    goto 0xa7f7;
}

void fun_ac2a() {
    goto 0xa74d;
}

void fun_c9e0() {
    fun_35b0();
    goto fun_3880;
}

void fun_f4b0() {
    goto 0xedff;
}

void fun_10530() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x102e6;
    goto 0x10490;
}

void fun_107ac() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x10717;
}

struct s192 {
    int32_t f0;
    int32_t f4;
};

struct s193 {
    int32_t f0;
    int32_t f4;
};

struct s194 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s195 {
    signed char[8] pad8;
    int64_t f8;
};

struct s196 {
    signed char[8] pad8;
    int64_t f8;
};

struct s197 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_10e80(struct s192* rdi, struct s193* rsi) {
    struct s194* rcx3;
    struct s195* rcx4;
    struct s196* rcx5;
    struct s197* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x10dd6;
}

void fun_1118f() {
    goto 0x11158;
}

void fun_11ade() {
    goto 0x113f8;
}

void fun_3b00() {
    goto 0x39e8;
}

void fun_6787() {
}

struct s198 {
    signed char[208] pad208;
    int64_t fd0;
};

struct s199 {
    signed char[208] pad208;
    int64_t fd0;
};

struct s200 {
    signed char[168] pad168;
    int64_t fa8;
};

struct s201 {
    signed char[168] pad168;
    int64_t fa8;
};

void fun_691c() {
    struct s198* r12_1;
    struct s199* r12_2;
    struct s200* r12_3;
    struct s201* r12_4;

    r12_1->fd0 = r12_2->fd0 + 1;
    r12_3->fa8 = r12_4->fa8 + 1;
}

struct s202 {
    signed char[152] pad152;
    int32_t f98;
};

struct s203 {
    signed char[48] pad48;
    int32_t f30;
};

struct s204 {
    signed char[152] pad152;
    int32_t f98;
};

struct s205 {
    signed char[144] pad144;
    int64_t f90;
};

struct s206 {
    signed char[144] pad144;
    int64_t f90;
};

struct s207 {
    signed char[40] pad40;
    int64_t f28;
};

struct s208 {
    signed char[136] pad136;
    int64_t f88;
};

struct s209 {
    signed char[136] pad136;
    int64_t f88;
};

struct s210 {
    signed char[32] pad32;
    int64_t f20;
};

struct s211 {
    signed char[128] pad128;
    int64_t f80;
};

struct s212 {
    signed char[128] pad128;
    int64_t f80;
};

struct s213 {
    signed char[24] pad24;
    int64_t f18;
};

struct s214 {
    signed char[120] pad120;
    int64_t f78;
};

struct s215 {
    signed char[120] pad120;
    int64_t f78;
};

struct s216 {
    signed char[16] pad16;
    int64_t f10;
};

struct s217 {
    signed char[112] pad112;
    int64_t f70;
};

struct s218 {
    signed char[112] pad112;
    int64_t f70;
};

struct s219 {
    signed char[8] pad8;
    int64_t f8;
};

struct s220 {
    signed char[104] pad104;
    int64_t f68;
};

struct s221 {
    signed char[104] pad104;
    int64_t f68;
};

struct s222 {
    signed char[161] pad161;
    signed char fa1;
};

void fun_6a12() {
    struct s16* r12_1;
    uint32_t r8d2;
    struct s202* r12_3;
    struct s203* rbx4;
    struct s204* r12_5;
    uint32_t edi6;
    struct s205* r12_7;
    struct s206* r12_8;
    struct s207* rbx9;
    struct s208* r12_10;
    struct s209* r12_11;
    struct s210* rbx12;
    uint32_t esi13;
    struct s211* r12_14;
    struct s212* r12_15;
    struct s213* rbx16;
    uint32_t r10d17;
    struct s214* r12_18;
    struct s215* r12_19;
    struct s216* rbx20;
    struct s217* r12_21;
    struct s218* r12_22;
    struct s219* rbx23;
    int64_t rcx24;
    struct s220* r12_25;
    struct s221* r12_26;
    int64_t* rbx27;
    uint32_t eax28;
    struct s222* r12_29;

    digits_to_date_time(r12_1);
    r8d2 = 0;
    r12_3->f98 = rbx4->f30 + r12_5->f98;
    *reinterpret_cast<unsigned char*>(&r8d2) = __intrinsic();
    edi6 = 0;
    r12_7->f90 = r12_8->f90 + rbx9->f28;
    *reinterpret_cast<unsigned char*>(&edi6) = __intrinsic();
    r12_10->f88 = r12_11->f88 + rbx12->f20;
    esi13 = 0;
    *reinterpret_cast<unsigned char*>(&esi13) = __intrinsic();
    r12_14->f80 = r12_15->f80 + rbx16->f18;
    r10d17 = 0;
    *reinterpret_cast<unsigned char*>(&r10d17) = __intrinsic();
    r12_18->f78 = r12_19->f78 + rbx20->f10;
    r12_21->f70 = r12_22->f70 + rbx23->f8;
    *reinterpret_cast<int32_t*>(&rcx24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx24) = __intrinsic();
    r12_25->f68 = r12_26->f68 + *rbx27;
    eax28 = static_cast<uint32_t>(static_cast<unsigned char>(__intrinsic())) | r8d2 | edi6 | esi13 | r10d17;
    if (*reinterpret_cast<unsigned char*>(&eax28) | static_cast<unsigned char>(__intrinsic())) 
        goto 0x61b0;
    if (rcx24) 
        goto 0x61b0;
    r12_29->fa1 = 1;
    goto 0x5f40;
}

struct s223 {
    signed char[28] pad28;
    int32_t f1c;
};

struct s224 {
    signed char[88] pad88;
    int64_t f58;
};

struct s225 {
    signed char[72] pad72;
    int64_t f48;
};

struct s226 {
    signed char[80] pad80;
    int64_t f50;
};

struct s227 {
    signed char[96] pad96;
    int64_t f60;
};

void fun_6b4f() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s223* r12_5;
    int64_t rcx6;
    int64_t rbx7;
    struct s224* r12_8;
    int64_t rax9;
    int64_t rbx10;
    struct s225* r12_11;
    struct s226* r12_12;
    struct s227* r12_13;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 0xa0);
    r12_5->f1c = 2;
    rcx6 = *reinterpret_cast<int64_t*>(rbx7 - 0x110);
    r12_8->f58 = rax1;
    rax9 = *reinterpret_cast<int32_t*>(rbx10 - 48);
    r12_11->f48 = rcx6;
    r12_12->f50 = rdx3;
    r12_13->f60 = rax9;
    goto 0x5f40;
}

struct s228 {
    signed char[168] pad168;
    int64_t fa8;
};

struct s229 {
    signed char[168] pad168;
    int64_t fa8;
};

void fun_6cf9() {
    struct s228* r12_1;
    struct s229* r12_2;

    r12_1->fa8 = r12_2->fa8 + 1;
    goto 0x6949;
}

struct s230 {
    signed char[232] pad232;
    signed char fe8;
};

struct s231 {
    signed char[8] pad8;
    int64_t f8;
};

struct s232 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_6e88() {
    struct s230* r12_1;
    struct s231* r12_2;
    int64_t rbx3;
    int64_t rax4;
    int64_t* rbx5;
    struct s232* r12_6;

    r12_1->fe8 = 1;
    r12_2->f8 = *reinterpret_cast<int64_t*>(rbx3 - 48);
    rax4 = *rbx5;
    r12_6->f10 = *reinterpret_cast<int32_t*>(&rax4);
    goto 0x5f40;
}

struct s233 {
    signed char[200] pad200;
    int64_t fc8;
};

struct s234 {
    signed char[200] pad200;
    int64_t fc8;
};

struct s235 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_709d() {
    struct s233* r12_1;
    struct s234* r12_2;
    struct s235* r12_3;

    r12_1->fc8 = r12_2->fc8 + 1;
    r12_3->f14 = 1;
    goto 0x5f40;
}

struct s236 {
    signed char[56] pad56;
    int64_t f38;
};

struct s237 {
    signed char[64] pad64;
    int64_t f40;
};

struct s238 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_71fb() {
    struct s236* r12_1;
    int64_t rbx2;
    struct s237* r12_3;
    struct s238* rbx4;

    r12_1->f38 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    r12_3->f40 = rbx4->f8;
    goto 0x5f40;
}

void fun_a52d() {
    goto 0xa172;
}

void fun_a868() {
    goto 0xa7f7;
}

void fun_ac37() {
    goto 0xa79e;
}

void fun_ca20() {
    fun_35b0();
    goto fun_3880;
}

void fun_107bb() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x10717;
}

void fun_11199() {
    goto 0x11158;
}

void fun_3b10() {
    no_create = 1;
    goto 0x39e8;
}

struct s239 {
    signed char[88] pad88;
    int64_t f58;
};

struct s240 {
    signed char[96] pad96;
    int64_t f60;
};

struct s241 {
    signed char[72] pad72;
    int64_t f48;
};

struct s242 {
    signed char[80] pad80;
    int64_t f50;
};

struct s243 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_6b87() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s239* r12_5;
    struct s240* r12_6;
    struct s241* r12_7;
    struct s242* r12_8;
    struct s243* r12_9;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 48);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 0xa0);
    r12_5->f58 = 0;
    r12_6->f60 = 0;
    r12_7->f48 = rdx3;
    r12_8->f50 = rax1;
    r12_9->f1c = 2;
    goto 0x5f40;
}

struct s244 {
    signed char[88] pad88;
    int64_t f58;
};

struct s245 {
    signed char[72] pad72;
    int64_t f48;
};

struct s246 {
    signed char[96] pad96;
    int64_t f60;
};

struct s247 {
    signed char[80] pad80;
    int64_t f50;
};

struct s248 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_6d22() {
    int64_t rdx1;
    int64_t rbx2;
    int64_t rcx3;
    int64_t rbx4;
    struct s244* r12_5;
    int64_t rbx6;
    int64_t rax7;
    int64_t rbx8;
    struct s245* r12_9;
    struct s246* r12_10;
    int64_t rax11;
    int64_t* rbx12;
    struct s247* r12_13;
    struct s248* r12_14;

    rdx1 = *reinterpret_cast<int64_t*>(rbx2 - 0xa0);
    rcx3 = *reinterpret_cast<int64_t*>(rbx4 - 0x110);
    r12_5->f58 = *reinterpret_cast<int64_t*>(rbx6 - 56);
    rax7 = *reinterpret_cast<int32_t*>(rbx8 - 48);
    r12_9->f48 = rcx3;
    r12_10->f60 = rax7;
    rax11 = *rbx12;
    r12_13->f50 = rdx1;
    r12_14->f1c = *reinterpret_cast<int32_t*>(&rax11);
    goto 0x5f40;
}

struct s249 {
    signed char[232] pad232;
    signed char fe8;
};

struct s250 {
    signed char[8] pad8;
    int64_t f8;
};

struct s251 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_6ea7() {
    struct s249* r12_1;
    struct s250* r12_2;
    int64_t rbx3;
    int64_t rax4;
    int64_t* rbx5;
    struct s251* r12_6;

    r12_1->fe8 = 1;
    r12_2->f8 = *reinterpret_cast<int64_t*>(rbx3 - 56);
    rax4 = *rbx5;
    r12_6->f10 = *reinterpret_cast<int32_t*>(&rax4);
    goto 0x5f40;
}

struct s252 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_70b4() {
    int64_t rax1;
    int64_t* rbx2;
    struct s252* r12_3;

    rax1 = *rbx2;
    r12_3->f14 = *reinterpret_cast<int32_t*>(&rax1);
    goto 0x5f40;
}

void fun_7212() {
    goto 0x5f15;
}

void fun_a55a() {
    goto 0xa172;
}

void fun_a874() {
    goto 0xa770;
}

void fun_ca60() {
    fun_35b0();
    goto fun_3880;
}

void fun_111a3() {
    goto 0x11158;
}

void fun_3b20() {
    change_times = change_times | 1;
    goto 0x39e8;
}

void fun_6bbc() {
    struct s16* r12_1;

    digits_to_date_time(r12_1);
    goto 0x5f40;
}

struct s253 {
    signed char[88] pad88;
    int64_t f58;
};

struct s254 {
    signed char[96] pad96;
    int64_t f60;
};

struct s255 {
    signed char[80] pad80;
    int64_t f50;
};

struct s256 {
    signed char[72] pad72;
    int64_t f48;
};

struct s257 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_6d59() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s253* r12_5;
    struct s254* r12_6;
    struct s255* r12_7;
    int64_t rax8;
    int64_t* rbx9;
    struct s256* r12_10;
    struct s257* r12_11;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 48);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 0xa0);
    r12_5->f58 = 0;
    r12_6->f60 = 0;
    r12_7->f50 = rax1;
    rax8 = *rbx9;
    r12_10->f48 = rdx3;
    r12_11->f1c = *reinterpret_cast<int32_t*>(&rax8);
    goto 0x5f40;
}

struct s258 {
    signed char[8] pad8;
    int64_t f8;
};

struct s259 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_6ec6() {
    struct s258* r12_1;
    int64_t rax2;
    int64_t rbx3;
    struct s259* r12_4;

    r12_1->f8 = 0;
    rax2 = *reinterpret_cast<int64_t*>(rbx3 - 56);
    r12_4->f10 = *reinterpret_cast<int32_t*>(&rax2);
    goto 0x5f40;
}

struct s260 {
    signed char[216] pad216;
    int64_t fd8;
};

struct s261 {
    signed char[216] pad216;
    int64_t fd8;
};

void fun_70c1() {
    int64_t rcx1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s260* r12_5;
    struct s261* r12_6;
    uint32_t esi7;
    int64_t rbx8;
    int64_t r8_9;
    int64_t* rbx10;
    struct s17* r12_11;
    signed char al12;

    rcx1 = *reinterpret_cast<int64_t*>(rbx2 - 40);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    r12_5->fd8 = r12_6->fd8 + 1;
    esi7 = *reinterpret_cast<unsigned char*>(rbx8 - 56);
    r8_9 = *rbx10;
    al12 = time_zone_hhmm_isra_0(r12_11, *reinterpret_cast<signed char*>(&esi7), rdx3, rcx1, r8_9);
    if (al12) 
        goto 0x5f40;
    goto 0x61b0;
}

void fun_7228() {
    goto 0x5f40;
}

void fun_a57c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0xaf10;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0xa441;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0xa441;
    }
    if (v11) 
        goto 0xb273;
    if (r10_12 > r15_13) 
        goto addr_b2c3_8;
    addr_b2c8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0xb001;
    addr_b2c3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_b2c8_9;
}

void fun_cab0() {
    void** rbp1;
    int64_t v2;

    fun_35b0();
    fun_3880(rbp1, rbp1);
    goto v2;
}

void fun_6bee() {
    goto 0x673c;
}

struct s262 {
    signed char[80] pad80;
    int64_t f50;
};

struct s263 {
    signed char[88] pad88;
    int64_t f58;
};

struct s264 {
    signed char[72] pad72;
    int64_t f48;
};

struct s265 {
    signed char[96] pad96;
    int64_t f60;
};

struct s266 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_6d8d() {
    struct s262* r12_1;
    int64_t rax2;
    int64_t rbx3;
    struct s263* r12_4;
    struct s264* r12_5;
    int64_t rax6;
    int64_t* rbx7;
    struct s265* r12_8;
    struct s266* r12_9;

    r12_1->f50 = 0;
    rax2 = *reinterpret_cast<int64_t*>(rbx3 - 48);
    r12_4->f58 = 0;
    r12_5->f48 = rax2;
    rax6 = *rbx7;
    r12_8->f60 = 0;
    r12_9->f1c = *reinterpret_cast<int32_t*>(&rax6);
    goto 0x5f40;
}

struct s267 {
    signed char[8] pad8;
    int64_t f8;
};

struct s268 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_6edd() {
    struct s267* r12_1;
    int64_t rax2;
    int64_t* rbx3;
    struct s268* r12_4;

    r12_1->f8 = 0;
    rax2 = *rbx3;
    r12_4->f10 = *reinterpret_cast<int32_t*>(&rax2);
    goto 0x5f40;
}

void fun_7265() {
    goto 0x5f40;
}

void fun_cb08() {
    fun_35b0();
    goto 0xcad9;
}

struct s269 {
    signed char[216] pad216;
    int64_t fd8;
};

struct s270 {
    signed char[216] pad216;
    int64_t fd8;
};

void fun_6bf7() {
    struct s269* r12_1;
    struct s270* r12_2;

    r12_1->fd8 = r12_2->fd8 + 1;
    goto 0x6949;
}

void fun_6dbe() {
}

struct s271 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_6ef3() {
    struct s271* r12_1;
    int64_t rbx2;

    r12_1->f18 = *reinterpret_cast<int32_t*>(rbx2 - 56) + reinterpret_cast<int32_t>("ort");
    goto 0x5f40;
}

struct s272 {
    signed char[56] pad56;
    int64_t f38;
};

struct s273 {
    signed char[64] pad64;
    int64_t f40;
};

struct s274 {
    signed char[8] pad8;
    int64_t f8;
};

struct s275 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_726e() {
    struct s272* r12_1;
    int64_t rbx2;
    int64_t rax3;
    int64_t rbx4;
    struct s273* r12_5;
    int64_t rax6;
    struct s274* rbx7;
    struct s275* r12_8;

    r12_1->f38 = *reinterpret_cast<int64_t*>(rbx2 - 0x70);
    rax3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    r12_5->f40 = -rax3;
    if (rax3 == 0x8000000000000000) 
        goto 0x61b0;
    rax6 = rbx7->f8;
    r12_8->f28 = -rax6;
    if (rax6 != 0x8000000000000000) 
        goto 0x718c;
    goto 0x61b0;
}

void fun_cb40() {
    void** rbp1;
    int64_t v2;

    fun_35b0();
    fun_3880(rbp1, rbp1);
    goto v2;
}

struct s276 {
    signed char[184] pad184;
    int64_t fb8;
};

struct s277 {
    signed char[184] pad184;
    int64_t fb8;
};

struct s278 {
    signed char[225] pad225;
    signed char fe1;
};

void fun_6c20() {
    struct s276* r12_1;
    struct s277* r12_2;
    struct s278* r12_3;
    void** r12_4;
    int64_t rdx5;
    void** rcx6;
    void** r8_7;
    void** r9_8;

    r12_1->fb8 = r12_2->fb8 + 1;
    if (!r12_3->fe1) 
        goto 0x5f40;
    debug_print_current_time_part_0("J", r12_4, rdx5, rcx6, r8_7, r9_8, __return_address());
    goto 0x5f40;
}

struct s279 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_6f05() {
    struct s279* r12_1;
    int32_t* rbx2;

    r12_1->f18 = *rbx2 + reinterpret_cast<int32_t>("ort");
    goto 0x5f40;
}

void fun_cbb8() {
    fun_35b0();
    goto 0xcb7b;
}

struct s280 {
    signed char[24] pad24;
    int32_t f18;
};

struct s281 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_6f16() {
    int64_t rcx1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    uint32_t esi5;
    int64_t rbx6;
    int64_t r8_7;
    int64_t* rbx8;
    struct s17* r12_9;
    signed char al10;
    int32_t edx11;
    int64_t rax12;
    struct s280* r12_13;
    int64_t rbx14;
    struct s281* r12_15;

    rcx1 = *reinterpret_cast<int64_t*>(rbx2 - 40);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    esi5 = *reinterpret_cast<unsigned char*>(rbx6 - 56);
    r8_7 = *rbx8;
    al10 = time_zone_hhmm_isra_0(r12_9, *reinterpret_cast<signed char*>(&esi5), rdx3, rcx1, r8_7);
    if (!al10) 
        goto 0x61b0;
    edx11 = 0;
    rax12 = r12_13->f18 + *reinterpret_cast<int64_t*>(rbx14 - 0x70);
    *reinterpret_cast<unsigned char*>(&edx11) = __intrinsic();
    r12_15->f18 = *reinterpret_cast<int32_t*>(&rax12);
    if (rax12 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax12))) {
        edx11 = 1;
    }
    if (!edx11) 
        goto 0x5f40;
    goto 0x61b0;
}
