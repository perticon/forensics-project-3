undefined4 main(int param_1,undefined8 *param_2)

{
  char *pcVar1;
  byte *pbVar2;
  int iVar3;
  _IO_FILE *p_Var4;
  ushort **ppuVar5;
  long *plVar6;
  int *piVar7;
  ulong uVar8;
  ulong uVar9;
  undefined8 uVar10;
  uint uVar11;
  undefined8 *puVar12;
  ulong uVar13;
  ushort uVar14;
  long in_FS_OFFSET;
  char local_51;
  long local_50;
  undefined local_42;
  undefined local_41;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  convert_entire_line = 1;
LAB_00102724:
  do {
    iVar3 = getopt_long(param_1,param_2,shortopts,longopts,0);
    if (iVar3 == -1) {
      finalize_tab_stops();
      puVar12 = (undefined8 *)0x0;
      if (optind < param_1) {
        puVar12 = param_2 + optind;
      }
      set_file_list(puVar12);
      p_Var4 = (_IO_FILE *)next_file(0);
      if (p_Var4 != (_IO_FILE *)0x0) {
        plVar6 = &local_50;
LAB_00102838:
        local_50 = 0;
        uVar13 = 0;
        uVar14 = 1;
LAB_001028aa:
        pbVar2 = (byte *)p_Var4->_IO_read_ptr;
        if (pbVar2 < p_Var4->_IO_read_end) {
          p_Var4->_IO_read_ptr = (char *)(pbVar2 + 1);
          uVar11 = (uint)*pbVar2;
LAB_001028bf:
          if ((char)uVar14 == '\0') goto LAB_00102886;
          if (uVar11 == 9) goto LAB_00102999;
          if (uVar11 == 8) {
            uVar13 = (uVar13 - 1) + (ulong)(uVar13 == 0);
            local_50 = local_50 + -1 + (ulong)(local_50 == 0);
            goto LAB_001028f0;
          }
        }
        else {
          uVar11 = __uflow(p_Var4);
          if (-1 < (int)uVar11) goto LAB_001028bf;
          p_Var4 = (_IO_FILE *)next_file(p_Var4);
          if (p_Var4 != (_IO_FILE *)0x0) goto LAB_001028aa;
          if ((char)uVar14 == '\0') goto LAB_0010292f;
        }
        uVar13 = uVar13 + 1;
        if (uVar13 != 0) {
          uVar14 = (ushort)convert_entire_line;
          if (convert_entire_line != 0) goto LAB_0010287e;
          while( true ) {
            ppuVar5 = __ctype_b_loc();
            uVar14 = (*ppuVar5)[(int)uVar11] & 1;
LAB_0010287e:
            if ((int)uVar11 < 0) break;
LAB_00102886:
            do {
              pcVar1 = stdout->_IO_write_ptr;
              if (pcVar1 < stdout->_IO_write_end) {
                stdout->_IO_write_ptr = pcVar1 + 1;
                *pcVar1 = (char)uVar11;
LAB_001028a5:
                if (uVar11 == 10) goto LAB_00102838;
                goto LAB_001028aa;
              }
              iVar3 = __overflow(stdout,uVar11 & 0xff);
              if (-1 < iVar3) goto LAB_001028a5;
              plVar6 = (long *)dcgettext(0,"write error",5);
              piVar7 = __errno_location();
              error(1,*piVar7,plVar6);
LAB_00102999:
              uVar8 = get_next_tab_column(uVar13,plVar6,&local_51);
              uVar9 = uVar8;
              if (local_51 == '\0') goto LAB_001029df;
              if (uVar13 == 0xffffffffffffffff) {
                do {
                  uVar10 = dcgettext(0,"input line is too long",5);
                  uVar9 = error(1,0,uVar10);
LAB_001029df:
                } while (uVar9 < uVar13);
                do {
                  while( true ) {
                    uVar13 = uVar13 + 1;
                    if (uVar8 <= uVar13) goto LAB_00102a53;
                    pcVar1 = stdout->_IO_write_ptr;
                    if (stdout->_IO_write_end <= pcVar1) break;
                    stdout->_IO_write_ptr = pcVar1 + 1;
                    *pcVar1 = ' ';
                  }
                  iVar3 = __overflow(stdout,0x20);
                } while (-1 < iVar3);
                plVar6 = (long *)dcgettext(0,"write error",5);
                piVar7 = __errno_location();
                error(1,*piVar7,plVar6);
              }
              uVar13 = uVar13 + 1;
LAB_00102a53:
              uVar11 = 0x20;
LAB_001028f0:
              uVar14 = (ushort)convert_entire_line;
            } while (convert_entire_line != 0);
          }
          goto LAB_0010292f;
        }
        goto LAB_00102a67;
      }
LAB_0010292f:
      cleanup_file_list_stdin();
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        return exit_status;
      }
LAB_00102a8b:
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    if (iVar3 < 0x3a) {
      if (0x2f < iVar3) {
        if (optarg == 0) {
          local_42 = (undefined)iVar3;
          local_41 = 0;
          parse_tab_stops();
        }
        else {
          parse_tab_stops();
        }
        goto LAB_00102724;
      }
      if (iVar3 == -0x83) {
        version_etc(stdout,"expand","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 == -0x82) {
        iVar3 = usage(0);
LAB_001027b0:
        if (iVar3 == 0x74) {
          parse_tab_stops();
          goto LAB_00102724;
        }
      }
      usage(1);
LAB_00102a67:
      uVar10 = dcgettext(0,"input line is too long",5);
      error(1,0,uVar10);
      goto LAB_00102a8b;
    }
    if (iVar3 != 0x69) goto LAB_001027b0;
    convert_entire_line = 0;
  } while( true );
}