
int64_t fun_2420();

int64_t fun_2370(void** rdi, ...);

void* quotearg_buffer_restyled(void** rdi, void* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2420();
    if (r8d > 10) {
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x7800 + rax11 * 4) + 0x7800;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

uint32_t* fun_2380();

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_24b0();

struct s1 {
    void* f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2360(void** rdi);

void** xcharalloc(void* rdi, ...);

void fun_2440();

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    uint32_t* rax8;
    void** r15_9;
    uint32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s1* rbx16;
    uint32_t r15d17;
    void* rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void* rax23;
    void* rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x509f;
    rax8 = fun_2380();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x5e51]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x512b;
            fun_24b0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->pad40, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax23) + 1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb180) {
                fun_2360(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x51ba);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2440();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s2 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s2* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, int64_t rdx) {
    struct s2* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x779b);
            if (!reinterpret_cast<int1_t>(*rdi == 96)) {
                rax7 = reinterpret_cast<void**>(0x7794);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x779f);
            if (!reinterpret_cast<int1_t>(*rdi == 96)) {
                rax10 = reinterpret_cast<void**>(0x7790);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2343() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_2353() {
    __asm__("cli ");
    goto __uflow;
}

int64_t free = 0x2040;

void fun_2363() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2373() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2383() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2393() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23a3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23b3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_23c3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clearerr_unlocked = 0x20b0;

void fun_23d3() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x20c0;

void fun_23e3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_23f3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2403() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2413() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2423() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2433() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2443() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2453() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2463() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2150;

void fun_2473() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2160;

void fun_2483() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_2493() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2180;

void fun_24a3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2190;

void fun_24b3() {
    __asm__("cli ");
    goto memset;
}

int64_t strspn = 0x21a0;

void fun_24c3() {
    __asm__("cli ");
    goto strspn;
}

int64_t posix_fadvise = 0x21b0;

void fun_24d3() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x21c0;

void fun_24e3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21d0;

void fun_24f3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21e0;

void fun_2503() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21f0;

void fun_2513() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2200;

void fun_2523() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2210;

void fun_2533() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2220;

void fun_2543() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2230;

void fun_2553() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2240;

void fun_2563() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2250;

void fun_2573() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2260;

void fun_2583() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2270;

void fun_2593() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2280;

void fun_25a3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2290;

void fun_25b3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22a0;

void fun_25c3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22b0;

void fun_25d3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x22c0;

void fun_25e3() {
    __asm__("cli ");
    goto fopen;
}

int64_t __cxa_atexit = 0x22d0;

void fun_25f3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22e0;

void fun_2603() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22f0;

void fun_2613() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2300;

void fun_2623() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2310;

void fun_2633() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2320;

void fun_2643() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2330;

void fun_2653() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void* rdi);

void** fun_25a0(int64_t rdi, ...);

void fun_2400(int64_t rdi, int64_t rsi);

void fun_23e0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

unsigned char convert_entire_line = 0;

int32_t fun_2450(int64_t rdi, void** rsi, void** rdx, int64_t rcx);

void* optarg = reinterpret_cast<void*>(0);

void parse_tab_stops(void* rdi, void** rsi, void** rdx, int64_t rcx, ...);

int32_t usage();

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0x773a;

void version_etc(void** rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8);

int32_t fun_2600();

void finalize_tab_stops(int64_t rdi, void** rsi, void** rdx, int64_t rcx);

int32_t optind = 0;

void set_file_list(void** rdi, void** rsi, void** rdx, int64_t rcx);

void** next_file();

void cleanup_file_list_stdin(void** rdi, void** rsi, void** rdx, int64_t rcx);

int32_t exit_status = 0;

void** fun_2410();

void** fun_25c0();

int32_t fun_2470();

uint32_t fun_2350(void** rdi, void** rsi, void** rdx, int64_t rcx);

void** get_next_tab_column(void** rdi, void** rsi, void** rdx, int64_t rcx);

void*** fun_2650(void** rdi, ...);

int64_t fun_26a3(int32_t edi, void** rsi) {
    int32_t ebp3;
    void** rbx4;
    void* rsp5;
    void* rdi6;
    void** rax7;
    void** v8;
    void* r14_9;
    void* rsp10;
    int64_t rcx11;
    void** rdx12;
    void** rsi13;
    int64_t rdi14;
    int32_t eax15;
    void* rdi16;
    void* rdi17;
    void** rdi18;
    int64_t rax19;
    void** rdi20;
    void** rdi21;
    void** rax22;
    void* rsp23;
    void** rbp24;
    void** r13_25;
    void** r12_26;
    int64_t rax27;
    void* rdx28;
    void** r14_29;
    void** rax30;
    void** rbx31;
    void** rax32;
    int32_t eax33;
    void** rax34;
    uint32_t* rax35;
    void** rax36;
    void** rax37;
    uint32_t ebx38;
    int32_t eax39;
    void** rax40;
    uint32_t r15d41;
    void** rax42;
    uint32_t eax43;
    void** rax44;
    signed char v45;
    void*** rax46;

    __asm__("cli ");
    ebp3 = edi;
    rbx4 = rsi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    rdi6 = *rsi;
    rax7 = g28;
    v8 = rax7;
    r14_9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) + 22);
    set_program_name(rdi6);
    fun_25a0(6, 6);
    fun_2400("coreutils", "/usr/local/share/locale");
    fun_23e0("coreutils", "/usr/local/share/locale");
    atexit(0x3870, "/usr/local/share/locale");
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    convert_entire_line = 1;
    while (rcx11 = 0xab40, rdx12 = reinterpret_cast<void**>("it:0::1::2::3::4::5::6::7::8::9::"), rsi13 = rbx4, *reinterpret_cast<int32_t*>(&rdi14) = ebp3, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0, eax15 = fun_2450(rdi14, rsi13, "it:0::1::2::3::4::5::6::7::8::9::", 0xab40), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), eax15 != -1) {
        if (eax15 > 57) {
            addr_2790_4:
            if (eax15 == 0x69) {
                convert_entire_line = 0;
                continue;
            }
        } else {
            if (eax15 > 47) {
                rdi16 = optarg;
                if (!rdi16) {
                    parse_tab_stops(r14_9, rsi13, "it:0::1::2::3::4::5::6::7::8::9::", 0xab40);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                    continue;
                } else {
                    parse_tab_stops(reinterpret_cast<int64_t>(rdi16) - 1, rsi13, "it:0::1::2::3::4::5::6::7::8::9::", 0xab40);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                    continue;
                }
            }
            if (eax15 != 0xffffff7d) 
                goto addr_279e_11; else 
                goto addr_2755_12;
        }
        addr_27b0_13:
        if (eax15 != 0x74) 
            goto addr_2a5d_14;
        rdi17 = optarg;
        parse_tab_stops(rdi17, rsi13, rdx12, rcx11, rdi17, rsi13, rdx12, rcx11);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        continue;
        addr_279e_11:
        if (eax15 != 0xffffff7e) 
            goto addr_2a5d_14;
        eax15 = usage();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        goto addr_27b0_13;
        addr_2755_12:
        rdi18 = stdout;
        rcx11 = Version;
        rdx12 = reinterpret_cast<void**>("GNU coreutils");
        rsi13 = reinterpret_cast<void**>("expand");
        version_etc(rdi18, "expand", "GNU coreutils", rcx11, "David MacKenzie");
        eax15 = fun_2600();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
        goto addr_2790_4;
    }
    finalize_tab_stops(rdi14, rsi13, "it:0::1::2::3::4::5::6::7::8::9::", 0xab40);
    rax19 = optind;
    *reinterpret_cast<int32_t*>(&rdi20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
    if (*reinterpret_cast<int32_t*>(&rax19) < ebp3) {
        rdi20 = reinterpret_cast<void**>(rbx4 + rax19 * 8);
    }
    set_file_list(rdi20, rsi13, "it:0::1::2::3::4::5::6::7::8::9::", 0xab40);
    *reinterpret_cast<int32_t*>(&rdi21) = 0;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    rax22 = next_file();
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8);
    rbp24 = rax22;
    if (rax22) {
        r13_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 7);
        r12_26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 8);
        goto addr_2838_21;
    }
    addr_292f_22:
    cleanup_file_list_stdin(rdi21, rsi13, rdx12, 0xab40);
    *reinterpret_cast<int32_t*>(&rax27) = exit_status;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
    rdx28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rdx28) {
        return rax27;
    }
    addr_2a8b_24:
    fun_2440();
    addr_2a5d_14:
    usage();
    addr_2a67_25:
    fun_2410();
    fun_25c0();
    goto addr_2a8b_24;
    while (1) {
        if (reinterpret_cast<unsigned char>(r14_29) <= reinterpret_cast<unsigned char>(rax30)) {
            while (++r14_29, reinterpret_cast<unsigned char>(rbx31) > reinterpret_cast<unsigned char>(r14_29)) {
                rdi21 = stdout;
                rax32 = *reinterpret_cast<void***>(rdi21 + 40);
                if (reinterpret_cast<unsigned char>(rax32) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi21 + 48))) {
                    rdx12 = rax32 + 1;
                    *reinterpret_cast<void***>(rdi21 + 40) = rdx12;
                    *rax32 = reinterpret_cast<void*>(32);
                } else {
                    *reinterpret_cast<uint32_t*>(&rsi13) = 32;
                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                    eax33 = fun_2470();
                    if (eax33 < 0) 
                        goto addr_2a23_31;
                }
            }
            goto addr_2a53_32;
            addr_2a23_31:
            rax34 = fun_2410();
            r12_26 = rax34;
            rax35 = fun_2380();
            rdx12 = r12_26;
            *reinterpret_cast<int32_t*>(&rdi21) = 1;
            *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi13) = *rax35;
            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
            fun_25c0();
            goto addr_2a4f_33;
        }
        addr_29bb_35:
        rax36 = fun_2410();
        *reinterpret_cast<uint32_t*>(&rsi13) = 0;
        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi21) = 1;
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx12 = rax36;
        rax30 = fun_25c0();
        continue;
        while (1) {
            rdi21 = stdout;
            rax37 = *reinterpret_cast<void***>(rdi21 + 40);
            if (reinterpret_cast<unsigned char>(rax37) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi21 + 48))) {
                *reinterpret_cast<uint32_t*>(&rsi13) = reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(&ebx38));
                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                eax39 = fun_2470();
                if (eax39 < 0) {
                    rax40 = fun_2410();
                    r12_26 = rax40;
                    fun_2380();
                    fun_25c0();
                    goto addr_2999_39;
                }
            } else {
                rdx12 = rax37 + 1;
                *reinterpret_cast<void***>(rdi21 + 40) = rdx12;
                *rax37 = *reinterpret_cast<void**>(&ebx38);
            }
            if (ebx38 == 10) {
                addr_2838_21:
                *reinterpret_cast<int32_t*>(&r14_29) = 0;
                *reinterpret_cast<int32_t*>(&r14_29 + 4) = 0;
                r15d41 = 1;
            }
            do {
                rax42 = *reinterpret_cast<void***>(rbp24 + 8);
                if (reinterpret_cast<unsigned char>(rax42) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp24 + 16))) 
                    break;
                rdi21 = rbp24;
                eax43 = fun_2350(rdi21, rsi13, rdx12, 0xab40);
                ebx38 = eax43;
                if (reinterpret_cast<int32_t>(eax43) >= reinterpret_cast<int32_t>(0)) 
                    goto addr_28bf_44;
                rdi21 = rbp24;
                rax44 = next_file();
                rbp24 = rax44;
            } while (rax44);
            goto addr_2926_46;
            rdx12 = rax42 + 1;
            *reinterpret_cast<void***>(rbp24 + 8) = rdx12;
            ebx38 = reinterpret_cast<unsigned char>(*rax42);
            addr_28bf_44:
            if (!*reinterpret_cast<signed char*>(&r15d41)) 
                continue;
            if (ebx38 == 9) {
                addr_2999_39:
                rdx12 = r13_25;
                rsi13 = r12_26;
                rdi21 = r14_29;
                rax30 = get_next_tab_column(rdi21, rsi13, rdx12, 0xab40);
                rbx31 = rax30;
                if (!v45) 
                    break;
            } else {
                if (ebx38 != 8) {
                    addr_2850_50:
                    ++r14_29;
                    if (!r14_29) 
                        goto addr_2a67_25; else 
                        goto addr_285a_51;
                } else {
                    r14_29 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_29 + 0xffffffffffffffff) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r14_29) < reinterpret_cast<unsigned char>(1)));
                    goto addr_28f0_53;
                }
            }
            if (reinterpret_cast<int1_t>(r14_29 == 0xffffffffffffffff)) 
                goto addr_29bb_35;
            addr_2a4f_33:
            ++r14_29;
            addr_2a53_32:
            ebx38 = 32;
            addr_28f0_53:
            r15d41 = convert_entire_line;
            if (*reinterpret_cast<signed char*>(&r15d41)) 
                continue;
            goto addr_2867_56;
            addr_285a_51:
            r15d41 = convert_entire_line;
            if (*reinterpret_cast<signed char*>(&r15d41)) {
                addr_287e_57:
                if (reinterpret_cast<int32_t>(ebx38) < reinterpret_cast<int32_t>(0)) 
                    goto addr_292f_22; else 
                    continue;
            } else {
                addr_2867_56:
                rax46 = fun_2650(rdi21, rdi21);
                rdx12 = *rax46;
                r15d41 = static_cast<uint32_t>(reinterpret_cast<uint16_t>(*reinterpret_cast<void**>(rdx12 + ebx38 * 2))) & 1;
                goto addr_287e_57;
            }
            addr_2926_46:
            if (*reinterpret_cast<signed char*>(&r15d41)) 
                goto addr_2850_50; else 
                goto addr_292f_22;
        }
    }
}

int64_t __libc_start_main = 0;

void fun_2a93() {
    __asm__("cli ");
    __libc_start_main(0x26a0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_2340(int64_t rdi);

int64_t fun_2b33() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2340(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2b73() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_25b0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_24f0(void** rdi, void** rsi, int64_t rdx, void** rcx);

void emit_tab_list_info(void** rdi, void** rsi, int64_t rdx, void** rcx);

int32_t fun_2510(int64_t rdi);

int32_t fun_2390(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2620(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2b83(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    int32_t eax19;
    void** r13_20;
    void** rax21;
    void** rax22;
    int32_t eax23;
    void** rax24;
    void** rax25;
    void** rax26;
    int32_t eax27;
    void** rax28;
    void** r15_29;
    void** rax30;
    void** rax31;
    void** rax32;
    void** rdi33;
    void** r8_34;
    void** r9_35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2410();
            fun_25b0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2410();
            fun_24f0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2410();
            fun_24f0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2410();
            fun_24f0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2410();
            fun_24f0(rax14, r12_13, 5, rcx6);
            emit_tab_list_info(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2410();
            fun_24f0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2410();
            fun_24f0(rax18, r12_17, 5, rcx6);
            do {
                if (1) 
                    break;
                eax19 = fun_2510("expand");
            } while (eax19);
            r13_20 = v4;
            if (!r13_20) {
                rax21 = fun_2410();
                fun_25b0(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax22 = fun_25a0(5);
                if (!rax22 || (eax23 = fun_2390(rax22, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax23)) {
                    rax24 = fun_2410();
                    r13_20 = reinterpret_cast<void**>("expand");
                    fun_25b0(1, rax24, "https://www.gnu.org/software/coreutils/", "expand");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_20 = reinterpret_cast<void**>("expand");
                    goto addr_2f00_9;
                }
            } else {
                rax25 = fun_2410();
                fun_25b0(1, rax25, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax26 = fun_25a0(5);
                if (!rax26 || (eax27 = fun_2390(rax26, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax27)) {
                    addr_2e06_11:
                    rax28 = fun_2410();
                    fun_25b0(1, rax28, "https://www.gnu.org/software/coreutils/", "expand");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_20 == "expand")) {
                        r12_2 = reinterpret_cast<void**>(0x7bc1);
                    }
                } else {
                    addr_2f00_9:
                    r15_29 = stdout;
                    rax30 = fun_2410();
                    fun_24f0(rax30, r15_29, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_2e06_11;
                }
            }
            rax31 = fun_2410();
            rcx6 = r12_2;
            fun_25b0(1, rax31, r13_20, rcx6);
            addr_2bde_14:
            fun_2600();
        }
    } else {
        rax32 = fun_2410();
        rdi33 = stderr;
        rcx6 = r12_2;
        fun_2620(rdi33, 1, rax32, rcx6, r8_34, r9_35, v36, v37, v38, v39, v40, v41);
        goto addr_2bde_14;
    }
}

uint64_t first_free_tab = 0;

void*** tab_list = reinterpret_cast<void***>(0);

uint64_t n_tabs_allocated = 0;

void*** x2nrealloc();

void** max_column_width = reinterpret_cast<void**>(0);

void fun_2f33(void** rdi) {
    uint64_t rdx2;
    void*** rdi3;
    uint64_t rax4;
    void** rbp5;
    void* rcx6;
    void*** rax7;
    int1_t cf8;

    __asm__("cli ");
    rdx2 = first_free_tab;
    rdi3 = tab_list;
    rax4 = n_tabs_allocated;
    if (!rdx2) {
        rbp5 = rdi;
    } else {
        rcx6 = reinterpret_cast<void*>(rdx2 * 8);
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<int64_t>(rdi3) + reinterpret_cast<uint64_t>(rcx6) - 8)) <= reinterpret_cast<unsigned char>(rdi)) {
            rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<int64_t>(rdi3) + reinterpret_cast<uint64_t>(rcx6) - 8)));
        } else {
            if (rdx2 == rax4) {
                *reinterpret_cast<int32_t*>(&rbp5) = 0;
                *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
                goto addr_2fc2_7;
            } else {
                *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rdi3) + reinterpret_cast<uint64_t>(rcx6)) = rdi;
                first_free_tab = rdx2 + 1;
                return;
            }
        }
    }
    if (rdx2 == rax4) {
        addr_2fc2_7:
        rax7 = x2nrealloc();
        rdx2 = first_free_tab;
        tab_list = rax7;
        rdi3 = rax7;
        goto addr_2f90_10;
    } else {
        addr_2f90_10:
        rdi3[rdx2 * 8] = rdi;
        cf8 = reinterpret_cast<unsigned char>(max_column_width) < reinterpret_cast<unsigned char>(rbp5);
        first_free_tab = rdx2 + 1;
        if (cf8) {
            max_column_width = rbp5;
        }
    }
    return;
}

void** extend_size = reinterpret_cast<void**>(0);

void add_tab_stop(void** rdi);

void** increment_size = reinterpret_cast<void**>(0);

int64_t quote(void** rdi, ...);

void* fun_24c0(void** rdi, int64_t rsi);

void** ximemdup0(void** rdi, void* rsi);

void fun_2ff3(void** rdi) {
    uint32_t r13d2;
    uint32_t r15d3;
    signed char v4;
    void** r14_5;
    int32_t r12d6;
    signed char v7;
    void** rbp8;
    int32_t ebx9;
    void** r13_10;
    void*** rax11;
    int64_t rax12;
    void** rax13;
    int1_t zf14;
    int1_t zf15;
    uint32_t edx16;
    void** rax17;
    void* rax18;
    void** rax19;
    int1_t zf20;
    int1_t zf21;

    __asm__("cli ");
    r13d2 = reinterpret_cast<unsigned char>(*rdi);
    if (!*reinterpret_cast<signed char*>(&r13d2)) 
        goto addr_311c_2;
    r15d3 = r13d2;
    v4 = 1;
    *reinterpret_cast<int32_t*>(&r14_5) = 0;
    *reinterpret_cast<int32_t*>(&r14_5 + 4) = 0;
    r12d6 = 0;
    v7 = 0;
    *reinterpret_cast<int32_t*>(&rbp8) = 0;
    *reinterpret_cast<int32_t*>(&rbp8 + 4) = 0;
    ebx9 = 0;
    r13_10 = rdi;
    do {
        addr_308e_4:
        if (*reinterpret_cast<unsigned char*>(&r15d3) == 44 || (rax11 = fun_2650(rdi), *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<unsigned char*>(&r15d3), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0, !!(reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*rax11) + reinterpret_cast<uint64_t>(rax12 * 2))) & 1))) {
            if (!*reinterpret_cast<signed char*>(&ebx9)) {
                addr_3150_6:
                rax13 = r13_10;
                continue;
            } else {
                if (*reinterpret_cast<signed char*>(&r12d6)) {
                    zf14 = extend_size == 0;
                    if (!zf14) 
                        goto addr_3298_9;
                    extend_size = rbp8;
                    rax13 = r13_10;
                    ebx9 = 0;
                    continue;
                } else {
                    if (!v7) {
                        rdi = rbp8;
                        ebx9 = 0;
                        add_tab_stop(rdi);
                        rax13 = r13_10;
                        continue;
                    } else {
                        zf15 = increment_size == 0;
                        if (!zf15) 
                            goto addr_3260_14;
                        increment_size = rbp8;
                        ebx9 = 0;
                        r15d3 = reinterpret_cast<unsigned char>(r13_10[1]);
                        ++r13_10;
                        if (*reinterpret_cast<unsigned char*>(&r15d3)) 
                            goto addr_308e_4; else 
                            goto addr_30d8_16;
                    }
                }
            }
        } else {
            if (*reinterpret_cast<unsigned char*>(&r15d3) == 47) {
                if (*reinterpret_cast<signed char*>(&ebx9)) {
                    quote(r13_10, r13_10);
                    fun_2410();
                    *reinterpret_cast<int32_t*>(&rdi) = 0;
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    r12d6 = ebx9;
                    fun_25c0();
                    v4 = 0;
                    rax13 = r13_10;
                    v7 = 0;
                    continue;
                } else {
                    v7 = 0;
                    rax13 = r13_10;
                    r12d6 = 1;
                    continue;
                }
            } else {
                if (*reinterpret_cast<unsigned char*>(&r15d3) == 43) {
                    if (*reinterpret_cast<signed char*>(&ebx9)) {
                        quote(r13_10, r13_10);
                        fun_2410();
                        *reinterpret_cast<int32_t*>(&rdi) = 0;
                        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                        r12d6 = 0;
                        fun_25c0();
                        v7 = *reinterpret_cast<signed char*>(&ebx9);
                        rax13 = r13_10;
                        v4 = 0;
                        continue;
                    } else {
                        v7 = 1;
                        r12d6 = 0;
                        rax13 = r13_10;
                        continue;
                    }
                } else {
                    edx16 = *reinterpret_cast<signed char*>(&r15d3) - 48;
                    if (edx16 > 9) 
                        goto addr_338d_26;
                    if (!*reinterpret_cast<signed char*>(&ebx9)) 
                        goto addr_3071_28;
                }
            }
        }
        if (reinterpret_cast<unsigned char>(rbp8) > reinterpret_cast<unsigned char>(0x1999999999999999) || (rax17 = reinterpret_cast<void**>(reinterpret_cast<int32_t>(edx16) + reinterpret_cast<uint64_t>(rbp8 + reinterpret_cast<unsigned char>(rbp8) * 4) * 2), reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(rbp8))) {
            rax18 = fun_24c0(r14_5, "0123456789");
            rax19 = ximemdup0(r14_5, rax18);
            quote(rax19, rax19);
            fun_2410();
            fun_25c0();
            rdi = rax19;
            fun_2360(rdi);
            v4 = 0;
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<uint64_t>(rax18) + 0xffffffffffffffff);
            continue;
        } else {
            rbp8 = rax17;
            goto addr_3150_6;
        }
        addr_3071_28:
        rbp8 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(edx16)));
        r14_5 = r13_10;
        rax13 = r13_10;
        ebx9 = 1;
        r15d3 = reinterpret_cast<unsigned char>(rax13[1]);
        r13_10 = rax13 + 1;
    } while (*reinterpret_cast<unsigned char*>(&r15d3));
    addr_30e0_33:
    if (!*reinterpret_cast<signed char*>(&ebx9)) {
        if (v4) {
            addr_311c_2:
            return;
        } else {
            goto addr_3288_36;
        }
    }
    if (!v4) {
        while (1) {
            addr_3288_36:
            fun_2600();
            addr_3298_9:
            fun_2410();
            fun_25c0();
            extend_size = rbp8;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&r12d6)) {
            zf20 = extend_size == 0;
            if (!zf20) 
                goto addr_3298_9;
            extend_size = rbp8;
            return;
        }
        if (!v7) {
            goto add_tab_stop;
        }
        zf21 = increment_size == 0;
        if (zf21) 
            goto addr_3115_44;
    }
    addr_3260_14:
    fun_2410();
    fun_25c0();
    increment_size = rbp8;
    goto addr_3288_36;
    addr_3115_44:
    increment_size = rbp8;
    goto addr_311c_2;
    addr_30d8_16:
    goto addr_30e0_33;
    addr_338d_26:
    quote(r13_10);
    fun_2410();
    fun_25c0();
    goto addr_3288_36;
}

void** tab_size = reinterpret_cast<void**>(0);

void fun_33e3() {
    uint64_t rsi1;
    void*** rdi2;
    void** rdx3;
    void** rax4;
    void** rax5;
    uint64_t rdx6;
    void** rcx7;
    void** rax8;
    void** rdx9;
    int1_t zf10;
    void** rax11;
    uint64_t rdx12;

    __asm__("cli ");
    rsi1 = first_free_tab;
    rdi2 = tab_list;
    if (!rsi1) {
        while (rdx3 = increment_size, rax4 = extend_size, !!rdx3) {
            addr_350a_3:
            if (!rax4) 
                goto addr_3513_4;
            addr_3462_5:
            fun_2410();
            fun_25c0();
        }
    } else {
        *reinterpret_cast<int32_t*>(&rax5) = 0;
        *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
        do {
            rcx7 = rax5;
            rax5 = rdi2[rdx6 * 8];
            if (!rax5) 
                goto addr_34e6_8;
            if (reinterpret_cast<unsigned char>(rcx7) >= reinterpret_cast<unsigned char>(rax5)) 
                goto addr_34c2_10;
            ++rdx6;
        } while (rsi1 != rdx6);
        goto addr_342a_12;
    }
    if (!rax4) {
        rax4 = reinterpret_cast<void**>(8);
    }
    addr_34af_15:
    max_column_width = rax4;
    tab_size = rax4;
    return;
    addr_3513_4:
    rax4 = rdx3;
    goto addr_34af_15;
    addr_34e6_8:
    rax8 = fun_2410();
    rdx3 = rax8;
    rax4 = fun_25c0();
    goto addr_350a_3;
    addr_34c2_10:
    fun_2410();
    fun_25c0();
    goto addr_34e6_8;
    addr_342a_12:
    rdx9 = increment_size;
    if (!rdx9) 
        goto addr_3440_16;
    zf10 = extend_size == 0;
    if (!zf10) 
        goto addr_3462_5;
    addr_3440_16:
    *reinterpret_cast<int32_t*>(&rax11) = 0;
    *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    if (rsi1 == 1 && (rdx12 = reinterpret_cast<unsigned char>(rdx9) | reinterpret_cast<unsigned char>(extend_size), !rdx12)) {
        rax11 = *rdi2;
    }
    tab_size = rax11;
    return;
}

void** fun_3523(void** rdi, uint64_t* rsi, signed char* rdx) {
    void** rcx4;
    void** rax5;
    uint64_t rcx6;
    uint64_t rdi7;
    void*** r9_8;
    void** r8_9;
    void** r8_10;
    void*** rdx11;

    __asm__("cli ");
    rcx4 = tab_size;
    *rdx = 0;
    rax5 = rdi;
    if (rcx4) 
        goto addr_3570_2;
    rcx6 = *rsi;
    rdi7 = first_free_tab;
    r9_8 = tab_list;
    if (rdi7 > rcx6) {
        do {
            r8_9 = r9_8[rcx6 * 8];
            if (reinterpret_cast<unsigned char>(rax5) < reinterpret_cast<unsigned char>(r8_9)) 
                break;
            ++rcx6;
            *rsi = rcx6;
        } while (rcx6 != rdi7);
        goto addr_3580_6;
    } else {
        goto addr_3580_6;
    }
    addr_3565_8:
    return r8_9;
    addr_3580_6:
    rcx4 = extend_size;
    if (rcx4) {
        addr_3570_2:
        return reinterpret_cast<unsigned char>(rcx4) + reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax5) % reinterpret_cast<unsigned char>(rcx4));
    } else {
        r8_10 = increment_size;
        if (r8_10) {
            rdx11 = tab_list;
            r8_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_10) + reinterpret_cast<unsigned char>(rax5) - (reinterpret_cast<unsigned char>(rax5) - *reinterpret_cast<uint64_t*>(rdx11 + rdi7 * 8 - 8)) % reinterpret_cast<unsigned char>(r8_10));
            goto addr_3565_8;
        } else {
            *rdx = 1;
            return r8_10;
        }
    }
}

struct s3 {
    signed char f0;
    signed char f1;
};

signed char have_read_stdin = 0;

struct s3** file_list = reinterpret_cast<struct s3**>(0);

struct s3** fun_35c3(struct s3** rdi) {
    __asm__("cli ");
    have_read_stdin = 0;
    if (!rdi) {
        rdi = reinterpret_cast<struct s3**>("(w");
    }
    file_list = rdi;
    return "(w";
}

/* prev_file.1 */
struct s3* prev_file_1 = reinterpret_cast<struct s3*>(0);

void fun_24a0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t rpl_fclose(unsigned char* rdi);

void fun_23d0(unsigned char* rdi);

int64_t quotearg_n_style_colon();

unsigned char* fun_25e0(struct s3* rdi, int64_t rsi);

unsigned char* stdin = reinterpret_cast<unsigned char*>(0);

void fadvise(unsigned char* rdi, int64_t rsi);

unsigned char* fun_35f3(unsigned char* rdi) {
    struct s3* r13_2;
    uint32_t* rax3;
    uint32_t r12d4;
    int64_t rax5;
    struct s3** rax6;
    struct s3* r12_7;
    unsigned char* rax8;
    struct s3** rax9;

    __asm__("cli ");
    if (rdi) {
        r13_2 = prev_file_1;
        if (!r13_2) {
            fun_24a0("prev_file", "src/expand-common.c", 0x154, "next_file");
        } else {
            rax3 = fun_2380();
            r12d4 = *rax3;
            if (!(*rdi & 32)) {
                r12d4 = 0;
            }
            if (r13_2->f0 != 45 || r13_2->f1) {
                rax5 = rpl_fclose(rdi);
                if (*reinterpret_cast<int32_t*>(&rax5)) {
                    r12d4 = *rax3;
                }
            } else {
                fun_23d0(rdi);
            }
            if (r12d4) {
                quotearg_n_style_colon();
                fun_25c0();
                exit_status = 1;
            }
        }
    }
    rax6 = file_list;
    r12_7 = *rax6;
    file_list = rax6 + 1;
    if (!r12_7) {
        addr_3760_13:
        return 0;
    } else {
        do {
            if (r12_7->f0 != 45 || r12_7->f1) {
                rax8 = fun_25e0(r12_7, "r");
                if (rax8) 
                    break;
            } else {
                rax8 = stdin;
                have_read_stdin = 1;
                if (rax8) 
                    break;
            }
            quotearg_n_style_colon();
            fun_2380();
            fun_25c0();
            rax9 = file_list;
            exit_status = 1;
            r12_7 = *rax9;
            file_list = rax9 + 1;
        } while (r12_7);
        goto addr_3760_13;
    }
    prev_file_1 = r12_7;
    fadvise(rax8, 2);
    return rax8;
}

void fun_37b3() {
    int1_t zf1;
    unsigned char* rdi2;
    int64_t rax3;

    __asm__("cli ");
    zf1 = have_read_stdin == 0;
    if (!zf1) {
        rdi2 = stdin;
        rax3 = rpl_fclose(rdi2);
        if (*reinterpret_cast<int32_t*>(&rax3)) {
            fun_2380();
            fun_25c0();
        } else {
            return;
        }
    } else {
        return;
    }
}

void fun_3803() {
    void** rbp1;
    void** rax2;
    void** rcx3;

    __asm__("cli ");
    rbp1 = stdout;
    rax2 = fun_2410();
    fun_24f0(rax2, rbp1, 5, rcx3);
    fun_2410();
    goto fun_24f0;
}

int64_t file_name = 0;

void fun_3853(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3863(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_23a0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3873() {
    void** rdi1;
    int32_t eax2;
    uint32_t* rax3;
    int1_t zf4;
    uint32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2380(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2410();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3903_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25c0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23a0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3903_5:
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25c0();
    }
}

void fun_3923() {
    __asm__("cli ");
}

struct s4 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2540(struct s4* rdi);

void fun_3933(struct s4* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2540(rdi);
        goto 0x24d0;
    }
}

int32_t fun_2580(struct s4* rdi);

int64_t fun_2490(int64_t rdi, ...);

int32_t rpl_fflush(struct s4* rdi);

int64_t fun_23f0(struct s4* rdi);

int64_t fun_3963(struct s4* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    uint32_t* rax8;
    uint32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2540(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2580(rdi);
        if (!(eax3 && (eax4 = fun_2540(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2490(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2380();
            r12d9 = *rax8;
            rax10 = fun_23f0(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_23f0;
}

void rpl_fseeko(struct s4* rdi);

void fun_39f3(struct s4* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2580(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_3a43(struct s4* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2540(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2490(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_2610(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s5 {
    signed char[1] pad1;
    void* f1;
    signed char[2] pad4;
    void* f4;
};

struct s5* fun_2480();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_3ac3(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s5* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2610("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2370("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2480();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2390(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void**>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(r12_5[1] == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5263(int64_t rdi) {
    int64_t rbp2;
    uint32_t* rax3;
    uint32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2380();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xb280;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_52a3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb280);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_52c3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb280);
    }
    *rdi = esi;
    return 0xb280;
}

int64_t fun_52e3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb280);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5323(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xb280);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_5343(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xb280);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x266a;
    if (!rdx) 
        goto 0x266a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb280;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void* fun_5383(void** rdi, void* rsi, int64_t rdx, int64_t rcx, struct s8* r8) {
    struct s8* rbx6;
    uint32_t* rax7;
    uint32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xb280);
    }
    rax7 = fun_2380();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->pad40, v12, v10, 0x53b6);
    *rax7 = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5403(int64_t rdi, int64_t rsi, void** rdx, struct s9* rcx) {
    struct s9* rbx5;
    uint32_t* rax6;
    uint32_t r9d7;
    void* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    uint32_t v11;
    int64_t v12;
    int64_t v13;
    void* rax14;
    void* rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xb280);
    }
    rax6 = fun_2380();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void*>(&rbx5->pad40);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5431);
    rsi15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax14) + 1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x548c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_54f3() {
    __asm__("cli ");
}

void** gb098 = reinterpret_cast<void**>(0x80);

int64_t slotvec0 = 0x100;

void fun_5503() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2360(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xb180) {
        fun_2360(rdi7);
        gb098 = reinterpret_cast<void**>(0xb180);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb090) {
        fun_2360(r12_2);
        slotvec = reinterpret_cast<void**>(0xb090);
    }
    nslots = 1;
    return;
}

void fun_55a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_55c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_55d3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_55f3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5613(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2670;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

void** fun_56a3(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2675;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

void** fun_5733(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x267a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2440();
    } else {
        return rax5;
    }
}

void** fun_57c3(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x267f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

void** fun_5853(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5a20]");
    __asm__("movdqa xmm1, [rip+0x5a28]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5a11]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2440();
    } else {
        return rax10;
    }
}

void** fun_58f3(int64_t rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5980]");
    __asm__("movdqa xmm1, [rip+0x5988]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5971]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2440();
    } else {
        return rax9;
    }
}

void** fun_5993(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x58e0]");
    __asm__("movdqa xmm1, [rip+0x58e8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x58c9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2440();
    } else {
        return rax3;
    }
}

void** fun_5a23(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5850]");
    __asm__("movdqa xmm1, [rip+0x5858]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x5846]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2440();
    } else {
        return rax4;
    }
}

void** fun_5ab3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2684;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

void** fun_5b53(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x571a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5712]");
    __asm__("movdqa xmm2, [rip+0x571a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2689;
    if (!rdx) 
        goto 0x2689;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

void** fun_5bf3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x567a]");
    __asm__("movdqa xmm1, [rip+0x5682]");
    __asm__("movdqa xmm2, [rip+0x568a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x268e;
    if (!rdx) 
        goto 0x268e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2440();
    } else {
        return rax9;
    }
}

void** fun_5ca3(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x55ca]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x55c2]");
    __asm__("movdqa xmm2, [rip+0x55ca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2693;
    if (!rsi) 
        goto 0x2693;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

void** fun_5d43(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x552a]");
    __asm__("movdqa xmm1, [rip+0x5532]");
    __asm__("movdqa xmm2, [rip+0x553a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2698;
    if (!rsi) 
        goto 0x2698;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

void fun_5de3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5df3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5e13() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5e33(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s10 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2520(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_5e53(void** rdi, void** rsi, void** rdx, void** rcx, struct s10* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2620(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2620(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2410();
    fun_2620(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2520(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2410();
    fun_2620(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2520(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2410();
        fun_2620(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7e68 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7e68;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_62c3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_62e3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2440();
    } else {
        return;
    }
}

void fun_6383(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6426_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6430_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2440();
    } else {
        return;
    }
    addr_6426_5:
    goto addr_6430_7;
}

void fun_6463() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2520(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2410();
    fun_25b0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2410();
    fun_25b0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2410();
    goto fun_25b0;
}

int64_t fun_23c0();

void xalloc_die();

void fun_6503(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2550(void** rdi);

void fun_6543(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2550(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6563(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2550(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6583(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2550(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2590();

void fun_65a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2590();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_65d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2590();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6603(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6643() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6683(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_66b3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6703(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23c0();
            if (rax5) 
                break;
            addr_674d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_674d_5;
        rax8 = fun_23c0();
        if (rax8) 
            goto addr_6736_9;
        if (rbx4) 
            goto addr_674d_5;
        addr_6736_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6793(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23c0();
            if (rax8) 
                break;
            addr_67da_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_67da_5;
        rax11 = fun_23c0();
        if (rax11) 
            goto addr_67c2_9;
        if (!rbx6) 
            goto addr_67c2_9;
        if (r12_4) 
            goto addr_67da_5;
        addr_67c2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6823(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_68cd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_68e0_10:
                *r12_8 = 0;
            }
            addr_6880_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_68a6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_68f4_14;
            if (rcx10 <= rsi9) 
                goto addr_689d_16;
            if (rsi9 >= 0) 
                goto addr_68f4_14;
            addr_689d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_68f4_14;
            addr_68a6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2590();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_68f4_14;
            if (!rbp13) 
                break;
            addr_68f4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_68cd_9;
        } else {
            if (!r13_6) 
                goto addr_68e0_10;
            goto addr_6880_11;
        }
    }
}

int64_t fun_2500();

void fun_6923() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6953() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6983() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_69a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2530(signed char* rdi, void** rsi, void** rdx);

void fun_69c3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2550(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2530;
    }
}

void fun_6a03(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2550(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2530;
    }
}

struct s12 {
    signed char[1] pad1;
    void* f1;
};

void fun_6a43(int64_t rdi, struct s12* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2550(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2530;
    }
}

void** fun_2430(void** rdi, ...);

void fun_6a83(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2430(rdi);
    rax3 = fun_2550(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2530;
    }
}

void fun_6ac3() {
    void** rdi1;

    __asm__("cli ");
    fun_2410();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_25c0();
    fun_2370(rdi1);
}

int64_t fun_23b0();

int64_t fun_6b03(unsigned char* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    uint32_t* rax5;
    uint32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23b0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_6b5e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2380();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_6b5e_3;
            rax6 = fun_2380();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

signed char* fun_2570(int64_t rdi);

signed char* fun_6b73() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2570(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2460(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6bb3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2460(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2440();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6c43() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax3;
    }
}

int64_t fun_6cc3(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25a0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2430(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2530(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2530(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6d73() {
    __asm__("cli ");
    goto fun_25a0;
}

void fun_6d83() {
    __asm__("cli ");
}

void fun_6d97() {
    __asm__("cli ");
    return;
}

uint32_t fun_24e0(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, unsigned char* rsi);

int32_t fun_2640(int64_t rdi, unsigned char* rsi);

uint32_t fun_2630(void** rdi, unsigned char* rsi);

void fun_3cf5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rax10;
    void** r11_11;
    void** v12;
    int32_t ebp13;
    void** rax14;
    void* r15_15;
    int32_t ebx16;
    void** rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void* rsi30;
    void* v31;
    void** v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rdx44;
    void** rcx45;
    uint32_t edx46;
    unsigned char al47;
    void** v48;
    int64_t v49;
    void** v50;
    void** v51;
    void** rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    void** v61;
    unsigned char v62;
    void* v63;
    void* v64;
    void** v65;
    signed char* v66;
    void** r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    void** r13_72;
    unsigned char* rsi73;
    void* v74;
    void** r15_75;
    unsigned char* rdx76;
    void* v77;
    int64_t rax78;
    int64_t rdi79;
    int32_t v80;
    int32_t eax81;
    void* rdi82;
    uint32_t edx83;
    unsigned char v84;
    void* rdi85;
    void* v86;
    uint32_t esi87;
    uint32_t ebp88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    void* rdx95;
    void* rcx96;
    void* v97;
    void*** rax98;
    uint1_t zf99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    int64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    int64_t rax111;
    int64_t rax112;
    uint32_t r12d113;
    void* v114;
    void* rdx115;
    void* rax116;
    void* v117;
    uint64_t rax118;
    int64_t v119;
    int64_t rax120;
    int64_t rax121;
    int64_t rax122;
    int64_t v123;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2410();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_2410();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*rdx17), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2430(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*rbx41);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3ff3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3ff3_22; else 
                            goto addr_43ed_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_44ad_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4800_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*rbx41);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3ff0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3ff0_30; else 
                                goto addr_4819_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2430(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4800_28;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24e0(rdi39, v26, v28, rcx45);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4800_28; else 
                            goto addr_3e9c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4960_39:
                    *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_47e0_40:
                        if (r11_27 == 1) {
                            addr_436d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_4928_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx45);
                                goto addr_3fa7_44;
                            }
                        } else {
                            goto addr_47f0_46;
                        }
                    } else {
                        addr_496f_47:
                        rax24 = v48;
                        if (!rax24[1]) {
                            goto addr_436d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3ff3_22:
                                if (v49 != 1) {
                                    addr_4549_52:
                                    v50 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_2430(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_4594_54;
                                    }
                                } else {
                                    goto addr_4000_56;
                                }
                            } else {
                                addr_3fa5_57:
                                ebp36 = 0;
                                goto addr_3fa7_44;
                            }
                        } else {
                            addr_47d4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_496f_47; else 
                                goto addr_47de_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_436d_41;
                        if (v49 == 1) 
                            goto addr_4000_56; else 
                            goto addr_4549_52;
                    }
                }
                addr_4061_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<void**>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3ef8_63:
                    if (!1 && (edx54 = *reinterpret_cast<uint32_t*>(&rcx45), *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<unsigned char*>(&rcx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3f1d_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_4220_65;
                    } else {
                        addr_4089_66:
                        ++r9_37;
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_48d8_67;
                    }
                } else {
                    goto addr_4080_69;
                }
                addr_3f31_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                ++r9_37;
                addr_3f7c_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_48d8_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_3f7c_81;
                }
                addr_4080_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3f1d_64; else 
                    goto addr_4089_66;
                addr_3fa7_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_405f_91;
                if (v22) 
                    goto addr_3fbf_93;
                addr_405f_91:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4061_62;
                addr_4594_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx45 = r12_67;
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4d1b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4d8b_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72)) + 1);
                        rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = *rdx76 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_4b8f_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    *reinterpret_cast<int32_t*>(&rdi79) = v80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
                    eax81 = fun_2640(rdi79, rsi73);
                    if (!eax81) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2630(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx83 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx83) & reinterpret_cast<unsigned char>(v32));
                addr_468e_109:
                if (reinterpret_cast<uint64_t>(rdi82) <= 1) {
                    addr_404c_110:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        edx83 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_4698_112;
                    }
                } else {
                    addr_4698_112:
                    v84 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi85 = v86;
                    esi87 = 0;
                    ebp88 = reinterpret_cast<unsigned char>(v22);
                    rcx45 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi82) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4769_114;
                }
                addr_4058_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_405f_91;
                while (1) {
                    addr_4769_114:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        *reinterpret_cast<unsigned char*>(&esi87) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax89 = esi87;
                        if (*reinterpret_cast<signed char*>(&ebp88)) 
                            goto addr_4c77_117;
                        eax90 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) & *reinterpret_cast<unsigned char*>(&esi87));
                        if (*reinterpret_cast<unsigned char*>(&eax90)) 
                            goto addr_46d6_119;
                    } else {
                        eax57 = (esi87 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                            goto addr_4c85_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_4757_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_4757_128;
                        }
                    }
                    addr_4705_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax91 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax91) >> 6);
                        eax92 = eax91 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax92);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax93 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax93) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax93) >> 3);
                        eax94 = (eax93 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax94);
                    }
                    ++r9_37;
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                        break;
                    esi87 = edx83;
                    addr_4757_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi85) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_46d6_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax90;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_4705_134;
                }
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_3f7c_81;
                addr_4c85_125:
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_48d8_67;
                addr_4d1b_96:
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx83 = reinterpret_cast<unsigned char>(v32);
                goto addr_468e_109;
                addr_4d8b_98:
                r11_27 = v65;
                rdi82 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx95 = rdi82;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx96 = v97;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx96) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx95 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx95) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx95));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi82 = rdx95;
                }
                edx83 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_468e_109;
                addr_4000_56:
                rax98 = fun_2650(rdi39, rdi39);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi82) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf99 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax98 + reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf99);
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf99) & reinterpret_cast<unsigned char>(v32));
                goto addr_404c_110;
                addr_47de_59:
                goto addr_47e0_40;
                addr_44ad_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3ff3_22;
                *reinterpret_cast<uint32_t*>(&rcx45) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx45));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_4058_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3fa5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3ff3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_44f2_160;
                if (!v22) 
                    goto addr_48c7_162; else 
                    goto addr_4ad3_163;
                addr_44f2_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_48c7_162:
                    ++r9_37;
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    goto addr_48d8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!v32) 
                        goto addr_439b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                addr_4203_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_3f31_70; else 
                    goto addr_4217_169;
                addr_439b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3ef8_63;
                goto addr_4080_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = 0;
                        goto addr_47d4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_490f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3ff0_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_3ee8_178; else 
                        goto addr_4892_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_47d4_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3ff3_22;
                }
                addr_490f_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3ff0_30:
                    r8d42 = 0;
                    goto addr_3ff3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4061_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        goto addr_4928_42;
                    }
                }
                addr_3ee8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_3ef8_63;
                addr_4892_179:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_47f0_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4061_62;
                } else {
                    addr_48a2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3ff3_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_5052_188;
                if (v28) 
                    goto addr_48c7_162;
                addr_5052_188:
                *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                goto addr_4203_168;
                addr_3e9c_37:
                if (v22) 
                    goto addr_4e93_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*rbx41);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3eb3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4960_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_49eb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3ff3_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_3ee8_178; else 
                        goto addr_49c7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_47d4_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3ff3_22;
                }
                addr_49eb_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3ff3_22;
                }
                addr_49c7_199:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_47f0_46;
                goto addr_48a2_186;
                addr_3eb3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3ff3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3ff3_22; else 
                    goto addr_3ec4_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx45) = reinterpret_cast<uint1_t>(r15_15 == 0);
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (*reinterpret_cast<unsigned char*>(&rcx45) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_4f9e_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_4e24_210;
            if (1) 
                goto addr_4e22_212;
            if (!v29) 
                goto addr_4a5e_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v49 = rax108;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_4f91_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4220_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_3fdb_219; else 
            goto addr_423a_220;
        addr_3fbf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax89 = reinterpret_cast<unsigned char>(v32);
        addr_3fd3_221:
        if (*reinterpret_cast<signed char*>(&eax89)) 
            goto addr_423a_220; else 
            goto addr_3fdb_219;
        addr_4b8f_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_423a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_4bad_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_5020_225:
        v31 = r13_34;
        addr_4a86_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_4c77_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3fd3_221;
        addr_4ad3_163:
        eax89 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3fd3_221;
        addr_4217_169:
        goto addr_4220_65;
        addr_4f9e_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_423a_220;
        goto addr_4bad_222;
        addr_4e24_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (*reinterpret_cast<uint32_t*>(&rcx45) = reinterpret_cast<unsigned char>(*v26), *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx45)))) {
            rsi30 = v114;
            rdx115 = r15_15;
            rax116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx115)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi30) + reinterpret_cast<uint64_t>(rdx115)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                rdx115 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx115) + 1);
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax116) + reinterpret_cast<uint64_t>(rdx115));
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx45));
            r15_15 = rdx115;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v117) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax118 = reinterpret_cast<uint64_t>(v119 - reinterpret_cast<unsigned char>(g28));
        if (!rax118) 
            goto addr_4e7e_236;
        fun_2440();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5020_225;
        addr_4e22_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4e24_210;
        addr_4a5e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_4e24_210;
        } else {
            goto addr_4a86_226;
        }
        addr_4f91_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_43ed_24:
    *reinterpret_cast<uint32_t*>(&rax120) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax120) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x792c + rax120 * 4) + 0x792c;
    addr_4819_32:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x7a2c + rax121 * 4) + 0x7a2c;
    addr_4e93_190:
    addr_3fdb_219:
    goto 0x3cc0;
    addr_3ec4_206:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x782c + rax122 * 4) + 0x782c;
    addr_4e7e_236:
    goto v123;
}

void fun_3ee0() {
}

void fun_4098() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3d92;
}

void fun_40f1() {
    goto 0x3d92;
}

void fun_41de() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4061;
    }
    if (v2) 
        goto 0x4ad3;
    if (!r10_3) 
        goto addr_4c3e_5;
    if (!v4) 
        goto addr_4b0e_7;
    addr_4c3e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_4b0e_7:
    goto 0x3f14;
}

void fun_41fc() {
}

void fun_42a7() {
    signed char v1;

    if (v1) {
        goto 0x422f;
    } else {
        goto 0x3f6a;
    }
}

void fun_42c1() {
    signed char v1;

    if (!v1) 
        goto 0x42ba; else 
        goto "???";
}

void fun_42e8() {
    goto 0x4203;
}

void fun_4368() {
}

void fun_4380() {
}

void fun_43af() {
    goto 0x4203;
}

void fun_4401() {
    goto 0x4390;
}

void fun_4430() {
    goto 0x4390;
}

void fun_4463() {
    goto 0x4390;
}

void fun_4830() {
    goto 0x3ee8;
}

void fun_4b2e() {
    signed char v1;

    if (v1) 
        goto 0x4ad3;
    goto 0x3f14;
}

void fun_4bd5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3f14;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3ef8;
        goto 0x3f14;
    }
}

void fun_4ff2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4260;
    } else {
        goto 0x3d92;
    }
}

void fun_5f28() {
    fun_2410();
}

void fun_411e() {
    goto 0x3d92;
}

void fun_42f4() {
    goto 0x42ac;
}

void fun_43bb() {
    goto 0x3ee8;
}

void fun_440d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4390;
    goto 0x3fbf;
}

void fun_443f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x439b;
        goto 0x3dc0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x423a;
        goto 0x3fdb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4bd8;
    if (r10_8 > r15_9) 
        goto addr_4325_9;
    addr_432a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4be3;
    goto 0x3f14;
    addr_4325_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_432a_10;
}

void fun_4472() {
    goto 0x3fa7;
}

void fun_4840() {
    goto 0x3fa7;
}

void fun_4fdf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x40fc;
    } else {
        goto 0x4260;
    }
}

void fun_5fe0() {
}

void fun_447c() {
    goto 0x4417;
}

void fun_484a() {
    goto 0x436d;
}

void fun_6040() {
    fun_2410();
    goto fun_2620;
}

void fun_414d() {
    goto 0x3d92;
}

void fun_4488() {
    goto 0x4417;
}

void fun_4857() {
    goto 0x43be;
}

void fun_6080() {
    fun_2410();
    goto fun_2620;
}

void fun_417a() {
    goto 0x3d92;
}

void fun_4494() {
    goto 0x4390;
}

void fun_60c0() {
    fun_2410();
    goto fun_2620;
}

void fun_419c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4b30;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4061;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4061;
    }
    if (v11) 
        goto 0x4e93;
    if (r10_12 > r15_13) 
        goto addr_4ee3_8;
    addr_4ee8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4c21;
    addr_4ee3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4ee8_9;
}

struct s13 {
    signed char[24] pad24;
    int64_t f18;
};

struct s14 {
    signed char[16] pad16;
    void** f10;
};

struct s15 {
    signed char[8] pad8;
    void** f8;
};

void fun_6110() {
    int64_t r15_1;
    struct s13* rbx2;
    void** r14_3;
    struct s14* rbx4;
    void** r13_5;
    struct s15* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2410();
    fun_2620(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6132, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6168() {
    fun_2410();
    goto 0x6139;
}

struct s16 {
    signed char[32] pad32;
    int64_t f20;
};

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    void** f10;
};

struct s19 {
    signed char[8] pad8;
    void** f8;
};

struct s20 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_61a0() {
    int64_t rcx1;
    struct s16* rbx2;
    int64_t r15_3;
    struct s17* rbx4;
    void** r14_5;
    struct s18* rbx6;
    void** r13_7;
    struct s19* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s20* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2410();
    fun_2620(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x61d4, __return_address(), rcx1);
    goto v15;
}

void fun_6218() {
    fun_2410();
    goto 0x61db;
}
