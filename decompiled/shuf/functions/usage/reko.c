void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000027F0(fn0000000000002550(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000036EE;
	}
	fn0000000000002740(fn0000000000002550(0x05, "Usage: %s [OPTION]... [FILE]\n  or:  %s -e [OPTION]... [ARG]...\n  or:  %s -i LO-HI [OPTION]...\n", null), 0x01);
	fn0000000000002650(stdout, fn0000000000002550(0x05, "Write a random permutation of the input lines to standard output.\n", null));
	fn0000000000002650(stdout, fn0000000000002550(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002650(stdout, fn0000000000002550(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002650(stdout, fn0000000000002550(0x05, "  -e, --echo                treat each ARG as an input line\n  -i, --input-range=LO-HI   treat each number LO through HI as an input line\n  -n, --head-count=COUNT    output at most COUNT lines\n  -o, --output=FILE         write result to FILE instead of standard output\n      --random-source=FILE  get random bytes from FILE\n  -r, --repeat              output lines can be repeated\n", null));
	fn0000000000002650(stdout, fn0000000000002550(0x05, "  -z, --zero-terminated     line delimiter is NUL, not newline\n", null));
	fn0000000000002650(stdout, fn0000000000002550(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002650(stdout, fn0000000000002550(0x05, "      --version     output version information and exit\n", null));
	struct Eq_1610 * rbx_193 = fp - 0xB8 + 16;
	do
	{
		char * rsi_195 = rbx_193->qw0000;
		++rbx_193;
	} while (rsi_195 != null && fn0000000000002680(rsi_195, 0xB004) != 0x00);
	ptr64 r13_208 = rbx_193->qw0008;
	if (r13_208 != 0x00)
	{
		fn0000000000002740(fn0000000000002550(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_8 rax_295 = fn0000000000002730(null, 0x05);
		if (rax_295 == 0x00 || fn0000000000002480(0x03, "en_", rax_295) == 0x00)
			goto l000000000000393E;
	}
	else
	{
		fn0000000000002740(fn0000000000002550(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_8 rax_237 = fn0000000000002730(null, 0x05);
		if (rax_237 == 0x00 || fn0000000000002480(0x03, "en_", rax_237) == 0x00)
		{
			fn0000000000002740(fn0000000000002550(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000397B:
			fn0000000000002740(fn0000000000002550(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000036EE:
			fn00000000000027D0(edi);
		}
		r13_208 = 0xB004;
	}
	fn0000000000002650(stdout, fn0000000000002550(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l000000000000393E:
	fn0000000000002740(fn0000000000002550(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000397B;
}