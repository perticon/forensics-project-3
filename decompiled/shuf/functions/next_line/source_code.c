next_line (char *line, char eolbyte)
{
  char *p = rawmemchr (line, eolbyte);
  return p + 1;
}