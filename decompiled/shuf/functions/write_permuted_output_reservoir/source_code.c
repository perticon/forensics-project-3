write_permuted_output_reservoir (size_t n_lines, struct linebuffer *lines,
                                 size_t const *permutation)
{
  for (size_t i = 0; i < n_lines; i++)
    {
      const struct linebuffer *p = &lines[permutation[i]];
      if (fwrite (p->buffer, sizeof (char), p->length, stdout) != p->length)
        return -1;
    }

  return 0;
}