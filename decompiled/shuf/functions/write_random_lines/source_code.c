write_random_lines (struct randint_source *s, size_t count,
                    char *const *lines, size_t n_lines)
{
  for (size_t i = 0; i < count; i++)
    {
      const randint j = randint_choose (s, n_lines);
      char *const *p = lines + j;
      size_t len = p[1] - p[0];
      if (fwrite (p[0], sizeof *p[0], len, stdout) != len)
        return -1;
    }

  return 0;
}