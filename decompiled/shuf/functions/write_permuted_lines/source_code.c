write_permuted_lines (size_t n_lines, char *const *line,
                      size_t const *permutation)
{
  for (size_t i = 0; i < n_lines; i++)
    {
      char *const *p = line + permutation[i];
      size_t len = p[1] - p[0];
      if (fwrite (p[0], sizeof *p[0], len, stdout) != len)
        return -1;
    }

  return 0;
}