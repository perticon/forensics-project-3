read_input_reservoir_sampling (FILE *in, char eolbyte, size_t k,
                               struct randint_source *s,
                               struct linebuffer **out_rsrv)
{
  randint n_lines = 0;
  size_t n_alloc_lines = MIN (k, RESERVOIR_LINES_INCREMENT);
  struct linebuffer *line = NULL;
  struct linebuffer *rsrv;

  rsrv = xcalloc (n_alloc_lines, sizeof (struct linebuffer));

  /* Fill the first K lines, directly into the reservoir.  */
  while (n_lines < k
         && (line =
             readlinebuffer_delim (&rsrv[n_lines], in, eolbyte)) != NULL)
    {
      n_lines++;

      /* Enlarge reservoir.  */
      if (n_lines >= n_alloc_lines)
        {
          n_alloc_lines += RESERVOIR_LINES_INCREMENT;
          rsrv = xnrealloc (rsrv, n_alloc_lines, sizeof (struct linebuffer));
          memset (&rsrv[n_lines], 0,
                  RESERVOIR_LINES_INCREMENT * sizeof (struct linebuffer));
        }
    }

  /* last line wasn't NULL - so there may be more lines to read.  */
  if (line != NULL)
    {
      struct linebuffer dummy;
      initbuffer (&dummy);  /* space for lines not put in reservoir.  */

      /* Choose the fate of the next line, with decreasing probability (as
         n_lines increases in size).

         If the line will be used, store it directly in the reservoir.
         Otherwise, store it in dummy space.

         With 'struct linebuffer', storing into existing buffer will reduce
         re-allocations (will only re-allocate if the new line is longer than
         the currently allocated space).  */
      do
        {
          randint j = randint_choose (s, n_lines + 1);  /* 0 .. n_lines.  */
          line = (j < k) ? (&rsrv[j]) : (&dummy);
        }
      while (readlinebuffer_delim (line, in, eolbyte) != NULL && n_lines++);

      if (! n_lines)
        die (EXIT_FAILURE, EOVERFLOW, _("too many input lines"));

      freebuffer (&dummy);
    }

  /* no more input lines, or an input error.  */
  if (ferror (in))
    die (EXIT_FAILURE, errno, _("read error"));

  *out_rsrv = rsrv;
  return MIN (k, n_lines);
}