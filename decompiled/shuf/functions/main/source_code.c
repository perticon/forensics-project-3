main (int argc, char **argv)
{
  bool echo = false;
  bool input_range = false;
  size_t lo_input = SIZE_MAX;
  size_t hi_input = 0;
  size_t head_lines = SIZE_MAX;
  char const *outfile = NULL;
  char *random_source = NULL;
  char eolbyte = '\n';
  char **input_lines = NULL;
  bool use_reservoir_sampling = false;
  bool repeat = false;

  int optc;
  int n_operands;
  char **operand;
  size_t n_lines;
  char **line = NULL;
  struct linebuffer *reservoir = NULL;
  struct randint_source *randint_source;
  size_t *permutation = NULL;
  int i;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  while ((optc = getopt_long (argc, argv, "ei:n:o:rz", long_opts, NULL)) != -1)
    switch (optc)
      {
      case 'e':
        echo = true;
        break;

      case 'i':
        {
          char *p = strchr (optarg, '-');
          char const *hi_optarg = optarg;
          bool invalid = !p;

          if (input_range)
            die (EXIT_FAILURE, 0, _("multiple -i options specified"));
          input_range = true;

          if (p)
            {
              *p = '\0';
              lo_input = xdectoumax (optarg, 0, SIZE_MAX, "",
                                     _("invalid input range"), 0);
              *p = '-';
              hi_optarg = p + 1;
            }

          hi_input = xdectoumax (hi_optarg, 0, SIZE_MAX, "",
                                 _("invalid input range"), 0);

          n_lines = hi_input - lo_input + 1;
          invalid |= ((lo_input <= hi_input) == (n_lines == 0));
          if (invalid)
            die (EXIT_FAILURE, errno, "%s: %s", _("invalid input range"),
                 quote (optarg));
        }
        break;

      case 'n':
        {
          uintmax_t argval;
          strtol_error e = xstrtoumax (optarg, NULL, 10, &argval, "");

          if (e == LONGINT_OK)
            head_lines = MIN (head_lines, argval);
          else if (e != LONGINT_OVERFLOW)
            die (EXIT_FAILURE, 0, _("invalid line count: %s"),
                 quote (optarg));
        }
        break;

      case 'o':
        if (outfile && !STREQ (outfile, optarg))
          die (EXIT_FAILURE, 0, _("multiple output files specified"));
        outfile = optarg;
        break;

      case RANDOM_SOURCE_OPTION:
        if (random_source && !STREQ (random_source, optarg))
          die (EXIT_FAILURE, 0, _("multiple random sources specified"));
        random_source = optarg;
        break;

      case 'r':
        repeat = true;
        break;

      case 'z':
        eolbyte = '\0';
        break;

      case_GETOPT_HELP_CHAR;
      case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);
      default:
        usage (EXIT_FAILURE);
      }

  n_operands = argc - optind;
  operand = argv + optind;

  /* Check invalid usage.  */
  if (echo && input_range)
    {
      error (0, 0, _("cannot combine -e and -i options"));
      usage (EXIT_FAILURE);
    }
  if (input_range ? 0 < n_operands : !echo && 1 < n_operands)
    {
      error (0, 0, _("extra operand %s"), quote (operand[!input_range]));
      usage (EXIT_FAILURE);
    }

  /* Prepare input.  */
  if (head_lines == 0)
    {
      n_lines = 0;
      line = NULL;
    }
  else if (echo)
    {
      input_from_argv (operand, n_operands, eolbyte);
      n_lines = n_operands;
      line = operand;
    }
  else if (input_range)
    {
      n_lines = hi_input - lo_input + 1;
      line = NULL;
    }
  else
    {
      /* If an input file is specified, re-open it as stdin.  */
      if (n_operands == 1
          && ! (STREQ (operand[0], "-")
                || freopen (operand[0], "r", stdin)))
        die (EXIT_FAILURE, errno, "%s", quotef (operand[0]));

      fadvise (stdin, FADVISE_SEQUENTIAL);

      if (repeat || head_lines == SIZE_MAX
          || input_size () <= RESERVOIR_MIN_INPUT)
        {
          n_lines = read_input (stdin, eolbyte, &input_lines);
          line = input_lines;
        }
      else
        {
          use_reservoir_sampling = true;
          n_lines = SIZE_MAX;   /* unknown number of input lines, for now.  */
        }
    }

  /* The adjusted head line count; can be less than HEAD_LINES if the
     input is small and if not repeating.  */
  size_t ahead_lines = repeat || head_lines < n_lines ? head_lines : n_lines;

  randint_source = randint_all_new (random_source,
                                    (use_reservoir_sampling || repeat
                                     ? SIZE_MAX
                                     : randperm_bound (ahead_lines, n_lines)));
  if (! randint_source)
    die (EXIT_FAILURE, errno, "%s",
         quotef (random_source ? random_source : "getrandom"));

  if (use_reservoir_sampling)
    {
      /* Instead of reading the entire file into 'line',
         use reservoir-sampling to store just AHEAD_LINES random lines.  */
      n_lines = read_input_reservoir_sampling (stdin, eolbyte, ahead_lines,
                                               randint_source, &reservoir);
      ahead_lines = n_lines;
    }

  /* Close stdin now, rather than earlier, so that randint_all_new
     doesn't have to worry about opening something other than
     stdin.  */
  if (! (head_lines == 0 || echo || input_range || fclose (stdin) == 0))
    die (EXIT_FAILURE, errno, _("read error"));

  if (!repeat)
    permutation = randperm_new (randint_source, ahead_lines, n_lines);

  if (outfile && ! freopen (outfile, "w", stdout))
    die (EXIT_FAILURE, errno, "%s", quotef (outfile));

  /* Generate output according to requested method */
  if (repeat)
    {
      if (head_lines == 0)
        i = 0;
      else
        {
          if (n_lines == 0)
            die (EXIT_FAILURE, 0, _("no lines to repeat"));
          if (input_range)
            i = write_random_numbers (randint_source, ahead_lines,
                                      lo_input, hi_input, eolbyte);
          else
            i = write_random_lines (randint_source, ahead_lines, line, n_lines);
        }
    }
  else
    {
      if (use_reservoir_sampling)
        i = write_permuted_output_reservoir (n_lines, reservoir, permutation);
      else if (input_range)
        i = write_permuted_numbers (ahead_lines, lo_input,
                                    permutation, eolbyte);
      else
        i = write_permuted_lines (ahead_lines, line, permutation);
    }

  if (i != 0)
    die (EXIT_FAILURE, errno, _("write error"));

  main_exit (EXIT_SUCCESS);
}