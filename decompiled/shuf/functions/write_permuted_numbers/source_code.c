write_permuted_numbers (size_t n_lines, size_t lo_input,
                        size_t const *permutation, char eolbyte)
{
  for (size_t i = 0; i < n_lines; i++)
    {
      unsigned long int n = lo_input + permutation[i];
      if (printf ("%lu%c", n, eolbyte) < 0)
        return -1;
    }

  return 0;
}