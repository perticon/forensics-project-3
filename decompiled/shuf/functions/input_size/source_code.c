input_size (void)
{
  off_t file_size;

  struct stat stat_buf;
  if (fstat (STDIN_FILENO, &stat_buf) != 0)
    return OFF_T_MAX;
  if (usable_st_size (&stat_buf))
    file_size = stat_buf.st_size;
  else
    return OFF_T_MAX;

  off_t input_offset = lseek (STDIN_FILENO, 0, SEEK_CUR);
  if (input_offset < 0)
    return OFF_T_MAX;

  file_size -= input_offset;

  return file_size;
}