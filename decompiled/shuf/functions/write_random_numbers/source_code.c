write_random_numbers (struct randint_source *s, size_t count,
                      size_t lo_input, size_t hi_input, char eolbyte)
{
  const randint range = hi_input - lo_input + 1;

  for (size_t i = 0; i < count; i++)
    {
      unsigned long int j = lo_input + randint_choose (s, range);
      if (printf ("%lu%c", j, eolbyte) < 0)
        return -1;
    }

  return 0;
}