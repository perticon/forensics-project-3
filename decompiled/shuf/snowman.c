
int64_t fun_2560();

int64_t fun_2460(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2560();
    if (r8d > 10) {
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb7c0 + rax11 * 4) + 0xb7c0;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

int32_t* fun_2470();

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_2600(void*** rdi, ...);

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2430(void** rdi, void** rsi, ...);

void** xcharalloc(void** rdi, ...);

void fun_2580();

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    void*** rdi15;
    int64_t rax16;
    uint32_t r8d17;
    struct s1* rbx18;
    uint32_t r15d19;
    void** rsi20;
    void** r14_21;
    int64_t v22;
    int64_t v23;
    uint32_t r15d24;
    void** rax25;
    void** rsi26;
    void** rax27;
    uint32_t r8d28;
    int64_t v29;
    int64_t v30;
    void* rax31;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x570f;
    rax8 = fun_2470();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x10070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xa7c1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            rdi15 = reinterpret_cast<void***>((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            v7 = 0x579b;
            fun_2600(rdi15, rdi15);
            rax16 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax16);
        }
        r8d17 = rcx->f0;
        rbx18 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d19 = rcx->f4;
        rsi20 = rbx18->f0;
        r14_21 = rbx18->f8;
        v22 = rcx->f30;
        v23 = rcx->f28;
        r15d24 = r15d19 | 1;
        rax25 = quotearg_buffer_restyled(r14_21, rsi20, rsi, rdx, r8d17, r15d24, &rcx->f8, v23, v22, v7);
        if (reinterpret_cast<unsigned char>(rsi20) <= reinterpret_cast<unsigned char>(rax25)) {
            rsi26 = rax25 + 1;
            rbx18->f0 = rsi26;
            if (r14_21 != 0x10100) {
                fun_2430(r14_21, rsi26, r14_21, rsi26);
                rsi26 = rsi26;
            }
            rax27 = xcharalloc(rsi26, rsi26);
            r8d28 = rcx->f0;
            rbx18->f8 = rax27;
            v29 = rcx->f30;
            r14_21 = rax27;
            v30 = rcx->f28;
            quotearg_buffer_restyled(rax27, rsi26, rsi, rdx, r8d28, r15d24, rsi26, v30, v29, 0x582a);
        }
        *rax8 = v10;
        rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax31) {
            fun_2580();
        } else {
            return r14_21;
        }
    }
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0xc050);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0xc050);
    if (*reinterpret_cast<void***>(rdi + 40) != 0xc050) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x3608]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0xc050);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x346f]");
        if (1) 
            goto addr_8cec_6;
        __asm__("comiss xmm1, [rip+0x3466]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x3458]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_8ccc_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_8cc2_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_8ccc_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_8cc2_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_8cc2_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_8ccc_13:
        return 0;
    } else {
        addr_8cec_6:
        return r8_3;
    }
}

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_26c0(void** rdi, void** rsi, void** rdx, ...);

int32_t transfer_entries(void** rdi, void** rsi, int32_t edx) {
    void** rdx3;
    void** r14_4;
    int32_t r12d5;
    void** rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s2* rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s3* r13_18;
    void** rax19;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = *reinterpret_cast<void***>(rsi);
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) {
        do {
            addr_8d56_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_8d48_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_8d56_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = *reinterpret_cast<void***>(r14_4 + 16);
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi10)), rsi10 = *reinterpret_cast<void***>(r14_4 + 16), reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_8dce_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax15 = *reinterpret_cast<void***>(r14_4 + 72);
                            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            *reinterpret_cast<void***>(r14_4 + 72) = r13_9;
                            if (!rdx3) 
                                goto addr_8dce_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_2897_12;
                    addr_8dce_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_8d48_3;
            }
            rsi16 = *reinterpret_cast<void***>(r14_4 + 16);
            rdi12 = r15_8;
            rax17 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 16))) 
                goto addr_2897_12;
            r13_18 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
            if (r13_18->f0) 
                goto addr_8e08_16;
            r13_18->f0 = r15_8;
            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
            continue;
            addr_8e08_16:
            rax19 = *reinterpret_cast<void***>(r14_4 + 72);
            if (!rax19) {
                rax19 = fun_26c0(16, rsi16, rdx3);
                if (!rax19) 
                    goto addr_8e7a_19;
            } else {
                *reinterpret_cast<void***>(r14_4 + 72) = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx3 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx3;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            *reinterpret_cast<void***>(rbp6 + 24) = *reinterpret_cast<void***>(rbp6 + 24) - 1;
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_2897_12:
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    addr_8e7a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_8b7f_10:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_8b24_13;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<int64_t*>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_8b90_17;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<int64_t*>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_8b90_17;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_8b7f_10;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_8b24_13;
            }
        }
    }
    addr_8b81_21:
    return rax11;
    addr_8b24_13:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_8b81_21;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_8b90_17:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x10080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xb74b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xb744);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xb74f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xb740);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gfdb8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gfdb8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t free = 0;

void fun_2433() {
    __asm__("cli ");
    goto free;
}

int64_t __cxa_finalize = 0;

void fun_2443() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_2453() {
    __asm__("cli ");
    goto __uflow;
}

int64_t abort = 0x2040;

void fun_2463() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2473() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_2483() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_2493() {
    __asm__("cli ");
    goto _exit;
}

int64_t ftello = 0x2080;

void fun_24a3() {
    __asm__("cli ");
    goto ftello;
}

int64_t __fpending = 0x2090;

void fun_24b3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t ferror = 0x20a0;

void fun_24c3() {
    __asm__("cli ");
    goto ferror;
}

int64_t fread = 0x20b0;

void fun_24d3() {
    __asm__("cli ");
    goto fread;
}

int64_t reallocarray = 0x20c0;

void fun_24e3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20d0;

void fun_24f3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t fread_unlocked = 0x20e0;

void fun_2503() {
    __asm__("cli ");
    goto fread_unlocked;
}

int64_t textdomain = 0x20f0;

void fun_2513() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2100;

void fun_2523() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2110;

void fun_2533() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x2120;

void fun_2543() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x2130;

void fun_2553() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2140;

void fun_2563() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2150;

void fun_2573() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2160;

void fun_2583() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2170;

void fun_2593() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2180;

void fun_25a3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x2190;

void fun_25b3() {
    __asm__("cli ");
    goto dup2;
}

int64_t strchr = 0x21a0;

void fun_25c3() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x21b0;

void fun_25d3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21c0;

void fun_25e3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21d0;

void fun_25f3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21e0;

void fun_2603() {
    __asm__("cli ");
    goto memset;
}

int64_t freopen = 0x21f0;

void fun_2613() {
    __asm__("cli ");
    goto freopen;
}

int64_t close = 0x2200;

void fun_2623() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x2210;

void fun_2633() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x2220;

void fun_2643() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2230;

void fun_2653() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x2240;

void fun_2663() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x2250;

void fun_2673() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2260;

void fun_2683() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2270;

void fun_2693() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2280;

void fun_26a3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2290;

void fun_26b3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x22a0;

void fun_26c3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22b0;

void fun_26d3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22c0;

void fun_26e3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22d0;

void fun_26f3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x22e0;

void fun_2703() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x22f0;

void fun_2713() {
    __asm__("cli ");
    goto realloc;
}

int64_t fdopen = 0x2300;

void fun_2723() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x2310;

void fun_2733() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2320;

void fun_2743() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t setvbuf = 0x2330;

void fun_2753() {
    __asm__("cli ");
    goto setvbuf;
}

int64_t error = 0x2340;

void fun_2763() {
    __asm__("cli ");
    goto error;
}

int64_t __explicit_bzero_chk = 0x2350;

void fun_2773() {
    __asm__("cli ");
    goto __explicit_bzero_chk;
}

int64_t open = 0x2360;

void fun_2783() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2370;

void fun_2793() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2380;

void fun_27a3() {
    __asm__("cli ");
    goto fopen;
}

int64_t strtoumax = 0x2390;

void fun_27b3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x23a0;

void fun_27c3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x23b0;

void fun_27d3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23c0;

void fun_27e3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23d0;

void fun_27f3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getrandom = 0x23e0;

void fun_2803() {
    __asm__("cli ");
    goto getrandom;
}

int64_t mbsinit = 0x23f0;

void fun_2813() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2400;

void fun_2823() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2410;

void fun_2833() {
    __asm__("cli ");
    goto fstat;
}

int64_t __ctype_b_loc = 0x2420;

void fun_2843() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2730(int64_t rdi, ...);

void fun_2530(int64_t rdi, int64_t rsi);

void fun_2510(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2590(int64_t rdi);

int32_t optind = 0;

void usage();

void** optarg = reinterpret_cast<void**>(0);

int32_t fun_2680(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0xb6e4;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

int32_t fun_27d0();

void** quote(void** rdi, ...);

void** fun_2550();

void fun_2760();

void** randint_all_new(void** rdi, void** rsi, void** rdx, void** rcx);

void** randperm_bound(void** rdi, void** rsi, void** rdx, void** rcx);

void** randperm_new(void** rdi);

int64_t freopen_safer(void** rdi, void** rsi, void** rdx, void** rcx);

void** randint_genmax(void** rdi, void** rsi, void** rdx, void** rcx, ...);

int32_t fun_2740(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

void** stdin = reinterpret_cast<void**>(0);

void** quotearg_n_style_colon();

void fadvise(void** rdi, int64_t rsi, void** rdx, void** rcx);

int32_t fun_2830();

uint64_t fun_25e0();

void** fread_file(void** rdi, ...);

void** xcalloc(void** rdi, int64_t rsi, void** rdx, void** rcx);

int64_t readlinebuffer_delim(void** rdi, void** rsi, void** rdx, void** rcx);

void** xreallocarray(void** rdi, void** rsi, int64_t rdx, void** rcx);

void** quotearg_colon();

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, ...);

void** fun_2700(void** rdi);

struct s5 {
    void** f0;
    signed char[7] pad8;
    void* f8;
};

struct s6 {
    void** f0;
    signed char[7] pad8;
    void* f8;
};

void initbuffer(void** rdi, void** rsi, void** rdx, void** rcx);

void freebuffer(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_2570(void** rdi, ...);

void** xmalloc(void** rdi, void** rsi, ...);

struct s7 {
    signed char f0;
    void** f1;
};

struct s7* fun_2540(void** rdi);

struct s8 {
    signed char[1] pad1;
    void** f1;
};

struct s8* fun_2660(void** rdi, void** rsi, void** rdx, void** rcx);

void** xnmalloc(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void fun_28c3(int32_t edi, void** rsi) {
    void** r13_3;
    int32_t ebp4;
    void** rbx5;
    void** rdi6;
    void** r12_7;
    void* rsp8;
    unsigned char v9;
    void** v10;
    void** v11;
    void** rcx12;
    void** rdx13;
    void** rsi14;
    int64_t rdi15;
    int32_t eax16;
    void* rsp17;
    int64_t rax18;
    void** rdx19;
    int32_t eax20;
    void** rdi21;
    int64_t rcx22;
    int32_t eax23;
    void** rdi24;
    void** rax25;
    int64_t rax26;
    void** rbp27;
    void** rbx28;
    void* rax29;
    void** rax30;
    void** r12_31;
    void* rsp32;
    void** rax33;
    void** rax34;
    void** rsi35;
    void** rax36;
    void** rax37;
    void** r14_38;
    int64_t rax39;
    void** rax40;
    void** r15_41;
    uint32_t r12d42;
    void** r14_43;
    void** rax44;
    int32_t eax45;
    void** rdi46;
    void** rax47;
    void** rax48;
    void** r12_49;
    int32_t eax50;
    int64_t rax51;
    void** rax52;
    void* rsp53;
    void* rsp54;
    int32_t eax55;
    uint32_t v56;
    void** v57;
    uint64_t rax58;
    void** rdi59;
    void** rax60;
    void* rsp61;
    void** rax62;
    int32_t* rax63;
    void** rax64;
    void** rax65;
    int32_t* rax66;
    int32_t r12d67;
    void** r15_68;
    void** r14_69;
    void** rax70;
    int64_t rax71;
    void** rax72;
    void** rdi73;
    int64_t rax74;
    int64_t rax75;
    void** rax76;
    int64_t rax77;
    void** r15_78;
    void** rdi79;
    void** rax80;
    void** rax81;
    int32_t* rax82;
    void** r12_83;
    struct s5* rax84;
    void** rdi85;
    void** rax86;
    uint32_t r12d87;
    int32_t eax88;
    int64_t r14_89;
    void** r12_90;
    void** rax91;
    struct s6* rax92;
    void** rdi93;
    void** rax94;
    void** rax95;
    void** rdi96;
    int64_t rax97;
    void** rbp98;
    void** rax99;
    void** rdi100;
    int64_t rax101;
    int64_t r13_102;
    void** r14_103;
    void** rdi104;
    void** rax105;
    void** rax106;
    uint32_t r15d107;
    void** r14_108;
    struct s7* rax109;
    void* rbx110;
    void* v111;
    void* rax112;
    void** rbx113;
    void** rdi114;
    void** r14_115;
    void** rsi116;
    struct s8* rax117;
    void** r14_118;
    void** rax119;
    void** rdi120;
    struct s8* rax121;

    __asm__("cli ");
    r13_3 = reinterpret_cast<void**>("ei:n:o:rz");
    ebp4 = edi;
    rbx5 = rsi;
    rdi6 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi6);
    fun_2730(6, 6);
    fun_2530("coreutils", "/usr/local/share/locale");
    r12_7 = reinterpret_cast<void**>(0xb674);
    fun_2510("coreutils", "/usr/local/share/locale");
    atexit(0x3a90, "/usr/local/share/locale");
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x108 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v9 = 10;
    v10 = reinterpret_cast<void**>(0);
    v11 = reinterpret_cast<void**>(0);
    while (1) {
        rcx12 = reinterpret_cast<void**>(0xfa20);
        rdx13 = reinterpret_cast<void**>("ei:n:o:rz");
        rsi14 = rbx5;
        *reinterpret_cast<int32_t*>(&rdi15) = ebp4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        eax16 = fun_2590(rdi15);
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        if (eax16 == -1) {
            addr_29d9_3:
            rax18 = optind;
            if (1) 
                break;
            if (1) 
                break;
        } else {
            if (eax16 > 0x80) {
                addr_2a16_6:
                usage();
                rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                rdx19 = optarg;
                if (!v10) 
                    goto addr_2a4b_7;
                eax20 = fun_2680(v10, rdx19, rdx19, 0xfa20);
                rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
                rdx19 = rdx19;
                if (eax20) 
                    goto addr_3531_9; else 
                    goto addr_2a4b_7;
            } else {
                if (eax16 <= 100) {
                    if (eax16 == 0xffffff7d) {
                        rdi21 = stdout;
                        rcx22 = Version;
                        version_etc(rdi21, "shuf", "GNU coreutils", rcx22, "Paul Eggert");
                        eax23 = fun_27d0();
                        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8);
                        if (!(eax23 - 1)) 
                            continue;
                        rdi24 = optarg;
                        rax25 = quote(rdi24, rdi24);
                        r12_7 = rax25;
                        fun_2550();
                        fun_2760();
                        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 + 8 - 8 + 8);
                        v9 = 0;
                        continue;
                    }
                    if (eax16 != 0xffffff7e) 
                        goto addr_2a16_6; else 
                        goto addr_29d2_16;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax26) = eax16 - 0x65;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax26) > 27) 
                        goto addr_2a16_6; else 
                        goto addr_29b4_18;
                }
            }
        }
        fun_2550();
        fun_2760();
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8);
        goto addr_2a16_6;
        addr_2a4b_7:
        v10 = rdx19;
        continue;
        addr_29d2_16:
        usage();
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
        goto addr_29d9_3;
    }
    *reinterpret_cast<int32_t*>(&rbp27) = ebp4 - *reinterpret_cast<int32_t*>(&rax18);
    *reinterpret_cast<int32_t*>(&rbp27 + 4) = 0;
    rbx28 = rbx5 + rax18 * 8;
    if (1) 
        goto addr_2dd6_21;
    *reinterpret_cast<int32_t*>(&rax29) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp27) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp27) == 0))) 
        goto addr_2de7_23;
    if (1) 
        goto addr_31aa_25; else 
        goto addr_2ce8_26;
    addr_3531_9:
    fun_2550();
    fun_2760();
    fun_2550();
    fun_2760();
    fun_2550();
    fun_2760();
    addr_29b4_18:
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_7) + reinterpret_cast<uint64_t>(rax26 * 4)))) + reinterpret_cast<unsigned char>(r12_7);
    while (1) {
        addr_3012_29:
        if (0) 
            goto addr_2e97_30;
        r13_3 = reinterpret_cast<void**>(0xffffffffffffffff);
        goto addr_2d06_32;
        addr_2fe6_33:
        rax30 = fun_2550();
        r12_31 = rax30;
        fun_2470();
        rdx13 = r12_31;
        fun_2760();
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8);
        continue;
        addr_323d_34:
        goto addr_2fe6_33;
        while (1) {
            addr_2ce8_26:
            *reinterpret_cast<int32_t*>(&r12_31) = 0;
            *reinterpret_cast<int32_t*>(&r12_31 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbx28) = 0;
            *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
            while (1) {
                while (1) {
                    if (reinterpret_cast<unsigned char>(0xffffffffffffffff) < reinterpret_cast<unsigned char>(r12_31)) 
                        goto addr_3012_29;
                    r13_3 = r12_31;
                    if (0) {
                        addr_2e97_30:
                        rsi14 = reinterpret_cast<void**>(0xffffffffffffffff);
                        rax33 = randint_all_new(v10, 0xffffffffffffffff, rdx13, rcx12);
                        rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                        rbp27 = rax33;
                        if (rax33) 
                            goto addr_2eea_37; else 
                            goto addr_2ead_38;
                    }
                    addr_2d06_32:
                    rax34 = randperm_bound(r13_3, r12_31, rdx13, rcx12);
                    rsi35 = rax34;
                    rax36 = randint_all_new(v10, rsi35, rdx13, rcx12);
                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8);
                    v11 = rax36;
                    if (!rax36) 
                        goto addr_2ead_38;
                    *reinterpret_cast<signed char*>(&v10) = 0;
                    if (!1) 
                        goto addr_2d4c_40;
                    *reinterpret_cast<int32_t*>(&rbp27) = 0;
                    *reinterpret_cast<int32_t*>(&rbp27 + 4) = 0;
                    goto addr_316a_42;
                    addr_2d4c_40:
                    rdx13 = r12_31;
                    rsi14 = r13_3;
                    rax37 = randperm_new(v11);
                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                    r14_38 = rax37;
                    if (1) 
                        goto addr_2d85_43;
                    rdx13 = stdout;
                    rsi14 = reinterpret_cast<void**>("w");
                    rax39 = freopen_safer(0, "w", rdx13, rcx12);
                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                    if (!rax39) 
                        goto addr_31bf_45; else 
                        goto addr_2d85_43;
                    addr_352c_46:
                    addr_317e_47:
                    rax40 = fun_2550();
                    fun_2470();
                    rdx13 = rax40;
                    fun_2760();
                    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8);
                    addr_31aa_25:
                    *reinterpret_cast<int32_t*>(&rbx28) = 0;
                    *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
                    r12_31 = v11 + 1 - 0xffffffffffffffff;
                    continue;
                    while (1) {
                        rbx28 = reinterpret_cast<void**>("%lu%c");
                        r15_41 = reinterpret_cast<void**>(0);
                        r12d42 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(v9)));
                        r14_43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(0xffffffffffffffff));
                        r13_3 = rbp27;
                        rbp27 = reinterpret_cast<void**>(0xffffffffffffffff);
                        while (!reinterpret_cast<int1_t>(0xffffffffffffffff == r15_41)) {
                            rax44 = randint_genmax(r13_3, r14_43, rdx13, rcx12);
                            *reinterpret_cast<uint32_t*>(&rcx12) = r12d42;
                            *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                            rsi14 = reinterpret_cast<void**>("%lu%c");
                            rdx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(0xffffffffffffffff) + reinterpret_cast<unsigned char>(rax44));
                            eax45 = fun_2740(1, "%lu%c", rdx13, rcx12);
                            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
                            if (eax45 < 0) 
                                goto addr_2fe6_33;
                            ++r15_41;
                        }
                        while (1) {
                            fun_27d0();
                            rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                            addr_2dd6_21:
                            if (!0 && *reinterpret_cast<int32_t*>(&rbp27) > 1) {
                                *reinterpret_cast<int32_t*>(&rax29) = 8;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
                                addr_2de7_23:
                                rdi46 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbx28) + reinterpret_cast<uint64_t>(rax29));
                                rax47 = quote(rdi46);
                                rax48 = fun_2550();
                                rcx12 = rax47;
                                *reinterpret_cast<int32_t*>(&rsi14) = 0;
                                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                                rdx13 = rax48;
                                fun_2760();
                                usage();
                                rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                            }
                            if (0) 
                                goto addr_2ce8_26;
                            if (!1) 
                                goto addr_2e38_57;
                            *reinterpret_cast<int32_t*>(&rbp27) = *reinterpret_cast<int32_t*>(&rbp27) - 1;
                            *reinterpret_cast<int32_t*>(&rbp27 + 4) = 0;
                            r12_49 = stdin;
                            if (!*reinterpret_cast<int32_t*>(&rbp27) && ((rbp27 = *reinterpret_cast<void***>(rbx28), eax50 = fun_2680(rbp27, "-", rdx13, rcx12), rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8), !!eax50) && (rdx13 = r12_49, rax51 = freopen_safer(rbp27, "r", rdx13, rcx12), rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8), r12_49 = stdin, !rax51))) {
                                rax52 = quotearg_n_style_colon();
                                fun_2470();
                                rcx12 = rax52;
                                fun_2760();
                                rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8);
                                goto addr_33f1_60;
                            }
                            fadvise(r12_49, 2, rdx13, rcx12);
                            rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                            if (1 || (eax55 = fun_2830(), rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8), !eax55) && ((v56 & 0xd000) == 0x8000 && ((*reinterpret_cast<int32_t*>(&rdx13) = 1, *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0, rbx28 = v57, rax58 = fun_25e0(), rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8), reinterpret_cast<int64_t>(rax58) >= reinterpret_cast<int64_t>(0)) && (rbx28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx28) - rax58), reinterpret_cast<signed char>(rbx28) <= reinterpret_cast<signed char>(0x800000))))) {
                                rdi59 = stdin;
                                rdx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 64);
                                *reinterpret_cast<int32_t*>(&rbp27) = reinterpret_cast<signed char>(v9);
                                *reinterpret_cast<int32_t*>(&rbp27 + 4) = 0;
                                rax60 = fread_file(rdi59, rdi59);
                                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                                r13_3 = rax60;
                                if (rax60) 
                                    goto addr_30e4_63;
                                rax62 = fun_2550();
                                r12_31 = rax62;
                                rax63 = fun_2470();
                                rdx13 = r12_31;
                                *reinterpret_cast<int32_t*>(&rsi14) = *rax63;
                                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                                fun_2760();
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8 - 8 + 8 - 8 + 8);
                            } else {
                                rax64 = randint_all_new(v10, 0xffffffffffffffff, rdx13, rcx12);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                                v11 = rax64;
                                if (!rax64) {
                                    addr_2ead_38:
                                    if (!v10) {
                                        v10 = reinterpret_cast<void**>("getrandom");
                                    }
                                    rax65 = quotearg_n_style_colon();
                                    r12_31 = rax65;
                                    rax66 = fun_2470();
                                    rcx12 = r12_31;
                                    rdx13 = reinterpret_cast<void**>("%s");
                                    *reinterpret_cast<int32_t*>(&rsi14) = *rax66;
                                    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                                    fun_2760();
                                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8);
                                    addr_2eea_37:
                                    if (0) 
                                        goto addr_2f00_68;
                                    if (!1) 
                                        goto addr_2f00_68;
                                } else {
                                    *reinterpret_cast<int32_t*>(&rbx28) = 0x400;
                                    *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
                                    r12d67 = reinterpret_cast<signed char>(v9);
                                    r15_68 = stdin;
                                    if (0) {
                                        rbx28 = reinterpret_cast<void**>(0xffffffffffffffff);
                                    }
                                    *reinterpret_cast<int32_t*>(&r14_69) = 0;
                                    *reinterpret_cast<int32_t*>(&r14_69 + 4) = 0;
                                    rax70 = xcalloc(rbx28, 24, rdx13, rcx12);
                                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                    rbp27 = rax70;
                                    do {
                                        r13_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_69) * 24);
                                        *reinterpret_cast<int32_t*>(&rdx13) = r12d67;
                                        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                                        rsi35 = r15_68;
                                        rax71 = readlinebuffer_delim(reinterpret_cast<unsigned char>(rbp27) + reinterpret_cast<unsigned char>(r13_3), rsi35, rdx13, rcx12);
                                        rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                        if (!rax71) 
                                            goto addr_336c_74;
                                        ++r14_69;
                                        if (reinterpret_cast<unsigned char>(rbx28) <= reinterpret_cast<unsigned char>(r14_69)) {
                                            rbx28 = rbx28 + 0x400;
                                            rax72 = xreallocarray(rbp27, rbx28, 24, rcx12);
                                            *reinterpret_cast<int32_t*>(&rdx13) = reinterpret_cast<int32_t>(quotearg_colon);
                                            *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                                            *reinterpret_cast<int32_t*>(&rsi35) = 0;
                                            *reinterpret_cast<int32_t*>(&rsi35 + 4) = 0;
                                            rbp27 = rax72;
                                            fun_2600(reinterpret_cast<unsigned char>(rax72) + reinterpret_cast<unsigned char>(r13_3) + 24);
                                            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
                                        }
                                    } while (0xffffffffffffffff != r14_69);
                                    goto addr_32fc_78;
                                }
                            }
                            rdi73 = stdin;
                            rax74 = rpl_fclose(rdi73, rsi14, rdx13, rdi73, rsi14, rdx13);
                            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                            if (*reinterpret_cast<int32_t*>(&rax74)) 
                                goto addr_352c_46;
                            addr_2f00_68:
                            if (1) 
                                goto addr_2f29_80;
                            rdx13 = stdout;
                            rsi14 = reinterpret_cast<void**>("w");
                            rax75 = freopen_safer(0, "w", rdx13, rcx12);
                            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                            if (!rax75) 
                                goto addr_31bf_45;
                            addr_2f29_80:
                            if (0) 
                                continue;
                            if (r12_31) 
                                goto addr_2f3e_83;
                            addr_3444_84:
                            fun_2550();
                            fun_2760();
                            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
                            addr_3468_85:
                            rdx13 = r12_31;
                            rsi14 = r13_3;
                            rax76 = randperm_new(v11);
                            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                            r14_38 = rax76;
                            if (1 || (rdx13 = stdout, rsi14 = reinterpret_cast<void**>("w"), rax77 = freopen_safer(0, "w", rdx13, rcx12), rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8), !!rax77)) {
                                *reinterpret_cast<int32_t*>(&r15_78) = 0;
                                *reinterpret_cast<int32_t*>(&r15_78 + 4) = 0;
                                if (*reinterpret_cast<signed char*>(&v10)) {
                                    while (r15_78 != r12_31) {
                                        rcx12 = stdout;
                                        *reinterpret_cast<int32_t*>(&rsi14) = 1;
                                        *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                                        rbx28 = rbp27 + reinterpret_cast<int64_t>(r14_38[reinterpret_cast<unsigned char>(r15_78)]) * 24;
                                        rdx13 = *reinterpret_cast<void***>(rbx28 + 8);
                                        rdi79 = *reinterpret_cast<void***>(rbx28 + 16);
                                        rax80 = fun_2700(rdi79);
                                        rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                        if (rax80 != *reinterpret_cast<void***>(rbx28 + 8)) 
                                            goto addr_2fe6_33;
                                        ++r15_78;
                                    }
                                    continue;
                                }
                                addr_2d85_43:
                                if (!1) 
                                    goto addr_2d8f_92;
                            } else {
                                addr_31bf_45:
                                rax81 = quotearg_n_style_colon();
                                r12_31 = rax81;
                                rax82 = fun_2470();
                                rcx12 = r12_31;
                                rdx13 = reinterpret_cast<void**>("%s");
                                *reinterpret_cast<int32_t*>(&rsi14) = *rax82;
                                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                                fun_2760();
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8);
                                goto addr_31f0_93;
                            }
                            *reinterpret_cast<int32_t*>(&r12_83) = 0;
                            *reinterpret_cast<int32_t*>(&r12_83 + 4) = 0;
                            while (r13_3 != r12_83) {
                                rcx12 = stdout;
                                *reinterpret_cast<int32_t*>(&rsi14) = 1;
                                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                                rax84 = reinterpret_cast<struct s5*>(rbx28 + reinterpret_cast<int64_t>(r14_38[reinterpret_cast<unsigned char>(r12_83)]) * 8);
                                rdi85 = rax84->f0;
                                rbp27 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax84->f8) - reinterpret_cast<unsigned char>(rdi85));
                                rdx13 = rbp27;
                                rax86 = fun_2700(rdi85);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                if (rbp27 != rax86) 
                                    goto addr_2fe6_33;
                                ++r12_83;
                            }
                            continue;
                            addr_2d8f_92:
                            r12d87 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(v9)));
                            *reinterpret_cast<int32_t*>(&rbx28) = 0;
                            *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
                            rbp27 = reinterpret_cast<void**>("%lu%c");
                            while (r13_3 != rbx28) {
                                *reinterpret_cast<uint32_t*>(&rcx12) = r12d87;
                                *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                                rsi14 = reinterpret_cast<void**>("%lu%c");
                                rdx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_38[reinterpret_cast<unsigned char>(rbx28)]) + 0xffffffffffffffff);
                                eax88 = fun_2740(1, "%lu%c", rdx13, rcx12);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                if (eax88 < 0) 
                                    goto addr_2fe6_33;
                                ++rbx28;
                            }
                            continue;
                            addr_31f0_93:
                            *reinterpret_cast<int32_t*>(&r14_89) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_89) + 4) = 0;
                            r12_90 = r12_31 - 1;
                            while (-1 != r14_89) {
                                rax91 = randint_genmax(rbp27, r12_90, rdx13, rcx12);
                                rcx12 = stdout;
                                *reinterpret_cast<int32_t*>(&rsi14) = 1;
                                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                                rax92 = reinterpret_cast<struct s6*>(rbx28 + reinterpret_cast<unsigned char>(rax91) * 8);
                                rdi93 = rax92->f0;
                                r13_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax92->f8) - reinterpret_cast<unsigned char>(rdi93));
                                rdx13 = r13_3;
                                rax94 = fun_2700(rdi93);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
                                if (r13_3 != rax94) 
                                    goto addr_323d_34;
                                ++r14_89;
                            }
                            continue;
                            addr_2f3e_83:
                            if (1) 
                                goto addr_31f0_93; else 
                                break;
                            addr_336c_74:
                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_68)) & 32) {
                                addr_3418_104:
                                rax95 = fun_2550();
                                r12_31 = rax95;
                                fun_2470();
                                fun_2760();
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8);
                                goto addr_3444_84;
                            } else {
                                *reinterpret_cast<signed char*>(&v10) = 1;
                                r12_31 = reinterpret_cast<void**>(0xffffffffffffffff);
                                if (reinterpret_cast<unsigned char>(0xffffffffffffffff) > reinterpret_cast<unsigned char>(r14_69)) {
                                    r12_31 = r14_69;
                                }
                                *reinterpret_cast<int32_t*>(&rbx28) = 0;
                                *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
                                r13_3 = r12_31;
                                addr_316a_42:
                                rdi96 = stdin;
                                rax97 = rpl_fclose(rdi96, rsi35, rdx13, rdi96, rsi35, rdx13);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                if (!*reinterpret_cast<int32_t*>(&rax97)) 
                                    goto addr_3468_85; else 
                                    goto addr_317e_47;
                            }
                            addr_32fc_78:
                            r13_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp32) + 64);
                            initbuffer(r13_3, rsi35, rdx13, rcx12);
                            rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                            v10 = rbp27;
                            rbp98 = v11;
                            do {
                                rbx28 = r14_69;
                                ++r14_69;
                                rax99 = randint_genmax(rbp98, rbx28, rdx13, rcx12);
                                rdi100 = r13_3;
                                if (reinterpret_cast<unsigned char>(0xffffffffffffffff) > reinterpret_cast<unsigned char>(rax99)) {
                                    rcx12 = v10;
                                    rdi100 = rcx12 + reinterpret_cast<unsigned char>(rax99) * 24;
                                }
                                *reinterpret_cast<int32_t*>(&rdx13) = r12d67;
                                *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                                rsi35 = r15_68;
                                rax101 = readlinebuffer_delim(rdi100, rsi35, rdx13, rcx12);
                                rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8 - 8 + 8);
                                if (!rax101) 
                                    break;
                            } while (rbx28);
                            goto addr_3394_112;
                            rbp27 = v10;
                            if (!rbx28) {
                                addr_33f1_60:
                                fun_2550();
                                fun_2760();
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8 - 8 + 8);
                                goto addr_3418_104;
                            } else {
                                addr_3361_114:
                                r14_69 = rbx28;
                                freebuffer(r13_3, rsi35, rdx13, rcx12);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8);
                                goto addr_336c_74;
                            }
                            addr_3394_112:
                            rbp27 = v10;
                            *reinterpret_cast<int32_t*>(&rbx28) = 1;
                            *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
                            goto addr_3361_114;
                        }
                    }
                }
                addr_2e38_57:
                r12_31 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp27)));
                *reinterpret_cast<int32_t*>(&r13_102) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_102) + 4) = 0;
                r14_103 = r12_31;
                while (*reinterpret_cast<int32_t*>(&rbp27) > *reinterpret_cast<int32_t*>(&r13_102)) {
                    rdi104 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbx28) + reinterpret_cast<uint64_t>(r13_102 * 8));
                    ++r13_102;
                    rax105 = fun_2570(rdi104);
                    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                    r14_103 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_103) + reinterpret_cast<unsigned char>(rax105));
                }
                *reinterpret_cast<int32_t*>(&r13_3) = 0;
                *reinterpret_cast<int32_t*>(&r13_3 + 4) = 0;
                rax106 = xmalloc(r14_103, rsi14, r14_103, rsi14);
                rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                r15d107 = v9;
                r14_108 = rax106;
                while (*reinterpret_cast<int32_t*>(&rbp27) > *reinterpret_cast<int32_t*>(&r13_3)) {
                    rax109 = fun_2540(r14_108);
                    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                    *reinterpret_cast<void***>(rbx28 + reinterpret_cast<unsigned char>(r13_3) * 8) = r14_108;
                    ++r13_3;
                    rax109->f0 = *reinterpret_cast<signed char*>(&r15d107);
                    r14_108 = reinterpret_cast<void**>(&rax109->f1);
                }
                *reinterpret_cast<void***>(rbx28 + reinterpret_cast<unsigned char>(r12_31) * 8) = r14_108;
                continue;
                addr_30e4_63:
                rbx110 = v111;
                if (rbx110 && (*reinterpret_cast<uint32_t*>(&rcx12) = v9, *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0, *reinterpret_cast<signed char*>(&rcx12) != *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<uint64_t>(rbx110) + 0xffffffffffffffff))) {
                    rax112 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx110) + 1);
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_3) + reinterpret_cast<uint64_t>(rbx110)) = *reinterpret_cast<signed char*>(&rcx12);
                    v111 = rax112;
                    rbx110 = rax112;
                }
                rbx113 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx110) + reinterpret_cast<unsigned char>(r13_3));
                rdi114 = r13_3;
                *reinterpret_cast<int32_t*>(&r12_31) = 0;
                *reinterpret_cast<int32_t*>(&r12_31 + 4) = 0;
                while (r14_115 = r12_31 + 1, reinterpret_cast<unsigned char>(rbx113) > reinterpret_cast<unsigned char>(rdi114)) {
                    *reinterpret_cast<int32_t*>(&rsi116) = *reinterpret_cast<int32_t*>(&rbp27);
                    *reinterpret_cast<int32_t*>(&rsi116 + 4) = 0;
                    r12_31 = r14_115;
                    rax117 = fun_2660(rdi114, rsi116, rdx13, rcx12);
                    rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                    rdi114 = reinterpret_cast<void**>(&rax117->f1);
                }
                *reinterpret_cast<int32_t*>(&r14_118) = 1;
                *reinterpret_cast<int32_t*>(&r14_118 + 4) = 0;
                rax119 = xnmalloc(r14_115, 8, rdx13, rcx12);
                rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                rdi120 = r13_3;
                *reinterpret_cast<void***>(rax119) = r13_3;
                rbx28 = rax119;
                while (reinterpret_cast<unsigned char>(r12_31) >= reinterpret_cast<unsigned char>(r14_118)) {
                    *reinterpret_cast<int32_t*>(&rsi14) = *reinterpret_cast<int32_t*>(&rbp27);
                    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                    rax121 = fun_2660(rdi120, rsi14, rdx13, rcx12);
                    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                    rdi120 = reinterpret_cast<void**>(&rax121->f1);
                    *reinterpret_cast<void***>(rbx28 + reinterpret_cast<unsigned char>(r14_118) * 8) = rdi120;
                    ++r14_118;
                }
            }
        }
    }
}

int64_t __libc_start_main = 0;

void fun_35a3() {
    __asm__("cli ");
    __libc_start_main(0x28c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x10008;

void fun_2440(int64_t rdi);

int64_t fun_3643() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2440(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3683() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2650(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_2480(void** rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void** stderr = reinterpret_cast<void**>(0);

void fun_27f0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3693(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r8_6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    int32_t eax21;
    void** r13_22;
    void** rax23;
    void** rax24;
    int32_t eax25;
    void** rax26;
    void** rax27;
    void** rax28;
    int32_t eax29;
    void** rax30;
    void** r15_31;
    void** rax32;
    void** rax33;
    void** rax34;
    void** rdi35;
    void** r8_36;
    void** r9_37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2550();
            r8_6 = r12_2;
            fun_2740(1, rax5, r12_2, r12_2, 1, rax5, r12_2, r12_2);
            r12_7 = stdout;
            rax8 = fun_2550();
            fun_2650(rax8, r12_7, 5, r12_2, r8_6);
            r12_9 = stdout;
            rax10 = fun_2550();
            fun_2650(rax10, r12_9, 5, r12_2, r8_6);
            r12_11 = stdout;
            rax12 = fun_2550();
            fun_2650(rax12, r12_11, 5, r12_2, r8_6);
            r12_13 = stdout;
            rax14 = fun_2550();
            fun_2650(rax14, r12_13, 5, r12_2, r8_6);
            r12_15 = stdout;
            rax16 = fun_2550();
            fun_2650(rax16, r12_15, 5, r12_2, r8_6);
            r12_17 = stdout;
            rax18 = fun_2550();
            fun_2650(rax18, r12_17, 5, r12_2, r8_6);
            r12_19 = stdout;
            rax20 = fun_2550();
            fun_2650(rax20, r12_19, 5, r12_2, r8_6);
            do {
                if (1) 
                    break;
                eax21 = fun_2680("shuf", 0, 5, "sha512sum", "shuf", 0, 5, "sha512sum");
            } while (eax21);
            r13_22 = v4;
            if (!r13_22) {
                rax23 = fun_2550();
                fun_2740(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax24 = fun_2730(5);
                if (!rax24 || (eax25 = fun_2480(rax24, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax25)) {
                    rax26 = fun_2550();
                    r13_22 = reinterpret_cast<void**>("shuf");
                    fun_2740(1, rax26, "https://www.gnu.org/software/coreutils/", "shuf", 1, rax26, "https://www.gnu.org/software/coreutils/", "shuf");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_22 = reinterpret_cast<void**>("shuf");
                    goto addr_3a38_9;
                }
            } else {
                rax27 = fun_2550();
                fun_2740(1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax28 = fun_2730(5);
                if (!rax28 || (eax29 = fun_2480(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax29)) {
                    addr_393e_11:
                    rax30 = fun_2550();
                    fun_2740(1, rax30, "https://www.gnu.org/software/coreutils/", "shuf", 1, rax30, "https://www.gnu.org/software/coreutils/", "shuf");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_22 == "shuf")) {
                        r12_2 = reinterpret_cast<void**>(0xbbaa);
                    }
                } else {
                    addr_3a38_9:
                    r15_31 = stdout;
                    rax32 = fun_2550();
                    fun_2650(rax32, r15_31, 5, "https://www.gnu.org/software/coreutils/", r8_6);
                    goto addr_393e_11;
                }
            }
            rax33 = fun_2550();
            fun_2740(1, rax33, r13_22, r12_2, 1, rax33, r13_22, r12_2);
            addr_36ee_14:
            fun_27d0();
        }
    } else {
        rax34 = fun_2550();
        rdi35 = stderr;
        fun_27f0(rdi35, 1, rax34, r12_2, r8_36, r9_37, v38, v39, v40, v41, v42, v43);
        goto addr_36ee_14;
    }
}

int64_t file_name = 0;

void fun_3a73(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3a83(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

int32_t exit_failure = 1;

void** fun_2490(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3a93() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2470(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2550();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3b23_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2760();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2490(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3b23_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2760();
    }
}

void fun_3b43() {
    __asm__("cli ");
}

uint32_t fun_26b0(void** rdi);

void fun_3b53(void** rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_26b0(rdi);
        goto 0x2630;
    }
}

int32_t fun_26f0(void** rdi);

int32_t rpl_fflush(void** rdi);

int64_t fun_2520(void** rdi);

int64_t fun_3b83(void** rdi) {
    uint32_t eax2;
    int32_t eax3;
    uint64_t rax4;
    int32_t eax5;
    int32_t* rax6;
    int32_t r12d7;
    int64_t rax8;

    __asm__("cli ");
    eax2 = fun_26b0(rdi);
    if (reinterpret_cast<int32_t>(eax2) >= reinterpret_cast<int32_t>(0)) {
        eax3 = fun_26f0(rdi);
        if (!(eax3 && (fun_26b0(rdi), rax4 = fun_25e0(), rax4 == 0xffffffffffffffff) || (eax5 = rpl_fflush(rdi), eax5 == 0))) {
            rax6 = fun_2470();
            r12d7 = *rax6;
            rax8 = fun_2520(rdi);
            if (r12d7) {
                *rax6 = r12d7;
                *reinterpret_cast<int32_t*>(&rax8) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            }
            return rax8;
        }
    }
    goto fun_2520;
}

void rpl_fseeko(void** rdi);

void fun_3c13(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_26f0(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int32_t fun_25b0();

int32_t fun_2780(int64_t rdi);

int64_t fun_2610(int64_t rdi, int64_t rsi, void** rdx);

void fun_2620();

int64_t fun_3c63(int64_t rdi, int64_t rsi, void** rdx) {
    uint32_t eax4;
    int32_t eax5;
    int32_t* rax6;
    int32_t* rbx7;
    int32_t eax8;
    int32_t r14d9;
    int32_t r15d10;
    signed char v11;
    int32_t eax12;
    int32_t eax13;
    int32_t eax14;
    int32_t* rax15;
    int64_t rax16;
    int64_t r12_17;
    int32_t* rax18;
    int32_t ebp19;
    int32_t eax20;
    int32_t eax21;

    __asm__("cli ");
    eax4 = fun_26b0(rdx);
    if (eax4 == 1) {
        eax5 = fun_25b0();
        rax6 = fun_2470();
        rbx7 = rax6;
        if (eax5) {
            eax8 = fun_2780("/dev/null");
            if (eax8) {
                r14d9 = 0;
                r15d10 = 0;
                goto addr_3da8_5;
            } else {
                v11 = 1;
                r14d9 = 0;
                r15d10 = 0;
            }
        } else {
            addr_3ddd_7:
            v11 = 0;
            r14d9 = 0;
            r15d10 = 0;
        }
    } else {
        if (eax4 == 2) {
            r14d9 = 0;
        } else {
            if (!eax4) 
                goto addr_3ddd_7;
            eax12 = fun_25b0();
            *reinterpret_cast<unsigned char*>(&r14d9) = reinterpret_cast<uint1_t>(eax12 != 2);
        }
        eax13 = fun_25b0();
        *reinterpret_cast<unsigned char*>(&r15d10) = reinterpret_cast<uint1_t>(eax13 != 1);
        eax14 = fun_25b0();
        rax15 = fun_2470();
        rbx7 = rax15;
        if (eax14) 
            goto addr_3d90_13; else 
            goto addr_3cee_14;
    }
    rax16 = fun_2610(rdi, rsi, rdx);
    r12_17 = rax16;
    rax18 = fun_2470();
    ebp19 = *rax18;
    rbx7 = rax18;
    addr_3e03_16:
    if (*reinterpret_cast<unsigned char*>(&r14d9)) {
        addr_3d4c_17:
        fun_2620();
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) {
            addr_3e15_18:
            if (v11) {
                addr_3d75_19:
                fun_2620();
                if (r12_17) {
                    addr_3e29_20:
                    return r12_17;
                } else {
                    addr_3d85_21:
                    *rbx7 = ebp19;
                    goto addr_3e29_20;
                }
            } else {
                addr_3e20_22:
                if (!r12_17) 
                    goto addr_3d85_21; else 
                    goto addr_3e29_20;
            }
        }
    } else {
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) 
            goto addr_3e15_18;
    }
    addr_3d60_25:
    fun_2620();
    if (!v11) 
        goto addr_3e20_22; else 
        goto addr_3d75_19;
    addr_3d90_13:
    eax8 = fun_2780("/dev/null");
    if (!eax8) {
        v11 = 1;
        if (eax13 != 1) {
            addr_3cfe_27:
            eax20 = fun_2780("/dev/null");
            if (eax20 != 1) {
                if (eax20 >= 0) {
                    ebp19 = 9;
                    fun_2620();
                    *rbx7 = 9;
                } else {
                    ebp19 = *rbx7;
                }
                *reinterpret_cast<int32_t*>(&r12_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&r14d9)) {
                    fun_2620();
                    goto addr_3d60_25;
                }
            } else {
                r15d10 = 1;
            }
        } else {
            addr_3eb0_34:
            r15d10 = 0;
        }
    } else {
        addr_3da8_5:
        if (eax8 >= 0) {
            ebp19 = 9;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            fun_2620();
            *rbx7 = 9;
            v11 = 1;
            goto addr_3e03_16;
        } else {
            v11 = 1;
            ebp19 = *rbx7;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            goto addr_3e03_16;
        }
    }
    if (*reinterpret_cast<unsigned char*>(&r14d9) && (eax21 = fun_2780("/dev/null"), eax21 != 2)) {
        if (eax21 >= 0) {
            ebp19 = 9;
            fun_2620();
            *rbx7 = 9;
        } else {
            ebp19 = *rbx7;
        }
        *reinterpret_cast<int32_t*>(&r12_17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
        goto addr_3d4c_17;
    }
    addr_3cee_14:
    v11 = 0;
    if (eax13 == 1) 
        goto addr_3eb0_34; else 
        goto addr_3cfe_27;
}

int64_t fun_3f33(void** rdi, int64_t rsi, int32_t edx) {
    uint64_t rax4;
    int64_t rax5;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 72)))) {
        fun_26b0(rdi);
        rax4 = fun_25e0();
        if (rax4 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax5) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<uint64_t*>(rdi + 0x90) = rax4;
            *reinterpret_cast<uint32_t*>(&rax5) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
        return rax5;
    }
}

struct s9 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_3fb3(struct s9* rdi) {
    __asm__("cli ");
    __asm__("pxor xmm0, xmm0");
    rdi->f10 = 0;
    __asm__("movups [rdi], xmm0");
    return;
}

struct s10 {
    void* f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

uint32_t fun_2450(struct s10* rdi, struct s10* rsi, void** rdx);

struct s10* fun_3fd3(struct s10* rdi, struct s10* rsi, int32_t edx) {
    void** rdx3;
    uint32_t r13d4;
    void** r15_5;
    void* rcx6;
    int32_t v7;
    unsigned char v8;
    void** rcx9;
    struct s10* r14_10;
    struct s10* rbp11;
    void** r12_12;
    void** rax13;
    uint32_t ebx14;
    uint32_t eax15;
    uint32_t eax16;
    uint32_t r11d17;
    void* r12_18;
    void** rax19;
    void* rcx20;
    void** rax21;
    void** rax22;
    uint32_t esi23;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    __asm__("cli ");
    r13d4 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&rdx3)));
    r15_5 = rdi->f10;
    rcx6 = rdi->f0;
    v7 = *reinterpret_cast<int32_t*>(&rdx3);
    v8 = *reinterpret_cast<unsigned char*>(&r13d4);
    rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx6) + reinterpret_cast<unsigned char>(r15_5));
    if (reinterpret_cast<unsigned char>(rsi->f0) & 16) {
        addr_40c0_2:
        return 0;
    } else {
        r14_10 = rdi;
        rbp11 = rsi;
        r12_12 = r15_5;
        do {
            rax13 = rbp11->f8;
            if (reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rbp11->f10)) {
                rdx3 = rax13 + 1;
                rbp11->f8 = rdx3;
                ebx14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13));
            } else {
                eax15 = fun_2450(rbp11, rsi, rdx3);
                rcx9 = rcx9;
                ebx14 = eax15;
                if (eax15 == 0xffffffff) {
                    if (r15_5 == r12_12) 
                        goto addr_40c0_2;
                    if (reinterpret_cast<unsigned char>(rbp11->f0) & 32) 
                        goto addr_40c0_2;
                    eax16 = v8;
                    if (*reinterpret_cast<void***>(r12_12 + 0xffffffffffffffff) == *reinterpret_cast<void***>(&eax16)) 
                        break;
                    ebx14 = r13d4;
                    if (r12_12 != rcx9) 
                        goto addr_40d8_11;
                    r11d17 = v8;
                    goto addr_4080_13;
                }
            }
            r11d17 = ebx14;
            if (r12_12 == rcx9) {
                addr_4080_13:
                rsi = r14_10;
                r12_18 = r14_10->f0;
                *reinterpret_cast<int32_t*>(&rdx3) = 1;
                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                rax19 = xpalloc();
                rcx20 = r14_10->f0;
                r11d17 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r11d17));
                r15_5 = rax19;
                rax21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax19) + reinterpret_cast<unsigned char>(r12_18));
                r14_10->f10 = r15_5;
                rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx20) + reinterpret_cast<unsigned char>(r15_5));
            } else {
                rax21 = r12_12;
            }
            *reinterpret_cast<void***>(rax21) = *reinterpret_cast<void***>(&r11d17);
            r12_12 = rax21 + 1;
        } while (r13d4 != ebx14);
    }
    rax22 = r12_12;
    addr_40e6_18:
    r14_10->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax22) - reinterpret_cast<unsigned char>(r15_5));
    return r14_10;
    addr_40d8_11:
    esi23 = *reinterpret_cast<unsigned char*>(&v7);
    rax22 = r12_12 + 1;
    *reinterpret_cast<void***>(r12_12) = *reinterpret_cast<void***>(&esi23);
    goto addr_40e6_18;
}

void fun_4113() {
    __asm__("cli ");
    goto readlinebuffer_delim;
}

void fun_4123(int64_t rdi) {
    __asm__("cli ");
    goto fun_2430;
}

void fun_27e0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s11 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s11* fun_25d0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4133(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s11* rax4;
    void** r12_5;
    void** rcx6;
    void** r8_7;
    int32_t eax8;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_27e0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2460("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_25d0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax8 = fun_2480(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6, r8_7), !eax8))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_58d3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2470();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x10200;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5913(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x10200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5933(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x10200);
    }
    *rdi = esi;
    return 0x10200;
}

int64_t fun_5953(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x10200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s12 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5993(struct s12* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s12*>(0x10200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s13 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s13* fun_59b3(struct s13* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s13*>(0x10200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x285a;
    if (!rdx) 
        goto 0x285a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x10200;
}

struct s14 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_59f3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s14* r8) {
    struct s14* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s14*>(0x10200);
    }
    rax7 = fun_2470();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5a26);
    *rax7 = r15d8;
    return rax13;
}

struct s15 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5a73(int64_t rdi, int64_t rsi, void*** rdx, struct s15* rcx) {
    struct s15* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s15*>(0x10200);
    }
    rax6 = fun_2470();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5aa1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5afc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5b63() {
    __asm__("cli ");
}

void** g10078 = reinterpret_cast<void**>(0);

int64_t slotvec0 = 0x100;

void fun_5b73() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2430(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0x10100) {
        fun_2430(rdi8, rsi9);
        g10078 = reinterpret_cast<void**>(0x10100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x10070) {
        fun_2430(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0x10070);
    }
    nslots = 1;
    return;
}

void fun_5c13() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c33() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c43(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c63(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5c83(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2860;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2580();
    } else {
        return rax6;
    }
}

void** fun_5d13(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2865;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2580();
    } else {
        return rax7;
    }
}

void** fun_5da3(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x286a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2580();
    } else {
        return rax5;
    }
}

void** fun_5e33(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x286f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2580();
    } else {
        return rax6;
    }
}

void** fun_5ec3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xa330]");
    __asm__("movdqa xmm1, [rip+0xa338]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xa321]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2580();
    } else {
        return rax10;
    }
}

void** fun_5f63(int64_t rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xa290]");
    __asm__("movdqa xmm1, [rip+0xa298]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xa281]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2580();
    } else {
        return rax9;
    }
}

void** fun_6003(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa1f0]");
    __asm__("movdqa xmm1, [rip+0xa1f8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xa1d9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2580();
    } else {
        return rax3;
    }
}

void** fun_6093(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa160]");
    __asm__("movdqa xmm1, [rip+0xa168]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xa156]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2580();
    } else {
        return rax4;
    }
}

void** fun_6123(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2874;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2580();
    } else {
        return rax6;
    }
}

void** fun_61c3(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa02a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xa022]");
    __asm__("movdqa xmm2, [rip+0xa02a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2879;
    if (!rdx) 
        goto 0x2879;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2580();
    } else {
        return rax7;
    }
}

void** fun_6263(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x9f8a]");
    __asm__("movdqa xmm1, [rip+0x9f92]");
    __asm__("movdqa xmm2, [rip+0x9f9a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x287e;
    if (!rdx) 
        goto 0x287e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2580();
    } else {
        return rax9;
    }
}

void** fun_6313(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x9eda]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x9ed2]");
    __asm__("movdqa xmm2, [rip+0x9eda]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2883;
    if (!rsi) 
        goto 0x2883;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2580();
    } else {
        return rax6;
    }
}

void** fun_63b3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x9e3a]");
    __asm__("movdqa xmm1, [rip+0x9e42]");
    __asm__("movdqa xmm2, [rip+0x9e4a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2888;
    if (!rsi) 
        goto 0x2888;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2580();
    } else {
        return rax7;
    }
}

void fun_6453() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6463(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6483() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_64a3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_64c3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = xmalloc(24, rsi);
    *reinterpret_cast<void***>(rax3) = rdi;
    *reinterpret_cast<void***>(rax3 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax3 + 8) = reinterpret_cast<void**>(0);
    return;
}

int64_t randread_new();

int64_t fun_64f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = randread_new();
    if (!rax1) {
        return 0;
    }
}

int64_t fun_6513(int64_t* rdi) {
    __asm__("cli ");
    return *rdi;
}

struct s16 {
    int64_t f0;
    uint64_t f8;
    uint64_t f10;
};

void randread(int64_t rdi, void* rsi, int64_t rdx);

uint64_t fun_6523(struct s16* rdi, uint64_t rsi) {
    struct s16* r14_3;
    uint64_t r13_4;
    uint64_t rbp5;
    int64_t rcx6;
    uint64_t r12_7;
    void** rax8;
    void** v9;
    uint64_t rbx10;
    void* r15_11;
    uint64_t rdx12;
    int64_t r8_13;
    uint64_t rax14;
    unsigned char v15;
    uint64_t rax16;
    uint64_t rdx17;
    uint64_t rdx18;
    void* rax19;

    __asm__("cli ");
    r14_3 = rdi;
    r13_4 = rsi + 1;
    rbp5 = rsi;
    rcx6 = rdi->f0;
    r12_7 = rdi->f8;
    rax8 = g28;
    v9 = rax8;
    rbx10 = rdi->f10;
    r15_11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 + 16);
    while (1) {
        if (rbx10 >= rbp5) {
            if (rbx10 == rbp5) 
                break;
        } else {
            rdx12 = rbx10;
            *reinterpret_cast<int32_t*>(&r8_13) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_13) + 4) = 0;
            do {
                ++r8_13;
                rdx12 = (rdx12 << 8) + 0xff;
            } while (rbp5 > rdx12);
            randread(rcx6, r15_11, r8_13);
            rcx6 = rcx6;
            do {
                *reinterpret_cast<uint32_t*>(&rax14) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                rbx10 = (rbx10 << 8) + 0xff;
                r12_7 = (r12_7 << 8) + rax14;
            } while (rbp5 > rbx10);
            if (rbx10 == rbp5) 
                break;
        }
        rax16 = rbx10 - rbp5;
        rdx17 = rax16 % r13_4;
        rdx18 = r12_7 % r13_4;
        if (r12_7 <= rbx10 - rdx17) 
            goto addr_6630_10;
        rbx10 = rdx17 + 0xffffffffffffffff;
        r12_7 = rdx18;
    }
    r14_3->f10 = 0;
    r14_3->f8 = 0;
    addr_6607_13:
    rax19 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax19) {
        fun_2580();
    } else {
        return r12_7;
    }
    addr_6630_10:
    r14_3->f8 = r12_7 / r13_4;
    r12_7 = rdx18;
    r14_3->f10 = rax16 / r13_4;
    goto addr_6607_13;
}

void fun_2770(void** rdi, void** rsi, void** rdx, ...);

void fun_6653(void** rdi) {
    __asm__("cli ");
    fun_2770(rdi, 24, 0xffffffffffffffff);
    goto fun_2430;
}

int32_t randread_free(int64_t rdi);

void randint_free(int64_t* rdi);

int64_t fun_6683(int64_t* rdi) {
    int64_t rdi2;
    int32_t eax3;
    int32_t* rax4;
    int32_t r13d5;
    int64_t rax6;

    __asm__("cli ");
    rdi2 = *rdi;
    eax3 = randread_free(rdi2);
    rax4 = fun_2470();
    r13d5 = *rax4;
    randint_free(rdi);
    *reinterpret_cast<int32_t*>(&rax6) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    *rax4 = r13d5;
    return rax6;
}

uint64_t fun_66c3(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_66d3(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

uint64_t fun_66e3(int64_t rdi, uint64_t rsi) {
    uint64_t rsi3;

    __asm__("cli ");
    if (!rsi) {
        return 0;
    } else {
        __asm__("bsr rsi, rsi");
        rsi3 = rsi ^ 63;
        return reinterpret_cast<uint64_t>((64 - *reinterpret_cast<int32_t*>(&rsi3)) * rdi + 7) >> 3;
    }
}

int64_t hash_initialize(void* rdi);

void** hash_remove(int64_t rdi, void** rsi);

int64_t hash_insert(int64_t rdi, void** rsi);

void hash_free(int64_t rdi, void** rsi);

void xalloc_die();

void** fun_6723(void** rdi, void** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void** rax6;
    void** v7;
    void** r12_8;
    void** r13_9;
    void** rbp10;
    void** rbx11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    int64_t r14_15;
    void** rax16;
    void** rax17;
    void* rax18;
    int32_t v19;
    void** rbx20;
    void** v21;
    void** v22;
    void** rsi23;
    void** rax24;
    void** r15_25;
    void*** rax26;
    void** rsi27;
    void** rax28;
    void* rsp29;
    void** rsi30;
    void** rax31;
    void* rsp32;
    void** r9_33;
    void** r8_34;
    void** rax35;
    void** rax36;
    void** rax37;
    int64_t rax38;
    int64_t rax39;
    void* rax40;
    void** rax41;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68);
    rax6 = g28;
    v7 = rax6;
    if (!rsi) {
        *reinterpret_cast<int32_t*>(&r12_8) = 0;
        *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
    } else {
        r13_9 = rdi;
        rbp10 = rsi;
        rbx11 = rdx;
        if (!reinterpret_cast<int1_t>(rsi == 1)) {
            if (reinterpret_cast<unsigned char>(rdx) <= reinterpret_cast<unsigned char>(0x1ffff)) {
                rax12 = xnmalloc(rdx, 8, rdx, rcx);
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                r12_8 = rax12;
                if (rbx11) {
                    addr_67d7_6:
                    *reinterpret_cast<int32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    goto addr_67e0_7;
                } else {
                    goto addr_67ed_9;
                }
            } else {
                if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx) / reinterpret_cast<unsigned char>(rsi)) <= 31) {
                    rax13 = xnmalloc(rbx11, 8, reinterpret_cast<unsigned char>(rdx) % reinterpret_cast<unsigned char>(rsi), rcx);
                    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                    r12_8 = rax13;
                    goto addr_67d7_6;
                }
                rcx = reinterpret_cast<void**>(0x66d0);
                rdx = reinterpret_cast<void**>(0x66c0);
                rax14 = hash_initialize(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rsi));
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                r14_15 = rax14;
                if (!rax14) 
                    goto addr_6a0b_13; else 
                    goto addr_696c_14;
            }
        } else {
            rax16 = xmalloc(8, rsi);
            r12_8 = rax16;
            rax17 = randint_genmax(r13_9, rbx11 + 0xffffffffffffffff, rdx, rcx);
            *reinterpret_cast<void***>(r12_8) = rax17;
        }
    }
    addr_6773_16:
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (!rax18) {
        return r12_8;
    }
    addr_6a10_18:
    fun_2580();
    do {
        addr_67e0_7:
        *reinterpret_cast<void***>(r12_8 + reinterpret_cast<unsigned char>(rdx) * 8) = rdx;
        ++rdx;
    } while (reinterpret_cast<unsigned char>(rbx11) > reinterpret_cast<unsigned char>(rdx));
    addr_67ed_9:
    v19 = 0;
    *reinterpret_cast<int32_t*>(&r14_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_15) + 4) = 0;
    addr_67f8_19:
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
    v21 = rbx11 + 0xffffffffffffffff;
    v22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 64);
    while (1) {
        rsi23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v21) - reinterpret_cast<unsigned char>(rbx20));
        rax24 = randint_genmax(r13_9, rsi23, rdx, rcx, r13_9, rsi23, rdx, rcx);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        *reinterpret_cast<int32_t*>(&rdx) = v19;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        r15_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rbx20));
        if (!*reinterpret_cast<int32_t*>(&rdx)) {
            rax26 = reinterpret_cast<void***>(r12_8 + reinterpret_cast<unsigned char>(r15_25) * 8);
            rsi27 = *reinterpret_cast<void***>(r12_8 + reinterpret_cast<unsigned char>(rbx20) * 8);
            rcx = *rax26;
            *reinterpret_cast<void***>(r12_8 + reinterpret_cast<unsigned char>(rbx20) * 8) = rcx;
            ++rbx20;
            *rax26 = rsi27;
            if (reinterpret_cast<unsigned char>(rbp10) <= reinterpret_cast<unsigned char>(rbx20)) 
                break;
        } else {
            rax28 = hash_remove(r14_15, v22);
            rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
            rsi30 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp29) + 48);
            rax31 = hash_remove(r14_15, rsi30);
            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
            r9_33 = rax28;
            r8_34 = rax31;
            if (!r9_33) {
                rax35 = xmalloc(16, rsi30);
                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                r8_34 = rax31;
                *reinterpret_cast<void***>(rax35 + 8) = rbx20;
                r9_33 = rax35;
                *reinterpret_cast<void***>(rax35) = rbx20;
            }
            if (!r8_34) {
                rax36 = xmalloc(16, rsi30);
                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                r9_33 = r9_33;
                *reinterpret_cast<void***>(rax36 + 8) = r15_25;
                r8_34 = rax36;
                *reinterpret_cast<void***>(rax36) = r15_25;
            } else {
                r15_25 = *reinterpret_cast<void***>(r8_34 + 8);
            }
            rax37 = *reinterpret_cast<void***>(r9_33 + 8);
            *reinterpret_cast<void***>(r9_33 + 8) = r15_25;
            *reinterpret_cast<void***>(r8_34 + 8) = rax37;
            rax38 = hash_insert(r14_15, r9_33);
            if (!rax38) 
                goto addr_6a0b_13;
            rsi27 = r8_34;
            rax39 = hash_insert(r14_15, rsi27);
            rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
            if (!rax39) 
                goto addr_6a0b_13;
            *reinterpret_cast<void***>(r12_8 + reinterpret_cast<unsigned char>(rbx20) * 8) = *reinterpret_cast<void***>(r9_33 + 8);
            ++rbx20;
            if (reinterpret_cast<unsigned char>(rbp10) <= reinterpret_cast<unsigned char>(rbx20)) 
                break;
        }
    }
    if (!v19) {
        rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
        if (!rax40) {
            goto xreallocarray;
        }
    } else {
        hash_free(r14_15, rsi27);
        goto addr_6773_16;
    }
    addr_6a0b_13:
    xalloc_die();
    goto addr_6a10_18;
    addr_696c_14:
    rax41 = xnmalloc(rbp10, 8, 0x66c0, 0x66d0, rbp10, 8, 0x66c0, 0x66d0);
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    v19 = 1;
    r12_8 = rax41;
    goto addr_67f8_19;
}

void fun_6a23(void** rdi) {
    int32_t* rax2;

    __asm__("cli ");
    if (!rdi) 
        goto 0x288d;
    quote(rdi);
    rax2 = fun_2470();
    if (*rax2) {
        fun_2550();
    } else {
        fun_2550();
    }
    fun_2760();
    goto 0x288d;
}

void** fopen_safer();

void fun_2750(void** rdi, ...);

void* fun_2800(void*** rdi);

void isaac_seed(void*** rdi);

void** fun_6a93(void** rdi, void** rsi) {
    void** rax3;
    void** r12_4;
    void** rbp5;
    void** rax6;
    void*** r13_7;
    void*** rbx8;
    void*** rbp9;
    void** rax10;
    void** rax11;
    void* rax12;
    int32_t* rax13;
    int32_t r15d14;
    void** rbp15;

    __asm__("cli ");
    if (!rsi) {
        rax3 = xmalloc(0x1038, rsi);
        *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(0);
        r12_4 = rax3;
        *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x6a20);
        *reinterpret_cast<void***>(r12_4 + 16) = reinterpret_cast<void**>(0);
    } else {
        rbp5 = rsi;
        if (!rdi) {
            rax6 = xmalloc(0x1038, rsi);
            r12_4 = rax6;
            *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x6a20);
            r13_7 = reinterpret_cast<void***>(r12_4 + 32);
            *reinterpret_cast<void***>(r12_4 + 16) = reinterpret_cast<void**>(0);
            rbx8 = r13_7;
            *reinterpret_cast<void***>(r12_4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<unsigned char>(rbp5) > reinterpret_cast<unsigned char>(0x800)) {
                rbp5 = reinterpret_cast<void**>(0x800);
            }
            rbp9 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<uint64_t>(r13_7));
            if (reinterpret_cast<uint64_t>(r13_7) < reinterpret_cast<uint64_t>(rbp9)) 
                goto addr_6b78_7; else 
                goto addr_6b6e_8;
        } else {
            rax10 = fopen_safer();
            if (!rax10) {
                *reinterpret_cast<int32_t*>(&r12_4) = 0;
                *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
            } else {
                rax11 = xmalloc(0x1038, "rb");
                r12_4 = rax11;
                *reinterpret_cast<void***>(rax11) = rax10;
                *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x6a20);
                *reinterpret_cast<void***>(r12_4 + 16) = rdi;
                if (reinterpret_cast<unsigned char>(rbp5) <= reinterpret_cast<unsigned char>(0x1000)) {
                }
                fun_2750(rax10, rax10);
            }
        }
    }
    addr_6b0d_14:
    return r12_4;
    do {
        addr_6b78_7:
        rax12 = fun_2800(rbx8);
        if (reinterpret_cast<int64_t>(rax12) >= reinterpret_cast<int64_t>(0)) {
            rbx8 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx8) + reinterpret_cast<uint64_t>(rax12));
        } else {
            rax13 = fun_2470();
            r15d14 = *rax13;
            if (r15d14 != 4) 
                goto addr_6b9e_17;
        }
    } while (reinterpret_cast<uint64_t>(rbp9) > reinterpret_cast<uint64_t>(rbx8));
    addr_6bd8_19:
    isaac_seed(r13_7);
    goto addr_6b0d_14;
    addr_6b9e_17:
    rbp15 = *reinterpret_cast<void***>(r12_4);
    fun_2770(r12_4, 0x1038, 0x1038);
    fun_2430(r12_4, 0x1038, r12_4, 0x1038);
    if (rbp15) {
        rpl_fclose(rbp15, 0x1038, 0x1038);
    }
    *rax13 = r15d14;
    *reinterpret_cast<int32_t*>(&r12_4) = 0;
    *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
    goto addr_6b0d_14;
    addr_6b6e_8:
    goto addr_6bd8_19;
}

struct s17 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_6c33(struct s17* rdi, int64_t rsi) {
    __asm__("cli ");
    rdi->f8 = rsi;
    return;
}

struct s18 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_6c43(struct s18* rdi, int64_t rsi) {
    __asm__("cli ");
    rdi->f10 = rsi;
    return;
}

struct s19 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
    int64_t f10;
    void** f18;
    signed char[2079] pad2104;
    void** f838;
};

void fun_26a0(void** rdi, void** rsi, void** rdx, void** rcx);

void isaac_refill(int64_t rdi, void** rsi, void** rdx);

void* fun_2500(void** rdi, int64_t rsi, void** rdx, void** rcx);

void fun_6c53(struct s19* rdi, void** rsi, void** rdx) {
    void** r14_4;
    struct s19* rbp5;
    void** rbx6;
    int1_t zf7;
    void** v8;
    void** rcx9;
    int64_t r12_10;
    void** rax11;
    void** v12;
    void** r13_13;
    void** rdx14;
    int32_t* rax15;
    void** rcx16;
    int32_t* r12_17;
    void* rax18;
    int32_t edx19;
    int64_t rdi20;
    void** r13_21;
    void** r15_22;
    void* rbx23;

    __asm__("cli ");
    r14_4 = rsi;
    rbp5 = rdi;
    rbx6 = rdx;
    zf7 = rdi->f0 == 0;
    v8 = rdi->f0;
    if (zf7) {
        rcx9 = rdi->f18;
        r12_10 = reinterpret_cast<int64_t>(rdi) + 32;
        rax11 = reinterpret_cast<void**>(&rdi->f838);
        v12 = rax11;
        if (reinterpret_cast<unsigned char>(rdx) <= reinterpret_cast<unsigned char>(rcx9)) {
            r13_13 = rdx;
            v12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(rcx9) + 0x800);
        } else {
            do {
                rdx14 = rcx9;
                fun_26a0(r14_4, reinterpret_cast<uint64_t>(0x800 - reinterpret_cast<unsigned char>(rcx9)) + reinterpret_cast<unsigned char>(v12), rdx14, rcx9);
                r14_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<unsigned char>(rcx9));
                rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(rcx9));
                if (!(*reinterpret_cast<unsigned char*>(&r14_4) & 7)) 
                    goto addr_6d48_5;
                isaac_refill(r12_10, v12, rdx14);
                *reinterpret_cast<int32_t*>(&rcx9) = 0x800;
                *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            } while (reinterpret_cast<unsigned char>(rbx6) > reinterpret_cast<unsigned char>(0x800));
            goto addr_6d3f_7;
        }
    } else {
        rax15 = fun_2470();
        rcx16 = v8;
        r12_17 = rax15;
        while (rax18 = fun_2500(r14_4, 1, rbx6, rcx16), edx19 = *r12_17, r14_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<uint64_t>(rax18)), rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<uint64_t>(rax18)), !!rbx6) {
            rdi20 = rbp5->f10;
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5->f0)) & 32)) {
                edx19 = 0;
            }
            *r12_17 = edx19;
            rbp5->f8(rdi20, 1);
            rcx16 = rbp5->f0;
        }
        goto addr_6cc6_13;
    }
    addr_6d90_14:
    fun_26a0(r14_4, v12, r13_13, rcx9);
    rbp5->f18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx9) - reinterpret_cast<unsigned char>(r13_13));
    addr_6cc6_13:
    return;
    addr_6d48_5:
    r13_21 = rbx6;
    r15_22 = rbx6;
    rbx23 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<unsigned char>(rbx6));
    *reinterpret_cast<uint32_t*>(&r13_13) = *reinterpret_cast<uint32_t*>(&r13_21) & 0x7ff;
    *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
    do {
        r14_4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx23) - reinterpret_cast<unsigned char>(r15_22));
        if (r15_22 == r13_13) 
            break;
        isaac_refill(r12_10, r14_4, rdx14);
        r15_22 = r15_22 - 0x800;
    } while (r15_22);
    goto addr_6db8_17;
    isaac_refill(r12_10, v12, rdx14);
    *reinterpret_cast<int32_t*>(&rcx9) = 0x800;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    goto addr_6d90_14;
    addr_6db8_17:
    rbp5->f18 = reinterpret_cast<void**>(0);
    return;
    addr_6d3f_7:
    r13_13 = rbx6;
    goto addr_6d90_14;
}

int64_t fun_6df3(void** rdi) {
    void** r12_2;

    __asm__("cli ");
    r12_2 = *reinterpret_cast<void***>(rdi);
    fun_2770(rdi, 0x1038, 0xffffffffffffffff);
    fun_2430(rdi, 0x1038, rdi, 0x1038);
    if (!r12_2) {
        return 0;
    } else {
        goto rpl_fclose;
    }
}

struct s20 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    int64_t f18;
    signed char[992] pad1024;
    int64_t f400;
    int64_t f408;
    int64_t f410;
    int64_t f418;
    signed char[992] pad2048;
    uint64_t f800;
    int64_t f808;
    int64_t f810;
};

struct s21 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    signed char[1000] pad1024;
    uint64_t f400;
};

struct s22 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
};

void fun_6e43(struct s20* rdi, struct s21* rsi) {
    struct s20* rdx3;
    struct s21* r8_4;
    uint64_t rcx5;
    struct s20* r9_6;
    int64_t r11_7;
    struct s20* rax8;
    int64_t r11_9;
    struct s21* rdi10;
    uint64_t r10_11;
    uint64_t rsi12;
    int64_t rsi13;
    uint64_t rcx14;
    uint64_t rsi15;
    uint64_t rsi16;
    int64_t rsi17;
    uint64_t r10_18;
    uint64_t rbx19;
    uint64_t rcx20;
    uint64_t rsi21;
    int64_t rsi22;
    uint64_t rsi23;
    uint64_t rsi24;
    int64_t rsi25;
    uint64_t rbx26;
    uint64_t r11_27;
    uint64_t r10_28;
    uint64_t rcx29;
    int64_t rcx30;
    uint64_t rsi31;
    uint64_t rsi32;
    int64_t rsi33;
    uint64_t r11_34;
    int64_t r10_35;
    int64_t rsi36;
    int64_t rsi37;
    uint64_t rsi38;
    uint64_t rsi39;
    int64_t rsi40;
    struct s22* r8_41;
    struct s20* rdi42;
    uint64_t r9_43;
    uint64_t rsi44;
    int64_t rsi45;
    uint64_t rcx46;
    uint64_t rsi47;
    uint64_t rsi48;
    int64_t rsi49;
    uint64_t r9_50;
    uint64_t r11_51;
    uint64_t rcx52;
    uint64_t rsi53;
    int64_t rsi54;
    uint64_t rsi55;
    uint64_t rsi56;
    int64_t rsi57;
    uint64_t r11_58;
    uint64_t r10_59;
    uint64_t r9_60;
    uint64_t rcx61;
    int64_t rcx62;
    uint64_t rsi63;
    uint64_t rsi64;
    int64_t rsi65;
    uint64_t r10_66;
    int64_t r11_67;
    int64_t rsi68;
    int64_t rsi69;
    uint64_t rsi70;
    uint64_t rsi71;
    int64_t rsi72;

    __asm__("cli ");
    rdx3 = rdi;
    r8_4 = rsi;
    rcx5 = rdi->f800;
    r9_6 = reinterpret_cast<struct s20*>(&rdi->f400);
    r11_7 = rdi->f810 + 1;
    rax8 = rdx3;
    rdi->f810 = r11_7;
    r11_9 = r11_7 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>("^"));
    rdi10 = rsi;
    do {
        r10_11 = rax8->f0;
        rsi12 = r10_11;
        *reinterpret_cast<uint32_t*>(&rsi13) = *reinterpret_cast<uint32_t*>(&rsi12) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
        rcx14 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(~(rcx5 ^ rcx5 << 21)) + rax8->f400);
        rsi15 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi13) + rcx14 + r11_9;
        rax8->f0 = rsi15;
        rsi16 = rsi15 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi17) = *reinterpret_cast<uint32_t*>(&rsi16) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        r10_18 = r10_11 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi17);
        rdi10->f0 = r10_18;
        rbx19 = rax8->f8;
        rcx20 = (rcx14 ^ rcx14 >> 5) + rax8->f408;
        rsi21 = rbx19;
        *reinterpret_cast<uint32_t*>(&rsi22) = *reinterpret_cast<uint32_t*>(&rsi21) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi22) + 4) = 0;
        rsi23 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi22) + rcx20 + r10_18;
        rax8->f8 = rsi23;
        rsi24 = rsi23 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&rsi24) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0;
        rbx26 = rbx19 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi25);
        rdi10->f8 = rbx26;
        r11_27 = rax8->f10;
        r10_28 = (rcx20 << 12 ^ rcx20) + rax8->f410;
        rcx29 = r11_27;
        *reinterpret_cast<uint32_t*>(&rcx30) = *reinterpret_cast<uint32_t*>(&rcx29) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx30) + 4) = 0;
        rsi31 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rcx30) + r10_28 + rbx26;
        rax8->f10 = rsi31;
        rsi32 = rsi31 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi33) = *reinterpret_cast<uint32_t*>(&rsi32) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi33) + 4) = 0;
        r11_34 = r11_27 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi33);
        rdi10->f10 = r11_34;
        r10_35 = rax8->f18;
        rcx5 = (r10_28 >> 33 ^ r10_28) + rax8->f418;
        rsi36 = r10_35;
        *reinterpret_cast<uint32_t*>(&rsi37) = *reinterpret_cast<uint32_t*>(&rsi36) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi37) + 4) = 0;
        rsi38 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi37) + rcx5 + r11_34;
        rax8 = reinterpret_cast<struct s20*>(&rax8->pad1024);
        rdi10 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rdi10) + 32);
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax8) - 8) = rsi38;
        rsi39 = rsi38 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi40) = *reinterpret_cast<uint32_t*>(&rsi39) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi40) + 4) = 0;
        r11_9 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi40) + r10_35;
        *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdi10) - 8) = r11_9;
    } while (rax8 != r9_6);
    r8_41 = reinterpret_cast<struct s22*>(&r8_4->f400);
    rdi42 = reinterpret_cast<struct s20*>(&rdx3->f800);
    do {
        r9_43 = rax8->f0;
        rsi44 = r9_43;
        *reinterpret_cast<uint32_t*>(&rsi45) = *reinterpret_cast<uint32_t*>(&rsi44) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi45) + 4) = 0;
        rcx46 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(~(rcx5 ^ rcx5 << 21)) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x400));
        rsi47 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi45) + rcx46 + r11_9;
        rax8->f0 = rsi47;
        rsi48 = rsi47 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi49) = *reinterpret_cast<uint32_t*>(&rsi48) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi49) + 4) = 0;
        r9_50 = r9_43 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi49);
        r8_41->f0 = r9_50;
        r11_51 = rax8->f8;
        rcx52 = (rcx46 ^ rcx46 >> 5) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3f8);
        rsi53 = r11_51;
        *reinterpret_cast<uint32_t*>(&rsi54) = *reinterpret_cast<uint32_t*>(&rsi53) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi54) + 4) = 0;
        rsi55 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi54) + rcx52 + r9_50;
        rax8->f8 = rsi55;
        rsi56 = rsi55 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi57) = *reinterpret_cast<uint32_t*>(&rsi56) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi57) + 4) = 0;
        r11_58 = r11_51 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi57);
        r8_41->f8 = r11_58;
        r10_59 = rax8->f10;
        r9_60 = (rcx52 << 12 ^ rcx52) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3f0);
        rcx61 = r10_59;
        *reinterpret_cast<uint32_t*>(&rcx62) = *reinterpret_cast<uint32_t*>(&rcx61) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx62) + 4) = 0;
        rsi63 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rcx62) + r9_60 + r11_58;
        rax8->f10 = rsi63;
        rsi64 = rsi63 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi65) = *reinterpret_cast<uint32_t*>(&rsi64) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi65) + 4) = 0;
        r10_66 = r10_59 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi65);
        r8_41->f10 = r10_66;
        r11_67 = rax8->f18;
        rcx5 = (r9_60 >> 33 ^ r9_60) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3e8);
        rsi68 = r11_67;
        *reinterpret_cast<uint32_t*>(&rsi69) = *reinterpret_cast<uint32_t*>(&rsi68) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi69) + 4) = 0;
        rsi70 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi69) + rcx5 + r10_66;
        rax8 = reinterpret_cast<struct s20*>(&rax8->pad1024);
        r8_41 = reinterpret_cast<struct s22*>(reinterpret_cast<int64_t>(r8_41) + 32);
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax8) - 8) = rsi70;
        rsi71 = rsi70 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi72) = *reinterpret_cast<uint32_t*>(&rsi71) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi72) + 4) = 0;
        r11_9 = r11_67 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi72);
        *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r8_41) - 8) = r11_9;
    } while (rax8 != rdi42);
    rdx3->f800 = rcx5;
    *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + reinterpret_cast<int64_t>("^")) = r11_9;
    return;
}

struct s23 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    uint64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    signed char[1984] pad2048;
    int64_t f800;
    int64_t f808;
    int64_t f810;
};

void fun_70d3(struct s23* rdi) {
    struct s23* rdx2;
    struct s23* rax3;
    uint64_t rcx4;
    uint64_t r11_5;
    uint64_t rsi6;
    uint64_t r10_7;
    uint64_t r9_8;
    uint64_t r8_9;
    uint64_t r12_10;
    struct s23* rbp11;
    struct s23* rbx12;
    uint64_t rdi13;
    uint64_t rsi14;
    uint64_t rcx15;
    uint64_t r13_16;
    uint64_t r11_17;
    uint64_t r12_18;
    uint64_t rdi19;
    uint64_t rcx20;
    uint64_t r13_21;
    uint64_t r14_22;
    uint64_t rsi23;
    uint64_t r11_24;
    uint64_t r12_25;
    uint64_t rsi26;
    uint64_t rcx27;
    uint64_t rax28;
    uint64_t r11_29;
    uint64_t r12_30;
    uint64_t rdi31;
    uint64_t r8_32;
    uint64_t rcx33;
    uint64_t rax34;
    uint64_t rsi35;
    uint64_t r11_36;
    uint64_t r12_37;

    __asm__("cli ");
    rdx2 = rdi;
    rax3 = rdi;
    rcx4 = 0x98f5704f6c44c0ab;
    r11_5 = 0x48fe4a0fa5a09315;
    rsi6 = 0x82f053db8355e0ce;
    r10_7 = 0xb29b2e824a595524;
    r9_8 = 0x8c0ea5053d4712a0;
    r8_9 = 0xb9f8b322c73ac862;
    r12_10 = 0xae985bf2cbfc89ed;
    rbp11 = rdi;
    rbx12 = reinterpret_cast<struct s23*>(&rdi->f800);
    rdi13 = 0x647c4677a2884b7c;
    do {
        rsi14 = rsi6 + rax3->f20;
        rcx15 = rcx4 + rax3->f38;
        r13_16 = rax3->f0 - rsi14 + rdi13;
        r11_17 = r11_5 + rax3->f28 ^ rcx15 >> 9;
        r12_18 = r12_10 + rax3->f30 ^ r13_16 << 9;
        rdi19 = rax3->f8 - r11_17 + r8_9;
        rcx20 = rdi19 >> 23 ^ rcx15 + r13_16;
        r13_21 = rax3->f10 - r12_18 + r9_8;
        r14_22 = rax3->f18 - rcx20 + r10_7;
        rdi13 = r13_21 << 15 ^ r13_16 + rdi19;
        rsi23 = rsi14 - rdi13;
        rax3->f0 = rdi13;
        r8_9 = r14_22 >> 14 ^ rdi19 + r13_21;
        r11_24 = r11_17 - r8_9;
        rax3->f8 = r8_9;
        r9_8 = rsi23 << 20 ^ r13_21 + r14_22;
        r12_25 = r12_18 - r9_8;
        rax3->f10 = r9_8;
        r10_7 = r11_24 >> 17 ^ r14_22 + rsi23;
        r11_5 = r11_24 + r12_25;
        rcx4 = rcx20 - r10_7;
        rax3->f18 = r10_7;
        rax3 = reinterpret_cast<struct s23*>(&rax3->pad2048);
        rsi6 = r12_25 << 14 ^ rsi23 + r11_24;
        r12_10 = r12_25 + rcx4;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 24) = r11_5;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 32) = rsi6;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 16) = r12_10;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 8) = rcx4;
    } while (rbx12 != rax3);
    do {
        rsi26 = rsi6 + rdx2->f20;
        rcx27 = rcx4 + rdx2->f38;
        rax28 = rdx2->f0 - rsi26 + rdi13;
        r11_29 = r11_5 + rdx2->f28 ^ rcx27 >> 9;
        r12_30 = r12_10 + rdx2->f30 ^ rax28 << 9;
        rdi31 = rdx2->f8 - r11_29 + r8_9;
        r8_32 = rdx2->f10 - r12_30 + r9_8;
        rcx33 = rdi31 >> 23 ^ rcx27 + rax28;
        rax34 = rdx2->f18 - rcx33 + r10_7;
        rdi13 = r8_32 << 15 ^ rax28 + rdi31;
        rsi35 = rsi26 - rdi13;
        rdx2->f0 = rdi13;
        r8_9 = rax34 >> 14 ^ rdi31 + r8_32;
        r11_36 = r11_29 - r8_9;
        rdx2->f8 = r8_9;
        r9_8 = rsi35 << 20 ^ r8_32 + rax34;
        r12_37 = r12_30 - r9_8;
        rdx2->f10 = r9_8;
        r10_7 = r11_36 >> 17 ^ rax34 + rsi35;
        r11_5 = r11_36 + r12_37;
        rcx4 = rcx33 - r10_7;
        rdx2->f18 = r10_7;
        rdx2 = reinterpret_cast<struct s23*>(&rdx2->pad2048);
        rsi6 = r12_37 << 14 ^ rsi35 + r11_36;
        r12_10 = r12_37 + rcx4;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 24) = r11_5;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 32) = rsi6;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 16) = r12_10;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 8) = rcx4;
    } while (rbx12 != rdx2);
    rbp11->f810 = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rbp11) + reinterpret_cast<int64_t>("^")) = 0;
    rbp11->f800 = 0;
    return;
}

void** fun_24a0(void** rdi, void** rsi);

void** fun_24d0(void* rdi, void** rsi);

void** fun_2710(void** rdi, void** rsi);

int32_t fun_24c0(void** rdi, void** rsi);

void** fun_7333(void** rdi, int32_t esi, void** rdx) {
    void** v4;
    int32_t v5;
    void** v6;
    void** rax7;
    void** v8;
    void** rsi9;
    int32_t eax10;
    uint32_t v11;
    void** rax12;
    void** v13;
    void** r13_14;
    int32_t* rax15;
    void** rbp16;
    void** rax17;
    void* rax18;
    void** rbx19;
    void** rsi20;
    void** r15_21;
    void** rax22;
    void* rcx23;
    void** rcx24;
    void** rax25;
    void** r15_26;
    void** rax27;
    int32_t* rax28;
    int32_t r15d29;
    int32_t* r14_30;
    int32_t eax31;
    void** rdi32;
    void** rdi33;
    void** rsi34;
    void** rax35;
    void** rax36;
    void** rdi37;
    void** rsi38;
    void** rdi39;
    int32_t* rax40;
    int32_t* rax41;
    int32_t* rax42;

    __asm__("cli ");
    v4 = rdi;
    v5 = esi;
    v6 = rdx;
    rax7 = g28;
    v8 = rax7;
    fun_26b0(rdi);
    rsi9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8 - 8 + 8 + 32);
    eax10 = fun_2830();
    if (eax10 < 0 || ((v11 & 0xf000) != 0x8000 || ((rax12 = fun_24a0(v4, rsi9), reinterpret_cast<signed char>(rax12) < reinterpret_cast<signed char>(0)) || (rdx = v13, reinterpret_cast<signed char>(rdx) <= reinterpret_cast<signed char>(rax12))))) {
        *reinterpret_cast<int32_t*>(&r13_14) = 0x2000;
        *reinterpret_cast<int32_t*>(&r13_14 + 4) = 0;
    } else {
        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(rax12));
        r13_14 = rdx + 1;
        if (reinterpret_cast<int1_t>(rdx == 0x7fffffffffffffff)) {
            rax15 = fun_2470();
            *reinterpret_cast<int32_t*>(&rbp16) = 0;
            *reinterpret_cast<int32_t*>(&rbp16 + 4) = 0;
            *rax15 = 12;
            goto addr_748d_5;
        }
    }
    rax17 = fun_26c0(r13_14, rsi9, rdx);
    rbp16 = rax17;
    if (!rax17) {
        addr_748d_5:
        rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        if (rax18) {
            fun_2580();
        } else {
            return rbp16;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rbx19) = 0;
        *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        while (*reinterpret_cast<int32_t*>(&rsi20) = 1, *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0, r15_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_14) - reinterpret_cast<unsigned char>(rbx19)), rax22 = fun_24d0(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<unsigned char>(rbx19), 1), rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rax22)), r15_21 == rax22) {
            if (r13_14 == 0x7fffffffffffffff) 
                goto addr_7548_12;
            rcx23 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_14) >> 1);
            rcx24 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx23) + reinterpret_cast<unsigned char>(r13_14));
            if (reinterpret_cast<unsigned char>(0x7fffffffffffffff - reinterpret_cast<uint64_t>(rcx23)) <= reinterpret_cast<unsigned char>(r13_14)) {
                rcx24 = reinterpret_cast<void**>(0x7fffffffffffffff);
            }
            if (*reinterpret_cast<unsigned char*>(&v5) & 2) {
                rax25 = fun_26c0(rcx24, 1, r15_21);
                r15_26 = rax25;
                if (!rax25) 
                    goto addr_75ec_17;
                fun_26a0(rax25, rbp16, r13_14, rcx24);
                fun_2770(rbp16, r13_14, 0xffffffffffffffff);
                fun_2430(rbp16, r13_14, rbp16, r13_14);
            } else {
                rsi20 = rcx24;
                rax27 = fun_2710(rbp16, rsi20);
                r15_26 = rax27;
                if (!rax27) 
                    goto addr_746b_20;
            }
            r13_14 = rcx24;
            rbp16 = r15_26;
        }
    }
    rax28 = fun_2470();
    r15d29 = *rax28;
    r14_30 = rax28;
    eax31 = fun_24c0(v4, 1);
    if (eax31) {
        addr_7556_23:
        if (!(*reinterpret_cast<unsigned char*>(&v5) & 2)) {
            addr_7480_24:
            rdi32 = rbp16;
            *reinterpret_cast<int32_t*>(&rbp16) = 0;
            *reinterpret_cast<int32_t*>(&rbp16 + 4) = 0;
            fun_2430(rdi32, rsi20, rdi32, rsi20);
            *r14_30 = r15d29;
            goto addr_748d_5;
        } else {
            addr_7561_25:
            fun_2770(rbp16, r13_14, 0xffffffffffffffff, rbp16, r13_14, 0xffffffffffffffff);
            rdi33 = rbp16;
            *reinterpret_cast<int32_t*>(&rbp16) = 0;
            *reinterpret_cast<int32_t*>(&rbp16 + 4) = 0;
            fun_2430(rdi33, r13_14, rdi33, r13_14);
            *r14_30 = r15d29;
            goto addr_748d_5;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_14 + 0xffffffffffffffff) > reinterpret_cast<unsigned char>(rbx19)) {
            rsi34 = rbx19 + 1;
            if (!(*reinterpret_cast<unsigned char*>(&v5) & 2)) {
                rax35 = fun_2710(rbp16, rsi34);
                if (rax35) {
                    rbp16 = rax35;
                }
            } else {
                rax36 = fun_26c0(rsi34, rsi34, r15_21, rsi34, rsi34, r15_21);
                if (!rax36) {
                    rdi37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<unsigned char>(rbx19));
                    rsi38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_14) - reinterpret_cast<unsigned char>(rbx19));
                    fun_2770(rdi37, rsi38, 0xffffffffffffffff, rdi37, rsi38, 0xffffffffffffffff);
                } else {
                    fun_26a0(rax36, rbp16, rbx19, v4);
                    fun_2770(rbp16, r13_14, 0xffffffffffffffff, rbp16, r13_14, 0xffffffffffffffff);
                    rdi39 = rbp16;
                    rbp16 = rax36;
                    fun_2430(rdi39, r13_14, rdi39, r13_14);
                }
            }
        }
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<unsigned char>(rbx19)) = 0;
        *reinterpret_cast<void***>(v6) = rbx19;
        goto addr_748d_5;
    }
    addr_7548_12:
    rax40 = fun_2470();
    r15d29 = 12;
    r14_30 = rax40;
    goto addr_7556_23;
    addr_75ec_17:
    rax41 = fun_2470();
    r13_14 = rcx24;
    r15d29 = *rax41;
    r14_30 = rax41;
    goto addr_7561_25;
    addr_746b_20:
    rax42 = fun_2470();
    r15d29 = *rax42;
    r14_30 = rax42;
    goto addr_7480_24;
}

void** fun_27a0();

void** fun_7623() {
    uint32_t esi1;
    void** rax2;
    uint32_t r14d3;
    uint32_t esi4;
    void** rsi5;
    void** rax6;
    void** rdx7;
    int64_t rax8;

    __asm__("cli ");
    if (!(esi1 & 1)) {
    }
    rax2 = fun_27a0();
    if (rax2) {
        r14d3 = esi4 & 2;
        if (r14d3) {
            fun_2750(rax2, rax2);
        }
        *reinterpret_cast<uint32_t*>(&rsi5) = esi4;
        *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
        rax6 = fread_file(rax2, rax2);
        rax8 = rpl_fclose(rax2, rsi5, rdx7);
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            if (rax6) {
                if (r14d3) {
                    rsi5 = *reinterpret_cast<void***>(rdx7);
                    fun_2770(rax6, rsi5, 0xffffffffffffffff);
                }
                fun_2430(rax6, rsi5, rax6, rsi5);
            }
        } else {
            return rax6;
        }
    }
    return 0;
}

struct s24 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2690(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_76e3(void** rdi, void** rsi, void** rdx, void** rcx, struct s24* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_27f0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_27f0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2550();
    fun_27f0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2690(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2550();
    fun_27f0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2690(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2550();
        fun_27f0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xbe50 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xbe50;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_7b53() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s25 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_7b73(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s25* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s25* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2580();
    } else {
        return;
    }
}

void fun_7c13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_7cb6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7cc0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2580();
    } else {
        return;
    }
    addr_7cb6_5:
    goto addr_7cc0_7;
}

void fun_7cf3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2690(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2550();
    fun_2740(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2550();
    fun_2740(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2550();
    goto fun_2740;
}

int64_t fun_24e0();

void fun_7d93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7dd3(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_26c0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7df3(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_26c0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7e13(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_26c0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7e33(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2710(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7e63(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2710(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7e93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7ed3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7f13(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7f43(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7f93(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24e0();
            if (rax5) 
                break;
            addr_7fdd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_7fdd_5;
        rax8 = fun_24e0();
        if (rax8) 
            goto addr_7fc6_9;
        if (rbx4) 
            goto addr_7fdd_5;
        addr_7fc6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_8023(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24e0();
            if (rax8) 
                break;
            addr_806a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_806a_5;
        rax11 = fun_24e0();
        if (rax11) 
            goto addr_8052_9;
        if (!rbx6) 
            goto addr_8052_9;
        if (r12_4) 
            goto addr_806a_5;
        addr_8052_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_80b3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_815d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_8170_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_8110_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_8136_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_8184_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_812d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_8184_14;
            addr_812d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_8184_14;
            addr_8136_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2710(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_8184_14;
            if (!rbp13) 
                break;
            addr_8184_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_815d_9;
        } else {
            if (!r13_6) 
                goto addr_8170_10;
            goto addr_8110_11;
        }
    }
}

void** fun_2670(void** rdi, void** rsi);

void fun_81b3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2670(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_81e3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2670(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8213(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2670(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8233(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2670(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8253(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_26c0(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

void fun_8293(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_26c0(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

void fun_82d3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_26c0(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_26a0;
    }
}

void fun_8313(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_2570(rdi);
    rax5 = fun_26c0(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

void fun_8353() {
    void** rdi1;

    __asm__("cli ");
    fun_2550();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2760();
    fun_2460(rdi1);
}

int32_t xstrtoumax(void** rdi);

uint64_t fun_8393(void** rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    void** rax5;
    int32_t eax6;
    int32_t* rax7;
    int32_t* r12_8;
    uint64_t v9;
    void* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax5 = g28;
    eax6 = xstrtoumax(rdi);
    if (eax6) {
        rax7 = fun_2470();
        r12_8 = rax7;
        if (eax6 == 1) {
            addr_8430_3:
            *r12_8 = 75;
        } else {
            if (eax6 == 3) {
                *rax7 = 0;
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (rax10) {
                fun_2580();
            } else {
                return v9;
            }
        }
        rax11 = fun_2470();
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_8430_3;
        *rax11 = 34;
    }
    quote(rdi);
    if (*r12_8 != 22) 
        goto addr_844c_13;
    while (1) {
        addr_844c_13:
        if (!1) {
        }
        fun_2760();
    }
}

void xnumtoumax();

void fun_84a3() {
    __asm__("cli ");
    xnumtoumax();
    return;
}

void fun_25f0(int64_t rdi);

void** fun_2840(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_27b0(void** rdi);

void** fun_25c0(void** rdi, ...);

int64_t fun_84d3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    void* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    void** rax26;
    int64_t rax27;
    void** rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    void** rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_25f0("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2580();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_8844_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_8844_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_858d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_8595_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2470();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_2840(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_85cb_21;
    rsi = r15_14;
    rax24 = fun_27b0(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_25c0(r13_18), r8 = r8, rax26 == 0))) {
            addr_85cb_21:
            r12d11 = 4;
            goto addr_8595_13;
        } else {
            addr_8609_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_25c0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xbef8 + rbp33 * 4) + 0xbef8;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_85cb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_858d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_858d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_25c0(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_8609_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_8595_13;
}

int64_t fun_24b0();

int64_t fun_8903(void** rdi, void** rsi, void** rdx) {
    int64_t rax4;
    uint32_t ebx5;
    int64_t rax6;
    int32_t* rax7;
    int32_t* rax8;

    __asm__("cli ");
    rax4 = fun_24b0();
    ebx5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax6 = rpl_fclose(rdi, rsi, rdx);
    if (ebx5) {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            addr_895e_3:
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            rax7 = fun_2470();
            *rax7 = 0;
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            if (rax4) 
                goto addr_895e_3;
            rax8 = fun_2470();
            *reinterpret_cast<int32_t*>(&rax6) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax8 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    }
    return rax6;
}

int32_t dup_safer(int64_t rdi);

void** fun_2720(int64_t rdi, int64_t rsi);

void** fun_8973() {
    void** rax1;
    void** r12_2;
    uint32_t eax3;
    int64_t rdi4;
    int32_t eax5;
    int32_t* rax6;
    void** rdi7;
    int32_t r13d8;
    void** rsi9;
    void** rdx10;
    void** rsi11;
    void** rdx12;
    int64_t rax13;
    int64_t rdi14;
    int64_t rsi15;
    void** rax16;
    int32_t* rax17;
    int32_t r12d18;

    __asm__("cli ");
    rax1 = fun_27a0();
    r12_2 = rax1;
    if (!rax1) 
        goto addr_8996_2;
    eax3 = fun_26b0(rax1);
    if (eax3 > 2) 
        goto addr_8996_2;
    *reinterpret_cast<uint32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    eax5 = dup_safer(rdi4);
    if (eax5 < 0) {
        rax6 = fun_2470();
        rdi7 = r12_2;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        r13d8 = *rax6;
        rpl_fclose(rdi7, rsi9, rdx10);
        *rax6 = r13d8;
        goto addr_8996_2;
    } else {
        rax13 = rpl_fclose(r12_2, rsi11, rdx12);
        if (!*reinterpret_cast<int32_t*>(&rax13)) {
            *reinterpret_cast<int32_t*>(&rdi14) = eax5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
            rax16 = fun_2720(rdi14, rsi15);
            r12_2 = rax16;
            if (rax16) {
                addr_8996_2:
                return r12_2;
            }
        }
        rax17 = fun_2470();
        r12d18 = *rax17;
        fun_2620();
        *rax17 = r12d18;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        goto addr_8996_2;
    }
}

uint64_t fun_8a13(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_8a33(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s26 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_8e93(struct s26* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s27 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_8ea3(struct s27* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s28 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_8eb3(struct s28* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s31 {
    signed char[8] pad8;
    struct s31* f8;
};

struct s30 {
    int64_t f0;
    struct s31* f8;
};

struct s29 {
    struct s30* f0;
    struct s30* f8;
};

uint64_t fun_8ec3(struct s29* rdi) {
    struct s30* rcx2;
    struct s30* rsi3;
    uint64_t r8_4;
    struct s31* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s34 {
    signed char[8] pad8;
    struct s34* f8;
};

struct s33 {
    int64_t f0;
    struct s34* f8;
};

struct s32 {
    struct s33* f0;
    struct s33* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_8f23(struct s32* rdi) {
    struct s33* rcx2;
    struct s33* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s34* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s37 {
    signed char[8] pad8;
    struct s37* f8;
};

struct s36 {
    int64_t f0;
    struct s37* f8;
};

struct s35 {
    struct s36* f0;
    struct s36* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_8f93(struct s35* rdi, void** rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r13_5;
    int64_t v6;
    int64_t r12_7;
    uint64_t r12_8;
    int64_t v9;
    int64_t rbp10;
    void** rbp11;
    int64_t v12;
    int64_t rbx13;
    struct s36* rcx14;
    struct s36* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s37* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_27f0(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_27f0(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x2fbc]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_904a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_90c9_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_27f0(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_27f0;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x303b]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_90c9_14; else 
            goto addr_904a_13;
    }
}

struct s38 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s39 {
    int64_t f0;
    struct s39* f8;
};

int64_t fun_90f3(struct s38* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s38* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s39* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x289c;
    rbx7 = reinterpret_cast<struct s39*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_9158_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_914b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_914b_7;
    }
    addr_915b_10:
    return r12_3;
    addr_9158_5:
    r12_3 = rbx7->f0;
    goto addr_915b_10;
    addr_914b_7:
    return 0;
}

struct s40 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_9173(struct s40* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x28a1;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_91af_7;
    return *rax2;
    addr_91af_7:
    goto 0x28a1;
}

struct s42 {
    int64_t f0;
    struct s42* f8;
};

struct s41 {
    int64_t* f0;
    struct s42* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_91c3(struct s41* rdi, int64_t rsi) {
    struct s41* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s42* rax7;
    struct s42* rdx8;
    struct s42* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x28a7;
    rax7 = reinterpret_cast<struct s42*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_920e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_920e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_922c_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_922c_10:
    return r8_10;
}

struct s44 {
    int64_t f0;
    struct s44* f8;
};

struct s43 {
    struct s44* f0;
    struct s44* f8;
};

void fun_9253(struct s43* rdi, int64_t rsi, uint64_t rdx) {
    struct s44* r9_4;
    uint64_t rax5;
    struct s44* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_9292_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_9292_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s46 {
    int64_t f0;
    struct s46* f8;
};

struct s45 {
    struct s46* f0;
    struct s46* f8;
};

int64_t fun_92a3(struct s45* rdi, int64_t rsi, int64_t rdx) {
    struct s46* r14_4;
    int64_t r12_5;
    struct s45* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s46* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_92cf_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_9311_10;
            }
            addr_92cf_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_92d9_11:
    return r12_5;
    addr_9311_10:
    goto addr_92d9_11;
}

uint64_t fun_9323(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s47 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_9363(struct s47* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_9393(uint64_t rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8) {
    void** r15_6;
    void** rbp7;
    int64_t rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    void** rax12;
    void** rax13;
    void** rdi14;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x8a10);
    }
    if (!rcx) {
        rbx8 = 0x8a30;
    }
    rax9 = fun_26c0(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0xc050);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((*reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax12 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&rsi)), *reinterpret_cast<void***>(r12_10 + 16) = rax12, rax12 == 0) || (*reinterpret_cast<uint32_t*>(&rsi) = 16, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax13 = fun_2670(rax12, 16), *reinterpret_cast<void***>(r12_10) = rax13, rax13 == 0))) {
            rdi14 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_2430(rdi14, rsi);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<int64_t*>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int64_t*>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s50 {
    int64_t f0;
    struct s50* f8;
};

struct s49 {
    int64_t f0;
    struct s50* f8;
};

struct s48 {
    struct s49* f0;
    struct s49* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s50* f48;
};

void fun_9493(struct s48* rdi) {
    struct s48* rbp2;
    struct s49* r12_3;
    struct s50* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s50* rax7;
    struct s50* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s51 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_9543(struct s51* rdi, void** rsi) {
    struct s51* r12_3;
    void** r13_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rdi9;
    void** rbx10;
    void** rbx11;
    void** rdi12;
    void** rdi13;

    __asm__("cli ");
    r12_3 = rdi;
    r13_4 = rdi->f0;
    rax5 = rdi->f8;
    rbp6 = r13_4;
    if (!rdi->f40 || !rdi->f20) {
        addr_95b3_2:
        if (reinterpret_cast<unsigned char>(rax5) > reinterpret_cast<unsigned char>(rbp6)) {
            do {
                rbx7 = *reinterpret_cast<void***>(rbp6 + 8);
                if (rbx7) {
                    do {
                        rdi8 = rbx7;
                        rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
                        fun_2430(rdi8, rsi);
                    } while (rbx7);
                }
                rbp6 = rbp6 + 16;
            } while (reinterpret_cast<unsigned char>(r12_3->f8) > reinterpret_cast<unsigned char>(rbp6));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax5)) {
            while (1) {
                rdi9 = *reinterpret_cast<void***>(r13_4);
                if (!rdi9) {
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                } else {
                    rbx10 = r13_4;
                    while (r12_3->f40(rdi9), rbx10 = *reinterpret_cast<void***>(rbx10 + 8), !!rbx10) {
                        rdi9 = *reinterpret_cast<void***>(rbx10);
                    }
                    rax5 = r12_3->f8;
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                }
            }
            rbp6 = r12_3->f0;
            goto addr_95b3_2;
        }
    }
    rbx11 = r12_3->f48;
    if (rbx11) {
        do {
            rdi12 = rbx11;
            rbx11 = *reinterpret_cast<void***>(rbx11 + 8);
            fun_2430(rdi12, rsi);
        } while (rbx11);
    }
    rdi13 = r12_3->f0;
    fun_2430(rdi13, rsi);
    goto fun_2430;
}

int64_t fun_9633(void** rdi, uint64_t rsi) {
    void** r12_3;
    void** rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    void** r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi + 40);
    rax4 = g28;
    esi5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 16));
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_9770_2;
    if (*reinterpret_cast<void***>(rdi + 16) == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_2670(rax6, 16);
        if (!rax8) {
            addr_9770_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8) - 8 + 8);
            v10 = *reinterpret_cast<void***>(rdi + 72);
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = *reinterpret_cast<void***>(rdi);
                fun_2430(rdi12, rdi);
                *reinterpret_cast<void***>(rdi) = rax8;
                *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                *reinterpret_cast<void***>(rdi + 16) = rax6;
                *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rdi + 72) = v10;
            } else {
                *reinterpret_cast<void***>(rdi + 72) = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x28ac;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x28ac;
                fun_2430(rax8, r13_9);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_2580();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s52 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_97c3(void** rdi, void** rsi, void*** rdx) {
    void** rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s52* v17;
    int32_t r8d18;
    void** rax19;
    void* rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x28b1;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_98de_5; else 
                goto addr_984f_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_984f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_98de_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x2741]");
            if (!cf14) 
                goto addr_9935_12;
            __asm__("comiss xmm4, [rip+0x26f1]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x26b0]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_9935_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x28b1;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_26c0(16, rsi, rdx6);
                if (!rax19) {
                    addr_9935_12:
                    r8d18 = -1;
                } else {
                    goto addr_9892_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_9892_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_980e_28:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2580();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_9892_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_980e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_99e3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2580();
    } else {
        return rax3;
    }
}

void** fun_9a43(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_9ad0_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_9b86_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x255e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x24c8]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_2430(rdi15, rsi, rdi15, rsi);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_9b86_5; else 
                goto addr_9ad0_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_2580();
    } else {
        return r12_7;
    }
}

void fun_9bd3() {
    __asm__("cli ");
    goto hash_remove;
}

signed char* fun_26e0(int64_t rdi);

signed char* fun_9be3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_26e0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_25a0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_9c23(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_25a0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2580();
    } else {
        return r12_7;
    }
}

void fun_9cb3() {
    __asm__("cli ");
}

uint32_t fun_24f0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_9cd3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    int32_t* rax20;
    int32_t r13d21;
    uint32_t eax22;
    int32_t* rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_24f0(rdi);
        r12d10 = eax9;
        goto addr_9dd4_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_24f0(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_9dd4_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2580();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_9e89_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_24f0(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_24f0(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_2470();
                    r12d10 = 0xffffffff;
                    r13d21 = *rax20;
                    fun_2620();
                    *rax20 = r13d21;
                    goto addr_9dd4_3;
                }
            }
        } else {
            eax22 = fun_24f0(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_2470(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, *rax23 != 22)) {
                have_dupfd_cloexec_0 = 1;
                goto addr_9dd4_3;
            } else {
                eax25 = fun_24f0(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_9dd4_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_9e89_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_9d39_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_9d3d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_9d3d_18:
            if (0) {
            }
        } else {
            addr_9d85_23:
            eax28 = fun_24f0(rdi);
            r12d10 = eax28;
            goto addr_9dd4_3;
        }
        eax29 = fun_24f0(rdi);
        r12d10 = eax29;
        goto addr_9dd4_3;
    }
    if (0) {
    }
    eax30 = fun_24f0(rdi);
    r12d10 = eax30;
    goto addr_9dd4_3;
    addr_9d39_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_9d3d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_9d85_23;
        goto addr_9d3d_18;
    }
}

int32_t setlocale_null_r();

int64_t fun_9f43() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2580();
    } else {
        return rax3;
    }
}

int64_t fun_9fc3(int64_t rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    int32_t r13d6;
    void** rax7;
    int64_t rax8;

    __asm__("cli ");
    rax5 = fun_2730(rdi);
    if (!rax5) {
        r13d6 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax7 = fun_2570(rax5);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax7)) {
            fun_26a0(rsi, rax5, rax7 + 1, rcx);
            return 0;
        } else {
            r13d6 = 34;
            if (rdx) {
                fun_26a0(rsi, rax5, rdx + 0xffffffffffffffff, rcx);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax8) = r13d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
}

void fun_a073() {
    __asm__("cli ");
    goto fun_2730;
}

void fun_a083() {
    __asm__("cli ");
}

void fun_a097() {
    __asm__("cli ");
    return;
}

void fun_2a55() {
    void** rdx1;
    void** v2;
    void** rcx3;
    int32_t eax4;

    rdx1 = optarg;
    if (v2) {
        eax4 = fun_2680(v2, rdx1, rdx1, rcx3);
        if (eax4) 
            goto 0x3555;
    }
    goto 0x2988;
}

void fun_2a8a() {
    void** rdi1;
    int32_t eax2;
    uint64_t v3;
    uint64_t v4;

    rdi1 = optarg;
    eax2 = xstrtoumax(rdi1);
    if (eax2) 
        goto 0x2bf5;
    if (v3 <= v4) {
    }
    goto 0x2988;
}

uint64_t xdectoumax(void** rdi);

void fun_2acc() {
    int64_t v1;
    void** r11_2;
    void** rax3;
    void** rdi4;
    uint64_t rax5;
    uint64_t rax6;
    void** rdi7;

    v1 = reinterpret_cast<int64_t>(__return_address());
    r11_2 = optarg;
    rax3 = fun_25c0(r11_2, r11_2);
    if (*reinterpret_cast<void***>(&v1)) 
        goto 0x3579;
    if (!rax3) {
        fun_2550();
        xdectoumax(r11_2);
    } else {
        *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(0);
        fun_2550();
        rdi4 = optarg;
        rax5 = xdectoumax(rdi4);
        *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(45);
        fun_2550();
        rax6 = xdectoumax(rax3 + 1);
        if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(rax6 - rax5 + 1 == 0)) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(rax5 > rax6)) && rax3) {
            goto 0x2988;
        }
    }
    rdi7 = optarg;
    quote(rdi7, rdi7);
    fun_2550();
    fun_2470();
    fun_2760();
}

void fun_2bb7() {
    goto 0x2988;
}

void fun_2c3e() {
    goto 0x2988;
}

uint32_t fun_2640(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2820(int64_t rdi, void** rsi);

uint32_t fun_2810(void** rdi, void** rsi);

void fun_4365() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2550();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2550();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2570(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4663_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4663_22; else 
                            goto addr_4a5d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4b1d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4e70_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4660_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4660_30; else 
                                goto addr_4e89_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2570(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4e70_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2640(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4e70_28; else 
                            goto addr_450c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4fd0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4e50_40:
                        if (r11_27 == 1) {
                            addr_49dd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4f98_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4617_44;
                            }
                        } else {
                            goto addr_4e60_46;
                        }
                    } else {
                        addr_4fdf_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_49dd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4663_22:
                                if (v47 != 1) {
                                    addr_4bb9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2570(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4c04_54;
                                    }
                                } else {
                                    goto addr_4670_56;
                                }
                            } else {
                                addr_4615_57:
                                ebp36 = 0;
                                goto addr_4617_44;
                            }
                        } else {
                            addr_4e44_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_4fdf_47; else 
                                goto addr_4e4e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_49dd_41;
                        if (v47 == 1) 
                            goto addr_4670_56; else 
                            goto addr_4bb9_52;
                    }
                }
                addr_46d1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4568_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_458d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4890_65;
                    } else {
                        addr_46f9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4f48_67;
                    }
                } else {
                    goto addr_46f0_69;
                }
                addr_45a1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_45ec_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4f48_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_45ec_81;
                }
                addr_46f0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_458d_64; else 
                    goto addr_46f9_66;
                addr_4617_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_46cf_91;
                if (v22) 
                    goto addr_462f_93;
                addr_46cf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_46d1_62;
                addr_4c04_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_538b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_53fb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_51ff_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2820(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2810(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_4cfe_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_46bc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4d08_112;
                    }
                } else {
                    addr_4d08_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4dd9_114;
                }
                addr_46c8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_46cf_91;
                while (1) {
                    addr_4dd9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_52e7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4d46_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_52f5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4dc7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4dc7_128;
                        }
                    }
                    addr_4d75_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4dc7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_4d46_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4d75_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_45ec_81;
                addr_52f5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4f48_67;
                addr_538b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_4cfe_109;
                addr_53fb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_4cfe_109;
                addr_4670_56:
                rax93 = fun_2840(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_46bc_110;
                addr_4e4e_59:
                goto addr_4e50_40;
                addr_4b1d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4663_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_46c8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4615_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4663_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4b62_160;
                if (!v22) 
                    goto addr_4f37_162; else 
                    goto addr_5143_163;
                addr_4b62_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4f37_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4f48_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4a0b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4873_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_45a1_70; else 
                    goto addr_4887_169;
                addr_4a0b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4568_63;
                goto addr_46f0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4e44_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4f7f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4660_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4558_178; else 
                        goto addr_4f02_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4e44_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4663_22;
                }
                addr_4f7f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4660_30:
                    r8d42 = 0;
                    goto addr_4663_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_46d1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4f98_42;
                    }
                }
                addr_4558_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4568_63;
                addr_4f02_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4e60_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_46d1_62;
                } else {
                    addr_4f12_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4663_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_56c2_188;
                if (v28) 
                    goto addr_4f37_162;
                addr_56c2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4873_168;
                addr_450c_37:
                if (v22) 
                    goto addr_5503_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4523_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4fd0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_505b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4663_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4558_178; else 
                        goto addr_5037_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4e44_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4663_22;
                }
                addr_505b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4663_22;
                }
                addr_5037_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4e60_46;
                goto addr_4f12_186;
                addr_4523_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4663_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4663_22; else 
                    goto addr_4534_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_560e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5494_210;
            if (1) 
                goto addr_5492_212;
            if (!v29) 
                goto addr_50ce_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_5601_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4890_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_464b_219; else 
            goto addr_48aa_220;
        addr_462f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4643_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_48aa_220; else 
            goto addr_464b_219;
        addr_51ff_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_48aa_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_521d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_5690_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_50f6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_52e7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4643_221;
        addr_5143_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4643_221;
        addr_4887_169:
        goto addr_4890_65;
        addr_560e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_48aa_220;
        goto addr_521d_222;
        addr_5494_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_54ee_236;
        fun_2580();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5690_225;
        addr_5492_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5494_210;
        addr_50ce_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5494_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_50f6_226;
        }
        addr_5601_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4a5d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb8ec + rax113 * 4) + 0xb8ec;
    addr_4e89_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb9ec + rax114 * 4) + 0xb9ec;
    addr_5503_190:
    addr_464b_219:
    goto 0x4330;
    addr_4534_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb7ec + rax115 * 4) + 0xb7ec;
    addr_54ee_236:
    goto v116;
}

void fun_4550() {
}

void fun_4708() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4402;
}

void fun_4761() {
    goto 0x4402;
}

void fun_484e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x46d1;
    }
    if (v2) 
        goto 0x5143;
    if (!r10_3) 
        goto addr_52ae_5;
    if (!v4) 
        goto addr_517e_7;
    addr_52ae_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_517e_7:
    goto 0x4584;
}

void fun_486c() {
}

void fun_4917() {
    signed char v1;

    if (v1) {
        goto 0x489f;
    } else {
        goto 0x45da;
    }
}

void fun_4931() {
    signed char v1;

    if (!v1) 
        goto 0x492a; else 
        goto "???";
}

void fun_4958() {
    goto 0x4873;
}

void fun_49d8() {
}

void fun_49f0() {
}

void fun_4a1f() {
    goto 0x4873;
}

void fun_4a71() {
    goto 0x4a00;
}

void fun_4aa0() {
    goto 0x4a00;
}

void fun_4ad3() {
    goto 0x4a00;
}

void fun_4ea0() {
    goto 0x4558;
}

void fun_519e() {
    signed char v1;

    if (v1) 
        goto 0x5143;
    goto 0x4584;
}

void fun_5245() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4584;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4568;
        goto 0x4584;
    }
}

void fun_5662() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x48d0;
    } else {
        goto 0x4402;
    }
}

void fun_77b8() {
    fun_2550();
}

void fun_867c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x868b;
}

void fun_874c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x8759;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x868b;
}

void fun_8770() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_879c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x868b;
}

void fun_87bd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x868b;
}

void fun_87e1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x868b;
}

void fun_8805() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8794;
}

void fun_8829() {
}

void fun_8849() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8794;
}

void fun_8865() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8794;
}

void fun_478e() {
    goto 0x4402;
}

void fun_4964() {
    goto 0x491c;
}

void fun_4a2b() {
    goto 0x4558;
}

void fun_4a7d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4a00;
    goto 0x462f;
}

void fun_4aaf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4a0b;
        goto 0x4430;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x48aa;
        goto 0x464b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5248;
    if (r10_8 > r15_9) 
        goto addr_4995_9;
    addr_499a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5253;
    goto 0x4584;
    addr_4995_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_499a_10;
}

void fun_4ae2() {
    goto 0x4617;
}

void fun_4eb0() {
    goto 0x4617;
}

void fun_564f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x476c;
    } else {
        goto 0x48d0;
    }
}

void fun_7870() {
}

void fun_871f() {
    if (__intrinsic()) 
        goto 0x8759; else 
        goto "???";
}

void fun_4aec() {
    goto 0x4a87;
}

void fun_4eba() {
    goto 0x49dd;
}

void fun_78d0() {
    fun_2550();
    goto fun_27f0;
}

void fun_47bd() {
    goto 0x4402;
}

void fun_4af8() {
    goto 0x4a87;
}

void fun_4ec7() {
    goto 0x4a2e;
}

void fun_7910() {
    fun_2550();
    goto fun_27f0;
}

void fun_47ea() {
    goto 0x4402;
}

void fun_4b04() {
    goto 0x4a00;
}

void fun_7950() {
    fun_2550();
    goto fun_27f0;
}

void fun_480c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x51a0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x46d1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x46d1;
    }
    if (v11) 
        goto 0x5503;
    if (r10_12 > r15_13) 
        goto addr_5553_8;
    addr_5558_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5291;
    addr_5553_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5558_9;
}

struct s53 {
    signed char[24] pad24;
    int64_t f18;
};

struct s54 {
    signed char[16] pad16;
    void** f10;
};

struct s55 {
    signed char[8] pad8;
    void** f8;
};

void fun_79a0() {
    int64_t r15_1;
    struct s53* rbx2;
    void** r14_3;
    struct s54* rbx4;
    void** r13_5;
    struct s55* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2550();
    fun_27f0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x79c2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_79f8() {
    fun_2550();
    goto 0x79c9;
}

struct s56 {
    signed char[32] pad32;
    int64_t f20;
};

struct s57 {
    signed char[24] pad24;
    int64_t f18;
};

struct s58 {
    signed char[16] pad16;
    void** f10;
};

struct s59 {
    signed char[8] pad8;
    void** f8;
};

struct s60 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_7a30() {
    int64_t rcx1;
    struct s56* rbx2;
    int64_t r15_3;
    struct s57* rbx4;
    void** r14_5;
    struct s58* rbx6;
    void** r13_7;
    struct s59* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s60* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2550();
    fun_27f0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x7a64, __return_address(), rcx1);
    goto v15;
}

void fun_7aa8() {
    fun_2550();
    goto 0x7a6b;
}
