byte main(int param_1,undefined8 *param_2)

{
  char cVar1;
  char cVar2;
  int iVar3;
  char *pcVar4;
  char *pcVar5;
  char *pcVar6;
  char **ppcVar7;
  int iVar8;
  long lVar9;
  bool bVar10;
  undefined8 uVar11;
  
  bVar10 = false;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  exit_failure = 2;
  atexit(close_stdout);
  while( true ) {
    uVar11 = 0x102668;
    iVar3 = getopt_long(param_1,param_2,"+iu:0",longopts,0);
    if (iVar3 == -1) break;
    if (iVar3 == -0x82) {
                    /* WARNING: Subroutine does not return */
      usage(0);
    }
    bVar10 = true;
    if (iVar3 != 0x30) {
      if (iVar3 == -0x83) {
        version_etc(stdout,"printenv","GNU coreutils",Version,"David MacKenzie","Richard Mlynarik",0
                    ,uVar11);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
                    /* WARNING: Subroutine does not return */
      usage(2);
    }
  }
  lVar9 = (long)optind;
  if (optind < param_1) {
    iVar3 = 0;
    do {
      pcVar4 = strchr((char *)param_2[lVar9],0x3d);
      if (pcVar4 == (char *)0x0) {
        iVar8 = 0;
        for (ppcVar7 = environ; pcVar4 = *ppcVar7, pcVar4 != (char *)0x0; ppcVar7 = ppcVar7 + 1) {
          cVar2 = *pcVar4;
          pcVar6 = (char *)param_2[lVar9];
          while (cVar2 != '\0') {
            while( true ) {
              pcVar5 = pcVar4;
              cVar1 = *pcVar6;
              if (cVar1 == '\0') goto LAB_001027df;
              pcVar4 = pcVar5 + 1;
              pcVar6 = pcVar6 + 1;
              if (cVar2 != cVar1) goto LAB_001027df;
              cVar2 = pcVar5[1];
              if (cVar2 != '=') break;
              if (*pcVar6 == '\0') {
                __printf_chk(1,&DAT_001070ef,pcVar5 + 2,-!bVar10 & 10);
                iVar8 = 1;
                goto LAB_001027df;
              }
            }
          }
LAB_001027df:
        }
        iVar3 = iVar3 + iVar8;
      }
      lVar9 = lVar9 + 1;
    } while ((int)lVar9 < param_1);
    bVar10 = param_1 - optind == iVar3;
  }
  else {
    for (ppcVar7 = environ; *ppcVar7 != (char *)0x0; ppcVar7 = ppcVar7 + 1) {
      __printf_chk(1,&DAT_001070ef,*ppcVar7,-!bVar10 & 10);
    }
    bVar10 = true;
  }
  return bVar10 ^ 1;
}