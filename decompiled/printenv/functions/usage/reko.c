void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002560(fn0000000000002390(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000295E;
	}
	fn0000000000002500(fn0000000000002390(0x05, "Usage: %s [OPTION]... [VARIABLE]...\nPrint the values of the specified environment VARIABLE(s).\nIf no VARIABLE is specified, print name and value pairs for them all.\n\n", null), 0x01);
	fn0000000000002440(stdout, fn0000000000002390(0x05, "  -0, --null     end each output line with NUL, not newline\n", null));
	fn0000000000002440(stdout, fn0000000000002390(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002440(stdout, fn0000000000002390(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002500(fn0000000000002390(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_630 * rbx_158 = fp - 0xB8 + 16;
	do
	{
		char * rsi_160 = rbx_158->qw0000;
		++rbx_158;
	} while (rsi_160 != null && fn0000000000002460(rsi_160, "printenv") != 0x00);
	ptr64 r13_173 = rbx_158->qw0008;
	if (r13_173 != 0x00)
	{
		fn0000000000002500(fn0000000000002390(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_260 = fn00000000000024F0(null, 0x05);
		if (rax_260 == 0x00 || fn0000000000002320(0x03, "en_", rax_260) == 0x00)
			goto l0000000000002B36;
	}
	else
	{
		fn0000000000002500(fn0000000000002390(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_202 = fn00000000000024F0(null, 0x05);
		if (rax_202 == 0x00 || fn0000000000002320(0x03, "en_", rax_202) == 0x00)
		{
			fn0000000000002500(fn0000000000002390(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002B73:
			fn0000000000002500(fn0000000000002390(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000295E:
			fn0000000000002540(edi);
		}
		r13_173 = 0x7004;
	}
	fn0000000000002440(stdout, fn0000000000002390(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002B36:
	fn0000000000002500(fn0000000000002390(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002B73;
}