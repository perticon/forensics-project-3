
struct s0 {
    void* f0;
    signed char[4] pad8;
    struct s0* f8;
    struct s0* f10;
    struct s0* f18;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int64_t fun_23a0();

int64_t fun_2300(struct s0* rdi, ...);

void* quotearg_buffer_restyled(struct s0* rdi, void* rsi, struct s0* rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_23a0();
    if (r8d > 10) {
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x74c0 + rax11 * 4) + 0x74c0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

struct s2 {
    unsigned char f0;
    unsigned char f1;
};

struct s2* g28;

int32_t* fun_2310(struct s0* rdi);

struct s0* slotvec = reinterpret_cast<struct s0*>(0xb070);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_2420();

struct s3 {
    void* f0;
    signed char[4] pad8;
    struct s0* f8;
};

void fun_22f0(struct s0* rdi);

struct s0* xcharalloc(void* rdi, ...);

void fun_23c0();

struct s0* quotearg_n_options(struct s0* rdi, struct s0* rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s2* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    void* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void* rax23;
    void* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x430f;
    rax8 = fun_2310(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
        fun_2300(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6bc1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x439b;
            fun_2420();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<int64_t>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->pad40, v21, v20, v7);
        if (reinterpret_cast<uint32_t>(rsi18) <= reinterpret_cast<uint32_t>(rax23)) {
            rsi24 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rax23) + 1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb100) {
                fun_22f0(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x442a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_23c0();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s2* gettext_quote_part_0(struct s2* rdi, int32_t esi, struct s2* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s2* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s2* rax10;
    struct s2* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s2*>(0x745b);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax7 = reinterpret_cast<struct s2*>(0x7454);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s2*>(0x745f);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax10 = reinterpret_cast<struct s2*>(0x7450);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s2*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s2*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae78 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae78;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_22e3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_22f3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_2303() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2313() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_2323() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_2333() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_2343() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x2090;

void fun_2353() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20a0;

void fun_2363() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20b0;

void fun_2373() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20c0;

void fun_2383() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20d0;

void fun_2393() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20e0;

void fun_23a3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x20f0;

void fun_23b3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2100;

void fun_23c3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2110;

void fun_23d3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2120;

void fun_23e3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2130;

void fun_23f3() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2140;

void fun_2403() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2150;

void fun_2413() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2160;

void fun_2423() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x2170;

void fun_2433() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2180;

void fun_2443() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2190;

void fun_2453() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21a0;

void fun_2463() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21b0;

void fun_2473() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x21c0;

void fun_2483() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x21d0;

void fun_2493() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x21e0;

void fun_24a3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x21f0;

void fun_24b3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2200;

void fun_24c3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2210;

void fun_24d3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2220;

void fun_24e3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2230;

void fun_24f3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2240;

void fun_2503() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2250;

void fun_2513() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2260;

void fun_2523() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x2270;

void fun_2533() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2280;

void fun_2543() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2290;

void fun_2553() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x22a0;

void fun_2563() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x22b0;

void fun_2573() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x22c0;

void fun_2583() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x22d0;

void fun_2593() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s2* rdi);

struct s2* fun_24f0(int64_t rdi, ...);

void fun_2380(int64_t rdi, int64_t rsi);

void fun_2360(int64_t rdi, int64_t rsi);

int32_t exit_failure = 1;

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_23d0(int64_t rdi, struct s2** rsi, struct s2* rdx, struct s2* rcx);

uint32_t optind = 0;

int64_t fun_23f0(struct s2* rdi, int64_t rsi, struct s2* rdx, struct s2* rcx, struct s2** r8, int64_t r9);

struct s2** __environ = reinterpret_cast<struct s2**>(0);

void fun_2500(int64_t rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s2** r8, ...);

void usage();

struct s0* stdout = reinterpret_cast<struct s0*>(0);

struct s2* Version = reinterpret_cast<struct s2*>(0xeb);

void version_etc(struct s0* rdi, int64_t rsi, struct s2* rdx, struct s2* rcx, struct s2** r8, int64_t r9, int64_t a7, int64_t a8);

void fun_2540();

int64_t fun_25e3(uint32_t edi, struct s2** rsi) {
    uint32_t r13d3;
    uint32_t ebp4;
    struct s2** rbx5;
    struct s2* rdi6;
    struct s2** r8_7;
    struct s2* rcx8;
    struct s2* rdx9;
    int64_t rdi10;
    int64_t v11;
    int32_t eax12;
    int64_t r15_13;
    uint32_t r12d14;
    uint32_t r13d15;
    struct s2* rdi16;
    int64_t r9_17;
    int64_t rax18;
    uint32_t esi19;
    struct s2* rcx20;
    uint32_t ebp21;
    struct s2** rbx22;
    uint32_t ebp23;
    struct s2* rdx24;
    struct s2* rcx25;
    uint32_t eax26;
    int64_t rax27;
    struct s0* rdi28;

    __asm__("cli ");
    r13d3 = 0;
    ebp4 = edi;
    rbx5 = rsi;
    rdi6 = *rsi;
    set_program_name(rdi6);
    fun_24f0(6, 6);
    fun_2380("coreutils", "/usr/local/share/locale");
    fun_2360("coreutils", "/usr/local/share/locale");
    exit_failure = 2;
    atexit(0x2c80, "/usr/local/share/locale");
    do {
        *reinterpret_cast<int32_t*>(&r8_7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_7) + 4) = 0;
        rcx8 = reinterpret_cast<struct s2*>(0xaba0);
        rdx9 = reinterpret_cast<struct s2*>("+iu:0");
        *reinterpret_cast<uint32_t*>(&rdi10) = ebp4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
        v11 = 0x2668;
        eax12 = fun_23d0(rdi10, rbx5, "+iu:0", 0xaba0);
        if (eax12 == -1) 
            break;
        if (eax12 == 0xffffff7e) 
            goto addr_27fe_4;
        r13d3 = 1;
    } while (eax12 == 48);
    goto addr_2683_6;
    addr_26d6_7:
    r15_13 = reinterpret_cast<int32_t>(optind);
    if (*reinterpret_cast<int32_t*>(&r15_13) < reinterpret_cast<int32_t>(ebp4)) {
        r12d14 = 0;
        r13d15 = r13d3 - (r13d3 + reinterpret_cast<uint1_t>(r13d3 < r13d3 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&r13d3) < 1))) & 10;
        do {
            rdi16 = rbx5[r15_13 * 8];
            rax18 = fun_23f0(rdi16, 61, rdx9, rcx8, r8_7, r9_17);
            if (!rax18) {
                r8_7 = __environ;
                *reinterpret_cast<uint32_t*>(&r9_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_17) + 4) = 0;
                while (rcx8 = *r8_7, !!rcx8) {
                    *reinterpret_cast<uint32_t*>(&rax18) = *reinterpret_cast<unsigned char*>(&rcx8->f0);
                    rdx9 = rbx5[r15_13 * 8];
                    if (*reinterpret_cast<unsigned char*>(&rax18)) {
                        while ((esi19 = *reinterpret_cast<unsigned char*>(&rdx9->f0), !!*reinterpret_cast<unsigned char*>(&esi19)) && (rdx9 = reinterpret_cast<struct s2*>(&rdx9->f1), *reinterpret_cast<unsigned char*>(&rax18) == *reinterpret_cast<unsigned char*>(&esi19))) {
                            *reinterpret_cast<uint32_t*>(&rax18) = rcx8->f1;
                            if (*reinterpret_cast<unsigned char*>(&rax18) != 61) {
                                if (!*reinterpret_cast<unsigned char*>(&rax18)) 
                                    break;
                            } else {
                                if (!*reinterpret_cast<unsigned char*>(&rdx9->f0)) 
                                    goto addr_27b9_17;
                            }
                            rcx8 = reinterpret_cast<struct s2*>(&rcx8->f1);
                        }
                    }
                    addr_27df_20:
                    r8_7 = r8_7 + 8;
                    continue;
                    addr_27b9_17:
                    rdx9 = rcx8 + 1;
                    *reinterpret_cast<uint32_t*>(&rcx20) = r13d15;
                    *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax18) = 0;
                    fun_2500(1, "%s%c", rdx9, rcx20, r8_7);
                    r8_7 = r8_7;
                    *reinterpret_cast<uint32_t*>(&r9_17) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_17) + 4) = 0;
                    goto addr_27df_20;
                }
                r12d14 = r12d14 + *reinterpret_cast<uint32_t*>(&r9_17);
            }
            ++r15_13;
        } while (reinterpret_cast<int32_t>(ebp4) > *reinterpret_cast<int32_t*>(&r15_13));
        ebp21 = ebp4 - optind;
        *reinterpret_cast<unsigned char*>(&rax18) = reinterpret_cast<uint1_t>(ebp21 == r12d14);
    } else {
        rbx22 = __environ;
        ebp23 = ebp4 - (ebp4 + reinterpret_cast<uint1_t>(ebp4 < ebp4 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&r13d3) < 1))) & 10;
        while (rdx24 = *rbx22, !!rdx24) {
            *reinterpret_cast<uint32_t*>(&rcx25) = ebp23;
            *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
            fun_2500(1, "%s%c", rdx24, rcx25, r8_7, 1, "%s%c", rdx24, rcx25, r8_7);
            rbx22 = rbx22 + 8;
        }
        *reinterpret_cast<uint32_t*>(&rax18) = 1;
    }
    eax26 = *reinterpret_cast<uint32_t*>(&rax18) ^ 1;
    *reinterpret_cast<uint32_t*>(&rax27) = *reinterpret_cast<unsigned char*>(&eax26);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
    return rax27;
    addr_27fe_4:
    usage();
    addr_2683_6:
    if (eax12 != 0xffffff7d) {
        v11 = 0x2694;
        usage();
    }
    rdi28 = stdout;
    rcx8 = Version;
    r9_17 = reinterpret_cast<int64_t>("Richard Mlynarik");
    r8_7 = reinterpret_cast<struct s2**>("David MacKenzie");
    rdx9 = reinterpret_cast<struct s2*>("GNU coreutils");
    version_etc(rdi28, "printenv", "GNU coreutils", rcx8, "David MacKenzie", "Richard Mlynarik", 0, v11);
    fun_2540();
    goto addr_26d6_7;
}

int64_t __libc_start_main = 0;

void fun_2813() {
    __asm__("cli ");
    __libc_start_main(0x25e0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_22e0(int64_t rdi);

int64_t fun_28b3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_22e0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_28f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s2* program_name = reinterpret_cast<struct s2*>(0);

struct s2* fun_2390();

void fun_2440(struct s2* rdi, struct s0* rsi, int64_t rdx, struct s2* rcx);

int32_t fun_2460(int64_t rdi);

int32_t fun_2320(struct s2* rdi, int64_t rsi, int64_t rdx, struct s2* rcx);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

void fun_2560(struct s0* rdi, int64_t rsi, struct s2* rdx, struct s2* rcx, struct s2* r8, struct s2* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2903(int32_t edi) {
    struct s2* r12_2;
    struct s2* rax3;
    struct s2* v4;
    struct s2* rax5;
    struct s2* rcx6;
    struct s2** r8_7;
    struct s0* r12_8;
    struct s2* rax9;
    struct s0* r12_10;
    struct s2* rax11;
    struct s0* r12_12;
    struct s2* rax13;
    struct s2* rax14;
    struct s2** r8_15;
    int32_t eax16;
    struct s2* r13_17;
    struct s2* rax18;
    struct s2** r8_19;
    struct s2* rax20;
    int32_t eax21;
    struct s2* rax22;
    struct s2** r8_23;
    struct s2* rax24;
    struct s2** r8_25;
    struct s2* rax26;
    int32_t eax27;
    struct s2* rax28;
    struct s2** r8_29;
    struct s0* r15_30;
    struct s2* rax31;
    struct s2* rax32;
    struct s2** r8_33;
    struct s2* rax34;
    struct s0* rdi35;
    struct s2* r8_36;
    struct s2* r9_37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2390();
            fun_2500(1, rax5, r12_2, rcx6, r8_7);
            r12_8 = stdout;
            rax9 = fun_2390();
            fun_2440(rax9, r12_8, 5, rcx6);
            r12_10 = stdout;
            rax11 = fun_2390();
            fun_2440(rax11, r12_10, 5, rcx6);
            r12_12 = stdout;
            rax13 = fun_2390();
            fun_2440(rax13, r12_12, 5, rcx6);
            rax14 = fun_2390();
            fun_2500(1, rax14, "printenv", rcx6, r8_15);
            do {
                if (1) 
                    break;
                eax16 = fun_2460("printenv");
            } while (eax16);
            r13_17 = v4;
            if (!r13_17) {
                rax18 = fun_2390();
                fun_2500(1, rax18, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_19);
                rax20 = fun_24f0(5);
                if (!rax20 || (eax21 = fun_2320(rax20, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax21)) {
                    rax22 = fun_2390();
                    r13_17 = reinterpret_cast<struct s2*>("printenv");
                    fun_2500(1, rax22, "https://www.gnu.org/software/coreutils/", "printenv", r8_23);
                    r12_2 = reinterpret_cast<struct s2*>(" invocation");
                } else {
                    r13_17 = reinterpret_cast<struct s2*>("printenv");
                    goto addr_2c30_9;
                }
            } else {
                rax24 = fun_2390();
                fun_2500(1, rax24, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_25);
                rax26 = fun_24f0(5);
                if (!rax26 || (eax27 = fun_2320(rax26, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax27)) {
                    addr_2b36_11:
                    rax28 = fun_2390();
                    fun_2500(1, rax28, "https://www.gnu.org/software/coreutils/", "printenv", r8_29);
                    r12_2 = reinterpret_cast<struct s2*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_17 == "printenv")) {
                        r12_2 = reinterpret_cast<struct s2*>(0x7881);
                    }
                } else {
                    addr_2c30_9:
                    r15_30 = stdout;
                    rax31 = fun_2390();
                    fun_2440(rax31, r15_30, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_2b36_11;
                }
            }
            rax32 = fun_2390();
            rcx6 = r12_2;
            fun_2500(1, rax32, r13_17, rcx6, r8_33);
            addr_295e_14:
            fun_2540();
        }
    } else {
        rax34 = fun_2390();
        rdi35 = stderr;
        rcx6 = r12_2;
        fun_2560(rdi35, 1, rax34, rcx6, r8_36, r9_37, v38, v39, v40, v41, v42, v43);
        goto addr_295e_14;
    }
}

int64_t file_name = 0;

void fun_2c63(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_2c73(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

struct s2* quotearg_colon();

void fun_2510();

struct s2* fun_2330(int64_t rdi, int64_t rsi, int64_t rdx, struct s2* rcx, struct s2* r8);

void fun_2c83() {
    struct s0* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s0* rdi6;
    int32_t eax7;
    struct s2* rax8;
    int64_t rdi9;
    struct s2* rax10;
    int64_t rsi11;
    struct s2* r8_12;
    struct s2* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2310(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2390();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_2d13_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2510();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2330(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_2d13_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2510();
    }
}

void fun_2550(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s5 {
    signed char[1] pad1;
    unsigned char f1;
    signed char[2] pad4;
    unsigned char f4;
};

struct s5* fun_2400();

struct s2* __progname = reinterpret_cast<struct s2*>(0);

struct s2* __progname_full = reinterpret_cast<struct s2*>(0);

void fun_2d33(struct s2* rdi) {
    struct s0* rcx2;
    struct s2* rbx3;
    struct s5* rax4;
    struct s2* r12_5;
    struct s2* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2550("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2300("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2400();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s2*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2320(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (*reinterpret_cast<unsigned char*>(&rax4->f1) != 0x6c || (r12_5->f1 != 0x74 || *reinterpret_cast<unsigned char*>(&(r12_5 + 1)->f0) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s2*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(struct s0* rdi, int64_t rsi);

void fun_44d3(struct s0* rdi) {
    struct s0* rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2310(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<struct s0*>(0xb200);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_4513(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4533(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb200);
    }
    *rdi = esi;
    return 0xb200;
}

int64_t fun_4553(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_4593(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xb200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_45b3(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xb200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x25aa;
    if (!rdx) 
        goto 0x25aa;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb200;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void* fun_45f3(struct s0* rdi, void* rsi, struct s0* rdx, int64_t rcx, struct s8* r8) {
    struct s8* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xb200);
    }
    rax7 = fun_2310(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->pad40, v12, v10, 0x4626);
    *rax7 = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4673(struct s0* rdi, int64_t rsi, void** rdx, struct s9* rcx) {
    struct s9* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void* rax14;
    void* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xb200);
    }
    rax6 = fun_2310(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void*>(&rbx5->pad40);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x46a1);
    rsi15 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rax14) + 1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x46fc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_4763() {
    __asm__("cli ");
}

struct s0* gb078 = reinterpret_cast<struct s0*>(0xb100);

int64_t slotvec0 = 0x100;

void fun_4773() {
    uint32_t eax1;
    struct s0* r12_2;
    int64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 2;
            fun_22f0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xb100) {
        fun_22f0(rdi7);
        gb078 = reinterpret_cast<struct s0*>(0xb100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb070) {
        fun_22f0(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xb070);
    }
    nslots = 1;
    return;
}

void fun_4813() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4833() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4843(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4863(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_4883(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x25b0;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_23c0();
    } else {
        return rax6;
    }
}

struct s0* fun_4913(struct s0* rdi, int32_t esi, struct s0* rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x25b5;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_23c0();
    } else {
        return rax7;
    }
}

struct s0* fun_49a3(int32_t edi, struct s0* rsi) {
    struct s2* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x25ba;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_23c0();
    } else {
        return rax5;
    }
}

struct s0* fun_4a33(int32_t edi, struct s0* rsi, int64_t rdx) {
    struct s2* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x25bf;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_23c0();
    } else {
        return rax6;
    }
}

struct s0* fun_4ac3(struct s0* rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s2* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6730]");
    __asm__("movdqa xmm1, [rip+0x6738]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6721]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_23c0();
    } else {
        return rax10;
    }
}

struct s0* fun_4b63(struct s0* rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s2* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6690]");
    __asm__("movdqa xmm1, [rip+0x6698]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6681]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_23c0();
    } else {
        return rax9;
    }
}

struct s0* fun_4c03(struct s0* rdi) {
    struct s2* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x65f0]");
    __asm__("movdqa xmm1, [rip+0x65f8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x65d9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_23c0();
    } else {
        return rax3;
    }
}

struct s0* fun_4c93(struct s0* rdi, int64_t rsi) {
    struct s2* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6560]");
    __asm__("movdqa xmm1, [rip+0x6568]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6556]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_23c0();
    } else {
        return rax4;
    }
}

struct s0* fun_4d23(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x25c4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_23c0();
    } else {
        return rax6;
    }
}

struct s0* fun_4dc3(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x642a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6422]");
    __asm__("movdqa xmm2, [rip+0x642a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x25c9;
    if (!rdx) 
        goto 0x25c9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_23c0();
    } else {
        return rax7;
    }
}

struct s0* fun_4e63(int32_t edi, int64_t rsi, int64_t rdx, struct s0* rcx, int64_t r8) {
    struct s2* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x638a]");
    __asm__("movdqa xmm1, [rip+0x6392]");
    __asm__("movdqa xmm2, [rip+0x639a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x25ce;
    if (!rdx) 
        goto 0x25ce;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_23c0();
    } else {
        return rax9;
    }
}

struct s0* fun_4f13(int64_t rdi, int64_t rsi, struct s0* rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x62da]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x62d2]");
    __asm__("movdqa xmm2, [rip+0x62da]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x25d3;
    if (!rsi) 
        goto 0x25d3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_23c0();
    } else {
        return rax6;
    }
}

struct s0* fun_4fb3(int64_t rdi, int64_t rsi, struct s0* rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x623a]");
    __asm__("movdqa xmm1, [rip+0x6242]");
    __asm__("movdqa xmm2, [rip+0x624a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x25d8;
    if (!rsi) 
        goto 0x25d8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_23c0();
    } else {
        return rax7;
    }
}

void fun_5053() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5063(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5083() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_50a3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s10 {
    struct s2* f0;
    signed char[7] pad8;
    struct s2* f8;
    signed char[7] pad16;
    struct s2* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2470(int64_t rdi, struct s0* rsi, struct s2* rdx, struct s2* rcx, struct s2* r8, struct s2* r9);

void fun_50c3(struct s0* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s10* r8, struct s2* r9) {
    struct s2* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s2* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s2* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s2* r14_40;
    struct s2* r13_41;
    struct s2* r12_42;
    struct s2* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2560(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2560(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2390();
    fun_2560(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2470(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2390();
    fun_2560(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2470(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2390();
        fun_2560(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7b28 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7b28;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5533() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5553(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    struct s2* rax9;
    struct s2* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_23c0();
    } else {
        return;
    }
}

void fun_55f3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s2* rax15;
    struct s2* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_5696_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_56a0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_23c0();
    } else {
        return;
    }
    addr_5696_5:
    goto addr_56a0_7;
}

void fun_56d3() {
    struct s0* rsi1;
    struct s2* rdx2;
    struct s2* rcx3;
    struct s2* r8_4;
    struct s2* r9_5;
    struct s2* rax6;
    struct s2* rcx7;
    struct s2** r8_8;
    struct s2* rax9;
    struct s2** r8_10;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2470(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2390();
    fun_2500(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8);
    rax9 = fun_2390();
    fun_2500(1, rax9, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_10);
    fun_2390();
    goto fun_2500;
}

int64_t fun_2350();

void xalloc_die();

void fun_5773(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2350();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_24a0(unsigned char* rdi);

void fun_57b3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_24a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_57d3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_24a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_57f3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_24a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_24e0();

void fun_5813(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24e0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5843() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5873(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2350();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_58b3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2350();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_58f3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2350();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5923(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2350();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5973(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2350();
            if (rax5) 
                break;
            addr_59bd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_59bd_5;
        rax8 = fun_2350();
        if (rax8) 
            goto addr_59a6_9;
        if (rbx4) 
            goto addr_59bd_5;
        addr_59a6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_5a03(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2350();
            if (rax8) 
                break;
            addr_5a4a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_5a4a_5;
        rax11 = fun_2350();
        if (rax11) 
            goto addr_5a32_9;
        if (!rbx6) 
            goto addr_5a32_9;
        if (r12_4) 
            goto addr_5a4a_5;
        addr_5a32_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_5a93(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_5b3d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_5b50_10:
                *r12_8 = 0;
            }
            addr_5af0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_5b16_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_5b64_14;
            if (rcx10 <= rsi9) 
                goto addr_5b0d_16;
            if (rsi9 >= 0) 
                goto addr_5b64_14;
            addr_5b0d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_5b64_14;
            addr_5b16_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_24e0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_5b64_14;
            if (!rbp13) 
                break;
            addr_5b64_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_5b3d_9;
        } else {
            if (!r13_6) 
                goto addr_5b50_10;
            goto addr_5af0_11;
        }
    }
}

int64_t fun_2450();

void fun_5b93() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2450();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5bc3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2450();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5bf3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2450();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5c13() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2450();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2480(signed char* rdi, struct s2* rsi, unsigned char* rdx);

void fun_5c33(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_24a0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2480;
    }
}

void fun_5c73(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_24a0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2480;
    }
}

struct s12 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_5cb3(int64_t rdi, struct s12* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_24a0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2480;
    }
}

struct s2* fun_23b0(struct s2* rdi, ...);

void fun_5cf3(struct s2* rdi) {
    struct s2* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_23b0(rdi);
    rax3 = fun_24a0(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2480;
    }
}

void fun_5d33() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2390();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    fun_2510();
    fun_2300(rdi1);
}

int64_t fun_2340();

int64_t rpl_fclose(struct s0* rdi);

int64_t fun_5d73(struct s0* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_2340();
    ebx3 = reinterpret_cast<uint32_t>(rdi->f0) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_5dce_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2310(rdi);
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_5dce_3;
            rax6 = fun_2310(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_2490(struct s0* rdi);

int32_t fun_24d0(struct s0* rdi);

int64_t fun_2410(int64_t rdi, ...);

int32_t rpl_fflush(struct s0* rdi);

int64_t fun_2370(struct s0* rdi);

int64_t fun_5de3(struct s0* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2490(rdi);
    if (eax2 >= 0) {
        eax3 = fun_24d0(rdi);
        if (!(eax3 && (eax4 = fun_2490(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2410(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2310(rdi);
            r12d9 = *rax8;
            rax10 = fun_2370(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2370;
}

void rpl_fseeko(struct s0* rdi);

void fun_5e73(struct s0* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_24d0(rdi), !eax2) || !(reinterpret_cast<uint32_t>(rdi->f0) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_5ec3(struct s0* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2490(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2410(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rdi->f0) & 0xffffffef);
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_24c0(int64_t rdi);

signed char* fun_5f43() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_24c0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_23e0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_5f83(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s2* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_23e0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_23c0();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6013() {
    struct s2* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_23c0();
    } else {
        return rax3;
    }
}

int64_t fun_6093(int64_t rdi, signed char* rsi, struct s2* rdx) {
    struct s2* rax4;
    int32_t r13d5;
    struct s2* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_24f0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_23b0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2480(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2480(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6143() {
    __asm__("cli ");
    goto fun_24f0;
}

void fun_6153() {
    __asm__("cli ");
}

void fun_6167() {
    __asm__("cli ");
    return;
}

uint32_t fun_2430(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

struct s2* rpl_mbrtowc(void* rdi, struct s2* rsi);

int32_t fun_2580(int64_t rdi, struct s2* rsi);

uint32_t fun_2570(struct s2* rdi, struct s2* rsi);

void** fun_2590(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

void fun_2f65() {
    struct s2** rsp1;
    int32_t ebp2;
    struct s2* rax3;
    struct s2** rsp4;
    struct s2* r11_5;
    struct s2* r11_6;
    struct s2* v7;
    int32_t ebp8;
    struct s2* rax9;
    struct s2* rdx10;
    struct s2* rax11;
    struct s2* r11_12;
    struct s2* v13;
    int32_t ebp14;
    struct s2* rax15;
    struct s2* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s2* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s2* v22;
    int32_t ebx23;
    struct s2* rax24;
    struct s2** rsp25;
    struct s2* v26;
    struct s2* r11_27;
    struct s2* v28;
    struct s2* v29;
    struct s2* rsi30;
    struct s2* v31;
    struct s2* v32;
    struct s2* r10_33;
    struct s2* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s2* r9_37;
    struct s2* v38;
    struct s2* rdi39;
    struct s2* v40;
    struct s2* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s2* rcx44;
    unsigned char al45;
    struct s2* v46;
    int64_t v47;
    struct s2* v48;
    struct s2* v49;
    struct s2* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s2* v58;
    unsigned char v59;
    struct s2* v60;
    struct s2* v61;
    struct s2* v62;
    signed char* v63;
    struct s2* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s2* r13_69;
    struct s2* rsi70;
    void* v71;
    struct s2* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s2* v107;
    struct s2* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s2**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2390();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2390();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *reinterpret_cast<unsigned char*>(&rdx10->f0), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_23b0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s2*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s2*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s2**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3263_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3263_22; else 
                            goto addr_365d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_371d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_3a70_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3260_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3260_30; else 
                                goto addr_3a89_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_23b0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_3a70_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2430(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_3a70_28; else 
                            goto addr_310c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_3bd0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_3a50_40:
                        if (r11_27 == 1) {
                            addr_35dd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_3b98_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3217_44;
                            }
                        } else {
                            goto addr_3a60_46;
                        }
                    } else {
                        addr_3bdf_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_35dd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3263_22:
                                if (v47 != 1) {
                                    addr_37b9_52:
                                    v48 = reinterpret_cast<struct s2*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_23b0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_3804_54;
                                    }
                                } else {
                                    goto addr_3270_56;
                                }
                            } else {
                                addr_3215_57:
                                ebp36 = 0;
                                goto addr_3217_44;
                            }
                        } else {
                            addr_3a44_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_3bdf_47; else 
                                goto addr_3a4e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_35dd_41;
                        if (v47 == 1) 
                            goto addr_3270_56; else 
                            goto addr_37b9_52;
                    }
                }
                addr_32d1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s2*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3168_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_318d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_3490_65;
                    } else {
                        addr_32f9_66:
                        r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_3b48_67;
                    }
                } else {
                    goto addr_32f0_69;
                }
                addr_31a1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 1;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r15_16) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                addr_31ec_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_3b48_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_31ec_81;
                }
                addr_32f0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_318d_64; else 
                    goto addr_32f9_66;
                addr_3217_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_32cf_91;
                if (v22) 
                    goto addr_322f_93;
                addr_32cf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_32d1_62;
                addr_3804_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_3f8b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_3ffb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *reinterpret_cast<unsigned char*>(&rdx10->f0) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_3dff_103;
                            rdx10 = reinterpret_cast<struct s2*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2580(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2570(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_38fe_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_32bc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_3908_112;
                    }
                } else {
                    addr_3908_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_39d9_114;
                }
                addr_32c8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_32cf_91;
                while (1) {
                    addr_39d9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_3ee7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_3946_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_3ef5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_39c7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            ++r15_16;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_39c7_128;
                        }
                    }
                    addr_3975_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r15_16) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_39c7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                    continue;
                    addr_3946_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r15_16) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3975_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_31ec_81;
                addr_3ef5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3b48_67;
                addr_3f8b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_38fe_109;
                addr_3ffb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_38fe_109;
                addr_3270_56:
                rax93 = fun_2590(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_32bc_110;
                addr_3a4e_59:
                goto addr_3a50_40;
                addr_371d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3263_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_32c8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3215_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3263_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_3762_160;
                if (!v22) 
                    goto addr_3b37_162; else 
                    goto addr_3d43_163;
                addr_3762_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_3b37_162:
                    r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_3b48_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_360b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_3473_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_31a1_70; else 
                    goto addr_3487_169;
                addr_360b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3168_63;
                goto addr_32f0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_3a44_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_3b7f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3260_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3158_178; else 
                        goto addr_3b02_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3a44_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3263_22;
                }
                addr_3b7f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3260_30:
                    r8d42 = 0;
                    goto addr_3263_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_32d1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_3b98_42;
                    }
                }
                addr_3158_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3168_63;
                addr_3b02_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_3a60_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_32d1_62;
                } else {
                    addr_3b12_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3263_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_42c2_188;
                if (v28) 
                    goto addr_3b37_162;
                addr_42c2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_3473_168;
                addr_310c_37:
                if (v22) 
                    goto addr_4103_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3123_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_3bd0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_3c5b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3263_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3158_178; else 
                        goto addr_3c37_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3a44_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3263_22;
                }
                addr_3c5b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3263_22;
                }
                addr_3c37_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3a60_46;
                goto addr_3b12_186;
                addr_3123_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3263_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3263_22; else 
                    goto addr_3134_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_420e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4094_210;
            if (1) 
                goto addr_4092_212;
            if (!v29) 
                goto addr_3cce_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_23a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s2*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s2*>("\"");
            if (!0) 
                goto addr_4201_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s2*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s2*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3490_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_324b_219; else 
            goto addr_34aa_220;
        addr_322f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3243_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_34aa_220; else 
            goto addr_324b_219;
        addr_3dff_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_34aa_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_23a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_3e1d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_23a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s2*>("'");
        v29 = reinterpret_cast<struct s2*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s2*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s2*>(1);
        v22 = reinterpret_cast<struct s2*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s2*>(0);
            continue;
        }
        addr_4290_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_3cf6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s2*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s2*>(0);
        v22 = reinterpret_cast<struct s2*>(0);
        v28 = reinterpret_cast<struct s2*>(1);
        v26 = reinterpret_cast<struct s2*>("'");
        continue;
        addr_3ee7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3243_221;
        addr_3d43_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3243_221;
        addr_3487_169:
        goto addr_3490_65;
        addr_420e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_34aa_220;
        goto addr_3e1d_222;
        addr_4094_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(&v26->f0), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s2*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_40ee_236;
        fun_23c0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4290_225;
        addr_4092_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4094_210;
        addr_3cce_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4094_210;
        } else {
            rdx10 = reinterpret_cast<struct s2*>(0);
            goto addr_3cf6_226;
        }
        addr_4201_216:
        r13_34 = reinterpret_cast<struct s2*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s2*>("\"");
        v29 = reinterpret_cast<struct s2*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s2*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s2*>(1);
        v22 = reinterpret_cast<struct s2*>(0);
        v31 = reinterpret_cast<struct s2*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_365d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x75ec + rax113 * 4) + 0x75ec;
    addr_3a89_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x76ec + rax114 * 4) + 0x76ec;
    addr_4103_190:
    addr_324b_219:
    goto 0x2f30;
    addr_3134_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x74ec + rax115 * 4) + 0x74ec;
    addr_40ee_236:
    goto v116;
}

void fun_3150() {
}

void fun_3308() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3002;
}

void fun_3361() {
    goto 0x3002;
}

void fun_344e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x32d1;
    }
    if (v2) 
        goto 0x3d43;
    if (!r10_3) 
        goto addr_3eae_5;
    if (!v4) 
        goto addr_3d7e_7;
    addr_3eae_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_3d7e_7:
    goto 0x3184;
}

void fun_346c() {
}

void fun_3517() {
    signed char v1;

    if (v1) {
        goto 0x349f;
    } else {
        goto 0x31da;
    }
}

void fun_3531() {
    signed char v1;

    if (!v1) 
        goto 0x352a; else 
        goto "???";
}

void fun_3558() {
    goto 0x3473;
}

void fun_35d8() {
}

void fun_35f0() {
}

void fun_361f() {
    goto 0x3473;
}

void fun_3671() {
    goto 0x3600;
}

void fun_36a0() {
    goto 0x3600;
}

void fun_36d3() {
    goto 0x3600;
}

void fun_3aa0() {
    goto 0x3158;
}

void fun_3d9e() {
    signed char v1;

    if (v1) 
        goto 0x3d43;
    goto 0x3184;
}

void fun_3e45() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3184;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3168;
        goto 0x3184;
    }
}

void fun_4262() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x34d0;
    } else {
        goto 0x3002;
    }
}

void fun_5198() {
    fun_2390();
}

void fun_338e() {
    goto 0x3002;
}

void fun_3564() {
    goto 0x351c;
}

void fun_362b() {
    goto 0x3158;
}

void fun_367d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3600;
    goto 0x322f;
}

void fun_36af() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x360b;
        goto 0x3030;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x34aa;
        goto 0x324b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x3e48;
    if (r10_8 > r15_9) 
        goto addr_3595_9;
    addr_359a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x3e53;
    goto 0x3184;
    addr_3595_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_359a_10;
}

void fun_36e2() {
    goto 0x3217;
}

void fun_3ab0() {
    goto 0x3217;
}

void fun_424f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x336c;
    } else {
        goto 0x34d0;
    }
}

void fun_5250() {
}

void fun_36ec() {
    goto 0x3687;
}

void fun_3aba() {
    goto 0x35dd;
}

void fun_52b0() {
    fun_2390();
    goto fun_2560;
}

void fun_33bd() {
    goto 0x3002;
}

void fun_36f8() {
    goto 0x3687;
}

void fun_3ac7() {
    goto 0x362e;
}

void fun_52f0() {
    fun_2390();
    goto fun_2560;
}

void fun_33ea() {
    goto 0x3002;
}

void fun_3704() {
    goto 0x3600;
}

void fun_5330() {
    fun_2390();
    goto fun_2560;
}

void fun_340c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x3da0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x32d1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x32d1;
    }
    if (v11) 
        goto 0x4103;
    if (r10_12 > r15_13) 
        goto addr_4153_8;
    addr_4158_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x3e91;
    addr_4153_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4158_9;
}

struct s13 {
    signed char[24] pad24;
    int64_t f18;
};

struct s14 {
    signed char[16] pad16;
    struct s2* f10;
};

struct s15 {
    signed char[8] pad8;
    struct s2* f8;
};

void fun_5380() {
    int64_t r15_1;
    struct s13* rbx2;
    struct s2* r14_3;
    struct s14* rbx4;
    struct s2* r13_5;
    struct s15* rbx6;
    struct s2* r12_7;
    struct s2** rbx8;
    struct s2* rax9;
    struct s0* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2390();
    fun_2560(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x53a2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_53d8() {
    fun_2390();
    goto 0x53a9;
}

struct s16 {
    signed char[32] pad32;
    int64_t f20;
};

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s2* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s2* f8;
};

struct s20 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_5410() {
    int64_t rcx1;
    struct s16* rbx2;
    int64_t r15_3;
    struct s17* rbx4;
    struct s2* r14_5;
    struct s18* rbx6;
    struct s2* r13_7;
    struct s19* rbx8;
    struct s2* r12_9;
    struct s2** rbx10;
    int64_t v11;
    struct s20* rbx12;
    struct s2* rax13;
    struct s0* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2390();
    fun_2560(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x5444, __return_address(), rcx1);
    goto v15;
}

void fun_5488() {
    fun_2390();
    goto 0x544b;
}
