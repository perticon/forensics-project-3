print_element_env (char const *element, MAYBE_UNUSED char const *envvar)
{
#ifdef __APPLE__
  if (envvar)
    {
      char const *val = getenv (envvar);
      if (val)
        element = val;
    }
#endif
  print_element (element);
}