void usage(int param_1)

{
  FILE *pFVar1;
  int iVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  char *pcVar5;
  undefined **ppuVar6;
  char *__s1;
  char *pcVar7;
  char *pcVar8;
  long in_FS_OFFSET;
  undefined *local_b8;
  char *local_b0;
  char *local_a8 [4];
  char *local_88;
  char *local_80;
  char *local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  undefined8 local_58;
  undefined8 local_50;
  undefined8 local_40;
  
  uVar4 = program_name;
  ppuVar6 = &local_b8;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  if (param_1 == 0) {
    uVar3 = dcgettext(0,"Usage: %s [OPTION]...\n",5);
    __printf_chk(1,uVar3,uVar4);
    pFVar1 = stdout;
    pcVar7 = "Print machine architecture.\n\n";
    if (uname_mode == 1) {
      pcVar7 = (char *)dcgettext(0,
                                 "Print certain system information.  With no OPTION, same as -s.\n\n  -a, --all                print all information, in the following order,\n                             except omit -p and -i if unknown:\n  -s, --kernel-name        print the kernel name\n  -n, --nodename           print the network node hostname\n  -r, --kernel-release     print the kernel release\n"
                                 ,5);
      fputs_unlocked(pcVar7,pFVar1);
      pcVar7 = 
      "  -v, --kernel-version     print the kernel version\n  -m, --machine            print the machine hardware name\n  -p, --processor          print the processor type (non-portable)\n  -i, --hardware-platform  print the hardware platform (non-portable)\n  -o, --operating-system   print the operating system\n"
      ;
    }
    pFVar1 = stdout;
    pcVar7 = (char *)dcgettext(0,pcVar7,5);
    fputs_unlocked(pcVar7,pFVar1);
    pFVar1 = stdout;
    pcVar7 = (char *)dcgettext(0,"      --help        display this help and exit\n",5);
    fputs_unlocked(pcVar7,pFVar1);
    pFVar1 = stdout;
    pcVar7 = (char *)dcgettext(0,"      --version     output version information and exit\n",5);
    __s1 = "uname";
    fputs_unlocked(pcVar7,pFVar1);
    local_b8 = &DAT_0010700f;
    pcVar7 = "[";
    local_58 = 0;
    if (uname_mode != 1) {
      __s1 = "arch";
    }
    local_88 = "sha256sum";
    local_b0 = "test invocation";
    local_a8[0] = "coreutils";
    local_a8[1] = "Multi-call invocation";
    local_a8[2] = "sha224sum";
    local_78 = "sha384sum";
    local_a8[3] = "sha2 utilities";
    local_80 = "sha2 utilities";
    local_70 = "sha2 utilities";
    local_68 = "sha512sum";
    local_60 = "sha2 utilities";
    local_50 = 0;
    do {
      iVar2 = strcmp(__s1,pcVar7);
      if (iVar2 == 0) break;
      pcVar7 = *(char **)((long)ppuVar6 + 0x10);
      ppuVar6 = (undefined **)((long)ppuVar6 + 0x10);
    } while (pcVar7 != (char *)0x0);
    pcVar7 = *(char **)((long)ppuVar6 + 8);
    if (*(char **)((long)ppuVar6 + 8) == (char *)0x0) {
      pcVar7 = __s1;
    }
    uVar4 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar4,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar5 = setlocale(5,(char *)0x0);
    if (pcVar5 != (char *)0x0) {
      iVar2 = strncmp(pcVar5,"en_",3);
      pFVar1 = stdout;
      if (iVar2 != 0) {
        pcVar5 = (char *)dcgettext(0,
                                   "Report any translation bugs to <https://translationproject.org/team/>\n"
                                   ,5);
        fputs_unlocked(pcVar5,pFVar1);
      }
    }
    iVar2 = strcmp(__s1,"[");
    pcVar5 = "test";
    if (iVar2 != 0) {
      pcVar5 = __s1;
    }
    uVar4 = dcgettext(0,"Full documentation <%s%s>\n",5);
    pcVar8 = " invocation";
    __printf_chk(1,uVar4,"https://www.gnu.org/software/coreutils/",pcVar5);
    if (__s1 != pcVar7) {
      pcVar8 = "";
    }
    uVar4 = dcgettext(0,"or available locally via: info \'(coreutils) %s%s\'\n",5);
    __printf_chk(1,uVar4,pcVar7,pcVar8);
  }
  else {
    uVar3 = dcgettext(0,"Try \'%s --help\' for more information.\n",5);
    __fprintf_chk(stderr,1,uVar3,uVar4);
  }
                    /* WARNING: Subroutine does not return */
  exit(param_1);
}