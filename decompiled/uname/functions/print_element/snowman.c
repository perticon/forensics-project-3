void print_element(void* rdi, int64_t* rsi, struct s0* rdx, int64_t rcx, ...) {
    int1_t zf5;
    struct s1* rdi6;
    signed char* rax7;

    zf5 = printed_0 == 0;
    if (!zf5) {
        rdi6 = stdout;
        rax7 = rdi6->f28;
        if (reinterpret_cast<uint64_t>(rax7) >= reinterpret_cast<uint64_t>(rdi6->f30)) {
            fun_2400();
        } else {
            rdi6->f28 = rax7 + 1;
            *rax7 = 32;
        }
    }
    printed_0 = 1;
}