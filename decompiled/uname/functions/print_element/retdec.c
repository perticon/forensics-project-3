void print_element(char * element) {
    // 0x2ad0
    if (g25 == 0) {
        // 0x2af9
        g25 = 1;
        function_2460();
        return;
    }
    int64_t v1 = (int64_t)g20; // 0x2add
    int64_t * v2 = (int64_t *)(v1 + 40); // 0x2ae4
    uint64_t v3 = *v2; // 0x2ae4
    if (v3 >= *(int64_t *)(v1 + 48)) {
        // 0x2b10
        function_2400();
    } else {
        // 0x2aee
        *v2 = v3 + 1;
        *(char *)v3 = 32;
    }
    // 0x2af9
    g25 = 1;
    function_2460();
}