void print_element(char *param_1)

{
  char *pcVar1;
  
  if (printed_0 != '\0') {
    pcVar1 = stdout->_IO_write_ptr;
    if (pcVar1 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar1 + 1;
      *pcVar1 = ' ';
    }
    else {
      __overflow(stdout,0x20);
    }
  }
  printed_0 = 1;
  fputs_unlocked(param_1,stdout);
  return;
}