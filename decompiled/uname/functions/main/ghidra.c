undefined8 main(int param_1,undefined8 *param_2)

{
  int iVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  int unaff_EBX;
  undefined8 *puVar5;
  char *pcVar6;
  char *pcVar7;
  undefined1 *unaff_R15;
  long in_FS_OFFSET;
  utsname local_1c8;
  long local_40;
  
  pcVar7 = "";
  pcVar6 = "coreutils";
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  if (uname_mode == 2) {
    iVar1 = getopt_long(param_1,param_2,"",arch_long_options,0);
    if (iVar1 != -1) {
      if (iVar1 != -0x83) goto LAB_0010275d;
      pcVar6 = "uname";
      if (uname_mode != 1) {
        pcVar6 = "arch";
      }
      version_etc(stdout,pcVar6,"GNU coreutils",Version,"David MacKenzie","Karel Zak",0,
                  &DAT_0010700a);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (param_1 != optind) goto LAB_0010293f;
    iVar1 = uname(&local_1c8);
    if (iVar1 == -1) {
LAB_001029ad:
      uVar2 = dcgettext(0,"cannot get system name",5);
      piVar4 = __errno_location();
      error(1,*piVar4,uVar2);
      goto LAB_001029d9;
    }
    param_2 = (undefined8 *)0x10;
    unaff_EBX = 0x10;
LAB_00102815:
    if (((ulong)param_2 & 8) != 0) {
      print_element(local_1c8.version);
    }
    if (((ulong)param_2 & 0x10) == 0) goto LAB_00102829;
LAB_0010297a:
    print_element(local_1c8.machine);
  }
  else {
    puVar5 = (undefined8 *)0x0;
    unaff_R15 = uname_long_options;
    pcVar7 = "asnrvmpio";
    pcVar6 = &DAT_001075b8;
    while( true ) {
      unaff_EBX = (int)puVar5;
      iVar1 = getopt_long(param_1,param_2,pcVar7,unaff_R15,0);
      if (iVar1 == -1) break;
      if (0x76 < iVar1) goto LAB_00102970;
      if (0x60 < iVar1) {
        if (iVar1 - 0x61U < 0x16) {
                    /* WARNING: Could not recover jumptable at 0x001026df. Too many branches */
                    /* WARNING: Treating indirect jump as call */
          uVar2 = (*(code *)(pcVar6 + *(int *)(pcVar6 + (ulong)(iVar1 - 0x61U) * 4)))();
          return uVar2;
        }
        goto LAB_00102970;
      }
      if (iVar1 == -0x83) {
        pcVar6 = "uname";
        if (uname_mode != 1) {
          pcVar6 = "arch";
        }
        version_etc(stdout,pcVar6,"GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
LAB_0010275d:
      if (iVar1 != -0x82) goto LAB_00102970;
      usage();
      puVar5 = (undefined8 *)0xffffffff;
    }
    if (param_1 != optind) {
LAB_0010293f:
      uVar2 = quote(param_2[optind]);
      uVar3 = dcgettext(0,"extra operand %s",5);
      error(0,0,uVar3,uVar2);
LAB_00102970:
      usage(1);
      goto LAB_0010297a;
    }
    param_2 = (undefined8 *)0x1;
    if (unaff_EBX != 0) {
      param_2 = puVar5;
    }
    if (((ulong)param_2 & 0x1f) != 0) {
      iVar1 = uname(&local_1c8);
      if (iVar1 == -1) goto LAB_001029ad;
      if (((ulong)param_2 & 1) != 0) {
        print_element();
      }
      if (((ulong)param_2 & 2) != 0) {
        print_element(local_1c8.nodename);
      }
      if (((ulong)param_2 & 4) != 0) {
        print_element(local_1c8.release);
      }
      goto LAB_00102815;
    }
  }
LAB_00102829:
  if (((ulong)param_2 & 0x20) == 0) {
    if ((((ulong)param_2 & 0x40) != 0) && (unaff_EBX != -1)) {
LAB_001028b0:
      print_element("unknown");
    }
  }
  else if (unaff_EBX != -1) {
    print_element("unknown");
    if (((ulong)param_2 & 0x40) != 0) goto LAB_001028b0;
  }
  if (((ulong)param_2 & 0x80) != 0) {
    print_element("GNU/Linux");
  }
  pcVar6 = stdout->_IO_write_ptr;
  if (pcVar6 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar6 + 1;
    *pcVar6 = '\n';
  }
  else {
    __overflow(stdout,10);
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
LAB_001029d9:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}