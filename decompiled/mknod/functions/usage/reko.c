void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002640(fn0000000000002410(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002E8E;
	}
	fn00000000000025D0(fn0000000000002410(0x05, "Usage: %s [OPTION]... NAME TYPE [MAJOR MINOR]\n", null), 0x01);
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "Create the special file NAME of the given TYPE.\n", null));
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n", null));
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n", null));
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "      --version     output version information and exit\n", null));
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "\nBoth MAJOR and MINOR must be specified when TYPE is b, c, or u, and they\nmust be omitted when TYPE is p.  If MAJOR or MINOR begins with 0x or 0X,\nit is interpreted as hexadecimal; otherwise, if it begins with 0, as octal;\notherwise, as decimal.  TYPE may be:\n", null));
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "\n  b      create a block (buffered) special file\n  c, u   create a character (unbuffered) special file\n  p      create a FIFO\n", null));
	fn00000000000025D0(fn0000000000002410(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_1131 * rbx_223 = fp - 0xB8 + 16;
	do
	{
		char * rsi_225 = rbx_223->qw0000;
		++rbx_223;
	} while (rsi_225 != null && fn00000000000024F0(rsi_225, "mknod") != 0x00);
	ptr64 r13_238 = rbx_223->qw0008;
	if (r13_238 != 0x00)
	{
		fn00000000000025D0(fn0000000000002410(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_325 = fn00000000000025C0(null, 0x05);
		if (rax_325 == 0x00 || fn0000000000002390(0x03, "en_", rax_325) == 0x00)
			goto l0000000000003126;
	}
	else
	{
		fn00000000000025D0(fn0000000000002410(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_267 = fn00000000000025C0(null, 0x05);
		if (rax_267 == 0x00 || fn0000000000002390(0x03, "en_", rax_267) == 0x00)
		{
			fn00000000000025D0(fn0000000000002410(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003163:
			fn00000000000025D0(fn0000000000002410(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002E8E:
			fn0000000000002620(edi);
		}
		r13_238 = 0x8004;
	}
	fn00000000000024D0(stdout, fn0000000000002410(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003126:
	fn00000000000025D0(fn0000000000002410(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003163;
}