undefined8 main(int param_1,undefined8 *param_2)

{
  char cVar1;
  int iVar2;
  __mode_t _Var3;
  uint uVar4;
  undefined8 uVar5;
  void *__ptr;
  long lVar6;
  undefined8 uVar7;
  int *piVar8;
  undefined8 uVar9;
  void *pvVar10;
  ulong uVar11;
  long extraout_RDX;
  long lVar12;
  long lVar13;
  char *pcVar14;
  undefined *puVar15;
  long lVar16;
  long in_FS_OFFSET;
  undefined auVar17 [16];
  uint local_50;
  undefined4 uStack76;
  ulong local_48;
  long local_40;
  
  lVar13 = (long)param_1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar15 = &DAT_001080d5;
  textdomain("coreutils");
  atexit(close_stdout);
  lVar6 = 0;
LAB_00102743:
  do {
    lVar16 = lVar6;
    iVar2 = getopt_long(param_1,param_2,&DAT_001080d5,longopts,0);
    if (iVar2 == -1) {
      if (lVar16 == 0) {
        puVar15 = (undefined *)0x1b6;
      }
      else {
        __ptr = (void *)mode_compile();
        if (__ptr == (void *)0x0) {
          uVar5 = dcgettext(0,"invalid mode",5);
          error(1,0,uVar5);
          goto LAB_00102b53;
        }
        _Var3 = umask(0);
        umask(_Var3);
        pvVar10 = __ptr;
        uVar4 = mode_adjust(0x1b6,0,_Var3,__ptr,0);
        puVar15 = (undefined *)(ulong)uVar4;
        free(__ptr);
        if ((uVar4 & 0xfffffe00) != 0) {
          uVar5 = dcgettext(0,"mode must specify only file permission bits",5);
          error(1,0,uVar5);
          goto LAB_00102cfa;
        }
      }
      pvVar10 = (void *)(long)optind;
      uVar11 = (ulong)(param_1 - optind);
      if (optind < param_1) {
        if ((optind + 1 < param_1) &&
           (auVar17 = CONCAT88(uVar11,(long)optind), *(char *)param_2[(long)optind + 1] == 'p'))
        goto LAB_00102b15;
        if (uVar11 < 4) {
          puVar15 = (undefined *)quote(param_2[(long)param_1 + -1]);
          uVar5 = dcgettext(0,"missing operand after %s",5);
          error(0,0,uVar5,puVar15);
          if (param_1 - optind == 2) {
            pcVar14 = "Special files require major and minor device numbers.";
            goto LAB_00102aa0;
          }
          break;
        }
        if (uVar11 != 4) {
LAB_00102cfa:
          puVar15 = (undefined *)quote(param_2[(long)pvVar10 + 4]);
          pcVar14 = "extra operand %s";
          goto LAB_00102a13;
        }
      }
      else {
        if (uVar11 < 2) {
          uVar5 = dcgettext(0,"missing operand",5);
          error(0,0,uVar5);
          break;
        }
        if (uVar11 != 2) {
          uVar5 = param_2[(long)pvVar10 + 2];
          goto LAB_00102a5d;
        }
      }
      lVar6 = (long)optind;
      goto LAB_001028a0;
    }
    if (iVar2 == 0x5a) {
LAB_001027d0:
      lVar6 = lVar16;
      if (optarg != 0) {
        uVar5 = dcgettext(0,
                          "warning: ignoring --context; it requires an SELinux/SMACK-enabled kernel"
                          ,5);
        error(0,0,uVar5);
      }
      goto LAB_00102743;
    }
    if (iVar2 < 0x5b) {
      if (iVar2 == -0x83) {
        version_etc(stdout,"mknod","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar2 == -0x82) {
        usage();
        goto LAB_001027d0;
      }
      break;
    }
    lVar6 = optarg;
  } while (iVar2 == 0x6d);
  goto LAB_00102a2b;
LAB_001028a0:
  do {
    _Var3 = (__mode_t)puVar15;
    lVar12 = (lVar6 + 1) * 8;
    cVar1 = *(char *)param_2[lVar6 + 1];
    if (cVar1 != 'p') {
      if ('p' < cVar1) goto LAB_00102a35;
      uVar4 = 0x6000;
      if ((cVar1 != 'b') && (uVar4 = 0x2000, cVar1 != 'c')) goto LAB_001029ff;
      goto LAB_001028cf;
    }
    iVar2 = mkfifo((char *)param_2[lVar6],_Var3);
    if (iVar2 == 0) goto LAB_001029a1;
    puVar15 = (undefined *)quotearg_n_style_colon(0,3,param_2[optind]);
    piVar8 = __errno_location();
    auVar17 = error(1,*piVar8,"%s",puVar15);
LAB_00102b15:
    lVar6 = SUB168(auVar17,0);
    if (auVar17 < (undefined  [16])0x2) {
LAB_00102b53:
      puVar15 = (undefined *)quote(param_2[lVar13 + -1]);
      pcVar14 = "missing operand after %s";
      goto LAB_00102a13;
    }
  } while (SUB168(auVar17 >> 0x40,0) == 2);
  uVar5 = param_2[lVar6 + 2];
LAB_00102a5d:
  puVar15 = (undefined *)quote(uVar5);
  uVar5 = dcgettext(0,"extra operand %s",5);
  error(0,0,uVar5,puVar15);
  pcVar14 = "Fifos do not have major and minor device numbers.";
  if (param_1 - optind == 4) {
LAB_00102aa0:
    uVar5 = dcgettext(0,pcVar14,5);
    __fprintf_chk(stderr,1,"%s\n",uVar5);
  }
LAB_00102a2b:
  while( true ) {
    _Var3 = (__mode_t)puVar15;
    cVar1 = usage(1);
    lVar12 = extraout_RDX;
LAB_00102a35:
    if (cVar1 == 'u') break;
LAB_001029ff:
    puVar15 = (undefined *)quote();
    pcVar14 = "invalid device type %s";
LAB_00102a13:
    uVar5 = dcgettext(0,pcVar14,5);
    error(0,0,uVar5,puVar15);
  }
  uVar4 = 0x2000;
LAB_001028cf:
  uVar5 = *(undefined8 *)((long)param_2 + lVar12 + 8);
  lVar13 = *(long *)((long)param_2 + lVar12 + 0x10);
  iVar2 = xstrtoumax(uVar5,0,0,&local_50,"");
  if ((iVar2 == 0) && (CONCAT44(uStack76,local_50) == (ulong)local_50)) {
    iVar2 = xstrtoumax(lVar13,0,0,&local_48,"");
    if ((iVar2 == 0) && (local_48 == (local_48 & 0xffffffff))) {
      uVar11 = (local_48 & 0xffffff00) << 0xc |
               local_48 & 0xff |
               ((ulong)local_50 & 0xfffff000) << 0x20 | (ulong)((local_50 & 0xfff) << 8);
      if (uVar11 != 0xffffffffffffffff) {
        iVar2 = mknod((char *)param_2[optind],uVar4 | _Var3,uVar11);
        if (iVar2 == 0) {
LAB_001029a1:
          if ((lVar16 == 0) || (iVar2 = lchmod((char *)param_2[optind],_Var3), iVar2 == 0)) {
            if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
              return 0;
            }
            goto LAB_00102cd1;
          }
          uVar5 = quotearg_style(4,param_2[optind]);
          uVar7 = dcgettext(0,"cannot set permissions of %s",5);
          piVar8 = __errno_location();
          error(1,*piVar8,uVar7,uVar5);
        }
        uVar7 = quotearg_n_style_colon(0,3,param_2[optind]);
        piVar8 = __errno_location();
        error(1,*piVar8,"%s",uVar7);
      }
      uVar7 = dcgettext(0,"invalid device %s %s",5);
      error(1,0,uVar7,uVar5,lVar13);
    }
    uVar7 = quote(lVar13);
    uVar9 = dcgettext(0,"invalid minor device number %s",5);
    error(1,0,uVar9,uVar7);
  }
  uVar5 = quote(uVar5);
  uVar7 = dcgettext(0,"invalid major device number %s",5);
  error(1,0,uVar7,uVar5);
LAB_00102cd1:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}