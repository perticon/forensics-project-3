void main(struct Eq_100 * rsi, word32 edi, struct Eq_393 * fs)
{
	ptr64 fp;
	word64 rax_34 = fs->qw0028;
	set_program_name(rsi->t0000);
	fn00000000000025C0("", 0x06);
	fn0000000000002400("/usr/local/share/locale", "coreutils");
	fn00000000000023E0("coreutils");
	atexit(&g_t3270);
	Eq_26 rbp_100 = (int64) edi;
	union Eq_426 * r14_12 = null;
	word32 ebp_968 = (word32) rbp_100;
	struct Eq_100 * rbx_101 = rsi;
	struct Eq_431 * rsp_1003 = fp - 88;
	while (true)
	{
		int32 eax_81 = fn0000000000002450(&g_tBB40, "m:Z", rsi, ebp_968, null);
		if (eax_81 == ~0x00)
			break;
		if (eax_81 != 0x5A)
		{
			if (eax_81 > 0x5A)
			{
				if (eax_81 != 0x6D)
					goto l0000000000002A2B;
				r14_12 = optarg;
				continue;
			}
			if (eax_81 != ~0x82)
			{
				if (eax_81 != ~0x81)
					goto l0000000000002A2B;
				usage(0x00);
			}
			else
			{
				version_etc(0x8004, stdout, fs);
				fn0000000000002620(0x00);
			}
		}
		if (optarg != null)
			fn00000000000025E0(fn0000000000002410(0x05, "warning: ignoring --context; it requires an SELinux/SMACK-enabled kernel", null), 0x00, 0x00);
	}
	char * rsi_372;
	int64 rcx_161;
	if (r14_12 != null)
	{
		struct Eq_11 * rax_92 = mode_compile(r14_12, out rbx_101, out rbp_100);
		rsp_1003 = (struct Eq_431 *) <invalid>;
		word32 eax_135 = (word32) rax_92;
		if (rax_92 == null)
		{
			fn00000000000025E0(fn0000000000002410(0x05, "invalid mode", null), 0x00, 0x01);
			goto l0000000000002B53;
		}
		fn0000000000002520();
		fn0000000000002520();
		ui32 r12d_170 = mode_adjust(rax_92, eax_135, 0x00, 0x01B6, null, out rcx_161);
		fn0000000000002360(rax_92);
		if ((r12d_170 & ~0x01FF) != 0x00)
		{
			fn00000000000025E0(fn0000000000002410(0x05, "mode must specify only file permission bits", null), 0x00, 0x01);
			goto l0000000000002CFA;
		}
	}
	char * rsi_275;
	Eq_26 rdi_213;
	Eq_527 rax_219;
	int64 rcx_176 = (int64) g_dwC090;
	int32 ebp_177 = (word32) rbp_100;
	int32 ecx_182 = (word32) rcx_176;
	Eq_536 rdx_186 = (int64) (ebp_177 - ecx_182);
	if (ecx_182 >= ebp_177)
	{
		if (rdx_186 <= 0x01)
		{
			fn00000000000025E0(fn0000000000002410(0x05, "missing operand", null), 0x00, 0x00);
l0000000000002A2B:
			usage(0x01);
		}
		if (rdx_186 != 0x02)
		{
			rdi_213 = rbx_101->a0010[rcx_176];
l0000000000002A5D:
			quote(rdi_213, fs);
			fn00000000000025E0(fn0000000000002410(0x05, "extra operand %s", null), 0x00, 0x00);
			rsi_275 = (char *) "Fifos do not have major and minor device numbers.";
			if (ebp_177 - g_dwC090 == 0x04)
				goto l0000000000002AA0;
			goto l0000000000002A2B;
		}
		goto l000000000000289D;
	}
	if ((word32) rcx_176 + 0x01 < ebp_177)
	{
		rax_219.u0 = (int64) ecx_182;
		if (rbx_101->a0008[rax_219].b0000 + 0x00 == 0x70)
			goto l0000000000002B15;
	}
	rcx_161 = rcx_176;
	if (rdx_186 <= 0x03)
	{
		quote((rbx_101 - 8)[(int64) ebp_177], fs);
		fn00000000000025E0(fn0000000000002410(0x05, "missing operand after %s", null), 0x00, 0x00);
		if (ebp_177 - g_dwC090 != 0x02)
			goto l0000000000002A2B;
		rsi_275 = (char *) "Special files require major and minor device numbers.";
l0000000000002AA0:
		fn0000000000002410(0x05, rsi_275, null);
		fn0000000000002640(0x8CD0, 0x01, stderr);
		goto l0000000000002A2B;
	}
	if (rdx_186 == 0x04)
	{
l000000000000289D:
		rax_219.u0 = (int64) ecx_182;
l00000000000028A0:
		Eq_26 r13_566;
		Mem284 = Mem35;
		Eq_645 rax_282 = (word64) rax_219.u1 + 1;
		Eq_26 rdi_285 = rbx_101[rax_282 * 0x08 /64 32];
		uint64 rax_288 = (uint64) *rdi_285;
		union Eq_426 * r14_310 = r14_12;
		struct Eq_100 * rbx_283 = rbx_101;
		ci8 al_289 = (byte) rax_288;
		word32 eax_296 = (word32) rax_288;
		if (al_289 != 0x70)
		{
			struct Eq_553 ** r15_447;
			if (al_289 <= 0x70)
			{
				r15_447 = &g_ptr6000;
				if (al_289 == 0x62)
					goto l00000000000028CF;
				r15_447 = &g_ptr2000;
				if (al_289 != 99)
				{
l00000000000029FF:
					quote(rdi_285, fs);
					rsi_372 = (char *) "invalid device type %s";
					goto l0000000000002A13;
				}
			}
			else
			{
				if (al_289 != 117)
					goto l00000000000029FF;
				r15_447 = &g_ptr2000;
			}
l00000000000028CF:
			struct Eq_431 * rsp_572 = (struct Eq_431 *) <invalid>;
			struct Eq_553 ** r15_568;
			struct Eq_553 * rbp_579;
			word64 rbx_1142;
			word32 r12d_1143;
			word64 r14_1144;
			if (xstrtoumax(&rsp_1003->t0008, 0x00, null, rbx_101->a0008[rax_282].b0000, "", r15_447, fs, out rbx_1142, out rbp_579, out r12d_1143, out r13_566, out r14_1144, out r15_568) != 0x00)
				goto l0000000000002C9F;
			Eq_26 rax_588 = rsp_572->t0008;
			if (rax_588 != (uint64) ((word32) rax_588))
			{
l0000000000002C9F:
				quote(r13_566, fs);
				fn00000000000025E0(fn0000000000002410(0x05, "invalid major device number %s", null), 0x00, 0x01);
				goto l0000000000002CD1;
			}
			struct Eq_431 * rsp_611 = (struct Eq_431 *) <invalid>;
			word32 r12d_1145;
			word64 r15_1146;
			if (xstrtoumax(&rsp_572->t0010, 0x00, null, rbp_579, "", r15_568, fs, out rbx_283, out rbp_100, out r12d_1145, out r13_566, out r14_310, out r15_1146) != 0x00)
			{
l0000000000002C6D:
				quote(rbp_100, fs);
				fn00000000000025E0(fn0000000000002410(0x05, "invalid minor device number %s", null), 0x00, 0x01);
				goto l0000000000002C9F;
			}
			Eq_26 rax_627 = rsp_611->t0010;
			byte al_646 = (byte) rax_627;
			if (rax_627 != (uint64) ((word32) rax_627))
				goto l0000000000002C6D;
			uint64 rdx_634 = (uint64) rsp_611->t0008;
			if ((rax_627 << 0x0C & 0xFFFFFF00000 | ((uint64) al_646 | (rdx_634 << 0x20 & 0xFFFFF00000000000 | (uint64) ((word32) rdx_634 << 0x08 & 0x000FFF00)))) == ~0x00)
			{
l0000000000002C43:
				fn00000000000025E0(fn0000000000002410(0x05, "invalid device %s %s", null), 0x00, 0x01);
				goto l0000000000002C6D;
			}
			int32 eax_671 = g_dwC090;
			fn0000000000002540();
			if (eax_671 != 0x00)
			{
l0000000000002C0C:
				quotearg_n_style_colon(rbx_283[(int64) g_dwC090 * 0x08 /64 32], 0x03, 0x00, fs);
				fn00000000000025E0(0x882C, *fn0000000000002380(), 0x01);
				goto l0000000000002C43;
			}
l00000000000029A1:
			if (r14_310 == null)
			{
l00000000000029C1:
				if (rax_34 - fs->qw0028 == 0x00)
					return;
l0000000000002CD1:
				fn0000000000002440();
			}
			int32 eax_686 = g_dwC090;
			fn00000000000023C0();
			if (eax_686 == 0x00)
				goto l00000000000029C1;
			r13_566 = quotearg_style(rbx_283[(int64) g_dwC090 * 0x08 /64 32], 0x04, fs);
			fn00000000000025E0(fn0000000000002410(0x05, "cannot set permissions of %s", null), *fn0000000000002380(), 0x01);
			goto l0000000000002C0C;
		}
		fn0000000000002510();
		if (eax_296 == 0x00)
			goto l00000000000029A1;
		quotearg_n_style_colon(rbx_101[(int64) g_dwC090 * 0x08 /64 32], 0x03, 0x00, fs);
		fn00000000000025E0(0x882C, *fn0000000000002380(), 0x01);
		rdx_186.u1 = 0x882C;
		rax_219.u1 = 0x00;
l0000000000002B15:
		Mem353 = Mem35;
		ebp_177 = (word32) rbp_100;
		if (rdx_186 > 0x01)
		{
			if (rdx_186 != 0x02)
			{
				rdi_213 = rbx_101->a0010[rax_219];
				goto l0000000000002A5D;
			}
			goto l00000000000028A0;
		}
l0000000000002B53:
		quote((rbx_101 - 8)[rbp_100], fs);
		rsi_372 = (char *) "missing operand after %s";
l0000000000002A13:
		fn00000000000025E0(fn0000000000002410(0x05, rsi_372, null), 0x00, 0x00);
		goto l0000000000002A2B;
	}
l0000000000002CFA:
	quote(rbx_101->a0020[rcx_161], fs);
	rsi_372 = (char *) "extra operand %s";
	goto l0000000000002A13;
}