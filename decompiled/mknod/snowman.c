
struct s0 {
    struct s0* f0;
    unsigned char f1;
    signed char f2;
    signed char[1] pad4;
    int32_t f4;
    struct s0* f8;
    signed char[3] pad12;
    uint32_t fc;
    signed char[1] pad17;
    signed char f11;
    signed char[6] pad24;
    struct s0* f18;
};

int64_t fun_2420();

int64_t fun_2370(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2420();
    if (r8d > 10) {
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8960 + rax11 * 4) + 0x8960;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

int32_t* fun_2380();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_24b0();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_2360(struct s0* rdi, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_2440();

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x4ebf;
    rax8 = fun_2380();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
        fun_2370(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7011]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x4f4b;
            fun_24b0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xc100) {
                fun_2360(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x4fda);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2440();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x8903);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0x88fc);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x8907);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0x88f8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbe38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbe38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2353() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_2363() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_2373() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2383() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_2393() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_23a3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_23b3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t lchmod = 0x2090;

void fun_23c3() {
    __asm__("cli ");
    goto lchmod;
}

int64_t reallocarray = 0x20a0;

void fun_23d3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20b0;

void fun_23e3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_23f3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_2403() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20e0;

void fun_2413() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20f0;

void fun_2423() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2100;

void fun_2433() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2110;

void fun_2443() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2120;

void fun_2453() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2130;

void fun_2463() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2140;

void fun_2473() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2150;

void fun_2483() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_2493() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2170;

void fun_24a3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2180;

void fun_24b3() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x2190;

void fun_24c3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21a0;

void fun_24d3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21b0;

void fun_24e3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21c0;

void fun_24f3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21d0;

void fun_2503() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t mkfifo = 0x21e0;

void fun_2513() {
    __asm__("cli ");
    goto mkfifo;
}

int64_t umask = 0x21f0;

void fun_2523() {
    __asm__("cli ");
    goto umask;
}

int64_t stat = 0x2200;

void fun_2533() {
    __asm__("cli ");
    goto stat;
}

int64_t mknod = 0x2210;

void fun_2543() {
    __asm__("cli ");
    goto mknod;
}

int64_t memcpy = 0x2220;

void fun_2553() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_2563() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_2573() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_2583() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_2593() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25a3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2280;

void fun_25b3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2290;

void fun_25c3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22a0;

void fun_25d3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22b0;

void fun_25e3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22c0;

void fun_25f3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x22d0;

void fun_2603() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x22e0;

void fun_2613() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22f0;

void fun_2623() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2300;

void fun_2633() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2310;

void fun_2643() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2320;

void fun_2653() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2330;

void fun_2663() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2340;

void fun_2673() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(unsigned char* rdi);

struct s0* fun_25c0(int64_t rdi, ...);

void fun_2400(int64_t rdi, int64_t rsi);

void fun_23e0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2450(int64_t rdi, unsigned char** rsi);

int64_t optarg = 0;

signed char usage();

int64_t stdout = 0;

void version_etc(int64_t rdi, int64_t rsi);

int32_t fun_2620();

struct s0* fun_2410();

int64_t fun_25e0();

struct s0* mode_compile(int64_t rdi, unsigned char** rsi);

int32_t fun_2520();

uint32_t mode_adjust(int64_t rdi);

int32_t optind = 0;

int32_t fun_2510(int64_t rdi, int64_t rsi);

struct s0* quotearg_n_style_colon();

struct s0* quote(unsigned char* rdi, ...);

int32_t xstrtoumax(unsigned char* rdi);

int32_t fun_2540(unsigned char* rdi);

int32_t fun_23c0(unsigned char* rdi, int64_t rsi);

unsigned char* quotearg_style(int64_t rdi, unsigned char* rsi);

int64_t stderr = 0;

void fun_2640(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, ...);

int64_t fun_26c3(int32_t edi, unsigned char** rsi) {
    int64_t r14_3;
    unsigned char* rbp4;
    unsigned char** rbx5;
    unsigned char* rdi6;
    struct s0* rax7;
    struct s0* v8;
    struct s0* r12_9;
    struct s0* rdx10;
    int64_t rdi11;
    int32_t eax12;
    int1_t zf13;
    int64_t rdi14;
    struct s0* rax15;
    struct s0* rcx16;
    uint32_t eax17;
    uint64_t rdx18;
    struct s0* rax19;
    int64_t rax20;
    int64_t rax21;
    unsigned char* rdi22;
    uint32_t eax23;
    int64_t rdi24;
    int64_t rsi25;
    int32_t eax26;
    struct s0* rax27;
    unsigned char* rdi28;
    struct s0* rax29;
    unsigned char* rdi30;
    struct s0* rax31;
    int32_t ebp32;
    struct s0* rax33;
    unsigned char* r13_34;
    int32_t eax35;
    int64_t rax36;
    int64_t v37;
    int64_t rdx38;
    struct s0* rax39;
    struct s0* rax40;
    unsigned char* rdi41;
    struct s0* rax42;
    int32_t eax43;
    uint64_t rax44;
    uint64_t v45;
    uint64_t rdx46;
    uint64_t rdx47;
    int32_t v48;
    uint64_t rcx49;
    uint64_t rcx50;
    uint64_t rcx51;
    int64_t rax52;
    unsigned char* rdi53;
    int32_t eax54;
    int64_t rax55;
    int64_t rsi56;
    unsigned char* rdi57;
    int32_t eax58;
    void* rax59;
    int64_t rax60;
    unsigned char* rsi61;
    unsigned char* rax62;
    struct s0* rax63;
    int64_t rdi64;
    unsigned char* rdi65;
    struct s0* rax66;
    struct s0* rax67;
    int32_t ebp68;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r14_3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_3) + 4) = 0;
    rbp4 = reinterpret_cast<unsigned char*>(static_cast<int64_t>(edi));
    rbx5 = rsi;
    rdi6 = *rsi;
    rax7 = g28;
    v8 = rax7;
    set_program_name(rdi6);
    fun_25c0(6, 6);
    fun_2400("coreutils", "/usr/local/share/locale");
    r12_9 = reinterpret_cast<struct s0*>("m:Z");
    fun_23e0("coreutils", "/usr/local/share/locale");
    atexit(0x3270, "/usr/local/share/locale");
    while (rdx10 = reinterpret_cast<struct s0*>("m:Z"), *reinterpret_cast<int32_t*>(&rdi11) = *reinterpret_cast<int32_t*>(&rbp4), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0, eax12 = fun_2450(rdi11, rbx5), eax12 != -1) {
        if (eax12 == 90) {
            addr_27d0_4:
            zf13 = optarg == 0;
            if (zf13) 
                continue;
        } else {
            if (eax12 > 90) {
                addr_27a8_6:
                if (eax12 != 0x6d) 
                    goto addr_2a2b_7; else 
                    goto addr_27b1_8;
            } else {
                if (eax12 != 0xffffff7d) {
                    if (eax12 != 0xffffff7e) 
                        goto addr_2a2b_7;
                    usage();
                    goto addr_27d0_4;
                } else {
                    rdi14 = stdout;
                    rdx10 = reinterpret_cast<struct s0*>("GNU coreutils");
                    version_etc(rdi14, "mknod");
                    eax12 = fun_2620();
                    goto addr_27a8_6;
                }
            }
        }
        fun_2410();
        fun_25e0();
        continue;
        addr_27b1_8:
        r14_3 = optarg;
    }
    if (!r14_3) {
        *reinterpret_cast<uint32_t*>(&r12_9) = 0x1b6;
    } else {
        rax15 = mode_compile(r14_3, rbx5);
        if (!rax15) {
            fun_2410();
            fun_25e0();
            goto addr_2b53_18;
        } else {
            fun_2520();
            fun_2520();
            rcx16 = rax15;
            eax17 = mode_adjust(0x1b6);
            *reinterpret_cast<uint32_t*>(&r12_9) = eax17;
            fun_2360(rax15, rax15);
            if (*reinterpret_cast<uint32_t*>(&r12_9) & 0xfffffe00) 
                goto addr_2cd6_20;
        }
    }
    rcx16 = reinterpret_cast<struct s0*>(static_cast<int64_t>(optind));
    rdx18 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp4) - *reinterpret_cast<int32_t*>(&rcx16)));
    if (*reinterpret_cast<int32_t*>(&rcx16) >= *reinterpret_cast<int32_t*>(&rbp4)) {
        if (rdx18 <= 1) {
            rax19 = fun_2410();
            rdx10 = rax19;
            fun_25e0();
            goto addr_2a2b_7;
        }
        if (rdx18 != 2) 
            goto addr_2a58_25;
    } else {
        if (static_cast<int32_t>(reinterpret_cast<uint64_t>(&rcx16->f1)) >= *reinterpret_cast<int32_t*>(&rbp4)) 
            goto addr_2889_27;
        rax20 = *reinterpret_cast<int32_t*>(&rcx16);
        if (**reinterpret_cast<signed char**>(rbx5 + rax20 + 1) == 0x70) 
            goto addr_2b15_29;
        addr_2889_27:
        if (rdx18 <= 3) 
            goto addr_2b71_30; else 
            goto addr_2893_31;
    }
    addr_289d_32:
    rax20 = *reinterpret_cast<int32_t*>(&rcx16);
    do {
        rax21 = rax20 + 1;
        rdi22 = rbx5[rax21];
        rdx10 = reinterpret_cast<struct s0*>(rax21 * 8);
        eax23 = *rdi22;
        if (*reinterpret_cast<signed char*>(&eax23) != 0x70) 
            break;
        rdi24 = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbx5) + reinterpret_cast<unsigned char>(rdx10)) - 8);
        *reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&r12_9);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0;
        eax26 = fun_2510(rdi24, rsi25);
        if (!eax26) 
            goto addr_29a1_35;
        rax27 = quotearg_n_style_colon();
        r12_9 = rax27;
        fun_2380();
        rcx16 = r12_9;
        rdx18 = reinterpret_cast<uint64_t>("%s");
        rax20 = fun_25e0();
        addr_2b15_29:
        if (rdx18 <= 1) 
            goto addr_2b53_18;
    } while (rdx18 == 2);
    goto addr_2b25_38;
    if (*reinterpret_cast<signed char*>(&eax23) > 0x70) 
        goto addr_2a35_40;
    if (*reinterpret_cast<signed char*>(&eax23) == 98) 
        goto addr_28cf_42;
    if (*reinterpret_cast<signed char*>(&eax23) == 99) 
        goto addr_28cf_42; else 
        goto addr_29ff_44;
    addr_2b53_18:
    rdi28 = *(rbx5 + static_cast<int64_t>(rbp4) - 1);
    rax29 = quote(rdi28, rdi28);
    r12_9 = rax29;
    goto addr_2a13_45;
    addr_2b25_38:
    rdi30 = (rbx5 + rax20)[2];
    addr_2a5d_46:
    rax31 = quote(rdi30, rdi30);
    r12_9 = rax31;
    fun_2410();
    fun_25e0();
    ebp32 = *reinterpret_cast<int32_t*>(&rbp4) - optind;
    *reinterpret_cast<int32_t*>(&rdx10) = 5;
    *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
    if (ebp32 != 4) {
        while (1) {
            addr_2a2b_7:
            *reinterpret_cast<int32_t*>(&rdi22) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
            *reinterpret_cast<signed char*>(&eax23) = usage();
            addr_2a35_40:
            if (*reinterpret_cast<signed char*>(&eax23) != 0x75) {
                addr_29ff_44:
                rax33 = quote(rdi22, rdi22);
                r12_9 = rax33;
            } else {
                addr_28cf_42:
                r13_34 = *reinterpret_cast<unsigned char**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbx5) + reinterpret_cast<unsigned char>(rdx10)) + 8);
                rbp4 = *reinterpret_cast<unsigned char**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbx5) + reinterpret_cast<unsigned char>(rdx10)) + 16);
                eax35 = xstrtoumax(r13_34);
                if (eax35) 
                    goto addr_2c9f_48;
                rax36 = v37;
                *reinterpret_cast<int32_t*>(&rdx38) = *reinterpret_cast<int32_t*>(&rax36);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                if (rax36 != rdx38) 
                    goto addr_2c9f_48; else 
                    goto addr_2909_50;
            }
            addr_2a13_45:
            rax39 = fun_2410();
            rdx10 = rax39;
            fun_25e0();
            continue;
            addr_2c9f_48:
            rax40 = quote(r13_34, r13_34);
            fun_2410();
            rcx16 = rax40;
            fun_25e0();
            addr_2cd1_51:
            fun_2440();
            addr_2cd6_20:
            fun_2410();
            fun_25e0();
            addr_2cfa_52:
            rdi41 = (rbx5 + reinterpret_cast<unsigned char>(rcx16))[4];
            rax42 = quote(rdi41, rdi41);
            r12_9 = rax42;
            goto addr_2a13_45;
            addr_2909_50:
            eax43 = xstrtoumax(rbp4);
            if (eax43 || (rax44 = v45, *reinterpret_cast<int32_t*>(&rdx46) = *reinterpret_cast<int32_t*>(&rax44), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx46) + 4) = 0, rax44 != rdx46)) {
                addr_2c6d_53:
                quote(rbp4, rbp4);
                fun_2410();
                fun_25e0();
                goto addr_2c9f_48;
            } else {
                *reinterpret_cast<int32_t*>(&rdx47) = v48;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                rcx49 = rdx47 << 8;
                *reinterpret_cast<uint32_t*>(&rcx50) = *reinterpret_cast<uint32_t*>(&rcx49) & 0xfff00;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx50) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx51) = *reinterpret_cast<unsigned char*>(&rax44);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx51) + 4) = 0;
                rcx16 = reinterpret_cast<struct s0*>(rcx51 | (rdx47 << 32 & 0xfffff00000000000 | rcx50));
                if ((rax44 << 12 & 0xffffff00000 | reinterpret_cast<unsigned char>(rcx16)) == 0xffffffffffffffff) {
                    addr_2c43_55:
                    fun_2410();
                    fun_25e0();
                    goto addr_2c6d_53;
                } else {
                    rax52 = optind;
                    rdi53 = rbx5[rax52];
                    eax54 = fun_2540(rdi53);
                    if (eax54) {
                        addr_2c0c_57:
                        quotearg_n_style_colon();
                        fun_2380();
                        fun_25e0();
                        goto addr_2c43_55;
                    } else {
                        addr_29a1_35:
                        if (!r14_3 || (rax55 = optind, *reinterpret_cast<uint32_t*>(&rsi56) = *reinterpret_cast<uint32_t*>(&r12_9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi56) + 4) = 0, rdi57 = rbx5[rax55], eax58 = fun_23c0(rdi57, rsi56), !eax58)) {
                            rax59 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
                            if (rax59) 
                                goto addr_2cd1_51; else 
                                break;
                        } else {
                            rax60 = optind;
                            rsi61 = rbx5[rax60];
                            rax62 = quotearg_style(4, rsi61);
                            r13_34 = rax62;
                            fun_2410();
                            fun_2380();
                            fun_25e0();
                            goto addr_2c0c_57;
                        }
                    }
                }
            }
        }
    } else {
        addr_2aa0_60:
        rax63 = fun_2410();
        rdi64 = stderr;
        rdx10 = reinterpret_cast<struct s0*>("%s\n");
        fun_2640(rdi64, 1, "%s\n", rax63);
        goto addr_2a2b_7;
    }
    return 0;
    addr_2a58_25:
    rdi30 = (rbx5 + reinterpret_cast<unsigned char>(rcx16))[2];
    goto addr_2a5d_46;
    addr_2b71_30:
    rdi65 = *(rbx5 + *reinterpret_cast<int32_t*>(&rbp4) - 1);
    rax66 = quote(rdi65, rdi65);
    r12_9 = rax66;
    rax67 = fun_2410();
    rdx10 = rax67;
    fun_25e0();
    ebp68 = *reinterpret_cast<int32_t*>(&rbp4) - optind;
    if (ebp68 != 2) 
        goto addr_2a2b_7;
    goto addr_2aa0_60;
    addr_2893_31:
    if (rdx18 != 4) 
        goto addr_2cfa_52; else 
        goto addr_289d_32;
}

int64_t __libc_start_main = 0;

void fun_2d43() {
    __asm__("cli ");
    __libc_start_main(0x26c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2350(int64_t rdi);

int64_t fun_2de3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2350(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2e23() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_25d0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_24d0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int32_t fun_24f0(int64_t rdi);

int32_t fun_2390(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

void fun_2e33(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    int64_t r12_7;
    struct s0* rax8;
    int64_t r12_9;
    struct s0* rax10;
    int64_t r12_11;
    struct s0* rax12;
    int64_t r12_13;
    struct s0* rax14;
    int64_t r12_15;
    struct s0* rax16;
    int64_t r12_17;
    struct s0* rax18;
    int64_t r12_19;
    struct s0* rax20;
    int64_t r12_21;
    struct s0* rax22;
    struct s0* rax23;
    int32_t eax24;
    struct s0* r13_25;
    struct s0* rax26;
    struct s0* rax27;
    int32_t eax28;
    struct s0* rax29;
    struct s0* rax30;
    struct s0* rax31;
    int32_t eax32;
    struct s0* rax33;
    int64_t r15_34;
    struct s0* rax35;
    struct s0* rax36;
    struct s0* rax37;
    int64_t rdi38;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2410();
            fun_25d0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2410();
            fun_24d0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2410();
            fun_24d0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2410();
            fun_24d0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2410();
            fun_24d0(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2410();
            fun_24d0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2410();
            fun_24d0(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2410();
            fun_24d0(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_2410();
            fun_24d0(rax22, r12_21, 5, rcx6);
            rax23 = fun_2410();
            fun_25d0(1, rax23, "mknod", rcx6);
            do {
                if (1) 
                    break;
                eax24 = fun_24f0("mknod");
            } while (eax24);
            r13_25 = v4;
            if (!r13_25) {
                rax26 = fun_2410();
                fun_25d0(1, rax26, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax27 = fun_25c0(5);
                if (!rax27 || (eax28 = fun_2390(rax27, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax28)) {
                    rax29 = fun_2410();
                    r13_25 = reinterpret_cast<struct s0*>("mknod");
                    fun_25d0(1, rax29, "https://www.gnu.org/software/coreutils/", "mknod");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_25 = reinterpret_cast<struct s0*>("mknod");
                    goto addr_3220_9;
                }
            } else {
                rax30 = fun_2410();
                fun_25d0(1, rax30, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax31 = fun_25c0(5);
                if (!rax31 || (eax32 = fun_2390(rax31, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax32)) {
                    addr_3126_11:
                    rax33 = fun_2410();
                    fun_25d0(1, rax33, "https://www.gnu.org/software/coreutils/", "mknod");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_25 == "mknod")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x8d21);
                    }
                } else {
                    addr_3220_9:
                    r15_34 = stdout;
                    rax35 = fun_2410();
                    fun_24d0(rax35, r15_34, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3126_11;
                }
            }
            rax36 = fun_2410();
            rcx6 = r12_2;
            fun_25d0(1, rax36, r13_25, rcx6);
            addr_2e8e_14:
            fun_2620();
        }
    } else {
        rax37 = fun_2410();
        rdi38 = stderr;
        rcx6 = r12_2;
        fun_2640(rdi38, 1, rax37, rcx6);
        goto addr_2e8e_14;
    }
}

int64_t file_name = 0;

void fun_3253(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3263(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_23a0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_3273() {
    int64_t rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2380(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2410();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3303_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25e0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23a0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3303_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25e0();
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
};

struct s0* xnmalloc();

struct s0* xmalloc(int64_t rdi, void* rsi);

struct s5 {
    struct s0* f0;
    signed char f1;
    signed char[2] pad4;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
};

struct s0* fun_3323(struct s4* rdi, void* rsi) {
    struct s4* rbx3;
    int64_t rcx4;
    int32_t eax5;
    struct s4* rdx6;
    struct s0* rax7;
    uint64_t rdi8;
    struct s0* r9_9;
    struct s4* rax10;
    int64_t rbp11;
    int32_t edx12;
    uint32_t ebx13;
    struct s0* rax14;
    int64_t r8_15;
    int32_t r8d16;
    int64_t rax17;
    struct s4* rcx18;
    uint32_t edx19;
    int32_t ebx20;
    struct s5* rdi21;
    uint32_t esi22;
    uint32_t r10d23;
    uint32_t edx24;
    uint64_t r11_25;
    int1_t less_or_equal26;
    int1_t less_or_equal27;
    uint32_t eax28;
    int64_t rdx29;
    int32_t r8d30;

    __asm__("cli ");
    rbx3 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rdi->f0)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
    eax5 = static_cast<int32_t>(rcx4 - 48);
    if (*reinterpret_cast<unsigned char*>(&eax5) > 7) {
        rdx6 = rdi;
        if (*reinterpret_cast<unsigned char*>(&rcx4)) {
            do {
                if (*reinterpret_cast<unsigned char*>(&rcx4) <= 61) {
                }
                *reinterpret_cast<uint32_t*>(&rcx4) = rdx6->f1;
                rdx6 = reinterpret_cast<struct s4*>(&rdx6->f1);
            } while (*reinterpret_cast<unsigned char*>(&rcx4));
        }
        rax7 = xnmalloc();
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        r9_9 = rax7;
        goto addr_338e_8;
    }
    rax10 = rdi;
    *reinterpret_cast<struct s0**>(&rbp11) = reinterpret_cast<struct s0*>(0);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp11) + 4) = 0;
    do {
        *reinterpret_cast<struct s0**>(&rbp11) = reinterpret_cast<struct s0*>(static_cast<uint32_t>(rcx4 + rbp11 * 8 - 48));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp11) + 4) = 0;
        rax10 = reinterpret_cast<struct s4*>(&rax10->f1);
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp11)) > reinterpret_cast<unsigned char>(0xfff)) 
            break;
        *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rax10->f0)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
        edx12 = static_cast<int32_t>(rcx4 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edx12) <= 7);
    goto addr_3620_12;
    return 0;
    addr_3620_12:
    *reinterpret_cast<int32_t*>(&r9_9) = 0;
    *reinterpret_cast<int32_t*>(&r9_9 + 4) = 0;
    if (!*reinterpret_cast<unsigned char*>(&rcx4)) {
        ebx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp11)) & reinterpret_cast<uint32_t>("tion") | 0x3ff;
        if (reinterpret_cast<int64_t>(rax10) - reinterpret_cast<int64_t>(rbx3) >= 5) {
            ebx13 = 0xfff;
        }
        rax14 = xmalloc(32, rsi);
        *reinterpret_cast<struct s0**>(&rax14->f0) = reinterpret_cast<struct s0*>(0x13d);
        rax14->f4 = 0xfff;
        rax14->f8 = *reinterpret_cast<struct s0**>(&rbp11);
        rax14->fc = ebx13;
        rax14->f11 = 0;
        return rax14;
    }
    addr_33bb_17:
    return r9_9;
    addr_34f5_18:
    *reinterpret_cast<uint32_t*>(&r8_15) = *reinterpret_cast<unsigned char*>(&r8d16);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8830 + r8_15 * 4) + 0x8830;
    while (1) {
        addr_3450_19:
        *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
        rcx18 = reinterpret_cast<struct s4*>(&rbx3->f2);
        edx19 = 7;
        ebx20 = 3;
        do {
            addr_3409_20:
            rdi21->f0 = *reinterpret_cast<struct s0**>(&esi22);
            rdi21->f1 = *reinterpret_cast<signed char*>(&ebx20);
            rdi21->f4 = r10d23;
            rdi21->f8 = edx19;
            if (r10d23) {
                edx19 = edx19 & r10d23;
            }
            esi22 = *reinterpret_cast<uint32_t*>(&rax17);
            rbx3 = rcx18;
            while (1) {
                rdi21->fc = edx19;
                ++rdi21;
                edx24 = *reinterpret_cast<uint32_t*>(&rax17) & 0xffffffef;
                if (*reinterpret_cast<signed char*>(&edx24) == 45 || *reinterpret_cast<signed char*>(&rax17) == 43) {
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rbx3->f1)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    ++r11_25;
                    rcx18 = reinterpret_cast<struct s4*>(&rbx3->f1);
                    less_or_equal26 = *reinterpret_cast<signed char*>(&rax17) <= 0x6f;
                    if (*reinterpret_cast<signed char*>(&rax17) == 0x6f) 
                        goto addr_3450_19;
                } else {
                    if (*reinterpret_cast<signed char*>(&rax17) != 44) 
                        goto addr_369a_26;
                    rbx3 = reinterpret_cast<struct s4*>(&rcx18->f1);
                    rdi8 = r11_25;
                    addr_338e_8:
                    esi22 = rbx3->f0;
                    r10d23 = 0;
                    less_or_equal27 = reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&esi22)) <= reinterpret_cast<signed char>(0x67);
                    if (*reinterpret_cast<struct s0**>(&esi22) != 0x67) 
                        goto addr_339e_28;
                    while (1) {
                        r10d23 = r10d23 | 0x438;
                        rbx3 = reinterpret_cast<struct s4*>(&rbx3->f1);
                        while (esi22 = rbx3->f0, less_or_equal27 = reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&esi22)) <= reinterpret_cast<signed char>(0x67), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&esi22) == 0x67)) {
                            addr_339e_28:
                            if (!less_or_equal27) {
                                if (*reinterpret_cast<struct s0**>(&esi22) == 0x6f) {
                                    r10d23 = r10d23 | 0x207;
                                    rbx3 = reinterpret_cast<struct s4*>(&rbx3->f1);
                                } else {
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&esi22) == 0x75)) 
                                        goto addr_33b0_34;
                                    r10d23 = r10d23 | 0x9c0;
                                    rbx3 = reinterpret_cast<struct s4*>(&rbx3->f1);
                                }
                            } else {
                                if (*reinterpret_cast<struct s0**>(&esi22) != 97) 
                                    goto addr_33ae_37;
                                r10d23 = 0xfff;
                                rbx3 = reinterpret_cast<struct s4*>(&rbx3->f1);
                            }
                        }
                    }
                    addr_33ae_37:
                    if (reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&esi22)) > reinterpret_cast<signed char>(97)) 
                        goto addr_33b0_34;
                    eax28 = esi22 & 0xffffffef;
                    if (*reinterpret_cast<signed char*>(&eax28) == 45) 
                        goto addr_33d7_40;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&esi22) == 43)) 
                        goto addr_33b0_34;
                    addr_33d7_40:
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rbx3->f1)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    r11_25 = rdi8 + 1;
                    rcx18 = reinterpret_cast<struct s4*>(&rbx3->f1);
                    rdi21 = reinterpret_cast<struct s5*>((rdi8 << 4) + reinterpret_cast<unsigned char>(r9_9));
                    less_or_equal26 = *reinterpret_cast<signed char*>(&rax17) <= 0x6f;
                    if (*reinterpret_cast<signed char*>(&rax17) == 0x6f) 
                        goto addr_3450_19;
                }
                if (!less_or_equal26) 
                    break;
                if (*reinterpret_cast<signed char*>(&rax17) > 55) 
                    goto addr_3468_45;
                if (*reinterpret_cast<signed char*>(&rax17) <= 47) 
                    goto addr_3402_47;
                *reinterpret_cast<uint32_t*>(&rdx29) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx29) = static_cast<uint32_t>(rax17 + rdx29 * 8 - 48);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                    rcx18 = reinterpret_cast<struct s4*>(&rcx18->f1);
                    if (*reinterpret_cast<uint32_t*>(&rdx29) > 0xfff) 
                        goto addr_33b0_34;
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rcx18->f0)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    r8d30 = static_cast<int32_t>(rax17 - 48);
                } while (*reinterpret_cast<unsigned char*>(&r8d30) <= 7);
                if (r10d23) 
                    goto addr_33b0_34;
                if (!*reinterpret_cast<signed char*>(&rax17)) 
                    goto addr_3540_53;
                if (*reinterpret_cast<signed char*>(&rax17) != 44) 
                    goto addr_33b0_34;
                addr_3540_53:
                rdi21->f0 = *reinterpret_cast<struct s0**>(&esi22);
                rbx3 = rcx18;
                esi22 = *reinterpret_cast<uint32_t*>(&rax17);
                r10d23 = 0xfff;
                rdi21->f8 = *reinterpret_cast<uint32_t*>(&rdx29);
                edx19 = 0xfff;
                rdi21->f1 = 1;
                rdi21->f4 = 0xfff;
            }
            if (*reinterpret_cast<signed char*>(&rax17) == 0x75) {
                *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
                rcx18 = reinterpret_cast<struct s4*>(&rbx3->f2);
                edx19 = 0x1c0;
                ebx20 = 3;
                goto addr_3409_20;
            }
            ebx20 = 1;
            edx19 = 0;
            continue;
            addr_3468_45:
            if (*reinterpret_cast<signed char*>(&rax17) == 0x67) {
                *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
                rcx18 = reinterpret_cast<struct s4*>(&rbx3->f2);
                edx19 = 56;
                ebx20 = 3;
                goto addr_3409_20;
            }
            addr_3402_47:
            ebx20 = 1;
            edx19 = 0;
            goto addr_3409_20;
            r8d16 = static_cast<int32_t>(rax17 - 88);
        } while (*reinterpret_cast<unsigned char*>(&r8d16) > 32);
        goto addr_34f5_18;
    }
    addr_369a_26:
    if (*reinterpret_cast<signed char*>(&rax17)) {
        addr_33b0_34:
        fun_2360(r9_9);
        *reinterpret_cast<int32_t*>(&r9_9) = 0;
        *reinterpret_cast<int32_t*>(&r9_9 + 4) = 0;
        goto addr_33bb_17;
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r9_9) + (r11_25 << 4) + 1) = 0;
        goto addr_33bb_17;
    }
}

int32_t fun_2530();

struct s0* fun_36c3() {
    struct s0* rax1;
    int32_t eax2;
    struct s0* rax3;
    struct s0* v4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    eax2 = fun_2530();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(&rax3 + 4) = 0;
    if (!eax2) {
        rax3 = xmalloc(32, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
        *reinterpret_cast<struct s0**>(&rax3->f0) = reinterpret_cast<struct s0*>(0x13d);
        rax3->f4 = 0xfff;
        rax3->f8 = v4;
        rax3->fc = 0xfff;
        rax3->f11 = 0;
    }
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2440();
    } else {
        return rax3;
    }
}

struct s6 {
    unsigned char f0;
    unsigned char f1;
    signed char[2] pad4;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
    signed char[1] pad17;
    unsigned char f11;
};

int64_t fun_3743(uint32_t edi, int32_t esi, int32_t edx, struct s6* rcx, uint32_t* r8) {
    uint32_t r9d6;
    uint32_t edi7;
    int64_t rax8;
    int32_t r11d9;
    uint32_t r10d10;
    uint32_t edx11;
    uint32_t eax12;
    uint32_t esi13;
    uint32_t ebx14;
    uint32_t eax15;
    uint32_t edi16;
    uint32_t edi17;
    uint32_t edi18;
    uint32_t r12d19;
    uint32_t edi20;
    uint32_t eax21;
    uint32_t eax22;
    int64_t rax23;

    __asm__("cli ");
    r9d6 = edi & 0xfff;
    edi7 = rcx->f1;
    if (!*reinterpret_cast<signed char*>(&edi7)) {
        if (r8) {
            *r8 = 0;
        }
        *reinterpret_cast<uint32_t*>(&rax8) = r9d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
    r11d9 = esi;
    r10d10 = 0;
    edx11 = reinterpret_cast<uint32_t>(~edx);
    while (1) {
        eax12 = rcx->f8;
        if (*reinterpret_cast<signed char*>(&r11d9)) {
            esi13 = rcx->fc | 0xfffff3ff;
            ebx14 = ~rcx->fc & reinterpret_cast<uint32_t>("tion");
            if (*reinterpret_cast<signed char*>(&edi7) != 2) {
                addr_37c1_8:
                if (*reinterpret_cast<signed char*>(&edi7) == 3) {
                    eax15 = eax12 & r9d6;
                    edi16 = eax15 & 0x124;
                    edi17 = -edi16;
                    edi18 = edi17 - (edi17 + reinterpret_cast<uint1_t>(edi17 < edi17 + reinterpret_cast<uint1_t>(!!edi16))) & 0x124;
                    r12d19 = edi18;
                    *reinterpret_cast<unsigned char*>(&r12d19) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d19) | 0x92);
                    if (*reinterpret_cast<unsigned char*>(&eax15) & 0x92) {
                        edi18 = r12d19;
                    }
                    if (*reinterpret_cast<unsigned char*>(&eax15) & 73) {
                        edi18 = edi18 | 73;
                    }
                    eax12 = eax15 | edi18;
                }
            } else {
                addr_3875_14:
                eax12 = eax12 | 73;
                esi13 = ~ebx14;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&edi7) == 2) {
                ebx14 = r9d6 & 73;
                if (ebx14) {
                    ebx14 = 0;
                    goto addr_3875_14;
                } else {
                    esi13 = 0xffffffff;
                }
            } else {
                esi13 = 0xffffffff;
                ebx14 = 0;
                goto addr_37c1_8;
            }
        }
        edi20 = rcx->f0;
        eax21 = eax12 & esi13;
        if (!rcx->f4) {
            eax22 = eax21 & edx11;
            if (*reinterpret_cast<signed char*>(&edi20) == 45) {
                r10d10 = r10d10 | eax22;
                r9d6 = r9d6 & ~eax22;
            } else {
                if (*reinterpret_cast<signed char*>(&edi20) == 61) {
                    addr_381f_24:
                    edi7 = rcx->f11;
                    rcx = reinterpret_cast<struct s6*>(&rcx->pad17);
                    r10d10 = r10d10 | esi13 & 0xfff;
                    r9d6 = ebx14 & r9d6 | eax22;
                    if (*reinterpret_cast<signed char*>(&edi7)) 
                        continue; else 
                        break;
                } else {
                    addr_3786_25:
                    if (*reinterpret_cast<signed char*>(&edi20) == 43) {
                        r10d10 = r10d10 | eax22;
                        r9d6 = r9d6 | eax22;
                    }
                }
            }
            edi7 = rcx->f11;
            rcx = reinterpret_cast<struct s6*>(&rcx->pad17);
            if (!*reinterpret_cast<signed char*>(&edi7)) 
                break;
        } else {
            eax22 = eax21 & rcx->f4;
            if (*reinterpret_cast<signed char*>(&edi20) != 45) {
                if (*reinterpret_cast<signed char*>(&edi20) != 61) 
                    goto addr_3786_25;
                ebx14 = ebx14 | ~rcx->f4;
                esi13 = ~ebx14;
                goto addr_381f_24;
            }
        }
    }
    if (r8) 
        goto addr_3846_32;
    addr_3849_33:
    *reinterpret_cast<uint32_t*>(&rax23) = r9d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
    return rax23;
    addr_3846_32:
    *r8 = r10d10;
    goto addr_3849_33;
}

void fun_2630(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s7 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s7* fun_2480();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_38e3(struct s0* rdi) {
    int64_t rcx2;
    struct s0* rbx3;
    struct s7* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2630("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2370("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2480();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2390(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5083(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2380();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xc200;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_50c3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_50e3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc200);
    }
    *rdi = esi;
    return 0xc200;
}

int64_t fun_5103(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5143(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xc200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_5163(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xc200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x268a;
    if (!rdx) 
        goto 0x268a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc200;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_51a3(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s10* r8) {
    struct s10* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0xc200);
    }
    rax7 = fun_2380();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x51d6);
    *rax7 = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5223(int64_t rdi, int64_t rsi, struct s0** rdx, struct s11* rcx) {
    struct s11* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0xc200);
    }
    rax6 = fun_2380();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5251);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x52ac);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5313() {
    __asm__("cli ");
}

struct s0* gc078 = reinterpret_cast<struct s0*>(0);

int64_t slotvec0 = 0x100;

void fun_5323() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2360(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xc100) {
        fun_2360(rdi7);
        gc078 = reinterpret_cast<struct s0*>(0xc100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc070) {
        fun_2360(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xc070);
    }
    nslots = 1;
    return;
}

void fun_53c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_53e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_53f3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5413(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_5433(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2690;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s0* fun_54c3(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2695;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

struct s0* fun_5553(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x269a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2440();
    } else {
        return rax5;
    }
}

struct s0* fun_55e3(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x269f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s0* fun_5673(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6b80]");
    __asm__("movdqa xmm1, [rip+0x6b88]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6b71]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2440();
    } else {
        return rax10;
    }
}

struct s0* fun_5713(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6ae0]");
    __asm__("movdqa xmm1, [rip+0x6ae8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6ad1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2440();
    } else {
        return rax9;
    }
}

struct s0* fun_57b3(int64_t rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6a40]");
    __asm__("movdqa xmm1, [rip+0x6a48]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6a29]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2440();
    } else {
        return rax3;
    }
}

struct s0* fun_5843(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x69b0]");
    __asm__("movdqa xmm1, [rip+0x69b8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x69a6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2440();
    } else {
        return rax4;
    }
}

struct s0* fun_58d3(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26a4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s0* fun_5973(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x687a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6872]");
    __asm__("movdqa xmm2, [rip+0x687a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26a9;
    if (!rdx) 
        goto 0x26a9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

struct s0* fun_5a13(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x67da]");
    __asm__("movdqa xmm1, [rip+0x67e2]");
    __asm__("movdqa xmm2, [rip+0x67ea]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ae;
    if (!rdx) 
        goto 0x26ae;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2440();
    } else {
        return rax9;
    }
}

struct s0* fun_5ac3(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x672a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6722]");
    __asm__("movdqa xmm2, [rip+0x672a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b3;
    if (!rsi) 
        goto 0x26b3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s0* fun_5b63(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x668a]");
    __asm__("movdqa xmm1, [rip+0x6692]");
    __asm__("movdqa xmm2, [rip+0x669a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b8;
    if (!rsi) 
        goto 0x26b8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

void fun_5c03() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c13(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c33() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c53(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_2500(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, int64_t r8, uint64_t r9);

void fun_5c73(int64_t rdi, struct s0* rsi, struct s0* rdx, uint64_t rcx, struct s0** r8, uint64_t r9) {
    uint64_t r12_7;
    struct s0* rax8;
    struct s0* rax9;
    struct s0* r12_10;
    struct s0* rax11;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2640(rdi, 1, "%s %s\n", rdx, rdi, 1, "%s %s\n", rdx);
    } else {
        r9 = rcx;
        fun_2640(rdi, 1, "%s (%s) %s\n", rsi, rdi, 1, "%s (%s) %s\n", rsi);
    }
    rax8 = fun_2410();
    fun_2640(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8);
    fun_2500(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2410();
    fun_2640(rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", rdi, 1, rax9, "https://gnu.org/licenses/gpl.html");
    fun_2500(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        r12_10 = *r8;
        rax11 = fun_2410();
        fun_2640(rdi, 1, rax11, r12_10, rdi, 1, rax11, r12_10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8fc8 + r12_7 * 4) + 0x8fc8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_60e3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s12 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6103(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s12* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s12* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2440();
    } else {
        return;
    }
}

void fun_61a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6246_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6250_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2440();
    } else {
        return;
    }
    addr_6246_5:
    goto addr_6250_7;
}

void fun_6283() {
    int64_t rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2500(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2410();
    fun_25d0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2410();
    fun_25d0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2410();
    goto fun_25d0;
}

int64_t fun_23d0();

void xalloc_die();

void fun_6323(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2570(unsigned char* rdi);

void fun_6363(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2570(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6383(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2570(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_63a3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2570(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25b0();

void fun_63c3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25b0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_63f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6423(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6463() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_64a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_64d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6523(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23d0();
            if (rax5) 
                break;
            addr_656d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_656d_5;
        rax8 = fun_23d0();
        if (rax8) 
            goto addr_6556_9;
        if (rbx4) 
            goto addr_656d_5;
        addr_6556_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_65b3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23d0();
            if (rax8) 
                break;
            addr_65fa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_65fa_5;
        rax11 = fun_23d0();
        if (rax11) 
            goto addr_65e2_9;
        if (!rbx6) 
            goto addr_65e2_9;
        if (r12_4) 
            goto addr_65fa_5;
        addr_65e2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6643(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_66ed_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_6700_10:
                *r12_8 = 0;
            }
            addr_66a0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_66c6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6714_14;
            if (rcx10 <= rsi9) 
                goto addr_66bd_16;
            if (rsi9 >= 0) 
                goto addr_6714_14;
            addr_66bd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6714_14;
            addr_66c6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25b0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6714_14;
            if (!rbp13) 
                break;
            addr_6714_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_66ed_9;
        } else {
            if (!r13_6) 
                goto addr_6700_10;
            goto addr_66a0_11;
        }
    }
}

int64_t fun_24e0();

void fun_6743() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6773() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_67a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_67c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2550(signed char* rdi, struct s0* rsi, unsigned char* rdx);

void fun_67e3(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2570(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

void fun_6823(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2570(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

struct s13 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_6863(int64_t rdi, struct s13* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2570(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2550;
    }
}

struct s0* fun_2430(struct s0* rdi, ...);

void fun_68a3(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2430(rdi);
    rax3 = fun_2570(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

void fun_68e3() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2410();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_25e0();
    fun_2370(rdi1);
}

void fun_24a0(int64_t rdi);

void** fun_2670(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* fun_2600(struct s0* rdi);

int64_t fun_2470(struct s0* rdi);

int64_t fun_6923(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* v6;
    struct s0* rax7;
    struct s0* v8;
    void* rcx9;
    struct s0* rbx10;
    uint32_t r12d11;
    void* r9_12;
    struct s0* rax13;
    struct s0* r15_14;
    void* rax15;
    int64_t rax16;
    struct s0* rbp17;
    struct s0* r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    void** rax21;
    struct s0* rax22;
    int64_t rdx23;
    struct s0* rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<struct s0*>("lib/xstrtol.c");
        fun_24a0("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2440();
            while (1) {
                rbx10 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_6c94_6;
                    rbx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_6c94_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<struct s0**>(&r15_14->f0) = rax13;
            if (*reinterpret_cast<struct s0**>(&rax13->f0)) {
                r12d11 = r12d11 | 2;
            }
            addr_69dd_12:
            *reinterpret_cast<struct s0**>(&v6->f0) = rbx10;
            addr_69e5_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2380();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0));
    rax21 = fun_2670(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = rax22->f1;
        rax22 = reinterpret_cast<struct s0*>(&rax22->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_6a1b_21;
    rsi = r15_14;
    rax24 = fun_2600(rbp17);
    r8 = *reinterpret_cast<struct s0**>(&r15_14->f0);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2470(r13_18), r8 = r8, rax26 == 0))) {
            addr_6a1b_21:
            r12d11 = 4;
            goto addr_69e5_13;
        } else {
            addr_6a59_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2470(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = r8->f1;
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(r8->f2 == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x9078 + rbp33 * 4) + 0x9078;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_6a1b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_69dd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_69dd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2470(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_6a59_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<struct s0**>(&v6->f0) = rbx10;
    goto addr_69e5_13;
}

int64_t fun_23b0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_6d53(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23b0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_6dae_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2380();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_6dae_3;
            rax6 = fun_2380();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2560(struct s14* rdi);

int32_t fun_25a0(struct s14* rdi);

int64_t fun_2490(int64_t rdi, ...);

int32_t rpl_fflush(struct s14* rdi);

int64_t fun_23f0(struct s14* rdi);

int64_t fun_6dc3(struct s14* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2560(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25a0(rdi);
        if (!(eax3 && (eax4 = fun_2560(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2490(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2380();
            r12d9 = *rax8;
            rax10 = fun_23f0(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_23f0;
}

void rpl_fseeko(struct s14* rdi);

void fun_6e53(struct s14* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25a0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6ea3(struct s14* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2560(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2490(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2590(int64_t rdi);

signed char* fun_6f23() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2590(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2460(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6f63(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2460(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2440();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6ff3() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax3;
    }
}

int64_t fun_7073(int64_t rdi, signed char* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25c0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2430(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2550(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2550(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7123() {
    __asm__("cli ");
    goto fun_25c0;
}

void fun_7133() {
    __asm__("cli ");
}

void fun_7147() {
    __asm__("cli ");
    return;
}

void fun_3680() {
    goto 0x34e7;
}

uint32_t fun_24c0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_2660(int64_t rdi, struct s0* rsi);

uint32_t fun_2650(struct s0* rdi, struct s0* rsi);

void fun_3b15() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2410();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2410();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2430(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3e13_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3e13_22; else 
                            goto addr_420d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_42cd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4620_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3e10_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3e10_30; else 
                                goto addr_4639_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2430(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4620_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24c0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4620_28; else 
                            goto addr_3cbc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4780_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4600_40:
                        if (r11_27 == 1) {
                            addr_418d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4748_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3dc7_44;
                            }
                        } else {
                            goto addr_4610_46;
                        }
                    } else {
                        addr_478f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_418d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3e13_22:
                                if (v47 != 1) {
                                    addr_4369_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2430(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_43b4_54;
                                    }
                                } else {
                                    goto addr_3e20_56;
                                }
                            } else {
                                addr_3dc5_57:
                                ebp36 = 0;
                                goto addr_3dc7_44;
                            }
                        } else {
                            addr_45f4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_478f_47; else 
                                goto addr_45fe_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_418d_41;
                        if (v47 == 1) 
                            goto addr_3e20_56; else 
                            goto addr_4369_52;
                    }
                }
                addr_3e81_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3d18_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3d3d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4040_65;
                    } else {
                        addr_3ea9_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_46f8_67;
                    }
                } else {
                    goto addr_3ea0_69;
                }
                addr_3d51_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad4);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_3d9c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_46f8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3d9c_81;
                }
                addr_3ea0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3d3d_64; else 
                    goto addr_3ea9_66;
                addr_3dc7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_3e7f_91;
                if (v22) 
                    goto addr_3ddf_93;
                addr_3e7f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3e81_62;
                addr_43b4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4b3b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4bab_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_49af_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2660(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2650(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_44ae_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_3e6c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_44b8_112;
                    }
                } else {
                    addr_44b8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4589_114;
                }
                addr_3e78_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_3e7f_91;
                while (1) {
                    addr_4589_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4a97_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_44f6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4aa5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4577_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4577_128;
                        }
                    }
                    addr_4525_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad4);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4577_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_44f6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad4);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4525_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3d9c_81;
                addr_4aa5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_46f8_67;
                addr_4b3b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_44ae_109;
                addr_4bab_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_44ae_109;
                addr_3e20_56:
                rax93 = fun_2670(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_3e6c_110;
                addr_45fe_59:
                goto addr_4600_40;
                addr_42cd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3e13_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3e78_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3dc5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3e13_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4312_160;
                if (!v22) 
                    goto addr_46e7_162; else 
                    goto addr_48f3_163;
                addr_4312_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_46e7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_46f8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_41bb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4023_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3d51_70; else 
                    goto addr_4037_169;
                addr_41bb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3d18_63;
                goto addr_3ea0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_45f4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_472f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3e10_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3d08_178; else 
                        goto addr_46b2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_45f4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3e13_22;
                }
                addr_472f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3e10_30:
                    r8d42 = 0;
                    goto addr_3e13_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3e81_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4748_42;
                    }
                }
                addr_3d08_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3d18_63;
                addr_46b2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4610_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3e81_62;
                } else {
                    addr_46c2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3e13_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4e72_188;
                if (v28) 
                    goto addr_46e7_162;
                addr_4e72_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4023_168;
                addr_3cbc_37:
                if (v22) 
                    goto addr_4cb3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3cd3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4780_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_480b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3e13_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3d08_178; else 
                        goto addr_47e7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_45f4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3e13_22;
                }
                addr_480b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3e13_22;
                }
                addr_47e7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4610_46;
                goto addr_46c2_186;
                addr_3cd3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3e13_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3e13_22; else 
                    goto addr_3ce4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_4dbe_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4c44_210;
            if (1) 
                goto addr_4c42_212;
            if (!v29) 
                goto addr_487e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_4db1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4040_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_3dfb_219; else 
            goto addr_405a_220;
        addr_3ddf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3df3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_405a_220; else 
            goto addr_3dfb_219;
        addr_49af_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_405a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_49cd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_4e40_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_48a6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_4a97_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3df3_221;
        addr_48f3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3df3_221;
        addr_4037_169:
        goto addr_4040_65;
        addr_4dbe_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_405a_220;
        goto addr_49cd_222;
        addr_4c44_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_4c9e_236;
        fun_2440();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4e40_225;
        addr_4c42_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4c44_210;
        addr_487e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4c44_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_48a6_226;
        }
        addr_4db1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_420d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8a8c + rax113 * 4) + 0x8a8c;
    addr_4639_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8b8c + rax114 * 4) + 0x8b8c;
    addr_4cb3_190:
    addr_3dfb_219:
    goto 0x3ae0;
    addr_3ce4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x898c + rax115 * 4) + 0x898c;
    addr_4c9e_236:
    goto v116;
}

void fun_3d00() {
}

void fun_3eb8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3bb2;
}

void fun_3f11() {
    goto 0x3bb2;
}

void fun_3ffe() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3e81;
    }
    if (v2) 
        goto 0x48f3;
    if (!r10_3) 
        goto addr_4a5e_5;
    if (!v4) 
        goto addr_492e_7;
    addr_4a5e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_492e_7:
    goto 0x3d34;
}

void fun_401c() {
}

void fun_40c7() {
    signed char v1;

    if (v1) {
        goto 0x404f;
    } else {
        goto 0x3d8a;
    }
}

void fun_40e1() {
    signed char v1;

    if (!v1) 
        goto 0x40da; else 
        goto "???";
}

void fun_4108() {
    goto 0x4023;
}

void fun_4188() {
}

void fun_41a0() {
}

void fun_41cf() {
    goto 0x4023;
}

void fun_4221() {
    goto 0x41b0;
}

void fun_4250() {
    goto 0x41b0;
}

void fun_4283() {
    goto 0x41b0;
}

void fun_4650() {
    goto 0x3d08;
}

void fun_494e() {
    signed char v1;

    if (v1) 
        goto 0x48f3;
    goto 0x3d34;
}

void fun_49f5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3d34;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3d18;
        goto 0x3d34;
    }
}

void fun_4e12() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4080;
    } else {
        goto 0x3bb2;
    }
}

void fun_5d48() {
    fun_2410();
}

void fun_6acc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x6adb;
}

void fun_6b9c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x6ba9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x6adb;
}

void fun_6bc0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_6bec() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x6adb;
}

void fun_6c0d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x6adb;
}

void fun_6c31() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x6adb;
}

void fun_6c55() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x6be4;
}

void fun_6c79() {
}

void fun_6c99() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x6be4;
}

void fun_6cb5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x6be4;
}

void fun_35d0() {
}

void fun_3f3e() {
    goto 0x3bb2;
}

void fun_4114() {
    goto 0x40cc;
}

void fun_41db() {
    goto 0x3d08;
}

void fun_422d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x41b0;
    goto 0x3ddf;
}

void fun_425f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x41bb;
        goto 0x3be0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x405a;
        goto 0x3dfb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x49f8;
    if (r10_8 > r15_9) 
        goto addr_4145_9;
    addr_414a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4a03;
    goto 0x3d34;
    addr_4145_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_414a_10;
}

void fun_4292() {
    goto 0x3dc7;
}

void fun_4660() {
    goto 0x3dc7;
}

void fun_4dff() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x3f1c;
    } else {
        goto 0x4080;
    }
}

void fun_5e00() {
}

void fun_6b6f() {
    if (__intrinsic()) 
        goto 0x6ba9; else 
        goto "???";
}

void fun_35e8() {
    goto 0x35d8;
}

void fun_429c() {
    goto 0x4237;
}

void fun_466a() {
    goto 0x418d;
}

void fun_5e60() {
    fun_2410();
    goto fun_2640;
}

void fun_35f0() {
    goto 0x35d8;
}

void fun_3f6d() {
    goto 0x3bb2;
}

void fun_42a8() {
    goto 0x4237;
}

void fun_4677() {
    goto 0x41de;
}

void fun_5ea0() {
    fun_2410();
    goto fun_2640;
}

void fun_35f8() {
    goto 0x35d8;
}

void fun_3f9a() {
    goto 0x3bb2;
}

void fun_42b4() {
    goto 0x41b0;
}

void fun_5ee0() {
    fun_2410();
    goto fun_2640;
}

void fun_3600() {
    goto 0x35d8;
}

void fun_3fbc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4950;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3e81;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3e81;
    }
    if (v11) 
        goto 0x4cb3;
    if (r10_12 > r15_13) 
        goto addr_4d03_8;
    addr_4d08_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4a41;
    addr_4d03_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4d08_9;
}

void fun_5f30() {
    struct s0* r12_1;
    struct s0** rbx2;
    struct s0* rax3;
    int64_t rbp4;
    int64_t v5;

    r12_1 = *rbx2;
    rax3 = fun_2410();
    fun_2640(rbp4, 1, rax3, r12_1, rbp4, 1, rax3, r12_1);
    goto v5;
}

void fun_5f88() {
    fun_2410();
    goto 0x5f59;
}

void fun_5fc0() {
    struct s0* r12_1;
    struct s0** rbx2;
    struct s0* rax3;
    int64_t rbp4;
    int64_t v5;

    r12_1 = *rbx2;
    rax3 = fun_2410();
    fun_2640(rbp4, 1, rax3, r12_1, rbp4, 1, rax3, r12_1);
    goto v5;
}

void fun_6038() {
    fun_2410();
    goto 0x5ffb;
}
