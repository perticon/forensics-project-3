void unblock_signal(int param_1)

{
  int iVar1;
  undefined8 uVar2;
  int *piVar3;
  long in_FS_OFFSET;
  sigset_t sStack168;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  sigemptyset(&sStack168);
  sigaddset(&sStack168,param_1);
  iVar1 = sigprocmask(1,&sStack168,(sigset_t *)0x0);
  if (iVar1 != 0) {
    uVar2 = dcgettext(0,"warning: sigprocmask",5);
    piVar3 = __errno_location();
    error(0,*piVar3,uVar2);
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}