void unblock_signal(uint32_t edi, ...) {
    struct s0* rax2;
    int32_t* rbp3;
    int64_t rsi4;
    int32_t eax5;
    void* rax6;

    rax2 = g28;
    rbp3 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x98);
    fun_26f0(rbp3);
    *reinterpret_cast<uint32_t*>(&rsi4) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi4) + 4) = 0;
    fun_28a0(rbp3, rsi4);
    eax5 = fun_24a0();
    if (eax5) {
        fun_25b0();
        fun_24f0();
        fun_27d0();
    }
    rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax6) {
        fun_25e0();
    } else {
        return;
    }
}