int64_t unblock_signal (int64_t arg1) {
    int64_t var_88h;
    rdi = arg1;
    r12d = edi;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rdi = rsp;
    sigemptyset ();
    esi = r12d;
    rdi = rbp;
    sigaddset ();
    edx = 0;
    rsi = rbp;
    edi = 1;
    eax = sigprocmask ();
    while (1) {
        rax = *((rsp + 0x88));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_0;
        }
        return rax;
        edx = 5;
        rax = dcgettext (0, "warning: sigprocmask");
        r12 = rax;
        rax = errno_location ();
        eax = 0;
        error (0, *(rax), r12);
    }
label_0:
    return stack_chk_fail ();
}