void cleanup(EVP_PKEY_CTX *ctx)

{
  double dVar1;
  int iVar2;
  int *piVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  int __sig;
  ulong uVar6;
  long in_FS_OFFSET;
  bool bVar7;
  undefined local_38 [24];
  long local_20;
  
  dVar1 = kill_after;
  uVar6 = (ulong)ctx & 0xffffffff;
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if ((int)ctx == 0xe) {
    timed_out = 1;
    uVar6 = (ulong)term_signal;
  }
  __sig = (int)uVar6;
  if (monitored_pid == 0) {
                    /* WARNING: Subroutine does not return */
    _exit(__sig + 0x80);
  }
  if (kill_after == _DAT_00108af0) {
    iVar2 = monitored_pid;
    if (verbose != '\0') {
LAB_00103358:
      iVar2 = sig2str(uVar6,local_38);
      if (iVar2 != 0) {
        __snprintf_chk(local_38,0x13,1,0x13,&DAT_00108046,uVar6);
      }
      uVar4 = quote();
      uVar5 = dcgettext(0,"sending signal %s to command %s",5);
      error(0,0,uVar5,local_38,uVar4);
      goto LAB_001033a5;
    }
  }
  else {
    piVar3 = __errno_location();
    term_signal = 9;
    iVar2 = *piVar3;
    settimeout(dVar1);
    bVar7 = verbose != '\0';
    kill_after = 0.0;
    *piVar3 = iVar2;
    if (bVar7) goto LAB_00103358;
LAB_001033a5:
    iVar2 = monitored_pid;
    if (monitored_pid == 0) {
      signal(__sig,(__sighandler_t)0x1);
    }
  }
  kill(iVar2,__sig);
  if (foreground == '\0') {
    signal(__sig,(__sighandler_t)0x1);
    kill(0,__sig);
    if ((__sig != 9) && (__sig != 0x12)) {
      if (iVar2 == 0) {
        signal(0x12,(__sighandler_t)0x1);
      }
      kill(iVar2,0x12);
      signal(0x12,(__sighandler_t)0x1);
      if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
        kill(0,0x12);
        return;
      }
      goto LAB_0010347c;
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_0010347c:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}