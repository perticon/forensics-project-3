void main(char * rsi[], int32 edi, struct Eq_503 * fs, word128 xmm0)
{
	ptr64 fp;
	word32 dwLoc016C;
	word64 rax_30 = fs->qw0028;
	set_program_name(rsi[0]);
	fn00000000000027B0("", 0x06);
	fn00000000000025A0("/usr/local/share/locale", "coreutils");
	fn0000000000002570("coreutils");
	g_dwC020 = 0x7D;
	atexit(&g_t3C60);
	uint64 rdi_77 = (uint64) edi;
	word32 edi_707 = (word32) rdi_77;
	int32 eax_78 = fn00000000000025F0(&g_tBA40, "+k:s:v", rsi, (word32) rdi_77, null);
	if (eax_78 != ~0x00)
	{
		if (eax_78 <= 0x81)
		{
			if (eax_78 > 0x6A)
			{
				uint64 rax_695 = (uint64) (eax_78 - 0x6B);
				if ((word32) rax_695 <= 22)
				{
					<anonymous> * rax_703 = (int64) g_a8A8C[rax_695 * 0x04] + 0x8A8C;
					word64 r9_714;
					word64 r10_715;
					word64 r11_716;
					rax_703();
					return;
				}
			}
			else
			{
				if (eax_78 == ~0x82)
				{
					version_etc(0x8049, stdout, fs);
					fn0000000000002840(0x00);
				}
				if (eax_78 == ~0x81)
					usage(0x00);
			}
		}
	}
	else
	{
		int64 rax_126 = (int64) g_dwC270;
		if (edi - (word32) rax_126 > 0x01)
		{
			byte * rdi_136 = rsi[rax_126];
			g_dwC270 = (word32) rax_126 + 0x01;
			word128 xmm0_148 = parse_duration(rdi_136, fs, xmm0);
			char ** rbx_157 = rsi + (int64) g_dwC270;
			command = (char *) *rbx_157;
			word64 r12_156 = (word64) xmm0_148;
			if (g_bC2BA == 0x00)
				fn0000000000002820();
			int32 r14d_231 = g_dwC010;
			fn00000000000026F0(fp - 0xE0);
			fn0000000000002540(null, fp - 232, 0x0E);
			fn0000000000002540(null, fp - 232, 0x02);
			fn0000000000002540(null, fp - 232, 0x03);
			fn0000000000002540(null, fp - 232, 0x01);
			fn0000000000002540(null, fp - 232, 0x0F);
			fn0000000000002540(null, fp - 232, r14d_231);
			fn00000000000026C0((void (*)(int32)) 0x01, 0x15);
			fn00000000000026C0((void (*)(int32)) 0x01, 22);
			fn00000000000026F0(fp - 0xE0);
			fn0000000000002540(null, fp - 232, 0x11);
			word32 eax_271 = unblock_signal(0x11, fs);
			fn00000000000028B0();
			g_dwC2C8 = eax_271;
			if (eax_271 != ~0x00)
			{
				if (eax_271 == 0x00)
				{
					fn00000000000026C0(null, 0x15);
					fn00000000000026C0(null, 22);
					fn0000000000002800(rbx_157, *rbx_157);
					word32 * rax_625 = fn00000000000024F0();
					quote(fs);
					fn00000000000027D0(fn00000000000025B0(0x05, "failed to run command %s", null), *rax_625, 0x00);
				}
				else
				{
					unblock_signal(0x0E, fs);
					settimeout(0x01, fs, (word128) r12_156);
					int32 r12d_354 = g_dwC010;
					fn00000000000026F0(fp - 232);
					fn00000000000028A0(0x0E, fp - 232);
					fn00000000000028A0(0x02, fp - 232);
					fn00000000000028A0(0x03, fp - 232);
					fn00000000000028A0(0x01, fp - 232);
					fn00000000000028A0(0x0F, fp - 232);
					fn00000000000028A0(r12d_354, fp - 232);
					fn00000000000028A0(0x11, fp - 232);
					int32 eax_371 = fn00000000000024A0(fp - 0x0168, fp - 232, 0x00);
					if (eax_371 != 0x00)
					{
						fn00000000000027D0(fn00000000000025B0(0x05, "warning: sigprocmask", null), *fn00000000000024F0(), 0x00);
						eax_371 = 0x00;
					}
					while (true)
					{
						fn00000000000027E0();
						if (eax_371 != 0x00)
							break;
						fn0000000000002780();
					}
					if (eax_371 >= 0x00)
					{
						uint64 r12_437 = (uint64) dwLoc016C;
						uint64 rbx_443 = (uint64) ((word32) r12_437 & 0x7F);
						byte r12b_477 = (byte) r12_437;
						int32 ebx_444 = (word32) rbx_443;
						if (ebx_444 != 0x00)
						{
							if ((byte) rbx_443 > 0x00)
							{
								if ((r12b_477 & 0x80) != 0x00)
									fn00000000000027D0(fn00000000000025B0(0x05, "the monitored command dumped core", null), 0x00, 0x00);
								if (g_dwC2CC == 0x00)
								{
									fn0000000000002720();
									if (true)
									{
										fn00000000000026C0(null, ebx_444);
										unblock_signal(ebx_444, fs);
										fn00000000000024C0(ebx_444);
									}
									else
										fn00000000000027D0(fn00000000000025B0(0x05, "warning: disabling core dumps failed", null), *fn00000000000024F0(), 0x00);
								}
								if (g_dwC2CC != 0x00 && ebx_444 == 0x09)
									g_bC2B9 = 0x01;
							}
							else
								fn00000000000027D0(fn00000000000025B0(0x05, "unknown status from command (%d)", null), 0x00, 0x00);
						}
					}
					else
						fn00000000000027D0(fn00000000000025B0(0x05, "error waiting for command", null), *fn00000000000024F0(), 0x00);
				}
			}
			else
				fn00000000000027D0(fn00000000000025B0(0x05, "fork system call failed", null), *fn00000000000024F0(), 0x00);
			if (rax_30 - fs->qw0028 == 0x00)
				return;
			fn00000000000025E0();
		}
	}
	usage(0x7D);
}