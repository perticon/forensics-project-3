uint main(int param_1,undefined8 *param_2)

{
  char **__argv;
  int iVar1;
  __pid_t _Var2;
  int *piVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  uint uVar6;
  long in_FS_OFFSET;
  undefined8 extraout_XMM0_Qa;
  undefined8 extraout_XMM0_Qa_00;
  uint local_16c;
  sigset_t local_168;
  sigaction local_e8;
  undefined local_48 [24];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  exit_failure = 0x7d;
  atexit(close_stdout);
LAB_001029b0:
  do {
    iVar1 = getopt_long(param_1,param_2,"+k:s:v",long_options,0);
    if (iVar1 == -1) {
LAB_00102ab9:
      iVar1 = param_1 - optind;
      param_1 = iVar1 + -1;
      if (param_1 != 0 && 0 < iVar1) {
        optind = optind + 1;
        parse_duration();
        __argv = (char **)(param_2 + optind);
        command = *__argv;
        if (foreground == '\0') {
          setpgid(0,0);
        }
        iVar1 = term_signal;
        sigemptyset(&local_e8.sa_mask);
        local_e8.sa_flags = 0x10000000;
        local_e8.__sigaction_handler = cleanup;
        sigaction(0xe,&local_e8,(sigaction *)0x0);
        sigaction(2,&local_e8,(sigaction *)0x0);
        sigaction(3,&local_e8,(sigaction *)0x0);
        sigaction(1,&local_e8,(sigaction *)0x0);
        sigaction(0xf,&local_e8,(sigaction *)0x0);
        sigaction(iVar1,&local_e8,(sigaction *)0x0);
        signal(0x15,(__sighandler_t)0x1);
        signal(0x16,(__sighandler_t)0x1);
        sigemptyset(&local_e8.sa_mask);
        local_e8.sa_flags = 0x10000000;
        local_e8.__sigaction_handler = chld;
        sigaction(0x11,&local_e8,(sigaction *)0x0);
        unblock_signal(0x11);
        monitored_pid = fork();
        if (monitored_pid == -1) {
          uVar4 = dcgettext(0,"fork system call failed",5);
          piVar3 = __errno_location();
          error(0,*piVar3,uVar4);
          uVar6 = 0x7d;
        }
        else if (monitored_pid == 0) {
          signal(0x15,(__sighandler_t)0x0);
          signal(0x16,(__sighandler_t)0x0);
          execvp(*__argv,__argv);
          piVar3 = __errno_location();
          iVar1 = *piVar3;
          uVar4 = quote(command);
          uVar5 = dcgettext(0,"failed to run command %s",5);
          error(0,*piVar3,uVar5,uVar4);
          uVar6 = (iVar1 == 2) + 0x7e;
        }
        else {
          unblock_signal(0xe);
          settimeout(extraout_XMM0_Qa_00,1);
          iVar1 = term_signal;
          sigemptyset((sigset_t *)&local_e8);
          sigaddset((sigset_t *)&local_e8,0xe);
          sigaddset((sigset_t *)&local_e8,2);
          sigaddset((sigset_t *)&local_e8,3);
          sigaddset((sigset_t *)&local_e8,1);
          sigaddset((sigset_t *)&local_e8,0xf);
          sigaddset((sigset_t *)&local_e8,iVar1);
          sigaddset((sigset_t *)&local_e8,0x11);
          iVar1 = sigprocmask(0,(sigset_t *)&local_e8,&local_168);
          if (iVar1 != 0) {
            uVar4 = dcgettext(0,"warning: sigprocmask",5);
            piVar3 = __errno_location();
            error(0,*piVar3,uVar4);
          }
          while( true ) {
            _Var2 = waitpid(monitored_pid,(int *)&local_16c,1);
            if (_Var2 != 0) break;
            sigsuspend(&local_168);
          }
          if (_Var2 < 0) {
            uVar4 = dcgettext(0,"error waiting for command",5);
            piVar3 = __errno_location();
            error(0,*piVar3,uVar4);
            local_16c = 0x7d;
          }
          else {
            uVar6 = local_16c & 0x7f;
            if (uVar6 == 0) {
              local_16c = local_16c >> 8 & 0xff;
            }
            else if ((char)uVar6 == '\0' || (char)((char)uVar6 + '\x01') < '\x01') {
              uVar4 = dcgettext(0,"unknown status from command (%d)",5);
              error(0,0,uVar4,local_16c);
              local_16c = 1;
            }
            else {
              if ((local_16c & 0x80) != 0) {
                uVar4 = dcgettext(0,"the monitored command dumped core",5);
                error(0,0,uVar4);
              }
              if (timed_out == 0) {
                iVar1 = prctl(4);
                if (iVar1 == 0) {
                  signal(uVar6,(__sighandler_t)0x0);
                  unblock_signal();
                  raise(uVar6);
                }
                else {
                  uVar4 = dcgettext(0,"warning: disabling core dumps failed",5);
                  piVar3 = __errno_location();
                  error(0,*piVar3,uVar4);
                }
              }
              if ((timed_out != 0) && (uVar6 == 9)) {
                preserve_status = '\x01';
              }
              local_16c = uVar6 + 0x80;
            }
          }
          uVar6 = local_16c;
          if ((timed_out != 0) && (preserve_status == '\0')) {
            local_16c = 0x7c;
            uVar6 = local_16c;
          }
        }
        if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
          return uVar6;
        }
                    /* WARNING: Subroutine does not return */
        __stack_chk_fail();
      }
      goto switchD_001029eb_caseD_6c;
    }
    if (0x81 < iVar1) goto switchD_001029eb_caseD_6c;
    if (iVar1 < 0x6b) {
      if (iVar1 == -0x83) {
        version_etc(stdout,"timeout","GNU coreutils",Version,"Padraig Brady",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar1 == -0x82) {
        usage(0);
        goto LAB_00102ab9;
      }
      goto switchD_001029eb_caseD_6c;
    }
    switch(iVar1) {
    case 0x6b:
      goto switchD_001029eb_caseD_6b;
    case 0x73:
      term_signal = operand2sig(optarg,local_48);
      if (term_signal != -1) break;
    default:
switchD_001029eb_caseD_6c:
      usage(0x7d);
      goto switchD_001029eb_caseD_6b;
    case 0x76:
      verbose = 1;
      break;
    case 0x80:
      foreground = '\x01';
      break;
    case 0x81:
      goto switchD_001029eb_caseD_81;
    }
  } while( true );
switchD_001029eb_caseD_81:
  preserve_status = '\x01';
  goto LAB_001029b0;
switchD_001029eb_caseD_6b:
  parse_duration();
  kill_after = extraout_XMM0_Qa;
  goto LAB_001029b0;
}