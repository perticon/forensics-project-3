disable_core_dumps (void)
{
#if HAVE_PRCTL && defined PR_SET_DUMPABLE
  if (prctl (PR_SET_DUMPABLE, 0) == 0)
    return true;

#elif HAVE_SETRLIMIT && defined RLIMIT_CORE
  /* Note this doesn't disable processing by a filter in
     /proc/sys/kernel/core_pattern on Linux.  */
  if (setrlimit (RLIMIT_CORE, &(struct rlimit) {0,0}) == 0)
    return true;

#else
  return false;
#endif

  error (0, errno, _("warning: disabling core dumps failed"));
  return false;
}