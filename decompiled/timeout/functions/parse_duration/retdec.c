float64_t parse_duration(char * str) {
    int64_t v1 = __readfsqword(40); // 0x38c1
    char * ep; // bp-48, 0x38b0
    char * v2; // bp-40, 0x38b0
    bool v3 = xstrtod(str, &v2, (float64_t *)&ep, (float64_t (*)(char *, char **))0x3b80); // 0x38db
    bool v4 = false; // 0x38e2
    if (v3) {
        goto lab_0x38ee;
    } else {
        uint32_t v5 = *(int32_t *)function_24f0(); // 0x38e9
        v4 = v5 < 34;
        if (v5 != 34) {
            goto lab_0x3954;
        } else {
            goto lab_0x38ee;
        }
    }
  lab_0x38ee:;
    int128_t v6 = __asm_movsd_1((int64_t)ep); // 0x38ee
    __asm_comisd(v6, g4);
    int128_t v7 = v6; // 0x38fc
    int64_t result; // 0x38b0
    int128_t v8; // 0x38b0
    if (v4) {
        goto lab_0x3954;
    } else {
        char v9 = *v2; // 0x3903
        int64_t v10 = v6;
        result = v10;
        if (v9 == 0) {
            goto lab_0x3922;
        } else {
            // 0x390a
            v7 = v6;
            if (*(char *)((int64_t)v2 + 1) != 0) {
                goto lab_0x3954;
            } else {
                // 0x3910
                v8 = v6;
                if (v9 == 104) {
                    goto lab_0x3990;
                } else {
                    if (v9 > 104) {
                        if (v9 != 109) {
                            // 0x3950
                            result = v10;
                            v7 = v6;
                            if (v9 == 115) {
                                goto lab_0x3922;
                            } else {
                                goto lab_0x3954;
                            }
                        } else {
                            // 0x391a
                            result = __asm_mulsd(v6, 0x404e000000000000);
                            goto lab_0x3922;
                        }
                    } else {
                        // 0x3940
                        v7 = v6;
                        if (v9 != 100) {
                            goto lab_0x3954;
                        } else {
                            // 0x3944
                            result = __asm_mulsd(v6, 0x40f5180000000000);
                            goto lab_0x3922;
                        }
                    }
                }
            }
        }
    }
  lab_0x3954:
    // 0x3954
    quote(str);
    function_25b0();
    function_27d0();
    usage(125);
    v8 = v7;
    goto lab_0x3990;
  lab_0x3990:
    // 0x3990
    result = __asm_mulsd(v8, 0x40ac200000000000);
    goto lab_0x3922;
  lab_0x3922:
    // 0x3922
    if (v1 == __readfsqword(40)) {
        // 0x3932
        return result;
    }
    // 0x399a
    function_25e0();
    return result;
}