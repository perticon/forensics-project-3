void parse_duration(undefined8 param_1)

{
  char cVar1;
  int *piVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  double local_30;
  char *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  cVar1 = xstrtod(param_1,&local_28,&local_30,cl_strtod);
  if (cVar1 == '\0') {
    piVar2 = __errno_location();
    if (*piVar2 == 0x22) goto LAB_001038ee;
LAB_00103954:
    uVar3 = quote(param_1);
    uVar4 = dcgettext(0,"invalid time interval %s",5);
    error(0,0,uVar4,uVar3);
    local_30 = (double)usage(0x7d);
  }
  else {
LAB_001038ee:
    if (local_30 < _DAT_00108af0) goto LAB_00103954;
    cVar1 = *local_28;
    if (cVar1 == '\0') goto LAB_00103922;
    if (local_28[1] != '\0') goto LAB_00103954;
    if (cVar1 != 'h') {
      if (cVar1 < 'i') {
        if (cVar1 == 'd') {
          local_30 = local_30 * _DAT_00108b08;
          goto LAB_00103922;
        }
      }
      else {
        if (cVar1 == 'm') {
          local_30 = local_30 * _DAT_00108af8;
          goto LAB_00103922;
        }
        if (cVar1 == 's') goto LAB_00103922;
      }
      goto LAB_00103954;
    }
  }
  local_30 = local_30 * _DAT_00108b00;
LAB_00103922:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail(local_30);
}