word128 parse_duration(byte * rdi, struct Eq_503 * fs, word128 xmm0)
{
	ptr64 fp;
	Eq_1191 rLoc30;
	struct Eq_1520 * qwLoc28;
	word64 rax_15 = fs->qw0028;
	if (xstrtod(&g_t3B80, fp - 0x30, fp - 0x28, rdi, fs, xmm0) != 0x00 || *fn00000000000024F0() == 0x22)
	{
		word128 xmm0_113 = SEQ(0x00, rLoc30);
		if (rLoc30 < g_t8AF0)
			goto l0000000000003954;
		ci8 al_53 = qwLoc28->b0000;
		if (al_53 != 0x00)
		{
			if (qwLoc28->b0001 != 0x00)
				goto l0000000000003954;
			if (al_53 != 0x68)
			{
				if (al_53 > 0x68)
				{
					if (al_53 == 0x6D)
						xmm0_113 = SEQ(0x00, rLoc30 * 60.0);
					else if (al_53 != 115)
						goto l0000000000003954;
				}
				else
				{
					if (al_53 != 100)
						goto l0000000000003954;
					xmm0_113 = SEQ(0x00, rLoc30 * 86400.0);
				}
			}
			else
				xmm0_113 = SEQ(0x00, rLoc30 * 3600.0);
		}
		if (rax_15 - fs->qw0028 == 0x00)
			return xmm0_113;
		fn00000000000025E0();
	}
	else
	{
l0000000000003954:
		quote(fs);
		fn00000000000027D0(fn00000000000025B0(0x05, "invalid time interval %s", null), 0x00, 0x00);
		usage(0x7D);
	}
}