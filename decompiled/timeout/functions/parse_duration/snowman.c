void parse_duration(struct s0* rdi, ...) {
    struct s0* rax2;
    struct s0** rsi3;
    signed char al4;
    int1_t cf5;
    uint32_t* rax6;
    struct s0* v7;
    uint32_t eax8;
    struct s1* v9;
    void* rax10;

    rax2 = g28;
    rsi3 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 40 + 16);
    al4 = xstrtod();
    cf5 = 0;
    if (al4) 
        goto addr_38ee_2;
    rax6 = fun_24f0();
    cf5 = *rax6 < 34;
    if (*rax6 != 34) 
        goto addr_3954_4;
    addr_38ee_2:
    *reinterpret_cast<struct s0**>(&rdi->f0) = v7;
    rsi3 = rsi3 + 4;
    __asm__("comisd xmm0, [rip+0x51f4]");
    if (cf5) 
        goto addr_3954_4;
    eax8 = v9->f0;
    if (!*reinterpret_cast<signed char*>(&eax8)) {
        addr_3922_9:
        rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
        if (rax10) {
            fun_25e0();
        } else {
            return;
        }
    } else {
        if (v9->f1) 
            goto addr_3954_4;
        if (*reinterpret_cast<signed char*>(&eax8) != 0x68) 
            goto addr_3914_15;
    }
    addr_3990_16:
    __asm__("mulsd xmm0, [rip+0x5168]");
    goto addr_3922_9;
    addr_3914_15:
    if (*reinterpret_cast<signed char*>(&eax8) <= 0x68) {
        if (*reinterpret_cast<signed char*>(&eax8) != 100) {
            addr_3954_4:
            quote(rdi, rsi3, rdi, rsi3);
            fun_25b0();
            fun_27d0();
            usage();
            goto addr_3990_16;
        } else {
            __asm__("mulsd xmm0, [rip+0x51bc]");
            goto addr_3922_9;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax8) != 0x6d) {
            if (*reinterpret_cast<signed char*>(&eax8) == 0x73) 
                goto addr_3922_9; else 
                goto addr_3954_4;
        } else {
            __asm__("mulsd xmm0, [rip+0x51d6]");
            goto addr_3922_9;
        }
    }
}