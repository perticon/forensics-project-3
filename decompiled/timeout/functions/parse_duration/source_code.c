parse_duration (char const *str)
{
  double duration;
  char const *ep;

  if (! (xstrtod (str, &ep, &duration, cl_strtod) || errno == ERANGE)
      /* Nonnegative interval.  */
      || ! (0 <= duration)
      /* No extra chars after the number and an optional s,m,h,d char.  */
      || (*ep && *(ep + 1))
      /* Check any suffix char and update timeout based on the suffix.  */
      || !apply_time_suffix (&duration, *ep))
    {
      error (0, 0, _("invalid time interval %s"), quote (str));
      usage (EXIT_CANCELED);
    }

  return duration;
}