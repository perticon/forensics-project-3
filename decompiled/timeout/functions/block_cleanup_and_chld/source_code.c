block_cleanup_and_chld (int sigterm, sigset_t *old_set)
{
  sigset_t block_set;
  sigemptyset (&block_set);

  sigaddset (&block_set, SIGALRM);
  sigaddset (&block_set, SIGINT);
  sigaddset (&block_set, SIGQUIT);
  sigaddset (&block_set, SIGHUP);
  sigaddset (&block_set, SIGTERM);
  sigaddset (&block_set, sigterm);

  sigaddset (&block_set, SIGCHLD);

  if (sigprocmask (SIG_BLOCK, &block_set, old_set) != 0)
    error (0, errno, _("warning: sigprocmask"));
}