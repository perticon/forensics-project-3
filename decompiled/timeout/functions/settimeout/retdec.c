void settimeout(float64_t duration, bool warn) {
    // 0x3040
    int128_t v1; // 0x3040
    int128_t v2 = v1;
    int64_t v3 = warn ? 0xffffffff : 0; // 0x3043
    int64_t v4 = __readfsqword(40); // 0x3049
    int64_t v5 = __asm_movsd((int128_t)(int32_t)(float32_t)duration); // 0x3059
    dtotimespec(duration);
    int64_t v6 = function_2600(); // 0x3089
    int128_t v7 = __asm_movsd_1(v5); // 0x308e
    if ((int32_t)v6 == 0) {
        // 0x3110
        if ((int32_t)function_2490() == 0) {
            goto lab_0x30cd;
        } else {
            int128_t v8 = __asm_movsd_1(v5); // 0x312f
            if ((char)v3 != 0) {
                // 0x31a0
                function_25b0();
                function_24f0();
                function_27d0();
                v8 = __asm_movsd_1(v5);
            }
            int64_t v9 = __asm_movsd(v8); // 0x3136
            function_26e0();
            __asm_comisd(__asm_movsd_1(v9), g3);
            // 0x30c8
            function_2670();
            goto lab_0x30cd;
        }
    } else {
        if ((char)v3 == 0) {
            // 0x30b9
            __asm_comisd(v7, g3);
        } else {
            int64_t v10 = __asm_movsd(v7); // 0x309c
            int64_t v11 = function_24f0(); // 0x30a2
            int128_t v12 = __asm_movsd_1(v10); // 0x30a7
            uint32_t v13 = *(int32_t *)v11; // 0x30ad
            if (v13 != 38) {
                // 0x3160
                function_25b0();
                function_27d0();
                __asm_comisd(__asm_movsd_1(v10), g3);
            } else {
                // 0x30b9
                __asm_comisd(v12, g3);
                if (v13 < 38) {
                    int64_t v14 = __asm_cvttsd2si(v12); // 0x30f0
                    __asm_pxor(v2, v2);
                    __asm_comisd(v12, __asm_cvtsi2sd(v14 & 0xffffffff));
                }
            }
        }
        // 0x30c8
        function_2670();
        goto lab_0x30cd;
    }
  lab_0x30cd:
    // 0x30cd
    if (v4 == __readfsqword(40)) {
        // 0x30e1
        return;
    }
    // 0x31d4
    function_25e0();
}