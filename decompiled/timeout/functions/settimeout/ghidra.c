void settimeout(double param_1,char param_2)

{
  int iVar1;
  int *piVar2;
  undefined8 uVar3;
  uint __seconds;
  long in_FS_OFFSET;
  timer_t local_50;
  itimerspec local_48;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_48.it_value = dtotimespec();
  local_48.it_interval.tv_sec = 0;
  local_48.it_interval.tv_nsec = 0;
  iVar1 = timer_create(0,(sigevent *)0x0,&local_50);
  if (iVar1 == 0) {
    iVar1 = timer_settime(local_50,0,&local_48,(itimerspec *)0x0);
    if (iVar1 == 0) goto LAB_001030cd;
    if (param_2 != '\0') {
      uVar3 = dcgettext(0,"warning: timer_settime",5);
      piVar2 = __errno_location();
      error(0,*piVar2,uVar3);
    }
    timer_delete(local_50);
  }
  else if (param_2 != '\0') {
    piVar2 = __errno_location();
    if (*piVar2 != 0x26) {
      uVar3 = dcgettext(0,"warning: timer_create",5);
      error(0,*piVar2,uVar3);
    }
  }
  __seconds = 0xffffffff;
  if (param_1 < _DAT_00108ae8) {
    __seconds = (uint)((double)((long)param_1 & 0xffffffff) < param_1) + (int)(long)param_1;
  }
  alarm(__seconds);
LAB_001030cd:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}