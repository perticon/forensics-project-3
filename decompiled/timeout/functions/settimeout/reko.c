void settimeout(word32 edi, struct Eq_503 * fs, word128 xmm0)
{
	word64 rax_15 = fs->qw0028;
	word32 eax_49 = dtotimespec(0x00, xmm0);
	fn0000000000002600();
	Eq_1036 rLoc60_190 = (real64) xmm0;
	byte bl_65 = (byte) edi;
	word128 xmm0_109 = SEQ(0x00, rLoc60_190);
	if (eax_49 != 0x00)
	{
		if (bl_65 == 0x00)
		{
l00000000000030B9:
			if ((real64) xmm0_109 >= g_t8AE8)
				goto l00000000000030C8;
			goto l00000000000030F0;
		}
		word32 * rax_108 = fn00000000000024F0();
		xmm0_109 = SEQ(0x00, rLoc60_190);
		if (*rax_108 == 0x26)
			goto l00000000000030B9;
		fn00000000000027D0(fn00000000000025B0(0x05, "warning: timer_create", null), *rax_108, 0x00);
		if (rLoc60_190 >= g_t8AE8)
			goto l00000000000030C8;
	}
	else
	{
		fn0000000000002490();
		if (eax_49 == 0x00)
			goto l00000000000030CD;
		word128 xmm0_68 = SEQ(0x00, rLoc60_190);
		if (bl_65 != 0x00)
		{
			fn00000000000027D0(fn00000000000025B0(0x05, "warning: timer_settime", null), *fn00000000000024F0(), 0x00);
			xmm0_68 = SEQ(0x00, rLoc60_190);
		}
		fn00000000000026E0();
		if ((real64) xmm0_68 >= g_t8AE8)
		{
l00000000000030C8:
			fn0000000000002670();
l00000000000030CD:
			if (rax_15 - fs->qw0028 == 0x00)
				return;
			fn00000000000025E0();
		}
	}
l00000000000030F0:
	goto l00000000000030C8;
}