usage (int status)
{
  if (status != EXIT_SUCCESS)
    emit_try_help ();
  else
    {
      printf (_("\
Usage: %s [OPTION] DURATION COMMAND [ARG]...\n\
  or:  %s [OPTION]\n"), program_name, program_name);

      fputs (_("\
Start COMMAND, and kill it if still running after DURATION.\n\
"), stdout);

      emit_mandatory_arg_note ();

      fputs (_("\
      --preserve-status\n\
                 exit with the same status as COMMAND, even when the\n\
                   command times out\n\
      --foreground\n\
                 when not running timeout directly from a shell prompt,\n\
                   allow COMMAND to read from the TTY and get TTY signals;\n\
                   in this mode, children of COMMAND will not be timed out\n\
  -k, --kill-after=DURATION\n\
                 also send a KILL signal if COMMAND is still running\n\
                   this long after the initial signal was sent\n\
  -s, --signal=SIGNAL\n\
                 specify the signal to be sent on timeout;\n\
                   SIGNAL may be a name like 'HUP' or a number;\n\
                   see 'kill -l' for a list of signals\n"), stdout);
      fputs (_("\
  -v, --verbose  diagnose to stderr any signal sent upon timeout\n"), stdout);

      fputs (HELP_OPTION_DESCRIPTION, stdout);
      fputs (VERSION_OPTION_DESCRIPTION, stdout);

      fputs (_("\n\
DURATION is a floating point number with an optional suffix:\n\
's' for seconds (the default), 'm' for minutes, 'h' for hours or \
'd' for days.\nA duration of 0 disables the associated timeout.\n"), stdout);

      fputs (_("\n\
Upon timeout, send the TERM signal to COMMAND, if no other SIGNAL specified.\n\
The TERM signal kills any process that does not block or catch that signal.\n\
It may be necessary to use the KILL signal, since this signal can't be caught.\
\n"), stdout);

      fputs (_("\n\
EXIT status:\n\
  124  if COMMAND times out, and --preserve-status is not specified\n\
  125  if the timeout command itself fails\n\
  126  if COMMAND is found but cannot be invoked\n\
  127  if COMMAND cannot be found\n\
  137  if COMMAND (or timeout itself) is sent the KILL (9) signal (128+9)\n\
  -    the exit status of COMMAND otherwise\n\
"), stdout);

      emit_ancillary_info (PROGRAM_NAME);
    }
  exit (status);
}