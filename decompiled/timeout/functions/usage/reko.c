void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002860(fn00000000000025B0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000034EE;
	}
	fn00000000000027C0(fn00000000000025B0(0x05, "Usage: %s [OPTION] DURATION COMMAND [ARG]...\n  or:  %s [OPTION]\n", null), 0x01);
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "Start COMMAND, and kill it if still running after DURATION.\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "      --preserve-status\n                 exit with the same status as COMMAND, even when the\n                   command times out\n      --foreground\n                 when not running timeout directly from a shell prompt,\n                   allow COMMAND to read from the TTY and get TTY signals;\n                   in this mode, children of COMMAND will not be timed out\n  -k, --kill-after=DURATION\n                 also send a KILL signal if COMMAND is still running\n                   this long after the initial signal was sent\n  -s, --signal=SIGNAL\n                 specify the signal to be sent on timeout;\n                   SIGNAL may be a name like 'HUP' or a number;\n                   see 'kill -l' for a list of signals\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "  -v, --verbose  diagnose to stderr any signal sent upon timeout\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "\nDURATION is a floating point number with an optional suffix:\n's' for seconds (the default), 'm' for minutes, 'h' for hours or 'd' for days.\nA duration of 0 disables the associated timeout.\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "\nUpon timeout, send the TERM signal to COMMAND, if no other SIGNAL specified.\nThe TERM signal kills any process that does not block or catch that signal.\nIt may be necessary to use the KILL signal, since this signal can't be caught.\n", null));
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "\nEXIT status:\n  124  if COMMAND times out, and --preserve-status is not specified\n  125  if the timeout command itself fails\n  126  if COMMAND is found but cannot be invoked\n  127  if COMMAND cannot be found\n  137  if COMMAND (or timeout itself) is sent the KILL (9) signal (128+9)\n  -    the exit status of COMMAND otherwise\n", null));
	struct Eq_1401 * rbx_218 = fp - 0xB8 + 16;
	do
	{
		Eq_49 rsi_220 = rbx_218->qw0000;
		++rbx_218;
	} while (rsi_220 != 0x00 && fn00000000000026B0(rsi_220, "timeout") != 0x00);
	ptr64 r13_233 = rbx_218->qw0008;
	if (r13_233 != 0x00)
	{
		fn00000000000027C0(fn00000000000025B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_49 rax_320 = fn00000000000027B0(null, 0x05);
		if (rax_320 == 0x00 || fn0000000000002500(0x03, "en_", rax_320) == 0x00)
			goto l0000000000003786;
	}
	else
	{
		fn00000000000027C0(fn00000000000025B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_49 rax_262 = fn00000000000027B0(null, 0x05);
		if (rax_262 == 0x00 || fn0000000000002500(0x03, "en_", rax_262) == 0x00)
		{
			fn00000000000027C0(fn00000000000025B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l00000000000037C3:
			fn00000000000027C0(fn00000000000025B0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000034EE:
			fn0000000000002840(edi);
		}
		r13_233 = 0x8049;
	}
	fn0000000000002690(stdout, fn00000000000025B0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003786:
	fn00000000000027C0(fn00000000000025B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l00000000000037C3;
}