undefined4 tls_parse_stoc_alpn(long param_1,ushort **param_2)

{
  byte bVar1;
  ushort uVar2;
  int iVar3;
  ushort *puVar4;
  ushort *puVar5;
  int iVar6;
  undefined8 *puVar7;
  long lVar8;
  ulong uVar9;
  undefined8 uVar10;
  undefined8 *puVar11;
  ushort *puVar12;
  byte bVar13;
  
  bVar13 = 0;
  if (*(int *)(param_1 + 0x4a8) == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x631,"tls_parse_stoc_alpn");
    ossl_statem_fatal(param_1,0x6e,0x6e,0);
    return 0;
  }
  puVar4 = param_2[1];
  if ((ushort *)0x1 < puVar4) {
    puVar5 = *param_2;
    uVar2 = *puVar5;
    *param_2 = puVar5 + 1;
    puVar12 = puVar4 + -1;
    param_2[1] = puVar12;
    if ((puVar12 != (ushort *)0x0) &&
       ((ushort *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8) == puVar12)) {
      bVar1 = *(byte *)(puVar5 + 1);
      puVar12 = (ushort *)(ulong)bVar1;
      puVar4 = (ushort *)((long)puVar4 + -3);
      *param_2 = (ushort *)((long)puVar5 + 3);
      param_2[1] = puVar4;
      if (puVar4 == puVar12) {
        CRYPTO_free(*(void **)(param_1 + 0x488));
        puVar7 = (undefined8 *)CRYPTO_malloc((int)puVar4,"ssl/statem/extensions_clnt.c",0x641);
        *(undefined8 **)(param_1 + 0x488) = puVar7;
        if (puVar7 == (undefined8 *)0x0) {
          *(undefined8 *)(param_1 + 0x490) = 0;
          ERR_new();
          uVar10 = 0x644;
LAB_00163974:
          ERR_set_debug("ssl/statem/extensions_clnt.c",uVar10,"tls_parse_stoc_alpn");
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
          return 0;
        }
        if (puVar12 < param_2[1] || puVar12 == param_2[1]) {
          puVar11 = (undefined8 *)*param_2;
          if (bVar1 < 8) {
            if ((bVar1 & 4) == 0) {
              if ((bVar1 != 0) && (*(undefined *)puVar7 = *(undefined *)puVar11, (bVar1 & 2) != 0))
              {
                *(undefined2 *)((undefined *)((long)puVar7 + -2) + (long)puVar12) =
                     *(undefined2 *)((long)((long)puVar11 + -2) + (long)puVar12);
              }
            }
            else {
              *(undefined4 *)puVar7 = *(undefined4 *)puVar11;
              *(undefined4 *)((long)((long)puVar7 + -4) + (long)puVar12) =
                   *(undefined4 *)((long)((long)puVar11 + -4) + (long)puVar12);
            }
          }
          else {
            *puVar7 = *puVar11;
            *(undefined8 *)((long)(puVar7 + -1) + (long)puVar12) =
                 *(undefined8 *)((long)(puVar11 + -1) + (long)puVar12);
            lVar8 = (long)puVar7 - (long)(undefined8 *)((ulong)(puVar7 + 1) & 0xfffffffffffffff8);
            puVar11 = (undefined8 *)((long)puVar11 - lVar8);
            puVar7 = (undefined8 *)((ulong)(puVar7 + 1) & 0xfffffffffffffff8);
            for (uVar9 = (ulong)((int)lVar8 + (uint)bVar1 >> 3); uVar9 != 0; uVar9 = uVar9 - 1) {
              *puVar7 = *puVar11;
              puVar11 = puVar11 + (ulong)bVar13 * -2 + 1;
              puVar7 = puVar7 + (ulong)bVar13 * -2 + 1;
            }
          }
          *param_2 = (ushort *)((long)*param_2 + (long)puVar12);
          param_2[1] = (ushort *)((long)param_2[1] - (long)puVar12);
          lVar8 = *(long *)(param_1 + 0x918);
          *(ushort **)(param_1 + 0x490) = puVar12;
          if (*(void **)(lVar8 + 0x358) == (void *)0x0) {
            *(undefined4 *)(param_1 + 0xb34) = 0;
            if (*(int *)(param_1 + 0x4d0) == 0) {
              uVar10 = CRYPTO_memdup(*(undefined8 *)(param_1 + 0x488),puVar12,
                                     "ssl/statem/extensions_clnt.c",0x65e);
              *(undefined8 *)(lVar8 + 0x358) = uVar10;
              lVar8 = *(long *)(param_1 + 0x918);
              if (*(long *)(lVar8 + 0x358) != 0) {
                *(undefined8 *)(lVar8 + 0x360) = *(undefined8 *)(param_1 + 0x490);
                return 1;
              }
              *(undefined8 *)(lVar8 + 0x360) = 0;
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_clnt.c",0x661,"tls_parse_stoc_alpn");
              ossl_statem_fatal(param_1,0x50,0xc0103,0);
              return 0;
            }
          }
          else {
            iVar3 = *(int *)(param_1 + 0x4d0);
            if ((*(ushort **)(lVar8 + 0x360) != puVar12) ||
               (iVar6 = memcmp(*(void **)(lVar8 + 0x358),*(void **)(param_1 + 0x488),(size_t)puVar12
                              ), iVar6 != 0)) {
              *(undefined4 *)(param_1 + 0xb34) = 0;
            }
            if (iVar3 == 0) {
              ERR_new();
              uVar10 = 0x65a;
              goto LAB_00163974;
            }
          }
          return 1;
        }
        ERR_new();
        uVar10 = 0x648;
        goto LAB_001637f9;
      }
    }
  }
  ERR_new();
  uVar10 = 0x63d;
LAB_001637f9:
  ERR_set_debug("ssl/statem/extensions_clnt.c",uVar10,"tls_parse_stoc_alpn");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  return 0;
}