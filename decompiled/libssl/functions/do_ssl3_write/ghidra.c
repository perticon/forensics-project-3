int do_ssl3_write(SSL *param_1,int param_2,uchar *param_3,long *param_4,ulong param_5,uint param_6,
                 long *param_7)

{
  void *pvVar1;
  long lVar2;
  long *plVar3;
  int iVar4;
  int iVar5;
  int iVar6;
  uint uVar7;
  int iVar8;
  long *plVar9;
  ulong uVar10;
  undefined8 uVar11;
  ulong uVar12;
  SSL_METHOD *pSVar13;
  undefined *puVar14;
  undefined *puVar15;
  uchar *puVar16;
  undefined8 *puVar17;
  char **ppcVar18;
  stack_st_OCSP_RESPID *psVar19;
  ulong uVar20;
  long in_FS_OFFSET;
  byte bVar21;
  long lVar22;
  long lVar23;
  ulong *puVar24;
  X509_EXTENSIONS *local_11a0;
  ulong local_1190;
  int local_1184;
  undefined8 *local_1170;
  ulong local_1168;
  long local_1160;
  ulong local_1158;
  ulong local_1150;
  undefined local_1148 [1792];
  undefined8 local_a48 [321];
  long local_40;
  
  bVar21 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_1168 = 0;
  if (param_5 == 0) {
    lVar22 = 0;
  }
  else {
    if (param_5 - 1 < 3) {
      uVar10 = 0;
      lVar22 = 0;
    }
    else {
      lVar22 = 0;
      lVar23 = 0;
      plVar9 = param_4;
      do {
        lVar2 = *plVar9;
        plVar3 = plVar9 + 1;
        plVar9 = plVar9 + 2;
        lVar22 = lVar22 + lVar2;
        lVar23 = lVar23 + *plVar3;
      } while (param_4 + (param_5 & 0xfffffffffffffffe) != plVar9);
      uVar10 = param_5 & 0xfffffffffffffffe;
      lVar22 = lVar22 + lVar23;
      if ((param_5 & 1) == 0) goto LAB_00156953;
    }
    lVar22 = lVar22 + param_4[uVar10];
    if (uVar10 + 1 < param_5) {
      lVar22 = lVar22 + param_4[uVar10 + 1];
      if (uVar10 + 2 < param_5) {
        lVar22 = lVar22 + param_4[uVar10 + 2];
      }
    }
  }
LAB_00156953:
  iVar4 = RECORD_LAYER_write_pending(&param_1[4].ex_data.dummy);
  if (iVar4 != 0) {
    iVar5 = ssl3_write_pending(param_1,param_2,param_3,lVar22,param_7);
    goto LAB_00156a40;
  }
  if ((*(int *)((long)&param_1->client_CA + 4) != 0) &&
     (iVar5 = (*param_1->method->ssl_dispatch_alert)(param_1), iVar5 < 1)) goto LAB_00156a40;
  if ((param_5 <= param_1[4].options) ||
     (iVar5 = ssl3_setup_write_buffer(param_1,param_5,0), iVar5 != 0)) {
    if ((lVar22 == 0) && (iVar5 = iVar4, param_6 == 0)) goto LAB_00156a40;
    puVar16 = param_1[3].packet;
    if ((*(long *)&param_1[3].sid_ctx_length != 0) && (puVar16 != (uchar *)0x0)) {
      lVar22 = EVP_MD_CTX_get0_md(param_1[3].s3);
      if (lVar22 == 0) {
        puVar16 = param_1[3].packet;
        goto LAB_00156a74;
      }
      uVar11 = EVP_MD_CTX_get0_md(param_1[3].s3);
      local_1184 = EVP_MD_get_size(uVar11);
      uVar7 = param_6;
      if (-1 < local_1184) goto LAB_00156a89;
      ERR_new();
      uVar11 = 0x2d6;
LAB_001571da:
      ERR_set_debug("ssl/record/rec_layer_s3.c",uVar11,"do_ssl3_write");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      goto LAB_00156a38;
    }
LAB_00156a74:
    local_1184 = 0;
    uVar7 = puVar16 == (uchar *)0x0 | param_6;
LAB_00156a89:
    iVar5 = iVar4;
    if (uVar7 == 0) {
      if (*(int *)&param_1->field_0x184 == 0) {
        if ((param_1->debug != 0) && (param_2 == 0x17)) {
          local_1150 = 0;
          puVar24 = &local_1168;
          iVar6 = do_ssl3_write(param_1,0x17,param_3,&local_1150,1,1);
          if (iVar6 < 1) goto LAB_00156a38;
          if (0x55 < local_1168) {
            ERR_new(puVar24);
            uVar11 = 0x2f7;
            goto LAB_001571da;
          }
        }
        *(undefined4 *)&param_1->field_0x184 = 1;
      }
LAB_00156aab:
      if (local_1168 != 0) {
        iVar6 = WPACKET_init_static_len
                          (local_1148,param_1[4].tlsext_hostname,
                           *(undefined8 *)&param_1[4].tlsext_status_expected,0);
        if ((iVar6 == 0) ||
           (iVar6 = WPACKET_allocate_bytes
                              (local_1148,
                               (long)&((param_1[4].tlsext_ocsp_ids)->stack).num + local_1168,0),
           iVar6 == 0)) {
          ERR_new();
          uVar11 = 0x326;
          goto LAB_001571da;
        }
        goto LAB_00156c25;
      }
      puVar15 = local_1148;
      ppcVar18 = &param_1[4].tlsext_hostname;
      local_1190 = local_1168;
      if (param_5 != 0) {
        do {
          psVar19 = (stack_st_OCSP_RESPID *)(ulong)(-(int)*ppcVar18 - 5U & 7);
          ppcVar18[3] = (char *)psVar19;
          iVar6 = WPACKET_init_static_len(puVar15,*ppcVar18,ppcVar18[2],0);
          if ((iVar6 == 0) || (iVar6 = WPACKET_allocate_bytes(puVar15,psVar19,0), iVar6 == 0)) {
            ERR_new();
            uVar11 = 0x337;
            goto LAB_00156b54;
          }
          local_1190 = local_1190 + 1;
          ppcVar18 = ppcVar18 + 6;
          puVar15 = puVar15 + 0x38;
        } while (param_5 != local_1190);
        puVar16 = param_1[3].packet;
        if (puVar16 == (uchar *)0x0) {
          pSVar13 = param_1->method;
          puVar17 = local_a48;
          for (lVar22 = 0x140; lVar22 != 0; lVar22 = lVar22 + -1) {
            *puVar17 = 0;
            puVar17 = puVar17 + (ulong)bVar21 * -2 + 1;
          }
          goto LAB_00156c85;
        }
        pSVar13 = param_1->method;
        uVar7 = *(uint *)(pSVar13->get_timeout + 0x60);
        if ((uVar7 & 1) == 0) {
          puVar17 = local_a48;
          for (lVar22 = 0x140; lVar22 != 0; lVar22 = lVar22 + -1) {
            *puVar17 = 0;
            puVar17 = puVar17 + (ulong)bVar21 * -2 + 1;
          }
          goto LAB_00156c85;
        }
        goto LAB_00156fd9;
      }
      puVar16 = param_1[3].packet;
      if (puVar16 == (uchar *)0x0) {
        puVar17 = local_a48;
        for (lVar22 = 0x140; lVar22 != 0; lVar22 = lVar22 + -1) {
          *puVar17 = 0;
          puVar17 = puVar17 + (ulong)bVar21 * -2 + 1;
        }
        goto LAB_001577d2;
      }
      pSVar13 = param_1->method;
      uVar7 = *(uint *)(pSVar13->get_timeout + 0x60);
      local_1190 = param_5;
      if ((uVar7 & 1) != 0) goto LAB_00156fd9;
      local_11a0 = (X509_EXTENSIONS *)0x0;
      puVar17 = local_a48;
      for (lVar22 = 0x140; lVar22 != 0; lVar22 = lVar22 + -1) {
        *puVar17 = 0;
        puVar17 = puVar17 + (ulong)bVar21 * -2 + 1;
      }
    }
    else {
      if (param_6 == 0) goto LAB_00156aab;
      psVar19 = (stack_st_OCSP_RESPID *)(ulong)(-(int)param_1[4].tlsext_hostname - 10U & 7);
      param_1[4].tlsext_ocsp_ids = psVar19;
      iVar6 = WPACKET_init_static_len
                        (local_1148,param_1[4].tlsext_hostname,
                         *(undefined8 *)&param_1[4].tlsext_status_expected,0);
      if ((iVar6 == 0) || (iVar6 = WPACKET_allocate_bytes(local_1148,psVar19,0), iVar6 == 0)) {
        ERR_new();
        uVar11 = 0x31b;
        goto LAB_001571da;
      }
LAB_00156c25:
      local_1190 = 1;
      puVar16 = param_1[3].packet;
      if (puVar16 != (uchar *)0x0) {
        pSVar13 = param_1->method;
        uVar7 = *(uint *)(pSVar13->get_timeout + 0x60);
        if ((uVar7 & 1) != 0) {
LAB_00156fd9:
          if (((((uVar7 & 8) != 0) || (pSVar13->version < 0x304)) || (pSVar13->version == 0x10000))
             && ((3 < *(int *)((long)&param_1->s3 + 4) - 1U && (*(int *)&param_1[3].read_hash != 1))
                )) {
            uVar11 = EVP_CIPHER_CTX_get0_cipher(puVar16);
            iVar6 = EVP_CIPHER_get_mode(uVar11);
            if (iVar6 == 2) {
              iVar5 = EVP_CIPHER_CTX_get_iv_length(param_1[3].packet);
              if (iVar5 < 2) {
                iVar5 = iVar4;
              }
            }
            else {
              iVar5 = 8;
              if (1 < iVar6 - 6U) {
                iVar5 = iVar4;
              }
            }
          }
        }
      }
      iVar4 = iVar5;
      puVar17 = local_a48;
      for (lVar22 = 0x140; lVar22 != 0; lVar22 = lVar22 + -1) {
        *puVar17 = 0;
        puVar17 = puVar17 + (ulong)bVar21 * -2 + 1;
      }
      if (param_5 != 0) {
        pSVar13 = param_1->method;
LAB_00156c85:
        local_1170 = local_a48;
        uVar10 = 0;
        local_11a0 = (X509_EXTENSIONS *)0x0;
        puVar15 = local_1148;
        do {
          iVar5 = param_1->version;
          if (iVar5 == 0x304) {
            iVar5 = 0x303;
          }
          local_1158 = 0;
          iVar6 = param_2;
          if (((((((byte)pSVar13->get_timeout[0x60] & 8) == 0) && (0x303 < pSVar13->version)) &&
               (pSVar13->version != 0x10000)) ||
              ((*(int *)((long)&param_1->s3 + 4) - 1U < 4 || (*(int *)&param_1[3].read_hash == 1))))
             && ((param_1[3].packet != (uchar *)0x0 &&
                 ((*(int *)((long)&param_1->s2 + 4) != 2 || (param_2 != 0x15)))))) {
            iVar6 = 0x17;
          }
          *(int *)((long)local_1170 + 4) = iVar6;
          iVar8 = SSL_get_state(param_1);
          if ((((iVar8 == 0xc) && (*(int *)&param_1[4].expand == 0)) &&
              (iVar8 = SSL_version(param_1), iVar8 >> 8 == 3)) &&
             ((iVar8 = SSL_version(param_1), 0x301 < iVar8 && (*(int *)&param_1[3].read_hash == 0)))
             ) {
            iVar5 = 0x301;
          }
          pvVar1 = param_1[3].init_msg;
          *(int *)local_1170 = iVar5;
          lVar22 = param_4[uVar10];
          if (pvVar1 != (void *)0x0) {
            lVar22 = lVar22 + 0x400;
          }
          iVar6 = WPACKET_put_bytes__(puVar15,iVar6,1);
          if ((((iVar6 == 0) || (iVar5 = WPACKET_put_bytes__(puVar15,iVar5,2), iVar5 == 0)) ||
              ((iVar5 = WPACKET_start_sub_packet_len__(puVar15,2), iVar5 == 0 ||
               ((iVar4 != 0 && (iVar5 = WPACKET_allocate_bytes(puVar15,(long)iVar4,0), iVar5 == 0)))
               ))) || ((lVar22 != 0 &&
                       (iVar5 = WPACKET_reserve_bytes(puVar15,lVar22,&local_1158), iVar5 == 0)))) {
            ERR_new();
            uVar11 = 0x385;
            goto LAB_00156b54;
          }
          puVar16 = param_3 + (long)local_11a0;
          local_11a0 = (X509_EXTENSIONS *)((long)&(local_11a0->stack).num + param_4[uVar10]);
          pvVar1 = param_1[3].init_msg;
          local_1170[1] = param_4[uVar10];
          local_1170[4] = local_1158;
          local_1170[5] = puVar16;
          if (pvVar1 == (void *)0x0) {
            iVar5 = WPACKET_memcpy(puVar15);
            if (iVar5 == 0) {
              ERR_new();
              uVar11 = 0x3a0;
              goto LAB_00156b54;
            }
            local_1170[5] = local_1170[4];
          }
          else {
            iVar5 = ssl3_do_compress(param_1,local_1170);
            if ((iVar5 == 0) ||
               (iVar5 = WPACKET_allocate_bytes(puVar15,local_1170[1],0), iVar5 == 0)) {
              ERR_new();
              ERR_set_debug("ssl/record/rec_layer_s3.c",0x398,"do_ssl3_write");
              ossl_statem_fatal(param_1,0x50,0x8d,0);
              goto LAB_00156b76;
            }
          }
          if (((((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
                (iVar5 = param_1->method->version, 0x303 < iVar5)) && (iVar5 != 0x10000)) ||
              ((*(int *)((long)&param_1->s3 + 4) - 1U < 4 || (*(int *)&param_1[3].read_hash == 1))))
             && ((param_1[3].packet != (uchar *)0x0 &&
                 ((*(int *)((long)&param_1->s2 + 4) != 2 || (param_2 != 0x15)))))) {
            iVar5 = WPACKET_put_bytes__(puVar15,param_2,1);
            if (iVar5 == 0) {
              ERR_new();
              uVar11 = 0x3af;
              goto LAB_00156b54;
            }
            local_1170[1] = local_1170[1] + 1;
            uVar7 = ssl_get_max_send_fragment(param_1);
            uVar20 = local_1170[1];
            if (uVar20 < uVar7) {
              if ((code *)param_1[10].tls_session_secret_cb_arg == (code *)0x0) {
                puVar16 = param_1[10].next_proto_negotiated;
                if (puVar16 != (uchar *)0x0) {
                  if (((ulong)puVar16 & (ulong)(puVar16 + -1)) == 0) {
                    uVar12 = (ulong)(puVar16 + -1) & uVar20;
                  }
                  else {
                    uVar12 = uVar20 % (ulong)puVar16;
                  }
                  if (uVar12 != 0) {
                    uVar12 = (long)puVar16 - uVar12;
                    goto LAB_00156db5;
                  }
                }
              }
              else {
                uVar12 = (*(code *)param_1[10].tls_session_secret_cb_arg)(param_1,param_2,uVar20);
LAB_00156db5:
                if (uVar12 != 0) {
                  uVar20 = uVar7 - uVar20;
                  if (uVar12 < uVar20) {
                    uVar20 = uVar12;
                  }
                  iVar5 = WPACKET_memset(puVar15,0,uVar20);
                  if (iVar5 == 0) {
                    ERR_new();
                    uVar11 = 0x3d0;
                    goto LAB_00156b54;
                  }
                  local_1170[1] = local_1170[1] + uVar20;
                }
              }
            }
          }
          if ((((*(byte *)((long)&param_1->hit + 1) & 4) == 0) && (local_1184 != 0)) &&
             ((iVar5 = WPACKET_allocate_bytes(puVar15,(long)local_1184,&local_1150), iVar5 == 0 ||
              (iVar5 = (**(code **)(param_1->method->get_timeout + 8))
                                 (param_1,local_1170,local_1150), iVar5 == 0)))) {
            ERR_new();
            uVar11 = 0x3e4;
            goto LAB_00156b54;
          }
          iVar5 = WPACKET_reserve_bytes(puVar15,0x10,0);
          if ((iVar5 == 0) || (iVar5 = WPACKET_get_length(puVar15), iVar5 == 0)) {
            ERR_new();
            uVar11 = 0x3f7;
            goto LAB_00156b54;
          }
          uVar10 = uVar10 + 1;
          lVar22 = WPACKET_get_curr(puVar15);
          local_1170[1] = local_1160;
          local_1170[4] = lVar22 - local_1160;
          local_1170[5] = lVar22 - local_1160;
          if (param_5 <= uVar10) goto LAB_001573b3;
          pSVar13 = param_1->method;
          puVar15 = puVar15 + 0x38;
          local_1170 = local_1170 + 10;
        } while( true );
      }
LAB_001577d2:
      local_11a0 = (X509_EXTENSIONS *)0x0;
    }
LAB_001573b3:
    local_1170 = local_a48;
    if (*(int *)((long)&param_1->s2 + 4) == 2) {
      iVar4 = tls13_enc(param_1,local_1170,param_5,1,0);
      if (0 < iVar4) goto LAB_001573f2;
      iVar4 = ossl_statem_in_error(param_1);
      if (iVar4 == 0) {
        ERR_new();
        uVar11 = 0x40a;
        goto LAB_00156b54;
      }
    }
    else {
      iVar4 = (**(code **)param_1->method->get_timeout)(param_1,local_1170,param_5,1,0);
      if (0 < iVar4) {
LAB_001573f2:
        if (param_5 != 0) {
          puVar15 = local_1148;
          uVar10 = 0;
          do {
            iVar4 = WPACKET_get_length(puVar15,&local_1158);
            if (((iVar4 == 0) || (uVar20 = local_1170[1], uVar20 < local_1158)) ||
               ((local_1158 < uVar20 &&
                (iVar4 = WPACKET_allocate_bytes(puVar15,uVar20 - local_1158,0), iVar4 == 0)))) {
              ERR_new();
              uVar11 = 0x42b;
              goto LAB_00156b54;
            }
            if (((*(byte *)((long)&param_1->hit + 1) & 4) != 0) && (local_1184 != 0)) {
              iVar4 = WPACKET_allocate_bytes(puVar15,(long)local_1184,&local_1150);
              if ((iVar4 == 0) ||
                 (iVar4 = (**(code **)(param_1->method->get_timeout + 8))
                                    (param_1,local_1170,local_1150,1), iVar4 == 0)) {
                ERR_new();
                uVar11 = 0x433;
                goto LAB_00156b54;
              }
              local_1170[1] = local_1170[1] + (long)local_1184;
            }
            iVar4 = WPACKET_get_length(puVar15);
            if ((iVar4 == 0) || (iVar4 = WPACKET_close(puVar15), iVar4 == 0)) {
              ERR_new();
              uVar11 = 0x43b;
              goto LAB_00156b54;
            }
            if (param_1[1].tlsext_ocsp_resp != (uchar *)0x0) {
              uVar11 = 0x157482;
              lVar22 = WPACKET_get_curr(puVar15);
              (*(code *)param_1[1].tlsext_ocsp_resp)
                        (1,*(undefined4 *)local_1170,0x100,(lVar22 - local_1160) + -5,5,param_1,
                         *(undefined8 *)&param_1[1].tlsext_ocsp_resplen,uVar11);
              if (((((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
                    (iVar4 = param_1->method->version, iVar4 != 0x10000)) && (0x303 < iVar4)) ||
                  ((*(int *)((long)&param_1->s3 + 4) - 1U < 4 ||
                   (*(int *)&param_1[3].read_hash == 1)))) && (param_1[3].packet != (uchar *)0x0)) {
                local_1150 = local_1150 & 0xffffffffffffff00 | (ulong)(byte)param_2;
                (*(code *)param_1[1].tlsext_ocsp_resp)
                          (1,*(undefined4 *)local_1170,0x101,&local_1150,1,param_1,
                           *(undefined8 *)&param_1[1].tlsext_ocsp_resplen);
              }
            }
            iVar4 = WPACKET_finish(puVar15);
            if (iVar4 == 0) {
              ERR_new();
              uVar11 = 0x44f;
              goto LAB_00156b54;
            }
            lVar22 = local_1170[1] + 5;
            local_1170[1] = lVar22;
            if (param_6 != 0) {
              if (uVar10 == 0) {
                iVar5 = 1;
                *param_7 = lVar22;
                goto LAB_00156a40;
              }
              ERR_new();
              uVar11 = 0x45d;
              goto LAB_00156b54;
            }
            *(int *)((long)local_1170 + 4) = param_2;
            uVar20 = uVar10 + 1;
            puVar15 = puVar15 + 0x38;
            local_1170 = local_1170 + 10;
            (&param_1[4].tlsext_ocsp_exts)[uVar10 * 6] = (X509_EXTENSIONS *)(lVar22 + local_1168);
            uVar10 = uVar20;
          } while (param_5 != uVar20);
        }
        *(X509_EXTENSIONS **)&param_1[10].tlsext_status_expected = local_11a0;
        param_1[10].tlsext_ocsp_resp = param_3;
        *(int *)&param_1[10].tlsext_ocsp_ids = param_2;
        param_1[10].tlsext_ocsp_exts = local_11a0;
        iVar5 = ssl3_write_pending(param_1,param_2,param_3,local_11a0,param_7);
        goto LAB_00156a40;
      }
      iVar4 = ossl_statem_in_error(param_1);
      if (iVar4 == 0) {
        ERR_new();
        uVar11 = 0x413;
LAB_00156b54:
        ERR_set_debug("ssl/record/rec_layer_s3.c",uVar11,"do_ssl3_write");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
    }
LAB_00156b76:
    if (local_1190 != 0) {
      puVar15 = local_1148;
      do {
        puVar14 = puVar15 + 0x38;
        WPACKET_cleanup(puVar15);
        puVar15 = puVar14;
      } while (puVar14 != local_1148 + local_1190 * 0x38);
    }
  }
LAB_00156a38:
  iVar5 = -1;
LAB_00156a40:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}