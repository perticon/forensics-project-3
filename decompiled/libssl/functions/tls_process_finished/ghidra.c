uint tls_process_finished(long param_1,void **param_2)

{
  void *b;
  int iVar1;
  void *len;
  long lVar2;
  int iVar3;
  uint uVar4;
  long in_FS_OFFSET;
  undefined auStack56 [8];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(int *)(param_1 + 0x38) == 0) {
LAB_001782df:
    uVar4 = *(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8;
    if ((uVar4 != 0) || (iVar3 = **(int **)(param_1 + 8), iVar3 < 0x304)) goto LAB_00178340;
    if (iVar3 != 0x10000) {
      iVar3 = RECORD_LAYER_processed_read_pending(param_1 + 0xc58);
      if (iVar3 != 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_lib.c",0x328,"tls_process_finished");
        ossl_statem_fatal(param_1,10,0xb6,0);
        goto LAB_0017839b;
      }
      if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) goto LAB_00178340;
      iVar3 = **(int **)(param_1 + 8);
    }
LAB_0017832b:
    if ((iVar3 < 0x304) || (iVar3 == 0x10000)) goto LAB_00178340;
  }
  else {
    *(undefined4 *)(param_1 + 0x80) = 0;
    if (*(int *)(param_1 + 0xba8) != 4) {
      *(undefined4 *)(param_1 + 0x70) = 1;
    }
    uVar4 = *(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8;
    if ((uVar4 == 0) && (iVar3 = **(int **)(param_1 + 8), 0x303 < iVar3)) {
      if (iVar3 != 0x10000) {
        iVar3 = tls13_save_handshake_digest_for_pha(param_1);
        if (iVar3 == 0) goto LAB_0017839b;
        goto LAB_001782df;
      }
      goto LAB_0017832b;
    }
LAB_00178340:
    if (*(int *)(param_1 + 0x198) == 0) {
      ERR_new();
      uVar4 = 0;
      ERR_set_debug("ssl/statem/statem_lib.c",0x32e,"tls_process_finished");
      ossl_statem_fatal(param_1,10,0x9a,0);
      goto LAB_0017839b;
    }
  }
  *(undefined4 *)(param_1 + 0x198) = 0;
  len = *(void **)(param_1 + 0x2c8);
  if (len == param_2[1]) {
    b = (void *)(param_1 + 0x248);
    iVar3 = CRYPTO_memcmp(*param_2,b,(size_t)len);
    if (iVar3 == 0) {
      if (len < (void *)0x41) {
        iVar3 = *(int *)(param_1 + 0x38);
        if (iVar3 == 0) {
          memcpy((void *)(param_1 + 0x438),b,(size_t)len);
          *(void **)(param_1 + 0x478) = len;
        }
        else {
          memcpy((void *)(param_1 + 0x3f0),b,(size_t)len);
          *(void **)(param_1 + 0x430) = len;
        }
        lVar2 = *(long *)(*(int **)(param_1 + 8) + 0x30);
        uVar4 = *(uint *)(lVar2 + 0x60) & 8;
        if (((uVar4 == 0) && (iVar1 = **(int **)(param_1 + 8), iVar1 != 0x10000)) && (0x303 < iVar1)
           ) {
          if (iVar3 == 0) {
            iVar3 = (**(code **)(lVar2 + 0x18))(param_1,param_1 + 0x5c4,param_1 + 0x584,0,auStack56)
            ;
            if ((iVar3 != 0) &&
               (iVar3 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x20))
                                  (param_1,0x111), iVar3 != 0)) {
              iVar3 = tls_process_initial_server_flight(param_1);
              uVar4 = (uint)(iVar3 != 0);
            }
          }
          else {
            uVar4 = 1;
            if (*(int *)(param_1 + 0xba8) != 4) {
              iVar3 = (**(code **)(lVar2 + 0x20))(param_1,0x121);
              uVar4 = (uint)(iVar3 != 0);
            }
          }
        }
        else {
          uVar4 = 1;
        }
      }
      else {
        ERR_new();
        uVar4 = 0;
        ERR_set_debug("ssl/statem/statem_lib.c",0x344,"tls_process_finished");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
    }
    else {
      ERR_new();
      uVar4 = 0;
      ERR_set_debug("ssl/statem/statem_lib.c",0x33c,"tls_process_finished");
      ossl_statem_fatal(param_1,0x33,0x95,0);
    }
  }
  else {
    ERR_new();
    uVar4 = 0;
    ERR_set_debug("ssl/statem/statem_lib.c",0x336,"tls_process_finished");
    ossl_statem_fatal(param_1,0x32,0x6f,0);
  }
LAB_0017839b:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}