undefined8 final_renegotiate(long param_1,undefined8 param_2,int param_3)

{
  if (*(int *)(param_1 + 0x38) == 0) {
    if (((*(ulong *)(param_1 + 0x9e8) & 0x40004) == 0) && (param_3 == 0)) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions.c",0x36e,"final_renegotiate");
      ossl_statem_fatal(param_1,0x28,0x152,0);
      return 0;
    }
  }
  else {
    if (*(int *)(param_1 + 0xba0) == 0) {
      return 1;
    }
    if (((*(byte *)(param_1 + 0x9ea) & 4) == 0) && (param_3 == 0)) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions.c",0x37a,"final_renegotiate");
      ossl_statem_fatal(param_1,0x28,0x152,0);
      return 0;
    }
  }
  return 1;
}