undefined8 init_server_name(long param_1)

{
  if (*(int *)(param_1 + 0x38) == 0) {
    return 1;
  }
  *(undefined4 *)(param_1 + 0xb60) = 0;
  CRYPTO_free(*(void **)(param_1 + 0xa58));
  *(undefined8 *)(param_1 + 0xa58) = 0;
  return 1;
}