undefined8 dtls1_preprocess_fragment(long param_1,byte *param_2)

{
  byte bVar1;
  ulong uVar2;
  long lVar3;
  int iVar4;
  ulong uVar5;
  undefined4 extraout_var;
  undefined8 uVar6;
  
  uVar2 = *(ulong *)(param_2 + 8);
  if ((ulong)(*(long *)(param_2 + 0x20) + *(long *)(param_2 + 0x18)) <= uVar2) {
    uVar5 = 0x454c;
    if (0x454b < *(ulong *)(param_1 + 0xa00)) {
      uVar5 = *(ulong *)(param_1 + 0xa00);
    }
    if (uVar2 <= uVar5) {
      if (*(long *)(*(long *)(param_1 + 0x4b8) + 0x1a8) == 0) {
        iVar4 = BUF_MEM_grow_clean(*(BUF_MEM **)(param_1 + 0x88),uVar2 + 0xc);
        if (CONCAT44(extraout_var,iVar4) == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_dtls.c",0x1cc,"dtls1_preprocess_fragment");
          ossl_statem_fatal(param_1,0x50,0x80007,0);
          return 0;
        }
        lVar3 = *(long *)(param_1 + 0x4b8);
        *(ulong *)(param_1 + 0x2d0) = uVar2;
        *(ulong *)(lVar3 + 0x198) = uVar2;
        bVar1 = *param_2;
        *(uint *)(param_1 + 0x2d8) = (uint)bVar1;
        *(byte *)(lVar3 + 400) = bVar1;
        *(undefined2 *)(lVar3 + 0x1a0) = *(undefined2 *)(param_2 + 0x10);
        return 1;
      }
      if (*(ulong *)(*(long *)(param_1 + 0x4b8) + 0x198) == uVar2) {
        return 1;
      }
      ERR_new();
      uVar6 = 0x1da;
      goto LAB_00172021;
    }
  }
  ERR_new();
  uVar6 = 0x1c2;
LAB_00172021:
  ERR_set_debug("ssl/statem/statem_dtls.c",uVar6,"dtls1_preprocess_fragment");
  ossl_statem_fatal(param_1,0x2f,0x98,0);
  return 0;
}