int32_t dtls1_preprocess_fragment(void** rdi, void** rsi) {
    void** r12_3;
    void** rax4;
    void** rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** ecx9;
    uint32_t edx10;

    r12_3 = *reinterpret_cast<void***>(rsi + 8);
    if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 32)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 24))) > reinterpret_cast<unsigned char>(r12_3)) {
        addr_72010_2:
        fun_20230();
    } else {
        *reinterpret_cast<int32_t*>(&rax4) = 0x454c;
        *reinterpret_cast<int32_t*>(&rax4 + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 0xa00)) >= reinterpret_cast<unsigned char>(0x454c)) {
            rax4 = *reinterpret_cast<void***>(rdi + 0xa00);
        }
        if (reinterpret_cast<unsigned char>(r12_3) > reinterpret_cast<unsigned char>(rax4)) 
            goto addr_72010_2; else 
            goto addr_71fe5_6;
    }
    addr_72021_7:
    fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
    ossl_statem_fatal(rdi, rdi);
    return 0;
    addr_71fe5_6:
    if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x4b8) + 0x1a8)) {
        rdi5 = *reinterpret_cast<void***>(rdi + 0x88);
        rax6 = fun_21450(rdi5, r12_3 + 12);
        if (!rax6) {
            fun_20230();
            fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
            ossl_statem_fatal(rdi, rdi);
            eax7 = 0;
            goto addr_72008_10;
        } else {
            rax8 = *reinterpret_cast<void***>(rdi + 0x4b8);
            *reinterpret_cast<void***>(rdi + 0x2d0) = r12_3;
            *reinterpret_cast<void***>(rax8 + 0x198) = r12_3;
            ecx9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi))));
            *reinterpret_cast<void***>(rdi + 0x2d8) = ecx9;
            *reinterpret_cast<void***>(rax8 + 0x190) = ecx9;
            edx10 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rsi + 16));
            *reinterpret_cast<void**>(rax8 + 0x1a0) = *reinterpret_cast<void**>(&edx10);
            return 1;
        }
    } else {
        eax7 = 1;
        if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x4b8) + 0x198) != r12_3) {
            fun_20230();
            goto addr_72021_7;
        } else {
            addr_72008_10:
            return eax7;
        }
    }
}