int64_t dtls1_preprocess_fragment(int64_t a1, int64_t a2, int64_t a3, int64_t a4) {
    int64_t v1 = a2;
    uint64_t v2 = *(int64_t *)(a2 + 8); // 0x71fb7
    if (*(int64_t *)(a2 + 24) + *(int64_t *)(a2 + 32) > v2) {
        // 0x72010
        function_20230();
        // 0x72021
        function_207e0();
        ossl_statem_fatal();
        return 0;
    }
    uint64_t v3 = *(int64_t *)(a1 + (int64_t)&g382); // 0x71fc8
    uint64_t v4 = v3 >= (int64_t)&g169 ? v3 : (int64_t)&g169; // 0x71fd8
    if (v2 > v4) {
        // 0x72010
        function_20230();
        // 0x72021
        function_207e0();
        ossl_statem_fatal();
        return 0;
    }
    int64_t * v5 = (int64_t *)(a1 + (int64_t)&g188); // 0x71fe5
    int64_t v6 = *v5; // 0x71fe5
    if (*(int64_t *)(v6 + 424) != 0) {
        // 0x71ff6
        if (*(int64_t *)(v6 + 408) == v2) {
            // 0x72008
            return 1;
        }
        // 0x720a8
        function_20230();
        // 0x72021
        function_207e0();
        ossl_statem_fatal();
        return 0;
    }
    // 0x72050
    if (function_21450() == 0) {
        // 0x720c0
        function_20230();
        function_207e0();
        ossl_statem_fatal();
        // 0x72008
        return 0;
    }
    int64_t v7 = *v5; // 0x72069
    *(int64_t *)(a1 + (int64_t)&g78) = v2;
    *(int64_t *)(v7 + 408) = v2;
    unsigned char v8 = *(char *)&v1; // 0x7207e
    *(int32_t *)(a1 + (int64_t)&g79) = (int32_t)v8;
    *(char *)(v7 + 400) = v8;
    *(int16_t *)(v7 + 416) = *(int16_t *)(a2 + 16);
    return 1;
}