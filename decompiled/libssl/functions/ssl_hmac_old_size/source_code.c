size_t ssl_hmac_old_size(const SSL_HMAC *ctx)
{
    return HMAC_size(ctx->old_ctx);
}