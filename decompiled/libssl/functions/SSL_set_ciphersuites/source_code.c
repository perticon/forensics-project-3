int SSL_set_ciphersuites(SSL *s, const char *str)
{
    STACK_OF(SSL_CIPHER) *cipher_list;
    int ret = set_ciphersuites(&(s->tls13_ciphersuites), str);

    if (s->cipher_list == NULL) {
        if ((cipher_list = SSL_get_ciphers(s)) != NULL)
            s->cipher_list = sk_SSL_CIPHER_dup(cipher_list);
    }
    if (ret && s->cipher_list != NULL)
        return update_cipher_list(s->ctx, &s->cipher_list, &s->cipher_list_by_id,
                                  s->tls13_ciphersuites);

    return ret;
}