undefined8 SSL_set_ciphersuites(SSL *param_1,char *param_2)

{
  long lVar1;
  int iVar2;
  void *arg;
  undefined8 uVar3;
  stack_st_SSL_CIPHER *psVar4;
  SSL_CTX *pSVar5;
  
  arg = (void *)OPENSSL_sk_new_null();
  if (arg != (void *)0x0) {
    if (*param_2 == '\0') {
LAB_00131982:
      OPENSSL_sk_free(*(undefined8 *)&param_1[1].next_proto_negotiated_len);
      *(void **)&param_1[1].next_proto_negotiated_len = arg;
      if (param_1[1].initial_ctx == (SSL_CTX *)0x0) {
        psVar4 = SSL_get_ciphers(param_1);
        if (psVar4 != (stack_st_SSL_CIPHER *)0x0) {
          pSVar5 = (SSL_CTX *)OPENSSL_sk_dup(psVar4);
          param_1[1].initial_ctx = pSVar5;
        }
        if (param_1[1].initial_ctx == (SSL_CTX *)0x0) {
          return 1;
        }
        arg = *(void **)&param_1[1].next_proto_negotiated_len;
      }
      lVar1 = *(long *)&param_1[3].ex_data.dummy;
      uVar3 = update_cipher_list_isra_0
                        (lVar1 + 0x640,lVar1 + 0x644,&param_1[1].initial_ctx,
                         &param_1[1].next_proto_negotiated,arg);
      return uVar3;
    }
    iVar2 = CONF_parse_list(param_2,0x3a,1,ciphersuite_cb,arg);
    if (0 < iVar2) {
      iVar2 = OPENSSL_sk_num(arg);
      if (iVar2 != 0) goto LAB_00131982;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_ciph.c",0x541,"set_ciphersuites");
    ERR_set_error(0x14,0xb9,0);
    OPENSSL_sk_free(arg);
  }
  if (param_1[1].initial_ctx == (SSL_CTX *)0x0) {
    psVar4 = SSL_get_ciphers(param_1);
    if (psVar4 != (stack_st_SSL_CIPHER *)0x0) {
      pSVar5 = (SSL_CTX *)OPENSSL_sk_dup(psVar4);
      param_1[1].initial_ctx = pSVar5;
    }
  }
  return 0;
}