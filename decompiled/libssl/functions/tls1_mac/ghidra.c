uint tls1_mac(ushort *param_1,long param_2,uchar *param_3,int param_4)

{
  char *pcVar1;
  ushort *puVar2;
  char cVar3;
  undefined2 uVar4;
  int iVar5;
  undefined8 uVar6;
  EVP_MD_CTX *out;
  uint uVar7;
  undefined8 *puVar8;
  EVP_MD_CTX *in;
  long in_FS_OFFSET;
  EVP_MD_CTX *local_100;
  uint local_f4;
  undefined8 local_e8;
  undefined8 uStack224;
  undefined8 local_d8;
  undefined8 uStack208;
  undefined8 local_c8;
  size_t local_b0;
  undefined8 local_a8;
  undefined8 uStack160;
  undefined8 local_98;
  undefined8 uStack144;
  undefined8 local_88;
  undefined8 local_80;
  undefined8 uStack120;
  undefined8 local_70;
  undefined8 uStack104;
  undefined8 local_60;
  undefined local_55;
  undefined uStack84;
  undefined4 uStack83;
  ushort uStack79;
  undefined8 local_4d;
  undefined local_45;
  ushort local_44;
  ushort local_42;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar7 = *(uint *)(param_1 + 0x2a0);
  if (param_4 == 0) {
    local_f4 = uVar7 & 4;
    in = *(EVP_MD_CTX **)(param_1 + 0x430);
    uVar7 = uVar7 & 1;
    puVar8 = (undefined8 *)(param_1 + 0xe7c);
  }
  else {
    local_f4 = uVar7 & 8;
    in = *(EVP_MD_CTX **)(param_1 + 0x448);
    uVar7 = uVar7 & 2;
    puVar8 = (undefined8 *)(param_1 + 0xe80);
  }
  uVar6 = EVP_MD_CTX_get0_md(in);
  iVar5 = EVP_MD_get_size(uVar6);
  if (iVar5 < 0) {
    uVar7 = 0;
    goto LAB_0015c13a;
  }
  local_100 = (EVP_MD_CTX *)0x0;
  local_b0 = (size_t)iVar5;
  out = in;
  if ((uVar7 != 0) ||
     ((out = (EVP_MD_CTX *)EVP_MD_CTX_new(), local_100 = out, out != (EVP_MD_CTX *)0x0 &&
      (uVar7 = EVP_MD_CTX_copy(out,in), uVar7 != 0)))) {
    uVar7 = *(uint *)(*(long *)(*(long *)(param_1 + 4) + 0xc0) + 0x60) & 8;
    if (uVar7 == 0) {
      if (local_f4 != 0) {
        iVar5 = EVP_MD_CTX_ctrl(out,4,0,puVar8);
        uVar7 = 0;
        if (iVar5 < 1) goto LAB_0015c130;
        uVar7 = *(uint *)(*(long *)(*(long *)(param_1 + 4) + 0xc0) + 0x60) & 8;
      }
      if (uVar7 != 0) goto LAB_0015c170;
      local_4d = *puVar8;
    }
    else {
LAB_0015c170:
      if (param_4 == 0) {
        uVar4 = **(undefined2 **)(param_1 + 0xe88);
      }
      else {
        uVar4 = (*(undefined2 **)(param_1 + 0xe88))[1];
      }
      local_55 = (undefined)((ushort)uVar4 >> 8);
      uStack84 = (undefined)uVar4;
      uStack83 = *(undefined4 *)((long)puVar8 + 2);
      uStack79 = *(ushort *)((long)puVar8 + 6);
      local_4d = CONCAT26(uStack79,CONCAT42(uStack83,CONCAT11(uStack84,local_55)));
    }
    local_45 = (undefined)*(undefined4 *)(param_2 + 4);
    local_44 = *param_1 << 8 | *param_1 >> 8;
    local_42 = *(ushort *)(param_2 + 8) << 8 | *(ushort *)(param_2 + 8) >> 8;
    if ((param_4 == 0) && ((*(byte *)((long)param_1 + 0xa9) & 1) == 0)) {
      uVar6 = EVP_CIPHER_CTX_get0_cipher(*(undefined8 *)(param_1 + 0x424));
      iVar5 = EVP_CIPHER_get_mode(uVar6);
      if ((iVar5 == 2) && (cVar3 = ssl3_cbc_record_digest_supported(out), cVar3 != '\0')) {
        OSSL_PARAM_construct_size_t(&local_e8,"tls-data-size",param_2 + 0x10);
        local_a8 = local_e8;
        uStack160 = uStack224;
        local_98 = local_d8;
        uStack144 = uStack208;
        local_88 = local_c8;
        OSSL_PARAM_construct_end(local_e8,local_d8,&local_e8);
        local_80 = local_e8;
        uStack120 = uStack224;
        local_70 = local_d8;
        uStack104 = uStack208;
        local_60 = local_c8;
        uVar6 = EVP_MD_CTX_get_pkey_ctx(out);
        uVar7 = EVP_PKEY_CTX_set_params(uVar6,&local_a8);
        if (uVar7 == 0) goto LAB_0015c130;
      }
    }
    iVar5 = EVP_DigestSignUpdate(out,&local_4d,0xd);
    uVar7 = 0;
    if ((((0 < iVar5) &&
         (iVar5 = EVP_DigestSignUpdate
                            (out,*(undefined8 *)(param_2 + 0x28),*(undefined8 *)(param_2 + 8)),
         0 < iVar5)) && (iVar5 = EVP_DigestSignFinal(out,param_3,&local_b0), uVar7 = 0, 0 < iVar5))
       && (uVar7 = 1, (*(byte *)(*(long *)(*(long *)(param_1 + 4) + 0xc0) + 0x60) & 8) == 0)) {
      pcVar1 = (char *)((long)puVar8 + 7);
      *pcVar1 = *pcVar1 + '\x01';
      if (*pcVar1 == '\0') {
        puVar2 = (ushort *)((long)puVar8 + 6);
        *(char *)puVar2 = *(char *)puVar2 + '\x01';
        if (*(char *)puVar2 == '\0') {
          pcVar1 = (char *)((long)puVar8 + 5);
          *pcVar1 = *pcVar1 + '\x01';
          if (*pcVar1 == '\0') {
            puVar2 = (ushort *)((long)puVar8 + 4);
            *(char *)puVar2 = *(char *)puVar2 + '\x01';
            if (*(char *)puVar2 == '\0') {
              pcVar1 = (char *)((long)puVar8 + 3);
              *pcVar1 = *pcVar1 + '\x01';
              if (*pcVar1 == '\0') {
                puVar2 = (ushort *)((long)puVar8 + 2);
                *(char *)puVar2 = *(char *)puVar2 + '\x01';
                if (*(char *)puVar2 == '\0') {
                  pcVar1 = (char *)((long)puVar8 + 1);
                  *pcVar1 = *pcVar1 + '\x01';
                  if ((*pcVar1 == '\0') &&
                     (*(char *)puVar8 = *(char *)puVar8 + '\x01', *(char *)puVar8 == '\0'))
                  goto LAB_0015c130;
                }
              }
            }
          }
        }
      }
      uVar7 = 1;
    }
  }
LAB_0015c130:
  EVP_MD_CTX_free(local_100);
LAB_0015c13a:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar7;
}