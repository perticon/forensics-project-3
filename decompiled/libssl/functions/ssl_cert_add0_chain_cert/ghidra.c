bool ssl_cert_add0_chain_cert(long param_1,long param_2,undefined8 param_3)

{
  int iVar1;
  long lVar2;
  long lVar3;
  
  if (param_1 == 0) {
    lVar3 = **(long **)(param_2 + 0x158);
  }
  else {
    lVar3 = **(long **)(param_1 + 0x898);
  }
  if (lVar3 != 0) {
    iVar1 = ssl_security_cert(param_1,param_2,param_3,0,0);
    if (iVar1 == 1) {
      lVar2 = *(long *)(lVar3 + 0x10);
      if (lVar2 == 0) {
        lVar2 = OPENSSL_sk_new_null();
        *(long *)(lVar3 + 0x10) = lVar2;
        if (lVar2 == 0) {
          return false;
        }
      }
      iVar1 = OPENSSL_sk_push(lVar2,param_3);
      return iVar1 != 0;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_cert.c",0x127,"ssl_cert_add0_chain_cert");
    ERR_set_error(0x14,iVar1,0);
  }
  return false;
}