undefined8 ssl_cert_set1_chain(undefined8 param_1,undefined8 param_2,long param_3)

{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  
  if (param_3 == 0) {
    uVar3 = ssl_cert_set0_chain(param_1,param_2,0);
    return uVar3;
  }
  lVar2 = X509_chain_up_ref(param_3);
  uVar3 = 0;
  if (lVar2 != 0) {
    iVar1 = ssl_cert_set0_chain(param_1,param_2,lVar2);
    if (iVar1 == 0) {
      OSSL_STACK_OF_X509_free(lVar2);
      return 0;
    }
    uVar3 = 1;
  }
  return uVar3;
}