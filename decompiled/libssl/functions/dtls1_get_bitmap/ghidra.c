ushort * dtls1_get_bitmap(long param_1,long param_2,undefined4 *param_3)

{
  ushort uVar1;
  ulong uVar2;
  ushort *puVar3;
  
  uVar2 = *(ulong *)(param_2 + 0x40);
  *param_3 = 0;
  puVar3 = *(ushort **)(param_1 + 0x1d10);
  uVar1 = *puVar3;
  if (uVar2 == uVar1) {
    return puVar3 + 4;
  }
  if (((uVar2 == (ulong)uVar1 + 1) && (uVar1 != puVar3[0x14])) &&
     (*(int *)(param_2 + 4) - 0x15U < 2)) {
    *param_3 = 1;
    return puVar3 + 0xc;
  }
  return (ushort *)0x0;
}