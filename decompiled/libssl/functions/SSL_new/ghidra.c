SSL * SSL_new(SSL_CTX *ctx)

{
  undefined4 uVar1;
  lhash_st_SSL_SESSION *plVar2;
  COMP_CTX *pCVar3;
  uchar *puVar4;
  stack_st_SSL_CIPHER *psVar5;
  _func_3091 *p_Var6;
  undefined8 uVar7;
  undefined8 uVar8;
  BIO *pBVar9;
  X509_EXTENSIONS *pXVar10;
  undefined1 *puVar11;
  stack_st_void *psVar12;
  uint uVar13;
  undefined4 uVar14;
  undefined4 uVar15;
  int iVar16;
  SSL *s;
  long lVar17;
  dtls1_state_st *pdVar18;
  X509_VERIFY_PARAM *to;
  SRTP_PROTECTION_PROFILE *pSVar19;
  SSL_METHOD *pSVar20;
  void *pvVar21;
  
  if (ctx == (SSL_CTX *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x2a5,"SSL_new");
    ERR_set_error(0x14,0xc3,0);
    return (SSL *)0x0;
  }
  if (ctx->cipher_list == (stack_st_SSL_CIPHER *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x2a9,"SSL_new");
    ERR_set_error(0x14,0xe4,0);
    return (SSL *)0x0;
  }
  s = (SSL *)CRYPTO_zalloc(0x1db8,"ssl/ssl_lib.c",0x2ad);
  if (s != (SSL *)0x0) {
    s[3].max_send_fragment = 1;
    lVar17 = CRYPTO_THREAD_lock_new();
    *(long *)&s[10].next_proto_negotiated_len = lVar17;
    if (lVar17 == 0) {
      CRYPTO_free(s);
      s = (SSL *)0x0;
    }
    else {
      RECORD_LAYER_init(&s[4].ex_data.dummy,s);
      plVar2 = ctx->sessions;
      s[3].tlsext_debug_cb = *(_func_3155 **)&ctx->read_ahead;
      s[1].tls_session_secret_cb = *(tls_session_secret_cb_fn *)&ctx[1].verify_mode;
      *(undefined4 *)((long)&s[3].tlsext_debug_arg + 4) =
           *(undefined4 *)((long)&ctx->msg_callback + 4);
      *(undefined4 *)&s[3].tlsext_hostname = *(undefined4 *)&ctx->msg_callback_arg;
      *(undefined4 *)&s[3].tlsext_debug_arg = *(undefined4 *)&ctx->msg_callback;
      *(undefined8 *)&s[3].servername_done = *(undefined8 *)&ctx->verify_mode;
      *(undefined4 *)&s[10].tls_session_ticket_ext_cb_arg =
           *(undefined4 *)&ctx[1].default_verify_callback;
      *(undefined4 *)((long)&s[10].tls_session_ticket_ext_cb_arg + 4) =
           *(undefined4 *)((long)&ctx[1].default_verify_callback + 4);
      s[10].srtp_profiles = *(stack_st_SRTP_PROTECTION_PROFILE **)ctx[1].tlsext_tick_key_name;
      *(undefined4 *)((long)&s[4].enc_write_ctx + 4) =
           *(undefined4 *)(ctx[1].tlsext_tick_hmac_key + 8);
      lVar17 = OPENSSL_sk_dup(plVar2);
      *(long *)&s[1].next_proto_negotiated_len = lVar17;
      if (lVar17 != 0) {
        pdVar18 = (dtls1_state_st *)ssl_cert_dup(*(undefined8 *)ctx->sid_ctx);
        s[3].d1 = pdVar18;
        if (pdVar18 != (dtls1_state_st *)0x0) {
          *(undefined4 *)&s[4].client_CA = *(undefined4 *)(ctx->sid_ctx + 8);
          s[1].tlsext_ocsp_resp = *(uchar **)(ctx->sid_ctx + 0x10);
          *(undefined8 *)&s[1].tlsext_ocsp_resplen = *(undefined8 *)(ctx->sid_ctx + 0x18);
          s[3].error = *(int *)&ctx->default_verify_callback;
          s[4].ex_data.sk = *(stack_st_void **)(ctx[1].sid_ctx + 8);
          s[10].tls_session_secret_cb_arg = ctx[1].generate_session_id;
          s[10].initial_ctx = (SSL_CTX *)ctx[1].param;
          s[10].next_proto_negotiated = *(uchar **)&ctx[1].quiet_shutdown;
          pCVar3 = (COMP_CTX *)ctx->generate_session_id;
          s[3].expand = pCVar3;
          if (pCVar3 < (COMP_CTX *)0x21) {
            uVar1 = *(undefined4 *)((long)&ctx->param + 4);
            iVar16 = ctx->quiet_shutdown;
            uVar13 = ctx->max_send_fragment;
            *(undefined4 *)&s[3].enc_write_ctx = *(undefined4 *)&ctx->param;
            *(undefined4 *)((long)&s[3].enc_write_ctx + 4) = uVar1;
            *(int *)&s[3].write_hash = iVar16;
            *(uint *)((long)&s[3].write_hash + 4) = uVar13;
            uVar1 = *(undefined4 *)((long)&ctx->client_cert_engine + 4);
            uVar14 = *(undefined4 *)&ctx->tlsext_servername_callback;
            uVar15 = *(undefined4 *)((long)&ctx->tlsext_servername_callback + 4);
            *(undefined4 *)&s[3].compress = *(undefined4 *)&ctx->client_cert_engine;
            *(undefined4 *)((long)&s[3].compress + 4) = uVar1;
            *(undefined4 *)&s[3].cert = uVar14;
            *(undefined4 *)((long)&s[3].cert + 4) = uVar15;
            s[3].kssl_ctx = (KSSL_CTX *)ctx->tlsext_servername_arg;
            *(undefined8 *)(s[3].sid_ctx + 0x1c) = *(undefined8 *)ctx->tlsext_tick_key_name;
            to = X509_VERIFY_PARAM_new();
            s[1].tlsext_ecpointformatlist = (uchar *)to;
            if (to != (X509_VERIFY_PARAM *)0x0) {
              X509_VERIFY_PARAM_inherit(to,*(X509_VERIFY_PARAM **)(ctx->tlsext_tick_key_name + 8));
              s->quiet_shutdown = *(int *)ctx->tlsext_tick_hmac_key;
              *(undefined *)((long)&s[4].d1 + 4) = *(undefined *)((long)&ctx[1].method + 4);
              pXVar10 = (X509_EXTENSIONS *)ctx->tlsext_status_cb;
              s[3].tlsext_ocsp_ids = (stack_st_OCSP_RESPID *)ctx->tlsext_ticket_key_cb;
              s[3].tlsext_ocsp_exts = pXVar10;
              puVar4 = (uchar *)ctx->tlsext_status_arg;
              s[3].tlsext_ocsp_resp = puVar4;
              if ((uchar *)0x1 < puVar4) {
                *(undefined4 *)&s[4].client_CA = 1;
              }
              if (ctx->tlsext_opaque_prf_input_callback != (_func_3100 *)0x0) {
                SSL_set_default_read_buffer_len(s);
              }
              SSL_CTX_up_ref(ctx);
              uVar1 = *(undefined4 *)&ctx[1].method;
              *(SSL_CTX **)&s[3].ex_data.dummy = ctx;
              s[3].tlsext_ellipticcurvelist = (uchar *)0x0;
              s[3].tlsext_opaque_prf_input = (void *)0x0;
              *(undefined4 *)&s[3].next_proto_negotiated_len = 0;
              *(undefined4 *)&s[3].tlsext_session_ticket = uVar1;
              *(undefined4 *)((long)&s[3].tls_session_ticket_ext_cb_arg + 4) = 0;
              s[3].initial_ctx = (SSL_CTX *)0x0;
              s[3].next_proto_negotiated = (uchar *)0x0;
              *(undefined (*) [16])&s[3].tls_session_secret_cb = (undefined  [16])0x0;
              SSL_CTX_up_ref(0,ctx);
              psVar5 = ctx[1].cipher_list_by_id;
              *(SSL_CTX **)&s[4].mac_flags = ctx;
              if (psVar5 != (stack_st_SSL_CIPHER *)0x0) {
                pSVar19 = (SRTP_PROTECTION_PROFILE *)
                          CRYPTO_memdup(psVar5,ctx[1].cipher_list,"ssl/ssl_lib.c",0x305);
                s[3].srtp_profile = pSVar19;
                if (pSVar19 == (SRTP_PROTECTION_PROFILE *)0x0) {
                  s[3].srtp_profiles = (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
                  goto LAB_0013e720;
                }
                s[3].srtp_profiles = (stack_st_SRTP_PROTECTION_PROFILE *)ctx[1].cipher_list;
              }
              if (ctx[1].sessions != (lhash_st_SSL_SESSION *)0x0) {
                pSVar20 = (SSL_METHOD *)
                          CRYPTO_memdup(ctx[1].sessions,(long)ctx[1].cert_store * 2,"ssl/ssl_lib.c",
                                        0x310);
                s[4].method = pSVar20;
                if (pSVar20 == (SSL_METHOD *)0x0) {
                  *(undefined8 *)(s + 4) = 0;
                  goto LAB_0013e720;
                }
                *(x509_store_st **)(s + 4) = ctx[1].cert_store;
              }
              lVar17 = *(long *)&s[3].ex_data.dummy;
              s[4].init_msg = (void *)0x0;
              if (*(long *)(lVar17 + 0x2a8) != 0) {
                pvVar21 = CRYPTO_malloc((int)*(undefined8 *)(lVar17 + 0x2b0),"ssl/ssl_lib.c",799);
                *(void **)&s[4].state = pvVar21;
                if (pvVar21 == (void *)0x0) {
                  s[4].init_buf = (BUF_MEM *)0x0;
                  goto LAB_0013e720;
                }
                lVar17 = *(long *)&s[3].ex_data.dummy;
                memcpy(pvVar21,*(void **)(lVar17 + 0x2a8),*(size_t *)(lVar17 + 0x2b0));
                s[4].init_buf = *(BUF_MEM **)(*(long *)&s[3].ex_data.dummy + 0x2b0);
              }
              p_Var6 = ctx->client_cert_cb;
              uVar7 = *(undefined8 *)(ctx[1].tlsext_tick_key_name + 8);
              s[3].client_CA = (stack_st_X509_NAME *)0x0;
              *(undefined8 *)&s[3].references = 0;
              s[10].tlsext_ellipticcurvelist = (uchar *)p_Var6;
              s[10].tlsext_opaque_prf_input = ctx->app_gen_cookie_cb;
              pSVar20 = (SSL_METHOD *)ctx->cipher_list;
              *(undefined8 *)&s[10].tlsext_hb_seq = uVar7;
              uVar7 = *(undefined8 *)ctx[1].tlsext_tick_hmac_key;
              s->method = pSVar20;
              *(undefined4 *)((long)&s[4].expand + 4) = 0xffffffff;
              *(undefined8 *)(s + 0xb) = uVar7;
              iVar16 = (*pSVar20->ssl_clear)(s);
              if (iVar16 != 0) {
                s->server = (uint)(ctx->cipher_list[1].stack.data != (char **)ssl_undefined_function
                                  );
                iVar16 = SSL_clear(s);
                if ((iVar16 != 0) &&
                   (iVar16 = CRYPTO_new_ex_data(0,s,(CRYPTO_EX_DATA *)&s[3].options), iVar16 != 0))
                {
                  puVar11 = ctx[1].default_passwd_callback;
                  psVar12 = (stack_st_void *)ctx[1].default_passwd_callback_userdata;
                  pvVar21 = ctx[1].app_verify_arg;
                  s[3].ctx = (SSL_CTX *)ctx[1].app_verify_callback;
                  *(void **)&s[3].debug = pvVar21;
                  s[3].verify_result = (long)puVar11;
                  s[3].ex_data.sk = psVar12;
                  uVar7 = *(undefined8 *)(ctx->tlsext_tick_aes_key + 8);
                  uVar8 = *(undefined8 *)ctx->tlsext_tick_aes_key;
                  s[0xb].method = *(SSL_METHOD **)ctx[1].tlsext_tick_aes_key;
                  pBVar9 = *(BIO **)(ctx[1].tlsext_tick_aes_key + 8);
                  s[10].tlsext_opaque_prf_input_len = 0;
                  s[0xb].rbio = pBVar9;
                  iVar16 = SSL_set_ct_validation_callback(puVar11,s,uVar8,uVar7);
                  if (iVar16 != 0) {
                    return s;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
LAB_0013e720:
  SSL_free(s);
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x355,"SSL_new");
  ERR_set_error(0x14,0xc0100,0);
  return (SSL *)0x0;
}