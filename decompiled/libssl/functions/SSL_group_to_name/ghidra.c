undefined8 * SSL_group_to_name(long param_1,uint param_2)

{
  ushort uVar1;
  undefined8 *puVar2;
  
  if ((param_2 & 0x1000000) == 0) {
    uVar1 = tls1_nid2group_id(param_2);
    param_2 = (uint)uVar1;
  }
  else {
    param_2 = param_2 & 0xffff;
  }
  puVar2 = (undefined8 *)tls1_group_id_lookup(*(undefined8 *)(param_1 + 0x9a8),param_2);
  if (puVar2 != (undefined8 *)0x0) {
    puVar2 = (undefined8 *)*puVar2;
  }
  return puVar2;
}