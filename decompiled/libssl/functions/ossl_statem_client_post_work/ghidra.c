undefined8 ossl_statem_client_post_work(long param_1)

{
  long lVar1;
  long lVar2;
  undefined4 *puVar3;
  int iVar4;
  undefined4 uVar5;
  
  *(undefined8 *)(param_1 + 0x98) = 0;
  switch(*(undefined4 *)(param_1 + 0x5c)) {
  case 0xc:
    if ((*(int *)(param_1 + 0x84) == 2) && (*(int *)(param_1 + 0x1d40) != 0)) {
      if (((*(byte *)(param_1 + 0x9ea) & 0x10) == 0) &&
         (iVar4 = tls13_change_cipher_state(param_1,0x52), iVar4 == 0)) {
        return 0;
      }
    }
    else {
      iVar4 = statem_flush(param_1);
      if (iVar4 == 0) {
        return 3;
      }
    }
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
      return 2;
    }
    *(undefined4 *)(param_1 + 0xa08) = 1;
    return 2;
  case 0xd:
  case 0xf:
  case 0x11:
  case 0x13:
  case 0x14:
  case 0x15:
  case 0x16:
  case 0x17:
  case 0x18:
  case 0x19:
  case 0x1a:
  case 0x1b:
  case 0x1c:
  case 0x1d:
  case 0x1e:
  case 0x1f:
  case 0x20:
  case 0x21:
  case 0x22:
  case 0x23:
  case 0x24:
  case 0x25:
  case 0x26:
  case 0x27:
  case 0x28:
  case 0x29:
  case 0x2a:
  case 0x2c:
  case 0x2d:
  case 0x2e:
  case 0x2f:
    goto LAB_0017003d;
  case 0xe:
    iVar4 = tls_client_key_exchange_post_work();
    break;
  case 0x10:
    lVar1 = *(long *)(*(int **)(param_1 + 8) + 0x30);
    if ((((*(byte *)(lVar1 + 0x60) & 8) == 0) && (iVar4 = **(int **)(param_1 + 8), iVar4 != 0x10000)
        ) && (0x303 < iVar4)) {
      return 2;
    }
    if (*(int *)(param_1 + 0x8e8) == 1) {
      return 2;
    }
    if ((*(int *)(param_1 + 0x84) != 2) || (*(int *)(param_1 + 0x1d40) == 0)) {
      lVar2 = *(long *)(param_1 + 0x918);
      puVar3 = *(undefined4 **)(param_1 + 0x340);
      *(undefined8 *)(lVar2 + 0x2f8) = *(undefined8 *)(param_1 + 0x2e0);
      uVar5 = 0;
      if (puVar3 != (undefined4 *)0x0) {
        uVar5 = *puVar3;
      }
      *(undefined4 *)(lVar2 + 0x2f4) = uVar5;
      iVar4 = (**(code **)(lVar1 + 0x10))(param_1);
      if (iVar4 == 0) {
        return 0;
      }
      iVar4 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x20))(param_1,0x12);
      if (iVar4 == 0) {
        return 0;
      }
      if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
        return 2;
      }
      dtls1_reset_seq_numbers(param_1,2);
      return 2;
    }
    iVar4 = tls13_change_cipher_state(param_1,0x52);
    break;
  case 0x12:
    iVar4 = statem_flush();
    if (iVar4 != 1) {
      return 4;
    }
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) {
      return 2;
    }
    iVar4 = **(int **)(param_1 + 8);
    if (iVar4 < 0x304) {
      return 2;
    }
    if (iVar4 == 0x10000) {
      return 2;
    }
    iVar4 = tls13_save_handshake_digest_for_pha(param_1);
    if (iVar4 == 0) {
      return 0;
    }
    if (*(int *)(param_1 + 0xba8) == 4) {
      return 2;
    }
    iVar4 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x20))(param_1,0x112);
    break;
  case 0x2b:
    iVar4 = statem_flush();
    if (iVar4 != 1) {
      return 3;
    }
    iVar4 = tls13_update_key(param_1,1);
    break;
  case 0x30:
    EVP_CIPHER_CTX_free(*(EVP_CIPHER_CTX **)(param_1 + 0x878));
    *(undefined8 *)(param_1 + 0x878) = 0;
    return 2;
  default:
    return 2;
  }
  if (iVar4 == 0) {
    return 0;
  }
LAB_0017003d:
  return 2;
}