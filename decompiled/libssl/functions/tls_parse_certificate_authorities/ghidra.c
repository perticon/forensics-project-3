undefined8 tls_parse_certificate_authorities(undefined8 param_1,long param_2)

{
  undefined8 uVar1;
  
  uVar1 = parse_ca_names();
  if (((int)uVar1 != 0) && (uVar1 = 1, *(long *)(param_2 + 8) != 0)) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions.c",0x4e2,"tls_parse_certificate_authorities");
    ossl_statem_fatal(param_1,0x32,0x6e,0);
    return 0;
  }
  return uVar1;
}