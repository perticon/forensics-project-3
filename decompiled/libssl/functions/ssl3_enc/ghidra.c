ssl3_enc(long param_1,long param_2,long param_3,int param_4,long param_5,undefined8 param_6)

{
  EVP_CIPHER_CTX *c;
  int iVar1;
  long lVar2;
  long lVar3;
  undefined8 uVar4;
  ulong uVar5;
  size_t __n;
  long in_FS_OFFSET;
  ulong local_f8;
  undefined8 local_d8;
  undefined8 uStack208;
  undefined8 local_c8;
  undefined8 uStack192;
  undefined8 local_b8;
  int local_9c;
  undefined8 local_98;
  undefined8 uStack144;
  undefined8 local_88;
  undefined8 uStack128;
  undefined8 local_78;
  undefined8 local_70;
  undefined8 uStack104;
  undefined8 local_60;
  undefined8 uStack88;
  undefined8 local_50;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 == 1) {
    if (param_4 == 0) {
      c = *(EVP_CIPHER_CTX **)(param_1 + 0x848);
    }
    else {
      c = *(EVP_CIPHER_CTX **)(param_1 + 0x878);
    }
    if (((c == (EVP_CIPHER_CTX *)0x0) ||
        (lVar2 = EVP_CIPHER_CTX_get0_cipher(c), *(long *)(param_1 + 0x918) == 0)) || (lVar2 == 0)) {
      memmove(*(void **)(param_2 + 0x20),*(void **)(param_2 + 0x28),*(size_t *)(param_2 + 8));
      *(undefined8 *)(param_2 + 0x28) = *(undefined8 *)(param_2 + 0x20);
      uVar4 = 1;
      goto LAB_0015ae2a;
    }
    lVar3 = EVP_CIPHER_get0_provider(lVar2);
    local_f8 = *(ulong *)(param_2 + 8);
    iVar1 = EVP_CIPHER_CTX_get_block_size(c);
    uVar5 = (ulong)iVar1;
    if (iVar1 == 1) {
      if (param_4 == 0) goto LAB_0015ae98;
    }
    else if (param_4 == 0) {
LAB_0015ae98:
      if ((local_f8 == 0) || (local_f8 % uVar5 != 0)) goto LAB_0015ae28;
    }
    else if (lVar3 == 0) {
      __n = uVar5 - local_f8 % uVar5;
      local_f8 = local_f8 + __n;
      memset((void *)(*(long *)(param_2 + 8) + *(long *)(param_2 + 0x28)),0,__n);
      *(long *)(param_2 + 8) = *(long *)(param_2 + 8) + __n;
      *(char *)(*(long *)(param_2 + 0x28) + -1 + local_f8) = (char)__n + -1;
    }
    lVar2 = EVP_CIPHER_get0_provider(lVar2);
    if (lVar2 == 0) {
      iVar1 = EVP_Cipher(c,*(uchar **)(param_2 + 0x20),*(uchar **)(param_2 + 0x28),(uint)local_f8);
      if (0 < iVar1) {
        uVar4 = 1;
        if (param_4 == 0) {
          lVar3 = param_5 + 8;
          if (param_5 == 0) {
            lVar3 = lVar2;
          }
          uVar4 = ssl3_cbc_remove_padding_and_mac
                            (param_2 + 8,*(undefined8 *)(param_2 + 0x10),
                             *(undefined8 *)(param_2 + 0x20),param_5,lVar3,uVar5,param_6,
                             **(undefined8 **)(param_1 + 0x9a8));
        }
        goto LAB_0015ae2a;
      }
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x3b4,__func___25963);
      ossl_statem_fatal(param_1,0x14,0xc0103,0);
    }
    else {
      iVar1 = EVP_CipherUpdate(c,*(uchar **)(param_2 + 0x20),&local_9c,*(uchar **)(param_2 + 0x28),
                               (uint)local_f8);
      if (iVar1 != 0) {
        *(long *)(param_2 + 8) = (long)local_9c;
        if ((param_4 == 0) && (param_5 != 0)) {
          *(undefined4 *)(param_5 + 8) = 0;
          OSSL_PARAM_construct_octet_ptr(&local_d8,"tls-mac",param_5,param_6);
          local_78 = local_b8;
          local_98 = local_d8;
          uStack144 = uStack208;
          local_88 = local_c8;
          uStack128 = uStack192;
          OSSL_PARAM_construct_end(local_d8,local_c8,&local_d8);
          local_70 = local_d8;
          uStack104 = uStack208;
          local_50 = local_b8;
          local_60 = local_c8;
          uStack88 = uStack192;
          iVar1 = EVP_CIPHER_CTX_get_params(c,&local_98);
          if (iVar1 == 0) {
            ERR_new();
            ERR_set_debug("ssl/record/ssl3_record.c",0x3ad,__func___25963);
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            goto LAB_0015ae28;
          }
        }
        uVar4 = 1;
        goto LAB_0015ae2a;
      }
    }
  }
LAB_0015ae28:
  uVar4 = 0;
LAB_0015ae2a:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}