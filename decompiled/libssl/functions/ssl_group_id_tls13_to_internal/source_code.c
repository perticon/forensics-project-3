uint16_t ssl_group_id_tls13_to_internal(uint16_t curve_id)
{
    switch(curve_id) {
    case OSSL_TLS_GROUP_ID_brainpoolP256r1:
    case OSSL_TLS_GROUP_ID_brainpoolP384r1:
    case OSSL_TLS_GROUP_ID_brainpoolP512r1:
        return 0;
    case OSSL_TLS_GROUP_ID_brainpoolP256r1_tls13:
        return OSSL_TLS_GROUP_ID_brainpoolP256r1;
    case OSSL_TLS_GROUP_ID_brainpoolP384r1_tls13:
        return OSSL_TLS_GROUP_ID_brainpoolP384r1;
    case OSSL_TLS_GROUP_ID_brainpoolP512r1_tls13:
        return OSSL_TLS_GROUP_ID_brainpoolP512r1;
    default:
        return curve_id;
    }
}