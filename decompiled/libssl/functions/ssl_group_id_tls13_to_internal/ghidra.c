undefined4 ssl_group_id_tls13_to_internal(undefined4 param_1)

{
  ushort uVar1;
  
  uVar1 = (ushort)param_1;
  if (uVar1 == 0x20) {
    return 0x1b;
  }
  if (uVar1 < 0x21) {
    if (0x1c < uVar1) {
      if (uVar1 == 0x1f) {
        param_1 = 0x1a;
      }
      return param_1;
    }
    if (0x19 < uVar1) {
      param_1 = 0;
    }
    return param_1;
  }
  if (uVar1 == 0x21) {
    param_1 = 0x1c;
  }
  return param_1;
}