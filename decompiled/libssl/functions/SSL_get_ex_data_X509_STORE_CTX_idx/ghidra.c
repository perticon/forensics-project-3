int SSL_get_ex_data_X509_STORE_CTX_idx(void)

{
  int iVar1;
  
  iVar1 = CRYPTO_THREAD_run_once(&ssl_x509_store_ctx_once,ssl_x509_store_ctx_init_ossl_);
  if ((iVar1 == 0) || (iVar1 = ssl_x509_store_ctx_idx, ssl_x509_store_ctx_init_ossl_ret_ == 0)) {
    iVar1 = -1;
  }
  return iVar1;
}