uint tls_process_server_hello(int *param_1,ushort **param_2)

{
  byte bVar1;
  int iVar2;
  int *piVar3;
  int iVar4;
  bool bVar5;
  int iVar6;
  uint uVar7;
  ushort *puVar8;
  ushort *puVar9;
  byte *pbVar10;
  long lVar11;
  void *pvVar12;
  int *piVar13;
  byte *__n;
  ushort uVar14;
  undefined8 uVar15;
  uint uVar16;
  ushort *puVar17;
  byte *__s1;
  undefined8 uVar18;
  long in_FS_OFFSET;
  byte bVar19;
  int local_6c;
  void *local_68;
  void *local_60;
  byte *local_58;
  byte *local_50;
  long local_40;
  
  bVar19 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  puVar8 = param_2[1];
  local_68 = (void *)0x0;
  if (puVar8 < (ushort *)0x2) {
    ERR_new();
    uVar15 = 0x56d;
    goto LAB_0016d109;
  }
  puVar17 = *param_2;
  puVar9 = puVar8 + -1;
  uVar14 = *puVar17;
  param_2[1] = puVar9;
  *param_2 = puVar17 + 1;
  uVar14 = uVar14 << 8 | uVar14 >> 8;
  if ((*param_1 == 0x304) && (uVar14 == 0x303)) {
    if ((ushort *)0x1f < puVar9) {
      if (((hrrrandom._8_8_ ^ *(ulong *)(puVar17 + 5) | hrrrandom._0_8_ ^ *(ulong *)(puVar17 + 1))
           != 0) ||
         ((hrrrandom._24_8_ ^ *(ulong *)(puVar17 + 0xd) | *(ulong *)(puVar17 + 9) ^ hrrrandom._16_8_
          ) != 0)) goto LAB_0016ce15;
      if (param_1[0x23a] == 0) {
        puVar17 = puVar17 + 0x11;
        puVar8 = puVar8 + -0x11;
        bVar5 = true;
        param_1[0x23a] = 1;
        *param_2 = puVar17;
        param_2[1] = puVar8;
        goto LAB_0016ce48;
      }
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x577,"tls_process_server_hello");
      ossl_statem_fatal(param_1,10,0xf4,0);
      goto LAB_0016d12b;
    }
LAB_0016d0f8:
    ERR_new();
    uVar15 = 0x582;
LAB_0016d109:
    ERR_set_debug("ssl/statem/statem_clnt.c",uVar15,"tls_process_server_hello");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
  }
  else {
    if (puVar9 < (ushort *)0x20) goto LAB_0016d0f8;
LAB_0016ce15:
    iVar6 = *(int *)(puVar17 + 3);
    iVar2 = *(int *)(puVar17 + 5);
    iVar4 = *(int *)(puVar17 + 7);
    bVar5 = false;
    param_1[0x50] = *(int *)(puVar17 + 1);
    param_1[0x51] = iVar6;
    param_1[0x52] = iVar2;
    param_1[0x53] = iVar4;
    iVar6 = *(int *)(puVar17 + 0xb);
    iVar2 = *(int *)(puVar17 + 0xd);
    iVar4 = *(int *)(puVar17 + 0xf);
    param_1[0x54] = *(int *)(puVar17 + 9);
    param_1[0x55] = iVar6;
    param_1[0x56] = iVar2;
    param_1[0x57] = iVar4;
    puVar17 = *param_2 + 0x10;
    *param_2 = puVar17;
    puVar8 = param_2[1] + -0x10;
    param_2[1] = puVar8;
LAB_0016ce48:
    if (puVar8 == (ushort *)0x0) {
LAB_0016d188:
      ERR_new();
      uVar15 = 0x589;
      goto LAB_0016d109;
    }
    __n = (byte *)(ulong)*(byte *)puVar17;
    if ((byte *)((long)puVar8 + -1) < __n) goto LAB_0016d188;
    __s1 = (byte *)((long)puVar17 + 1);
    puVar9 = (ushort *)((byte *)((long)puVar8 + -1) + -(long)__n);
    puVar8 = (ushort *)(__s1 + (long)__n);
    param_2[1] = puVar9;
    *param_2 = puVar8;
    if ((byte *)0x20 < __n) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x58f,"tls_process_server_hello");
      ossl_statem_fatal(param_1,0x2f,300,0);
      goto LAB_0016d12b;
    }
    if (puVar9 < (ushort *)0x2) {
      ERR_new();
      uVar15 = 0x594;
      goto LAB_0016d109;
    }
    *param_2 = puVar8 + 1;
    param_2[1] = puVar9 + -1;
    if (puVar9 + -1 == (ushort *)0x0) {
      ERR_new();
      uVar15 = 0x599;
      goto LAB_0016d109;
    }
    bVar1 = *(byte *)(puVar8 + 1);
    *param_2 = (ushort *)((long)puVar8 + 3);
    puVar17 = (ushort *)((long)puVar9 + -3);
    param_2[1] = puVar17;
    if (puVar17 == (ushort *)0x0) {
      if (!bVar5) {
        local_58 = (byte *)0x0;
        local_50 = (byte *)0x0;
LAB_0016ced9:
        iVar6 = tls_collect_extensions(param_1,&local_58,0x300,&local_68,0,1);
        if (iVar6 != 0) {
          iVar6 = ssl_choose_client_version(param_1,uVar14,local_68);
          if (iVar6 != 0) {
            piVar13 = *(int **)(param_1 + 2);
            uVar7 = *(uint *)(*(long *)(piVar13 + 0x30) + 0x60) & 8;
            uVar16 = (uint)bVar1;
            if (((uVar7 == 0) && (0x303 < *piVar13)) && (uVar16 = (uint)bVar1, *piVar13 != 0x10000))
            goto LAB_0016d33e;
            goto LAB_0016cf44;
          }
        }
        goto LAB_0016d12b;
      }
LAB_0016d2ab:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x5a2,"tls_process_server_hello");
      ossl_statem_fatal(param_1,0x32,0x10f,0);
    }
    else {
      if ((puVar17 == (ushort *)0x1) ||
         (pbVar10 = (byte *)((long)puVar9 + -5),
         pbVar10 !=
         (byte *)(ulong)(ushort)(*(ushort *)((long)puVar8 + 3) << 8 |
                                *(ushort *)((long)puVar8 + 3) >> 8))) goto LAB_0016d2ab;
      local_58 = (byte *)((long)puVar8 + 5);
      param_2[1] = (ushort *)0x0;
      *param_2 = (ushort *)(local_58 + (long)pbVar10);
      local_50 = pbVar10;
      if (!bVar5) goto LAB_0016ced9;
      piVar13 = *(int **)(param_1 + 2);
      uVar7 = *(uint *)(*(long *)(piVar13 + 0x30) + 0x60) & 8;
      if ((uVar7 == 0) && ((*piVar13 == 0x10000 || (*piVar13 < 0x304)))) {
        uVar7 = 0;
      }
LAB_0016d33e:
      uVar16 = (uint)bVar1;
      if (bVar1 == 0) {
        if ((*(byte **)(param_1 + 600) == __n) &&
           (iVar6 = memcmp(__s1,param_1 + 0x250,(size_t)__n), iVar6 == 0)) {
          if (bVar5) {
            iVar6 = set_client_ciphersuite(param_1,puVar8);
            if (iVar6 != 0) {
              local_60 = (void *)0x0;
              EVP_CIPHER_CTX_free(*(EVP_CIPHER_CTX **)(param_1 + 0x21e));
              *(undefined8 *)(param_1 + 0x21e) = 0;
              iVar6 = tls_collect_extensions(param_1,&local_58,0x800,&local_60,0,1);
              if ((iVar6 != 0) &&
                 (iVar6 = tls_parse_all_extensions(param_1,0x800,local_60,0,0,1), iVar6 != 0)) {
                CRYPTO_free(local_60);
                local_60 = (void *)0x0;
                if ((*(long *)(param_1 + 0x2d0) == 0) && (*(long *)(param_1 + 0xba) != 0)) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_clnt.c",0x6cb,
                                "tls_process_as_hello_retry_request");
                  ossl_statem_fatal(param_1,0x2f,0xd6,0);
                }
                else {
                  iVar6 = create_synthetic_message_hash(param_1,0,0,0,0);
                  if ((iVar6 != 0) &&
                     (iVar6 = ssl3_finish_mac(param_1,*(undefined8 *)(*(long *)(param_1 + 0x22) + 8)
                                              ,*(long *)(param_1 + 0x26) + 4), iVar6 != 0)) {
                    uVar16 = 1;
                    goto LAB_0016d144;
                  }
                }
              }
              CRYPTO_free(local_60);
              goto LAB_0016d144;
            }
          }
          else {
            if (uVar7 == 0) {
              if ((*piVar13 == 0x10000) || (*piVar13 < 0x304)) {
                uVar16 = 0;
                goto LAB_0016cf44;
              }
              uVar18 = 0x200;
              uVar15 = 0x200;
            }
            else {
LAB_0016cf44:
              uVar18 = 0x100;
              uVar15 = 0x100;
            }
            iVar6 = tls_validate_all_contexts(param_1,uVar15,local_68);
            if (iVar6 == 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x5d4,"tls_process_server_hello");
              ossl_statem_fatal(param_1,0x2f,0x6e,0);
            }
            else {
              param_1[0x134] = 0;
              if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
                  (iVar6 = **(int **)(param_1 + 2), 0x303 < iVar6)) && (iVar6 != 0x10000)) {
                iVar6 = RECORD_LAYER_processed_read_pending(param_1 + 0x316);
                if (iVar6 == 0) {
                  iVar6 = tls_parse_extension(param_1,0x19,0x200,local_68,0,0);
                  if (iVar6 != 0) {
                    piVar13 = *(int **)(param_1 + 0x246);
LAB_0016d053:
                    if (param_1[0x134] == 0) goto LAB_0016d582;
                    goto LAB_0016d061;
                  }
                }
                else {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_clnt.c",0x5e0,"tls_process_server_hello");
                  ossl_statem_fatal(param_1,10,0xb6,0);
                }
              }
              else {
                piVar13 = *(int **)(param_1 + 0x246);
                if (((*param_1 < 0x301) || (*(code **)(param_1 + 0x2be) == (code *)0x0)) ||
                   (*(long *)(piVar13 + 0xce) == 0)) {
                  if ((__n != (byte *)0x0) && (__n == *(byte **)(piVar13 + 0x94)))
                  goto LAB_0016d53e;
LAB_0016d582:
                  if (*(long *)(piVar13 + 0x94) != 0) {
LAB_0016d7ef:
                    LOCK();
                    *(int *)(*(long *)(param_1 + 0x2e2) + 0x90) =
                         *(int *)(*(long *)(param_1 + 0x2e2) + 0x90) + 1;
                    iVar6 = ssl_get_new_session(param_1,0);
                    if (iVar6 == 0) goto LAB_0016d12b;
                    piVar13 = *(int **)(param_1 + 0x246);
                  }
                  piVar3 = *(int **)(param_1 + 2);
                  iVar6 = *param_1;
                  lVar11 = *(long *)(piVar3 + 0x30);
                  *piVar13 = iVar6;
                  if (((((*(byte *)(lVar11 + 0x60) & 8) != 0) || (iVar2 = *piVar3, iVar2 < 0x304))
                      || (iVar2 == 0x10000)) &&
                     (*(byte **)(piVar13 + 0x94) = __n, __n != (byte *)0x0)) {
                    piVar13 = piVar13 + 0x96;
                    for (; __n != (byte *)0x0; __n = __n + -1) {
                      *(byte *)piVar13 = *__s1;
                      __s1 = __s1 + (ulong)bVar19 * -2 + 1;
                      piVar13 = (int *)((long)piVar13 + (ulong)bVar19 * -2 + 1);
                    }
                    piVar13 = *(int **)(param_1 + 0x246);
                    goto LAB_0016d0a4;
                  }
LAB_0016d5c4:
                  param_1[0xf9] = iVar6;
                  param_1[0xfa] = iVar6;
                  iVar6 = set_client_ciphersuite(param_1,puVar8);
                  if (iVar6 != 0) {
                    if ((param_1[0x134] == 0) ||
                       (*(uint *)(*(long *)(param_1 + 0x246) + 0x2f4) == uVar16)) {
                      lVar11 = 0;
                      if (uVar16 == 0) {
LAB_0016d615:
                        *(long *)(param_1 + 0xd0) = lVar11;
                        iVar6 = tls_parse_all_extensions(param_1,uVar18,local_68,0,0,1);
                        if (iVar6 != 0) {
                          lVar11 = *(long *)(*(int **)(param_1 + 2) + 0x30);
                          if (((((*(byte *)(lVar11 + 0x60) & 8) != 0) ||
                               (iVar6 = **(int **)(param_1 + 2), iVar6 == 0x10000)) ||
                              (iVar6 < 0x304)) ||
                             ((iVar6 = (**(code **)(lVar11 + 0x10))(param_1), iVar6 != 0 &&
                              (iVar6 = (**(code **)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x20)
                                       )(param_1,0x91), iVar6 != 0)))) {
                            CRYPTO_free(local_68);
                            uVar16 = 3;
                            goto LAB_0016d144;
                          }
                        }
                      }
                      else {
                        iVar6 = ssl_allow_compression(param_1);
                        if (iVar6 == 0) {
                          ERR_new();
                          ERR_set_debug("ssl/statem/statem_clnt.c",0x668,"tls_process_server_hello")
                          ;
                          ossl_statem_fatal(param_1,0x2f,0x157,0);
                        }
                        else {
                          lVar11 = ssl3_comp_find(*(undefined8 *)
                                                   (*(long *)(param_1 + 0x26a) + 0x118),bVar1);
                          if (lVar11 != 0) goto LAB_0016d615;
                          ERR_new();
                          ERR_set_debug("ssl/statem/statem_clnt.c",0x66f,"tls_process_server_hello")
                          ;
                          ossl_statem_fatal(param_1,0x2f,0x101,0);
                        }
                      }
                    }
                    else {
                      ERR_new();
                      ERR_set_debug("ssl/statem/statem_clnt.c",0x661,"tls_process_server_hello");
                      ossl_statem_fatal(param_1,0x2f,0x158,0);
                    }
                  }
                }
                else {
                  local_6c = 0x200;
                  local_60 = (void *)0x0;
                  iVar6 = (**(code **)(param_1 + 0x2be))
                                    (param_1,piVar13 + 0x14,&local_6c,0,&local_60,
                                     *(undefined8 *)(param_1 + 0x2c0));
                  if ((iVar6 == 0) || (local_6c < 1)) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_clnt.c",0x60b,"tls_process_server_hello");
                    ossl_statem_fatal(param_1,0x50,0xc0103,0);
                    goto LAB_0016d12b;
                  }
                  piVar13 = *(int **)(param_1 + 0x246);
                  *(long *)(piVar13 + 2) = (long)local_6c;
                  pvVar12 = local_60;
                  if (local_60 == (void *)0x0) {
                    pvVar12 = (void *)ssl_get_cipher_by_char(param_1,puVar8,0);
                    piVar13 = *(int **)(param_1 + 0x246);
                  }
                  *(void **)(piVar13 + 0xbe) = pvVar12;
                  if ((__n == (byte *)0x0) || (*(byte **)(piVar13 + 0x94) != __n))
                  goto LAB_0016d053;
LAB_0016d53e:
                  iVar6 = memcmp(__s1,piVar13 + 0x96,(size_t)__n);
                  if (iVar6 == 0) {
                    param_1[0x134] = 1;
                  }
                  else if (param_1[0x134] == 0) goto LAB_0016d7ef;
LAB_0016d061:
                  if ((*(size_t *)(param_1 + 0x23c) == *(size_t *)(piVar13 + 0x9e)) &&
                     (iVar6 = memcmp(piVar13 + 0xa0,param_1 + 0x23e,*(size_t *)(param_1 + 0x23c)),
                     iVar6 == 0)) {
LAB_0016d0a4:
                    iVar6 = *piVar13;
                    if (*param_1 == iVar6) goto LAB_0016d5c4;
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_clnt.c",0x641,"tls_process_server_hello");
                    ossl_statem_fatal(param_1,0x46,0xd2,0);
                  }
                  else {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_clnt.c",0x61b,"tls_process_server_hello");
                    ossl_statem_fatal(param_1,0x2f,0x110,0);
                  }
                }
              }
            }
          }
        }
        else {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_clnt.c",0x5bf,"tls_process_server_hello");
          ossl_statem_fatal(param_1,0x2f,999,0);
        }
      }
      else {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0x5b7,"tls_process_server_hello");
        ossl_statem_fatal(param_1,0x2f,0x155,0);
      }
    }
  }
LAB_0016d12b:
  CRYPTO_free(local_68);
  uVar16 = 0;
LAB_0016d144:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar16;
}