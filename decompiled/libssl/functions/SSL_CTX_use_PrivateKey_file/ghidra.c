int SSL_CTX_use_PrivateKey_file(SSL_CTX *ctx,char *file,int type)

{
  int iVar1;
  BIO_METHOD *type_00;
  BIO *bp;
  long lVar2;
  EVP_PKEY *pkey;
  undefined8 uVar3;
  
  type_00 = BIO_s_file();
  bp = BIO_new(type_00);
  if (bp == (BIO *)0x0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x16e,"SSL_CTX_use_PrivateKey_file");
    ERR_set_error(0x14,0x80007,0);
  }
  else {
    lVar2 = BIO_ctrl(bp,0x6c,3,file);
    if ((int)lVar2 < 1) {
      ERR_new();
      iVar1 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",0x173,"SSL_CTX_use_PrivateKey_file");
      ERR_set_error(0x14,0x80002,0);
    }
    else {
      if (type == 1) {
        uVar3 = 0x80009;
        pkey = (EVP_PKEY *)
               PEM_read_bio_PrivateKey_ex
                         (bp,0,ctx->client_cert_cb,ctx->app_gen_cookie_cb,ctx->method,
                          ctx[1].tlsext_ticket_key_cb);
      }
      else {
        if (type != 2) {
          ERR_new();
          iVar1 = 0;
          ERR_set_debug("ssl/ssl_rsa.c",0x180,"SSL_CTX_use_PrivateKey_file");
          ERR_set_error(0x14,0x7c,0);
          goto LAB_001410b3;
        }
        uVar3 = 0x8000d;
        pkey = (EVP_PKEY *)d2i_PrivateKey_ex_bio(bp,0,ctx->method,ctx[1].tlsext_ticket_key_cb);
      }
      if (pkey == (EVP_PKEY *)0x0) {
        ERR_new();
        iVar1 = 0;
        ERR_set_debug("ssl/ssl_rsa.c",0x184,"SSL_CTX_use_PrivateKey_file");
        ERR_set_error(0x14,uVar3,0);
      }
      else {
        iVar1 = SSL_CTX_use_PrivateKey(ctx,pkey);
        EVP_PKEY_free(pkey);
      }
    }
  }
LAB_001410b3:
  BIO_free(bp);
  return iVar1;
}