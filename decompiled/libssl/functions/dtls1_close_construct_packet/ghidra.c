bool dtls1_close_construct_packet(long param_1,undefined8 param_2,int param_3)

{
  long lVar1;
  int iVar2;
  long in_FS_OFFSET;
  bool bVar3;
  ulong local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 == 0x101) {
LAB_00173cbc:
    iVar2 = WPACKET_get_length(param_2,&local_28);
    if ((iVar2 != 0) && (local_28 < 0x80000000)) {
      if (param_3 == 0x101) {
        *(ulong *)(param_1 + 0x98) = local_28;
        *(undefined8 *)(param_1 + 0xa0) = 0;
      }
      else {
        lVar1 = *(long *)(param_1 + 0x4b8);
        *(ulong *)(lVar1 + 0x140) = local_28 - 0xc;
        *(ulong *)(lVar1 + 0x158) = local_28 - 0xc;
        *(ulong *)(param_1 + 0x98) = local_28;
        bVar3 = true;
        *(undefined8 *)(param_1 + 0xa0) = 0;
        if (param_3 == 3) goto LAB_00173d3a;
      }
      iVar2 = dtls1_buffer_message(param_1,param_3 == 0x101);
      bVar3 = iVar2 != 0;
      goto LAB_00173d3a;
    }
  }
  else {
    iVar2 = WPACKET_close(param_2);
    if (iVar2 != 0) goto LAB_00173cbc;
  }
  bVar3 = false;
LAB_00173d3a:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}