int SSL_use_PrivateKey_ASN1(int pk,SSL *ssl,uchar *d,long len)

{
  undefined8 *puVar1;
  int iVar2;
  EVP_PKEY *pkey;
  undefined4 in_register_0000003c;
  long in_FS_OFFSET;
  uchar *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  puVar1 = *(undefined8 **)&ssl[3].ex_data.dummy;
  local_28 = d;
  pkey = (EVP_PKEY *)
         d2i_PrivateKey_ex(CONCAT44(in_register_0000003c,pk),0,&local_28,len,*puVar1,puVar1[0x88]);
  if (pkey == (EVP_PKEY *)0x0) {
    ERR_new();
    iVar2 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0xcd,"SSL_use_PrivateKey_ASN1");
    ERR_set_error(0x14,0x8000d,0);
  }
  else {
    iVar2 = SSL_use_PrivateKey(ssl,pkey);
    EVP_PKEY_free(pkey);
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}