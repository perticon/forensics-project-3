byte tls_process_end_of_early_data(long param_1,long param_2)

{
  int iVar1;
  
  if (*(long *)(param_2 + 8) != 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0xfef,"tls_process_end_of_early_data");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
    return 0;
  }
  if (*(int *)(param_1 + 0x84) - 10U < 2) {
    iVar1 = RECORD_LAYER_processed_read_pending(param_1 + 0xc58);
    if (iVar1 == 0) {
      *(undefined4 *)(param_1 + 0x84) = 0xc;
      iVar1 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x20))(param_1,0xa1);
      return ~-(iVar1 == 0) & 3;
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0xffe,"tls_process_end_of_early_data");
    ossl_statem_fatal(param_1,10,0xb6,0);
    return 0;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_srvr.c",0xff5,"tls_process_end_of_early_data");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}