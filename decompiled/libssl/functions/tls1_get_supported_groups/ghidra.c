void tls1_get_supported_groups(long param_1,long *param_2,undefined8 *param_3)

{
  undefined8 uVar1;
  uint uVar2;
  
  uVar2 = *(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000;
  if (uVar2 == 0x20000) {
    *param_2 = 0x188640;
    *param_3 = 1;
    return;
  }
  if (uVar2 == 0x30000) {
    *param_2 = (long)&suiteb_curves;
    *param_3 = 2;
    return;
  }
  if (uVar2 == 0x10000) {
    *param_2 = (long)&suiteb_curves;
    *param_3 = 1;
    return;
  }
  if (*(long *)(param_1 + 0xac8) != 0) {
    *param_2 = *(long *)(param_1 + 0xac8);
    *param_3 = *(undefined8 *)(param_1 + 0xac0);
    return;
  }
  uVar1 = *(undefined8 *)(*(long *)(param_1 + 0x9a8) + 0x290);
  *param_2 = *(long *)(*(long *)(param_1 + 0x9a8) + 0x288);
  *param_3 = uVar1;
  return;
}