static int protocol_from_string(const char *value)
{
    struct protocol_versions {
        const char *name;
        int version;
    };
    /*
     * Note: To avoid breaking previously valid configurations, we must retain
     * legacy entries in this table even if the underlying protocol is no
     * longer supported.  This also means that the constants SSL3_VERSION, ...
     * need to be retained indefinitely.  This table can only grow, never
     * shrink.
     */
    static const struct protocol_versions versions[] = {
        {"None", 0},
        {"SSLv3", SSL3_VERSION},
        {"TLSv1", TLS1_VERSION},
        {"TLSv1.1", TLS1_1_VERSION},
        {"TLSv1.2", TLS1_2_VERSION},
        {"TLSv1.3", TLS1_3_VERSION},
        {"DTLSv1", DTLS1_VERSION},
        {"DTLSv1.2", DTLS1_2_VERSION}
    };
    size_t i;
    size_t n = OSSL_NELEM(versions);

    for (i = 0; i < n; i++)
        if (strcmp(versions[i].name, value) == 0)
            return versions[i].version;
    return -1;
}