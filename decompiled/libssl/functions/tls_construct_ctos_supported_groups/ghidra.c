undefined8 tls_construct_ctos_supported_groups(long param_1,undefined8 param_2)

{
  short sVar1;
  short sVar2;
  int iVar3;
  undefined8 uVar4;
  char *pcVar5;
  ulong uVar6;
  long in_FS_OFFSET;
  long local_78;
  long local_70;
  int local_5c;
  int local_58;
  int local_54;
  long local_50;
  ulong local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = 0;
  local_48 = 0;
  iVar3 = ssl_get_min_max_version(param_1,&local_5c,&local_58,0);
  if (iVar3 == 0) {
    iVar3 = use_ecc(param_1,local_5c,local_58);
    if ((iVar3 == 0) &&
       ((uVar4 = 2, (*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0 ||
        (local_58 < 0x304)))) goto LAB_001606b9;
    tls1_get_supported_groups(param_1,&local_50,&local_48);
    iVar3 = WPACKET_put_bytes__(param_2,10,2);
    if ((iVar3 == 0) ||
       (((iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 == 0 ||
         (iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 == 0)) ||
        (iVar3 = WPACKET_set_flags(param_2,1), iVar3 == 0)))) {
      ERR_new();
      uVar4 = 0xd9;
LAB_00160695:
      ERR_set_debug("ssl/statem/extensions_clnt.c",uVar4,"tls_construct_ctos_supported_groups");
      pcVar5 = (char *)0x0;
      iVar3 = 0xc0103;
    }
    else {
      if (local_48 == 0) {
        iVar3 = WPACKET_close(param_2);
        if ((iVar3 == 0) || (iVar3 = WPACKET_close(param_2), iVar3 == 0)) {
LAB_001608a7:
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_clnt.c",0xfd,"tls_construct_ctos_supported_groups");
          pcVar5 = "No groups enabled for max supported SSL/TLS version";
          iVar3 = 0x127;
          goto LAB_001606a8;
        }
      }
      else {
        local_78 = 0;
        uVar6 = 0;
        local_70 = 0;
        do {
          sVar1 = *(short *)(local_50 + uVar6 * 2);
          iVar3 = tls_valid_group(param_1,sVar1,local_5c,local_58,0,&local_54);
          if ((iVar3 != 0) && (iVar3 = tls_group_allowed(param_1,sVar1,0x20004), iVar3 != 0)) {
            sVar2 = ssl_group_id_internal_to_tls13(sVar1);
            if ((sVar2 != 0) && ((sVar2 != sVar1 && (local_58 == 0x304)))) {
              iVar3 = WPACKET_put_bytes__(param_2,sVar2,2);
              if (iVar3 == 0) {
                ERR_new();
                uVar4 = 0xe9;
                goto LAB_00160695;
              }
              local_70 = local_70 + 1;
              local_78 = local_78 + 1;
              if (local_5c == 0x304) goto LAB_00160768;
            }
            iVar3 = WPACKET_put_bytes__(param_2,sVar1,2);
            if (iVar3 == 0) {
              ERR_new();
              uVar4 = 0xf3;
              goto LAB_00160695;
            }
            if (local_54 != 0) {
              local_70 = local_70 + (ulong)(local_58 == 0x304);
            }
            local_78 = local_78 + 1;
          }
LAB_00160768:
          uVar6 = uVar6 + 1;
        } while (uVar6 < local_48);
        iVar3 = WPACKET_close(param_2);
        if ((iVar3 == 0) || (iVar3 = WPACKET_close(param_2), iVar3 == 0)) {
          if (local_78 == 0) goto LAB_001608a7;
          ERR_new();
          uVar4 = 0x100;
          goto LAB_00160695;
        }
        uVar4 = 1;
        if (local_70 != 0) goto LAB_001606b9;
      }
      uVar4 = 1;
      if (local_58 != 0x304) goto LAB_001606b9;
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x105,"tls_construct_ctos_supported_groups");
      pcVar5 = "No groups enabled for max supported SSL/TLS version";
      iVar3 = 0x127;
    }
  }
  else {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0xc3,"tls_construct_ctos_supported_groups");
    pcVar5 = (char *)0x0;
  }
LAB_001606a8:
  ossl_statem_fatal(param_1,0x50,iVar3,pcVar5);
  uVar4 = 0;
LAB_001606b9:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}