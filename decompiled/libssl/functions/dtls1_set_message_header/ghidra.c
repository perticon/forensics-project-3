void dtls1_set_message_header
               (long param_1,undefined param_2,undefined8 param_3,long param_4,undefined8 param_5)

{
  short sVar1;
  long lVar2;
  short sVar3;
  
  lVar2 = *(long *)(param_1 + 0x4b8);
  sVar1 = *(short *)(lVar2 + 0x10e);
  sVar3 = *(short *)(lVar2 + 0x10c);
  if (param_4 == 0) {
    *(short *)(lVar2 + 0x10c) = sVar1;
    *(short *)(lVar2 + 0x10e) = sVar1 + 1;
    sVar3 = sVar1;
  }
  *(undefined *)(lVar2 + 0x138) = param_2;
  *(undefined8 *)(lVar2 + 0x140) = param_3;
  *(short *)(lVar2 + 0x148) = sVar3;
  *(long *)(lVar2 + 0x150) = param_4;
  *(undefined8 *)(lVar2 + 0x158) = param_5;
  return;
}