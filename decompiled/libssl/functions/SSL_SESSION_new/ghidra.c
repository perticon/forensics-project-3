SSL_SESSION * SSL_SESSION_new(void)

{
  int iVar1;
  SSL_SESSION *ptr;
  time_t tVar2;
  long lVar3;
  
  iVar1 = OPENSSL_init_ssl(0x200000,0);
  ptr = (SSL_SESSION *)0x0;
  if (iVar1 != 0) {
    ptr = (SSL_SESSION *)CRYPTO_zalloc(0x3a0,"ssl/ssl_sess.c",0x81);
    if (ptr == (SSL_SESSION *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_sess.c",0x83,"SSL_SESSION_new");
      ERR_set_error(0x14,0xc0100,0);
    }
    else {
      *(undefined8 *)(ptr[1].sid_ctx + 4) = 1;
      *(undefined4 *)(ptr[1].sid_ctx + 0xc) = 1;
      *(undefined8 *)(ptr[1].sid_ctx + 0x14) = 0x130;
      tVar2 = time((time_t *)0x0);
      *(time_t *)(ptr[1].sid_ctx + 0x1c) = tVar2;
      ssl_session_calculate_timeout(ptr);
      lVar3 = CRYPTO_THREAD_lock_new();
      *(long *)(ptr[1].krb5_client_princ + 0xb0) = lVar3;
      if (lVar3 == 0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_sess.c",0x8e,"SSL_SESSION_new");
        ERR_set_error(0x14,0xc0100,0);
        CRYPTO_free(ptr);
        ptr = (SSL_SESSION *)0x0;
      }
      else {
        iVar1 = CRYPTO_new_ex_data(2,ptr,(CRYPTO_EX_DATA *)(ptr[1].krb5_client_princ + 0x28));
        if (iVar1 == 0) {
          CRYPTO_THREAD_lock_free(*(undefined8 *)(ptr[1].krb5_client_princ + 0xb0));
          CRYPTO_free(ptr);
          ptr = (SSL_SESSION *)0x0;
        }
      }
    }
  }
  return ptr;
}