ulong tls_psk_do_binder(long param_1,EVP_MD *param_2,void *param_3,size_t param_4,void *param_5,
                       uchar *param_6,long param_7,int param_8,int param_9)

{
  byte *pbVar1;
  undefined8 uVar2;
  int iVar3;
  uint uVar4;
  EVP_MD_CTX *ctx;
  EVP_PKEY *pkey;
  uchar *sigret;
  undefined8 uVar5;
  ulong uVar6;
  ulong uVar7;
  void *len;
  undefined8 uVar8;
  ulong uVar9;
  long lVar10;
  long in_FS_OFFSET;
  undefined8 uVar11;
  undefined8 uVar12;
  char *local_170;
  void *local_150;
  uchar local_148 [64];
  undefined local_108 [64];
  undefined local_c8 [64];
  uchar local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar3 = EVP_MD_get_size(param_2);
  len = (void *)(long)iVar3;
  if (iVar3 < 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions.c",0x5bc,"tls_psk_do_binder");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    if (param_9 == 0) {
      local_170 = "res binder";
LAB_0015f937:
      lVar10 = param_1 + 0x544;
    }
    else {
      uVar4 = *(uint *)(param_1 + 0x38);
      if ((*(int *)(param_1 + 0x84) == 2) && (*(int *)(*(long *)(param_1 + 0x918) + 0x354) == 0)) {
        uVar4 = uVar4 | *(uint *)(param_7 + 0x354);
      }
      if (uVar4 != 0) {
        local_170 = "ext binder";
        goto LAB_0015f937;
      }
      lVar10 = param_7 + 0x10;
      local_170 = "ext binder";
    }
    iVar3 = tls13_generate_secret
                      (param_1,param_2,0,param_7 + 0x50,*(undefined8 *)(param_7 + 8),lVar10);
    if (iVar3 != 0) {
      ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
      if ((ctx != (EVP_MD_CTX *)0x0) &&
         (iVar3 = EVP_DigestInit_ex(ctx,param_2,(ENGINE *)0x0), 0 < iVar3)) {
        iVar3 = EVP_DigestFinal_ex(ctx,local_148,(uint *)0x0);
        if (0 < iVar3) {
          iVar3 = tls13_hkdf_expand(param_1,param_2,lVar10,local_170,10,local_148,len,local_108,len,
                                    1);
          if (iVar3 == 0) {
            uVar9 = 0xffffffff;
            pkey = (EVP_PKEY *)0x0;
            goto LAB_0015fa9c;
          }
          iVar3 = tls13_derive_finishedkey(param_1,param_2,local_108,local_c8);
          if (iVar3 == 0) {
LAB_0015fa13:
            uVar9 = 0xffffffff;
            pkey = (EVP_PKEY *)0x0;
          }
          else {
            iVar3 = EVP_DigestInit_ex(ctx,param_2,(ENGINE *)0x0);
            if (iVar3 < 1) {
              ERR_new();
              uVar8 = 0x5fc;
            }
            else {
              if (*(int *)(param_1 + 0x8e8) == 1) {
                uVar9 = BIO_ctrl(*(BIO **)(param_1 + 0x188),3,0,&local_150);
                if ((long)uVar9 < 1) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/extensions.c",0x60d,"tls_psk_do_binder");
                  uVar8 = 0x14c;
                }
                else {
                  if (*(int *)(param_1 + 0x38) == 0) {
LAB_0015fe73:
                    iVar3 = EVP_DigestUpdate(ctx,local_150,uVar9);
                    if (0 < iVar3) goto LAB_0015fbd7;
                    ERR_new();
                    uVar8 = 0x625;
                  }
                  else {
                    if (3 < uVar9) {
                      uVar6 = (ulong)*(byte *)((long)local_150 + 1) << 0x10 |
                              (ulong)*(byte *)((long)local_150 + 2) << 8 |
                              (ulong)*(byte *)((long)local_150 + 3);
                      if ((uVar6 <= uVar9 - 4) && (uVar7 = (uVar9 - 4) - uVar6, 3 < uVar7)) {
                        pbVar1 = (byte *)((long)local_150 + uVar6 + 5);
                        uVar7 = uVar7 - 4;
                        uVar6 = (ulong)*pbVar1 << 0x10 | (ulong)pbVar1[1] << 8 | (ulong)pbVar1[2];
                        if (uVar6 <= uVar7) {
                          uVar9 = (uVar9 + uVar6) - uVar7;
                          goto LAB_0015fe73;
                        }
                      }
                    }
                    ERR_new();
                    uVar8 = 0x61e;
                  }
                  ERR_set_debug("ssl/statem/extensions.c",uVar8,"tls_psk_do_binder");
                  uVar8 = 0xc0103;
                }
                ossl_statem_fatal(param_1,0x50,uVar8,0);
                goto LAB_0015fa13;
              }
LAB_0015fbd7:
              iVar3 = EVP_DigestUpdate(ctx,param_3,param_4);
              if ((0 < iVar3) && (iVar3 = EVP_DigestFinal_ex(ctx,local_148,(uint *)0x0), 0 < iVar3))
              {
                pkey = (EVP_PKEY *)
                       EVP_PKEY_new_raw_private_key_ex
                                 (**(undefined8 **)(param_1 + 0x9a8),&DAT_00188069,
                                  (*(undefined8 **)(param_1 + 0x9a8))[0x88],local_c8,len);
                if (pkey == (EVP_PKEY *)0x0) {
                  ERR_new();
                  uVar8 = 0x634;
                }
                else {
                  sigret = local_88;
                  if (param_8 != 0) {
                    sigret = param_6;
                  }
                  uVar8 = (*(undefined8 **)(param_1 + 0x9a8))[0x88];
                  uVar2 = **(undefined8 **)(param_1 + 0x9a8);
                  uVar12 = 0x15fc92;
                  local_150 = len;
                  uVar5 = EVP_MD_get0_name(param_2);
                  uVar11 = 0;
                  iVar3 = EVP_DigestSignInit_ex(ctx,0,uVar5,uVar2,uVar8,pkey,0,uVar12);
                  if ((((0 < iVar3) &&
                       (iVar3 = EVP_DigestSignUpdate(ctx,local_148,len,uVar11), 0 < iVar3)) &&
                      (iVar3 = EVP_DigestSignFinal(ctx,sigret,(size_t *)&local_150), 0 < iVar3)) &&
                     (local_150 == len)) {
                    uVar9 = 1;
                    if (param_8 == 0) {
                      iVar3 = CRYPTO_memcmp(param_5,sigret,(size_t)len);
                      uVar9 = (ulong)(iVar3 == 0);
                      if (iVar3 != 0) {
                        ERR_new();
                        ERR_set_debug("ssl/statem/extensions.c",0x64b,"tls_psk_do_binder");
                        ossl_statem_fatal(param_1,0x2f,0xfd,0);
                      }
                    }
                    goto LAB_0015fa9c;
                  }
                  ERR_new();
                  uVar8 = 0x641;
                }
                uVar9 = 0xffffffff;
                ERR_set_debug("ssl/statem/extensions.c",uVar8,"tls_psk_do_binder");
                ossl_statem_fatal(param_1,0x50,0xc0103,0);
                goto LAB_0015fa9c;
              }
              ERR_new();
              uVar8 = 0x62c;
            }
            uVar9 = 0xffffffff;
            pkey = (EVP_PKEY *)0x0;
            ERR_set_debug("ssl/statem/extensions.c",uVar8,"tls_psk_do_binder");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
          }
          goto LAB_0015fa9c;
        }
      }
      ERR_new();
      ERR_set_debug("ssl/statem/extensions.c",0x5ea,"tls_psk_do_binder");
      pkey = (EVP_PKEY *)0x0;
      uVar9 = 0xffffffff;
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      goto LAB_0015fa9c;
    }
  }
  uVar9 = 0xffffffff;
  ctx = (EVP_MD_CTX *)0x0;
  pkey = (EVP_PKEY *)0x0;
LAB_0015fa9c:
  OPENSSL_cleanse(local_108,0x40);
  OPENSSL_cleanse(local_c8,0x40);
  EVP_PKEY_free(pkey);
  EVP_MD_CTX_free(ctx);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar9;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}