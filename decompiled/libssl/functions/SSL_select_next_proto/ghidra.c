int SSL_select_next_proto
              (uchar **out,uchar *outlen,uchar *in,uint inlen,uchar *client,uint client_len)

{
  byte bVar1;
  byte bVar2;
  int iVar3;
  uint uVar4;
  ulong uVar5;
  byte *local_60;
  uint local_50;
  
  local_50 = 0;
  if (inlen != 0) {
    do {
      uVar5 = 0;
      local_60 = in + local_50;
      bVar1 = *local_60;
      if (client_len != 0) {
        do {
          bVar2 = client[uVar5];
          if ((bVar2 == bVar1) &&
             (iVar3 = memcmp(in + (local_50 + 1),client + ((int)uVar5 + 1),(ulong)bVar1), iVar3 == 0
             )) {
            iVar3 = 1;
            goto LAB_001394fc;
          }
          uVar4 = (int)uVar5 + 1 + (uint)bVar2;
          uVar5 = (ulong)uVar4;
        } while (uVar4 < client_len);
      }
      local_50 = local_50 + 1 + (uint)bVar1;
    } while (local_50 < inlen);
  }
  iVar3 = 2;
  local_60 = client;
LAB_001394fc:
  *out = local_60 + 1;
  *outlen = *local_60;
  return iVar3;
}