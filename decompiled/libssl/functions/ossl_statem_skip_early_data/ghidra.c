uint ossl_statem_skip_early_data(long param_1)

{
  uint uVar1;
  
  uVar1 = 0;
  if (((*(int *)(param_1 + 0xb30) == 1) && (uVar1 = *(uint *)(param_1 + 0x38), uVar1 != 0)) &&
     (uVar1 = 0, *(int *)(param_1 + 0x5c) == 0x2e)) {
    return (uint)(*(int *)(param_1 + 0x8e8) != 2);
  }
  return uVar1;
}