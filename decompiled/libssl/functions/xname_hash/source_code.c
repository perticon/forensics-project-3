static unsigned long xname_hash(const X509_NAME *a)
{
    /* This returns 0 also if SHA1 is not available */
    return X509_NAME_hash_ex((X509_NAME *)a, NULL, NULL, NULL);
}