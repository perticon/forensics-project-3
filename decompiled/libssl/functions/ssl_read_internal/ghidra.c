uint ssl_read_internal(long param_1,undefined8 param_2,undefined8 param_3,undefined8 *param_4)

{
  long lVar1;
  uint uVar2;
  long in_FS_OFFSET;
  long local_58;
  undefined8 uStack80;
  undefined8 local_48;
  undefined4 local_40;
  undefined8 local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(long *)(param_1 + 0x30) == 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x71f,"ssl_read_internal");
    uVar2 = 0xffffffff;
    ERR_set_error(0x14,0x114,0);
  }
  else {
    uVar2 = *(uint *)(param_1 + 0x44) & 2;
    if (uVar2 == 0) {
      if ((*(int *)(param_1 + 0x84) == 1) || (*(int *)(param_1 + 0x84) == 8)) {
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0x72a,"ssl_read_internal");
        ERR_set_error(0x14,0xc0101,0);
      }
      else {
        ossl_statem_check_finish_init(param_1,0);
        if ((*(byte *)(param_1 + 0x9f1) & 1) != 0) {
          lVar1 = ASYNC_get_current_job();
          if (lVar1 == 0) {
            local_38 = *(undefined8 *)(*(long *)(param_1 + 8) + 0x38);
            local_40 = 0;
            local_58 = param_1;
            uStack80 = param_2;
            local_48 = param_3;
            uVar2 = ssl_start_async_job(param_1,&local_58,ssl_io_intern);
            *param_4 = *(undefined8 *)(param_1 + 0x1d38);
            goto LAB_00137a2a;
          }
        }
        uVar2 = (**(code **)(*(long *)(param_1 + 8) + 0x38))(param_1,param_2,param_3,param_4);
      }
    }
    else {
      *(undefined4 *)(param_1 + 0x28) = 1;
      uVar2 = 0;
    }
  }
LAB_00137a2a:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}