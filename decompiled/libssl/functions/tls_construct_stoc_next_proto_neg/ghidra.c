undefined8 tls_construct_stoc_next_proto_neg(long param_1,undefined8 param_2)

{
  code *pcVar1;
  int iVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  undefined4 local_2c;
  undefined8 local_28;
  long local_20;
  
  uVar3 = 2;
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar2 = *(int *)(param_1 + 0x484);
  *(undefined4 *)(param_1 + 0x484) = 0;
  if (iVar2 != 0) {
    pcVar1 = *(code **)(*(long *)(param_1 + 0x9a8) + 0x2b8);
    if (pcVar1 != (code *)0x0) {
      iVar2 = (*pcVar1)(param_1,&local_28,&local_2c,
                        *(undefined8 *)(*(long *)(param_1 + 0x9a8) + 0x2c0));
      uVar3 = 1;
      if (iVar2 == 0) {
        iVar2 = WPACKET_put_bytes__(param_2,0x3374,2);
        if (iVar2 != 0) {
          iVar2 = WPACKET_sub_memcpy__(param_2,local_28,local_2c,2);
          if (iVar2 != 0) {
            *(undefined4 *)(param_1 + 0x484) = 1;
            goto LAB_001686dd;
          }
        }
        ERR_new();
        uVar3 = 0;
        ERR_set_debug("ssl/statem/extensions_srvr.c",0x5b6,"tls_construct_stoc_next_proto_neg");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
    }
  }
LAB_001686dd:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}