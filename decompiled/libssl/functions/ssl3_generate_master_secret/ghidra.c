ssl3_generate_master_secret(long param_1,uchar *param_2,void *param_3,size_t param_4,long *param_5)

{
  char *__s;
  int iVar1;
  EVP_MD_CTX *ctx;
  size_t cnt;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined1 *local_c0;
  long local_b8;
  uchar *local_b0;
  uint local_8c;
  uchar local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
  local_b8 = 0;
  local_c0 = salt_25795;
  local_b0 = param_2;
  if (ctx == (EVP_MD_CTX *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x1ea,"ssl3_generate_master_secret");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
    uVar2 = 0;
  }
  else {
    do {
      iVar1 = EVP_DigestInit_ex(ctx,*(EVP_MD **)(*(long *)(param_1 + 0x9a8) + 0x108),(ENGINE *)0x0);
      if (iVar1 < 1) {
LAB_00128a30:
        ERR_new();
        ERR_set_debug("ssl/s3_enc.c",0x1fb,"ssl3_generate_master_secret");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        EVP_MD_CTX_free(ctx);
        OPENSSL_cleanse(local_88,0x40);
        uVar2 = 0;
        goto LAB_001289fe;
      }
      __s = *(char **)local_c0;
      cnt = strlen(__s);
      iVar1 = EVP_DigestUpdate(ctx,__s,cnt);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestUpdate(ctx,param_3,param_4);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestUpdate(ctx,(void *)(param_1 + 0x160),0x20);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestUpdate(ctx,(void *)(param_1 + 0x140),0x20);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestFinal_ex(ctx,local_88,&local_8c);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestInit_ex(ctx,*(EVP_MD **)(*(long *)(param_1 + 0x9a8) + 0x100),(ENGINE *)0x0);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestUpdate(ctx,param_3,param_4);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestUpdate(ctx,local_88,(ulong)local_8c);
      if (iVar1 < 1) goto LAB_00128a30;
      iVar1 = EVP_DigestFinal_ex(ctx,local_b0,&local_8c);
      if (iVar1 < 1) goto LAB_00128a30;
      local_c0 = (undefined1 *)((long)local_c0 + 8);
      local_b8 = local_b8 + (ulong)local_8c;
      local_b0 = local_b0 + local_8c;
    } while (local_c0 != &DAT_0019fc78);
    EVP_MD_CTX_free(ctx);
    OPENSSL_cleanse(local_88,0x40);
    *param_5 = local_b8;
    uVar2 = 1;
  }
LAB_001289fe:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}