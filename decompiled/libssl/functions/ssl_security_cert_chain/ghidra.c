int ssl_security_cert_chain(undefined8 param_1,undefined8 param_2,long param_3,undefined4 param_4)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  int iVar4;
  
  iVar4 = 0;
  if (param_3 == 0) {
    param_3 = OPENSSL_sk_value(param_2,0);
    if (param_3 == 0) {
      return 0xc0103;
    }
    iVar4 = 1;
  }
  iVar1 = ssl_security_cert(param_1,0,param_3,param_4,1);
  if (iVar1 == 1) {
    for (; iVar2 = OPENSSL_sk_num(param_2), iVar4 < iVar2; iVar4 = iVar4 + 1) {
      uVar3 = OPENSSL_sk_value(param_2,iVar4);
      iVar2 = ssl_security_cert(param_1,0,uVar3,param_4,0);
      if (iVar2 != 1) {
        return iVar2;
      }
    }
  }
  return iVar1;
}