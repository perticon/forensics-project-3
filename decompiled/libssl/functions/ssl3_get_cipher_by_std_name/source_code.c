const SSL_CIPHER *ssl3_get_cipher_by_std_name(const char *stdname)
{
    SSL_CIPHER *tbl;
    SSL_CIPHER *alltabs[] = {tls13_ciphers, ssl3_ciphers, ssl3_scsvs};
    size_t i, j, tblsize[] = {TLS13_NUM_CIPHERS, SSL3_NUM_CIPHERS,
                              SSL3_NUM_SCSVS};

    /* this is not efficient, necessary to optimize this? */
    for (j = 0; j < OSSL_NELEM(alltabs); j++) {
        for (i = 0, tbl = alltabs[j]; i < tblsize[j]; i++, tbl++) {
            if (tbl->stdname == NULL)
                continue;
            if (strcmp(stdname, tbl->stdname) == 0) {
                return tbl;
            }
        }
    }
    return NULL;
}