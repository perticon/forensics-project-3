int ssl_set_version_bound(int method_version, int version, int *bound)
{
    int valid_tls;
    int valid_dtls;

    if (version == 0) {
        *bound = version;
        return 1;
    }

    valid_tls = version >= SSL3_VERSION && version <= TLS_MAX_VERSION_INTERNAL;
    valid_dtls =
        DTLS_VERSION_LE(version, DTLS_MAX_VERSION_INTERNAL) &&
        DTLS_VERSION_GE(version, DTLS1_BAD_VER);

    if (!valid_tls && !valid_dtls)
        return 0;

    /*-
     * Restrict TLS methods to TLS protocol versions.
     * Restrict DTLS methods to DTLS protocol versions.
     * Note, DTLS version numbers are decreasing, use comparison macros.
     *
     * Note that for both lower-bounds we use explicit versions, not
     * (D)TLS_MIN_VERSION.  This is because we don't want to break user
     * configurations.  If the MIN (supported) version ever rises, the user's
     * "floor" remains valid even if no longer available.  We don't expect the
     * MAX ceiling to ever get lower, so making that variable makes sense.
     *
     * We ignore attempts to set bounds on version-inflexible methods,
     * returning success.
     */
    switch (method_version) {
    default:
        break;

    case TLS_ANY_VERSION:
        if (valid_tls)
            *bound = version;
        break;

    case DTLS_ANY_VERSION:
        if (valid_dtls)
            *bound = version;
        break;
    }
    return 1;
}