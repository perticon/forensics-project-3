int pqueue_size(pqueue pq)

{
  long lVar1;
  int iVar2;
  
  iVar2 = 0;
  for (lVar1 = *(long *)pq; lVar1 != 0; lVar1 = *(long *)(lVar1 + 0x10)) {
    iVar2 = iVar2 + 1;
  }
  return iVar2;
}