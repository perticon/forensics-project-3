undefined8 ssl_cert_set_current(long **param_1,long param_2)

{
  long **pplVar1;
  int iVar2;
  
  if (param_1 != (long **)0x0) {
    if (param_2 == 1) {
      iVar2 = 0;
    }
    else {
      if (param_2 != 2) {
        return 0;
      }
      iVar2 = (int)((long)*param_1 - (long)(param_1 + 4) >> 3) * -0x33333333 + 1;
      if (8 < iVar2) {
        return 0;
      }
    }
    pplVar1 = param_1 + (long)iVar2 * 5 + 4;
    do {
      if ((*pplVar1 != (long *)0x0) && (pplVar1[1] != (long *)0x0)) {
        *param_1 = (long *)pplVar1;
        return 1;
      }
      pplVar1 = pplVar1 + 5;
    } while (pplVar1 != param_1 + ((ulong)(8 - iVar2) + (long)iVar2) * 5 + 9);
  }
  return 0;
}