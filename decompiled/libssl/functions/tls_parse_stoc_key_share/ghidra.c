undefined8 tls_parse_stoc_key_share(long param_1,ushort **param_2,ulong param_3)

{
  ushort *puVar1;
  EVP_PKEY *from;
  ushort uVar2;
  int iVar3;
  long lVar4;
  EVP_PKEY *to;
  undefined8 uVar5;
  ushort *puVar6;
  undefined8 uVar7;
  uint uVar8;
  long in_FS_OFFSET;
  long local_50;
  ushort *local_48;
  long local_40;
  
  from = *(EVP_PKEY **)(param_1 + 0x2e8);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((from == (EVP_PKEY *)0x0) || (*(long *)(param_1 + 0x4b0) != 0)) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x6e2,"tls_parse_stoc_key_share");
    uVar5 = 0xc0103;
LAB_00163e4f:
    ossl_statem_fatal(param_1,0x50,uVar5,0);
    uVar5 = 0;
    goto LAB_00163e60;
  }
  if (param_2[1] < (ushort *)0x2) {
    ERR_new();
    uVar5 = 0x6e7;
LAB_00163f4a:
    ERR_set_debug("ssl/statem/extensions_clnt.c",uVar5,"tls_parse_stoc_key_share");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
    uVar5 = 0;
  }
  else {
    uVar2 = **param_2;
    param_2[1] = param_2[1] + -1;
    *param_2 = *param_2 + 1;
    uVar2 = ssl_group_id_tls13_to_internal(uVar2 << 8 | uVar2 >> 8);
    uVar8 = (uint)uVar2;
    if ((param_3 & 0x800) == 0) {
      if (uVar8 == *(ushort *)(param_1 + 0x4ae)) {
        lVar4 = *(long *)(param_1 + 0x918);
        if (*(int *)(param_1 + 0x4d0) == 0) {
          *(uint *)(lVar4 + 0x308) = uVar8;
        }
        else if (uVar8 != *(uint *)(lVar4 + 0x308)) {
          lVar4 = ssl_session_dup(lVar4,0);
          if (lVar4 == 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_clnt.c",0x727,"tls_parse_stoc_key_share");
            uVar5 = 0xc0100;
            goto LAB_00163e4f;
          }
          SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x918));
          *(long *)(param_1 + 0x918) = lVar4;
          *(uint *)(lVar4 + 0x308) = uVar8;
        }
        lVar4 = tls1_group_id_lookup(*(undefined8 *)(param_1 + 0x9a8),uVar2);
        if (lVar4 != 0) {
          if ((ushort *)0x1 < param_2[1]) {
            puVar6 = param_2[1] + -1;
            uVar2 = **param_2;
            if (puVar6 == (ushort *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8)) {
              puVar1 = *param_2 + 1;
              param_2[1] = (ushort *)0x0;
              *param_2 = (ushort *)((long)puVar1 + (long)puVar6);
              if (puVar6 != (ushort *)0x0) {
                if (*(char *)(lVar4 + 0x30) == '\0') {
                  to = EVP_PKEY_new();
                  if ((to == (EVP_PKEY *)0x0) ||
                     (iVar3 = EVP_PKEY_copy_parameters(to,from), iVar3 < 1)) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/extensions_clnt.c",0x73e,"tls_parse_stoc_key_share");
                    uVar5 = 0x128;
                    uVar7 = 0x50;
                  }
                  else {
                    iVar3 = tls13_set_encoded_pub_key(to,puVar1,puVar6);
                    if (0 < iVar3) {
                      iVar3 = ssl_derive(param_1,from,to,1);
                      if (iVar3 == 0) {
                        EVP_PKEY_free(to);
                        uVar5 = 0;
                        goto LAB_00163e60;
                      }
                      *(EVP_PKEY **)(param_1 + 0x4b0) = to;
                      goto LAB_00164112;
                    }
                    ERR_new();
                    ERR_set_debug("ssl/statem/extensions_clnt.c",0x745,"tls_parse_stoc_key_share");
                    uVar5 = 0x132;
                    uVar7 = 0x2f;
                  }
                  ossl_statem_fatal(param_1,uVar7,uVar5,0);
                  EVP_PKEY_free(to);
                  uVar5 = 0;
                }
                else {
                  uVar5 = ssl_decapsulate(param_1,from,puVar1,puVar6,1);
                  if ((int)uVar5 == 0) goto LAB_00163e60;
LAB_00164112:
                  *(undefined *)(param_1 + 0x4ad) = 1;
                  uVar5 = 1;
                }
                goto LAB_00163e60;
              }
            }
          }
          ERR_new();
          uVar5 = 0x736;
          goto LAB_00163f4a;
        }
        ERR_new();
        uVar5 = 0x730;
      }
      else {
        ERR_new();
        uVar5 = 0x715;
      }
    }
    else {
      local_50 = 0;
      puVar6 = param_2[1];
      if (puVar6 != (ushort *)0x0) {
        ERR_new();
        uVar5 = 0x6f1;
        goto LAB_00163f4a;
      }
      if (uVar2 == *(ushort *)(param_1 + 0x4ae)) {
        ERR_new();
        uVar5 = 0x6fa;
      }
      else {
        tls1_get_supported_groups(param_1,&local_50,&local_48);
        if (local_48 != (ushort *)0x0) {
          do {
            if (uVar8 == *(ushort *)(local_50 + (long)puVar6 * 2)) {
              iVar3 = tls_group_allowed(param_1,uVar2,0x20004);
              if (iVar3 != 0) {
                *(ushort *)(param_1 + 0x4ae) = uVar2;
                EVP_PKEY_free(*(EVP_PKEY **)(param_1 + 0x2e8));
                uVar5 = 1;
                *(undefined8 *)(param_1 + 0x2e8) = 0;
                goto LAB_00163e60;
              }
              break;
            }
            puVar6 = (ushort *)((long)puVar6 + 1);
          } while (puVar6 != local_48);
        }
        ERR_new();
        uVar5 = 0x706;
      }
    }
    ERR_set_debug("ssl/statem/extensions_clnt.c",uVar5,"tls_parse_stoc_key_share");
    ossl_statem_fatal(param_1,0x2f,0x6c,0);
    uVar5 = 0;
  }
LAB_00163e60:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
}