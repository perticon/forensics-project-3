custom_ext_parse(long param_1,uint param_2,undefined4 param_3,undefined8 param_4,undefined8 param_5,
                undefined8 param_6,undefined8 param_7)

{
  int iVar1;
  long lVar2;
  long in_FS_OFFSET;
  undefined uVar3;
  undefined8 uVar4;
  undefined4 local_44;
  long local_40;
  
  uVar3 = 2;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((param_2 & 0x180) != 0) {
    uVar3 = *(int *)(param_1 + 0x38) != 0;
  }
  lVar2 = custom_ext_find(*(long *)(param_1 + 0x898) + 0x1d8,uVar3,param_3,0);
  if (lVar2 != 0) {
    uVar4 = 0x16484d;
    iVar1 = extension_is_relevant(param_1,*(undefined4 *)(lVar2 + 8),param_2);
    if (iVar1 != 0) {
      if (((param_2 & 0x700) != 0) && ((*(byte *)(lVar2 + 0xc) & 2) == 0)) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_cust.c",0x8e,"custom_ext_parse");
        ossl_statem_fatal(param_1,0x6e,0x6e,0);
        uVar4 = 0;
        goto LAB_001648ad;
      }
      if ((param_2 & 0x4080) != 0) {
        *(uint *)(lVar2 + 0xc) = *(uint *)(lVar2 + 0xc) | 1;
      }
      if (*(code **)(lVar2 + 0x28) != (code *)0x0) {
        iVar1 = (**(code **)(lVar2 + 0x28))
                          (param_1,param_3,param_2,param_4,param_5,param_6,param_7,&local_44,
                           *(undefined8 *)(lVar2 + 0x30),uVar4);
        if (iVar1 < 1) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_cust.c",0xa2,"custom_ext_parse");
          ossl_statem_fatal(param_1,local_44,0x6e,0);
          uVar4 = 0;
          goto LAB_001648ad;
        }
      }
    }
  }
  uVar4 = 1;
LAB_001648ad:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}