int custom_ext_parse(SSL *s, unsigned int context, unsigned int ext_type,
                     const unsigned char *ext_data, size_t ext_size, X509 *x,
                     size_t chainidx)
{
    int al;
    custom_ext_methods *exts = &s->cert->custext;
    custom_ext_method *meth;
    ENDPOINT role = ENDPOINT_BOTH;

    if ((context & (SSL_EXT_CLIENT_HELLO | SSL_EXT_TLS1_2_SERVER_HELLO)) != 0)
        role = s->server ? ENDPOINT_SERVER : ENDPOINT_CLIENT;

    meth = custom_ext_find(exts, role, ext_type, NULL);
    /* If not found return success */
    if (!meth)
        return 1;

    /* Check if extension is defined for our protocol. If not, skip */
    if (!extension_is_relevant(s, meth->context, context))
        return 1;

    if ((context & (SSL_EXT_TLS1_2_SERVER_HELLO
                    | SSL_EXT_TLS1_3_SERVER_HELLO
                    | SSL_EXT_TLS1_3_ENCRYPTED_EXTENSIONS)) != 0) {
        /*
         * If it's ServerHello or EncryptedExtensions we can't have any
         * extensions not sent in ClientHello.
         */
        if ((meth->ext_flags & SSL_EXT_FLAG_SENT) == 0) {
            SSLfatal(s, TLS1_AD_UNSUPPORTED_EXTENSION, SSL_R_BAD_EXTENSION);
            return 0;
        }
    }

    /*
     * Extensions received in the ClientHello or CertificateRequest are marked
     * with the SSL_EXT_FLAG_RECEIVED. This is so we know to add the equivalent
     * extensions in the response messages
     */
    if ((context & (SSL_EXT_CLIENT_HELLO | SSL_EXT_TLS1_3_CERTIFICATE_REQUEST))
            != 0)
        meth->ext_flags |= SSL_EXT_FLAG_RECEIVED;

    /* If no parse function set return success */
    if (!meth->parse_cb)
        return 1;

    if (meth->parse_cb(s, ext_type, context, ext_data, ext_size, x, chainidx,
                       &al, meth->parse_arg) <= 0) {
        SSLfatal(s, al, SSL_R_BAD_EXTENSION);
        return 0;
    }

    return 1;
}