SSL_CTX_add_custom_ext
          (long param_1,uint param_2,uint param_3,long param_4,long param_5,undefined8 param_6,
          undefined8 param_7,undefined8 param_8)

{
  undefined (*pauVar1) [16];
  long lVar2;
  int iVar3;
  long lVar4;
  void *pvVar5;
  
  if ((param_4 != 0) || (param_5 == 0)) {
    lVar2 = *(long *)(param_1 + 0x158);
    if (param_2 == 0x12) {
      if (((param_3 & 0x80) != 0) && (iVar3 = SSL_CTX_ct_is_enabled(), iVar3 != 0)) {
        return 0;
      }
      SSL_extension_supported(0x12);
    }
    else {
      iVar3 = SSL_extension_supported(param_2);
      if (iVar3 != 0) {
        return 0;
      }
      if (0xffff < param_2) {
        return 0;
      }
    }
    lVar4 = custom_ext_find(lVar2 + 0x1d8,2,param_2,0);
    if ((lVar4 == 0) &&
       (pvVar5 = CRYPTO_realloc(*(void **)(lVar2 + 0x1d8),
                                ((int)*(undefined8 *)(lVar2 + 0x1e0) + 1) * 0x38,
                                "ssl/statem/extensions_cust.c",0x181), pvVar5 != (void *)0x0)) {
      *(void **)(lVar2 + 0x1d8) = pvVar5;
      pauVar1 = (undefined (*) [16])((long)pvVar5 + *(long *)(lVar2 + 0x1e0) * 0x38);
      *pauVar1 = (undefined  [16])0x0;
      *(undefined8 *)(pauVar1[2] + 8) = param_7;
      *(undefined4 *)(*pauVar1 + 4) = 2;
      *(uint *)(*pauVar1 + 8) = param_3;
      *(short *)*pauVar1 = (short)param_2;
      *(undefined8 *)pauVar1[2] = param_6;
      *(undefined8 *)pauVar1[3] = param_8;
      *(long *)pauVar1[1] = param_4;
      *(long *)(pauVar1[1] + 8) = param_5;
      *(long *)(lVar2 + 0x1e0) = *(long *)(lVar2 + 0x1e0) + 1;
      return 1;
    }
  }
  return 0;
}