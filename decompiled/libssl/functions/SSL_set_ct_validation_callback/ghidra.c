undefined4 SSL_set_ct_validation_callback(SSL *param_1,long param_2,X509_VERIFY_PARAM *param_3)

{
  int iVar1;
  long lVar2;
  
  if (param_2 != 0) {
    iVar1 = SSL_CTX_has_client_custom_ext(*(undefined8 *)&param_1[3].ex_data.dummy,0x12);
    if (iVar1 != 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x1416,"SSL_set_ct_validation_callback");
      ERR_set_error(0x14,0xce,0);
      return 0;
    }
    lVar2 = SSL_ctrl(param_1,0x41,1,(void *)0x0);
    if (lVar2 == 0) {
      return 0;
    }
  }
  *(long *)&param_1[4].hit = param_2;
  param_1[4].param = param_3;
  return 1;
}