undefined8 init_status_request(long param_1)

{
  if (*(int *)(param_1 + 0x38) != 0) {
    *(undefined4 *)(param_1 + 0xa60) = 0xffffffff;
    return 1;
  }
  CRYPTO_free(*(void **)(param_1 + 0xa88));
  *(undefined8 *)(param_1 + 0xa88) = 0;
  *(undefined8 *)(param_1 + 0xa90) = 0;
  return 1;
}