tls1_enc(undefined4 *param_1,long param_2,ulong param_3,int param_4,long param_5,long param_6)

{
  undefined4 uVar1;
  char cVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  long lVar6;
  undefined8 uVar7;
  ulong uVar8;
  ulong uVar9;
  undefined8 *puVar10;
  undefined8 *puVar11;
  ulong uVar12;
  undefined4 *puVar13;
  long lVar14;
  uint uVar15;
  size_t *psVar16;
  size_t *psVar17;
  long *plVar18;
  undefined8 *puVar19;
  long lVar20;
  ulong *puVar21;
  EVP_CIPHER_CTX *ctx;
  ulong uVar22;
  undefined8 *puVar23;
  ulong uVar24;
  long in_FS_OFFSET;
  bool bVar25;
  undefined8 uVar26;
  long local_448;
  uint local_42c;
  undefined8 local_428;
  undefined8 uStack1056;
  undefined8 local_418;
  undefined8 uStack1040;
  undefined8 local_408;
  int local_3ec;
  ulong local_3e8 [32];
  undefined8 local_2e8 [4];
  undefined8 local_2c8;
  undefined8 local_2c0;
  undefined8 uStack696;
  undefined8 local_2b0;
  undefined8 uStack680;
  undefined8 local_2a0;
  undefined8 local_1e8 [52];
  undefined local_48;
  undefined uStack71;
  undefined4 uStack70;
  undefined2 uStack66;
  long local_40;
  
  local_42c = param_1[0x150];
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_4 == 0) {
    if (param_3 == 0) goto LAB_0015b430;
    lVar6 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x218));
    if (lVar6 != 0) {
      uVar7 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x218));
      iVar3 = EVP_MD_get_size(uVar7);
      if (iVar3 < 0) {
        ERR_new();
        uVar7 = 0x40d;
        goto LAB_0015b441;
      }
    }
    ctx = *(EVP_CIPHER_CTX **)(param_1 + 0x212);
    if (ctx != (EVP_CIPHER_CTX *)0x0) {
      local_42c = local_42c & 4;
      local_448 = EVP_CIPHER_CTX_get0_cipher(ctx);
      goto LAB_0015b16e;
    }
    goto LAB_0015b3c0;
  }
  local_42c = local_42c & 8;
  if (param_3 == 0) {
LAB_0015b430:
    ERR_new();
    uVar7 = 0x3df;
    goto LAB_0015b441;
  }
  lVar6 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x224));
  if (lVar6 != 0) {
    uVar7 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x224));
    iVar3 = EVP_MD_get_size(uVar7);
    if (iVar3 < 0) {
      ERR_new();
      uVar7 = 999;
      goto LAB_0015b441;
    }
  }
  ctx = *(EVP_CIPHER_CTX **)(param_1 + 0x21e);
  if (ctx == (EVP_CIPHER_CTX *)0x0) {
LAB_0015b3c0:
    psVar16 = (size_t *)(param_2 + 8);
    do {
      psVar17 = psVar16 + 10;
      memmove((void *)psVar16[3],(void *)psVar16[4],*psVar16);
      psVar16[4] = psVar16[3];
      psVar16 = psVar17;
    } while (psVar17 != (size_t *)(param_2 + 8) + param_3 * 10);
  }
  else {
    local_448 = EVP_CIPHER_CTX_get0_cipher(ctx);
    if (((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 1) != 0) &&
       (iVar3 = EVP_CIPHER_get_mode(local_448), iVar3 == 2)) {
      iVar3 = EVP_CIPHER_get_iv_length(local_448);
      if (1 < iVar3) {
        uVar22 = 0;
        plVar18 = (long *)(param_2 + 0x20);
        do {
          if (*plVar18 != plVar18[1]) {
            ERR_new();
            uVar7 = 0x3ff;
            goto LAB_0015b441;
          }
          iVar4 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x26a),plVar18[1],(long)iVar3);
          if (iVar4 < 1) {
            ERR_new();
            uVar7 = 0x403;
            goto LAB_0015b441;
          }
          uVar22 = uVar22 + 1;
          plVar18 = plVar18 + 10;
        } while (param_3 != uVar22);
      }
    }
LAB_0015b16e:
    if (*(long *)(param_1 + 0x246) == 0) goto LAB_0015b3c0;
    if (local_448 == 0) {
      if (param_3 == 0) goto LAB_0015b402;
      goto LAB_0015b3c0;
    }
    lVar6 = EVP_CIPHER_get0_provider(local_448);
    uVar7 = EVP_CIPHER_CTX_get0_cipher(ctx);
    iVar3 = EVP_CIPHER_get_block_size(uVar7);
    uVar22 = (ulong)iVar3;
    if (1 < param_3) {
      uVar7 = EVP_CIPHER_CTX_get0_cipher(ctx);
      uVar24 = EVP_CIPHER_get_flags(uVar7);
      if ((uVar24 & 0x800000) != 0) goto LAB_0015b1c4;
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x429,__func___25985);
      uVar7 = 0x196;
LAB_0015b454:
      ossl_statem_fatal(param_1,0x50,uVar7,0);
LAB_0015b463:
      uVar7 = 0;
      goto LAB_0015b407;
    }
    if (param_3 == 0) {
      iVar3 = 0;
LAB_0015bac2:
      iVar4 = EVP_CIPHER_CTX_ctrl(ctx,0x22,0,local_2e8);
      if (iVar4 < 1) {
LAB_0015ba96:
        ERR_new();
        uVar7 = 0x47e;
      }
      else {
        iVar4 = 0;
LAB_0015b664:
        iVar5 = EVP_CIPHER_CTX_ctrl(ctx,0x23,iVar4,local_2e8);
        if ((0 < iVar5) && (iVar4 = EVP_CIPHER_CTX_ctrl(ctx,0x24,iVar4,local_3e8), 0 < iVar4))
        goto LAB_0015b6a6;
        ERR_new();
        uVar7 = 0x489;
      }
      ERR_set_debug("ssl/record/ssl3_record.c",uVar7,__func___25985);
      ossl_statem_fatal(param_1,0x50,0x196,0);
      uVar7 = 0;
      goto LAB_0015b407;
    }
LAB_0015b1c4:
    puVar19 = local_1e8;
    iVar3 = 0;
    puVar23 = (undefined8 *)(param_1 + 0x73e);
    if (param_4 != 0) {
      puVar23 = (undefined8 *)(param_1 + 0x740);
    }
    puVar13 = (undefined4 *)(param_2 + 4);
    uVar24 = 0;
    do {
      *(undefined8 *)((long)local_3e8 + uVar24 * 2 * 4) = *(undefined8 *)(puVar13 + 1);
      EVP_CIPHER_CTX_get0_cipher(ctx);
      uVar8 = EVP_CIPHER_get_flags();
      if ((uVar8 & 0x200000) == 0) {
        if (uVar22 == 1 || param_4 == 0) {
          if (param_4 == 0) {
            uVar8 = *(ulong *)((long)local_3e8 + uVar24 * 2 * 4);
LAB_0015b481:
            if (uVar8 == 0) goto LAB_0015b463;
            if (uVar8 % uVar22 != 0) {
              uVar7 = 0;
              goto LAB_0015b407;
            }
          }
        }
        else if (lVar6 == 0) {
          uVar8 = *(ulong *)((long)local_3e8 + uVar24 * 2 * 4);
          uVar12 = uVar22 - uVar8 % uVar22;
          if (0x100 < uVar12) {
            ERR_new();
            uVar7 = 0x463;
            goto LAB_0015b441;
          }
          uVar9 = uVar8 + uVar12;
          if (uVar8 < uVar9) {
            do {
              *(char *)(*(long *)(puVar13 + 9) + uVar8) = (char)uVar12 + -1;
              uVar8 = uVar8 + 1;
              uVar9 = *(long *)((long)local_3e8 + uVar24 * 2 * 4) + uVar12;
            } while (uVar8 < uVar9);
          }
          *(ulong *)(puVar13 + 1) = *(long *)(puVar13 + 1) + uVar12;
          *(ulong *)((long)local_3e8 + uVar24 * 2 * 4) = uVar9;
        }
      }
      else {
        uVar15 = *(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8;
        if (param_4 == 0) {
          if (uVar15 != 0) {
            uVar8 = (ulong)**(ushort **)(param_1 + 0x744);
            local_48 = (undefined)(uVar8 >> 8);
            puVar11 = puVar23;
            goto LAB_0015b271;
          }
LAB_0015b4c8:
          *puVar19 = *puVar23;
          cVar2 = *(char *)((long)puVar23 + 7) + '\x01';
          *(char *)((long)puVar23 + 7) = cVar2;
          if (((((cVar2 == '\0') &&
                (cVar2 = *(char *)((long)puVar23 + 6) + '\x01', *(char *)((long)puVar23 + 6) = cVar2
                , cVar2 == '\0')) &&
               (cVar2 = *(char *)((long)puVar23 + 5) + '\x01', *(char *)((long)puVar23 + 5) = cVar2,
               cVar2 == '\0')) &&
              ((cVar2 = *(char *)((long)puVar23 + 4) + '\x01', *(char *)((long)puVar23 + 4) = cVar2,
               cVar2 == '\0' &&
               (cVar2 = *(char *)((long)puVar23 + 3) + '\x01', *(char *)((long)puVar23 + 3) = cVar2,
               cVar2 == '\0')))) &&
             ((cVar2 = *(char *)((long)puVar23 + 2) + '\x01', *(char *)((long)puVar23 + 2) = cVar2,
              cVar2 == '\0' &&
              (cVar2 = *(char *)((long)puVar23 + 1) + '\x01', *(char *)((long)puVar23 + 1) = cVar2,
              cVar2 == '\0')))) {
            *(char *)puVar23 = *(char *)puVar23 + '\x01';
          }
        }
        else {
          if (uVar15 == 0) goto LAB_0015b4c8;
          uVar8 = (ulong)*(ushort *)(*(long *)(param_1 + 0x744) + 2);
          local_48 = (undefined)(uVar8 >> 8);
          puVar11 = (undefined8 *)(param_1 + 0x740);
LAB_0015b271:
          uStack71 = (undefined)uVar8;
          uStack70 = *(undefined4 *)((long)puVar11 + 2);
          uStack66 = *(undefined2 *)((long)puVar11 + 6);
          *puVar19 = CONCAT26(uStack66,CONCAT42(uStack70,CONCAT11(uStack71,local_48)));
        }
        *(char *)(puVar19 + 1) = (char)*puVar13;
        uVar1 = *param_1;
        *(char *)((long)puVar19 + 9) = (char)((uint)uVar1 >> 8);
        *(char *)((long)puVar19 + 10) = (char)uVar1;
        uVar7 = *(undefined8 *)(puVar13 + 1);
        *(char *)((long)puVar19 + 0xc) = (char)uVar7;
        *(char *)((long)puVar19 + 0xb) = (char)((ulong)uVar7 >> 8);
        iVar3 = EVP_CIPHER_CTX_ctrl(ctx,0x16,0xd,puVar19);
        if (iVar3 < 1) {
          ERR_new();
          uVar7 = 0x450;
          goto LAB_0015b441;
        }
        uVar8 = *(ulong *)((long)local_3e8 + uVar24 * 2 * 4);
        if (param_4 == 0) goto LAB_0015b481;
        *(long *)(puVar13 + 1) = *(long *)(puVar13 + 1) + (long)iVar3;
        *(ulong *)((long)local_3e8 + uVar24 * 2 * 4) = uVar8 + (long)iVar3;
      }
      uVar24 = uVar24 + 1;
      puVar13 = puVar13 + 0x14;
      puVar19 = (undefined8 *)((long)puVar19 + 0xd);
    } while (uVar24 < param_3);
    if (param_3 != 1) {
      if (param_3 == 0) goto LAB_0015bac2;
      puVar23 = local_2e8 + param_3;
      puVar19 = (undefined8 *)(param_2 + 0x20);
      puVar11 = local_2e8;
      do {
        uVar7 = *puVar19;
        puVar10 = puVar11 + 1;
        puVar19 = puVar19 + 10;
        *puVar11 = uVar7;
        puVar11 = puVar10;
      } while (puVar23 != puVar10);
      iVar4 = (int)param_3;
      iVar5 = EVP_CIPHER_CTX_ctrl(ctx,0x22,iVar4,local_2e8);
      puVar11 = (undefined8 *)(param_2 + 0x28);
      puVar19 = local_2e8;
      if (iVar5 < 1) goto LAB_0015ba96;
      do {
        uVar7 = *puVar11;
        puVar10 = puVar19 + 1;
        puVar11 = puVar11 + 10;
        *puVar19 = uVar7;
        puVar19 = puVar10;
      } while (puVar23 != puVar10);
      goto LAB_0015b664;
    }
LAB_0015b6a6:
    if (((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) && (local_42c != 0))
    {
      if (param_4 == 0) {
        puVar13 = param_1 + 0x73e;
        uVar15 = 0;
      }
      else {
        puVar13 = param_1 + 0x740;
        uVar15 = ((uint)(*(ulong *)(param_1 + 0x2a) >> 10) ^ 1) & 1;
      }
      iVar4 = EVP_CIPHER_CTX_ctrl(ctx,0x2a,uVar15,puVar13);
      if (iVar4 < 1) {
        ERR_new();
        uVar7 = 0x49d;
        goto LAB_0015b441;
      }
    }
    if (lVar6 == 0) {
      iVar4 = EVP_Cipher(ctx,*(uchar **)(param_2 + 0x20),*(uchar **)(param_2 + 0x28),
                         (uint)local_3e8[0]);
      uVar7 = EVP_CIPHER_CTX_get0_cipher(ctx);
      uVar24 = EVP_CIPHER_get_flags(uVar7);
      if ((uVar24 & 0x100000) == 0) {
        bVar25 = iVar4 == 0;
      }
      else {
        bVar25 = SUB41((uint)iVar4 >> 0x1f,0);
      }
      if (bVar25 != false) goto LAB_0015b463;
      if ((param_4 == 0) && (param_3 != 0)) {
        if (iVar3 != 0) {
          param_6 = (long)iVar3;
        }
        uVar24 = 0;
        puVar21 = (ulong *)(param_2 + 8);
        lVar6 = param_5;
        do {
          iVar3 = EVP_CIPHER_get_mode(local_448);
          if ((iVar3 == 6) || (iVar3 = EVP_CIPHER_get_mode(local_448), iVar3 == 7)) {
            uVar8 = puVar21[3] + _DAT_0018b8b0;
            uVar12 = puVar21[4] + _UNK_0018b8b8;
            *puVar21 = *puVar21 - 8;
            puVar21[3] = uVar8;
            puVar21[4] = uVar12;
          }
          else if ((uVar22 != 1) &&
                  ((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 1) != 0)) {
            if (*puVar21 < uVar22) goto LAB_0015b463;
            puVar21[1] = puVar21[1] - uVar22;
            *puVar21 = *puVar21 - uVar22;
            puVar21[3] = puVar21[3] + uVar22;
            puVar21[4] = puVar21[4] + uVar22;
          }
          uVar7 = **(undefined8 **)(param_1 + 0x26a);
          uVar26 = 0x15ba09;
          uVar8 = EVP_CIPHER_get_flags(local_448);
          if (param_5 == 0) {
            lVar20 = 0;
            lVar14 = 0;
          }
          else {
            lVar20 = lVar6 + 8;
            lVar14 = lVar6;
          }
          iVar3 = tls1_cbc_remove_padding_and_mac
                            (puVar21,puVar21[1],puVar21[3],lVar14,lVar20,uVar22,param_6,
                             (uint)(uVar8 >> 0x15) & 1,uVar7,uVar26);
          if (iVar3 == 0) goto LAB_0015b463;
          uVar24 = uVar24 + 1;
          lVar6 = lVar6 + 0x10;
          puVar21 = puVar21 + 10;
        } while (param_3 != uVar24);
      }
    }
    else {
      if (param_3 != 1) {
        ERR_new();
        uVar7 = 0x4a7;
LAB_0015b441:
        ERR_set_debug("ssl/record/ssl3_record.c",uVar7,__func___25985);
        uVar7 = 0xc0103;
        goto LAB_0015b454;
      }
      iVar3 = EVP_CipherUpdate(ctx,*(uchar **)(param_2 + 0x20),&local_3ec,
                               *(uchar **)(param_2 + 0x28),(uint)local_3e8[0]);
      if (iVar3 == 0) goto LAB_0015b463;
      *(long *)(param_2 + 8) = (long)local_3ec;
      if (param_4 == 0) {
        iVar3 = EVP_CIPHER_get_mode(local_448);
        if (iVar3 == 6) {
          lVar6 = _UNK_0018b8b8 + *(long *)(param_2 + 0x28);
          *(long *)(param_2 + 0x20) = _DAT_0018b8b0 + *(long *)(param_2 + 0x20);
          *(long *)(param_2 + 0x28) = lVar6;
        }
        else {
          iVar3 = EVP_CIPHER_get_mode(local_448);
          if (iVar3 == 7) {
            lVar6 = _UNK_0018b8b8 + *(long *)(param_2 + 0x28);
            *(long *)(param_2 + 0x20) = _DAT_0018b8b0 + *(long *)(param_2 + 0x20);
            *(long *)(param_2 + 0x28) = lVar6;
          }
          else if ((uVar22 != 1) &&
                  ((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 1) != 0)) {
            *(long *)(param_2 + 0x10) = *(long *)(param_2 + 0x10) - uVar22;
            *(ulong *)(param_2 + 0x20) = uVar22 + *(long *)(param_2 + 0x20);
            *(ulong *)(param_2 + 0x28) = uVar22 + *(long *)(param_2 + 0x28);
          }
        }
        if (param_5 != 0) {
          *(undefined4 *)(param_5 + 8) = 0;
          OSSL_PARAM_construct_octet_ptr(&local_428,"tls-mac",param_5,param_6);
          local_2c8 = local_408;
          local_2e8[0] = local_428;
          local_2e8[1] = uStack1056;
          local_2e8[2] = local_418;
          local_2e8[3] = uStack1040;
          OSSL_PARAM_construct_end(&local_428);
          local_2a0 = local_408;
          local_2c0 = local_428;
          uStack696 = uStack1056;
          local_2b0 = local_418;
          uStack680 = uStack1040;
          iVar3 = EVP_CIPHER_CTX_get_params(ctx,local_2e8);
          if (iVar3 == 0) {
            ERR_new();
            uVar7 = 0x4d0;
            goto LAB_0015b441;
          }
        }
      }
    }
  }
LAB_0015b402:
  uVar7 = 1;
LAB_0015b407:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar7;
}