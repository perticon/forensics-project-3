bool ssl_security_default_callback
               (long param_1,undefined8 param_2,int param_3,int param_4,int param_5,long param_6)

{
  int iVar1;
  long in_FS_OFFSET;
  bool bVar2;
  int local_34;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = ssl_get_security_level_bits(param_1,param_2,&local_34);
  if (local_34 == 0) {
    bVar2 = 0x4f < param_4 || param_3 != 0x40007;
  }
  else if (param_3 == 0xf) {
    bVar2 = local_34 < 2;
  }
  else {
    if (param_3 < 0x10) {
      if (param_3 == 9) {
        if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
          bVar2 = local_34 < 1 || 0x302 < param_5;
        }
        else {
          bVar2 = param_5 != 0x100 && param_5 < 0xfefe || local_34 < 1;
        }
        goto LAB_0012f3e8;
      }
      if (param_3 == 10) {
        bVar2 = local_34 < 3;
        goto LAB_0012f3e8;
      }
    }
    else if (param_3 - 0x10001U < 3) {
      bVar2 = false;
      if ((((iVar1 <= param_4) && ((*(byte *)(param_6 + 0x20) & 4) == 0)) &&
          (bVar2 = false, (*(uint *)(param_6 + 0x28) & 1) == 0)) &&
         (((iVar1 < 0xa1 || ((*(uint *)(param_6 + 0x28) & 2) == 0)) &&
          ((bVar2 = true, 2 < local_34 && (*(int *)(param_6 + 0x2c) != 0x304)))))) {
        bVar2 = (*(uint *)(param_6 + 0x1c) & 0x186) != 0;
      }
      goto LAB_0012f3e8;
    }
    bVar2 = iVar1 <= param_4;
  }
LAB_0012f3e8:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}