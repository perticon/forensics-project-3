void SSL_CTX_free(SSL_CTX *param_1)

{
  int *piVar1;
  int iVar2;
  undefined8 uVar3;
  ssl3_buf_freelist_st *psVar4;
  ssl_session_st *psVar5;
  undefined *puVar6;
  ssl3_buf_freelist_st **ppsVar7;
  ulong uVar8;
  ulong uVar9;
  
  if (param_1 == (SSL_CTX *)0x0) {
    return;
  }
  LOCK();
  piVar1 = (int *)((long)&param_1->app_verify_arg + 4);
  iVar2 = *piVar1;
  *piVar1 = *piVar1 + -1;
  if ((iVar2 != 1) && (0 < iVar2 + -1)) {
    return;
  }
  X509_VERIFY_PARAM_free(*(X509_VERIFY_PARAM **)(param_1->tlsext_tick_key_name + 8));
  CRYPTO_free(*(void **)&param_1[1].read_ahead);
  *(undefined8 *)&param_1[1].read_ahead = 0;
  CRYPTO_free(param_1[1].msg_callback);
  psVar5 = param_1->session_cache_head;
  param_1[1].msg_callback = (_func_3095 *)0x0;
  *(undefined *)&param_1[1].msg_callback_arg = 0;
  if (psVar5 != (ssl_session_st *)0x0) {
    SSL_CTX_flush_sessions(param_1,0);
  }
  CRYPTO_free_ex_data(1,param_1,(CRYPTO_EX_DATA *)&param_1->sha1);
  ppsVar7 = (ssl3_buf_freelist_st **)&param_1[1].freelist_max_len;
  OPENSSL_LH_free(param_1->session_cache_head);
  X509_STORE_free((X509_STORE *)param_1->session_cache_size);
  CTLOG_STORE_free(*(undefined8 *)(param_1->tlsext_tick_hmac_key + 8));
  OPENSSL_sk_free(param_1->cipher_list_by_id);
  OPENSSL_sk_free(param_1->cert_store);
  OPENSSL_sk_free(param_1->sessions);
  ssl_cert_free(*(undefined8 *)param_1->sid_ctx);
  puVar6 = PTR_X509_NAME_free_001a7fc8;
  OPENSSL_sk_pop_free(param_1->max_cert_list,PTR_X509_NAME_free_001a7fc8);
  OPENSSL_sk_pop_free(param_1->cert,puVar6);
  OSSL_STACK_OF_X509_free(param_1->client_CA);
  uVar3 = *(undefined8 *)param_1[1].sid_ctx;
  param_1->options = 0;
  OPENSSL_sk_free(uVar3);
  ssl_ctx_srp_ctx_free_intern(param_1);
  tls_engine_finish((ENGINE *)param_1->tlsext_opaque_prf_input_callback_arg);
  CRYPTO_free(param_1[1].cipher_list_by_id);
  CRYPTO_free(param_1[1].sessions);
  CRYPTO_free((void *)param_1[1].session_cache_size);
  CRYPTO_free((void *)param_1[1].session_timeout);
  CRYPTO_secure_free(param_1->next_protos_advertised_cb,"ssl/ssl_lib.c",0xd90);
  ssl_evp_md_free(param_1->comp_methods);
  ssl_evp_md_free(param_1->info_callback);
  do {
    uVar3 = *ppsVar7;
    ppsVar7 = ppsVar7 + 1;
    ssl_evp_cipher_free(uVar3);
  } while (ppsVar7 != (ssl3_buf_freelist_st **)&param_1[2].stats.sess_miss);
  do {
    psVar4 = *ppsVar7;
    ppsVar7 = ppsVar7 + 1;
    ssl_evp_md_free(psVar4);
  } while (&param_1[2].sha1 != (EVP_MD **)ppsVar7);
  if (*(long *)(param_1[2].sid_ctx + 0x18) != 0) {
    uVar8 = 0;
    do {
      uVar9 = uVar8 + 1;
      CRYPTO_free(*(void **)(*(long *)(param_1[2].sid_ctx + 0x10) + uVar8 * 0x38));
      CRYPTO_free(*(void **)(*(long *)(param_1[2].sid_ctx + 0x10) + 8 + uVar8 * 0x38));
      CRYPTO_free(*(void **)(*(long *)(param_1[2].sid_ctx + 0x10) + 0x10 + uVar8 * 0x38));
      uVar8 = uVar9;
    } while (uVar9 < *(ulong *)(param_1[2].sid_ctx + 0x18));
  }
  CRYPTO_free(*(void **)(param_1[2].sid_ctx + 0x10));
  CRYPTO_free(*(void **)(param_1[2].sid_ctx + 8));
  CRYPTO_THREAD_lock_free(*(undefined8 *)(param_1[1].sid_ctx + 0x10));
  CRYPTO_free(param_1[1].tlsext_ticket_key_cb);
  CRYPTO_free(param_1);
  return;
}