const EVP_MD *ssl_handshake_md(SSL *s)
{
    return ssl_md(s->ctx, ssl_get_algorithm2(s));
}