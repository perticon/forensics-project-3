bool ssl3_take_mac(long param_1)

{
  long lVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  
  lVar1 = *(long *)(*(long *)(param_1 + 8) + 0xc0);
  if (*(int *)(param_1 + 0x38) == 0) {
    uVar3 = *(undefined8 *)(lVar1 + 0x40);
    uVar2 = *(undefined8 *)(lVar1 + 0x48);
  }
  else {
    uVar3 = *(undefined8 *)(lVar1 + 0x30);
    uVar2 = *(undefined8 *)(lVar1 + 0x38);
  }
  lVar1 = (**(code **)(lVar1 + 0x28))(param_1,uVar3,uVar2,param_1 + 0x248);
  *(long *)(param_1 + 0x2c8) = lVar1;
  return lVar1 != 0;
}