undefined8 tls_process_change_cipher_spec(int *param_1,long param_2)

{
  long lVar1;
  int iVar2;
  
  lVar1 = *(long *)(param_2 + 8);
  if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
    if (lVar1 != 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x2ea,"tls_process_change_cipher_spec");
      ossl_statem_fatal(param_1,0x32,0x67,0);
      return 0;
    }
  }
  else if (*param_1 == 0x100) {
    if (lVar1 != 2) goto LAB_0017548a;
  }
  else if (lVar1 != 0) {
LAB_0017548a:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x2e5,"tls_process_change_cipher_spec");
    ossl_statem_fatal(param_1,0x32,0x67,0);
    return 0;
  }
  if (*(long *)(param_1 + 0xb8) == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x2f1,"tls_process_change_cipher_spec");
    ossl_statem_fatal(param_1,10,0x85,0);
    return 0;
  }
  param_1[0x66] = 1;
  iVar2 = ssl3_do_change_cipher_spec(param_1);
  if (iVar2 != 0) {
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) != 0) {
      dtls1_reset_seq_numbers(param_1,1);
      if (*param_1 == 0x100) {
        *(short *)(*(long *)(param_1 + 0x12e) + 0x110) =
             *(short *)(*(long *)(param_1 + 0x12e) + 0x110) + 1;
      }
    }
    return 3;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_lib.c",0x2f7,"tls_process_change_cipher_spec");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}