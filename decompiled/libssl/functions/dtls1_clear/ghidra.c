undefined8 dtls1_clear(int *param_1)

{
  int iVar1;
  long lVar2;
  undefined auVar3 [16];
  undefined auVar4 [16];
  undefined8 uVar5;
  ulong uVar6;
  undefined8 *puVar7;
  byte bVar8;
  
  bVar8 = 0;
  DTLS_RECORD_LAYER_clear(param_1 + 0x316);
  lVar2 = *(long *)(param_1 + 0x12e);
  if (lVar2 != 0) {
    uVar5 = *(undefined8 *)(lVar2 + 0x208);
    auVar3 = *(undefined (*) [16])(lVar2 + 0x118);
    auVar4 = *(undefined (*) [16])(lVar2 + 0x128);
    dtls1_clear_received_buffer(param_1);
    dtls1_clear_sent_buffer(param_1);
    puVar7 = *(undefined8 **)(param_1 + 0x12e);
    *puVar7 = 0;
    puVar7[0x41] = 0;
    uVar6 = (ulong)(((int)puVar7 - (int)(undefined8 *)((ulong)(puVar7 + 1) & 0xfffffffffffffff8)) +
                    0x210U >> 3);
    puVar7 = (undefined8 *)((ulong)(puVar7 + 1) & 0xfffffffffffffff8);
    for (; uVar6 != 0; uVar6 = uVar6 - 1) {
      *puVar7 = 0;
      puVar7 = puVar7 + (ulong)bVar8 * -2 + 1;
    }
    lVar2 = *(long *)(param_1 + 0x12e);
    iVar1 = param_1[0xe];
    *(undefined8 *)(lVar2 + 0x208) = uVar5;
    if (iVar1 != 0) {
      *(undefined8 *)(lVar2 + 0x100) = 0xff;
    }
    uVar6 = SSL_get_options(param_1);
    lVar2 = *(long *)(param_1 + 0x12e);
    if ((uVar6 & 0x1000) != 0) {
      *(undefined (*) [16])(lVar2 + 0x128) = auVar4;
    }
    *(undefined (*) [16])(lVar2 + 0x118) = auVar3;
  }
  uVar5 = ssl3_clear(param_1);
  if ((int)uVar5 != 0) {
    if (**(int **)(param_1 + 2) == 0x1ffff) {
      *param_1 = 0xfefd;
      return 1;
    }
    if ((*(byte *)((long)param_1 + 0x9e9) & 0x80) == 0) {
      *param_1 = **(int **)(param_1 + 2);
      return 1;
    }
    *param_1 = 0x100;
    uVar5 = 1;
    param_1[0x283] = 0x100;
  }
  return uVar5;
}