int SSL_CIPHER_get_digest_nid(const SSL_CIPHER *c)
{
    int i = ssl_cipher_info_lookup(ssl_cipher_table_mac, c->algorithm_mac);

    if (i == -1)
        return NID_undef;
    return ssl_cipher_table_mac[i].nid;
}