int tls_parse_ctos_key_share(long param_1,ushort **param_2)

{
  ushort *puVar1;
  long lVar2;
  short sVar3;
  undefined2 uVar4;
  int iVar5;
  long lVar6;
  ushort *puVar7;
  ushort *puVar8;
  undefined8 uVar9;
  ushort uVar10;
  int iVar11;
  ushort *puVar12;
  long in_FS_OFFSET;
  undefined8 local_50;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*(int *)(param_1 + 0x4d0) != 0) && ((*(byte *)(param_1 + 0xb28) & 2) == 0)) {
LAB_00166760:
    iVar11 = 1;
    goto LAB_001666f7;
  }
  if (*(long *)(param_1 + 0x4b0) != 0) {
    ERR_new();
    iVar11 = 0;
    ERR_set_debug("ssl/statem/extensions_srvr.c",0x248,"tls_parse_ctos_key_share");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    goto LAB_001666f7;
  }
  if (param_2[1] < (ushort *)0x2) {
LAB_001666c1:
    ERR_new();
    uVar9 = 0x24d;
  }
  else {
    puVar1 = *param_2;
    puVar8 = param_2[1] + -1;
    uVar10 = *puVar1 << 8 | *puVar1 >> 8;
    puVar12 = (ushort *)(ulong)uVar10;
    if (puVar8 != puVar12) goto LAB_001666c1;
    puVar7 = puVar1 + 1;
    param_2[1] = (ushort *)0x0;
    *param_2 = (ushort *)((long)puVar7 + (long)puVar8);
    tls1_get_supported_groups(param_1,&local_50,&local_48);
    lVar2 = *(long *)(param_1 + 0xad0);
    if (lVar2 == 0) {
      ERR_new();
      iVar11 = 0;
      ERR_set_debug("ssl/statem/extensions_srvr.c",0x25b,"tls_parse_ctos_key_share");
      ossl_statem_fatal(param_1,0x6d,0xd1,0);
      goto LAB_001666f7;
    }
    if ((*(short *)(param_1 + 0x4ae) != 0) && (puVar8 == (ushort *)0x0)) {
      ERR_new();
      uVar9 = 0x266;
LAB_00166991:
      iVar11 = 0;
      ERR_set_debug("ssl/statem/extensions_srvr.c",uVar9,"tls_parse_ctos_key_share");
      ossl_statem_fatal(param_1,0x2f,0x6c,0);
      goto LAB_001666f7;
    }
    if (puVar12 == (ushort *)0x0) goto LAB_00166760;
    if (3 < uVar10) {
      uVar9 = *(undefined8 *)(param_1 + 0xad8);
      iVar11 = 0;
      uVar10 = puVar1[1] << 8 | puVar1[1] >> 8;
      do {
        puVar8 = (ushort *)(ulong)(ushort)(puVar7[1] << 8 | puVar7[1] >> 8);
        if ((puVar12 + -2 < puVar8) ||
           (puVar12 = (ushort *)((long)(puVar12 + -2) - (long)puVar8), puVar8 == (ushort *)0x0))
        break;
        if (iVar11 == 0) {
          if ((*(short *)(param_1 + 0x4ae) != 0) &&
             ((sVar3 = ssl_group_id_tls13_to_internal(uVar10), *(short *)(param_1 + 0x4ae) != sVar3
              || (puVar12 != (ushort *)0x0)))) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x280,"tls_parse_ctos_key_share");
            ossl_statem_fatal(param_1,0x2f,0x6c,0);
            goto LAB_001666f7;
          }
          iVar5 = check_in_list(param_1,uVar10,uVar9,lVar2,0);
          if (iVar5 == 0) {
            ERR_new();
            uVar9 = 0x286;
            goto LAB_00166991;
          }
          iVar5 = check_in_list(param_1,uVar10,local_50,local_48,1);
          if (iVar5 != 0) {
            *(ushort *)(param_1 + 0x4ae) = uVar10;
            *(uint *)(*(long *)(param_1 + 0x918) + 0x308) = (uint)uVar10;
            uVar4 = ssl_group_id_tls13_to_internal(uVar10);
            lVar6 = ssl_generate_param_group(param_1,uVar4);
            *(long *)(param_1 + 0x4b0) = lVar6;
            if (lVar6 == 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_srvr.c",0x297,"tls_parse_ctos_key_share");
              ossl_statem_fatal(param_1,0x50,0x13a,0);
            }
            else {
              iVar5 = tls13_set_encoded_pub_key(lVar6,puVar7 + 2,puVar8);
              if (0 < iVar5) goto LAB_001668db;
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_srvr.c",0x29f,"tls_parse_ctos_key_share");
              ossl_statem_fatal(param_1,0x2f,0x132,0);
            }
            goto LAB_001666f7;
          }
          iVar11 = 0;
        }
        else {
LAB_001668db:
          iVar11 = 1;
        }
        if (puVar12 == (ushort *)0x0) goto LAB_00166760;
        if (puVar12 == (ushort *)0x1) break;
        puVar7 = (ushort *)((long)(puVar7 + 2) + (long)puVar8);
        uVar10 = *puVar7 << 8 | *puVar7 >> 8;
      } while ((ushort *)0x1 < puVar12 + -1);
    }
    ERR_new();
    uVar9 = 0x26e;
  }
  iVar11 = 0;
  ERR_set_debug("ssl/statem/extensions_srvr.c",uVar9,"tls_parse_ctos_key_share");
  ossl_statem_fatal(param_1,0x32,0x9f,0);
LAB_001666f7:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar11;
}