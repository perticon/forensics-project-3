undefined8 ssl_handshake_hash(long param_1,uchar *param_2,ulong param_3,ulong *param_4)

{
  EVP_MD_CTX *in;
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  EVP_MD_CTX *out;
  undefined8 uVar4;
  
  in = *(EVP_MD_CTX **)(param_1 + 400);
  uVar3 = EVP_MD_CTX_get0_md(in);
  iVar1 = EVP_MD_get_size(uVar3);
  if ((iVar1 < 0) || (param_3 < (ulong)(long)iVar1)) {
    ERR_new();
    uVar4 = 0;
    out = (EVP_MD_CTX *)0x0;
    ERR_set_debug("ssl/ssl_lib.c",0x12c1,"ssl_handshake_hash");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    out = (EVP_MD_CTX *)EVP_MD_CTX_new();
    if (out == (EVP_MD_CTX *)0x0) {
      ERR_new();
      uVar3 = 0x12c7;
    }
    else {
      iVar2 = EVP_MD_CTX_copy_ex(out,in);
      if (iVar2 != 0) {
        iVar2 = EVP_DigestFinal_ex(out,param_2,(uint *)0x0);
        if (0 < iVar2) {
          uVar4 = 1;
          *param_4 = (long)iVar1;
          goto LAB_0013b93c;
        }
      }
      ERR_new();
      uVar3 = 0x12cd;
    }
    uVar4 = 0;
    ERR_set_debug("ssl/ssl_lib.c",uVar3,"ssl_handshake_hash");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
LAB_0013b93c:
  EVP_MD_CTX_free(out);
  return uVar4;
}