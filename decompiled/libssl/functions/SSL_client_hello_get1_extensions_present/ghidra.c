undefined8 SSL_client_hello_get1_extensions_present(long param_1,void **param_2,ulong *param_3)

{
  long lVar1;
  void *ptr;
  long lVar2;
  int *piVar3;
  ulong uVar4;
  
  lVar1 = *(long *)(param_1 + 0xb58);
  if (lVar1 == 0) {
    return 0;
  }
  if ((param_2 != (void **)0x0) && (param_3 != (ulong *)0x0)) {
    if (*(long *)(lVar1 + 0x280) != 0) {
      uVar4 = 0;
      piVar3 = (int *)(*(long *)(lVar1 + 0x288) + 0x10);
      do {
        uVar4 = (uVar4 + 1) - (ulong)(*piVar3 == 0);
        piVar3 = piVar3 + 10;
      } while (piVar3 != (int *)(*(long *)(lVar1 + 0x288) + 0x10 + *(long *)(lVar1 + 0x280) * 0x28))
      ;
      if (uVar4 != 0) {
        ptr = CRYPTO_malloc((int)uVar4 * 4,"ssl/ssl_lib.c",0x1524);
        if (ptr == (void *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/ssl_lib.c",0x1525,"SSL_client_hello_get1_extensions_present");
          ERR_set_error(0x14,0xc0100,0);
          return 0;
        }
        lVar1 = *(long *)(*(long *)(param_1 + 0xb58) + 0x280);
        if (lVar1 != 0) {
          lVar2 = 0;
          piVar3 = (int *)(*(long *)(*(long *)(param_1 + 0xb58) + 0x288) + 0x10);
          do {
            if (*piVar3 != 0) {
              if (uVar4 <= *(ulong *)(piVar3 + 4)) {
                CRYPTO_free(ptr);
                return 0;
              }
              *(int *)((long)ptr + *(ulong *)(piVar3 + 4) * 4) = piVar3[2];
            }
            lVar2 = lVar2 + 1;
            piVar3 = piVar3 + 10;
          } while (lVar2 != lVar1);
        }
        *param_2 = ptr;
        *param_3 = uVar4;
        return 1;
      }
    }
    *param_2 = (void *)0x0;
    *param_3 = 0;
    return 1;
  }
  return 0;
}