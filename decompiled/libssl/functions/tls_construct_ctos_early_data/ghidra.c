undefined8 tls_construct_ctos_early_data(long param_1,undefined8 param_2)

{
  byte *a;
  char *__s2;
  byte *pbVar1;
  int iVar2;
  uint uVar3;
  uint uVar4;
  long lVar5;
  undefined8 uVar6;
  undefined8 *puVar7;
  undefined8 *puVar8;
  ulong uVar9;
  SSL_SESSION *pSVar10;
  code *pcVar11;
  long in_FS_OFFSET;
  bool bVar12;
  byte bVar13;
  undefined8 *local_378;
  undefined *local_370;
  SSL_SESSION *local_368;
  undefined2 local_35a;
  undefined8 local_358 [34];
  undefined local_248 [520];
  long local_40;
  
  bVar13 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_378 = (undefined8 *)0x0;
  local_370 = (undefined *)0x0;
  local_368 = (SSL_SESSION *)0x0;
  if (*(int *)(param_1 + 0x8e8) == 1) {
    uVar6 = ssl_handshake_md();
    pcVar11 = *(code **)(param_1 + 0x9a0);
    if (pcVar11 != (code *)0x0) goto LAB_00161be7;
    if (local_368 == (SSL_SESSION *)0x0) goto LAB_00161db0;
LAB_00161c1c:
    SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x920));
    *(SSL_SESSION **)(param_1 + 0x920) = local_368;
    if (local_368 != (SSL_SESSION *)0x0) {
      CRYPTO_free(*(void **)(param_1 + 0x928));
      lVar5 = CRYPTO_memdup(local_378,local_370,"ssl/statem/extensions_clnt.c",0x33c);
      *(long *)(param_1 + 0x928) = lVar5;
      if (lVar5 == 0) {
        *(undefined8 *)(param_1 + 0x930) = 0;
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x33f,"tls_construct_ctos_early_data");
        uVar6 = 0xc0103;
        goto LAB_00161d49;
      }
      *(undefined **)(param_1 + 0x930) = local_370;
    }
    if (*(int *)(param_1 + 0x84) == 2) {
      iVar2 = *(int *)((*(SSL_SESSION **)(param_1 + 0x918))[1].krb5_client_princ + 0x6c);
      pSVar10 = *(SSL_SESSION **)(param_1 + 0x918);
      if ((iVar2 != 0) ||
         ((local_368 != (SSL_SESSION *)0x0 &&
          (iVar2 = *(int *)(local_368[1].krb5_client_princ + 0x6c), pSVar10 = local_368, iVar2 != 0)
          ))) {
        __s2 = *(char **)(pSVar10[1].krb5_client_princ + 0x48);
        *(int *)(param_1 + 0x1d40) = iVar2;
        if ((__s2 == (char *)0x0) ||
           ((*(char **)(param_1 + 0xa58) != (char *)0x0 &&
            (iVar2 = strcmp(*(char **)(param_1 + 0xa58),__s2), iVar2 == 0)))) {
          pbVar1 = *(byte **)(param_1 + 0xb08);
          if (pbVar1 == (byte *)0x0) {
            if (*(long *)(pSVar10[1].krb5_client_princ + 0x70) != 0) {
              ERR_new();
              uVar6 = 0x359;
LAB_00161f5d:
              ERR_set_debug("ssl/statem/extensions_clnt.c",uVar6,"tls_construct_ctos_early_data");
              uVar6 = 0xde;
              goto LAB_00161d49;
            }
          }
          else if (*(long *)(pSVar10[1].krb5_client_princ + 0x70) != 0) {
            lVar5 = *(long *)(param_1 + 0xb10);
            if (lVar5 < 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_clnt.c",0x366,"tls_construct_ctos_early_data");
              uVar6 = 0xc0103;
              goto LAB_00161d49;
            }
            do {
              if (lVar5 == 0) {
LAB_00161f4c:
                ERR_new();
                uVar6 = 0x371;
                goto LAB_00161f5d;
              }
              uVar9 = (ulong)*pbVar1;
              if (lVar5 - 1U < uVar9) goto LAB_00161f4c;
              a = pbVar1 + 1;
              lVar5 = (lVar5 - 1U) - uVar9;
              pbVar1 = a + uVar9;
            } while ((*(ulong *)(pSVar10[1].krb5_client_princ + 0x78) != uVar9) ||
                    (iVar2 = CRYPTO_memcmp(a,*(void **)(pSVar10[1].krb5_client_princ + 0x70),uVar9),
                    iVar2 != 0));
          }
          iVar2 = WPACKET_put_bytes__(param_2,0x2a,2);
          if ((iVar2 != 0) &&
             ((iVar2 = WPACKET_start_sub_packet_len__(param_2,2), iVar2 != 0 &&
              (iVar2 = WPACKET_close(param_2), iVar2 != 0)))) {
            *(undefined8 *)(param_1 + 0xb30) = 0x100000001;
            uVar6 = 1;
            goto LAB_00161d5a;
          }
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_clnt.c",0x37a,"tls_construct_ctos_early_data");
          uVar6 = 0xc0103;
        }
        else {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_clnt.c",0x352,"tls_construct_ctos_early_data");
          uVar6 = 0xe7;
        }
LAB_00161d49:
        ossl_statem_fatal(param_1,0x50,uVar6,0);
        goto LAB_00161d58;
      }
    }
    *(undefined4 *)(param_1 + 0x1d40) = 0;
    uVar6 = 2;
  }
  else {
    pcVar11 = *(code **)(param_1 + 0x9a0);
    uVar6 = 0;
    if (pcVar11 != (code *)0x0) {
LAB_00161be7:
      iVar2 = (*pcVar11)(param_1,uVar6,&local_378,&local_370,&local_368);
      if (iVar2 != 0) {
        if (local_368 == (SSL_SESSION *)0x0) goto LAB_00161db0;
        if (local_368->ssl_version == 0x304) goto LAB_00161c1c;
      }
      SSL_SESSION_free(local_368);
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x305,"tls_construct_ctos_early_data");
      uVar6 = 0xdb;
      goto LAB_00161d49;
    }
LAB_00161db0:
    pcVar11 = *(code **)(param_1 + 0x988);
    if (pcVar11 == (code *)0x0) goto LAB_00161c1c;
    puVar8 = local_358;
    for (lVar5 = 0x20; lVar5 != 0; lVar5 = lVar5 + -1) {
      *puVar8 = 0;
      puVar8 = puVar8 + (ulong)bVar13 * -2 + 1;
    }
    *(undefined *)puVar8 = 0;
    uVar3 = (*pcVar11)(param_1,0,local_358,0x100,local_248,0x200);
    uVar9 = (ulong)uVar3;
    if (uVar3 < 0x201) {
      if (uVar9 == 0) goto LAB_00161c1c;
      local_35a = 0x113;
      puVar8 = local_358;
      do {
        puVar7 = puVar8;
        uVar4 = *(uint *)puVar7 + 0xfefefeff & ~*(uint *)puVar7;
        uVar3 = uVar4 & 0x80808080;
        puVar8 = (undefined8 *)((long)puVar7 + 4);
      } while (uVar3 == 0);
      bVar12 = (uVar4 & 0x8080) == 0;
      if (bVar12) {
        uVar3 = uVar3 >> 0x10;
      }
      if (bVar12) {
        puVar8 = (undefined8 *)((long)puVar7 + 6);
      }
      local_370 = (undefined *)
                  ((long)puVar8 + ((-3 - (ulong)CARRY1((byte)uVar3,(byte)uVar3)) - (long)local_358))
      ;
      local_378 = local_358;
      lVar5 = SSL_CIPHER_find(param_1,&local_35a);
      if (lVar5 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x326,"tls_construct_ctos_early_data");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
      else {
        local_368 = SSL_SESSION_new();
        if ((((local_368 != (SSL_SESSION *)0x0) &&
             (iVar2 = SSL_SESSION_set1_master_key(local_368,local_248,uVar9), iVar2 != 0)) &&
            (iVar2 = SSL_SESSION_set_cipher(local_368,lVar5), iVar2 != 0)) &&
           (iVar2 = SSL_SESSION_set_protocol_version(local_368,0x304), iVar2 != 0)) {
          OPENSSL_cleanse(local_248,uVar9);
          goto LAB_00161c1c;
        }
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x32f,"tls_construct_ctos_early_data");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        OPENSSL_cleanse(local_248,uVar9);
      }
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x313,"tls_construct_ctos_early_data");
      ossl_statem_fatal(param_1,0x28,0xc0103,0);
    }
LAB_00161d58:
    uVar6 = 0;
  }
LAB_00161d5a:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar6;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}