undefined4 srp_verify_server_param(long param_1)

{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  
  iVar1 = BN_ucmp(*(BIGNUM **)(param_1 + 0xc00),*(BIGNUM **)(param_1 + 0xbf8));
  if (((-1 < iVar1) ||
      (iVar1 = BN_ucmp(*(BIGNUM **)(param_1 + 0xc10),*(BIGNUM **)(param_1 + 0xbf8)), -1 < iVar1)) ||
     (iVar1 = BN_is_zero(*(undefined8 *)(param_1 + 0xc10)), iVar1 != 0)) {
    ERR_new();
    ERR_set_debug("ssl/tls_srp.c",0x188,"srp_verify_server_param");
    ossl_statem_fatal(param_1,0x2f,0x186,0);
    return 0;
  }
  iVar1 = BN_num_bits(*(BIGNUM **)(param_1 + 0xbf8));
  if (*(int *)(param_1 + 0xc40) <= iVar1) {
    if (*(code **)(param_1 + 0xbe0) == (code *)0x0) {
      lVar2 = SRP_check_known_gN_param
                        (*(undefined8 *)(param_1 + 0xc00),*(undefined8 *)(param_1 + 0xbf8));
      if (lVar2 == 0) {
        ERR_new();
        uVar3 = 0x197;
        goto LAB_001540a1;
      }
    }
    else {
      iVar1 = (**(code **)(param_1 + 0xbe0))(param_1,*(undefined8 *)(param_1 + 0xbd0));
      if (iVar1 < 1) {
        ERR_new();
        ERR_set_debug("ssl/tls_srp.c",0x193,"srp_verify_server_param");
        ossl_statem_fatal(param_1,0x47,0xea,0);
        return 0;
      }
    }
    return 1;
  }
  ERR_new();
  uVar3 = 0x18d;
LAB_001540a1:
  ERR_set_debug("ssl/tls_srp.c",uVar3,"srp_verify_server_param");
  ossl_statem_fatal(param_1,0x47,0xf1,0);
  return 0;
}