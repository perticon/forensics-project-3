bool SSL_extension_supported(int param_1)

{
  switch(param_1) {
  case 0:
  case 1:
  case 5:
  case 10:
  case 0xb:
  case 0xc:
  case 0xd:
  case 0xe:
  case 0x10:
  case 0x12:
  case 0x15:
  case 0x16:
  case 0x17:
  case 0x23:
  case 0x29:
  case 0x2a:
  case 0x2b:
  case 0x2c:
  case 0x2d:
  case 0x2f:
  case 0x31:
  case 0x33:
    goto switchD_00164eb9_caseD_0;
  case 2:
  case 3:
  case 4:
  case 6:
  case 7:
  case 8:
  case 9:
  case 0xf:
  case 0x11:
  case 0x13:
  case 0x14:
  case 0x18:
  case 0x19:
  case 0x1a:
  case 0x1b:
  case 0x1c:
  case 0x1d:
  case 0x1e:
  case 0x1f:
  case 0x20:
  case 0x21:
  case 0x22:
  case 0x24:
  case 0x25:
  case 0x26:
  case 0x27:
  case 0x28:
  case 0x2e:
  case 0x30:
  case 0x32:
    return false;
  default:
    if (param_1 != 0x3374) {
      return param_1 == 0xff01;
    }
switchD_00164eb9_caseD_0:
    return true;
  }
}