long SSL_SESSION_set1_alpn_selected(long param_1,long param_2,long param_3)

{
  long lVar1;
  
  CRYPTO_free(*(void **)(param_1 + 0x358));
  if ((param_2 == 0) || (param_3 == 0)) {
    *(undefined8 *)(param_1 + 0x358) = 0;
    lVar1 = 1;
    *(undefined8 *)(param_1 + 0x360) = 0;
  }
  else {
    lVar1 = CRYPTO_memdup(param_2,param_3,"ssl/ssl_sess.c",0x407);
    *(long *)(param_1 + 0x358) = lVar1;
    if (lVar1 != 0) {
      *(long *)(param_1 + 0x360) = param_3;
      return 1;
    }
    *(undefined8 *)(param_1 + 0x360) = 0;
  }
  return lVar1;
}