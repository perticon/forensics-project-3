undefined8 ssl3_output_cert_chain(long param_1,undefined8 param_2,X509 **param_3)

{
  X509 *x509;
  undefined8 *puVar1;
  int iVar2;
  int iVar3;
  int iVar4;
  X509_STORE_CTX *ctx;
  undefined8 uVar5;
  undefined8 uVar6;
  X509 *store;
  
  iVar2 = WPACKET_start_sub_packet_len__(param_2,3);
  if (iVar2 == 0) {
    ERR_new();
    uVar6 = 0x3fd;
  }
  else {
    if ((param_3 != (X509 **)0x0) && (x509 = *param_3, x509 != (X509 *)0x0)) {
      store = param_3[2];
      if (store == (X509 *)0x0) {
        puVar1 = *(undefined8 **)(param_1 + 0x9a8);
        store = (X509 *)puVar1[0x22];
        if ((((*(byte *)(param_1 + 0x9f0) & 8) == 0) && (store == (X509 *)0x0)) &&
           ((store = *(X509 **)(*(long *)(param_1 + 0x898) + 0x1c8), store != (X509 *)0x0 ||
            (store = (X509 *)puVar1[5], store != (X509 *)0x0)))) {
          ctx = (X509_STORE_CTX *)X509_STORE_CTX_new_ex(*puVar1,puVar1[0x88]);
          if (ctx == (X509_STORE_CTX *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x3bc,"ssl_add_cert_chain");
            iVar2 = 0xc0100;
            goto LAB_00175704;
          }
          iVar2 = X509_STORE_CTX_init(ctx,(X509_STORE *)store,x509,(stack_st_X509 *)0x0);
          if (iVar2 == 0) {
            X509_STORE_CTX_free(ctx);
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x3c1,"ssl_add_cert_chain");
            iVar2 = 0x8000b;
            goto LAB_00175704;
          }
          X509_verify_cert(ctx);
          ERR_clear_error();
          uVar6 = X509_STORE_CTX_get0_chain(ctx);
          iVar2 = ssl_security_cert_chain(param_1,uVar6,0,0);
          if (iVar2 != 1) {
            X509_STORE_CTX_free(ctx);
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x3d7,"ssl_add_cert_chain");
            goto LAB_00175704;
          }
          iVar2 = OPENSSL_sk_num(uVar6);
          if (0 < iVar2) {
            iVar3 = 0;
            do {
              uVar5 = OPENSSL_sk_value(uVar6,iVar3);
              iVar4 = ssl_add_cert_to_wpacket(param_1,param_2,uVar5,iVar3);
              if (iVar4 == 0) {
                X509_STORE_CTX_free(ctx);
                return 0;
              }
              iVar3 = iVar3 + 1;
            } while (iVar2 != iVar3);
          }
          X509_STORE_CTX_free(ctx);
          goto LAB_001756b5;
        }
      }
      iVar2 = ssl_security_cert_chain(param_1,store,x509,0);
      if (iVar2 != 1) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_lib.c",1000,"ssl_add_cert_chain");
        goto LAB_00175704;
      }
      iVar3 = ssl_add_cert_to_wpacket(param_1,param_2,x509,0);
      iVar2 = 0;
      while( true ) {
        if (iVar3 == 0) {
          return 0;
        }
        iVar3 = OPENSSL_sk_num(store);
        if (iVar3 <= iVar2) break;
        uVar6 = OPENSSL_sk_value(store,iVar2);
        iVar3 = ssl_add_cert_to_wpacket(param_1,param_2,uVar6,iVar2 + 1);
        iVar2 = iVar2 + 1;
      }
    }
LAB_001756b5:
    iVar2 = WPACKET_close(param_2);
    if (iVar2 != 0) {
      return 1;
    }
    ERR_new();
    uVar6 = 0x405;
  }
  ERR_set_debug("ssl/statem/statem_lib.c",uVar6,"ssl3_output_cert_chain");
  iVar2 = 0xc0103;
LAB_00175704:
  ossl_statem_fatal(param_1,0x50,iVar2,0);
  return 0;
}