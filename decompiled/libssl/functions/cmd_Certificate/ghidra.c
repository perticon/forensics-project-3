bool cmd_Certificate(byte *param_1,char *param_2)

{
  long *plVar1;
  int iVar2;
  long lVar3;
  char *pcVar4;
  bool bVar5;
  
  if (*(SSL_CTX **)(param_1 + 0x18) == (SSL_CTX *)0x0) {
    lVar3 = *(long *)(param_1 + 0x20);
    if (lVar3 == 0) {
      return true;
    }
  }
  else {
    iVar2 = SSL_CTX_use_certificate_chain_file(*(SSL_CTX **)(param_1 + 0x18),param_2);
    lVar3 = *(long *)(param_1 + 0x20);
    if (lVar3 == 0) {
      plVar1 = *(long **)(*(long *)(param_1 + 0x18) + 0x158);
      goto joined_r0x00133683;
    }
  }
  iVar2 = SSL_use_certificate_chain_file(lVar3,param_2);
  plVar1 = *(long **)(*(long *)(param_1 + 0x20) + 0x898);
joined_r0x00133683:
  bVar5 = 0 < iVar2;
  if ((plVar1 != (long *)0x0) && (bVar5)) {
    if ((*param_1 & 0x40) == 0) {
      return true;
    }
    lVar3 = *plVar1 - (long)(plVar1 + 4) >> 3;
    CRYPTO_free(*(void **)(param_1 + lVar3 * 0x6666666666666668 + 0x30));
    pcVar4 = CRYPTO_strdup(param_2,"ssl/ssl_conf.c",0x1bd);
    bVar5 = pcVar4 != (char *)0x0;
    *(char **)(param_1 + lVar3 * 0x6666666666666668 + 0x30) = pcVar4;
  }
  return bVar5;
}