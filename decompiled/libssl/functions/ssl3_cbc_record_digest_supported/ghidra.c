ulong ssl3_cbc_record_digest_supported(void)

{
  int iVar1;
  undefined8 uVar2;
  ulong uVar3;
  
  uVar2 = EVP_MD_CTX_get0_md();
  uVar3 = EVP_MD_get_type(uVar2);
  iVar1 = (int)uVar3;
  if (iVar1 == 0x40) {
    uVar3 = 1;
  }
  else {
    if (0x40 < iVar1) {
      return (ulong)(iVar1 - 0x2a0U & 0xffffff00 | (uint)(iVar1 - 0x2a0U < 4));
    }
    uVar3 = uVar3 & 0xffffffffffffff00 | (ulong)(iVar1 == 4);
  }
  return uVar3;
}