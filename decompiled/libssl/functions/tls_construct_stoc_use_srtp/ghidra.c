undefined8 tls_construct_stoc_use_srtp(long param_1,undefined8 param_2)

{
  int iVar1;
  
  if (*(long *)(param_1 + 0xb98) == 0) {
    return 2;
  }
  iVar1 = WPACKET_put_bytes__(param_2,0xe,2);
  if ((((iVar1 != 0) && (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 != 0)) &&
      (iVar1 = WPACKET_put_bytes__(param_2,2,2), iVar1 != 0)) &&
     (((iVar1 = WPACKET_put_bytes__(param_2,*(undefined8 *)(*(long *)(param_1 + 0xb98) + 8),2),
       iVar1 != 0 && (iVar1 = WPACKET_put_bytes__(param_2,0,1), iVar1 != 0)) &&
      (iVar1 = WPACKET_close(param_2), iVar1 != 0)))) {
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x5e3,"tls_construct_stoc_use_srtp");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}