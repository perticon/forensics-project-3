undefined8 dtls1_process_buffered_records(long param_1)

{
  int iVar1;
  pitem *ppVar2;
  short *psVar3;
  long lVar4;
  undefined8 uVar5;
  long in_FS_OFFSET;
  undefined local_34 [4];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  ppVar2 = pqueue_peek(*(pqueue *)(*(long *)(param_1 + 0x1d10) + 0x30));
  if (ppVar2 != (pitem *)0x0) {
    psVar3 = *(short **)(param_1 + 0x1d10);
    uVar5 = 1;
    if ((psVar3[0x14] != *psVar3) || (*(long *)(param_1 + 0xc98) != 0)) goto LAB_00155d6e;
    while( true ) {
      ppVar2 = pqueue_peek(*(pqueue *)(psVar3 + 0x18));
      if (ppVar2 == (pitem *)0x0) break;
      dtls1_retrieve_buffered_record(param_1,*(long *)(param_1 + 0x1d10) + 0x28);
      lVar4 = dtls1_get_bitmap(param_1,param_1 + 0x12a8,local_34);
      if (lVar4 == 0) {
        ERR_new();
        ERR_set_debug("ssl/record/rec_layer_d1.c",0x10c,"dtls1_process_buffered_records");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        uVar5 = 0;
        goto LAB_00155d6e;
      }
      iVar1 = dtls1_record_replay_check(param_1,lVar4);
      if (iVar1 != 0) {
        iVar1 = dtls1_process_record(param_1,lVar4);
        if (iVar1 == 0) goto LAB_00155d5f;
        iVar1 = dtls1_buffer_record(param_1,*(long *)(param_1 + 0x1d10) + 0x38,param_1 + 0x12f0);
        if (-1 < iVar1) goto LAB_00155dce;
LAB_00155d6b:
        uVar5 = 0;
        goto LAB_00155d6e;
      }
LAB_00155d5f:
      iVar1 = ossl_statem_in_error(param_1);
      if (iVar1 != 0) goto LAB_00155d6b;
      *(undefined8 *)(param_1 + 0x12b0) = 0;
      *(undefined8 *)(param_1 + 0x1cb0) = 0;
LAB_00155dce:
      psVar3 = *(short **)(param_1 + 0x1d10);
    }
  }
  psVar3 = *(short **)(param_1 + 0x1d10);
  uVar5 = 1;
  psVar3[0x1c] = *psVar3;
  psVar3[0x14] = *psVar3 + 1;
LAB_00155d6e:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}