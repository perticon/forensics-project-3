undefined8 dtls_get_message(undefined4 *param_1,int *param_2)

{
  undefined2 uVar1;
  int iVar2;
  long lVar3;
  long lVar4;
  undefined *puVar5;
  int iVar6;
  ushort uVar7;
  undefined8 uVar8;
  ulong uVar9;
  undefined uVar10;
  undefined8 *puVar11;
  long in_FS_OFFSET;
  byte bVar12;
  undefined8 uVar13;
  int local_4c;
  undefined local_48 [8];
  long local_40;
  
  bVar12 = 0;
  lVar3 = *(long *)(param_1 + 0x12e);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar6 = (int)lVar3 + 400;
  *(undefined8 *)(lVar3 + 400) = 0;
  puVar11 = (undefined8 *)(lVar3 + 0x198U & 0xfffffffffffffff8);
  *(undefined8 *)(lVar3 + 0x1e0) = 0;
  uVar9 = (ulong)((iVar6 - (int)puVar11) + 0x58U >> 3);
  for (; uVar9 != 0; uVar9 = uVar9 - 1) {
    *puVar11 = 0;
    puVar11 = puVar11 + 1;
  }
  do {
    uVar13 = 0x173a5e;
    uVar8 = dtls_get_reassembled_message(param_1,&local_4c,local_48);
    if ((int)uVar8 != 0) {
      iVar2 = param_1[0xb6];
      lVar4 = *(long *)(param_1 + 0x22);
      *param_2 = iVar2;
      puVar5 = *(undefined **)(lVar4 + 8);
      if (iVar2 == 0x101) {
        uVar8 = 1;
        if (*(code **)(param_1 + 0x130) != (code *)0x0) {
          (**(code **)(param_1 + 0x130))
                    (0,*param_1,0x14,puVar5,1,param_1,*(undefined8 *)(param_1 + 0x132),uVar13);
          uVar8 = 1;
        }
      }
      else {
        uVar8 = *(undefined8 *)(lVar3 + 0x198);
        uVar10 = *(undefined *)(lVar3 + 400);
        puVar11 = (undefined8 *)(lVar3 + 0x198U & 0xfffffffffffffff8);
        uVar7 = *(ushort *)(lVar3 + 0x198) << 8 | *(ushort *)(lVar3 + 0x198) >> 8;
        *(ushort *)(puVar5 + 2) = uVar7;
        *puVar5 = uVar10;
        uVar10 = (undefined)((ulong)uVar8 >> 0x10);
        puVar5[1] = uVar10;
        puVar5[4] = *(undefined *)(lVar3 + 0x1a1);
        uVar1 = *(undefined2 *)(lVar3 + 0x1a0);
        *(ushort *)(puVar5 + 10) = uVar7;
        puVar5[5] = (char)uVar1;
        *(undefined2 *)(puVar5 + 6) = 0;
        puVar5[8] = 0;
        puVar5[9] = uVar10;
        *(undefined8 *)(lVar3 + 400) = 0;
        *(undefined8 *)(lVar3 + 0x1e0) = 0;
        uVar9 = (ulong)((iVar6 - (int)puVar11) + 0x58U >> 3);
        for (; uVar9 != 0; uVar9 = uVar9 - 1) {
          *puVar11 = 0;
          puVar11 = puVar11 + (ulong)bVar12 * -2 + 1;
        }
        *(short *)(*(long *)(param_1 + 0x12e) + 0x110) =
             *(short *)(*(long *)(param_1 + 0x12e) + 0x110) + 1;
        *(long *)(param_1 + 0x24) = *(long *)(*(long *)(param_1 + 0x22) + 8) + 0xc;
        uVar8 = 1;
      }
      break;
    }
  } while (local_4c + 3U < 2);
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar8;
}