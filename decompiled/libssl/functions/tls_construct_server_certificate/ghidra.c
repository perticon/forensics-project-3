bool tls_construct_server_certificate(long param_1,undefined8 param_2)

{
  int iVar1;
  long lVar2;
  bool bVar3;
  
  lVar2 = *(long *)(param_1 + 0x388);
  if (lVar2 == 0) {
    ERR_new();
    bVar3 = false;
    ERR_set_debug("ssl/statem/statem_srvr.c",0xe11,"tls_construct_server_certificate");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
         (iVar1 = **(int **)(param_1 + 8), 0x303 < iVar1)) && (iVar1 != 0x10000)) &&
       (iVar1 = WPACKET_put_bytes__(param_2,0,1), iVar1 == 0)) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0xe1a,"tls_construct_server_certificate");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return false;
    }
    lVar2 = ssl3_output_cert_chain(param_1,param_2,lVar2);
    bVar3 = lVar2 != 0;
  }
  return bVar3;
}