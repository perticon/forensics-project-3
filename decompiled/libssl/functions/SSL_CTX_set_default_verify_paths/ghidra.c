int SSL_CTX_set_default_verify_paths(SSL_CTX *ctx)

{
  int iVar1;
  
  iVar1 = (*(code *)PTR_X509_STORE_set_default_paths_ex_001a8c08)
                    (ctx->session_cache_size,ctx->method,ctx[1].tlsext_ticket_key_cb);
  return iVar1;
}