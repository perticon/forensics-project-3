serverinfoex_srv_add_cb
          (undefined8 param_1,uint param_2,ulong param_3,long *param_4,ulong *param_5,
          undefined8 param_6,long param_7,undefined4 *param_8)

{
  ushort *puVar1;
  long lVar2;
  int iVar3;
  ulong uVar4;
  undefined8 uVar5;
  long lVar6;
  ulong uVar7;
  long in_FS_OFFSET;
  long local_40;
  ulong local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_40 = 0;
  local_38 = 0;
  if ((((param_3 & 0x1000) == 0) || (param_7 == 0)) &&
     (iVar3 = ssl_get_server_cert_serverinfo(param_1,&local_40,&local_38), iVar3 != 0)) {
    *param_4 = 0;
    *param_5 = 0;
    if ((local_40 == 0) || (uVar4 = local_38, lVar6 = local_40, (long)local_38 < 1)) {
LAB_00140148:
      *param_8 = 0x50;
      uVar5 = 0xffffffff;
      goto LAB_001400bf;
    }
    do {
      if (uVar4 < 8) goto LAB_00140148;
      uVar7 = (ulong)(ushort)(*(ushort *)(lVar6 + 6) << 8 | *(ushort *)(lVar6 + 6) >> 8);
      if (uVar4 - 8 < uVar7) goto LAB_00140148;
      puVar1 = (ushort *)(lVar6 + 4);
      lVar2 = lVar6 + 8;
      uVar4 = (uVar4 - 8) - uVar7;
      lVar6 = lVar2 + uVar7;
      if (param_2 == (ushort)(*puVar1 << 8 | *puVar1 >> 8)) {
        *param_4 = lVar2;
        uVar5 = 1;
        *param_5 = uVar7;
        goto LAB_001400bf;
      }
    } while (uVar4 != 0);
  }
  uVar5 = 0;
LAB_001400bf:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
}