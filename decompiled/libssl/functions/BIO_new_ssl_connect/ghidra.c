BIO * BIO_new_ssl_connect(SSL_CTX *ctx)

{
  BIO_METHOD *type;
  BIO *append;
  BIO *b;
  BIO *pBVar1;
  
  type = BIO_s_connect();
  append = BIO_new(type);
  if (append == (BIO *)0x0) {
    return (BIO *)0x0;
  }
  b = BIO_new_ssl(ctx,1);
  if (b != (BIO *)0x0) {
    pBVar1 = BIO_push(b,append);
    if (pBVar1 != (BIO *)0x0) {
      return pBVar1;
    }
  }
  BIO_free(b);
  BIO_free(append);
  return (BIO *)0x0;
}