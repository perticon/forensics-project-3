static void pqueue_add_freelist(OSSL_PQUEUE *pq, size_t from)
{
    struct pq_elem_st *e = pq->elements;
    size_t i;

#ifndef NDEBUG
    for (i = from; i < pq->hmax; i++)
        e[i].used = 0;
#endif
    e[from].posn = pq->freelist;
    for (i = from + 1; i < pq->hmax; i++)
        e[i].posn = i - 1;
    pq->freelist = pq->hmax - 1;
}