int ssl3_write_bytes(ushort *param_1,int param_2,long param_3,ulong param_4,ulong *param_5)

{
  ushort *puVar1;
  char *pcVar2;
  undefined8 *puVar3;
  int iVar4;
  uint uVar5;
  uint uVar6;
  int iVar7;
  undefined8 uVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  ulong uVar12;
  ulong uVar13;
  ulong uVar14;
  long lVar15;
  ulong uVar16;
  ulong uVar17;
  ulong uVar18;
  long in_FS_OFFSET;
  undefined auVar19 [16];
  undefined8 uVar20;
  ulong local_1a8;
  ulong local_180;
  undefined8 local_178;
  undefined8 *local_170;
  ulong local_168;
  uint local_160;
  undefined local_158 [16];
  undefined local_148 [16];
  undefined local_138 [16];
  undefined local_128 [16];
  undefined local_118 [16];
  undefined local_108 [16];
  undefined local_f8 [16];
  undefined local_e8 [16];
  undefined local_d8 [16];
  undefined local_c8 [16];
  undefined local_b8 [16];
  undefined local_a8 [16];
  undefined local_98 [16];
  undefined local_88 [16];
  undefined local_78 [16];
  undefined local_68 [16];
  undefined8 local_4d;
  undefined local_45;
  ushort local_44;
  undefined2 local_42;
  long local_40;
  
  uVar18 = *(ulong *)(param_1 + 0xe5c);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  *(undefined4 *)(param_1 + 0x14) = 1;
  if (param_4 < uVar18) {
LAB_00158108:
    ERR_new();
    ERR_set_debug("ssl/record/rec_layer_s3.c",0x177,"ssl3_write_bytes");
    uVar20 = 0x10f;
LAB_001580f4:
    ossl_statem_fatal(param_1,0x50,uVar20,0);
  }
  else {
    if (*(long *)(param_1 + 0x664) != 0) {
      if (param_4 < *(long *)(param_1 + 0xe6c) + uVar18) goto LAB_00158108;
      if (*(int *)(param_1 + 0x42) == 4) goto LAB_0015801b;
      *(undefined8 *)(param_1 + 0xe5c) = 0;
LAB_001578da:
      iVar4 = SSL_in_init(param_1);
joined_r0x00157be1:
      if (((iVar4 != 0) && (iVar4 = ossl_statem_get_in_handshake(param_1), iVar4 == 0)) &&
         (*(int *)(param_1 + 0x42) != 6)) {
        iVar4 = (**(code **)(param_1 + 0x18))(param_1);
        if (iVar4 < 0) goto LAB_00157fe7;
        if (iVar4 == 0) goto LAB_00157c1d;
      }
      if (*(long *)(param_1 + 0x664) == 0) {
        if (param_2 != 0x17) goto LAB_00157902;
LAB_00157cc5:
        uVar5 = ssl_get_max_send_fragment(param_1);
        uVar17 = (ulong)uVar5;
        if (((param_4 <= uVar17 * 4 && uVar17 * 4 - param_4 != 0) ||
            (*(long *)(param_1 + 0x434) != 0)) ||
           ((*(long *)(param_1 + 0x260) != 0 ||
            (((*(byte *)((long)param_1 + 0xa9) & 4) != 0 ||
             ((*(byte *)(*(long *)(*(long *)(param_1 + 4) + 0xc0) + 0x60) & 1) == 0))))))
        goto LAB_00157902;
        uVar20 = EVP_CIPHER_CTX_get0_cipher(*(undefined8 *)(param_1 + 0x43c));
        uVar11 = EVP_CIPHER_get_flags(uVar20);
        if ((uVar11 & 0x400000) == 0) goto LAB_00157902;
        if ((uVar5 & 0xfff) == 0) {
          uVar17 = uVar17 - 0x200;
        }
        if ((uVar18 != 0) && (*(long *)(param_1 + 0x654) != 0)) {
          if (uVar18 != param_4) goto LAB_00157d71;
          goto LAB_001580c6;
        }
        ssl3_release_write_buffer(param_1);
        iVar4 = EVP_CIPHER_CTX_ctrl(*(EVP_CIPHER_CTX **)(param_1 + 0x43c),0x1c,(int)uVar17,
                                    (void *)0x0);
        if (uVar17 * 8 < param_4 || uVar17 * 8 - param_4 == 0) {
          lVar15 = (long)iVar4 << 3;
        }
        else {
          lVar15 = (long)iVar4 << 2;
        }
        iVar4 = ssl3_setup_write_buffer(param_1,1,lVar15);
        if (iVar4 == 0) goto LAB_00157c1d;
LAB_00157d71:
        uVar11 = uVar17 * 4;
        uVar9 = param_4 - uVar18;
        if (uVar11 < uVar9 || uVar11 - uVar9 == 0) {
          do {
            if ((*(int *)(param_1 + 0xd2) != 0) &&
               (iVar4 = (**(code **)(*(long *)(param_1 + 4) + 0x78))(param_1), iVar4 < 1))
            goto LAB_00157fe0;
            local_160 = 8;
            uVar13 = uVar17 * 8;
            if (uVar9 < uVar17 * 8) {
              local_160 = 4;
              uVar13 = uVar11;
            }
            local_4d = *(undefined8 *)(param_1 + 0xe80);
            local_42 = 0;
            local_45 = 0x17;
            local_178 = 0;
            local_44 = *param_1 << 8 | *param_1 >> 8;
            local_170 = &local_4d;
            local_168 = uVar13;
            iVar4 = EVP_CIPHER_CTX_ctrl(*(EVP_CIPHER_CTX **)(param_1 + 0x43c),0x19,0x20,&local_178);
            if ((iVar4 < 1) || (*(ulong *)(param_1 + 0x65c) < (ulong)(long)iVar4)) break;
            local_178 = *(undefined8 *)(param_1 + 0x654);
            puVar3 = (undefined8 *)(param_3 + uVar18);
            local_170 = puVar3;
            local_168 = uVar13;
            iVar7 = EVP_CIPHER_CTX_ctrl(*(EVP_CIPHER_CTX **)(param_1 + 0x43c),0x1a,0x20,&local_178);
            if (iVar7 < 1) goto LAB_00157c1d;
            uVar5 = *(byte *)((long)param_1 + 0x1d07) + local_160;
            *(char *)((long)param_1 + 0x1d07) = (char)uVar5;
            if ((uVar5 & 0xff) < local_160) {
              puVar1 = param_1 + 0xe83;
              *(char *)puVar1 = *(char *)puVar1 + '\x01';
              if (*(char *)puVar1 == '\0') {
                pcVar2 = (char *)((long)param_1 + 0x1d05);
                *pcVar2 = *pcVar2 + '\x01';
                if (*pcVar2 == '\0') {
                  puVar1 = param_1 + 0xe82;
                  *(char *)puVar1 = *(char *)puVar1 + '\x01';
                  if (*(char *)puVar1 == '\0') {
                    pcVar2 = (char *)((long)param_1 + 0x1d03);
                    *pcVar2 = *pcVar2 + '\x01';
                    if (*pcVar2 == '\0') {
                      puVar1 = param_1 + 0xe81;
                      *(char *)puVar1 = *(char *)puVar1 + '\x01';
                      if (*(char *)puVar1 == '\0') {
                        pcVar2 = (char *)((long)param_1 + 0x1d01);
                        *pcVar2 = *pcVar2 + '\x01';
                        if (*pcVar2 == '\0') {
                          *(char *)(param_1 + 0xe80) = *(char *)(param_1 + 0xe80) + '\x01';
                        }
                      }
                    }
                  }
                }
              }
            }
            *(long *)(param_1 + 0x664) = (long)iVar4;
            *(undefined8 *)(param_1 + 0x660) = 0;
            *(ulong *)(param_1 + 0xe6c) = uVar13;
            *(undefined8 **)(param_1 + 0xe78) = puVar3;
            *(undefined4 *)(param_1 + 0xe70) = 0x17;
            *(ulong *)(param_1 + 0xe74) = uVar13;
            iVar4 = ssl3_write_pending(param_1,0x17,puVar3);
            if (iVar4 < 1) {
              if ((iVar4 != 0) &&
                 ((*(BIO **)(param_1 + 0xc) == (BIO *)0x0 ||
                  (iVar7 = BIO_test_flags(*(BIO **)(param_1 + 0xc),8), iVar7 == 0)))) {
                ssl3_release_write_buffer(param_1);
              }
              goto LAB_00157fe0;
            }
            if (local_180 == uVar9) {
              ssl3_release_write_buffer(param_1);
              iVar4 = 1;
              *param_5 = uVar18 + local_180;
              goto LAB_00157fe7;
            }
            uVar9 = uVar9 - local_180;
            uVar18 = uVar18 + local_180;
          } while (uVar11 <= uVar9);
        }
        ssl3_release_write_buffer(param_1);
      }
      else {
        iVar4 = ssl3_write_pending(param_1,param_2,param_3 + uVar18,*(undefined8 *)(param_1 + 0xe6c)
                                   ,&local_180);
        if (iVar4 < 1) {
LAB_00157fe0:
          *(ulong *)(param_1 + 0xe5c) = uVar18;
          goto LAB_00157fe7;
        }
        uVar18 = uVar18 + local_180;
        if (param_2 == 0x17) goto LAB_00157cc5;
LAB_00157902:
        if (uVar18 == param_4) {
          if (((*(byte *)(param_1 + 0x4f8) & 0x10) != 0) &&
             ((*(byte *)(*(long *)(*(long *)(param_1 + 4) + 0xc0) + 0x60) & 8) == 0)) {
LAB_001580c6:
            ssl3_release_write_buffer(param_1);
          }
          iVar4 = 1;
          *param_5 = uVar18;
          goto LAB_00157fe7;
        }
      }
      param_4 = param_4 - uVar18;
      uVar5 = ssl_get_max_send_fragment(param_1);
      uVar11 = (ulong)uVar5;
      uVar20 = 0x157920;
      uVar6 = ssl_get_split_send_fragment(param_1);
      uVar17 = (ulong)uVar6;
      local_1a8 = *(ulong *)(param_1 + 0x510);
      if (local_1a8 < 0x21) {
        if ((local_1a8 == 0) || (*(long *)(param_1 + 0x43c) == 0)) {
LAB_00157980:
          local_1a8 = 1;
        }
        else {
          uVar8 = EVP_CIPHER_CTX_get0_cipher();
          uVar20 = 0x15795e;
          uVar9 = EVP_CIPHER_get_flags(uVar8);
          if (((uVar9 & 0x800000) == 0) ||
             ((*(byte *)(*(long *)(*(long *)(param_1 + 4) + 0xc0) + 0x60) & 1) == 0))
          goto LAB_00157980;
        }
        if ((uVar11 != 0 && uVar17 != 0) && (uVar17 <= uVar11)) {
          do {
            if (param_4 == 0) {
              uVar10 = 0;
              uVar13 = 0;
              uVar9 = 1;
LAB_00157b8c:
              *(ulong *)(local_158 + uVar13 * 8) = uVar10;
              auVar19 = local_68;
            }
            else {
              uVar9 = (param_4 - 1) / uVar17 + 1;
              if (local_1a8 <= uVar9) {
                uVar9 = local_1a8;
              }
              uVar10 = param_4 / uVar9;
              uVar13 = param_4 % uVar9;
              if (uVar10 < uVar11) {
                if (uVar13 != 0) {
                  uVar14 = uVar13;
                  if (uVar9 <= uVar13) {
                    uVar14 = uVar9;
                  }
                  uVar16 = 1;
                  if (uVar14 != 0) {
                    uVar16 = uVar14;
                  }
                  if (uVar14 < 4) {
                    uVar12 = 0;
LAB_001583e5:
                    lVar15 = uVar10 + 1;
                    uVar13 = uVar12 + 1;
                    *(long *)(local_158 + uVar12 * 8) = lVar15;
                    if (uVar13 < uVar14) {
                      *(long *)(local_158 + uVar13 * 8) = lVar15;
                      uVar13 = uVar12 + 2;
                      if (uVar13 < uVar14) {
                        *(long *)(local_158 + uVar13 * 8) = lVar15;
                        uVar13 = uVar12 + 3;
                      }
                    }
                  }
                  else {
                    lVar15 = uVar10 + 1;
                    uVar12 = uVar16 >> 1;
                    local_158 = CONCAT88(lVar15,lVar15);
                    uVar13 = _UNK_0018b658;
                    if (((((((uVar12 != 1) &&
                            (local_148 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b668,
                            uVar12 != 2)) &&
                           (local_138 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b678, uVar12 != 3
                           )) && ((local_128 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b688,
                                  uVar12 != 4 &&
                                  (local_118 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b698,
                                  uVar12 != 5)))) &&
                         (local_108 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b6a8, uVar12 != 6))
                        && (((local_f8 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b6b8,
                             uVar12 != 7 &&
                             (local_e8 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b6c8,
                             uVar12 != 8)) &&
                            ((local_d8 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b6d8,
                             uVar12 != 9 &&
                             (((local_c8 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b6e8,
                               uVar12 != 10 &&
                               (local_b8 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b6f8,
                               uVar12 != 0xb)) &&
                              (local_a8 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b638,
                              uVar12 != 0xc)))))))) &&
                       ((local_98 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b648, uVar12 != 0xd
                        && (local_88 = CONCAT88(lVar15,lVar15), uVar13 = _UNK_0018b708,
                           uVar12 == 0xf)))) {
                      local_78 = CONCAT88(lVar15,lVar15);
                      uVar13 = _UNK_0018b628;
                    }
                    uVar12 = uVar16 & 0xfffffffffffffffe;
                    if ((uVar16 & 1) != 0) goto LAB_001583e5;
                  }
                  auVar19 = local_68;
                  if (uVar9 <= uVar13) goto LAB_00157b30;
                }
                uVar14 = 1;
                if (uVar13 + 1 <= uVar9) {
                  uVar14 = uVar9 - uVar13;
                }
                if ((uVar9 - uVar13 != 1) && (uVar13 + 1 <= uVar9)) {
                  lVar15 = uVar13 * 8;
                  auVar19 = CONCAT88(uVar10,uVar10) & (undefined  [16])0xffffffffffffffff;
                  uVar16 = uVar14 >> 1;
                  *(undefined (*) [16])(local_158 + lVar15) = auVar19;
                  if ((((uVar16 != 1) &&
                       (((*(undefined (*) [16])(local_148 + lVar15) = auVar19, uVar16 != 2 &&
                         (*(undefined (*) [16])(local_138 + lVar15) = auVar19, uVar16 != 3)) &&
                        (*(undefined (*) [16])(local_128 + lVar15) = auVar19, uVar16 != 4)))) &&
                      (((*(undefined (*) [16])(local_118 + lVar15) = auVar19, uVar16 != 5 &&
                        (*(undefined (*) [16])(local_108 + lVar15) = auVar19, uVar16 != 6)) &&
                       (*(undefined (*) [16])(local_f8 + lVar15) = auVar19, uVar16 != 7)))) &&
                     (((*(undefined (*) [16])(local_e8 + lVar15) = auVar19, uVar16 != 8 &&
                       (*(undefined (*) [16])(local_d8 + lVar15) = auVar19, uVar16 != 9)) &&
                      ((*(undefined (*) [16])(local_c8 + lVar15) = auVar19, uVar16 != 10 &&
                       ((((*(undefined (*) [16])(local_b8 + lVar15) = auVar19, uVar16 != 0xb &&
                          (*(undefined (*) [16])(local_a8 + lVar15) = auVar19, uVar16 != 0xc)) &&
                         (*(undefined (*) [16])(local_98 + lVar15) = auVar19, uVar16 != 0xd)) &&
                        ((*(undefined (*) [16])(local_88 + lVar15) = auVar19, uVar16 != 0xe &&
                         (*(undefined (*) [16])(local_78 + lVar15) = auVar19, uVar16 != 0xf)))))))))
                     ) {
                    *(undefined (*) [16])(local_68 + lVar15) = auVar19;
                  }
                  uVar13 = uVar13 + (uVar14 & 0xfffffffffffffffe);
                  auVar19 = local_68;
                  if (uVar14 == (uVar14 & 0xfffffffffffffffe)) goto LAB_00157b30;
                }
                goto LAB_00157b8c;
              }
              if (uVar9 == 1) {
                uVar13 = 0;
LAB_00157c41:
                *(ulong *)(local_158 + uVar13 * 8) = uVar11;
                auVar19 = local_68;
              }
              else {
                local_158 = ZEXT1216(CONCAT48(uVar5,uVar11));
                uVar13 = uVar9 >> 1;
                if (((((uVar13 == 1) || (local_148 = local_158, uVar13 == 2)) ||
                     (((local_138 = local_158, uVar13 == 3 ||
                       (((local_128 = local_158, uVar13 == 4 || (local_118 = local_158, uVar13 == 5)
                         ) || (local_108 = local_158, uVar13 == 6)))) ||
                      (((local_f8 = local_158, uVar13 == 7 || (local_e8 = local_158, uVar13 == 8))
                       || (local_d8 = local_158, uVar13 == 9)))))) ||
                    (((local_c8 = local_158, uVar13 == 10 || (local_b8 = local_158, uVar13 == 0xb))
                     || ((local_a8 = local_158, uVar13 == 0xc ||
                         (((local_98 = local_158, uVar13 == 0xd ||
                           (local_88 = local_158, uVar13 == 0xe)) ||
                          (local_78 = local_158, auVar19 = local_158, uVar13 != 0x10)))))))) &&
                   (uVar13 = uVar9 & 0xfffffffffffffffe, auVar19 = local_68, (uVar9 & 1) != 0))
                goto LAB_00157c41;
              }
            }
LAB_00157b30:
            local_68 = auVar19;
            iVar4 = do_ssl3_write(param_1,param_2,param_3 + uVar18,local_158,uVar9,0,&local_180,
                                  uVar20);
            if (iVar4 < 1) goto LAB_00157fe0;
            if (local_180 == param_4) {
              *(undefined4 *)(param_1 + 0xc2) = 0;
              if (((*(byte *)(param_1 + 0x4f8) & 0x10) != 0) &&
                 ((*(byte *)(*(long *)(*(long *)(param_1 + 4) + 0xc0) + 0x60) & 8) == 0)) {
                ssl3_release_write_buffer(param_1);
                param_4 = local_180;
              }
              goto LAB_00157c6e;
            }
            if ((param_2 == 0x17) && ((*(byte *)(param_1 + 0x4f8) & 1) != 0)) goto code_r0x00157c5f;
            param_4 = param_4 - local_180;
            uVar18 = uVar18 + local_180;
          } while( true );
        }
        ERR_new();
        uVar20 = 0x259;
      }
      else {
        ERR_new();
        uVar20 = 0x249;
      }
      ERR_set_debug("ssl/record/rec_layer_s3.c",uVar20,"ssl3_write_bytes");
      uVar20 = 0xc0103;
      goto LAB_001580f4;
    }
    if (*(int *)(param_1 + 0x42) != 4) {
      *(undefined8 *)(param_1 + 0xe5c) = 0;
LAB_00157bb0:
      if ((*(int *)(param_1 + 0x5d2) == -1) && (*(int *)(param_1 + 0x54e) < 1)) goto LAB_001578da;
      ossl_statem_set_in_init(param_1);
      iVar4 = SSL_in_init(param_1);
      goto joined_r0x00157be1;
    }
LAB_0015801b:
    iVar4 = early_data_count_ok(param_1,param_4,0);
    if (iVar4 != 0) {
      *(undefined8 *)(param_1 + 0xe5c) = 0;
      if (*(long *)(param_1 + 0x664) != 0) goto LAB_001578da;
      goto LAB_00157bb0;
    }
  }
LAB_00157c1d:
  iVar4 = -1;
LAB_00157fe7:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
code_r0x00157c5f:
  *(undefined4 *)(param_1 + 0xc2) = 0;
  param_4 = local_180;
LAB_00157c6e:
  iVar4 = 1;
  *param_5 = param_4 + uVar18;
  goto LAB_00157fe7;
}