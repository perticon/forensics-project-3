bool create_synthetic_message_hash
               (long param_1,undefined *param_2,undefined8 param_3,long param_4,undefined8 param_5)

{
  int iVar1;
  long in_FS_OFFSET;
  bool bVar2;
  undefined8 local_90 [2];
  undefined4 local_7c;
  undefined local_78 [72];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_7c = 0;
  local_90[0] = param_3;
  if (param_2 == (undefined *)0x0) {
    local_90[0] = 0;
    iVar1 = ssl3_digest_cached_records(param_1,0);
    if (iVar1 != 0) {
      param_2 = local_78;
      iVar1 = ssl_handshake_hash(param_1,param_2,0x40,local_90);
      if (iVar1 != 0) goto LAB_00177b51;
    }
  }
  else {
LAB_00177b51:
    iVar1 = ssl3_init_finished_mac(param_1);
    if (iVar1 != 0) {
      local_7c = CONCAT31(local_7c._1_3_,0xfe);
      local_7c = local_7c & 0xffffff | (uint)(byte)local_90[0] << 0x18;
      iVar1 = ssl3_finish_mac(param_1,&local_7c,4);
      if ((iVar1 != 0) && (iVar1 = ssl3_finish_mac(param_1,param_2,local_90[0]), iVar1 != 0)) {
        bVar2 = true;
        if (param_4 == 0) goto LAB_00177b5f;
        iVar1 = ssl3_finish_mac(param_1,param_4,param_5);
        if (iVar1 != 0) {
          iVar1 = ssl3_finish_mac(param_1,*(undefined8 *)(*(long *)(param_1 + 0x88) + 8),
                                  *(long *)(param_1 + 0x2d0) + 4);
          bVar2 = iVar1 != 0;
          goto LAB_00177b5f;
        }
      }
    }
  }
  bVar2 = false;
LAB_00177b5f:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return bVar2;
}