ulong ssl_get_split_send_fragment(long param_1)

{
  byte bVar1;
  ulong uVar2;
  uint uVar3;
  ulong uVar4;
  
  uVar2 = *(ulong *)(param_1 + 0xa10);
  if ((((*(long *)(param_1 + 0x918) == 0) ||
       (bVar1 = *(char *)(*(long *)(param_1 + 0x918) + 0x368) - 1, 3 < bVar1)) ||
      (uVar3 = 0x200 << (bVar1 & 0x1f), uVar4 = (ulong)uVar3, uVar2 <= uVar3)) &&
     (uVar4 = *(ulong *)(param_1 + 0xa18) & 0xffffffff, uVar2 <= *(ulong *)(param_1 + 0xa18))) {
    uVar4 = uVar2 & 0xffffffff;
  }
  return uVar4;
}