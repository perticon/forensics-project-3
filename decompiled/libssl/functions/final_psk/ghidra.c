undefined4 final_psk(long param_1,undefined8 param_2,int param_3)

{
  if ((*(int *)(param_1 + 0x38) != 0) && (param_3 != 0)) {
    if (*(long *)(param_1 + 0xb58) == 0) {
      return 1;
    }
    if (*(int *)(*(long *)(*(long *)(param_1 + 0xb58) + 0x288) + 0x2e0) == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions.c",0x6a9,"final_psk");
      ossl_statem_fatal(param_1,0x6d,0x136,0);
      return 0;
    }
  }
  return 1;
}