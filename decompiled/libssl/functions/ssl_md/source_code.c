const EVP_MD *ssl_md(SSL_CTX *ctx, int idx)
{
    idx &= SSL_HANDSHAKE_MAC_MASK;
    if (idx < 0 || idx >= SSL_MD_NUM_IDX)
        return NULL;
    return ctx->ssl_digest_methods[idx];
}