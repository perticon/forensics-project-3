undefined4 ssl_derive(long param_1,long param_2,EVP_PKEY *param_3,int param_4)

{
  int iVar1;
  undefined4 uVar2;
  EVP_PKEY_CTX *ctx;
  uchar *key;
  undefined8 uVar3;
  long in_FS_OFFSET;
  size_t local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = 0;
  if ((param_2 == 0) || (param_3 == (EVP_PKEY *)0x0)) {
    ERR_new();
    uVar2 = 0;
    ERR_set_debug("ssl/s3_lib.c",0x12c6,"ssl_derive");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    goto LAB_0012b859;
  }
  ctx = (EVP_PKEY_CTX *)
        EVP_PKEY_CTX_new_from_pkey
                  (**(undefined8 **)(param_1 + 0x9a8),param_2,
                   (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
  iVar1 = EVP_PKEY_derive_init(ctx);
  if (iVar1 < 1) {
LAB_0012b800:
    ERR_new();
    key = (uchar *)0x0;
    uVar2 = 0;
    ERR_set_debug("ssl/s3_lib.c",0x12cf,"ssl_derive");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    iVar1 = EVP_PKEY_derive_set_peer(ctx,param_3);
    if (iVar1 < 1) goto LAB_0012b800;
    iVar1 = EVP_PKEY_derive(ctx,(uchar *)0x0,&local_38);
    if (iVar1 < 1) goto LAB_0012b800;
    if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
        (iVar1 = **(int **)(param_1 + 8), 0x303 < iVar1)) && (iVar1 != 0x10000)) {
      iVar1 = EVP_PKEY_is_a(param_2,&DAT_00183578);
      if (iVar1 != 0) {
        EVP_PKEY_CTX_set_dh_pad(ctx,1);
      }
    }
    key = (uchar *)CRYPTO_malloc((int)local_38,"ssl/s3_lib.c",0x12d6);
    if (key == (uchar *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/s3_lib.c",0x12d8,"ssl_derive");
      uVar3 = 0xc0100;
LAB_0012b8e4:
      uVar2 = 0;
      ossl_statem_fatal(param_1,0x50,uVar3,0);
    }
    else {
      iVar1 = EVP_PKEY_derive(ctx,key,&local_38);
      if (iVar1 < 1) {
        ERR_new();
        ERR_set_debug("ssl/s3_lib.c",0x12dd,"ssl_derive");
        uVar3 = 0xc0103;
        goto LAB_0012b8e4;
      }
      if (param_4 == 0) {
        *(uchar **)(param_1 + 0x360) = key;
        uVar2 = 1;
        key = (uchar *)0x0;
        *(size_t *)(param_1 + 0x368) = local_38;
      }
      else {
        uVar2 = ssl_gensecret(param_1,key,local_38);
      }
    }
  }
  CRYPTO_clear_free(key,local_38,"ssl/s3_lib.c",0x12ed);
  EVP_PKEY_CTX_free(ctx);
LAB_0012b859:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}