long construct_key_exchange_tbs(long param_1,undefined8 *param_2,void *param_3,size_t param_4)

{
  undefined4 uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  undefined4 *puVar4;
  long lVar5;
  
  lVar5 = param_4 + 0x40;
  puVar4 = (undefined4 *)CRYPTO_malloc((int)lVar5,"ssl/statem/statem_lib.c",0x937);
  if (puVar4 == (undefined4 *)0x0) {
    ERR_new();
    lVar5 = 0;
    ERR_set_debug("ssl/statem/statem_lib.c",0x93a,"construct_key_exchange_tbs");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
  }
  else {
    uVar1 = *(undefined4 *)(param_1 + 0x164);
    uVar2 = *(undefined4 *)(param_1 + 0x168);
    uVar3 = *(undefined4 *)(param_1 + 0x16c);
    *puVar4 = *(undefined4 *)(param_1 + 0x160);
    puVar4[1] = uVar1;
    puVar4[2] = uVar2;
    puVar4[3] = uVar3;
    uVar1 = *(undefined4 *)(param_1 + 0x174);
    uVar2 = *(undefined4 *)(param_1 + 0x178);
    uVar3 = *(undefined4 *)(param_1 + 0x17c);
    puVar4[4] = *(undefined4 *)(param_1 + 0x170);
    puVar4[5] = uVar1;
    puVar4[6] = uVar2;
    puVar4[7] = uVar3;
    uVar1 = *(undefined4 *)(param_1 + 0x144);
    uVar2 = *(undefined4 *)(param_1 + 0x148);
    uVar3 = *(undefined4 *)(param_1 + 0x14c);
    puVar4[8] = *(undefined4 *)(param_1 + 0x140);
    puVar4[9] = uVar1;
    puVar4[10] = uVar2;
    puVar4[0xb] = uVar3;
    uVar1 = *(undefined4 *)(param_1 + 0x154);
    uVar2 = *(undefined4 *)(param_1 + 0x158);
    uVar3 = *(undefined4 *)(param_1 + 0x15c);
    puVar4[0xc] = *(undefined4 *)(param_1 + 0x150);
    puVar4[0xd] = uVar1;
    puVar4[0xe] = uVar2;
    puVar4[0xf] = uVar3;
    memcpy(puVar4 + 0x10,param_3,param_4);
    *param_2 = puVar4;
  }
  return lVar5;
}