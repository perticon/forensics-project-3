undefined8 ssl_set_client_hello_version(int *param_1)

{
  long lVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined auStack24 [4];
  int local_14;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = 0;
  if ((*(long *)(param_1 + 0x90) == 0) || (*(long *)(param_1 + 0xb2) == 0)) {
    uVar2 = ssl_get_min_max_version(param_1,auStack24,&local_14,0);
    if ((int)uVar2 == 0) {
      lVar1 = *(long *)(*(long *)(param_1 + 2) + 0xc0);
      *param_1 = local_14;
      if (((*(byte *)(lVar1 + 0x60) & 8) == 0) && (0x303 < local_14)) {
        local_14 = 0x303;
      }
      param_1[0x283] = local_14;
    }
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}