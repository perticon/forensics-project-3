evp_pkey_st * SSL_get_privatekey(SSL *ssl)

{
  dtls1_state_st *pdVar1;
  
  pdVar1 = ssl[3].d1;
  if (pdVar1 != (dtls1_state_st *)0x0) {
    pdVar1 = *(dtls1_state_st **)(*(long *)pdVar1 + 8);
  }
  return (evp_pkey_st *)pdVar1;
}