void tls1_get_formatlist(long param_1,long *param_2,long *param_3)

{
  if (*(long *)(param_1 + 0xaa8) != 0) {
    *param_2 = *(long *)(param_1 + 0xaa8);
    *param_3 = *(long *)(param_1 + 0xaa0);
    return;
  }
  *param_2 = (long)&ecformats_default;
  *param_3 = (ulong)((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) == 0) + 2;
  return;
}