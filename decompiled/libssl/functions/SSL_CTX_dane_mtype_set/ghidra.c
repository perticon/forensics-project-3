undefined8 SSL_CTX_dane_mtype_set(long param_1,long param_2,byte param_3,undefined param_4)

{
  void *__s;
  byte bVar1;
  uint uVar2;
  long lVar3;
  undefined8 uVar4;
  void *pvVar5;
  void *pvVar6;
  ulong uVar7;
  long lVar8;
  long lVar9;
  
  if ((param_3 == 0) && (param_2 != 0)) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0xdf,"dane_mtype_set");
    ERR_set_error(0x14,0xad,0);
    return 0;
  }
  pvVar5 = *(void **)(param_1 + 0x398);
  if (param_3 < *(byte *)(param_1 + 0x3a8) || param_3 == *(byte *)(param_1 + 0x3a8)) {
    pvVar6 = *(void **)(param_1 + 0x3a0);
LAB_00136cf0:
    if (param_2 == 0) {
      param_4 = 0;
    }
    *(long *)((long)pvVar5 + (ulong)param_3 * 8) = param_2;
    *(undefined *)((long)pvVar6 + (ulong)param_3) = param_4;
    uVar4 = 1;
  }
  else {
    uVar2 = (uint)param_3;
    pvVar5 = CRYPTO_realloc(pvVar5,(uVar2 + 1) * 8,"ssl/ssl_lib.c",0xe8);
    if (pvVar5 == (void *)0x0) {
      ERR_new();
      uVar4 = 0xea;
    }
    else {
      *(void **)(param_1 + 0x398) = pvVar5;
      pvVar6 = CRYPTO_realloc(*(void **)(param_1 + 0x3a0),uVar2 + 1,"ssl/ssl_lib.c",0xef);
      if (pvVar6 != (void *)0x0) {
        *(void **)(param_1 + 0x3a0) = pvVar6;
        bVar1 = *(byte *)(param_1 + 0x3a8);
        if (bVar1 + 1 < uVar2) {
          lVar9 = (long)(int)(bVar1 + 1);
          __s = (void *)((long)pvVar5 + lVar9 * 8);
          uVar7 = (ulong)((uVar2 - bVar1) - 2);
          if (((void *)((long)pvVar6 + (ulong)bVar1 + 1) <
               (void *)((long)pvVar5 + (lVar9 + 1 + uVar7) * 8)) &&
             (lVar3 = lVar9 + 1, __s < (void *)((ulong)bVar1 + 2 + uVar7 + (long)pvVar6))) {
            while( true ) {
              lVar8 = lVar3;
              *(undefined8 *)((long)pvVar5 + lVar9 * 8) = 0;
              *(undefined *)((long)pvVar6 + lVar9) = 0;
              if ((int)uVar2 <= (int)lVar8) break;
              lVar3 = lVar8 + 1;
              lVar9 = lVar8;
            }
          }
          else {
            memset(__s,0,(uVar7 + 1) * 8);
            memset((void *)((long)pvVar6 + lVar9),0,uVar7 + 1);
          }
          pvVar6 = *(void **)(param_1 + 0x3a0);
        }
        *(byte *)(param_1 + 0x3a8) = param_3;
        pvVar5 = *(void **)(param_1 + 0x398);
        goto LAB_00136cf0;
      }
      ERR_new();
      uVar4 = 0xf1;
    }
    ERR_set_debug("ssl/ssl_lib.c",uVar4,"dane_mtype_set");
    ERR_set_error(0x14,0xc0100,0);
    uVar4 = 0xffffffff;
  }
  return uVar4;
}