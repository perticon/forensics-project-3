int SSL_CTX_use_certificate(SSL_CTX *ctx,X509 *x)

{
  int iVar1;
  
  if (x == (X509 *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",0xda,"SSL_CTX_use_certificate");
    ERR_set_error(0x14,0xc0102,0);
    return 0;
  }
  iVar1 = ssl_security_cert(0,ctx,x,0,1);
  if (iVar1 != 1) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",0xe0,"SSL_CTX_use_certificate");
    ERR_set_error(0x14,iVar1,0);
    return 0;
  }
  iVar1 = ssl_set_cert(*(undefined8 *)ctx->sid_ctx,x);
  return iVar1;
}