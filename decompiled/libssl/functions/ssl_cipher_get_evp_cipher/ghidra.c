bool ssl_cipher_get_evp_cipher(undefined8 *param_1,long param_2,long *param_3)

{
  int iVar1;
  long lVar2;
  
  lVar2 = 0;
  do {
    if (*(int *)(param_2 + 0x24) == *(int *)(ssl_cipher_table_cipher + lVar2 * 8)) {
      if (lVar2 == 5) {
        lVar2 = EVP_CIPHER_fetch(*param_1,"NULL",param_1[0x88]);
        *param_3 = lVar2;
        return lVar2 != 0;
      }
      if (param_1[(long)(int)lVar2 + 0x90] != 0) {
        iVar1 = ssl_evp_cipher_up_ref();
        if (iVar1 != 0) {
          *param_3 = param_1[(long)(int)lVar2 + 0x90];
          return true;
        }
      }
      return false;
    }
    lVar2 = lVar2 + 1;
  } while (lVar2 != 0x18);
  *param_3 = 0;
  return true;
}