int ssl_cipher_get_evp_cipher(SSL_CTX *ctx, const SSL_CIPHER *sslc,
                              const EVP_CIPHER **enc)
{
    int i = ssl_cipher_info_lookup(ssl_cipher_table_cipher, sslc->algorithm_enc);

    if (i == -1) {
        *enc = NULL;
    } else {
        if (i == SSL_ENC_NULL_IDX) {
            /*
             * We assume we don't care about this coming from an ENGINE so
             * just do a normal EVP_CIPHER_fetch instead of
             * ssl_evp_cipher_fetch()
             */
            *enc = EVP_CIPHER_fetch(ctx->libctx, "NULL", ctx->propq);
            if (*enc == NULL)
                return 0;
        } else {
            const EVP_CIPHER *cipher = ctx->ssl_cipher_methods[i];

            if (cipher == NULL
                    || !ssl_evp_cipher_up_ref(cipher))
                return 0;
            *enc = ctx->ssl_cipher_methods[i];
        }
    }
    return 1;
}