int ssl_set_cert(long *param_1,X509 *param_2)

{
  int iVar1;
  EVP_PKEY *to;
  long lVar2;
  long in_FS_OFFSET;
  long local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  to = (EVP_PKEY *)X509_get0_pubkey(param_2);
  if (to == (EVP_PKEY *)0x0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0xed,"ssl_set_cert");
    ERR_set_error(0x14,0x10c,0);
  }
  else {
    lVar2 = ssl_cert_lookup_by_pkey(to,&local_38);
    if (lVar2 == 0) {
      ERR_new();
      iVar1 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",0xf2,"ssl_set_cert");
      ERR_set_error(0x14,0xf7,0);
    }
    else if ((local_38 == 3) && (iVar1 = EVP_PKEY_can_sign(to), iVar1 == 0)) {
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0xf7,"ssl_set_cert");
      ERR_set_error(0x14,0x13e,0);
    }
    else {
      if ((EVP_PKEY *)param_1[local_38 * 5 + 5] != (EVP_PKEY *)0x0) {
        EVP_PKEY_copy_parameters(to,(EVP_PKEY *)param_1[local_38 * 5 + 5]);
        ERR_clear_error();
        iVar1 = X509_check_private_key(param_2,(EVP_PKEY *)param_1[local_38 * 5 + 5]);
        if (iVar1 == 0) {
          EVP_PKEY_free((EVP_PKEY *)param_1[local_38 * 5 + 5]);
          param_1[local_38 * 5 + 5] = 0;
          ERR_clear_error();
        }
      }
      iVar1 = 1;
      X509_free((X509 *)param_1[local_38 * 5 + 4]);
      X509_up_ref(param_2);
      param_1[local_38 * 5 + 4] = (long)param_2;
      *param_1 = (long)(param_1 + local_38 * 5 + 4);
    }
  }
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}