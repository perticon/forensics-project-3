int64_t ssl_set_cert(int64_t a1, int64_t a2) {
    int64_t v1 = __readfsqword(40); // 0x3f853
    int64_t result; // 0x3f840
    int64_t v2; // 0x3f840
    if (function_20e60() == 0) {
        // 0x3f958
        function_20230();
        function_207e0();
        function_20680();
        result = 0;
        goto lab_0x3f903;
    } else {
        // 0x3f871
        if (ssl_cert_lookup_by_pkey() == 0) {
            // 0x3f9b0
            function_20230();
            function_207e0();
            function_20680();
            result = 0;
            goto lab_0x3f903;
        } else {
            if (v2 == 3) {
                int64_t v3 = function_1ffd0(); // 0x3f993
                if ((int32_t)v3 == 0) {
                    // 0x3f9f0
                    function_20230();
                    function_207e0();
                    function_20680();
                    result = v3 & 0xffffffff;
                    goto lab_0x3f903;
                } else {
                    goto lab_0x3f896;
                }
            } else {
                goto lab_0x3f896;
            }
        }
    }
  lab_0x3f903:
    // 0x3f903
    if (v1 != __readfsqword(40)) {
        // 0x3fa25
        return function_20ff0();
    }
    // 0x3f917
    return result;
  lab_0x3f896:;
    int64_t v4 = 40 * v2 + a1;
    int64_t * v5 = (int64_t *)(v4 + 40); // 0x3f89a
    if (*v5 != 0) {
        // 0x3f8a4
        function_21630();
        function_21360();
        if ((int32_t)function_20940() == 0) {
            // 0x3f928
            function_20810();
            *v5 = 0;
            function_21360();
        }
    }
    // 0x3f8ce
    function_20690();
    function_20dc0();
    int64_t v6 = v4 + 32; // 0x3f8f6
    *(int64_t *)v6 = a2;
    *(int64_t *)a1 = v6;
    result = 1;
    goto lab_0x3f903;
}