int SSL_write_early_data(SSL *param_1,undefined8 param_2,undefined8 param_3,undefined8 *param_4)

{
  undefined4 uVar1;
  uint uVar2;
  int iVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  undefined auStack56 [8];
  long local_30;
  
  uVar1 = *(undefined4 *)((long)&param_1->s3 + 4);
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  switch(uVar1) {
  case 0:
    if (((param_1->server == 0) && (iVar3 = SSL_in_before(), iVar3 != 0)) &&
       (((*(long *)&param_1[3].sid_ctx_length != 0 &&
         (*(int *)(*(long *)&param_1[3].sid_ctx_length + 0x354) != 0)) ||
        (param_1[3].ex_data.sk != (stack_st_void *)0x0)))) goto switchD_0013b71b_caseD_1;
    ERR_new();
    uVar4 = 0x86e;
    break;
  case 1:
switchD_0013b71b_caseD_1:
    *(undefined4 *)((long)&param_1->s3 + 4) = 2;
    iVar3 = SSL_connect(param_1);
    if (iVar3 < 1) {
      *(undefined4 *)((long)&param_1->s3 + 4) = 1;
      iVar3 = 0;
      goto LAB_0013b7e5;
    }
  case 3:
    uVar2 = *(uint *)&param_1[3].tlsext_debug_arg;
    *(undefined4 *)((long)&param_1->s3 + 4) = 4;
    *(uint *)&param_1[3].tlsext_debug_arg = uVar2 & 0xfffffffe;
    iVar3 = SSL_write_ex(param_1,param_2,param_3,auStack56);
    *(uint *)&param_1[3].tlsext_debug_arg = *(uint *)&param_1[3].tlsext_debug_arg | uVar2 & 1;
    if (iVar3 != 0) {
      *(undefined4 *)((long)&param_1->s3 + 4) = 5;
switchD_0013b71b_caseD_5:
      iVar3 = statem_flush(param_1);
      if (iVar3 != 1) {
        iVar3 = 0;
        goto LAB_0013b7e5;
      }
      *param_4 = param_3;
    }
    *(undefined4 *)((long)&param_1->s3 + 4) = 3;
    goto LAB_0013b7e5;
  default:
    ERR_new();
    uVar4 = 0x8a4;
    break;
  case 5:
    goto switchD_0013b71b_caseD_5;
  case 10:
  case 0xc:
    *(undefined4 *)((long)&param_1->s3 + 4) = 6;
    iVar3 = SSL_write_ex(param_1,param_2,param_3);
    if (iVar3 != 0) {
      BIO_ctrl(param_1->wbio,0xb,0,(void *)0x0);
    }
    *(undefined4 *)((long)&param_1->s3 + 4) = uVar1;
    goto LAB_0013b7e5;
  }
  iVar3 = 0;
  ERR_set_debug("ssl/ssl_lib.c",uVar4,"SSL_write_early_data");
  ERR_set_error(0x14,0xc0101,0);
LAB_0013b7e5:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}