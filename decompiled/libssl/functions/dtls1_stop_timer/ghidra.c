void dtls1_stop_timer(SSL *param_1)

{
  X509_EXTENSIONS *pXVar1;
  X509_EXTENSIONS *pXVar2;
  BIO *bp;
  
  pXVar1 = param_1[1].tlsext_ocsp_exts;
  *(undefined (*) [16])&pXVar1[0xf].stack.sorted = (undefined  [16])0x0;
  pXVar2 = param_1[1].tlsext_ocsp_exts;
  *(undefined4 *)&pXVar1[0xf].stack.data = 0;
  pXVar2[0x10].stack.num = 1000000;
  bp = SSL_get_rbio(param_1);
  BIO_ctrl(bp,0x2d,0,&pXVar2[0xf].stack.sorted);
  dtls1_clear_sent_buffer(param_1);
  return;
}