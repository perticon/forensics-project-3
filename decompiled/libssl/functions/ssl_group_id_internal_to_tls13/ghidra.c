undefined4 ssl_group_id_internal_to_tls13(undefined4 param_1)

{
  undefined4 uVar1;
  ushort uVar2;
  
  uVar2 = (ushort)param_1;
  if (uVar2 == 0x1c) {
    uVar1 = 0x21;
  }
  else {
    if (0x1c < uVar2) {
      uVar1 = 0;
      if (2 < (ushort)(uVar2 - 0x1f)) {
        uVar1 = param_1;
      }
      return uVar1;
    }
    uVar1 = 0x1f;
    if (uVar2 != 0x1a) {
      uVar1 = 0x20;
      if (uVar2 != 0x1b) {
        uVar1 = param_1;
      }
      return uVar1;
    }
  }
  return uVar1;
}