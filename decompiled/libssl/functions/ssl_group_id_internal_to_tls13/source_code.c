uint16_t ssl_group_id_internal_to_tls13(uint16_t curve_id)
{
    switch(curve_id) {
    case OSSL_TLS_GROUP_ID_brainpoolP256r1:
        return OSSL_TLS_GROUP_ID_brainpoolP256r1_tls13;
    case OSSL_TLS_GROUP_ID_brainpoolP384r1:
        return OSSL_TLS_GROUP_ID_brainpoolP384r1_tls13;
    case OSSL_TLS_GROUP_ID_brainpoolP512r1:
        return OSSL_TLS_GROUP_ID_brainpoolP512r1_tls13;
    case OSSL_TLS_GROUP_ID_brainpoolP256r1_tls13:
    case OSSL_TLS_GROUP_ID_brainpoolP384r1_tls13:
    case OSSL_TLS_GROUP_ID_brainpoolP512r1_tls13:
        return 0;
    default:
        return curve_id;
    }
}