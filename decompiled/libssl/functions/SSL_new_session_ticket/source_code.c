int SSL_new_session_ticket(SSL *s)
{
    /* If we are in init because we're sending tickets, okay to send more. */
    if ((SSL_in_init(s) && s->ext.extra_tickets_expected == 0)
            || SSL_IS_FIRST_HANDSHAKE(s) || !s->server
            || !SSL_IS_TLS13(s))
        return 0;
    s->ext.extra_tickets_expected++;
    if (!RECORD_LAYER_write_pending(&s->rlayer) && !SSL_in_init(s))
        ossl_statem_set_in_init(s, 1);
    return 1;
}