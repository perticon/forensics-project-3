int SSL_use_RSAPrivateKey(SSL *ssl,RSA *rsa)

{
  int iVar1;
  EVP_PKEY *pkey;
  
  if (rsa == (RSA *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa_legacy.c",0x17,"SSL_use_RSAPrivateKey");
    ERR_set_error(0x14,0xc0102,0);
    return 0;
  }
  pkey = EVP_PKEY_new();
  if (pkey != (EVP_PKEY *)0x0) {
    RSA_up_ref(rsa);
    iVar1 = EVP_PKEY_assign(pkey,6,rsa);
    if (0 < iVar1) {
      iVar1 = SSL_use_PrivateKey(ssl,pkey);
      EVP_PKEY_free(pkey);
      return iVar1;
    }
    RSA_free(rsa);
    EVP_PKEY_free(pkey);
    return 0;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_rsa_legacy.c",0x1b,"SSL_use_RSAPrivateKey");
  ERR_set_error(0x14,0x80006,0);
  return 0;
}