int ssl3_check_cert_and_algorithm(long param_1)

{
  uint uVar1;
  uint uVar2;
  int iVar3;
  undefined8 uVar4;
  long lVar5;
  long in_FS_OFFSET;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20);
  if ((uVar1 & 0xab) != 0) {
    uVar2 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x1c);
    uVar4 = X509_get0_pubkey(*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2b8));
    lVar5 = ssl_cert_lookup_by_pkey(uVar4,&local_28);
    if ((lVar5 == 0) || ((uVar1 & *(uint *)(lVar5 + 4)) == 0)) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0xde1,"ssl3_check_cert_and_algorithm");
      uVar4 = 0xdd;
LAB_001703b4:
      iVar3 = 0;
      ossl_statem_fatal(param_1,0x28,uVar4,0);
      goto LAB_00170261;
    }
    if ((*(uint *)(lVar5 + 4) & 8) == 0) {
      if (((uVar2 & 0x41) != 0) && (local_28 != 0)) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xded,"ssl3_check_cert_and_algorithm");
        uVar4 = 0xa9;
        goto LAB_001703b4;
      }
      if (((uVar2 & 2) != 0) && (*(long *)(param_1 + 0x4b0) == 0)) {
        ERR_new();
        iVar3 = 0;
        ERR_set_debug("ssl/statem/statem_clnt.c",0xdf3,"ssl3_check_cert_and_algorithm");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        goto LAB_00170261;
      }
    }
    else {
      iVar3 = ssl_check_srvr_ecc_cert_and_alg
                        (*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2b8),param_1);
      if (iVar3 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xde8,"ssl3_check_cert_and_algorithm");
        ossl_statem_fatal(param_1,0x28,0x130,0);
        goto LAB_00170261;
      }
    }
  }
  iVar3 = 1;
LAB_00170261:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}