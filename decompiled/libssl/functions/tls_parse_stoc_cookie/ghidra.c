undefined8 tls_parse_stoc_cookie(long param_1,ushort **param_2)

{
  ushort *puVar1;
  ushort *puVar2;
  ushort uVar3;
  long lVar4;
  
  if ((ushort *)0x1 < param_2[1]) {
    puVar1 = param_2[1] + -1;
    uVar3 = **param_2;
    if (puVar1 == (ushort *)(ulong)(ushort)(uVar3 << 8 | uVar3 >> 8)) {
      puVar2 = *param_2 + 1;
      param_2[1] = (ushort *)0x0;
      *param_2 = (ushort *)((long)puVar2 + (long)puVar1);
      CRYPTO_free(*(void **)(param_1 + 0xb38));
      *(undefined8 *)(param_1 + 0xb38) = 0;
      *(undefined8 *)(param_1 + 0xb40) = 0;
      if (puVar1 == (ushort *)0x0) {
        return 1;
      }
      lVar4 = CRYPTO_memdup(puVar2,puVar1,"include/internal/packet.h",0x1c5);
      *(long *)(param_1 + 0xb38) = lVar4;
      if (lVar4 != 0) {
        *(ushort **)(param_1 + 0xb40) = puVar1;
        return 1;
      }
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x768,"tls_parse_stoc_cookie");
  ossl_statem_fatal(param_1,0x32,0x9f,0);
  return 0;
}