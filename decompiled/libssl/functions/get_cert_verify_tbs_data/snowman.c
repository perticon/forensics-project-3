int32_t get_cert_verify_tbs_data(void** rdi, void** rsi, void*** rdx, void*** rcx, void** r8, void** r9) {
    uint64_t rax7;
    void** rdi8;
    void** rax9;
    int32_t eax10;
    uint32_t eax11;
    uint32_t eax12;
    void** rdi13;
    void** rsi14;
    struct s16* rdx15;
    void** v16;
    struct s16* v17;
    uint64_t rbx18;

    rax7 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 8) + 0xc0) + 96)) & 8 || (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 8)) == 0x10000 || reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 8))) <= reinterpret_cast<signed char>(0x303))) {
        rdi8 = *reinterpret_cast<void***>(rdi + 0x188);
        rax9 = fun_202b0(rdi8, 3, rdi8, 3);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax9 == 0)) {
            fun_20230();
            fun_207e0("ssl/statem/statem_lib.c", "ssl/statem/statem_lib.c");
            ossl_statem_fatal(rdi, rdi);
            eax10 = 0;
            goto addr_73f4c_4;
        } else {
            *rcx = rax9;
            eax10 = 1;
            goto addr_73f4c_4;
        }
    }
    __asm__("movdqa xmm0, [rip+0x19d28]");
    __asm__("movaps [rsi], xmm0");
    __asm__("movaps [rsi+0x10], xmm0");
    __asm__("movaps [rsi+0x20], xmm0");
    __asm__("movaps [rsi+0x30], xmm0");
    if (*reinterpret_cast<uint32_t*>(rdi + 92) - 39 <= 1) {
        __asm__("movdqa xmm3, [rip+0x19a28]");
        __asm__("movdqa xmm4, [rip+0x19a30]");
        eax11 = g8dac0;
        __asm__("movaps [rsi+0x40], xmm3");
        *reinterpret_cast<void***>(rsi + 96) = *reinterpret_cast<void***>(&eax11);
        __asm__("movaps [rsi+0x50], xmm4");
    } else {
        eax12 = g8da80;
        __asm__("movdqa xmm1, [rip+0x19ab8]");
        __asm__("movdqa xmm2, [rip+0x19ac0]");
        *reinterpret_cast<void***>(rsi + 96) = *reinterpret_cast<void***>(&eax12);
        __asm__("movaps [rsi+0x40], xmm1");
        __asm__("movaps [rsi+0x50], xmm2");
    }
    rdi13 = rsi + 98;
    if (*reinterpret_cast<uint32_t*>(rdi + 92) == 39) 
        goto addr_74050_10;
    if (*reinterpret_cast<uint32_t*>(rdi + 92) != 29) 
        goto addr_73fd1_12;
    addr_74050_10:
    rsi14 = rdi + 0x8a0;
    fun_20b20(rdi13, rsi14, rdi13, rsi14);
    rdx15 = *reinterpret_cast<struct s16**>(rdi + 0x8e0);
    goto addr_73ff0_13;
    addr_73fd1_12:
    eax10 = ssl_handshake_hash(rdi, rdi13, 64, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24, r8, r9, v16);
    rdx15 = v17;
    if (!eax10) {
        addr_73f4c_4:
        rbx18 = rax7 ^ g28;
        if (rbx18) {
            fun_20ff0();
        } else {
            return eax10;
        }
    } else {
        addr_73ff0_13:
        *rdx = rsi;
        eax10 = 1;
        *rcx = reinterpret_cast<void**>(&rdx15->f62);
        goto addr_73f4c_4;
    }
}