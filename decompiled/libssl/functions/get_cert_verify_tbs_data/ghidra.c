get_cert_verify_tbs_data(long param_1,undefined8 *param_2,undefined8 *param_3,long *param_4)

{
  int iVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  undefined4 uVar8;
  undefined4 uVar9;
  undefined4 uVar10;
  undefined4 uVar11;
  undefined4 uVar12;
  undefined4 uVar13;
  undefined4 uVar14;
  undefined4 uVar15;
  undefined4 uVar16;
  undefined2 uVar17;
  undefined8 uVar18;
  long lVar19;
  undefined8 uVar20;
  long in_FS_OFFSET;
  long local_38;
  long local_30;
  
  uVar18 = s__ssl_statem_statem_srvr_c_0018dca0._8_8_;
  uVar20 = s__ssl_statem_statem_srvr_c_0018dca0._0_8_;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
      (iVar1 = **(int **)(param_1 + 8), iVar1 != 0x10000)) && (0x303 < iVar1)) {
    *param_2 = s__ssl_statem_statem_srvr_c_0018dca0._0_8_;
    param_2[1] = uVar18;
    param_2[2] = uVar20;
    param_2[3] = uVar18;
    param_2[4] = uVar20;
    param_2[5] = uVar18;
    param_2[6] = uVar20;
    param_2[7] = uVar18;
    uVar17 = servercontext_26484._32_2_;
    uVar16 = servercontext_26484._28_4_;
    uVar15 = servercontext_26484._24_4_;
    uVar14 = servercontext_26484._20_4_;
    uVar13 = servercontext_26484._16_4_;
    uVar12 = servercontext_26484._12_4_;
    uVar11 = servercontext_26484._8_4_;
    uVar10 = servercontext_26484._4_4_;
    uVar9 = clientcontext_26485._28_4_;
    uVar8 = clientcontext_26485._24_4_;
    uVar7 = clientcontext_26485._20_4_;
    uVar6 = clientcontext_26485._16_4_;
    uVar5 = clientcontext_26485._12_4_;
    uVar4 = clientcontext_26485._8_4_;
    uVar3 = clientcontext_26485._4_4_;
    uVar2 = clientcontext_26485._0_4_;
    if (*(int *)(param_1 + 0x5c) - 0x27U < 2) {
      *(undefined4 *)(param_2 + 8) = servercontext_26484._0_4_;
      *(undefined4 *)((long)param_2 + 0x44) = uVar10;
      *(undefined4 *)(param_2 + 9) = uVar11;
      *(undefined4 *)((long)param_2 + 0x4c) = uVar12;
      *(undefined2 *)(param_2 + 0xc) = uVar17;
      *(undefined4 *)(param_2 + 10) = uVar13;
      *(undefined4 *)((long)param_2 + 0x54) = uVar14;
      *(undefined4 *)(param_2 + 0xb) = uVar15;
      *(undefined4 *)((long)param_2 + 0x5c) = uVar16;
    }
    else {
      *(undefined2 *)(param_2 + 0xc) = clientcontext_26485._32_2_;
      *(undefined4 *)(param_2 + 8) = uVar2;
      *(undefined4 *)((long)param_2 + 0x44) = uVar3;
      *(undefined4 *)(param_2 + 9) = uVar4;
      *(undefined4 *)((long)param_2 + 0x4c) = uVar5;
      *(undefined4 *)(param_2 + 10) = uVar6;
      *(undefined4 *)((long)param_2 + 0x54) = uVar7;
      *(undefined4 *)(param_2 + 0xb) = uVar8;
      *(undefined4 *)((long)param_2 + 0x5c) = uVar9;
    }
    if ((*(int *)(param_1 + 0x5c) == 0x27) || (*(int *)(param_1 + 0x5c) == 0x1d)) {
      memcpy((void *)((long)param_2 + 0x62),(void *)(param_1 + 0x8a0),*(size_t *)(param_1 + 0x8e0));
      local_38 = *(long *)(param_1 + 0x8e0);
    }
    else {
      uVar20 = ssl_handshake_hash(uVar20,param_1,(void *)((long)param_2 + 0x62),0x40,&local_38);
      if ((int)uVar20 == 0) goto LAB_00173f4c;
    }
    *param_3 = param_2;
    uVar20 = 1;
    *param_4 = local_38 + 0x62;
  }
  else {
    lVar19 = BIO_ctrl(*(BIO **)(param_1 + 0x188),3,0,param_3);
    if (lVar19 < 1) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x108,"get_cert_verify_tbs_data");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      uVar20 = 0;
    }
    else {
      *param_4 = lVar19;
      uVar20 = 1;
    }
  }
LAB_00173f4c:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar20;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}