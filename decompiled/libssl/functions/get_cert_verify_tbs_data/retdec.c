int64_t get_cert_verify_tbs_data(int64_t a1, int64_t * a2, int64_t * a3, int64_t * a4) {
    int64_t v1 = __readfsqword(40); // 0x73ef3
    int64_t v2 = *(int64_t *)(a1 + 8); // 0x73f03
    int64_t result; // 0x73ee0
    int64_t v3; // 0x73ee0
    if ((*(char *)(*(int64_t *)(v2 + 192) + 96) & 8) != 0) {
        goto lab_0x73f24;
    } else {
        int32_t v4 = *(int32_t *)v2; // 0x73f14
        if (v4 != (int32_t)&g2 == v4 > (int32_t)&g90) {
            // 0x73f70
            v3 = (int64_t)a2;
            int128_t v5 = __asm_movdqa(0x20202020202020202020202020202020); // 0x73f70
            *(int128_t *)a2 = (int128_t)__asm_movaps(v5);
            *(int128_t *)(v3 + 16) = (int128_t)__asm_movaps(v5);
            *(int128_t *)(v3 + 32) = (int128_t)__asm_movaps(v5);
            *(int128_t *)(v3 + 48) = (int128_t)__asm_movaps(v5);
            int32_t * v6 = (int32_t *)(a1 + 92); // 0x73f8a
            if (*v6 < 41) {
                int128_t v7 = __asm_movdqa(0x20726576726573202c332e3120534c54); // 0x74070
                int128_t v8 = __asm_movdqa(0x66697265566574616369666974726543); // 0x74078
                *(int128_t *)(v3 + 64) = (int128_t)__asm_movaps(v7);
                *(int16_t *)(v3 + 96) = 121;
                *(int128_t *)(v3 + 80) = (int128_t)__asm_movaps(v8);
            } else {
                int128_t v9 = __asm_movdqa(0x20746e65696c63202c332e3120534c54); // 0x73fa0
                int128_t v10 = __asm_movdqa(0x66697265566574616369666974726543); // 0x73fa8
                *(int16_t *)(v3 + 96) = 121;
                *(int128_t *)(v3 + 64) = (int128_t)__asm_movaps(v9);
                *(int128_t *)(v3 + 80) = (int128_t)__asm_movaps(v10);
            }
            // 0x73fbc
            switch (*v6) {
                case 39: {
                    goto lab_0x74050;
                }
                case 29: {
                    goto lab_0x74050;
                }
                default: {
                    int64_t v11 = ssl_handshake_hash(); // 0x73fdf
                    result = v11;
                    if ((int32_t)v11 == 0) {
                        goto lab_0x73f4c;
                    } else {
                        goto lab_0x73ff0;
                    }
                }
            }
        } else {
            goto lab_0x73f24;
        }
    }
  lab_0x73f24:;
    int64_t v12 = function_202b0(); // 0x73f35
    if (v12 < 1) {
        // 0x74010
        function_20230();
        function_207e0();
        ossl_statem_fatal();
        result = 0;
    } else {
        // 0x73f43
        *a4 = v12;
        result = 1;
    }
    goto lab_0x73f4c;
  lab_0x73f4c:
    // 0x73f4c
    if (v1 != __readfsqword(40)) {
        // 0x74098
        return function_20ff0();
    }
    // 0x73f60
    return result;
  lab_0x74050:
    // 0x74050
    function_20b20();
    int64_t v13 = *(int64_t *)(a1 + (int64_t)&g336); // 0x7406a
    goto lab_0x73ff0;
  lab_0x73ff0:
    // 0x73ff0
    *a3 = v3;
    *a4 = v13 + 98;
    result = 1;
    goto lab_0x73f4c;
}