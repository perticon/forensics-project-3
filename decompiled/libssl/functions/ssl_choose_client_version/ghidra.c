int ssl_choose_client_version(int *param_1,int param_2,undefined8 param_3)

{
  int iVar1;
  bool bVar2;
  int iVar3;
  int iVar4;
  uint uVar5;
  int iVar6;
  undefined8 uVar7;
  int iVar8;
  undefined1 *puVar9;
  long in_FS_OFFSET;
  int local_3c;
  int local_38;
  int local_34;
  long local_30;
  
  iVar4 = *param_1;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  *param_1 = param_2;
  iVar3 = tls_parse_extension(param_1,0x11,0x300,param_3,0,0);
  if (iVar3 == 0) {
    *param_1 = iVar4;
    iVar3 = 0;
    goto LAB_00177746;
  }
  if ((param_1[0x23a] == 0) || (*param_1 == 0x304)) {
    iVar1 = **(int **)(param_1 + 2);
    if (iVar1 == 0x10000) {
      puVar9 = tls_version_table;
    }
    else {
      if (iVar1 != 0x1ffff) {
        iVar3 = 1;
        if (iVar1 == *param_1) goto LAB_00177746;
        *param_1 = iVar4;
        ERR_new();
        uVar7 = 0x784;
        goto LAB_00177647;
      }
      puVar9 = dtls_version_table;
    }
    iVar3 = ssl_get_min_max_version(param_1,&local_3c,&local_38);
    if (iVar3 == 0) {
      iVar1 = *param_1;
      uVar5 = *(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8;
      if (uVar5 == 0) {
        if (iVar1 < local_3c) goto LAB_00177860;
        bVar2 = local_38 < iVar1;
LAB_001776d1:
        if (bVar2) {
          *param_1 = iVar4;
          ERR_new();
          uVar7 = 0x7a5;
        }
        else {
          if ((*(byte *)(param_1 + 0x27c) & 0x80) == 0) {
            local_34 = local_38;
          }
          if (iVar1 == 0x303) {
            if ((0x303 < local_34) && (*(long *)(param_1 + 0x56) == tls12downgrade)) {
              *param_1 = iVar4;
              ERR_new();
              uVar7 = 0x7b3;
              goto LAB_001778d5;
            }
          }
          else if ((((iVar1 < 0x303) && (uVar5 == 0)) && (iVar1 < local_34)) &&
                  (*(long *)(param_1 + 0x56) == tls11downgrade)) {
            *param_1 = iVar4;
            ERR_new();
            uVar7 = 0x7bf;
LAB_001778d5:
            ERR_set_debug("ssl/statem/statem_lib.c",uVar7,"ssl_choose_client_version");
            ossl_statem_fatal(param_1,0x2f,0x175,0);
            goto LAB_00177746;
          }
          while( true ) {
            if (*(int *)puVar9 == 0) break;
            if ((*(code **)((long)puVar9 + 8) != (code *)0x0) && (iVar1 == *(int *)puVar9)) {
              uVar7 = (**(code **)((long)puVar9 + 8))();
              iVar3 = 1;
              *(undefined8 *)(param_1 + 2) = uVar7;
              goto LAB_00177746;
            }
            puVar9 = (undefined1 *)((long)puVar9 + 0x18);
          }
          *param_1 = iVar4;
          ERR_new();
          uVar7 = 0x7ce;
        }
      }
      else {
        if (iVar1 == 0x100) {
          if ((local_3c == 0x100) || (0xfeff < local_3c)) {
            iVar8 = 0xff00;
            goto LAB_001777a3;
          }
        }
        else {
          if (local_3c == 0x100) {
            local_3c = 0xff00;
          }
          iVar8 = iVar1;
          if (iVar1 <= local_3c) {
LAB_001777a3:
            iVar6 = 0xff00;
            if (local_38 != 0x100) {
              iVar6 = local_38;
            }
            bVar2 = iVar8 < iVar6;
            goto LAB_001776d1;
          }
        }
LAB_00177860:
        *param_1 = iVar4;
        ERR_new();
        uVar7 = 0x7a0;
      }
      ERR_set_debug("ssl/statem/statem_lib.c",uVar7,"ssl_choose_client_version");
      ossl_statem_fatal(param_1,0x46,0x102,0);
      goto LAB_00177746;
    }
    *param_1 = iVar4;
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x79a,"ssl_choose_client_version");
    iVar4 = iVar3;
  }
  else {
    *param_1 = iVar4;
    ERR_new();
    uVar7 = 0x77c;
LAB_00177647:
    ERR_set_debug("ssl/statem/statem_lib.c",uVar7,"ssl_choose_client_version");
    iVar4 = 0x10a;
  }
  iVar3 = 0;
  ossl_statem_fatal(param_1,0x46,iVar4,0);
LAB_00177746:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}