int SSL_CIPHER_is_aead(const SSL_CIPHER *c)
{
    return (c->algorithm_mac & SSL_AEAD) ? 1 : 0;
}