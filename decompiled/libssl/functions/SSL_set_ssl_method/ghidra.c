int SSL_set_ssl_method(SSL *s,SSL_METHOD *method)

{
  SSL_METHOD *pSVar1;
  _func_3058 *p_Var2;
  _func_3058 *p_Var3;
  int iVar4;
  
  iVar4 = 1;
  pSVar1 = s->method;
  if (pSVar1 != method) {
    p_Var2 = (_func_3058 *)s->handshake_func;
    if (pSVar1->version == method->version) {
      s->method = method;
      p_Var3 = pSVar1->ssl_read;
    }
    else {
      (*pSVar1->ssl_accept)(s);
      s->method = method;
      iVar4 = (*method->ssl_clear)(s);
      p_Var3 = pSVar1->ssl_read;
    }
    if (p_Var3 == p_Var2) {
      s->handshake_func = (_func_3149 *)method->ssl_read;
      return iVar4;
    }
    if ((_func_3058 *)pSVar1->ssl_connect == p_Var2) {
      s->handshake_func = (_func_3149 *)method->ssl_connect;
      return iVar4;
    }
  }
  return iVar4;
}