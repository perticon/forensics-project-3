X509 * SSL_get_certificate(SSL *ssl)

{
  dtls1_state_st *pdVar1;
  
  pdVar1 = ssl[3].d1;
  if (pdVar1 != (dtls1_state_st *)0x0) {
    pdVar1 = **(dtls1_state_st ***)pdVar1;
  }
  return (X509 *)pdVar1;
}