char tls_process_certificate_request(long param_1,ushort **param_2)

{
  byte *pbVar1;
  ushort uVar2;
  int iVar3;
  byte *pbVar4;
  ushort *puVar5;
  long lVar6;
  ushort *puVar7;
  undefined8 uVar8;
  char cVar9;
  byte *pbVar10;
  long in_FS_OFFSET;
  void *local_50;
  ushort *local_48;
  ushort *local_40;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  *(undefined (*) [16])(param_1 + 0x3b8) = (undefined  [16])0x0;
  *(undefined4 *)(param_1 + 0x3d8) = 0;
  *(undefined (*) [16])(param_1 + 0x3c8) = (undefined  [16])0x0;
  if ((*(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
    cVar9 = '\0';
    iVar3 = **(int **)(param_1 + 8);
    if ((iVar3 == 0x10000) || (iVar3 < 0x304)) goto LAB_0016e25c;
    local_50 = (void *)0x0;
    if ((*(byte *)(param_1 + 0x44) & 1) != 0) {
      cVar9 = '\x01';
      goto LAB_0016e3e6;
    }
    CRYPTO_free(*(void **)(param_1 + 0x2f8));
    *(undefined8 *)(param_1 + 0x2f8) = 0;
    *(undefined8 *)(param_1 + 0x300) = 0;
    CRYPTO_free(*(void **)(param_1 + 0xbb0));
    puVar7 = param_2[1];
    *(undefined8 *)(param_1 + 0xbb0) = 0;
    *(undefined8 *)(param_1 + 3000) = 0;
    if (puVar7 == (ushort *)0x0) {
LAB_0016e640:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x93b,"tls_process_certificate_request");
      ossl_statem_fatal(param_1,0x32,0x9f,0);
      goto LAB_0016e3e6;
    }
    pbVar4 = (byte *)((long)puVar7 + -1);
    pbVar10 = (byte *)(ulong)*(byte *)*param_2;
    if (pbVar4 < pbVar10) goto LAB_0016e640;
    pbVar1 = (byte *)((long)*param_2 + 1);
    param_2[1] = (ushort *)(pbVar4 + -(long)pbVar10);
    *param_2 = (ushort *)(pbVar1 + (long)pbVar10);
    CRYPTO_free(*(void **)(param_1 + 0xbb0));
    *(undefined8 *)(param_1 + 0xbb0) = 0;
    *(undefined8 *)(param_1 + 3000) = 0;
    if (pbVar10 != (byte *)0x0) {
      lVar6 = CRYPTO_memdup(pbVar1,pbVar10,"include/internal/packet.h",0x1c5);
      *(long *)(param_1 + 0xbb0) = lVar6;
      if (lVar6 == 0) goto LAB_0016e640;
      *(byte **)(param_1 + 3000) = pbVar10;
    }
    if (param_2[1] < (ushort *)0x2) {
LAB_0016e762:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x940,"tls_process_certificate_request");
      ossl_statem_fatal(param_1,0x32,0x10f,0);
      goto LAB_0016e3e6;
    }
    puVar7 = param_2[1] + -1;
    uVar2 = **param_2;
    puVar5 = (ushort *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8);
    if (puVar7 < puVar5) goto LAB_0016e762;
    local_48 = *param_2 + 1;
    param_2[1] = (ushort *)((long)puVar7 - (long)puVar5);
    *param_2 = (ushort *)((long)local_48 + (long)puVar5);
    local_40 = puVar5;
    iVar3 = tls_collect_extensions(param_1,&local_48,0x4000,&local_50,0,1);
    if (iVar3 == 0) {
LAB_0016e79a:
      CRYPTO_free(local_50);
      goto LAB_0016e3e6;
    }
    iVar3 = tls_parse_all_extensions(param_1,0x4000,local_50,0,0,1);
    if (iVar3 == 0) goto LAB_0016e79a;
    CRYPTO_free(local_50);
    iVar3 = tls1_process_sigalgs(param_1);
    if (iVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x94e,"tls_process_certificate_request");
      ossl_statem_fatal(param_1,0x50,0x10f,0);
      goto LAB_0016e3e6;
    }
LAB_0016e356:
    if (param_2[1] == (ushort *)0x0) {
      cVar9 = '\x02';
      *(undefined4 *)(param_1 + 0x2f0) = 1;
      if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
        iVar3 = **(int **)(param_1 + 8);
        if ((iVar3 == 0x10000) || (iVar3 < 0x304)) {
          cVar9 = '\x02';
        }
        else {
          cVar9 = (*(int *)(param_1 + 0xba8) != 4) + '\x02';
        }
      }
      goto LAB_0016e3e6;
    }
    ERR_new();
    uVar8 = 0x97e;
LAB_0016e3c1:
    ERR_set_debug("ssl/statem/statem_clnt.c",uVar8,"tls_process_certificate_request");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
  }
  else {
LAB_0016e25c:
    if (param_2[1] == (ushort *)0x0) {
LAB_0016e3b0:
      ERR_new(0);
      uVar8 = 0x956;
      goto LAB_0016e3c1;
    }
    pbVar4 = (byte *)((long)param_2[1] + -1);
    pbVar10 = (byte *)(ulong)*(byte *)*param_2;
    if (pbVar4 < pbVar10) goto LAB_0016e3b0;
    pbVar1 = (byte *)((long)*param_2 + 1);
    param_2[1] = (ushort *)(pbVar4 + -(long)pbVar10);
    *param_2 = (ushort *)(pbVar1 + (long)pbVar10);
    CRYPTO_free(*(void **)(param_1 + 0x2f8));
    *(undefined8 *)(param_1 + 0x2f8) = 0;
    *(undefined8 *)(param_1 + 0x300) = 0;
    if (pbVar10 == (byte *)0x0) {
LAB_0016e2c7:
      if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 2) != 0) {
        if ((ushort *)0x1 < param_2[1]) {
          puVar7 = param_2[1] + -1;
          uVar2 = **param_2;
          puVar5 = (ushort *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8);
          if (puVar5 <= puVar7) {
            local_48 = *param_2 + 1;
            param_2[1] = (ushort *)((long)puVar7 - (long)puVar5);
            *param_2 = (ushort *)((long)local_48 + (long)puVar5);
            local_40 = puVar5;
            iVar3 = tls1_save_sigalgs(param_1,&local_48,0);
            if (iVar3 == 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x96c,"tls_process_certificate_request");
              ossl_statem_fatal(param_1,0x50,0x168,0);
            }
            else {
              iVar3 = tls1_process_sigalgs(param_1);
              if (iVar3 != 0) goto LAB_0016e343;
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x971,"tls_process_certificate_request");
              ossl_statem_fatal(param_1,0x50,0xc0100,0);
            }
            goto LAB_0016e3e3;
          }
        }
        ERR_new();
        uVar8 = 0x963;
        goto LAB_0016e3c1;
      }
LAB_0016e343:
      iVar3 = parse_ca_names(param_1,param_2);
      if (iVar3 != 0) goto LAB_0016e356;
    }
    else {
      lVar6 = CRYPTO_memdup(pbVar1,pbVar10,"include/internal/packet.h",0x1c5);
      *(long *)(param_1 + 0x2f8) = lVar6;
      if (lVar6 != 0) {
        *(byte **)(param_1 + 0x300) = pbVar10;
        goto LAB_0016e2c7;
      }
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x95b,"tls_process_certificate_request");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
  }
LAB_0016e3e3:
  cVar9 = '\0';
LAB_0016e3e6:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return cVar9;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}