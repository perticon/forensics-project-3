bool ssl_do_config(ulong param_1,undefined8 *param_2,char *param_3,uint param_4)

{
  char cVar1;
  int iVar2;
  undefined4 uVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  ulong uVar6;
  long lVar7;
  bool bVar8;
  char cVar9;
  ulong uVar10;
  long in_FS_OFFSET;
  char *local_70 [2];
  undefined8 local_60;
  ulong local_58;
  undefined8 local_50;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar6 = param_1 | (ulong)param_2;
  local_70[0] = param_3;
  if (uVar6 == 0) {
    ERR_new();
    uVar5 = 0;
    bVar8 = false;
    ERR_set_debug("ssl/ssl_mcnf.c",0x23,"ssl_do_config");
    ERR_set_error(0x14,0xc0102,0);
    goto LAB_0013f4fb;
  }
  if ((param_3 == (char *)0x0) && ((param_4 & 1) != 0)) {
    local_70[0] = "system_default";
    iVar2 = conf_ssl_name_find("system_default",&local_60);
    if (iVar2 != 0) {
LAB_0013f590:
      uVar4 = conf_ssl_get(local_60,local_70,&local_58);
      uVar6 = SSL_CONF_CTX_new();
      if (uVar6 != 0) {
        if (param_1 == 0) {
          lVar7 = param_2[1];
          SSL_CONF_CTX_set_ssl_ctx(uVar6,param_2);
          uVar5 = *param_2;
        }
        else {
          lVar7 = *(long *)(param_1 + 8);
          SSL_CONF_CTX_set_ssl(uVar6,param_1);
          uVar5 = **(undefined8 **)(param_1 + 0x9a8);
        }
        cVar9 = (-(param_4 == 0) & 0x60U) + 0xe;
        cVar1 = (-(param_4 == 0) & 0x60U) + 10;
        if (*(code **)(lVar7 + 0x28) == ssl_undefined_function) {
          cVar9 = (-(param_4 == 0) & 0x60U) + 6;
          cVar1 = (-(param_4 == 0) & 0x60U) + 2;
        }
        if (*(code **)(lVar7 + 0x30) == ssl_undefined_function) {
          cVar9 = cVar1;
        }
        uVar10 = 0;
        SSL_CONF_CTX_set_flags(uVar6,cVar9);
        uVar5 = OSSL_LIB_CTX_set0_default(uVar5);
        if (local_58 != 0) {
          do {
            conf_ssl_get_cmd(uVar4,uVar10,&local_50,&local_48);
            iVar2 = SSL_CONF_cmd(uVar6,local_50,local_48);
            if (iVar2 < 1) {
              uVar3 = 0x180;
              if (iVar2 == -2) {
                uVar3 = 0x8b;
              }
              ERR_new();
              ERR_set_debug("ssl/ssl_mcnf.c",0x4d,"ssl_do_config");
              bVar8 = false;
              ERR_set_error(0x14,uVar3,"section=%s, cmd=%s, arg=%s",local_70[0],local_50,local_48);
              goto LAB_0013f4fb;
            }
            uVar10 = uVar10 + 1;
          } while (uVar10 < local_58);
        }
        iVar2 = SSL_CONF_CTX_finish(uVar6);
        bVar8 = 0 < iVar2;
        goto LAB_0013f4fb;
      }
    }
  }
  else {
    iVar2 = conf_ssl_name_find(param_3,&local_60);
    if (iVar2 != 0) goto LAB_0013f590;
    if (param_4 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_mcnf.c",0x2b,"ssl_do_config");
      ERR_set_error(0x14,0x71,"name=%s",local_70[0]);
    }
  }
  bVar8 = false;
  uVar5 = 0;
  uVar6 = 0;
LAB_0013f4fb:
  OSSL_LIB_CTX_set0_default(uVar5);
  SSL_CONF_CTX_free(uVar6);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar8;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}