int64_t ssl_do_config(int64_t a1, int32_t a2, int64_t a3, uint32_t a4) {
    // 0x3f490
    int64_t v1; // 0x3f490
    uint64_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x3f4a6
    int64_t result; // 0x3f490
    if (((int64_t)a2 || a1) == 0) {
        // 0x3f718
        function_20230();
        function_207e0();
        function_20680();
        result = 0;
        goto lab_0x3f4fb;
    } else {
        int64_t v4 = function_20c20();
        if (a3 != 0 || a4 % 2 == 0) {
            if ((int32_t)v4 != 0) {
                goto lab_0x3f590;
            } else {
                // 0x3f546
                result = 0;
                if (a4 == 0) {
                    // 0x3f54b
                    function_20230();
                    function_207e0();
                    function_20680();
                    result = 0;
                }
                goto lab_0x3f4fb;
            }
        } else {
            // 0x3f4d5
            result = 0;
            if ((int32_t)v4 != 0) {
                goto lab_0x3f590;
            } else {
                goto lab_0x3f4fb;
            }
        }
    }
  lab_0x3f4fb:
    // 0x3f4fb
    function_207c0();
    SSL_CONF_CTX_free();
    if (v3 != __readfsqword(40)) {
        // 0x3f79a
        return function_20ff0();
    }
    // 0x3f51f
    return result;
  lab_0x3f590:
    // 0x3f590
    function_21100();
    result = 0;
    if (SSL_CONF_CTX_new() != 0) {
        if (a1 == 0) {
            // 0x3f770
            SSL_CONF_CTX_set_ssl_ctx();
        } else {
            // 0x3f5ff
            SSL_CONF_CTX_set_ssl();
        }
        // 0x3f62c
        SSL_CONF_CTX_set_flags();
        function_207c0();
        int64_t v5 = 0; // 0x3f670
        if (v2 != 0) {
            function_211b0();
            int32_t v6 = SSL_CONF_cmd(); // 0x3f6b2
            while (v6 >= 0 == (v6 != 0)) {
                int64_t v7 = v5 + 1; // 0x3f680
                v5 = v7;
                if (v2 <= v7) {
                    goto lab_0x3f758;
                }
                function_211b0();
                v6 = SSL_CONF_cmd();
            }
            // 0x3f6b6
            function_20230();
            function_207e0();
            function_20680();
            result = 0;
        } else {
          lab_0x3f758:;
            int32_t v8 = SSL_CONF_CTX_finish(); // 0x3f763
            result = v8 >= 0 == (v8 != 0);
        }
    }
    goto lab_0x3f4fb;
}