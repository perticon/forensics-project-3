undefined8 tls_parse_stoc_server_name(long param_1,long param_2)

{
  long lVar1;
  char *pcVar2;
  undefined8 uVar3;
  
  if (*(char **)(param_1 + 0xa58) == (char *)0x0) {
    ERR_new();
    uVar3 = 0;
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x521,"tls_parse_stoc_server_name");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    if (*(long *)(param_2 + 8) != 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x526,"tls_parse_stoc_server_name");
      ossl_statem_fatal(param_1,0x32,0x6e,0);
      return 0;
    }
    if (*(int *)(param_1 + 0x4d0) == 0) {
      lVar1 = *(long *)(param_1 + 0x918);
      if (*(long *)(lVar1 + 0x330) != 0) {
        ERR_new();
        uVar3 = 0x52c;
LAB_00162db2:
        ERR_set_debug("ssl/statem/extensions_clnt.c",uVar3,"tls_parse_stoc_server_name");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        return 0;
      }
      pcVar2 = CRYPTO_strdup(*(char **)(param_1 + 0xa58),"ssl/statem/extensions_clnt.c",0x52f);
      *(char **)(lVar1 + 0x330) = pcVar2;
      if (*(long *)(*(long *)(param_1 + 0x918) + 0x330) == 0) {
        ERR_new();
        uVar3 = 0x531;
        goto LAB_00162db2;
      }
    }
    uVar3 = 1;
  }
  return uVar3;
}