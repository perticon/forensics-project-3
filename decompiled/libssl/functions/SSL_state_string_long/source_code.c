const char *SSL_state_string_long(const SSL *s)
{
    if (ossl_statem_in_error(s))
        return "error";

    switch (SSL_get_state(s)) {
    case TLS_ST_CR_CERT_STATUS:
        return "SSLv3/TLS read certificate status";
    case TLS_ST_CW_NEXT_PROTO:
        return "SSLv3/TLS write next proto";
    case TLS_ST_SR_NEXT_PROTO:
        return "SSLv3/TLS read next proto";
    case TLS_ST_SW_CERT_STATUS:
        return "SSLv3/TLS write certificate status";
    case TLS_ST_BEFORE:
        return "before SSL initialization";
    case TLS_ST_OK:
        return "SSL negotiation finished successfully";
    case TLS_ST_CW_CLNT_HELLO:
        return "SSLv3/TLS write client hello";
    case TLS_ST_CR_SRVR_HELLO:
        return "SSLv3/TLS read server hello";
    case TLS_ST_CR_CERT:
        return "SSLv3/TLS read server certificate";
    case TLS_ST_CR_KEY_EXCH:
        return "SSLv3/TLS read server key exchange";
    case TLS_ST_CR_CERT_REQ:
        return "SSLv3/TLS read server certificate request";
    case TLS_ST_CR_SESSION_TICKET:
        return "SSLv3/TLS read server session ticket";
    case TLS_ST_CR_SRVR_DONE:
        return "SSLv3/TLS read server done";
    case TLS_ST_CW_CERT:
        return "SSLv3/TLS write client certificate";
    case TLS_ST_CW_KEY_EXCH:
        return "SSLv3/TLS write client key exchange";
    case TLS_ST_CW_CERT_VRFY:
        return "SSLv3/TLS write certificate verify";
    case TLS_ST_CW_CHANGE:
    case TLS_ST_SW_CHANGE:
        return "SSLv3/TLS write change cipher spec";
    case TLS_ST_CW_FINISHED:
    case TLS_ST_SW_FINISHED:
        return "SSLv3/TLS write finished";
    case TLS_ST_CR_CHANGE:
    case TLS_ST_SR_CHANGE:
        return "SSLv3/TLS read change cipher spec";
    case TLS_ST_CR_FINISHED:
    case TLS_ST_SR_FINISHED:
        return "SSLv3/TLS read finished";
    case TLS_ST_SR_CLNT_HELLO:
        return "SSLv3/TLS read client hello";
    case TLS_ST_SW_HELLO_REQ:
        return "SSLv3/TLS write hello request";
    case TLS_ST_SW_SRVR_HELLO:
        return "SSLv3/TLS write server hello";
    case TLS_ST_SW_CERT:
        return "SSLv3/TLS write certificate";
    case TLS_ST_SW_KEY_EXCH:
        return "SSLv3/TLS write key exchange";
    case TLS_ST_SW_CERT_REQ:
        return "SSLv3/TLS write certificate request";
    case TLS_ST_SW_SESSION_TICKET:
        return "SSLv3/TLS write session ticket";
    case TLS_ST_SW_SRVR_DONE:
        return "SSLv3/TLS write server done";
    case TLS_ST_SR_CERT:
        return "SSLv3/TLS read client certificate";
    case TLS_ST_SR_KEY_EXCH:
        return "SSLv3/TLS read client key exchange";
    case TLS_ST_SR_CERT_VRFY:
        return "SSLv3/TLS read certificate verify";
    case DTLS_ST_CR_HELLO_VERIFY_REQUEST:
        return "DTLS1 read hello verify request";
    case DTLS_ST_SW_HELLO_VERIFY_REQUEST:
        return "DTLS1 write hello verify request";
    case TLS_ST_SW_ENCRYPTED_EXTENSIONS:
        return "TLSv1.3 write encrypted extensions";
    case TLS_ST_CR_ENCRYPTED_EXTENSIONS:
        return "TLSv1.3 read encrypted extensions";
    case TLS_ST_CR_CERT_VRFY:
        return "TLSv1.3 read server certificate verify";
    case TLS_ST_SW_CERT_VRFY:
        return "TLSv1.3 write server certificate verify";
    case TLS_ST_CR_HELLO_REQ:
        return "SSLv3/TLS read hello request";
    case TLS_ST_SW_KEY_UPDATE:
        return "TLSv1.3 write server key update";
    case TLS_ST_CW_KEY_UPDATE:
        return "TLSv1.3 write client key update";
    case TLS_ST_SR_KEY_UPDATE:
        return "TLSv1.3 read client key update";
    case TLS_ST_CR_KEY_UPDATE:
        return "TLSv1.3 read server key update";
    case TLS_ST_EARLY_DATA:
        return "TLSv1.3 early data";
    case TLS_ST_PENDING_EARLY_DATA_END:
        return "TLSv1.3 pending early data end";
    case TLS_ST_CW_END_OF_EARLY_DATA:
        return "TLSv1.3 write end of early data";
    case TLS_ST_SR_END_OF_EARLY_DATA:
        return "TLSv1.3 read end of early data";
    default:
        return "unknown state";
    }
}