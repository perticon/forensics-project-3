char * SSL_state_string_long(SSL *s)

{
  int iVar1;
  undefined4 uVar2;
  
  iVar1 = ossl_statem_in_error();
  if (iVar1 == 0) {
    uVar2 = SSL_get_state(s);
    switch(uVar2) {
    case 0:
      return "before SSL initialization";
    case 1:
      return "SSL negotiation finished successfully";
    case 2:
      return "DTLS1 read hello verify request";
    case 3:
      return "SSLv3/TLS read server hello";
    case 4:
      return "SSLv3/TLS read server certificate";
    case 5:
      return "SSLv3/TLS read certificate status";
    case 6:
      return "SSLv3/TLS read server key exchange";
    case 7:
      return "SSLv3/TLS read server certificate request";
    case 8:
      return "SSLv3/TLS read server done";
    case 9:
      return "SSLv3/TLS read server session ticket";
    case 10:
    case 0x1f:
      return "SSLv3/TLS read change cipher spec";
    case 0xb:
    case 0x20:
      return "SSLv3/TLS read finished";
    case 0xc:
      return "SSLv3/TLS write client hello";
    case 0xd:
      return "SSLv3/TLS write client certificate";
    case 0xe:
      return "SSLv3/TLS write client key exchange";
    case 0xf:
      return "SSLv3/TLS write certificate verify";
    case 0x10:
    case 0x23:
      return "SSLv3/TLS write change cipher spec";
    case 0x11:
      return "SSLv3/TLS write next proto";
    case 0x12:
    case 0x24:
      return "SSLv3/TLS write finished";
    case 0x13:
      return "SSLv3/TLS write hello request";
    case 0x14:
      return "SSLv3/TLS read client hello";
    case 0x15:
      return "DTLS1 write hello verify request";
    case 0x16:
      return "SSLv3/TLS write server hello";
    case 0x17:
      return "SSLv3/TLS write certificate";
    case 0x18:
      return "SSLv3/TLS write key exchange";
    case 0x19:
      return "SSLv3/TLS write certificate request";
    case 0x1a:
      return "SSLv3/TLS write server done";
    case 0x1b:
      return "SSLv3/TLS read client certificate";
    case 0x1c:
      return "SSLv3/TLS read client key exchange";
    case 0x1d:
      return "SSLv3/TLS read certificate verify";
    case 0x1e:
      return "SSLv3/TLS read next proto";
    case 0x21:
      return "SSLv3/TLS write session ticket";
    case 0x22:
      return "SSLv3/TLS write certificate status";
    case 0x25:
      return "TLSv1.3 write encrypted extensions";
    case 0x26:
      return "TLSv1.3 read encrypted extensions";
    case 0x27:
      return "TLSv1.3 read server certificate verify";
    case 0x28:
      return "TLSv1.3 write server certificate verify";
    case 0x29:
      return "SSLv3/TLS read hello request";
    case 0x2a:
      return "TLSv1.3 write server key update";
    case 0x2b:
      return "TLSv1.3 write client key update";
    case 0x2c:
      return "TLSv1.3 read client key update";
    case 0x2d:
      return "TLSv1.3 read server key update";
    case 0x2e:
      return "TLSv1.3 early data";
    case 0x2f:
      return "TLSv1.3 pending early data end";
    case 0x30:
      return "TLSv1.3 write end of early data";
    case 0x31:
      return "TLSv1.3 read end of early data";
    default:
      return "unknown state";
    }
  }
  return "error";
}