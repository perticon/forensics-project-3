undefined8 tls_parse_ctos_early_data(long param_1,long param_2)

{
  if (*(long *)(param_2 + 8) != 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_srvr.c",0x3ab,"tls_parse_ctos_early_data");
    ossl_statem_fatal(param_1,0x32,0x6e,0);
    return 0;
  }
  if (*(int *)(param_1 + 0x8e8) == 0) {
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x3b0,"tls_parse_ctos_early_data");
  ossl_statem_fatal(param_1,0x2f,0x6e,0);
  return 0;
}