undefined8 tls_parse_ctos_post_handshake_auth(long param_1,long param_2)

{
  if (*(long *)(param_2 + 8) == 0) {
    *(undefined4 *)(param_1 + 0xba8) = 2;
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x4c1,"tls_parse_ctos_post_handshake_auth");
  ossl_statem_fatal(param_1,0x32,0x116,0);
  return 0;
}