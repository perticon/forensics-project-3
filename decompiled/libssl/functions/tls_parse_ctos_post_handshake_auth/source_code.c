int tls_parse_ctos_post_handshake_auth(SSL *s, PACKET *pkt,
                                       ossl_unused unsigned int context,
                                       ossl_unused X509 *x,
                                       ossl_unused size_t chainidx)
{
    if (PACKET_remaining(pkt) != 0) {
        SSLfatal(s, SSL_AD_DECODE_ERROR,
                 SSL_R_POST_HANDSHAKE_AUTH_ENCODING_ERR);
        return 0;
    }

    s->post_handshake_auth = SSL_PHA_EXT_RECEIVED;

    return 1;
}