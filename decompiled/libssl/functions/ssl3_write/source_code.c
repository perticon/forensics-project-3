int ssl3_write(SSL *s, const void *buf, size_t len, size_t *written)
{
    clear_sys_error();
    if (s->s3.renegotiate)
        ssl3_renegotiate_check(s, 0);

    return s->method->ssl_write_bytes(s, SSL3_RT_APPLICATION_DATA, buf, len,
                                      written);
}