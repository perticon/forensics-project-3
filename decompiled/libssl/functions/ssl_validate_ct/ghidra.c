int ssl_validate_ct(SSL *param_1)

{
  undefined8 *puVar1;
  int iVar2;
  long lVar3;
  undefined8 uVar4;
  SSL_SESSION *s;
  long lVar5;
  
  if (*(long *)&param_1[3].sid_ctx_length == 0) {
    return 1;
  }
  lVar5 = *(long *)(*(long *)&param_1[3].sid_ctx_length + 0x2b8);
  if (((((*(long *)&param_1[4].hit != 0) && (lVar5 != 0)) && (*(long *)&param_1[3].references == 0))
      && ((param_1[3].client_CA != (stack_st_X509_NAME *)0x0 &&
          (iVar2 = OPENSSL_sk_num(), 1 < iVar2)))) &&
     ((iVar2 = OPENSSL_sk_num(param_1[1].tlsext_ellipticcurvelist), iVar2 < 1 ||
      (((byte *)param_1[1].tlsext_opaque_prf_input_len == (byte *)0x0 ||
       (1 < *(byte *)param_1[1].tlsext_opaque_prf_input_len - 2)))))) {
    puVar1 = *(undefined8 **)&param_1[3].ex_data.dummy;
    lVar3 = CT_POLICY_EVAL_CTX_new_ex(*puVar1,puVar1[0x88]);
    if (lVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x146c,"ssl_validate_ct");
      ossl_statem_fatal(param_1,0x50,0xc0100,0);
      CT_POLICY_EVAL_CTX_free(0);
    }
    else {
      uVar4 = OPENSSL_sk_value(param_1[3].client_CA,1);
      CT_POLICY_EVAL_CTX_set1_cert(lVar3,lVar5);
      CT_POLICY_EVAL_CTX_set1_issuer(lVar3,uVar4);
      CT_POLICY_EVAL_CTX_set_shared_CTLOG_STORE
                (lVar3,*(undefined8 *)(*(long *)&param_1[3].ex_data.dummy + 0x1c8));
      s = SSL_get_session(param_1);
      lVar5 = SSL_SESSION_get_time(s);
      CT_POLICY_EVAL_CTX_set_time(lVar3,lVar5 * 1000);
      uVar4 = SSL_get0_peer_scts(param_1);
      iVar2 = SCT_LIST_validate(uVar4,lVar3);
      if (iVar2 < 0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0x1488,"ssl_validate_ct");
        uVar4 = 0xd0;
      }
      else {
        iVar2 = (**(code **)&param_1[4].hit)(lVar3,uVar4,param_1[4].param);
        if (0 < iVar2) {
          CT_POLICY_EVAL_CTX_free(lVar3);
          return iVar2;
        }
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0x1490,"ssl_validate_ct");
        uVar4 = 0xea;
      }
      ossl_statem_fatal(param_1,0x28,uVar4,0);
      CT_POLICY_EVAL_CTX_free(lVar3);
    }
    *(undefined8 *)&param_1[3].references = 0x47;
    return 0;
  }
  return 1;
}