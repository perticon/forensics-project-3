undefined8 ssl_log_secret(long param_1,char *param_2,undefined *param_3,long param_4)

{
  long lVar1;
  undefined uVar2;
  size_t sVar3;
  char *__dest;
  char *pcVar4;
  undefined *puVar5;
  char *pcVar6;
  char *pcVar7;
  
  if (*(long *)(*(long *)(param_1 + 0x9a8) + 0x3d0) != 0) {
    sVar3 = strlen(param_2);
    lVar1 = param_4 * 2 + 0x43 + sVar3;
    __dest = (char *)CRYPTO_malloc((int)lVar1,"ssl/ssl_lib.c",0x15a3);
    if (__dest == (char *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x15a4,"nss_keylog_int");
      ossl_statem_fatal(param_1,0x50,0xc0100,0);
      return 0;
    }
    pcVar4 = __dest + sVar3;
    strcpy(__dest,param_2);
    *pcVar4 = ' ';
    puVar5 = (undefined *)(param_1 + 0x160);
    pcVar6 = pcVar4 + 1;
    do {
      uVar2 = *puVar5;
      pcVar7 = pcVar6 + 2;
      puVar5 = puVar5 + 1;
      __sprintf_chk(pcVar6,1,0xffffffffffffffff,&DAT_00186168,uVar2);
      pcVar6 = pcVar7;
    } while (pcVar7 != pcVar4 + 0x41);
    pcVar4[0x41] = ' ';
    pcVar6 = pcVar4 + 0x42;
    if (param_4 != 0) {
      puVar5 = param_3 + param_4;
      do {
        uVar2 = *param_3;
        param_3 = param_3 + 1;
        __sprintf_chk(pcVar6,1,0xffffffffffffffff,&DAT_00186168,uVar2);
        pcVar6 = pcVar6 + 2;
      } while (param_3 != puVar5);
      pcVar6 = pcVar4 + param_4 * 2 + 0x42;
    }
    *pcVar6 = '\0';
    (**(code **)(*(long *)(param_1 + 0x9a8) + 0x3d0))(param_1,__dest);
    CRYPTO_clear_free(__dest,lVar1,"ssl/ssl_lib.c",0x15b9);
  }
  return 1;
}