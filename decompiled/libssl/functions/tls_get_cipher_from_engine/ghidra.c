EVP_CIPHER * tls_get_cipher_from_engine(int param_1)

{
  ENGINE *e;
  EVP_CIPHER *pEVar1;
  
  pEVar1 = (EVP_CIPHER *)0x0;
  e = ENGINE_get_cipher_engine(param_1);
  if (e != (ENGINE *)0x0) {
    pEVar1 = ENGINE_get_cipher(e,param_1);
    ENGINE_finish(e);
  }
  return pEVar1;
}