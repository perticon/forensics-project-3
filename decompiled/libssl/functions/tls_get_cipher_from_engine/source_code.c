const EVP_CIPHER *tls_get_cipher_from_engine(int nid)
{
    const EVP_CIPHER *ret = NULL;
#ifndef OPENSSL_NO_ENGINE
    ENGINE *eng;

    /*
     * If there is an Engine available for this cipher we use the "implicit"
     * form to ensure we use that engine later.
     */
    eng = ENGINE_get_cipher_engine(nid);
    if (eng != NULL) {
        ret = ENGINE_get_cipher(eng, nid);
        ENGINE_finish(eng);
    }
#endif
    return ret;
}