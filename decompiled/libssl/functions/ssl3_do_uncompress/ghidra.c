undefined8 ssl3_do_uncompress(long param_1,long param_2)

{
  int iVar1;
  uchar *out;
  
  out = *(uchar **)(param_2 + 0x30);
  if (out == (uchar *)0x0) {
    out = (uchar *)CRYPTO_malloc(0x4540,"ssl/record/ssl3_record.c",0x32a);
    *(uchar **)(param_2 + 0x30) = out;
    if (out == (uchar *)0x0) {
      return 0;
    }
  }
  iVar1 = COMP_expand_block(*(COMP_CTX **)(param_1 + 0x870),out,0x4000,*(uchar **)(param_2 + 0x20),
                            *(int *)(param_2 + 8));
  if (iVar1 < 0) {
    return 0;
  }
  *(long *)(param_2 + 8) = (long)iVar1;
  *(undefined8 *)(param_2 + 0x20) = *(undefined8 *)(param_2 + 0x30);
  return 1;
}