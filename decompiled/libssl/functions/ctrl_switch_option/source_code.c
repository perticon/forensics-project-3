static int ctrl_switch_option(SSL_CONF_CTX *cctx, const ssl_conf_cmd_tbl * cmd)
{
    /* Find index of command in table */
    size_t idx = cmd - ssl_conf_cmds;
    const ssl_switch_tbl *scmd;
    /* Sanity check index */
    if (idx >= OSSL_NELEM(ssl_cmd_switches))
        return 0;
    /* Obtain switches entry with same index */
    scmd = ssl_cmd_switches + idx;
    ssl_set_option(cctx, scmd->name_flags, scmd->option_value, 1);
    return 1;
}