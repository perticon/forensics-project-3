EVP_PKEY * ssl_generate_pkey_group(long param_1,undefined2 param_2)

{
  int iVar1;
  long lVar2;
  EVP_PKEY_CTX *ctx;
  undefined8 uVar3;
  long in_FS_OFFSET;
  EVP_PKEY *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  lVar2 = tls1_group_id_lookup(*(undefined8 *)(param_1 + 0x9a8),param_2);
  local_28 = (EVP_PKEY *)0x0;
  if (lVar2 == 0) {
    ERR_new();
    ctx = (EVP_PKEY_CTX *)0x0;
    ERR_set_debug("ssl/s3_lib.c",0x1264,"ssl_generate_pkey_group");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    ctx = (EVP_PKEY_CTX *)
          EVP_PKEY_CTX_new_from_name
                    (**(undefined8 **)(param_1 + 0x9a8),*(undefined8 *)(lVar2 + 0x10),
                     (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
    if (ctx == (EVP_PKEY_CTX *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/s3_lib.c",0x126c,"ssl_generate_pkey_group");
      ossl_statem_fatal(param_1,0x50,0xc0100,0);
    }
    else {
      iVar1 = EVP_PKEY_keygen_init(ctx);
      if (iVar1 < 1) {
        ERR_new();
        uVar3 = 0x1270;
      }
      else {
        iVar1 = EVP_PKEY_CTX_set_group_name(ctx,*(undefined8 *)(lVar2 + 8));
        if (0 < iVar1) {
          iVar1 = EVP_PKEY_keygen(ctx,&local_28);
          if (iVar1 < 1) {
            ERR_new();
            ERR_set_debug("ssl/s3_lib.c",0x1278,"ssl_generate_pkey_group");
            ossl_statem_fatal(param_1,0x50,0x80006,0);
            EVP_PKEY_free(local_28);
            local_28 = (EVP_PKEY *)0x0;
          }
          goto LAB_0012b3cc;
        }
        ERR_new();
        uVar3 = 0x1274;
      }
      ERR_set_debug("ssl/s3_lib.c",uVar3,"ssl_generate_pkey_group");
      ossl_statem_fatal(param_1,0x50,0x80006,0);
    }
  }
LAB_0012b3cc:
  EVP_PKEY_CTX_free(ctx);
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return local_28;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}