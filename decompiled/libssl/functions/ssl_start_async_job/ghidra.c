undefined4 ssl_start_async_job(long param_1,undefined8 param_2,undefined8 param_3)

{
  int iVar1;
  long lVar2;
  long in_FS_OFFSET;
  undefined4 local_24;
  long local_20;
  
  lVar2 = *(long *)(param_1 + 0x1d30);
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (lVar2 == 0) {
    lVar2 = ASYNC_WAIT_CTX_new();
    *(long *)(param_1 + 0x1d30) = lVar2;
    if (lVar2 != 0) {
      if (*(long *)(param_1 + 0x1d98) != 0) {
        iVar1 = ASYNC_WAIT_CTX_set_callback(lVar2,ssl_async_wait_ctx_cb,param_1);
        if (iVar1 == 0) goto LAB_0013576f;
        lVar2 = *(long *)(param_1 + 0x1d30);
      }
      goto LAB_00135661;
    }
  }
  else {
LAB_00135661:
    *(undefined4 *)(param_1 + 0x28) = 1;
    iVar1 = ASYNC_start_job(param_1 + 0x1d28,lVar2,&local_24,param_3,param_2,0x28);
    if (iVar1 == 2) {
      *(undefined4 *)(param_1 + 0x28) = 5;
      local_24 = 0xffffffff;
      goto LAB_001356b0;
    }
    if (iVar1 < 3) {
      if (iVar1 == 0) {
        *(undefined4 *)(param_1 + 0x28) = 1;
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0x6f3,"ssl_start_async_job");
        ERR_set_error(0x14,0x195,0);
        local_24 = 0xffffffff;
        goto LAB_001356b0;
      }
      if (iVar1 == 1) {
        *(undefined4 *)(param_1 + 0x28) = 6;
        local_24 = 0xffffffff;
        goto LAB_001356b0;
      }
    }
    else if (iVar1 == 3) {
      *(undefined8 *)(param_1 + 0x1d28) = 0;
      goto LAB_001356b0;
    }
    *(undefined4 *)(param_1 + 0x28) = 1;
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x700,"ssl_start_async_job");
    ERR_set_error(0x14,0xc0103,0);
  }
LAB_0013576f:
  local_24 = 0xffffffff;
LAB_001356b0:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return local_24;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}