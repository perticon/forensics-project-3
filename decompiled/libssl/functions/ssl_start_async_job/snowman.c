int64_t ssl_start_async_job(void** rdi, void** rsi, int64_t rdx, ...) {
    void** rbx4;
    void** r10_5;
    uint64_t rax6;
    void** rax7;
    int32_t eax8;
    int64_t rax9;
    int32_t v10;
    int32_t eax11;
    uint64_t rcx12;
    int64_t v13;
    int64_t v14;
    int32_t eax15;
    int64_t v16;
    int64_t v17;
    int64_t v18;

    rbx4 = rdi;
    r10_5 = *reinterpret_cast<void***>(rdi + 0x1d30);
    rax6 = g28;
    if (!r10_5) {
        rax7 = fun_207f0();
        *reinterpret_cast<void***>(rbx4 + 0x1d30) = rax7;
        r10_5 = rax7;
        if (!rax7) 
            goto addr_3576f_3;
        if (!*reinterpret_cast<void***>(rbx4 + 0x1d98)) 
            goto addr_35661_5;
    } else {
        addr_35661_5:
        *reinterpret_cast<void***>(rbx4 + 40) = reinterpret_cast<void**>(1);
        rsi = r10_5;
        rdi = rbx4 + 0x1d28;
        eax8 = fun_21dd0();
        if (eax8 == 2) {
            *reinterpret_cast<void***>(rbx4 + 40) = reinterpret_cast<void**>(5);
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
            goto addr_356b0_7;
        } else {
            if (eax8 > 2) {
                if (eax8 != 3) {
                    addr_35738_10:
                    *reinterpret_cast<void***>(rbx4 + 40) = reinterpret_cast<void**>(1);
                    fun_20230();
                    fun_207e0("ssl/ssl_lib.c", "ssl/ssl_lib.c");
                    *reinterpret_cast<int32_t*>(&rsi) = 0xc0103;
                    *reinterpret_cast<int32_t*>(&rdi) = 20;
                    fun_20680();
                    goto addr_3576f_3;
                } else {
                    *reinterpret_cast<int64_t*>(rbx4 + 0x1d28) = 0;
                    *reinterpret_cast<int32_t*>(&rax9) = v10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                    goto addr_356b0_7;
                }
            } else {
                if (!eax8) {
                    *reinterpret_cast<void***>(rbx4 + 40) = reinterpret_cast<void**>(1);
                    fun_20230();
                    fun_207e0("ssl/ssl_lib.c", "ssl/ssl_lib.c");
                    *reinterpret_cast<int32_t*>(&rsi) = 0x195;
                    *reinterpret_cast<int32_t*>(&rdi) = 20;
                    fun_20680();
                    *reinterpret_cast<int32_t*>(&rax9) = -1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                    goto addr_356b0_7;
                } else {
                    if (eax8 != 1) 
                        goto addr_35738_10;
                    *reinterpret_cast<void***>(rbx4 + 40) = reinterpret_cast<void**>(6);
                    *reinterpret_cast<int32_t*>(&rax9) = -1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                    goto addr_356b0_7;
                }
            }
        }
    }
    rsi = reinterpret_cast<void**>(0x354e0);
    rdi = rax7;
    eax11 = fun_20610();
    if (!eax11) {
        addr_3576f_3:
        *reinterpret_cast<int32_t*>(&rax9) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    } else {
        r10_5 = *reinterpret_cast<void***>(rbx4 + 0x1d30);
        goto addr_35661_5;
    }
    addr_356b0_7:
    rcx12 = rax6 ^ g28;
    if (!rcx12) {
        return rax9;
    }
    fun_20ff0();
    if (*reinterpret_cast<int32_t*>(&rdi) != 0x100) 
        goto addr_357e8_21;
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x100) {
        addr_35854_23:
        goto v13;
    } else {
        if (*reinterpret_cast<int32_t*>(&rsi) >> 8 == 0xfe) {
            goto v14;
        }
    }
    addr_357e8_21:
    eax15 = *reinterpret_cast<int32_t*>(&rdi) >> 8;
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x100 || *reinterpret_cast<int32_t*>(&rsi) >> 8 == 0xfe) {
        if (eax15 != 0xfe) {
            goto v16;
        }
    } else {
        if (eax15 != 0xfe) {
            if (*reinterpret_cast<int32_t*>(&rdi)) {
                if (!*reinterpret_cast<int32_t*>(&rsi)) {
                }
                if (*reinterpret_cast<int32_t*>(&rdi) != 0x300) {
                    goto v17;
                }
            }
        } else {
            goto v18;
        }
    }
    goto addr_35854_23;
}