bool tls_process_key_update(long param_1,byte **param_2)

{
  byte bVar1;
  int iVar2;
  byte *pbVar3;
  
  iVar2 = RECORD_LAYER_processed_read_pending(param_1 + 0xc58);
  if (iVar2 != 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x295,"tls_process_key_update");
    ossl_statem_fatal(param_1,10,0xb6,0);
    return false;
  }
  if (param_2[1] != (byte *)0x0) {
    bVar1 = **param_2;
    pbVar3 = param_2[1] + -1;
    *param_2 = *param_2 + 1;
    param_2[1] = pbVar3;
    if (pbVar3 == (byte *)0x0) {
      if (1 < bVar1) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_lib.c",0x2a5,"tls_process_key_update");
        ossl_statem_fatal(param_1,0x2f,0x7a,0);
        return false;
      }
      if (bVar1 == 1) {
        *(undefined4 *)(param_1 + 0xba4) = 0;
      }
      iVar2 = tls13_update_key(param_1,0);
      return iVar2 != 0;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_lib.c",0x29b,"tls_process_key_update");
  ossl_statem_fatal(param_1,0x32,0x7a,0);
  return false;
}