byte tls_decrypt_ticket(long param_1,ulong *param_2,ulong param_3,void *param_4,size_t param_5,
                       SSL_SESSION **param_6)

{
  code *pcVar1;
  long lVar2;
  int iVar3;
  long lVar4;
  EVP_CIPHER_CTX *ctx;
  undefined8 uVar5;
  size_t len;
  uchar *out;
  SSL_SESSION *ses;
  EVP_CIPHER *cipher;
  ulong uVar6;
  byte bVar7;
  long in_FS_OFFSET;
  bool bVar8;
  int local_98;
  int local_94;
  uchar *local_90;
  undefined local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 == 0) {
    ctx = (EVP_CIPHER_CTX *)0x0;
    lVar4 = 0;
    bVar7 = 3;
LAB_0014af35:
    EVP_CIPHER_CTX_free(ctx);
    ssl_hmac_free(lVar4);
    pcVar1 = *(code **)(*(long *)(param_1 + 0xb88) + 0x400);
    ses = (SSL_SESSION *)0x0;
    if (pcVar1 == (code *)0x0) {
LAB_0014af5f:
      if (*(long *)(param_1 + 0xaf8) != 0) goto LAB_0014af6d;
    }
    else {
      uVar6 = 0x10;
      if (param_3 < 0x11) {
        uVar6 = param_3;
      }
      iVar3 = (*pcVar1)(param_1,0,param_2,uVar6,bVar7,
                        *(undefined8 *)(*(long *)(param_1 + 0xb88) + 0x408));
      if (iVar3 == 2) {
        ses = (SSL_SESSION *)0x0;
LAB_0014b3d3:
        SSL_SESSION_free(ses);
        ses = (SSL_SESSION *)0x0;
        goto LAB_0014af5f;
      }
      if (iVar3 < 3) {
        ses = (SSL_SESSION *)0x0;
LAB_0014b328:
        if (iVar3 != 1) goto LAB_0014b3a3;
        bVar7 = 2;
        SSL_SESSION_free(ses);
        ses = (SSL_SESSION *)0x0;
      }
      else {
        if (iVar3 - 3U < 2) {
          ses = (SSL_SESSION *)0x0;
          lVar4 = *(long *)(param_1 + 0xaf8);
          bVar7 = 1;
          goto joined_r0x0014b359;
        }
        ses = (SSL_SESSION *)0x0;
LAB_0014b3a3:
        bVar7 = 1;
      }
LAB_0014b2e0:
      lVar4 = *(long *)(param_1 + 0xaf8);
joined_r0x0014b359:
      if (lVar4 == 0) goto LAB_0014af98;
LAB_0014af6d:
      if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
          (iVar3 = **(int **)(param_1 + 8), iVar3 == 0x10000)) || (iVar3 < 0x304))
      goto LAB_0014af98;
      if (4 < bVar7) goto LAB_0014b3b6;
      if (bVar7 < 3) goto LAB_0014af98;
    }
  }
  else {
    if ((((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
          (iVar3 = **(int **)(param_1 + 8), iVar3 == 0x10000)) || (iVar3 < 0x304)) &&
        (*(long *)(param_1 + 0xaf8) != 0)) || (param_3 < 0x20)) {
      ctx = (EVP_CIPHER_CTX *)0x0;
      lVar4 = 0;
      bVar7 = 4;
      goto LAB_0014af35;
    }
    lVar2 = *(long *)(param_1 + 0xb88);
    lVar4 = ssl_hmac_new(lVar2);
    if (lVar4 == 0) {
      ctx = (EVP_CIPHER_CTX *)0x0;
      bVar7 = 0;
LAB_0014b3ee:
      ses = (SSL_SESSION *)0x0;
      EVP_CIPHER_CTX_free(ctx);
      ssl_hmac_free(lVar4);
      lVar4 = *(long *)(param_1 + 0xaf8);
      goto joined_r0x0014b359;
    }
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == (EVP_CIPHER_CTX *)0x0) {
      bVar7 = 0;
      goto LAB_0014b3ee;
    }
    pcVar1 = *(code **)(lVar2 + 0x248);
    if (pcVar1 != (code *)0x0) {
      uVar5 = ssl_hmac_get0_EVP_MAC_CTX(lVar4);
      iVar3 = (*pcVar1)(param_1,param_2,param_2 + 2,ctx,uVar5,0);
LAB_0014b057:
      if (-1 < iVar3) {
        if (iVar3 == 0) goto LAB_0014b418;
        bVar8 = iVar3 == 2;
        goto LAB_0014b072;
      }
      goto LAB_0014b480;
    }
    pcVar1 = *(code **)(lVar2 + 0x240);
    if (pcVar1 != (code *)0x0) {
      uVar5 = ssl_hmac_get0_HMAC_CTX(lVar4);
      iVar3 = (*pcVar1)(param_1,param_2,param_2 + 2,ctx,uVar5,0);
      goto LAB_0014b057;
    }
    if ((param_2[1] ^ *(ulong *)(lVar2 + 0x230) | *param_2 ^ *(ulong *)(lVar2 + 0x228)) != 0) {
LAB_0014b418:
      bVar7 = 4;
      goto LAB_0014af35;
    }
    bVar8 = false;
    cipher = (EVP_CIPHER *)
             EVP_CIPHER_fetch(**(undefined8 **)(param_1 + 0x9a8),&DAT_00188288,
                              (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
    if (((cipher == (EVP_CIPHER *)0x0) ||
        (iVar3 = ssl_hmac_init(lVar4,*(undefined8 *)(lVar2 + 0x238),0x20,"SHA256"), iVar3 < 1)) ||
       (iVar3 = EVP_DecryptInit_ex(ctx,cipher,(ENGINE *)0x0,
                                   (uchar *)(*(long *)(lVar2 + 0x238) + 0x20),(uchar *)(param_2 + 2)
                                  ), iVar3 < 1)) {
      bVar7 = 1;
      EVP_CIPHER_free(cipher);
      goto LAB_0014b3ee;
    }
    EVP_CIPHER_free(cipher);
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
      iVar3 = **(int **)(param_1 + 8);
      bVar8 = 0x303 < iVar3 && iVar3 != 0x10000;
    }
LAB_0014b072:
    bVar7 = 1;
    len = ssl_hmac_size(lVar4);
    if (len == 0) goto LAB_0014b3ee;
    bVar7 = 4;
    iVar3 = EVP_CIPHER_CTX_get_iv_length(ctx);
    if (param_3 <= (long)(iVar3 + 0x10) + len) goto LAB_0014af35;
    param_3 = param_3 - len;
    iVar3 = ssl_hmac_update(lVar4,param_2,param_3);
    if (iVar3 < 1) {
LAB_0014b480:
      bVar7 = 1;
      goto LAB_0014b3ee;
    }
    iVar3 = ssl_hmac_final(lVar4,local_88,0,0x40);
    if (iVar3 < 1) goto LAB_0014b480;
    iVar3 = CRYPTO_memcmp(local_88,(void *)((long)param_2 + param_3),len);
    if (iVar3 != 0) goto LAB_0014af35;
    iVar3 = EVP_CIPHER_CTX_get_iv_length(ctx);
    local_90 = (uchar *)((long)param_2 + (long)iVar3 + 0x10);
    iVar3 = EVP_CIPHER_CTX_get_iv_length(ctx);
    param_3 = param_3 - (long)(iVar3 + 0x10);
    out = (uchar *)CRYPTO_malloc((int)param_3,"ssl/t1_lib.c",0x7ba);
    if ((out == (uchar *)0x0) ||
       (iVar3 = EVP_DecryptUpdate(ctx,out,&local_98,local_90,(int)param_3), iVar3 < 1)) {
      CRYPTO_free(out);
      bVar7 = 1;
      goto LAB_0014b3ee;
    }
    iVar3 = EVP_DecryptFinal(ctx,out + local_98,&local_94);
    if (iVar3 < 1) {
      CRYPTO_free(out);
      goto LAB_0014af35;
    }
    local_98 = local_94 + local_98;
    local_90 = out;
    ses = d2i_SSL_SESSION((SSL_SESSION **)0x0,&local_90,(long)local_98);
    local_98 = local_98 - ((int)local_90 - (int)out);
    CRYPTO_free(out);
    if (ses == (SSL_SESSION *)0x0) {
      ERR_clear_error();
      goto LAB_0014af35;
    }
    if (local_98 != 0) {
      bVar7 = 4;
      SSL_SESSION_free(ses);
      goto LAB_0014af35;
    }
    if (param_5 != 0) {
      memcpy(ses + 1,param_4,param_5);
      ses->tlsext_tick_lifetime_hint = param_5;
    }
    EVP_CIPHER_CTX_free(ctx);
    ssl_hmac_free(lVar4);
    bVar7 = bVar8 + 5;
    pcVar1 = *(code **)(*(long *)(param_1 + 0xb88) + 0x400);
    if (pcVar1 != (code *)0x0) {
      uVar6 = 0x10;
      if (param_3 < 0x11) {
        uVar6 = param_3;
      }
      iVar3 = (*pcVar1)(param_1,ses,param_2,uVar6,bVar7,
                        *(undefined8 *)(*(long *)(param_1 + 0xb88) + 0x408));
      if (iVar3 != 2) {
        if (iVar3 < 3) goto LAB_0014b328;
        if (1 < iVar3 - 3U) goto LAB_0014b3a3;
        bVar7 = 6;
        if (iVar3 != 3) goto LAB_0014af5f;
        bVar7 = 5;
        goto LAB_0014b2e0;
      }
      bVar7 = 4;
      goto LAB_0014b3d3;
    }
    if (*(long *)(param_1 + 0xaf8) != 0) goto LAB_0014af6d;
LAB_0014b3b6:
    if (bVar7 != 6) {
      bVar7 = 5;
      goto LAB_0014af98;
    }
  }
  *(undefined4 *)(param_1 + 0xa98) = 1;
LAB_0014af98:
  *param_6 = ses;
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}