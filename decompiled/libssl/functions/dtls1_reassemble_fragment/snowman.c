void** dtls1_reassemble_fragment(void** rdi, void** rsi) {
    void** rsp3;
    void** r12_4;
    void** rdx5;
    uint64_t rax6;
    uint64_t v7;
    void** rdx8;
    void** rbp9;
    void** r14_10;
    void** rbx11;
    void** rdi12;
    struct s15* rax13;
    struct s15* r13_14;
    void** rdi15;
    void** rax16;
    void** r15_17;
    void** rax18;
    int32_t rax19;
    void** r14_20;
    int32_t eax21;
    void** v22;
    int32_t eax23;
    void** v24;
    void** eax25;
    uint64_t rcx26;
    void** rax27;
    int64_t rax28;
    unsigned char* rdx29;
    uint32_t eax30;
    void* rcx31;
    void* rdx32;
    void* rax33;
    unsigned char* rax34;
    int64_t rcx35;
    uint32_t edx36;
    void** rdx37;
    uint32_t ecx38;
    uint32_t ecx39;
    unsigned char* rdx40;
    int32_t edi41;
    void** rdx42;
    void** rdi43;
    int64_t rdx44;
    void* rax45;
    uint32_t ecx46;
    void* rax47;
    void** rax48;
    void** rdi49;
    int64_t rax50;

    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128);
    r12_4 = *reinterpret_cast<void***>(rsi + 32);
    rdx5 = *reinterpret_cast<void***>(rsi + 24);
    rax6 = g28;
    v7 = rax6;
    if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdx5) + reinterpret_cast<unsigned char>(r12_4)) > reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) 
        goto addr_72350_2;
    *reinterpret_cast<int32_t*>(&rdx8) = 0x454c;
    *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 0xa00)) >= reinterpret_cast<unsigned char>(0x454c)) {
        rdx8 = *reinterpret_cast<void***>(rdi + 0xa00);
    }
    rbp9 = rdi;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8)) > reinterpret_cast<unsigned char>(rdx8)) 
        goto addr_72350_2;
    if (r12_4) {
        r14_10 = rsp3 + 8;
        rbx11 = rsi;
        rsi = r14_10;
        __asm__("rol ax, 0x8");
        rdi12 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x4b8) + 0x118);
        rax13 = pqueue_find(rdi12, rsi);
        rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8);
        r13_14 = rax13;
        if (!rax13) {
            rdi15 = *reinterpret_cast<void***>(rbx11 + 8);
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rax16 = dtls1_hm_fragment_new(rdi15, 1);
            rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8);
            r15_17 = rax16;
            if (!rax16) {
                addr_72350_2:
                *reinterpret_cast<int32_t*>(&r15_17) = 0;
                *reinterpret_cast<int32_t*>(&r15_17 + 4) = 0;
                goto addr_72353_9;
            } else {
                __asm__("movdqa xmm0, [rbx]");
                __asm__("movups [rax], xmm0");
                __asm__("movdqa xmm1, [rbx+0x10]");
                __asm__("movups [rax+0x10], xmm1");
                __asm__("movdqa xmm2, [rbx+0x20]");
                __asm__("movups [rax+0x20], xmm2");
                __asm__("movdqa xmm3, [rbx+0x30]");
                __asm__("movups [rax+0x30], xmm3");
                __asm__("movdqa xmm4, [rbx+0x40]");
                __asm__("movups [rax+0x40], xmm4");
                rax18 = *reinterpret_cast<void***>(rbx11 + 80);
                *reinterpret_cast<void***>(r15_17 + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(r15_17 + 80) = rax18;
                *reinterpret_cast<void***>(r15_17 + 32) = *reinterpret_cast<void***>(r15_17 + 8);
            }
        } else {
            r15_17 = rax13->f8;
            if (*reinterpret_cast<void***>(r15_17 + 8) != *reinterpret_cast<void***>(rbx11 + 8)) 
                goto addr_72350_2;
        }
        rax19 = *reinterpret_cast<int32_t*>(*reinterpret_cast<void***>(rbp9 + 8) + 0x68);
        if (!*reinterpret_cast<void***>(r15_17 + 96)) {
            r14_20 = rsp3;
            while (1) {
                if (reinterpret_cast<unsigned char>(r12_4) <= reinterpret_cast<unsigned char>(0x100)) {
                }
                eax21 = reinterpret_cast<int32_t>(rax19(rbp9, 22));
                rsi = r14_20;
                if (reinterpret_cast<uint1_t>(eax21 < 0) | reinterpret_cast<uint1_t>(eax21 == 0)) 
                    goto addr_72390_17;
                r12_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_4) - reinterpret_cast<unsigned char>(v22));
                if (!r12_4) 
                    break;
                rax19 = *reinterpret_cast<int32_t*>(*reinterpret_cast<void***>(rbp9 + 8) + 0x68);
            }
        } else {
            *reinterpret_cast<int32_t*>(&rsi) = 22;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            eax23 = reinterpret_cast<int32_t>(rax19(rbp9, 22));
            if (reinterpret_cast<uint1_t>(eax23 < 0) | reinterpret_cast<uint1_t>(eax23 == 0) || v24 != r12_4) {
                addr_72390_17:
                eax25 = reinterpret_cast<void**>(0xffffffff);
                if (r13_14) {
                    addr_72360_22:
                    rcx26 = v7 ^ g28;
                    if (rcx26) {
                        fun_20ff0();
                    } else {
                        return eax25;
                    }
                } else {
                    goto addr_72353_9;
                }
            } else {
                rax27 = *reinterpret_cast<void***>(rbx11 + 24);
                if (reinterpret_cast<signed char>(r12_4) > reinterpret_cast<signed char>(8)) {
                    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint32_t*>(&rax27) & 7;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
                    rdx29 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rax27) >> 3) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17 + 96)));
                    eax30 = *reinterpret_cast<unsigned char*>(0x8d6d8 + rax28);
                    *rdx29 = reinterpret_cast<unsigned char>(*rdx29 | *reinterpret_cast<unsigned char*>(&eax30));
                    rcx31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx11 + 24)) + reinterpret_cast<unsigned char>(r12_4));
                    rdx32 = reinterpret_cast<void*>((reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx11 + 24)) >> 3) + 1);
                    rax33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(reinterpret_cast<uint64_t>(rcx31) + 0xffffffffffffffff) >> 3);
                    if (reinterpret_cast<int64_t>(rdx32) < reinterpret_cast<int64_t>(rax33)) {
                        do {
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17 + 96)) + reinterpret_cast<uint64_t>(rdx32)) = -1;
                            rdx32 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx32) + 1);
                            rcx31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx11 + 24)) + reinterpret_cast<unsigned char>(r12_4));
                            rax33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(reinterpret_cast<uint64_t>(rcx31) + 0xffffffffffffffff) >> 3);
                        } while (reinterpret_cast<int64_t>(rax33) > reinterpret_cast<int64_t>(rdx32));
                    }
                    rax34 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax33) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17 + 96)));
                    *reinterpret_cast<uint32_t*>(&rcx35) = *reinterpret_cast<uint32_t*>(&rcx31) & 7;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx35) + 4) = 0;
                    edx36 = *reinterpret_cast<unsigned char*>(0x8d6d0 + rcx35);
                    *rax34 = reinterpret_cast<unsigned char>(*rax34 | *reinterpret_cast<unsigned char*>(&edx36));
                } else {
                    *reinterpret_cast<int32_t*>(&rsi) = 1;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(rax27)) > reinterpret_cast<signed char>(rax27)) {
                        do {
                            rdx37 = rax27;
                            ecx38 = *reinterpret_cast<uint32_t*>(&rax27);
                            ++rax27;
                            ecx39 = ecx38 & 7;
                            rdx40 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rdx37) >> 3) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17 + 96)));
                            edi41 = 1 << *reinterpret_cast<unsigned char*>(&ecx39);
                            *rdx40 = reinterpret_cast<unsigned char>(*rdx40 | *reinterpret_cast<unsigned char*>(&edi41));
                        } while (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx11 + 24)) + reinterpret_cast<unsigned char>(r12_4)) > reinterpret_cast<signed char>(rax27));
                    }
                }
                rdx42 = *reinterpret_cast<void***>(rbx11 + 8);
                if (!rdx42) 
                    goto addr_72390_17;
            }
            rdi43 = *reinterpret_cast<void***>(r15_17 + 96);
            *reinterpret_cast<uint32_t*>(&rdx44) = *reinterpret_cast<uint32_t*>(&rdx42) & 7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx44) + 4) = 0;
            rax45 = reinterpret_cast<void*>(reinterpret_cast<signed char>(rdx42 + 0xffffffffffffffff) >> 3);
            ecx46 = *reinterpret_cast<unsigned char*>(0x8d6d0 + rdx44);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi43) + reinterpret_cast<uint64_t>(rax45)) == *reinterpret_cast<signed char*>(&ecx46)) {
                rax47 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax45) - 1);
                if (reinterpret_cast<int64_t>(rax47) < reinterpret_cast<int64_t>(0)) {
                    addr_724f4_38:
                    fun_208d0();
                    *reinterpret_cast<void***>(r15_17 + 96) = reinterpret_cast<void**>(0);
                } else {
                    do {
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi43) + reinterpret_cast<uint64_t>(rax47)) != -1) 
                            break;
                        rax47 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax47) - 1);
                    } while (!reinterpret_cast<int1_t>(rax47 == 0xffffffffffffffff));
                    goto addr_724f4_38;
                }
            }
            if (!r13_14) {
                rax48 = pitem_new(r14_10, r15_17, r14_10, r15_17);
                rsi = rax48;
                if (!rax48 || (rdi49 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp9 + 0x4b8) + 0x118), rax50 = pqueue_insert(rdi49, rsi, rdi49, rsi), rax50 == 0)) {
                    addr_72353_9:
                    dtls1_hm_fragment_free(r15_17, rsi, r15_17, rsi);
                    eax25 = reinterpret_cast<void**>(0xffffffff);
                    goto addr_72360_22;
                }
            }
        }
    }
    eax25 = reinterpret_cast<void**>(0xfffffffd);
    goto addr_72360_22;
}