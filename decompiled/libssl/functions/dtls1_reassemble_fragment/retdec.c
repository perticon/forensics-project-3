int64_t dtls1_reassemble_fragment(int64_t a1, int64_t * a2, int16_t a3, int64_t a4, int64_t a5, int64_t a6) {
    int64_t v1 = (int64_t)a2;
    int64_t v2 = v1;
    int64_t v3; // bp-344, 0x72180
    int64_t v4 = &v3; // 0x7218a
    int64_t v5 = v1 + 32;
    int64_t v6 = *(int64_t *)v5; // 0x72191
    int64_t * v7 = (int64_t *)(v1 + 24);
    __readfsqword(40);
    int64_t * v8 = (int64_t *)(v1 + 8);
    uint64_t v9 = *v8; // 0x721ac
    int64_t v10; // 0x72180
    int64_t result; // 0x72180
    int64_t v11; // 0x72220
    if (*v7 + v6 > v9) {
        // 0x72353
        dtls1_hm_fragment_free();
        result = 0xffffffff;
        goto lab_0x72360;
    } else {
        uint64_t v12 = *(int64_t *)(a1 + (int64_t)&g382); // 0x721bc
        uint64_t v13 = v12 >= (int64_t)&g169 ? v12 : (int64_t)&g169; // 0x721cc
        if (v9 > v13) {
            // 0x72353
            dtls1_hm_fragment_free();
            result = 0xffffffff;
            goto lab_0x72360;
        } else {
            // 0x721e0
            result = 0xfffffffd;
            if (v6 == 0) {
                goto lab_0x72360;
            } else {
                // 0x721e9
                v11 = pqueue_find();
                if (v11 == 0) {
                    int64_t v14 = dtls1_hm_fragment_new(*v8, 1); // 0x723a9
                    if (v14 == 0) {
                        // 0x72353
                        dtls1_hm_fragment_free();
                        result = 0xffffffff;
                        goto lab_0x72360;
                    } else {
                        // 0x723b6
                        __asm_movups(*(int128_t *)v14, __asm_movdqa(*(int128_t *)&v2));
                        __asm_movups(*(int128_t *)(v14 + 16), __asm_movdqa(*(int128_t *)(v1 + 16)));
                        int128_t v15 = __asm_movdqa(*(int128_t *)v5); // 0x723c6
                        int64_t v16 = v14 + 32; // 0x723cb
                        __asm_movups(*(int128_t *)v16, v15);
                        __asm_movups(*(int128_t *)(v14 + 48), __asm_movdqa(*(int128_t *)(v1 + 48)));
                        __asm_movups(*(int128_t *)(v14 + 64), __asm_movdqa(*(int128_t *)(v1 + 64)));
                        *(int64_t *)(v14 + 24) = 0;
                        *(int64_t *)(v14 + 80) = *(int64_t *)(v1 + 80);
                        *(int64_t *)v16 = *(int64_t *)(v14 + 8);
                        v10 = v14;
                        goto lab_0x72243;
                    }
                } else {
                    int64_t v17 = *(int64_t *)(v11 + 8); // 0x72231
                    v10 = v17;
                    if (*(int64_t *)(v17 + 8) != *v8) {
                        // 0x72353
                        dtls1_hm_fragment_free();
                        result = 0xffffffff;
                        goto lab_0x72360;
                    } else {
                        goto lab_0x72243;
                    }
                }
            }
        }
    }
  lab_0x72390:
    // 0x72390
    result = 0xffffffff;
    if (v11 != 0) {
        goto lab_0x72360;
    } else {
        // 0x72353
        dtls1_hm_fragment_free();
        result = 0xffffffff;
        goto lab_0x72360;
    }
  lab_0x72360:
    // 0x72360
    if (*(int64_t *)(v4 + 280) != __readfsqword(40)) {
        // 0x72512
        return function_20ff0();
    }
    // 0x72377
    return result;
  lab_0x72243:;
    int64_t * v18 = (int64_t *)(a1 + 8); // 0x72243
    int64_t * v19 = (int64_t *)(v10 + 96); // 0x72247
    int64_t v20 = *v19; // 0x72247
    int64_t v21 = *(int64_t *)(*v18 + 104); // 0x7224c
    if (v20 == 0) {
        int64_t * v22 = (int64_t *)(v4 - 16); // 0x7243b
        *v22 = v4;
        if ((int32_t)v21 < 1) {
            goto lab_0x72390;
        } else {
            int64_t v23 = v6 - v3; // 0x7244b
            result = 0xfffffffd;
            while (v23 != 0) {
                // 0x72410
                *v22 = v4;
                if ((int32_t)*(int64_t *)(*v18 + 104) < 1) {
                    goto lab_0x72390;
                }
                v23 -= v3;
                result = 0xfffffffd;
            }
            goto lab_0x72360;
        }
    } else {
        if ((int32_t)v21 < 1) {
            goto lab_0x72390;
        } else {
            // 0x72284
            if (v3 != v6) {
                goto lab_0x72390;
            } else {
                int64_t v24 = *v7; // 0x7228e
                if (v6 > 8) {
                    char * v25 = (char *)((v24 >> 3) + v20); // 0x72479
                    *v25 = *v25 | *(char *)(v24 % 8 | (int64_t)&g329);
                    int64_t v26 = *v7; // 0x7247b
                    int64_t v27 = v26 + v6; // 0x72482
                    int64_t v28 = (v26 >> 3) + 1; // 0x7248d
                    int64_t v29 = v27 - 1 >> 3; // 0x72491
                    int64_t v30 = v29; // 0x72498
                    int64_t v31 = v27; // 0x72498
                    if (v28 < v29) {
                        *(char *)(*v19 + v28) = -1;
                        int64_t v32 = v28 + 1; // 0x724ac
                        int64_t v33 = *v7 + v6; // 0x724b0
                        int64_t v34 = v33 - 1 >> 3; // 0x724b7
                        int64_t v35 = v32; // 0x724be
                        v30 = v34;
                        v31 = v33;
                        while (v34 > v32) {
                            // 0x724a0
                            *(char *)(*v19 + v35) = -1;
                            v32 = v35 + 1;
                            v33 = *v7 + v6;
                            v34 = v33 - 1 >> 3;
                            v35 = v32;
                            v30 = v34;
                            v31 = v33;
                        }
                    }
                    char * v36 = (char *)(*v19 + v30); // 0x724d2
                    *v36 = *v36 | *(char *)(v31 % 8 | (int64_t)&g328);
                } else {
                    // 0x7229c
                    v2 = 1;
                    if (v24 + v6 > v24) {
                        int64_t v37 = v24 + 1; // 0x722b7
                        char * v38 = (char *)(*v19 + (v24 >> 3)); // 0x722c8
                        *v38 = *v38 | (char)(1 << (int32_t)v24 % 8);
                        if (*v7 + v6 > v37) {
                            int64_t v39 = v2;
                            int64_t v40 = v37 + 1; // 0x722b7
                            uint32_t v41 = (int32_t)v37 % 8; // 0x722bf
                            int64_t v42 = v41 == 0 ? v39 : (int64_t)((int32_t)v39 << v41);
                            char * v43 = (char *)(*v19 + (v37 >> 3)); // 0x722c8
                            *v43 = *v43 | (char)v42;
                            int64_t v44 = v40; // 0x722d5
                            while (*v7 + v6 > v40) {
                                // 0x722b0
                                v39 = v2;
                                v40 = v44 + 1;
                                v41 = (int32_t)v44 % 8;
                                v42 = v41 == 0 ? v39 : (int64_t)((int32_t)v39 << v41);
                                v43 = (char *)(*v19 + (v44 >> 3));
                                *v43 = *v43 | (char)v42;
                                v44 = v40;
                            }
                        }
                    }
                }
                uint64_t v45 = *v8; // 0x722d7
                if (v45 == 0) {
                    goto lab_0x72390;
                } else {
                    int64_t v46 = v45 - 1; // 0x722e4
                    int64_t v47 = *v19; // 0x722e8
                    int64_t v48 = v46 >> 3; // 0x722f6
                    char v49 = *(char *)(v47 + v48); // 0x722fe
                    if (v49 == *(char *)(v45 % 8 || (int64_t)&g328)) {
                        // 0x724d9
                        if (v46 < 8) {
                          lab_0x724f4:
                            // 0x724f4
                            function_208d0();
                            *v19 = 0;
                        } else {
                            int64_t v50 = v48 - 1;
                            while (*(char *)(v50 + v47) == -1) {
                                // 0x724ea
                                if (v50 == 0) {
                                    goto lab_0x724f4;
                                }
                                v50--;
                            }
                        }
                    }
                    // 0x72307
                    result = 0xfffffffd;
                    if (v11 != 0) {
                        goto lab_0x72360;
                    } else {
                        // 0x7230c
                        if (pitem_new() == 0) {
                            // 0x72353
                            dtls1_hm_fragment_free();
                            result = 0xffffffff;
                            goto lab_0x72360;
                        } else {
                            // 0x7231f
                            result = 0xfffffffd;
                            if (pqueue_insert() == 0) {
                                // 0x72353
                                dtls1_hm_fragment_free();
                                result = 0xffffffff;
                                goto lab_0x72360;
                            } else {
                                goto lab_0x72360;
                            }
                        }
                    }
                }
            }
        }
    }
}