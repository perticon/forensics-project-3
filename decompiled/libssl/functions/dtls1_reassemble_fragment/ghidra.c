undefined8 dtls1_reassemble_fragment(long param_1,undefined8 *param_2)

{
  void *ptr;
  undefined8 uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  int iVar5;
  pitem *ppVar6;
  long lVar7;
  undefined8 *data;
  code *pcVar8;
  long lVar9;
  ulong uVar10;
  byte *pbVar11;
  long lVar12;
  ulong uVar13;
  long in_FS_OFFSET;
  undefined8 uVar14;
  ulong local_158;
  undefined4 local_150;
  undefined2 local_14c;
  ushort local_14a;
  undefined local_148 [264];
  long local_40;
  
  uVar13 = param_2[4];
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((ulong)param_2[1] < param_2[3] + uVar13) {
LAB_00172350:
    data = (undefined8 *)0x0;
  }
  else {
    uVar10 = 0x454c;
    if (0x454b < *(ulong *)(param_1 + 0xa00)) {
      uVar10 = *(ulong *)(param_1 + 0xa00);
    }
    if (uVar10 < (ulong)param_2[1]) goto LAB_00172350;
    if (uVar13 == 0) {
LAB_00172340:
      uVar14 = 0xfffffffd;
      goto LAB_00172360;
    }
    local_150 = 0;
    local_14a = *(ushort *)(param_2 + 2) << 8 | *(ushort *)(param_2 + 2) >> 8;
    local_14c = 0;
    uVar14 = 0x172225;
    ppVar6 = pqueue_find(*(pqueue *)(*(long *)(param_1 + 0x4b8) + 0x118),(uchar *)&local_150);
    if (ppVar6 == (pitem *)0x0) {
      uVar14 = 0x1723ae;
      data = (undefined8 *)dtls1_hm_fragment_new(param_2[1],1);
      if (data == (undefined8 *)0x0) goto LAB_00172350;
      uVar1 = param_2[1];
      *data = *param_2;
      data[1] = uVar1;
      uVar2 = *(undefined4 *)((long)param_2 + 0x14);
      uVar3 = *(undefined4 *)(param_2 + 3);
      uVar4 = *(undefined4 *)((long)param_2 + 0x1c);
      *(undefined4 *)(data + 2) = *(undefined4 *)(param_2 + 2);
      *(undefined4 *)((long)data + 0x14) = uVar2;
      *(undefined4 *)(data + 3) = uVar3;
      *(undefined4 *)((long)data + 0x1c) = uVar4;
      uVar2 = *(undefined4 *)((long)param_2 + 0x24);
      uVar3 = *(undefined4 *)(param_2 + 5);
      uVar4 = *(undefined4 *)((long)param_2 + 0x2c);
      *(undefined4 *)(data + 4) = *(undefined4 *)(param_2 + 4);
      *(undefined4 *)((long)data + 0x24) = uVar2;
      *(undefined4 *)(data + 5) = uVar3;
      *(undefined4 *)((long)data + 0x2c) = uVar4;
      uVar2 = *(undefined4 *)((long)param_2 + 0x34);
      uVar3 = *(undefined4 *)(param_2 + 7);
      uVar4 = *(undefined4 *)((long)param_2 + 0x3c);
      *(undefined4 *)(data + 6) = *(undefined4 *)(param_2 + 6);
      *(undefined4 *)((long)data + 0x34) = uVar2;
      *(undefined4 *)(data + 7) = uVar3;
      *(undefined4 *)((long)data + 0x3c) = uVar4;
      uVar2 = *(undefined4 *)((long)param_2 + 0x44);
      uVar3 = *(undefined4 *)(param_2 + 9);
      uVar4 = *(undefined4 *)((long)param_2 + 0x4c);
      *(undefined4 *)(data + 8) = *(undefined4 *)(param_2 + 8);
      *(undefined4 *)((long)data + 0x44) = uVar2;
      *(undefined4 *)(data + 9) = uVar3;
      *(undefined4 *)((long)data + 0x4c) = uVar4;
      uVar1 = param_2[10];
      data[3] = 0;
      data[10] = uVar1;
      data[4] = data[1];
    }
    else {
      data = (undefined8 *)ppVar6->data;
      if (data[1] != param_2[1]) goto LAB_00172350;
    }
    pcVar8 = *(code **)(*(long *)(param_1 + 8) + 0x68);
    if (data[0xc] == 0) {
      while( true ) {
        uVar10 = 0x100;
        if (uVar13 < 0x101) {
          uVar10 = uVar13;
        }
        iVar5 = (*pcVar8)(param_1,0x16,0,local_148,uVar10,0,&local_158,uVar14);
        if (iVar5 < 1) goto LAB_00172390;
        uVar13 = uVar13 - local_158;
        if (uVar13 == 0) break;
        pcVar8 = *(code **)(*(long *)(param_1 + 8) + 0x68);
      }
      goto LAB_00172340;
    }
    iVar5 = (*pcVar8)(param_1,0x16,0,param_2[3] + data[0xb],uVar13,0,&local_158);
    if ((iVar5 < 1) || (local_158 != uVar13)) {
LAB_00172390:
      uVar14 = 0xffffffff;
      if (ppVar6 != (pitem *)0x0) goto LAB_00172360;
    }
    else {
      lVar9 = param_2[3];
      if ((long)uVar13 < 9) {
        if (lVar9 < (long)(uVar13 + lVar9)) {
          do {
            lVar7 = lVar9 + 1;
            pbVar11 = (byte *)((lVar9 >> 3) + data[0xc]);
            *pbVar11 = *pbVar11 | (byte)(1 << ((byte)lVar9 & 7));
            lVar9 = lVar7;
          } while (lVar7 < (long)(param_2[3] + uVar13));
        }
      }
      else {
        pbVar11 = (byte *)((lVar9 >> 3) + data[0xc]);
        *pbVar11 = *pbVar11 | *(byte *)((long)&bitmask_start_values + (ulong)((uint)lVar9 & 7));
        lVar7 = param_2[3] + uVar13;
        lVar12 = ((long)param_2[3] >> 3) + 1;
        lVar9 = lVar7 + -1 >> 3;
        if (lVar12 < lVar9) {
          do {
            *(undefined *)(data[0xc] + lVar12) = 0xff;
            lVar12 = lVar12 + 1;
            lVar7 = param_2[3] + uVar13;
            lVar9 = lVar7 + -1 >> 3;
          } while (lVar12 < lVar9);
        }
        *(byte *)(lVar9 + data[0xc]) =
             *(byte *)(lVar9 + data[0xc]) |
             *(byte *)((long)&bitmask_end_values + (ulong)((uint)lVar7 & 7));
      }
      lVar9 = param_2[1];
      if (lVar9 == 0) goto LAB_00172390;
      ptr = (void *)data[0xc];
      lVar7 = lVar9 + -1 >> 3;
      if (*(char *)((long)ptr + lVar7) ==
          *(char *)((long)&bitmask_end_values + (ulong)((uint)lVar9 & 7))) {
        lVar7 = lVar7 + -1;
        if (-1 < lVar7) {
          do {
            if (*(char *)((long)ptr + lVar7) != -1) goto LAB_00172307;
            lVar7 = lVar7 + -1;
          } while (lVar7 != -1);
        }
        CRYPTO_free(ptr);
        data[0xc] = 0;
      }
LAB_00172307:
      if ((ppVar6 != (pitem *)0x0) ||
         ((ppVar6 = pitem_new((uchar *)&local_150,data), ppVar6 != (pitem *)0x0 &&
          (ppVar6 = pqueue_insert(*(pqueue *)(*(long *)(param_1 + 0x4b8) + 0x118),ppVar6),
          ppVar6 != (pitem *)0x0)))) goto LAB_00172340;
    }
  }
  dtls1_hm_fragment_free(data);
  uVar14 = 0xffffffff;
LAB_00172360:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar14;
}