int SSL_shutdown(SSL *s)

{
  int iVar1;
  long lVar2;
  long in_FS_OFFSET;
  SSL *local_38;
  undefined local_30 [16];
  undefined8 local_20;
  _func_3062 *local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  if (s->handshake_func == (_func_3149 *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x8b3,"SSL_shutdown");
    ERR_set_error(0x14,0x114,0);
    iVar1 = -1;
  }
  else {
    iVar1 = SSL_in_init();
    if (iVar1 == 0) {
      if ((*(byte *)((long)&s[3].tlsext_debug_arg + 1) & 1) != 0) {
        lVar2 = ASYNC_get_current_job();
        if (lVar2 == 0) {
          local_18 = s->method->ssl_renegotiate;
          local_30 = (undefined  [16])0x0;
          local_20 = 2;
          local_38 = s;
          iVar1 = ssl_start_async_job(0,s,&local_38,ssl_io_intern);
          goto LAB_001381e3;
        }
      }
      iVar1 = (*s->method->ssl_renegotiate)(s);
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x8c5,"SSL_shutdown");
      ERR_set_error(0x14,0x197,0);
      iVar1 = -1;
    }
  }
LAB_001381e3:
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}