void dtls1_reset_seq_numbers(long param_1,uint param_2)

{
  short *psVar1;
  undefined8 uVar2;
  
  psVar1 = *(short **)(param_1 + 0x1d10);
  if ((param_2 & 1) != 0) {
    uVar2 = *(undefined8 *)(psVar1 + 0xc);
    *psVar1 = *psVar1 + 1;
    *(undefined8 *)(psVar1 + 4) = uVar2;
    *(undefined8 *)(psVar1 + 8) = *(undefined8 *)(psVar1 + 0x10);
    *(undefined (*) [16])(*(long *)(param_1 + 0x1d10) + 0x18) = (undefined  [16])0x0;
    dtls1_clear_received_buffer(0,uVar2);
    *(undefined8 *)(param_1 + 0x1cf8) = 0;
    return;
  }
  *(undefined8 *)(psVar1 + 0x2c) = *(undefined8 *)(param_1 + 0x1d00);
  psVar1 = (short *)(*(long *)(param_1 + 0x1d10) + 2);
  *psVar1 = *psVar1 + 1;
  *(undefined8 *)(param_1 + 0x1d00) = 0;
  return;
}