int SSL_CIPHER_get_auth_nid(const SSL_CIPHER *c)
{
    int i = ssl_cipher_info_lookup(ssl_cipher_table_auth, c->algorithm_auth);

    if (i == -1)
        return NID_undef;
    return ssl_cipher_table_auth[i].nid;
}