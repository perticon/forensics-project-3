undefined8 tls_construct_stoc_cookie(long param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  time_t tVar3;
  long lVar4;
  EVP_PKEY *pkey;
  undefined8 uVar5;
  long in_FS_OFFSET;
  undefined auVar6 [16];
  long local_98;
  long local_90;
  long local_88;
  long local_80;
  long local_78;
  long local_70;
  long local_68;
  long local_60;
  undefined local_58 [8];
  ulong local_50;
  undefined8 local_48;
  long local_40;
  undefined8 local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = 2;
  if ((*(byte *)(param_1 + 0xa9) & 8) == 0) goto LAB_001691bd;
  if (*(long *)(*(long *)(param_1 + 0x9a8) + 0xe0) == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_srvr.c",0x6bf,"tls_construct_stoc_cookie");
    uVar2 = 0x11f;
LAB_001691ac:
    ossl_statem_fatal(param_1,0x50,uVar2,0);
  }
  else {
    iVar1 = WPACKET_put_bytes__(param_2,0x2c,2);
    if (iVar1 == 0) {
LAB_00169188:
      ERR_new();
      uVar2 = 0x6d2;
LAB_00169199:
      ERR_set_debug("ssl/statem/extensions_srvr.c",uVar2,"tls_construct_stoc_cookie");
      uVar2 = 0xc0103;
      goto LAB_001691ac;
    }
    iVar1 = WPACKET_start_sub_packet_len__(param_2,2);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_start_sub_packet_len__(param_2,2);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_get_total_written(param_2,&local_60);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_reserve_bytes(param_2,0x1072,&local_78);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_put_bytes__(param_2,0,2);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_put_bytes__(param_2,0x304,2);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_put_bytes__(param_2,*(undefined2 *)(param_1 + 0x4ae),2);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = (**(code **)(*(long *)(param_1 + 8) + 0x98))
                      (*(undefined8 *)(param_1 + 0x2e0),param_2,local_58);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_put_bytes__(param_2,*(long *)(param_1 + 0x4b0) == 0,1);
    if (iVar1 == 0) goto LAB_00169188;
    tVar3 = time((time_t *)0x0);
    iVar1 = WPACKET_put_bytes__(param_2,tVar3,4);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_start_sub_packet_len__(param_2,2);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = WPACKET_reserve_bytes(param_2,0x40,&local_98);
    if (iVar1 == 0) goto LAB_00169188;
    iVar1 = ssl3_digest_cached_records(param_1,0);
    if (iVar1 != 0) {
      iVar1 = ssl_handshake_hash(param_1,local_98,0x40,&local_48);
      if (iVar1 == 0) goto LAB_001691bb;
      iVar1 = WPACKET_allocate_bytes(param_2,local_48,&local_90);
      if ((iVar1 == 0) || (local_98 != local_90)) {
LAB_00169335:
        ERR_new();
        uVar2 = 0x6e6;
      }
      else {
        iVar1 = WPACKET_close(param_2);
        if (iVar1 == 0) goto LAB_00169335;
        iVar1 = WPACKET_start_sub_packet_len__(param_2,1);
        if (iVar1 == 0) goto LAB_00169335;
        iVar1 = WPACKET_reserve_bytes(param_2,0x1000,&local_88);
        if (iVar1 == 0) goto LAB_00169335;
        iVar1 = (**(code **)(*(long *)(param_1 + 0x9a8) + 0xe0))(param_1,local_88,&local_38);
        if (iVar1 == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_srvr.c",0x6ec,"tls_construct_stoc_cookie");
          uVar2 = 400;
          goto LAB_001691ac;
        }
        iVar1 = WPACKET_allocate_bytes(param_2,local_38,&local_80);
        if ((iVar1 == 0) || (local_88 != local_80)) {
LAB_001693f1:
          ERR_new();
          uVar2 = 0x6f5;
        }
        else {
          iVar1 = WPACKET_close(param_2);
          if (iVar1 == 0) goto LAB_001693f1;
          iVar1 = WPACKET_get_total_written(param_2,&local_50);
          if (iVar1 == 0) goto LAB_001693f1;
          iVar1 = WPACKET_reserve_bytes(param_2,0x20,&local_70);
          if (iVar1 == 0) goto LAB_001693f1;
          local_40 = 0x20;
          local_50 = local_50 - local_60;
          if (local_50 < 0x1053) {
            lVar4 = EVP_MD_CTX_new();
            auVar6 = EVP_PKEY_new_raw_private_key_ex
                               (**(undefined8 **)(param_1 + 0x9a8),&DAT_00188069,
                                (*(undefined8 **)(param_1 + 0x9a8))[0x88],
                                *(long *)(param_1 + 0xb88) + 0x2d8,0x20);
            pkey = SUB168(auVar6,0);
            if ((lVar4 == 0) || (pkey == (EVP_PKEY *)0x0)) {
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_srvr.c",0x707,"tls_construct_stoc_cookie");
              uVar5 = 0xc0100;
LAB_00169585:
              uVar2 = 0;
              ossl_statem_fatal(param_1,0x50,uVar5,0);
            }
            else {
              iVar1 = EVP_DigestSignInit_ex
                                (lVar4,0,"SHA2-256",**(undefined8 **)(param_1 + 0x9a8),
                                 (*(undefined8 **)(param_1 + 0x9a8))[0x88],pkey,0,
                                 SUB168(auVar6 >> 0x40,0));
              if (iVar1 < 1) {
LAB_001695c0:
                ERR_new();
                uVar2 = 0x70f;
LAB_00169572:
                ERR_set_debug("ssl/statem/extensions_srvr.c",uVar2,"tls_construct_stoc_cookie");
                uVar5 = 0xc0103;
                goto LAB_00169585;
              }
              iVar1 = EVP_DigestSign(lVar4,local_70,&local_40,local_78,local_50);
              if (iVar1 < 1) goto LAB_001695c0;
              if (0x1072 < local_50 + local_40) {
                ERR_new();
                uVar2 = 0x714;
                goto LAB_00169572;
              }
              iVar1 = WPACKET_allocate_bytes(param_2,local_40,&local_68);
              if (((iVar1 == 0) || (local_70 != local_68)) || (local_78 != local_70 - local_50)) {
LAB_00169561:
                ERR_new();
                uVar2 = 0x71d;
                goto LAB_00169572;
              }
              iVar1 = WPACKET_close(param_2);
              if (iVar1 == 0) goto LAB_00169561;
              iVar1 = WPACKET_close(param_2);
              if (iVar1 == 0) goto LAB_00169561;
              uVar2 = 1;
            }
            EVP_MD_CTX_free(lVar4);
            EVP_PKEY_free(pkey);
            goto LAB_001691bd;
          }
          ERR_new();
          uVar2 = 0x6fc;
        }
      }
      goto LAB_00169199;
    }
  }
LAB_001691bb:
  uVar2 = 0;
LAB_001691bd:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}