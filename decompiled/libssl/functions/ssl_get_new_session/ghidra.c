undefined4 ssl_get_new_session(SSL *param_1,int param_2)

{
  int iVar1;
  SSL_SESSION *ses;
  long lVar2;
  
  ses = SSL_SESSION_new();
  if (ses == (SSL_SESSION *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_sess.c",0x1af,"ssl_get_new_session");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
    return 0;
  }
  lVar2 = *(long *)(*(long *)&param_1[4].mac_flags + 0x58);
  if (lVar2 == 0) {
    lVar2 = SSL_get_default_timeout(param_1);
    *(long *)(ses[1].sid_ctx + 0x14) = lVar2;
  }
  else {
    *(long *)(ses[1].sid_ctx + 0x14) = lVar2;
  }
  ssl_session_calculate_timeout(ses);
  SSL_SESSION_free(*(SSL_SESSION **)&param_1[3].sid_ctx_length);
  *(undefined8 *)&param_1[3].sid_ctx_length = 0;
  if ((param_2 == 0) ||
     (((((byte)param_1->method->get_timeout[0x60] & 8) == 0 &&
       (iVar1 = param_1->method->version, iVar1 != 0x10000)) && (0x303 < iVar1)))) {
    ses->tlsext_tick_lifetime_hint = 0;
  }
  else {
    iVar1 = ssl_generate_session_id(param_1,ses);
    if (iVar1 == 0) {
      SSL_SESSION_free(ses);
      return 0;
    }
  }
  if (param_1[3].expand < (COMP_CTX *)0x21) {
    memcpy(ses[1].master_key + 0x14,&param_1[3].enc_write_ctx,(size_t)param_1[3].expand);
    *(COMP_CTX **)(ses[1].master_key + 0xc) = param_1[3].expand;
    iVar1 = param_1->version;
    *(SSL_SESSION **)&param_1[3].sid_ctx_length = ses;
    ses->ssl_version = iVar1;
    *(undefined8 *)(ses[1].sid_ctx + 4) = 0;
    if ((*(byte *)((long)&param_1->hit + 1) & 2) == 0) {
      return 1;
    }
    *(uint *)(ses[1].krb5_client_princ + 0xa0) = *(uint *)(ses[1].krb5_client_princ + 0xa0) | 1;
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_sess.c",0x1cf,"ssl_get_new_session");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  SSL_SESSION_free(ses);
  return 0;
}