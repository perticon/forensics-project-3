bool ssl_check_srvr_ecc_cert_and_alg(undefined8 param_1,long param_2)

{
  ulong uVar1;
  bool bVar2;
  
  if ((*(byte *)(*(long *)(param_2 + 0x2e0) + 0x20) & 8) != 0) {
    uVar1 = X509_get_key_usage();
    bVar2 = (uVar1 & 0x80) == 0;
    if (bVar2) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0xe5f,"ssl_check_srvr_ecc_cert_and_alg");
      ERR_set_error(0x14,0x13e,0);
    }
    return !bVar2;
  }
  return true;
}