long SSL_get_srp_username(long param_1)

{
  if (*(long *)(param_1 + 0xbf0) != 0) {
    return *(long *)(param_1 + 0xbf0);
  }
  return *(long *)(*(long *)(param_1 + 0x9a8) + 0x338);
}