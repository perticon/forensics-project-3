int tls1_set_groups_list(undefined8 param_1,long *param_2,undefined8 *param_3,char *param_4)

{
  int iVar1;
  long lVar2;
  long in_FS_OFFSET;
  undefined8 local_58;
  undefined4 local_50;
  undefined4 uStack76;
  undefined4 uStack72;
  undefined4 uStack68;
  void *local_40;
  long local_30;
  
  iVar1 = 0;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = _DAT_001887f0;
  uStack76 = _UNK_001887f4;
  uStack72 = _UNK_001887f8;
  uStack68 = _UNK_001887fc;
  local_40 = CRYPTO_malloc(0x50,"ssl/t1_lib.c",0x319);
  if (local_40 != (void *)0x0) {
    local_58 = param_1;
    iVar1 = CONF_parse_list(param_4,0x3a,1,gid_cb,&local_58);
    if ((iVar1 != 0) && (iVar1 = 1, param_2 != (long *)0x0)) {
      lVar2 = CRYPTO_memdup(local_40,CONCAT44(uStack76,local_50) * 2,"ssl/t1_lib.c",0x328);
      if (lVar2 == 0) {
        iVar1 = 0;
      }
      else {
        *param_2 = lVar2;
        *param_3 = CONCAT44(uStack76,local_50);
      }
    }
    CRYPTO_free(local_40);
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}