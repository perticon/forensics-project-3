int tls1_set_groups_list(SSL_CTX *ctx, uint16_t **pext, size_t *pextlen,
                         const char *str)
{
    gid_cb_st gcb;
    uint16_t *tmparr;
    int ret = 0;

    gcb.gidcnt = 0;
    gcb.gidmax = GROUPLIST_INCREMENT;
    gcb.gid_arr = OPENSSL_malloc(gcb.gidmax * sizeof(*gcb.gid_arr));
    if (gcb.gid_arr == NULL)
        return 0;
    gcb.ctx = ctx;
    if (!CONF_parse_list(str, ':', 1, gid_cb, &gcb))
        goto end;
    if (pext == NULL) {
        ret = 1;
        goto end;
    }

    /*
     * gid_cb ensurse there are no duplicates so we can just go ahead and set
     * the result
     */
    tmparr = OPENSSL_memdup(gcb.gid_arr, gcb.gidcnt * sizeof(*tmparr));
    if (tmparr == NULL)
        goto end;
    *pext = tmparr;
    *pextlen = gcb.gidcnt;
    ret = 1;
 end:
    OPENSSL_free(gcb.gid_arr);
    return ret;
}