__owur static int sess_timedout(time_t t, SSL_SESSION *ss)
{
    /* if timeout overflowed, it can never timeout! */
    if (ss->timeout_ovf)
        return 0;
    return t > ss->calc_timeout;
}