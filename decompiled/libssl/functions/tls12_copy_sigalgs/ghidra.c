bool tls12_copy_sigalgs(long param_1,undefined8 param_2,long param_3,long param_4)

{
  long lVar1;
  int iVar2;
  long lVar3;
  bool bVar4;
  long lVar5;
  
  if (param_4 != 0) {
    bVar4 = false;
    lVar5 = 0;
    do {
      lVar3 = *(long *)(*(long *)(param_1 + 0x9a8) + 0x620);
      lVar1 = lVar3 + 0x4d8;
      do {
        if (*(short *)(param_3 + lVar5 * 2) == *(short *)(lVar3 + 8)) {
          if ((*(int *)(lVar3 + 0x24) != 0) &&
             (iVar2 = tls12_sigalg_allowed_part_0(param_1,0x5000b,lVar3), iVar2 != 0)) {
            iVar2 = WPACKET_put_bytes__(param_2,*(undefined2 *)(param_3 + lVar5 * 2),2);
            if (iVar2 == 0) {
              return false;
            }
            if (bVar4 == false) {
              if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
                  (iVar2 = **(int **)(param_1 + 8), iVar2 != 0x10000)) && (0x303 < iVar2)) {
                if (*(int *)(lVar3 + 0x14) != 6) {
                  bVar4 = *(int *)(lVar3 + 0xc) != 0x40 && *(int *)(lVar3 + 0xc) != 0x2a3;
                }
              }
              else {
                bVar4 = true;
              }
            }
          }
          break;
        }
        lVar3 = lVar3 + 0x28;
      } while (lVar3 != lVar1);
      lVar5 = lVar5 + 1;
    } while (param_4 != lVar5);
    if (bVar4 != false) {
      return bVar4;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/t1_lib.c",0x8b4,"tls12_copy_sigalgs");
  ERR_set_error(0x14,0x76,0);
  return false;
}