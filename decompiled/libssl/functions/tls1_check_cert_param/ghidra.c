undefined8 tls1_check_cert_param(long param_1,undefined8 param_2,int param_3)

{
  ushort uVar1;
  int iVar2;
  uint uVar3;
  long lVar4;
  char *pcVar5;
  char cVar6;
  char *pcVar7;
  
  lVar4 = X509_get0_pubkey(param_2);
  if (lVar4 == 0) {
    return 0;
  }
  iVar2 = EVP_PKEY_is_a(lVar4,&DAT_00188294);
  if (iVar2 == 0) {
    return 1;
  }
  iVar2 = EVP_PKEY_is_a(lVar4);
  if (iVar2 != 0) {
    iVar2 = EVP_PKEY_get_ec_point_conv_form(lVar4);
    if (iVar2 == 0) {
      return 0;
    }
    if (iVar2 == 4) {
      cVar6 = '\0';
    }
    else {
      if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
          (iVar2 = **(int **)(param_1 + 8), 0x303 < iVar2)) && (iVar2 != 0x10000))
      goto LAB_0014c677;
      iVar2 = EVP_PKEY_get_field_type(lVar4);
      if (iVar2 == 0x196) {
        cVar6 = '\x01';
      }
      else {
        cVar6 = '\x02';
        if (iVar2 != 0x197) {
          return 0;
        }
      }
    }
    pcVar5 = *(char **)(param_1 + 0xab8);
    if (pcVar5 != (char *)0x0) {
      if (*(long *)(param_1 + 0xab0) == 0) {
        return 0;
      }
      pcVar7 = pcVar5 + *(long *)(param_1 + 0xab0);
      while (*pcVar5 != cVar6) {
        pcVar5 = pcVar5 + 1;
        if (pcVar7 == pcVar5) {
          return 0;
        }
      }
    }
  }
LAB_0014c677:
  uVar1 = 0;
  uVar3 = ssl_get_EC_curve_nid(lVar4);
  if (uVar3 != 0) {
    uVar1 = tls1_nid2group_id(uVar3);
    uVar3 = (uint)uVar1;
  }
  iVar2 = tls1_check_group_id(param_1,uVar3,*(int *)(param_1 + 0x38) == 0);
  if (iVar2 != 0) {
    if ((param_3 == 0) || ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) == 0)) {
      return 1;
    }
    if (uVar1 == 0x17) {
      iVar2 = 0x31a;
    }
    else {
      iVar2 = 0x31b;
      if (uVar1 != 0x18) {
        return 0;
      }
    }
    if (*(long *)(param_1 + 0x1db0) != 0) {
      lVar4 = 0;
      do {
        if (*(int *)(*(long *)(*(long *)(param_1 + 0x1da8) + lVar4 * 8) + 0x1c) == iVar2) {
          return 1;
        }
        lVar4 = lVar4 + 1;
      } while (lVar4 != *(long *)(param_1 + 0x1db0));
    }
  }
  return 0;
}