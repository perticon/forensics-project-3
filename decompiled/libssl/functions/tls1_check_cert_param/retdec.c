int64_t tls1_check_cert_param(int64_t a1, int64_t a2, int64_t a3, int64_t a4) {
    // 0x4c620
    if (function_20e60() == 0) {
        // 0x4c651
        return 0;
    }
    // 0x4c63b
    if ((int32_t)function_21680() == 0) {
        // 0x4c651
        return 1;
    }
    // 0x4c660
    int64_t result; // 0x4c620
    char v1; // 0x4c620
    if ((int32_t)function_21680() != 0) {
        // 0x4c710
        result = 0;
        v1 = 0;
        switch ((int32_t)v2) {
            case 0: {
                return result;
            }
            case 4: {
                goto lab_0x4c765;
            }
            default: {
                int64_t v3 = *(int64_t *)(a1 + 8); // 0x4c725
                if ((*(char *)(*(int64_t *)(v3 + 192) + 96) & 8) != 0) {
                    goto lab_0x4c74a;
                } else {
                    int32_t v4 = *(int32_t *)v3; // 0x4c736
                    if (v4 > (int32_t)&g90 == (v4 != (int32_t)&g2)) {
                        goto lab_0x4c677;
                    } else {
                        goto lab_0x4c74a;
                    }
                }
            }
        }
    } else {
        goto lab_0x4c677;
    }
  lab_0x4c677:;
    int16_t v5 = 0; // 0x4c685
    if ((int32_t)ssl_get_EC_curve_nid() != 0) {
        // 0x4c7b0
        v5 = tls1_nid2group_id();
    }
    // 0x4c68b
    if ((int32_t)tls1_check_group_id() == 0) {
        // 0x4c651
        return 0;
    }
    // 0x4c6a1
    if ((int32_t)a3 == 0) {
        // 0x4c651
        return 1;
    }
    int32_t v6 = *(int32_t *)(*(int64_t *)(a1 + (int64_t)&g297) + 28); // 0x4c6ad
    if ((v6 & 0x30000) == 0) {
        // 0x4c651
        return 1;
    }
    int32_t v7 = &g95; // 0x4c6ba
    if (v5 != 23) {
        // 0x4c6c0
        v7 = &g96;
        if (v5 != 24) {
            // 0x4c651
            return 0;
        }
    }
    int64_t v8 = *(int64_t *)(a1 + (int64_t)&g68); // 0x4c6cb
    if (v8 == 0) {
        // 0x4c651
        return 0;
    }
    int64_t v9 = *(int64_t *)(a1 + (int64_t)&g67); // 0x4c6d7
    int64_t v10 = 0; // 0x4c6e0
    result = 1;
    while (*(int32_t *)(*(int64_t *)(8 * v10 + v9) + 28) != v7) {
        int64_t v11 = v10 + 1; // 0x4c6e8
        result = 0;
        v10 = v11;
        if (v11 == v8) {
            // break -> 0x4c651
            break;
        }
        result = 1;
    }
    // 0x4c651
    return result;
  lab_0x4c765:;
    int64_t v12 = *(int64_t *)(a1 + (int64_t)&g459); // 0x4c765
    if (v12 != 0) {
        int64_t v13 = *(int64_t *)(a1 + (int64_t)&g458); // 0x4c775
        if (v13 == 0) {
            // 0x4c651
            return 0;
        }
        int64_t v14 = v12; // 0x4c788
        while (*(char *)v14 != v1) {
            // 0x4c790
            v14++;
            if (v13 + v12 == v14) {
                // 0x4c651
                return 0;
            }
        }
    }
    goto lab_0x4c677;
  lab_0x4c74a:;
    int32_t v15 = function_21d90(); // 0x4c752
    v1 = 1;
    if (v15 != 406) {
        // 0x4c759
        v1 = 2;
        if (v15 != 407) {
            // 0x4c651
            return 0;
        }
    }
    goto lab_0x4c765;
}