int32_t tls1_check_cert_param(void** rdi, void** rsi, void** edx, void** rcx) {
    void** rdx3;
    void** r12d5;
    void** rbx6;
    void** rax7;
    void** rbp8;
    int32_t eax9;
    int32_t eax10;
    int32_t eax11;
    int32_t ecx12;
    void** rax13;
    int32_t eax14;
    void** ebp15;
    void** eax16;
    void** rsi17;
    int64_t rdi18;
    int32_t eax19;
    int64_t rdx20;
    void** eax21;
    void** esi22;
    void** rcx23;
    void** rdi24;
    void** rax25;

    rdx3 = edx;
    r12d5 = rdx3;
    rbx6 = rdi;
    rax7 = fun_20e60(rsi);
    if (!rax7) 
        goto addr_4c700_2;
    rbp8 = rax7;
    eax9 = fun_21680(rax7, "EC");
    if (eax9) {
        eax10 = fun_21680(rbp8, "EC");
        if (!eax10) 
            goto addr_4c677_5;
        eax11 = fun_20400(rbp8, "EC");
        if (!eax11) 
            goto addr_4c700_2;
        if (eax11 != 4) 
            goto addr_4c725_8;
    } else {
        addr_4c651_9:
        return 1;
    }
    ecx12 = 0;
    addr_4c765_11:
    rax13 = *reinterpret_cast<void***>(rbx6 + 0xab8);
    if (rax13) {
        if (!*reinterpret_cast<void***>(rbx6 + 0xab0)) 
            goto addr_4c700_2;
        rdx3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx6 + 0xab0)) + reinterpret_cast<unsigned char>(rax13));
        do {
            if (*reinterpret_cast<void***>(rax13) == *reinterpret_cast<void***>(&ecx12)) 
                break;
            ++rax13;
        } while (rdx3 != rax13);
        goto addr_4c700_2;
        goto addr_4c677_5;
    }
    addr_4c725_8:
    rdx3 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx6 + 8) + 0xc0);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx3 + 96)) & 8 || (reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx6 + 8))) <= reinterpret_cast<signed char>(0x303) || reinterpret_cast<int1_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx6 + 8)) == 0x10000))) {
        eax14 = fun_21d90(rbp8, "EC");
        if (eax14 == 0x196) {
            ecx12 = 1;
            goto addr_4c765_11;
        } else {
            ecx12 = 2;
            if (eax14 != 0x197) 
                goto addr_4c700_2; else 
                goto addr_4c765_11;
        }
    } else {
        addr_4c677_5:
        ebp15 = reinterpret_cast<void**>(0);
        eax16 = ssl_get_EC_curve_nid(rbp8, "EC");
        rsi17 = eax16;
        *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
        if (eax16) {
            *reinterpret_cast<void***>(&rdi18) = eax16;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0;
            eax19 = tls1_nid2group_id(rdi18, rsi17, rdx3);
            rsi17 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<uint16_t*>(&eax19)));
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            ebp15 = rsi17;
        }
    }
    *reinterpret_cast<int32_t*>(&rdx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rdx20) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx6 + 56) == 0);
    eax21 = tls1_check_group_id(rbx6, rsi17, rdx20);
    if (!eax21) 
        goto addr_4c700_2;
    if (r12d5 && reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx6 + 0x898) + 28)) & 0x30000) {
        if (*reinterpret_cast<int16_t*>(&ebp15) == 23) {
            esi22 = reinterpret_cast<void**>("H");
        } else {
            esi22 = reinterpret_cast<void**>(0x31b);
            if (*reinterpret_cast<int16_t*>(&ebp15) != 24) 
                goto addr_4c700_2;
        }
        rcx23 = *reinterpret_cast<void***>(rbx6 + 0x1db0);
        if (!rcx23) {
            addr_4c700_2:
            return 0;
        } else {
            rdi24 = *reinterpret_cast<void***>(rbx6 + 0x1da8);
            *reinterpret_cast<int32_t*>(&rax25) = 0;
            *reinterpret_cast<int32_t*>(&rax25 + 4) = 0;
            do {
                if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi24 + reinterpret_cast<unsigned char>(rax25) * 8) + 28) == esi22) 
                    break;
                ++rax25;
            } while (rax25 != rcx23);
            goto addr_4c700_2;
        }
        goto addr_4c651_9;
    }
}