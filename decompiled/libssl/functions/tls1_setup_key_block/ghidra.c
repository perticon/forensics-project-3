undefined8 tls1_setup_key_block(long param_1)

{
  int iVar1;
  undefined8 uVar2;
  ulong uVar3;
  long lVar4;
  void *pvVar5;
  long lVar6;
  long in_FS_OFFSET;
  undefined *puVar7;
  undefined4 local_44;
  undefined8 local_40;
  undefined8 local_38;
  undefined local_30 [8];
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_44 = 0;
  local_28 = 0;
  if (*(long *)(param_1 + 0x310) == 0) {
    uVar3 = (ulong)*(uint *)(param_1 + 0xb2c);
    puVar7 = local_30;
    iVar1 = ssl_cipher_get_evp(*(undefined8 *)(param_1 + 0x9a8),*(undefined8 *)(param_1 + 0x918),
                               &local_40,&local_38,&local_44,&local_28);
    if (iVar1 == 0) {
      ossl_statem_send_fatal(param_1,0x50,puVar7,uVar3);
      uVar2 = 0;
      goto LAB_00146a5c;
    }
    ssl_evp_cipher_free(*(undefined8 *)(param_1 + 800));
    *(undefined8 *)(param_1 + 800) = local_40;
    ssl_evp_md_free(*(undefined8 *)(param_1 + 0x328));
    *(undefined8 *)(param_1 + 0x328) = local_38;
    *(undefined4 *)(param_1 + 0x330) = local_44;
    *(long *)(param_1 + 0x338) = local_28;
    iVar1 = EVP_CIPHER_get_key_length(local_40);
    lVar6 = iVar1 + local_28;
    iVar1 = EVP_CIPHER_get_mode(local_40);
    if (iVar1 == 6) {
LAB_00146c30:
      lVar4 = 4;
    }
    else {
      iVar1 = EVP_CIPHER_get_mode(local_40);
      if (iVar1 == 7) goto LAB_00146c30;
      iVar1 = EVP_CIPHER_get_iv_length(local_40);
      lVar4 = (long)iVar1;
    }
    ssl3_cleanup_key_block(param_1);
    lVar6 = (lVar6 + lVar4) * 2;
    pvVar5 = CRYPTO_malloc((int)lVar6,"ssl/t1_enc.c",0x1ef);
    if (pvVar5 == (void *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/t1_enc.c",0x1f0,"tls1_setup_key_block");
      ossl_statem_fatal(param_1,0x50,0xc0100,0);
      uVar2 = 0;
      goto LAB_00146a5c;
    }
    *(long *)(param_1 + 0x310) = lVar6;
    *(void **)(param_1 + 0x318) = pvVar5;
    uVar2 = tls1_PRF_constprop_0
                      (param_1,"key expansion",0xd,param_1 + 0x140,0x20,param_1 + 0x160,0x20,0,0,
                       *(long *)(param_1 + 0x918) + 0x50,
                       *(undefined8 *)(*(long *)(param_1 + 0x918) + 8),pvVar5,lVar6,1);
    if ((int)uVar2 == 0) goto LAB_00146a5c;
    if (((*(byte *)(param_1 + 0x9e9) & 8) == 0) && (**(int **)(param_1 + 8) < 0x302)) {
      *(undefined4 *)(param_1 + 0x180) = 1;
      lVar6 = *(long *)(*(long *)(param_1 + 0x918) + 0x2f8);
      if ((lVar6 != 0) && ((iVar1 = *(int *)(lVar6 + 0x24), iVar1 == 0x20 || (iVar1 == 4)))) {
        *(undefined4 *)(param_1 + 0x180) = 0;
      }
    }
  }
  uVar2 = 1;
LAB_00146a5c:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}