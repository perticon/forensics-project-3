undefined8 dtls1_buffer_message(int *param_1,int param_2)

{
  undefined auVar1 [16];
  undefined uVar2;
  undefined2 uVar3;
  long lVar4;
  long lVar5;
  byte bVar6;
  undefined extraout_AH;
  undefined *data;
  pitem *item;
  undefined8 uVar7;
  long in_FS_OFFSET;
  undefined auVar8 [16];
  ulong local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar7 = 0;
  if (*(long *)(param_1 + 0x28) != 0) goto LAB_00172d1b;
  data = (undefined *)dtls1_hm_fragment_new(*(undefined8 *)(param_1 + 0x26),0);
  if (data == (undefined *)0x0) goto LAB_00172d1b;
  memcpy(*(void **)(data + 0x58),*(void **)(*(long *)(param_1 + 0x22) + 8),
         *(size_t *)(param_1 + 0x26));
  lVar4 = *(long *)(param_1 + 0x12e);
  lVar5 = *(long *)(lVar4 + 0x140);
  if (param_2 == 0) {
    if (lVar5 + 0xcU != (ulong)(uint)param_1[0x26]) {
LAB_00172e87:
      dtls1_hm_fragment_free(data);
      uVar7 = 0;
      goto LAB_00172d1b;
    }
  }
  else if (lVar5 + 1 + (ulong)(*param_1 == 0x100) * 2 != (ulong)(uint)param_1[0x26])
  goto LAB_00172e87;
  uVar3 = *(undefined2 *)(lVar4 + 0x148);
  *(long *)(data + 8) = lVar5;
  *(undefined2 *)(data + 0x10) = uVar3;
  uVar2 = *(undefined *)(lVar4 + 0x138);
  *(long *)(data + 0x20) = lVar5;
  *data = uVar2;
  *(undefined8 *)(data + 0x18) = 0;
  *(int *)(data + 0x28) = param_2;
  uVar7 = *(undefined8 *)(param_1 + 0x246);
  auVar8 = *(undefined (*) [16])(param_1 + 0x222);
  auVar1 = *(undefined (*) [16])(param_1 + 0x21e);
  *(undefined8 *)(data + 0x40) = *(undefined8 *)(param_1 + 0x21a);
  *(undefined8 *)(data + 0x48) = uVar7;
  auVar8 = shufpd(auVar1,auVar8,2);
  *(undefined (*) [16])(data + 0x30) = auVar8;
  local_38 = 0;
  *(undefined2 *)(data + 0x50) = *(undefined2 *)(*(long *)(param_1 + 0x744) + 2);
  dtls1_get_queue_priority(uVar3,param_2);
  local_38._0_7_ = CONCAT16(extraout_AH,(undefined6)local_38);
  local_38 = local_38 & 0xff00000000000000 | (ulong)(uint7)local_38;
  bVar6 = dtls1_get_queue_priority(*(undefined2 *)(data + 0x10),*(undefined4 *)(data + 0x28));
  local_38 = local_38 & 0xffffffffffffff | (ulong)bVar6 << 0x38;
  item = pitem_new((uchar *)&local_38,data);
  if (item == (pitem *)0x0) {
    dtls1_hm_fragment_free(data);
    uVar7 = 0;
  }
  else {
    pqueue_insert(*(pqueue *)(*(long *)(param_1 + 0x12e) + 0x120),item);
    uVar7 = 1;
  }
LAB_00172d1b:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}