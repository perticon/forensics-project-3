undefined8 tls1_set_groups(void **param_1,long *param_2,long param_3,long param_4)

{
  ulong *puVar1;
  ulong uVar2;
  ushort uVar3;
  void *ptr;
  undefined8 uVar4;
  ulong uVar5;
  long lVar6;
  long in_FS_OFFSET;
  ulong local_50;
  ulong local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = 0;
  local_48 = 0;
  if (param_4 == 0) {
    ERR_new();
    ERR_set_debug("ssl/t1_lib.c",0x2c6,"tls1_set_groups");
    ERR_set_error(0x14,0x10f,0);
    uVar4 = 0;
  }
  else {
    lVar6 = 0;
    ptr = CRYPTO_malloc((int)param_4 * 2,"ssl/t1_lib.c",0x2c9);
    if (ptr == (void *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/t1_lib.c",0x2ca,"tls1_set_groups");
      ERR_set_error(0x14,0xc0100,0);
      uVar4 = 0;
    }
    else {
      do {
        uVar3 = tls1_nid2group_id(*(undefined4 *)(param_3 + lVar6 * 4));
        if ((uVar3 & 0xc0) != 0) {
LAB_00148910:
          CRYPTO_free(ptr);
          uVar4 = 0;
          goto LAB_00148926;
        }
        uVar5 = 1 << ((byte)uVar3 & 0x3f);
        if (uVar3 < 0x100) {
          if (uVar3 == 0) goto LAB_00148910;
          puVar1 = &local_50;
          uVar2 = local_50;
        }
        else {
          puVar1 = &local_48;
          uVar2 = local_48;
        }
        if ((uVar2 & uVar5) != 0) goto LAB_00148910;
        *(ushort *)((long)ptr + lVar6 * 2) = uVar3;
        lVar6 = lVar6 + 1;
        *puVar1 = uVar2 | uVar5;
      } while (param_4 != lVar6);
      CRYPTO_free(*param_1);
      *param_1 = ptr;
      *param_2 = param_4;
      uVar4 = 1;
    }
  }
LAB_00148926:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}