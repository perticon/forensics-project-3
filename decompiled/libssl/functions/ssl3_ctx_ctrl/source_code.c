long ssl3_ctx_ctrl(SSL_CTX *ctx, int cmd, long larg, void *parg)
{
    switch (cmd) {
#if !defined(OPENSSL_NO_DEPRECATED_3_0)
    case SSL_CTRL_SET_TMP_DH:
        {
            EVP_PKEY *pkdh = NULL;
            if (parg == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_PASSED_NULL_PARAMETER);
                return 0;
            }
            pkdh = ssl_dh_to_pkey(parg);
            if (pkdh == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
                return 0;
            }
            if (!SSL_CTX_set0_tmp_dh_pkey(ctx, pkdh)) {
                EVP_PKEY_free(pkdh);
                return 0;
            }
            return 1;
        }
    case SSL_CTRL_SET_TMP_DH_CB:
        {
            ERR_raise(ERR_LIB_SSL, ERR_R_SHOULD_NOT_HAVE_BEEN_CALLED);
            return 0;
        }
#endif
    case SSL_CTRL_SET_DH_AUTO:
        ctx->cert->dh_tmp_auto = larg;
        return 1;
#if !defined(OPENSSL_NO_DEPRECATED_3_0)
    case SSL_CTRL_SET_TMP_ECDH:
        {
            if (parg == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_PASSED_NULL_PARAMETER);
                return 0;
            }
            return ssl_set_tmp_ecdh_groups(&ctx->ext.supportedgroups,
                                           &ctx->ext.supportedgroups_len,
                                           parg);
        }
#endif                          /* !OPENSSL_NO_DEPRECATED_3_0 */
    case SSL_CTRL_SET_TLSEXT_SERVERNAME_ARG:
        ctx->ext.servername_arg = parg;
        break;
    case SSL_CTRL_SET_TLSEXT_TICKET_KEYS:
    case SSL_CTRL_GET_TLSEXT_TICKET_KEYS:
        {
            unsigned char *keys = parg;
            long tick_keylen = (sizeof(ctx->ext.tick_key_name) +
                                sizeof(ctx->ext.secure->tick_hmac_key) +
                                sizeof(ctx->ext.secure->tick_aes_key));
            if (keys == NULL)
                return tick_keylen;
            if (larg != tick_keylen) {
                ERR_raise(ERR_LIB_SSL, SSL_R_INVALID_TICKET_KEYS_LENGTH);
                return 0;
            }
            if (cmd == SSL_CTRL_SET_TLSEXT_TICKET_KEYS) {
                memcpy(ctx->ext.tick_key_name, keys,
                       sizeof(ctx->ext.tick_key_name));
                memcpy(ctx->ext.secure->tick_hmac_key,
                       keys + sizeof(ctx->ext.tick_key_name),
                       sizeof(ctx->ext.secure->tick_hmac_key));
                memcpy(ctx->ext.secure->tick_aes_key,
                       keys + sizeof(ctx->ext.tick_key_name) +
                       sizeof(ctx->ext.secure->tick_hmac_key),
                       sizeof(ctx->ext.secure->tick_aes_key));
            } else {
                memcpy(keys, ctx->ext.tick_key_name,
                       sizeof(ctx->ext.tick_key_name));
                memcpy(keys + sizeof(ctx->ext.tick_key_name),
                       ctx->ext.secure->tick_hmac_key,
                       sizeof(ctx->ext.secure->tick_hmac_key));
                memcpy(keys + sizeof(ctx->ext.tick_key_name) +
                       sizeof(ctx->ext.secure->tick_hmac_key),
                       ctx->ext.secure->tick_aes_key,
                       sizeof(ctx->ext.secure->tick_aes_key));
            }
            return 1;
        }

    case SSL_CTRL_GET_TLSEXT_STATUS_REQ_TYPE:
        return ctx->ext.status_type;

    case SSL_CTRL_SET_TLSEXT_STATUS_REQ_TYPE:
        ctx->ext.status_type = larg;
        break;

    case SSL_CTRL_SET_TLSEXT_STATUS_REQ_CB_ARG:
        ctx->ext.status_arg = parg;
        return 1;

    case SSL_CTRL_GET_TLSEXT_STATUS_REQ_CB_ARG:
        *(void**)parg = ctx->ext.status_arg;
        break;

    case SSL_CTRL_GET_TLSEXT_STATUS_REQ_CB:
        *(int (**)(SSL*, void*))parg = ctx->ext.status_cb;
        break;

#ifndef OPENSSL_NO_SRP
    case SSL_CTRL_SET_TLS_EXT_SRP_USERNAME:
        ctx->srp_ctx.srp_Mask |= SSL_kSRP;
        OPENSSL_free(ctx->srp_ctx.login);
        ctx->srp_ctx.login = NULL;
        if (parg == NULL)
            break;
        if (strlen((const char *)parg) > 255 || strlen((const char *)parg) < 1) {
            ERR_raise(ERR_LIB_SSL, SSL_R_INVALID_SRP_USERNAME);
            return 0;
        }
        if ((ctx->srp_ctx.login = OPENSSL_strdup((char *)parg)) == NULL) {
            ERR_raise(ERR_LIB_SSL, ERR_R_INTERNAL_ERROR);
            return 0;
        }
        break;
    case SSL_CTRL_SET_TLS_EXT_SRP_PASSWORD:
        ctx->srp_ctx.SRP_give_srp_client_pwd_callback =
            srp_password_from_info_cb;
        if (ctx->srp_ctx.info != NULL)
            OPENSSL_free(ctx->srp_ctx.info);
        if ((ctx->srp_ctx.info = OPENSSL_strdup((char *)parg)) == NULL) {
            ERR_raise(ERR_LIB_SSL, ERR_R_INTERNAL_ERROR);
            return 0;
        }
        break;
    case SSL_CTRL_SET_SRP_ARG:
        ctx->srp_ctx.srp_Mask |= SSL_kSRP;
        ctx->srp_ctx.SRP_cb_arg = parg;
        break;

    case SSL_CTRL_SET_TLS_EXT_SRP_STRENGTH:
        ctx->srp_ctx.strength = larg;
        break;
#endif

    case SSL_CTRL_SET_GROUPS:
        return tls1_set_groups(&ctx->ext.supportedgroups,
                               &ctx->ext.supportedgroups_len,
                               parg, larg);

    case SSL_CTRL_SET_GROUPS_LIST:
        return tls1_set_groups_list(ctx, &ctx->ext.supportedgroups,
                                    &ctx->ext.supportedgroups_len,
                                    parg);

    case SSL_CTRL_SET_SIGALGS:
        return tls1_set_sigalgs(ctx->cert, parg, larg, 0);

    case SSL_CTRL_SET_SIGALGS_LIST:
        return tls1_set_sigalgs_list(ctx->cert, parg, 0);

    case SSL_CTRL_SET_CLIENT_SIGALGS:
        return tls1_set_sigalgs(ctx->cert, parg, larg, 1);

    case SSL_CTRL_SET_CLIENT_SIGALGS_LIST:
        return tls1_set_sigalgs_list(ctx->cert, parg, 1);

    case SSL_CTRL_SET_CLIENT_CERT_TYPES:
        return ssl3_set_req_cert_type(ctx->cert, parg, larg);

    case SSL_CTRL_BUILD_CERT_CHAIN:
        return ssl_build_cert_chain(NULL, ctx, larg);

    case SSL_CTRL_SET_VERIFY_CERT_STORE:
        return ssl_cert_set_cert_store(ctx->cert, parg, 0, larg);

    case SSL_CTRL_SET_CHAIN_CERT_STORE:
        return ssl_cert_set_cert_store(ctx->cert, parg, 1, larg);

    case SSL_CTRL_GET_VERIFY_CERT_STORE:
        return ssl_cert_get_cert_store(ctx->cert, parg, 0);

    case SSL_CTRL_GET_CHAIN_CERT_STORE:
        return ssl_cert_get_cert_store(ctx->cert, parg, 1);

        /* A Thawte special :-) */
    case SSL_CTRL_EXTRA_CHAIN_CERT:
        if (ctx->extra_certs == NULL) {
            if ((ctx->extra_certs = sk_X509_new_null()) == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
                return 0;
            }
        }
        if (!sk_X509_push(ctx->extra_certs, (X509 *)parg)) {
            ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
            return 0;
        }
        break;

    case SSL_CTRL_GET_EXTRA_CHAIN_CERTS:
        if (ctx->extra_certs == NULL && larg == 0)
            *(STACK_OF(X509) **)parg = ctx->cert->key->chain;
        else
            *(STACK_OF(X509) **)parg = ctx->extra_certs;
        break;

    case SSL_CTRL_CLEAR_EXTRA_CHAIN_CERTS:
        OSSL_STACK_OF_X509_free(ctx->extra_certs);
        ctx->extra_certs = NULL;
        break;

    case SSL_CTRL_CHAIN:
        if (larg)
            return ssl_cert_set1_chain(NULL, ctx, (STACK_OF(X509) *)parg);
        else
            return ssl_cert_set0_chain(NULL, ctx, (STACK_OF(X509) *)parg);

    case SSL_CTRL_CHAIN_CERT:
        if (larg)
            return ssl_cert_add1_chain_cert(NULL, ctx, (X509 *)parg);
        else
            return ssl_cert_add0_chain_cert(NULL, ctx, (X509 *)parg);

    case SSL_CTRL_GET_CHAIN_CERTS:
        *(STACK_OF(X509) **)parg = ctx->cert->key->chain;
        break;

    case SSL_CTRL_SELECT_CURRENT_CERT:
        return ssl_cert_select_current(ctx->cert, (X509 *)parg);

    case SSL_CTRL_SET_CURRENT_CERT:
        return ssl_cert_set_current(ctx->cert, larg);

    default:
        return 0;
    }
    return 1;
}