long ssl3_ctx_ctrl(long param_1,int param_2,ulong param_3,ulong *param_4)

{
  undefined4 *puVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  int iVar4;
  ulong uVar5;
  size_t sVar6;
  long lVar7;
  EVP_PKEY *pkey;
  char *pcVar8;
  long lVar9;
  undefined8 uVar10;
  undefined4 uVar11;
  
  if (0x87 < param_2 - 3U) {
    return 0;
  }
  uVar11 = (undefined4)param_3;
  switch(param_2) {
  case 3:
    if (param_4 == (ulong *)0x0) {
      ERR_new();
      uVar10 = 0xecb;
      goto LAB_001298de;
    }
    pkey = (EVP_PKEY *)ssl_dh_to_pkey(param_4);
    if (pkey != (EVP_PKEY *)0x0) {
      iVar4 = SSL_CTX_set0_tmp_dh_pkey(param_1,pkey);
      if (iVar4 == 0) {
        EVP_PKEY_free(pkey);
        return 0;
      }
      return 1;
    }
    ERR_new();
    uVar10 = 0xed0;
    goto LAB_00129b42;
  case 4:
    if (param_4 != (ulong *)0x0) {
      iVar4 = ssl_set_tmp_ecdh_groups(param_1 + 0x280,param_1 + 0x278,param_4);
      return (long)iVar4;
    }
    ERR_new();
    uVar10 = 0xee6;
LAB_001298de:
    ERR_set_debug("ssl/s3_lib.c",uVar10,"ssl3_ctx_ctrl");
    ERR_set_error(0x14,0xc0102,0);
  default:
switchD_001298ca_caseD_5:
    lVar7 = 0;
    break;
  case 6:
    ERR_new();
    ERR_set_debug("ssl/s3_lib.c",0xedb,"ssl3_ctx_ctrl");
    ERR_set_error(0x14,0xc0101,0);
    lVar7 = 0;
    break;
  case 0xe:
    lVar7 = *(long *)(param_1 + 0x110);
    if (lVar7 == 0) {
      lVar7 = OPENSSL_sk_new_null();
      *(long *)(param_1 + 0x110) = lVar7;
      if (lVar7 == 0) {
        ERR_new(0);
        uVar10 = 0xf79;
        goto LAB_00129b42;
      }
    }
    iVar4 = OPENSSL_sk_push(lVar7,param_4);
    if (iVar4 != 0) {
      return 1;
    }
    ERR_new();
    uVar10 = 0xf7e;
LAB_00129b42:
    ERR_set_debug("ssl/s3_lib.c",uVar10,"ssl3_ctx_ctrl");
    ERR_set_error(0x14,0xc0100,0);
    lVar7 = 0;
    break;
  case 0x36:
    *(ulong **)(param_1 + 0x220) = param_4;
    lVar7 = 1;
    break;
  case 0x3a:
  case 0x3b:
    if (param_4 == (ulong *)0x0) {
      lVar7 = 0x50;
    }
    else if (param_3 == 0x50) {
      if (param_2 == 0x3b) {
        uVar11 = *(undefined4 *)((long)param_4 + 4);
        uVar2 = *(undefined4 *)(param_4 + 1);
        uVar3 = *(undefined4 *)((long)param_4 + 0xc);
        puVar1 = *(undefined4 **)(param_1 + 0x238);
        *(undefined4 *)(param_1 + 0x228) = *(undefined4 *)param_4;
        *(undefined4 *)(param_1 + 0x22c) = uVar11;
        *(undefined4 *)(param_1 + 0x230) = uVar2;
        *(undefined4 *)(param_1 + 0x234) = uVar3;
        uVar11 = *(undefined4 *)((long)param_4 + 0x14);
        uVar2 = *(undefined4 *)(param_4 + 3);
        uVar3 = *(undefined4 *)((long)param_4 + 0x1c);
        *puVar1 = *(undefined4 *)(param_4 + 2);
        puVar1[1] = uVar11;
        puVar1[2] = uVar2;
        puVar1[3] = uVar3;
        uVar11 = *(undefined4 *)((long)param_4 + 0x24);
        uVar2 = *(undefined4 *)(param_4 + 5);
        uVar3 = *(undefined4 *)((long)param_4 + 0x2c);
        puVar1[4] = *(undefined4 *)(param_4 + 4);
        puVar1[5] = uVar11;
        puVar1[6] = uVar2;
        puVar1[7] = uVar3;
        lVar7 = *(long *)(param_1 + 0x238);
        uVar11 = *(undefined4 *)((long)param_4 + 0x34);
        uVar2 = *(undefined4 *)(param_4 + 7);
        uVar3 = *(undefined4 *)((long)param_4 + 0x3c);
        *(undefined4 *)(lVar7 + 0x20) = *(undefined4 *)(param_4 + 6);
        *(undefined4 *)(lVar7 + 0x24) = uVar11;
        *(undefined4 *)(lVar7 + 0x28) = uVar2;
        *(undefined4 *)(lVar7 + 0x2c) = uVar3;
        uVar11 = *(undefined4 *)((long)param_4 + 0x44);
        uVar2 = *(undefined4 *)(param_4 + 9);
        uVar3 = *(undefined4 *)((long)param_4 + 0x4c);
        *(undefined4 *)(lVar7 + 0x30) = *(undefined4 *)(param_4 + 8);
        *(undefined4 *)(lVar7 + 0x34) = uVar11;
        *(undefined4 *)(lVar7 + 0x38) = uVar2;
        *(undefined4 *)(lVar7 + 0x3c) = uVar3;
        lVar7 = 1;
      }
      else {
        uVar11 = *(undefined4 *)(param_1 + 0x22c);
        uVar2 = *(undefined4 *)(param_1 + 0x230);
        uVar3 = *(undefined4 *)(param_1 + 0x234);
        *(undefined4 *)param_4 = *(undefined4 *)(param_1 + 0x228);
        *(undefined4 *)((long)param_4 + 4) = uVar11;
        *(undefined4 *)(param_4 + 1) = uVar2;
        *(undefined4 *)((long)param_4 + 0xc) = uVar3;
        puVar1 = *(undefined4 **)(param_1 + 0x238);
        uVar11 = puVar1[1];
        uVar2 = puVar1[2];
        uVar3 = puVar1[3];
        *(undefined4 *)(param_4 + 2) = *puVar1;
        *(undefined4 *)((long)param_4 + 0x14) = uVar11;
        *(undefined4 *)(param_4 + 3) = uVar2;
        *(undefined4 *)((long)param_4 + 0x1c) = uVar3;
        uVar11 = puVar1[5];
        uVar2 = puVar1[6];
        uVar3 = puVar1[7];
        *(undefined4 *)(param_4 + 4) = puVar1[4];
        *(undefined4 *)((long)param_4 + 0x24) = uVar11;
        *(undefined4 *)(param_4 + 5) = uVar2;
        *(undefined4 *)((long)param_4 + 0x2c) = uVar3;
        lVar9 = *(long *)(param_1 + 0x238);
        uVar11 = *(undefined4 *)(lVar9 + 0x24);
        uVar2 = *(undefined4 *)(lVar9 + 0x28);
        uVar3 = *(undefined4 *)(lVar9 + 0x2c);
        *(undefined4 *)(param_4 + 6) = *(undefined4 *)(lVar9 + 0x20);
        *(undefined4 *)((long)param_4 + 0x34) = uVar11;
        *(undefined4 *)(param_4 + 7) = uVar2;
        *(undefined4 *)((long)param_4 + 0x3c) = uVar3;
        uVar11 = *(undefined4 *)(lVar9 + 0x34);
        uVar2 = *(undefined4 *)(lVar9 + 0x38);
        uVar3 = *(undefined4 *)(lVar9 + 0x3c);
        lVar7 = 1;
        *(undefined4 *)(param_4 + 8) = *(undefined4 *)(lVar9 + 0x30);
        *(undefined4 *)((long)param_4 + 0x44) = uVar11;
        *(undefined4 *)(param_4 + 9) = uVar2;
        *(undefined4 *)((long)param_4 + 0x4c) = uVar3;
      }
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/s3_lib.c",0xefb,"ssl3_ctx_ctrl");
      ERR_set_error(0x14,0x145,0);
      lVar7 = 0;
    }
    break;
  case 0x40:
    *(ulong **)(param_1 + 600) = param_4;
    lVar7 = 1;
    break;
  case 0x41:
    *(undefined4 *)(param_1 + 0x260) = uVar11;
    lVar7 = 1;
    break;
  case 0x4e:
    *(ulong *)(param_1 + 0x390) = *(ulong *)(param_1 + 0x390) | 0x20;
    lVar7 = 1;
    *(ulong **)(param_1 + 0x318) = param_4;
    break;
  case 0x4f:
    *(ulong *)(param_1 + 0x390) = *(ulong *)(param_1 + 0x390) | 0x20;
    CRYPTO_free(*(void **)(param_1 + 0x338));
    *(undefined8 *)(param_1 + 0x338) = 0;
    if (param_4 == (ulong *)0x0) {
      return 1;
    }
    sVar6 = strlen((char *)param_4);
    if ((0xff < sVar6) || (*(char *)param_4 == '\0')) {
      ERR_new();
      ERR_set_debug("ssl/s3_lib.c",0xf31,"ssl3_ctx_ctrl");
      ERR_set_error(0x14,0x165,0);
      return 0;
    }
    pcVar8 = CRYPTO_strdup((char *)param_4,"ssl/s3_lib.c",0xf34);
    *(char **)(param_1 + 0x338) = pcVar8;
    if (pcVar8 != (char *)0x0) {
      return 1;
    }
    ERR_new();
    uVar10 = 0xf35;
    goto LAB_00129a6b;
  case 0x50:
    *(undefined4 *)(param_1 + 0x388) = uVar11;
    lVar7 = 1;
    break;
  case 0x51:
    *(code **)(param_1 + 0x330) = srp_password_from_info_cb;
    if (*(void **)(param_1 + 0x380) != (void *)0x0) {
      CRYPTO_free(*(void **)(param_1 + 0x380));
    }
    pcVar8 = CRYPTO_strdup((char *)param_4,"ssl/s3_lib.c",0xf3e);
    *(char **)(param_1 + 0x380) = pcVar8;
    if (pcVar8 != (char *)0x0) {
      return 1;
    }
    ERR_new();
    uVar10 = 0xf3f;
LAB_00129a6b:
    ERR_set_debug("ssl/s3_lib.c",uVar10,"ssl3_ctx_ctrl");
    ERR_set_error(0x14,0xc0103,0);
    lVar7 = 0;
    break;
  case 0x52:
    uVar5 = *(ulong *)(param_1 + 0x110);
    if ((param_3 | uVar5) != 0) goto LAB_001299a3;
  case 0x73:
    uVar5 = *(ulong *)(**(long **)(param_1 + 0x158) + 0x10);
LAB_001299a3:
    *param_4 = uVar5;
    return 1;
  case 0x53:
    OSSL_STACK_OF_X509_free(*(undefined8 *)(param_1 + 0x110));
    lVar7 = 1;
    *(undefined8 *)(param_1 + 0x110) = 0;
    break;
  case 0x58:
    if (param_3 == 0) {
      iVar4 = ssl_cert_set0_chain(0,param_1,param_4);
      lVar7 = (long)iVar4;
    }
    else {
      iVar4 = ssl_cert_set1_chain();
      lVar7 = (long)iVar4;
    }
    break;
  case 0x59:
    if (param_3 == 0) {
      iVar4 = ssl_cert_add0_chain_cert(0,param_1,param_4);
      lVar7 = (long)iVar4;
    }
    else {
      iVar4 = ssl_cert_add1_chain_cert();
      lVar7 = (long)iVar4;
    }
    break;
  case 0x5b:
    iVar4 = tls1_set_groups(param_1 + 0x280,param_1 + 0x278,param_4,param_3);
    lVar7 = (long)iVar4;
    break;
  case 0x5c:
    iVar4 = tls1_set_groups_list(param_1,param_1 + 0x280,param_1 + 0x278);
    lVar7 = (long)iVar4;
    break;
  case 0x61:
    iVar4 = tls1_set_sigalgs(*(undefined8 *)(param_1 + 0x158),param_4,param_3,0);
    lVar7 = (long)iVar4;
    break;
  case 0x62:
    iVar4 = tls1_set_sigalgs_list(*(undefined8 *)(param_1 + 0x158),param_4,0);
    lVar7 = (long)iVar4;
    break;
  case 0x65:
    iVar4 = tls1_set_sigalgs(*(undefined8 *)(param_1 + 0x158),param_4,param_3,1);
    lVar7 = (long)iVar4;
    break;
  case 0x66:
    iVar4 = tls1_set_sigalgs_list(*(undefined8 *)(param_1 + 0x158),param_4,1);
    lVar7 = (long)iVar4;
    break;
  case 0x68:
    lVar7 = *(long *)(param_1 + 0x158);
    CRYPTO_free(*(void **)(lVar7 + 0x188));
    *(undefined8 *)(lVar7 + 0x188) = 0;
    *(undefined8 *)(lVar7 + 400) = 0;
    if (param_4 == (ulong *)0x0) {
      return 1;
    }
    if (param_3 == 0) {
      return 1;
    }
    if (param_3 < 0x100) {
      lVar9 = CRYPTO_memdup(param_4,param_3,"ssl/s3_lib.c",0x112b);
      *(long *)(lVar7 + 0x188) = lVar9;
      if (lVar9 != 0) {
        *(ulong *)(lVar7 + 400) = param_3;
        return 1;
      }
    }
    goto switchD_001298ca_caseD_5;
  case 0x69:
    iVar4 = ssl_build_cert_chain(0,param_1,param_3 & 0xffffffff);
    lVar7 = (long)iVar4;
    break;
  case 0x6a:
    iVar4 = ssl_cert_set_cert_store(*(undefined8 *)(param_1 + 0x158),param_4,0,param_3 & 0xffffffff)
    ;
    lVar7 = (long)iVar4;
    break;
  case 0x6b:
    iVar4 = ssl_cert_set_cert_store(*(undefined8 *)(param_1 + 0x158),param_4,1,param_3 & 0xffffffff)
    ;
    lVar7 = (long)iVar4;
    break;
  case 0x74:
    iVar4 = ssl_cert_select_current(*(undefined8 *)(param_1 + 0x158),param_4);
    lVar7 = (long)iVar4;
    break;
  case 0x75:
    iVar4 = ssl_cert_set_current(*(undefined8 *)(param_1 + 0x158),param_3);
    lVar7 = (long)iVar4;
    break;
  case 0x76:
    *(undefined4 *)(*(long *)(param_1 + 0x158) + 0x18) = uVar11;
    lVar7 = 1;
    break;
  case 0x7f:
    lVar7 = (long)*(int *)(param_1 + 0x260);
    break;
  case 0x80:
    *param_4 = *(ulong *)(param_1 + 0x250);
    lVar7 = 1;
    break;
  case 0x81:
    *param_4 = *(ulong *)(param_1 + 600);
    lVar7 = 1;
    break;
  case 0x89:
    iVar4 = ssl_cert_get_cert_store(*(undefined8 *)(param_1 + 0x158),param_4,0);
    lVar7 = (long)iVar4;
    break;
  case 0x8a:
    iVar4 = ssl_cert_get_cert_store(*(undefined8 *)(param_1 + 0x158),param_4,1);
    lVar7 = (long)iVar4;
  }
  return lVar7;
}