bool RECORD_LAYER_processed_read_pending(long param_1)

{
  ulong uVar1;
  ulong uVar2;
  int *piVar3;
  
  uVar1 = *(ulong *)(param_1 + 0x10);
  if (uVar1 == 0) {
    uVar2 = 0;
  }
  else {
    piVar3 = (int *)(param_1 + 0x688);
    uVar2 = 0;
    while (*piVar3 != 0) {
      uVar2 = uVar2 + 1;
      piVar3 = piVar3 + 0x14;
      if (uVar1 == uVar2) {
        return false;
      }
    }
  }
  return uVar2 < uVar1;
}