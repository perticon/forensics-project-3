undefined tls_construct_ctos_psk(long param_1,undefined8 param_2,undefined8 param_3,ulong param_4)

{
  bool bVar1;
  uint uVar2;
  int iVar3;
  long lVar4;
  int *piVar5;
  long lVar6;
  time_t tVar7;
  undefined8 extraout_RDX;
  long lVar8;
  undefined8 uVar9;
  long lVar10;
  long in_FS_OFFSET;
  undefined uVar11;
  undefined auVar12 [16];
  uint local_70;
  undefined8 local_60;
  long local_58;
  undefined8 local_50;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  piVar5 = *(int **)(param_1 + 0x918);
  local_50 = 0;
  local_48 = 0;
  *(undefined4 *)(param_1 + 0xb50) = 0;
  if (*piVar5 != 0x304) {
LAB_001624e0:
    uVar11 = 2;
    goto LAB_00162557;
  }
  lVar10 = *(long *)(piVar5 + 0xd0);
  if (lVar10 != 0) {
    if (*(int *)(param_1 + 0x8e8) == 1) {
LAB_00162740:
      lVar8 = ssl_handshake_md(param_1);
      piVar5 = *(int **)(param_1 + 0x918);
      if (*(long *)(piVar5 + 0xd0) != 0) goto LAB_0016245f;
      local_70 = 0;
      lVar4 = *(long *)(param_1 + 0x920);
      lVar6 = 0;
LAB_001624d4:
      if (lVar4 == 0) goto LAB_001624e0;
      bVar1 = false;
      lVar10 = 0;
      goto LAB_001623e9;
    }
    lVar8 = 0;
LAB_0016245f:
    if (*(long *)(piVar5 + 0xbe) != 0) {
      lVar6 = ssl_md(*(undefined8 *)(param_1 + 0x9a8),
                     *(undefined4 *)(*(long *)(piVar5 + 0xbe) + 0x40));
      if ((lVar6 == 0) || ((*(int *)(param_1 + 0x8e8) == 1 && (lVar8 != lVar6)))) {
LAB_001624c5:
        local_70 = 0;
        lVar4 = *(long *)(param_1 + 0x920);
        goto LAB_001624d4;
      }
      tVar7 = time((time_t *)0x0);
      lVar10 = *(long *)(param_1 + 0x918);
      local_70 = (int)tVar7 - *(int *)(lVar10 + 0x2e0);
      if (local_70 != 0) {
        uVar2 = local_70 - 1;
        param_4 = (ulong)uVar2;
        if (*(ulong *)(lVar10 + 0x348) < (ulong)uVar2) goto LAB_001624c5;
        local_70 = uVar2 * 1000;
        if ((uVar2 != 0) && (local_70 / 1000 != uVar2)) {
          lVar4 = *(long *)(param_1 + 0x920);
          goto LAB_001624d4;
        }
      }
      param_4 = (ulong)*(uint *)(lVar10 + 0x350);
      local_70 = local_70 + *(uint *)(lVar10 + 0x350);
      iVar3 = EVP_MD_get_size(lVar6);
      lVar4 = *(long *)(param_1 + 0x920);
      *(int *)(param_1 + 0xb50) = *(int *)(param_1 + 0xb50) + 1;
      bVar1 = true;
      lVar10 = (long)iVar3;
      if (lVar4 != 0) goto LAB_001623e9;
      lVar8 = 0;
      goto LAB_001624fb;
    }
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x3fe,"tls_construct_ctos_psk");
    uVar9 = 0xc0103;
    goto LAB_00162546;
  }
  lVar4 = *(long *)(param_1 + 0x920);
  if (lVar4 == 0) goto LAB_001624e0;
  if (*(int *)(param_1 + 0x8e8) == 1) goto LAB_00162740;
  bVar1 = false;
  lVar6 = 0;
  lVar8 = 0;
  local_70 = 0;
LAB_001623e9:
  lVar4 = ssl_md(*(undefined8 *)(param_1 + 0x9a8),*(undefined4 *)(*(long *)(lVar4 + 0x2f8) + 0x40));
  if (lVar4 == 0) {
    ERR_new();
    uVar9 = 0x450;
LAB_00162433:
    ERR_set_debug("ssl/statem/extensions_clnt.c",uVar9,"tls_construct_ctos_psk");
    uVar9 = 0xdb;
  }
  else {
    if ((*(int *)(param_1 + 0x8e8) == 1) && (lVar4 != lVar8)) {
      ERR_new();
      uVar9 = 0x459;
      goto LAB_00162433;
    }
    iVar3 = EVP_MD_get_size(lVar4);
    lVar8 = (long)iVar3;
LAB_001624fb:
    iVar3 = WPACKET_put_bytes__(param_2,0x29,2);
    if (((iVar3 == 0) || (iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 == 0)) ||
       (iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 == 0)) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x464,"tls_construct_ctos_psk");
      uVar9 = 0xc0103;
    }
    else {
      if (bVar1) {
        param_4 = 2;
        iVar3 = WPACKET_sub_memcpy__
                          (param_2,*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x338),
                           *(undefined8 *)(*(long *)(param_1 + 0x918) + 0x340));
        if ((iVar3 == 0) || (iVar3 = WPACKET_put_bytes__(param_2,local_70,4), iVar3 == 0)) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_clnt.c",0x46c,"tls_construct_ctos_psk");
          uVar9 = 0xc0103;
          goto LAB_00162546;
        }
      }
      if (*(long *)(param_1 + 0x920) != 0) {
        param_4 = 2;
        iVar3 = WPACKET_sub_memcpy__
                          (param_2,*(undefined8 *)(param_1 + 0x928),*(undefined8 *)(param_1 + 0x930)
                          );
        if ((iVar3 == 0) || (iVar3 = WPACKET_put_bytes__(param_2,0,4), iVar3 == 0)) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_clnt.c",0x475,"tls_construct_ctos_psk");
          uVar9 = 0xc0103;
          goto LAB_00162546;
        }
        *(int *)(param_1 + 0xb50) = *(int *)(param_1 + 0xb50) + 1;
      }
      iVar3 = WPACKET_close(param_2);
      if (((iVar3 != 0) && (iVar3 = WPACKET_get_total_written(param_2,&local_60), iVar3 != 0)) &&
         (iVar3 = WPACKET_start_sub_packet_len__(param_2), iVar3 != 0)) {
        if (bVar1) {
          param_4 = 1;
          iVar3 = WPACKET_sub_allocate_bytes__(param_2,lVar10,&local_50);
          if (iVar3 == 0) goto LAB_00162810;
        }
        if (*(long *)(param_1 + 0x920) != 0) {
          param_4 = 1;
          iVar3 = WPACKET_sub_allocate_bytes__(param_2,lVar8,&local_48);
          if (iVar3 == 0) goto LAB_00162810;
        }
        iVar3 = WPACKET_close(param_2);
        if (((iVar3 != 0) && (iVar3 = WPACKET_close(param_2), iVar3 != 0)) &&
           ((iVar3 = WPACKET_get_total_written(param_2,&local_58), iVar3 != 0 &&
            (iVar3 = WPACKET_fill_lengths(param_2), iVar3 != 0)))) {
          auVar12 = WPACKET_get_curr(param_2);
          uVar9 = SUB168(auVar12 >> 0x40,0);
          local_58 = SUB168(auVar12,0) - local_58;
          if (bVar1) {
            iVar3 = tls_psk_do_binder(param_1,lVar6,local_58,local_60,0,local_50,
                                      *(undefined8 *)(param_1 + 0x918),1,0,param_4);
            uVar11 = 0;
            uVar9 = extraout_RDX;
            if (iVar3 != 1) goto LAB_00162557;
          }
          if (*(long *)(param_1 + 0x920) == 0) {
            uVar11 = 1;
          }
          else {
            iVar3 = tls_psk_do_binder(param_1,lVar4,local_58,local_60,0,local_48,
                                      *(long *)(param_1 + 0x920),1,1,uVar9);
            uVar11 = iVar3 == 1;
          }
          goto LAB_00162557;
        }
      }
LAB_00162810:
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x48a,"tls_construct_ctos_psk");
      uVar9 = 0xc0103;
    }
  }
LAB_00162546:
  ossl_statem_fatal(param_1,0x50,uVar9,0);
  uVar11 = 0;
LAB_00162557:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar11;
}