undefined8 tls1_check_group_id(long param_1,short param_2,int param_3)

{
  int iVar1;
  undefined8 uVar2;
  long lVar3;
  long in_FS_OFFSET;
  long local_30;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = 0;
  if (param_2 == 0) goto LAB_00148b72;
  if (((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) == 0) ||
     (*(long *)(param_1 + 0x2e0) == 0)) {
LAB_00148ba0:
    if (param_3 == 0) {
LAB_00148be0:
      iVar1 = tls_group_allowed(param_1,param_2,0x20006);
      if (iVar1 != 0) {
        uVar2 = 1;
        if ((*(int *)(param_1 + 0x38) == 0) || (*(long *)(param_1 + 0xad0) == 0)) goto LAB_00148b72;
        lVar3 = 0;
        do {
          if (param_2 == *(short *)(*(long *)(param_1 + 0xad8) + lVar3 * 2)) {
            uVar2 = 1;
            goto LAB_00148b72;
          }
          lVar3 = lVar3 + 1;
        } while (*(long *)(param_1 + 0xad0) != lVar3);
      }
    }
    else {
      tls1_get_supported_groups(param_1,&local_30,&local_28);
      if (local_28 != 0) {
        lVar3 = 0;
        do {
          if (param_2 == *(short *)(local_30 + lVar3 * 2)) goto LAB_00148be0;
          lVar3 = lVar3 + 1;
        } while (local_28 != lVar3);
      }
    }
  }
  else {
    iVar1 = *(int *)(*(long *)(param_1 + 0x2e0) + 0x18);
    if (iVar1 == 0x300c02b) {
      if (param_2 != 0x17) goto LAB_00148b72;
      goto LAB_00148ba0;
    }
    if ((param_2 == 0x18) && (iVar1 == 0x300c02c)) goto LAB_00148ba0;
  }
  uVar2 = 0;
LAB_00148b72:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar2;
}