void tls_engine_load_ssl_client_cert(SSL *param_1,X509 **param_2,EVP_PKEY **param_3)

{
  stack_st_X509_NAME *ca_dn;
  
  ca_dn = SSL_get_client_CA_list(param_1);
  ENGINE_load_ssl_client_cert
            (*(ENGINE **)(*(long *)&param_1[3].ex_data.dummy + 0x200),param_1,ca_dn,param_2,param_3,
             (stack_st_X509 **)0x0,(UI_METHOD *)0x0,(void *)0x0);
  return;
}