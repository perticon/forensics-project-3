int tls_engine_load_ssl_client_cert(SSL *s, X509 **px509, EVP_PKEY **ppkey)
{
    return ENGINE_load_ssl_client_cert(s->ctx->client_cert_engine, s,
                                       SSL_get_client_CA_list(s),
                                       px509, ppkey, NULL, NULL, NULL);
}