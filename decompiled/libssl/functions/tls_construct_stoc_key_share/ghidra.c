ulong tls_construct_stoc_key_share(long param_1,undefined8 param_2)

{
  long lVar1;
  undefined2 uVar2;
  int iVar3;
  long lVar4;
  EVP_PKEY *pkey;
  undefined8 uVar5;
  EVP_PKEY *pEVar6;
  long in_FS_OFFSET;
  void *local_40;
  void *local_38;
  long local_30;
  
  pEVar6 = (EVP_PKEY *)(ulong)*(uint *)(param_1 + 0x8e8);
  lVar1 = *(long *)(param_1 + 0x4b0);
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(uint *)(param_1 + 0x8e8) == 1) {
    if (lVar1 == 0) {
      iVar3 = WPACKET_put_bytes__(param_2,0x33,2);
      if ((iVar3 != 0) && (iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 != 0)) {
        uVar2 = ssl_group_id_internal_to_tls13(*(undefined2 *)(param_1 + 0x4ae));
        iVar3 = WPACKET_put_bytes__(param_2,uVar2,2);
        if ((iVar3 != 0) && (iVar3 = WPACKET_close(param_2), iVar3 != 0)) goto LAB_00168cee;
      }
      ERR_new();
      uVar5 = 0x63f;
      goto LAB_00168cc9;
    }
  }
  else if (lVar1 == 0) {
    if ((*(int *)(param_1 + 0x4d0) == 0) ||
       (iVar3 = tls13_generate_handshake_secret(param_1,0,0), iVar3 == 0)) {
      ERR_new();
      uVar5 = 0x649;
      goto LAB_00168cc9;
    }
  }
  else if ((*(int *)(param_1 + 0x4d0) == 0) || ((*(byte *)(param_1 + 0xb28) & 2) != 0)) {
    iVar3 = WPACKET_put_bytes__(param_2,0x33,2);
    if (((iVar3 == 0) || (iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 == 0)) ||
       (iVar3 = WPACKET_put_bytes__(param_2,*(undefined2 *)(param_1 + 0x4ae),2), iVar3 == 0)) {
      ERR_new();
      uVar5 = 0x659;
LAB_00168cc9:
      ERR_set_debug("ssl/statem/extensions_srvr.c",uVar5,"tls_construct_stoc_key_share");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
    else {
      lVar4 = tls1_group_id_lookup
                        (*(undefined8 *)(param_1 + 0x9a8),*(undefined2 *)(param_1 + 0x4ae));
      if (lVar4 == 0) {
        ERR_new();
        uVar5 = 0x65e;
        goto LAB_00168cc9;
      }
      if (*(char *)(lVar4 + 0x30) == '\0') {
        pkey = (EVP_PKEY *)ssl_generate_pkey(param_1,lVar1);
        if (pkey == (EVP_PKEY *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_srvr.c",0x666,"tls_construct_stoc_key_share");
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
          pEVar6 = pkey;
          goto LAB_00168cee;
        }
        lVar4 = EVP_PKEY_get1_encoded_public_key(pkey,&local_38);
        if (lVar4 == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_srvr.c",0x66d,"tls_construct_stoc_key_share");
          ossl_statem_fatal(param_1,0x50,0x80010,0);
          EVP_PKEY_free(pkey);
          pEVar6 = (EVP_PKEY *)0x0;
          goto LAB_00168cee;
        }
        iVar3 = WPACKET_sub_memcpy__(param_2,local_38,lVar4,2);
        if ((iVar3 == 0) || (iVar3 = WPACKET_close(param_2), iVar3 == 0)) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_srvr.c",0x674,"tls_construct_stoc_key_share");
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
          EVP_PKEY_free(pkey);
          CRYPTO_free(local_38);
          pEVar6 = (EVP_PKEY *)0x0;
          goto LAB_00168cee;
        }
        CRYPTO_free(local_38);
        *(EVP_PKEY **)(param_1 + 0x2e8) = pkey;
        pEVar6 = (EVP_PKEY *)0x0;
        iVar3 = ssl_derive(param_1,pkey,lVar1,1);
        if (iVar3 == 0) goto LAB_00168cee;
LAB_00168e78:
        *(undefined *)(param_1 + 0x4ad) = 1;
        pEVar6 = (EVP_PKEY *)0x1;
        goto LAB_00168cee;
      }
      local_40 = (void *)0x0;
      local_38 = (void *)0x0;
      iVar3 = ssl_encapsulate(param_1,lVar1,&local_40,&local_38,0);
      if (iVar3 != 0) {
        if (local_38 == (void *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_srvr.c",0x694,"tls_construct_stoc_key_share");
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
          CRYPTO_free(local_40);
        }
        else {
          iVar3 = WPACKET_sub_memcpy__(param_2,local_40,local_38,2);
          if ((iVar3 == 0) || (iVar3 = WPACKET_close(param_2), iVar3 == 0)) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x69b,"tls_construct_stoc_key_share");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            CRYPTO_free(local_40);
          }
          else {
            CRYPTO_free(local_40);
            iVar3 = ssl_gensecret(param_1,*(undefined8 *)(param_1 + 0x360),
                                  *(undefined8 *)(param_1 + 0x368));
            if (iVar3 != 0) goto LAB_00168e78;
          }
        }
      }
    }
    pEVar6 = (EVP_PKEY *)0x0;
    goto LAB_00168cee;
  }
  pEVar6 = (EVP_PKEY *)0x2;
LAB_00168cee:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return (ulong)pEVar6 & 0xffffffff;
}