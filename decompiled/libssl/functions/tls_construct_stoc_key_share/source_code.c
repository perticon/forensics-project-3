EXT_RETURN tls_construct_stoc_key_share(SSL *s, WPACKET *pkt,
                                        unsigned int context, X509 *x,
                                        size_t chainidx)
{
#ifndef OPENSSL_NO_TLS1_3
    unsigned char *encodedPoint;
    size_t encoded_pt_len = 0;
    EVP_PKEY *ckey = s->s3.peer_tmp, *skey = NULL;
    const TLS_GROUP_INFO *ginf = NULL;

    if (s->hello_retry_request == SSL_HRR_PENDING) {
        if (ckey != NULL) {
            /* Original key_share was acceptable so don't ask for another one */
            return EXT_RETURN_NOT_SENT;
        }
        if (!WPACKET_put_bytes_u16(pkt, TLSEXT_TYPE_key_share)
                || !WPACKET_start_sub_packet_u16(pkt)
                || !WPACKET_put_bytes_u16(pkt, ssl_group_id_internal_to_tls13(
                                          s->s3.group_id))
                || !WPACKET_close(pkt)) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
            return EXT_RETURN_FAIL;
        }

        return EXT_RETURN_SENT;
    }

    if (ckey == NULL) {
        /* No key_share received from client - must be resuming */
        if (!s->hit || !tls13_generate_handshake_secret(s, NULL, 0)) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
            return EXT_RETURN_FAIL;
        }
        return EXT_RETURN_NOT_SENT;
    }
    if (s->hit && (s->ext.psk_kex_mode & TLSEXT_KEX_MODE_FLAG_KE_DHE) == 0) {
        /*
         * PSK ('hit') and explicitly not doing DHE (if the client sent the
         * DHE option we always take it); don't send key share.
         */
        return EXT_RETURN_NOT_SENT;
    }

    if (!WPACKET_put_bytes_u16(pkt, TLSEXT_TYPE_key_share)
            || !WPACKET_start_sub_packet_u16(pkt)
            || !WPACKET_put_bytes_u16(pkt, s->s3.group_id)) {
        SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
        return EXT_RETURN_FAIL;
    }

    if ((ginf = tls1_group_id_lookup(s->ctx, s->s3.group_id)) == NULL) {
        SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
        return EXT_RETURN_FAIL;
    }

    if (!ginf->is_kem) {
        /* Regular KEX */
        skey = ssl_generate_pkey(s, ckey);
        if (skey == NULL) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_MALLOC_FAILURE);
            return EXT_RETURN_FAIL;
        }

        /* Generate encoding of server key */
        encoded_pt_len = EVP_PKEY_get1_encoded_public_key(skey, &encodedPoint);
        if (encoded_pt_len == 0) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_EC_LIB);
            EVP_PKEY_free(skey);
            return EXT_RETURN_FAIL;
        }

        if (!WPACKET_sub_memcpy_u16(pkt, encodedPoint, encoded_pt_len)
                || !WPACKET_close(pkt)) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
            EVP_PKEY_free(skey);
            OPENSSL_free(encodedPoint);
            return EXT_RETURN_FAIL;
        }
        OPENSSL_free(encodedPoint);

        /*
         * This causes the crypto state to be updated based on the derived keys
         */
        s->s3.tmp.pkey = skey;
        if (ssl_derive(s, skey, ckey, 1) == 0) {
            /* SSLfatal() already called */
            return EXT_RETURN_FAIL;
        }
    } else {
        /* KEM mode */
        unsigned char *ct = NULL;
        size_t ctlen = 0;

        /*
         * This does not update the crypto state.
         *
         * The generated pms is stored in `s->s3.tmp.pms` to be later used via
         * ssl_gensecret().
         */
        if (ssl_encapsulate(s, ckey, &ct, &ctlen, 0) == 0) {
            /* SSLfatal() already called */
            return EXT_RETURN_FAIL;
        }

        if (ctlen == 0) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
            OPENSSL_free(ct);
            return EXT_RETURN_FAIL;
        }

        if (!WPACKET_sub_memcpy_u16(pkt, ct, ctlen)
                || !WPACKET_close(pkt)) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
            OPENSSL_free(ct);
            return EXT_RETURN_FAIL;
        }
        OPENSSL_free(ct);

        /*
         * This causes the crypto state to be updated based on the generated pms
         */
        if (ssl_gensecret(s, s->s3.tmp.pms, s->s3.tmp.pmslen) == 0) {
            /* SSLfatal() already called */
            return EXT_RETURN_FAIL;
        }
    }
    s->s3.did_kex = 1;
    return EXT_RETURN_SENT;
#else
    return EXT_RETURN_FAIL;
#endif
}