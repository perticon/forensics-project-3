undefined8 init_alpn(long param_1)

{
  CRYPTO_free(*(void **)(param_1 + 0x488));
  *(undefined8 *)(param_1 + 0x488) = 0;
  *(undefined8 *)(param_1 + 0x490) = 0;
  if (*(int *)(param_1 + 0x38) == 0) {
    return 1;
  }
  CRYPTO_free(*(void **)(param_1 + 0x498));
  *(undefined8 *)(param_1 + 0x498) = 0;
  *(undefined8 *)(param_1 + 0x4a0) = 0;
  return 1;
}