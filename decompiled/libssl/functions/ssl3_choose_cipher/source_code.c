const SSL_CIPHER *ssl3_choose_cipher(SSL *s, STACK_OF(SSL_CIPHER) *clnt,
                                     STACK_OF(SSL_CIPHER) *srvr)
{
    const SSL_CIPHER *c, *ret = NULL;
    STACK_OF(SSL_CIPHER) *prio, *allow;
    int i, ii, ok, prefer_sha256 = 0;
    unsigned long alg_k = 0, alg_a = 0, mask_k = 0, mask_a = 0;
    STACK_OF(SSL_CIPHER) *prio_chacha = NULL;

    /* Let's see which ciphers we can support */

    /*
     * Do not set the compare functions, because this may lead to a
     * reordering by "id". We want to keep the original ordering. We may pay
     * a price in performance during sk_SSL_CIPHER_find(), but would have to
     * pay with the price of sk_SSL_CIPHER_dup().
     */

    OSSL_TRACE_BEGIN(TLS_CIPHER) {
        BIO_printf(trc_out, "Server has %d from %p:\n",
                   sk_SSL_CIPHER_num(srvr), (void *)srvr);
        for (i = 0; i < sk_SSL_CIPHER_num(srvr); ++i) {
            c = sk_SSL_CIPHER_value(srvr, i);
            BIO_printf(trc_out, "%p:%s\n", (void *)c, c->name);
        }
        BIO_printf(trc_out, "Client sent %d from %p:\n",
                   sk_SSL_CIPHER_num(clnt), (void *)clnt);
        for (i = 0; i < sk_SSL_CIPHER_num(clnt); ++i) {
            c = sk_SSL_CIPHER_value(clnt, i);
            BIO_printf(trc_out, "%p:%s\n", (void *)c, c->name);
        }
    } OSSL_TRACE_END(TLS_CIPHER);

    /* SUITE-B takes precedence over server preference and ChaCha priortiy */
    if (tls1_suiteb(s)) {
        prio = srvr;
        allow = clnt;
    } else if (s->options & SSL_OP_CIPHER_SERVER_PREFERENCE) {
        prio = srvr;
        allow = clnt;

        /* If ChaCha20 is at the top of the client preference list,
           and there are ChaCha20 ciphers in the server list, then
           temporarily prioritize all ChaCha20 ciphers in the servers list. */
        if (s->options & SSL_OP_PRIORITIZE_CHACHA && sk_SSL_CIPHER_num(clnt) > 0) {
            c = sk_SSL_CIPHER_value(clnt, 0);
            if (c->algorithm_enc == SSL_CHACHA20POLY1305) {
                /* ChaCha20 is client preferred, check server... */
                int num = sk_SSL_CIPHER_num(srvr);
                int found = 0;
                for (i = 0; i < num; i++) {
                    c = sk_SSL_CIPHER_value(srvr, i);
                    if (c->algorithm_enc == SSL_CHACHA20POLY1305) {
                        found = 1;
                        break;
                    }
                }
                if (found) {
                    prio_chacha = sk_SSL_CIPHER_new_reserve(NULL, num);
                    /* if reserve fails, then there's likely a memory issue */
                    if (prio_chacha != NULL) {
                        /* Put all ChaCha20 at the top, starting with the one we just found */
                        sk_SSL_CIPHER_push(prio_chacha, c);
                        for (i++; i < num; i++) {
                            c = sk_SSL_CIPHER_value(srvr, i);
                            if (c->algorithm_enc == SSL_CHACHA20POLY1305)
                                sk_SSL_CIPHER_push(prio_chacha, c);
                        }
                        /* Pull in the rest */
                        for (i = 0; i < num; i++) {
                            c = sk_SSL_CIPHER_value(srvr, i);
                            if (c->algorithm_enc != SSL_CHACHA20POLY1305)
                                sk_SSL_CIPHER_push(prio_chacha, c);
                        }
                        prio = prio_chacha;
                    }
                }
            }
        }
    } else {
        prio = clnt;
        allow = srvr;
    }

    if (SSL_IS_TLS13(s)) {
#ifndef OPENSSL_NO_PSK
        int j;

        /*
         * If we allow "old" style PSK callbacks, and we have no certificate (so
         * we're not going to succeed without a PSK anyway), and we're in
         * TLSv1.3 then the default hash for a PSK is SHA-256 (as per the
         * TLSv1.3 spec). Therefore we should prioritise ciphersuites using
         * that.
         */
        if (s->psk_server_callback != NULL) {
            for (j = 0; j < SSL_PKEY_NUM && !ssl_has_cert(s, j); j++);
            if (j == SSL_PKEY_NUM) {
                /* There are no certificates */
                prefer_sha256 = 1;
            }
        }
#endif
    } else {
        tls1_set_cert_validity(s);
        ssl_set_masks(s);
    }

    for (i = 0; i < sk_SSL_CIPHER_num(prio); i++) {
        c = sk_SSL_CIPHER_value(prio, i);

        /* Skip ciphers not supported by the protocol version */
        if (!SSL_IS_DTLS(s) &&
            ((s->version < c->min_tls) || (s->version > c->max_tls)))
            continue;
        if (SSL_IS_DTLS(s) &&
            (DTLS_VERSION_LT(s->version, c->min_dtls) ||
             DTLS_VERSION_GT(s->version, c->max_dtls)))
            continue;

        /*
         * Since TLS 1.3 ciphersuites can be used with any auth or
         * key exchange scheme skip tests.
         */
        if (!SSL_IS_TLS13(s)) {
            mask_k = s->s3.tmp.mask_k;
            mask_a = s->s3.tmp.mask_a;
#ifndef OPENSSL_NO_SRP
            if (s->srp_ctx.srp_Mask & SSL_kSRP) {
                mask_k |= SSL_kSRP;
                mask_a |= SSL_aSRP;
            }
#endif

            alg_k = c->algorithm_mkey;
            alg_a = c->algorithm_auth;

#ifndef OPENSSL_NO_PSK
            /* with PSK there must be server callback set */
            if ((alg_k & SSL_PSK) && s->psk_server_callback == NULL)
                continue;
#endif                          /* OPENSSL_NO_PSK */

            ok = (alg_k & mask_k) && (alg_a & mask_a);
            OSSL_TRACE7(TLS_CIPHER,
                        "%d:[%08lX:%08lX:%08lX:%08lX]%p:%s\n",
                        ok, alg_k, alg_a, mask_k, mask_a, (void *)c, c->name);

            /*
             * if we are considering an ECC cipher suite that uses an ephemeral
             * EC key check it
             */
            if (alg_k & SSL_kECDHE)
                ok = ok && tls1_check_ec_tmp_key(s, c->id);

            if (!ok)
                continue;
        }
        ii = sk_SSL_CIPHER_find(allow, c);
        if (ii >= 0) {
            /* Check security callback permits this cipher */
            if (!ssl_security(s, SSL_SECOP_CIPHER_SHARED,
                              c->strength_bits, 0, (void *)c))
                continue;

            if ((alg_k & SSL_kECDHE) && (alg_a & SSL_aECDSA)
                && s->s3.is_probably_safari) {
                if (!ret)
                    ret = sk_SSL_CIPHER_value(allow, ii);
                continue;
            }

            if (prefer_sha256) {
                const SSL_CIPHER *tmp = sk_SSL_CIPHER_value(allow, ii);

                if (EVP_MD_is_a(ssl_md(s->ctx, tmp->algorithm2),
                                       OSSL_DIGEST_NAME_SHA2_256)) {
                    ret = tmp;
                    break;
                }
                if (ret == NULL)
                    ret = tmp;
                continue;
            }
            ret = sk_SSL_CIPHER_value(allow, ii);
            break;
        }
    }

    sk_SSL_CIPHER_free(prio_chacha);

    return ret;
}