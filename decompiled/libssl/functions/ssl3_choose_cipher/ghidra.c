long ssl3_choose_cipher(int *param_1,long param_2,long param_3)

{
  uint uVar1;
  int iVar2;
  int iVar3;
  long lVar4;
  long lVar5;
  undefined8 uVar6;
  long lVar7;
  long lVar8;
  uint uVar9;
  int iVar10;
  uint uVar11;
  uint uVar12;
  bool bVar13;
  long local_50;
  long local_48;
  
  lVar5 = param_3;
  lVar4 = param_2;
  if ((((((*(uint *)(*(long *)(param_1 + 0x226) + 0x1c) & 0x30000) == 0) &&
        (lVar5 = param_2, lVar4 = param_3, (*(ulong *)(param_1 + 0x27a) & 0x400000) != 0)) &&
       (lVar5 = param_3, lVar4 = param_2, (*(ulong *)(param_1 + 0x27a) & 0x200000) != 0)) &&
      ((iVar10 = OPENSSL_sk_num(param_2), 0 < iVar10 &&
       (lVar7 = OPENSSL_sk_value(param_2,0), *(int *)(lVar7 + 0x24) == 0x80000)))) &&
     (iVar10 = OPENSSL_sk_num(param_3), 0 < iVar10)) {
    iVar2 = 0;
LAB_0012a730:
    lVar7 = OPENSSL_sk_value(param_3,iVar2);
    if (*(int *)(lVar7 + 0x24) != 0x80000) goto LAB_0012a724;
    lVar8 = OPENSSL_sk_new_reserve(0,iVar10);
    if (lVar8 != 0) {
      iVar2 = iVar2 + 1;
      OPENSSL_sk_push(lVar8,lVar7);
      if (iVar2 < iVar10) {
        do {
          lVar5 = OPENSSL_sk_value(param_3,iVar2);
          if (*(int *)(lVar5 + 0x24) == 0x80000) {
            OPENSSL_sk_push(lVar8);
          }
          iVar2 = iVar2 + 1;
        } while (iVar10 != iVar2);
      }
      iVar2 = 0;
      do {
        lVar5 = OPENSSL_sk_value(param_3,iVar2);
        if (*(int *)(lVar5 + 0x24) != 0x80000) {
          OPENSSL_sk_push(lVar8);
        }
        iVar2 = iVar2 + 1;
        local_50 = lVar8;
      } while (iVar10 != iVar2);
      goto LAB_0012a319;
    }
  }
LAB_0012a310:
  lVar8 = lVar5;
  local_50 = 0;
  param_2 = lVar4;
LAB_0012a319:
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
      (iVar10 = **(int **)(param_1 + 2), iVar10 != 0x10000)) && (0x303 < iVar10)) {
    bVar13 = false;
    if ((((((*(long *)(param_1 + 0x264) != 0) &&
           ((lVar5 = *(long *)(param_1 + 0x226), *(long *)(lVar5 + 0x20) == 0 ||
            (*(long *)(lVar5 + 0x28) == 0)))) &&
          ((*(long *)(lVar5 + 0x48) == 0 || (bVar13 = false, *(long *)(lVar5 + 0x50) == 0)))) &&
         (((*(long *)(lVar5 + 0x70) == 0 || (bVar13 = false, *(long *)(lVar5 + 0x78) == 0)) &&
          (((*(long *)(lVar5 + 0x98) == 0 || (bVar13 = false, *(long *)(lVar5 + 0xa0) == 0)) &&
           ((*(long *)(lVar5 + 0xc0) == 0 || (bVar13 = false, *(long *)(lVar5 + 200) == 0)))))))) &&
        ((*(long *)(lVar5 + 0xe8) == 0 || (bVar13 = false, *(long *)(lVar5 + 0xf0) == 0)))) &&
       (((*(long *)(lVar5 + 0x110) == 0 || (bVar13 = false, *(long *)(lVar5 + 0x118) == 0)) &&
        ((*(long *)(lVar5 + 0x138) == 0 || (bVar13 = false, *(long *)(lVar5 + 0x140) == 0)))))) {
      if (*(long *)(lVar5 + 0x160) == 0) {
        bVar13 = true;
      }
      else {
        bVar13 = *(long *)(lVar5 + 0x168) == 0;
      }
    }
  }
  else {
    tls1_set_cert_validity(param_1);
    ssl_set_masks(param_1);
    bVar13 = false;
  }
  uVar12 = 0;
  uVar11 = 0;
  iVar10 = 0;
  local_48 = 0;
  do {
    iVar2 = OPENSSL_sk_num(lVar8);
    lVar5 = local_48;
    if (iVar2 <= iVar10) {
LAB_0012a642:
      OPENSSL_sk_free(local_50);
      return lVar5;
    }
    lVar4 = OPENSSL_sk_value(lVar8,iVar10);
    iVar2 = *param_1;
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) {
      if ((*(int *)(lVar4 + 0x2c) <= iVar2) && (iVar2 <= *(int *)(lVar4 + 0x30))) {
        iVar2 = **(int **)(param_1 + 2);
        if ((iVar2 == 0x10000) || (iVar2 < 0x304)) goto LAB_0012a4d2;
LAB_0012a52a:
        iVar2 = OPENSSL_sk_find(param_2);
        if ((-1 < iVar2) &&
           (iVar3 = ssl_security(param_1,0x10002,*(undefined4 *)(lVar4 + 0x44),0,lVar4), iVar3 != 0)
           ) {
          if ((((uVar11 & 4) == 0) || ((uVar12 & 8) == 0)) || (*(char *)(param_1 + 299) == '\0')) {
            if (!bVar13) {
              lVar5 = OPENSSL_sk_value(param_2,iVar2);
              goto LAB_0012a642;
            }
            lVar5 = OPENSSL_sk_value(param_2,iVar2);
            uVar6 = ssl_md(*(undefined8 *)(param_1 + 0x26a),*(undefined4 *)(lVar5 + 0x40));
            iVar2 = EVP_MD_is_a(uVar6);
            if (iVar2 != 0) goto LAB_0012a642;
            if (local_48 != 0) {
              lVar5 = local_48;
            }
          }
          else if (local_48 == 0) {
            lVar5 = OPENSSL_sk_value(param_2,iVar2);
          }
        }
      }
    }
    else {
      iVar3 = *(int *)(lVar4 + 0x34);
      if (iVar2 == 0x100) {
        iVar2 = 0xff00;
        if (iVar3 != 0x100) goto LAB_0012a4b5;
        iVar3 = *(int *)(lVar4 + 0x38);
        if (iVar3 == 0x100) goto LAB_0012a4d2;
      }
      else {
        if (iVar3 == 0x100) {
          iVar3 = 0xff00;
        }
LAB_0012a4b5:
        if (iVar3 < iVar2) goto LAB_0012a548;
        iVar3 = *(int *)(lVar4 + 0x38);
        if (iVar3 == 0x100) {
          iVar3 = 0xff00;
        }
      }
      if (iVar3 <= iVar2) {
LAB_0012a4d2:
        uVar1 = param_1[0xf7];
        uVar9 = param_1[0xf8];
        if ((*(byte *)(param_1 + 0x312) & 0x20) != 0) {
          uVar1 = uVar1 | 0x20;
          uVar9 = uVar9 | 0x40;
        }
        uVar11 = *(uint *)(lVar4 + 0x1c);
        uVar12 = *(uint *)(lVar4 + 0x20);
        if (((((uVar11 & 0x1c8) == 0) || (*(long *)(param_1 + 0x264) != 0)) &&
            ((uVar1 & uVar11) != 0)) &&
           (((uVar9 & uVar12) != 0 &&
            (((uVar11 & 4) == 0 || (iVar2 = tls1_check_ec_tmp_key(param_1), iVar2 != 0))))))
        goto LAB_0012a52a;
      }
    }
LAB_0012a548:
    local_48 = lVar5;
    iVar10 = iVar10 + 1;
  } while( true );
LAB_0012a724:
  iVar2 = iVar2 + 1;
  if (iVar10 == iVar2) goto LAB_0012a310;
  goto LAB_0012a730;
}