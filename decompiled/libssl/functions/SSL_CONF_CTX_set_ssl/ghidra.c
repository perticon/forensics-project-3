void SSL_CONF_CTX_set_ssl(long param_1,long param_2)

{
  *(long *)(param_1 + 0x20) = param_2;
  *(undefined8 *)(param_1 + 0x18) = 0;
  if (param_2 != 0) {
    *(long *)(param_1 + 0x28) = param_2 + 0x9e8;
    *(long *)(param_1 + 0x78) = *(long *)(param_2 + 0x898) + 0x1c;
    *(long *)(param_1 + 0x80) = param_2 + 0x968;
    *(long *)(param_1 + 0x88) = param_2 + 0x9f4;
    *(long *)(param_1 + 0x90) = param_2 + 0x9f8;
    return;
  }
  *(undefined8 *)(param_1 + 0x28) = 0;
  *(undefined (*) [16])(param_1 + 0x78) = (undefined  [16])0x0;
  *(undefined (*) [16])(param_1 + 0x88) = (undefined  [16])0x0;
  return;
}