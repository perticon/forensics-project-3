undefined2 tls1_nid2group_id(int param_1)

{
  long lVar1;
  
  lVar1 = 0;
  do {
    if (*(int *)(nid_to_group + lVar1 * 8) == param_1) {
      return *(undefined2 *)(nid_to_group + lVar1 * 8 + 4);
    }
    lVar1 = lVar1 + 1;
  } while (lVar1 != 0x2a);
  return 0;
}