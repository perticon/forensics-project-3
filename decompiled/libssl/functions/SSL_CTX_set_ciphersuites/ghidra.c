undefined8 SSL_CTX_set_ciphersuites(long param_1,char *param_2)

{
  int iVar1;
  void *arg;
  undefined8 uVar2;
  
  arg = (void *)OPENSSL_sk_new_null();
  if (arg == (void *)0x0) {
    return 0;
  }
  if (*param_2 != '\0') {
    iVar1 = CONF_parse_list(param_2,0x3a,1,ciphersuite_cb,arg);
    if (0 < iVar1) {
      iVar1 = OPENSSL_sk_num(arg);
      if (iVar1 != 0) goto LAB_001318a2;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_ciph.c",0x541,"set_ciphersuites");
    ERR_set_error(0x14,0xb9,0);
    OPENSSL_sk_free(arg);
    return 0;
  }
LAB_001318a2:
  OPENSSL_sk_free(*(undefined8 *)(param_1 + 0x20));
  *(void **)(param_1 + 0x20) = arg;
  if (*(long *)(param_1 + 0x10) == 0) {
    return 1;
  }
  uVar2 = update_cipher_list_isra_0
                    (param_1 + 0x640,param_1 + 0x644,param_1 + 0x10,param_1 + 0x18,arg);
  return uVar2;
}