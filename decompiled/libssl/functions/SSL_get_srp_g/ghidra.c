long SSL_get_srp_g(long param_1)

{
  if (*(long *)(param_1 + 0xc00) != 0) {
    return *(long *)(param_1 + 0xc00);
  }
  return *(long *)(*(long *)(param_1 + 0x9a8) + 0x348);
}