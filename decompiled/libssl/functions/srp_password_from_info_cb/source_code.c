static char *srp_password_from_info_cb(SSL *s, void *arg)
{
    return OPENSSL_strdup(s->srp_ctx.info);
}