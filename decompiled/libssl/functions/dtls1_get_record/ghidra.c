undefined8 dtls1_get_record(uint *param_1)

{
  byte bVar1;
  byte bVar2;
  byte bVar3;
  char cVar4;
  byte *pbVar5;
  int iVar6;
  undefined8 uVar7;
  long lVar8;
  ushort uVar9;
  ulong uVar10;
  long in_FS_OFFSET;
  undefined8 uVar11;
  int local_3c;
  ulong local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
LAB_0015c850:
  iVar6 = dtls1_process_buffered_records(param_1);
  do {
    if (iVar6 == 0) {
LAB_0015c919:
      uVar7 = 0xffffffff;
LAB_0015c91e:
      if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
        return uVar7;
      }
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    iVar6 = dtls1_retrieve_buffered_record(param_1,*(long *)(param_1 + 0x744) + 0x38);
    if (iVar6 != 0) {
LAB_0015cb68:
      uVar7 = 1;
      goto LAB_0015c91e;
    }
    if ((param_1[0x319] == 0xf1) && (0xc < *(ulong *)(param_1 + 0x72c))) {
      uVar10 = *(ulong *)(param_1 + 0x4ac);
      if (*(ulong *)(param_1 + 0x72c) - 0xd < uVar10) {
LAB_0015c8b0:
        iVar6 = ssl3_read_n(param_1,uVar10,uVar10,1,1,&local_38);
        if ((0 < iVar6) && (local_38 == uVar10)) goto LAB_0015ca85;
LAB_0015c8dd:
        iVar6 = ossl_statem_in_error(param_1);
        if (iVar6 != 0) goto LAB_0015c919;
      }
      else {
LAB_0015ca85:
        param_1[0x319] = 0xf0;
        lVar8 = dtls1_get_bitmap(param_1,param_1 + 0x4aa,&local_3c);
        if (lVar8 == 0) break;
        iVar6 = dtls1_record_replay_check(param_1,lVar8);
        if (iVar6 != 0) {
          if (*(long *)(param_1 + 0x4ac) == 0) {
            param_1[0x4b8] = 1;
            goto LAB_0015c850;
          }
          if (local_3c == 0) {
            iVar6 = dtls1_process_record(param_1,lVar8);
            if (iVar6 != 0) goto LAB_0015cb68;
            goto LAB_0015c8dd;
          }
          iVar6 = SSL_in_init(param_1);
          if (((iVar6 != 0) || (iVar6 = ossl_statem_get_in_handshake(param_1), iVar6 != 0)) &&
             (iVar6 = dtls1_buffer_record(param_1,*(long *)(param_1 + 0x744) + 0x28,param_1 + 0x4bc)
             , iVar6 < 0)) goto LAB_0015c919;
        }
      }
    }
    else {
      uVar11 = 0x15c961;
      uVar7 = ssl3_read_n(param_1,0xd,*(undefined8 *)(param_1 + 0x322),0,1,&local_38);
      if ((int)uVar7 < 1) goto LAB_0015c91e;
      if (*(long *)(param_1 + 0x72c) != 0xd) goto LAB_0015cb18;
      pbVar5 = *(byte **)(param_1 + 0x72a);
      param_1[0x319] = 0xf1;
      if (*(code **)(param_1 + 0x130) != (code *)0x0) {
        (**(code **)(param_1 + 0x130))
                  (0,0,0x100,pbVar5,0xd,param_1,*(undefined8 *)(param_1 + 0x132),uVar11);
      }
      bVar1 = *pbVar5;
      param_1[0x4ab] = (uint)bVar1;
      bVar2 = pbVar5[1];
      bVar3 = pbVar5[2];
      *(ulong *)(param_1 + 0x4ba) =
           (ulong)(ushort)(*(ushort *)(pbVar5 + 3) << 8 | *(ushort *)(pbVar5 + 3) >> 8);
      *(undefined4 *)((long)param_1 + 0x1cfa) = *(undefined4 *)(pbVar5 + 5);
      *(undefined2 *)((long)param_1 + 0x1cfe) = *(undefined2 *)(pbVar5 + 9);
      uVar9 = *(ushort *)(pbVar5 + 0xb);
      param_1[0x4b8] = 0;
      uVar9 = uVar9 << 8 | uVar9 >> 8;
      uVar10 = (ulong)uVar9;
      *(ulong *)(param_1 + 0x4ac) = uVar10;
      if ((param_1[0x282] == 0) && (bVar1 != 0x15)) {
        if ((uint)CONCAT11(bVar2,bVar3) == *param_1) {
LAB_0015ca34:
          if ((uVar9 < 0x4541) &&
             (((*(long *)(param_1 + 0x246) == 0 ||
               (cVar4 = *(char *)(*(long *)(param_1 + 0x246) + 0x368), 3 < (byte)(cVar4 - 1U))) ||
              (uVar10 <= (0x200 << (cVar4 - 1U & 0x1f)) + 0x140)))) {
            if (*(long *)(param_1 + 0x72c) - 0xdU < uVar10) goto LAB_0015c8b0;
            goto LAB_0015ca85;
          }
        }
      }
      else if (bVar2 == (byte)(*param_1 >> 8)) goto LAB_0015ca34;
    }
    *(undefined8 *)(param_1 + 0x4ac) = 0;
    param_1[0x4b8] = 1;
    *(undefined8 *)(param_1 + 0x72c) = 0;
    iVar6 = dtls1_process_buffered_records(param_1);
  } while( true );
  *(undefined8 *)(param_1 + 0x4ac) = 0;
LAB_0015cb18:
  *(undefined8 *)(param_1 + 0x72c) = 0;
  goto LAB_0015c850;
}