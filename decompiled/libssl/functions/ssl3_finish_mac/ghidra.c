undefined8 ssl3_finish_mac(long param_1,void *param_2,size_t param_3)

{
  int iVar1;
  undefined8 uVar2;
  
  if (*(EVP_MD_CTX **)(param_1 + 400) != (EVP_MD_CTX *)0x0) {
    iVar1 = EVP_DigestUpdate(*(EVP_MD_CTX **)(param_1 + 400),param_2,param_3);
    if (iVar1 != 0) {
      return 1;
    }
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x168,"ssl3_finish_mac");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  }
  if (param_3 < 0x80000000) {
    iVar1 = BIO_write(*(BIO **)(param_1 + 0x188),param_2,(int)param_3);
    if ((iVar1 == (int)param_3) && (0 < iVar1)) {
      return 1;
    }
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x162,"ssl3_finish_mac");
    uVar2 = 0xc0103;
  }
  else {
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x15d,"ssl3_finish_mac");
    uVar2 = 0xed;
  }
  ossl_statem_fatal(param_1,0x50,uVar2,0);
  return 0;
}