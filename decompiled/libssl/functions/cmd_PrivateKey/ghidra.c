ulong cmd_PrivateKey(byte *param_1,char *param_2)

{
  int iVar1;
  
  if ((*param_1 & 0x20) != 0) {
    iVar1 = 1;
    if (*(SSL_CTX **)(param_1 + 0x18) != (SSL_CTX *)0x0) {
      iVar1 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 0x18),param_2,1);
    }
    if (*(SSL **)(param_1 + 0x20) != (SSL *)0x0) {
      iVar1 = SSL_use_PrivateKey_file(*(SSL **)(param_1 + 0x20),param_2,1);
    }
    return (ulong)(0 < iVar1);
  }
  return 0xfffffffe;
}