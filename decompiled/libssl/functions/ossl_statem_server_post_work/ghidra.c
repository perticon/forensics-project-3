uint ossl_statem_server_post_work(SSL *param_1)

{
  undefined4 uVar1;
  int iVar2;
  int *piVar3;
  _func_3076 *p_Var4;
  SSL_METHOD *pSVar5;
  uint uVar6;
  long in_FS_OFFSET;
  undefined auStack40 [8];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = *(undefined4 *)((long)&param_1->init_msg + 4);
  param_1->msg_callback = (_func_3150 *)0x0;
  switch(uVar1) {
  case 0x13:
    iVar2 = statem_flush();
    if (iVar2 != 1) {
LAB_0017b94d:
      uVar6 = 3;
      goto LAB_0017b916;
    }
    iVar2 = ssl3_init_finished_mac(param_1);
    goto joined_r0x0017ba7d;
  case 0x15:
    iVar2 = statem_flush();
    if (iVar2 != 1) goto LAB_0017b94d;
    if ((param_1->version == 0x100) || (iVar2 = ssl3_init_finished_mac(param_1), iVar2 != 0)) {
      param_1[3].tlsext_status_expected = 1;
      uVar6 = 2;
      goto LAB_0017b916;
    }
    goto LAB_0017ba83;
  case 0x16:
    pSVar5 = param_1->method;
    p_Var4 = pSVar5->get_timeout;
    if (((byte)p_Var4[0x60] & 8) == 0) {
      iVar2 = pSVar5->version;
      if (iVar2 < 0x304) {
        if ((iVar2 != 0x10000) && (0x303 < iVar2)) {
          iVar2 = *(int *)&param_1[3].read_hash;
          if ((*(byte *)((long)&param_1[3].tlsext_debug_cb + 2) & 0x10) == 0) {
            if (iVar2 != 1) goto LAB_0017b8da;
            goto LAB_0017bc20;
          }
LAB_0017bb40:
          if (iVar2 == 2) goto LAB_0017bb49;
        }
      }
      else if (iVar2 != 0x10000) {
        iVar2 = *(int *)&param_1[3].read_hash;
        if (iVar2 != 1) {
          if (((ulong)param_1[3].tlsext_debug_cb & 0x100000) != 0) goto LAB_0017bb40;
          goto LAB_0017bb63;
        }
        if (((ulong)param_1[3].tlsext_debug_cb & 0x100000) == 0) goto switchD_0017b8b2_caseD_1a;
      }
    }
    break;
  case 0x19:
    if (*(int *)&param_1[4].enc_write_ctx == 3) goto switchD_0017b8b2_caseD_1a;
    break;
  case 0x1a:
switchD_0017b8b2_caseD_1a:
    iVar2 = statem_flush(param_1);
    if (iVar2 != 1) goto LAB_0017b94d;
    break;
  case 0x21:
    piVar3 = __errno_location();
    *piVar3 = 0;
    if (((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
        (iVar2 = param_1->method->version, iVar2 != 0x10000)) &&
       ((0x303 < iVar2 && (iVar2 = statem_flush(param_1), iVar2 != 1)))) {
      iVar2 = SSL_get_error(param_1,0);
      if (iVar2 == 5) {
        if ((*piVar3 == 0x20) || (uVar6 = 3, *piVar3 == 0x68)) {
          param_1->rwstate = 1;
          uVar6 = 2;
        }
        goto LAB_0017b916;
      }
      goto LAB_0017b94d;
    }
    break;
  case 0x23:
    if (*(int *)&param_1[3].read_hash == 1) {
LAB_0017bc20:
      iVar2 = statem_flush(param_1);
      if (iVar2 == 0) goto LAB_0017b94d;
    }
    else {
      pSVar5 = param_1->method;
      p_Var4 = pSVar5->get_timeout;
      if (((byte)p_Var4[0x60] & 8) == 0) {
LAB_0017bb49:
        if ((0x303 < pSVar5->version) && (pSVar5->version != 0x10000)) {
LAB_0017bb63:
          iVar2 = (**(code **)(p_Var4 + 0x10))(param_1);
          if (((iVar2 != 0) &&
              (iVar2 = (**(code **)(param_1->method->get_timeout + 0x20))(param_1,0xa2), iVar2 != 0)
              ) && ((param_1[4].packet_length == 2 ||
                    (iVar2 = (**(code **)(param_1->method->get_timeout + 0x20))(param_1,0xa1),
                    iVar2 != 0)))) {
            *(undefined4 *)&param_1->s3 = 1;
            uVar6 = 2;
            goto LAB_0017b916;
          }
          goto LAB_0017ba83;
        }
      }
LAB_0017b8da:
      iVar2 = (**(code **)(p_Var4 + 0x20))(param_1,0x22);
      if (iVar2 == 0) goto LAB_0017ba83;
      if (((byte)param_1->method->get_timeout[0x60] & 8) != 0) {
        dtls1_reset_seq_numbers(param_1,2);
      }
    }
    break;
  case 0x24:
    iVar2 = statem_flush();
    if (iVar2 != 1) goto LAB_0017b94d;
    p_Var4 = param_1->method->get_timeout;
    uVar6 = *(uint *)(p_Var4 + 0x60) & 8;
    if (((uVar6 == 0) && (iVar2 = param_1->method->version, 0x303 < iVar2)) && (iVar2 != 0x10000)) {
      iVar2 = (**(code **)(p_Var4 + 0x18))
                        (param_1,&param_1[2].init_off,(undefined *)((long)&param_1[2].bbio + 4),0,
                         auStack40);
      if ((iVar2 != 0) &&
         (iVar2 = (**(code **)(param_1->method->get_timeout + 0x20))(param_1,0x122), iVar2 != 0)) {
        uVar6 = 2;
      }
      goto LAB_0017b916;
    }
    break;
  case 0x2a:
    iVar2 = statem_flush();
    if (iVar2 != 1) goto LAB_0017b94d;
    iVar2 = tls13_update_key(param_1,1);
joined_r0x0017ba7d:
    if (iVar2 == 0) {
LAB_0017ba83:
      uVar6 = 0;
      goto LAB_0017b916;
    }
  }
  uVar6 = 2;
LAB_0017b916:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar6;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}