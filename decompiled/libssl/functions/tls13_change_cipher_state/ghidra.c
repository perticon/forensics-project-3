uint tls13_change_cipher_state(long param_1,uint param_2)

{
  long lVar1;
  ulong uVar2;
  int iVar3;
  EVP_CIPHER_CTX *pEVar4;
  undefined8 uVar5;
  long lVar6;
  size_t sVar7;
  EVP_MD_CTX *ctx;
  uint uVar8;
  char *pcVar9;
  long in_FS_OFFSET;
  uchar *local_160;
  EVP_MD *local_158;
  long local_150;
  char *local_148;
  long local_140;
  uint local_124;
  ulong local_120;
  long local_118;
  void *local_110;
  undefined local_108 [64];
  undefined local_c8 [64];
  uchar local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_120 = 0;
  local_118 = 0;
  uVar8 = param_2 & 1;
  if ((param_2 & 1) == 0) {
    *(undefined4 *)(param_1 + 0x7c) = 1;
    if (*(long *)(param_1 + 0x878) == 0) {
      pEVar4 = EVP_CIPHER_CTX_new();
      *(EVP_CIPHER_CTX **)(param_1 + 0x878) = pEVar4;
      if (pEVar4 == (EVP_CIPHER_CTX *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/tls13_enc.c",0x1ce,"tls13_change_cipher_state");
        ossl_statem_fatal(param_1,0x50,0xc0100,0);
        goto LAB_00152258;
      }
    }
    else {
      EVP_CIPHER_CTX_reset();
      pEVar4 = *(EVP_CIPHER_CTX **)(param_1 + 0x878);
    }
    lVar1 = param_1 + 0x880;
    RECORD_LAYER_reset_write_sequence(param_1 + 0xc58);
joined_r0x00151fa2:
    if (((param_2 & 0x12) != 0x12) && ((param_2 & 0x21) != 0x21)) {
      if ((param_2 & 0x80) != 0) {
        local_150 = param_1 + 0x684;
        lVar6 = param_1 + 0x584;
        uVar5 = ssl_handshake_md(param_1);
        iVar3 = EVP_MD_get_size(uVar5);
        local_140 = (long)iVar3;
        if ((param_2 & 0x40) == 0) {
          pcVar9 = "s hs traffic";
          local_148 = "SERVER_HANDSHAKE_TRAFFIC_SECRET";
          local_160 = local_88;
          goto LAB_00151ecb;
        }
        local_160 = local_88;
        local_158 = (EVP_MD *)0x0;
        local_148 = "SERVER_HANDSHAKE_TRAFFIC_SECRET";
LAB_001523c4:
        pcVar9 = "s hs traffic";
        memcpy((void *)(param_1 + 0x704),local_88,local_120);
        uVar5 = 0xc;
        goto joined_r0x00152027;
      }
      lVar6 = param_1 + 0x5c4;
      if ((param_2 & 0x40) != 0) {
        local_160 = local_88;
        local_158 = (EVP_MD *)0x0;
        local_140 = 0;
        local_148 = "SERVER_TRAFFIC_SECRET_0";
        local_150 = 0;
LAB_00152002:
        pcVar9 = "s ap traffic";
        memcpy((void *)(param_1 + 0x6c4),local_88,local_120);
        uVar5 = 0xc;
        goto joined_r0x00152027;
      }
      pcVar9 = "s ap traffic";
      local_140 = 0;
      local_148 = "SERVER_TRAFFIC_SECRET_0";
      local_160 = local_88;
      local_150 = 0;
LAB_00151ecb:
      local_158 = (EVP_MD *)ssl_handshake_md(param_1);
      local_118 = *(long *)(param_1 + 800);
      iVar3 = ssl3_digest_cached_records(param_1,1);
      if (iVar3 == 0) {
LAB_00151ef9:
        uVar8 = 0;
        goto LAB_00151f10;
      }
      iVar3 = ssl_handshake_hash(param_1,local_88,0x40,&local_120);
      uVar2 = local_120;
      if (iVar3 == 0) goto LAB_00151ef9;
      if (pcVar9 == "s ap traffic") goto LAB_00152002;
      if (pcVar9 == "s hs traffic") goto LAB_001523c4;
      if (pcVar9 == "c ap traffic") {
        uVar5 = ssl_handshake_md(param_1);
        iVar3 = tls13_hkdf_expand(param_1,uVar5,lVar6,"res master",10,local_88,uVar2,param_1 + 0x604
                                  ,uVar2,1);
        uVar5 = 0xc;
        if (iVar3 != 0) goto joined_r0x00152027;
      }
      else {
        uVar5 = 0xc;
joined_r0x00152027:
        if (local_118 != 0) {
          iVar3 = derive_secret_key_and_iv
                            (param_1,param_2 & 2,local_158,local_118,lVar6,local_160,pcVar9,uVar5,
                             local_c8,local_108,lVar1,pEVar4);
          sVar7 = local_120;
          if (iVar3 != 0) {
            if (pcVar9 == "s ap traffic") {
              memcpy((void *)(param_1 + 0x784),local_c8,local_120);
              uVar5 = ssl_handshake_md(param_1);
              iVar3 = tls13_hkdf_expand(param_1,uVar5,lVar6,"exp master",10,local_160,sVar7,
                                        param_1 + 0x7c4,sVar7,1);
              if ((iVar3 == 0) ||
                 (iVar3 = ssl_log_secret(param_1,"EXPORTER_SECRET",param_1 + 0x7c4,local_120),
                 sVar7 = local_120, iVar3 == 0)) goto LAB_0015203d;
            }
            else if (pcVar9 == "c ap traffic") {
              memcpy((void *)(param_1 + 0x744),local_c8,local_120);
            }
            iVar3 = ssl_log_secret(param_1,local_148,local_c8,sVar7);
            if (iVar3 != 0) {
              if (local_150 != 0) {
                uVar5 = ssl_handshake_md(param_1);
                iVar3 = tls13_derive_finishedkey(param_1,uVar5,local_c8,local_150,local_140);
                if (iVar3 == 0) goto LAB_0015203d;
              }
              if ((*(int *)(param_1 + 0x38) == 0) && (pcVar9 == "c e traffic")) {
                *(undefined4 *)(param_1 + 0x7c) = 2;
                uVar8 = 1;
              }
              else {
                *(undefined4 *)(param_1 + 0x7c) = 0;
                uVar8 = 1;
              }
              goto LAB_00152258;
            }
          }
        }
      }
LAB_0015203d:
      uVar8 = 0;
      goto LAB_00152258;
    }
    if ((param_2 & 0x40) == 0) {
      if ((param_2 & 0x80) == 0) {
        local_160 = (uchar *)(param_1 + 0x6c4);
        lVar6 = param_1 + 0x5c4;
        local_140 = 0;
        pcVar9 = "c ap traffic";
        local_148 = "CLIENT_TRAFFIC_SECRET_0";
        local_150 = 0;
      }
      else {
        local_150 = param_1 + 0x644;
        lVar6 = param_1 + 0x584;
        pcVar9 = "c hs traffic";
        uVar5 = ssl_handshake_md(param_1);
        iVar3 = EVP_MD_get_size(uVar5);
        local_140 = (long)iVar3;
        local_160 = (uchar *)(param_1 + 0x704);
        local_148 = "CLIENT_HANDSHAKE_TRAFFIC_SECRET";
      }
      goto LAB_00151ecb;
    }
    lVar6 = SSL_SESSION_get0_cipher(*(undefined8 *)(param_1 + 0x918));
    sVar7 = BIO_ctrl(*(BIO **)(param_1 + 0x188),3,0,&local_110);
    if ((long)sVar7 < 1) {
      ERR_new();
      ERR_set_debug("ssl/tls13_enc.c",0x1e8,"tls13_change_cipher_state");
      ossl_statem_fatal(param_1,0x50,0x14c,0);
    }
    else if (((*(int *)(param_1 + 0x84) == 2) && (*(int *)(param_1 + 0x1d40) != 0)) &&
            (*(int *)(*(long *)(param_1 + 0x918) + 0x354) == 0)) {
      if ((*(long *)(param_1 + 0x920) != 0) &&
         (*(int *)(param_1 + 0x1d40) == *(int *)(*(long *)(param_1 + 0x920) + 0x354))) {
        lVar6 = SSL_SESSION_get0_cipher();
        goto LAB_001522b3;
      }
      ERR_new();
      uVar5 = 0x1f7;
LAB_001524fd:
      ERR_set_debug("ssl/tls13_enc.c",uVar5,"tls13_change_cipher_state");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
    else {
LAB_001522b3:
      if (lVar6 == 0) {
        ERR_new();
        ERR_set_debug("ssl/tls13_enc.c",0x1fd,"tls13_change_cipher_state");
        ossl_statem_fatal(param_1,0x50,0xdb,0);
      }
      else {
        ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
        if (ctx == (EVP_MD_CTX *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/tls13_enc.c",0x208,"tls13_change_cipher_state");
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
        }
        else {
          iVar3 = ssl_cipher_get_evp_cipher(*(undefined8 *)(param_1 + 0x9a8),lVar6,&local_118);
          if (iVar3 == 0) {
            ossl_statem_send_fatal(param_1,0x50);
            EVP_MD_CTX_free(ctx);
          }
          else {
            local_158 = (EVP_MD *)
                        ssl_md(*(undefined8 *)(param_1 + 0x9a8),*(undefined4 *)(lVar6 + 0x40));
            if (((local_158 != (EVP_MD *)0x0) &&
                (iVar3 = EVP_DigestInit_ex(ctx,local_158,(ENGINE *)0x0), iVar3 != 0)) &&
               (iVar3 = EVP_DigestUpdate(ctx,local_110,sVar7), iVar3 != 0)) {
              local_160 = local_88;
              iVar3 = EVP_DigestFinal_ex(ctx,local_160,&local_124);
              if (iVar3 != 0) {
                local_120 = (ulong)local_124;
                lVar6 = param_1 + 0x544;
                EVP_MD_CTX_free(ctx);
                iVar3 = tls13_hkdf_expand(param_1,local_158,lVar6,"e exp master",0xc,local_160,
                                          local_120,param_1 + 0x804,local_120,1);
                if (iVar3 == 0) {
                  ERR_new();
                  uVar5 = 0x228;
                  goto LAB_001524fd;
                }
                iVar3 = ssl_log_secret(param_1,"EARLY_EXPORTER_SECRET",param_1 + 0x804,local_120);
                if (iVar3 != 0) {
                  uVar5 = 0xb;
                  local_140 = 0;
                  pcVar9 = "c e traffic";
                  local_148 = "CLIENT_EARLY_TRAFFIC_SECRET";
                  local_150 = 0;
                  goto joined_r0x00152027;
                }
                goto LAB_00152353;
              }
            }
            ERR_new();
            ERR_set_debug("ssl/tls13_enc.c",0x21b,"tls13_change_cipher_state");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            EVP_MD_CTX_free(ctx);
          }
        }
      }
    }
LAB_00152353:
    uVar8 = 0;
  }
  else {
    if (*(long *)(param_1 + 0x848) != 0) {
      EVP_CIPHER_CTX_reset();
      pEVar4 = *(EVP_CIPHER_CTX **)(param_1 + 0x848);
LAB_00151e5d:
      lVar1 = param_1 + 0x850;
      RECORD_LAYER_reset_read_sequence(param_1 + 0xc58);
      goto joined_r0x00151fa2;
    }
    pEVar4 = EVP_CIPHER_CTX_new();
    *(EVP_CIPHER_CTX **)(param_1 + 0x848) = pEVar4;
    if (pEVar4 != (EVP_CIPHER_CTX *)0x0) goto LAB_00151e5d;
    ERR_new();
    uVar8 = 0;
    ERR_set_debug("ssl/tls13_enc.c",0x1bf,"tls13_change_cipher_state");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
LAB_00152258:
    if ((param_2 & 0x40) == 0) goto LAB_00151f10;
  }
  ssl_evp_cipher_free(local_118);
LAB_00151f10:
  OPENSSL_cleanse(local_108,0x40);
  OPENSSL_cleanse(local_c8,0x40);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar8;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}