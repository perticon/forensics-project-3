int64_t set_client_ciphersuite(int64_t a1, int64_t a2, int64_t a3, int64_t a4) {
    int64_t v1 = ssl_get_cipher_by_char(); // 0x6b37f
    if (v1 == 0) {
        // 0x6b5c0
        function_20230();
        function_207e0();
        // 0x6b51c
        ossl_statem_fatal();
        return 0;
    }
    int64_t v2 = ssl_cipher_disabled(); // 0x6b3a0
    if ((int32_t)v2 != 0) {
        // 0x6b4f8
        function_20230();
        function_207e0();
        // 0x6b51c
        ossl_statem_fatal();
        return 0;
    }
    int64_t result = v2 & 0xffffffff; // 0x6b3a5
    ssl_get_ciphers_by_id();
    if ((int32_t)function_21370() < 0) {
        // 0x6b540
        function_20230();
        // 0x6b551
        function_207e0();
        ossl_statem_fatal();
        // 0x6b4e5
        return result;
    }
    int64_t v3 = *(int64_t *)(a1 + 8); // 0x6b3cb
    int32_t v4 = *(int32_t *)(*(int64_t *)(v3 + 192) + 96); // 0x6b3d6
    if ((v4 & 8) == 0) {
        int32_t v5 = *(int32_t *)v3; // 0x6b3de
        if (v5 > (int32_t)&g90 && v5 != (int32_t)&g2) {
            int64_t v6 = *(int64_t *)(a1 + (int64_t)&g80); // 0x6b3f0
            if (v6 != 0) {
                // 0x6b3fc
                if (*(int32_t *)(v6 + 24) != *(int32_t *)(v1 + 24)) {
                    // 0x6b5f0
                    function_20230();
                    // 0x6b551
                    function_207e0();
                    ossl_statem_fatal();
                    // 0x6b4e5
                    return result;
                }
            }
        }
    }
    int64_t v7 = *(int64_t *)(a1 + (int64_t)&g350); // 0x6b410
    int64_t v8 = *(int64_t *)(v7 + (int64_t)&g84); // 0x6b417
    if (v8 != 0) {
        uint32_t v9 = *(int32_t *)(v8 + 24); // 0x6b423
        *(int64_t *)(v7 + (int64_t)&g86) = (int64_t)v9;
    }
    // 0x6b42d
    if (*(int32_t *)(a1 + (int64_t)&g191) == 0) {
        // 0x6b4d8
        *(int64_t *)(a1 + (int64_t)&g80) = v1;
        // 0x6b4e5
        return 1;
    }
    int64_t v10 = *(int64_t *)(v7 + (int64_t)&g86); // 0x6b43e
    if (v10 == (int64_t)*(int32_t *)(v1 + 24)) {
        // 0x6b4d8
        *(int64_t *)(a1 + (int64_t)&g80) = v1;
        // 0x6b4e5
        return 1;
    }
    if ((v4 & 8) == 0) {
        int32_t v11 = *(int32_t *)v3; // 0x6b453
        if (v11 != (int32_t)&g2 == v11 > (int32_t)&g90) {
            int64_t v12 = ssl_md(); // 0x6b475
            if (v12 == ssl_md()) {
                // 0x6b4d8
                *(int64_t *)(a1 + (int64_t)&g80) = v1;
                // 0x6b4e5
                return 1;
            }
            // 0x6b49f
            function_20230();
            function_207e0();
            ossl_statem_fatal();
            // 0x6b4e5
            return result;
        }
    }
    // 0x6b580
    function_20230();
    function_207e0();
    ossl_statem_fatal();
    // 0x6b4e5
    return result;
}