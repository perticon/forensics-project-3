int32_t set_client_ciphersuite(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t eax5;
    int32_t r12d6;
    void** rax7;
    int32_t eax8;
    void** rcx9;
    uint32_t eax10;
    void** rdx11;
    void** rdi12;
    void** rsi13;
    void** rsi14;
    void** rdi15;
    void** rax16;
    void** rdi17;
    void** rsi18;
    void** rax19;

    rax4 = ssl_get_cipher_by_char(rdi, rsi);
    if (!rax4) {
        fun_20230();
        fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
    } else {
        eax5 = ssl_cipher_disabled(rdi, rax4, 0x10003, rdi, rax4, 0x10003);
        r12d6 = eax5;
        if (eax5) {
            fun_20230();
            fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
        } else {
            rax7 = ssl_get_ciphers_by_id(rdi, rax4, 0x10003, 1);
            eax8 = fun_21370(rax7, rax4, rax7, rax4);
            if (eax8 < 0) {
                fun_20230();
                goto addr_6b551_7;
            } else {
                rcx9 = *reinterpret_cast<void***>(rdi + 8);
                eax10 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rcx9 + 0xc0) + 96)) & 8;
                if (eax10 || (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rcx9)) <= reinterpret_cast<signed char>(0x303) || (*reinterpret_cast<void***>(rcx9) == 0x10000 || !*reinterpret_cast<void***>(rdi + 0x2e0)))) {
                    addr_6b410_9:
                    rdx11 = *reinterpret_cast<void***>(rdi + 0x918);
                    if (*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx11) + reinterpret_cast<uint64_t>("@"))) {
                        rdi12 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx11) + reinterpret_cast<uint64_t>("@")) + 24);
                        *reinterpret_cast<int32_t*>(&rdi12 + 4) = 0;
                        *reinterpret_cast<void***>(rdx11 + 0x300) = rdi12;
                        goto addr_6b42d_11;
                    }
                } else {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x2e0) + 24) != *reinterpret_cast<void***>(rax4 + 24)) {
                        fun_20230();
                        goto addr_6b551_7;
                    } else {
                        goto addr_6b410_9;
                    }
                }
            }
        }
    }
    ossl_statem_fatal(rdi, rdi);
    return 0;
    addr_6b551_7:
    fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
    ossl_statem_fatal(rdi, rdi);
    addr_6b4e5_16:
    return r12d6;
    addr_6b42d_11:
    if (!*reinterpret_cast<void***>(rdi + 0x4d0)) 
        goto addr_6b4d8_17;
    rsi13 = *reinterpret_cast<void***>(rax4 + 24);
    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
    if (*reinterpret_cast<void***>(rdx11 + 0x300) == rsi13) 
        goto addr_6b4d8_17;
    if (eax10 || (*reinterpret_cast<void***>(rcx9) == 0x10000 || reinterpret_cast<signed char>(*reinterpret_cast<void***>(rcx9)) <= reinterpret_cast<signed char>(0x303))) {
        fun_20230();
        fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
        ossl_statem_fatal(rdi, rdi);
        goto addr_6b4e5_16;
    } else {
        rsi14 = *reinterpret_cast<void***>(rax4 + 64);
        *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
        rdi15 = *reinterpret_cast<void***>(rdi + 0x9a8);
        rax16 = ssl_md(rdi15, rsi14);
        rdi17 = *reinterpret_cast<void***>(rdi + 0x9a8);
        rsi18 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 0x918)) + reinterpret_cast<uint64_t>("@")) + 64);
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        rax19 = ssl_md(rdi17, rsi18);
        if (rax16 == rax19) {
            addr_6b4d8_17:
            *reinterpret_cast<void***>(rdi + 0x2e0) = rax4;
            r12d6 = 1;
            goto addr_6b4e5_16;
        } else {
            fun_20230();
            fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
            ossl_statem_fatal(rdi, rdi);
            goto addr_6b4e5_16;
        }
    }
}