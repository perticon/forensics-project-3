undefined4 set_client_ciphersuite(long param_1,undefined8 param_2)

{
  int *piVar1;
  int iVar2;
  uint uVar3;
  long lVar4;
  undefined8 uVar5;
  long lVar6;
  long lVar7;
  
  lVar4 = ssl_get_cipher_by_char(param_1,param_2,0);
  if (lVar4 == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x521,"set_client_ciphersuite");
    uVar5 = 0xf8;
  }
  else {
    iVar2 = ssl_cipher_disabled(param_1,lVar4,0x10003,1);
    if (iVar2 == 0) {
      uVar5 = ssl_get_ciphers_by_id(param_1);
      iVar2 = OPENSSL_sk_find(uVar5,lVar4);
      if (iVar2 < 0) {
        ERR_new();
        uVar5 = 0x531;
      }
      else {
        piVar1 = *(int **)(param_1 + 8);
        uVar3 = *(uint *)(*(long *)(piVar1 + 0x30) + 0x60) & 8;
        if ((((uVar3 != 0) || (*piVar1 < 0x304)) || (*piVar1 == 0x10000)) ||
           ((*(long *)(param_1 + 0x2e0) == 0 ||
            (*(int *)(*(long *)(param_1 + 0x2e0) + 0x18) == *(int *)(lVar4 + 0x18))))) {
          lVar6 = *(long *)(param_1 + 0x918);
          if (*(long *)(lVar6 + 0x2f8) != 0) {
            *(ulong *)(lVar6 + 0x300) = (ulong)*(uint *)(*(long *)(lVar6 + 0x2f8) + 0x18);
          }
          if ((*(int *)(param_1 + 0x4d0) != 0) &&
             (*(ulong *)(lVar6 + 0x300) != (ulong)*(uint *)(lVar4 + 0x18))) {
            if ((uVar3 != 0) || ((*piVar1 == 0x10000 || (*piVar1 < 0x304)))) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x554,"set_client_ciphersuite");
              ossl_statem_fatal(param_1,0x2f,0xc5,0);
              return 0;
            }
            lVar6 = ssl_md(*(undefined8 *)(param_1 + 0x9a8),*(undefined4 *)(lVar4 + 0x40));
            lVar7 = ssl_md(*(undefined8 *)(param_1 + 0x9a8),
                           *(undefined4 *)(*(long *)(*(long *)(param_1 + 0x918) + 0x2f8) + 0x40));
            if (lVar6 != lVar7) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x54b,"set_client_ciphersuite");
              ossl_statem_fatal(param_1,0x2f,0xda,0);
              return 0;
            }
          }
          *(long *)(param_1 + 0x2e0) = lVar4;
          return 1;
        }
        ERR_new();
        uVar5 = 0x538;
      }
      ERR_set_debug("ssl/statem/statem_clnt.c",uVar5,"set_client_ciphersuite");
      ossl_statem_fatal(param_1,0x2f,0x105,0);
      return 0;
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x529,"set_client_ciphersuite");
    uVar5 = 0x105;
  }
  ossl_statem_fatal(param_1,0x2f,uVar5,0);
  return 0;
}