int ssl_cert_get_cert_store(CERT *c, X509_STORE **pstore, int chain)
{
    *pstore = (chain ? c->chain_store : c->verify_store);
    return 1;
}