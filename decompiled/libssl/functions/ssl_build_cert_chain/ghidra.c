uint ssl_build_cert_chain(long param_1,undefined8 *param_2,uint param_3)

{
  uint uVar1;
  int iVar2;
  uint uVar3;
  int iVar4;
  X509 *pXVar5;
  X509 *pXVar6;
  undefined8 uVar7;
  X509 *ctx;
  ulong uVar8;
  char *pcVar9;
  undefined8 *puVar10;
  X509 **ppXVar11;
  long **pplVar12;
  X509_STORE_CTX *local_48;
  
  if (param_1 == 0) {
    pplVar12 = (long **)param_2[0x2b];
    ppXVar11 = (X509 **)*pplVar12;
    puVar10 = param_2;
  }
  else {
    pplVar12 = *(long ***)(param_1 + 0x898);
    ppXVar11 = (X509 **)*pplVar12;
    puVar10 = *(undefined8 **)(param_1 + 0x9a8);
  }
  ctx = *ppXVar11;
  if (ctx == (X509 *)0x0) {
    ERR_new();
    uVar1 = 0;
    ERR_set_debug("ssl/ssl_cert.c",0x360,"ssl_build_cert_chain");
    ERR_set_error(0x14,0xb3,0);
    local_48 = (X509_STORE_CTX *)0x0;
LAB_0012f0a0:
    if ((param_3 & 4) == 0) goto LAB_0012f04c;
  }
  else {
    if ((param_3 & 4) == 0) {
      ctx = (X509 *)pplVar12[0x39];
      if (ctx == (X509 *)0x0) {
        if (param_1 == 0) {
          ctx = (X509 *)param_2[5];
        }
        else {
          ctx = *(X509 **)(*(long *)(param_1 + 0x9a8) + 0x28);
        }
      }
      pXVar5 = (X509 *)0x0;
      if ((param_3 & 1) != 0) {
        pXVar5 = ppXVar11[2];
      }
LAB_0012eeb8:
      local_48 = (X509_STORE_CTX *)X509_STORE_CTX_new_ex(*puVar10,puVar10[0x88]);
      if (local_48 == (X509_STORE_CTX *)0x0) {
        ERR_new();
        uVar1 = 0;
        ERR_set_debug("ssl/ssl_cert.c",0x37e,"ssl_build_cert_chain");
        ERR_set_error(0x14,0xc0100,0);
      }
      else {
        uVar1 = X509_STORE_CTX_init(local_48,(X509_STORE *)ctx,*ppXVar11,(stack_st_X509 *)pXVar5);
        if (uVar1 == 0) {
          ERR_new();
          ERR_set_debug("ssl/ssl_cert.c",0x382,"ssl_build_cert_chain");
          ERR_set_error(0x14,0x8000b,0);
        }
        else {
          X509_STORE_CTX_set_flags(local_48,(ulong)(*(uint *)((long)pplVar12 + 0x1c) & 0x30000));
          iVar2 = X509_verify_cert(local_48);
          if (iVar2 < 1) {
            uVar1 = param_3 & 8;
            if (uVar1 == 0) {
              iVar2 = X509_STORE_CTX_get_error(local_48);
              ERR_new();
              ERR_set_debug("ssl/ssl_cert.c",0x394,"ssl_build_cert_chain");
              pcVar9 = X509_verify_cert_error_string((long)iVar2);
              ERR_set_error(0x14,0x86,"Verify error:%s",pcVar9);
              goto LAB_0012f0a0;
            }
            if ((param_3 & 0x10) == 0) {
              uVar1 = 2;
              pXVar5 = (X509 *)X509_STORE_CTX_get1_chain(local_48);
            }
            else {
              ERR_clear_error();
              uVar1 = 2;
              pXVar5 = (X509 *)X509_STORE_CTX_get1_chain(local_48);
            }
          }
          else {
            uVar1 = 0;
            pXVar5 = (X509 *)X509_STORE_CTX_get1_chain(local_48);
          }
          pXVar6 = (X509 *)OPENSSL_sk_shift(pXVar5);
          X509_free(pXVar6);
          if (((param_3 & 2) != 0) && (iVar2 = OPENSSL_sk_num(pXVar5), 0 < iVar2)) {
            iVar2 = OPENSSL_sk_num(pXVar5);
            uVar7 = OPENSSL_sk_value(pXVar5,iVar2 + -1);
            uVar8 = X509_get_extension_flags(uVar7);
            if ((uVar8 & 0x2000) != 0) {
              pXVar6 = (X509 *)OPENSSL_sk_pop(pXVar5);
              X509_free(pXVar6);
            }
          }
          for (iVar2 = 0; iVar4 = OPENSSL_sk_num(pXVar5), iVar2 < iVar4; iVar2 = iVar2 + 1) {
            uVar7 = OPENSSL_sk_value(pXVar5,iVar2);
            uVar3 = ssl_security_cert(param_1,param_2,uVar7,0,0);
            if (uVar3 != 1) {
              ERR_new();
              ERR_set_debug("ssl/ssl_cert.c",0x3ae,"ssl_build_cert_chain");
              uVar1 = 0;
              ERR_set_error(0x14,uVar3,0);
              OSSL_STACK_OF_X509_free(pXVar5);
              goto LAB_0012f0a0;
            }
            uVar1 = uVar3;
          }
          OSSL_STACK_OF_X509_free(ppXVar11[2]);
          ppXVar11[2] = pXVar5;
          if (uVar1 == 0) {
            uVar1 = 1;
          }
        }
      }
      goto LAB_0012f0a0;
    }
    ctx = (X509 *)X509_STORE_new();
    if (ctx != (X509 *)0x0) {
      for (iVar2 = 0; iVar4 = OPENSSL_sk_num(ppXVar11[2]), iVar2 < iVar4; iVar2 = iVar2 + 1) {
        pXVar5 = (X509 *)OPENSSL_sk_value(ppXVar11[2],iVar2);
        iVar4 = X509_STORE_add_cert((X509_STORE *)ctx,pXVar5);
        if (iVar4 == 0) goto LAB_0012f038;
      }
      pXVar5 = (X509 *)0x0;
      iVar2 = X509_STORE_add_cert((X509_STORE *)ctx,*ppXVar11);
      if (iVar2 != 0) goto LAB_0012eeb8;
    }
LAB_0012f038:
    local_48 = (X509_STORE_CTX *)0x0;
    uVar1 = 0;
  }
  X509_STORE_free((X509_STORE *)ctx);
LAB_0012f04c:
  X509_STORE_CTX_free(local_48);
  return uVar1;
}