undefined8 ssl_new(BIO *param_1)

{
  long lVar1;
  
  lVar1 = CRYPTO_zalloc(0x30,"ssl/bio_ssl.c",0x3a);
  if (lVar1 != 0) {
    BIO_set_init(param_1,0);
    BIO_set_data(param_1,lVar1);
    BIO_clear_flags(param_1,-1);
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/bio_ssl.c",0x3d,"ssl_new");
  ERR_set_error(0x20,0xc0100,0);
  return 0;
}