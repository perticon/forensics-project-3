bool tls13_hkdf_expand(long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
                      ulong param_5,long param_6,undefined8 param_7,undefined8 param_8,
                      undefined8 param_9,int param_10)

{
  bool bVar1;
  int iVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long lVar5;
  undefined8 *puVar6;
  long in_FS_OFFSET;
  undefined4 local_198;
  undefined4 uStack404;
  undefined4 uStack400;
  undefined4 uStack396;
  undefined4 local_188;
  undefined4 uStack388;
  undefined4 uStack384;
  undefined4 uStack380;
  undefined8 local_178;
  undefined4 local_15c;
  undefined local_158 [32];
  undefined8 local_138;
  undefined8 local_110;
  undefined8 local_e8;
  undefined8 local_c0;
  undefined8 local_98;
  undefined4 local_90;
  undefined4 uStack140;
  undefined4 uStack136;
  undefined4 uStack132;
  undefined4 local_80;
  undefined4 uStack124;
  undefined4 uStack120;
  undefined4 uStack116;
  undefined8 local_70;
  undefined8 local_68 [5];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar3 = EVP_KDF_fetch(**(undefined8 **)(param_1 + 0x9a8),&DAT_0018b146,
                        (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
  local_15c = 2;
  uVar4 = EVP_MD_get0_name(param_2);
  lVar5 = EVP_KDF_CTX_new(uVar3);
  EVP_KDF_free(uVar3);
  if (lVar5 == 0) {
    bVar1 = false;
  }
  else if (param_5 < 0xfa) {
    iVar2 = EVP_MD_get_size(param_2);
    if (iVar2 < 1) {
      EVP_KDF_CTX_free(lVar5);
      if (param_10 == 0) {
        ERR_new();
        ERR_set_debug("ssl/tls13_enc.c",0x48,"tls13_hkdf_expand");
        ERR_set_error(0x14,0xc0103,0);
        bVar1 = false;
      }
      else {
        ERR_new();
        ERR_set_debug("ssl/tls13_enc.c",0x46,"tls13_hkdf_expand");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        bVar1 = false;
      }
    }
    else {
      OSSL_PARAM_construct_int(&local_198,"mode",&local_15c);
      local_138 = local_178;
      puVar6 = (undefined8 *)&local_90;
      OSSL_PARAM_construct_utf8_string
                (CONCAT44(uStack404,local_198),CONCAT44(uStack388,local_188),&local_198,"digest",
                 uVar4,0);
      local_110 = local_178;
      OSSL_PARAM_construct_octet_string(&local_198,"key",param_3,(long)iVar2);
      local_e8 = local_178;
      OSSL_PARAM_construct_octet_string(&local_198,"prefix","tls13 ",6);
      local_c0 = local_178;
      OSSL_PARAM_construct_octet_string(&local_198,"label",param_4,param_5);
      local_98 = local_178;
      if (param_6 != 0) {
        puVar6 = local_68;
        OSSL_PARAM_construct_octet_string
                  (CONCAT44(uStack404,local_198),CONCAT44(uStack388,local_188),&local_198,"data",
                   param_6,param_7);
        local_70 = local_178;
        local_90 = local_198;
        uStack140 = uStack404;
        uStack136 = uStack400;
        uStack132 = uStack396;
        local_80 = local_188;
        uStack124 = uStack388;
        uStack120 = uStack384;
        uStack116 = uStack380;
      }
      OSSL_PARAM_construct_end(&local_198);
      *puVar6 = CONCAT44(uStack404,local_198);
      puVar6[1] = CONCAT44(uStack396,uStack400);
      puVar6[2] = CONCAT44(uStack388,local_188);
      puVar6[3] = CONCAT44(uStack380,uStack384);
      puVar6[4] = local_178;
      iVar2 = EVP_KDF_derive(lVar5,param_8,param_9,local_158);
      EVP_KDF_CTX_free(lVar5);
      if (iVar2 < 1) {
        if (param_10 == 0) {
          ERR_new();
          ERR_set_debug("ssl/tls13_enc.c",100,"tls13_hkdf_expand");
          ERR_set_error(0x14,0xc0103,0);
        }
        else {
          ERR_new();
          ERR_set_debug("ssl/tls13_enc.c",0x62,"tls13_hkdf_expand");
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
        }
      }
      bVar1 = 0 < iVar2;
    }
  }
  else {
    if (param_10 == 0) {
      ERR_new();
      ERR_set_debug("ssl/tls13_enc.c",0x3d,"tls13_hkdf_expand");
      ERR_set_error(0x14,0x16f,0);
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/tls13_enc.c",0x37,"tls13_hkdf_expand");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
    EVP_KDF_CTX_free(lVar5);
    bVar1 = false;
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}