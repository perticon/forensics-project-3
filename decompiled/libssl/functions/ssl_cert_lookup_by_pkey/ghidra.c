int * ssl_cert_lookup_by_pkey(undefined8 param_1,long *param_2)

{
  int iVar1;
  int iVar2;
  char *pcVar3;
  long lVar4;
  undefined1 *puVar5;
  
  puVar5 = ssl_cert_info;
  lVar4 = 0;
  while( true ) {
    iVar2 = *(int *)puVar5;
    pcVar3 = OBJ_nid2sn(iVar2);
    iVar1 = EVP_PKEY_is_a(param_1,pcVar3);
    if (iVar1 != 0) break;
    pcVar3 = OBJ_nid2ln(iVar2);
    iVar2 = EVP_PKEY_is_a(param_1,pcVar3);
    if (iVar2 != 0) break;
    lVar4 = lVar4 + 1;
    puVar5 = (undefined1 *)((long)puVar5 + 8);
    if (lVar4 == 9) {
      return (int *)0x0;
    }
  }
  if (param_2 == (long *)0x0) {
    return (int *)puVar5;
  }
  *param_2 = lVar4;
  return (int *)puVar5;
}