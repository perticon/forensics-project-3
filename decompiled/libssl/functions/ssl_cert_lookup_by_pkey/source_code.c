const SSL_CERT_LOOKUP *ssl_cert_lookup_by_pkey(const EVP_PKEY *pk, size_t *pidx)
{
    size_t i;

    for (i = 0; i < OSSL_NELEM(ssl_cert_info); i++) {
        const SSL_CERT_LOOKUP *tmp_lu = &ssl_cert_info[i];

        if (EVP_PKEY_is_a(pk, OBJ_nid2sn(tmp_lu->nid))
            || EVP_PKEY_is_a(pk, OBJ_nid2ln(tmp_lu->nid))) {
            if (pidx != NULL)
                *pidx = i;
            return tmp_lu;
        }
    }

    return NULL;
}