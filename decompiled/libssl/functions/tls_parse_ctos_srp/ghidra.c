undefined8 tls_parse_ctos_srp(long param_1,byte **param_2)

{
  byte *__n;
  byte *__s;
  void *pvVar1;
  long lVar2;
  
  if (param_2[1] != (byte *)0x0) {
    __n = param_2[1] + -1;
    if (__n == (byte *)(ulong)**param_2) {
      __s = *param_2 + 1;
      param_2[1] = (byte *)0x0;
      *param_2 = __s + (long)__n;
      pvVar1 = memchr(__s,0,(size_t)__n);
      if (pvVar1 == (void *)0x0) {
        CRYPTO_free(*(void **)(param_1 + 0xbf0));
        lVar2 = CRYPTO_strndup(__s,__n,"include/internal/packet.h",0x1dc);
        *(long *)(param_1 + 0xbf0) = lVar2;
        if (lVar2 != 0) {
          return 1;
        }
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_srvr.c",0xd7,"tls_parse_ctos_srp");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        return 0;
      }
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0xd2,"tls_parse_ctos_srp");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  return 0;
}