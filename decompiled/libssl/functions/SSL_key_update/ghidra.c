int SSL_key_update(long param_1,uint param_2)

{
  int iVar1;
  
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
      (iVar1 = **(int **)(param_1 + 8), 0x303 < iVar1)) && (iVar1 != 0x10000)) {
    if (1 < param_2) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x8d3,"SSL_key_update");
      ERR_set_error(0x14,0x78,0);
      return 0;
    }
    iVar1 = SSL_is_init_finished();
    if (iVar1 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x8d8,"SSL_key_update");
      ERR_set_error(0x14,0x79,0);
    }
    else {
      iVar1 = RECORD_LAYER_write_pending(param_1 + 0xc58);
      if (iVar1 == 0) {
        ossl_statem_set_in_init(param_1,1);
        *(uint *)(param_1 + 0xba4) = param_2;
        return 1;
      }
      ERR_new();
      iVar1 = 0;
      ERR_set_debug("ssl/ssl_lib.c",0x8dd,"SSL_key_update");
      ERR_set_error(0x14,0x7f,0);
    }
  }
  else {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_lib.c",0x8cd,"SSL_key_update");
    ERR_set_error(0x14,0x10a,0);
  }
  return iVar1;
}