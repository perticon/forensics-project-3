undefined8 tls_process_client_hello(long param_1,ushort **param_2)

{
  int *piVar1;
  byte bVar2;
  ushort uVar3;
  ushort uVar4;
  int iVar5;
  uint uVar6;
  int *ptr;
  ushort *puVar7;
  undefined8 uVar8;
  byte *pbVar9;
  long lVar10;
  byte *pbVar11;
  ushort *puVar12;
  ulong uVar13;
  ushort *puVar14;
  ulong uVar15;
  undefined8 *puVar16;
  ushort uVar17;
  ushort *puVar18;
  undefined8 *puVar19;
  ushort uVar20;
  uint uVar21;
  long in_FS_OFFSET;
  byte bVar22;
  undefined8 local_38;
  undefined8 uStack48;
  long local_20;
  
  bVar22 = 0;
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (((*(int *)(param_1 + 0xba0) == 0) && (*(long *)(param_1 + 0x240) != 0)) &&
     (*(long *)(param_1 + 0x2c8) != 0)) {
    if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
        (iVar5 = **(int **)(param_1 + 8), iVar5 == 0x10000)) || (iVar5 < 0x304)) {
      if ((((uint)*(ulong *)(param_1 + 0x9e8) & 0x40000100) != 0x100) ||
         ((*(int *)(param_1 + 0x480) == 0 && ((*(ulong *)(param_1 + 0x9e8) & 0x40000) == 0)))) {
        ssl3_send_alert(param_1,1,100);
        uVar8 = 1;
        goto LAB_0017c3be;
      }
      *(undefined4 *)(param_1 + 0xba0) = 1;
      *(undefined4 *)(param_1 + 0x3c) = 1;
      goto LAB_0017c0ba;
    }
    ERR_new();
    ptr = (int *)0x0;
    ERR_set_debug("ssl/statem/statem_srvr.c",0x569,"tls_process_client_hello");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
LAB_0017c0ba:
    ptr = (int *)CRYPTO_zalloc(0x290,"ssl/statem/statem_srvr.c",0x577);
    if (ptr == (int *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0x579,"tls_process_client_hello");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
    else {
      iVar5 = RECORD_LAYER_is_sslv2_record();
      *ptr = iVar5;
      if (iVar5 == 0) {
        puVar12 = param_2[1];
LAB_0017c0f8:
        if (puVar12 < (ushort *)0x2) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0x5a8,"tls_process_client_hello");
          ossl_statem_fatal(param_1,0x32,0xa0,0);
        }
        else {
          puVar14 = *param_2;
          bVar2 = *(byte *)puVar14;
          ptr[1] = (uint)bVar2 << 8;
          ptr[1] = (uint)CONCAT11(bVar2,*(byte *)((long)puVar14 + 1));
          puVar7 = puVar12 + -1;
          *param_2 = puVar14 + 1;
          param_2[1] = puVar7;
          if (iVar5 == 0) {
            if (puVar7 < (ushort *)0x20) {
LAB_0017c540:
              ERR_new();
              uVar8 = 0x5e6;
            }
            else {
              uVar8 = *(undefined8 *)(puVar14 + 5);
              *(undefined8 *)(ptr + 2) = *(undefined8 *)(puVar14 + 1);
              *(undefined8 *)(ptr + 4) = uVar8;
              uVar8 = *(undefined8 *)(puVar14 + 0xd);
              *(undefined8 *)(ptr + 6) = *(undefined8 *)(puVar14 + 9);
              *(undefined8 *)(ptr + 8) = uVar8;
              puVar7 = *param_2;
              *param_2 = puVar7 + 0x10;
              puVar14 = param_2[1];
              puVar12 = puVar14 + -0x10;
              param_2[1] = puVar12;
              if (puVar12 == (ushort *)0x0) goto LAB_0017c540;
              bVar2 = *(byte *)(puVar7 + 0x10);
              pbVar11 = (byte *)(ulong)bVar2;
              pbVar9 = (byte *)((long)puVar14 + -0x21);
              if (pbVar9 < pbVar11) goto LAB_0017c540;
              param_2[1] = (ushort *)(pbVar9 + -(long)pbVar11);
              *param_2 = (ushort *)((byte *)((long)puVar7 + 0x21) + (long)pbVar11);
              if ((byte *)0x20 < pbVar11) {
                *(undefined8 *)(ptr + 10) = 0;
                goto LAB_0017c540;
              }
              *(byte **)(ptr + 10) = pbVar11;
              piVar1 = ptr + 0xc;
              if (bVar2 < 8) {
                if ((bVar2 & 4) == 0) {
                  if ((bVar2 != 0) &&
                     (*(byte *)piVar1 = *(byte *)((long)puVar7 + 0x21), (bVar2 & 2) != 0)) {
                    *(undefined2 *)(pbVar11 + (long)ptr + 0x2e) =
                         *(undefined2 *)((byte *)((long)puVar7 + 0x1f) + (long)pbVar11);
                  }
                }
                else {
                  *piVar1 = *(int *)((long)puVar7 + 0x21);
                  *(undefined4 *)((long)(ptr + 0xb) + (long)pbVar11) =
                       *(undefined4 *)((byte *)((long)puVar7 + 0x1d) + (long)pbVar11);
                }
              }
              else {
                *(undefined8 *)(ptr + 0xc) = *(undefined8 *)((long)puVar7 + 0x21);
                *(undefined8 *)((long)(ptr + 10) + (long)pbVar11) =
                     *(undefined8 *)((byte *)((long)puVar7 + 0x19) + (long)pbVar11);
                lVar10 = (long)piVar1 -
                         (long)(undefined8 *)((ulong)(ptr + 0xe) & 0xfffffffffffffff8);
                puVar16 = (undefined8 *)((byte *)((long)puVar7 + 0x21) + -lVar10);
                puVar19 = (undefined8 *)((ulong)(ptr + 0xe) & 0xfffffffffffffff8);
                for (uVar13 = (ulong)((uint)bVar2 + (int)lVar10 >> 3); uVar13 != 0;
                    uVar13 = uVar13 - 1) {
                  *puVar19 = *puVar16;
                  puVar16 = puVar16 + (ulong)bVar22 * -2 + 1;
                  puVar19 = puVar19 + (ulong)bVar22 * -2 + 1;
                }
              }
              if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
LAB_0017c7a9:
                if ((ushort *)0x1 < param_2[1]) {
                  puVar12 = param_2[1] + -1;
                  uVar20 = **param_2;
                  puVar7 = (ushort *)(ulong)(ushort)(uVar20 << 8 | uVar20 >> 8);
                  if (puVar7 <= puVar12) {
                    puVar14 = *param_2 + 1;
                    param_2[1] = (ushort *)((long)puVar12 - (long)puVar7);
                    *param_2 = (ushort *)((long)puVar14 + (long)puVar7);
                    *(ushort **)(ptr + 0x56) = puVar14;
                    *(ushort **)(ptr + 0x58) = puVar7;
                    if (param_2[1] != (ushort *)0x0) {
                      pbVar9 = (byte *)((long)param_2[1] + -1);
                      pbVar11 = (byte *)(ulong)*(byte *)*param_2;
                      if (pbVar11 <= pbVar9) {
                        puVar16 = (undefined8 *)((long)*param_2 + 1);
                        puVar7 = (ushort *)(pbVar9 + -(long)pbVar11);
                        puVar12 = (ushort *)((long)puVar16 + (long)pbVar11);
                        param_2[1] = puVar7;
                        *param_2 = puVar12;
                        if (puVar7 == (ushort *)0x0) {
                          *(undefined8 *)(ptr + 0x9c) = 0;
                          *(undefined8 *)(ptr + 0x9e) = 0;
LAB_0017c2c5:
                          *(byte **)(ptr + 0x5a) = pbVar11;
                          piVar1 = ptr + 0x5c;
                          uVar21 = (uint)pbVar11;
                          if (uVar21 < 8) {
                            if (((ulong)pbVar11 & 4) == 0) {
                              if ((uVar21 != 0) &&
                                 (*(byte *)piVar1 = *(byte *)puVar16, ((ulong)pbVar11 & 2) != 0)) {
                                *(undefined2 *)(pbVar11 + (long)ptr + 0x16e) =
                                     *(undefined2 *)((byte *)((long)puVar16 + -2) + (long)pbVar11);
                              }
                            }
                            else {
                              *piVar1 = *(int *)puVar16;
                              *(undefined4 *)((long)(ptr + 0x5b) + (long)pbVar11) =
                                   *(undefined4 *)((long)((long)puVar16 + -4) + (long)pbVar11);
                            }
                          }
                          else {
                            *(undefined8 *)(ptr + 0x5c) = *puVar16;
                            *(undefined8 *)((long)(ptr + 0x5a) + (long)pbVar11) =
                                 *(undefined8 *)((long)(puVar16 + -1) + (long)pbVar11);
                            lVar10 = (long)piVar1 -
                                     (long)(undefined8 *)((ulong)(ptr + 0x5e) & 0xfffffffffffffff8);
                            puVar16 = (undefined8 *)((long)puVar16 - lVar10);
                            puVar19 = (undefined8 *)((ulong)(ptr + 0x5e) & 0xfffffffffffffff8);
                            for (uVar13 = (ulong)((int)lVar10 + uVar21 >> 3); uVar13 != 0;
                                uVar13 = uVar13 - 1) {
                              *puVar19 = *puVar16;
                              puVar16 = puVar16 + (ulong)bVar22 * -2 + 1;
                              puVar19 = puVar19 + (ulong)bVar22 * -2 + 1;
                            }
                          }
                          local_38 = *(undefined8 *)(ptr + 0x9c);
                          uStack48 = *(undefined8 *)(ptr + 0x9e);
                          iVar5 = tls_collect_extensions
                                            (param_1,&local_38,0x80,ptr + 0xa2,ptr + 0xa0,1);
                          if (iVar5 != 0) {
                            *(int **)(param_1 + 0xb58) = ptr;
                            uVar8 = 2;
                            goto LAB_0017c3be;
                          }
                          goto LAB_0017c38f;
                        }
                        if (puVar7 != (ushort *)0x1) {
                          puVar14 = (ushort *)(ulong)(ushort)(*puVar12 << 8 | *puVar12 >> 8);
                          if (puVar14 <= puVar7 + -1) {
                            param_2[1] = (ushort *)((long)(puVar7 + -1) - (long)puVar14);
                            *param_2 = (ushort *)((long)(puVar12 + 1) + (long)puVar14);
                            *(ushort **)(ptr + 0x9e) = puVar14;
                            puVar7 = param_2[1];
                            *(ushort **)(ptr + 0x9c) = puVar12 + 1;
                            if (puVar7 == (ushort *)0x0) goto LAB_0017c2c5;
                          }
                        }
                        ERR_new();
                        uVar8 = 0x612;
                        goto LAB_0017c551;
                      }
                    }
                    ERR_new();
                    uVar8 = 0x608;
                    goto LAB_0017c551;
                  }
                }
                ERR_new();
                uVar8 = 0x603;
              }
              else {
                if (param_2[1] != (ushort *)0x0) {
                  puVar12 = *param_2;
                  pbVar9 = (byte *)((long)param_2[1] - 1);
                  bVar2 = *(byte *)puVar12;
                  pbVar11 = (byte *)(ulong)bVar2;
                  if (pbVar11 <= pbVar9) {
                    param_2[1] = (ushort *)(pbVar9 + -(long)pbVar11);
                    piVar1 = ptr + 0x16;
                    *param_2 = (ushort *)((byte *)((long)puVar12 + 1) + (long)pbVar11);
                    *(byte **)(ptr + 0x14) = pbVar11;
                    if (bVar2 < 8) {
                      if ((bVar2 & 4) == 0) {
                        if ((bVar2 != 0) &&
                           (*(byte *)piVar1 = *(byte *)((long)puVar12 + 1), (bVar2 & 2) != 0)) {
                          *(undefined2 *)(pbVar11 + (long)ptr + 0x56) =
                               *(undefined2 *)((byte *)((long)puVar12 + -1) + (long)pbVar11);
                        }
                      }
                      else {
                        *piVar1 = *(int *)((long)puVar12 + 1);
                        *(undefined4 *)((long)(ptr + 0x15) + (long)pbVar11) =
                             *(undefined4 *)((byte *)((long)puVar12 + -3) + (long)pbVar11);
                      }
                    }
                    else {
                      *(undefined8 *)(ptr + 0x16) = *(undefined8 *)((long)puVar12 + 1);
                      *(undefined8 *)((long)(ptr + 0x14) + (long)pbVar11) =
                           *(undefined8 *)((byte *)((long)puVar12 + -7) + (long)pbVar11);
                      lVar10 = (long)piVar1 -
                               (long)(undefined8 *)((ulong)(ptr + 0x18) & 0xfffffffffffffff8);
                      puVar16 = (undefined8 *)((byte *)((long)puVar12 + 1) + -lVar10);
                      puVar19 = (undefined8 *)((ulong)(ptr + 0x18) & 0xfffffffffffffff8);
                      for (uVar13 = (ulong)((uint)bVar2 + (int)lVar10 >> 3); uVar13 != 0;
                          uVar13 = uVar13 - 1) {
                        *puVar19 = *puVar16;
                        puVar16 = puVar16 + (ulong)bVar22 * -2 + 1;
                        puVar19 = puVar19 + (ulong)bVar22 * -2 + 1;
                      }
                    }
                    uVar13 = SSL_get_options(param_1);
                    if (((uVar13 & 0x2000) != 0) && (*(long *)(ptr + 0x14) == 0)) {
                      CRYPTO_free(ptr);
                      uVar8 = 1;
                      goto LAB_0017c3be;
                    }
                    goto LAB_0017c7a9;
                  }
                }
                ERR_new();
                uVar8 = 0x5ec;
              }
            }
LAB_0017c551:
            ERR_set_debug("ssl/statem/statem_srvr.c",uVar8,"tls_process_client_hello");
            ossl_statem_fatal(param_1,0x32,0x9f,0);
          }
          else {
            if (puVar7 < (ushort *)0x2) {
LAB_0017c660:
              ERR_new();
              uVar8 = 0x5b9;
            }
            else {
              uVar20 = puVar14[1];
              *param_2 = puVar14 + 2;
              param_2[1] = puVar12 + -2;
              if (puVar12 + -2 < (ushort *)0x2) goto LAB_0017c660;
              uVar17 = puVar14[2];
              *param_2 = puVar14 + 3;
              param_2[1] = puVar12 + -3;
              if (puVar12 + -3 < (ushort *)0x2) goto LAB_0017c660;
              uVar3 = puVar14[3];
              puVar14 = puVar14 + 4;
              uVar4 = uVar17 >> 8;
              uVar17 = uVar17 << 8 | uVar4;
              *param_2 = puVar14;
              param_2[1] = puVar12 + -4;
              if (0x20 < uVar17) {
                ERR_new();
                ERR_set_debug("ssl/statem/statem_srvr.c",0x5be,"tls_process_client_hello");
                ossl_statem_fatal(param_1,0x2f,0x9f,0);
                goto LAB_0017c38f;
              }
              puVar7 = (ushort *)(ulong)(ushort)(uVar20 << 8 | uVar20 >> 8);
              if (puVar7 <= puVar12 + -4) {
                *(ushort **)(ptr + 0x58) = puVar7;
                puVar12 = param_2[1];
                puVar18 = (ushort *)(ulong)uVar17;
                *(ushort **)(ptr + 0x56) = puVar14;
                puVar16 = (undefined8 *)((long)puVar14 + (long)puVar7);
                *param_2 = (ushort *)puVar16;
                param_2[1] = (ushort *)((long)puVar12 - (long)puVar7);
                if (puVar18 <= (ushort *)((long)puVar12 - (long)puVar7)) {
                  uVar20 = uVar3 << 8 | uVar3 >> 8;
                  piVar1 = ptr + 0xc;
                  puVar12 = (ushort *)(ulong)uVar20;
                  if (uVar17 < 8) {
                    if ((uVar4 & 4) == 0) {
                      if ((uVar17 != 0) && (*(byte *)piVar1 = *(byte *)puVar16, (uVar4 & 2) != 0)) {
                        *(undefined2 *)((long)ptr + 0x2e + (long)puVar18) =
                             *(undefined2 *)((long)((long)puVar16 + -2) + (long)puVar18);
                      }
                    }
                    else {
                      *piVar1 = *(int *)puVar16;
                      *(undefined4 *)((long)(ptr + 0xb) + (long)puVar18) =
                           *(undefined4 *)((long)((long)puVar16 + -4) + (long)puVar18);
                    }
                  }
                  else {
                    *(undefined8 *)(ptr + 0xc) = *puVar16;
                    *(undefined8 *)((long)(ptr + 10) + (long)puVar18) =
                         *(undefined8 *)((long)(puVar16 + -1) + (long)puVar18);
                    lVar10 = (long)piVar1 - ((ulong)(ptr + 0xe) & 0xfffffffffffffff8);
                    uVar21 = (int)lVar10 + (uint)uVar17 & 0xfffffff8;
                    if (7 < uVar21) {
                      uVar6 = 0;
                      do {
                        uVar13 = (ulong)uVar6;
                        uVar6 = uVar6 + 8;
                        *(undefined8 *)(((ulong)(ptr + 0xe) & 0xfffffffffffffff8) + uVar13) =
                             *(undefined8 *)((long)puVar16 + (uVar13 - lVar10));
                      } while (uVar6 < uVar21);
                    }
                  }
                  puVar14 = (ushort *)(ulong)uVar20;
                  puVar16 = (undefined8 *)((long)*param_2 + (long)puVar18);
                  puVar7 = (ushort *)((long)param_2[1] - (long)puVar18);
                  *param_2 = (ushort *)puVar16;
                  param_2[1] = puVar7;
                  if (puVar14 <= puVar7) {
                    *param_2 = (ushort *)((long)puVar16 + (long)puVar14);
                    param_2[1] = (ushort *)((long)puVar7 - (long)puVar14);
                    if ((ushort *)((long)puVar7 - (long)puVar14) == (ushort *)0x0) {
                      *(ushort **)(ptr + 10) = puVar18;
                      if (0x20 < uVar20) {
                        puVar12 = (ushort *)0x20;
                      }
                      *(undefined (*) [16])(ptr + 2) = (undefined  [16])0x0;
                      *(undefined (*) [16])(ptr + 6) = (undefined  [16])0x0;
                      uVar21 = (uint)puVar12;
                      if (puVar14 < puVar12) {
                        ERR_new(0);
                        uVar8 = 0x5da;
                        goto LAB_0017c4a7;
                      }
                      puVar19 = (undefined8 *)((long)ptr + -(long)puVar12 + 0x28);
                      if (uVar21 < 8) {
                        if (((ulong)puVar12 & 4) == 0) {
                          if ((uVar21 != 0) &&
                             (*(byte *)puVar19 = *(byte *)puVar16, ((ulong)puVar12 & 2) != 0)) {
                            *(undefined2 *)((long)ptr + 0x26) =
                                 *(undefined2 *)((long)((long)puVar16 + -2) + (long)puVar12);
                          }
                        }
                        else {
                          *(undefined4 *)puVar19 = *(undefined4 *)puVar16;
                          ptr[9] = *(int *)((long)((long)puVar16 + -4) + (long)puVar12);
                        }
                      }
                      else {
                        *puVar19 = *puVar16;
                        *(undefined8 *)(ptr + 8) =
                             *(undefined8 *)((long)(puVar16 + -1) + (long)puVar12);
                        uVar13 = (long)ptr + -(long)puVar12 + 0x30 & 0xfffffffffffffff8;
                        lVar10 = (long)puVar19 - uVar13;
                        uVar21 = uVar21 + (int)lVar10 & 0xfffffff8;
                        if (7 < uVar21) {
                          uVar6 = 0;
                          do {
                            uVar15 = (ulong)uVar6;
                            uVar6 = uVar6 + 8;
                            *(undefined8 *)(uVar13 + uVar15) =
                                 *(undefined8 *)((long)puVar16 + (uVar15 - lVar10));
                          } while (uVar6 < uVar21);
                        }
                      }
                      pbVar11 = (byte *)0x1;
                      puVar16 = (undefined8 *)&null_compression_27270;
                      *(undefined8 *)(ptr + 0x9c) = 0;
                      *(undefined8 *)(ptr + 0x9e) = 0;
                      goto LAB_0017c2c5;
                    }
                  }
                }
              }
              ERR_new();
              uVar8 = 0x5c8;
            }
            ERR_set_debug("ssl/statem/statem_srvr.c",uVar8,"tls_process_client_hello");
            ossl_statem_fatal(param_1,0x32,0xd5,0);
          }
        }
      }
      else if (((*(long *)(param_1 + 0x240) == 0) || (*(long *)(param_1 + 0x2c8) == 0)) &&
              (*(int *)(param_1 + 0x8e8) == 0)) {
        if (param_2[1] != (ushort *)0x0) {
          puVar12 = (ushort *)((long)param_2[1] + -1);
          bVar2 = *(byte *)*param_2;
          param_2[1] = puVar12;
          *param_2 = (ushort *)((long)*param_2 + 1);
          if (bVar2 == 1) goto LAB_0017c0f8;
        }
        ERR_new();
        uVar8 = 0x5a2;
LAB_0017c4a7:
        ERR_set_debug("ssl/statem/statem_srvr.c",uVar8,"tls_process_client_hello");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
      else {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0x588,"tls_process_client_hello");
        ossl_statem_fatal(param_1,10,0xf4,0);
      }
LAB_0017c38f:
      CRYPTO_free(*(void **)(ptr + 0xa2));
    }
  }
  CRYPTO_free(ptr);
  uVar8 = 0;
LAB_0017c3be:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar8;
}