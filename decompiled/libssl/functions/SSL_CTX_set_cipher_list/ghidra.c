int SSL_CTX_set_cipher_list(SSL_CTX *param_1,char *str)

{
  int iVar1;
  long lVar2;
  long lVar3;
  int iVar4;
  int iVar5;
  
  iVar5 = 0;
  lVar2 = ssl_create_cipher_list
                    (param_1,param_1->sessions,&param_1->cipher_list_by_id,&param_1->cert_store,str,
                     *(undefined8 *)param_1->sid_ctx);
  if (lVar2 != 0) {
    iVar4 = 0;
    while( true ) {
      iVar1 = OPENSSL_sk_num(lVar2);
      if (iVar1 <= iVar4) break;
      lVar3 = OPENSSL_sk_value(lVar2,iVar4);
      iVar4 = iVar4 + 1;
      iVar5 = iVar5 + (uint)(*(int *)(lVar3 + 0x2c) < 0x304);
    }
    if (iVar5 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0xaba,"SSL_CTX_set_cipher_list");
      ERR_set_error(0x14,0xb9,0);
      return 0;
    }
    iVar5 = 1;
  }
  return iVar5;
}