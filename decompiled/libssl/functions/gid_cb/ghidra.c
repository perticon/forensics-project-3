undefined8 gid_cb(long param_1,int param_2,long *param_3)

{
  short sVar1;
  int iVar2;
  long lVar3;
  void *pvVar4;
  undefined8 uVar5;
  short *psVar6;
  char **ppcVar7;
  long lVar8;
  long in_FS_OFFSET;
  char acStack120 [72];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 != 0) {
    if (param_3[1] == param_3[2]) {
      pvVar4 = CRYPTO_realloc((void *)param_3[3],(int)param_3[2] + 0x28,"ssl/t1_lib.c",0x2f7);
      if (pvVar4 == (void *)0x0) goto LAB_001478f1;
      param_3[2] = param_3[2] + 0x28;
      param_3[3] = (long)pvVar4;
    }
    if (param_2 < 0x40) {
      __memcpy_chk(acStack120,param_1,(long)param_2,0x40);
      lVar3 = *param_3;
      acStack120[param_2] = '\0';
      lVar8 = *(long *)(lVar3 + 0x630);
      ppcVar7 = *(char ***)(lVar3 + 0x628);
      if (lVar8 != 0) {
        lVar3 = 0;
        do {
          iVar2 = strcmp(*ppcVar7,acStack120);
          if ((iVar2 == 0) || (iVar2 = strcmp(ppcVar7[1],acStack120), iVar2 == 0)) {
            sVar1 = *(short *)((long)ppcVar7 + 0x1c);
            if (sVar1 != 0) {
              psVar6 = (short *)param_3[3];
              lVar3 = 0;
              if (param_3[1] != 0) goto LAB_001479b1;
              lVar8 = 1;
              goto LAB_001479f8;
            }
            break;
          }
          lVar3 = lVar3 + 1;
          ppcVar7 = ppcVar7 + 7;
        } while (lVar8 != lVar3);
      }
      ERR_new();
      ERR_set_debug("ssl/t1_lib.c",0x304,"gid_cb");
      ERR_set_error(0x14,0x80106,"group \'%s\' cannot be set",acStack120);
      uVar5 = 0;
      goto LAB_001478f3;
    }
  }
LAB_001478f1:
  uVar5 = 0;
LAB_001478f3:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
  while (lVar3 = lVar8 + 1, lVar3 != param_3[1]) {
LAB_001479b1:
    lVar8 = lVar3;
    if (psVar6[lVar8] == sVar1) goto LAB_001478f1;
  }
  lVar8 = lVar8 + 2;
  psVar6 = psVar6 + lVar3;
LAB_001479f8:
  param_3[1] = lVar8;
  uVar5 = 1;
  *psVar6 = sVar1;
  goto LAB_001478f3;
}