static int gid_cb(const char *elem, int len, void *arg)
{
    gid_cb_st *garg = arg;
    size_t i;
    uint16_t gid = 0;
    char etmp[GROUP_NAME_BUFFER_LENGTH];

    if (elem == NULL)
        return 0;
    if (garg->gidcnt == garg->gidmax) {
        uint16_t *tmp =
            OPENSSL_realloc(garg->gid_arr, garg->gidmax + GROUPLIST_INCREMENT);
        if (tmp == NULL)
            return 0;
        garg->gidmax += GROUPLIST_INCREMENT;
        garg->gid_arr = tmp;
    }
    if (len > (int)(sizeof(etmp) - 1))
        return 0;
    memcpy(etmp, elem, len);
    etmp[len] = 0;

    gid = tls1_group_name2id(garg->ctx, etmp);
    if (gid == 0) {
        ERR_raise_data(ERR_LIB_SSL, ERR_R_PASSED_INVALID_ARGUMENT,
                       "group '%s' cannot be set", etmp);
        return 0;
    }
    for (i = 0; i < garg->gidcnt; i++)
        if (garg->gid_arr[i] == gid)
            return 0;
    garg->gid_arr[garg->gidcnt++] = gid;
    return 1;
}