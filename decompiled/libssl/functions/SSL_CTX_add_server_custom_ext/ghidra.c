SSL_CTX_add_server_custom_ext
          (long param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,
          undefined8 param_6,undefined8 param_7)

{
  undefined (*pauVar1) [16];
  long lVar2;
  int iVar3;
  undefined8 *ptr;
  undefined8 *ptr_00;
  long lVar4;
  void *pvVar5;
  
  ptr = (undefined8 *)CRYPTO_malloc(0x18,"ssl/statem/extensions_cust.c",0x19e);
  ptr_00 = (undefined8 *)CRYPTO_malloc(0x10,"ssl/statem/extensions_cust.c",0x1a0);
  if ((ptr == (undefined8 *)0x0) || (ptr_00 == (undefined8 *)0x0)) {
    CRYPTO_free(ptr);
    CRYPTO_free(ptr_00);
    return 0;
  }
  *ptr = param_5;
  ptr[1] = param_3;
  ptr[2] = param_4;
  ptr_00[1] = param_6;
  lVar2 = *(long *)(param_1 + 0x158);
  *ptr_00 = param_7;
  if (param_2 == 0x12) {
    iVar3 = SSL_CTX_ct_is_enabled(param_1);
    if (iVar3 != 0) goto LAB_0016532a;
    SSL_extension_supported(0x12);
  }
  else {
    iVar3 = SSL_extension_supported(param_2);
    if ((iVar3 != 0) || (0xffff < param_2)) goto LAB_0016532a;
  }
  lVar4 = custom_ext_find(lVar2 + 0x1d8,1,param_2,0);
  if ((lVar4 == 0) &&
     (pvVar5 = CRYPTO_realloc(*(void **)(lVar2 + 0x1d8),
                              ((int)*(undefined8 *)(lVar2 + 0x1e0) + 1) * 0x38,
                              "ssl/statem/extensions_cust.c",0x181), pvVar5 != (void *)0x0)) {
    *(void **)(lVar2 + 0x1d8) = pvVar5;
    pauVar1 = (undefined (*) [16])((long)pvVar5 + *(long *)(lVar2 + 0x1e0) * 0x38);
    *pauVar1 = (undefined  [16])0x0;
    *(undefined8 *)(*pauVar1 + 4) = 0x1d000000001;
    *(code **)(pauVar1[2] + 8) = custom_ext_parse_old_cb_wrap;
    *(short *)*pauVar1 = (short)param_2;
    *(undefined8 **)pauVar1[2] = ptr;
    *(undefined8 **)pauVar1[3] = ptr_00;
    *(code **)pauVar1[1] = custom_ext_add_old_cb_wrap;
    *(code **)(pauVar1[1] + 8) = custom_ext_free_old_cb_wrap;
    *(long *)(lVar2 + 0x1e0) = *(long *)(lVar2 + 0x1e0) + 1;
    return 1;
  }
LAB_0016532a:
  CRYPTO_free(ptr);
  CRYPTO_free(ptr_00);
  return 0;
}