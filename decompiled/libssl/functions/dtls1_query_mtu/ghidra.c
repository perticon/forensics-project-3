undefined8 dtls1_query_mtu(SSL *param_1)

{
  char **ppcVar1;
  X509_EXTENSIONS *pXVar2;
  long larg;
  ulong uVar3;
  ulong uVar4;
  BIO *pBVar5;
  undefined8 uVar6;
  
  ppcVar1 = param_1[1].tlsext_ocsp_exts[9].stack.data;
  if (ppcVar1 == (char **)0x0) {
    uVar4 = *(ulong *)&param_1[1].tlsext_ocsp_exts[9].stack.sorted;
    uVar3 = dtls1_min_mtu(param_1);
    if (uVar3 <= uVar4) {
      return 1;
    }
  }
  else {
    pBVar5 = SSL_get_wbio(param_1);
    uVar4 = BIO_ctrl(pBVar5,0x31,0,(void *)0x0);
    pXVar2 = param_1[1].tlsext_ocsp_exts;
    pXVar2[9].stack.data = (char **)0x0;
    uVar3 = (long)ppcVar1 - (uVar4 & 0xffffffff);
    *(ulong *)&pXVar2[9].stack.sorted = uVar3;
    uVar4 = dtls1_min_mtu(param_1);
    if (uVar4 <= uVar3) {
      return 1;
    }
  }
  uVar4 = SSL_get_options(param_1);
  if ((uVar4 & 0x1000) == 0) {
    pBVar5 = SSL_get_wbio(param_1);
    uVar4 = BIO_ctrl(pBVar5,0x28,0,(void *)0x0);
    *(ulong *)&param_1[1].tlsext_ocsp_exts[9].stack.sorted = uVar4;
    uVar3 = dtls1_min_mtu(param_1);
    if (uVar4 < uVar3) {
      pXVar2 = param_1[1].tlsext_ocsp_exts;
      uVar6 = dtls1_min_mtu(param_1);
      *(undefined8 *)&pXVar2[9].stack.sorted = uVar6;
      larg = *(long *)&param_1[1].tlsext_ocsp_exts[9].stack.sorted;
      pBVar5 = SSL_get_wbio(param_1);
      BIO_ctrl(pBVar5,0x2a,larg,(void *)0x0);
    }
    return 1;
  }
  return 0;
}