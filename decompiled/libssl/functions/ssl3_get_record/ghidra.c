undefined8 ssl3_get_record(uint *param_1)

{
  byte bVar1;
  long lVar2;
  ushort *puVar3;
  code *pcVar4;
  void *ptr;
  uint uVar5;
  int iVar6;
  int iVar7;
  undefined8 uVar8;
  ulong uVar9;
  char *pcVar10;
  long lVar11;
  void **ptr_00;
  long lVar12;
  byte *pbVar13;
  ushort uVar14;
  uint *puVar15;
  void **ppvVar16;
  uint *puVar17;
  ushort uVar18;
  ulong uVar19;
  byte *pbVar20;
  byte *pbVar21;
  long lVar22;
  long lVar23;
  size_t len;
  uint *puVar24;
  long in_FS_OFFSET;
  bool bVar25;
  bool bVar26;
  bool bVar27;
  byte bVar28;
  undefined8 uVar29;
  undefined local_90 [8];
  undefined local_88 [72];
  long local_40;
  
  bVar28 = 0;
  puVar15 = param_1 + 0x4aa;
  lVar22 = *(long *)(param_1 + 0x288);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (lVar22 == 0) {
    lVar22 = 1;
  }
  lVar2 = *(long *)(param_1 + 0x246);
  lVar23 = 0;
  puVar17 = puVar15;
  do {
    puVar24 = puVar17;
    if ((param_1[0x319] != 0xf1) || (*(ulong *)(param_1 + 0x72c) < 5)) {
      uVar29 = 0x159a5f;
      uVar8 = ssl3_read_n(param_1,5,*(undefined8 *)(param_1 + 0x322),0);
      if ((int)uVar8 < 1) goto LAB_00159c70;
      uVar9 = *(ulong *)(param_1 + 0x72c);
      param_1[0x319] = 0xf1;
      if ((long)uVar9 < 0) {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0xf6,"ssl3_get_record");
        uVar8 = 0xc0103;
        goto LAB_00159fb4;
      }
      if (uVar9 < 3) {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0xfc,"ssl3_get_record");
        uVar8 = 0xc0103;
        goto LAB_00159d34;
      }
      puVar3 = *(ushort **)(param_1 + 0x72a);
      if ((((param_1[0xe] == 0) || (param_1[0x742] == 0)) ||
          (uVar14 = *puVar3 << 8 | *puVar3 >> 8, *(byte *)(puVar3 + 1) != 1)) ||
         (-1 < (short)uVar14)) {
        pcVar4 = *(code **)(param_1 + 0x130);
        if (uVar9 - 3 < 2) {
          if (pcVar4 != (code *)0x0) {
            (*pcVar4)(0,0,0x100,puVar3,5,param_1,*(undefined8 *)(param_1 + 0x132),uVar29);
          }
          ERR_new();
          ERR_set_debug("ssl/record/ssl3_record.c",0x128,"ssl3_get_record");
          uVar8 = 0xc0103;
          goto LAB_00159d34;
        }
        uVar14 = *(ushort *)((long)puVar3 + 1);
        bVar1 = *(byte *)puVar3;
        uVar18 = uVar14 << 8 | uVar14 >> 8;
        *(ulong *)(puVar24 + 2) =
             (ulong)(ushort)(*(ushort *)((long)puVar3 + 3) << 8 | *(ushort *)((long)puVar3 + 3) >> 8
                            );
        puVar24[1] = (uint)bVar1;
        *puVar24 = (uint)uVar18;
        if (pcVar4 != (code *)0x0) {
          (*pcVar4)(0,uVar18,0x100);
        }
        uVar5 = (uint)uVar18;
        if (param_1[0x282] == 0) {
          if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) != 0) ||
              (iVar6 = **(int **)(param_1 + 2), iVar6 < 0x304)) || (iVar6 == 0x10000)) {
            if ((param_1[0x23a] == 1) || (*param_1 == uVar5)) goto LAB_00159d65;
            if ((((char)(*param_1 >> 8) == (char)(uVar5 >> 8)) && (*(long *)(param_1 + 0x21e) == 0))
               && (*(long *)(param_1 + 0x224) == 0)) {
              if (puVar24[1] == 0x15) {
                ERR_new();
                ERR_set_debug("ssl/record/ssl3_record.c",0x147,"ssl3_get_record");
                uVar8 = 0x10b;
                goto LAB_0015a9bf;
              }
              *param_1 = uVar5;
            }
            ERR_new();
            uVar8 = 0x150;
            goto LAB_0015aa61;
          }
          if ((uVar14 & 0xff) != 3) goto LAB_0015a8ee;
LAB_00159bd5:
          if (*(long *)(param_1 + 0x212) != 0) {
            uVar5 = puVar24[1];
            if (uVar5 != 0x17) {
              if (uVar5 == 0x14) {
                if ((*(long *)(param_1 + 0x90) != 0) && (*(long *)(param_1 + 0xb2) != 0))
                goto LAB_0015a8b3;
              }
              else if ((uVar5 != 0x15) || (param_1[0x20] != 1)) {
LAB_0015a8b3:
                ERR_new();
                ERR_set_debug("ssl/record/ssl3_record.c",0x17a,"ssl3_get_record");
                uVar8 = 0x1bb;
                goto LAB_0015a8d7;
              }
            }
            if (*puVar24 != 0x303) {
              ERR_new();
              ERR_set_debug("ssl/record/ssl3_record.c",0x17f,"ssl3_get_record");
              uVar8 = 0x10b;
              goto LAB_00159d34;
            }
          }
          uVar9 = *(ulong *)(puVar24 + 2);
          if (uVar9 <= *(long *)(param_1 + 0x322) - 5U) goto LAB_00159cd1;
        }
        else {
LAB_00159d65:
          if ((uVar14 & 0xff) != 3) {
LAB_0015a8ee:
            bVar25 = false;
            bVar27 = param_1[0x742] == 0;
            if (!bVar27) {
              pbVar20 = *(byte **)(param_1 + 0x72a);
              lVar22 = 4;
              pbVar13 = pbVar20;
              pbVar21 = &DAT_0018b780;
              goto code_r0x0015a914;
            }
            ERR_new();
            uVar8 = 0x16b;
LAB_0015aa61:
            ERR_set_debug("ssl/record/ssl3_record.c",uVar8,"ssl3_get_record");
            ossl_statem_fatal(param_1,0x46,0x10b,0);
            uVar8 = 0xffffffff;
            goto LAB_00159c70;
          }
          if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
              (iVar6 = **(int **)(param_1 + 2), iVar6 != 0x10000)) && (0x303 < iVar6))
          goto LAB_00159bd5;
          uVar9 = *(ulong *)(puVar24 + 2);
          if (uVar9 <= *(long *)(param_1 + 0x322) - 5U) goto LAB_00159d9e;
        }
        ERR_new();
        uVar8 = 0x187;
      }
      else {
        uVar9 = (ulong)(uVar14 & 0x7fff);
        puVar24[1] = 0x16;
        *(ulong *)(puVar24 + 2) = uVar9;
        lVar11 = *(long *)(param_1 + 0x322);
        *puVar24 = 2;
        if (uVar9 <= lVar11 - 2U) {
          if (8 < uVar9) goto LAB_00159ca4;
          ERR_new();
          uVar8 = 0x11b;
          goto LAB_00159b08;
        }
        ERR_new();
        uVar8 = 0x115;
      }
      ERR_set_debug("ssl/record/ssl3_record.c",uVar8,"ssl3_get_record");
      uVar8 = 0xc6;
LAB_00159c52:
      ossl_statem_fatal(param_1,0x16,uVar8,0);
      uVar8 = 0xffffffff;
      goto LAB_00159c70;
    }
    uVar9 = *(ulong *)(puVar24 + 2);
LAB_00159ca4:
    if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
        (iVar6 = **(int **)(param_1 + 2), 0x303 < iVar6)) && (iVar6 != 0x10000)) {
LAB_00159cd1:
      if (0x4100 < uVar9) {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0x198,"ssl3_get_record");
        uVar8 = 0x96;
        goto LAB_00159c52;
      }
    }
    else {
LAB_00159d9e:
      if ((-(ulong)(*(long *)(param_1 + 0x21c) == 0) & 0xfffffffffffffc00) + 0x4540 < uVar9) {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0x1ad,"ssl3_get_record");
        uVar8 = 0x96;
        goto LAB_00159c52;
      }
    }
    uVar19 = uVar9 - 3;
    if (*puVar24 != 2) {
      uVar19 = uVar9;
    }
    if (uVar19 != 0) {
      uVar8 = ssl3_read_n(param_1,uVar19,uVar19,1,0,local_90);
      if ((int)uVar8 < 1) goto LAB_00159c70;
      uVar9 = *(ulong *)(puVar24 + 2);
    }
    lVar11 = *(long *)(param_1 + 0x72a);
    param_1[0x319] = 0xf0;
    *(ulong *)(puVar24 + 4) = uVar9;
    puVar24[0xe] = 0;
    lVar12 = lVar11 + 2;
    if (*puVar24 != 2) {
      lVar12 = lVar11 + 5;
    }
    lVar23 = lVar23 + 1;
    *(long *)(puVar24 + 8) = lVar12;
    *(long *)(puVar24 + 10) = lVar12;
    *(undefined8 *)(param_1 + 0x72c) = 0;
    param_1[0x742] = 0;
    if ((lVar23 == lVar22) || (puVar24[1] != 0x17)) break;
    if (((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 1) == 0) ||
       (*(long *)(param_1 + 0x212) == 0)) goto LAB_00159ea9;
    EVP_CIPHER_CTX_get0_cipher();
    uVar9 = EVP_CIPHER_get_flags();
    if (((((uVar9 & 0x800000) == 0) || (*(long *)(param_1 + 0x31e) == 0)) ||
        (*(ulong *)(param_1 + 0x326) < 5)) ||
       ((pcVar10 = (char *)(*(long *)(param_1 + 0x31e) + *(long *)(param_1 + 0x324)),
        *pcVar10 != '\x17' ||
        (uVar14 = *(ushort *)(pcVar10 + 3), puVar17 = puVar24 + 0x14,
        *(ulong *)(param_1 + 0x326) < (ulong)(ushort)(uVar14 << 8 | uVar14 >> 8) + 5)))) break;
  } while( true );
  if (((lVar23 == 1) && (puVar24[1] == 0x14)) &&
     ((((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0 &&
         (iVar6 = **(int **)(param_1 + 2), 0x303 < iVar6)) && (iVar6 != 0x10000)) ||
       (param_1[0x23a] != 0)) &&
      ((*(long *)(param_1 + 0x90) == 0 || (*(long *)(param_1 + 0xb2) == 0)))))) {
    if ((*(long *)(puVar24 + 2) == 1) && (**(char **)(puVar24 + 8) == '\x01')) {
      puVar24[1] = 0x16;
      lVar22 = *(long *)(param_1 + 0x734);
      *(ulong *)(param_1 + 0x734) = lVar22 + 1U;
      if (lVar22 + 1U < 0x21) {
        puVar24[0xe] = 1;
        uVar8 = 1;
        *(undefined8 *)(param_1 + 0x31a) = 1;
      }
      else {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0x20c,"ssl3_get_record");
        uVar8 = 0x106;
LAB_0015a8d7:
        ossl_statem_fatal(param_1,10,uVar8,0);
        uVar8 = 0xffffffff;
      }
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x200,"ssl3_get_record");
      ossl_statem_fatal(param_1,0x2f,0x104,0);
      uVar8 = 0xffffffff;
    }
    goto LAB_00159c70;
  }
LAB_00159ea9:
  ptr_00 = *(void ***)(param_1 + 0x218);
  if (ptr_00 == (void **)0x0) {
    len = 0;
  }
  else {
    lVar22 = EVP_MD_CTX_get0_md(ptr_00);
    if (lVar22 != 0) {
      uVar5 = EVP_MD_get_size(lVar22);
      if (uVar5 < 0x41) {
        len = (size_t)(int)uVar5;
        if (((*(byte *)((long)param_1 + 0xa9) & 1) != 0) && (*(long *)(param_1 + 0x218) != 0))
        goto LAB_00159efa;
        if (uVar5 == 0) goto LAB_00159f85;
        ptr_00 = (void **)CRYPTO_zalloc(lVar23 * 0x10,"ssl/record/ssl3_record.c",0x245);
        if (ptr_00 != (void **)0x0) goto LAB_00159fd3;
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0x247,"ssl3_get_record");
        uVar8 = 0xc0100;
      }
      else {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0x21f,"ssl3_get_record");
        uVar8 = 0x80006;
      }
LAB_00159fb4:
      ossl_statem_fatal(param_1,0x50,uVar8,0);
      uVar8 = 0xffffffff;
      goto LAB_00159c70;
    }
    if (((*(byte *)((long)param_1 + 0xa9) & 1) != 0) && (len = 0, *(long *)(param_1 + 0x218) != 0))
    {
LAB_00159efa:
      puVar17 = puVar15;
      do {
        if (*(ulong *)(puVar17 + 2) < len) {
          ERR_new();
          uVar8 = 0x231;
LAB_00159b08:
          ERR_set_debug("ssl/record/ssl3_record.c",uVar8,"ssl3_get_record");
          uVar8 = 0xa0;
LAB_00159d34:
          ossl_statem_fatal(param_1,0x32,uVar8,0);
          uVar8 = 0xffffffff;
          goto LAB_00159c70;
        }
        lVar11 = *(ulong *)(puVar17 + 2) - len;
        *(long *)(puVar17 + 2) = lVar11;
        lVar22 = *(long *)(puVar17 + 8);
        iVar6 = (**(code **)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 8))
                          (param_1,puVar17,local_88,0);
        if ((iVar6 == 0) ||
           (iVar6 = CRYPTO_memcmp(local_88,(void *)(lVar11 + lVar22),len), iVar6 != 0)) {
          ERR_new();
          ERR_set_debug("ssl/record/ssl3_record.c",0x238,"ssl3_get_record");
          ossl_statem_fatal(param_1,0x14,0x119,0);
          uVar8 = 0xffffffff;
          goto LAB_00159c70;
        }
        puVar17 = puVar17 + 0x14;
      } while (puVar15 + lVar23 * 0x14 != puVar17);
    }
LAB_00159f85:
    len = 0;
    ptr_00 = (void **)0x0;
  }
LAB_00159fd3:
  uVar8 = 0x159ff2;
  iVar6 = (***(code ***)(*(long *)(param_1 + 2) + 0xc0))(param_1,puVar15,lVar23,0,ptr_00,len);
  if (iVar6 == 0) {
    iVar6 = ossl_statem_in_error(param_1);
    if (iVar6 != 0) goto LAB_0015a340;
    if ((lVar23 == 1) && (iVar6 = ossl_statem_skip_early_data(param_1), iVar6 != 0)) {
      iVar6 = early_data_count_ok(param_1,*(undefined8 *)(param_1 + 0x4ac),0x68,0);
      if (iVar6 == 0) goto LAB_0015a340;
      *(undefined8 *)(param_1 + 0x4ac) = 0;
      param_1[0x4b8] = 1;
      *(undefined8 *)(param_1 + 0x31a) = 1;
      RECORD_LAYER_reset_read_sequence(param_1 + 0x316);
      uVar8 = 1;
    }
    else {
      ERR_new();
      uVar8 = 0x26e;
LAB_0015a591:
      ERR_set_debug("ssl/record/ssl3_record.c",uVar8,"ssl3_get_record");
      ossl_statem_fatal(param_1,0x14,0x119,0);
      uVar8 = 0xffffffff;
    }
  }
  else {
    if (((lVar2 != 0) && (*(long *)(param_1 + 0x212) != 0)) &&
       ((*(byte *)((long)param_1 + 0xa9) & 1) == 0)) {
      uVar8 = 0x15a600;
      lVar22 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x218));
      if (lVar22 != 0) {
        puVar17 = puVar15;
        ppvVar16 = ptr_00;
        do {
          uVar8 = 0x15a671;
          iVar7 = (**(code **)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 8))
                            (param_1,puVar17,local_88,0);
          if (((iVar7 == 0) || (ppvVar16 == (void **)0x0)) || (*ppvVar16 == (void *)0x0)) {
LAB_0015a700:
            iVar6 = 0;
          }
          else {
            uVar8 = 0x15a697;
            iVar7 = CRYPTO_memcmp(local_88,*ppvVar16,len);
            if (iVar7 != 0) goto LAB_0015a700;
          }
          if (len + 0x4400 <= *(ulong *)(puVar17 + 2) && *(ulong *)(puVar17 + 2) != len + 0x4400) {
            iVar6 = 0;
          }
          puVar17 = puVar17 + 0x14;
          ppvVar16 = ppvVar16 + 2;
        } while (ptr_00 + lVar23 * 2 != ppvVar16);
        if (iVar6 == 0) {
          iVar6 = ossl_statem_in_error(param_1);
          if (iVar6 == 0) {
            ERR_new();
            uVar8 = 0x296;
            goto LAB_0015a591;
          }
          goto LAB_0015a340;
        }
      }
    }
    puVar17 = puVar15 + lVar23 * 0x14;
    do {
      uVar9 = *(ulong *)(puVar15 + 2);
      if (*(long *)(param_1 + 0x21c) != 0) {
        if (uVar9 < 0x4401) {
          uVar8 = 0x15a086;
          iVar6 = ssl3_do_uncompress(param_1,puVar15);
          if (iVar6 != 0) {
            uVar9 = *(ulong *)(puVar15 + 2);
            goto LAB_0015a092;
          }
          ERR_new();
          ERR_set_debug("ssl/record/ssl3_record.c",0x2a8,"ssl3_get_record");
          ossl_statem_fatal(param_1,0x1e,0x6b,0);
          uVar8 = 0xffffffff;
        }
        else {
          ERR_new();
          ERR_set_debug("ssl/record/ssl3_record.c",0x2a3,"ssl3_get_record");
          uVar8 = 0x8c;
LAB_0015a764:
          ossl_statem_fatal(param_1,0x16,uVar8,0);
          uVar8 = 0xffffffff;
        }
        goto LAB_0015a345;
      }
LAB_0015a092:
      if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) != 0) ||
          (iVar6 = **(int **)(param_1 + 2), iVar6 == 0x10000)) || (iVar6 < 0x304))
      goto LAB_0015a0e0;
      uVar5 = puVar15[1];
      if ((*(long *)(param_1 + 0x212) == 0) || (uVar5 == 0x15)) goto LAB_0015a0cb;
      if ((uVar9 == 0) || (uVar5 != 0x17)) {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",700,"ssl3_get_record");
        uVar8 = 0x1bb;
LAB_0015a330:
        ossl_statem_fatal(param_1,10,uVar8,0);
        goto LAB_0015a340;
      }
      pbVar20 = *(byte **)(puVar15 + 8);
      do {
        uVar9 = uVar9 - 1;
        pbVar13 = pbVar20;
        if (uVar9 == 0) break;
        pbVar13 = pbVar20 + uVar9;
      } while (pbVar20[uVar9] == 0);
      *(ulong *)(puVar15 + 2) = uVar9;
      bVar28 = *pbVar13;
      uVar5 = (uint)bVar28;
      puVar15[1] = (uint)bVar28;
      if (2 < (byte)(bVar28 - 0x15)) {
        ERR_new();
        ERR_set_debug("ssl/record/ssl3_record.c",0x2cb,"ssl3_get_record");
        uVar8 = 0x1bb;
        goto LAB_0015a330;
      }
      if (*(code **)(param_1 + 0x130) == (code *)0x0) {
LAB_0015a0cb:
        if ((uVar5 - 0x15 < 2) && (uVar9 == 0)) {
          ERR_new();
          ERR_set_debug("ssl/record/ssl3_record.c",0x2db,"ssl3_get_record");
          uVar8 = 0x10f;
          goto LAB_0015a330;
        }
      }
      else {
        (**(code **)(param_1 + 0x130))
                  (0,*param_1,0x101,puVar15 + 1,1,param_1,*(undefined8 *)(param_1 + 0x132),uVar8);
        if ((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) {
          iVar6 = **(int **)(param_1 + 2);
          uVar9 = *(ulong *)(puVar15 + 2);
          if ((iVar6 != 0x10000) && (0x303 < iVar6)) {
            uVar5 = puVar15[1];
            goto LAB_0015a0cb;
          }
        }
        else {
          uVar9 = *(ulong *)(puVar15 + 2);
        }
      }
LAB_0015a0e0:
      if (0x4000 < uVar9) {
        ERR_new();
        uVar8 = 0x2e8;
LAB_0015a751:
        ERR_set_debug("ssl/record/ssl3_record.c",uVar8,"ssl3_get_record");
        uVar8 = 0x92;
        goto LAB_0015a764;
      }
      if (((*(long *)(param_1 + 0x246) != 0) &&
          (bVar28 = *(char *)(*(long *)(param_1 + 0x246) + 0x368) - 1, bVar28 < 4)) &&
         ((uint)(0x200 << (bVar28 & 0x1f)) < uVar9)) {
        ERR_new();
        uVar8 = 0x2f3;
        goto LAB_0015a751;
      }
      *(undefined8 *)(puVar15 + 6) = 0;
      if (uVar9 == 0) {
        lVar22 = *(long *)(param_1 + 0x734);
        *(ulong *)(param_1 + 0x734) = lVar22 + 1U;
        if (0x20 < lVar22 + 1U) {
          ERR_new();
          ERR_set_debug("ssl/record/ssl3_record.c",0x305,"ssl3_get_record");
          uVar8 = 0x12a;
          goto LAB_0015a330;
        }
      }
      else {
        *(undefined8 *)(param_1 + 0x734) = 0;
      }
      puVar15 = puVar15 + 0x14;
    } while (puVar15 != puVar17);
    if (((param_1[0x21] != 0xb) || (param_1[0x4ab] != 0x17)) ||
       (iVar6 = early_data_count_ok(param_1,*(undefined8 *)(param_1 + 0x4ac),0,0), iVar6 != 0)) {
      *(long *)(param_1 + 0x31a) = lVar23;
      uVar8 = 1;
      goto LAB_0015a345;
    }
LAB_0015a340:
    uVar8 = 0xffffffff;
  }
LAB_0015a345:
  if (ptr_00 != (void **)0x0) {
    ppvVar16 = ptr_00;
    do {
      while (*(int *)(ppvVar16 + 1) != 0) {
        ptr = *ppvVar16;
        ppvVar16 = ppvVar16 + 2;
        CRYPTO_free(ptr);
        if (ppvVar16 == ptr_00 + lVar23 * 2) goto LAB_0015a391;
      }
      ppvVar16 = ppvVar16 + 2;
    } while (ppvVar16 != ptr_00 + lVar23 * 2);
LAB_0015a391:
    CRYPTO_free(ptr_00);
  }
  goto LAB_00159c70;
  while( true ) {
    lVar22 = lVar22 + -1;
    bVar25 = *pbVar13 < *pbVar21;
    bVar27 = *pbVar13 == *pbVar21;
    pbVar13 = pbVar13 + (ulong)bVar28 * -2 + 1;
    pbVar21 = pbVar21 + (ulong)bVar28 * -2 + 1;
    if (!bVar27) break;
code_r0x0015a914:
    if (lVar22 == 0) break;
  }
  bVar26 = false;
  bVar25 = (!bVar25 && !bVar27) == bVar25;
  if (bVar25) {
LAB_0015ab46:
    ERR_new();
    ERR_set_debug("ssl/record/ssl3_record.c",0x15e,"ssl3_get_record");
    uVar8 = 0x9c;
  }
  else {
    lVar22 = 5;
    pbVar13 = pbVar20;
    pbVar21 = (byte *)"POST ";
    do {
      if (lVar22 == 0) break;
      lVar22 = lVar22 + -1;
      bVar26 = *pbVar13 < *pbVar21;
      bVar25 = *pbVar13 == *pbVar21;
      pbVar13 = pbVar13 + (ulong)bVar28 * -2 + 1;
      pbVar21 = pbVar21 + (ulong)bVar28 * -2 + 1;
    } while (bVar25);
    bVar27 = false;
    bVar25 = (!bVar26 && !bVar25) == bVar26;
    if (bVar25) goto LAB_0015ab46;
    lVar22 = 5;
    pbVar13 = pbVar20;
    pbVar21 = (byte *)"HEAD ";
    do {
      if (lVar22 == 0) break;
      lVar22 = lVar22 + -1;
      bVar27 = *pbVar13 < *pbVar21;
      bVar25 = *pbVar13 == *pbVar21;
      pbVar13 = pbVar13 + (ulong)bVar28 * -2 + 1;
      pbVar21 = pbVar21 + (ulong)bVar28 * -2 + 1;
    } while (bVar25);
    bVar26 = false;
    bVar25 = (!bVar27 && !bVar25) == bVar27;
    if (bVar25) goto LAB_0015ab46;
    lVar22 = 4;
    pbVar13 = pbVar20;
    pbVar21 = &DAT_0018b791;
    do {
      if (lVar22 == 0) break;
      lVar22 = lVar22 + -1;
      bVar26 = *pbVar13 < *pbVar21;
      bVar25 = *pbVar13 == *pbVar21;
      pbVar13 = pbVar13 + (ulong)bVar28 * -2 + 1;
      pbVar21 = pbVar21 + (ulong)bVar28 * -2 + 1;
    } while (bVar25);
    bVar27 = false;
    bVar25 = (!bVar26 && !bVar25) == bVar26;
    if (bVar25) goto LAB_0015ab46;
    lVar22 = 5;
    pbVar13 = (byte *)"CONNE";
    do {
      if (lVar22 == 0) break;
      lVar22 = lVar22 + -1;
      bVar27 = *pbVar20 < *pbVar13;
      bVar25 = *pbVar20 == *pbVar13;
      pbVar20 = pbVar20 + (ulong)bVar28 * -2 + 1;
      pbVar13 = pbVar13 + (ulong)bVar28 * -2 + 1;
    } while (bVar25);
    if ((!bVar27 && !bVar25) == bVar27) {
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x161,"ssl3_get_record");
      uVar8 = 0x9b;
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x167,"ssl3_get_record");
      uVar8 = 0x10b;
    }
  }
LAB_0015a9bf:
  ossl_statem_fatal(param_1,0xffffffff,uVar8,0);
  uVar8 = 0xffffffff;
LAB_00159c70:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar8;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}