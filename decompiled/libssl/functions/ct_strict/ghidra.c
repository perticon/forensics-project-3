undefined8 ct_strict(undefined8 param_1,long param_2)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  int iVar4;
  
  if (param_2 != 0) {
    iVar1 = OPENSSL_sk_num(param_2);
    if (0 < iVar1) {
      iVar4 = 0;
      do {
        uVar3 = OPENSSL_sk_value(param_2,iVar4);
        iVar2 = SCT_get_validation_status(uVar3);
        if (iVar2 == 2) {
          return 1;
        }
        iVar4 = iVar4 + 1;
      } while (iVar1 != iVar4);
    }
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x1408,"ct_strict");
  ERR_set_error(0x14,0xd8,0);
  return 0;
}