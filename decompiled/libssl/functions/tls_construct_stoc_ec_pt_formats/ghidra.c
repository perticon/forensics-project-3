undefined8 tls_construct_stoc_ec_pt_formats(long param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined8 local_30;
  undefined8 local_28;
  long local_20;
  
  uVar2 = 2;
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (((*(uint *)(*(long *)(param_1 + 0x2e0) + 0x1c) & 4 |
       *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20) & 8) != 0) && (*(long *)(param_1 + 0xab8) != 0))
  {
    tls1_get_formatlist(param_1,&local_30,&local_28);
    iVar1 = WPACKET_put_bytes__(param_2,0xb,2);
    if (iVar1 != 0) {
      iVar1 = WPACKET_start_sub_packet_len__(param_2,2);
      if (iVar1 != 0) {
        iVar1 = WPACKET_sub_memcpy__(param_2,local_30,local_28,1);
        if (iVar1 != 0) {
          iVar1 = WPACKET_close(param_2);
          if (iVar1 != 0) {
            uVar2 = 1;
            goto LAB_001681fe;
          }
        }
      }
    }
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_srvr.c",0x527,"tls_construct_stoc_ec_pt_formats");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    uVar2 = 0;
  }
LAB_001681fe:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}