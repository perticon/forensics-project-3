long DTLS_get_data_mtu(SSL *param_1)

{
  int iVar1;
  SSL_CIPHER *pSVar2;
  ulong uVar3;
  long lVar4;
  long in_FS_OFFSET;
  long local_40;
  ulong local_38;
  ulong local_30;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  pSVar2 = SSL_get_current_cipher(param_1);
  if (pSVar2 != (SSL_CIPHER *)0x0) {
    uVar3 = *(ulong *)&param_1[1].tlsext_ocsp_exts[9].stack.sorted;
    iVar1 = ssl_cipher_get_overhead(pSVar2,&local_40,&local_38,&local_30,&local_28);
    if (iVar1 != 0) {
      if ((*(byte *)((long)&param_1->hit + 1) & 1) == 0) {
        local_38 = local_38 + local_40;
      }
      else {
        local_28 = local_28 + local_40;
      }
      if (local_28 + 0xdU < uVar3) {
        uVar3 = (uVar3 - 0xd) - local_28;
        if (local_30 != 0) {
          uVar3 = uVar3 - uVar3 % local_30;
        }
        if (local_38 < uVar3) {
          lVar4 = uVar3 - local_38;
          goto LAB_00125782;
        }
      }
    }
  }
  lVar4 = 0;
LAB_00125782:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return lVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}