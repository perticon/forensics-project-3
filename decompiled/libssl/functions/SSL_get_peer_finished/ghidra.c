size_t SSL_get_peer_finished(SSL *s,void *buf,size_t count)

{
  BIO *pBVar1;
  
  pBVar1 = s[1].wbio;
  if (pBVar1 <= count) {
    count = (size_t)pBVar1;
  }
  memcpy(buf,&s->tlsext_opaque_prf_input_len,count);
  return (size_t)pBVar1;
}