undefined8 dtls1_record_replay_check(long param_1,ulong *param_2)

{
  int iVar1;
  long lVar2;
  ulong uVar3;
  ulong uVar4;
  
  uVar3 = *(ulong *)(param_1 + 0x1cf8);
  uVar4 = param_2[1];
  uVar3 = uVar3 >> 0x38 | (uVar3 & 0xff000000000000) >> 0x28 | (uVar3 & 0xff0000000000) >> 0x18 |
          (uVar3 & 0xff00000000) >> 8 | (uVar3 & 0xff000000) << 8 | (uVar3 & 0xff0000) << 0x18 |
          (uVar3 & 0xff00) << 0x28 | uVar3 << 0x38;
  uVar4 = uVar4 >> 0x38 | (uVar4 & 0xff000000000000) >> 0x28 | (uVar4 & 0xff0000000000) >> 0x18 |
          (uVar4 & 0xff00000000) >> 8 | (uVar4 & 0xff000000) << 8 | (uVar4 & 0xff0000) << 0x18 |
          (uVar4 & 0xff00) << 0x28 | uVar4 << 0x38;
  lVar2 = uVar3 - uVar4;
  if (((uVar3 <= uVar4) || (-1 < lVar2)) &&
     (((uVar3 < uVar4 && (0 < lVar2)) ||
      ((lVar2 < 0x81 &&
       ((lVar2 < -0x80 ||
        ((iVar1 = (int)lVar2, iVar1 < 1 &&
         ((iVar1 < -0x3f || ((*param_2 >> ((ulong)(uint)-iVar1 & 0x3f) & 1) != 0)))))))))))) {
    return 0;
  }
  SSL3_RECORD_set_seq_num(param_1 + 0x12a8,param_1 + 0x1cf8);
  return 1;
}