static int cmd_RequestCAPath(SSL_CONF_CTX *cctx, const char *value)
{
    if (cctx->canames == NULL)
        cctx->canames = sk_X509_NAME_new_null();
    if (cctx->canames == NULL)
        return 0;
    return SSL_add_dir_cert_subjects_to_stack(cctx->canames, value);
}