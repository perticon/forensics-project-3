long ssl_get_auto_dh(long param_1)

{
  int iVar1;
  int iVar2;
  BIGNUM *a;
  EVP_PKEY_CTX *ctx;
  long lVar3;
  long lVar4;
  long in_FS_OFFSET;
  long local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = 0;
  if (*(int *)(*(long *)(param_1 + 0x898) + 0x18) == 2) {
    iVar1 = 0x50;
  }
  else if ((*(byte *)(*(long *)(param_1 + 0x2e0) + 0x20) & 0x14) == 0) {
    lVar4 = *(long *)(param_1 + 0x388);
    if (lVar4 == 0) goto LAB_0014a799;
    iVar1 = EVP_PKEY_get_security_bits(*(undefined8 *)(lVar4 + 8));
  }
  else {
    iVar1 = 0x80;
    if (*(int *)(*(long *)(param_1 + 0x2e0) + 0x44) != 0x100) {
      iVar1 = 0x50;
    }
  }
  iVar2 = ssl_get_security_level_bits(param_1,0,0);
  if (iVar2 <= iVar1) {
    iVar2 = iVar1;
  }
  if (iVar2 < 0xc0) {
    if (0x97 < iVar2) {
      a = (BIGNUM *)BN_get_rfc3526_prime_4096();
      goto LAB_0014a738;
    }
    if (0x7f < iVar2) {
      a = (BIGNUM *)BN_get_rfc3526_prime_3072();
      goto LAB_0014a738;
    }
    if (iVar2 < 0x70) {
      a = (BIGNUM *)BN_get_rfc2409_prime_1024();
      goto LAB_0014a738;
    }
    a = (BIGNUM *)BN_get_rfc3526_prime_2048();
    if (a != (BIGNUM *)0x0) goto LAB_0014a73d;
LAB_0014a700:
    lVar3 = 0;
    ctx = (EVP_PKEY_CTX *)0x0;
    lVar4 = 0;
  }
  else {
    a = (BIGNUM *)BN_get_rfc3526_prime_8192(0);
LAB_0014a738:
    if (a == (BIGNUM *)0x0) goto LAB_0014a700;
LAB_0014a73d:
    ctx = (EVP_PKEY_CTX *)
          EVP_PKEY_CTX_new_from_name
                    (**(undefined8 **)(param_1 + 0x9a8),&DAT_00183578,
                     (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
    if (ctx == (EVP_PKEY_CTX *)0x0) goto LAB_0014a700;
    lVar3 = 0;
    iVar1 = EVP_PKEY_fromdata_init(ctx);
    lVar4 = 0;
    if ((((iVar1 == 1) && (lVar3 = OSSL_PARAM_BLD_new(), lVar3 != 0)) &&
        (iVar1 = OSSL_PARAM_BLD_push_BN(lVar3,&DAT_00188a4e,a), lVar4 = 0, iVar1 != 0)) &&
       ((iVar1 = OSSL_PARAM_BLD_push_uint(lVar3,&DAT_00188a53,2), iVar1 != 0 &&
        (lVar4 = OSSL_PARAM_BLD_to_param(lVar3), lVar4 != 0)))) {
      EVP_PKEY_fromdata(ctx,&local_38,0x84,lVar4);
    }
  }
  OSSL_PARAM_free(lVar4);
  OSSL_PARAM_BLD_free(lVar3);
  EVP_PKEY_CTX_free(ctx);
  BN_free(a);
  lVar4 = local_38;
LAB_0014a799:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return lVar4;
}