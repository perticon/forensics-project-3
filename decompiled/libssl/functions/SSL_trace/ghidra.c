void SSL_trace(uint param_1,undefined8 param_2,int param_3,uint *param_4,uint *param_5,SSL *param_6,
              BIO *param_7)

{
  ushort *puVar1;
  byte *pbVar2;
  byte bVar3;
  byte bVar4;
  byte bVar5;
  byte bVar6;
  uint uVar7;
  int iVar8;
  undefined1 *puVar9;
  char *pcVar10;
  X509 *x;
  X509_NAME *nm;
  ushort uVar11;
  undefined8 uVar12;
  ushort uVar13;
  char *pcVar14;
  undefined8 uVar15;
  uint uVar16;
  ulong uVar17;
  long lVar18;
  ulong uVar19;
  uint *puVar20;
  uint **ppuVar21;
  uint **ppuVar22;
  uint *puVar23;
  uint *puVar24;
  uint *puVar25;
  long in_FS_OFFSET;
  uint local_80;
  uint *local_68;
  uint *local_60;
  uint *local_58;
  uint *local_50;
  uint *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 != 0x16) {
    if (param_3 < 0x17) {
      if (param_3 != 0x14) {
        if (param_3 == 0x15) {
          if (param_5 == (uint *)0x2) {
            bVar3 = *(byte *)((long)param_4 + 1);
            pcVar14 = SSL_alert_desc_string_long((uint)bVar3);
            bVar4 = *(byte *)param_4;
            pcVar10 = SSL_alert_type_string_long((uint)bVar4 << 8);
            BIO_printf(param_7,"    Level=%s(%d), description=%s(%d)\n",pcVar10,(ulong)bVar4,pcVar14
                       ,(ulong)bVar3);
          }
          else {
            BIO_puts(param_7,"    Illegal Alert Length\n");
          }
        }
        goto LAB_0014e32f;
      }
      if (param_5 == (uint *)0x1) {
        if (*(byte *)param_4 == 1) {
          BIO_puts(param_7,"    change_cipher_spec (1)\n");
          goto LAB_0014e32f;
        }
        BIO_indent(param_7,4,0x50);
        BIO_printf(param_7,"%s (len=%d): ","unknown value",1);
        BIO_printf(param_7,"%02X",(ulong)*(byte *)param_4);
      }
      else {
        BIO_indent(param_7,4,0x50);
        BIO_printf(param_7,"%s (len=%d): ","unknown value",(ulong)param_5 & 0xffffffff);
        if (param_5 != (uint *)0x0) {
          puVar24 = (uint *)0x0;
          do {
            pbVar2 = (byte *)((long)param_4 + (long)puVar24);
            puVar24 = (uint *)((long)puVar24 + 1);
            BIO_printf(param_7,"%02X",(ulong)*pbVar2);
          } while (puVar24 < param_5);
        }
      }
      goto LAB_0014e471;
    }
    if (param_3 != 0x100) {
      if (param_3 != 0x101) goto LAB_0014e32f;
      bVar3 = *(byte *)param_4;
      if (bVar3 == 0x14) {
        puVar9 = ssl_content_tbl;
LAB_0014e803:
        pcVar14 = *(char **)(puVar9 + 8);
      }
      else {
        if (bVar3 == 0x15) {
          puVar9 = (undefined1 *)0x1a57f0;
          goto LAB_0014e803;
        }
        if (bVar3 == 0x16) {
          puVar9 = (undefined1 *)0x1a5800;
          goto LAB_0014e803;
        }
        pcVar14 = "UNKNOWN";
        puVar9 = (undefined1 *)0x1a5810;
        if (bVar3 == 0x17) goto LAB_0014e803;
      }
      BIO_printf(param_7,"  Inner Content Type = %s (%d)",pcVar14);
      goto LAB_0014e32f;
    }
    if (param_5 < (uint *)((-(ulong)((*(uint *)(param_6->method->get_timeout + 0x60) & 8) == 0) &
                           0xfffffffffffffff8) + 0xd)) {
      puVar24 = (uint *)((long)param_4 + (long)param_5);
      pcVar14 = "Sent";
      if (param_1 == 0) {
        pcVar14 = "Received";
      }
      BIO_puts(param_7,pcVar14);
      BIO_indent(param_7,0,0x50);
      BIO_printf(param_7,"%s (len=%d): "," too short message",(ulong)param_5 & 0xffffffff);
      if (param_5 != (uint *)0x0) {
        do {
          bVar3 = *(byte *)param_4;
          param_4 = (uint *)((long)param_4 + 1);
          BIO_printf(param_7,"%02X",(ulong)bVar3);
        } while (puVar24 != param_4);
      }
      goto LAB_0014e471;
    }
    pcVar14 = "Sent";
    uVar13 = *(ushort *)((long)param_4 + 1) << 8 | *(ushort *)((long)param_4 + 1) >> 8;
    if (param_1 == 0) {
      pcVar14 = "Received";
    }
    BIO_puts(param_7,pcVar14);
    if (uVar13 == 0x300) {
      puVar9 = ssl_version_tbl;
LAB_0014f9d0:
      pcVar14 = *(char **)(puVar9 + 8);
    }
    else {
      if (uVar13 == 0x301) {
        puVar9 = (undefined1 *)0x1a5830;
        goto LAB_0014f9d0;
      }
      if (uVar13 == 0x302) {
        puVar9 = (undefined1 *)0x1a5840;
        goto LAB_0014f9d0;
      }
      if (uVar13 == 0x303) {
        puVar9 = (undefined1 *)0x1a5850;
        goto LAB_0014f9d0;
      }
      if (uVar13 == 0x304) {
        puVar9 = (undefined1 *)0x1a5860;
        goto LAB_0014f9d0;
      }
      if (uVar13 == 0xfeff) {
        puVar9 = (undefined1 *)0x1a5870;
        goto LAB_0014f9d0;
      }
      if (uVar13 == 0xfefd) {
        puVar9 = (undefined1 *)0x1a5880;
        goto LAB_0014f9d0;
      }
      pcVar14 = "UNKNOWN";
      puVar9 = (undefined1 *)0x1a5890;
      if (uVar13 == 0x100) goto LAB_0014f9d0;
    }
    BIO_printf(param_7," Record\nHeader:\n  Version = %s (0x%x)\n",pcVar14);
    if (((byte)param_6->method->get_timeout[0x60] & 8) != 0) {
      BIO_printf(param_7,"  epoch=%d, sequence_number=%04x%04x%04x\n",
                 (ulong)(ushort)(*(ushort *)((long)param_4 + 3) << 8 |
                                *(ushort *)((long)param_4 + 3) >> 8),
                 (ulong)(ushort)(*(ushort *)((long)param_4 + 5) << 8 |
                                *(ushort *)((long)param_4 + 5) >> 8),
                 (ulong)(ushort)(*(ushort *)((long)param_4 + 7) << 8 |
                                *(ushort *)((long)param_4 + 7) >> 8),
                 (ulong)(ushort)(*(ushort *)((long)param_4 + 9) << 8 |
                                *(ushort *)((long)param_4 + 9) >> 8));
    }
    bVar3 = *(byte *)param_4;
    if (bVar3 == 0x14) {
      puVar9 = ssl_content_tbl;
LAB_0014f9bf:
      pcVar14 = *(char **)(puVar9 + 8);
    }
    else {
      if (bVar3 == 0x15) {
        puVar9 = (undefined1 *)0x1a57f0;
        goto LAB_0014f9bf;
      }
      if (bVar3 == 0x16) {
        puVar9 = (undefined1 *)0x1a5800;
        goto LAB_0014f9bf;
      }
      pcVar14 = "UNKNOWN";
      puVar9 = (undefined1 *)0x1a5810;
      if (bVar3 == 0x17) goto LAB_0014f9bf;
    }
    BIO_printf(param_7,"  Content Type = %s (%d)\n  Length = %d",pcVar14,(ulong)bVar3,
               (ulong)CONCAT11(*(byte *)((long)((long)param_4 + -2) + (long)param_5),
                               ((byte *)((long)param_4 + -1))[(long)param_5]));
    goto LAB_0014e32f;
  }
  if (param_6->server == 0) {
    param_1 = (uint)(param_1 == 0);
  }
  local_68 = param_4;
  local_60 = param_5;
  puVar24 = local_50;
  if (param_5 < (uint *)0x4) goto LAB_0014e506;
  bVar3 = *(byte *)((long)param_4 + 1);
  bVar4 = *(byte *)((long)param_4 + 2);
  bVar5 = *(byte *)param_4;
  bVar6 = *(byte *)((long)param_4 + 3);
  local_68 = param_4;
  local_60 = param_5;
  BIO_indent(param_7,4,0x50);
  puVar9 = ssl_handshake_tbl;
  do {
    if ((uint)bVar5 == *(uint *)puVar9) {
      pcVar14 = *(char **)((long)puVar9 + 8);
      goto LAB_0014e708;
    }
    puVar9 = (undefined1 *)((long)puVar9 + 0x10);
  } while (puVar9 != ssl_content_tbl);
  pcVar14 = "UNKNOWN";
LAB_0014e708:
  BIO_printf(param_7,"%s, Length=%d\n",pcVar14);
  puVar20 = local_68 + 1;
  puVar23 = local_60 + -1;
  if (((byte)param_6->method->get_timeout[0x60] & 8) != 0) {
    local_68 = puVar20;
    local_60 = puVar23;
    puVar24 = local_50;
    if (puVar23 < &DAT_00000008) goto LAB_0014e506;
    BIO_indent(param_7,4,0x50);
    BIO_printf(param_7,"message_seq=%d, fragment_offset=%d, fragment_length=%d\n",
               (ulong)(ushort)(*(ushort *)local_68 << 8 | *(ushort *)local_68 >> 8));
    puVar20 = local_68 + 2;
    puVar23 = local_60 + -2;
  }
  local_68 = puVar20;
  local_60 = puVar23;
  puVar24 = local_50;
  if (puVar23 < (uint *)(long)(int)((uint)bVar6 | (uint)bVar3 << 0x10 | (uint)bVar4 << 8))
  goto LAB_0014e506;
  puVar24 = puVar20;
  switch(bVar5) {
  default:
    BIO_indent(param_7,6,0x50);
    BIO_puts(param_7,"Unsupported, hex dump follows:\n");
    BIO_dump_indent(param_7,(char *)local_68,(int)local_60,8);
    goto LAB_0014e32f;
  case 1:
    local_48 = puVar23;
    if ((uint *)0x1 < puVar23) {
      uVar13 = *(ushort *)puVar20;
      local_50 = puVar20;
      BIO_indent(param_7,6,0x50);
      ppuVar22 = &local_48;
      BIO_printf(param_7,"%s=0x%x (%s)\n","client_version",
                 (ulong)(ushort)(uVar13 << 8 | uVar13 >> 8));
      ppuVar21 = &local_50;
      local_50 = (uint *)((long)local_50 + 2);
      local_48 = (uint *)((long)local_48 + -2);
      iVar8 = ssl_print_random_constprop_0(param_7,ppuVar21,ppuVar22);
      puVar24 = local_50;
      if ((iVar8 != 0) && (local_48 != (uint *)0x0)) {
        uVar17 = (ulong)*(byte *)local_50;
        puVar20 = (uint *)(uVar17 + 1);
        if (puVar20 <= local_48) {
          BIO_indent(param_7,6,0x50);
          BIO_printf(param_7,"%s (len=%d): ","session_id",uVar17);
          puVar23 = (uint *)((long)puVar24 + uVar17);
          if (uVar17 != 0) {
            do {
              BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + 1));
              puVar24 = (uint *)((long)puVar24 + 1);
            } while (puVar24 != puVar23);
          }
          BIO_puts(param_7,"\n");
          puVar24 = (uint *)((long)local_50 + (long)puVar20);
          local_48 = (uint *)((long)local_48 - (long)puVar20);
          local_50 = puVar24;
          if (((byte)param_6->method->get_timeout[0x60] & 8) == 0) {
LAB_0014f124:
            puVar24 = local_50;
            if ((uint *)0x1 < local_48) {
              local_48 = (uint *)((long)local_48 + -2);
              uVar13 = *(ushort *)local_50;
              local_50 = (uint *)((long)local_50 + 2);
              BIO_indent(param_7,6,0x50);
              uVar11 = uVar13 >> 8;
              uVar13 = uVar13 << 8 | uVar11;
              puVar20 = (uint *)(ulong)uVar13;
              BIO_printf(param_7,"cipher_suites (len=%d)\n",(ulong)uVar13);
              puVar24 = local_50;
              if ((puVar20 <= local_48) && ((uVar11 & 1) == 0)) {
                for (; puVar20 != (uint *)0x0; puVar20 = (uint *)((long)puVar20 + -2)) {
                  uVar13 = *(ushort *)local_50;
                  BIO_indent(param_7,8,0x50);
                  puVar9 = ssl_ciphers_tbl;
                  do {
                    if ((uint)(ushort)(uVar13 << 8 | uVar13 >> 8) == *(uint *)puVar9) {
                      pcVar14 = *(char **)((long)puVar9 + 8);
                      goto LAB_0014f1e7;
                    }
                    puVar9 = (undefined1 *)((long)puVar9 + 0x10);
                  } while (puVar9 != ssl_handshake_tbl);
                  pcVar14 = "UNKNOWN";
LAB_0014f1e7:
                  BIO_printf(param_7,"{0x%02X, 0x%02X} %s\n",(ulong)*(byte *)local_50,
                             (ulong)*(byte *)((long)local_50 + 1),pcVar14);
                  local_50 = (uint *)((long)local_50 + 2);
                  local_48 = (uint *)((long)local_48 + -2);
                }
                puVar24 = local_50;
                if (local_48 != (uint *)0x0) {
                  local_48 = (uint *)((long)local_48 + -1);
                  puVar20 = (uint *)(ulong)*(byte *)local_50;
                  local_50 = (uint *)((long)local_50 + 1);
                  puVar24 = local_50;
                  if (puVar20 <= local_48) {
                    BIO_indent(param_7,6,0x50);
                    BIO_printf(param_7,"compression_methods (len=%d)\n",puVar20);
                    for (; puVar20 != (uint *)0x0; puVar20 = (uint *)((long)puVar20 + -1)) {
                      BIO_indent(param_7,8,0x50);
                      if (*(byte *)local_50 == 0) {
                        puVar9 = ssl_comp_tbl;
LAB_00150c1b:
                        pcVar14 = *(char **)(puVar9 + 8);
                      }
                      else {
                        pcVar14 = "UNKNOWN";
                        puVar9 = (undefined1 *)0x1a4130;
                        if (*(byte *)local_50 == 1) goto LAB_00150c1b;
                      }
                      BIO_printf(param_7,"%s (0x%02X)\n",pcVar14);
                      local_50 = (uint *)((long)local_50 + 1);
                      local_48 = (uint *)((long)local_48 + -1);
                    }
                    uVar12 = 1;
                    uVar15 = 0;
                    goto LAB_0014f742;
                  }
                }
              }
            }
          }
          else if (local_48 != (uint *)0x0) {
            uVar17 = (ulong)*(byte *)puVar24;
            puVar20 = (uint *)(uVar17 + 1);
            if (puVar20 <= local_48) {
              BIO_indent(param_7,6,0x50);
              uVar19 = 0;
              BIO_printf(param_7,"%s (len=%d): ","cookie",uVar17);
              while (uVar19 < uVar17) {
                uVar19 = uVar19 + 1;
                BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + uVar19));
              }
              BIO_puts(param_7,"\n");
              local_50 = (uint *)((long)local_50 + (long)puVar20);
              local_48 = (uint *)((long)local_48 - (long)puVar20);
              goto LAB_0014f124;
            }
          }
        }
      }
    }
    break;
  case 2:
    local_48 = puVar23;
    if ((uint *)0x1 < puVar23) {
      uVar13 = *(ushort *)puVar20 << 8 | *(ushort *)puVar20 >> 8;
      local_50 = puVar20;
      BIO_indent(param_7,6,0x50);
      if ((((uVar13 == 0x300) || (uVar13 == 0x301)) || (uVar13 == 0x302)) ||
         (((uVar13 == 0x303 || (uVar13 == 0x304)) ||
          ((uVar13 == 0xfeff || ((uVar13 == 0xfefd || (uVar13 == 0x100)))))))) {
        BIO_printf(param_7,"%s=0x%x (%s)\n","server_version");
        local_50 = (uint *)((long)local_50 + 2);
        local_48 = (uint *)((long)local_48 + -2);
        iVar8 = ssl_print_random_constprop_0(param_7,&local_50,&local_48);
        puVar24 = local_50;
        if (iVar8 != 0) {
          if (uVar13 != 0x304) goto LAB_0014ed93;
LAB_0014ee53:
          ppuVar22 = &local_48;
          ppuVar21 = &local_50;
          puVar24 = local_50;
          if ((uint *)0x1 < local_48) {
            uVar11 = *(ushort *)local_50;
            BIO_indent(param_7,6,0x50);
            puVar9 = ssl_ciphers_tbl;
            do {
              if ((uint)(ushort)(uVar11 << 8 | uVar11 >> 8) == *(uint *)puVar9) {
                pcVar14 = *(char **)((long)puVar9 + 8);
                goto LAB_0014eeaa;
              }
              puVar9 = (undefined1 *)((long)puVar9 + 0x10);
            } while (puVar9 != ssl_handshake_tbl);
            pcVar14 = "UNKNOWN";
LAB_0014eeaa:
            BIO_printf(param_7,"cipher_suite {0x%02X, 0x%02X} %s\n",(ulong)*(byte *)local_50,
                       (ulong)*(byte *)((long)local_50 + 1),pcVar14);
            local_50 = (uint *)((long)local_50 + 2);
            local_48 = (uint *)((long)local_48 + -2);
            if (uVar13 != 0x304) {
              puVar24 = local_50;
              if (local_48 == (uint *)0x0) break;
              BIO_indent(param_7,6,0x50);
              if (*(byte *)local_50 == 0) {
                puVar9 = ssl_comp_tbl;
LAB_00150a90:
                pcVar14 = *(char **)(puVar9 + 8);
              }
              else {
                pcVar14 = "UNKNOWN";
                puVar9 = (undefined1 *)0x1a4130;
                if (*(byte *)local_50 == 1) goto LAB_00150a90;
              }
              BIO_printf(param_7,"compression_method: %s (0x%02X)\n",pcVar14);
              local_50 = (uint *)((long)local_50 + 1);
              local_48 = (uint *)((long)local_48 + -1);
            }
            uVar12 = 2;
            goto LAB_0014f73d;
          }
        }
      }
      else {
        BIO_printf(param_7,"%s=0x%x (%s)\n","server_version",(ulong)uVar13);
        local_50 = (uint *)((long)local_50 + 2);
        local_48 = (uint *)((long)local_48 + -2);
        iVar8 = ssl_print_random_constprop_0(param_7,&local_50,&local_48);
        puVar24 = local_50;
        if (iVar8 != 0) {
LAB_0014ed93:
          puVar24 = local_50;
          if (local_48 != (uint *)0x0) {
            uVar17 = (ulong)*(byte *)local_50;
            puVar20 = (uint *)(uVar17 + 1);
            if (puVar20 <= local_48) {
              BIO_indent(param_7,6,0x50);
              BIO_printf(param_7,"%s (len=%d): ","session_id",uVar17);
              puVar23 = (uint *)((long)puVar24 + uVar17);
              if (uVar17 != 0) {
                do {
                  BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + 1));
                  puVar24 = (uint *)((long)puVar24 + 1);
                } while (puVar23 != puVar24);
              }
              BIO_puts(param_7,"\n");
              local_50 = (uint *)((long)local_50 + (long)puVar20);
              local_48 = (uint *)((long)local_48 - (long)puVar20);
              goto LAB_0014ee53;
            }
          }
        }
      }
    }
    break;
  case 3:
    puVar24 = local_50;
    if ((uint *)0x1 < puVar23) {
      uVar13 = *(ushort *)puVar20;
      BIO_indent(param_7,6,0x50);
      uVar13 = uVar13 << 8 | uVar13 >> 8;
      if (uVar13 == 0x300) {
        puVar9 = ssl_version_tbl;
LAB_0014fcb0:
        pcVar14 = *(char **)(puVar9 + 8);
      }
      else {
        if (uVar13 == 0x301) {
          puVar9 = (undefined1 *)0x1a5830;
          goto LAB_0014fcb0;
        }
        if (uVar13 == 0x302) {
          puVar9 = (undefined1 *)0x1a5840;
          goto LAB_0014fcb0;
        }
        if (uVar13 == 0x303) {
          puVar9 = (undefined1 *)0x1a5850;
          goto LAB_0014fcb0;
        }
        if (uVar13 == 0x304) {
          puVar9 = (undefined1 *)0x1a5860;
          goto LAB_0014fcb0;
        }
        if (uVar13 == 0xfeff) {
          puVar9 = (undefined1 *)0x1a5870;
          goto LAB_0014fcb0;
        }
        if (uVar13 == 0xfefd) {
          puVar9 = (undefined1 *)0x1a5880;
          goto LAB_0014fcb0;
        }
        pcVar14 = "UNKNOWN";
        puVar9 = (undefined1 *)0x1a5890;
        if (uVar13 == 0x100) goto LAB_0014fcb0;
      }
      BIO_printf(param_7,"%s=0x%x (%s)\n","server_version",(ulong)uVar13,pcVar14);
      puVar24 = local_50;
      if ((puVar23 != (uint *)0x2) &&
         (uVar17 = (ulong)*(byte *)((long)puVar20 + 2),
         (ushort *)(uVar17 + 1) <= (ushort *)((long)puVar23 + -2))) {
        BIO_indent(param_7,6,0x50);
        BIO_printf(param_7,"%s (len=%d): ","cookie",uVar17);
        puVar24 = (uint *)((long)puVar20 + uVar17);
        if (uVar17 != 0) {
          do {
            pbVar2 = (byte *)((long)puVar20 + 3);
            puVar20 = (uint *)((long)puVar20 + 1);
            BIO_printf(param_7,"%02X",(ulong)*pbVar2);
          } while (puVar24 != puVar20);
        }
        goto LAB_0014e471;
      }
    }
    break;
  case 4:
    if (puVar23 == (uint *)0x0) {
      local_50 = puVar20;
      local_48 = puVar23;
      BIO_indent(param_7,8,0x50);
      BIO_puts(param_7,"No Ticket\n");
      goto LAB_0014e32f;
    }
    local_48 = puVar23;
    if ((uint *)0x3 < puVar23) {
      uVar7 = *puVar20;
      local_48 = puVar23 + -1;
      local_50 = puVar20 + 1;
      BIO_indent(param_7,8,0x50);
      BIO_printf(param_7,"ticket_lifetime_hint=%u\n",
                 (ulong)(uVar7 >> 0x18 | (uVar7 & 0xff0000) >> 8 | (uVar7 & 0xff00) << 8 |
                        uVar7 << 0x18));
      if (((((byte)param_6->method->get_timeout[0x60] & 8) == 0) &&
          (iVar8 = param_6->method->version, 0x303 < iVar8)) && (iVar8 != 0x10000)) {
        puVar24 = local_50;
        if ((uint *)0x3 < local_48) {
          local_48 = local_48 + -1;
          uVar7 = *local_50;
          local_50 = local_50 + 1;
          BIO_indent(param_7,8,0x50);
          BIO_printf(param_7,"ticket_age_add=%u\n",
                     (ulong)(uVar7 >> 0x18 | (uVar7 & 0xff0000) >> 8 | (uVar7 & 0xff00) << 8 |
                            uVar7 << 0x18));
          puVar24 = local_50;
          if (local_48 != (uint *)0x0) {
            uVar17 = (ulong)*(byte *)local_50;
            puVar20 = (uint *)(uVar17 + 1);
            if (puVar20 <= local_48) {
              uVar19 = 0;
              BIO_indent(param_7,8,0x50);
              BIO_printf(param_7,"%s (len=%d): ","ticket_nonce",uVar17);
              while (uVar19 < uVar17) {
                uVar19 = uVar19 + 1;
                BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + uVar19));
              }
              BIO_puts(param_7,"\n");
              local_50 = (uint *)((long)local_50 + (long)puVar20);
              local_48 = (uint *)((long)local_48 - (long)puVar20);
              goto LAB_0014eba9;
            }
          }
        }
      }
      else {
LAB_0014eba9:
        puVar24 = local_50;
        if ((uint *)0x1 < local_48) {
          uVar13 = *(ushort *)local_50 << 8 | *(ushort *)local_50 >> 8;
          puVar20 = (uint *)((ulong)uVar13 + 2);
          if (puVar20 <= local_48) {
            puVar23 = (uint *)((ulong)uVar13 + (long)local_50);
            BIO_indent(param_7,8,0x50);
            BIO_printf(param_7,"%s (len=%d): ","ticket",(ulong)uVar13);
            if (uVar13 != 0) {
              do {
                puVar1 = (ushort *)((long)puVar24 + 2);
                puVar24 = (uint *)((long)puVar24 + 1);
                BIO_printf(param_7,"%02X",(ulong)*(byte *)puVar1);
              } while (puVar23 != puVar24);
            }
            BIO_puts(param_7,"\n");
            local_50 = (uint *)((long)local_50 + (long)puVar20);
            local_48 = (uint *)((long)local_48 - (long)puVar20);
            puVar23 = local_48;
            if (((((byte)param_6->method->get_timeout[0x60] & 8) != 0) ||
                (iVar8 = param_6->method->version, iVar8 == 0x10000)) ||
               ((iVar8 < 0x304 ||
                (iVar8 = ssl_print_extensions(param_7,8,0,4,&local_50,&local_48), puVar24 = local_50
                , puVar23 = local_48, iVar8 != 0)))) goto joined_r0x0014faec;
          }
        }
      }
    }
    break;
  case 8:
    ppuVar22 = &local_60;
    ppuVar21 = &local_68;
    uVar12 = 8;
LAB_0014f73d:
    uVar15 = 1;
LAB_0014f742:
    iVar8 = ssl_print_extensions(param_7,6,uVar15,uVar12,ppuVar21,ppuVar22);
    goto joined_r0x0014f484;
  case 0xb:
    local_58 = puVar20;
    if (((((byte)param_6->method->get_timeout[0x60] & 8) == 0) &&
        (iVar8 = param_6->method->version, 0x303 < iVar8)) && (iVar8 != 0x10000)) {
      puVar24 = local_50;
      if (puVar23 != (uint *)0x0) {
        bVar3 = *(byte *)puVar20;
        uVar17 = (ulong)bVar3;
        puVar25 = (uint *)(uVar17 + 1);
        if (puVar25 <= puVar23) {
          BIO_indent(param_7,6,0x50);
          BIO_printf(param_7,"%s (len=%d): ","context",bVar3);
          uVar19 = 0;
          if (uVar17 != 0) {
            do {
              lVar18 = uVar19 + 1;
              uVar19 = uVar19 + 1;
              BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar20 + lVar18));
            } while (uVar19 < uVar17);
          }
          BIO_puts(param_7,"\n");
          local_58 = (uint *)((long)local_58 + (long)puVar25);
          puVar23 = (uint *)((long)puVar23 - (long)puVar25);
          goto LAB_0014f529;
        }
      }
    }
    else {
LAB_0014f529:
      puVar24 = local_50;
      if (((uint *)0x2 < puVar23) &&
         (local_50 = (uint *)(long)(int)((uint)*(byte *)local_58 << 0x10 |
                                         (uint)*(byte *)((long)local_58 + 1) << 8 |
                                        (uint)*(byte *)((long)local_58 + 2)), puVar24 = local_50,
         (uint *)((long)local_50 + 3) == puVar23)) {
        local_58 = (uint *)((long)local_58 + 3);
        BIO_indent(param_7,6,0x50);
        BIO_printf(param_7,"certificate_list, length=%d\n",(ulong)local_50 & 0xffffffff);
        if (local_50 != (uint *)0x0) {
          do {
            puVar20 = local_58;
            puVar24 = local_50;
            if (local_50 < (uint *)0x3) goto LAB_0014e506;
            uVar7 = (uint)*(byte *)local_58 << 0x10 | (uint)*(byte *)((long)local_58 + 1) << 8 |
                    (uint)*(byte *)((long)local_58 + 2);
            lVar18 = (long)(int)uVar7;
            puVar23 = (uint *)(lVar18 + 3);
            if (local_50 < puVar23) goto LAB_0014e506;
            local_48 = (uint *)((long)local_58 + 3);
            BIO_indent(param_7,8,0x50);
            BIO_printf(param_7,"ASN.1Cert, length=%d",(ulong)uVar7);
            x = d2i_X509((X509 **)0x0,(uchar **)&local_48,lVar18);
            if (x == (X509 *)0x0) {
              BIO_puts(param_7,"<UNPARSEABLE CERTIFICATE>\n");
            }
            else {
              BIO_puts(param_7,"\n------details-----\n");
              X509_print_ex(param_7,x,0x82031f,0);
              PEM_write_bio_X509(param_7,x);
              BIO_puts(param_7,"------------------\n");
              X509_free(x);
            }
            if (local_48 != (uint *)((long)puVar20 + (long)puVar23)) {
              BIO_puts(param_7,"<TRAILING GARBAGE AFTER CERTIFICATE>\n");
            }
            local_58 = (uint *)((long)local_58 + (long)puVar23);
            local_50 = (uint *)((long)local_50 + (-3 - lVar18));
            if ((((((byte)param_6->method->get_timeout[0x60] & 8) == 0) &&
                 (iVar8 = param_6->method->version, 0x303 < iVar8)) && (iVar8 != 0x10000)) &&
               (iVar8 = ssl_print_extensions(param_7,8,param_1,0xb,&local_58,&local_50),
               puVar24 = local_50, iVar8 == 0)) goto LAB_0014e506;
          } while (local_50 != (uint *)0x0);
        }
        goto LAB_0014e32f;
      }
    }
    break;
  case 0xc:
    uVar7 = *(uint *)(param_6[1].handshake_func + 0x1c);
    local_80 = uVar7 & 1;
    if ((uVar7 & 1) == 0) {
      uVar16 = uVar7 & 2;
      if (uVar16 == 0) {
        if ((uVar7 & 4) == 0) {
          if ((uVar7 & 8) == 0) {
            if ((uVar7 & 0x40) == 0) {
              if ((uVar7 & 0x100) == 0) {
                if ((uVar7 & 0x80) == 0) {
                  pcVar14 = "SRP";
                  if ((((uVar7 & 0x20) == 0) && (pcVar14 = "GOST", (uVar7 & 0x10) == 0)) &&
                     (pcVar14 = "GOST18", (uVar7 & 0x200) == 0)) {
                    pcVar14 = "UNKNOWN";
                  }
                  local_50 = puVar20;
                  local_48 = puVar23;
                  BIO_indent(param_7,6,0x50);
                  BIO_printf(param_7,"KeyExchangeAlgorithm=%s\n",pcVar14);
                  goto LAB_0014f804;
                }
                pcVar14 = "ECDHEPSK";
                uVar16 = 0x80;
              }
              else {
                pcVar14 = "DHEPSK";
                uVar16 = 0x100;
              }
            }
            else {
              pcVar14 = "RSAPSK";
              uVar16 = 0x40;
            }
          }
          else {
            pcVar14 = "PSK";
            uVar16 = 8;
          }
          local_50 = puVar20;
          local_48 = puVar23;
          BIO_indent(param_7,6,0x50);
          BIO_printf(param_7,"KeyExchangeAlgorithm=%s\n",pcVar14);
          puVar24 = local_50;
          if ((uint *)0x1 < local_48) {
            uVar13 = *(ushort *)local_50 << 8 | *(ushort *)local_50 >> 8;
            puVar20 = (uint *)((ulong)uVar13 + 2);
            if (puVar20 <= local_48) {
              BIO_indent(param_7,8,0x50);
              BIO_printf(param_7,"%s (len=%d): ","psk_identity_hint",(ulong)uVar13);
              puVar23 = (uint *)((ulong)uVar13 + (long)puVar24);
              if (uVar13 != 0) {
                do {
                  BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + 2));
                  puVar24 = (uint *)((long)puVar24 + 1);
                } while (puVar23 != puVar24);
              }
              BIO_puts(param_7,"\n");
              local_50 = (uint *)((long)local_50 + (long)puVar20);
              local_48 = (uint *)((long)local_48 - (long)puVar20);
              if (uVar16 == 0x100) {
                local_80 = 0x100;
                goto LAB_00150074;
              }
              puVar23 = local_48;
              if (uVar16 == 0x80) goto LAB_001502ba;
              goto joined_r0x0014faec;
            }
          }
        }
        else {
          local_50 = puVar20;
          local_48 = puVar23;
          BIO_indent(param_7,6,0x50);
          BIO_printf(param_7,"KeyExchangeAlgorithm=%s\n",&DAT_00183508);
LAB_001502ba:
          puVar24 = local_50;
          if (local_48 != (uint *)0x0) {
            BIO_indent(param_7,8,0x50);
            bVar3 = *(byte *)local_50;
            local_80 = uVar16;
            if (bVar3 == 1) {
              BIO_puts(param_7,"explicit_prime\n");
            }
            else {
              if (bVar3 != 2) {
                if (bVar3 == 3) {
                  puVar24 = local_50;
                  if ((uint *)0x2 < local_48) {
                    puVar9 = ssl_groups_tbl;
                    do {
                      if ((uint)(ushort)(*(ushort *)((long)local_50 + 1) << 8 |
                                        *(ushort *)((long)local_50 + 1) >> 8) == *(uint *)puVar9) {
                        pcVar14 = *(char **)((long)puVar9 + 8);
                        goto LAB_00150ca6;
                      }
                      puVar9 = (undefined1 *)((long)puVar9 + 0x10);
                    } while (puVar9 != ssl_exts_tbl);
                    pcVar14 = "UNKNOWN";
LAB_00150ca6:
                    BIO_printf(param_7,"named_curve: %s (%d)\n",pcVar14);
                    puVar20 = local_50;
                    puVar24 = (uint *)((long)local_50 + 3);
                    local_48 = (uint *)((long)local_48 + -3);
                    if (local_48 != (uint *)0x0) {
                      uVar17 = (ulong)*(byte *)((long)local_50 + 3);
                      puVar23 = (uint *)(uVar17 + 1);
                      if (puVar23 <= local_48) {
                        local_50 = puVar24;
                        BIO_indent(param_7,8,0x50);
                        BIO_printf(param_7,"%s (len=%d): ","point",uVar17);
                        for (uVar19 = 0; uVar19 < uVar17; uVar19 = uVar19 + 1) {
                          BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar20 + uVar19 + 4));
                        }
                        BIO_puts(param_7,"\n");
                        local_50 = (uint *)((long)local_50 + (long)puVar23);
                        local_48 = (uint *)((long)local_48 - (long)puVar23);
                        goto LAB_00150271;
                      }
                    }
                  }
                }
                else {
                  BIO_printf(param_7,"UNKNOWN CURVE PARAMETER TYPE %d\n",(ulong)bVar3);
                  puVar24 = local_50;
                }
                break;
              }
              BIO_puts(param_7,"explicit_char2\n");
            }
LAB_00150271:
            puVar23 = local_48;
            if (local_80 == 0) {
LAB_0014f804:
              ssl_print_signature_isra_0_constprop_0(param_7,&param_6->method,&local_50,&local_48);
              puVar23 = local_48;
            }
            goto joined_r0x0014faec;
          }
        }
      }
      else {
        local_50 = puVar20;
        local_48 = puVar23;
        BIO_indent(param_7,6,0x50);
        BIO_printf(param_7,"KeyExchangeAlgorithm=%s\n",&DAT_0018350a);
LAB_00150074:
        puVar24 = local_50;
        if ((uint *)0x1 < local_48) {
          uVar13 = *(ushort *)local_50 << 8 | *(ushort *)local_50 >> 8;
          puVar20 = (uint *)((ulong)uVar13 + 2);
          if (puVar20 <= local_48) {
            puVar23 = (uint *)((ulong)uVar13 + (long)local_50);
            BIO_indent(param_7,8,0x50);
            BIO_printf(param_7,"%s (len=%d): ",&DAT_00188a4b,(ulong)uVar13);
            if (uVar13 != 0) {
              do {
                BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + 2));
                puVar24 = (uint *)((long)puVar24 + 1);
              } while (puVar23 != puVar24);
            }
            BIO_puts(param_7,"\n");
            puVar24 = (uint *)((long)local_50 + (long)puVar20);
            local_48 = (uint *)((long)local_48 - (long)puVar20);
            if ((uint *)0x1 < local_48) {
              uVar13 = *(ushort *)puVar24 << 8 | *(ushort *)puVar24 >> 8;
              puVar20 = (uint *)((ulong)uVar13 + 2);
              if (puVar20 <= local_48) {
                local_50 = puVar24;
                BIO_indent(param_7,8,0x50);
                BIO_printf(param_7,"%s (len=%d): ",&DAT_00188a50,(ulong)uVar13);
                for (uVar17 = 0; uVar17 < uVar13; uVar17 = uVar17 + 1) {
                  BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + uVar17 + 2));
                }
                BIO_puts(param_7,"\n");
                puVar24 = (uint *)((long)local_50 + (long)puVar20);
                local_48 = (uint *)((long)local_48 - (long)puVar20);
                if ((uint *)0x1 < local_48) {
                  uVar13 = *(ushort *)puVar24 << 8 | *(ushort *)puVar24 >> 8;
                  puVar20 = (uint *)((ulong)uVar13 + 2);
                  if (puVar20 <= local_48) {
                    local_50 = puVar24;
                    BIO_indent(param_7,8,0x50);
                    BIO_printf(param_7,"%s (len=%d): ","dh_Ys",(ulong)uVar13);
                    for (uVar17 = 0; uVar17 < uVar13; uVar17 = uVar17 + 1) {
                      BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + uVar17 + 2));
                    }
                    BIO_puts(param_7,"\n");
                    local_50 = (uint *)((long)local_50 + (long)puVar20);
                    local_48 = (uint *)((long)local_48 - (long)puVar20);
                    goto LAB_00150271;
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
      local_50 = puVar20;
      local_48 = puVar23;
      BIO_indent(param_7,6,0x50);
      BIO_printf(param_7,"KeyExchangeAlgorithm=%s\n",&DAT_00188981);
      puVar24 = local_50;
      if ((uint *)0x1 < local_48) {
        uVar13 = *(ushort *)local_50 << 8 | *(ushort *)local_50 >> 8;
        puVar20 = (uint *)((ulong)uVar13 + 2);
        if (puVar20 <= local_48) {
          puVar23 = (uint *)((ulong)uVar13 + (long)local_50);
          BIO_indent(param_7,8,0x50);
          BIO_printf(param_7,"%s (len=%d): ","rsa_modulus",(ulong)uVar13);
          if (uVar13 != 0) {
            do {
              puVar1 = (ushort *)((long)puVar24 + 2);
              puVar24 = (uint *)((long)puVar24 + 1);
              BIO_printf(param_7,"%02X",(ulong)*(byte *)puVar1);
            } while (puVar23 != puVar24);
          }
          BIO_puts(param_7,"\n");
          puVar24 = (uint *)((long)local_50 + (long)puVar20);
          local_48 = (uint *)((long)local_48 - (long)puVar20);
          if ((uint *)0x1 < local_48) {
            uVar13 = *(ushort *)puVar24 << 8 | *(ushort *)puVar24 >> 8;
            puVar20 = (uint *)((ulong)uVar13 + 2);
            if (puVar20 <= local_48) {
              local_50 = puVar24;
              BIO_indent(param_7,8,0x50);
              BIO_printf(param_7,"%s (len=%d): ","rsa_exponent",(ulong)uVar13);
              for (uVar17 = 0; uVar17 < uVar13; uVar17 = uVar17 + 1) {
                BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + uVar17 + 2));
              }
              BIO_puts(param_7,"\n");
              local_50 = (uint *)((long)local_50 + (long)puVar20);
              local_48 = (uint *)((long)local_48 - (long)puVar20);
              goto LAB_0014f804;
            }
          }
        }
      }
    }
    break;
  case 0xd:
    local_50 = puVar23;
    puVar24 = puVar23;
    if (((((byte)param_6->method->get_timeout[0x60] & 8) == 0) &&
        (iVar8 = param_6->method->version, 0x303 < iVar8)) && (iVar8 != 0x10000)) {
      local_58 = puVar20;
      if (puVar23 != (uint *)0x0) {
        uVar17 = (ulong)*(byte *)puVar20;
        puVar25 = (uint *)(uVar17 + 1);
        if (puVar25 <= puVar23) {
          uVar19 = 0;
          BIO_indent(param_7,6,0x50);
          BIO_printf(param_7,"%s (len=%d): ","request_context",uVar17);
          if (uVar17 != 0) {
            do {
              uVar19 = uVar19 + 1;
              BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar20 + uVar19));
            } while (uVar19 < uVar17);
          }
          BIO_puts(param_7,"\n");
          local_58 = (uint *)((long)local_58 + (long)puVar25);
          ppuVar22 = &local_50;
          ppuVar21 = &local_58;
          local_50 = (uint *)((long)local_50 - (long)puVar25);
          uVar12 = 0xd;
          goto LAB_0014f73d;
        }
      }
    }
    else {
      local_58 = puVar20;
      if ((puVar23 != (uint *)0x0) &&
         (uVar17 = (ulong)*(byte *)puVar20, (uint *)(uVar17 + 1) <= puVar23)) {
        local_58 = (uint *)((long)puVar20 + 1);
        BIO_indent(param_7,6,0x50);
        BIO_printf(param_7,"certificate_types (len=%d)\n",uVar17);
        puVar24 = (uint *)((long)local_58 + uVar17);
        puVar20 = local_58;
        if (uVar17 != 0) {
          do {
            bVar3 = *(byte *)puVar20;
            BIO_indent(param_7,8,0x50);
            if (bVar3 == 1) {
              puVar9 = ssl_ctype_tbl;
LAB_0014fad3:
              pcVar14 = *(char **)(puVar9 + 8);
            }
            else {
              if (bVar3 == 2) {
                puVar9 = (undefined1 *)0x1a39b0;
                goto LAB_0014fad3;
              }
              if (bVar3 == 3) {
                puVar9 = (undefined1 *)0x1a39c0;
                goto LAB_0014fad3;
              }
              if (bVar3 == 4) {
                puVar9 = (undefined1 *)0x1a39d0;
                goto LAB_0014fad3;
              }
              if (bVar3 == 5) {
                puVar9 = (undefined1 *)0x1a39e0;
                goto LAB_0014fad3;
              }
              if (bVar3 == 6) {
                puVar9 = (undefined1 *)0x1a39f0;
                goto LAB_0014fad3;
              }
              if (bVar3 == 0x14) {
                puVar9 = (undefined1 *)0x1a3a00;
                goto LAB_0014fad3;
              }
              if (bVar3 == 0x40) {
                puVar9 = (undefined1 *)0x1a3a10;
                goto LAB_0014fad3;
              }
              if (bVar3 == 0x41) {
                puVar9 = (undefined1 *)0x1a3a20;
                goto LAB_0014fad3;
              }
              if (bVar3 == 0x42) {
                puVar9 = (undefined1 *)0x1a3a30;
                goto LAB_0014fad3;
              }
              if (bVar3 == 0x43) {
                puVar9 = (undefined1 *)0x1a3a40;
                goto LAB_0014fad3;
              }
              pcVar14 = "UNKNOWN";
              puVar9 = (undefined1 *)0x1a3a50;
              if (bVar3 == 0x44) goto LAB_0014fad3;
            }
            BIO_printf(param_7,"%s (%d)\n",pcVar14,(ulong)bVar3);
            puVar20 = (uint *)((long)puVar20 + 1);
          } while (puVar24 != puVar20);
        }
        local_58 = (uint *)((long)local_58 + uVar17);
        local_50 = (uint *)(~uVar17 + (long)local_50);
        if (((byte)param_6->method->get_timeout[0x60] & 2) != 0) {
          puVar24 = local_50;
          if ((uint *)0x1 < local_50) {
            uVar13 = *(ushort *)local_58 >> 8;
            uVar11 = *(ushort *)local_58 << 8 | uVar13;
            uVar17 = (ulong)uVar11;
            if (((uint *)(uVar17 + 2) <= local_50) && ((uVar13 & 1) == 0)) {
              local_58 = (uint *)((long)local_58 + 2);
              local_50 = (uint *)((long)local_50 + (-2 - uVar17));
              BIO_indent(param_7,6,0x50);
              BIO_printf(param_7,"signature_algorithms (len=%d)\n",(ulong)uVar11);
              for (; uVar17 != 0; uVar17 = uVar17 - 2) {
                BIO_indent(param_7,8,0x50);
                puVar9 = ssl_sigalg_tbl;
                do {
                  if ((uint)(ushort)(*(ushort *)local_58 << 8 | *(ushort *)local_58 >> 8) ==
                      *(uint *)puVar9) {
                    pcVar14 = *(char **)((long)puVar9 + 8);
                    goto LAB_0014f44f;
                  }
                  puVar9 = (undefined1 *)((long)puVar9 + 0x10);
                } while (puVar9 != &DAT_001a3c50);
                pcVar14 = "UNKNOWN";
LAB_0014f44f:
                BIO_printf(param_7,"%s (0x%04x)\n",pcVar14);
                local_58 = (uint *)((long)local_58 + 2);
              }
              goto LAB_00150343;
            }
          }
          break;
        }
LAB_00150343:
        puVar24 = local_50;
        if (local_50 < (uint *)0x2) break;
        uVar13 = *(ushort *)local_58;
        BIO_indent(param_7,6,0x50);
        uVar13 = uVar13 << 8 | uVar13 >> 8;
        uVar17 = (ulong)uVar13;
        puVar24 = local_50;
        if (local_50 < (uint *)(uVar17 + 2)) break;
        local_58 = (uint *)((long)local_58 + 2);
        local_50 = (uint *)((long)local_50 + (-2 - uVar17));
        BIO_printf(param_7,"certificate_authorities (len=%d)\n",(ulong)uVar13);
        for (; puVar20 = local_58, puVar24 = local_50, uVar17 != 0; uVar17 = (uVar17 - uVar19) - 2)
        {
          if (uVar17 < 2) goto LAB_0014e506;
          uVar13 = *(ushort *)local_58 << 8 | *(ushort *)local_58 >> 8;
          uVar19 = (ulong)uVar13;
          if (uVar17 < uVar19 + 2) goto LAB_0014e506;
          local_58 = (uint *)((long)local_58 + 2);
          BIO_indent(param_7,8,0x50);
          BIO_printf(param_7,"DistinguishedName (len=%d): ",(ulong)uVar13);
          local_48 = local_58;
          nm = d2i_X509_NAME((X509_NAME **)0x0,(uchar **)&local_48,uVar19);
          if (nm == (X509_NAME *)0x0) {
            BIO_puts(param_7,"<UNPARSEABLE DN>\n");
          }
          else {
            X509_NAME_print_ex(param_7,nm,0,0x82031f);
            BIO_puts(param_7,"\n");
            X509_NAME_free(nm);
          }
          local_58 = (uint *)((long)local_58 + uVar19);
        }
        puVar23 = local_50;
        if (((((byte)param_6->method->get_timeout[0x60] & 8) == 0) &&
            (iVar8 = param_6->method->version, iVar8 != 0x10000)) && (0x303 < iVar8)) {
          if ((uint *)0x1 < local_50) {
            uVar13 = *(ushort *)local_58 << 8 | *(ushort *)local_58 >> 8;
            puVar23 = (uint *)((ulong)uVar13 + 2);
            if (puVar23 <= local_50) {
              BIO_indent(param_7,6,0x50);
              BIO_printf(param_7,"%s (len=%d): ","request_extensions",(ulong)uVar13);
              for (; uVar17 < uVar13; uVar17 = uVar17 + 1) {
                BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar20 + uVar17 + 2));
              }
              BIO_puts(param_7,"\n");
              local_50 = (uint *)((long)local_50 - (long)puVar23);
              puVar23 = local_50;
              goto joined_r0x0014faec;
            }
          }
          break;
        }
        goto joined_r0x0014faec;
      }
    }
    break;
  case 0xe:
    if (puVar23 == (uint *)0x0) goto LAB_0014e32f;
    BIO_indent(param_7,6,0x50);
    BIO_printf(param_7,"%s (len=%d): ","unexpected value",(ulong)puVar23 & 0xffffffff);
    puVar23 = (uint *)((long)puVar20 + (long)puVar23);
    do {
      bVar3 = *(byte *)puVar20;
      puVar20 = (uint *)((long)puVar20 + 1);
      BIO_printf(param_7,"%02X",(ulong)bVar3);
    } while (puVar20 != puVar23);
    goto LAB_0014e471;
  case 0xf:
    iVar8 = ssl_print_signature_isra_0_constprop_0(param_7,&param_6->method,&local_68,&local_60);
joined_r0x0014f484:
    puVar24 = local_50;
    if (iVar8 != 0) goto LAB_0014e32f;
    break;
  case 0x10:
    uVar7 = *(uint *)(param_6[1].handshake_func + 0x1c);
    if ((uVar7 & 1) == 0) {
      if ((uVar7 & 2) != 0) {
        pcVar14 = "DHE";
        uVar7 = 2;
        goto LAB_0014e974;
      }
      if ((uVar7 & 4) != 0) {
        pcVar14 = "ECDHE";
        uVar7 = 4;
        goto LAB_0014e974;
      }
      if ((uVar7 & 8) == 0) {
        if ((uVar7 & 0x40) == 0) {
          if ((uVar7 & 0x100) == 0) {
            if ((uVar7 & 0x80) == 0) {
              if ((uVar7 & 0x20) == 0) {
                if ((uVar7 & 0x10) == 0) {
                  uVar7 = uVar7 & 0x200;
                  pcVar14 = "GOST18";
                  if (uVar7 == 0) {
                    pcVar14 = "UNKNOWN";
                  }
                }
                else {
                  pcVar14 = "GOST";
                  uVar7 = 0x10;
                }
              }
              else {
                pcVar14 = "SRP";
                uVar7 = 0x20;
              }
              goto LAB_0014e974;
            }
            pcVar14 = "ECDHEPSK";
            uVar7 = 0x80;
          }
          else {
            pcVar14 = "DHEPSK";
            uVar7 = 0x100;
          }
        }
        else {
          pcVar14 = "RSAPSK";
          uVar7 = 0x40;
        }
      }
      else {
        pcVar14 = "PSK";
        uVar7 = 8;
      }
      BIO_indent(param_7,6,0x50);
      BIO_printf(param_7,"KeyExchangeAlgorithm=%s\n",pcVar14);
      puVar24 = local_50;
      if (puVar23 < (uint *)0x2) break;
      uVar13 = *(ushort *)puVar20 << 8 | *(ushort *)puVar20 >> 8;
      puVar25 = (uint *)((ulong)uVar13 + 2);
      if (puVar23 < puVar25) break;
      BIO_indent(param_7,8,0x50);
      BIO_printf(param_7,"%s (len=%d): ","psk_identity",(ulong)uVar13);
      puVar24 = puVar20;
      if (uVar13 != 0) {
        do {
          BIO_printf(param_7,"%02X",(ulong)*(byte *)((long)puVar24 + 2));
          puVar24 = (uint *)((long)puVar24 + 1);
        } while ((uint *)((ulong)uVar13 + (long)puVar20) != puVar24);
      }
      puVar20 = (uint *)((long)puVar20 + (long)puVar25);
      BIO_puts(param_7,"\n");
      puVar23 = (uint *)((long)puVar23 - (long)puVar25);
      if (uVar7 != 0x40) {
        if (uVar7 < 0x41) goto LAB_0014fb0a;
        if (uVar7 != 0x100) goto LAB_0014e9b8;
        goto LAB_0014fb9a;
      }
LAB_0014ff00:
      iVar8 = SSL_version(param_6);
      if ((iVar8 >> 8 == 3) && (iVar8 = SSL_version(param_6), iVar8 == 0x300)) {
        BIO_indent(param_7,8,0x50);
        BIO_printf(param_7,"%s (len=%d): ","EncryptedPreMasterSecret",(ulong)puVar23 & 0xffffffff);
        puVar24 = (uint *)((long)puVar20 + (long)puVar23);
        for (; puVar20 != puVar24; puVar20 = (uint *)((long)puVar20 + 1)) {
          BIO_printf(param_7,"%02X",(ulong)*(byte *)puVar20);
        }
        BIO_puts(param_7,"\n");
joined_r0x0014faec:
        puVar24 = local_50;
        if (puVar23 == (uint *)0x0) goto LAB_0014e32f;
      }
      else {
        puVar24 = local_50;
        if ((uint *)0x1 < puVar23) {
          uVar13 = *(ushort *)puVar20 << 8 | *(ushort *)puVar20 >> 8;
          puVar25 = (uint *)((ulong)uVar13 + 2);
          if (puVar25 <= puVar23) {
            puVar24 = (uint *)((ulong)uVar13 + (long)puVar20);
            BIO_indent(param_7,8,0x50);
            BIO_printf(param_7,"%s (len=%d): ","EncryptedPreMasterSecret",(ulong)uVar13);
            if (uVar13 != 0) {
              do {
                puVar1 = (ushort *)((long)puVar20 + 2);
                puVar20 = (uint *)((long)puVar20 + 1);
                BIO_printf(param_7,"%02X",(ulong)*(byte *)puVar1);
              } while (puVar20 != puVar24);
            }
            goto LAB_0014fc27;
          }
        }
      }
    }
    else {
      pcVar14 = "rsa";
      uVar7 = 1;
LAB_0014e974:
      BIO_indent(param_7,6,0x50);
      BIO_printf(param_7,"KeyExchangeAlgorithm=%s\n",pcVar14);
      if (uVar7 < 0x41) {
        if (uVar7 != 4) {
LAB_0014fb0a:
          if (uVar7 < 5) {
            if (uVar7 == 1) goto LAB_0014ff00;
            if (uVar7 == 2) {
LAB_0014fb9a:
              puVar24 = local_50;
              if ((uint *)0x1 < puVar23) {
                uVar13 = *(ushort *)puVar20 << 8 | *(ushort *)puVar20 >> 8;
                puVar25 = (uint *)((ulong)uVar13 + 2);
                if (puVar25 <= puVar23) {
                  puVar24 = (uint *)((ulong)uVar13 + (long)puVar20);
                  BIO_indent(param_7,8,0x50);
                  BIO_printf(param_7,"%s (len=%d): ","dh_Yc",(ulong)uVar13);
                  if (uVar13 != 0) {
                    do {
                      puVar1 = (ushort *)((long)puVar20 + 2);
                      puVar20 = (uint *)((long)puVar20 + 1);
                      BIO_printf(param_7,"%02X",(ulong)*(byte *)puVar1);
                    } while (puVar20 != puVar24);
                  }
                  goto LAB_0014fc27;
                }
              }
              break;
            }
          }
          else if (uVar7 == 0x10) {
            BIO_indent(param_7,8,0x50);
            BIO_printf(param_7,"%s (len=%d): ","GostKeyTransportBlob",(ulong)puVar23 & 0xffffffff);
            puVar24 = (uint *)((long)puVar20 + (long)puVar23);
            if (puVar23 != (uint *)0x0) {
              do {
                bVar3 = *(byte *)puVar20;
                puVar20 = (uint *)((long)puVar20 + 1);
                BIO_printf(param_7,"%02X",(ulong)bVar3);
              } while (puVar24 != puVar20);
            }
            goto LAB_0014e471;
          }
          goto joined_r0x0014faec;
        }
      }
      else {
LAB_0014e9b8:
        if (uVar7 == 0x200) {
          BIO_indent(param_7,8,0x50);
          BIO_printf(param_7,"%s (len=%d): ","GOST-wrapped PreMasterSecret",
                     (ulong)puVar23 & 0xffffffff);
          if (puVar23 != (uint *)0x0) {
            puVar23 = (uint *)((long)puVar20 + (long)puVar23);
            do {
              bVar3 = *(byte *)puVar20;
              puVar20 = (uint *)((long)puVar20 + 1);
              BIO_printf(param_7,"%02X",(ulong)bVar3);
            } while (puVar23 != puVar20);
          }
          goto LAB_0014e471;
        }
        if (uVar7 != 0x80) goto joined_r0x0014faec;
      }
      puVar24 = local_50;
      if (puVar23 != (uint *)0x0) {
        uVar17 = (ulong)*(byte *)puVar20;
        puVar25 = (uint *)(uVar17 + 1);
        if (puVar25 <= puVar23) {
          BIO_indent(param_7,8,0x50);
          BIO_printf(param_7,"%s (len=%d): ",&DAT_00188aaa,uVar17);
          puVar24 = (uint *)((long)puVar20 + uVar17);
          if (uVar17 != 0) {
            do {
              pbVar2 = (byte *)((long)puVar20 + 1);
              puVar20 = (uint *)((long)puVar20 + 1);
              BIO_printf(param_7,"%02X",(ulong)*pbVar2);
            } while (puVar20 != puVar24);
          }
LAB_0014fc27:
          BIO_puts(param_7,"\n");
          puVar23 = (uint *)((long)puVar23 - (long)puVar25);
          goto joined_r0x0014faec;
        }
      }
    }
    break;
  case 0x14:
    BIO_indent(param_7,6,0x50);
    BIO_printf(param_7,"%s (len=%d): ","verify_data",(ulong)puVar23 & 0xffffffff);
    if (puVar23 != (uint *)0x0) {
      puVar23 = (uint *)((long)puVar20 + (long)puVar23);
      do {
        bVar3 = *(byte *)puVar20;
        puVar20 = (uint *)((long)puVar20 + 1);
        BIO_printf(param_7,"%02X",(ulong)bVar3);
      } while (puVar23 != puVar20);
    }
LAB_0014e471:
    BIO_puts(param_7,"\n");
    goto LAB_0014e32f;
  case 0x18:
    if (puVar23 == (uint *)0x1) {
      bVar3 = *(byte *)puVar20;
      BIO_indent(param_7,6,0x50);
      puVar9 = ssl_key_update_tbl;
      if (bVar3 == 0) {
LAB_0014fd74:
        pcVar14 = *(char **)(puVar9 + 8);
      }
      else {
        if (bVar3 == 1) {
          puVar9 = (undefined1 *)0x1a3990;
          goto LAB_0014fd74;
        }
        pcVar14 = "UNKNOWN";
      }
      BIO_printf(param_7,"%s (%d)\n",pcVar14,(ulong)(uint)bVar3);
      goto LAB_0014e32f;
    }
    BIO_indent(param_7,6,0x50);
    BIO_printf(param_7,"%s (len=%d): ","unexpected value",(ulong)puVar23 & 0xffffffff);
    puVar24 = (uint *)((long)puVar20 + (long)puVar23);
    if (puVar23 != (uint *)0x0) {
      do {
        bVar3 = *(byte *)puVar20;
        puVar20 = (uint *)((long)puVar20 + 1);
        BIO_printf(param_7,"%02X",(ulong)bVar3);
      } while (puVar24 != puVar20);
    }
    BIO_puts(param_7,"\n");
    puVar24 = local_50;
  }
LAB_0014e506:
  local_50 = puVar24;
  BIO_printf(param_7,"Message length parse error!\n");
LAB_0014e32f:
  BIO_puts(param_7,"\n");
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}