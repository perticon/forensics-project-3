void ssl_set_masks(SSL *param_1)

{
  uint uVar1;
  bool bVar2;
  char cVar3;
  int iVar4;
  dtls1_state_st *pdVar5;
  uint uVar6;
  uint uVar7;
  uint uVar8;
  bool bVar9;
  
  pdVar5 = param_1[3].d1;
  if (pdVar5 == (dtls1_state_st *)0x0) {
    return;
  }
  bVar9 = true;
  if ((*(long *)(pdVar5->cookie + 4) == 0) && (*(long *)(pdVar5->cookie + 0xc) == 0)) {
    bVar9 = *(int *)(pdVar5->cookie + 0x14) != 0;
  }
  uVar8 = *(uint *)(param_1[1].sid_ctx + 4);
  uVar1 = *(uint *)(param_1[1].sid_ctx + 8);
  if (*(long *)(pdVar5->rcvd_cookie + 0xc) == 0) {
    uVar7 = 0;
    uVar6 = 0;
  }
  else {
    uVar7 = ~-(uint)(*(long *)(pdVar5->rcvd_cookie + 0x14) == 0) & 0x80;
    uVar6 = ~-(uint)(*(long *)(pdVar5->rcvd_cookie + 0x14) == 0) & 0x210;
  }
  if ((*(long *)(pdVar5->cookie + 0xe4) != 0) && (*(long *)(pdVar5->cookie + 0xec) != 0)) {
    uVar7 = 0x80;
    uVar6 = 0x210;
  }
  if ((*(long *)(pdVar5->cookie + 0xbc) != 0) && (*(long *)(pdVar5->cookie + 0xc4) != 0)) {
    uVar6 = uVar6 | 0x10;
    uVar7 = uVar7 | 0x20;
  }
  if ((param_1[1].sid_ctx_length & 1) == 0) {
    if (bVar9) {
      uVar6 = uVar6 | 2;
    }
    bVar2 = false;
    bVar9 = bVar2;
    if ((((*(long *)(pdVar5->cookie + 0x44) != 0) && (*(long *)(pdVar5->cookie + 0x4c) != 0)) &&
        ((param_1[1].sid_ctx[1] & 1) != 0)) && (iVar4 = SSL_version(param_1), iVar4 >> 8 == 3)) {
      iVar4 = SSL_version(param_1);
      bVar9 = false;
      if (iVar4 == 0x303) goto LAB_0013a31c;
    }
  }
  else {
    if (bVar9) {
      uVar6 = uVar6 | 3;
      bVar2 = true;
    }
    else {
      uVar6 = uVar6 | 1;
      bVar2 = true;
    }
LAB_0013a31c:
    uVar7 = uVar7 | 1;
    bVar9 = bVar2;
  }
  if ((uVar8 & 1) != 0) {
    uVar7 = uVar7 | 2;
  }
  uVar8 = uVar7 | 4;
  if ((((uVar1 & 1) != 0) &&
      (cVar3 = X509_get_key_usage(*(undefined8 *)(pdVar5->cookie + 0x94)),
      (param_1[1].sid_ctx[8] & 2) != 0)) && (cVar3 < '\0')) {
    uVar8 = uVar7 | 0xc;
  }
  if ((uVar8 & 8) == 0) {
    pdVar5 = param_1[3].d1;
    if (((*(long *)(pdVar5->rcvd_cookie + 0x34) != 0) &&
        (*(long *)(pdVar5->rcvd_cookie + 0x3c) != 0)) && ((param_1[1].sid_ctx[0x19] & 1) != 0)) {
      iVar4 = SSL_version(param_1);
      if ((iVar4 >> 8 == 3) && (iVar4 = SSL_version(param_1), iVar4 == 0x303)) {
        uVar8 = uVar8 | 8;
        goto LAB_0013a390;
      }
      pdVar5 = param_1[3].d1;
    }
    if ((((*(long *)(pdVar5->rcvd_cookie + 0x5c) != 0) &&
         (*(long *)(pdVar5->rcvd_cookie + 100) != 0)) && ((param_1[1].sid_ctx[0x1d] & 1) != 0)) &&
       (iVar4 = SSL_version(param_1), iVar4 >> 8 == 3)) {
      iVar4 = SSL_version(param_1);
      if (iVar4 == 0x303) {
        uVar8 = uVar8 | 8;
      }
    }
  }
LAB_0013a390:
  *(uint *)&param_1[1].session = uVar8 | 0x10;
  uVar8 = uVar6 | 0x4c;
  if (!bVar9) {
    uVar8 = uVar6 | 0xc;
  }
  if ((uVar8 & 2) != 0) {
    uVar8 = uVar8 | 0x100;
  }
  *(uint *)&param_1[1].field_0x12c = uVar8 | 0x80;
  return;
}