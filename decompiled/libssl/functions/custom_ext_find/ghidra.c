ushort * custom_ext_find(ushort **param_1,int param_2,uint param_3,ushort **param_4)

{
  ushort *puVar1;
  ushort *puVar2;
  ushort *puVar3;
  
  puVar1 = param_1[1];
  puVar2 = *param_1;
  if (puVar1 != (ushort *)0x0) {
    puVar3 = (ushort *)0x0;
    if (param_2 == 2) {
      do {
        if (*puVar2 == param_3) {
LAB_00164775:
          if (param_4 == (ushort **)0x0) {
            return puVar2;
          }
          *param_4 = puVar3;
          return puVar2;
        }
        puVar3 = (ushort *)((long)puVar3 + 1);
        puVar2 = puVar2 + 0x1c;
      } while (puVar3 != puVar1);
    }
    else {
      do {
        if ((param_3 == *puVar2) &&
           ((*(int *)(puVar2 + 2) == 2 || (param_2 == *(int *)(puVar2 + 2))))) goto LAB_00164775;
        puVar3 = (ushort *)((long)puVar3 + 1);
        puVar2 = puVar2 + 0x1c;
      } while (puVar1 != puVar3);
    }
  }
  return (ushort *)0x0;
}