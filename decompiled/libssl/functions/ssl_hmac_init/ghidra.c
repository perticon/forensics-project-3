undefined8 ssl_hmac_init(long *param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4)

{
  int iVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined8 local_b8;
  undefined8 uStack176;
  undefined8 local_a8;
  undefined8 uStack160;
  undefined8 local_98;
  undefined8 local_88;
  undefined8 uStack128;
  undefined8 local_78;
  undefined8 uStack112;
  undefined8 local_68;
  undefined8 local_60;
  undefined8 uStack88;
  undefined8 local_50;
  undefined8 uStack72;
  undefined8 local_40;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (*param_1 != 0) {
    OSSL_PARAM_construct_utf8_string(&local_b8,"digest",param_4,0);
    local_68 = local_98;
    local_88 = local_b8;
    uStack128 = uStack176;
    local_78 = local_a8;
    uStack112 = uStack160;
    OSSL_PARAM_construct_end(local_b8,local_a8,&local_b8);
    local_60 = local_b8;
    uStack88 = uStack176;
    local_40 = local_98;
    local_50 = local_a8;
    uStack72 = uStack160;
    iVar1 = EVP_MAC_init(*param_1,param_2,param_3,&local_88);
    uVar2 = 1;
    if (iVar1 != 0) goto LAB_0014adf1;
  }
  uVar2 = 0;
  if (param_1[1] != 0) {
    uVar2 = ssl_hmac_old_init(param_1,param_2,param_3,param_4);
  }
LAB_0014adf1:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}