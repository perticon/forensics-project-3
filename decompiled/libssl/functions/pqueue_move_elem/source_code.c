static ossl_inline void pqueue_move_elem(OSSL_PQUEUE *pq, size_t from, size_t to)
{
    struct pq_heap_st *h = pq->heap;
    struct pq_elem_st *e = pq->elements;

    ASSERT_USED(pq, from);

    h[to] = h[from];
    e[h[to].index].posn = to;
}