undefined8 ossl_statem_server_read_transition(SSL *param_1,int param_2)

{
  int iVar1;
  int iVar2;
  BIO *b;
  uint uVar3;
  
  iVar1 = *(int *)((long)&param_1->init_msg + 4);
  uVar3 = *(uint *)(param_1->method->get_timeout + 0x60) & 8;
  if (uVar3 != 0) {
    switch(iVar1) {
    case 0:
    case 1:
    case 0x15:
      goto switchD_0017aec3_caseD_0;
    default:
      goto switchD_0017aec3_caseD_2;
    case 0x1a:
switchD_0017aec3_caseD_1a:
      if (param_2 == 0x10) {
        if (*(int *)&param_1[1].msg_callback == 0) {
LAB_0017af3d:
          *(undefined4 *)((long)&param_1->init_msg + 4) = 0x1c;
          return 1;
        }
        if (param_1->version == 0x300) {
          if ((param_1[3].error & 3U) == 3) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",200,"ossl_statem_server_read_transition");
            ossl_statem_fatal(param_1,0x28,199,0);
            return 0;
          }
          goto LAB_0017af3d;
        }
        goto switchD_0017af73_caseD_1c;
      }
      if ((param_2 == 0xb) && (*(int *)&param_1[1].msg_callback != 0)) goto LAB_0017afe5;
      break;
    case 0x1b:
switchD_0017aec3_caseD_1b:
      if (param_2 == 0x10) goto LAB_0017af3d;
      break;
    case 0x1c:
switchD_0017aec3_caseD_1c:
      if ((*(long *)(*(long *)&param_1[3].sid_ctx_length + 0x2b8) == 0) ||
         (*(int *)&param_1->field_0x74 != 0)) goto switchD_0017aec3_caseD_1d;
      if (param_2 == 0xf) {
LAB_0017b00d:
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x1d;
        return 1;
      }
      break;
    case 0x1d:
    case 0x24:
switchD_0017aec3_caseD_1d:
      if (param_2 == 0x101) {
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x1f;
        return 1;
      }
      goto switchD_0017af73_caseD_1c;
    case 0x1e:
switchD_0017aec3_caseD_1e:
      if (param_2 == 0x14) goto LAB_0017afc4;
      break;
    case 0x1f:
switchD_0017aec3_caseD_1f:
      if (*(int *)&param_1[1].field_0x1d4 == 0) goto switchD_0017aec3_caseD_1e;
      if (param_2 == 0x43) {
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x1e;
        return 1;
      }
    }
LAB_0017ae67:
    if (uVar3 != 0) {
switchD_0017aec3_caseD_2:
      if (param_2 == 0x101) {
        param_1->msg_callback = (_func_3150 *)0x0;
        param_1->rwstate = 3;
        b = SSL_get_rbio(param_1);
        BIO_clear_flags(b,0xf);
        BIO_set_flags(b,9);
        return 0;
      }
    }
    goto switchD_0017af73_caseD_1c;
  }
  iVar2 = param_1->method->version;
  if ((iVar2 == 0x10000) || (iVar2 < 0x304)) {
    switch(iVar1) {
    case 0:
    case 1:
    case 0x15:
      goto switchD_0017aec3_caseD_0;
    default:
      goto switchD_0017af73_caseD_1c;
    case 0x1a:
      goto switchD_0017aec3_caseD_1a;
    case 0x1b:
      goto switchD_0017aec3_caseD_1b;
    case 0x1c:
      goto switchD_0017aec3_caseD_1c;
    case 0x1d:
    case 0x24:
      goto switchD_0017aec3_caseD_1d;
    case 0x1e:
      goto switchD_0017aec3_caseD_1e;
    case 0x1f:
      goto switchD_0017aec3_caseD_1f;
    }
  }
  if (iVar1 == 1) {
    if (*(int *)((long)&param_1->s3 + 4) != 0xb) {
      if (param_2 != 0xb) {
        if (param_2 == 0x18) {
          *(undefined4 *)((long)&param_1->init_msg + 4) = 0x2c;
          return 1;
        }
        goto switchD_0017af73_caseD_1c;
      }
      if (*(int *)&param_1[4].enc_write_ctx != 4) goto switchD_0017af73_caseD_1c;
      goto LAB_0017afe5;
    }
    goto switchD_0017af73_caseD_1c;
  }
  switch(iVar1) {
  case 0x1b:
    if (*(long *)(*(long *)&param_1[3].sid_ctx_length + 0x2b8) != 0) {
      if (param_2 != 0xf) goto switchD_0017af73_caseD_1c;
      goto LAB_0017b00d;
    }
    break;
  default:
    goto switchD_0017af73_caseD_1c;
  case 0x1d:
    break;
  case 0x24:
  case 0x31:
    goto switchD_0017af73_caseD_24;
  case 0x2e:
    if (*(int *)&param_1[3].read_hash == 1) {
      if (param_2 != 1) goto switchD_0017af73_caseD_1c;
      goto LAB_0017af01;
    }
    if (param_1[4].packet_length == 2) {
      if (param_2 == 5) {
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x31;
        return 1;
      }
      goto switchD_0017af73_caseD_1c;
    }
switchD_0017af73_caseD_24:
    if (*(int *)&param_1[1].msg_callback != 0) {
      if (param_2 == 0xb) {
LAB_0017afe5:
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x1b;
        return 1;
      }
      goto switchD_0017af73_caseD_1c;
    }
  }
  if (param_2 == 0x14) {
LAB_0017afc4:
    *(undefined4 *)((long)&param_1->init_msg + 4) = 0x20;
    return 1;
  }
switchD_0017af73_caseD_1c:
  ERR_new();
  ERR_set_debug("ssl/statem/statem_srvr.c",0x138,"ossl_statem_server_read_transition");
  ossl_statem_fatal(param_1,10,0xf4,0);
  return 0;
switchD_0017aec3_caseD_0:
  if (param_2 == 1) {
LAB_0017af01:
    *(undefined4 *)((long)&param_1->init_msg + 4) = 0x14;
    return 1;
  }
  goto LAB_0017ae67;
}