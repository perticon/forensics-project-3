int32_t ssl_cipher_strength_sort(struct s0** rdi, struct s0** rsi, ...) {
    struct s0** r13_3;
    struct s0** r12_4;
    struct s0* rax5;
    void** rdi6;
    int64_t rbx7;
    int64_t rdx8;
    void** rax9;
    void** rbp10;
    int32_t eax11;
    struct s0* rdx12;
    int64_t rax13;

    r13_3 = rdi;
    r12_4 = rsi;
    rax5 = *rdi;
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rdi6) = 4;
        *reinterpret_cast<int32_t*>(&rdi6 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
    } else {
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
        do {
            if (rax5->f8 && (rdx8 = rax5->f0->f44, *reinterpret_cast<int32_t*>(&rbx7) < *reinterpret_cast<int32_t*>(&rdx8))) {
                rbx7 = rdx8;
            }
            rax5 = rax5->f10;
        } while (rax5);
        rdi6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(static_cast<int64_t>(static_cast<int32_t>(rbx7 + 1))) << 2);
    }
    rax9 = fun_219a0(rdi6, "ssl/ssl_ciph.c", 0x3c2);
    rbp10 = rax9;
    if (!rax9) {
        fun_20230();
        fun_207e0("ssl/ssl_ciph.c", "ssl/ssl_ciph.c");
        fun_20680();
        eax11 = 0;
    } else {
        rdx12 = *r13_3;
        if (!rdx12) {
            do {
                addr_301b0_11:
                if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp10 + rbx7 * 4)) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbp10 + rbx7 * 4) == 0))) {
                    ssl_cipher_apply_rule_constprop_1(0, 0, 0, 0, *reinterpret_cast<int32_t*>(&rbx7), r13_3, r12_4);
                }
                --rbx7;
            } while (*reinterpret_cast<int32_t*>(&rbx7) != -1);
            goto addr_301dc_14;
        } else {
            while (1) {
                if (!rdx12->f8) {
                    rdx12 = rdx12->f10;
                    if (!rdx12) 
                        goto addr_30209_18;
                } else {
                    rdx12 = rdx12->f10;
                    rax13 = rdx12->f0->f44;
                    *reinterpret_cast<void***>(rbp10 + rax13 * 4) = *reinterpret_cast<void***>(rbp10 + rax13 * 4) + 1;
                    if (!rdx12) 
                        goto addr_301ac_20;
                }
            }
        }
    }
    addr_301f5_21:
    return eax11;
    addr_301dc_14:
    fun_208d0();
    eax11 = 1;
    goto addr_301f5_21;
    addr_30209_18:
    goto addr_301b0_11;
    addr_301ac_20:
    goto addr_301b0_11;
}