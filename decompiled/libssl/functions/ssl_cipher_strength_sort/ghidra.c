undefined8 ssl_cipher_strength_sort(long **param_1,undefined8 param_2)

{
  int *piVar1;
  long *plVar2;
  void *ptr;
  ulong uVar3;
  long lVar4;
  undefined8 uVar5;
  
  plVar2 = *param_1;
  if (plVar2 == (long *)0x0) {
    lVar4 = 4;
    uVar3 = 0;
  }
  else {
    uVar3 = 0;
    do {
      if ((*(int *)(plVar2 + 1) != 0) && ((int)uVar3 < *(int *)(*plVar2 + 0x44))) {
        uVar3 = (long)*(int *)(*plVar2 + 0x44);
      }
      plVar2 = (long *)plVar2[2];
    } while (plVar2 != (long *)0x0);
    lVar4 = (long)((int)uVar3 + 1) << 2;
  }
  uVar5 = 0x130178;
  ptr = (void *)CRYPTO_zalloc(lVar4,"ssl/ssl_ciph.c",0x3c2);
  if (ptr == (void *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_ciph.c",0x3c4,"ssl_cipher_strength_sort");
    ERR_set_error(0x14,0xc0100,0);
    uVar5 = 0;
  }
  else {
    plVar2 = *param_1;
    while (plVar2 != (long *)0x0) {
      while (*(int *)(plVar2 + 1) == 0) {
        plVar2 = (long *)plVar2[2];
        if (plVar2 == (long *)0x0) goto LAB_001301b0;
      }
      lVar4 = *plVar2;
      plVar2 = (long *)plVar2[2];
      piVar1 = (int *)((long)ptr + (long)*(int *)(lVar4 + 0x44) * 4);
      *piVar1 = *piVar1 + 1;
    }
LAB_001301b0:
    do {
      if (0 < *(int *)((long)ptr + uVar3 * 4)) {
        ssl_cipher_apply_rule_constprop_1(0,0,0,0,uVar3 & 0xffffffff,param_1,param_2,uVar5);
      }
      uVar3 = uVar3 - 1;
    } while ((int)uVar3 != -1);
    CRYPTO_free(ptr);
    uVar5 = 1;
  }
  return uVar5;
}