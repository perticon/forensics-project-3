int64_t ssl_cipher_strength_sort(int64_t a1, int64_t a2) {
    int64_t v1 = 0; // 0x30136
    int64_t v2 = 4; // 0x30136
    if (a1 != 0) {
        int64_t v3 = v1;
        int64_t v4 = v3; // 0x30145
        int32_t v5; // 0x3014a
        int32_t v6; // 0x3014e
        int32_t v7; // 0x3014e
        if (*(int32_t *)(a1 + 8) != 0) {
            // 0x30147
            v5 = *(int32_t *)(*(int64_t *)a1 + 68);
            v6 = v3;
            v7 = v6 - v5;
            v4 = v7 < 0 == ((v7 ^ v6) & (v5 ^ v6)) < 0 ? v3 : (int64_t)v5;
        }
        // 0x30154
        v1 = v4;
        int64_t v8 = *(int64_t *)(a1 + 16); // 0x30154
        while (v8 != 0) {
            // 0x30140
            v3 = v1;
            int64_t v9 = v8;
            v4 = v3;
            if (*(int32_t *)(v9 + 8) != 0) {
                // 0x30147
                v5 = *(int32_t *)(*(int64_t *)v9 + 68);
                v6 = v3;
                v7 = v6 - v5;
                v4 = v7 < 0 == ((v7 ^ v6) & (v5 ^ v6)) < 0 ? v3 : (int64_t)v5;
            }
            // 0x30154
            v1 = v4;
            v8 = *(int64_t *)(v9 + 16);
        }
        // 0x3015d
        v2 = 0x100000000 * v1 + 0x100000000 >> 30;
    }
    int64_t v10 = function_219a0(); // 0x30173
    if (v10 == 0) {
        // 0x30220
        function_20230();
        function_207e0();
        function_20680();
        // 0x301f5
        return 0;
    }
    int64_t v11 = v2; // 0x3018b
    if (v2 != 0) {
        while (true) {
            int64_t v12 = v11;
            if (*(int32_t *)(v12 + 8) == 0) {
                int64_t v13 = *(int64_t *)(v12 + 16); // 0x30200
                v11 = v13;
                if (v13 == 0) {
                    // break -> 0x301b0
                    break;
                }
            } else {
                int64_t v14 = *(int64_t *)(v12 + 16); // 0x3019a
                int32_t v15 = *(int32_t *)(*(int64_t *)v12 + 68); // 0x3019e
                int32_t * v16 = (int32_t *)(4 * (int64_t)v15 + v10); // 0x301a2
                *v16 = *v16 + 1;
                v11 = v14;
                if (v14 == 0) {
                    // break -> 0x301b0
                    break;
                }
            }
        }
    }
    // 0x301b0
    int64_t v17; // bp-40, 0x30120
    if (*(int32_t *)(4 * v1 + v10) >= 1) {
        // 0x301b8
        *(int64_t *)((int64_t)&v17 - 16) = a2;
        ssl_cipher_apply_rule_constprop_1(0, 0, 0, 0, v1 & 0xffffffff, a1, (int64_t)&g551);
    }
    int64_t v18 = v1 - 1; // 0x301d3
    while ((int32_t)v18 != -1) {
        int64_t v19 = v18;
        if (*(int32_t *)(4 * v19 + v10) >= 1) {
            // 0x301b8
            *(int64_t *)((int64_t)&v17 - 16) = a2;
            ssl_cipher_apply_rule_constprop_1(0, 0, 0, 0, v19 & 0xffffffff, a1, (int64_t)&g551);
        }
        // 0x301d3
        v18 = v19 - 1;
    }
    // 0x301dc
    function_208d0();
    // 0x301f5
    return 1;
}