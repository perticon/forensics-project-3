undefined8 SSL_CONF_cmd_argv(uint *param_1,int *param_2,long **param_3)

{
  long lVar1;
  long lVar2;
  uint uVar3;
  int iVar4;
  undefined8 uVar5;
  
  if (param_2 == (int *)0x0) {
    lVar1 = **param_3;
    if (lVar1 == 0) {
      return 0;
    }
    lVar2 = (*param_3)[1];
    *param_1 = *param_1 & 0xfffffffd | 1;
    uVar5 = SSL_CONF_cmd(param_1,lVar1,lVar2);
    if (0 < (int)uVar5) {
      *param_3 = *param_3 + (int)uVar5;
      return uVar5;
    }
  }
  else {
    if (*param_2 < 1) {
      return 0;
    }
    lVar1 = **param_3;
    if (lVar1 == 0) {
      return 0;
    }
    uVar3 = *param_1 & 0xfffffffd | 1;
    if (*param_2 == 1) {
      *param_1 = uVar3;
      uVar5 = SSL_CONF_cmd(param_1,lVar1,0);
      iVar4 = (int)uVar5;
    }
    else {
      lVar2 = (*param_3)[1];
      *param_1 = uVar3;
      uVar5 = SSL_CONF_cmd(param_1,lVar1,lVar2);
      iVar4 = (int)uVar5;
    }
    if (0 < iVar4) {
      *param_3 = *param_3 + (int)uVar5;
      *param_2 = *param_2 - (int)uVar5;
      return uVar5;
    }
  }
  if ((int)uVar5 == -2) {
    return 0;
  }
  if ((int)uVar5 == 0) {
    uVar5 = 0xffffffff;
  }
  return uVar5;
}