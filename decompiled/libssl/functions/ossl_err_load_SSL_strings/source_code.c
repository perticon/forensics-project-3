int ossl_err_load_SSL_strings(void)
{
#ifndef OPENSSL_NO_ERR
    if (ERR_reason_error_string(SSL_str_reasons[0].error) == NULL)
        ERR_load_strings_const(SSL_str_reasons);
#endif
    return 1;
}