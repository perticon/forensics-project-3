long SSL_callback_ctrl(SSL *param_1,int param_2,void *param_3)

{
  long lVar1;
  
  if (param_2 != 0xf) {
                    /* WARNING: Could not recover jumptable at 0x00138aed. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    lVar1 = (long)(*param_1->method->ssl_version)();
    return lVar1;
  }
  param_1[1].tlsext_ocsp_resp = (uchar *)param_3;
  return 1;
}