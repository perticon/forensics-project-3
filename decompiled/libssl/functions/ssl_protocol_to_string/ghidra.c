char * ssl_protocol_to_string(int param_1)

{
  char *pcVar1;
  
  if (param_1 < 0x305) {
    if (param_1 < 0x300) {
      pcVar1 = "unknown";
      if (param_1 == 0x100) {
        pcVar1 = "DTLSv0.9";
      }
      return pcVar1;
    }
    pcVar1 = "SSLv3";
    switch(param_1) {
    case 0x301:
      return "TLSv1";
    case 0x302:
      return "TLSv1.1";
    case 0x303:
      return "TLSv1.2";
    case 0x304:
      pcVar1 = "TLSv1.3";
    }
  }
  else {
    pcVar1 = "DTLSv1.2";
    if (param_1 != 0xfefd) {
      pcVar1 = "unknown";
      if (param_1 == 0xfeff) {
        pcVar1 = "DTLSv1";
      }
      return pcVar1;
    }
  }
  return pcVar1;
}