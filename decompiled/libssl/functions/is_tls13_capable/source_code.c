static int is_tls13_capable(const SSL *s)
{
    int i;
    int curve;

    if (!ossl_assert(s->ctx != NULL) || !ossl_assert(s->session_ctx != NULL))
        return 0;

    /*
     * A servername callback can change the available certs, so if a servername
     * cb is set then we just assume TLSv1.3 will be ok
     */
    if (s->ctx->ext.servername_cb != NULL
            || s->session_ctx->ext.servername_cb != NULL)
        return 1;

#ifndef OPENSSL_NO_PSK
    if (s->psk_server_callback != NULL)
        return 1;
#endif

    if (s->psk_find_session_cb != NULL || s->cert->cert_cb != NULL)
        return 1;

    for (i = 0; i < SSL_PKEY_NUM; i++) {
        /* Skip over certs disallowed for TLSv1.3 */
        switch (i) {
        case SSL_PKEY_DSA_SIGN:
        case SSL_PKEY_GOST01:
        case SSL_PKEY_GOST12_256:
        case SSL_PKEY_GOST12_512:
            continue;
        default:
            break;
        }
        if (!ssl_has_cert(s, i))
            continue;
        if (i != SSL_PKEY_ECC)
            return 1;
        /*
         * Prior to TLSv1.3 sig algs allowed any curve to be used. TLSv1.3 is
         * more restrictive so check that our sig algs are consistent with this
         * EC cert. See section 4.2.3 of RFC8446.
         */
        curve = ssl_get_EC_curve_nid(s->cert->pkeys[SSL_PKEY_ECC].privatekey);
        if (tls_check_sigalg_curve(s, curve))
            return 1;
    }

    return 0;
}