undefined8 tls1_save_u16(ushort **param_1,void **param_2,ulong *param_3)

{
  ushort uVar1;
  ushort *puVar2;
  void *ptr;
  ulong uVar3;
  undefined8 uVar4;
  ulong uVar5;
  
  puVar2 = param_1[1];
  uVar4 = 0;
  if (puVar2 != (ushort *)0x0) {
    uVar3 = (ulong)((uint)puVar2 & 1);
    if (((ulong)puVar2 & 1) == 0) {
      uVar5 = (ulong)puVar2 >> 1;
      ptr = CRYPTO_malloc((uint)puVar2 & 0xfffffffe,"ssl/t1_lib.c",0x910);
      if (ptr == (void *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/t1_lib.c",0x911,"tls1_save_u16");
        ERR_set_error(0x14,0xc0100,0);
        uVar4 = 0;
      }
      else {
        if (uVar5 != 0) {
          puVar2 = param_1[1];
          do {
            if ((ulong)puVar2 >> 1 == uVar3) {
              CRYPTO_free(ptr);
              return 0;
            }
            uVar1 = **param_1;
            *param_1 = *param_1 + 1;
            param_1[1] = puVar2 + (-1 - uVar3);
            *(ushort *)((long)ptr + uVar3 * 2) = uVar1 << 8 | uVar1 >> 8;
            uVar3 = uVar3 + 1;
          } while (uVar5 != uVar3);
        }
        CRYPTO_free(*param_2);
        *param_2 = ptr;
        *param_3 = uVar5;
        uVar4 = 1;
      }
    }
  }
  return uVar4;
}