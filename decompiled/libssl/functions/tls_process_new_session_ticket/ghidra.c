undefined8 tls_process_new_session_ticket(long param_1,uint **param_2)

{
  uint uVar1;
  int *piVar2;
  uint *puVar3;
  long lVar4;
  uint uVar5;
  int iVar6;
  int iVar7;
  EVP_MD *pEVar8;
  ulong uVar9;
  void *pvVar10;
  uint *puVar11;
  ushort *puVar12;
  ushort uVar13;
  undefined8 uVar14;
  uint *puVar15;
  uint *puVar16;
  long lVar17;
  uint uVar18;
  long in_FS_OFFSET;
  EVP_MD *local_78;
  void *local_60;
  ushort *local_58;
  ushort *local_50;
  long local_40;
  
  puVar11 = param_2[1];
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = (void *)0x0;
  if (puVar11 < (uint *)0x4) {
LAB_0016e880:
    ERR_new();
    uVar14 = 0x9a5;
LAB_0016e891:
    ERR_set_debug("ssl/statem/statem_clnt.c",uVar14,"tls_process_new_session_ticket");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
    pEVar8 = (EVP_MD *)0x0;
  }
  else {
    piVar2 = *(int **)(param_1 + 8);
    puVar3 = *param_2;
    puVar16 = puVar11 + -1;
    lVar17 = *(long *)(piVar2 + 0x30);
    puVar15 = puVar3 + 1;
    uVar1 = *puVar3;
    param_2[1] = puVar16;
    *param_2 = puVar15;
    uVar5 = *(uint *)(lVar17 + 0x60) & 8;
    if (((uVar5 == 0) && (*piVar2 != 0x10000)) && (0x303 < *piVar2)) {
      if ((uint *)0x3 < puVar16) {
        uVar18 = puVar3[1];
        *param_2 = puVar3 + 2;
        param_2[1] = puVar11 + -2;
        if (puVar11 + -2 != (uint *)0x0) {
          local_78 = (EVP_MD *)(ulong)*(byte *)(puVar3 + 2);
          if (local_78 <= (long)puVar11 - 9U) {
            lVar17 = (long)puVar3 + 9;
            puVar16 = (uint *)(((long)puVar11 - 9U) - (long)local_78);
            uVar18 = uVar18 >> 0x18 | (uVar18 & 0xff0000) >> 8 | (uVar18 & 0xff00) << 8 |
                     uVar18 << 0x18;
            puVar15 = (uint *)(lVar17 + (long)local_78);
            param_2[1] = puVar16;
            *param_2 = puVar15;
            goto LAB_0016e83a;
          }
        }
      }
      goto LAB_0016e880;
    }
    local_78 = (EVP_MD *)0x0;
    uVar18 = 0;
    lVar17 = 0;
LAB_0016e83a:
    if (puVar16 < (uint *)0x2) goto LAB_0016e880;
    uVar13 = *(ushort *)puVar15;
    puVar16 = (uint *)((long)puVar16 + -2);
    *param_2 = (uint *)((long)puVar15 + 2);
    param_2[1] = puVar16;
    uVar13 = uVar13 << 8 | uVar13 >> 8;
    puVar11 = (uint *)(ulong)uVar13;
    if (((uVar5 != 0) || (*piVar2 < 0x304)) || (*piVar2 == 0x10000)) {
      if (puVar11 == puVar16) {
        if (uVar13 != 0) {
          pEVar8 = *(EVP_MD **)(param_1 + 0x918);
          if ((((uVar5 == 0) && (0x303 < *piVar2)) && (*piVar2 != 0x10000)) ||
             (pEVar8[4].md_ctrl != (_func_1085 *)0x0)) goto LAB_0016e99a;
          goto LAB_0016ea11;
        }
LAB_0016ebe8:
        uVar14 = 3;
        goto LAB_0016e8d6;
      }
      goto LAB_0016e880;
    }
    if ((uVar13 == 0) || (puVar16 < puVar11)) goto LAB_0016e880;
    pEVar8 = *(EVP_MD **)(param_1 + 0x918);
    puVar16 = puVar11;
LAB_0016e99a:
    pEVar8 = (EVP_MD *)ssl_session_dup(pEVar8,0);
    if (pEVar8 != (EVP_MD *)0x0) {
      if (((*(byte *)&(*(SSL_CTX **)(param_1 + 0xb88))->new_session_cb & 1) != 0) &&
         ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0 ||
           (iVar6 = **(int **)(param_1 + 8), iVar6 < 0x304)) || (iVar6 == 0x10000)))) {
        SSL_CTX_remove_session(*(SSL_CTX **)(param_1 + 0xb88),*(SSL_SESSION **)(param_1 + 0x918));
      }
      SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x918));
      *(EVP_MD **)(param_1 + 0x918) = pEVar8;
LAB_0016ea11:
      uVar9 = time((time_t *)0x0);
      uVar14 = *(undefined8 *)(param_1 + 0x918);
      pEVar8[6].flags = uVar9;
      ssl_session_calculate_timeout(uVar14);
      CRYPTO_free(*(void **)(*(long *)(param_1 + 0x918) + 0x338));
      lVar4 = *(long *)(param_1 + 0x918);
      *(undefined8 *)(lVar4 + 0x338) = 0;
      *(undefined8 *)(lVar4 + 0x340) = 0;
      pvVar10 = CRYPTO_malloc((int)puVar16,"ssl/statem/statem_clnt.c",0x9da);
      *(void **)(lVar4 + 0x338) = pvVar10;
      pEVar8 = *(EVP_MD **)(*(long *)(param_1 + 0x918) + 0x338);
      if (pEVar8 == (EVP_MD *)0x0) {
        ERR_new();
        uVar14 = 0x9dc;
        local_78 = pEVar8;
        goto LAB_0016edc5;
      }
      if (puVar16 <= param_2[1]) {
        memcpy(pEVar8,*param_2,(size_t)puVar16);
        lVar4 = *(long *)(param_1 + 0x918);
        puVar15 = (uint *)((long)*param_2 + (long)puVar16);
        puVar11 = (uint *)((long)param_2[1] - (long)puVar16);
        *param_2 = puVar15;
        param_2[1] = puVar11;
        *(ulong *)(lVar4 + 0x348) =
             (ulong)(uVar1 >> 0x18 | (uVar1 & 0xff0000) >> 8 | (uVar1 & 0xff00) << 8 | uVar1 << 0x18
                    );
        *(uint *)(lVar4 + 0x350) = uVar18;
        *(uint **)(lVar4 + 0x340) = puVar16;
        if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
            (iVar6 = **(int **)(param_1 + 8), iVar6 == 0x10000)) || (iVar6 < 0x304)) {
LAB_0016ec60:
          pEVar8 = (EVP_MD *)
                   EVP_MD_fetch(**(undefined8 **)(param_1 + 0x9a8),"SHA2-256",
                                (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
          if (pEVar8 == (EVP_MD *)0x0) {
            ossl_statem_send_fatal(param_1,0x50);
            goto LAB_0016e8b6;
          }
          iVar6 = EVP_Digest(*(void **)(*(long *)(param_1 + 0x918) + 0x338),(size_t)puVar16,
                             (uchar *)(*(long *)(param_1 + 0x918) + 600),(uint *)&local_58,pEVar8,
                             (ENGINE *)0x0);
          if (iVar6 != 0) {
            EVP_MD_free(pEVar8);
            lVar4 = *(long *)(param_1 + 0x918);
            *(undefined4 *)(lVar4 + 0x2b0) = 0;
            *(ulong *)(lVar4 + 0x250) = (ulong)local_58 & 0xffffffff;
            if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
                (iVar6 = **(int **)(param_1 + 8), iVar6 == 0x10000)) || (iVar6 < 0x304))
            goto LAB_0016ebe8;
            uVar14 = ssl_handshake_md(param_1);
            iVar6 = EVP_MD_get_size(uVar14);
            if (iVar6 < 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0xa25,"tls_process_new_session_ticket");
              ossl_statem_fatal(param_1,0x50,0xc0103,0);
              goto LAB_0016eb94;
            }
            iVar7 = tls13_hkdf_expand(param_1,uVar14,param_1 + 0x604,"resumption",10,lVar17,local_78
                                      ,*(long *)(param_1 + 0x918) + 0x50,(long)iVar6,1);
            pEVar8 = (EVP_MD *)0x0;
            if (iVar7 != 0) {
              *(long *)(*(long *)(param_1 + 0x918) + 8) = (long)iVar6;
              CRYPTO_free(local_60);
              ssl_update_cache(param_1,1);
              uVar14 = 1;
              goto LAB_0016e8d6;
            }
            goto LAB_0016e8b6;
          }
          ERR_new();
          ERR_set_debug("ssl/statem/statem_clnt.c",0xa14,"tls_process_new_session_ticket");
          uVar14 = 0x80006;
          local_78 = pEVar8;
          goto LAB_0016edd8;
        }
        if ((puVar11 < (uint *)0x2) ||
           (puVar12 = (ushort *)((long)puVar11 + -2),
           puVar12 != (ushort *)(ulong)(ushort)(*(ushort *)puVar15 << 8 | *(ushort *)puVar15 >> 8)))
        {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_clnt.c",0x9ed,"tls_process_new_session_ticket");
          ossl_statem_fatal(param_1,0x32,0x9f,0);
        }
        else {
          local_58 = (ushort *)((long)puVar15 + 2);
          param_2[1] = (uint *)0x0;
          *param_2 = (uint *)((long)local_58 + (long)puVar12);
          local_50 = puVar12;
          iVar6 = tls_collect_extensions(param_1,&local_58,0x2000,&local_60,0,1);
          if ((iVar6 != 0) &&
             (iVar6 = tls_parse_all_extensions(param_1,0x2000,local_60,0,0), iVar6 != 0))
          goto LAB_0016ec60;
        }
LAB_0016eb94:
        pEVar8 = (EVP_MD *)0x0;
        goto LAB_0016e8b6;
      }
      ERR_new();
      uVar14 = 0x9e0;
      goto LAB_0016e891;
    }
    ERR_new();
    uVar14 = 0x9c1;
    local_78 = pEVar8;
LAB_0016edc5:
    ERR_set_debug("ssl/statem/statem_clnt.c",uVar14,"tls_process_new_session_ticket");
    uVar14 = 0xc0100;
LAB_0016edd8:
    ossl_statem_fatal(param_1,0x50,uVar14,0);
    pEVar8 = local_78;
  }
LAB_0016e8b6:
  EVP_MD_free(pEVar8);
  CRYPTO_free(local_60);
  uVar14 = 0;
LAB_0016e8d6:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar14;
}