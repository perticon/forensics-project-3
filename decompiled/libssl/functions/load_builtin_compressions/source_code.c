static int load_builtin_compressions(void)
{
    return RUN_ONCE(&ssl_load_builtin_comp_once, do_load_builtin_compressions);
}