const char *SSL_COMP_get0_name(const SSL_COMP *comp)
{
#ifndef OPENSSL_NO_COMP
    return comp->name;
#else
    return NULL;
#endif
}