undefined4 SSL_CTX_use_serverinfo_ex(long param_1,int param_2,void *param_3,size_t param_4)

{
  int iVar1;
  void *__dest;
  
  if ((param_3 == (void *)0x0 || param_4 == 0) || (param_1 == 0)) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",0x2cd,"SSL_CTX_use_serverinfo_ex");
    ERR_set_error(0x14,0xc0102,0);
    return 0;
  }
  if ((param_2 - 1U < 2) &&
     ((-1 < (long)param_4 &&
      (iVar1 = serverinfo_process_buffer_part_0(param_2,param_3,param_4,0), iVar1 != 0)))) {
    if (**(long **)(param_1 + 0x158) == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0x2d6,"SSL_CTX_use_serverinfo_ex");
      ERR_set_error(0x14,0xc0103,0);
      return 0;
    }
    __dest = CRYPTO_realloc(*(void **)(**(long **)(param_1 + 0x158) + 0x18),(int)param_4,
                            "ssl/ssl_rsa.c",0x2d9);
    if (__dest != (void *)0x0) {
      *(void **)(**(long **)(param_1 + 0x158) + 0x18) = __dest;
      memcpy(__dest,param_3,param_4);
      *(size_t *)(**(long **)(param_1 + 0x158) + 0x20) = param_4;
      iVar1 = serverinfo_process_buffer_part_0(param_2,param_3,param_4,param_1);
      if (iVar1 == 0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_rsa.c",0x2e9,"SSL_CTX_use_serverinfo_ex");
        ERR_set_error(0x14,0x184,0);
        return 0;
      }
      return 1;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",0x2dc,"SSL_CTX_use_serverinfo_ex");
    ERR_set_error(0x14,0xc0100,0);
    return 0;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_rsa.c",0x2d2,"SSL_CTX_use_serverinfo_ex");
  ERR_set_error(0x14,0x184,0);
  return 0;
}