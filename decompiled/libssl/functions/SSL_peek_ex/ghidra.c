int SSL_peek_ex(long param_1,undefined8 param_2,undefined8 param_3,undefined8 *param_4)

{
  int iVar1;
  long lVar2;
  int iVar3;
  long in_FS_OFFSET;
  long local_58;
  undefined8 uStack80;
  undefined8 local_48;
  undefined4 local_40;
  undefined8 local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(long *)(param_1 + 0x30) == 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x7a2,"ssl_peek_internal");
    ERR_set_error(0x14,0x114,0);
    iVar3 = 0;
  }
  else {
    iVar3 = 0;
    if ((*(byte *)(param_1 + 0x44) & 2) == 0) {
      if (((*(byte *)(param_1 + 0x9f1) & 1) == 0) || (lVar2 = ASYNC_get_current_job(), lVar2 != 0))
      {
        iVar1 = (**(code **)(*(long *)(param_1 + 8) + 0x40))(param_1,param_2,param_3,param_4);
        iVar3 = 0;
        if (-1 < iVar1) {
          iVar3 = iVar1;
        }
      }
      else {
        local_38 = *(undefined8 *)(*(long *)(param_1 + 8) + 0x40);
        local_40 = 0;
        local_58 = param_1;
        uStack80 = param_2;
        local_48 = param_3;
        iVar1 = ssl_start_async_job(param_1,&local_58,ssl_io_intern);
        *param_4 = *(undefined8 *)(param_1 + 0x1d38);
        if (-1 < iVar1) {
          iVar3 = iVar1;
        }
      }
    }
  }
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar3;
}