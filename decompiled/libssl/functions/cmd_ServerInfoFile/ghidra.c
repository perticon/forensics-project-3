bool cmd_ServerInfoFile(long param_1)

{
  int iVar1;
  
  if (*(long *)(param_1 + 0x18) != 0) {
    iVar1 = SSL_CTX_use_serverinfo_file();
    return 0 < iVar1;
  }
  return true;
}