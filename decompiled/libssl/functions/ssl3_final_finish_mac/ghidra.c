long ssl3_final_finish_mac(long param_1,void *param_2,size_t param_3,uchar *param_4)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  EVP_MD_CTX *out;
  long lVar4;
  long in_FS_OFFSET;
  undefined local_b8 [120];
  long local_40;
  
  lVar4 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = ssl3_digest_cached_records(param_1,0);
  if (iVar1 == 0) goto LAB_00128622;
  uVar3 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 400));
  iVar1 = EVP_MD_get_type(uVar3);
  if (iVar1 != 0x72) {
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x1ac,"ssl3_final_finish_mac");
    ossl_statem_fatal(param_1,0x50,0x144,0);
    goto LAB_00128622;
  }
  out = (EVP_MD_CTX *)EVP_MD_CTX_new();
  if (out == (EVP_MD_CTX *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x1b2,"ssl3_final_finish_mac");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
    goto LAB_00128622;
  }
  iVar1 = EVP_MD_CTX_copy_ex(out,*(EVP_MD_CTX **)(param_1 + 400));
  if (iVar1 == 0) {
    ERR_new();
    uVar3 = 0x1b6;
LAB_00128751:
    ERR_set_debug("ssl/s3_enc.c",uVar3,"ssl3_final_finish_mac");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    uVar3 = EVP_MD_CTX_get0_md(out);
    iVar1 = EVP_MD_get_size(uVar3);
    if (iVar1 < 0) {
      ERR_new();
      uVar3 = 0x1bd;
      goto LAB_00128751;
    }
    lVar4 = (long)iVar1;
    if (param_2 != (void *)0x0) {
      ssl3_digest_master_key_set_params(*(undefined8 *)(param_1 + 0x918),local_b8);
      iVar2 = EVP_DigestUpdate(out,param_2,param_3);
      if (0 < iVar2) {
        iVar2 = EVP_MD_CTX_set_params(out,local_b8);
        if (0 < iVar2) {
          iVar2 = EVP_DigestFinal_ex(out,param_4,(uint *)0x0);
          if (0 < iVar2) {
            lVar4 = (long)iVar1;
            goto LAB_00128773;
          }
        }
      }
      ERR_new();
      lVar4 = 0;
      ERR_set_debug("ssl/s3_enc.c",0x1ca,"ssl3_final_finish_mac");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
  }
LAB_00128773:
  EVP_MD_CTX_free(out);
LAB_00128622:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return lVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}