int ssl_write(BIO *param_1,long param_2,undefined8 param_3,long *param_4)

{
  SSL *s;
  int ret_code;
  int iVar1;
  SSL **ppSVar2;
  SSL *pSVar3;
  
  ret_code = 0;
  if (param_2 != 0) {
    ppSVar2 = (SSL **)BIO_get_data();
    s = *ppSVar2;
    BIO_clear_flags(param_1,0xf);
    ret_code = ssl_write_internal(s,param_2,param_3,param_4);
    iVar1 = SSL_get_error(s,ret_code);
    switch(iVar1) {
    case 0:
      if ((ppSVar2[2] == (SSL *)0x0) ||
         (pSVar3 = (SSL *)((long)&ppSVar2[3]->version + *param_4), ppSVar2[3] = pSVar3,
         pSVar3 <= ppSVar2[2])) {
        if ((ppSVar2[4] != (SSL *)0x0) &&
           (pSVar3 = (SSL *)time((time_t *)0x0),
           (SSL *)((long)&ppSVar2[5]->version + (long)&ppSVar2[4]->version) < pSVar3)) {
          *(int *)(ppSVar2 + 1) = *(int *)(ppSVar2 + 1) + 1;
          ppSVar2[5] = pSVar3;
          SSL_renegotiate(s);
        }
      }
      else {
        *(int *)(ppSVar2 + 1) = *(int *)(ppSVar2 + 1) + 1;
        ppSVar2[3] = (SSL *)0x0;
        SSL_renegotiate(s);
      }
      break;
    default:
      iVar1 = 0;
      break;
    case 2:
      BIO_set_flags(param_1,9);
      iVar1 = 0;
      break;
    case 3:
      BIO_set_flags(param_1,10);
      iVar1 = 0;
      break;
    case 4:
      BIO_set_flags(param_1,0xc);
      iVar1 = 1;
      break;
    case 7:
      BIO_set_flags(param_1,0xc);
      iVar1 = 2;
    }
    BIO_set_retry_reason(param_1,iVar1);
  }
  return ret_code;
}