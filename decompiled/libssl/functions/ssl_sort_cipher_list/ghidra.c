void ssl_sort_cipher_list(void)

{
  qsort(tls13_ciphers,5,0x50,cipher_compare);
  qsort(ssl3_ciphers,0xa7,0x50,cipher_compare);
  qsort(ssl3_scsvs,2,0x50,cipher_compare);
  return;
}