undefined2 SSL_CONF_cmd_value_type(uint *param_1,char *param_2)

{
  uint uVar1;
  char *__s2;
  int iVar2;
  size_t sVar3;
  long lVar4;
  ulong __n;
  
  if (param_2 == (char *)0x0) {
    return 0;
  }
  __s2 = *(char **)(param_1 + 2);
  if (__s2 == (char *)0x0) {
    if ((*(byte *)param_1 & 1) != 0) {
      if (*param_2 != '-') {
        return 0;
      }
      if (param_2[1] == '\0') {
        return 0;
      }
      param_2 = param_2 + 1;
    }
  }
  else {
    sVar3 = strlen(param_2);
    __n = *(ulong *)(param_1 + 4);
    if (sVar3 <= __n) {
      return 0;
    }
    uVar1 = *param_1;
    if (((uVar1 & 1) != 0) && (iVar2 = strncmp(param_2,__s2,__n), iVar2 != 0)) {
      return 0;
    }
    if ((uVar1 & 2) != 0) {
      iVar2 = OPENSSL_strncasecmp(param_2,__s2,__n);
      if (iVar2 != 0) {
        return 0;
      }
      __n = *(ulong *)(param_1 + 4);
    }
    param_2 = param_2 + __n;
  }
  lVar4 = ssl_conf_cmd_lookup_part_0(param_1,param_2);
  if (lVar4 == 0) {
    return 0;
  }
  return *(undefined2 *)(lVar4 + 0x1a);
}