undefined8 ssl_set_client_disabled(long param_1)

{
  byte bVar1;
  int iVar2;
  undefined8 uVar3;
  
  *(undefined8 *)(param_1 + 0x3dc) = 0;
  ssl_set_sig_mask(param_1 + 0x3e0,param_1,0x5000e);
  iVar2 = ssl_get_min_max_version(param_1,param_1 + 0x3e4,param_1 + 1000,0);
  uVar3 = 0;
  if (iVar2 == 0) {
    if (*(long *)(param_1 + 0x988) == 0) {
      *(ulong *)(param_1 + 0x3dc) = *(ulong *)(param_1 + 0x3dc) | 0x10000001c8;
      bVar1 = *(byte *)(param_1 + 0xc48);
    }
    else {
      bVar1 = *(byte *)(param_1 + 0xc48);
    }
    uVar3 = 1;
    if ((bVar1 & 0x20) == 0) {
      uVar3 = 1;
      *(ulong *)(param_1 + 0x3dc) = *(ulong *)(param_1 + 0x3dc) | 0x4000000020;
    }
  }
  return uVar3;
}