bool ssl_cert_is_disabled(long param_1,undefined8 param_2)

{
  long lVar1;
  
  lVar1 = ssl_cert_lookup_by_idx(param_2);
  if (lVar1 != 0) {
    return (*(uint *)(param_1 + 0x64c) & *(uint *)(lVar1 + 4)) != 0;
  }
  return true;
}