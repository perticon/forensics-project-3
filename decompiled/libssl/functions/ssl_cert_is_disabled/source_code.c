int ssl_cert_is_disabled(SSL_CTX *ctx, size_t idx)
{
    const SSL_CERT_LOOKUP *cl = ssl_cert_lookup_by_idx(idx);

    if (cl == NULL || (cl->amask & ctx->disabled_auth_mask) != 0)
        return 1;
    return 0;
}