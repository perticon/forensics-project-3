bool dtls1_new(long param_1)

{
  int iVar1;
  void *ptr;
  pqueue p_Var2;
  
  iVar1 = DTLS_RECORD_LAYER_new(param_1 + 0xc58);
  if (iVar1 != 0) {
    iVar1 = ssl3_new(param_1);
    if (iVar1 != 0) {
      ptr = (void *)CRYPTO_zalloc(0x210,"ssl/d1_lib.c",0x4e);
      if (ptr != (void *)0x0) {
        p_Var2 = pqueue_new();
        *(pqueue *)((long)ptr + 0x118) = p_Var2;
        p_Var2 = pqueue_new();
        iVar1 = *(int *)(param_1 + 0x38);
        *(pqueue *)((long)ptr + 0x120) = p_Var2;
        if (iVar1 != 0) {
          *(undefined8 *)((long)ptr + 0x100) = 0xff;
        }
        *(undefined (*) [16])((long)ptr + 0x128) = (undefined  [16])0x0;
        if ((p_Var2 != (pqueue)0x0) && (*(pqueue *)((long)ptr + 0x118) != (pqueue)0x0)) {
          *(void **)(param_1 + 0x4b8) = ptr;
          iVar1 = (**(code **)(*(long *)(param_1 + 8) + 0x18))(0,param_1);
          return iVar1 != 0;
        }
        pqueue_free(*(pqueue *)((long)ptr + 0x118));
        pqueue_free(*(pqueue *)((long)ptr + 0x120));
        CRYPTO_free(ptr);
      }
      ssl3_free(param_1);
    }
  }
  return false;
}