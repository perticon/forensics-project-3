int do_dtls1_write(undefined4 *param_1,undefined4 param_2,undefined *param_3,size_t param_4,
                  int param_5,size_t *param_6)

{
  undefined *puVar1;
  undefined *puVar2;
  uint uVar3;
  int iVar4;
  int iVar5;
  long lVar6;
  undefined8 uVar7;
  code **ppcVar8;
  long lVar9;
  long in_FS_OFFSET;
  int local_a4;
  undefined local_98 [4];
  undefined4 local_94;
  size_t local_90;
  undefined *local_78;
  undefined *puStack112;
  long local_40;
  
  lVar9 = *(long *)(param_1 + 0x332);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (lVar9 != 0) {
    ERR_new();
    ERR_set_debug("ssl/record/rec_layer_d1.c",0x32a,"do_dtls1_write");
    uVar7 = 0xc0103;
LAB_00155974:
    iVar4 = 0;
    ossl_statem_fatal(param_1,0x50,uVar7,0);
    goto LAB_00155635;
  }
  if (((param_1[0x69] != 0) && (iVar4 = (**(code **)(*(long *)(param_1 + 2) + 0x78))(), iVar4 < 1))
     || ((param_4 == 0 && (iVar4 = 0, param_5 == 0)))) goto LAB_00155635;
  uVar3 = ssl_get_max_send_fragment(param_1);
  if (uVar3 < param_4) {
    ERR_new();
    ERR_set_debug("ssl/record/rec_layer_d1.c",0x33a,"do_dtls1_write");
    uVar7 = 0xc2;
    goto LAB_00155974;
  }
  if (((*(long *)(param_1 + 0x246) == 0) || (*(long *)(param_1 + 0x21e) == 0)) ||
     (lVar6 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x224)), lVar6 == 0)) {
    iVar4 = 0;
LAB_00155718:
    puVar2 = *(undefined **)(param_1 + 0x32a);
    *puVar2 = (char)param_2;
    if ((**(int **)(param_1 + 2) == 0x1ffff) && (param_1[0x27e] != 0x100)) {
      *(undefined2 *)(puVar2 + 1) = 0xfffe;
    }
    else {
      puVar2[1] = (char)((uint)*param_1 >> 8);
      puVar2[2] = (char)*param_1;
    }
    puVar1 = puVar2 + 0xd;
    local_94 = param_2;
    if (*(long *)(param_1 + 0x21e) == 0) {
LAB_00155990:
      local_a4 = 0;
      local_78 = puVar2 + 0xd;
    }
    else {
      uVar7 = EVP_CIPHER_CTX_get0_cipher();
      iVar5 = EVP_CIPHER_get_mode(uVar7);
      if (iVar5 == 2) {
        local_a4 = EVP_CIPHER_CTX_get_iv_length(*(undefined8 *)(param_1 + 0x21e));
        if (local_a4 < 2) goto LAB_00155990;
        lVar9 = (long)local_a4;
        local_78 = puVar2 + lVar9 + 0xd;
      }
      else {
        if (1 < iVar5 - 6U) goto LAB_00155990;
        local_a4 = 8;
        local_78 = puVar2 + 0x15;
        lVar9 = 8;
      }
    }
    local_90 = param_4;
    puStack112 = param_3;
    if (*(long *)(param_1 + 0x21a) == 0) {
      memcpy(local_78,param_3,param_4);
      puStack112 = local_78;
    }
    else {
      iVar5 = ssl3_do_compress();
      if (iVar5 == 0) {
        ERR_new();
        ERR_set_debug("ssl/record/rec_layer_d1.c",0x386,"do_dtls1_write");
        uVar7 = 0x8d;
        goto LAB_001556f3;
      }
    }
    ppcVar8 = *(code ***)(*(long *)(param_1 + 2) + 0xc0);
    if (((*(byte *)((long)param_1 + 0xa9) & 4) == 0) && (iVar4 != 0)) {
      iVar5 = (*ppcVar8[1])(param_1,local_98,puVar1 + local_90 + lVar9);
      if (iVar5 == 0) {
        ERR_new();
        ERR_set_debug("ssl/record/rec_layer_d1.c",0x399,"do_dtls1_write");
        uVar7 = 0xc0103;
        goto LAB_001556f3;
      }
      local_90 = local_90 + (long)iVar4;
      ppcVar8 = *(code ***)(*(long *)(param_1 + 2) + 0xc0);
    }
    if (local_a4 != 0) {
      local_90 = local_90 + lVar9;
    }
    local_78 = puVar1;
    puStack112 = puVar1;
    iVar5 = (**ppcVar8)(param_1,local_98,1,1,0);
    if (iVar5 < 1) {
      iVar4 = -1;
      iVar5 = ossl_statem_in_error(param_1,local_98);
      if (iVar5 == 0) {
        ERR_new();
        ERR_set_debug("ssl/record/rec_layer_d1.c",0x3a8,"do_dtls1_write");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
      goto LAB_00155635;
    }
    if (((*(byte *)((long)param_1 + 0xa9) & 4) != 0) && (iVar4 != 0)) {
      iVar5 = (**(code **)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 8))
                        (param_1,local_98,puVar2 + local_90 + 0xd,1);
      if (iVar5 == 0) {
        ERR_new();
        ERR_set_debug("ssl/record/rec_layer_d1.c",0x3b0,"do_dtls1_write");
        uVar7 = 0xc0103;
        goto LAB_001556f3;
      }
      local_90 = local_90 + (long)iVar4;
    }
    puVar2[3] = *(undefined *)(*(long *)(param_1 + 0x744) + 3);
    puVar2[4] = (char)*(undefined2 *)(*(long *)(param_1 + 0x744) + 2);
    *(undefined4 *)(puVar2 + 5) = *(undefined4 *)((long)param_1 + 0x1d02);
    *(undefined2 *)(puVar2 + 9) = *(undefined2 *)((long)param_1 + 0x1d06);
    puVar2[0xb] = (char)(local_90 >> 8);
    puVar2[0xc] = (char)local_90;
    if (*(code **)(param_1 + 0x130) != (code *)0x0) {
      (**(code **)(param_1 + 0x130))(1,0,0x100,puVar2,0xd,param_1,*(undefined8 *)(param_1 + 0x132));
    }
    local_90 = local_90 + 0xd;
    local_94 = param_2;
    ssl3_record_sequence_update(param_1 + 0x740);
    if (param_5 == 0) {
      *(size_t *)(param_1 + 0x332) = local_90;
      *(size_t *)(param_1 + 0x736) = param_4;
      *(undefined8 *)(param_1 + 0x330) = 0;
      *(undefined **)(param_1 + 0x73c) = param_3;
      param_1[0x738] = param_2;
      *(size_t *)(param_1 + 0x73a) = param_4;
      iVar4 = ssl3_write_pending(param_1,param_2,param_3,param_4,param_6);
    }
    else {
      iVar4 = 1;
      *param_6 = local_90;
    }
  }
  else {
    uVar7 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x224));
    iVar4 = EVP_MD_get_size(uVar7);
    if (-1 < iVar4) goto LAB_00155718;
    ERR_new();
    ERR_set_debug("ssl/record/rec_layer_d1.c",0x34a,"do_dtls1_write");
    uVar7 = 0xc2;
LAB_001556f3:
    iVar4 = -1;
    ossl_statem_fatal(param_1,0x50,uVar7,0);
  }
LAB_00155635:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}