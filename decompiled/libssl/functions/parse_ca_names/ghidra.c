undefined8 parse_ca_names(long param_1,ushort **param_2)

{
  ushort uVar1;
  int iVar2;
  long lVar3;
  X509_NAME *a;
  ushort *puVar4;
  ushort *puVar5;
  ushort *puVar6;
  undefined8 uVar7;
  long in_FS_OFFSET;
  ushort *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  lVar3 = OPENSSL_sk_new();
  if (lVar3 == 0) {
    ERR_new();
    a = (X509_NAME *)0x0;
    ERR_set_debug("ssl/statem/statem_lib.c",0x8d0,"parse_ca_names");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
  }
  else {
    if ((ushort *)0x1 < param_2[1]) {
      puVar4 = param_2[1] + -1;
      uVar1 = **param_2;
      puVar5 = (ushort *)(ulong)(ushort)(uVar1 << 8 | uVar1 >> 8);
      if (puVar5 <= puVar4) {
        puVar6 = *param_2 + 1;
        param_2[1] = (ushort *)((long)puVar4 - (long)puVar5);
        *param_2 = (ushort *)((long)puVar6 + (long)puVar5);
        do {
          if (puVar5 == (ushort *)0x0) {
            OPENSSL_sk_pop_free(*(undefined8 *)(param_1 + 0x308),PTR_X509_NAME_free_001a7fc8);
            *(long *)(param_1 + 0x308) = lVar3;
            uVar7 = 1;
            goto LAB_00177daf;
          }
          if (puVar5 == (ushort *)0x1) {
LAB_00177d46:
            ERR_new();
            uVar7 = 0x8df;
            goto LAB_00177d71;
          }
          puVar4 = (ushort *)(ulong)(ushort)(*puVar6 << 8 | *puVar6 >> 8);
          if (puVar5 + -1 < puVar4) goto LAB_00177d46;
          local_48 = puVar6 + 1;
          puVar5 = (ushort *)((long)(puVar5 + -1) - (long)puVar4);
          puVar6 = (ushort *)((long)local_48 + (long)puVar4);
          a = d2i_X509_NAME((X509_NAME **)0x0,(uchar **)&local_48,(long)puVar4);
          if (a == (X509_NAME *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x8e5,"parse_ca_names");
            ossl_statem_fatal(param_1,0x32,0x8000d,0);
            goto LAB_00177d96;
          }
          if (local_48 != puVar6) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x8e9,"parse_ca_names");
            ossl_statem_fatal(param_1,0x32,0x83,0);
            goto LAB_00177d96;
          }
          iVar2 = OPENSSL_sk_push(lVar3,a);
        } while (iVar2 != 0);
        ERR_new();
        ERR_set_debug("ssl/statem/statem_lib.c",0x8ee,"parse_ca_names");
        ossl_statem_fatal(param_1,0x50,0xc0100,0);
        goto LAB_00177d96;
      }
    }
    ERR_new();
    uVar7 = 0x8d5;
LAB_00177d71:
    a = (X509_NAME *)0x0;
    ERR_set_debug("ssl/statem/statem_lib.c",uVar7,"parse_ca_names");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
  }
LAB_00177d96:
  OPENSSL_sk_pop_free(lVar3,PTR_X509_NAME_free_001a7fc8);
  X509_NAME_free(a);
  uVar7 = 0;
LAB_00177daf:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}