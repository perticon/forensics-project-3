long ossl_statem_client_max_message_size(int *param_1)

{
  int iVar1;
  
  switch(param_1[0x17]) {
  default:
    return 0;
  case 2:
    return 0x102;
  case 3:
    return 0x10047;
  case 4:
  case 7:
    return *(long *)(param_1 + 0x280);
  case 5:
  case 0x27:
    return 0x4000;
  case 6:
    return 0x19000;
  case 9:
    break;
  case 10:
    return (ulong)(*param_1 == 0x100) * 2 + 1;
  case 0xb:
    return 0x40;
  case 0x26:
    return 20000;
  case 0x2d:
    return 1;
  }
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
      (iVar1 = **(int **)(param_1 + 2), 0x303 < iVar1)) && (iVar1 != 0x10000)) {
    return 0x2010a;
  }
  return 0x10005;
}