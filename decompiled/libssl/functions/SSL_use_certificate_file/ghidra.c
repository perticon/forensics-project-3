int SSL_use_certificate_file(SSL *ssl,char *file,int type)

{
  undefined8 *puVar1;
  int iVar2;
  BIO_METHOD *type_00;
  BIO *bp;
  long lVar3;
  X509 *pXVar4;
  undefined8 uVar5;
  long in_FS_OFFSET;
  X509 *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = (X509 *)0x0;
  type_00 = BIO_s_file();
  bp = BIO_new(type_00);
  if (bp == (BIO *)0x0) {
    ERR_new();
    iVar2 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x39,"SSL_use_certificate_file");
    ERR_set_error(0x14,0x80007,0);
  }
  else {
    lVar3 = BIO_ctrl(bp,0x6c,3,file);
    if ((int)lVar3 < 1) {
      ERR_new();
      iVar2 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",0x3e,"SSL_use_certificate_file");
      ERR_set_error(0x14,0x80002,0);
    }
    else if (type - 1U < 2) {
      puVar1 = *(undefined8 **)&ssl[3].ex_data.dummy;
      local_38 = (X509 *)X509_new_ex(*puVar1,puVar1[0x88]);
      if (local_38 == (X509 *)0x0) {
        ERR_new();
        iVar2 = 0;
        ERR_set_debug("ssl/ssl_rsa.c",0x48,"SSL_use_certificate_file");
        ERR_set_error(0x14,0xc0100,0);
      }
      else {
        if (type == 2) {
          uVar5 = 0x8000d;
          pXVar4 = d2i_X509_bio(bp,&local_38);
        }
        else {
          uVar5 = 0x80009;
          pXVar4 = PEM_read_bio_X509(bp,&local_38,ssl[10].tlsext_ellipticcurvelist,
                                     ssl[10].tlsext_opaque_prf_input);
        }
        if (pXVar4 == (X509 *)0x0) {
          ERR_new();
          iVar2 = 0;
          ERR_set_debug("ssl/ssl_rsa.c",0x58,"SSL_use_certificate_file");
          ERR_set_error(0x14,uVar5,0);
        }
        else {
          iVar2 = SSL_use_certificate(ssl,local_38);
        }
      }
    }
    else {
      ERR_new();
      iVar2 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",0x43,"SSL_use_certificate_file");
      ERR_set_error(0x14,0x7c,0);
    }
  }
  X509_free(local_38);
  BIO_free(bp);
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}