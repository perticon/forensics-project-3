int SSL_set_rfd(SSL *s,int fd)

{
  int iVar1;
  BIO *pBVar2;
  BIO_METHOD *type;
  long lVar3;
  
  pBVar2 = SSL_get_wbio(s);
  if (((pBVar2 != (BIO *)0x0) && (iVar1 = BIO_method_type(pBVar2), iVar1 == 0x505)) &&
     (lVar3 = BIO_ctrl(pBVar2,0x69,0,(void *)0x0), fd == (int)lVar3)) {
    BIO_up_ref(pBVar2);
    SSL_set0_rbio(s,pBVar2);
    return 1;
  }
  type = BIO_s_socket();
  pBVar2 = BIO_new(type);
  if (pBVar2 != (BIO *)0x0) {
    BIO_int_ctrl(pBVar2,0x68,0,fd);
    SSL_set0_rbio(s,pBVar2);
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x5a3,"SSL_set_rfd");
  ERR_set_error(0x14,0x80007,0);
  return 0;
}