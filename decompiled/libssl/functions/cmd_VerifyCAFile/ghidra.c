bool cmd_VerifyCAFile(long param_1,long param_2)

{
  undefined8 *puVar1;
  int iVar2;
  undefined8 *puVar3;
  X509_STORE *pXVar4;
  long lVar5;
  undefined8 uVar6;
  
  puVar3 = *(undefined8 **)(param_1 + 0x18);
  if (puVar3 == (undefined8 *)0x0) {
    lVar5 = *(long *)(param_1 + 0x20);
    if (lVar5 == 0) {
      return true;
    }
    puVar3 = *(undefined8 **)(lVar5 + 0x9a8);
    lVar5 = *(long *)(lVar5 + 0x898);
    if (puVar3 == (undefined8 *)0x0) {
      pXVar4 = *(X509_STORE **)(lVar5 + 0x1d0);
      uVar6 = 0;
      puVar1 = puVar3;
      goto joined_r0x00134369;
    }
  }
  else {
    lVar5 = puVar3[0x2b];
  }
  pXVar4 = *(X509_STORE **)(lVar5 + 0x1d0);
  puVar1 = (undefined8 *)*puVar3;
  uVar6 = puVar3[0x88];
joined_r0x00134369:
  if (pXVar4 == (X509_STORE *)0x0) {
    pXVar4 = X509_STORE_new();
    *(X509_STORE **)(lVar5 + 0x1d0) = pXVar4;
    if (pXVar4 == (X509_STORE *)0x0) {
      return false;
    }
  }
  if (param_2 != 0) {
    iVar2 = X509_STORE_load_file_ex(pXVar4,param_2,puVar1,uVar6);
    return iVar2 != 0;
  }
  return true;
}