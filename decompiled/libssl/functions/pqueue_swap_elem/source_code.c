static ossl_inline void pqueue_swap_elem(OSSL_PQUEUE *pq, size_t i, size_t j)
{
    struct pq_heap_st *h = pq->heap, t_h;
    struct pq_elem_st *e = pq->elements;

    ASSERT_USED(pq, i);
    ASSERT_USED(pq, j);

    t_h = h[i];
    h[i] = h[j];
    h[j] = t_h;

    e[h[i].index].posn = i;
    e[h[j].index].posn = j;
}