static ossl_inline void pqueue_move_up(OSSL_PQUEUE *pq, size_t n)
{
    struct pq_heap_st *h = pq->heap;
    size_t p = n * 2 + 1;

    ASSERT_USED(pq, n);
    if (pq->htop > p + 1) {
        ASSERT_USED(pq, p);
        ASSERT_USED(pq, p + 1);
        if (pq->compare(h[p].data, h[p + 1].data) > 0)
            p++;
    }
    while (pq->htop > p && pq->compare(h[p].data, h[n].data) < 0) {
        ASSERT_USED(pq, p);
        pqueue_swap_elem(pq, n, p);
        n = p;
        p = n * 2 + 1;
        if (pq->htop > p + 1) {
            ASSERT_USED(pq, p + 1);
            if (pq->compare(h[p].data, h[p + 1].data) > 0)
                p++;
        }
    }
}