undefined8 should_add_extension(long param_1,uint param_2,uint param_3,int param_4)

{
  int iVar1;
  
  if ((param_2 & param_3) == 0) {
    return 0;
  }
  iVar1 = extension_is_relevant();
  if ((iVar1 != 0) &&
     ((((param_2 & 0x20) == 0 || ((param_3 & 0x80) == 0)) ||
      (((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0 && (0x303 < param_4)))
      ))) {
    return 1;
  }
  return 0;
}