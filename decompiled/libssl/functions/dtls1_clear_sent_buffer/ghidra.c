void dtls1_clear_sent_buffer(long param_1)

{
  pitem *item;
  
  while( true ) {
    item = pqueue_pop(*(pqueue *)(*(long *)(param_1 + 0x4b8) + 0x120));
    if (item == (pitem *)0x0) break;
    dtls1_hm_fragment_free(item->data);
    pitem_free(item);
  }
  return;
}