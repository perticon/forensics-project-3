undefined8 tls_construct_ctos_psk_kex_modes(long param_1,undefined8 param_2)

{
  ulong uVar1;
  int iVar2;
  
  uVar1 = *(ulong *)(param_1 + 0x9e8);
  iVar2 = WPACKET_put_bytes__(param_2,0x2d,2);
  if (((((iVar2 != 0) && (iVar2 = WPACKET_start_sub_packet_len__(param_2,2), iVar2 != 0)) &&
       (iVar2 = WPACKET_start_sub_packet_len__(param_2,1), iVar2 != 0)) &&
      ((iVar2 = WPACKET_put_bytes__(param_2,1,1), iVar2 != 0 &&
       (((uVar1 & 0x400) == 0 || (iVar2 = WPACKET_put_bytes__(param_2,0,1), iVar2 != 0)))))) &&
     ((iVar2 = WPACKET_close(param_2), iVar2 != 0 && (iVar2 = WPACKET_close(param_2), iVar2 != 0))))
  {
    if ((uVar1 & 0x400) == 0) {
      *(undefined4 *)(param_1 + 0xb28) = 2;
      return 1;
    }
    *(undefined4 *)(param_1 + 0xb28) = 3;
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x253,"tls_construct_ctos_psk_kex_modes");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}