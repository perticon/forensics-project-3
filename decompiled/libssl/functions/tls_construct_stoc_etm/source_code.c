EXT_RETURN tls_construct_stoc_etm(SSL *s, WPACKET *pkt, unsigned int context,
                                  X509 *x, size_t chainidx)
{
    if (!s->ext.use_etm)
        return EXT_RETURN_NOT_SENT;

    /*
     * Don't use encrypt_then_mac if AEAD or RC4 might want to disable
     * for other cases too.
     */
    if (s->s3.tmp.new_cipher->algorithm_mac == SSL_AEAD
        || s->s3.tmp.new_cipher->algorithm_enc == SSL_RC4
        || s->s3.tmp.new_cipher->algorithm_enc == SSL_eGOST2814789CNT
        || s->s3.tmp.new_cipher->algorithm_enc == SSL_eGOST2814789CNT12
        || s->s3.tmp.new_cipher->algorithm_enc == SSL_MAGMA
        || s->s3.tmp.new_cipher->algorithm_enc == SSL_KUZNYECHIK) {
        s->ext.use_etm = 0;
        return EXT_RETURN_NOT_SENT;
    }

    if (!WPACKET_put_bytes_u16(pkt, TLSEXT_TYPE_encrypt_then_mac)
            || !WPACKET_put_bytes_u16(pkt, 0)) {
        SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
        return EXT_RETURN_FAIL;
    }

    return EXT_RETURN_SENT;
}