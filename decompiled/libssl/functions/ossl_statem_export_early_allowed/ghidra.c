bool ossl_statem_export_early_allowed(long param_1)

{
  bool bVar1;
  
  bVar1 = true;
  if (*(int *)(param_1 + 0xb30) != 2) {
    bVar1 = *(int *)(param_1 + 0x38) == 0 && *(int *)(param_1 + 0xb30) != 0;
  }
  return bVar1;
}