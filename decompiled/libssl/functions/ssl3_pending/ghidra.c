long ssl3_pending(long param_1)

{
  long lVar1;
  int *piVar2;
  long lVar3;
  
  lVar3 = 0;
  if ((*(int *)(param_1 + 0xc64) != 0xf1) && (*(long *)(param_1 + 0xc68) != 0)) {
    piVar2 = (int *)(param_1 + 0x12ac);
    lVar1 = 0;
    do {
      if (*piVar2 != 0x17) {
        return 0;
      }
      lVar1 = lVar1 + 1;
      lVar3 = lVar3 + *(long *)(piVar2 + 1);
      piVar2 = piVar2 + 0x14;
    } while (lVar1 != *(long *)(param_1 + 0xc68));
  }
  return lVar3;
}