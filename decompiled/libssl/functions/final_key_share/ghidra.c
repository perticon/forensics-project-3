uint final_key_share(long param_1,uint param_2,uint param_3)

{
  undefined2 uVar1;
  int iVar2;
  undefined8 uVar3;
  int iVar4;
  ulong uVar5;
  char cVar6;
  undefined8 uVar7;
  uint uVar8;
  long in_FS_OFFSET;
  long local_40;
  ulong local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
       (iVar4 = **(int **)(param_1 + 8), 0x303 < iVar4)) && (iVar4 != 0x10000)) &&
     ((param_2 & 0x800) == 0)) {
    uVar8 = param_3 | *(uint *)(param_1 + 0x38);
    if (uVar8 == 0) {
      if ((*(int *)(param_1 + 0x4d0) == 0) || ((*(byte *)(param_1 + 0xb28) & 1) == 0)) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions.c",0x517,"final_key_share");
        ossl_statem_fatal(param_1,0x6d,0x65,0);
        goto LAB_0015e7e6;
      }
LAB_0015e7c4:
      uVar8 = tls13_generate_handshake_secret(param_1,0,0);
      if (uVar8 != 0) goto LAB_0015e7e0;
      ERR_new();
      uVar7 = 0x595;
    }
    else {
      if (*(uint *)(param_1 + 0x38) == 0) {
        if (param_3 != 0) goto LAB_0015e7e0;
        goto LAB_0015e7c4;
      }
      iVar4 = *(int *)(param_1 + 0x8e8);
      if (*(long *)(param_1 + 0x4b0) == 0) {
        iVar2 = *(int *)(param_1 + 0x4d0);
        if (iVar4 == 0) {
          if (param_3 == 0) {
            if ((iVar2 != 0) && ((*(byte *)(param_1 + 0xb28) & 1) != 0)) {
              if (((*(byte *)(param_1 + 0xa9) & 8) != 0) && (*(int *)(param_1 + 0xb48) == 0))
              goto LAB_0015e845;
              goto LAB_0015e7e0;
            }
            ERR_new();
            ERR_set_debug("ssl/statem/extensions.c",0x572,"final_key_share");
            cVar6 = 'm';
            goto LAB_0015ea45;
          }
          if ((iVar2 == 0) || (uVar8 = *(uint *)(param_1 + 0xb28), (uVar8 & 2) != 0)) {
            uVar7 = *(undefined8 *)(param_1 + 0xad0);
            uVar3 = *(undefined8 *)(param_1 + 0xad8);
            tls1_get_supported_groups(param_1,&local_40,&local_38);
            if (local_38 != 0) {
              uVar5 = 0;
              do {
                uVar1 = *(undefined2 *)(local_40 + uVar5 * 2);
                iVar4 = check_in_list(param_1,uVar1,uVar3,uVar7,2);
                if (iVar4 != 0) {
                  if (uVar5 < local_38) {
                    *(undefined2 *)(param_1 + 0x4ae) = uVar1;
                    goto LAB_0015e845;
                  }
                  break;
                }
                uVar5 = uVar5 + 1;
              } while (uVar5 < local_38);
            }
            if (*(int *)(param_1 + 0x4d0) != 0) {
              uVar8 = *(uint *)(param_1 + 0xb28);
              goto LAB_0015ea84;
            }
          }
          else {
LAB_0015ea84:
            if ((uVar8 & 1) != 0) {
              iVar4 = *(int *)(param_1 + 0x8e8);
              goto LAB_0015e9ad;
            }
          }
          ERR_new();
          ERR_set_debug("ssl/statem/extensions.c",0x572,"final_key_share");
          cVar6 = '(';
        }
        else {
          if ((iVar2 != 0) && ((*(byte *)(param_1 + 0xb28) & 1) != 0)) {
LAB_0015e9ad:
            if (((*(byte *)(param_1 + 0xa9) & 8) != 0) &&
               (uVar8 = *(uint *)(param_1 + 0xb48), uVar8 == 0)) {
              if (iVar4 == 0) goto LAB_0015e845;
              ERR_new();
              uVar7 = 0x580;
              goto LAB_0015e8c9;
            }
            goto LAB_0015e858;
          }
          ERR_new();
          ERR_set_debug("ssl/statem/extensions.c",0x572,"final_key_share");
          cVar6 = (-(param_3 == 0) & 0x45U) + 0x28;
        }
LAB_0015ea45:
        ossl_statem_fatal(param_1,cVar6,0x65,0);
        uVar8 = 0;
        goto LAB_0015e7e6;
      }
      if (((*(byte *)(param_1 + 0xa9) & 8) == 0) || (uVar8 = *(uint *)(param_1 + 0xb48), uVar8 != 0)
         ) {
LAB_0015e858:
        if (iVar4 == 1) {
          *(undefined4 *)(param_1 + 0x8e8) = 2;
        }
        goto LAB_0015e7e0;
      }
      if (iVar4 == 0) {
LAB_0015e845:
        *(undefined4 *)(param_1 + 0x8e8) = 1;
        goto LAB_0015e7e0;
      }
      ERR_new();
      uVar7 = 0x547;
    }
LAB_0015e8c9:
    ERR_set_debug("ssl/statem/extensions.c",uVar7,"final_key_share");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
LAB_0015e7e0:
    uVar8 = 1;
  }
LAB_0015e7e6:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar8;
}