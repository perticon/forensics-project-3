static void ssl_set_option(SSL_CONF_CTX *cctx, unsigned int name_flags,
                           uint64_t option_value, int onoff)
{
    uint32_t *pflags;

    if (cctx->poptions == NULL)
        return;
    if (name_flags & SSL_TFLAG_INV)
        onoff ^= 1;
    switch (name_flags & SSL_TFLAG_TYPE_MASK) {

    case SSL_TFLAG_CERT:
        pflags = cctx->pcert_flags;
        break;

    case SSL_TFLAG_VFY:
        pflags = cctx->pvfy_flags;
        break;

    case SSL_TFLAG_OPTION:
        if (onoff)
            *cctx->poptions |= option_value;
        else
            *cctx->poptions &= ~option_value;
        return;

    default:
        return;

    }
    if (onoff)
        *pflags |= option_value;
    else
        *pflags &= ~option_value;
}