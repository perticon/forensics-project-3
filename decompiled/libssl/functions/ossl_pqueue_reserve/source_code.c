int ossl_pqueue_reserve(OSSL_PQUEUE *pq, size_t n)
{
    size_t new_max, cur_max;
    struct pq_heap_st *h;
    struct pq_elem_st *e;

    if (pq == NULL)
        return 0;
    cur_max = pq->hmax;
    if (pq->htop + n < cur_max)
        return 1;

    new_max = compute_pqueue_growth(n + cur_max, cur_max);
    if (new_max == 0) {
        ERR_raise(ERR_LIB_SSL, ERR_R_INTERNAL_ERROR);
        return 0;
    }

    h = OPENSSL_realloc(pq->heap, new_max * sizeof(*pq->heap));
    if (h == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        return 0;
    }
    pq->heap = h;

    e = OPENSSL_realloc(pq->elements, new_max * sizeof(*pq->elements));
    if (e == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        return 0;
    }
    pq->elements = e;

    pq->hmax = new_max;
    pqueue_add_freelist(pq, cur_max);
    return 1;
}