long * ssl_cert_dup(long *param_1)

{
  void *__src;
  int iVar1;
  long *ptr;
  long lVar2;
  long lVar3;
  void *pvVar4;
  char *pcVar5;
  undefined8 uVar6;
  
  ptr = (long *)CRYPTO_zalloc(0x218,"ssl/ssl_cert.c",0x4d);
  if (ptr == (long *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_cert.c",0x51,"ssl_cert_dup");
    ERR_set_error(0x14,0xc0100,0);
    return (long *)0x0;
  }
  *(undefined4 *)(ptr + 0x41) = 1;
  *ptr = (long)ptr + (*param_1 - (long)param_1);
  lVar2 = CRYPTO_THREAD_lock_new();
  ptr[0x42] = lVar2;
  if (lVar2 == 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_cert.c",0x59,"ssl_cert_dup");
    ERR_set_error(0x14,0xc0100,0);
    CRYPTO_free(ptr);
    return (long *)0x0;
  }
  if (param_1[1] != 0) {
    ptr[1] = param_1[1];
    EVP_PKEY_up_ref();
  }
  lVar2 = 0x20;
  ptr[2] = param_1[2];
  *(undefined4 *)(ptr + 3) = *(undefined4 *)(param_1 + 3);
  do {
    if (*(long *)((long)param_1 + lVar2) != 0) {
      *(long *)((long)ptr + lVar2) = *(long *)((long)param_1 + lVar2);
      X509_up_ref();
    }
    lVar3 = *(long *)((long)param_1 + lVar2 + 8);
    if (lVar3 != 0) {
      *(long *)((long)ptr + lVar2 + 8) = lVar3;
      EVP_PKEY_up_ref(*(undefined8 *)((long)param_1 + lVar2 + 8));
    }
    if (*(long *)((long)param_1 + lVar2 + 0x10) != 0) {
      lVar3 = X509_chain_up_ref();
      *(long *)((long)ptr + lVar2 + 0x10) = lVar3;
      if (lVar3 != 0) goto LAB_0012d6c1;
      ERR_new();
      uVar6 = 0x76;
LAB_0012d951:
      ERR_set_debug("ssl/ssl_cert.c",uVar6,"ssl_cert_dup");
      ERR_set_error(0x14,0xc0100,0);
      goto LAB_0012d970;
    }
LAB_0012d6c1:
    if (*(long *)((long)param_1 + lVar2 + 0x18) != 0) {
      pvVar4 = CRYPTO_malloc((int)*(undefined8 *)((long)param_1 + lVar2 + 0x20),"ssl/ssl_cert.c",
                             0x7d);
      *(void **)((long)ptr + lVar2 + 0x18) = pvVar4;
      if (pvVar4 == (void *)0x0) {
        ERR_new();
        uVar6 = 0x7f;
        goto LAB_0012d951;
      }
      __src = *(void **)((long)param_1 + lVar2 + 0x18);
      *(undefined8 *)((long)ptr + lVar2 + 0x20) = *(undefined8 *)((long)param_1 + lVar2 + 0x20);
      memcpy(pvVar4,__src,*(size_t *)((long)param_1 + lVar2 + 0x20));
    }
    lVar2 = lVar2 + 0x28;
  } while (lVar2 != 0x188);
  if (param_1[0x33] == 0) {
    lVar2 = param_1[0x35];
    ptr[0x33] = 0;
    if (lVar2 != 0) goto LAB_0012d788;
LAB_0012d92a:
    ptr[0x35] = 0;
  }
  else {
    pvVar4 = CRYPTO_malloc((int)param_1[0x34] * 2,"ssl/ssl_cert.c",0x8a);
    ptr[0x33] = (long)pvVar4;
    if (pvVar4 == (void *)0x0) goto LAB_0012d970;
    memcpy(pvVar4,(void *)param_1[0x33],param_1[0x34] * 2);
    lVar2 = param_1[0x35];
    ptr[0x34] = param_1[0x34];
    if (lVar2 == 0) goto LAB_0012d92a;
LAB_0012d788:
    pvVar4 = CRYPTO_malloc((int)param_1[0x36] * 2,"ssl/ssl_cert.c",0x95);
    ptr[0x35] = (long)pvVar4;
    if (pvVar4 == (void *)0x0) goto LAB_0012d970;
    memcpy(pvVar4,(void *)param_1[0x35],param_1[0x36] * 2);
    ptr[0x36] = param_1[0x36];
  }
  if (param_1[0x31] != 0) {
    lVar2 = CRYPTO_memdup(param_1[0x31],param_1[0x32],"ssl/ssl_cert.c",0xa0);
    ptr[0x31] = lVar2;
    if (lVar2 == 0) goto LAB_0012d970;
    ptr[0x32] = param_1[0x32];
  }
  lVar2 = param_1[0x3a];
  *(undefined4 *)((long)ptr + 0x1c) = *(undefined4 *)((long)param_1 + 0x1c);
  ptr[0x37] = param_1[0x37];
  ptr[0x38] = param_1[0x38];
  if (lVar2 != 0) {
    X509_STORE_up_ref();
    ptr[0x3a] = param_1[0x3a];
  }
  if (param_1[0x39] != 0) {
    X509_STORE_up_ref();
    ptr[0x39] = param_1[0x39];
  }
  ptr[0x3d] = param_1[0x3d];
  *(undefined4 *)(ptr + 0x3e) = *(undefined4 *)(param_1 + 0x3e);
  ptr[0x3f] = param_1[0x3f];
  iVar1 = custom_exts_copy(ptr + 0x3b,param_1 + 0x3b);
  if (iVar1 == 0) {
LAB_0012d970:
    ssl_cert_free(ptr);
    return (long *)0x0;
  }
  if ((char *)param_1[0x40] != (char *)0x0) {
    pcVar5 = CRYPTO_strdup((char *)param_1[0x40],"ssl/ssl_cert.c",0xbd);
    ptr[0x40] = (long)pcVar5;
    if (pcVar5 == (char *)0x0) goto LAB_0012d970;
  }
  return ptr;
}