int ssl_load_groups(undefined8 *param_1)

{
  long lVar1;
  int iVar2;
  short *psVar3;
  void *__dest;
  long lVar4;
  long lVar5;
  undefined1 *puVar6;
  long in_FS_OFFSET;
  short local_58 [20];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  iVar2 = OSSL_PROVIDER_do_all(*param_1,discover_provider_groups,param_1);
  if (iVar2 != 0) {
    puVar6 = supported_groups_default;
    lVar1 = param_1[0xc6];
    lVar5 = 0;
    do {
      if (lVar1 != 0) {
        lVar4 = 0;
        psVar3 = (short *)(param_1[0xc5] + 0x1c);
        do {
          if (*psVar3 == *(short *)puVar6) {
            local_58[lVar5] = *(short *)puVar6;
            lVar5 = lVar5 + 1;
            break;
          }
          lVar4 = lVar4 + 1;
          psVar3 = psVar3 + 0x1c;
        } while (lVar4 != lVar1);
      }
      puVar6 = (undefined1 *)((long)puVar6 + 2);
    } while ((undefined3 *)puVar6 != &ecformats_default);
    iVar2 = 1;
    if (lVar5 != 0) {
      __dest = CRYPTO_malloc((int)(lVar5 * 2),"ssl/t1_lib.c",0x19a);
      param_1[0x51] = __dest;
      if (__dest == (void *)0x0) {
        ERR_new();
        iVar2 = 0;
        ERR_set_debug("ssl/t1_lib.c",0x19d,"ssl_load_groups");
        ERR_set_error(0x14,0xc0100,0);
      }
      else {
        memcpy(__dest,local_58,lVar5 * 2);
        param_1[0x52] = lVar5;
      }
    }
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}