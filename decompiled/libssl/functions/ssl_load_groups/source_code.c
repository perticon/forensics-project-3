int ssl_load_groups(SSL_CTX *ctx)
{
    size_t i, j, num_deflt_grps = 0;
    uint16_t tmp_supp_groups[OSSL_NELEM(supported_groups_default)];

    if (!OSSL_PROVIDER_do_all(ctx->libctx, discover_provider_groups, ctx))
        return 0;

    for (i = 0; i < OSSL_NELEM(supported_groups_default); i++) {
        for (j = 0; j < ctx->group_list_len; j++) {
            if (ctx->group_list[j].group_id == supported_groups_default[i]) {
                tmp_supp_groups[num_deflt_grps++] = ctx->group_list[j].group_id;
                break;
            }
        }
    }

    if (num_deflt_grps == 0)
        return 1;

    ctx->ext.supported_groups_default
        = OPENSSL_malloc(sizeof(uint16_t) * num_deflt_grps);

    if (ctx->ext.supported_groups_default == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        return 0;
    }

    memcpy(ctx->ext.supported_groups_default,
           tmp_supp_groups,
           num_deflt_grps * sizeof(tmp_supp_groups[0]));
    ctx->ext.supported_groups_default_len = num_deflt_grps;

    return 1;
}