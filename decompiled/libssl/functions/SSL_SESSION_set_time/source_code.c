long SSL_SESSION_set_time(SSL_SESSION *s, long t)
{
    time_t new_time = (time_t)t;

    if (s == NULL)
        return 0;
    if (s->owner != NULL) {
        if (!CRYPTO_THREAD_write_lock(s->owner->lock))
            return 0;
        s->time = new_time;
        ssl_session_calculate_timeout(s);
        SSL_SESSION_list_add(s->owner, s);
        CRYPTO_THREAD_unlock(s->owner->lock);
    } else {
        s->time = new_time;
        ssl_session_calculate_timeout(s);
    }
    return t;
}