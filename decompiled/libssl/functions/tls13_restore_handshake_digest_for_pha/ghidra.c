undefined8 tls13_restore_handshake_digest_for_pha(long param_1)

{
  int iVar1;
  
  if (*(EVP_MD_CTX **)(param_1 + 0xbc8) == (EVP_MD_CTX *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x968,"tls13_restore_handshake_digest_for_pha");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  }
  iVar1 = EVP_MD_CTX_copy_ex(*(EVP_MD_CTX **)(param_1 + 400),*(EVP_MD_CTX **)(param_1 + 0xbc8));
  if (iVar1 != 0) {
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_lib.c",0x96d,"tls13_restore_handshake_digest_for_pha");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}