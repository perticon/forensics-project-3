uint tls_process_initial_server_flight(long param_1)

{
  code *pcVar1;
  int iVar2;
  
  iVar2 = ssl3_check_cert_and_algorithm();
  if (iVar2 == 0) {
    return 0;
  }
  if (*(int *)(param_1 + 0xa60) != -1) {
    pcVar1 = *(code **)(*(long *)(param_1 + 0x9a8) + 0x250);
    if (pcVar1 != (code *)0x0) {
      iVar2 = (*pcVar1)(param_1,*(undefined8 *)(*(long *)(param_1 + 0x9a8) + 600));
      if (iVar2 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xa8a,"tls_process_initial_server_flight");
        ossl_statem_fatal(param_1,0x71,0x148,0);
        return 0;
      }
      if (iVar2 < 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xa8f,"tls_process_initial_server_flight");
        ossl_statem_fatal(param_1,0x50,0x131,0);
        return 0;
      }
    }
  }
  if ((*(long *)(param_1 + 0xb68) != 0) && (iVar2 = ssl_validate_ct(param_1), iVar2 == 0)) {
    return ~*(uint *)(param_1 + 0x968) & 1;
  }
  return 1;
}