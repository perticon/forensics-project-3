void ossl_pqueue_pop_free(OSSL_PQUEUE *pq, void (*freefunc)(void *))
{
    size_t i;

    if (pq != NULL) {
        for (i = 0; i < pq->htop; i++)
            (*freefunc)(pq->heap[i].data);
        ossl_pqueue_free(pq);
    }
}