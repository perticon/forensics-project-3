undefined4 tls_parse_stoc_ec_pt_formats(long param_1,byte **param_2)

{
  byte *pbVar1;
  int iVar2;
  byte *pbVar3;
  undefined8 *puVar4;
  long lVar5;
  ulong uVar6;
  undefined8 *puVar7;
  uint num;
  byte bVar8;
  
  bVar8 = 0;
  if (param_2[1] != (byte *)0x0) {
    pbVar3 = *param_2;
    pbVar1 = param_2[1] + -1;
    if (pbVar1 == (byte *)(ulong)*pbVar3) {
      iVar2 = *(int *)(param_1 + 0x4d0);
      param_2[1] = (byte *)0x0;
      *param_2 = pbVar3 + 1 + (long)pbVar1;
      if (iVar2 != 0) {
        return 1;
      }
      if (pbVar1 == (byte *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x546,"tls_parse_stoc_ec_pt_formats");
        ossl_statem_fatal(param_1,0x32,0x10f,0);
        return 0;
      }
      *(undefined8 *)(param_1 + 0xab0) = 0;
      CRYPTO_free(*(void **)(param_1 + 0xab8));
      num = (uint)pbVar1;
      puVar4 = (undefined8 *)CRYPTO_malloc(num,"ssl/statem/extensions_clnt.c",0x54c);
      *(undefined8 **)(param_1 + 0xab8) = puVar4;
      if (puVar4 == (undefined8 *)0x0) {
        *(undefined8 *)(param_1 + 0xab0) = 0;
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x54f,"tls_parse_stoc_ec_pt_formats");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        return 0;
      }
      *(byte **)(param_1 + 0xab0) = pbVar1;
      if (num < 8) {
        if (((ulong)pbVar1 & 4) != 0) {
          *(undefined4 *)puVar4 = *(undefined4 *)(pbVar3 + 1);
          *(undefined4 *)((long)puVar4 + (((ulong)pbVar1 & 0xffffffff) - 4)) =
               *(undefined4 *)(pbVar3 + (((ulong)pbVar1 & 0xffffffff) - 3));
          return 1;
        }
        if (num == 0) {
          return 1;
        }
        *(byte *)puVar4 = pbVar3[1];
        if (((ulong)pbVar1 & 2) == 0) {
          return 1;
        }
        *(undefined2 *)((long)puVar4 + (((ulong)pbVar1 & 0xffffffff) - 2)) =
             *(undefined2 *)(pbVar3 + (((ulong)pbVar1 & 0xffffffff) - 1));
        return 1;
      }
      *puVar4 = *(undefined8 *)(pbVar3 + 1);
      *(undefined8 *)((long)puVar4 + (((ulong)pbVar1 & 0xffffffff) - 8)) =
           *(undefined8 *)(pbVar3 + (((ulong)pbVar1 & 0xffffffff) - 7));
      lVar5 = (long)puVar4 - (long)(undefined8 *)((ulong)(puVar4 + 1) & 0xfffffffffffffff8);
      puVar7 = (undefined8 *)(pbVar3 + 1 + -lVar5);
      puVar4 = (undefined8 *)((ulong)(puVar4 + 1) & 0xfffffffffffffff8);
      for (uVar6 = (ulong)(num + (int)lVar5 >> 3); uVar6 != 0; uVar6 = uVar6 - 1) {
        *puVar4 = *puVar7;
        puVar7 = puVar7 + (ulong)bVar8 * -2 + 1;
        puVar4 = puVar4 + (ulong)bVar8 * -2 + 1;
      }
      return 1;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x540,"tls_parse_stoc_ec_pt_formats");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  return 0;
}