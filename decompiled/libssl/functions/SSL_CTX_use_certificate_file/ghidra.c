int SSL_CTX_use_certificate_file(SSL_CTX *ctx,char *file,int type)

{
  int iVar1;
  BIO_METHOD *type_00;
  BIO *bp;
  long lVar2;
  X509 *pXVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  X509 *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = (X509 *)0x0;
  type_00 = BIO_s_file();
  bp = BIO_new(type_00);
  if (bp == (BIO *)0x0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x122,"SSL_CTX_use_certificate_file");
    ERR_set_error(0x14,0x80007,0);
  }
  else {
    lVar2 = BIO_ctrl(bp,0x6c,3,file);
    if ((int)lVar2 < 1) {
      ERR_new();
      iVar1 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",0x127,"SSL_CTX_use_certificate_file");
      ERR_set_error(0x14,0x80002,0);
    }
    else if (type - 1U < 2) {
      local_38 = (X509 *)X509_new_ex(ctx->method,ctx[1].tlsext_ticket_key_cb);
      if (local_38 == (X509 *)0x0) {
        ERR_new();
        iVar1 = 0;
        ERR_set_debug("ssl/ssl_rsa.c",0x130,"SSL_CTX_use_certificate_file");
        ERR_set_error(0x14,0xc0100,0);
      }
      else {
        if (type == 2) {
          uVar4 = 0x8000d;
          pXVar3 = d2i_X509_bio(bp,&local_38);
        }
        else {
          uVar4 = 0x80009;
          pXVar3 = PEM_read_bio_X509(bp,&local_38,ctx->client_cert_cb,ctx->app_gen_cookie_cb);
        }
        if (pXVar3 == (X509 *)0x0) {
          ERR_new();
          iVar1 = 0;
          ERR_set_debug("ssl/ssl_rsa.c",0x13c,"SSL_CTX_use_certificate_file");
          ERR_set_error(0x14,uVar4,0);
        }
        else {
          iVar1 = SSL_CTX_use_certificate(ctx,local_38);
        }
      }
    }
    else {
      ERR_new();
      iVar1 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",299,"SSL_CTX_use_certificate_file");
      ERR_set_error(0x14,0x7c,0);
    }
  }
  X509_free(local_38);
  BIO_free(bp);
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}