int SSL_CTX_use_certificate_file(SSL_CTX *ctx, const char *file, int type)
{
    int j = SSL_R_BAD_VALUE;
    BIO *in;
    int ret = 0;
    X509 *x = NULL, *cert = NULL;

    in = BIO_new(BIO_s_file());
    if (in == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_BUF_LIB);
        goto end;
    }

    if (BIO_read_filename(in, file) <= 0) {
        ERR_raise(ERR_LIB_SSL, ERR_R_SYS_LIB);
        goto end;
    }
    if (type != SSL_FILETYPE_ASN1 && type != SSL_FILETYPE_PEM) {
        ERR_raise(ERR_LIB_SSL, SSL_R_BAD_SSL_FILETYPE);
        goto end;
    }
    x = X509_new_ex(ctx->libctx, ctx->propq);
    if (x == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        goto end;
    }
    if (type == SSL_FILETYPE_ASN1) {
        j = ERR_R_ASN1_LIB;
        cert = d2i_X509_bio(in, &x);
    } else if (type == SSL_FILETYPE_PEM) {
        j = ERR_R_PEM_LIB;
        cert = PEM_read_bio_X509(in, &x, ctx->default_passwd_callback,
                                 ctx->default_passwd_callback_userdata);
    }
    if (cert == NULL) {
        ERR_raise(ERR_LIB_SSL, j);
        goto end;
    }

    ret = SSL_CTX_use_certificate(ctx, x);
 end:
    X509_free(x);
    BIO_free(in);
    return ret;
}