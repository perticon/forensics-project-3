undefined8 SSL_SESSION_set1_id(long param_1,undefined8 *param_2,uint param_3)

{
  uint uVar1;
  long lVar2;
  ulong uVar3;
  uint uVar4;
  ulong uVar5;
  
  if (param_3 < 0x21) {
    uVar3 = (ulong)param_3;
    *(ulong *)(param_1 + 0x250) = uVar3;
    if ((undefined8 *)(param_1 + 600) != param_2) {
      if (param_3 < 8) {
        if ((param_3 & 4) == 0) {
          if ((param_3 != 0) &&
             (*(undefined *)(param_1 + 600) = *(undefined *)param_2, (param_3 & 2) != 0)) {
            *(undefined2 *)(param_1 + 0x256 + uVar3) = *(undefined2 *)((long)param_2 + (uVar3 - 2));
          }
        }
        else {
          *(undefined4 *)(param_1 + 600) = *(undefined4 *)param_2;
          *(undefined4 *)(param_1 + 0x254 + uVar3) = *(undefined4 *)((long)param_2 + (uVar3 - 4));
        }
      }
      else {
        *(undefined8 *)(param_1 + 600) = *param_2;
        uVar5 = param_1 + 0x260U & 0xfffffffffffffff8;
        *(undefined8 *)(param_1 + 0x250 + uVar3) = *(undefined8 *)((long)param_2 + (uVar3 - 8));
        lVar2 = (long)(undefined8 *)(param_1 + 600) - uVar5;
        uVar4 = param_3 + (int)lVar2 & 0xfffffff8;
        if (7 < uVar4) {
          uVar1 = 0;
          do {
            uVar3 = (ulong)uVar1;
            uVar1 = uVar1 + 8;
            *(undefined8 *)(uVar5 + uVar3) = *(undefined8 *)((long)param_2 + (uVar3 - lVar2));
          } while (uVar1 < uVar4);
        }
      }
    }
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_sess.c",0x372,"SSL_SESSION_set1_id");
  ERR_set_error(0x14,0x198,0);
  return 0;
}