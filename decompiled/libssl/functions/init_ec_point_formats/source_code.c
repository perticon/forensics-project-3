static int init_ec_point_formats(SSL *s, unsigned int context)
{
    OPENSSL_free(s->ext.peer_ecpointformats);
    s->ext.peer_ecpointformats = NULL;
    s->ext.peer_ecpointformats_len = 0;

    return 1;
}