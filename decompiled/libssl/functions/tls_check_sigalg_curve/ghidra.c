undefined8 tls_check_sigalg_curve(long param_1,int param_2)

{
  long lVar1;
  long lVar2;
  ulong uVar3;
  undefined1 *puVar4;
  ulong uVar5;
  
  puVar4 = *(undefined1 **)(*(long *)(param_1 + 0x898) + 0x198);
  if (puVar4 == (undefined1 *)0x0) {
    uVar5 = 0x1f;
    puVar4 = tls12_sigalgs;
  }
  else {
    uVar5 = *(ulong *)(*(long *)(param_1 + 0x898) + 0x1a0);
    if (uVar5 == 0) {
      return 0;
    }
  }
  uVar3 = 0;
  lVar1 = *(long *)(*(long *)(param_1 + 0x9a8) + 0x620);
  do {
    lVar2 = lVar1;
    do {
      if (*(short *)(puVar4 + uVar3 * 2) == *(short *)(lVar2 + 8)) {
        if ((((*(int *)(lVar2 + 0x24) != 0) && (*(int *)(lVar2 + 0x14) == 0x198)) &&
            (*(int *)(lVar2 + 0x20) == param_2)) && (*(int *)(lVar2 + 0x20) != 0)) {
          return 1;
        }
        break;
      }
      lVar2 = lVar2 + 0x28;
    } while (lVar2 != lVar1 + 0x4d8);
    uVar3 = uVar3 + 1;
    if (uVar5 <= uVar3) {
      return 0;
    }
  } while( true );
}