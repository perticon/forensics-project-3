SSL_SESSION * ssl_session_dup(undefined8 *param_1,int param_2)

{
  int iVar1;
  SSL_SESSION *ses;
  char *pcVar2;
  long lVar3;
  ulong uVar4;
  undefined8 *puVar5;
  undefined8 *puVar6;
  byte bVar7;
  
  bVar7 = 0;
  ses = (SSL_SESSION *)CRYPTO_malloc(0x3a0,"ssl/ssl_sess.c",0xa8);
  if (ses != (SSL_SESSION *)0x0) {
    puVar6 = (undefined8 *)((ulong)ses->key_arg & 0xfffffffffffffff8);
    *(undefined8 *)ses = *param_1;
    lVar3 = (long)ses - (long)puVar6;
    *(undefined8 *)(ses[1].krb5_client_princ + 0xb0) = param_1[0x73];
    puVar5 = (undefined8 *)((long)param_1 - lVar3);
    for (uVar4 = (ulong)((int)lVar3 + 0x3a0U >> 3); uVar4 != 0; uVar4 = uVar4 - 1) {
      *puVar6 = *puVar5;
      puVar5 = puVar5 + (ulong)bVar7 * -2 + 1;
      puVar6 = puVar6 + (ulong)bVar7 * -2 + 1;
    }
    *(undefined8 *)(ses[1].krb5_client_princ + 0x70) = 0;
    *(undefined (*) [16])ses[1].session_id = (undefined  [16])0x0;
    *(undefined (*) [16])(ses[1].krb5_client_princ + 0x48) = (undefined  [16])0x0;
    *(undefined (*) [16])(ses[1].session_id + 0x18) = (undefined  [16])0x0;
    *(undefined (*) [16])(ses[1].krb5_client_princ + 0x88) = (undefined  [16])0x0;
    *(undefined (*) [16])(ses[1].krb5_client_princ + 0x28) = (undefined  [16])0x0;
    *(undefined (*) [16])(ses[1].krb5_client_princ + 0x38) = (undefined  [16])0x0;
    *(undefined4 *)(ses[1].sid_ctx + 0xc) = 1;
    lVar3 = CRYPTO_THREAD_lock_new(0,0);
    *(long *)(ses[1].krb5_client_princ + 0xb0) = lVar3;
    if ((lVar3 != 0) &&
       (iVar1 = CRYPTO_new_ex_data(2,ses,(CRYPTO_EX_DATA *)(ses[1].krb5_client_princ + 0x28)),
       iVar1 != 0)) {
      if (param_1[0x57] != 0) {
        iVar1 = X509_up_ref();
        if (iVar1 == 0) goto LAB_00143060;
        *(undefined8 *)(ses[1].session_id + 0x18) = param_1[0x57];
      }
      if (param_1[0x58] != 0) {
        lVar3 = X509_chain_up_ref();
        *(long *)&ses[1].sid_ctx_length = lVar3;
        if (lVar3 == 0) goto LAB_00143060;
      }
      if ((char *)param_1[0x54] != (char *)0x0) {
        pcVar2 = CRYPTO_strdup((char *)param_1[0x54],"ssl/ssl_sess.c",0xdb);
        *(char **)ses[1].session_id = pcVar2;
        if (pcVar2 == (char *)0x0) goto LAB_00143060;
      }
      if ((char *)param_1[0x55] != (char *)0x0) {
        pcVar2 = CRYPTO_strdup((char *)param_1[0x55],"ssl/ssl_sess.c",0xe1);
        *(char **)(ses[1].session_id + 8) = pcVar2;
        if (pcVar2 == (char *)0x0) goto LAB_00143060;
      }
      iVar1 = CRYPTO_dup_ex_data(2,(CRYPTO_EX_DATA *)(ses[1].krb5_client_princ + 0x28),
                                 (CRYPTO_EX_DATA *)(param_1 + 0x62));
      if (iVar1 != 0) {
        if ((char *)param_1[0x66] != (char *)0x0) {
          pcVar2 = CRYPTO_strdup((char *)param_1[0x66],"ssl/ssl_sess.c",0xee);
          *(char **)(ses[1].krb5_client_princ + 0x48) = pcVar2;
          if (pcVar2 == (char *)0x0) goto LAB_00143060;
        }
        if ((param_2 == 0) || (param_1[0x67] == 0)) {
          *(undefined (*) [16])(ses[1].krb5_client_princ + 0x58) = (undefined  [16])0x0;
        }
        else {
          lVar3 = CRYPTO_memdup(param_1[0x67],param_1[0x68],"ssl/ssl_sess.c",0xf6);
          *(long *)(ses[1].krb5_client_princ + 0x50) = lVar3;
          if (lVar3 == 0) goto LAB_00143060;
        }
        if (param_1[0x6b] != 0) {
          lVar3 = CRYPTO_memdup(param_1[0x6b],param_1[0x6c],"ssl/ssl_sess.c",0xff);
          *(long *)(ses[1].krb5_client_princ + 0x70) = lVar3;
          if (lVar3 == 0) goto LAB_00143060;
        }
        if ((char *)param_1[0x6e] != (char *)0x0) {
          pcVar2 = CRYPTO_strdup((char *)param_1[0x6e],"ssl/ssl_sess.c",0x107);
          *(char **)(ses[1].krb5_client_princ + 0x88) = pcVar2;
          if (pcVar2 == (char *)0x0) goto LAB_00143060;
        }
        if (param_1[0x6f] == 0) {
          return ses;
        }
        lVar3 = CRYPTO_memdup(param_1[0x6f],param_1[0x70],"ssl/ssl_sess.c",0x110);
        *(long *)(ses[1].krb5_client_princ + 0x90) = lVar3;
        if (lVar3 != 0) {
          return ses;
        }
      }
    }
  }
LAB_00143060:
  ERR_new();
  ERR_set_debug("ssl/ssl_sess.c",0x117,"ssl_session_dup");
  ERR_set_error(0x14,0xc0100,0);
  SSL_SESSION_free(ses);
  return (SSL_SESSION *)0x0;
}