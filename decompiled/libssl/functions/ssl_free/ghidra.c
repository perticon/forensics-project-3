undefined8 ssl_free(BIO *param_1)

{
  int iVar1;
  SSL **ptr;
  
  if (param_1 != (BIO *)0x0) {
    ptr = (SSL **)BIO_get_data();
    iVar1 = BIO_get_shutdown(param_1);
    if (iVar1 != 0) {
      if (*ptr != (SSL *)0x0) {
        SSL_shutdown(*ptr);
      }
      iVar1 = BIO_get_init(param_1);
      if (iVar1 != 0) {
        SSL_free(*ptr);
      }
      BIO_clear_flags(param_1,-1);
      BIO_set_init(param_1,0);
    }
    CRYPTO_free(ptr);
    return 1;
  }
  return 0;
}