uint tls1_group_id2nid(ushort param_1,int param_2)

{
  uint uVar1;
  long lVar2;
  
  uVar1 = 0;
  if (param_1 != 0) {
    lVar2 = 0;
    do {
      if (param_1 == *(ushort *)(nid_to_group + lVar2 * 8 + 4)) {
        return *(uint *)(nid_to_group + lVar2 * 8);
      }
      lVar2 = lVar2 + 1;
    } while (lVar2 != 0x2a);
    uVar1 = 0;
    if (param_2 != 0) {
      uVar1 = param_1 | 0x1000000;
    }
  }
  return uVar1;
}