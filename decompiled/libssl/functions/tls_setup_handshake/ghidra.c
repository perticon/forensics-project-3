int tls_setup_handshake(SSL *param_1)

{
  int *piVar1;
  SSL_METHOD *pSVar2;
  _func_3076 *p_Var3;
  int iVar4;
  int iVar5;
  stack_st_SSL_CIPHER *psVar6;
  long lVar7;
  int iVar8;
  int iVar9;
  long in_FS_OFFSET;
  int local_38;
  int local_34;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  iVar4 = ssl3_init_finished_mac();
  if (iVar4 == 0) goto LAB_001771ff;
  *(undefined2 *)&param_1[3].tlsext_ellipticcurvelist_length = 0;
  param_1[3].tlsext_ecpointformatlist = (uchar *)0x0;
  *(undefined (*) [16])&param_1[3].tlsext_ocsp_resplen = (undefined  [16])0x0;
  iVar4 = ssl_get_min_max_version(0,param_1,&local_38,&local_34);
  if (iVar4 != 0) {
    ERR_new();
    iVar4 = 0;
    ERR_set_debug("ssl/statem/statem_lib.c",0x68,"tls_setup_handshake");
    ossl_statem_fatal(param_1,0x46,0xbf,0);
    goto LAB_001771ff;
  }
  if (*(long *)(*(long *)&param_1[3].ex_data.dummy + 0x588) == 0) {
    if (((byte)param_1->method->get_timeout[0x60] & 8) == 0) {
      if (local_34 < 0x303) goto LAB_00177520;
      if (local_38 < 0x303) {
        lVar7 = SSL_ctrl(param_1,0x7b,0x303,(void *)0x0);
        iVar9 = (int)lVar7;
        goto LAB_00177350;
      }
    }
    else {
      if ((local_34 == 0x100) || (0xfefe < local_34)) {
LAB_00177520:
        ERR_new();
        ERR_set_debug("ssl/statem/statem_lib.c",0x79,"tls_setup_handshake");
        ossl_statem_fatal(param_1,0x28,0x129,
                          "The max supported SSL/TLS version needs the MD5-SHA1 digest but it is not available in the loaded providers. Use (D)TLSv1.2 or above, or load different providers"
                         );
        goto LAB_001771ff;
      }
      if ((local_38 == 0x100) || (0xfefd < local_38)) {
        lVar7 = SSL_ctrl(param_1,0x7b,0xfefd,(void *)0x0);
        iVar9 = (int)lVar7;
LAB_00177350:
        if (iVar9 == 0) {
          ERR_new();
          iVar4 = 0;
          ERR_set_debug("ssl/statem/statem_lib.c",0x8d,"tls_setup_handshake");
          ossl_statem_fatal(param_1,0x28,0xc0103,0);
          goto LAB_001771ff;
        }
      }
    }
  }
  if (param_1->server == 0) {
    if ((param_1->tlsext_opaque_prf_input == (void *)0x0) || (param_1[1].wbio == (BIO *)0x0)) {
      LOCK();
      piVar1 = (int *)(*(long *)&param_1[4].mac_flags + 0x78);
      *piVar1 = *piVar1 + 1;
    }
    else {
      LOCK();
      piVar1 = (int *)(*(long *)&param_1[4].mac_flags + 0x7c);
      *piVar1 = *piVar1 + 1;
    }
    pSVar2 = param_1->method;
    *(undefined4 *)&param_1[1].tlsext_ecpointformatlist_length = 0;
    iVar4 = 1;
    *(undefined (*) [16])&param_1->kssl_ctx = (undefined  [16])0x0;
    *(undefined (*) [16])&param_1->psk_server_callback = (undefined  [16])0x0;
    p_Var3 = pSVar2->get_timeout;
    param_1[1].quiet_shutdown = 0;
    if (((byte)p_Var3[0x60] & 8) != 0) {
      *(undefined4 *)&param_1->s2 = 1;
    }
  }
  else {
    psVar6 = SSL_get_ciphers(param_1);
    for (iVar9 = 0; iVar5 = OPENSSL_sk_num(psVar6), iVar9 < iVar5; iVar9 = iVar9 + 1) {
      lVar7 = OPENSSL_sk_value(psVar6,iVar9);
      if (((byte)param_1->method->get_timeout[0x60] & 8) == 0) {
        if ((*(int *)(lVar7 + 0x2c) == local_34 || *(int *)(lVar7 + 0x2c) < local_34) &&
           (local_34 <= *(int *)(lVar7 + 0x30))) {
LAB_00177410:
          if ((param_1->tlsext_opaque_prf_input == (void *)0x0) || (param_1[1].wbio == (BIO *)0x0))
          {
            LOCK();
            piVar1 = (int *)(*(long *)&param_1[4].mac_flags + 0x84);
            *piVar1 = *piVar1 + 1;
            iVar4 = 1;
          }
          else {
            LOCK();
            piVar1 = (int *)(*(long *)&param_1[3].ex_data.dummy + 0x88);
            *piVar1 = *piVar1 + 1;
            iVar4 = 1;
            *(undefined4 *)&param_1[1].msg_callback = 0;
          }
          goto LAB_001771ff;
        }
      }
      else {
        iVar5 = *(int *)(lVar7 + 0x34);
        if (local_34 == 0x100) {
          iVar8 = 0xff00;
          if (iVar5 != 0x100) goto LAB_001773cd;
          iVar5 = *(int *)(lVar7 + 0x38);
          if (iVar5 == 0x100) goto LAB_00177410;
        }
        else {
          iVar8 = local_34;
          if (iVar5 == 0x100) {
            iVar5 = 0xff00;
          }
LAB_001773cd:
          if (iVar5 < iVar8) goto LAB_001773d1;
          iVar5 = *(int *)(lVar7 + 0x38);
          if (iVar5 == 0x100) {
            iVar5 = 0xff00;
          }
        }
        if (iVar5 <= iVar8) goto LAB_00177410;
      }
LAB_001773d1:
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0xaa,"tls_setup_handshake");
    ossl_statem_fatal(param_1,0x28,0xb5,"No ciphers enabled for max supported SSL/TLS version");
  }
LAB_001771ff:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar4;
}