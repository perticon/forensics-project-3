int tls_setup_handshake(SSL *s)
{
    int ver_min, ver_max, ok;

    if (!ssl3_init_finished_mac(s)) {
        /* SSLfatal() already called */
        return 0;
    }

    /* Reset any extension flags */
    memset(s->ext.extflags, 0, sizeof(s->ext.extflags));

    if (ssl_get_min_max_version(s, &ver_min, &ver_max, NULL) != 0) {
        SSLfatal(s, SSL_AD_PROTOCOL_VERSION, SSL_R_NO_PROTOCOLS_AVAILABLE);
        return 0;
    }

    /* Sanity check that we have MD5-SHA1 if we need it */
    if (s->ctx->ssl_digest_methods[SSL_MD_MD5_SHA1_IDX] == NULL) {
        int md5sha1_needed = 0;

        /* We don't have MD5-SHA1 - do we need it? */
        if (SSL_IS_DTLS(s)) {
            if (DTLS_VERSION_LE(ver_max, DTLS1_VERSION))
                md5sha1_needed = 1;
        } else {
            if (ver_max <= TLS1_1_VERSION)
                md5sha1_needed = 1;
        }
        if (md5sha1_needed) {
            SSLfatal_data(s, SSL_AD_HANDSHAKE_FAILURE,
                          SSL_R_NO_SUITABLE_DIGEST_ALGORITHM,
                          "The max supported SSL/TLS version needs the"
                          " MD5-SHA1 digest but it is not available"
                          " in the loaded providers. Use (D)TLSv1.2 or"
                          " above, or load different providers");
            return 0;
        }

        ok = 1;
        /* Don't allow TLSv1.1 or below to be negotiated */
        if (SSL_IS_DTLS(s)) {
            if (DTLS_VERSION_LT(ver_min, DTLS1_2_VERSION))
                ok = SSL_set_min_proto_version(s, DTLS1_2_VERSION);
        } else {
            if (ver_min < TLS1_2_VERSION)
                ok = SSL_set_min_proto_version(s, TLS1_2_VERSION);
        }
        if (!ok) {
            /* Shouldn't happen */
            SSLfatal(s, SSL_AD_HANDSHAKE_FAILURE, ERR_R_INTERNAL_ERROR);
            return 0;
        }
    }

    ok = 0;
    if (s->server) {
        STACK_OF(SSL_CIPHER) *ciphers = SSL_get_ciphers(s);
        int i;

        /*
         * Sanity check that the maximum version we accept has ciphers
         * enabled. For clients we do this check during construction of the
         * ClientHello.
         */
        for (i = 0; i < sk_SSL_CIPHER_num(ciphers); i++) {
            const SSL_CIPHER *c = sk_SSL_CIPHER_value(ciphers, i);

            if (SSL_IS_DTLS(s)) {
                if (DTLS_VERSION_GE(ver_max, c->min_dtls) &&
                        DTLS_VERSION_LE(ver_max, c->max_dtls))
                    ok = 1;
            } else if (ver_max >= c->min_tls && ver_max <= c->max_tls) {
                ok = 1;
            }
            if (ok)
                break;
        }
        if (!ok) {
            SSLfatal_data(s, SSL_AD_HANDSHAKE_FAILURE,
                          SSL_R_NO_CIPHERS_AVAILABLE,
                          "No ciphers enabled for max supported "
                          "SSL/TLS version");
            return 0;
        }
        if (SSL_IS_FIRST_HANDSHAKE(s)) {
            /* N.B. s->session_ctx == s->ctx here */
            ssl_tsan_counter(s->session_ctx, &s->session_ctx->stats.sess_accept);
        } else {
            /* N.B. s->ctx may not equal s->session_ctx */
            ssl_tsan_counter(s->ctx, &s->ctx->stats.sess_accept_renegotiate);

            s->s3.tmp.cert_request = 0;
        }
    } else {
        if (SSL_IS_FIRST_HANDSHAKE(s))
            ssl_tsan_counter(s->session_ctx, &s->session_ctx->stats.sess_connect);
        else
            ssl_tsan_counter(s->session_ctx,
                         &s->session_ctx->stats.sess_connect_renegotiate);

        /* mark client_random uninitialized */
        memset(s->s3.client_random, 0, sizeof(s->s3.client_random));
        s->hit = 0;

        s->s3.tmp.cert_req = 0;

        if (SSL_IS_DTLS(s))
            s->statem.use_timer = 1;
    }

    return 1;
}