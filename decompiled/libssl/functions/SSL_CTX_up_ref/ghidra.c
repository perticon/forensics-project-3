bool SSL_CTX_up_ref(long param_1)

{
  int *piVar1;
  int iVar2;
  
  LOCK();
  piVar1 = (int *)(param_1 + 0xa4);
  iVar2 = *piVar1;
  *piVar1 = *piVar1 + 1;
  return 0 < iVar2;
}