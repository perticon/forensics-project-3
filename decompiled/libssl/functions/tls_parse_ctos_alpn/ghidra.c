undefined8 tls_parse_ctos_alpn(long param_1,ushort **param_2)

{
  ushort *puVar1;
  ushort uVar2;
  ushort *puVar3;
  long lVar4;
  ushort *puVar5;
  byte *pbVar6;
  ushort *puVar7;
  undefined8 uVar8;
  
  if ((*(long *)(param_1 + 0x240) != 0) && (*(long *)(param_1 + 0x2c8) != 0)) {
    return 1;
  }
  if ((ushort *)0x1 < param_2[1]) {
    uVar2 = **param_2;
    puVar7 = (ushort *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8);
    if (param_2[1] + -1 == puVar7) {
      puVar1 = *param_2 + 1;
      param_2[1] = (ushort *)0x0;
      *param_2 = (ushort *)((long)puVar1 + (long)puVar7);
      puVar3 = puVar7;
      puVar5 = puVar1;
      if ((ushort *)0x1 < puVar7) {
        while( true ) {
          pbVar6 = (byte *)(ulong)*(byte *)puVar5;
          if ((byte *)((long)puVar3 + -1) < pbVar6) break;
          puVar3 = (ushort *)((byte *)((long)puVar3 + -1) + -(long)pbVar6);
          if (pbVar6 == (byte *)0x0) break;
          puVar5 = (ushort *)((byte *)((long)puVar5 + 1) + (long)pbVar6);
          if (puVar3 == (ushort *)0x0) {
            CRYPTO_free(*(void **)(param_1 + 0x498));
            *(undefined8 *)(param_1 + 0x498) = 0;
            *(undefined8 *)(param_1 + 0x4a0) = 0;
            CRYPTO_free((void *)0x0);
            *(undefined8 *)(param_1 + 0x498) = 0;
            *(undefined8 *)(param_1 + 0x4a0) = 0;
            lVar4 = CRYPTO_memdup(puVar1,puVar7,"include/internal/packet.h",0x1c5);
            *(long *)(param_1 + 0x498) = lVar4;
            if (lVar4 == 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_srvr.c",0x1c4,"tls_parse_ctos_alpn");
              ossl_statem_fatal(param_1,0x50,0xc0103,0);
              return 0;
            }
            *(ushort **)(param_1 + 0x4a0) = puVar7;
            return 1;
          }
        }
        ERR_new();
        uVar8 = 0x1ba;
        goto LAB_00166215;
      }
    }
  }
  ERR_new();
  uVar8 = 0x1b1;
LAB_00166215:
  ERR_set_debug("ssl/statem/extensions_srvr.c",uVar8,"tls_parse_ctos_alpn");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  return 0;
}