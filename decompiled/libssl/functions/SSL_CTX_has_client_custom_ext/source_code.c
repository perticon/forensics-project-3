int SSL_CTX_has_client_custom_ext(const SSL_CTX *ctx, unsigned int ext_type)
{
    return custom_ext_find(&ctx->cert->custext, ENDPOINT_CLIENT, ext_type,
                           NULL) != NULL;
}