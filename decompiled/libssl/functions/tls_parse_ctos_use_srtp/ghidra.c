undefined8 tls_parse_ctos_use_srtp(SSL *param_1,ushort **param_2)

{
  ushort uVar1;
  int iVar2;
  stack_st_SRTP_PROTECTION_PROFILE *psVar3;
  EVP_MD_CTX *pEVar4;
  ushort *puVar5;
  ushort *puVar6;
  ushort *puVar7;
  undefined8 uVar8;
  int iVar9;
  
  psVar3 = SSL_get_srtp_profiles(param_1);
  if (psVar3 == (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
    return 1;
  }
  if (param_2[1] < (ushort *)0x2) {
LAB_00166498:
    ERR_new();
    uVar8 = 0x1db;
  }
  else {
    puVar6 = param_2[1] + -1;
    uVar1 = **param_2;
    puVar5 = *param_2 + 1;
    param_2[1] = puVar6;
    *param_2 = puVar5;
    if (((uVar1 & 0x100) != 0) ||
       (puVar7 = (ushort *)(ulong)(ushort)(uVar1 << 8 | uVar1 >> 8), puVar6 < puVar7))
    goto LAB_00166498;
    *param_2 = (ushort *)((long)puVar5 + (long)puVar7);
    param_2[1] = (ushort *)((long)puVar6 - (long)puVar7);
    psVar3 = SSL_get_srtp_profiles(param_1);
    param_1[4].read_hash = (EVP_MD_CTX *)0x0;
    iVar2 = OPENSSL_sk_num(psVar3);
    if (puVar7 == (ushort *)0x0) {
LAB_001664f4:
      if (param_2[1] != (ushort *)0x0) {
        puVar5 = (ushort *)((long)param_2[1] + -1);
        puVar6 = (ushort *)(ulong)*(byte *)*param_2;
        puVar7 = (ushort *)((long)*param_2 + 1);
        param_2[1] = puVar5;
        *param_2 = puVar7;
        if (puVar6 <= puVar5) {
          *param_2 = (ushort *)((long)puVar7 + (long)puVar6);
          param_2[1] = (ushort *)((long)puVar5 - (long)puVar6);
          if ((ushort *)((long)puVar5 - (long)puVar6) == (ushort *)0x0) {
            return 1;
          }
        }
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_srvr.c",0x207,"tls_parse_ctos_use_srtp");
        uVar8 = 0x160;
        goto LAB_001664bc;
      }
      ERR_new();
      uVar8 = 0x200;
    }
    else {
      while (puVar7 != (ushort *)0x1) {
        puVar7 = puVar7 + -1;
        iVar9 = 0;
        uVar1 = *puVar5;
        if (0 < iVar2) {
          do {
            pEVar4 = (EVP_MD_CTX *)OPENSSL_sk_value(psVar3,iVar9);
            if (pEVar4->engine == (ENGINE *)(ulong)(ushort)(uVar1 << 8 | uVar1 >> 8)) {
              param_1[4].read_hash = pEVar4;
              iVar2 = iVar9;
              break;
            }
            iVar9 = iVar9 + 1;
          } while (iVar9 != iVar2);
        }
        if (puVar7 == (ushort *)0x0) goto LAB_001664f4;
        puVar5 = puVar5 + 1;
      }
      ERR_new();
      uVar8 = 0x1e7;
    }
  }
  ERR_set_debug("ssl/statem/extensions_srvr.c",uVar8,"tls_parse_ctos_use_srtp");
  uVar8 = 0x161;
LAB_001664bc:
  ossl_statem_fatal(param_1,0x32,uVar8,0);
  return 0;
}