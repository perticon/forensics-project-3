int SSL_set0_tmp_dh_pkey(SSL *s, EVP_PKEY *dhpkey)
{
    if (!ssl_security(s, SSL_SECOP_TMP_DH,
                      EVP_PKEY_get_security_bits(dhpkey), 0, dhpkey)) {
        ERR_raise(ERR_LIB_SSL, SSL_R_DH_KEY_TOO_SMALL);
        return 0;
    }
    EVP_PKEY_free(s->cert->dh_tmp);
    s->cert->dh_tmp = dhpkey;
    return 1;
}