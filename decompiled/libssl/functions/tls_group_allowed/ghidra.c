undefined8 tls_group_allowed(long param_1,ushort param_2,undefined4 param_3)

{
  undefined4 uVar1;
  long lVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  ushort local_32;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  lVar2 = tls1_group_id_lookup(*(undefined8 *)(param_1 + 0x9a8),param_2);
  uVar3 = 0;
  if (lVar2 != 0) {
    local_32 = param_2 << 8 | param_2 >> 8;
    uVar1 = tls1_group_id2nid(*(undefined2 *)(lVar2 + 0x1c),0);
    uVar3 = ssl_security(param_1,param_3,*(undefined4 *)(lVar2 + 0x18),uVar1,&local_32);
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}