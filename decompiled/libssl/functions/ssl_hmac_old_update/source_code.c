int ssl_hmac_old_update(SSL_HMAC *ctx, const unsigned char *data, size_t len)
{
    return HMAC_Update(ctx->old_ctx, data, len);
}