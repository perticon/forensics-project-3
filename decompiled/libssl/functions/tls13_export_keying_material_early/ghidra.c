bool tls13_export_keying_material_early
               (long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
               undefined8 param_5,void *param_6,size_t param_7)

{
  int iVar1;
  EVP_MD_CTX *ctx;
  long lVar2;
  EVP_MD *type;
  long in_FS_OFFSET;
  bool bVar3;
  uint local_110;
  uint local_10c;
  undefined local_108 [64];
  uchar local_c8 [64];
  uchar local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
  if (ctx != (EVP_MD_CTX *)0x0) {
    iVar1 = ossl_statem_export_early_allowed(param_1);
    if (iVar1 != 0) {
      if (((*(int *)(param_1 + 0x38) == 0) && (*(int *)(param_1 + 0x1d40) != 0)) &&
         (*(int *)(*(long *)(param_1 + 0x918) + 0x354) == 0)) {
        lVar2 = SSL_SESSION_get0_cipher(*(undefined8 *)(param_1 + 0x920));
      }
      else {
        lVar2 = SSL_SESSION_get0_cipher();
      }
      type = (EVP_MD *)ssl_md(*(undefined8 *)(param_1 + 0x9a8),*(undefined4 *)(lVar2 + 0x40));
      iVar1 = EVP_DigestInit_ex(ctx,type,(ENGINE *)0x0);
      if (0 < iVar1) {
        iVar1 = EVP_DigestUpdate(ctx,param_6,param_7);
        if (0 < iVar1) {
          iVar1 = EVP_DigestFinal_ex(ctx,local_c8,&local_110);
          if (0 < iVar1) {
            iVar1 = EVP_DigestInit_ex(ctx,type,(ENGINE *)0x0);
            if (0 < iVar1) {
              iVar1 = EVP_DigestFinal_ex(ctx,local_88,&local_10c);
              if (0 < iVar1) {
                iVar1 = tls13_hkdf_expand(param_1,type,param_1 + 0x804,param_4,param_5,local_88,
                                          local_10c,local_108,local_110,0);
                if (iVar1 != 0) {
                  iVar1 = tls13_hkdf_expand(param_1,type,local_108,"exporter",8,local_c8,local_110,
                                            param_2,param_3,0);
                  bVar3 = iVar1 != 0;
                  goto LAB_00152d23;
                }
              }
            }
          }
        }
      }
    }
  }
  bVar3 = false;
LAB_00152d23:
  EVP_MD_CTX_free(ctx);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}