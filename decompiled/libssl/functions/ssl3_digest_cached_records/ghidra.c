undefined8 ssl3_digest_cached_records(long param_1,int param_2)

{
  int iVar1;
  size_t cnt;
  long lVar2;
  EVP_MD *type;
  undefined8 uVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  void *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(long *)(param_1 + 400) == 0) {
    cnt = BIO_ctrl(*(BIO **)(param_1 + 0x188),3,0,&local_28);
    if ((long)cnt < 1) {
      ERR_new();
      ERR_set_debug("ssl/s3_enc.c",0x178,"ssl3_digest_cached_records");
      uVar3 = 0x14c;
    }
    else {
      lVar2 = EVP_MD_CTX_new();
      *(long *)(param_1 + 400) = lVar2;
      if (lVar2 == 0) {
        ERR_new();
        ERR_set_debug("ssl/s3_enc.c",0x17e,"ssl3_digest_cached_records");
        uVar3 = 0xc0100;
      }
      else {
        type = (EVP_MD *)ssl_handshake_md(param_1);
        if (type == (EVP_MD *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/s3_enc.c",0x184,"ssl3_digest_cached_records");
          uVar3 = 0x129;
        }
        else {
          iVar1 = EVP_DigestInit_ex(*(EVP_MD_CTX **)(param_1 + 400),type,(ENGINE *)0x0);
          if (iVar1 != 0) {
            iVar1 = EVP_DigestUpdate(*(EVP_MD_CTX **)(param_1 + 400),local_28,cnt);
            if (iVar1 != 0) goto LAB_001283ab;
          }
          ERR_new();
          ERR_set_debug("ssl/s3_enc.c",0x18a,"ssl3_digest_cached_records");
          uVar3 = 0xc0103;
        }
      }
    }
    uVar4 = 0;
    ossl_statem_fatal(param_1,0x50,uVar3,0);
  }
  else {
LAB_001283ab:
    uVar4 = 1;
    if (param_2 == 0) {
      BIO_free(*(BIO **)(param_1 + 0x188));
      *(undefined8 *)(param_1 + 0x188) = 0;
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}