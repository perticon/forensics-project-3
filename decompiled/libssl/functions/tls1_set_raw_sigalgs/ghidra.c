undefined8 tls1_set_raw_sigalgs(long param_1,void *param_2,long param_3,int param_4)

{
  void *__dest;
  
  __dest = CRYPTO_malloc((int)(param_3 * 2),"ssl/t1_lib.c",0x9f6);
  if (__dest == (void *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/t1_lib.c",0x9f7,"tls1_set_raw_sigalgs");
    ERR_set_error(0x14,0xc0100,0);
    return 0;
  }
  memcpy(__dest,param_2,param_3 * 2);
  if (param_4 == 0) {
    CRYPTO_free(*(void **)(param_1 + 0x198));
    *(void **)(param_1 + 0x198) = __dest;
    *(long *)(param_1 + 0x1a0) = param_3;
    return 1;
  }
  CRYPTO_free(*(void **)(param_1 + 0x1a8));
  *(void **)(param_1 + 0x1a8) = __dest;
  *(long *)(param_1 + 0x1b0) = param_3;
  return 1;
}