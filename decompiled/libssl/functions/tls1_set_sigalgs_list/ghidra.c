int tls1_set_sigalgs_list(long param_1,char *param_2,undefined4 param_3)

{
  int iVar1;
  long in_FS_OFFSET;
  undefined8 local_a8;
  undefined local_a0 [128];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_a8 = 0;
  iVar1 = CONF_parse_list(param_2,0x3a,1,sig_cb,&local_a8);
  if ((iVar1 != 0) && (iVar1 = 1, param_1 != 0)) {
    iVar1 = tls1_set_raw_sigalgs(param_1,local_a0,local_a8,param_3);
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}