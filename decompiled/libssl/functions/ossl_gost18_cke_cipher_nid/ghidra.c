uint ossl_gost18_cke_cipher_nid(long param_1)

{
  uint uVar1;
  uint uVar2;
  
  uVar1 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x24);
  uVar2 = 0x4a4;
  if ((uVar1 & 0x400000) == 0) {
    uVar2 = (int)(uVar1 << 8) >> 0x1f & 0x3f5;
  }
  return uVar2;
}