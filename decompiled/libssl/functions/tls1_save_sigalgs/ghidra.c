undefined8 tls1_save_sigalgs(long param_1,undefined8 param_2,int param_3)

{
  undefined8 uVar1;
  
  if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 2) == 0) {
    return 1;
  }
  if (*(long *)(param_1 + 0x898) != 0) {
    if (param_3 != 0) {
      uVar1 = tls1_save_u16(param_2,param_1 + 0x398,param_1 + 0x3a8);
      return uVar1;
    }
    uVar1 = tls1_save_u16(param_2,param_1 + 0x390,param_1 + 0x3a0);
    return uVar1;
  }
  return 0;
}