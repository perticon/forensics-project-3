int tls_parse_ctos_supported_groups(long param_1,ushort **param_2)

{
  ushort uVar1;
  ushort uVar2;
  int iVar3;
  ushort *puVar4;
  long in_FS_OFFSET;
  ushort *local_38;
  ushort *local_30;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if ((ushort *)0x1 < param_2[1]) {
    puVar4 = param_2[1] + -1;
    uVar1 = **param_2;
    uVar2 = uVar1 >> 8;
    if (puVar4 == (ushort *)(ulong)(ushort)(uVar1 << 8 | uVar2)) {
      local_38 = *param_2 + 1;
      param_2[1] = (ushort *)0x0;
      *param_2 = (ushort *)((long)local_38 + (long)puVar4);
      local_30 = puVar4;
      if ((puVar4 != (ushort *)0x0) && ((uVar2 & 1) == 0)) {
        if (*(int *)(param_1 + 0x4d0) == 0) {
LAB_00167360:
          CRYPTO_free(*(void **)(param_1 + 0xad8));
          *(undefined8 *)(param_1 + 0xad8) = 0;
          *(undefined8 *)(param_1 + 0xad0) = 0;
          iVar3 = tls1_save_u16(&local_38,param_1 + 0xad8,param_1 + 0xad0);
          if (iVar3 == 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x38d,"tls_parse_ctos_supported_groups");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            goto LAB_001672d4;
          }
        }
        else if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
                 (iVar3 = **(int **)(param_1 + 8), iVar3 != 0x10000)) && (0x303 < iVar3))
        goto LAB_00167360;
        iVar3 = 1;
        goto LAB_001672d4;
      }
    }
  }
  ERR_new();
  iVar3 = 0;
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x382,"tls_parse_ctos_supported_groups");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
LAB_001672d4:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}