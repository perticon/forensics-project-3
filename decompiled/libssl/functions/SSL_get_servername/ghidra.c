long SSL_get_servername(long param_1,int param_2)

{
  long lVar1;
  int *piVar2;
  int iVar3;
  
  if (*(long *)(param_1 + 0x30) == 0) {
    if (param_2 != 0) {
      return 0;
    }
  }
  else {
    if (param_2 != 0) {
      return 0;
    }
    if (*(int *)(param_1 + 0x38) != 0) {
      if ((*(int *)(param_1 + 0x4d0) != 0) &&
         ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0 ||
           (iVar3 = **(int **)(param_1 + 8), iVar3 == 0x10000)) || (iVar3 < 0x304)))) {
        return *(long *)(*(long *)(param_1 + 0x918) + 0x330);
      }
      goto LAB_00139378;
    }
  }
  iVar3 = SSL_in_before(param_1);
  if (iVar3 != 0) {
    if (*(long *)(param_1 + 0xa58) != 0) {
      return *(long *)(param_1 + 0xa58);
    }
    piVar2 = *(int **)(param_1 + 0x918);
    if (piVar2 == (int *)0x0) {
      return 0;
    }
    if (*piVar2 == 0x304) {
      return 0;
    }
    return *(long *)(piVar2 + 0xcc);
  }
  if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
       (iVar3 = **(int **)(param_1 + 8), iVar3 == 0x10000)) || (iVar3 < 0x304)) &&
     ((*(int *)(param_1 + 0x4d0) != 0 &&
      (lVar1 = *(long *)(*(long *)(param_1 + 0x918) + 0x330), lVar1 != 0)))) {
    return lVar1;
  }
LAB_00139378:
  return *(long *)(param_1 + 0xa58);
}