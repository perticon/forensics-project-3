void SSL_SESSION_list_add(void** rdi, void** rsi) {
    void** rax3;
    void** rdx4;
    void** rcx5;
    void** r8_6;
    void** r9_7;
    void** rax8;
    void** r8_9;
    void** rcx10;
    void** rdx11;

    rax3 = *reinterpret_cast<void***>(rsi + 0x328);
    if (!rax3 || (rdx4 = *reinterpret_cast<void***>(rsi + 0x320), rdx4 == 0)) {
        rax3 = *reinterpret_cast<void***>(rdi + 64);
        if (!rax3) {
            addr_4286e_3:
        } else {
            addr_428a9_4:
            rcx5 = *reinterpret_cast<void***>(rsi + 0x2e8);
            if (!*reinterpret_cast<int32_t*>(rsi + 0x2f0)) {
                if (*reinterpret_cast<int32_t*>(rax3 + 0x2f0) || reinterpret_cast<signed char>(rcx5) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax3 + 0x2e8))) {
                    r8_6 = *reinterpret_cast<void***>(rdi + 72);
                    r9_7 = *reinterpret_cast<void***>(r8_6 + 0x2e8);
                    if (!*reinterpret_cast<int32_t*>(r8_6 + 0x2f0)) 
                        goto addr_428f9_7; else 
                        goto addr_429bb_8;
                } else {
                    addr_4297a_9:
                    *reinterpret_cast<void***>(rsi + 0x328) = rax3;
                    *reinterpret_cast<void***>(rax3 + 0x320) = rsi;
                    *reinterpret_cast<void***>(rsi + 0x320) = rdi + 64;
                    *reinterpret_cast<void***>(rdi + 64) = rsi;
                    goto addr_42997_10;
                }
            } else {
                if (!*reinterpret_cast<int32_t*>(rax3 + 0x2f0)) 
                    goto addr_4297a_9;
                if (reinterpret_cast<signed char>(rcx5) >= reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax3 + 0x2e8))) 
                    goto addr_4297a_9;
                r8_6 = *reinterpret_cast<void***>(rdi + 72);
                r9_7 = *reinterpret_cast<void***>(r8_6 + 0x2e8);
                if (!*reinterpret_cast<int32_t*>(r8_6 + 0x2f0)) {
                    rax8 = *reinterpret_cast<void***>(rax3 + 0x328);
                    r8_9 = rdi + 72;
                    if (rax8 != r8_9) 
                        goto addr_42a0c_15;
                    goto addr_42997_10;
                }
            }
        }
    } else {
        rcx10 = rdi + 64;
        if (rax3 == rdi + 72) {
            if (rdx4 == rcx10) {
                __asm__("pxor xmm0, xmm0");
                __asm__("movups [rdi+0x40], xmm0");
                __asm__("movups [rsi+0x320], xmm0");
            } else {
                *reinterpret_cast<void***>(rdi + 72) = rdx4;
                *reinterpret_cast<void***>(rdx4 + 0x328) = rax3;
                rax3 = *reinterpret_cast<void***>(rdi + 64);
                goto addr_42853_21;
            }
        } else {
            if (rdx4 == rcx10) {
                *reinterpret_cast<void***>(rdi + 64) = rax3;
                *reinterpret_cast<void***>(rax3 + 0x320) = rdx4;
                goto addr_42853_21;
            } else {
                *reinterpret_cast<void***>(rax3 + 0x320) = rdx4;
                *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 0x320) + 0x328) = rax3;
                rax3 = *reinterpret_cast<void***>(rdi + 64);
                goto addr_42853_21;
            }
        }
    }
    __asm__("movd xmm0, rsi");
    __asm__("movd xmm1, r8");
    __asm__("punpcklqdq xmm0, xmm0");
    __asm__("movups [rdi+0x40], xmm0");
    __asm__("movd xmm0, rcx");
    __asm__("punpcklqdq xmm0, xmm1");
    *reinterpret_cast<void***>(rsi + 0x390) = rdi;
    __asm__("movups [rsi+0x320], xmm0");
    return;
    addr_428f9_7:
    if (reinterpret_cast<signed char>(rcx5) < reinterpret_cast<signed char>(r9_7)) {
        addr_429bb_8:
        *reinterpret_cast<void***>(rsi + 0x320) = r8_6;
        *reinterpret_cast<void***>(r8_6 + 0x328) = rsi;
        *reinterpret_cast<void***>(rsi + 0x328) = rdi + 72;
        *reinterpret_cast<void***>(rdi + 72) = rsi;
        goto addr_42997_10;
    } else {
        rax8 = *reinterpret_cast<void***>(rax3 + 0x328);
        r8_9 = rdi + 72;
        if (rax8 == r8_9) {
            addr_42997_10:
            *reinterpret_cast<void***>(rsi + 0x390) = rdi;
            return;
        } else {
            if (*reinterpret_cast<int32_t*>(rsi + 0x2f0)) {
                do {
                    addr_42a0c_15:
                    if (!*reinterpret_cast<int32_t*>(rax8 + 0x2f0)) 
                        break;
                    if (reinterpret_cast<signed char>(rcx5) >= reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax8 + 0x2e8))) 
                        break;
                    rax8 = *reinterpret_cast<void***>(rax8 + 0x328);
                } while (rax8 != r8_9);
                goto addr_42997_10;
            } else {
                do {
                    if (*reinterpret_cast<int32_t*>(rax8 + 0x2f0)) 
                        continue;
                    if (reinterpret_cast<signed char>(rcx5) >= reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax8 + 0x2e8))) 
                        break;
                    rax8 = *reinterpret_cast<void***>(rax8 + 0x328);
                } while (rax8 != r8_9);
                goto addr_42a50_34;
            }
        }
    }
    rdx11 = *reinterpret_cast<void***>(rax8 + 0x320);
    *reinterpret_cast<void***>(rsi + 0x328) = rax8;
    *reinterpret_cast<void***>(rsi + 0x320) = rdx11;
    *reinterpret_cast<void***>(*reinterpret_cast<void***>(rax8 + 0x320) + 0x328) = rsi;
    *reinterpret_cast<void***>(rax8 + 0x320) = rsi;
    *reinterpret_cast<void***>(rsi + 0x390) = rdi;
    return;
    addr_42a50_34:
    goto addr_42997_10;
    addr_42853_21:
    *reinterpret_cast<void***>(rsi + 0x390) = reinterpret_cast<void**>(0);
    __asm__("pxor xmm0, xmm0");
    __asm__("movups [rsi+0x320], xmm0");
    if (rax3) 
        goto addr_428a9_4; else 
        goto addr_4286e_3;
}