static void SSL_SESSION_list_add(SSL_CTX *ctx, SSL_SESSION *s)
{
    SSL_SESSION *next;

    if ((s->next != NULL) && (s->prev != NULL))
        SSL_SESSION_list_remove(ctx, s);

    if (ctx->session_cache_head == NULL) {
        ctx->session_cache_head = s;
        ctx->session_cache_tail = s;
        s->prev = (SSL_SESSION *)&(ctx->session_cache_head);
        s->next = (SSL_SESSION *)&(ctx->session_cache_tail);
    } else {
        if (timeoutcmp(s, ctx->session_cache_head) >= 0) {
            /*
             * if we timeout after (or the same time as) the first
             * session, put us first - usual case
             */
            s->next = ctx->session_cache_head;
            s->next->prev = s;
            s->prev = (SSL_SESSION *)&(ctx->session_cache_head);
            ctx->session_cache_head = s;
        } else if (timeoutcmp(s, ctx->session_cache_tail) < 0) {
            /* if we timeout before the last session, put us last */
            s->prev = ctx->session_cache_tail;
            s->prev->next = s;
            s->next = (SSL_SESSION *)&(ctx->session_cache_tail);
            ctx->session_cache_tail = s;
        } else {
            /*
             * we timeout somewhere in-between - if there is only
             * one session in the cache it will be caught above
             */
            next = ctx->session_cache_head->next;
            while (next != (SSL_SESSION*)&(ctx->session_cache_tail)) {
                if (timeoutcmp(s, next) >= 0) {
                    s->next = next;
                    s->prev = next->prev;
                    next->prev->next = s;
                    next->prev = s;
                    break;
                }
                next = next->next;
            }
        }
    }
    s->owner = ctx;
}