int64_t SSL_SESSION_list_add(int64_t a1, int64_t a2) {
    // 0x42800
    int128_t v1; // 0x42800
    int128_t v2 = v1;
    int64_t * v3 = (int64_t *)(a2 + (int64_t)&g106); // 0x42800
    int64_t v4 = *v3; // 0x42800
    int64_t v5; // 0x42800
    int128_t * v6; // 0x42800
    int64_t v7; // 0x42800
    int64_t v8; // 0x42800
    int64_t result2; // 0x42800
    int64_t v9; // 0x42800
    int64_t v10; // 0x42800
    int64_t v11; // 0x42800
    if (v4 == 0) {
        goto lab_0x428a0;
    } else {
        // 0x42810
        v11 = a2 + (int64_t)&g98;
        int64_t * v12 = (int64_t *)v11; // 0x42810
        int64_t v13 = *v12; // 0x42810
        if (v13 == 0) {
            goto lab_0x428a0;
        } else {
            // 0x42820
            v9 = a1 + 72;
            v10 = a1 + 64;
            if (v4 == v9) {
                if (v13 == v10) {
                    int128_t v14 = __asm_pxor(v2, v2); // 0x42a70
                    __asm_movups(*(int128_t *)v13, v14);
                    int128_t * v15 = (int128_t *)v11;
                    __asm_movups(*v15, v14);
                    v6 = v15;
                    v5 = v13;
                    result2 = v4;
                    v7 = v4;
                    goto lab_0x42876;
                } else {
                    // 0x429e9
                    *(int64_t *)v4 = v13;
                    *(int64_t *)(v13 + (int64_t)&g106) = v4;
                    v8 = *(int64_t *)v10;
                    goto lab_0x42853;
                }
            } else {
                if (v13 == v10) {
                    // 0x42a30
                    *(int64_t *)v13 = v4;
                    *(int64_t *)(v4 + (int64_t)&g98) = v13;
                    v8 = v4;
                } else {
                    // 0x4283a
                    *(int64_t *)(v4 + (int64_t)&g98) = v13;
                    *(int64_t *)(*v12 + (int64_t)&g106) = v4;
                    v8 = *(int64_t *)v10;
                }
                goto lab_0x42853;
            }
        }
    }
  lab_0x428a0:;
    int64_t v16 = a1 + 64;
    int64_t v17 = *(int64_t *)v16; // 0x428a0
    int64_t v18 = v17; // 0x428a7
    if (v17 == 0) {
        // 0x428a0
        v6 = (int128_t *)(a2 + (int64_t)&g98);
        v5 = v16;
        result2 = 0;
        v7 = a1 + 72;
        goto lab_0x42876;
    } else {
        goto lab_0x428a9;
    }
  lab_0x428a9:;
    int32_t v19 = *(int32_t *)(a2 + (int64_t)&g82); // 0x428a9
    int64_t v20 = *(int64_t *)(a2 + (int64_t)&g81); // 0x428af
    int64_t v21 = *(int64_t *)(v18 + (int64_t)&g81); // 0x428b6
    int32_t v22 = *(int32_t *)(v18 + (int64_t)&g82); // 0x428bd
    int64_t result6; // 0x42800
    int64_t * v23; // 0x42800
    int64_t v24; // 0x42800
    int64_t v25; // 0x42800
    int64_t result7; // 0x42800
    int64_t v26; // 0x42800
    if (v19 == 0) {
        if (v20 < v21 || v22 != 0) {
            int64_t v27 = a1 + 72;
            int64_t * v28 = (int64_t *)v27;
            int64_t v29 = *v28; // 0x429a0
            v24 = v29;
            v23 = v28;
            result6 = v27;
            v25 = v29;
            if (*(int32_t *)(v29 + (int64_t)&g82) == 0) {
                goto lab_0x428f9;
            } else {
                goto lab_0x429bb;
            }
        } else {
            goto lab_0x4297a;
        }
    } else {
        if (v20 < v21 == (v22 != 0)) {
            int64_t v30 = a1 + 72; // 0x428de
            int64_t v31 = *(int64_t *)v30; // 0x428de
            v24 = v31;
            if (*(int32_t *)(v31 + (int64_t)&g82) == 0) {
                int64_t result = *(int64_t *)(v18 + (int64_t)&g106); // 0x42a58
                v26 = result;
                result7 = v30;
                if (result == v30) {
                    // 0x42997
                    *(int64_t *)(a2 + (int64_t)&g125) = a1;
                    return result;
                }
                goto lab_0x42a0c;
            } else {
                goto lab_0x428f9;
            }
        } else {
            goto lab_0x4297a;
        }
    }
  lab_0x42876:;
    int128_t v32 = __asm_movq(a2); // 0x42876
    int128_t v33 = __asm_movq(v7); // 0x4287b
    __asm_movups(*(int128_t *)v5, __asm_punpcklqdq(v32, v32));
    int128_t v34 = __asm_punpcklqdq(__asm_movq(v5), v33); // 0x4288d
    *(int64_t *)(a2 + (int64_t)&g125) = a1;
    __asm_movups(*v6, v34);
    return result2;
  lab_0x4297a:
    // 0x4297a
    *v3 = v18;
    *(int64_t *)(v18 + (int64_t)&g98) = a2;
    int64_t result3 = a1 + 64; // 0x42988
    *(int64_t *)(a2 + (int64_t)&g98) = result3;
    *(int64_t *)result3 = a2;
    // 0x42997
    *(int64_t *)(a2 + (int64_t)&g125) = a1;
    return result3;
  lab_0x42853:
    // 0x42853
    *(int64_t *)(a2 + (int64_t)&g125) = 0;
    int128_t v35 = __asm_pxor(v2, v2); // 0x4285e
    int128_t * v36 = (int128_t *)v11;
    __asm_movups(*v36, v35);
    v6 = v36;
    v5 = v10;
    result2 = v8;
    v7 = v9;
    v18 = v8;
    if (v8 != 0) {
        goto lab_0x428a9;
    } else {
        goto lab_0x42876;
    }
  lab_0x428f9:;
    // 0x428f9
    int64_t result8; // 0x42800
    if (v20 < *(int64_t *)(v24 + (int64_t)&g81)) {
        int64_t v37 = a1 + 72; // 0x429bb
        v23 = (int64_t *)v37;
        result6 = v37;
        v25 = v24;
        goto lab_0x429bb;
    } else {
        int64_t v38 = *(int64_t *)(v18 + (int64_t)&g106); // 0x42902
        int64_t result4 = a1 + 72; // 0x42909
        if (v38 == result4) {
            // 0x42997
            *(int64_t *)(a2 + (int64_t)&g125) = a1;
            return result4;
        }
        // 0x42916
        v26 = v38;
        result7 = result4;
        if (v19 == 0) {
            if (*(int32_t *)(v38 + (int64_t)&g82) == 0) {
                // 0x42935
                result8 = v38;
                if (v20 >= *(int64_t *)(v38 + (int64_t)&g81)) {
                    goto lab_0x4293e;
                }
            }
            int64_t result5 = *(int64_t *)(v38 + (int64_t)&g106); // 0x42a40
            while (result5 != result4) {
                int64_t v39 = result5;
                if (*(int32_t *)(v39 + (int64_t)&g82) == 0) {
                    // 0x42935
                    result8 = v39;
                    if (v20 >= *(int64_t *)(v39 + (int64_t)&g81)) {
                        goto lab_0x4293e;
                    }
                }
                // 0x42a40
                result5 = *(int64_t *)(v39 + (int64_t)&g106);
            }
            // 0x42997
            *(int64_t *)(a2 + (int64_t)&g125) = a1;
            return result5;
        }
        goto lab_0x42a0c;
    }
  lab_0x429bb:
    // 0x429bb
    *(int64_t *)(a2 + (int64_t)&g98) = v25;
    *(int64_t *)(v25 + (int64_t)&g106) = a2;
    *v3 = result6;
    *v23 = a2;
    // 0x42997
    *(int64_t *)(a2 + (int64_t)&g125) = a1;
    return result6;
  lab_0x42a0c:;
    int64_t v40 = v26; // 0x42800
    result8 = v40;
    while (*(int32_t *)(v40 + (int64_t)&g82) != 0) {
        // 0x42a23
        result8 = v40;
        if (v20 >= *(int64_t *)(v40 + (int64_t)&g81)) {
            // break -> 0x4293e
            break;
        }
        int64_t v41 = *(int64_t *)(v40 + (int64_t)&g106); // 0x42a00
        v40 = v41;
        if (v41 == result7) {
            // 0x42997
            *(int64_t *)(a2 + (int64_t)&g125) = a1;
            return result7;
        }
        result8 = v40;
    }
  lab_0x4293e:;
    int64_t * v42 = (int64_t *)(result8 + (int64_t)&g98); // 0x4293e
    *v3 = result8;
    *(int64_t *)(a2 + (int64_t)&g98) = *v42;
    *(int64_t *)(*v42 + (int64_t)&g106) = a2;
    *v42 = a2;
    *(int64_t *)(a2 + (int64_t)&g125) = a1;
    return result8;
}