void SSL_CTX_flush_sessions(SSL_CTX *s, long t)
{
    STACK_OF(SSL_SESSION) *sk;
    SSL_SESSION *current;
    unsigned long i;

    if (!CRYPTO_THREAD_write_lock(s->lock))
        return;

    sk = sk_SSL_SESSION_new_null();
    i = lh_SSL_SESSION_get_down_load(s->sessions);
    lh_SSL_SESSION_set_down_load(s->sessions, 0);

    /*
     * Iterate over the list from the back (oldest), and stop
     * when a session can no longer be removed.
     * Add the session to a temporary list to be freed outside
     * the SSL_CTX lock.
     * But still do the remove_session_cb() within the lock.
     */
    while (s->session_cache_tail != NULL) {
        current = s->session_cache_tail;
        if (t == 0 || sess_timedout((time_t)t, current)) {
            lh_SSL_SESSION_delete(s->sessions, current);
            SSL_SESSION_list_remove(s, current);
            current->not_resumable = 1;
            if (s->remove_session_cb != NULL)
                s->remove_session_cb(s, current);
            /*
             * Throw the session on a stack, it's entirely plausible
             * that while freeing outside the critical section, the
             * session could be re-added, so avoid using the next/prev
             * pointers. If the stack failed to create, or the session
             * couldn't be put on the stack, just free it here
             */
            if (sk == NULL || !sk_SSL_SESSION_push(sk, current))
                SSL_SESSION_free(current);
        } else {
            break;
        }
    }

    lh_SSL_SESSION_set_down_load(s->sessions, i);
    CRYPTO_THREAD_unlock(s->lock);

    sk_SSL_SESSION_pop_free(sk, SSL_SESSION_free);
}