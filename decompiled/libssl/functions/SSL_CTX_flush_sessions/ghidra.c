void SSL_CTX_flush_sessions(SSL_CTX *ctx,long tm)

{
  int *piVar1;
  SSL_SESSION *ses;
  undefined *puVar2;
  undefined *puVar3;
  int iVar4;
  long lVar5;
  undefined8 uVar6;
  
  iVar4 = CRYPTO_THREAD_write_lock(*(undefined8 *)(ctx[1].sid_ctx + 0x10));
  if (iVar4 != 0) {
    lVar5 = OPENSSL_sk_new_null();
    uVar6 = OPENSSL_LH_get_down_load(ctx->session_cache_head);
    OPENSSL_LH_set_down_load(ctx->session_cache_head,0);
    ses = (SSL_SESSION *)ctx->session_timeout;
    while ((ses != (SSL_SESSION *)0x0 &&
           ((tm == 0 ||
            ((*(long *)ses[1].krb5_client_princ < tm &&
             (*(int *)(ses[1].krb5_client_princ + 8) == 0))))))) {
      OPENSSL_LH_delete(ctx->session_cache_head,ses);
      puVar2 = *(undefined **)(ses[1].krb5_client_princ + 0x40);
      if ((puVar2 != (undefined *)0x0) &&
         (puVar3 = *(undefined **)(ses[1].krb5_client_princ + 0x38), puVar3 != (undefined *)0x0)) {
        piVar1 = &ctx->session_cache_mode;
        if ((long *)puVar2 == &ctx->session_timeout) {
          if (piVar1 == (int *)puVar3) {
            *(undefined (*) [16])&ctx->session_cache_mode = (undefined  [16])0x0;
          }
          else {
            ctx->session_timeout = (long)puVar3;
            *(long **)(puVar3 + 0x328) = &ctx->session_timeout;
          }
        }
        else if (piVar1 == (int *)puVar3) {
          *(undefined **)&ctx->session_cache_mode = puVar2;
          *(int **)(puVar2 + 800) = piVar1;
        }
        else {
          *(undefined **)(puVar2 + 800) = puVar3;
          *(undefined **)(puVar3 + 0x328) = puVar2;
        }
        *(undefined8 *)(ses[1].krb5_client_princ + 0xa8) = 0;
        *(undefined (*) [16])(ses[1].krb5_client_princ + 0x38) = (undefined  [16])0x0;
      }
      *(undefined4 *)(ses[1].session_id + 0x10) = 1;
      if (*(code **)&ctx->stats != (code *)0x0) {
        (**(code **)&ctx->stats)(ctx,ses);
      }
      if ((lVar5 == 0) || (iVar4 = OPENSSL_sk_push(lVar5,ses), iVar4 == 0)) {
        SSL_SESSION_free(ses);
      }
      ses = (SSL_SESSION *)ctx->session_timeout;
    }
    OPENSSL_LH_set_down_load(ctx->session_cache_head,uVar6);
    CRYPTO_THREAD_unlock(*(undefined8 *)(ctx[1].sid_ctx + 0x10));
    OPENSSL_sk_pop_free(lVar5,SSL_SESSION_free);
    return;
  }
  return;
}