bool SSL_SESSION_print_keylog(BIO *param_1,long param_2)

{
  int iVar1;
  ulong uVar2;
  
  if (param_2 == 0) {
    return false;
  }
  if ((((*(long *)(param_2 + 0x250) != 0) && (*(long *)(param_2 + 8) != 0)) &&
      (iVar1 = BIO_puts(param_1,"RSA "), 0 < iVar1)) &&
     (iVar1 = BIO_puts(param_1,"Session-ID:"), 0 < iVar1)) {
    if (*(long *)(param_2 + 0x250) != 0) {
      uVar2 = 0;
      do {
        iVar1 = BIO_printf(param_1,"%02X",(ulong)*(byte *)(param_2 + 600 + uVar2));
        if (iVar1 < 1) {
          return false;
        }
        uVar2 = uVar2 + 1;
      } while (uVar2 <= *(ulong *)(param_2 + 0x250) && *(ulong *)(param_2 + 0x250) != uVar2);
    }
    iVar1 = BIO_puts(param_1," Master-Key:");
    if (0 < iVar1) {
      if (*(long *)(param_2 + 8) != 0) {
        uVar2 = 0;
        do {
          iVar1 = BIO_printf(param_1,"%02X",(ulong)*(byte *)(param_2 + 0x50 + uVar2));
          if (iVar1 < 1) {
            return false;
          }
          uVar2 = uVar2 + 1;
        } while (uVar2 <= *(ulong *)(param_2 + 8) && *(ulong *)(param_2 + 8) != uVar2);
      }
      iVar1 = BIO_puts(param_1,"\n");
      return 0 < iVar1;
    }
  }
  return false;
}