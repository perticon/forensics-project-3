undefined8 ssl_ctx_srp_ctx_free_intern(long param_1)

{
  ulong uVar1;
  undefined8 *puVar2;
  byte bVar3;
  
  bVar3 = 0;
  if (param_1 != 0) {
    CRYPTO_free(*(void **)(param_1 + 0x338));
    CRYPTO_free(*(void **)(param_1 + 0x380));
    BN_free(*(BIGNUM **)(param_1 + 0x340));
    BN_free(*(BIGNUM **)(param_1 + 0x348));
    BN_free(*(BIGNUM **)(param_1 + 0x350));
    BN_free(*(BIGNUM **)(param_1 + 0x358));
    BN_free(*(BIGNUM **)(param_1 + 0x360));
    BN_free(*(BIGNUM **)(param_1 + 0x368));
    BN_free(*(BIGNUM **)(param_1 + 0x370));
    BN_free(*(BIGNUM **)(param_1 + 0x378));
    *(undefined8 *)(param_1 + 0x318) = 0;
    *(undefined8 *)(param_1 + 0x390) = 0;
    puVar2 = (undefined8 *)(param_1 + 800U & 0xfffffffffffffff8);
    uVar1 = (ulong)(((int)param_1 - (int)puVar2) + 0x398U >> 3);
    for (; uVar1 != 0; uVar1 = uVar1 - 1) {
      *puVar2 = 0;
      puVar2 = puVar2 + (ulong)bVar3 * -2 + 1;
    }
    *(undefined4 *)(param_1 + 0x388) = 0x400;
    return 1;
  }
  return 0;
}