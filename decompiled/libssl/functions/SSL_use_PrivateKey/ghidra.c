int SSL_use_PrivateKey(SSL *ssl,EVP_PKEY *pkey)

{
  dtls1_state_st *pdVar1;
  int iVar2;
  long lVar3;
  long in_FS_OFFSET;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (pkey == (EVP_PKEY *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",0x92,"SSL_use_PrivateKey");
    ERR_set_error(0x14,0xc0102,0);
    iVar2 = 0;
  }
  else {
    pdVar1 = ssl[3].d1;
    lVar3 = ssl_cert_lookup_by_pkey(pkey,&local_28);
    if (lVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0x7e,"ssl_set_pkey");
      ERR_set_error(0x14,0xf7,0);
      iVar2 = 0;
    }
    else if ((*(X509 **)(pdVar1->cookie + local_28 * 0x28 + 0x1c) == (X509 *)0x0) ||
            (iVar2 = X509_check_private_key
                               (*(X509 **)(pdVar1->cookie + local_28 * 0x28 + 0x1c),pkey),
            iVar2 != 0)) {
      EVP_PKEY_free(*(EVP_PKEY **)(pdVar1->cookie + local_28 * 0x28 + 0x24));
      EVP_PKEY_up_ref(pkey);
      *(EVP_PKEY **)(pdVar1->cookie + local_28 * 0x28 + 0x24) = pkey;
      *(uchar **)pdVar1 = pdVar1->cookie + local_28 * 0x28 + 0x1c;
      iVar2 = 1;
    }
  }
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar2;
}