undefined8 ssl3_setup_read_buffer(long param_1)

{
  uint uVar1;
  int iVar2;
  void *pvVar3;
  ulong uVar4;
  undefined8 uVar5;
  
  uVar5 = 1;
  uVar1 = *(uint *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8;
  if (*(long *)(param_1 + 0xc78) == 0) {
    iVar2 = ssl_allow_compression();
    uVar4 = (-(ulong)(uVar1 == 0) & 0xfffffffffffffff8) + 0x4150;
    if (iVar2 != 0) {
      uVar4 = (-(ulong)(uVar1 == 0) & 0xfffffffffffffff8) + 0x4550;
    }
    if (uVar4 <= *(ulong *)(param_1 + 0xc80)) {
      uVar4 = *(ulong *)(param_1 + 0xc80);
    }
    pvVar3 = CRYPTO_malloc((int)uVar4,"ssl/record/ssl3_buffer.c",0x3f);
    if (pvVar3 != (void *)0x0) {
      *(void **)(param_1 + 0xc78) = pvVar3;
      *(ulong *)(param_1 + 0xc88) = uVar4;
      return 1;
    }
    ERR_new();
    uVar5 = 0;
    ERR_set_debug("ssl/record/ssl3_buffer.c",0x45,"ssl3_setup_read_buffer");
    ossl_statem_fatal(param_1,0xffffffff,0xc0100,0);
  }
  return uVar5;
}