undefined8 ssl3_renegotiate_check(long param_1,int param_2)

{
  int iVar1;
  undefined8 uVar2;
  
  if (*(int *)(param_1 + 0x1ac) != 0) {
    iVar1 = RECORD_LAYER_read_pending(param_1 + 0xc58);
    if (((iVar1 == 0) && (iVar1 = RECORD_LAYER_write_pending(param_1 + 0xc58), iVar1 == 0)) &&
       ((param_2 != 0 || (iVar1 = SSL_in_init(param_1), iVar1 == 0)))) {
      ossl_statem_set_renegotiate(param_1);
      *(int *)(param_1 + 0x1b4) = *(int *)(param_1 + 0x1b4) + 1;
      uVar2 = 1;
      *(int *)(param_1 + 0x1b0) = *(int *)(param_1 + 0x1b0) + 1;
      *(undefined4 *)(param_1 + 0x1ac) = 0;
    }
    else {
      uVar2 = 0;
    }
    return uVar2;
  }
  return 0;
}