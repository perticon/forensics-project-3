int ssl3_renegotiate_check(SSL *s, int initok)
{
    int ret = 0;

    if (s->s3.renegotiate) {
        if (!RECORD_LAYER_read_pending(&s->rlayer)
            && !RECORD_LAYER_write_pending(&s->rlayer)
            && (initok || !SSL_in_init(s))) {
            /*
             * if we are the server, and we have sent a 'RENEGOTIATE'
             * message, we need to set the state machine into the renegotiate
             * state.
             */
            ossl_statem_set_renegotiate(s);
            s->s3.renegotiate = 0;
            s->s3.num_renegotiations++;
            s->s3.total_renegotiations++;
            ret = 1;
        }
    }
    return ret;
}