int ossl_quic_write(SSL *s, const void *buf, size_t len, size_t *written)
{
    BIO *wbio = SSL_get_wbio(s);
    int ret;

    if (wbio == NULL)
        return 0;

    s->rwstate = SSL_WRITING;
    ret = BIO_write_ex(wbio, buf, len, written);
    if (ret > 0 || !BIO_should_retry(wbio))
        s->rwstate = SSL_NOTHING;
    return ret;
}