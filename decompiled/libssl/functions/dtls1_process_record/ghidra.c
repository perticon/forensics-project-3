int dtls1_process_record(long param_1,undefined8 param_2)

{
  long lVar1;
  uint uVar2;
  int iVar3;
  int iVar4;
  long lVar5;
  long lVar6;
  long lVar7;
  ulong uVar8;
  ulong uVar9;
  undefined8 uVar10;
  byte bVar11;
  long lVar12;
  size_t len;
  long in_FS_OFFSET;
  void *local_98;
  int local_90;
  undefined local_88 [72];
  long local_40;
  
  lVar7 = *(long *)(param_1 + 0x918);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_98 = (void *)0x0;
  local_90 = 0;
  lVar5 = *(long *)(param_1 + 0x1ca8) + 0xd;
  *(long *)(param_1 + 0x12d0) = lVar5;
  if (0x4540 < *(ulong *)(param_1 + 0x12b0)) {
    ERR_new();
    iVar3 = 0;
    ERR_set_debug("ssl/record/ssl3_record.c",0x612,"dtls1_process_record");
    ossl_statem_fatal(param_1,0x16,0x96,0);
    goto LAB_0015c508;
  }
  *(long *)(param_1 + 0x12c8) = lVar5;
  lVar5 = param_1 + 0x12a8;
  *(ulong *)(param_1 + 0x12b8) = *(ulong *)(param_1 + 0x12b0);
  if (*(long *)(param_1 + 0x860) == 0) {
LAB_0015c4b0:
    len = 0;
  }
  else {
    lVar6 = EVP_MD_CTX_get0_md();
    if (lVar6 == 0) {
      len = 0;
    }
    else {
      uVar2 = EVP_MD_get_size(lVar6);
      len = (size_t)(int)uVar2;
      if (0x40 < uVar2) {
        ERR_new();
        iVar3 = 0;
        ERR_set_debug("ssl/record/ssl3_record.c",0x620,"dtls1_process_record");
        ossl_statem_fatal(param_1,0x50,0x80006,0);
        goto LAB_0015c508;
      }
    }
    if (((*(byte *)(param_1 + 0xa9) & 1) != 0) && (*(long *)(param_1 + 0x860) != 0)) {
      if (*(ulong *)(param_1 + 0x12b8) < len) {
        ERR_new();
        iVar3 = 0;
        ERR_set_debug("ssl/record/ssl3_record.c",0x62b,"dtls1_process_record");
        ossl_statem_fatal(param_1,0x32,0xa0,0);
        goto LAB_0015c508;
      }
      lVar6 = *(long *)(param_1 + 0x12c8);
      lVar12 = *(long *)(param_1 + 0x12b0) - len;
      lVar1 = *(long *)(*(long *)(param_1 + 8) + 0xc0);
      *(long *)(param_1 + 0x12b0) = lVar12;
      iVar3 = (**(code **)(lVar1 + 8))(param_1,lVar5,local_88,0);
      if ((iVar3 == 0) || (iVar3 = CRYPTO_memcmp(local_88,(void *)(lVar6 + lVar12),len), iVar3 != 0)
         ) {
        ERR_new();
        iVar3 = 0;
        ERR_set_debug("ssl/record/ssl3_record.c",0x632,"dtls1_process_record");
        ossl_statem_fatal(param_1,0x14,0x119,0);
        goto LAB_0015c508;
      }
      goto LAB_0015c4b0;
    }
  }
  ERR_set_mark();
  iVar3 = (***(code ***)(*(long *)(param_1 + 8) + 0xc0))(param_1,lVar5,1,0,&local_98,len);
  if (iVar3 == 0) {
    ERR_pop_to_mark();
    iVar4 = ossl_statem_in_error(param_1);
    if (iVar4 == 0) {
LAB_0015c5c0:
      iVar3 = 0;
      *(undefined8 *)(param_1 + 0x12b0) = 0;
      *(undefined8 *)(param_1 + 0x1cb0) = 0;
    }
  }
  else {
    ERR_clear_last_mark();
    if ((((lVar7 == 0) || ((*(byte *)(param_1 + 0xa9) & 1) != 0)) ||
        (*(long *)(param_1 + 0x848) == 0)) ||
       (lVar7 = EVP_MD_CTX_get0_md(*(undefined8 *)(param_1 + 0x860)), lVar7 == 0)) {
      uVar8 = *(ulong *)(param_1 + 0x12b0);
    }
    else {
      iVar3 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 8))(param_1,lVar5,local_88,0);
      if (((iVar3 == 0) || (local_98 == (void *)0x0)) ||
         ((iVar3 = CRYPTO_memcmp(local_88,local_98,len), iVar3 != 0 ||
          (uVar8 = *(ulong *)(param_1 + 0x12b0), len + 0x4400 < uVar8)))) goto LAB_0015c5c0;
    }
    if (*(long *)(param_1 + 0x870) == 0) {
LAB_0015c63a:
      uVar9 = 0x4000;
      if ((*(long *)(param_1 + 0x918) != 0) &&
         (bVar11 = *(char *)(*(long *)(param_1 + 0x918) + 0x368) - 1, bVar11 < 4)) {
        uVar9 = (ulong)(uint)(0x200 << (bVar11 & 0x1f));
      }
      if (uVar8 <= uVar9) {
        iVar3 = 1;
        *(undefined8 *)(param_1 + 0x12c0) = 0;
        *(undefined8 *)(param_1 + 0x1cb0) = 0;
        dtls1_record_bitmap_update(param_1,param_2);
        goto joined_r0x0015c5df;
      }
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x685,"dtls1_process_record");
      uVar10 = 0x92;
    }
    else {
      if (uVar8 < 0x4401) {
        iVar3 = ssl3_do_uncompress(param_1);
        if (iVar3 == 0) {
          ERR_new();
          ERR_set_debug("ssl/record/ssl3_record.c",0x67a,"dtls1_process_record");
          ossl_statem_fatal(param_1,0x1e,0x6b,0);
          goto joined_r0x0015c5df;
        }
        uVar8 = *(ulong *)(param_1 + 0x12b0);
        goto LAB_0015c63a;
      }
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x675,"dtls1_process_record");
      uVar10 = 0x8c;
    }
    iVar3 = 0;
    ossl_statem_fatal(param_1,0x16,uVar10,0);
  }
joined_r0x0015c5df:
  if (local_90 != 0) {
    CRYPTO_free(local_98);
  }
LAB_0015c508:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar3;
}