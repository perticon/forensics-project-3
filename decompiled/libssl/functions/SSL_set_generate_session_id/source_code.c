int SSL_set_generate_session_id(SSL *ssl, GEN_SESSION_CB cb)
{
    if (!CRYPTO_THREAD_write_lock(ssl->lock))
        return 0;
    ssl->generate_session_id = cb;
    CRYPTO_THREAD_unlock(ssl->lock);
    return 1;
}