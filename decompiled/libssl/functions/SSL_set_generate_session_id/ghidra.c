int SSL_set_generate_session_id(SSL *param_1,GEN_SESSION_CB param_2)

{
  int iVar1;
  
  iVar1 = CRYPTO_THREAD_write_lock(*(undefined8 *)&param_1[10].next_proto_negotiated_len);
  if (iVar1 != 0) {
    *(GEN_SESSION_CB *)(param_1[3].sid_ctx + 0x1c) = param_2;
    CRYPTO_THREAD_unlock(*(undefined8 *)&param_1[10].next_proto_negotiated_len);
    iVar1 = 1;
  }
  return iVar1;
}