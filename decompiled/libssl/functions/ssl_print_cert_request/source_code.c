static int ssl_print_cert_request(BIO *bio, int indent, const SSL *ssl,
                                  const unsigned char *msg, size_t msglen)
{
    size_t xlen;
    unsigned int sigalg;

    if (SSL_IS_TLS13(ssl)) {
        if (!ssl_print_hexbuf(bio, indent, "request_context", 1, &msg, &msglen))
            return 0;
        if (!ssl_print_extensions(bio, indent, 1,
                                  SSL3_MT_CERTIFICATE_REQUEST, &msg, &msglen))
            return 0;
        return 1;
    } else {
        if (msglen < 1)
            return 0;
        xlen = msg[0];
        if (msglen < xlen + 1)
            return 0;
        msg++;
        BIO_indent(bio, indent, 80);
        BIO_printf(bio, "certificate_types (len=%d)\n", (int)xlen);
        if (!ssl_trace_list(bio, indent + 2, msg, xlen, 1, ssl_ctype_tbl))
            return 0;
        msg += xlen;
        msglen -= xlen + 1;
    }
    if (SSL_USE_SIGALGS(ssl)) {
        if (msglen < 2)
            return 0;
        xlen = (msg[0] << 8) | msg[1];
        if (msglen < xlen + 2 || (xlen & 1))
            return 0;
        msg += 2;
        msglen -= xlen + 2;
        BIO_indent(bio, indent, 80);
        BIO_printf(bio, "signature_algorithms (len=%d)\n", (int)xlen);
        while (xlen > 0) {
            BIO_indent(bio, indent + 2, 80);
            sigalg = (msg[0] << 8) | msg[1];
            BIO_printf(bio, "%s (0x%04x)\n",
                       ssl_trace_str(sigalg, ssl_sigalg_tbl), sigalg);
            xlen -= 2;
            msg += 2;
        }
        msg += xlen;
    }

    if (msglen < 2)
        return 0;
    xlen = (msg[0] << 8) | msg[1];
    BIO_indent(bio, indent, 80);
    if (msglen < xlen + 2)
        return 0;
    msg += 2;
    msglen -= 2 + xlen;
    BIO_printf(bio, "certificate_authorities (len=%d)\n", (int)xlen);
    while (xlen > 0) {
        size_t dlen;
        X509_NAME *nm;
        const unsigned char *p;
        if (xlen < 2)
            return 0;
        dlen = (msg[0] << 8) | msg[1];
        if (xlen < dlen + 2)
            return 0;
        msg += 2;
        BIO_indent(bio, indent + 2, 80);
        BIO_printf(bio, "DistinguishedName (len=%d): ", (int)dlen);
        p = msg;
        nm = d2i_X509_NAME(NULL, &p, dlen);
        if (!nm) {
            BIO_puts(bio, "<UNPARSEABLE DN>\n");
        } else {
            X509_NAME_print_ex(bio, nm, 0, XN_FLAG_ONELINE);
            BIO_puts(bio, "\n");
            X509_NAME_free(nm);
        }
        xlen -= dlen + 2;
        msg += dlen;
    }
    if (SSL_IS_TLS13(ssl)) {
        if (!ssl_print_hexbuf(bio, indent, "request_extensions", 2,
                              &msg, &msglen))
            return 0;
    }
    return msglen == 0;
}