SSL_client_hello_get0_ext(long param_1,int param_2,undefined8 *param_3,undefined8 *param_4)

{
  long lVar1;
  undefined8 *puVar2;
  long lVar3;
  
  lVar1 = *(long *)(param_1 + 0xb58);
  if ((lVar1 != 0) && (*(long *)(lVar1 + 0x280) != 0)) {
    puVar2 = *(undefined8 **)(lVar1 + 0x288);
    lVar3 = 0;
    do {
      if ((*(int *)(puVar2 + 2) != 0) && (*(int *)(puVar2 + 3) == param_2)) {
        if (param_3 != (undefined8 *)0x0) {
          *param_3 = *puVar2;
        }
        if (param_4 == (undefined8 *)0x0) {
          return 1;
        }
        *param_4 = puVar2[1];
        return 1;
      }
      lVar3 = lVar3 + 1;
      puVar2 = puVar2 + 5;
    } while (lVar3 != *(long *)(lVar1 + 0x280));
  }
  return 0;
}