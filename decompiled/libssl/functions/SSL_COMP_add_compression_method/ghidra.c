int SSL_COMP_add_compression_method(int id,COMP_METHOD *cm)

{
  int iVar1;
  int *ptr;
  undefined8 uVar2;
  
  if (cm == (COMP_METHOD *)0x0) {
    return 1;
  }
  iVar1 = COMP_get_type(cm);
  if (iVar1 != 0) {
    if (0x3e < id - 0xc1U) {
      ERR_new();
      ERR_set_debug("ssl/ssl_ciph.c",0x7fa,"SSL_COMP_add_compression_method");
      ERR_set_error(0x14,0x133,0);
      return 1;
    }
    ptr = (int *)CRYPTO_malloc(0x18,"ssl/ssl_ciph.c",0x7fe);
    if (ptr == (int *)0x0) {
      ERR_new();
      uVar2 = 0x800;
    }
    else {
      *ptr = id;
      *(COMP_METHOD **)(ptr + 4) = cm;
      CRYPTO_THREAD_run_once(&ssl_load_builtin_comp_once,do_load_builtin_compressions_ossl_);
      if (ssl_comp_methods != 0) {
        iVar1 = OPENSSL_sk_find(ssl_comp_methods,ptr);
        if (-1 < iVar1) {
          CRYPTO_free(ptr);
          ERR_new();
          ERR_set_debug("ssl/ssl_ciph.c",0x809,"SSL_COMP_add_compression_method");
          ERR_set_error(0x14,0x135,0);
          return 1;
        }
        if ((ssl_comp_methods != 0) && (iVar1 = OPENSSL_sk_push(ssl_comp_methods,ptr), iVar1 != 0))
        {
          return 0;
        }
      }
      CRYPTO_free(ptr);
      ERR_new();
      uVar2 = 0x80e;
    }
    ERR_set_debug("ssl/ssl_ciph.c",uVar2,"SSL_COMP_add_compression_method");
    ERR_set_error(0x14,0xc0100,0);
  }
  return 1;
}