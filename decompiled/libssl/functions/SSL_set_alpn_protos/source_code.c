int SSL_set_alpn_protos(SSL *ssl, const unsigned char *protos,
                        unsigned int protos_len)
{
    unsigned char *alpn;

    if (protos_len == 0 || protos == NULL) {
        OPENSSL_free(ssl->ext.alpn);
        ssl->ext.alpn = NULL;
        ssl->ext.alpn_len = 0;
        return 0;
    }
    /* Not valid per RFC */
    if (!alpn_value_ok(protos, protos_len))
        return 1;

    alpn = OPENSSL_memdup(protos, protos_len);
    if (alpn == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        return 1;
    }
    OPENSSL_free(ssl->ext.alpn);
    ssl->ext.alpn = alpn;
    ssl->ext.alpn_len = protos_len;

    return 0;
}