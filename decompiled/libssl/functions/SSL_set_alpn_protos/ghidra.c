undefined8 SSL_set_alpn_protos(long param_1,long param_2,uint param_3)

{
  long lVar1;
  uint uVar2;
  
  if ((param_3 != 0) && (param_2 != 0)) {
    if (1 < param_3) {
      uVar2 = 0;
      do {
        if (*(byte *)(param_2 + (ulong)uVar2) == 0) {
          return 1;
        }
        uVar2 = uVar2 + 1 + (uint)*(byte *)(param_2 + (ulong)uVar2);
      } while (uVar2 < param_3);
      if (param_3 == uVar2) {
        lVar1 = CRYPTO_memdup(param_2,(ulong)param_3,"ssl/ssl_lib.c",0xc05);
        if (lVar1 != 0) {
          CRYPTO_free(*(void **)(param_1 + 0xb08));
          *(long *)(param_1 + 0xb08) = lVar1;
          *(ulong *)(param_1 + 0xb10) = (ulong)param_3;
          return 0;
        }
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0xc07,"SSL_set_alpn_protos");
        ERR_set_error(0x14,0xc0100,0);
      }
    }
    return 1;
  }
  CRYPTO_free(*(void **)(param_1 + 0xb08));
  *(undefined8 *)(param_1 + 0xb08) = 0;
  *(undefined8 *)(param_1 + 0xb10) = 0;
  return 0;
}