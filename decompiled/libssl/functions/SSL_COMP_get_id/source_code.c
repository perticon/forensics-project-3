int SSL_COMP_get_id(const SSL_COMP *comp)
{
#ifndef OPENSSL_NO_COMP
    return comp->id;
#else
    return -1;
#endif
}