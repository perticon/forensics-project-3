bool cmd_SignatureAlgorithms(long param_1,void *param_2)

{
  long lVar1;
  
  if (*(SSL **)(param_1 + 0x20) != (SSL *)0x0) {
    lVar1 = SSL_ctrl(*(SSL **)(param_1 + 0x20),0x62,0,param_2);
    return 0 < (int)lVar1;
  }
  lVar1 = SSL_CTX_ctrl(*(SSL_CTX **)(param_1 + 0x18),0x62,0,param_2);
  return 0 < (int)lVar1;
}