int ssl_security_cert(long param_1,undefined8 param_2,undefined8 param_3,int param_4,int param_5)

{
  undefined4 uVar1;
  int iVar2;
  uint uVar3;
  int iVar4;
  long lVar5;
  long in_FS_OFFSET;
  undefined4 local_4c;
  int local_48;
  int local_44;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_5 == 0) {
    lVar5 = X509_get0_pubkey(param_3);
    uVar1 = 0xffffffff;
    if (lVar5 != 0) {
      uVar1 = EVP_PKEY_get_security_bits(lVar5);
    }
    if (param_1 == 0) {
      iVar2 = ssl_ctx_security(param_2,(-(uint)(param_4 == 0) & 0xfffff000) + 0x61011,uVar1,0);
    }
    else {
      iVar2 = ssl_security(param_1);
    }
    iVar4 = 0x18d;
  }
  else {
    lVar5 = X509_get0_pubkey(param_3);
    uVar1 = 0xffffffff;
    if (lVar5 != 0) {
      uVar1 = EVP_PKEY_get_security_bits(lVar5);
    }
    if (param_1 == 0) {
      iVar2 = ssl_ctx_security(param_2,(-(uint)(param_4 == 0) & 0xfffff000) + 0x61010,uVar1,0);
    }
    else {
      iVar2 = ssl_security(param_1);
    }
    iVar4 = 399;
  }
  if (iVar2 != 0) {
    uVar3 = X509_get_extension_flags(param_3);
    iVar4 = 1;
    if ((uVar3 & 0x2000) == 0) {
      iVar2 = X509_get_signature_info(param_3,&local_48,&local_44,&local_4c,0);
      if (iVar2 == 0) {
        local_4c = 0xffffffff;
      }
      if (local_48 == 0) {
        local_48 = local_44;
      }
      if (param_1 == 0) {
        iVar2 = ssl_ctx_security(param_2,(-(uint)(param_4 == 0) & 0xfffff000) + 0x61012,local_4c,
                                 local_48,param_3);
      }
      else {
        iVar2 = ssl_security(param_1);
      }
      iVar4 = (-(uint)(iVar2 == 0) & 0x18d) + 1;
    }
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}