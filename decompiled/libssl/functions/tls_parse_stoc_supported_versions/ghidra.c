undefined8 tls_parse_stoc_supported_versions(undefined4 *param_1,short **param_2,int param_3)

{
  short sVar1;
  short *psVar2;
  
  if ((short *)0x1 < param_2[1]) {
    sVar1 = **param_2;
    psVar2 = param_2[1] + -1;
    *param_2 = *param_2 + 1;
    param_2[1] = psVar2;
    if (psVar2 == (short *)0x0) {
      if (sVar1 == 0x403) {
        if (param_3 != 0x800) {
          *param_1 = 0x304;
        }
        return 1;
      }
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x6c8,"tls_parse_stoc_supported_versions");
      ossl_statem_fatal(param_1,0x2f,0x74,0);
      return 0;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x6bf,"tls_parse_stoc_supported_versions");
  ossl_statem_fatal(param_1,0x32,0x9f,0);
  return 0;
}