bool tls_construct_server_done(long param_1)

{
  int iVar1;
  
  if (*(int *)(param_1 + 0x348) != 0) {
    return true;
  }
  iVar1 = ssl3_digest_cached_records(param_1,0);
  return iVar1 != 0;
}