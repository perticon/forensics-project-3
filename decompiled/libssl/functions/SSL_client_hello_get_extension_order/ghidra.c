undefined8 SSL_client_hello_get_extension_order(long param_1,long param_2,ulong *param_3)

{
  long lVar1;
  int *piVar2;
  long lVar3;
  ulong uVar4;
  int *piVar5;
  
  lVar3 = *(long *)(param_1 + 0xb58);
  if ((lVar3 == 0) || (param_3 == (ulong *)0x0)) {
    return 0;
  }
  lVar1 = *(long *)(lVar3 + 0x280);
  if (lVar1 != 0) {
    piVar5 = (int *)(*(long *)(lVar3 + 0x288) + 0x10);
    uVar4 = 0;
    piVar2 = piVar5;
    do {
      uVar4 = (uVar4 + 1) - (ulong)(*piVar2 == 0);
      piVar2 = piVar2 + 10;
    } while (piVar2 != (int *)(*(long *)(lVar3 + 0x288) + 0x10 + lVar1 * 0x28));
    if (uVar4 != 0) {
      if (param_2 != 0) {
        if (*param_3 < uVar4) {
          return 0;
        }
        lVar3 = 0;
        do {
          if (*piVar5 != 0) {
            if (uVar4 <= *(ulong *)(piVar5 + 4)) {
              return 0;
            }
            *(short *)(param_2 + *(ulong *)(piVar5 + 4) * 2) = (short)piVar5[2];
          }
          lVar3 = lVar3 + 1;
          piVar5 = piVar5 + 10;
        } while (lVar3 != lVar1);
      }
      *param_3 = uVar4;
      return 1;
    }
  }
  *param_3 = 0;
  return 1;
}