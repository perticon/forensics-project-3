int32_t ssl_print_extensions(int64_t rdi, int32_t esi, int32_t edx, signed char cl, struct s9** r8, uint64_t* r9, int64_t a7) {
    int64_t rbp8;
    uint64_t rbx9;
    struct s9* r13_10;
    int32_t v11;
    int32_t v12;
    struct s9** v13;
    uint64_t* v14;
    int32_t eax15;
    uint32_t eax16;
    struct s9* r13_17;
    uint64_t rdi18;
    void** rdx19;
    uint64_t v20;
    uint64_t rdi21;
    uint64_t v22;
    uint64_t v23;
    uint32_t r14d24;
    uint32_t r12d25;
    void* rax26;
    uint32_t ebx27;
    uint32_t r15d28;
    void* v29;
    struct s9* v30;
    struct s10* rax31;
    void** rdx32;
    int64_t rax33;
    void** rdx34;
    void** rcx35;
    void* r14_36;
    void* rbx37;
    void* rbx38;
    void** rdx39;
    uint64_t rax40;
    void* r15_41;
    void* r12_42;
    void** rdx43;
    void** rdx44;
    void** rdx45;
    void** rcx46;
    int64_t r12_47;

    rbp8 = rdi;
    rbx9 = *r9;
    r13_10 = *r8;
    v11 = edx;
    v12 = esi;
    v13 = r8;
    v14 = r9;
    fun_21c80(rdi);
    if (!rbx9) {
        fun_21330(rbp8, "No extensions\n", rbp8, "No extensions\n");
        return 1;
    }
    if (rbx9 <= 1) {
        addr_4d6c0_4:
        eax15 = 0;
    } else {
        eax16 = r13_10->f0;
        r13_17 = reinterpret_cast<struct s9*>(&r13_10->f2);
        __asm__("rol ax, 0x8");
        *reinterpret_cast<uint32_t*>(&rdi18) = *reinterpret_cast<uint16_t*>(&eax16);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx19) = *reinterpret_cast<uint16_t*>(&eax16);
        *reinterpret_cast<int32_t*>(&rdx19 + 4) = 0;
        v20 = rdi18;
        rdi21 = rbx9 - 2;
        v22 = rdi21;
        if (!*reinterpret_cast<uint16_t*>(&eax16)) {
            fun_21330(rbp8, "No extensions\n", rbp8, "No extensions\n");
            *v13 = r13_17;
            *v14 = v22;
            return 1;
        }
        if (v20 > rdi21) 
            goto addr_4d6c0_4; else 
            goto addr_4d5a2_8;
    }
    addr_4d6c2_9:
    return eax15;
    addr_4d5a2_8:
    fun_21df0(rbp8, "extensions, length = %d\n", rdx19);
    v23 = v20;
    while (v23 > 3) {
        r14d24 = r13_17->f2;
        r12d25 = r13_17->f0;
        __asm__("rol r14w, 0x8");
        __asm__("rol r12w, 0x8");
        *reinterpret_cast<uint32_t*>(&rax26) = *reinterpret_cast<uint16_t*>(&r14d24);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
        ebx27 = *reinterpret_cast<uint16_t*>(&r12d25);
        r15d28 = *reinterpret_cast<uint16_t*>(&r14d24);
        v29 = rax26;
        if (reinterpret_cast<uint64_t>(rax26) + 4 > v23) 
            goto addr_4d688_12;
        v30 = r13_17 + 1;
        fun_21c80(rbp8, rbp8);
        rax31 = reinterpret_cast<struct s10*>(0xa3f20);
        do {
            if (ebx27 == rax31->f0) 
                break;
            rax31 = reinterpret_cast<struct s10*>(reinterpret_cast<int64_t>(rax31) + 16);
        } while (!reinterpret_cast<int1_t>(rax31 == 0xa4120));
        goto addr_4d640_16;
        rdx32 = rax31->f8;
        addr_4d647_18:
        fun_21df0(rbp8, "extension_type=%s(%d), length=%d\n", rdx32, rbp8, "extension_type=%s(%d), length=%d\n", rdx32);
        if (*reinterpret_cast<uint16_t*>(&r12d25) <= 51) {
            if (!ebx27) 
                goto addr_4de53_20; else 
                goto addr_4d66c_21;
        }
        if (ebx27 != 0xff01) {
            addr_4de53_20:
            *reinterpret_cast<int32_t*>(&rax33) = v12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdx34) = r15d28;
            *reinterpret_cast<int32_t*>(&rdx34 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx35) = static_cast<int32_t>(rax33 + 4);
            *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
            fun_212c0(rbp8, v30, rdx34, rcx35, rbp8, v30, rdx34, rcx35);
            goto addr_4da48_23;
        } else {
            if (!*reinterpret_cast<uint16_t*>(&r14d24)) 
                break;
            *reinterpret_cast<uint32_t*>(&r14_36) = (r13_17 + 1)->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_36) + 4) = 0;
            rbx37 = r14_36;
            if (!reinterpret_cast<int1_t>(v29 == reinterpret_cast<uint64_t>(r14_36) + 1)) 
                break;
            if (r14_36) 
                goto addr_4d706_27;
        }
        fun_21c80(rbp8, rbp8);
        fun_21330(rbp8, "<EMPTY>\n", rbp8, "<EMPTY>\n");
        goto addr_4da48_23;
        addr_4d706_27:
        if (!v11) {
            fun_21c80(rbp8, rbp8);
            fun_21df0(rbp8, "%s (len=%d): ", "client_verify_data", rbp8, "%s (len=%d): ", "client_verify_data");
        } else {
            if (*reinterpret_cast<uint32_t*>(&rbx37) & 1) 
                break;
            r14_36 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_36) >> 1);
            fun_21c80(rbp8, rbp8);
            fun_21df0(rbp8, "%s (len=%d): ", "client_verify_data", rbp8, "%s (len=%d): ", "client_verify_data");
            if (!r14_36) 
                goto addr_4e248_32;
        }
        *reinterpret_cast<int32_t*>(&rbx38) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx38) + 4) = 0;
        do {
            *reinterpret_cast<uint32_t*>(&rdx39) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(r13_17) + reinterpret_cast<uint64_t>(rbx38) + 5);
            *reinterpret_cast<int32_t*>(&rdx39 + 4) = 0;
            rbx38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx38) + 1);
            fun_21df0(rbp8, "%02X", rdx39, rbp8, "%02X", rdx39);
        } while (reinterpret_cast<uint64_t>(rbx38) < reinterpret_cast<uint64_t>(r14_36));
        fun_21330(rbp8, "\n", rbp8, "\n");
        if (!v11) {
            addr_4da48_23:
            r13_17 = reinterpret_cast<struct s9*>(reinterpret_cast<uint64_t>(v30) + reinterpret_cast<uint64_t>(v29));
            rax40 = v23 - reinterpret_cast<uint64_t>(v29) - 4;
            v23 = rax40;
            if (rax40) 
                continue; else 
                goto addr_4da6c_36;
        } else {
            *reinterpret_cast<int32_t*>(&r15_41) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_41) + 4) = 0;
            fun_21c80(rbp8, rbp8);
            fun_21df0(rbp8, "%s (len=%d): ", "server_verify_data", rbp8, "%s (len=%d): ", "server_verify_data");
            r12_42 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_17) + reinterpret_cast<uint64_t>(r14_36));
            do {
                *reinterpret_cast<uint32_t*>(&rdx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(r12_42) + reinterpret_cast<uint64_t>(r15_41) + 5);
                *reinterpret_cast<int32_t*>(&rdx43 + 4) = 0;
                r15_41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_41) + 1);
                fun_21df0(rbp8, "%02X", rdx43, rbp8, "%02X", rdx43);
            } while (reinterpret_cast<uint64_t>(r15_41) < reinterpret_cast<uint64_t>(r14_36));
        }
        addr_4d800_40:
        fun_21330(rbp8, "\n", rbp8, "\n");
        goto addr_4da48_23;
        addr_4e248_32:
        fun_21330(rbp8, "\n", rbp8, "\n");
        fun_21c80(rbp8, rbp8);
        fun_21df0(rbp8, "%s (len=%d): ", "server_verify_data", rbp8, "%s (len=%d): ", "server_verify_data");
        goto addr_4d800_40;
        addr_4d640_16:
        rdx32 = reinterpret_cast<void**>("UNKNOWN");
        goto addr_4d647_18;
    }
    goto addr_4d6c0_4;
    addr_4d688_12:
    *reinterpret_cast<uint32_t*>(&rdx44) = ebx27;
    *reinterpret_cast<int32_t*>(&rdx44 + 4) = 0;
    fun_21df0(rbp8, "extensions, extype = %d, extlen = %d\n", rdx44, rbp8, "extensions, extype = %d, extlen = %d\n", rdx44);
    *reinterpret_cast<int32_t*>(&rdx45) = *reinterpret_cast<int32_t*>(&v23);
    *reinterpret_cast<int32_t*>(&rdx45 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rcx46) = v12 + 2;
    *reinterpret_cast<int32_t*>(&rcx46 + 4) = 0;
    fun_212c0(rbp8, r13_17, rdx45, rcx46);
    goto addr_4d6c0_4;
    addr_4da6c_36:
    *v13 = r13_17;
    *v14 = v22 - v20;
    eax15 = 1;
    goto addr_4d6c2_9;
    addr_4d66c_21:
    *reinterpret_cast<uint32_t*>(&r12_47) = *reinterpret_cast<uint16_t*>(&r12d25);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_47) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8afdc + r12_47 * 4) + 0x8afdc;
}