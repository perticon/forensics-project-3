int64_t ssl_print_extensions(int64_t a1, int64_t a2, int32_t a3, int64_t a4, int64_t * a5, int64_t * a6, int64_t a7, int64_t a8) {
    // 0x4d520
    function_21c80();
    if (a6 == NULL) {
        // 0x4d828
        function_21330();
        return 1;
    }
    // 0x4d560
    if (a6 < (int64_t *)2) {
        // 0x4d6c2
        return 0;
    }
    // 0x4d56a
    int64_t v1; // 0x4d520
    int16_t v2 = v1;
    int64_t v3 = (int64_t)a5 + 2; // 0x4d56f
    uint64_t v4 = (int64_t)a6 - 2; // 0x4d582
    if (v2 == 0) {
        // 0x4de70
        function_21330();
        *a5 = v3;
        *a6 = v4;
        return 1;
    }
    int64_t v5 = llvm_bswap_i16(v2); // 0x4d573
    if (v4 < v5) {
        // 0x4d6c2
        return 0;
    }
    // 0x4d5a2
    function_21df0();
    int64_t v6 = v5; // 0x4d5b8
    int64_t v7 = v3; // 0x4d5b8
    int64_t v8; // 0x4d520
    int64_t result; // 0x4d520
    int64_t v9; // 0x4d5dd
    int64_t v10; // 0x4d608
    while (true) {
      lab_0x4d5c0:
        // 0x4d5c0
        result = 0;
        if (v6 < 4) {
            // break -> 0x4d6c2
            break;
        }
        int16_t v11 = *(int16_t *)(v7 + 2); // 0x4d5cf
        v9 = llvm_bswap_i16(v11);
        if (v9 + 4 > v6) {
            // 0x4d688
            function_21df0();
            function_212c0();
            result = 0;
            return result;
        }
        int16_t v12 = *(int16_t *)v7; // 0x4d5d4
        uint16_t v13 = llvm_bswap_i16(v12); // 0x4d5e2
        v10 = v7 + 4;
        function_21c80();
        int32_t v14 = v13; // 0x4d628
        int64_t v15 = (int64_t)&g405;
        while (*(int32_t *)v15 != v14) {
            int64_t v16 = v15 + 16; // 0x4d630
            if (v16 == (int64_t)&g407) {
                // break -> 0x4d647
                break;
            }
            v15 = v16;
        }
        // 0x4d647
        function_21df0();
        if (v13 < 52) {
            if (v12 != 0) {
                int32_t v17 = *(int32_t *)(4 * (int64_t)v13 + (int64_t)&g298); // 0x4d677
                return (int64_t)v17 + (int64_t)&g298;
            }
            // 0x4de53
            function_212c0();
            goto lab_0x4da48;
        } else {
            if (v14 != (int32_t)"J\b") {
                // 0x4de53
                function_212c0();
                goto lab_0x4da48;
            } else {
                // 0x4d6e4
                result = 0;
                if (v11 == 0) {
                    // break -> 0x4d6c2
                    break;
                }
                unsigned char v18 = *(char *)v10; // 0x4d6ea
                int64_t v19 = v18; // 0x4d6ea
                result = 0;
                if (v19 + 1 != v9) {
                    // break -> 0x4d6c2
                    break;
                }
                if (v18 == 0) {
                    // 0x4e19f
                    function_21c80();
                    function_21330();
                    goto lab_0x4da48;
                } else {
                    if (a3 == 0) {
                        // 0x4e209
                        function_21c80();
                        function_21df0();
                        v8 = v19;
                        goto lab_0x4d75b;
                    } else {
                        // 0x4d712
                        result = 0;
                        if (v18 % 2 != 0) {
                            // break -> 0x4d6c2
                            break;
                        }
                        // 0x4d717
                        function_21c80();
                        function_21df0();
                        v8 = v19 / 2;
                        if (v18 < 2) {
                            // 0x4e248
                            function_21330();
                            function_21c80();
                            function_21df0();
                            // 0x4d800
                            function_21330();
                            goto lab_0x4da48;
                        } else {
                            goto lab_0x4d75b;
                        }
                    }
                }
            }
        }
    }
    // 0x4d6c2
    return result;
  lab_0x4da48:
    // 0x4da48
    v7 = v10 + v9;
    v6 = v6 - v9 - 4;
    if (v6 == 0) {
        // 0x4da6c
        *a5 = v7;
        *a6 = v4 - v5;
        result = 1;
        return result;
    }
    goto lab_0x4d5c0;
  lab_0x4d75b:;
    uint64_t v20 = v8;
    int64_t v21 = 1; // 0x4d776
    function_21df0();
    int64_t v22 = v21; // 0x4d782
    while (v21 < v20) {
        // 0x4d768
        v21 = v22 + 1;
        function_21df0();
        v22 = v21;
    }
    // 0x4d784
    function_21330();
    if (a3 == 0) {
        goto lab_0x4da48;
    } else {
        // 0x4d79f
        function_21c80();
        function_21df0();
        int64_t v23 = 0; // 0x4d7da
        v23++;
        function_21df0();
        while (v23 < v20) {
            // 0x4d7e0
            v23++;
            function_21df0();
        }
        // 0x4d800
        function_21330();
        goto lab_0x4da48;
    }
}