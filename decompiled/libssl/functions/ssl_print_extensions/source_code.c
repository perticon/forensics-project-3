static int ssl_print_extensions(BIO *bio, int indent, int server,
                                unsigned char mt, const unsigned char **msgin,
                                size_t *msginlen)
{
    size_t extslen, msglen = *msginlen;
    const unsigned char *msg = *msgin;

    BIO_indent(bio, indent, 80);
    if (msglen == 0) {
        BIO_puts(bio, "No extensions\n");
        return 1;
    }
    if (msglen < 2)
        return 0;
    extslen = (msg[0] << 8) | msg[1];
    msglen -= 2;
    msg += 2;
    if (extslen == 0) {
        BIO_puts(bio, "No extensions\n");
        *msgin = msg;
        *msginlen = msglen;
        return 1;
    }
    if (extslen > msglen)
        return 0;
    BIO_printf(bio, "extensions, length = %d\n", (int)extslen);
    msglen -= extslen;
    while (extslen > 0) {
        int extype;
        size_t extlen;
        if (extslen < 4)
            return 0;
        extype = (msg[0] << 8) | msg[1];
        extlen = (msg[2] << 8) | msg[3];
        if (extslen < extlen + 4) {
            BIO_printf(bio, "extensions, extype = %d, extlen = %d\n", extype,
                       (int)extlen);
            BIO_dump_indent(bio, (const char *)msg, extslen, indent + 2);
            return 0;
        }
        msg += 4;
        if (!ssl_print_extension(bio, indent + 2, server, mt, extype, msg,
                                 extlen))
            return 0;
        msg += extlen;
        extslen -= extlen + 4;
    }

    *msgin = msg;
    *msginlen = msglen;
    return 1;
}