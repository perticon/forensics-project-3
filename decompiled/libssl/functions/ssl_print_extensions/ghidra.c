ssl_print_extensions
          (BIO *param_1,int param_2,int param_3,char param_4,ushort **param_5,ulong *param_6)

{
  ushort *bytes;
  ushort *puVar1;
  byte bVar2;
  ulong uVar3;
  long lVar4;
  ushort uVar5;
  ulong uVar6;
  undefined1 *puVar7;
  char *pcVar8;
  ulong uVar9;
  ulong uVar10;
  uint uVar11;
  byte *pbVar12;
  ushort *puVar13;
  ushort *puVar14;
  ushort uVar15;
  ulong uVar16;
  ulong local_90;
  ulong local_48;
  
  uVar3 = *param_6;
  puVar14 = *param_5;
  BIO_indent(param_1,param_2,0x50);
  if (uVar3 == 0) {
    BIO_puts(param_1,"No extensions\n");
    return 1;
  }
  if (1 < uVar3) {
    uVar5 = *puVar14;
    puVar14 = puVar14 + 1;
    uVar5 = uVar5 << 8 | uVar5 >> 8;
    uVar10 = (ulong)uVar5;
    uVar3 = uVar3 - 2;
    if (uVar5 == 0) {
      BIO_puts(param_1,"No extensions\n");
      *param_5 = puVar14;
      *param_6 = uVar3;
      return 1;
    }
    if (uVar10 <= uVar3) {
      BIO_printf(param_1,"extensions, length = %d\n",(ulong)uVar5);
      local_90 = uVar10;
      while (3 < local_90) {
        uVar15 = puVar14[1] << 8 | puVar14[1] >> 8;
        uVar5 = *puVar14 << 8 | *puVar14 >> 8;
        uVar6 = (ulong)uVar15;
        if (local_90 < uVar6 + 4) {
          BIO_printf(param_1,"extensions, extype = %d, extlen = %d\n",(ulong)uVar5);
          BIO_dump_indent(param_1,(char *)puVar14,(int)local_90,param_2 + 2);
          return 0;
        }
        bytes = puVar14 + 2;
        BIO_indent(param_1,param_2 + 2,0x50);
        puVar7 = ssl_exts_tbl;
        do {
          if ((uint)uVar5 == *(uint *)puVar7) {
            pcVar8 = *(char **)((long)puVar7 + 8);
            goto LAB_0014d647;
          }
          puVar7 = (undefined1 *)((long)puVar7 + 0x10);
        } while (puVar7 != ssl_comp_tbl);
        pcVar8 = "UNKNOWN";
LAB_0014d647:
        BIO_printf(param_1,"extension_type=%s(%d), length=%d\n",pcVar8);
        switch(uVar5) {
        case 1:
          if (uVar15 == 0) {
            return 0;
          }
          puVar13 = bytes;
          do {
            bVar2 = *(byte *)puVar13;
            BIO_indent(param_1,param_2 + 4,0x50);
            pcVar8 = "disabled";
            if (((bVar2 != 0) && (pcVar8 = "max_fragment_length := 2^9 (512 bytes)", bVar2 != 1)) &&
               ((pcVar8 = "max_fragment_length := 2^10 (1024 bytes)", bVar2 != 2 &&
                ((pcVar8 = "max_fragment_length := 2^11 (2048 bytes)", bVar2 != 3 &&
                 (pcVar8 = "UNKNOWN", bVar2 == 4)))))) {
              pcVar8 = "max_fragment_length := 2^12 (4096 bytes)";
            }
            puVar13 = (ushort *)((long)puVar13 + 1);
            BIO_printf(param_1,"%s (%d)\n",pcVar8,(ulong)bVar2);
          } while (puVar13 != (ushort *)((long)puVar14 + uVar6 + 4));
          break;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 0xc:
        case 0xe:
        case 0xf:
        case 0x11:
        case 0x12:
        case 0x13:
        case 0x14:
        case 0x15:
        case 0x16:
        case 0x17:
        case 0x18:
        case 0x19:
        case 0x1a:
        case 0x1b:
        case 0x1c:
        case 0x1d:
        case 0x1e:
        case 0x1f:
        case 0x20:
        case 0x21:
        case 0x22:
        case 0x24:
        case 0x25:
        case 0x26:
        case 0x27:
        case 0x28:
        case 0x29:
        case 0x2c:
        case 0x2e:
        case 0x2f:
        case 0x30:
        case 0x31:
        case 0x32:
switchD_0014d67e_caseD_2:
          BIO_dump_indent(param_1,(char *)bytes,(uint)uVar15,param_2 + 4);
          break;
        case 10:
          if (uVar15 < 2) {
            return 0;
          }
          uVar5 = puVar14[2] >> 8;
          uVar15 = puVar14[2] << 8 | uVar5;
          if (uVar6 != (ulong)uVar15 + 2) {
            return 0;
          }
          if ((uVar5 & 1) != 0) {
            return 0;
          }
          if (uVar15 != 0) {
            puVar14 = puVar14 + 3;
            puVar13 = (ushort *)((ulong)uVar15 + (long)puVar14);
            do {
              uVar5 = *puVar14;
              BIO_indent(param_1,param_2 + 4,0x50);
              puVar7 = ssl_groups_tbl;
              do {
                uVar11 = (uint)(ushort)(uVar5 << 8 | uVar5 >> 8);
                if (uVar11 == *(uint *)puVar7) {
                  pcVar8 = *(char **)((long)puVar7 + 8);
                  goto LAB_0014dd89;
                }
                puVar7 = (undefined1 *)((long)puVar7 + 0x10);
              } while (puVar7 != ssl_exts_tbl);
              pcVar8 = "UNKNOWN";
LAB_0014dd89:
              BIO_printf(param_1,"%s (%d)\n",pcVar8,(ulong)uVar11);
              puVar14 = puVar14 + 1;
            } while (puVar14 != puVar13);
          }
          break;
        case 0xb:
          if (uVar15 == 0) {
            return 0;
          }
          uVar16 = (ulong)*(byte *)(puVar14 + 2);
          if (uVar6 != uVar16 + 1) {
            return 0;
          }
          pbVar12 = (byte *)((long)puVar14 + 5);
          if (uVar16 != 0) {
            do {
              bVar2 = *pbVar12;
              BIO_indent(param_1,param_2 + 4,0x50);
              pcVar8 = "uncompressed";
              if (((bVar2 != 0) && (pcVar8 = "ansiX962_compressed_prime", bVar2 != 1)) &&
                 (pcVar8 = "ansiX962_compressed_char2", bVar2 != 2)) {
                pcVar8 = "UNKNOWN";
              }
              pbVar12 = pbVar12 + 1;
              BIO_printf(param_1,"%s (%d)\n",pcVar8,(ulong)bVar2);
            } while ((byte *)((long)puVar14 + uVar16 + 5) != pbVar12);
          }
          break;
        case 0xd:
          if (uVar15 < 2) {
            return 0;
          }
          uVar5 = puVar14[2] >> 8;
          uVar15 = puVar14[2] << 8 | uVar5;
          if (uVar6 != (ulong)uVar15 + 2) {
            return 0;
          }
          if ((uVar5 & 1) != 0) {
            return 0;
          }
          puVar14 = puVar14 + 3;
          if (uVar15 != 0) {
            puVar13 = (ushort *)((ulong)uVar15 + (long)puVar14);
            do {
              BIO_indent(param_1,param_2 + 4,0x50);
              puVar7 = ssl_sigalg_tbl;
              do {
                if ((uint)(ushort)(*puVar14 << 8 | *puVar14 >> 8) == *(uint *)puVar7) {
                  pcVar8 = *(char **)((long)puVar7 + 8);
                  goto LAB_0014dc2b;
                }
                puVar7 = (undefined1 *)((long)puVar7 + 0x10);
              } while (puVar7 != &DAT_001a3c50);
              pcVar8 = "UNKNOWN";
LAB_0014dc2b:
              puVar14 = puVar14 + 1;
              BIO_printf(param_1,"%s (0x%04x)\n",pcVar8);
            } while (puVar14 != puVar13);
          }
          break;
        case 0x10:
          if (uVar15 < 2) {
            return 0;
          }
          uVar5 = puVar14[2] << 8 | puVar14[2] >> 8;
          uVar16 = (ulong)uVar5;
          if (uVar6 != uVar16 + 2) {
            return 0;
          }
          puVar14 = puVar14 + 3;
          if (uVar5 != 0) {
            do {
              bVar2 = *(byte *)puVar14;
              uVar9 = (ulong)(uint)bVar2;
              pbVar12 = (byte *)((long)puVar14 + 1);
              if (uVar16 < uVar9 + 1) {
                return 0;
              }
              puVar14 = (ushort *)(pbVar12 + uVar9);
              BIO_indent(param_1,param_2 + 4,0x50);
              BIO_write(param_1,pbVar12,(uint)bVar2);
              BIO_puts(param_1,"\n");
              uVar16 = uVar16 + ~uVar9;
            } while (uVar16 != 0);
          }
          break;
        case 0x23:
          if (uVar15 != 0) {
            BIO_indent(param_1,param_2 + 6,0x50);
            BIO_printf(param_1,"%s (len=%d): ","ticket",(ulong)uVar15);
            puVar13 = (ushort *)((long)puVar14 + uVar6);
            do {
              puVar1 = puVar14 + 2;
              puVar14 = (ushort *)((long)puVar14 + 1);
              BIO_printf(param_1,"%02X",(ulong)*(byte *)puVar1);
            } while (puVar14 != puVar13);
LAB_0014d800:
            BIO_puts(param_1,"\n");
          }
          break;
        case 0x2a:
          if (param_4 == '\x04') {
            if (uVar15 != 4) {
              return 0;
            }
            uVar11 = *(uint *)(puVar14 + 2);
            BIO_indent(param_1,param_2 + 4,0x50);
            BIO_printf(param_1,"max_early_data=%u\n",
                       (ulong)(uVar11 >> 0x18 | (uVar11 & 0xff0000) >> 8 | (uVar11 & 0xff00) << 8 |
                              uVar11 << 0x18));
          }
          break;
        case 0x2b:
          if (param_3 == 0) {
            if (uVar15 == 0) {
              return 0;
            }
            uVar16 = (ulong)*(byte *)(puVar14 + 2);
            if (uVar6 != uVar16 + 1) {
              return 0;
            }
            if ((*(byte *)(puVar14 + 2) & 1) != 0) {
              return 0;
            }
            if (uVar16 != 0) {
              puVar13 = (ushort *)((long)puVar14 + 5);
              do {
                uVar5 = *puVar13;
                BIO_indent(param_1,param_2 + 4,0x50);
                pcVar8 = "SSL 3.0";
                uVar5 = uVar5 << 8 | uVar5 >> 8;
                if ((((((uVar5 != 0x300) && (pcVar8 = "TLS 1.0", uVar5 != 0x301)) &&
                      (pcVar8 = "TLS 1.1", uVar5 != 0x302)) &&
                     ((pcVar8 = "TLS 1.2", uVar5 != 0x303 && (pcVar8 = "TLS 1.3", uVar5 != 0x304))))
                    && (pcVar8 = "DTLS 1.0", uVar5 != 0xfeff)) &&
                   ((pcVar8 = "DTLS 1.2", uVar5 != 0xfefd && (pcVar8 = "UNKNOWN", uVar5 == 0x100))))
                {
                  pcVar8 = "DTLS 1.0 (bad)";
                }
                puVar13 = puVar13 + 1;
                BIO_printf(param_1,"%s (%d)\n",pcVar8,(ulong)uVar5);
              } while (puVar13 != (ushort *)((long)(ushort *)((long)puVar14 + 5) + uVar16));
            }
          }
          else {
            if (uVar15 != 2) {
              return 0;
            }
            uVar5 = puVar14[2] << 8 | puVar14[2] >> 8;
            BIO_indent(param_1,param_2 + 6,0x50);
            pcVar8 = "SSL 3.0";
            if ((((uVar5 != 0x300) && (pcVar8 = "TLS 1.0", uVar5 != 0x301)) &&
                (pcVar8 = "TLS 1.1", uVar5 != 0x302)) &&
               (((pcVar8 = "TLS 1.2", uVar5 != 0x303 && (pcVar8 = "TLS 1.3", uVar5 != 0x304)) &&
                ((pcVar8 = "DTLS 1.0", uVar5 != 0xfeff &&
                 ((pcVar8 = "DTLS 1.2", uVar5 != 0xfefd && (pcVar8 = "UNKNOWN", uVar5 == 0x100))))))
               )) {
              pcVar8 = "DTLS 1.0 (bad)";
            }
            BIO_printf(param_1,"%s (%d)\n",pcVar8,(ulong)uVar5);
          }
          break;
        case 0x2d:
          if (uVar15 == 0) {
            return 0;
          }
          uVar16 = (ulong)*(byte *)(puVar14 + 2);
          if (uVar6 != uVar16 + 1) {
            return 0;
          }
          pbVar12 = (byte *)((long)puVar14 + 5);
          if (uVar16 != 0) {
            do {
              bVar2 = *pbVar12;
              BIO_indent(param_1,param_2 + 4,0x50);
              pcVar8 = "psk_ke";
              if ((bVar2 != 0) && (pcVar8 = "psk_dhe_ke", bVar2 != 1)) {
                pcVar8 = "UNKNOWN";
              }
              pbVar12 = pbVar12 + 1;
              BIO_printf(param_1,"%s (%d)\n",pcVar8,(ulong)(uint)bVar2);
            } while ((byte *)((long)puVar14 + uVar16 + 5) != pbVar12);
          }
          break;
        case 0x33:
          if ((param_3 == 0) || (uVar6 != 2)) {
            if (uVar15 < 2) {
              return 0;
            }
            uVar16 = uVar6;
            puVar13 = bytes;
            if (param_3 == 0) {
              uVar16 = (ulong)(ushort)(puVar14[2] << 8 | puVar14[2] >> 8);
              if (uVar6 != uVar16 + 2) {
                return 0;
              }
              puVar13 = puVar14 + 3;
            }
            if (uVar16 != 0) {
              if (uVar16 < 4) {
                return 0;
              }
              do {
                uVar5 = *puVar13;
                uVar9 = (ulong)(ushort)(puVar13[1] << 8 | puVar13[1] >> 8);
                if (uVar16 - 4 < uVar9) {
                  return 0;
                }
                BIO_indent(param_1,param_2 + 6,0x50);
                puVar7 = ssl_groups_tbl;
                do {
                  if ((uint)(ushort)(uVar5 << 8 | uVar5 >> 8) == *(uint *)puVar7) {
                    pcVar8 = *(char **)((long)puVar7 + 8);
                    goto LAB_0014df80;
                  }
                  puVar7 = (undefined1 *)((long)puVar7 + 0x10);
                } while (puVar7 != ssl_exts_tbl);
                pcVar8 = "UNKNOWN";
LAB_0014df80:
                BIO_printf(param_1,"NamedGroup: %s (%d)\n",pcVar8);
                BIO_indent(param_1,param_2 + 6,0x50);
                BIO_printf(param_1,"%s (len=%d): ","key_exchange: ");
                puVar14 = puVar13;
                if (uVar9 != 0) {
                  do {
                    puVar1 = puVar14 + 2;
                    puVar14 = (ushort *)((long)puVar14 + 1);
                    BIO_printf(param_1,"%02X",(ulong)*(byte *)puVar1);
                  } while (puVar14 != (ushort *)((long)puVar13 + uVar9));
                }
                puVar13 = (ushort *)((long)puVar13 + uVar9 + 4);
                BIO_puts(param_1,"\n");
                uVar16 = (uVar16 - 4) - uVar9;
                if (uVar16 == 0) break;
                if (uVar16 < 4) {
                  return 0;
                }
              } while( true );
            }
          }
          else {
            uVar5 = puVar14[2];
            BIO_indent(param_1,param_2 + 6,0x50);
            puVar7 = ssl_groups_tbl;
            do {
              uVar11 = (uint)(ushort)(uVar5 << 8 | uVar5 >> 8);
              if (uVar11 == *(uint *)puVar7) {
                pcVar8 = *(char **)((long)puVar7 + 8);
                goto LAB_0014d8b1;
              }
              puVar7 = (undefined1 *)((long)puVar7 + 0x10);
            } while (puVar7 != ssl_exts_tbl);
            pcVar8 = "UNKNOWN";
LAB_0014d8b1:
            BIO_printf(param_1,"NamedGroup: %s (%d)\n",pcVar8,(ulong)uVar11);
          }
          break;
        default:
          if (uVar5 != 0xff01) goto switchD_0014d67e_caseD_2;
          if (uVar15 == 0) {
            return 0;
          }
          bVar2 = *(byte *)(puVar14 + 2);
          uVar16 = (ulong)bVar2;
          if (uVar6 != uVar16 + 1) {
            return 0;
          }
          if (uVar16 == 0) {
            BIO_indent(param_1,param_2 + 6,0x50);
            BIO_puts(param_1,"<EMPTY>\n");
          }
          else {
            if (param_3 == 0) {
              BIO_indent(param_1,param_2 + 6,0x50);
              BIO_printf(param_1,"%s (len=%d): ","client_verify_data",uVar16);
            }
            else {
              if ((bVar2 & 1) != 0) {
                return 0;
              }
              bVar2 = bVar2 >> 1;
              uVar16 = (ulong)bVar2;
              BIO_indent(param_1,param_2 + 6,0x50);
              BIO_printf(param_1,"%s (len=%d): ","client_verify_data",uVar16);
              if (uVar16 == 0) {
                BIO_puts(param_1,"\n");
                BIO_indent(param_1,param_2 + 6,0x50);
                BIO_printf(param_1,"%s (len=%d): ","server_verify_data",0);
                goto LAB_0014d800;
              }
            }
            local_48 = (ulong)bVar2;
            uVar9 = 0;
            do {
              lVar4 = uVar9 + 5;
              uVar9 = uVar9 + 1;
              BIO_printf(param_1,"%02X",(ulong)*(byte *)((long)puVar14 + lVar4));
            } while (uVar9 < uVar16);
            BIO_puts(param_1,"\n");
            if (param_3 != 0) {
              uVar9 = 0;
              BIO_indent(param_1,param_2 + 6,0x50);
              BIO_printf(param_1,"%s (len=%d): ","server_verify_data",local_48);
              do {
                lVar4 = uVar9 + uVar16 + 5;
                uVar9 = uVar9 + 1;
                BIO_printf(param_1,"%02X",(ulong)*(byte *)((long)puVar14 + lVar4));
              } while (uVar9 < uVar16);
              goto LAB_0014d800;
            }
          }
        }
        puVar14 = (ushort *)((long)bytes + uVar6);
        local_90 = (local_90 - uVar6) - 4;
        if (local_90 == 0) {
          *param_5 = puVar14;
          *param_6 = uVar3 - uVar10;
          return 1;
        }
      }
    }
  }
  return 0;
}