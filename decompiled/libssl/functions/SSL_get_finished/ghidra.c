size_t SSL_get_finished(SSL *s,void *buf,size_t count)

{
  void *pvVar1;
  
  pvVar1 = s->tlsext_opaque_prf_input;
  if (pvVar1 <= count) {
    count = (size_t)pvVar1;
  }
  memcpy(buf,&s->max_cert_list,count);
  return (size_t)pvVar1;
}