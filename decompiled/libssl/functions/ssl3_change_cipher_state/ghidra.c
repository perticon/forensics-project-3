undefined4 ssl3_change_cipher_state(long param_1,uint param_2)

{
  void *__dest;
  long lVar1;
  EVP_CIPHER *cipher;
  void *pvVar2;
  int iVar3;
  long lVar4;
  COMP_CTX *pCVar5;
  long lVar6;
  EVP_CIPHER_CTX *pEVar7;
  size_t __n;
  undefined8 uVar8;
  long lVar9;
  COMP_METHOD *meth;
  void *__src;
  uint local_40;
  
  lVar1 = *(long *)(param_1 + 0x328);
  cipher = *(EVP_CIPHER **)(param_1 + 800);
  if (lVar1 == 0) {
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x6b,"ssl3_change_cipher_state");
    uVar8 = 0xc0103;
  }
  else {
    meth = *(COMP_METHOD **)(param_1 + 0x340);
    if (meth != (COMP_METHOD *)0x0) {
      meth = (COMP_METHOD *)meth->init;
    }
    local_40 = param_2 & 1;
    if (local_40 == 0) {
      pEVar7 = *(EVP_CIPHER_CTX **)(param_1 + 0x878);
      *(undefined4 *)(param_1 + 0x7c) = 1;
      local_40 = 1;
      if (pEVar7 == (EVP_CIPHER_CTX *)0x0) {
        pEVar7 = EVP_CIPHER_CTX_new();
        *(EVP_CIPHER_CTX **)(param_1 + 0x878) = pEVar7;
        if (pEVar7 == (EVP_CIPHER_CTX *)0x0) {
          ERR_new(0);
          uVar8 = 0x9b;
          goto LAB_00127b21;
        }
        EVP_CIPHER_CTX_reset();
        local_40 = 0;
        pEVar7 = *(EVP_CIPHER_CTX **)(param_1 + 0x878);
      }
      lVar4 = ssl_replace_hash(param_1 + 0x890,lVar1);
      if (lVar4 == 0) {
        ERR_new();
        uVar8 = 0xa5;
LAB_00127b21:
        ERR_set_debug("ssl/s3_enc.c",uVar8,"ssl3_change_cipher_state");
        ossl_statem_fatal(param_1,0x50,0xc0100,0);
        return 0;
      }
      COMP_CTX_free(*(COMP_CTX **)(param_1 + 0x868));
      *(undefined8 *)(param_1 + 0x868) = 0;
      if (meth != (COMP_METHOD *)0x0) {
        pCVar5 = COMP_CTX_new(meth);
        *(COMP_CTX **)(param_1 + 0x868) = pCVar5;
        if (pCVar5 == (COMP_CTX *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/s3_enc.c",0xaf,"ssl3_change_cipher_state");
          ossl_statem_fatal(param_1,0x50,0x8e,0);
          return 0;
        }
      }
      RECORD_LAYER_reset_write_sequence(param_1 + 0xc58);
      __dest = (void *)(param_1 + 0x100);
    }
    else {
      pEVar7 = *(EVP_CIPHER_CTX **)(param_1 + 0x848);
      if (pEVar7 == (EVP_CIPHER_CTX *)0x0) {
        pEVar7 = EVP_CIPHER_CTX_new();
        *(EVP_CIPHER_CTX **)(param_1 + 0x848) = pEVar7;
        if (pEVar7 == (EVP_CIPHER_CTX *)0x0) {
          ERR_new(0);
          ERR_set_debug("ssl/s3_enc.c",0x79,"ssl3_change_cipher_state");
          uVar8 = 0xc0100;
          goto LAB_001279ec;
        }
        EVP_CIPHER_CTX_reset();
        local_40 = 0;
        pEVar7 = *(EVP_CIPHER_CTX **)(param_1 + 0x848);
      }
      lVar4 = ssl_replace_hash(param_1 + 0x860,lVar1);
      if (lVar4 == 0) {
        ERR_new();
        ERR_set_debug("ssl/s3_enc.c",0x84,"ssl3_change_cipher_state");
        uVar8 = 0xc0103;
        goto LAB_001279ec;
      }
      COMP_CTX_free(*(COMP_CTX **)(param_1 + 0x870));
      *(undefined8 *)(param_1 + 0x870) = 0;
      if (meth != (COMP_METHOD *)0x0) {
        pCVar5 = COMP_CTX_new(meth);
        *(COMP_CTX **)(param_1 + 0x870) = pCVar5;
        if (pCVar5 == (COMP_CTX *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/s3_enc.c",0x8e,"ssl3_change_cipher_state");
          uVar8 = 0x8e;
          goto LAB_001279ec;
        }
      }
      RECORD_LAYER_reset_read_sequence(param_1 + 0xc58);
      __dest = (void *)(param_1 + 0xb8);
    }
    if (local_40 != 0) {
      EVP_CIPHER_CTX_reset(pEVar7);
    }
    pvVar2 = *(void **)(param_1 + 0x318);
    iVar3 = EVP_MD_get_size(lVar1);
    if (iVar3 < 0) {
      ERR_new();
      ERR_set_debug("ssl/s3_enc.c",0xbf,"ssl3_change_cipher_state");
      uVar8 = 0xc0103;
    }
    else {
      __n = (size_t)iVar3;
      iVar3 = EVP_CIPHER_get_key_length(cipher);
      lVar9 = (long)iVar3;
      iVar3 = EVP_CIPHER_get_iv_length(cipher);
      lVar6 = (long)iVar3;
      lVar4 = __n * 2;
      if ((param_2 == 0x12) || (param_2 == 0x21)) {
        lVar9 = lVar4 + lVar9 * 2;
        __src = pvVar2;
        if ((ulong)(lVar9 + lVar6 * 2) <= *(ulong *)(param_1 + 0x310)) goto LAB_00127867;
      }
      else {
        lVar4 = lVar4 + lVar9;
        lVar9 = lVar4 + lVar9 + lVar6;
        __src = (void *)((long)pvVar2 + __n);
        if ((ulong)(lVar6 + lVar9) <= *(ulong *)(param_1 + 0x310)) {
LAB_00127867:
          memcpy(__dest,__src,__n);
          iVar3 = EVP_CipherInit_ex(pEVar7,cipher,(ENGINE *)0x0,(uchar *)((long)pvVar2 + lVar4),
                                    (uchar *)((long)pvVar2 + lVar9),param_2 & 2);
          if (iVar3 == 0) {
            ERR_new();
            ERR_set_debug("ssl/s3_enc.c",0xe0,"ssl3_change_cipher_state");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            return 0;
          }
          lVar4 = EVP_CIPHER_get0_provider(cipher);
          if ((lVar4 != 0) &&
             (iVar3 = tls_provider_set_tls_params(param_1,pEVar7,cipher,lVar1), iVar3 == 0)) {
            return 0;
          }
          *(undefined4 *)(param_1 + 0x7c) = 0;
          return 1;
        }
      }
      ERR_new();
      ERR_set_debug("ssl/s3_enc.c",0xd9,"ssl3_change_cipher_state");
      uVar8 = 0xc0103;
    }
  }
LAB_001279ec:
  ossl_statem_fatal(param_1,0x50,uVar8,0);
  return 0;
}