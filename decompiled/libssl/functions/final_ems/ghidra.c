int final_ems(long param_1)

{
  int iVar1;
  
  if (((uint)*(ulong *)(param_1 + 0xa8) & 0x1200) != 0x1000) {
    iVar1 = *(int *)(param_1 + 0x38);
    if (((iVar1 == 0) && (*(int *)(param_1 + 0x4d0) != 0)) &&
       ((((byte)(*(ulong *)(param_1 + 0xa8) >> 9) ^ 1) & 1) !=
        (~(byte)*(undefined4 *)(*(long *)(param_1 + 0x918) + 0x388) & 1))) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions.c",0x4af,"final_ems");
      ossl_statem_fatal(param_1,0x28,0x68,0);
    }
    else {
      iVar1 = 1;
    }
    return iVar1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions.c",0x4a5,"final_ems");
  ossl_statem_fatal(param_1,0x28,0x68,0);
  return 0;
}