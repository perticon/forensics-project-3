bool SSL_SESSION_set1_hostname(long param_1,char *param_2)

{
  char *pcVar1;
  
  CRYPTO_free(*(void **)(param_1 + 0x330));
  if (param_2 != (char *)0x0) {
    pcVar1 = CRYPTO_strdup(param_2,"ssl/ssl_sess.c",0x3d3);
    *(char **)(param_1 + 0x330) = pcVar1;
    return pcVar1 != (char *)0x0;
  }
  *(undefined8 *)(param_1 + 0x330) = 0;
  return true;
}