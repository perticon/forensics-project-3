undefined8 tls_get_ticket_from_client(int *param_1,long param_2,undefined8 *param_3)

{
  long lVar1;
  int iVar2;
  undefined8 uVar3;
  
  iVar2 = *param_1;
  *param_3 = 0;
  param_1[0x2a6] = 0;
  if (iVar2 < 0x301) {
    return 2;
  }
  iVar2 = tls_use_ticket();
  if ((iVar2 != 0) && (lVar1 = *(long *)(param_2 + 0x288), *(int *)(lVar1 + 0x100) != 0)) {
    uVar3 = tls_decrypt_ticket(param_1,*(undefined8 *)(lVar1 + 0xf0),*(undefined8 *)(lVar1 + 0xf8),
                               param_2 + 0x30,*(undefined8 *)(param_2 + 0x28),param_3);
    return uVar3;
  }
  return 2;
}