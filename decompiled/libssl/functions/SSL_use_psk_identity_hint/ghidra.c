int SSL_use_psk_identity_hint(SSL *s,char *identity_hint)

{
  dtls1_state_st *pdVar1;
  size_t sVar2;
  char *pcVar3;
  
  if (s == (SSL *)0x0) {
    return 0;
  }
  if (identity_hint != (char *)0x0) {
    sVar2 = strlen(identity_hint);
    if (sVar2 < 0x101) {
      CRYPTO_free(*(void **)((s[3].d1)->rcvd_cookie + 0xfc));
      pdVar1 = s[3].d1;
      pcVar3 = CRYPTO_strdup(identity_hint,"ssl/ssl_lib.c",0x11e2);
      *(char **)(pdVar1->rcvd_cookie + 0xfc) = pcVar3;
      return (int)(*(long *)((s[3].d1)->rcvd_cookie + 0xfc) != 0);
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x11dd,"SSL_use_psk_identity_hint");
    ERR_set_error(0x14,0x92,0);
    return 0;
  }
  CRYPTO_free(*(void **)((s[3].d1)->rcvd_cookie + 0xfc));
  *(undefined8 *)((s[3].d1)->rcvd_cookie + 0xfc) = 0;
  return 1;
}