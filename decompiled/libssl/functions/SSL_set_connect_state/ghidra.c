void SSL_set_connect_state(SSL *s)

{
  s->server = 0;
  s->shutdown = 0;
  ossl_statem_clear();
  s->handshake_func = (_func_3149 *)s->method->ssl_read;
  ssl_clear_cipher_ctx(s);
  ssl_clear_hash_ctx(&s[3].init_buf);
  ssl_clear_hash_ctx(&s[3].s3);
  return;
}