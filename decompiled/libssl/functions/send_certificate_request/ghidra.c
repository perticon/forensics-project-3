bool send_certificate_request(long param_1)

{
  uint uVar1;
  uint uVar2;
  int iVar3;
  bool bVar4;
  
  uVar1 = *(uint *)(param_1 + 0x968);
  bVar4 = false;
  if ((uVar1 & 1) != 0) {
    if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
         (iVar3 = **(int **)(param_1 + 8), 0x303 < iVar3)) && (iVar3 != 0x10000)) &&
       (((uVar1 & 8) != 0 && (*(int *)(param_1 + 0xba8) != 3)))) {
      return false;
    }
    if (((*(int *)(param_1 + 0xbc0) < 1) || (bVar4 = false, (uVar1 & 4) == 0)) &&
       ((uVar2 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20), (uVar2 & 4) == 0 ||
        (bVar4 = false, (uVar1 & 2) != 0)))) {
      bVar4 = (uVar2 & 0x50) == 0;
    }
  }
  return bVar4;
}