int ossl_pqueue_push(OSSL_PQUEUE *pq, void *data, size_t *elem)
{
    size_t n, m;

    if (!ossl_pqueue_reserve(pq, 1))
        return 0;

    n = pq->htop++;
    m = pq->freelist;
    pq->freelist = pq->elements[m].posn;

    pq->heap[n].data = data;
    pq->heap[n].index = m;

    pq->elements[m].posn = n;
#ifndef NDEBUG
    pq->elements[m].used = 1;
#endif
    pqueue_move_down(pq, n);
    if (elem != NULL)
        *elem = m;
    return 1;
}