int ssl_session_cmp(int *param_1,int *param_2)

{
  int iVar1;
  
  if ((*param_1 == *param_2) && (*(size_t *)(param_1 + 0x94) == *(size_t *)(param_2 + 0x94))) {
    iVar1 = memcmp(param_1 + 0x96,param_2 + 0x96,*(size_t *)(param_1 + 0x94));
    return iVar1;
  }
  return 1;
}