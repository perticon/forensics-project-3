void SSL_SESSION_free(SSL_SESSION *ses)

{
  uchar *puVar1;
  int iVar2;
  
  if (ses == (SSL_SESSION *)0x0) {
    return;
  }
  LOCK();
  puVar1 = ses[1].sid_ctx + 0xc;
  iVar2 = *(int *)puVar1;
  *(int *)puVar1 = *(int *)puVar1 + -1;
  if ((iVar2 != 1) && (0 < iVar2 + -1)) {
    return;
  }
  CRYPTO_free_ex_data(2,ses,(CRYPTO_EX_DATA *)(ses[1].krb5_client_princ + 0x28));
  OPENSSL_cleanse(ses->session_id + 8,0x200);
  OPENSSL_cleanse(ses + 1,0x20);
  X509_free(*(X509 **)(ses[1].session_id + 0x18));
  OSSL_STACK_OF_X509_free(*(undefined8 *)&ses[1].sid_ctx_length);
  CRYPTO_free(*(void **)(ses[1].krb5_client_princ + 0x48));
  CRYPTO_free(*(void **)(ses[1].krb5_client_princ + 0x50));
  CRYPTO_free(*(void **)ses[1].session_id);
  CRYPTO_free(*(void **)(ses[1].session_id + 8));
  CRYPTO_free(*(void **)(ses[1].krb5_client_princ + 0x88));
  CRYPTO_free(*(void **)(ses[1].krb5_client_princ + 0x70));
  CRYPTO_free(*(void **)(ses[1].krb5_client_princ + 0x90));
  CRYPTO_THREAD_lock_free(*(undefined8 *)(ses[1].krb5_client_princ + 0xb0));
  CRYPTO_clear_free(ses,0x3a0,"ssl/ssl_sess.c",0x34d);
  return;
}