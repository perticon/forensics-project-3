ulong ssl3_read_n(SSL *param_1,_func_3155 *param_2,_func_3155 *param_3,int param_4,int param_5,
                 _func_3155 **param_6)

{
  byte bVar1;
  BIO *b;
  uint uVar2;
  int iVar3;
  int *piVar4;
  ulong uVar5;
  long lVar6;
  _func_3155 *p_Var7;
  _func_3155 *__n;
  char *__src;
  long lVar8;
  ulong uVar9;
  char *local_50;
  _func_3155 *local_48;
  
  if (param_2 == (_func_3155 *)0x0) {
LAB_00156258:
    uVar9 = 0;
    goto LAB_0015625b;
  }
  uVar9 = param_1[4].mode;
  if (uVar9 == 0) {
    iVar3 = ssl3_setup_read_buffer();
    if (iVar3 == 0) {
      uVar9 = 0xffffffff;
      goto LAB_0015625b;
    }
    uVar9 = param_1[4].mode;
  }
  __n = param_1[4].tlsext_debug_cb;
  uVar5 = (ulong)(-(int)uVar9 - 5U & 7);
  local_50 = (char *)(uVar9 + uVar5);
  if (param_4 == 0) {
    if (__n == (_func_3155 *)0x0) {
      *(ulong *)&param_1[4].max_send_fragment = uVar5;
      *(char **)&param_1[10].first_packet = local_50;
      *(undefined8 *)&param_1[10].max_send_fragment = 0;
    }
    else {
      __src = (char *)(uVar9 + *(long *)&param_1[4].max_send_fragment);
      if ((((uVar5 == 0) || (__n < (_func_3155 *)0x5)) || (*__src != '\x17')) ||
         ((ushort)(*(ushort *)(__src + 3) << 8 | *(ushort *)(__src + 3) >> 8) < 0x80)) {
        *(char **)&param_1[10].first_packet = __src;
        *(undefined8 *)&param_1[10].max_send_fragment = 0;
        lVar8 = 0;
        goto joined_r0x001562b1;
      }
      memmove(local_50,__src,(size_t)__n);
      *(ulong *)&param_1[4].max_send_fragment = uVar5;
      local_50 = (char *)(uVar5 + param_1[4].mode);
      *(char **)&param_1[10].first_packet = local_50;
      *(undefined8 *)&param_1[10].max_send_fragment = 0;
    }
    lVar8 = 0;
    uVar2 = *(uint *)(param_1->method->get_timeout + 0x60) & 8;
    if (uVar2 == 0) goto LAB_00156368;
LAB_0015631f:
    lVar6 = *(long *)&param_1[4].max_send_fragment;
    if (__n == (_func_3155 *)0x0) goto LAB_00156371;
    p_Var7 = (_func_3155 *)0x0;
    if (param_2 <= __n) goto LAB_00156590;
  }
  else {
    __src = *(char **)&param_1[10].first_packet;
    lVar8 = *(long *)&param_1[10].max_send_fragment;
joined_r0x001562b1:
    if (param_5 == 1 && __src != local_50) {
      memmove(local_50,__src,(size_t)(__n + lVar8));
      *(ulong *)&param_1[4].max_send_fragment = uVar5 + lVar8;
      *(char **)&param_1[10].first_packet = local_50;
    }
    uVar2 = *(uint *)(param_1->method->get_timeout + 0x60) & 8;
    if (uVar2 != 0) {
      if ((__n != (_func_3155 *)0x0) || (param_4 == 0)) goto LAB_0015631f;
      goto LAB_00156258;
    }
LAB_00156368:
    lVar6 = *(long *)&param_1[4].max_send_fragment;
    uVar2 = 0;
LAB_00156371:
    if (__n < param_2) {
      p_Var7 = (_func_3155 *)(*(long *)&param_1[4].first_packet - lVar6);
      if (param_2 <= p_Var7) {
        local_48 = param_2;
        if ((uVar2 | *(uint *)&param_1[4].client_CA) != 0) {
          if (param_3 <= param_2) {
            param_3 = param_2;
          }
          local_48 = p_Var7;
          if (param_3 <= p_Var7) {
            local_48 = param_3;
          }
        }
        piVar4 = __errno_location();
LAB_001563c0:
        b = param_1->rbio;
        *piVar4 = 0;
        do {
          if (b == (BIO *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/record/rec_layer_s3.c",0x139,"ssl3_read_n");
            uVar9 = 0xffffffff;
            ossl_statem_fatal(param_1,0x50,0xd3,0);
            param_1[4].tlsext_debug_cb = __n;
            bVar1 = *(byte *)&param_1[3].tlsext_debug_arg;
joined_r0x001564c0:
            if ((((bVar1 & 0x10) != 0) && (((byte)param_1->method->get_timeout[0x60] & 8) == 0)) &&
               (__n + lVar8 == (_func_3155 *)0x0)) {
              ssl3_release_read_buffer(param_1);
            }
            goto LAB_0015625b;
          }
          param_1->rwstate = 3;
          iVar3 = BIO_read(b,__n + lVar8 + (long)local_50,(int)local_48 - (int)__n);
          uVar9 = (ulong)iVar3;
          uVar5 = 0;
          if (-1 < iVar3) {
            uVar5 = uVar9;
          }
          if (iVar3 < 1) {
            iVar3 = BIO_test_flags(param_1->rbio,8);
            if ((iVar3 == 0) && (lVar6 = BIO_ctrl(param_1->rbio,2,0,(void *)0x0), (int)lVar6 != 0))
            {
              if ((*(byte *)&param_1[3].tlsext_debug_cb & 0x80) == 0) {
                ERR_new();
                ERR_set_debug("ssl/record/rec_layer_s3.c",0x134,"ssl3_read_n");
                ossl_statem_fatal(param_1,0x32,0x126,0);
              }
              else {
                SSL_set_shutdown(param_1,2);
                *(undefined4 *)&(param_1->ex_data).field_0xc = 0;
              }
            }
            param_1[4].tlsext_debug_cb = __n;
            bVar1 = *(byte *)&param_1[3].tlsext_debug_arg;
            goto joined_r0x001564c0;
          }
          __n = __n + uVar5;
          if (((byte)param_1->method->get_timeout[0x60] & 8) != 0) goto code_r0x00156419;
          if (param_2 <= __n) goto LAB_00156425;
          b = param_1->rbio;
          *piVar4 = 0;
        } while( true );
      }
      ERR_new();
      ERR_set_debug("ssl/record/rec_layer_s3.c",0x10a,"ssl3_read_n");
      uVar9 = 0xffffffff;
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      goto LAB_0015625b;
    }
LAB_00156590:
    p_Var7 = __n + -(long)param_2;
    __n = param_2;
  }
  param_1[4].tlsext_debug_cb = p_Var7;
  uVar9 = 1;
  *(_func_3155 **)&param_1[10].max_send_fragment = __n + *(long *)&param_1[10].max_send_fragment;
  *(_func_3155 **)&param_1[4].max_send_fragment = __n + lVar6;
  *param_6 = __n;
LAB_0015625b:
  return uVar9 & 0xffffffff;
code_r0x00156419:
  if (__n < param_2) {
    param_2 = __n;
  }
  if (param_2 <= __n) goto LAB_00156425;
  goto LAB_001563c0;
LAB_00156425:
  *(_func_3155 **)&param_1[4].max_send_fragment = param_2 + *(long *)&param_1[4].max_send_fragment;
  uVar9 = 1;
  *(_func_3155 **)&param_1[10].max_send_fragment = param_2 + *(long *)&param_1[10].max_send_fragment
  ;
  param_1[4].tlsext_debug_cb = __n + -(long)param_2;
  param_1->rwstate = 1;
  *param_6 = param_2;
  goto LAB_0015625b;
}