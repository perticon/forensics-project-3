undefined8 tls_get_message_header(undefined4 *param_1,uint *param_2,undefined8 param_3)

{
  byte *pbVar1;
  uint uVar2;
  int iVar3;
  ulong uVar4;
  long lVar5;
  undefined8 extraout_RDX;
  undefined8 uVar6;
  long in_FS_OFFSET;
  int local_2c;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  pbVar1 = *(byte **)(*(long *)(param_1 + 0x22) + 8);
  uVar4 = *(ulong *)(param_1 + 0x26);
  do {
    while (3 < uVar4) {
      iVar3 = param_1[0xe];
      uVar2 = (uint)*pbVar1;
      if ((((iVar3 != 0) || (param_1[0x17] == 1)) || (*pbVar1 != 0)) || (pbVar1[1] != 0)) {
LAB_00175de0:
        *param_2 = uVar2;
        param_1[0xb6] = (uint)*pbVar1;
        iVar3 = RECORD_LAYER_is_sslv2_record(param_1 + 0x316,iVar3,param_3);
        if (iVar3 == 0) {
          *(ulong *)(param_1 + 0xb4) =
               (ulong)pbVar1[1] << 0x10 | (ulong)pbVar1[2] << 8 | (ulong)pbVar1[3];
          lVar5 = *(long *)(*(long *)(param_1 + 0x22) + 8);
          *(undefined8 *)(param_1 + 0x26) = 0;
          *(long *)(param_1 + 0x24) = lVar5 + 4;
          uVar6 = 1;
        }
        else {
          lVar5 = RECORD_LAYER_get_rrec_length(param_1 + 0x316);
          *(long *)(param_1 + 0xb4) = lVar5 + 4;
          uVar6 = *(undefined8 *)(*(long *)(param_1 + 0x22) + 8);
          *(undefined8 *)(param_1 + 0x26) = 4;
          *(undefined8 *)(param_1 + 0x24) = uVar6;
          uVar6 = 1;
        }
        goto LAB_00175e47;
      }
      uVar2 = (uint)pbVar1[2];
      if (pbVar1[2] != 0) {
        uVar2 = 0;
        goto LAB_00175de0;
      }
      if (pbVar1[3] != 0) goto LAB_00175de0;
      *(undefined8 *)(param_1 + 0x26) = 0;
      if (*(code **)(param_1 + 0x130) == (code *)0x0) {
        uVar4 = 0;
        break;
      }
      param_3 = *(undefined8 *)(param_1 + 0x132);
      (**(code **)(param_1 + 0x130))(0,*param_1,0x16,pbVar1,4,param_1);
      uVar4 = *(ulong *)(param_1 + 0x26);
    }
    iVar3 = (**(code **)(*(long *)(param_1 + 2) + 0x68))
                      (param_1,0x16,&local_2c,pbVar1 + uVar4,4 - uVar4,0);
    if (iVar3 < 1) {
      param_1[10] = 3;
      uVar6 = 0;
      goto LAB_00175e47;
    }
    if (local_2c == 0x14) {
      if (((*(long *)(param_1 + 0x26) != 0) || (local_28 != 1)) || (*pbVar1 != 1)) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_lib.c",0x4a1,"tls_get_message_header");
        uVar6 = 0x67;
        goto LAB_00175f40;
      }
      if ((param_1[0x17] != 0) || (uVar6 = 0, (*(byte *)((long)param_1 + 0xa9) & 8) == 0)) {
        lVar5 = *(long *)(param_1 + 0x22);
        *param_2 = 0x101;
        param_1[0xb6] = 0x101;
        *(undefined8 *)(param_1 + 0x26) = 0;
        uVar6 = *(undefined8 *)(lVar5 + 8);
        *(undefined8 *)(param_1 + 0xb4) = 1;
        *(undefined8 *)(param_1 + 0x24) = uVar6;
        uVar6 = 1;
      }
      goto LAB_00175e47;
    }
    if (local_2c != 0x16) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x4b6,"tls_get_message_header");
      uVar6 = 0x85;
LAB_00175f40:
      ossl_statem_fatal(param_1,10,uVar6,0);
      uVar6 = 0;
LAB_00175e47:
      if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
        return uVar6;
      }
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    uVar4 = local_28 + *(long *)(param_1 + 0x26);
    *(ulong *)(param_1 + 0x26) = uVar4;
    param_3 = extraout_RDX;
  } while( true );
}