void SSL3_BUFFER_release(void **param_1)

{
  CRYPTO_free(*param_1);
  *param_1 = (void *)0x0;
  return;
}