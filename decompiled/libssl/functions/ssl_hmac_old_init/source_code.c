int ssl_hmac_old_init(SSL_HMAC *ctx, void *key, size_t len, char *md)
{
    return HMAC_Init_ex(ctx->old_ctx, key, len, EVP_get_digestbyname(md), NULL);
}