undefined8 ssl_cert_select_current(long **param_1,X509 *param_2)

{
  X509 *pXVar1;
  int iVar2;
  X509 **ppXVar3;
  X509 **ppXVar4;
  
  if (param_2 == (X509 *)0x0) {
    return 0;
  }
  ppXVar4 = (X509 **)(param_1 + 4);
  if ((param_2 == (X509 *)param_1[4]) && (ppXVar3 = ppXVar4, param_1[5] != (long *)0x0))
  goto LAB_0012ddd3;
  if (param_2 == (X509 *)param_1[9]) {
    ppXVar3 = (X509 **)(param_1 + 9);
    if (param_1[10] != (long *)0x0) goto LAB_0012ddd3;
    if (param_2 == (X509 *)param_1[0xe]) goto LAB_0012de05;
LAB_0012dd0a:
    if (param_2 != (X509 *)param_1[0x13]) goto LAB_0012dd1e;
LAB_0012de20:
    ppXVar3 = (X509 **)(param_1 + 0x13);
    if (param_1[0x14] != (long *)0x0) goto LAB_0012ddd3;
    if (param_2 == (X509 *)param_1[0x18]) goto LAB_0012de3e;
LAB_0012dd32:
    if (param_2 != (X509 *)param_1[0x1d]) goto LAB_0012dd46;
LAB_0012de5c:
    ppXVar3 = (X509 **)(param_1 + 0x1d);
    if (param_1[0x1e] != (long *)0x0) goto LAB_0012ddd3;
    if (param_2 == (X509 *)param_1[0x22]) goto LAB_0012de7e;
LAB_0012dd5a:
    if (param_2 != (X509 *)param_1[0x27]) goto LAB_0012dd6e;
LAB_0012dea0:
    ppXVar3 = (X509 **)(param_1 + 0x27);
    if (param_1[0x28] != (long *)0x0) goto LAB_0012ddd3;
    pXVar1 = (X509 *)param_1[0x2c];
  }
  else {
    if (param_2 != (X509 *)param_1[0xe]) goto LAB_0012dd0a;
LAB_0012de05:
    ppXVar3 = (X509 **)(param_1 + 0xe);
    if (param_1[0xf] != (long *)0x0) goto LAB_0012ddd3;
    if (param_2 == (X509 *)param_1[0x13]) goto LAB_0012de20;
LAB_0012dd1e:
    if (param_2 != (X509 *)param_1[0x18]) goto LAB_0012dd32;
LAB_0012de3e:
    ppXVar3 = (X509 **)(param_1 + 0x18);
    if (param_1[0x19] != (long *)0x0) goto LAB_0012ddd3;
    if (param_2 == (X509 *)param_1[0x1d]) goto LAB_0012de5c;
LAB_0012dd46:
    if (param_2 != (X509 *)param_1[0x22]) goto LAB_0012dd5a;
LAB_0012de7e:
    ppXVar3 = (X509 **)(param_1 + 0x22);
    if (param_1[0x23] != (long *)0x0) goto LAB_0012ddd3;
    if (param_2 == (X509 *)param_1[0x27]) goto LAB_0012dea0;
LAB_0012dd6e:
    pXVar1 = (X509 *)param_1[0x2c];
  }
  if ((pXVar1 != param_2) || (ppXVar3 = (X509 **)(param_1 + 0x2c), param_1[0x2d] == (long *)0x0)) {
    while (((ppXVar4[1] == (X509 *)0x0 || (*ppXVar4 == (X509 *)0x0)) ||
           (iVar2 = X509_cmp(*ppXVar4,param_2), iVar2 != 0))) {
      ppXVar4 = ppXVar4 + 5;
      if (ppXVar4 == (X509 **)(param_1 + 0x31)) {
        return 0;
      }
    }
    *param_1 = (long *)ppXVar4;
    return 1;
  }
LAB_0012ddd3:
  *param_1 = (long *)ppXVar3;
  return 1;
}