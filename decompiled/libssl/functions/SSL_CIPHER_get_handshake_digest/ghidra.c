EVP_MD * SSL_CIPHER_get_handshake_digest(long param_1)

{
  char *name;
  EVP_MD *pEVar1;
  
  if (*(byte *)(param_1 + 0x40) < 0xe) {
    name = OBJ_nid2sn(*(int *)(ssl_cipher_table_mac + (ulong)*(byte *)(param_1 + 0x40) * 8 + 4));
    pEVar1 = EVP_get_digestbyname(name);
    return pEVar1;
  }
  return (EVP_MD *)0x0;
}