undefined8 tls_construct_ctos_padding(long param_1,undefined8 param_2)

{
  int *piVar1;
  long lVar2;
  int iVar3;
  size_t sVar4;
  long lVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  long in_FS_OFFSET;
  void *local_30;
  size_t local_28;
  long local_20;
  
  uVar7 = 2;
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*(byte *)(param_1 + 0x9e8) & 0x10) == 0) goto LAB_00162260;
  iVar3 = WPACKET_get_total_written(param_2,&local_28);
  if (iVar3 == 0) {
    ERR_new();
    uVar6 = 0x3aa;
  }
  else {
    piVar1 = *(int **)(param_1 + 0x918);
    sVar4 = local_28;
    if ((((*piVar1 == 0x304) && (*(long *)(piVar1 + 0xd0) != 0)) && (*(long *)(piVar1 + 0xbe) != 0))
       && (lVar5 = ssl_md(*(undefined8 *)(param_1 + 0x9a8),
                          *(undefined4 *)(*(long *)(piVar1 + 0xbe) + 0x40)), sVar4 = local_28,
          lVar5 != 0)) {
      lVar2 = *(long *)(*(long *)(param_1 + 0x918) + 0x340);
      iVar3 = EVP_MD_get_size(lVar5);
      sVar4 = lVar2 + 0xf + local_28 + (long)iVar3;
    }
    uVar7 = 1;
    if (0xff < sVar4 - 0x100) goto LAB_00162260;
    local_28 = 0x1fc - sVar4;
    if (0x200 - sVar4 < 5) {
      local_28 = 1;
    }
    iVar3 = WPACKET_put_bytes__(param_2,0x15,2);
    if ((iVar3 != 0) &&
       (iVar3 = WPACKET_sub_allocate_bytes__(param_2,local_28,&local_30,2), iVar3 != 0)) {
      memset(local_30,0,local_28);
      goto LAB_00162260;
    }
    ERR_new();
    uVar6 = 0x3d2;
  }
  uVar7 = 0;
  ERR_set_debug("ssl/statem/extensions_clnt.c",uVar6,"tls_construct_ctos_padding");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
LAB_00162260:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar7;
}