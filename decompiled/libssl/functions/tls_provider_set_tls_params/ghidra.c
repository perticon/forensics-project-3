int tls_provider_set_tls_params
              (long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4)

{
  int iVar1;
  ulong uVar2;
  long in_FS_OFFSET;
  undefined8 local_d8;
  undefined8 uStack208;
  undefined8 local_c8;
  undefined8 uStack192;
  undefined8 local_b8;
  long local_a0;
  undefined8 local_98;
  undefined8 uStack144;
  undefined8 local_88;
  undefined8 uStack128;
  undefined8 local_78;
  undefined8 local_70;
  undefined8 uStack104;
  undefined8 local_60;
  undefined8 uStack88;
  undefined8 local_50;
  undefined8 local_48;
  undefined8 uStack64;
  undefined8 local_38;
  undefined8 uStack48;
  undefined8 local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_a0 = 0;
  uVar2 = EVP_CIPHER_get_flags(param_3);
  if (((uVar2 & 0x200000) == 0) && (*(int *)(param_1 + 0xb2c) == 0)) {
    iVar1 = EVP_MD_get_size(param_4);
    if (-1 < iVar1) {
      local_a0 = (long)iVar1;
    }
  }
  OSSL_PARAM_construct_int(&local_d8,"tls-version",param_1);
  local_98 = local_d8;
  uStack144 = uStack208;
  local_88 = local_c8;
  uStack128 = uStack192;
  local_78 = local_b8;
  OSSL_PARAM_construct_size_t(local_d8,local_c8,&local_d8,"tls-mac-size",&local_a0);
  local_50 = local_b8;
  local_70 = local_d8;
  uStack104 = uStack208;
  local_60 = local_c8;
  uStack88 = uStack192;
  OSSL_PARAM_construct_end(&local_d8);
  local_28 = local_b8;
  local_48 = local_d8;
  uStack64 = uStack208;
  local_38 = local_c8;
  uStack48 = uStack192;
  iVar1 = EVP_CIPHER_CTX_set_params(param_2,&local_98);
  if (iVar1 == 0) {
    ERR_new();
    ERR_set_debug("ssl/t1_enc.c",0x83,"tls_provider_set_tls_params");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    iVar1 = 1;
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}