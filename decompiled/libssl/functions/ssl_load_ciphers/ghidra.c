undefined8 ssl_load_ciphers(undefined8 *param_1)

{
  uint uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  undefined4 uVar8;
  undefined4 uVar9;
  undefined4 uVar10;
  undefined4 uVar11;
  undefined4 uVar12;
  undefined4 uVar13;
  int iVar14;
  long lVar15;
  EVP_PKEY_ASN1_METHOD *pEVar16;
  undefined8 uVar17;
  long lVar18;
  long in_FS_OFFSET;
  int local_3c;
  ENGINE *local_38;
  long local_30;
  
  lVar18 = 0;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  *(undefined4 *)(param_1 + 200) = 0;
LAB_00130d3a:
  do {
    if (*(int *)(ssl_cipher_table_cipher + lVar18 * 8 + 4) != 0) {
      lVar15 = ssl_evp_cipher_fetch
                         (*param_1,*(int *)(ssl_cipher_table_cipher + lVar18 * 8 + 4),param_1[0x88])
      ;
      param_1[lVar18 + 0x90] = lVar15;
      if (lVar15 == 0) {
        lVar15 = lVar18 * 8;
        lVar18 = lVar18 + 1;
        *(uint *)(param_1 + 200) =
             *(uint *)(param_1 + 200) | *(uint *)(ssl_cipher_table_cipher + lVar15);
        if (lVar18 == 0x18) break;
        goto LAB_00130d3a;
      }
    }
    lVar18 = lVar18 + 1;
  } while (lVar18 != 0x18);
  lVar18 = 0;
  *(undefined4 *)((long)param_1 + 0x644) = 0;
  do {
    while( true ) {
      lVar15 = ssl_evp_md_fetch(*param_1,*(undefined4 *)(ssl_cipher_table_mac + lVar18 * 8 + 4),
                                param_1[0x88]);
      param_1[lVar18 + 0xa8] = lVar15;
      if (lVar15 != 0) break;
      lVar15 = lVar18 * 8;
      lVar18 = lVar18 + 1;
      *(uint *)((long)param_1 + 0x644) =
           *(uint *)((long)param_1 + 0x644) | *(uint *)(ssl_cipher_table_mac + lVar15);
      if (lVar18 == 0xe) goto LAB_00130df2;
    }
    iVar14 = EVP_MD_get_size(lVar15);
    if (iVar14 < 0) {
      uVar17 = 0;
      goto LAB_0013120b;
    }
    param_1[lVar18 + 0xb6] = (long)iVar14;
    lVar18 = lVar18 + 1;
  } while (lVar18 != 0xe);
LAB_00130df2:
  param_1[0xc9] = 0;
  ERR_set_mark();
  lVar18 = EVP_SIGNATURE_fetch(*param_1,&DAT_00183553,param_1[0x88]);
  if (lVar18 == 0) {
    *(uint *)((long)param_1 + 0x64c) = *(uint *)((long)param_1 + 0x64c) | 2;
  }
  else {
    EVP_SIGNATURE_free(lVar18);
  }
  lVar18 = EVP_KEYEXCH_fetch(*param_1,&DAT_00183578,param_1[0x88]);
  if (lVar18 == 0) {
    *(uint *)(param_1 + 0xc9) = *(uint *)(param_1 + 0xc9) | 0x102;
  }
  else {
    EVP_KEYEXCH_free(lVar18);
  }
  lVar18 = EVP_KEYEXCH_fetch(*param_1,&DAT_0018357c,param_1[0x88]);
  if (lVar18 == 0) {
    *(uint *)(param_1 + 0xc9) = *(uint *)(param_1 + 0xc9) | 0x84;
  }
  else {
    EVP_KEYEXCH_free(lVar18);
  }
  lVar18 = EVP_SIGNATURE_fetch(*param_1,&DAT_00183551,param_1[0x88]);
  if (lVar18 == 0) {
    *(uint *)((long)param_1 + 0x64c) = *(uint *)((long)param_1 + 0x64c) | 8;
  }
  else {
    EVP_SIGNATURE_free(lVar18);
  }
  ERR_pop_to_mark();
  uVar13 = default_mac_pkey_id._44_4_;
  uVar12 = default_mac_pkey_id._40_4_;
  uVar11 = default_mac_pkey_id._36_4_;
  uVar10 = default_mac_pkey_id._32_4_;
  uVar9 = default_mac_pkey_id._28_4_;
  uVar8 = default_mac_pkey_id._24_4_;
  uVar7 = default_mac_pkey_id._20_4_;
  uVar6 = default_mac_pkey_id._16_4_;
  uVar5 = default_mac_pkey_id._12_4_;
  uVar4 = default_mac_pkey_id._8_4_;
  uVar3 = default_mac_pkey_id._4_4_;
  uVar2 = default_mac_pkey_id._0_4_;
  param_1[0x8f] = default_mac_pkey_id._48_8_;
  *(undefined4 *)(param_1 + 0x89) = uVar2;
  *(undefined4 *)((long)param_1 + 0x44c) = uVar3;
  *(undefined4 *)(param_1 + 0x8a) = uVar4;
  *(undefined4 *)((long)param_1 + 0x454) = uVar5;
  *(undefined4 *)(param_1 + 0x8b) = uVar6;
  *(undefined4 *)((long)param_1 + 0x45c) = uVar7;
  *(undefined4 *)(param_1 + 0x8c) = uVar8;
  *(undefined4 *)((long)param_1 + 0x464) = uVar9;
  *(undefined4 *)(param_1 + 0x8d) = uVar10;
  *(undefined4 *)((long)param_1 + 0x46c) = uVar11;
  *(undefined4 *)(param_1 + 0x8e) = uVar12;
  *(undefined4 *)((long)param_1 + 0x474) = uVar13;
  local_38 = (ENGINE *)0x0;
  local_3c = 0;
  pEVar16 = EVP_PKEY_asn1_find_str(&local_38,"gost-mac",-1);
  if (pEVar16 != (EVP_PKEY_ASN1_METHOD *)0x0) {
    iVar14 = EVP_PKEY_asn1_get0_info
                       (&local_3c,(int *)0x0,(int *)0x0,(char **)0x0,(char **)0x0,pEVar16);
    if (iVar14 < 1) {
      local_3c = 0;
    }
  }
  tls_engine_finish(local_38);
  *(int *)((long)param_1 + 0x454) = local_3c;
  if (local_3c == 0) {
    *(uint *)((long)param_1 + 0x644) = *(uint *)((long)param_1 + 0x644) | 8;
  }
  else {
    param_1[0xb9] = 0x20;
  }
  local_38 = (ENGINE *)0x0;
  local_3c = 0;
  pEVar16 = EVP_PKEY_asn1_find_str(&local_38,"gost-mac-12",-1);
  if (pEVar16 != (EVP_PKEY_ASN1_METHOD *)0x0) {
    iVar14 = EVP_PKEY_asn1_get0_info
                       (&local_3c,(int *)0x0,(int *)0x0,(char **)0x0,(char **)0x0,pEVar16);
    if (iVar14 < 1) {
      local_3c = 0;
    }
  }
  tls_engine_finish(local_38);
  *(int *)((long)param_1 + 0x464) = local_3c;
  if (local_3c == 0) {
    *(uint *)((long)param_1 + 0x644) = *(uint *)((long)param_1 + 0x644) | 0x100;
  }
  else {
    param_1[0xbd] = 0x20;
  }
  local_38 = (ENGINE *)0x0;
  local_3c = 0;
  pEVar16 = EVP_PKEY_asn1_find_str(&local_38,"magma-mac",-1);
  if (pEVar16 != (EVP_PKEY_ASN1_METHOD *)0x0) {
    iVar14 = EVP_PKEY_asn1_get0_info
                       (&local_3c,(int *)0x0,(int *)0x0,(char **)0x0,(char **)0x0,pEVar16);
    if (iVar14 < 1) {
      local_3c = 0;
    }
  }
  tls_engine_finish(local_38);
  *(int *)(param_1 + 0x8f) = local_3c;
  if (local_3c == 0) {
    *(uint *)((long)param_1 + 0x644) = *(uint *)((long)param_1 + 0x644) | 0x400;
  }
  else {
    param_1[0xc2] = 0x20;
  }
  local_38 = (ENGINE *)0x0;
  local_3c = 0;
  pEVar16 = EVP_PKEY_asn1_find_str(&local_38,"kuznyechik-mac",-1);
  if (pEVar16 != (EVP_PKEY_ASN1_METHOD *)0x0) {
    iVar14 = EVP_PKEY_asn1_get0_info
                       (&local_3c,(int *)0x0,(int *)0x0,(char **)0x0,(char **)0x0,pEVar16);
    if (iVar14 < 1) {
      local_3c = 0;
    }
  }
  tls_engine_finish(local_38);
  *(int *)((long)param_1 + 0x47c) = local_3c;
  if (local_3c == 0) {
    *(uint *)((long)param_1 + 0x644) = *(uint *)((long)param_1 + 0x644) | 0x800;
  }
  else {
    param_1[0xc3] = 0x20;
  }
  local_38 = (ENGINE *)0x0;
  local_3c = 0;
  pEVar16 = EVP_PKEY_asn1_find_str(&local_38,"gost2001",-1);
  if (pEVar16 != (EVP_PKEY_ASN1_METHOD *)0x0) {
    iVar14 = EVP_PKEY_asn1_get0_info
                       (&local_3c,(int *)0x0,(int *)0x0,(char **)0x0,(char **)0x0,pEVar16);
    if (iVar14 < 1) {
      local_3c = 0;
    }
  }
  tls_engine_finish(local_38);
  if (local_3c == 0) {
    *(uint *)((long)param_1 + 0x64c) = *(uint *)((long)param_1 + 0x64c) | 0xa0;
  }
  local_38 = (ENGINE *)0x0;
  local_3c = 0;
  pEVar16 = EVP_PKEY_asn1_find_str(&local_38,"gost2012_256",-1);
  if (pEVar16 != (EVP_PKEY_ASN1_METHOD *)0x0) {
    iVar14 = EVP_PKEY_asn1_get0_info
                       (&local_3c,(int *)0x0,(int *)0x0,(char **)0x0,(char **)0x0,pEVar16);
    if (iVar14 < 1) {
      local_3c = 0;
    }
  }
  tls_engine_finish(local_38);
  if (local_3c == 0) {
    *(uint *)((long)param_1 + 0x64c) = *(uint *)((long)param_1 + 0x64c) | 0x80;
  }
  local_38 = (ENGINE *)0x0;
  local_3c = 0;
  pEVar16 = EVP_PKEY_asn1_find_str(&local_38,"gost2012_512",-1);
  if (pEVar16 != (EVP_PKEY_ASN1_METHOD *)0x0) {
    iVar14 = EVP_PKEY_asn1_get0_info
                       (&local_3c,(int *)0x0,(int *)0x0,(char **)0x0,(char **)0x0,pEVar16);
    if (iVar14 < 1) {
      local_3c = 0;
    }
  }
  tls_engine_finish(local_38);
  uVar1 = *(uint *)((long)param_1 + 0x64c);
  if (local_3c == 0) {
    *(uint *)((long)param_1 + 0x64c) = uVar1 | 0x80;
    if ((uVar1 & 0xa0 | 0x80) == 0xa0) {
      *(uint *)(param_1 + 0xc9) = *(uint *)(param_1 + 0xc9) | 0x10;
    }
  }
  else {
    if ((uVar1 & 0xa0) == 0xa0) {
      *(uint *)(param_1 + 0xc9) = *(uint *)(param_1 + 0xc9) | 0x10;
    }
    uVar17 = 1;
    if ((uVar1 & 0x80) == 0) goto LAB_0013120b;
  }
  *(uint *)(param_1 + 0xc9) = *(uint *)(param_1 + 0xc9) | 0x200;
  uVar17 = 1;
LAB_0013120b:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar17;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}