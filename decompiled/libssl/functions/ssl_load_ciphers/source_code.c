int ssl_load_ciphers(SSL_CTX *ctx)
{
    size_t i;
    const ssl_cipher_table *t;
    EVP_KEYEXCH *kex = NULL;
    EVP_SIGNATURE *sig = NULL;

    ctx->disabled_enc_mask = 0;
    for (i = 0, t = ssl_cipher_table_cipher; i < SSL_ENC_NUM_IDX; i++, t++) {
        if (t->nid != NID_undef) {
            const EVP_CIPHER *cipher
                = ssl_evp_cipher_fetch(ctx->libctx, t->nid, ctx->propq);

            ctx->ssl_cipher_methods[i] = cipher;
            if (cipher == NULL)
                ctx->disabled_enc_mask |= t->mask;
        }
    }
    ctx->disabled_mac_mask = 0;
    for (i = 0, t = ssl_cipher_table_mac; i < SSL_MD_NUM_IDX; i++, t++) {
        const EVP_MD *md
            = ssl_evp_md_fetch(ctx->libctx, t->nid, ctx->propq);

        ctx->ssl_digest_methods[i] = md;
        if (md == NULL) {
            ctx->disabled_mac_mask |= t->mask;
        } else {
            int tmpsize = EVP_MD_get_size(md);
            if (!ossl_assert(tmpsize >= 0))
                return 0;
            ctx->ssl_mac_secret_size[i] = tmpsize;
        }
    }

    ctx->disabled_mkey_mask = 0;
    ctx->disabled_auth_mask = 0;

    /*
     * We ignore any errors from the fetches below. They are expected to fail
     * if theose algorithms are not available.
     */
    ERR_set_mark();
    sig = EVP_SIGNATURE_fetch(ctx->libctx, "DSA", ctx->propq);
    if (sig == NULL)
        ctx->disabled_auth_mask |= SSL_aDSS;
    else
        EVP_SIGNATURE_free(sig);
    kex = EVP_KEYEXCH_fetch(ctx->libctx, "DH", ctx->propq);
    if (kex == NULL)
        ctx->disabled_mkey_mask |= SSL_kDHE | SSL_kDHEPSK;
    else
        EVP_KEYEXCH_free(kex);
    kex = EVP_KEYEXCH_fetch(ctx->libctx, "ECDH", ctx->propq);
    if (kex == NULL)
        ctx->disabled_mkey_mask |= SSL_kECDHE | SSL_kECDHEPSK;
    else
        EVP_KEYEXCH_free(kex);
    sig = EVP_SIGNATURE_fetch(ctx->libctx, "ECDSA", ctx->propq);
    if (sig == NULL)
        ctx->disabled_auth_mask |= SSL_aECDSA;
    else
        EVP_SIGNATURE_free(sig);
    ERR_pop_to_mark();

#ifdef OPENSSL_NO_PSK
    ctx->disabled_mkey_mask |= SSL_PSK;
    ctx->disabled_auth_mask |= SSL_aPSK;
#endif
#ifdef OPENSSL_NO_SRP
    ctx->disabled_mkey_mask |= SSL_kSRP;
#endif

    /*
     * Check for presence of GOST 34.10 algorithms, and if they are not
     * present, disable appropriate auth and key exchange
     */
    memcpy(ctx->ssl_mac_pkey_id, default_mac_pkey_id,
           sizeof(ctx->ssl_mac_pkey_id));

    ctx->ssl_mac_pkey_id[SSL_MD_GOST89MAC_IDX] =
        get_optional_pkey_id(SN_id_Gost28147_89_MAC);
    if (ctx->ssl_mac_pkey_id[SSL_MD_GOST89MAC_IDX])
        ctx->ssl_mac_secret_size[SSL_MD_GOST89MAC_IDX] = 32;
    else
        ctx->disabled_mac_mask |= SSL_GOST89MAC;

    ctx->ssl_mac_pkey_id[SSL_MD_GOST89MAC12_IDX] =
        get_optional_pkey_id(SN_gost_mac_12);
    if (ctx->ssl_mac_pkey_id[SSL_MD_GOST89MAC12_IDX])
        ctx->ssl_mac_secret_size[SSL_MD_GOST89MAC12_IDX] = 32;
    else
        ctx->disabled_mac_mask |= SSL_GOST89MAC12;

    ctx->ssl_mac_pkey_id[SSL_MD_MAGMAOMAC_IDX] =
        get_optional_pkey_id(SN_magma_mac);
    if (ctx->ssl_mac_pkey_id[SSL_MD_MAGMAOMAC_IDX])
        ctx->ssl_mac_secret_size[SSL_MD_MAGMAOMAC_IDX] = 32;
    else
        ctx->disabled_mac_mask |= SSL_MAGMAOMAC;

    ctx->ssl_mac_pkey_id[SSL_MD_KUZNYECHIKOMAC_IDX] =
        get_optional_pkey_id(SN_kuznyechik_mac);
    if (ctx->ssl_mac_pkey_id[SSL_MD_KUZNYECHIKOMAC_IDX])
        ctx->ssl_mac_secret_size[SSL_MD_KUZNYECHIKOMAC_IDX] = 32;
    else
        ctx->disabled_mac_mask |= SSL_KUZNYECHIKOMAC;

    if (!get_optional_pkey_id(SN_id_GostR3410_2001))
        ctx->disabled_auth_mask |= SSL_aGOST01 | SSL_aGOST12;
    if (!get_optional_pkey_id(SN_id_GostR3410_2012_256))
        ctx->disabled_auth_mask |= SSL_aGOST12;
    if (!get_optional_pkey_id(SN_id_GostR3410_2012_512))
        ctx->disabled_auth_mask |= SSL_aGOST12;
    /*
     * Disable GOST key exchange if no GOST signature algs are available *
     */
    if ((ctx->disabled_auth_mask & (SSL_aGOST01 | SSL_aGOST12)) ==
        (SSL_aGOST01 | SSL_aGOST12))
        ctx->disabled_mkey_mask |= SSL_kGOST;

    if ((ctx->disabled_auth_mask & SSL_aGOST12) ==  SSL_aGOST12)
        ctx->disabled_mkey_mask |= SSL_kGOST18;

    return 1;
}