static int cmd_DHParameters(SSL_CONF_CTX *cctx, const char *value)
{
    int rv = 0;
    EVP_PKEY *dhpkey = NULL;
    BIO *in = NULL;
    SSL_CTX *sslctx = (cctx->ssl != NULL) ? cctx->ssl->ctx : cctx->ctx;
    OSSL_DECODER_CTX *decoderctx = NULL;

    if (cctx->ctx != NULL || cctx->ssl != NULL) {
        in = BIO_new(BIO_s_file());
        if (in == NULL)
            goto end;
        if (BIO_read_filename(in, value) <= 0)
            goto end;

        decoderctx
            = OSSL_DECODER_CTX_new_for_pkey(&dhpkey, "PEM", NULL, "DH",
                                            OSSL_KEYMGMT_SELECT_DOMAIN_PARAMETERS,
                                            sslctx->libctx, sslctx->propq);
        if (decoderctx == NULL)
            goto end;
        ERR_set_mark();
        while (!OSSL_DECODER_from_bio(decoderctx, in)
               && dhpkey == NULL
               && !BIO_eof(in));
        OSSL_DECODER_CTX_free(decoderctx);

        if (dhpkey == NULL) {
            ERR_clear_last_mark();
            goto end;
        }
        ERR_pop_to_mark();
    } else {
        return 1;
    }

    if (cctx->ctx != NULL) {
        if ((rv = SSL_CTX_set0_tmp_dh_pkey(cctx->ctx, dhpkey)) > 0)
            dhpkey = NULL;
    }
    if (cctx->ssl != NULL) {
        if ((rv = SSL_set0_tmp_dh_pkey(cctx->ssl, dhpkey)) > 0)
            dhpkey = NULL;
    }
 end:
    EVP_PKEY_free(dhpkey);
    BIO_free(in);
    return rv > 0;
}