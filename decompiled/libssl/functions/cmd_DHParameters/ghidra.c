bool cmd_DHParameters(long param_1,void *param_2)

{
  int iVar1;
  BIO_METHOD *type;
  BIO *bp;
  long lVar2;
  long lVar3;
  undefined8 *puVar4;
  bool bVar5;
  long in_FS_OFFSET;
  undefined8 uVar6;
  EVP_PKEY *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = (EVP_PKEY *)0x0;
  if (*(long *)(param_1 + 0x20) == 0) {
    puVar4 = *(undefined8 **)(param_1 + 0x18);
    bVar5 = true;
    if (puVar4 == (undefined8 *)0x0) goto LAB_00133414;
  }
  else {
    puVar4 = *(undefined8 **)(*(long *)(param_1 + 0x20) + 0x9a8);
  }
  type = BIO_s_file();
  bp = BIO_new(type);
  if (bp == (BIO *)0x0) {
LAB_00133400:
    bVar5 = false;
  }
  else {
    uVar6 = 0x133310;
    lVar2 = BIO_ctrl(bp,0x6c,3,param_2);
    if (((int)lVar2 < 1) ||
       (lVar2 = OSSL_DECODER_CTX_new_for_pkey
                          (&local_38,&DAT_00183cc0,0,&DAT_00183578,4,*puVar4,puVar4[0x88],uVar6),
       lVar2 == 0)) goto LAB_00133400;
    ERR_set_mark();
    do {
      iVar1 = OSSL_DECODER_from_bio(lVar2,bp);
      if ((iVar1 != 0) || (local_38 != (EVP_PKEY *)0x0)) break;
      lVar3 = BIO_ctrl(bp,2,0,(void *)0x0);
    } while ((int)lVar3 == 0);
    OSSL_DECODER_CTX_free(lVar2);
    if (local_38 == (EVP_PKEY *)0x0) {
      ERR_clear_last_mark();
      bVar5 = false;
    }
    else {
      ERR_pop_to_mark();
      if (*(long *)(param_1 + 0x18) == 0) {
        iVar1 = 0;
      }
      else {
        iVar1 = SSL_CTX_set0_tmp_dh_pkey(*(long *)(param_1 + 0x18),local_38);
        if (0 < iVar1) {
          local_38 = (EVP_PKEY *)0x0;
        }
      }
      if (*(long *)(param_1 + 0x20) == 0) {
        bVar5 = 0 < iVar1;
      }
      else {
        iVar1 = SSL_set0_tmp_dh_pkey(*(long *)(param_1 + 0x20),local_38);
        if (iVar1 < 1) goto LAB_00133400;
        local_38 = (EVP_PKEY *)0x0;
        bVar5 = true;
      }
    }
  }
  EVP_PKEY_free(local_38);
  BIO_free(bp);
LAB_00133414:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return bVar5;
}