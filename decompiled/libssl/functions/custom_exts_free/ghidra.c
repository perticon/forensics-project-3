void custom_exts_free(void **param_1)

{
  void *pvVar1;
  void *pvVar2;
  void *ptr;
  
  pvVar1 = param_1[1];
  ptr = *param_1;
  if (pvVar1 != (void *)0x0) {
    pvVar2 = (void *)0x0;
    do {
      while (*(code **)((long)ptr + 0x10) == custom_ext_add_old_cb_wrap) {
        pvVar2 = (void *)((long)pvVar2 + 1);
        CRYPTO_free(*(void **)((long)ptr + 0x20));
        CRYPTO_free(*(void **)((long)ptr + 0x30));
        pvVar1 = param_1[1];
        ptr = (void *)((long)ptr + 0x38);
        if (pvVar1 <= pvVar2) goto LAB_00164ca3;
      }
      pvVar2 = (void *)((long)pvVar2 + 1);
      ptr = (void *)((long)ptr + 0x38);
    } while (pvVar2 < pvVar1);
LAB_00164ca3:
    ptr = *param_1;
  }
  CRYPTO_free(ptr);
  return;
}