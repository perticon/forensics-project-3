void custom_exts_free(custom_ext_methods *exts)
{
    size_t i;
    custom_ext_method *meth;

    for (i = 0, meth = exts->meths; i < exts->meths_count; i++, meth++) {
        if (meth->add_cb != custom_ext_add_old_cb_wrap)
            continue;

        /* Old style API wrapper. Need to free the arguments too */
        OPENSSL_free(meth->add_arg);
        OPENSSL_free(meth->parse_arg);
    }
    OPENSSL_free(exts->meths);
}