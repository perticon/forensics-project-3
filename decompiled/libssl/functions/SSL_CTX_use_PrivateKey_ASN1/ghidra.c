int SSL_CTX_use_PrivateKey_ASN1(int pk,SSL_CTX *ctx,uchar *d,long len)

{
  int iVar1;
  EVP_PKEY *pkey;
  undefined4 in_register_0000003c;
  long in_FS_OFFSET;
  uchar *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_28 = d;
  pkey = (EVP_PKEY *)
         d2i_PrivateKey_ex(CONCAT44(in_register_0000003c,pk),0,&local_28,len,ctx->method,
                           ctx[1].tlsext_ticket_key_cb);
  if (pkey == (EVP_PKEY *)0x0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x198,"SSL_CTX_use_PrivateKey_ASN1");
    ERR_set_error(0x14,0x8000d,0);
  }
  else {
    iVar1 = SSL_CTX_use_PrivateKey(ctx,pkey);
    EVP_PKEY_free(pkey);
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}