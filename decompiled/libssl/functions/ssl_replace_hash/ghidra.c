EVP_MD_CTX * ssl_replace_hash(EVP_MD_CTX **param_1,EVP_MD *param_2)

{
  int iVar1;
  EVP_MD_CTX *ctx;
  
  ssl_clear_hash_ctx();
  ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
  *param_1 = ctx;
  if (ctx != (EVP_MD_CTX *)0x0) {
    if (param_2 != (EVP_MD *)0x0) {
      iVar1 = EVP_DigestInit_ex(ctx,param_2,(ENGINE *)0x0);
      ctx = *param_1;
      if (iVar1 < 1) goto LAB_0013b418;
    }
    return ctx;
  }
LAB_0013b418:
  EVP_MD_CTX_free(ctx);
  *param_1 = (EVP_MD_CTX *)0x0;
  return (EVP_MD_CTX *)0x0;
}