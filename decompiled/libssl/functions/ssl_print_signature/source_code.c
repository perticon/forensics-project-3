static int ssl_print_signature(BIO *bio, int indent, const SSL *ssl,
                               const unsigned char **pmsg, size_t *pmsglen)
{
    if (*pmsglen < 2)
        return 0;
    if (SSL_USE_SIGALGS(ssl)) {
        const unsigned char *p = *pmsg;
        unsigned int sigalg = (p[0] << 8) | p[1];

        BIO_indent(bio, indent, 80);
        BIO_printf(bio, "Signature Algorithm: %s (0x%04x)\n",
                   ssl_trace_str(sigalg, ssl_sigalg_tbl), sigalg);
        *pmsg += 2;
        *pmsglen -= 2;
    }
    return ssl_print_hexbuf(bio, indent, "Signature", 2, pmsg, pmsglen);
}