int64_t get_sigorhash(int32_t * a1, int32_t * a2, int64_t * a3) {
    int64_t v1 = (int64_t)a3;
    bool v2; // 0x47a60
    int64_t v3 = v2 ? -1 : 1;
    int64_t v4 = (int64_t)"RSA"; // 0x47a60
    int64_t v5 = v1; // 0x47a60
    int64_t v6 = 4; // 0x47a7a
    unsigned char v7 = *(char *)v5; // 0x47a7a
    char v8 = *(char *)v4; // 0x47a7a
    char v9 = v8; // 0x47a7a
    bool v10 = false; // 0x47a7a
    while (v7 == v8) {
        v6--;
        v4 += v3;
        v5 += v3;
        v9 = v7;
        v10 = true;
        if (v6 == 0) {
            // break -> 
            break;
        }
        v7 = *(char *)v5;
        v8 = *(char *)v4;
        v9 = v8;
        v10 = false;
    }
    unsigned char v11 = v9;
    int64_t v12; // 0x47a60
    int64_t v13 = v12 & -256; // 0x47a7c
    uint64_t v14 = (int64_t)!((v7 < v11 | v10)) - (int64_t)(v7 < v11); // 0x47a7f
    int64_t v15 = (int64_t)"RSA-PSS"; // 0x47a83
    int64_t v16 = v1; // 0x47a83
    int64_t v17 = 8; // 0x47a83
    if ((char)v14 == 0) {
        // 0x47a85
        *a1 = 6;
        // 0x47a8c
        return v14 % 256 | v13;
    }
    unsigned char v18 = *(char *)v16; // 0x47aa7
    char v19 = *(char *)v15; // 0x47aa7
    char v20 = v19; // 0x47aa7
    bool v21 = false; // 0x47aa7
    while (v18 == v19) {
        // 0x47a98
        v17--;
        v15 += v3;
        v16 += v3;
        v20 = v18;
        v21 = true;
        if (v17 == 0) {
            // break -> 
            break;
        }
        v18 = *(char *)v16;
        v19 = *(char *)v15;
        v20 = v19;
        v21 = false;
    }
    unsigned char v22 = v20;
    int64_t v23 = (int64_t)!((v18 < v22 | v21)) - (int64_t)(v18 < v22); // 0x47aac
    int64_t v24 = (int64_t)"PSS"; // 0x47ab0
    int64_t v25 = v1; // 0x47ab0
    int64_t v26 = 4; // 0x47ab0
    if ((char)v23 == 0) {
        // 0x47ab2
        *a1 = (int32_t)&g125;
        return v23 % 256 | v13;
    }
    unsigned char v27 = *(char *)v25; // 0x47acf
    char v28 = *(char *)v24; // 0x47acf
    char v29 = v28; // 0x47acf
    bool v30 = false; // 0x47acf
    while (v27 == v28) {
        // 0x47ac0
        v26--;
        v24 += v3;
        v25 += v3;
        v29 = v27;
        v30 = true;
        if (v26 == 0) {
            // break -> 
            break;
        }
        v27 = *(char *)v25;
        v28 = *(char *)v24;
        v29 = v28;
        v30 = false;
    }
    unsigned char v31 = v29;
    int64_t v32 = (int64_t)!((v27 < v31 | v30)) - (int64_t)(v27 < v31); // 0x47ad4
    int64_t v33 = (int64_t)"DSA"; // 0x47ad8
    int64_t v34 = v1; // 0x47ad8
    int64_t v35 = 4; // 0x47ad8
    if ((char)v32 == 0) {
        // 0x47ab2
        *a1 = (int32_t)&g125;
        return v32 % 256 | v13;
    }
    unsigned char v36 = *(char *)v34; // 0x47ae9
    char v37 = *(char *)v33; // 0x47ae9
    char v38 = v37; // 0x47ae9
    bool v39 = false; // 0x47ae9
    while (v36 == v37) {
        // 0x47ada
        v35--;
        v33 += v3;
        v34 += v3;
        v38 = v36;
        v39 = true;
        if (v35 == 0) {
            // break -> 
            break;
        }
        v36 = *(char *)v34;
        v37 = *(char *)v33;
        v38 = v37;
        v39 = false;
    }
    unsigned char v40 = v38;
    uint64_t v41 = (int64_t)!((v36 < v40 | v39)) - (int64_t)(v36 < v40); // 0x47aee
    int64_t v42 = (int64_t)"ECDSA"; // 0x47af2
    int64_t v43 = v1; // 0x47af2
    int64_t v44 = 6; // 0x47af2
    if ((char)v41 == 0) {
        // 0x47af4
        *a1 = 116;
        // 0x47a8c
        return v41 % 256 | v13;
    }
    unsigned char v45 = *(char *)v43; // 0x47b0f
    char v46 = *(char *)v42; // 0x47b0f
    char v47 = v46; // 0x47b0f
    bool v48 = false; // 0x47b0f
    while (v45 == v46) {
        // 0x47b00
        v44--;
        v42 += v3;
        v43 += v3;
        v47 = v45;
        v48 = true;
        if (v44 == 0) {
            // break -> 
            break;
        }
        v45 = *(char *)v43;
        v46 = *(char *)v42;
        v47 = v46;
        v48 = false;
    }
    unsigned char v49 = v47;
    uint64_t v50 = (int64_t)!((v45 < v49 | v48)) - (int64_t)(v45 < v49); // 0x47b14
    if ((char)v50 == 0) {
        // 0x47b1a
        *a1 = 408;
        // 0x47a8c
        return v50 % 256 | v13;
    }
    int64_t v51 = function_20d30(); // 0x47b38
    int32_t v52 = v51; // 0x47b42
    *a2 = v52;
    int64_t result = v51; // 0x47b46
    if (v52 == 0) {
        // 0x47b4c
        result = function_20db0();
        *a2 = (int32_t)result;
    }
    // 0x47a8c
    return result;
}