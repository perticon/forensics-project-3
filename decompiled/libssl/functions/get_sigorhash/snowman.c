void get_sigorhash(int32_t* rdi, int32_t* rsi, void** rdx, void** rcx) {
    void* rsp5;
    int32_t* r8_6;
    int32_t* rbx7;
    int32_t ecx8;
    void*** rdi9;
    void** rsi10;
    uint1_t cf11;
    int1_t below_or_equal12;
    unsigned char al13;
    uint1_t cf14;
    uint1_t zf15;
    uint1_t below_or_equal16;
    int32_t ecx17;
    void*** rdi18;
    void** rsi19;
    unsigned char al20;
    uint1_t cf21;
    uint1_t zf22;
    uint1_t below_or_equal23;
    int32_t ecx24;
    void*** rdi25;
    void** rsi26;
    unsigned char al27;
    uint1_t cf28;
    uint1_t zf29;
    uint1_t below_or_equal30;
    int32_t ecx31;
    void*** rdi32;
    void** rsi33;
    unsigned char al34;
    uint1_t cf35;
    uint1_t zf36;
    uint1_t below_or_equal37;
    int32_t ecx38;
    void*** rdi39;
    void** rsi40;
    unsigned char al41;
    int32_t eax42;
    int32_t eax43;

    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    r8_6 = rdi;
    rbx7 = rsi;
    ecx8 = 4;
    rdi9 = reinterpret_cast<void***>("RSA");
    rsi10 = rdx;
    cf11 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsp5) < 16);
    below_or_equal12 = reinterpret_cast<uint64_t>(rsp5) <= 16;
    do {
        if (!ecx8) 
            break;
        --ecx8;
        cf11 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi10)) < reinterpret_cast<unsigned char>(*rdi9));
        below_or_equal12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi10)) <= reinterpret_cast<unsigned char>(*rdi9);
        ++rdi9;
        ++rsi10;
    } while (*reinterpret_cast<void***>(rsi10) == *rdi9);
    al13 = reinterpret_cast<uint1_t>(!below_or_equal12);
    cf14 = 0;
    zf15 = reinterpret_cast<uint1_t>(al13 - reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(al13 < reinterpret_cast<unsigned char>(static_cast<uint32_t>(cf11))))) == 0);
    below_or_equal16 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf15));
    if (!zf15) {
        ecx17 = 8;
        rdi18 = reinterpret_cast<void***>("RSA-PSS");
        rsi19 = rdx;
        do {
            if (!ecx17) 
                break;
            --ecx17;
            cf14 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi19)) < reinterpret_cast<unsigned char>(*rdi18));
            below_or_equal16 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi19)) <= reinterpret_cast<unsigned char>(*rdi18));
            ++rdi18;
            ++rsi19;
        } while (*reinterpret_cast<void***>(rsi19) == *rdi18);
        al20 = reinterpret_cast<uint1_t>(!below_or_equal16);
        cf21 = 0;
        zf22 = reinterpret_cast<uint1_t>(al20 - reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(al20 < reinterpret_cast<unsigned char>(static_cast<uint32_t>(cf14))))) == 0);
        below_or_equal23 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf22));
        if (zf22) 
            goto addr_47ab2_9;
    } else {
        *r8_6 = 6;
        goto addr_47a8c_11;
    }
    ecx24 = 4;
    rdi25 = reinterpret_cast<void***>("PSS");
    rsi26 = rdx;
    do {
        if (!ecx24) 
            break;
        --ecx24;
        cf21 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi26)) < reinterpret_cast<unsigned char>(*rdi25));
        below_or_equal23 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi26)) <= reinterpret_cast<unsigned char>(*rdi25));
        ++rdi25;
        ++rsi26;
    } while (*reinterpret_cast<void***>(rsi26) == *rdi25);
    al27 = reinterpret_cast<uint1_t>(!below_or_equal23);
    cf28 = 0;
    zf29 = reinterpret_cast<uint1_t>(al27 - reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(al27 < reinterpret_cast<unsigned char>(static_cast<uint32_t>(cf21))))) == 0);
    below_or_equal30 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf29));
    if (!zf29) 
        goto addr_47ada_16;
    addr_47ab2_9:
    *r8_6 = 0x390;
    return;
    addr_47ada_16:
    ecx31 = 4;
    rdi32 = reinterpret_cast<void***>("DSA");
    rsi33 = rdx;
    do {
        if (!ecx31) 
            break;
        --ecx31;
        cf28 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi33)) < reinterpret_cast<unsigned char>(*rdi32));
        below_or_equal30 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi33)) <= reinterpret_cast<unsigned char>(*rdi32));
        ++rdi32;
        ++rsi33;
    } while (*reinterpret_cast<void***>(rsi33) == *rdi32);
    al34 = reinterpret_cast<uint1_t>(!below_or_equal30);
    cf35 = 0;
    zf36 = reinterpret_cast<uint1_t>(al34 - reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(al34 < reinterpret_cast<unsigned char>(static_cast<uint32_t>(cf28))))) == 0);
    below_or_equal37 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf36));
    if (zf36) 
        goto addr_47af4_20;
    ecx38 = 6;
    rdi39 = reinterpret_cast<void***>("ECDSA");
    rsi40 = rdx;
    do {
        if (!ecx38) 
            break;
        --ecx38;
        cf35 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi40)) < reinterpret_cast<unsigned char>(*rdi39));
        below_or_equal37 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi40)) <= reinterpret_cast<unsigned char>(*rdi39));
        ++rdi39;
        ++rsi40;
    } while (*reinterpret_cast<void***>(rsi40) == *rdi39);
    al41 = reinterpret_cast<uint1_t>(!below_or_equal37);
    if (!(al41 - reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(al41 < reinterpret_cast<unsigned char>(static_cast<uint32_t>(cf35))))))) 
        goto addr_47b1a_25;
    eax42 = fun_20d30(rdx);
    *rbx7 = eax42;
    if (!eax42) {
        eax43 = fun_20db0(rdx);
        *rbx7 = eax43;
    }
    addr_47a8c_11:
    return;
    addr_47b1a_25:
    *r8_6 = 0x198;
    goto addr_47a8c_11;
    addr_47af4_20:
    *r8_6 = 0x74;
    goto addr_47a8c_11;
}