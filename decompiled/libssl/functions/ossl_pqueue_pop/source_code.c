void *ossl_pqueue_pop(OSSL_PQUEUE *pq)
{
    void *res;
    size_t elem;

    if (pq == NULL || pq->htop == 0)
        return NULL;

    ASSERT_USED(pq, 0);
    res = pq->heap->data;
    elem = pq->heap->index;

    if (--pq->htop != 0) {
        pqueue_move_elem(pq, pq->htop, 0);
        pqueue_move_up(pq, 0);
    }

    pq->elements[elem].posn = pq->freelist;
    pq->freelist = elem;
#ifndef NDEBUG
    pq->elements[elem].used = 0;
#endif
    return res;
}