undefined8 tls_construct_ctos_ec_pt_formats(undefined8 param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined4 local_38;
  undefined4 local_34;
  undefined8 local_30;
  undefined8 local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = ssl_get_min_max_version(param_1,&local_38,&local_34,0);
  if (iVar1 == 0) {
    iVar1 = use_ecc(param_1,local_38,local_34);
    uVar2 = 2;
    if (iVar1 == 0) goto LAB_00160538;
    tls1_get_formatlist(param_1,&local_30,&local_28);
    iVar1 = WPACKET_put_bytes__(param_2,0xb,2);
    if (iVar1 != 0) {
      iVar1 = WPACKET_start_sub_packet_len__(param_2,2);
      if (iVar1 != 0) {
        iVar1 = WPACKET_sub_memcpy__(param_2,local_30,local_28,1);
        if (iVar1 != 0) {
          iVar1 = WPACKET_close(param_2);
          if (iVar1 != 0) {
            uVar2 = 1;
            goto LAB_00160538;
          }
        }
      }
    }
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0xb2,"tls_construct_ctos_ec_pt_formats");
    iVar1 = 0xc0103;
  }
  else {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0xa4,"tls_construct_ctos_ec_pt_formats");
  }
  ossl_statem_fatal(param_1,0x50,iVar1,0);
  uVar2 = 0;
LAB_00160538:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}