static int cmd_Ciphersuites(SSL_CONF_CTX *cctx, const char *value)
{
    int rv = 1;

    if (cctx->ctx)
        rv = SSL_CTX_set_ciphersuites(cctx->ctx, value);
    if (cctx->ssl)
        rv = SSL_set_ciphersuites(cctx->ssl, value);
    return rv > 0;
}