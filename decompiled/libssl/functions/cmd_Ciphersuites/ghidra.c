bool cmd_Ciphersuites(long param_1,undefined8 param_2)

{
  int iVar1;
  
  iVar1 = 1;
  if (*(long *)(param_1 + 0x18) != 0) {
    iVar1 = SSL_CTX_set_ciphersuites();
  }
  if (*(long *)(param_1 + 0x20) != 0) {
    iVar1 = SSL_set_ciphersuites(*(long *)(param_1 + 0x20),param_2);
  }
  return 0 < iVar1;
}