undefined8 SSL_CTX_set_ct_validation_callback(long param_1,long param_2,undefined8 param_3)

{
  int iVar1;
  
  if (param_2 != 0) {
    iVar1 = SSL_CTX_has_client_custom_ext(param_1,0x12);
    if (iVar1 != 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x1432,"SSL_CTX_set_ct_validation_callback");
      ERR_set_error(0x14,0xce,0);
      return 0;
    }
  }
  *(long *)(param_1 + 0x1d0) = param_2;
  *(undefined8 *)(param_1 + 0x1d8) = param_3;
  return 1;
}