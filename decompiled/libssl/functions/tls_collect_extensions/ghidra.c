tls_collect_extensions
          (long param_1,ushort **param_2,uint param_3,void **param_4,long *param_5,int param_6)

{
  ushort *puVar1;
  long lVar2;
  long lVar3;
  int iVar4;
  void *ptr;
  undefined1 *puVar5;
  ushort **ppuVar6;
  undefined8 uVar7;
  long lVar8;
  ulong uVar9;
  ushort uVar10;
  uint uVar11;
  ushort *puVar12;
  undefined *puVar13;
  ushort *puVar14;
  ushort *puVar15;
  long lVar16;
  long in_FS_OFFSET;
  ushort *local_90;
  
  lVar2 = *(long *)(param_1 + 0x898);
  puVar15 = *param_2;
  puVar12 = param_2[1];
  lVar3 = *(long *)(in_FS_OFFSET + 0x28);
  *param_4 = (void *)0x0;
  if ((param_3 & 0x80) != 0) {
    custom_ext_init(lVar2 + 0x1d8);
  }
  lVar16 = *(long *)(lVar2 + 0x1e0) + 0x1a;
  ptr = (void *)CRYPTO_zalloc(lVar16 * 0x28,"ssl/statem/extensions.c",0x236);
  if (ptr == (void *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions.c",0x238,"tls_collect_extensions");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
    uVar7 = 0;
  }
  else {
    if (puVar12 == (ushort *)0x0) {
LAB_0015f348:
      if (param_6 == 0) {
LAB_0015f3a8:
        *param_4 = ptr;
        if (param_5 != (long *)0x0) {
          *param_5 = lVar16;
        }
        uVar7 = 1;
        goto LAB_0015f1b9;
      }
      puVar5 = ext_defs;
      do {
        puVar13 = puVar5;
        puVar5 = puVar13 + 0x38;
        if (puVar5 == &DAT_001a7bf0) goto LAB_0015f3a8;
      } while (((*(long *)(puVar13 + 0x40) == 0) || ((*(uint *)(puVar13 + 0x3c) & param_3) == 0)) ||
              ((iVar4 = extension_is_relevant(param_1,*(uint *)(puVar13 + 0x3c),param_3), iVar4 == 0
               || (iVar4 = (**(code **)(puVar13 + 0x40))(param_1), iVar4 != 0))));
    }
    else {
      if ((ushort *)0x3 < puVar12) {
        local_90 = (ushort *)0x0;
        uVar10 = *puVar15 << 8 | *puVar15 >> 8;
        do {
          puVar14 = (ushort *)(ulong)(ushort)(puVar15[1] << 8 | puVar15[1] >> 8);
          if (puVar12 + -2 < puVar14) break;
          puVar12 = (ushort *)((long)(puVar12 + -2) - (long)puVar14);
          lVar8 = 0;
          puVar5 = ext_defs;
          do {
            if (*(uint *)puVar5 == (uint)uVar10) {
              uVar11 = *(uint *)((long)puVar5 + 4);
              if ((param_3 & uVar11) == 0) goto LAB_0015f023;
              if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
                uVar11 = uVar11 & 2;
              }
              else {
                uVar11 = uVar11 & 1;
              }
              if (uVar11 != 0) goto LAB_0015f023;
              lVar8 = lVar8 * 0x28;
              goto LAB_0015f06d;
            }
            puVar5 = (undefined1 *)((long)puVar5 + 0x38);
            lVar8 = lVar8 + 1;
          } while (puVar5 != &DAT_001a7bf0);
          ppuVar6 = (ushort **)custom_ext_find(lVar2 + 0x1d8);
          if (ppuVar6 != (ushort **)0x0) {
            uVar11 = *(uint *)(ppuVar6 + 1);
            if ((param_3 & uVar11) != 0) {
              if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
                uVar11 = uVar11 & 2;
              }
              else {
                uVar11 = uVar11 & 1;
              }
              if (uVar11 == 0) {
                lVar8 = 0x410;
LAB_0015f06d:
                ppuVar6 = (ushort **)((long)ptr + lVar8);
                if (*(int *)(ppuVar6 + 2) != 1) goto LAB_0015f073;
              }
            }
LAB_0015f023:
            ERR_new();
            ERR_set_debug("ssl/statem/extensions.c",0x251,"tls_collect_extensions");
            ossl_statem_fatal(param_1,0x2f,0x6e,0);
            goto LAB_0015f1a3;
          }
LAB_0015f073:
          uVar11 = (uint)uVar10;
          if (((param_3 & 0x80) != 0 && uVar11 == 0x29) && (puVar12 != (ushort *)0x0))
          goto LAB_0015f023;
          uVar9 = ((long)ppuVar6 - (long)ptr >> 3) * -0x3333333333333333;
          if ((((((uint)uVar9 < 0x1a) &&
                (((param_3 & 0x6080) == 0 && uVar11 != 0x2c) && uVar11 != 0xff01)) &&
               (uVar11 != 0x12)) && ((*(byte *)(param_1 + 0xa28 + (uVar9 & 0xffffffff)) & 2) == 0))
             && ((uVar11 != 65000 || ((param_3 & 0x100) == 0)))) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions.c",0x270,"tls_collect_extensions");
            ossl_statem_fatal(param_1,0x6e,0xd9,0);
            goto LAB_0015f1a3;
          }
          if (ppuVar6 != (ushort **)0x0) {
            *ppuVar6 = puVar15 + 2;
            ppuVar6[1] = puVar14;
            *(undefined4 *)(ppuVar6 + 2) = 1;
            puVar1 = (ushort *)((long)local_90 + 1);
            *(uint *)(ppuVar6 + 3) = uVar11;
            ppuVar6[4] = local_90;
            local_90 = puVar1;
            if (*(code **)(param_1 + 0xa48) != (code *)0x0) {
              (**(code **)(param_1 + 0xa48))(param_1);
            }
          }
          if (puVar12 == (ushort *)0x0) goto LAB_0015f348;
          if (puVar12 < (ushort *)0x2) break;
          puVar15 = (ushort *)((long)(puVar15 + 2) + (long)puVar14);
          uVar10 = *puVar15 << 8 | *puVar15 >> 8;
        } while ((ushort *)0x1 < puVar12 + -1);
      }
      ERR_new();
      ERR_set_debug("ssl/statem/extensions.c",0x244,"tls_collect_extensions");
      ossl_statem_fatal(param_1,0x32,0x6e,0);
    }
LAB_0015f1a3:
    CRYPTO_free(ptr);
    uVar7 = 0;
  }
LAB_0015f1b9:
  if (lVar3 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar7;
}