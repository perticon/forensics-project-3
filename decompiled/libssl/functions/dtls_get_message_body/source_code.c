int dtls_get_message_body(SSL *s, size_t *len)
{
    unsigned char *msg = (unsigned char *)s->init_buf->data;
    size_t msg_len = s->init_num + DTLS1_HM_HEADER_LENGTH;

    if (s->s3.tmp.message_type == SSL3_MT_CHANGE_CIPHER_SPEC) {
        /* Nothing to be done */
        goto end;
    }
    /*
     * If receiving Finished, record MAC of prior handshake messages for
     * Finished verification.
     */
    if (*(s->init_buf->data) == SSL3_MT_FINISHED && !ssl3_take_mac(s)) {
        /* SSLfatal() already called */
        return 0;
    }

    if (s->version == DTLS1_BAD_VER) {
        msg += DTLS1_HM_HEADER_LENGTH;
        msg_len -= DTLS1_HM_HEADER_LENGTH;
    }

    if (!ssl3_finish_mac(s, msg, msg_len))
        return 0;

    if (s->msg_callback)
        s->msg_callback(0, s->version, SSL3_RT_HANDSHAKE,
                        s->init_buf->data, s->init_num + DTLS1_HM_HEADER_LENGTH,
                        s, s->msg_callback_arg);

 end:
    *len = s->init_num;
    return 1;
}