ssl_log_rsa_client_key_exchange
          (long param_1,undefined *param_2,ulong param_3,undefined *param_4,long param_5)

{
  long lVar1;
  undefined *puVar2;
  undefined uVar3;
  undefined4 *puVar4;
  undefined8 uVar5;
  undefined *puVar6;
  undefined4 *puVar7;
  undefined4 *puVar8;
  
  if (param_3 < 8) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x15c5,"ssl_log_rsa_client_key_exchange");
    uVar5 = 0xc0103;
LAB_0013c9c4:
    ossl_statem_fatal(param_1,0x50,uVar5,0);
    return 0;
  }
  if (*(long *)(*(long *)(param_1 + 0x9a8) + 0x3d0) != 0) {
    lVar1 = param_5 * 2 + 0x16;
    puVar4 = (undefined4 *)CRYPTO_malloc((int)lVar1,"ssl/ssl_lib.c",0x15a3);
    if (puVar4 == (undefined4 *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x15a4,"nss_keylog_int");
      uVar5 = 0xc0100;
      goto LAB_0013c9c4;
    }
    *puVar4 = 0x415352;
    *(undefined *)((long)puVar4 + 3) = 0x20;
    puVar7 = puVar4 + 1;
    do {
      uVar3 = *param_2;
      puVar8 = (undefined4 *)((long)puVar7 + 2);
      param_2 = param_2 + 1;
      __sprintf_chk(puVar7,1,0xffffffffffffffff,&DAT_00186168,uVar3);
      puVar7 = puVar8;
    } while (puVar8 != puVar4 + 5);
    *(undefined *)(puVar4 + 5) = 0x20;
    puVar6 = (undefined *)((long)puVar4 + 0x15);
    if (param_5 != 0) {
      puVar2 = param_4 + param_5;
      do {
        uVar3 = *param_4;
        param_4 = param_4 + 1;
        __sprintf_chk(puVar6,1,0xffffffffffffffff,&DAT_00186168,uVar3);
        puVar6 = puVar6 + 2;
      } while (param_4 != puVar2);
      puVar6 = (undefined *)((long)puVar4 + param_5 * 2 + 0x15);
    }
    *puVar6 = 0;
    (**(code **)(*(long *)(param_1 + 0x9a8) + 0x3d0))(param_1,puVar4);
    CRYPTO_clear_free(puVar4,lVar1,"ssl/ssl_lib.c",0x15b9);
  }
  return 1;
}