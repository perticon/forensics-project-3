undefined8 tls_construct_ctos_key_share(long param_1,undefined8 param_2)

{
  short sVar1;
  undefined2 uVar2;
  int iVar3;
  long lVar4;
  EVP_PKEY *pkey;
  undefined8 uVar5;
  ulong uVar6;
  long in_FS_OFFSET;
  ulong local_48;
  long local_40;
  void *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_48 = 0;
  local_40 = 0;
  iVar3 = WPACKET_put_bytes__(param_2,0x33,2);
  if (((iVar3 == 0) || (iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 == 0)) ||
     (iVar3 = WPACKET_start_sub_packet_len__(param_2,2), iVar3 == 0)) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x2a7,"tls_construct_ctos_key_share");
    uVar5 = 0xc0103;
LAB_00161782:
    ossl_statem_fatal(param_1,0x50,uVar5,0);
  }
  else {
    tls1_get_supported_groups(param_1,&local_40,&local_48);
    sVar1 = *(short *)(param_1 + 0x4ae);
    if (sVar1 == 0) {
      if (local_48 != 0) {
        uVar6 = 0;
        do {
          sVar1 = ssl_group_id_internal_to_tls13(*(undefined2 *)(local_40 + uVar6 * 2));
          if ((sVar1 != 0) &&
             (iVar3 = tls_group_allowed(param_1,*(undefined2 *)(local_40 + uVar6 * 2),0x20004),
             iVar3 != 0)) {
            sVar1 = *(short *)(local_40 + uVar6 * 2);
            if (sVar1 != 0) goto LAB_00161868;
            break;
          }
          uVar6 = uVar6 + 1;
        } while (uVar6 < local_48);
      }
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x2c1,"tls_construct_ctos_key_share");
      uVar5 = 0x65;
      goto LAB_00161782;
    }
LAB_00161868:
    pkey = *(EVP_PKEY **)(param_1 + 0x2e8);
    local_38 = (void *)0x0;
    if (pkey != (EVP_PKEY *)0x0) {
      if (*(int *)(param_1 + 0x8e8) == 1) goto LAB_00161891;
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x268,"add_key_share");
      uVar5 = 0xc0103;
      goto LAB_00161782;
    }
    pkey = (EVP_PKEY *)ssl_generate_pkey_group(param_1,sVar1);
    if (pkey != (EVP_PKEY *)0x0) {
LAB_00161891:
      lVar4 = EVP_PKEY_get1_encoded_public_key(pkey,&local_38);
      if (lVar4 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x27b,"add_key_share");
        ossl_statem_fatal(param_1,0x50,0x80010,0);
      }
      else {
        uVar2 = ssl_group_id_internal_to_tls13(sVar1);
        iVar3 = WPACKET_put_bytes__(param_2,uVar2,2);
        if ((iVar3 != 0) && (iVar3 = WPACKET_sub_memcpy__(param_2,local_38,lVar4,2), iVar3 != 0)) {
          *(EVP_PKEY **)(param_1 + 0x2e8) = pkey;
          *(short *)(param_1 + 0x4ae) = sVar1;
          CRYPTO_free(local_38);
          iVar3 = WPACKET_close(param_2);
          if ((iVar3 != 0) && (iVar3 = WPACKET_close(param_2), iVar3 != 0)) {
            uVar5 = 1;
            goto LAB_00161793;
          }
          ERR_new();
          ERR_set_debug("ssl/statem/extensions_clnt.c",0x2cb,"tls_construct_ctos_key_share");
          uVar5 = 0xc0103;
          goto LAB_00161782;
        }
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x282,"add_key_share");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
      if (*(long *)(param_1 + 0x2e8) == 0) {
        EVP_PKEY_free(pkey);
      }
      CRYPTO_free(local_38);
      uVar5 = 0;
      goto LAB_00161793;
    }
  }
  uVar5 = 0;
LAB_00161793:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
}