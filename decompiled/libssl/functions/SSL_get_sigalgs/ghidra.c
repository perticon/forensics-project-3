ulong SSL_get_sigalgs(long param_1,int param_2,undefined4 *param_3,undefined4 *param_4,
                     undefined4 *param_5,undefined *param_6,undefined *param_7)

{
  long lVar1;
  short *psVar2;
  ulong uVar3;
  undefined4 uVar4;
  long lVar5;
  short sVar6;
  ulong uVar7;
  
  uVar3 = *(ulong *)(param_1 + 0x3a0);
  if ((*(long *)(param_1 + 0x390) == 0) || (0x7fffffff < uVar3)) {
    return 0;
  }
  uVar7 = uVar3 & 0xffffffff;
  if (param_2 < 0) {
    return uVar7;
  }
  if ((int)uVar3 <= param_2) {
    return 0;
  }
  psVar2 = (short *)(*(long *)(param_1 + 0x390) + (long)param_2 * 2);
  sVar6 = *psVar2;
  if (param_7 != (undefined *)0x0) {
    *param_7 = (char)((ushort)sVar6 >> 8);
    sVar6 = *psVar2;
  }
  if (param_6 != (undefined *)0x0) {
    *param_6 = (char)sVar6;
    sVar6 = *psVar2;
  }
  lVar5 = *(long *)(*(long *)(param_1 + 0x9a8) + 0x620);
  lVar1 = lVar5 + 0x4d8;
  do {
    if (*(short *)(lVar5 + 8) == sVar6) {
      if (*(int *)(lVar5 + 0x24) != 0) {
        if (param_3 != (undefined4 *)0x0) {
          *param_3 = *(undefined4 *)(lVar5 + 0x14);
        }
        if (param_4 != (undefined4 *)0x0) {
          *param_4 = *(undefined4 *)(lVar5 + 0xc);
        }
        if (param_5 == (undefined4 *)0x0) {
          return uVar7;
        }
        uVar4 = *(undefined4 *)(lVar5 + 0x1c);
        goto LAB_0014a242;
      }
      break;
    }
    lVar5 = lVar5 + 0x28;
  } while (lVar5 != lVar1);
  if (param_3 != (undefined4 *)0x0) {
    *param_3 = 0;
  }
  if (param_4 == (undefined4 *)0x0) {
    if (param_5 == (undefined4 *)0x0) {
      return uVar7;
    }
    uVar4 = 0;
  }
  else {
    *param_4 = 0;
    if (param_5 == (undefined4 *)0x0) {
      return uVar7;
    }
    uVar4 = 0;
  }
LAB_0014a242:
  *param_5 = uVar4;
  return uVar7;
}