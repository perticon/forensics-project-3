bool ssl_check_version_downgrade(int *param_1)

{
  int iVar1;
  int *piVar2;
  int iVar3;
  undefined1 *puVar4;
  int iVar5;
  
  iVar5 = **(int **)(*(long *)(param_1 + 0x26a) + 8);
  if (*param_1 == iVar5) {
    return true;
  }
  puVar4 = tls_version_table;
  piVar2 = (int *)TLS_method();
  if (iVar5 != *piVar2) {
    iVar5 = **(int **)(*(long *)(param_1 + 0x26a) + 8);
    piVar2 = (int *)DTLS_method();
    if (iVar5 != *piVar2) {
      return false;
    }
    puVar4 = dtls_version_table;
  }
  do {
    if (*(code **)((long)puVar4 + 0x10) == (code *)0x0) goto LAB_00176690;
    piVar2 = (int *)(**(code **)((long)puVar4 + 0x10))();
    iVar5 = *piVar2;
    iVar1 = param_1[0x27d];
    if ((iVar1 == 0) || (iVar5 == iVar1)) {
LAB_001766ba:
      iVar1 = ssl_security(param_1,9,0,iVar5,0);
      if (iVar1 != 0) {
        iVar1 = param_1[0x27e];
        if ((iVar1 == 0) || (iVar5 == iVar1)) {
LAB_00176720:
          if (((*(ulong *)(piVar2 + 2) & *(ulong *)(param_1 + 0x27a)) == 0) &&
             (((*(byte *)(piVar2 + 1) & 2) == 0 ||
              ((*(uint *)(*(long *)(param_1 + 0x226) + 0x1c) & 0x30000) == 0)))) {
            return *param_1 == *(int *)puVar4;
          }
        }
        else if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
          if (iVar5 < iVar1) goto LAB_00176720;
        }
        else {
          if (iVar5 == 0x100) {
            iVar5 = 0xff00;
            if (iVar1 == 0x100) goto LAB_00176690;
          }
          else if (iVar1 == 0x100) {
            iVar1 = 0xff00;
          }
          if (iVar1 < iVar5) goto LAB_00176720;
        }
      }
    }
    else if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
      if (iVar1 <= iVar5) goto LAB_001766ba;
    }
    else {
      if (iVar5 == 0x100) {
        iVar3 = 0xff00;
        if (iVar1 == 0x100) goto LAB_001766ba;
      }
      else {
        iVar3 = iVar5;
        if (iVar1 == 0x100) {
          iVar1 = 0xff00;
        }
      }
      if (iVar3 <= iVar1) goto LAB_001766ba;
    }
LAB_00176690:
    piVar2 = (int *)((long)puVar4 + 0x18);
    puVar4 = (undefined1 *)((long)puVar4 + 0x18);
    if (*piVar2 == 0) {
      return false;
    }
  } while( true );
}