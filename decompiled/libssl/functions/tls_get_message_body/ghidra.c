undefined8 tls_get_message_body(undefined4 *param_1,ulong *param_2)

{
  int iVar1;
  long lVar2;
  int iVar3;
  ulong uVar4;
  long lVar5;
  long in_FS_OFFSET;
  undefined8 uVar6;
  long local_38;
  long local_30;
  
  uVar4 = *(ulong *)(param_1 + 0x26);
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1[0xb6] == 0x101) {
    *param_2 = uVar4;
    uVar6 = 1;
    goto LAB_00176039;
  }
  lVar2 = *(long *)(param_1 + 0x24);
  for (lVar5 = *(long *)(param_1 + 0xb4) - uVar4; lVar5 != 0; lVar5 = lVar5 - local_38) {
    iVar3 = (**(code **)(*(long *)(param_1 + 2) + 0x68))
                      (param_1,0x16,0,uVar4 + lVar2,lVar5,0,&local_38);
    if (iVar3 < 1) {
      param_1[10] = 3;
      uVar6 = 0;
      *param_2 = 0;
      goto LAB_00176039;
    }
    uVar4 = *(long *)(param_1 + 0x26) + local_38;
    *(ulong *)(param_1 + 0x26) = uVar4;
  }
  if ((**(char **)(*(long *)(param_1 + 0x22) + 8) == '\x14') &&
     (iVar3 = ssl3_take_mac(param_1), iVar3 == 0)) {
LAB_00176180:
    *param_2 = 0;
    uVar6 = 0;
  }
  else {
    uVar6 = 0x176080;
    iVar3 = RECORD_LAYER_is_sslv2_record(param_1 + 0x316);
    if (iVar3 == 0) {
      iVar3 = param_1[0xb6];
      uVar4 = *(ulong *)(param_1 + 0x26);
      if ((((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) != 0) ||
            (iVar1 = **(int **)(param_1 + 2), iVar1 == 0x10000)) || (iVar1 < 0x304)) ||
          ((iVar3 != 4 && (iVar3 != 0x18)))) &&
         (((lVar2 = *(long *)(*(long *)(param_1 + 0x22) + 8), iVar3 != 2 ||
           ((uVar4 < 0x26 ||
            ((hrrrandom._8_8_ ^ *(ulong *)(lVar2 + 0xe) | hrrrandom._0_8_ ^ *(ulong *)(lVar2 + 6))
             != 0)))) ||
          ((hrrrandom._24_8_ ^ *(ulong *)(lVar2 + 0x1e) |
           hrrrandom._16_8_ ^ *(ulong *)(lVar2 + 0x16)) != 0)))) {
        uVar6 = 0x176104;
        iVar3 = ssl3_finish_mac(param_1,lVar2,uVar4 + 4);
        if (iVar3 == 0) goto LAB_00176180;
        uVar4 = *(ulong *)(param_1 + 0x26);
      }
      if (*(code **)(param_1 + 0x130) != (code *)0x0) {
        (**(code **)(param_1 + 0x130))
                  (0,*param_1,0x16,*(undefined8 *)(*(long *)(param_1 + 0x22) + 8),uVar4 + 4,param_1,
                   *(undefined8 *)(param_1 + 0x132),uVar6);
        uVar4 = *(ulong *)(param_1 + 0x26);
      }
    }
    else {
      uVar6 = 0x1761aa;
      iVar3 = ssl3_finish_mac(param_1,*(undefined8 *)(*(long *)(param_1 + 0x22) + 8),
                              *(undefined8 *)(param_1 + 0x26));
      if (iVar3 == 0) goto LAB_00176180;
      uVar4 = *(ulong *)(param_1 + 0x26);
      if (*(code **)(param_1 + 0x130) != (code *)0x0) {
        (**(code **)(param_1 + 0x130))
                  (0,2,0,*(undefined8 *)(*(long *)(param_1 + 0x22) + 8),uVar4,param_1,
                   *(undefined8 *)(param_1 + 0x132),uVar6);
        uVar4 = *(ulong *)(param_1 + 0x26);
      }
    }
    *param_2 = uVar4;
    uVar6 = 1;
  }
LAB_00176039:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar6;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}