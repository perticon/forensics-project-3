static int use_certificate_chain_file(SSL_CTX *ctx, SSL *ssl, const char *file)
{
    BIO *in;
    int ret = 0;
    X509 *x = NULL;
    pem_password_cb *passwd_callback;
    void *passwd_callback_userdata;
    SSL_CTX *real_ctx = (ssl == NULL) ? ctx : ssl->ctx;

    if (ctx == NULL && ssl == NULL)
        return 0;

    ERR_clear_error();          /* clear error stack for
                                 * SSL_CTX_use_certificate() */

    if (ctx != NULL) {
        passwd_callback = ctx->default_passwd_callback;
        passwd_callback_userdata = ctx->default_passwd_callback_userdata;
    } else {
        passwd_callback = ssl->default_passwd_callback;
        passwd_callback_userdata = ssl->default_passwd_callback_userdata;
    }

    in = BIO_new(BIO_s_file());
    if (in == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_BUF_LIB);
        goto end;
    }

    if (BIO_read_filename(in, file) <= 0) {
        ERR_raise(ERR_LIB_SSL, ERR_R_SYS_LIB);
        goto end;
    }

    x = X509_new_ex(real_ctx->libctx, real_ctx->propq);
    if (x == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        goto end;
    }
    if (PEM_read_bio_X509_AUX(in, &x, passwd_callback,
                              passwd_callback_userdata) == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_PEM_LIB);
        goto end;
    }

    if (ctx)
        ret = SSL_CTX_use_certificate(ctx, x);
    else
        ret = SSL_use_certificate(ssl, x);

    if (ERR_peek_error() != 0)
        ret = 0;                /* Key/certificate mismatch doesn't imply
                                 * ret==0 ... */
    if (ret) {
        /*
         * If we could set up our certificate, now proceed to the CA
         * certificates.
         */
        X509 *ca;
        int r;
        unsigned long err;

        if (ctx)
            r = SSL_CTX_clear_chain_certs(ctx);
        else
            r = SSL_clear_chain_certs(ssl);

        if (r == 0) {
            ret = 0;
            goto end;
        }

        while (1) {
            ca = X509_new_ex(real_ctx->libctx, real_ctx->propq);
            if (ca == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
                goto end;
            }
            if (PEM_read_bio_X509(in, &ca, passwd_callback,
                                  passwd_callback_userdata) != NULL) {
                if (ctx)
                    r = SSL_CTX_add0_chain_cert(ctx, ca);
                else
                    r = SSL_add0_chain_cert(ssl, ca);
                /*
                 * Note that we must not free ca if it was successfully added to
                 * the chain (while we must free the main certificate, since its
                 * reference count is increased by SSL_CTX_use_certificate).
                 */
                if (!r) {
                    X509_free(ca);
                    ret = 0;
                    goto end;
                }
            } else {
                X509_free(ca);
                break;
            }
        }
        /* When the while loop ends, it's usually just EOF. */
        err = ERR_peek_last_error();
        if (ERR_GET_LIB(err) == ERR_LIB_PEM
            && ERR_GET_REASON(err) == PEM_R_NO_START_LINE)
            ERR_clear_error();
        else
            ret = 0;            /* some real error */
    }

 end:
    X509_free(x);
    BIO_free(in);
    return ret;
}