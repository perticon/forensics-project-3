undefined8 tls_parse_ctos_server_name(long param_1,ushort **param_2)

{
  void *__s;
  ushort *puVar1;
  ushort *puVar2;
  char *__s_00;
  int iVar3;
  size_t sVar4;
  void *pvVar5;
  long lVar6;
  uint uVar7;
  undefined8 uVar8;
  ulong __n;
  
  puVar1 = param_2[1];
  if (((ushort *)0x1 < puVar1) &&
     (puVar2 = *param_2, puVar1 + -1 == (ushort *)(ulong)(ushort)(*puVar2 << 8 | *puVar2 >> 8))) {
    param_2[1] = (ushort *)0x0;
    *param_2 = (ushort *)((long)puVar2 + (long)puVar1);
    if (puVar1 != (ushort *)0x2) {
      if (((1 < (long)puVar1 - 3U) && (*(char *)(puVar2 + 1) == '\0')) &&
         (__n = (ulong)(ushort)(*(ushort *)((long)puVar2 + 3) << 8 |
                               *(ushort *)((long)puVar2 + 3) >> 8), (long)puVar1 - 5U == __n)) {
        __s = (void *)((long)puVar2 + 5);
        if ((*(int *)(param_1 + 0x4d0) != 0) &&
           ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0 ||
             (iVar3 = **(int **)(param_1 + 8), iVar3 == 0x10000)) || (iVar3 < 0x304)))) {
          uVar7 = 0;
          __s_00 = *(char **)(*(long *)(param_1 + 0x918) + 0x330);
          if ((__s_00 != (char *)0x0) && (sVar4 = strlen(__s_00), sVar4 == __n)) {
            iVar3 = CRYPTO_memcmp(__s,__s_00,__n);
            uVar7 = (uint)(iVar3 == 0);
          }
          *(uint *)(param_1 + 0xb60) = uVar7;
          return 1;
        }
        if (__n < 0x100) {
          pvVar5 = memchr(__s,0,__n);
          if (pvVar5 == (void *)0x0) {
            CRYPTO_free(*(void **)(param_1 + 0xa58));
            *(undefined8 *)(param_1 + 0xa58) = 0;
            CRYPTO_free((void *)0x0);
            lVar6 = CRYPTO_strndup(__s,__n,"include/internal/packet.h",0x1dc);
            *(long *)(param_1 + 0xa58) = lVar6;
            if (lVar6 != 0) {
              *(undefined4 *)(param_1 + 0xb60) = 1;
              return 1;
            }
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x93,"tls_parse_ctos_server_name");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            return 0;
          }
          ERR_new();
          uVar8 = 0x88;
        }
        else {
          ERR_new();
          uVar8 = 0x83;
        }
        ERR_set_debug("ssl/statem/extensions_srvr.c",uVar8,"tls_parse_ctos_server_name");
        ossl_statem_fatal(param_1,0x70,0x6e,0);
        return 0;
      }
      ERR_new();
      uVar8 = 0x79;
      goto LAB_001655d0;
    }
  }
  ERR_new();
  uVar8 = 0x67;
LAB_001655d0:
  ERR_set_debug("ssl/statem/extensions_srvr.c",uVar8,"tls_parse_ctos_server_name");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  return 0;
}