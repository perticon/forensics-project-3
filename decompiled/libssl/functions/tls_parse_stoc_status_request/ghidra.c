ulong tls_parse_stoc_status_request
                (long param_1,long param_2,int param_3,undefined8 param_4,long param_5)

{
  int iVar1;
  ulong uVar2;
  
  if (param_3 == 0x4000) {
    return 1;
  }
  if (*(uint *)(param_1 + 0xa60) != 1) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x587,"tls_parse_stoc_status_request");
    ossl_statem_fatal(param_1,0x6e,0x6e,0);
    return 0;
  }
  if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
    iVar1 = **(int **)(param_1 + 8);
    if ((iVar1 == 0x10000) || (iVar1 < 0x304)) {
      if (*(long *)(param_2 + 8) != 0) goto LAB_001632b8;
      if ((iVar1 < 0x304) || (iVar1 == 0x10000)) goto LAB_0016320b;
    }
    if (param_5 == 0) {
      uVar2 = tls_process_cert_status_body(param_1);
      return uVar2;
    }
  }
  else {
    if (*(long *)(param_2 + 8) != 0) {
LAB_001632b8:
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x58b,"tls_parse_stoc_status_request");
      ossl_statem_fatal(param_1,0x32,0x6e,0);
      return 0;
    }
LAB_0016320b:
    *(undefined4 *)(param_1 + 0xa74) = 1;
  }
  return (ulong)*(uint *)(param_1 + 0xa60);
}