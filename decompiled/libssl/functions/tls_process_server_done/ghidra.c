bool tls_process_server_done(long param_1,long param_2)

{
  int iVar1;
  
  if (*(long *)(param_2 + 8) != 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0xaa5,"tls_process_server_done");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
    return false;
  }
  if ((*(byte *)(*(long *)(param_1 + 0x2e0) + 0x1c) & 0x20) != 0) {
    iVar1 = ssl_srp_calc_a_param_intern();
    if (iVar1 < 1) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0xaab,"tls_process_server_done");
      ossl_statem_fatal(param_1,0x50,0x169,0);
      return false;
    }
  }
  iVar1 = tls_process_initial_server_flight(param_1);
  return iVar1 != 0;
}