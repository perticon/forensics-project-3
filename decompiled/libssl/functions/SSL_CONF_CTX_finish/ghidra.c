undefined8 SSL_CONF_CTX_finish(uint *param_1)

{
  SSL_CTX *ctx;
  char *pcVar1;
  int iVar2;
  long lVar3;
  SSL *pSVar4;
  
  ctx = *(SSL_CTX **)(param_1 + 6);
  if (ctx == (SSL_CTX *)0x0) {
    if (*(long *)(param_1 + 8) != 0) {
      lVar3 = *(long *)(*(long *)(param_1 + 8) + 0x898);
      goto LAB_00134b4f;
    }
    lVar3 = *(long *)(param_1 + 0x2a);
    if (lVar3 == 0) {
      return 1;
    }
  }
  else {
    lVar3 = *(long *)ctx->sid_ctx;
LAB_00134b4f:
    if ((lVar3 != 0) && ((*param_1 & 0x40) != 0)) {
      pcVar1 = *(char **)(param_1 + 0xc);
      if ((pcVar1 != (char *)0x0) && ((*(long *)(lVar3 + 0x28) == 0 && ((*param_1 & 0x20) != 0)))) {
        if (ctx == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134d27;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(ctx,pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134d27:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0xe);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 0x50) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134ce4;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134ce4:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0x10);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 0x78) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134d74;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134d74:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0x12);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 0xa0) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134dbc;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134dbc:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0x14);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 200) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134e0c;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134e0c:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0x16);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 0xf0) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134e5c;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134e5c:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0x18);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 0x118) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134eac;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134eac:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0x1a);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 0x140) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134f4c;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134f4c:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
      pcVar1 = *(char **)(param_1 + 0x1c);
      if (((pcVar1 != (char *)0x0) && (*(long *)(lVar3 + 0x168) == 0)) &&
         ((*(byte *)param_1 & 0x20) != 0)) {
        if (*(SSL_CTX **)(param_1 + 6) == (SSL_CTX *)0x0) {
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) goto LAB_00134efc;
        }
        else {
          iVar2 = SSL_CTX_use_PrivateKey_file(*(SSL_CTX **)(param_1 + 6),pcVar1,1);
          pSVar4 = *(SSL **)(param_1 + 8);
          if (pSVar4 != (SSL *)0x0) {
LAB_00134efc:
            iVar2 = SSL_use_PrivateKey_file(pSVar4,pcVar1,1);
          }
          if (iVar2 < 1) {
            return 0;
          }
        }
      }
    }
    lVar3 = *(long *)(param_1 + 0x2a);
    if (lVar3 == 0) {
      return 1;
    }
    if (*(long *)(param_1 + 8) != 0) {
      SSL_set0_CA_list(*(long *)(param_1 + 8),lVar3);
      goto LAB_00134c4d;
    }
    if (*(long *)(param_1 + 6) != 0) {
      SSL_CTX_set0_CA_list(*(long *)(param_1 + 6),lVar3);
      goto LAB_00134c4d;
    }
  }
  OPENSSL_sk_pop_free(lVar3,PTR_X509_NAME_free_001a7fc8);
LAB_00134c4d:
  *(undefined8 *)(param_1 + 0x2a) = 0;
  return 1;
}