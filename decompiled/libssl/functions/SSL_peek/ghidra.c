int SSL_peek(SSL *ssl,void *buf,int num)

{
  int iVar1;
  long lVar2;
  long in_FS_OFFSET;
  int local_50;
  SSL *local_48;
  void *pvStack64;
  long local_38;
  undefined4 local_30;
  _func_3060 *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (num < 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x7c1,"SSL_peek");
    ERR_set_error(0x14,0x10f,0);
    iVar1 = -1;
    goto LAB_00137c46;
  }
  if (ssl->handshake_func == (_func_3149 *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x7a2,"ssl_peek_internal");
    ERR_set_error(0x14,0x114,0);
    iVar1 = -1;
    goto LAB_00137c46;
  }
  iVar1 = 0;
  if ((*(byte *)&ssl->shutdown & 2) != 0) goto LAB_00137c46;
  if ((*(byte *)((long)&ssl[3].tlsext_debug_arg + 1) & 1) == 0) {
LAB_00137c2a:
    iVar1 = (*ssl->method->ssl_write)(ssl,buf,num);
  }
  else {
    lVar2 = ASYNC_get_current_job();
    if (lVar2 != 0) goto LAB_00137c2a;
    local_28 = ssl->method->ssl_write;
    local_30 = 0;
    local_48 = ssl;
    pvStack64 = buf;
    local_38 = (long)num;
    iVar1 = ssl_start_async_job(ssl,&local_48,ssl_io_intern);
    local_50 = (int)ssl[10].tls_session_ticket_ext_cb;
  }
  if (0 < iVar1) {
    iVar1 = local_50;
  }
LAB_00137c46:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}