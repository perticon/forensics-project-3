undefined8 ossl_statem_client_pre_work(long param_1,undefined8 param_2)

{
  uint uVar1;
  int iVar2;
  undefined8 uVar3;
  
  uVar1 = *(uint *)(param_1 + 0x5c);
  if (uVar1 == 0x10) {
    if (((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0) &&
       (*(int *)(param_1 + 0x4d0) != 0)) {
      *(undefined4 *)(param_1 + 0x78) = 0;
      return 2;
    }
  }
  else if (uVar1 < 0x11) {
    if (uVar1 == 1) {
      uVar3 = tls_finish_handshake(param_1,param_2,1,1);
      return uVar3;
    }
    if ((uVar1 == 0xc) &&
       (*(undefined4 *)(param_1 + 0x44) = 0,
       (*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0)) {
      iVar2 = ssl3_init_finished_mac();
      uVar3 = 0;
      if (iVar2 != 0) {
        uVar3 = 2;
      }
      return uVar3;
    }
  }
  else if ((uVar1 == 0x2e) ||
          (((uVar1 == 0x2f && (*(int *)(param_1 + 0x84) != 7)) && (*(int *)(param_1 + 0x84) != 0))))
  {
    uVar3 = tls_finish_handshake(param_1,param_2,0,1);
    return uVar3;
  }
  return 2;
}