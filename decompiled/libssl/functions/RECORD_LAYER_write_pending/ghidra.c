bool RECORD_LAYER_write_pending(long param_1)

{
  bool bVar1;
  
  bVar1 = false;
  if (*(long *)(param_1 + 0x18) != 0) {
    bVar1 = *(long *)(param_1 + 0x70 + (*(long *)(param_1 + 0x18) * 3 + -3) * 0x10) != 0;
  }
  return bVar1;
}