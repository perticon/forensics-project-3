undefined8 ossl_statem_server_construct_message(long param_1,code **param_2,undefined4 *param_3)

{
  code *pcVar1;
  
  switch(*(undefined4 *)(param_1 + 0x5c)) {
  case 0x13:
    *param_2 = (code *)0x0;
    *param_3 = 0;
    return 1;
  default:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0x401,"ossl_statem_server_construct_message");
    ossl_statem_fatal(param_1,0x50,0xec,0);
    return 0;
  case 0x15:
    *param_2 = dtls_construct_hello_verify_request;
    *param_3 = 3;
    return 1;
  case 0x16:
    *param_2 = tls_construct_server_hello;
    *param_3 = 2;
    return 1;
  case 0x17:
    *param_2 = tls_construct_server_certificate;
    *param_3 = 0xb;
    return 1;
  case 0x18:
    *param_2 = tls_construct_server_key_exchange;
    *param_3 = 0xc;
    return 1;
  case 0x19:
    *param_2 = tls_construct_certificate_request;
    *param_3 = 0xd;
    return 1;
  case 0x1a:
    *param_2 = tls_construct_server_done;
    *param_3 = 0xe;
    return 1;
  case 0x21:
    *param_2 = tls_construct_new_session_ticket;
    *param_3 = 4;
    return 1;
  case 0x22:
    *param_2 = tls_construct_cert_status;
    *param_3 = 0x16;
    return 1;
  case 0x23:
    break;
  case 0x24:
    *param_2 = tls_construct_finished;
    *param_3 = 0x14;
    return 1;
  case 0x25:
    *param_2 = tls_construct_encrypted_extensions;
    *param_3 = 8;
    return 1;
  case 0x28:
    *param_2 = tls_construct_cert_verify;
    *param_3 = 0xf;
    return 1;
  case 0x2a:
    *param_2 = tls_construct_key_update;
    *param_3 = 0x18;
    return 1;
  case 0x2e:
    *param_2 = (code *)0x0;
    *param_3 = 0xffffffff;
    return 1;
  }
  if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
    pcVar1 = tls_construct_change_cipher_spec;
  }
  else {
    pcVar1 = dtls_construct_change_cipher_spec;
  }
  *param_2 = pcVar1;
  *param_3 = 0x101;
  return 1;
}