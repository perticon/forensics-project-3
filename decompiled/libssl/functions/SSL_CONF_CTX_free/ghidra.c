void SSL_CONF_CTX_free(void *param_1)

{
  void *ptr;
  void **ppvVar1;
  
  if (param_1 != (void *)0x0) {
    ppvVar1 = (void **)((long)param_1 + 0x30);
    do {
      ptr = *ppvVar1;
      ppvVar1 = ppvVar1 + 1;
      CRYPTO_free(ptr);
    } while (ppvVar1 != (void **)((long)param_1 + 0x78));
    CRYPTO_free(*(void **)((long)param_1 + 8));
    OPENSSL_sk_pop_free(*(undefined8 *)((long)param_1 + 0xa8),PTR_X509_NAME_free_001a7fc8);
    CRYPTO_free(param_1);
    return;
  }
  return;
}