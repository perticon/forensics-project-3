long SSL_CTX_set_timeout(SSL_CTX *ctx,long t)

{
  _func_3088 *p_Var1;
  
  if (ctx != (SSL_CTX *)0x0) {
    p_Var1 = ctx->remove_session_cb;
    ctx->remove_session_cb = (_func_3088 *)t;
    return (long)p_Var1;
  }
  return 0;
}