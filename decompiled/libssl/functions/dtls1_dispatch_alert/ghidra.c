int dtls1_dispatch_alert(undefined4 *param_1)

{
  int iVar1;
  code *pcVar2;
  long in_FS_OFFSET;
  undefined8 uVar3;
  undefined local_30 [14];
  undefined2 local_22;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_22 = *(undefined2 *)(param_1 + 0x6a);
  param_1[0x69] = 0;
  iVar1 = do_dtls1_write(param_1,0x15,&local_22,2,0,local_30);
  if (iVar1 < 1) {
    param_1[0x69] = 1;
  }
  else {
    uVar3 = 0x125942;
    BIO_ctrl(*(BIO **)(param_1 + 6),0xb,0,(void *)0x0);
    if (*(code **)(param_1 + 0x130) != (code *)0x0) {
      (**(code **)(param_1 + 0x130))
                (1,*param_1,0x15,param_1 + 0x6a,2,param_1,*(undefined8 *)(param_1 + 0x132),uVar3);
    }
    pcVar2 = *(code **)(param_1 + 0x25e);
    if ((pcVar2 != (code *)0x0) ||
       (pcVar2 = *(code **)(*(long *)(param_1 + 0x26a) + 0x120), pcVar2 != (code *)0x0)) {
      (*pcVar2)(param_1,0x4008,*(ushort *)(param_1 + 0x6a) << 8 | *(ushort *)(param_1 + 0x6a) >> 8);
    }
  }
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}