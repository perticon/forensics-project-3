int ssl3_write_pending(long param_1,int param_2,long param_3,ulong param_4,undefined8 *param_5)

{
  BIO *b;
  int iVar1;
  int *piVar2;
  long lVar3;
  long lVar4;
  long *plVar5;
  ulong uVar6;
  
  if ((param_4 <= *(ulong *)(param_1 + 0x1cd8) && *(ulong *)(param_1 + 0x1cd8) != param_4) ||
     ((((*(byte *)(param_1 + 0x9f0) & 2) == 0 && (*(long *)(param_1 + 0x1cf0) != param_3)) ||
      (*(int *)(param_1 + 0x1ce0) != param_2)))) {
    ERR_new();
    ERR_set_debug("ssl/record/rec_layer_s3.c",0x492,"ssl3_write_pending");
    ossl_statem_fatal(param_1,0x50,0x7f,0);
    return -1;
  }
  lVar4 = *(long *)(param_1 + 0xcc8);
  uVar6 = 0;
  piVar2 = __errno_location();
LAB_001566d8:
  do {
    plVar5 = (long *)(param_1 + 0xca8 + uVar6 * 0x30);
    if (lVar4 == 0) goto LAB_00156750;
    do {
      b = *(BIO **)(param_1 + 0x18);
      *piVar2 = 0;
      if (b == (BIO *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/record/rec_layer_s3.c",0x4b2,"ssl3_write_pending");
        ossl_statem_fatal(param_1,0x50,0x80,0);
        iVar1 = -1;
LAB_001567a8:
        if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
          return iVar1;
        }
        plVar5[4] = 0;
        return iVar1;
      }
      *(undefined4 *)(param_1 + 0x28) = 2;
      iVar1 = BIO_write(b,(void *)(plVar5[3] + *plVar5),(int)lVar4);
      if (iVar1 < 0) goto LAB_001567a8;
      lVar4 = plVar5[4];
      lVar3 = (long)iVar1;
      if (lVar4 != lVar3) {
        if (iVar1 == 0) goto LAB_001567a8;
        lVar4 = lVar4 - lVar3;
        plVar5[3] = plVar5[3] + lVar3;
        plVar5[4] = lVar4;
        goto LAB_001566d8;
      }
      plVar5[3] = plVar5[3] + lVar4;
      plVar5[4] = 0;
      if (*(ulong *)(param_1 + 0xc70) <= uVar6 + 1) {
        *(undefined4 *)(param_1 + 0x28) = 1;
        *param_5 = *(undefined8 *)(param_1 + 0x1ce8);
        return 1;
      }
      plVar5 = (long *)(param_1 + 0xca8 + uVar6 * 0x30);
LAB_00156750:
      lVar4 = 0;
    } while (*(long *)(param_1 + 0xc70) - 1U <= uVar6);
    lVar4 = *(long *)(param_1 + 0xcf8 + uVar6 * 0x30);
    uVar6 = uVar6 + 1;
  } while( true );
}