undefined4 tls_parse_ctos_ec_pt_formats(long param_1,byte **param_2)

{
  byte *pbVar1;
  byte *pbVar2;
  long lVar3;
  
  if (param_2[1] != (byte *)0x0) {
    pbVar1 = param_2[1] + -1;
    if (pbVar1 == (byte *)(ulong)**param_2) {
      pbVar2 = *param_2 + 1;
      param_2[1] = (byte *)0x0;
      *param_2 = pbVar2 + (long)pbVar1;
      if (pbVar1 != (byte *)0x0) {
        if (*(int *)(param_1 + 0x4d0) != 0) {
          return 1;
        }
        CRYPTO_free(*(void **)(param_1 + 0xab8));
        *(undefined8 *)(param_1 + 0xab8) = 0;
        *(undefined8 *)(param_1 + 0xab0) = 0;
        lVar3 = CRYPTO_memdup(pbVar2,pbVar1,"include/internal/packet.h",0x1c5);
        *(long *)(param_1 + 0xab8) = lVar3;
        if (lVar3 != 0) {
          *(byte **)(param_1 + 0xab0) = pbVar1;
          return 1;
        }
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_srvr.c",0xee,"tls_parse_ctos_ec_pt_formats");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        return 0;
      }
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0xe6,"tls_parse_ctos_ec_pt_formats");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  return 0;
}