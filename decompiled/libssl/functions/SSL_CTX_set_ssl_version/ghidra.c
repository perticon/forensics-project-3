int SSL_CTX_set_ssl_version(SSL_CTX *ctx,SSL_METHOD *meth)

{
  int iVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  long lVar4;
  
  ctx->cipher_list = (stack_st_SSL_CIPHER *)meth;
  uVar2 = OSSL_default_ciphersuites();
  iVar1 = SSL_CTX_set_ciphersuites(ctx,uVar2);
  if (iVar1 == 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x290,"SSL_CTX_set_ssl_version");
    ERR_set_error(0x14,0xe6,0);
    return 0;
  }
  uVar2 = *(undefined8 *)ctx->sid_ctx;
  uVar3 = OSSL_default_cipher_list();
  lVar4 = ssl_create_cipher_list
                    (ctx,ctx->sessions,&ctx->cipher_list_by_id,&ctx->cert_store,uVar3,uVar2);
  if (lVar4 != 0) {
    iVar1 = OPENSSL_sk_num(lVar4);
    if (0 < iVar1) {
      return 1;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x299,"SSL_CTX_set_ssl_version");
  ERR_set_error(0x14,0xe6,0);
  return 0;
}