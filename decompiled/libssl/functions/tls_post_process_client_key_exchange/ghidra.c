undefined4 tls_post_process_client_key_exchange(long param_1)

{
  int iVar1;
  
  if ((*(int *)(param_1 + 0x74) == 0) && (*(long *)(*(long *)(param_1 + 0x918) + 0x2b8) != 0)) {
    if (*(long *)(param_1 + 0x188) == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0xd44,"tls_post_process_client_key_exchange");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    }
    iVar1 = ssl3_digest_cached_records(param_1,1);
  }
  else {
    iVar1 = ssl3_digest_cached_records(param_1,0);
  }
  if (iVar1 == 0) {
    return 0;
  }
  return 2;
}