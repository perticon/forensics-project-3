long SSL_get_srp_userinfo(long param_1)

{
  if (*(long *)(param_1 + 0xc38) != 0) {
    return *(long *)(param_1 + 0xc38);
  }
  return *(long *)(*(long *)(param_1 + 0x9a8) + 0x380);
}