int SSL_renegotiate_pending(SSL *s)

{
  return (uint)(*(int *)&s[4].expand != 0);
}