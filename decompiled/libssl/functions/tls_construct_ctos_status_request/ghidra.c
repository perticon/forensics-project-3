tls_construct_ctos_status_request(long param_1,undefined8 param_2,undefined8 param_3,long param_4)

{
  int iVar1;
  int iVar2;
  int iVar3;
  undefined8 uVar4;
  OCSP_RESPID *a;
  long in_FS_OFFSET;
  uchar *puStack72;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar4 = 2;
  if ((param_4 != 0) || (*(int *)(param_1 + 0xa60) != 1)) goto LAB_00160cc8;
  iVar1 = WPACKET_put_bytes__(param_2,5,2);
  if ((iVar1 == 0) ||
     ((iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 == 0 ||
      (iVar1 = WPACKET_put_bytes__(param_2,1,1), iVar1 == 0)))) {
LAB_00160d30:
    ERR_new();
    uVar4 = 0x164;
  }
  else {
    iVar3 = 0;
    iVar1 = WPACKET_start_sub_packet_len__(param_2);
    if (iVar1 == 0) goto LAB_00160d30;
    for (; iVar1 = OPENSSL_sk_num(*(undefined8 *)(param_1 + 0xa78)), iVar3 < iVar1;
        iVar3 = iVar3 + 1) {
      a = (OCSP_RESPID *)OPENSSL_sk_value(*(undefined8 *)(param_1 + 0xa78));
      iVar1 = i2d_OCSP_RESPID(a,(uchar **)0x0);
      if (((iVar1 < 1) ||
          (iVar2 = WPACKET_sub_allocate_bytes__(param_2,(long)iVar1,&puStack72,2), iVar2 == 0)) ||
         (iVar2 = i2d_OCSP_RESPID(a,&puStack72), iVar2 != iVar1)) {
        ERR_new();
        uVar4 = 0x170;
        goto LAB_00160d41;
      }
    }
    iVar1 = WPACKET_close(param_2);
    if ((iVar1 == 0) || (iVar1 = WPACKET_start_sub_packet_len__(param_2), iVar1 == 0)) {
      ERR_new();
      uVar4 = 0x176;
    }
    else if (*(X509_EXTENSIONS **)(param_1 + 0xa80) == (X509_EXTENSIONS *)0x0) {
LAB_00160ea4:
      iVar1 = WPACKET_close(param_2);
      if ((iVar1 != 0) && (iVar1 = WPACKET_close(param_2), iVar1 != 0)) {
        uVar4 = 1;
        goto LAB_00160cc8;
      }
      ERR_new();
      uVar4 = 0x189;
    }
    else {
      iVar1 = i2d_X509_EXTENSIONS(*(X509_EXTENSIONS **)(param_1 + 0xa80),(uchar **)0x0);
      if (iVar1 < 0) {
        ERR_new();
        uVar4 = 0x17e;
      }
      else {
        iVar3 = WPACKET_allocate_bytes(param_2,(long)iVar1,&puStack72);
        if ((iVar3 != 0) &&
           (iVar3 = i2d_X509_EXTENSIONS(*(X509_EXTENSIONS **)(param_1 + 0xa80),&puStack72),
           iVar3 == iVar1)) goto LAB_00160ea4;
        ERR_new();
        uVar4 = 0x184;
      }
    }
  }
LAB_00160d41:
  ERR_set_debug("ssl/statem/extensions_clnt.c",uVar4,"tls_construct_ctos_status_request");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  uVar4 = 0;
LAB_00160cc8:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}