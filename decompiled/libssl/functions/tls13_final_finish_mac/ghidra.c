undefined8 tls13_final_finish_mac(long param_1,long param_2,undefined8 param_3,undefined8 param_4)

{
  undefined8 uVar1;
  int iVar2;
  undefined8 uVar3;
  long lVar4;
  undefined8 uVar5;
  undefined8 *puVar6;
  undefined *puVar7;
  long in_FS_OFFSET;
  undefined4 local_158;
  undefined4 uStack340;
  undefined4 uStack336;
  undefined4 uStack332;
  undefined4 local_148;
  undefined4 uStack324;
  undefined4 uStack320;
  undefined4 uStack316;
  undefined8 local_138;
  undefined8 local_128;
  undefined8 local_120;
  undefined4 local_118;
  undefined4 uStack276;
  undefined4 uStack272;
  undefined4 uStack268;
  undefined4 local_108;
  undefined4 uStack260;
  undefined4 uStack256;
  undefined4 uStack252;
  undefined8 local_f8;
  undefined8 local_f0 [5];
  undefined local_c8 [64];
  undefined local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar3 = ssl_handshake_md();
  uVar3 = EVP_MD_get0_name(uVar3);
  local_128 = 0;
  lVar4 = *(long *)(*(long *)(param_1 + 0x9a8) + 0x440);
  if (lVar4 == 0) {
    puVar6 = (undefined8 *)&local_118;
  }
  else {
    puVar6 = local_f0;
    OSSL_PARAM_construct_utf8_string(&local_158,"properties",lVar4,0);
    local_f8 = local_138;
    local_118 = local_158;
    uStack276 = uStack340;
    uStack272 = uStack336;
    uStack268 = uStack332;
    local_108 = local_148;
    uStack260 = uStack324;
    uStack256 = uStack320;
    uStack252 = uStack316;
  }
  OSSL_PARAM_construct_end(&local_158);
  *puVar6 = CONCAT44(uStack340,local_158);
  puVar6[1] = CONCAT44(uStack332,uStack336);
  puVar6[2] = CONCAT44(uStack324,local_148);
  puVar6[3] = CONCAT44(uStack316,uStack320);
  puVar6[4] = local_138;
  iVar2 = ssl_handshake_hash(CONCAT44(uStack340,local_158),CONCAT44(uStack324,local_148),param_1,
                             local_c8,0x40,&local_120);
  uVar1 = local_120;
  if (iVar2 != 0) {
    if (*(long *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x40) == param_2) {
      puVar7 = (undefined *)(param_1 + 0x684);
    }
    else if ((*(long *)(param_1 + 0x240) == 0) || (*(long *)(param_1 + 0x2c8) == 0)) {
      puVar7 = (undefined *)(param_1 + 0x644);
    }
    else {
      puVar7 = local_88;
      uVar5 = ssl_handshake_md(param_1);
      iVar2 = tls13_derive_finishedkey(param_1,uVar5,param_1 + 0x744,puVar7,uVar1);
      if (iVar2 == 0) goto LAB_00151c26;
    }
    lVar4 = EVP_Q_mac(**(undefined8 **)(param_1 + 0x9a8),&DAT_00188069,
                      (*(undefined8 **)(param_1 + 0x9a8))[0x88],uVar3,&local_118,puVar7,local_120,
                      local_c8,local_120,param_4,0x80,&local_128);
    if (lVar4 == 0) {
      ERR_new();
      ERR_set_debug("ssl/tls13_enc.c",0x127,"tls13_final_finish_mac");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
  }
LAB_00151c26:
  OPENSSL_cleanse(local_88,0x40);
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_128;
}