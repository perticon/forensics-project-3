undefined8 SSL_CTX_enable_ct(undefined8 param_1,int param_2)

{
  undefined8 uVar1;
  
  if (param_2 == 0) {
    uVar1 = SSL_CTX_set_ct_validation_callback(param_1,ct_permissive,0);
    return uVar1;
  }
  if (param_2 == 1) {
    uVar1 = SSL_CTX_set_ct_validation_callback(param_1,ct_strict,0);
    return uVar1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x14ac,"SSL_CTX_enable_ct");
  ERR_set_error(0x14,0xd4,0);
  return 0;
}