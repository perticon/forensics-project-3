char * SSL_get_psk_identity(SSL *s)

{
  char *pcVar1;
  
  if (s == (SSL *)0x0) {
    pcVar1 = (char *)0x0;
  }
  else {
    pcVar1 = *(char **)&s[3].sid_ctx_length;
    if (pcVar1 != (char *)0x0) {
      return *(char **)(pcVar1 + 0x2a8);
    }
  }
  return pcVar1;
}