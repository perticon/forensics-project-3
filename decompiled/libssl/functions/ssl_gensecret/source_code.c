int ssl_gensecret(SSL *s, unsigned char *pms, size_t pmslen)
{
    int rv = 0;

    /* SSLfatal() called as appropriate in the below functions */
    if (SSL_IS_TLS13(s)) {
        /*
         * If we are resuming then we already generated the early secret
         * when we created the ClientHello, so don't recreate it.
         */
        if (!s->hit)
            rv = tls13_generate_secret(s, ssl_handshake_md(s), NULL, NULL,
                    0,
                    (unsigned char *)&s->early_secret);
        else
            rv = 1;

        rv = rv && tls13_generate_handshake_secret(s, pms, pmslen);
    } else {
        rv = ssl_generate_master_secret(s, pms, pmslen, 0);
    }

    return rv;
}