int SSL_waiting_for_async(SSL *s)
{
    if (s->job)
        return 1;

    return 0;
}