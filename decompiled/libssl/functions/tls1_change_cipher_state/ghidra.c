undefined4 tls1_change_cipher_state(long param_1,uint param_2)

{
  EVP_CIPHER *cipher;
  size_t __n;
  void *pvVar1;
  undefined8 uVar2;
  bool bVar3;
  uint uVar4;
  uint uVar5;
  int iVar6;
  int iVar7;
  uint uVar8;
  COMP_CTX *pCVar9;
  long lVar10;
  ulong uVar11;
  EVP_PKEY *pkey;
  undefined8 uVar12;
  ulong uVar13;
  EVP_CIPHER_CTX *pEVar14;
  long lVar15;
  long lVar16;
  uint uVar17;
  undefined8 uVar18;
  void *__src;
  undefined8 uVar19;
  size_t *psVar20;
  uchar *key;
  undefined8 uVar21;
  long local_60;
  uchar *local_50;
  uchar *local_48;
  int local_3c;
  
  uVar18 = *(undefined8 *)(param_1 + 0x328);
  uVar17 = param_2 & 1;
  cipher = *(EVP_CIPHER **)(param_1 + 800);
  iVar7 = *(int *)(param_1 + 0x330);
  lVar16 = *(long *)(param_1 + 0x340);
  uVar11 = *(ulong *)(param_1 + 0xa8);
  if (uVar17 == 0) {
    pEVar14 = *(EVP_CIPHER_CTX **)(param_1 + 0x878);
    *(undefined4 *)(param_1 + 0x7c) = 1;
    uVar13 = uVar11 & 0xfffffffffffffbff;
    if (*(int *)(param_1 + 0xb2c) != 0) {
      uVar13 = uVar11 | 0x400;
    }
    *(ulong *)(param_1 + 0xa8) = uVar13;
    uVar17 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x40);
    uVar8 = *(uint *)(param_1 + 0x540) & 0xfffffffd;
    if ((uVar17 & 0x10000) != 0) {
      uVar8 = *(uint *)(param_1 + 0x540) | 2;
    }
    uVar4 = uVar8 & 0xfffffff7;
    if ((uVar17 & 0x20000) != 0) {
      uVar4 = uVar8 | 8;
    }
    *(uint *)(param_1 + 0x540) = uVar4;
    if ((pEVar14 == (EVP_CIPHER_CTX *)0x0) ||
       ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0)) {
      pEVar14 = EVP_CIPHER_CTX_new();
      *(EVP_CIPHER_CTX **)(param_1 + 0x878) = pEVar14;
      if (pEVar14 == (EVP_CIPHER_CTX *)0x0) {
        ERR_new();
        uVar18 = 0xfa;
        goto LAB_00146911;
      }
      if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
        bVar3 = false;
        goto LAB_00146560;
      }
      local_60 = EVP_MD_CTX_new();
      if (local_60 == 0) {
        ERR_new();
        uVar18 = 0x101;
        goto LAB_00146911;
      }
      *(long *)(param_1 + 0x890) = local_60;
      bVar3 = false;
    }
    else {
      bVar3 = true;
LAB_00146560:
      local_60 = ssl_replace_hash(param_1 + 0x890,0);
      if (local_60 == 0) {
        ERR_new();
        uVar18 = 0x108;
LAB_00146911:
        ERR_set_debug("ssl/t1_enc.c",uVar18,"tls1_change_cipher_state");
        ossl_statem_fatal(param_1,0x50,0xc0100,0);
        return 0;
      }
    }
    COMP_CTX_free(*(COMP_CTX **)(param_1 + 0x868));
    *(undefined8 *)(param_1 + 0x868) = 0;
    if (lVar16 != 0) {
      pCVar9 = COMP_CTX_new(*(COMP_METHOD **)(lVar16 + 0x10));
      *(COMP_CTX **)(param_1 + 0x868) = pCVar9;
      if (pCVar9 == (COMP_CTX *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/t1_enc.c",0x112,"tls1_change_cipher_state");
        ossl_statem_fatal(param_1,0x50,0x8e,0);
        return 0;
      }
    }
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
      RECORD_LAYER_reset_write_sequence(param_1 + 0xc58);
    }
    key = (uchar *)(param_1 + 0x100);
    psVar20 = (size_t *)(param_1 + 0xf8);
    if (bVar3) {
LAB_001465e6:
      EVP_CIPHER_CTX_reset(pEVar14);
    }
LAB_001462ad:
    __n = *(size_t *)(param_1 + 0x338);
    pvVar1 = *(void **)(param_1 + 0x318);
    *psVar20 = __n;
    iVar6 = EVP_CIPHER_get_key_length(cipher);
    lVar16 = (long)iVar6;
    iVar6 = EVP_CIPHER_get_mode(cipher);
    if ((iVar6 == 6) || (iVar6 = EVP_CIPHER_get_mode(cipher), iVar6 == 7)) {
      local_3c = 4;
      lVar15 = 4;
      if (param_2 != 0x12) goto LAB_0014630f;
LAB_00146616:
      lVar10 = __n * 2;
      lVar16 = lVar10 + lVar16 * 2;
      __src = pvVar1;
      if (*(ulong *)(param_1 + 0x310) < (ulong)(lVar16 + lVar15 * 2)) {
LAB_0014663f:
        ERR_new();
        uVar18 = 0x13d;
        goto LAB_00146650;
      }
    }
    else {
      local_3c = EVP_CIPHER_get_iv_length(cipher);
      lVar15 = (long)local_3c;
      if (param_2 == 0x12) goto LAB_00146616;
LAB_0014630f:
      if (param_2 == 0x21) goto LAB_00146616;
      lVar10 = __n * 2 + lVar16;
      lVar16 = lVar16 + lVar10 + lVar15;
      __src = (void *)((long)pvVar1 + __n);
      if (*(ulong *)(param_1 + 0x310) < (ulong)(lVar16 + lVar15)) goto LAB_0014663f;
    }
    local_48 = (uchar *)((long)pvVar1 + lVar16);
    local_50 = (uchar *)((long)pvVar1 + lVar10);
    memcpy(key,__src,__n);
    uVar11 = EVP_CIPHER_get_flags();
    if ((uVar11 & 0x200000) == 0) {
      if (iVar7 == 0x357) {
        pkey = (EVP_PKEY *)
               EVP_PKEY_new_raw_private_key_ex
                         (**(undefined8 **)(param_1 + 0x9a8),&DAT_00188069,
                          (*(undefined8 **)(param_1 + 0x9a8))[0x88],key);
      }
      else {
        pkey = EVP_PKEY_new_mac_key(iVar7,(ENGINE *)0x0,key,(int)*psVar20);
      }
      if (pkey != (EVP_PKEY *)0x0) {
        uVar21 = (*(undefined8 **)(param_1 + 0x9a8))[0x88];
        uVar2 = **(undefined8 **)(param_1 + 0x9a8);
        uVar19 = 0x1463bf;
        uVar12 = EVP_MD_get0_name(uVar18);
        iVar7 = EVP_DigestSignInit_ex
                          (local_60,0,uVar12,uVar2,uVar21,pkey,0,uVar19,psVar20,key,uVar21);
        if (0 < iVar7) {
          EVP_PKEY_free(pkey);
          goto LAB_001463f1;
        }
      }
      EVP_PKEY_free(pkey);
      ERR_new();
      uVar18 = 0x156;
    }
    else {
LAB_001463f1:
      iVar7 = EVP_CIPHER_get_mode(cipher);
      param_2 = param_2 & 2;
      if (iVar7 == 6) {
        iVar7 = EVP_CipherInit_ex(pEVar14,cipher,(ENGINE *)0x0,local_50,(uchar *)0x0,param_2);
        if ((iVar7 == 0) || (iVar7 = EVP_CIPHER_CTX_ctrl(pEVar14,0x12,local_3c,local_48), iVar7 < 1)
           ) {
          ERR_new();
          uVar18 = 0x165;
          goto LAB_00146650;
        }
      }
      else {
        iVar7 = EVP_CIPHER_get_mode(cipher);
        if (iVar7 == 7) {
          uVar17 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x24);
          iVar7 = EVP_CipherInit_ex(pEVar14,cipher,(ENGINE *)0x0,(uchar *)0x0,(uchar *)0x0,param_2);
          if ((((iVar7 == 0) || (iVar7 = EVP_CIPHER_CTX_ctrl(pEVar14,9,0xc,(void *)0x0), iVar7 < 1))
              || (iVar7 = EVP_CIPHER_CTX_ctrl(pEVar14,0x11,
                                              (-(uint)((uVar17 & 0x30000) == 0) & 8) + 8,(void *)0x0
                                             ), iVar7 < 1)) ||
             ((iVar7 = EVP_CIPHER_CTX_ctrl(pEVar14,0x12,local_3c,local_48), iVar7 < 1 ||
              (iVar7 = EVP_CipherInit_ex(pEVar14,(EVP_CIPHER *)0x0,(ENGINE *)0x0,local_50,
                                         (uchar *)0x0,-1), iVar7 == 0)))) {
            ERR_new();
            uVar18 = 0x174;
            goto LAB_00146650;
          }
        }
        else {
          iVar7 = EVP_CipherInit_ex(pEVar14,cipher,(ENGINE *)0x0,local_50,local_48,param_2);
          if (iVar7 == 0) {
            ERR_new();
            ERR_set_debug("ssl/t1_enc.c",0x179,"tls1_change_cipher_state");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            return 0;
          }
        }
      }
      uVar11 = EVP_CIPHER_get_flags(cipher);
      if ((((uVar11 & 0x200000) == 0) || (*psVar20 == 0)) ||
         (iVar7 = EVP_CIPHER_CTX_ctrl(pEVar14,0x17,(int)*psVar20,key), 0 < iVar7)) {
        lVar16 = EVP_CIPHER_get0_provider(cipher);
        if ((lVar16 != 0) &&
           (iVar7 = tls_provider_set_tls_params(param_1,pEVar14,cipher,uVar18), iVar7 == 0)) {
          return 0;
        }
        *(undefined4 *)(param_1 + 0x7c) = 0;
        return 1;
      }
      ERR_new();
      uVar18 = 0x182;
    }
  }
  else {
    pEVar14 = *(EVP_CIPHER_CTX **)(param_1 + 0x848);
    uVar13 = uVar11 & 0xfffffffffffffeff;
    if (*(int *)(param_1 + 0xb2c) != 0) {
      uVar13 = uVar11 | 0x100;
    }
    *(ulong *)(param_1 + 0xa8) = uVar13;
    uVar8 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x40);
    uVar4 = *(uint *)(param_1 + 0x540) & 0xfffffffe;
    if ((uVar8 & 0x10000) != 0) {
      uVar4 = *(uint *)(param_1 + 0x540) | 1;
    }
    uVar5 = uVar4 & 0xfffffffb;
    if ((uVar8 & 0x20000) != 0) {
      uVar5 = uVar4 | 4;
    }
    *(uint *)(param_1 + 0x540) = uVar5;
    if (pEVar14 == (EVP_CIPHER_CTX *)0x0) {
      pEVar14 = EVP_CIPHER_CTX_new();
      *(EVP_CIPHER_CTX **)(param_1 + 0x848) = pEVar14;
      if (pEVar14 == (EVP_CIPHER_CTX *)0x0) {
        ERR_new(0);
        ERR_set_debug("ssl/t1_enc.c",0xc6,"tls1_change_cipher_state");
        uVar18 = 0xc0100;
        goto LAB_00146663;
      }
      EVP_CIPHER_CTX_reset();
      uVar17 = 0;
      pEVar14 = *(EVP_CIPHER_CTX **)(param_1 + 0x848);
    }
    local_60 = ssl_replace_hash(param_1 + 0x860,0);
    if (local_60 != 0) {
      COMP_CTX_free(*(COMP_CTX **)(param_1 + 0x870));
      *(undefined8 *)(param_1 + 0x870) = 0;
      if (lVar16 != 0) {
        pCVar9 = COMP_CTX_new(*(COMP_METHOD **)(lVar16 + 0x10));
        *(COMP_CTX **)(param_1 + 0x870) = pCVar9;
        if (pCVar9 == (COMP_CTX *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/t1_enc.c",0xda,"tls1_change_cipher_state");
          uVar18 = 0x8e;
          goto LAB_00146663;
        }
      }
      if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
        RECORD_LAYER_reset_read_sequence(param_1 + 0xc58);
      }
      key = (uchar *)(param_1 + 0xb8);
      psVar20 = (size_t *)(param_1 + 0xb0);
      if (uVar17 != 0) goto LAB_001465e6;
      goto LAB_001462ad;
    }
    ERR_new();
    uVar18 = 0xd1;
  }
LAB_00146650:
  ERR_set_debug("ssl/t1_enc.c",uVar18,"tls1_change_cipher_state");
  uVar18 = 0xc0103;
LAB_00146663:
  ossl_statem_fatal(param_1,0x50,uVar18,0);
  return 0;
}