int SSL_use_certificate(SSL *ssl, X509 *x)
{
    int rv;
    if (x == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_PASSED_NULL_PARAMETER);
        return 0;
    }

    rv = ssl_security_cert(ssl, NULL, x, 0, 1);
    if (rv != 1) {
        ERR_raise(ERR_LIB_SSL, rv);
        return 0;
    }

    return ssl_set_cert(ssl->cert, x);
}