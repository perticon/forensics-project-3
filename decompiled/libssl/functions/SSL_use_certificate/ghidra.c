int SSL_use_certificate(SSL *ssl,X509 *x)

{
  int iVar1;
  
  if (x == (X509 *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",0x23,"SSL_use_certificate");
    ERR_set_error(0x14,0xc0102,0);
    return 0;
  }
  iVar1 = ssl_security_cert(ssl,0,x,0,1);
  if (iVar1 != 1) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",0x29,"SSL_use_certificate");
    ERR_set_error(0x14,iVar1,0);
    return 0;
  }
  iVar1 = ssl_set_cert(ssl[3].d1,x);
  return iVar1;
}