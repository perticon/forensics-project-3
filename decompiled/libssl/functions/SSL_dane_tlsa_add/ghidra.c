SSL_dane_tlsa_add(long param_1,byte param_2,byte param_3,byte param_4,uchar *param_5,size_t param_6)

{
  long *plVar1;
  byte bVar2;
  int iVar3;
  byte *ptr;
  void *__dest;
  X509 *pXVar4;
  EVP_PKEY *pEVar5;
  byte *pbVar6;
  long lVar7;
  undefined8 uVar8;
  size_t length;
  int iVar9;
  long in_FS_OFFSET;
  bool bVar10;
  uchar *local_58;
  X509 *local_50;
  EVP_PKEY *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(long *)(param_1 + 0x4e8) == 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x119,"dane_tlsa_add");
    ERR_set_error(0x14,0xaf,0);
    uVar8 = 0xffffffff;
    goto LAB_00136526;
  }
  iVar3 = (int)param_6;
  if ((iVar3 < 0) || (length = (size_t)iVar3, param_6 != length)) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x11e,"dane_tlsa_add");
    ERR_set_error(0x14,0xbd,0);
    uVar8 = 0;
    goto LAB_00136526;
  }
  if (3 < param_2) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x123,"dane_tlsa_add");
    ERR_set_error(0x14,0xb8,0);
    uVar8 = 0;
    goto LAB_00136526;
  }
  if (1 < param_3) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x128,"dane_tlsa_add");
    ERR_set_error(0x14,0xca,0);
    uVar8 = 0;
    goto LAB_00136526;
  }
  if (param_4 != 0) {
    plVar1 = *(long **)(param_1 + 0x4e0) + 2;
    if ((*(byte *)plVar1 <= param_4 && param_4 != *(byte *)plVar1) ||
       (*(long *)(**(long **)(param_1 + 0x4e0) + (ulong)param_4 * 8) == 0)) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x12f,"dane_tlsa_add");
      ERR_set_error(0x14,200,0);
      uVar8 = 0;
      goto LAB_00136526;
    }
    iVar9 = EVP_MD_get_size();
    if (param_6 != (long)iVar9) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x135,"dane_tlsa_add");
      ERR_set_error(0x14,0xc0,0);
      uVar8 = 0;
      goto LAB_00136526;
    }
  }
  if (param_5 == (uchar *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x139,"dane_tlsa_add");
    ERR_set_error(0x14,0xcb,0);
    uVar8 = 0;
    goto LAB_00136526;
  }
  ptr = (byte *)CRYPTO_zalloc(0x20,"ssl/ssl_lib.c",0x13d);
  if (ptr == (byte *)0x0) {
    ERR_new();
    uVar8 = 0x13e;
  }
  else {
    ptr[1] = param_3;
    *ptr = param_2;
    ptr[2] = param_4;
    __dest = CRYPTO_malloc(iVar3,"ssl/ssl_lib.c",0x145);
    *(void **)(ptr + 8) = __dest;
    if (__dest == (void *)0x0) {
      CRYPTO_free((void *)0x0);
      EVP_PKEY_free(*(EVP_PKEY **)(ptr + 0x18));
      CRYPTO_free(ptr);
      ERR_new();
      uVar8 = 0x148;
    }
    else {
      memcpy(__dest,param_5,param_6);
      *(size_t *)(ptr + 0x10) = param_6;
      if (param_4 == 0) {
        local_50 = (X509 *)0x0;
        local_48 = (EVP_PKEY *)0x0;
        local_58 = param_5;
        if (param_3 != 1) {
          pXVar4 = d2i_X509(&local_50,&local_58,length);
          if (((pXVar4 == (X509 *)0x0) || (local_58 < param_5)) ||
             ((long)local_58 - (long)param_5 != param_6)) {
            CRYPTO_free(*(void **)(ptr + 8));
            EVP_PKEY_free(*(EVP_PKEY **)(ptr + 0x18));
            CRYPTO_free(ptr);
            ERR_new();
            uVar8 = 0x159;
          }
          else {
            lVar7 = X509_get0_pubkey(local_50);
            if (lVar7 != 0) {
              if ((5U >> (param_2 & 0x1f) & 1) == 0) {
                X509_free(local_50);
                goto LAB_001368e0;
              }
              lVar7 = *(long *)(param_1 + 0x4f0);
              if (lVar7 == 0) {
                lVar7 = OPENSSL_sk_new_null();
                *(long *)(param_1 + 0x4f0) = lVar7;
                if (lVar7 != 0) goto LAB_00136b77;
              }
              else {
LAB_00136b77:
                iVar3 = OPENSSL_sk_push(lVar7,local_50);
                if (iVar3 != 0) goto LAB_001368e0;
              }
              ERR_new();
              ERR_set_debug("ssl/ssl_lib.c",0x171,"dane_tlsa_add");
              ERR_set_error(0x14,0xc0100,0);
              X509_free(local_50);
              CRYPTO_free(*(void **)(ptr + 8));
              EVP_PKEY_free(*(EVP_PKEY **)(ptr + 0x18));
              CRYPTO_free(ptr);
              uVar8 = 0xffffffff;
              goto LAB_00136526;
            }
            CRYPTO_free(*(void **)(ptr + 8));
            EVP_PKEY_free(*(EVP_PKEY **)(ptr + 0x18));
            CRYPTO_free(ptr);
            ERR_new();
            uVar8 = 0x15e;
          }
          ERR_set_debug("ssl/ssl_lib.c",uVar8,"dane_tlsa_add");
          ERR_set_error(0x14,0xb4,0);
          uVar8 = 0;
          goto LAB_00136526;
        }
        pEVar5 = d2i_PUBKEY(&local_48,&local_58,length);
        if (((pEVar5 == (EVP_PKEY *)0x0) || (local_58 < param_5)) ||
           ((long)local_58 - (long)param_5 != param_6)) {
          CRYPTO_free(*(void **)(ptr + 8));
          EVP_PKEY_free(*(EVP_PKEY **)(ptr + 0x18));
          CRYPTO_free(ptr);
          ERR_new();
          ERR_set_debug("ssl/ssl_lib.c",0x17c,"dane_tlsa_add");
          ERR_set_error(0x14,0xc9,0);
          uVar8 = 0;
          goto LAB_00136526;
        }
        if (param_2 == 2) {
          *(EVP_PKEY **)(ptr + 0x18) = local_48;
        }
        else {
          EVP_PKEY_free(local_48);
        }
      }
LAB_001368e0:
      iVar9 = 0;
      iVar3 = OPENSSL_sk_num(*(undefined8 *)(param_1 + 0x4e8));
      if (0 < iVar3) {
        do {
          pbVar6 = (byte *)OPENSSL_sk_value(*(undefined8 *)(param_1 + 0x4e8),iVar9);
          if (*pbVar6 <= param_2) {
            if (*pbVar6 <= param_2 && param_2 != *pbVar6) break;
            bVar10 = pbVar6[1] <= param_3;
            if ((bVar10) &&
               ((bVar10 && param_3 != pbVar6[1] ||
                (lVar7 = *(long *)(*(long *)(param_1 + 0x4e0) + 8),
                bVar2 = *(byte *)(lVar7 + (ulong)param_4),
                pbVar6 = (byte *)(lVar7 + (ulong)pbVar6[2]), *pbVar6 < bVar2 || *pbVar6 == bVar2))))
            break;
          }
          iVar9 = iVar9 + 1;
        } while (iVar3 != iVar9);
      }
      iVar3 = OPENSSL_sk_insert(*(undefined8 *)(param_1 + 0x4e8),ptr,iVar9);
      if (iVar3 != 0) {
        *(uint *)(param_1 + 0x508) = *(uint *)(param_1 + 0x508) | 1 << (param_2 & 0x1f);
        uVar8 = 1;
        goto LAB_00136526;
      }
      CRYPTO_free(*(void **)(ptr + 8));
      EVP_PKEY_free(*(EVP_PKEY **)(ptr + 0x18));
      CRYPTO_free(ptr);
      ERR_new();
      uVar8 = 0x1ae;
    }
  }
  ERR_set_debug("ssl/ssl_lib.c",uVar8,"dane_tlsa_add");
  ERR_set_error(0x14,0xc0100,0);
  uVar8 = 0xffffffff;
LAB_00136526:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar8;
}