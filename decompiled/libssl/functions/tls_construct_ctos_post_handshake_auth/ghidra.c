undefined8 tls_construct_ctos_post_handshake_auth(long param_1,undefined8 param_2)

{
  int iVar1;
  
  if (*(int *)(param_1 + 0xbac) == 0) {
    return 2;
  }
  iVar1 = WPACKET_put_bytes__(param_2,0x31,2);
  if (iVar1 != 0) {
    iVar1 = WPACKET_start_sub_packet_len__(param_2,2);
    if (iVar1 != 0) {
      iVar1 = WPACKET_close(param_2);
      if (iVar1 != 0) {
        *(undefined4 *)(param_1 + 0xba8) = 1;
        return 1;
      }
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x4b1,"tls_construct_ctos_post_handshake_auth");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}