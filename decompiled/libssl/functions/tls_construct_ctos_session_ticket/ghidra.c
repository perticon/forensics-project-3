undefined8 tls_construct_ctos_session_ticket(long param_1,undefined8 param_2)

{
  int *piVar1;
  ushort *puVar2;
  int iVar3;
  void *pvVar4;
  undefined8 uVar5;
  ulong __n;
  
  iVar3 = tls_use_ticket();
  if (iVar3 == 0) {
    return 2;
  }
  piVar1 = *(int **)(param_1 + 0x918);
  if (*(int *)(param_1 + 0x3c) == 0) {
    if (piVar1 == (int *)0x0) goto LAB_00160a0b;
    if ((*(long *)(piVar1 + 0xce) == 0) || (*piVar1 == 0x304)) goto LAB_00160998;
    __n = *(ulong *)(piVar1 + 0xd0);
    if (__n == 0) goto LAB_00160a0b;
  }
  else {
    if (piVar1 == (int *)0x0) {
LAB_00160a0b:
      if ((*(long *)(param_1 + 0xae0) != 0) && (*(long *)(*(long *)(param_1 + 0xae0) + 8) == 0)) {
        return 2;
      }
    }
    else {
LAB_00160998:
      puVar2 = *(ushort **)(param_1 + 0xae0);
      if (puVar2 != (ushort *)0x0) {
        if (*(long *)(puVar2 + 4) == 0) {
          return 2;
        }
        __n = (ulong)*puVar2;
        pvVar4 = CRYPTO_malloc((uint)*puVar2,"ssl/statem/extensions_clnt.c",0x11d);
        *(void **)(piVar1 + 0xce) = pvVar4;
        pvVar4 = *(void **)(*(long *)(param_1 + 0x918) + 0x338);
        if (pvVar4 == (void *)0x0) {
          ERR_new();
          uVar5 = 0x11f;
          goto LAB_00160ad1;
        }
        memcpy(pvVar4,*(void **)(*(long *)(param_1 + 0xae0) + 8),__n);
        *(ulong *)(*(long *)(param_1 + 0x918) + 0x340) = __n;
        if (__n != 0) goto LAB_00160a25;
        goto LAB_00160a0b;
      }
    }
    __n = 0;
  }
LAB_00160a25:
  iVar3 = WPACKET_put_bytes__(param_2,0x23,2);
  if ((iVar3 != 0) &&
     (iVar3 = WPACKET_sub_memcpy__
                        (param_2,*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x338),__n,2),
     iVar3 != 0)) {
    return 1;
  }
  ERR_new();
  uVar5 = 0x12f;
LAB_00160ad1:
  ERR_set_debug("ssl/statem/extensions_clnt.c",uVar5,"tls_construct_ctos_session_ticket");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}