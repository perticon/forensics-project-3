tls_parse_all_extensions
          (long param_1,uint param_2,long param_3,undefined8 param_4,undefined8 param_5,int param_6)

{
  long lVar1;
  int iVar2;
  undefined4 *puVar3;
  ulong uVar4;
  undefined1 *puVar5;
  
  lVar1 = *(long *)(*(long *)(param_1 + 0x898) + 0x1e0);
  if (lVar1 != -0x1a) {
    uVar4 = 0;
    do {
      iVar2 = tls_parse_extension(param_1,uVar4 & 0xffffffff,param_2,param_3,param_4,param_5);
      if (iVar2 == 0) {
        return 0;
      }
      uVar4 = uVar4 + 1;
    } while (lVar1 + 0x1aU != uVar4);
  }
  if (param_6 != 0) {
    puVar5 = ext_defs;
    puVar3 = (undefined4 *)(param_3 + 0x10);
    do {
      if (((*(code **)(puVar5 + 0x30) != (code *)0x0) && ((*(uint *)(puVar5 + 4) & param_2) != 0))
         && (iVar2 = (**(code **)(puVar5 + 0x30))(param_1,param_2,*puVar3), iVar2 == 0)) {
        return 0;
      }
      puVar5 = puVar5 + 0x38;
      puVar3 = puVar3 + 10;
    } while (puVar5 != &DAT_001a7bf0);
  }
  return 1;
}