int SSL_write(SSL *ssl,void *buf,int num)

{
  int iVar1;
  long in_FS_OFFSET;
  int local_18 [2];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  if (num < 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x849,"SSL_write");
    ERR_set_error(0x14,0x10f,0);
    iVar1 = -1;
  }
  else {
    iVar1 = ssl_write_internal(ssl,buf,(long)num,local_18);
    if (0 < iVar1) {
      iVar1 = local_18[0];
    }
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}