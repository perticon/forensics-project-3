undefined8 custom_exts_copy(long *param_1,long *param_2)

{
  long lVar1;
  undefined8 uVar2;
  bool bVar3;
  long lVar4;
  ulong uVar5;
  long lVar6;
  
  if (param_2[1] == 0) {
    return 1;
  }
  lVar1 = CRYPTO_memdup(*param_2,param_2[1] * 0x38,"ssl/statem/extensions_cust.c");
  *param_1 = lVar1;
  if (lVar1 == 0) {
    return 0;
  }
  param_1[1] = param_2[1];
  uVar5 = param_2[1];
  if (uVar5 == 0) {
    return 1;
  }
  bVar3 = false;
  lVar4 = 0;
  do {
    lVar6 = *param_2 + lVar4 * 0x38;
    if (*(code **)(lVar6 + 0x10) == custom_ext_add_old_cb_wrap) {
      lVar1 = lVar1 + lVar4 * 0x38;
      if (!bVar3) {
        uVar2 = CRYPTO_memdup(*(undefined8 *)(lVar6 + 0x20),0x18,"ssl/statem/extensions_cust.c",299)
        ;
        *(undefined8 *)(lVar1 + 0x20) = uVar2;
        lVar6 = CRYPTO_memdup(*(undefined8 *)(lVar6 + 0x30),0x10,"ssl/statem/extensions_cust.c");
        uVar5 = param_2[1];
        *(long *)(lVar1 + 0x30) = lVar6;
        bVar3 = *(long *)(lVar1 + 0x20) == 0 || lVar6 == 0;
        goto LAB_00164d88;
      }
      *(undefined8 *)(lVar1 + 0x20) = 0;
      *(undefined8 *)(lVar1 + 0x30) = 0;
      if (uVar5 <= lVar4 + 1U) goto LAB_00164dd3;
    }
    else {
LAB_00164d88:
      if (uVar5 <= lVar4 + 1U) {
LAB_00164dd3:
        if (!bVar3) {
          return 1;
        }
        custom_exts_free(param_1);
        return 0;
      }
    }
    lVar4 = lVar4 + 1;
    lVar1 = *param_1;
  } while( true );
}