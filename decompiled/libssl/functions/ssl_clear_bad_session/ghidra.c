undefined8 ssl_clear_bad_session(long param_1)

{
  int iVar1;
  
  if (*(long *)(param_1 + 0x918) == 0) {
    return 0;
  }
  if ((*(byte *)(param_1 + 0x44) & 1) == 0) {
    iVar1 = SSL_in_init();
    if (iVar1 == 0) {
      iVar1 = SSL_in_before(param_1);
      if (iVar1 == 0) {
        SSL_CTX_remove_session(*(SSL_CTX **)(param_1 + 0xb88),*(SSL_SESSION **)(param_1 + 0x918));
        return 1;
      }
    }
  }
  return 0;
}