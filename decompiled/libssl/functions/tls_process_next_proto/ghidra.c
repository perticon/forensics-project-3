undefined8 tls_process_next_proto(long param_1,byte **param_2)

{
  byte *pbVar1;
  byte *pbVar2;
  byte *pbVar3;
  long lVar4;
  byte *pbVar5;
  byte *pbVar6;
  
  if (param_2[1] != (byte *)0x0) {
    pbVar3 = param_2[1] + -1;
    pbVar6 = (byte *)(ulong)**param_2;
    if (pbVar6 <= pbVar3) {
      pbVar1 = *param_2 + 1;
      pbVar3 = pbVar3 + -(long)pbVar6;
      pbVar2 = pbVar1 + (long)pbVar6;
      param_2[1] = pbVar3;
      *param_2 = pbVar2;
      if (pbVar3 != (byte *)0x0) {
        pbVar5 = (byte *)(ulong)*pbVar2;
        pbVar3 = pbVar3 + -1;
        if (pbVar5 <= pbVar3) {
          *param_2 = pbVar2 + 1 + (long)pbVar5;
          param_2[1] = pbVar3 + -(long)pbVar5;
          if (pbVar3 + -(long)pbVar5 == (byte *)0x0) {
            CRYPTO_free(*(void **)(param_1 + 0xb18));
            *(undefined8 *)(param_1 + 0xb18) = 0;
            if (pbVar6 != (byte *)0x0) {
              lVar4 = CRYPTO_memdup(pbVar1,pbVar6,"include/internal/packet.h",0x1c5);
              *(long *)(param_1 + 0xb18) = lVar4;
              if (lVar4 == 0) {
                *(undefined8 *)(param_1 + 0xb20) = 0;
                ERR_new();
                ERR_set_debug("ssl/statem/statem_srvr.c",0xfd7,"tls_process_next_proto");
                ossl_statem_fatal(param_1,0x50,0xc0103,0);
                return 0;
              }
            }
            *(byte **)(param_1 + 0xb20) = pbVar6;
            return 3;
          }
        }
      }
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_srvr.c",0xfd1,"tls_process_next_proto");
  ossl_statem_fatal(param_1,0x32,0x9f,0);
  return 0;
}