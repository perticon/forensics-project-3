int tls1_set_server_sigalgs(long param_1)

{
  int iVar1;
  long lVar2;
  long lVar3;
  long lVar4;
  ulong uVar5;
  long in_FS_OFFSET;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  CRYPTO_free(*(void **)(param_1 + 0x1da8));
  *(undefined8 *)(param_1 + 0x1da8) = 0;
  *(undefined8 *)(param_1 + 0x1db0) = 0;
  *(undefined4 *)(param_1 + 0x3d8) = 0;
  *(undefined (*) [16])(param_1 + 0x3b8) = (undefined  [16])0x0;
  *(undefined (*) [16])(param_1 + 0x3c8) = (undefined  [16])0x0;
  if ((*(long *)(param_1 + 0x398) == 0) && (*(long *)(param_1 + 0x390) == 0)) {
    uVar5 = 0;
    lVar2 = tls12_get_psigalgs(0,param_1,1,&local_28);
    do {
      lVar3 = tls1_get_legacy_sigalg(param_1,uVar5 & 0xffffffff);
      if ((lVar3 != 0) && (lVar2 != 0)) {
        lVar4 = 0;
        do {
          if (*(short *)(lVar3 + 8) == *(short *)(local_28 + lVar4 * 2)) {
            *(undefined4 *)(param_1 + 0x3b8 + uVar5 * 4) = 2;
            break;
          }
          lVar4 = lVar4 + 1;
        } while (lVar2 != lVar4);
      }
      uVar5 = uVar5 + 1;
    } while (uVar5 != 9);
    iVar1 = 1;
  }
  else {
    iVar1 = tls1_process_sigalgs(0,param_1);
    if (iVar1 == 0) {
      ERR_new();
      ERR_set_debug("ssl/t1_lib.c",0x6e7,"tls1_set_server_sigalgs");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
    else {
      iVar1 = 1;
      if (*(long *)(param_1 + 0x1da8) == 0) {
        ERR_new();
        iVar1 = 0;
        ERR_set_debug("ssl/t1_lib.c",0x6ee,"tls1_set_server_sigalgs");
        ossl_statem_fatal(param_1,0x28,0x178,0);
      }
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}