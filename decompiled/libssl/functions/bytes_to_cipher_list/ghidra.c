bytes_to_cipher_list
          (undefined8 param_1,long *param_2,long *param_3,long *param_4,int param_5,int param_6)

{
  long lVar1;
  int iVar2;
  long lVar3;
  long lVar4;
  undefined8 uVar5;
  ulong uVar6;
  int *piVar7;
  uint uVar8;
  char *pcVar9;
  ulong uVar10;
  uint uVar11;
  ulong uVar12;
  long in_FS_OFFSET;
  char local_43 [3];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar12 = (ulong)(param_5 != 0) + 2;
  if (param_2[1] == 0) {
    if (param_6 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x163f,"bytes_to_cipher_list");
      ERR_set_error(0x14,0xb7,0);
      uVar5 = 0;
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x163d,"bytes_to_cipher_list");
      ossl_statem_fatal(param_1,0x2f,0xb7,0);
      uVar5 = 0;
    }
    goto LAB_0013d050;
  }
  if ((ulong)param_2[1] % uVar12 != 0) {
    if (param_6 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x1648,"bytes_to_cipher_list");
      ERR_set_error(0x14,0x97,0);
      uVar5 = 0;
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x1645,"bytes_to_cipher_list");
      ossl_statem_fatal(param_1,0x32,0x97,0);
      uVar5 = 0;
    }
    goto LAB_0013d050;
  }
  lVar3 = OPENSSL_sk_new_null();
  lVar4 = OPENSSL_sk_new_null();
  if ((lVar3 == 0) || (lVar4 == 0)) {
    if (param_6 == 0) {
      ERR_new();
      uVar5 = 0x1652;
      goto LAB_0013d2c1;
    }
    ERR_new();
    uVar5 = 0x1650;
    goto LAB_0013d26e;
  }
  uVar6 = param_2[1];
  if (uVar12 <= uVar6) {
    pcVar9 = local_43 + 1;
    if (param_5 == 0) {
      pcVar9 = local_43;
    }
    uVar11 = (uint)uVar12;
    if (param_5 == 0) {
      do {
        lVar1 = *param_2;
        if (uVar11 != 0) {
          uVar8 = 0;
          do {
            uVar10 = (ulong)uVar8;
            uVar8 = uVar8 + 1;
            local_43[uVar10] = *(char *)(lVar1 + uVar10);
          } while (uVar8 < uVar11);
        }
        *param_2 = lVar1 + uVar12;
        param_2[1] = uVar6 - uVar12;
        piVar7 = (int *)ssl_get_cipher_by_char(param_1,local_43,1);
        if (piVar7 != (int *)0x0) {
          if (*piVar7 != 0) {
            iVar2 = OPENSSL_sk_push(lVar3);
            if (iVar2 == 0) goto LAB_0013d1a0;
            if (*piVar7 != 0) goto LAB_0013d385;
          }
          iVar2 = OPENSSL_sk_push(lVar4);
          if (iVar2 == 0) goto LAB_0013d1a0;
        }
LAB_0013d385:
        uVar6 = param_2[1];
      } while (uVar12 <= uVar6);
    }
    else {
      do {
        lVar1 = *param_2;
        if (uVar11 != 0) {
          uVar10 = 0;
          do {
            uVar8 = (int)uVar10 + 1;
            local_43[uVar10] = *(char *)(lVar1 + uVar10);
            uVar10 = (ulong)uVar8;
          } while (uVar8 < uVar11);
        }
        uVar6 = uVar6 - uVar12;
        *param_2 = lVar1 + uVar12;
        param_2[1] = uVar6;
        if (local_43[0] == '\0') {
          piVar7 = (int *)ssl_get_cipher_by_char(param_1,pcVar9,1);
          if (piVar7 != (int *)0x0) {
            if (*piVar7 != 0) {
              iVar2 = OPENSSL_sk_push(lVar3);
              if (iVar2 == 0) goto LAB_0013d1a0;
              if (*piVar7 != 0) goto LAB_0013d22e;
            }
            iVar2 = OPENSSL_sk_push(lVar4);
            if (iVar2 == 0) goto LAB_0013d1a0;
          }
LAB_0013d22e:
          uVar6 = param_2[1];
        }
      } while (uVar12 <= uVar6);
    }
  }
  if (uVar6 == 0) {
    if (param_3 == (long *)0x0) {
      OPENSSL_sk_free(lVar3);
    }
    else {
      *param_3 = lVar3;
    }
    if (param_4 == (long *)0x0) {
      OPENSSL_sk_free(lVar4);
      uVar5 = 1;
    }
    else {
      *param_4 = lVar4;
      uVar5 = 1;
    }
    goto LAB_0013d050;
  }
  if (param_6 == 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x1670,"bytes_to_cipher_list");
    ERR_set_error(0x14,0x10f,0);
  }
  else {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x166e,"bytes_to_cipher_list");
    ossl_statem_fatal(param_1,0x32,0x10f,0);
  }
  goto LAB_0013d290;
LAB_0013d1a0:
  if (param_6 == 0) {
    ERR_new();
    uVar5 = 0x1667;
LAB_0013d2c1:
    ERR_set_debug("ssl/ssl_lib.c",uVar5,"bytes_to_cipher_list");
    ERR_set_error(0x14,0xc0100,0);
  }
  else {
    ERR_new();
    uVar5 = 0x1665;
LAB_0013d26e:
    ERR_set_debug("ssl/ssl_lib.c",uVar5,"bytes_to_cipher_list");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
  }
LAB_0013d290:
  OPENSSL_sk_free(lVar3);
  OPENSSL_sk_free(lVar4);
  uVar5 = 0;
LAB_0013d050:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
}