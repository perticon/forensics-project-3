int ssl_get_max_send_fragment(long param_1)

{
  byte bVar1;
  
  if ((*(long *)(param_1 + 0x918) != 0) &&
     (bVar1 = *(char *)(*(long *)(param_1 + 0x918) + 0x368) - 1, bVar1 < 4)) {
    return 0x200 << (bVar1 & 0x1f);
  }
  return *(int *)(param_1 + 0xa18);
}