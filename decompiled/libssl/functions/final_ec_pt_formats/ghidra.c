int final_ec_pt_formats(long param_1)

{
  long lVar1;
  long lVar2;
  int iVar3;
  
  iVar3 = *(int *)(param_1 + 0x38);
  if ((((iVar3 == 0) && (*(long *)(param_1 + 0xaa8) != 0)) && (*(long *)(param_1 + 0xaa0) != 0)) &&
     (((*(long *)(param_1 + 0xab8) != 0 && (lVar1 = *(long *)(param_1 + 0xab0), lVar1 != 0)) &&
      ((*(uint *)(*(long *)(param_1 + 0x2e0) + 0x1c) & 4 |
       *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20) & 8) != 0)))) {
    lVar2 = 0;
    do {
      if (*(char *)(*(long *)(param_1 + 0xab8) + lVar2) == '\0') {
        if (lVar1 != lVar2) goto LAB_0015e4c0;
        break;
      }
      lVar2 = lVar2 + 1;
    } while (lVar1 != lVar2);
    ERR_new();
    ERR_set_debug("ssl/statem/extensions.c",0x417,"final_ec_pt_formats");
    ossl_statem_fatal(param_1,0x2f,0x9d,0);
  }
  else {
LAB_0015e4c0:
    iVar3 = 1;
  }
  return iVar3;
}