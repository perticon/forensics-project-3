undefined8 tls_construct_stoc_supported_versions(undefined4 *param_1,undefined8 param_2)

{
  int iVar1;
  
  if ((((*(uint *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) != 0) ||
      (iVar1 = **(int **)(param_1 + 2), iVar1 < 0x304)) || (iVar1 == 0x10000)) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_srvr.c",0x61c,"tls_construct_stoc_supported_versions");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  }
  iVar1 = WPACKET_put_bytes__(param_2,0x2b,2);
  if (((iVar1 != 0) && (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 != 0)) &&
     ((iVar1 = WPACKET_put_bytes__(param_2,*param_1,2), iVar1 != 0 &&
      (iVar1 = WPACKET_close(param_2), iVar1 != 0)))) {
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x624,"tls_construct_stoc_supported_versions");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}