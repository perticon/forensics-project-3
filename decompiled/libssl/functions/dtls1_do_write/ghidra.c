ulong dtls1_do_write(SSL *param_1,int param_2)

{
  undefined uVar1;
  undefined2 uVar2;
  void *pvVar3;
  X509_EXTENSIONS *pXVar4;
  dtls1_state_st *pdVar5;
  undefined uVar6;
  int iVar7;
  uint uVar8;
  ulong uVar9;
  undefined8 uVar10;
  ulong uVar11;
  long lVar12;
  undefined *puVar13;
  BIO *bp;
  _func_3150 *p_Var14;
  _func_3150 *p_Var15;
  uchar *puVar16;
  ssl3_state_st *psVar17;
  long lVar18;
  _func_3150 *p_Var19;
  long in_FS_OFFSET;
  _func_3150 *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar7 = dtls1_query_mtu();
  if (((iVar7 == 0) ||
      (uVar11 = *(ulong *)&param_1[1].tlsext_ocsp_exts[9].stack.sorted,
      uVar9 = dtls1_min_mtu(param_1), uVar11 < uVar9)) ||
     ((param_1->msg_callback_arg == (void *)0x0 &&
      ((param_2 == 0x16 &&
       (param_1->msg_callback !=
        (_func_3150 *)(*(long *)&param_1[1].tlsext_ocsp_exts[10].stack + 0xc))))))) {
LAB_00172960:
    uVar9 = 0xffffffff;
    goto LAB_00172966;
  }
  psVar17 = param_1[3].s3;
  puVar16 = param_1[3].packet;
  lVar18 = 0;
  if (psVar17 != (ssl3_state_st *)0x0) {
    if (puVar16 == (uchar *)0x0) {
LAB_001725be:
      uVar10 = EVP_MD_CTX_get0_md(psVar17);
      iVar7 = EVP_MD_get_size(uVar10);
      lVar18 = (long)iVar7;
    }
    else {
      uVar10 = EVP_CIPHER_CTX_get0_cipher();
      uVar11 = EVP_CIPHER_get_flags(uVar10);
      if ((uVar11 & 0x200000) == 0) {
        psVar17 = param_1[3].s3;
        goto LAB_001725be;
      }
    }
    puVar16 = param_1[3].packet;
  }
  if (puVar16 == (uchar *)0x0) {
LAB_001725f3:
    lVar12 = 0;
  }
  else {
    uVar10 = EVP_CIPHER_CTX_get0_cipher();
    iVar7 = EVP_CIPHER_get_mode(uVar10);
    if (iVar7 != 2) goto LAB_001725f3;
    iVar7 = EVP_CIPHER_CTX_get_block_size(param_1[3].packet);
    lVar12 = (long)(iVar7 * 2);
  }
  p_Var14 = param_1->msg_callback;
  param_1->rwstate = 1;
  if (p_Var14 != (_func_3150 *)0x0) {
    p_Var19 = (_func_3150 *)0x0;
    uVar6 = true;
    lVar18 = lVar18 + lVar12;
    while( true ) {
      while( true ) {
        if ((param_2 == 0x16) && (pvVar3 = param_1->msg_callback_arg, pvVar3 != (void *)0x0)) {
          if (p_Var19 == (_func_3150 *)0x0) {
            p_Var19 = *(_func_3150 **)&param_1[1].tlsext_ocsp_exts[10].stack.sorted;
          }
          else {
            if (pvVar3 < (void *)0xd) goto LAB_00172960;
            param_1->msg_callback_arg = (void *)((long)pvVar3 - 0xc);
            param_1->msg_callback = p_Var14 + 0xc;
          }
        }
        lVar12 = BIO_ctrl(param_1->wbio,0xd,0,(void *)0x0);
        uVar11 = *(ulong *)&param_1[1].tlsext_ocsp_exts[9].stack.sorted;
        uVar9 = ((int)lVar12 + 0xd) + lVar18;
        if ((uVar11 <= uVar9) ||
           (p_Var15 = (_func_3150 *)(uVar11 - uVar9), p_Var15 < (_func_3150 *)0xd)) {
          uVar11 = BIO_ctrl(param_1->wbio,0xb,0,(void *)0x0);
          uVar9 = uVar11 & 0xffffffff;
          if ((int)uVar11 < 1) {
            param_1->rwstate = 2;
            goto LAB_00172966;
          }
          uVar11 = *(ulong *)&param_1[1].tlsext_ocsp_exts[9].stack.sorted;
          if (uVar11 <= lVar18 + 0x19U) goto LAB_00172960;
          p_Var15 = (_func_3150 *)((uVar11 - 0xd) - lVar18);
        }
        if ((_func_3150 *)((ulong)param_1->msg_callback & 0xffffffff) <= p_Var15) {
          p_Var15 = param_1->msg_callback;
        }
        uVar8 = ssl_get_max_send_fragment(param_1);
        if ((_func_3150 *)(ulong)uVar8 < p_Var15) {
          uVar8 = ssl_get_max_send_fragment(param_1);
          p_Var15 = (_func_3150 *)(ulong)uVar8;
        }
        if (param_2 == 0x16) {
          if (p_Var15 < (_func_3150 *)0xc) goto LAB_00172960;
          pXVar4 = param_1[1].tlsext_ocsp_exts;
          pdVar5 = param_1->d1;
          pXVar4[10].stack.comp = (_func_257 *)(p_Var15 + -0xc);
          puVar13 = (undefined *)((long)param_1->msg_callback_arg + *(long *)(pdVar5->cookie + 4));
          uVar1 = *(undefined *)&pXVar4[9].stack.comp;
          *(_func_3150 **)&pXVar4[10].stack.sorted = p_Var19;
          *puVar13 = uVar1;
          puVar13[1] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack >> 0x10);
          puVar13[2] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack >> 8);
          puVar13[3] = (char)*(undefined8 *)&pXVar4[10].stack;
          puVar13[4] = *(undefined *)((long)&pXVar4[10].stack.data + 1);
          puVar13[5] = (char)*(undefined2 *)&pXVar4[10].stack.data;
          puVar13[6] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack.sorted >> 0x10);
          puVar13[7] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack.sorted >> 8);
          puVar13[8] = (char)*(undefined8 *)&pXVar4[10].stack.sorted;
          puVar13[9] = (char)((ulong)pXVar4[10].stack.comp >> 0x10);
          puVar13[10] = (char)((ulong)pXVar4[10].stack.comp >> 8);
          puVar13[0xb] = (char)pXVar4[10].stack.comp;
        }
        uVar10 = 0x1727e2;
        iVar7 = dtls1_write_bytes(param_1,param_2,
                                  (long)param_1->msg_callback_arg +
                                  *(long *)(param_1->d1->cookie + 4),p_Var15,&local_48);
        if (iVar7 < 1) break;
        if (local_48 != p_Var15) goto LAB_00172960;
        if ((param_2 == 0x16) &&
           (pXVar4 = param_1[1].tlsext_ocsp_exts, *(int *)&pXVar4[0x10].stack.field_0x4 == 0)) {
          puVar13 = (undefined *)
                    ((long)param_1->msg_callback_arg + *(long *)(param_1->d1->cookie + 4));
          if ((p_Var19 == (_func_3150 *)0x0) && (param_1->version != 0x100)) {
            *puVar13 = *(undefined *)&pXVar4[9].stack.comp;
            puVar13[1] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack >> 0x10);
            puVar13[2] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack >> 8);
            puVar13[3] = (char)*(undefined8 *)&pXVar4[10].stack;
            puVar13[4] = *(undefined *)((long)&pXVar4[10].stack.data + 1);
            uVar2 = *(undefined2 *)&pXVar4[10].stack.data;
            *(undefined2 *)(puVar13 + 6) = 0;
            puVar13[5] = (char)uVar2;
            puVar13[8] = 0;
            puVar13[9] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack >> 0x10);
            puVar13[10] = (char)((ulong)*(undefined8 *)&pXVar4[10].stack >> 8);
            puVar13[0xb] = (char)*(undefined8 *)&pXVar4[10].stack;
            p_Var14 = local_48;
          }
          else {
            puVar13 = puVar13 + 0xc;
            p_Var14 = p_Var15 + -0xc;
          }
          uVar10 = 0x172910;
          iVar7 = ssl3_finish_mac(param_1,puVar13,p_Var14);
          p_Var15 = local_48;
          if (iVar7 == 0) goto LAB_00172960;
        }
        if (param_1->msg_callback == p_Var15) {
          if ((code *)param_1[1].tlsext_ocsp_resp != (code *)0x0) {
            (*(code *)param_1[1].tlsext_ocsp_resp)
                      (1,param_1->version,param_2,*(undefined8 *)(param_1->d1->cookie + 4),
                       p_Var15 + (long)param_1->msg_callback_arg,param_1,
                       *(undefined8 *)&param_1[1].tlsext_ocsp_resplen,uVar10);
          }
          uVar9 = 1;
          *(undefined (*) [16])&param_1->msg_callback = (undefined  [16])0x0;
          goto LAB_00172966;
        }
        param_1->msg_callback_arg = (_func_3150 *)((long)param_1->msg_callback_arg + (long)p_Var15);
        pXVar4 = param_1[1].tlsext_ocsp_exts;
        p_Var14 = param_1->msg_callback + -(long)p_Var15;
        local_48 = p_Var15 + -0xc;
        p_Var19 = p_Var19 + (long)local_48;
        param_1->msg_callback = p_Var14;
        *(_func_3150 **)&pXVar4[10].stack.sorted = p_Var19;
        pXVar4[10].stack.comp = (_func_257 *)0x0;
      }
      if (!(bool)uVar6) goto LAB_00172960;
      bp = SSL_get_wbio(param_1);
      lVar12 = BIO_ctrl(bp,0x2b,0,(void *)0x0);
      if (((lVar12 < 1) || (uVar11 = SSL_get_options(param_1), (uVar11 & 0x1000) != 0)) ||
         (iVar7 = dtls1_query_mtu(param_1), iVar7 == 0)) goto LAB_00172960;
      p_Var14 = param_1->msg_callback;
      if (p_Var14 == (_func_3150 *)0x0) break;
      uVar6 = false;
    }
  }
  uVar9 = 0;
LAB_00172966:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar9;
}