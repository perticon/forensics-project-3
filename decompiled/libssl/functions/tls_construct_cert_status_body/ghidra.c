undefined8 tls_construct_cert_status_body(long param_1,undefined8 param_2)

{
  int iVar1;
  
  iVar1 = WPACKET_put_bytes__(param_2,*(undefined4 *)(param_1 + 0xa60),1);
  if (iVar1 != 0) {
    iVar1 = WPACKET_sub_memcpy__
                      (param_2,*(undefined8 *)(param_1 + 0xa88),*(undefined8 *)(param_1 + 0xa90),3);
    if (iVar1 != 0) {
      return 1;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_srvr.c",0xfac,"tls_construct_cert_status_body");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}