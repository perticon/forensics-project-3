SSL_SESSION * lookup_sess_in_cache(undefined4 *param_1,undefined8 param_2,ulong param_3)

{
  int iVar1;
  SSL_SESSION *c;
  long lVar2;
  long in_FS_OFFSET;
  int local_3dc;
  undefined4 local_3d8 [148];
  ulong local_188;
  undefined local_180 [336];
  long local_30;
  
  lVar2 = *(long *)(param_1 + 0x2e2);
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*(byte *)(lVar2 + 0x51) & 1) == 0) {
    local_3d8[0] = *param_1;
    if (param_3 < 0x21) {
      __memcpy_chk(local_180,param_2,param_3,0x148);
      local_188 = param_3;
      iVar1 = CRYPTO_THREAD_read_lock(*(undefined8 *)(lVar2 + 0x3c8));
      if (iVar1 != 0) {
        c = (SSL_SESSION *)
            OPENSSL_LH_retrieve(*(undefined8 *)(*(long *)(param_1 + 0x2e2) + 0x30),local_3d8);
        if (c != (SSL_SESSION *)0x0) {
          SSL_SESSION_up_ref(c);
          CRYPTO_THREAD_unlock(*(undefined8 *)(*(long *)(param_1 + 0x2e2) + 0x3c8));
          goto LAB_00143c40;
        }
        CRYPTO_THREAD_unlock(*(undefined8 *)(*(long *)(param_1 + 0x2e2) + 0x3c8));
        LOCK();
        *(int *)(*(long *)(param_1 + 0x2e2) + 0x90) =
             *(int *)(*(long *)(param_1 + 0x2e2) + 0x90) + 1;
        lVar2 = *(long *)(param_1 + 0x2e2);
        goto LAB_00143ca0;
      }
    }
    c = (SSL_SESSION *)0x0;
  }
  else {
LAB_00143ca0:
    c = (SSL_SESSION *)0x0;
    if (*(code **)(lVar2 + 0x70) != (code *)0x0) {
      local_3dc = 1;
      c = (SSL_SESSION *)
          (**(code **)(lVar2 + 0x70))(param_1,param_2,param_3 & 0xffffffff,&local_3dc);
      if (c != (SSL_SESSION *)0x0) {
        LOCK();
        *(int *)(*(long *)(param_1 + 0x2e2) + 0xa0) =
             *(int *)(*(long *)(param_1 + 0x2e2) + 0xa0) + 1;
        if (local_3dc != 0) {
          SSL_SESSION_up_ref(c);
        }
        if ((*(byte *)((long)&(*(SSL_CTX **)(param_1 + 0x2e2))->new_session_cb + 1) & 2) == 0) {
          SSL_CTX_add_session(*(SSL_CTX **)(param_1 + 0x2e2),c);
        }
      }
    }
  }
LAB_00143c40:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return c;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}