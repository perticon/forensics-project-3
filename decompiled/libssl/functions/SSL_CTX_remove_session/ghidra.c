int SSL_CTX_remove_session(SSL_CTX *param_1,SSL_SESSION *c)

{
  undefined *puVar1;
  undefined *puVar2;
  int iVar3;
  long lVar4;
  SSL_SESSION *ses;
  
  if (c == (SSL_SESSION *)0x0) {
    return 0;
  }
  if ((c->tlsext_tick_lifetime_hint != 0) &&
     (iVar3 = CRYPTO_THREAD_write_lock(*(undefined8 *)(param_1[1].sid_ctx + 0x10)), iVar3 != 0)) {
    lVar4 = OPENSSL_LH_retrieve(param_1->session_cache_head,c);
    if (lVar4 != 0) {
      ses = (SSL_SESSION *)OPENSSL_LH_delete(param_1->session_cache_head);
      puVar1 = *(undefined **)(ses[1].krb5_client_princ + 0x40);
      if ((puVar1 != (undefined *)0x0) &&
         (puVar2 = *(undefined **)(ses[1].krb5_client_princ + 0x38), puVar2 != (undefined *)0x0)) {
        if ((long *)puVar1 == &param_1->session_timeout) {
          if ((int *)puVar2 == &param_1->session_cache_mode) {
            *(undefined (*) [16])&param_1->session_cache_mode = (undefined  [16])0x0;
          }
          else {
            param_1->session_timeout = (long)puVar2;
            *(undefined **)(puVar2 + 0x328) = puVar1;
          }
        }
        else if ((int *)puVar2 == &param_1->session_cache_mode) {
          *(undefined **)&param_1->session_cache_mode = puVar1;
          *(undefined **)(puVar1 + 800) = puVar2;
        }
        else {
          *(undefined **)(puVar1 + 800) = puVar2;
          *(undefined **)(puVar2 + 0x328) = puVar1;
        }
        *(undefined8 *)(ses[1].krb5_client_princ + 0xa8) = 0;
        *(undefined (*) [16])(ses[1].krb5_client_princ + 0x38) = (undefined  [16])0x0;
      }
      *(undefined4 *)(c[1].session_id + 0x10) = 1;
      CRYPTO_THREAD_unlock(*(undefined8 *)(param_1[1].sid_ctx + 0x10));
      if (*(code **)&param_1->stats != (code *)0x0) {
        (**(code **)&param_1->stats)(param_1,c);
      }
      SSL_SESSION_free(ses);
      return 1;
    }
    *(undefined4 *)(c[1].session_id + 0x10) = 1;
    CRYPTO_THREAD_unlock(*(undefined8 *)(param_1[1].sid_ctx + 0x10),0);
    if (*(code **)&param_1->stats != (code *)0x0) {
      (**(code **)&param_1->stats)(param_1,c);
    }
  }
  return 0;
}