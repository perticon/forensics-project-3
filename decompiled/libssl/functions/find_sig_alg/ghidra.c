long find_sig_alg(long param_1,undefined8 param_2,long param_3)

{
  int iVar1;
  int iVar2;
  long lVar3;
  ulong uVar4;
  ulong uVar5;
  long lVar6;
  long in_FS_OFFSET;
  int local_54;
  int local_48;
  undefined4 uStack68;
  long local_40;
  
  uVar5 = 0;
  uVar4 = *(ulong *)(param_1 + 0x1db0);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_54 = -1;
  if (uVar4 != 0) {
    do {
      lVar6 = *(long *)(*(long *)(param_1 + 0x1da8) + uVar5 * 8);
      if ((((*(int *)(lVar6 + 0xc) != 0x40) && (*(int *)(lVar6 + 0xc) != 0x2a3)) &&
          (*(int *)(lVar6 + 0x14) != 6)) && (*(int *)(lVar6 + 0x14) != 0x74)) {
        iVar1 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x9a8),lVar6,0);
        if (iVar1 != 0) {
          if (param_3 == 0) {
            if (((*(uint *)(lVar6 + 0x18) < 9) &&
                (lVar3 = *(long *)(param_1 + 0x898) + (long)(int)*(uint *)(lVar6 + 0x18) * 0x28,
                *(long *)(lVar3 + 0x20) != 0)) &&
               ((*(long *)(lVar3 + 0x28) != 0 &&
                (iVar1 = check_cert_usable_isra_0(param_1,*(undefined4 *)(lVar6 + 0xc)), iVar1 != 0)
                ))) {
              lVar3 = *(long *)(*(long *)(param_1 + 0x898) + (long)*(int *)(lVar6 + 0x18) * 0x28 +
                               0x28);
              iVar1 = *(int *)(lVar6 + 0x14);
              if (iVar1 == 0x198) goto LAB_0014bf04;
LAB_0014bf90:
              if (iVar1 != 0x390) {
                uVar4 = *(ulong *)(param_1 + 0x1db0);
                break;
              }
              if (((lVar3 != 0) &&
                  (iVar1 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x9a8),lVar6,&local_48),
                  iVar1 != 0)) && (CONCAT44(uStack68,local_48) != 0)) {
                iVar1 = EVP_PKEY_get_size(lVar3);
                iVar2 = EVP_MD_get_size(CONCAT44(uStack68,local_48));
                uVar4 = *(ulong *)(param_1 + 0x1db0);
                if (iVar1 < iVar2 * 2 + 2) goto LAB_0014be46;
                break;
              }
            }
          }
          else {
            lVar3 = ssl_cert_lookup_by_pkey(param_3,&local_48);
            if (((lVar3 != 0) && (*(int *)(lVar6 + 0x18) == local_48)) &&
               (iVar1 = check_cert_usable_isra_0
                                  (param_1,*(undefined4 *)(lVar6 + 0xc),param_2,param_3), iVar1 != 0
               )) {
              iVar1 = *(int *)(lVar6 + 0x14);
              lVar3 = param_3;
              if (iVar1 != 0x198) goto LAB_0014bf90;
LAB_0014bf04:
              if (local_54 == -1) {
                local_54 = ssl_get_EC_curve_nid(lVar3);
              }
              uVar4 = *(ulong *)(param_1 + 0x1db0);
              if ((*(int *)(lVar6 + 0x20) != 0) && (*(int *)(lVar6 + 0x20) != local_54))
              goto LAB_0014be46;
              break;
            }
          }
        }
        uVar4 = *(ulong *)(param_1 + 0x1db0);
      }
LAB_0014be46:
      uVar5 = uVar5 + 1;
    } while (uVar5 < uVar4);
    if (uVar5 != uVar4) goto LAB_0014bf32;
  }
  lVar6 = 0;
LAB_0014bf32:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return lVar6;
}