int64_t find_sig_alg(int64_t a1, int32_t a2, int32_t a3) {
    int64_t * v1 = (int64_t *)(a1 + (int64_t)&g68); // 0x4bde6
    int64_t v2 = *v1; // 0x4bde6
    int64_t v3 = __readfsqword(40); // 0x4bdf7
    int64_t result; // 0x4bdd0
    int64_t v4; // 0x4bdd0
    int32_t v5; // 0x4bdd0
    int64_t v6; // 0x4bdd0
    int64_t v7; // 0x4bdd0
    int64_t v8; // 0x4bdd0
    int64_t v9; // 0x4bdd0
    int32_t v10; // 0x4bdd0
    int32_t v11; // 0x4bdd0
    int32_t v12; // 0x4bdd0
    int64_t v13; // 0x4be5a
    int32_t v14; // 0x4bdd0
    if (v2 != 0) {
        int64_t v15 = a3;
        int64_t * v16 = (int64_t *)(a1 + (int64_t)&g67); // 0x4be53
        int64_t * v17 = (int64_t *)(a1 + (int64_t)&g297);
        v11 = -1;
        v9 = v2;
        v6 = 0;
        while (true) {
          lab_0x4be53:
            // 0x4be53
            v7 = v6;
            int64_t v18 = v9;
            v12 = v11;
            v13 = *(int64_t *)(*v16 + 8 * v7);
            int32_t * v19 = (int32_t *)(v13 + 12); // 0x4be5e
            v10 = v12;
            v8 = v18;
            switch (*v19) {
                case 64: {
                    goto lab_0x4be46;
                }
                case 675: {
                    goto lab_0x4be46;
                }
                default: {
                    int32_t * v20 = (int32_t *)(v13 + 20); // 0x4be6f
                    v10 = v12;
                    v8 = v18;
                    switch (*v20) {
                        case 6: {
                            goto lab_0x4be46;
                        }
                        case 116: {
                            goto lab_0x4be46;
                        }
                        default: {
                            // 0x4be7e
                            if ((int32_t)tls1_lookup_md() == 0) {
                                goto lab_0x4be3f;
                            } else {
                                if (a3 != 0) {
                                    // 0x4be20
                                    if (ssl_cert_lookup_by_pkey() == 0) {
                                        goto lab_0x4be3f;
                                    } else {
                                        // 0x4be30
                                        if (*(int32_t *)(v13 + 24) == v14) {
                                            // 0x4bf60
                                            if ((int32_t)check_cert_usable_isra_0(a1, *v19, (int64_t)a2, v15) == 0) {
                                                goto lab_0x4be3f;
                                            } else {
                                                int32_t v21 = *v20; // 0x4bf80
                                                v5 = v21;
                                                v4 = v15;
                                                if (v21 == 408) {
                                                    goto lab_0x4bf04;
                                                } else {
                                                    goto lab_0x4bf90;
                                                }
                                            }
                                        } else {
                                            goto lab_0x4be3f;
                                        }
                                    }
                                } else {
                                    int32_t * v22 = (int32_t *)(v13 + 24); // 0x4be98
                                    uint32_t v23 = *v22; // 0x4be98
                                    if (v23 < 9) {
                                        int64_t v24 = *v17 + 40 * (int64_t)v23; // 0x4bead
                                        int64_t v25 = *(int64_t *)(v24 + 32); // 0x4beb1
                                        if (v25 == 0) {
                                            goto lab_0x4be3f;
                                        } else {
                                            int64_t v26 = *(int64_t *)(v24 + 40); // 0x4beba
                                            if (v26 == 0) {
                                                goto lab_0x4be3f;
                                            } else {
                                                // 0x4bec7
                                                if ((int32_t)check_cert_usable_isra_0(a1, *v19, v25, v26) == 0) {
                                                    goto lab_0x4be3f;
                                                } else {
                                                    int32_t v27 = *v20; // 0x4bef4
                                                    v5 = v27;
                                                    v4 = *(int64_t *)(*v17 + 40 + 40 * (int64_t)*v22);
                                                    if (v27 != 408) {
                                                        goto lab_0x4bf90;
                                                    } else {
                                                        goto lab_0x4bf04;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        goto lab_0x4be3f;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        // 0x4c018
        result = 0;
        goto lab_0x4bf32;
    }
  lab_0x4be46:;
    int64_t v28 = v7 + 1; // 0x4be46
    v11 = v10;
    v9 = v8;
    v6 = v28;
    int64_t v29 = v8; // 0x4be4d
    int64_t v30 = v28; // 0x4be4d
    if (v28 >= v8) {
        // break -> 0x4bf29
        goto lab_0x4bf29_4;
    }
    goto lab_0x4be53;
  lab_0x4be3f:
    // 0x4be3f
    v10 = v12;
    v8 = *v1;
    goto lab_0x4be46;
  lab_0x4bf04:;
    int32_t v31 = v12; // 0x4bf09
    if (v12 == -1) {
        // 0x4c000
        v31 = ssl_get_EC_curve_nid();
    }
    int32_t v32 = *(int32_t *)(v13 + 32); // 0x4bf0f
    int64_t v33 = *v1; // 0x4bf14
    v10 = v31;
    v8 = v33;
    v29 = v33;
    v30 = v7;
    if (v32 != 0 != v31 != v32) {
        // break -> 0x4bf29
        goto lab_0x4bf29_4;
    }
    goto lab_0x4be46;
  lab_0x4bf90:
    // 0x4bf90
    if (v5 != (int32_t)&g125) {
        // 0x4c020
        v29 = *v1;
        v30 = v7;
        goto lab_0x4bf29_4;
    }
    // 0x4bf9b
    if (v4 == 0) {
        goto lab_0x4be3f;
    } else {
        // 0x4bfa4
        if (v14 == 0 | (int32_t)tls1_lookup_md() == 0) {
            goto lab_0x4be3f;
        } else {
            int64_t v34 = function_21970(); // 0x4bfcd
            int64_t v35 = function_204a0(); // 0x4bfda
            int64_t v36 = *v1; // 0x4bfdf
            v10 = v12;
            v8 = v36;
            v29 = v36;
            v30 = v7;
            if ((v34 & 0xffffffff) >= (2 * v35 + 2 & 0xfffffffe)) {
                // break -> 0x4bf29
                goto lab_0x4bf29_4;
            }
            goto lab_0x4be46;
        }
    }
  lab_0x4bf29_4:
    // 0x4bf29
    result = v13;
    if (v30 == v29) {
        // 0x4c018
        result = 0;
        goto lab_0x4bf32;
    } else {
        goto lab_0x4bf32;
    }
  lab_0x4bf32:
    // 0x4bf32
    if (v3 != __readfsqword(40)) {
        // 0x4c02c
        return function_20ff0();
    }
    // 0x4bf46
    return result;
}