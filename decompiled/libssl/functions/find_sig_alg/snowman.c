void** find_sig_alg(void** rdi, void** rsi, void** rdx, ...) {
    void** r14_4;
    void** rbp5;
    void** rbx6;
    void** rdx7;
    void** v8;
    void** r13_9;
    uint64_t rax10;
    uint64_t v11;
    void** v12;
    void** r12_13;
    void** rdi14;
    void** eax15;
    struct s17* rax16;
    void** v17;
    void** rsi18;
    void** r15_19;
    int32_t eax20;
    void** eax21;
    void** eax22;
    void** rdi23;
    void** eax24;
    int64_t v25;
    int32_t eax26;
    void** v27;
    int64_t rax28;
    int64_t rax29;
    struct s18* rax30;
    void** rdx31;
    void** rcx32;
    int32_t eax33;
    int64_t rax34;
    uint64_t rax35;

    r14_4 = rdi;
    rbp5 = rdx;
    *reinterpret_cast<int32_t*>(&rbx6) = 0;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    rdx7 = *reinterpret_cast<void***>(rdi + 0x1db0);
    v8 = rsi;
    r13_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 + 16);
    rax10 = g28;
    v11 = rax10;
    v12 = reinterpret_cast<void**>(0xffffffff);
    if (!rdx7) {
        goto addr_4c018_3;
    }
    while (1) {
        r12_13 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r14_4 + 0x1da8) + reinterpret_cast<unsigned char>(rbx6) * 8);
        if (*reinterpret_cast<void***>(r12_13 + 12) == 64 || (*reinterpret_cast<void***>(r12_13 + 12) == 0x2a3 || (*reinterpret_cast<void***>(r12_13 + 20) == 6 || *reinterpret_cast<void***>(r12_13 + 20) == 0x74))) {
            addr_4be46_5:
            ++rbx6;
            if (reinterpret_cast<unsigned char>(rbx6) >= reinterpret_cast<unsigned char>(rdx7)) 
                break; else 
                continue;
        } else {
            rdi14 = *reinterpret_cast<void***>(r14_4 + 0x9a8);
            eax15 = tls1_lookup_md(rdi14, r12_13, rdi14, r12_13);
            if (!eax15) 
                goto addr_4be3f_7;
            if (!rbp5) 
                goto addr_4be98_9;
        }
        rax16 = ssl_cert_lookup_by_pkey(rbp5, rbp5);
        if (!rax16) 
            goto addr_4be3f_7;
        if (*reinterpret_cast<void***>(r12_13 + 24) != v17) 
            goto addr_4be3f_7;
        rsi18 = *reinterpret_cast<void***>(r12_13 + 12);
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        r15_19 = rbp5;
        eax20 = check_cert_usable_isra_0(r14_4, rsi18, v8, rbp5);
        if (!eax20) 
            goto addr_4be3f_7;
        eax21 = *reinterpret_cast<void***>(r12_13 + 20);
        if (eax21 != 0x198) 
            goto addr_4bf90_14;
        addr_4bf04_15:
        if (v12 == 0xffffffff) {
            eax22 = ssl_get_EC_curve_nid(r15_19, rsi18, r15_19, rsi18);
            v12 = eax22;
        }
        rdx7 = *reinterpret_cast<void***>(r14_4 + 0x1db0);
        if (!*reinterpret_cast<void***>(r12_13 + 32)) 
            break;
        if (*reinterpret_cast<void***>(r12_13 + 32) != v12) 
            goto addr_4be46_5; else 
            break;
        addr_4bf90_14:
        if (!reinterpret_cast<int1_t>(eax21 == 0x390)) 
            goto addr_4c020_19;
        if (!r15_19 || ((rdi23 = *reinterpret_cast<void***>(r14_4 + 0x9a8), eax24 = tls1_lookup_md(rdi23, r12_13, rdi23, r12_13), eax24 == 0) || !v25)) {
            addr_4be3f_7:
            rdx7 = *reinterpret_cast<void***>(r14_4 + 0x1db0);
            goto addr_4be46_5;
        } else {
            eax26 = fun_21970(r15_19, r12_13, r13_9, r15_19, r12_13, r13_9);
            rax28 = fun_204a0(v27, r12_13, v27, r12_13);
            rdx7 = *reinterpret_cast<void***>(r14_4 + 0x1db0);
            if (eax26 >= static_cast<int32_t>(rax28 + rax28 + 2)) 
                break;
            goto addr_4be46_5;
        }
        addr_4be98_9:
        rax29 = reinterpret_cast<int32_t>(*reinterpret_cast<void***>(r12_13 + 24));
        if (*reinterpret_cast<uint32_t*>(&rax29) > 8) 
            goto addr_4be3f_7;
        rax30 = reinterpret_cast<struct s18*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 0x898)) + reinterpret_cast<uint64_t>((rax29 + rax29 * 4) * 8));
        rdx31 = rax30->f20;
        if (!rdx31) 
            goto addr_4be3f_7;
        rcx32 = rax30->f28;
        if (!rcx32) 
            goto addr_4be3f_7;
        rsi18 = *reinterpret_cast<void***>(r12_13 + 12);
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        eax33 = check_cert_usable_isra_0(r14_4, rsi18, rdx31, rcx32);
        if (!eax33) 
            goto addr_4be3f_7;
        rax34 = reinterpret_cast<int32_t>(*reinterpret_cast<void***>(r12_13 + 24));
        r15_19 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 0x898)) + reinterpret_cast<uint64_t>((rax34 + rax34 * 4) * 8) + 40);
        eax21 = *reinterpret_cast<void***>(r12_13 + 20);
        if (!reinterpret_cast<int1_t>(eax21 == 0x198)) 
            goto addr_4bf90_14; else 
            goto addr_4bf04_15;
    }
    addr_4bf29_27:
    if (rbx6 == rdx7) {
        addr_4c018_3:
        *reinterpret_cast<int32_t*>(&r12_13) = 0;
        *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
        goto addr_4bf32_28;
    } else {
        addr_4bf32_28:
        rax35 = v11 ^ g28;
        if (rax35) {
            fun_20ff0();
        } else {
            return r12_13;
        }
    }
    addr_4c020_19:
    rdx7 = *reinterpret_cast<void***>(r14_4 + 0x1db0);
    goto addr_4bf29_27;
}