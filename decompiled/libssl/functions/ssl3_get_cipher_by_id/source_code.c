const SSL_CIPHER *ssl3_get_cipher_by_id(uint32_t id)
{
    SSL_CIPHER c;
    const SSL_CIPHER *cp;

    c.id = id;
    cp = OBJ_bsearch_ssl_cipher_id(&c, tls13_ciphers, TLS13_NUM_CIPHERS);
    if (cp != NULL)
        return cp;
    cp = OBJ_bsearch_ssl_cipher_id(&c, ssl3_ciphers, SSL3_NUM_CIPHERS);
    if (cp != NULL)
        return cp;
    return OBJ_bsearch_ssl_cipher_id(&c, ssl3_scsvs, SSL3_NUM_SCSVS);
}