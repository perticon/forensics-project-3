void ssl3_get_cipher_by_id(undefined4 param_1)

{
  long lVar1;
  long in_FS_OFFSET;
  undefined auStack104 [24];
  undefined4 local_50;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = param_1;
  lVar1 = OBJ_bsearch_ssl_cipher_id(auStack104,tls13_ciphers,5);
  if (lVar1 == 0) {
    lVar1 = OBJ_bsearch_ssl_cipher_id(auStack104,ssl3_ciphers,0xa7);
    if (lVar1 == 0) {
      OBJ_bsearch_ssl_cipher_id(auStack104,ssl3_scsvs,2);
    }
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}