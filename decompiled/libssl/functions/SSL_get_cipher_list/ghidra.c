char * SSL_get_cipher_list(SSL *s,int n)

{
  int iVar1;
  stack_st_SSL_CIPHER *psVar2;
  long lVar3;
  
  if (s == (SSL *)0x0) {
    return (char *)0x0;
  }
  psVar2 = SSL_get_ciphers(s);
  if (psVar2 != (stack_st_SSL_CIPHER *)0x0) {
    iVar1 = OPENSSL_sk_num(psVar2);
    if (n < iVar1) {
      lVar3 = OPENSSL_sk_value(psVar2,n);
      if (lVar3 != 0) {
        return *(char **)(lVar3 + 8);
      }
    }
  }
  return (char *)0x0;
}