void *ossl_pqueue_peek(const OSSL_PQUEUE *pq)
{
    if (pq->htop > 0) {
        ASSERT_USED(pq, 0);
        return pq->heap->data;
    }
    return NULL;
}