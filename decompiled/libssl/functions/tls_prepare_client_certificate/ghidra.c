undefined8 tls_prepare_client_certificate(SSL *param_1,int param_2)

{
  code *pcVar1;
  int iVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  X509 *local_30;
  EVP_PKEY *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_30 = (X509 *)0x0;
  local_28 = (EVP_PKEY *)0x0;
  if (param_2 == 3) {
    pcVar1 = *(code **)((param_1[3].d1)->rcvd_cookie + 0xb4);
    if (pcVar1 != (code *)0x0) {
      iVar2 = (*pcVar1)(param_1,*(undefined8 *)((param_1[3].d1)->rcvd_cookie + 0xbc));
      if (iVar2 < 0) {
        param_1->rwstate = 4;
        uVar3 = 3;
        goto LAB_00171b65;
      }
      if (iVar2 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xd6b,"tls_prepare_client_certificate");
        uVar3 = 0xea;
        goto LAB_00171b54;
      }
      param_1->rwstate = 1;
    }
    iVar2 = tls_choose_sigalg(param_1,0);
    if (((iVar2 == 0) || (param_1[1].enc_read_ctx == (EVP_CIPHER_CTX *)0x0)) ||
       (((*(uint *)((param_1[3].d1)->cookie + 0x18) & 0x30001) != 0 &&
        (iVar2 = tls1_check_chain(param_1,0,0,0), iVar2 == 0)))) goto LAB_00171aa1;
LAB_00171b14:
    if (*(int *)&param_1[4].enc_write_ctx == 4) {
      uVar3 = 1;
      goto LAB_00171b65;
    }
  }
  else {
    if (param_2 != 4) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0xda7,"tls_prepare_client_certificate");
      uVar3 = 0xc0103;
LAB_00171b54:
      ossl_statem_fatal(param_1,0x50,uVar3,0);
      uVar3 = 0;
      goto LAB_00171b65;
    }
LAB_00171aa1:
    iVar2 = ssl_do_client_cert_cb(param_1,&local_30,&local_28);
    if (iVar2 < 0) {
      param_1->rwstate = 4;
      uVar3 = 4;
      goto LAB_00171b65;
    }
    param_1->rwstate = 1;
    if (iVar2 == 1) {
      if ((local_28 == (EVP_PKEY *)0x0) || (local_30 == (X509 *)0x0)) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xd8c,"tls_prepare_client_certificate");
        ERR_set_error(0x14,0x6a,0);
      }
      else {
        iVar2 = SSL_use_certificate(param_1,local_30);
        if ((iVar2 != 0) && (iVar2 = SSL_use_PrivateKey(param_1,local_28), iVar2 != 0)) {
          X509_free(local_30);
          EVP_PKEY_free(local_28);
          goto LAB_00171c68;
        }
      }
      X509_free(local_30);
      EVP_PKEY_free(local_28);
      if (param_1->version == 0x300) goto LAB_00171cc0;
LAB_00171af6:
      param_1[1].quiet_shutdown = 2;
      iVar2 = ssl3_digest_cached_records(param_1,0);
      uVar3 = 0;
      if (iVar2 == 0) goto LAB_00171b65;
      goto LAB_00171b14;
    }
    X509_free(local_30);
    EVP_PKEY_free(local_28);
    if (iVar2 != 0) {
LAB_00171c68:
      iVar2 = tls_choose_sigalg(param_1,0);
      if (((iVar2 != 0) && (param_1[1].enc_read_ctx != (EVP_CIPHER_CTX *)0x0)) &&
         (((*(uint *)((param_1[3].d1)->cookie + 0x18) & 0x30001) == 0 ||
          (iVar2 = tls1_check_chain(param_1,0,0,0), iVar2 != 0)))) goto LAB_00171b14;
    }
    if (param_1->version != 0x300) goto LAB_00171af6;
LAB_00171cc0:
    param_1[1].quiet_shutdown = 0;
    ssl3_send_alert(param_1,1,0x29);
  }
  uVar3 = 2;
LAB_00171b65:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar3;
}