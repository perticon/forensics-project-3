undefined4 tls_parse_stoc_renegotiate(long param_1,byte **param_2)

{
  byte bVar1;
  size_t __n;
  int iVar2;
  byte *pbVar3;
  byte *pbVar4;
  undefined8 uVar5;
  byte *pbVar6;
  
  pbVar4 = (byte *)(*(long *)(param_1 + 0x430) + *(long *)(param_1 + 0x478));
  if ((pbVar4 != (byte *)0x0) &&
     ((*(long *)(param_1 + 0x430) == 0 || (*(long *)(param_1 + 0x478) == 0)))) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x4ce,"tls_parse_stoc_renegotiate");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  }
  if (param_2[1] == (byte *)0x0) {
    ERR_new();
    uVar5 = 0x4d4;
  }
  else {
    pbVar3 = param_2[1] + -1;
    bVar1 = **param_2;
    pbVar6 = *param_2 + 1;
    param_2[1] = pbVar3;
    *param_2 = pbVar6;
    if (pbVar3 == (byte *)(ulong)bVar1) {
      if (pbVar4 == pbVar3) {
        pbVar3 = *(byte **)(param_1 + 0x430);
        if (pbVar3 <= pbVar4) {
          pbVar4 = pbVar4 + -(long)pbVar3;
          pbVar3 = pbVar6 + (long)pbVar3;
          param_2[1] = pbVar4;
          __n = *(size_t *)(param_1 + 0x430);
          *param_2 = pbVar3;
          iVar2 = memcmp(pbVar6,(void *)(param_1 + 0x3f0),__n);
          if (iVar2 == 0) {
            pbVar6 = *(byte **)(param_1 + 0x478);
            if (pbVar6 <= pbVar4) {
              *param_2 = pbVar3 + (long)pbVar6;
              param_2[1] = pbVar4 + -(long)pbVar6;
              iVar2 = memcmp(pbVar3,(void *)(param_1 + 0x438),*(size_t *)(param_1 + 0x478));
              if (iVar2 == 0) {
                *(undefined4 *)(param_1 + 0x480) = 1;
                return 1;
              }
            }
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_clnt.c",0x4ee,"tls_parse_stoc_renegotiate");
            ossl_statem_fatal(param_1,0x2f,0x151,0);
            return 0;
          }
        }
        ERR_new();
        uVar5 = 0x4e7;
      }
      else {
        ERR_new();
        uVar5 = 0x4e0;
      }
      ERR_set_debug("ssl/statem/extensions_clnt.c",uVar5,"tls_parse_stoc_renegotiate");
      ossl_statem_fatal(param_1,0x2f,0x151,0);
      return 0;
    }
    ERR_new();
    uVar5 = 0x4da;
  }
  ERR_set_debug("ssl/statem/extensions_clnt.c",uVar5,"tls_parse_stoc_renegotiate");
  ossl_statem_fatal(param_1,0x32,0x150,0);
  return 0;
}