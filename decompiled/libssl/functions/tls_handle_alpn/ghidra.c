int tls_handle_alpn(long param_1)

{
  code *pcVar1;
  void *__s2;
  int iVar2;
  long lVar3;
  ulong __n;
  undefined8 uVar4;
  long in_FS_OFFSET;
  byte local_29;
  void *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_29 = 0;
  local_28 = (void *)0x0;
  pcVar1 = *(code **)(*(long *)(param_1 + 0x9a8) + 0x298);
  if ((pcVar1 == (code *)0x0) || (*(long *)(param_1 + 0x498) == 0)) {
LAB_0017cb7d:
    if (*(long *)(*(long *)(param_1 + 0x918) + 0x358) != 0) {
      *(undefined4 *)(param_1 + 0xb34) = 0;
      iVar2 = 1;
      goto LAB_0017cba2;
    }
  }
  else {
    iVar2 = (*pcVar1)(param_1,&local_28,&local_29,*(long *)(param_1 + 0x498),
                      *(undefined4 *)(param_1 + 0x4a0),
                      *(undefined8 *)(*(long *)(param_1 + 0x9a8) + 0x2a0));
    if (iVar2 != 0) {
      if (iVar2 != 3) {
        ERR_new();
        iVar2 = 0;
        ERR_set_debug("ssl/statem/statem_srvr.c",0x875,"tls_handle_alpn");
        ossl_statem_fatal(param_1,0x78,0xeb,0);
        goto LAB_0017cba2;
      }
      goto LAB_0017cb7d;
    }
    CRYPTO_free(*(void **)(param_1 + 0x488));
    lVar3 = CRYPTO_memdup(local_28,local_29,"ssl/statem/statem_srvr.c",0x849);
    *(long *)(param_1 + 0x488) = lVar3;
    if (lVar3 == 0) {
      *(undefined8 *)(param_1 + 0x490) = 0;
      ERR_new();
      uVar4 = 0x84c;
LAB_0017cd14:
      ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,"tls_handle_alpn");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      goto LAB_0017cba2;
    }
    lVar3 = *(long *)(param_1 + 0x918);
    __n = (ulong)local_29;
    *(undefined4 *)(param_1 + 0x484) = 0;
    __s2 = *(void **)(lVar3 + 0x358);
    *(ulong *)(param_1 + 0x490) = __n;
    if (__s2 == (void *)0x0) {
      iVar2 = *(int *)(param_1 + 0x4d0);
      *(undefined4 *)(param_1 + 0xb34) = 0;
      if (iVar2 == 0) {
        uVar4 = CRYPTO_memdup(local_28,__n,"ssl/statem/statem_srvr.c",0x868);
        *(undefined8 *)(lVar3 + 0x358) = uVar4;
        if (*(long *)(*(long *)(param_1 + 0x918) + 0x358) == 0) {
          ERR_new();
          uVar4 = 0x86b;
          goto LAB_0017cd14;
        }
        *(ulong *)(*(long *)(param_1 + 0x918) + 0x360) = (ulong)local_29;
      }
    }
    else if ((__n != *(ulong *)(lVar3 + 0x360)) || (iVar2 = memcmp(local_28,__s2,__n), iVar2 != 0))
    {
      iVar2 = *(int *)(param_1 + 0x4d0);
      *(undefined4 *)(param_1 + 0xb34) = 0;
      if (iVar2 == 0) {
        ERR_new();
        uVar4 = 0x864;
        goto LAB_0017cd14;
      }
    }
  }
  iVar2 = 1;
LAB_0017cba2:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar2;
}