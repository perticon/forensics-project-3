bool cmd_ECDHParameters(uint *param_1,byte *param_2)

{
  uint uVar1;
  int iVar2;
  char *pcVar3;
  long lVar4;
  byte *pbVar5;
  byte *pbVar6;
  bool bVar7;
  bool bVar8;
  byte bVar9;
  
  bVar9 = 0;
  uVar1 = *param_1;
  if ((uVar1 & 2) != 0) {
    iVar2 = OPENSSL_strcasecmp(param_2,"+automatic");
    if (iVar2 == 0) {
      return true;
    }
    iVar2 = OPENSSL_strcasecmp(param_2,"automatic");
    if (iVar2 == 0) {
      return true;
    }
    uVar1 = *param_1;
  }
  bVar7 = false;
  bVar8 = (uVar1 & 1) == 0;
  if (!bVar8) {
    lVar4 = 5;
    pbVar5 = param_2;
    pbVar6 = &DAT_00183cfe;
    do {
      if (lVar4 == 0) break;
      lVar4 = lVar4 + -1;
      bVar7 = *pbVar5 < *pbVar6;
      bVar8 = *pbVar5 == *pbVar6;
      pbVar5 = pbVar5 + (ulong)bVar9 * -2 + 1;
      pbVar6 = pbVar6 + (ulong)bVar9 * -2 + 1;
    } while (bVar8);
    if ((!bVar7 && !bVar8) == bVar7) {
      return true;
    }
  }
  pcVar3 = strchr((char *)param_2,0x3a);
  bVar7 = false;
  if (pcVar3 == (char *)0x0) {
    if (*(SSL_CTX **)(param_1 + 6) != (SSL_CTX *)0x0) {
      lVar4 = SSL_CTX_ctrl(*(SSL_CTX **)(param_1 + 6),0x5c,0,param_2);
      return 0 < (int)lVar4;
    }
    bVar7 = true;
    if (*(SSL **)(param_1 + 8) != (SSL *)0x0) {
      lVar4 = SSL_ctrl(*(SSL **)(param_1 + 8),0x5c,0,param_2);
      bVar7 = 0 < (int)lVar4;
    }
  }
  return bVar7;
}