undefined8 tls_parse_ctos_psk_kex_modes(long param_1,byte **param_2)

{
  byte *pbVar1;
  byte *pbVar2;
  byte *pbVar3;
  
  pbVar1 = param_2[1];
  if (pbVar1 != (byte *)0x0) {
    pbVar2 = *param_2;
    pbVar3 = pbVar1 + -1;
    if (pbVar3 == (byte *)(ulong)*pbVar2) {
      param_2[1] = (byte *)0x0;
      *param_2 = pbVar1 + (long)pbVar2;
      if (pbVar3 != (byte *)0x0) {
        pbVar3 = pbVar3 + (long)pbVar2;
        do {
          if (pbVar2[1] == 1) {
            *(uint *)(param_1 + 0xb28) = *(uint *)(param_1 + 0xb28) | 2;
          }
          else if ((pbVar2[1] == 0) && ((*(byte *)(param_1 + 0x9e9) & 4) != 0)) {
            *(uint *)(param_1 + 0xb28) = *(uint *)(param_1 + 0xb28) | 1;
          }
          pbVar2 = pbVar2 + 1;
        } while (pbVar3 != pbVar2);
        return 1;
      }
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x225,"tls_parse_ctos_psk_kex_modes");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  return 0;
}