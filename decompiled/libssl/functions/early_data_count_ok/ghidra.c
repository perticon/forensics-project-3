undefined8 early_data_count_ok(long param_1,long param_2,int param_3,int param_4)

{
  uint uVar1;
  uint uVar2;
  undefined8 uVar3;
  
  if (*(int *)(param_1 + 0x38) == 0) {
    uVar2 = *(uint *)(*(long *)(param_1 + 0x918) + 0x354);
    if ((uVar2 == 0) &&
       ((*(long *)(param_1 + 0x920) == 0 ||
        (uVar2 = *(uint *)(*(long *)(param_1 + 0x920) + 0x354), uVar2 == 0)))) {
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_record.c",0x76,"early_data_count_ok");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    }
  }
  else {
    uVar2 = *(uint *)(param_1 + 0x1d44);
    if ((*(int *)(param_1 + 0xb30) == 2) &&
       (uVar1 = *(uint *)(*(long *)(param_1 + 0x918) + 0x354), uVar1 < uVar2)) {
      uVar2 = uVar1;
    }
    if (uVar2 == 0) {
      ERR_new();
      uVar3 = 0x85;
      goto LAB_001598c1;
    }
  }
  if ((ulong)*(uint *)(param_1 + 0x1d48) + param_2 <= (ulong)(uVar2 + param_3)) {
    *(uint *)(param_1 + 0x1d48) = (int)param_2 + *(uint *)(param_1 + 0x1d48);
    return 1;
  }
  ERR_new();
  uVar3 = 0x8e;
LAB_001598c1:
  ERR_set_debug("ssl/record/ssl3_record.c",uVar3,"early_data_count_ok");
  ossl_statem_fatal(param_1,(-(uint)(param_4 == 0) & 0xffffffba) + 0x50,0xa4,0);
  return 0;
}