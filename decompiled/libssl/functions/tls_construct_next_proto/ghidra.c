undefined8 tls_construct_next_proto(long param_1,undefined8 param_2)

{
  int iVar1;
  uint uVar2;
  ulong uVar3;
  undefined8 uVar4;
  uint uVar5;
  long in_FS_OFFSET;
  undefined8 *local_28;
  long local_20;
  
  uVar4 = *(undefined8 *)(param_1 + 0xb20);
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_28 = (undefined8 *)0x0;
  iVar1 = WPACKET_sub_memcpy__(param_2,*(undefined8 *)(param_1 + 0xb18),uVar4,1);
  if (iVar1 != 0) {
    uVar3 = 0x20 - (ulong)((int)uVar4 + 2U & 0x1f);
    iVar1 = WPACKET_sub_allocate_bytes__(param_2,uVar3,&local_28);
    if (iVar1 != 0) {
      uVar5 = (uint)uVar3;
      if (uVar5 < 8) {
        if ((uVar3 & 4) == 0) {
          if ((uVar5 != 0) && (*(undefined *)local_28 = 0, (uVar3 & 2) != 0)) {
            *(undefined2 *)((long)local_28 + ((uVar3 & 0xffffffff) - 2)) = 0;
          }
        }
        else {
          *(undefined4 *)local_28 = 0;
          *(undefined4 *)((long)local_28 + ((uVar3 & 0xffffffff) - 4)) = 0;
        }
      }
      else {
        *local_28 = 0;
        *(undefined8 *)((long)local_28 + ((uVar3 & 0xffffffff) - 8)) = 0;
        uVar5 = uVar5 + ((int)local_28 - (int)((ulong)(local_28 + 1) & 0xfffffffffffffff8)) &
                0xfffffff8;
        if (7 < uVar5) {
          uVar2 = 0;
          do {
            uVar3 = (ulong)uVar2;
            uVar2 = uVar2 + 8;
            *(undefined8 *)(((ulong)(local_28 + 1) & 0xfffffffffffffff8) + uVar3) = 0;
          } while (uVar2 < uVar5);
        }
      }
      uVar4 = 1;
      goto LAB_0016c0ba;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_clnt.c",0xe05,"tls_construct_next_proto");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  uVar4 = 0;
LAB_0016c0ba:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}