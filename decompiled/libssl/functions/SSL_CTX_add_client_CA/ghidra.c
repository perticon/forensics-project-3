int SSL_CTX_add_client_CA(SSL_CTX *ctx,X509 *x)

{
  int iVar1;
  X509_NAME *pXVar2;
  cert_st *pcVar3;
  
  if (x == (X509 *)0x0) {
    return 0;
  }
  if (ctx->cert == (cert_st *)0x0) {
    pcVar3 = (cert_st *)OPENSSL_sk_new_null();
    ctx->cert = pcVar3;
    if (pcVar3 == (cert_st *)0x0) {
      return 0;
    }
  }
  pXVar2 = X509_get_subject_name(x);
  pXVar2 = X509_NAME_dup(pXVar2);
  if (pXVar2 == (X509_NAME *)0x0) {
    return 0;
  }
  iVar1 = OPENSSL_sk_push(ctx->cert,pXVar2);
  if (iVar1 == 0) {
    X509_NAME_free(pXVar2);
    return 0;
  }
  return 1;
}