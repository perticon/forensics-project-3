int SSL_CTX_use_serverinfo_file(long param_1,void *param_2)

{
  char cVar1;
  int iVar2;
  uint uVar3;
  BIO_METHOD *type;
  BIO *bp;
  long lVar4;
  size_t sVar5;
  void *pvVar6;
  long lVar7;
  void *addr;
  byte *pbVar8;
  byte *pbVar9;
  undefined8 uVar10;
  long lVar11;
  byte *pbVar12;
  long lVar13;
  long in_FS_OFFSET;
  bool bVar14;
  bool bVar15;
  byte bVar16;
  int local_98;
  long local_80;
  uchar *local_60;
  size_t local_58;
  byte *local_50;
  char *local_48;
  long local_40;
  
  bVar16 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = (uchar *)0x0;
  local_58 = 0;
  local_50 = (byte *)0x0;
  local_48 = (char *)0x0;
  if ((param_1 == 0) || (param_2 == (void *)0x0)) {
    ERR_new();
    bp = (BIO *)0x0;
    addr = (void *)0x0;
    ERR_set_debug("ssl/ssl_rsa.c",0x305,"SSL_CTX_use_serverinfo_file");
    ERR_set_error(0x14,0xc0102,0);
    local_98 = 0;
  }
  else {
    type = BIO_s_file();
    bp = BIO_new(type);
    if (bp == (BIO *)0x0) {
      ERR_new();
      addr = (void *)0x0;
      ERR_set_debug("ssl/ssl_rsa.c",0x30b,"SSL_CTX_use_serverinfo_file");
      ERR_set_error(0x14,0x80007,0);
      local_98 = 0;
    }
    else {
      lVar4 = BIO_ctrl(bp,0x6c,3,param_2);
      if ((int)lVar4 < 1) {
        ERR_new();
        addr = (void *)0x0;
        ERR_set_debug("ssl/ssl_rsa.c",0x30f,"SSL_CTX_use_serverinfo_file");
        ERR_set_error(0x14,0x80002,0);
        local_98 = 0;
      }
      else {
        lVar4 = 0;
        lVar13 = 0;
        addr = (void *)0x0;
        local_80 = 0;
        while (iVar2 = PEM_read_bio(bp,(char **)&local_50,&local_48,&local_60,(long *)&local_58),
              pbVar8 = local_50, iVar2 != 0) {
          sVar5 = strlen((char *)local_50);
          uVar3 = (uint)sVar5;
          bVar14 = uVar3 < 0xe;
          bVar15 = uVar3 == 0xe;
          if (uVar3 < 0xf) {
            ERR_new();
            uVar10 = 0x324;
LAB_00141f89:
            ERR_set_debug("ssl/ssl_rsa.c",uVar10,"SSL_CTX_use_serverinfo_file");
            ERR_set_error(0x14,0x188,0);
            local_98 = 0;
            goto LAB_00141e38;
          }
          lVar7 = 0xf;
          pbVar9 = pbVar8;
          pbVar12 = (byte *)"SERVERINFO FOR ";
          do {
            if (lVar7 == 0) break;
            lVar7 = lVar7 + -1;
            bVar14 = *pbVar9 < *pbVar12;
            bVar15 = *pbVar9 == *pbVar12;
            pbVar9 = pbVar9 + (ulong)bVar16 * -2 + 1;
            pbVar12 = pbVar12 + (ulong)bVar16 * -2 + 1;
          } while (bVar15);
          cVar1 = (!bVar14 && !bVar15) - bVar14;
          if (cVar1 == '\0') {
            if (((long)local_58 < 4) ||
               ((long)(int)((uint)local_60[2] * 0x100 + (uint)local_60[3]) != local_58 - 4)) {
              ERR_new();
              uVar10 = 0x33c;
LAB_00141fc4:
              local_98 = (int)cVar1;
              ERR_set_debug("ssl/ssl_rsa.c",uVar10,"SSL_CTX_use_serverinfo_file");
              ERR_set_error(0x14,0x186,0);
              goto LAB_00141e38;
            }
            pvVar6 = CRYPTO_realloc(addr,(int)local_58 + 4 + (int)lVar13,"ssl/ssl_rsa.c",0x34e);
            if (pvVar6 == (void *)0x0) {
LAB_00142010:
              ERR_new();
              ERR_set_debug("ssl/ssl_rsa.c",0x351,"SSL_CTX_use_serverinfo_file");
              ERR_set_error(0x14,0xc0100,0);
              local_98 = 0;
              goto LAB_00141e38;
            }
            lVar4 = 4;
            lVar7 = lVar13 + 4;
LAB_00141de6:
            *(undefined4 *)((long)pvVar6 + lVar13) = 0xd0010000;
            lVar11 = lVar7;
            addr = pvVar6;
          }
          else {
            bVar14 = uVar3 < 0x10;
            bVar15 = uVar3 == 0x10;
            if (uVar3 < 0x11) {
              ERR_new();
              uVar10 = 0x32b;
              goto LAB_00141f89;
            }
            lVar7 = 0x11;
            pbVar9 = (byte *)"SERVERINFOV2 FOR ";
            do {
              if (lVar7 == 0) break;
              lVar7 = lVar7 + -1;
              bVar14 = *pbVar8 < *pbVar9;
              bVar15 = *pbVar8 == *pbVar9;
              pbVar8 = pbVar8 + (ulong)bVar16 * -2 + 1;
              pbVar9 = pbVar9 + (ulong)bVar16 * -2 + 1;
            } while (bVar15);
            cVar1 = (!bVar14 && !bVar15) - bVar14;
            if (cVar1 != '\0') {
              ERR_new();
              ERR_set_debug("ssl/ssl_rsa.c",0x32f,"SSL_CTX_use_serverinfo_file");
              ERR_set_error(0x14,0x187,0);
              local_98 = 0;
              goto LAB_00141e38;
            }
            if (((long)local_58 < 8) ||
               ((long)(int)((uint)local_60[6] * 0x100 + (uint)local_60[7]) != local_58 - 8)) {
              ERR_new();
              uVar10 = 0x349;
              goto LAB_00141fc4;
            }
            lVar7 = lVar13 + lVar4;
            pvVar6 = CRYPTO_realloc(addr,(int)local_58 + (int)lVar7,"ssl/ssl_rsa.c",0x34e);
            if (pvVar6 == (void *)0x0) goto LAB_00142010;
            lVar11 = lVar13;
            addr = pvVar6;
            if (lVar4 != 0) goto LAB_00141de6;
          }
          memcpy((void *)(lVar11 + (long)addr),local_60,local_58);
          lVar13 = lVar13 + local_58 + lVar4;
          CRYPTO_free(local_50);
          local_50 = (byte *)0x0;
          CRYPTO_free(local_48);
          local_48 = (char *)0x0;
          CRYPTO_free(local_60);
          local_80 = local_80 + 1;
          local_60 = (uchar *)0x0;
        }
        if (local_80 == 0) {
          ERR_new();
          ERR_set_debug("ssl/ssl_rsa.c",0x31c,"SSL_CTX_use_serverinfo_file");
          ERR_set_error(0x14,0x185,0);
          local_98 = 0;
        }
        else {
          local_98 = SSL_CTX_use_serverinfo_ex(param_1,2,addr,lVar13);
        }
      }
    }
  }
LAB_00141e38:
  CRYPTO_free(local_50);
  CRYPTO_free(local_48);
  CRYPTO_free(local_60);
  CRYPTO_free(addr);
  BIO_free(bp);
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_98;
}