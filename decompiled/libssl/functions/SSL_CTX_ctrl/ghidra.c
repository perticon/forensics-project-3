long SSL_CTX_ctrl(SSL_CTX *ctx,int cmd,long larg,void *parg)

{
  int iVar1;
  ssl_session_st *psVar2;
  long lVar3;
  void **ppvVar4;
  undefined4 in_register_00000034;
  uint uVar5;
  
  if (ctx == (SSL_CTX *)0x0) {
    if ((cmd & 0xfffffffbU) == 0x62) {
      iVar1 = tls1_set_sigalgs_list(0,parg,0);
      return (long)iVar1;
    }
    if (cmd != 0x5c) {
      return 0;
    }
    iVar1 = tls1_set_groups_list(0,0,0);
    return (long)iVar1;
  }
  uVar5 = (uint)larg;
  switch(cmd) {
  case 0x10:
    *(void **)(ctx->sid_ctx + 0x18) = parg;
    psVar2 = (ssl_session_st *)0x1;
    break;
  default:
                    /* WARNING: Could not recover jumptable at 0x00138e0d. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    lVar3 = (*(code *)ctx->cipher_list[4].stack.data)(ctx,CONCAT44(in_register_00000034,cmd),larg);
    return lVar3;
  case 0x14:
    lVar3 = (*(code *)PTR_OPENSSL_LH_num_items_001a8c48)(ctx->session_cache_head);
    return lVar3;
  case 0x15:
    psVar2 = (ssl_session_st *)(long)(ctx->stats).sess_accept_renegotiate;
    break;
  case 0x16:
    psVar2 = (ssl_session_st *)(long)(ctx->stats).sess_miss;
    break;
  case 0x17:
    psVar2 = (ssl_session_st *)(long)(ctx->stats).sess_accept_good;
    break;
  case 0x18:
    psVar2 = (ssl_session_st *)(long)(ctx->stats).sess_timeout;
    break;
  case 0x19:
    psVar2 = (ssl_session_st *)(long)(ctx->stats).sess_hit;
    break;
  case 0x1a:
    psVar2 = (ssl_session_st *)(long)(ctx->stats).sess_cache_full;
    break;
  case 0x1b:
    psVar2 = (ssl_session_st *)(long)*(int *)((long)&ctx->app_verify_callback + 4);
    break;
  case 0x1c:
    psVar2 = (ssl_session_st *)(long)*(int *)&ctx->app_verify_arg;
    break;
  case 0x1d:
    psVar2 = (ssl_session_st *)(long)(ctx->stats).sess_cb_hit;
    break;
  case 0x1e:
    psVar2 = (ssl_session_st *)(long)ctx->references;
    break;
  case 0x1f:
    psVar2 = (ssl_session_st *)(long)*(int *)&ctx->app_verify_callback;
    break;
  case 0x21:
    uVar5 = uVar5 | *(uint *)&ctx->msg_callback;
    *(uint *)&ctx->msg_callback = uVar5;
    psVar2 = (ssl_session_st *)(ulong)uVar5;
    break;
  case 0x28:
    psVar2 = (ssl_session_st *)(long)*(int *)(ctx->sid_ctx + 8);
    break;
  case 0x29:
    psVar2 = (ssl_session_st *)(long)*(int *)(ctx->sid_ctx + 8);
    *(uint *)(ctx->sid_ctx + 8) = uVar5;
    break;
  case 0x2a:
    psVar2 = (ssl_session_st *)0x0;
    if (-1 < larg) {
      psVar2 = ctx->session_cache_tail;
      ctx->session_cache_tail = (ssl_session_st *)larg;
    }
    break;
  case 0x2b:
    psVar2 = ctx->session_cache_tail;
    break;
  case 0x2c:
    psVar2 = (ssl_session_st *)(ulong)*(uint *)&ctx->new_session_cb;
    *(uint *)&ctx->new_session_cb = uVar5;
    break;
  case 0x2d:
    psVar2 = (ssl_session_st *)(ulong)*(uint *)&ctx->new_session_cb;
    break;
  case 0x32:
    psVar2 = *(ssl_session_st **)&ctx->verify_mode;
    break;
  case 0x33:
    psVar2 = (ssl_session_st *)0x0;
    if (-1 < larg) {
      psVar2 = *(ssl_session_st **)&ctx->verify_mode;
      *(long *)&ctx->verify_mode = larg;
    }
    break;
  case 0x34:
    psVar2 = (ssl_session_st *)0x0;
    if (larg - 0x200U < 0x3e01) {
      ctx->tlsext_status_cb = (_func_3099 *)larg;
      psVar2 = (ssl_session_st *)0x1;
      if ((ulong)larg < ctx->tlsext_ticket_key_cb) {
        ctx->tlsext_ticket_key_cb = (_func_3098 *)larg;
      }
    }
    break;
  case 0x4e:
    uVar5 = ~uVar5 & *(uint *)&ctx->msg_callback;
    *(uint *)&ctx->msg_callback = uVar5;
    psVar2 = (ssl_session_st *)(ulong)uVar5;
    break;
  case 99:
    uVar5 = uVar5 | *(uint *)(*(long *)ctx->sid_ctx + 0x1c);
    *(uint *)(*(long *)ctx->sid_ctx + 0x1c) = uVar5;
    psVar2 = (ssl_session_st *)(ulong)uVar5;
    break;
  case 100:
    uVar5 = ~uVar5 & *(uint *)(*(long *)ctx->sid_ctx + 0x1c);
    *(uint *)(*(long *)ctx->sid_ctx + 0x1c) = uVar5;
    psVar2 = (ssl_session_st *)(ulong)uVar5;
    break;
  case 0x7b:
    iVar1 = ssl_check_allowed_versions(larg & 0xffffffff,*(undefined4 *)&ctx->msg_callback_arg);
    ppvVar4 = (void **)((long)&ctx->msg_callback + 4);
    if (iVar1 == 0) {
      return 0;
    }
    goto LAB_00138c03;
  case 0x7c:
    iVar1 = ssl_check_allowed_versions
                      (*(undefined4 *)((long)&ctx->msg_callback + 4),larg & 0xffffffff);
    if (iVar1 == 0) {
      return 0;
    }
    ppvVar4 = &ctx->msg_callback_arg;
LAB_00138c03:
    iVar1 = ssl_set_version_bound((ctx->cipher_list->stack).num,larg & 0xffffffff,ppvVar4);
    psVar2 = (ssl_session_st *)(ulong)(iVar1 != 0);
    break;
  case 0x7d:
    if ((ctx->tlsext_status_cb < (ulong)larg) || (larg == 0)) {
      psVar2 = (ssl_session_st *)0x0;
    }
    else {
      ctx->tlsext_ticket_key_cb = (_func_3098 *)larg;
      psVar2 = (ssl_session_st *)0x1;
    }
    break;
  case 0x7e:
    psVar2 = (ssl_session_st *)0x0;
    if (larg - 1U < 0x20) {
      ctx->tlsext_status_arg = (void *)larg;
      psVar2 = (ssl_session_st *)0x1;
    }
    break;
  case 0x82:
    psVar2 = (ssl_session_st *)(long)*(int *)((long)&ctx->msg_callback + 4);
    break;
  case 0x83:
    psVar2 = (ssl_session_st *)(long)*(int *)&ctx->msg_callback_arg;
  }
  return (long)psVar2;
}