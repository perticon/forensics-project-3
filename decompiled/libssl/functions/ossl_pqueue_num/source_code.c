size_t ossl_pqueue_num(const OSSL_PQUEUE *pq)
{
    return pq != NULL ? pq->htop : 0;
}