bool dtls1_is_timer_expired(undefined8 param_1)

{
  bool bVar1;
  long lVar2;
  long in_FS_OFFSET;
  long local_28;
  long local_20;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  lVar2 = dtls1_get_timeout(param_1,&local_28);
  bVar1 = false;
  if ((lVar2 != 0) && (local_28 < 1)) {
    bVar1 = local_20 < 1;
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}