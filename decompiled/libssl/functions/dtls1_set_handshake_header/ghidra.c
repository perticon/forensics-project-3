ulong dtls1_set_handshake_header(long param_1,undefined8 param_2,uint param_3)

{
  long lVar1;
  int iVar2;
  ulong uVar3;
  long in_FS_OFFSET;
  undefined auStack24 [8];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 == 0x101) {
    lVar1 = *(long *)(param_1 + 0x4b8);
    *(undefined *)(lVar1 + 0x138) = 1;
    *(undefined8 *)(lVar1 + 0x140) = 0;
    *(undefined2 *)(lVar1 + 0x10c) = *(undefined2 *)(lVar1 + 0x10e);
    *(undefined2 *)(lVar1 + 0x148) = *(undefined2 *)(lVar1 + 0x10e);
    *(undefined (*) [16])(lVar1 + 0x150) = (undefined  [16])0x0;
    iVar2 = WPACKET_put_bytes__(0,param_2,1,1);
    uVar3 = (ulong)(iVar2 != 0);
  }
  else {
    dtls1_set_message_header(param_1,param_3 & 0xff,0,0,0);
    uVar3 = WPACKET_allocate_bytes(param_2,0xc,auStack24);
    if ((int)uVar3 != 0) {
      iVar2 = WPACKET_start_sub_packet(param_2);
      uVar3 = (ulong)(iVar2 != 0);
    }
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}