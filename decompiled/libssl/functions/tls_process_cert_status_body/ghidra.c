undefined8 tls_process_cert_status_body(long param_1,char **param_2)

{
  char cVar1;
  byte bVar2;
  byte bVar3;
  byte bVar4;
  char *pcVar5;
  void *__dest;
  undefined8 uVar6;
  char *pcVar7;
  
  pcVar5 = param_2[1];
  if (pcVar5 != (char *)0x0) {
    pcVar7 = *param_2;
    cVar1 = *pcVar7;
    *param_2 = pcVar7 + 1;
    param_2[1] = pcVar5 + -1;
    if (cVar1 == '\x01') {
      if (pcVar5 + -1 < (char *)0x3) {
LAB_0016ef35:
        ERR_new();
        uVar6 = 0xa52;
      }
      else {
        bVar2 = pcVar7[1];
        bVar3 = pcVar7[2];
        bVar4 = pcVar7[3];
        *param_2 = pcVar7 + 4;
        param_2[1] = pcVar5 + -4;
        pcVar7 = (char *)((ulong)bVar2 << 0x10 | (ulong)bVar3 << 8 | (ulong)bVar4);
        if (pcVar7 != pcVar5 + -4) goto LAB_0016ef35;
        __dest = CRYPTO_malloc((int)pcVar7,"ssl/statem/statem_clnt.c",0xa55);
        *(void **)(param_1 + 0xa88) = __dest;
        if (__dest == (void *)0x0) {
          *(undefined8 *)(param_1 + 0xa90) = 0;
          ERR_new();
          ERR_set_debug("ssl/statem/statem_clnt.c",0xa58,"tls_process_cert_status_body");
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
          return 0;
        }
        *(char **)(param_1 + 0xa90) = pcVar7;
        if (pcVar7 <= param_2[1]) {
          memcpy(__dest,*param_2,(size_t)pcVar7);
          *param_2 = *param_2 + (long)pcVar7;
          param_2[1] = param_2[1] + -(long)pcVar7;
          return 1;
        }
        ERR_new();
        uVar6 = 0xa5d;
      }
      ERR_set_debug("ssl/statem/statem_clnt.c",uVar6,"tls_process_cert_status_body");
      uVar6 = 0x9f;
      goto LAB_0016eee6;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_clnt.c",0xa4d,"tls_process_cert_status_body");
  uVar6 = 0x149;
LAB_0016eee6:
  ossl_statem_fatal(param_1,0x32,uVar6,0);
  return 0;
}