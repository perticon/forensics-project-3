bool SSL_CTX_set_block_padding(long param_1,ulong param_2)

{
  if (param_2 != 1) {
    if (param_2 < 0x4001) {
      *(ulong *)(param_1 + 0x3f0) = param_2;
    }
    return param_2 < 0x4001;
  }
  *(undefined8 *)(param_1 + 0x3f0) = 0;
  return true;
}