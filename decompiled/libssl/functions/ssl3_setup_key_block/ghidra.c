int ssl3_setup_key_block(long param_1)

{
  long lVar1;
  int iVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  uchar *puVar6;
  EVP_MD *type;
  EVP_MD *type_00;
  EVP_MD_CTX *ctx;
  EVP_MD_CTX *ctx_00;
  size_t cnt;
  undefined8 uVar7;
  long in_FS_OFFSET;
  uchar *local_b0;
  int local_a8;
  undefined8 local_80;
  undefined8 local_78;
  undefined8 local_70;
  undefined local_68 [16];
  uchar local_58 [24];
  long local_40;
  
  iVar2 = 1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(long *)(param_1 + 0x310) != 0) goto LAB_00127c78;
  iVar2 = ssl_cipher_get_evp(*(undefined8 *)(param_1 + 0x9a8),*(undefined8 *)(param_1 + 0x918),
                             &local_80,&local_78,0,0);
  if (iVar2 == 0) {
    ossl_statem_send_fatal(param_1,0x50,&local_70,0);
    goto LAB_00127c78;
  }
  iVar2 = 0;
  ssl_evp_cipher_free(*(undefined8 *)(param_1 + 800));
  *(undefined8 *)(param_1 + 800) = local_80;
  ssl_evp_md_free(*(undefined8 *)(param_1 + 0x328));
  *(undefined8 *)(param_1 + 0x328) = local_78;
  *(undefined8 *)(param_1 + 0x340) = local_70;
  iVar3 = EVP_MD_get_size();
  if (iVar3 < 0) goto LAB_00127c78;
  iVar4 = EVP_CIPHER_get_key_length(local_80);
  iVar5 = EVP_CIPHER_get_iv_length(local_80);
  iVar3 = (iVar3 + iVar4 + iVar5) * 2;
  ssl3_cleanup_key_block(param_1);
  puVar6 = (uchar *)CRYPTO_malloc(iVar3,"ssl/s3_enc.c",0x116);
  if (puVar6 == (uchar *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x117,"ssl3_setup_key_block");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
    goto LAB_00127c78;
  }
  *(uchar **)(param_1 + 0x318) = puVar6;
  *(long *)(param_1 + 0x310) = (long)iVar3;
  type = (EVP_MD *)
         ssl_evp_md_fetch(**(undefined8 **)(param_1 + 0x9a8),4,
                          (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
  type_00 = (EVP_MD *)
            ssl_evp_md_fetch(**(undefined8 **)(param_1 + 0x9a8),0x40,
                             (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
  ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
  ctx_00 = (EVP_MD_CTX *)EVP_MD_CTX_new();
  if ((((type == (EVP_MD *)0x0) || (type_00 == (EVP_MD *)0x0)) || (ctx == (EVP_MD_CTX *)0x0)) ||
     (ctx_00 == (EVP_MD_CTX *)0x0)) {
    ERR_new();
    ERR_set_debug("ssl/s3_enc.c",0x25,"ssl3_generate_key_block");
    uVar7 = 0xc0100;
LAB_00128044:
    iVar2 = 0;
    ossl_statem_fatal(param_1,0x50,uVar7,0);
  }
  else {
    if (0 < iVar3) {
      cnt = 1;
      local_b0 = puVar6;
      do {
        iVar2 = (int)cnt;
        __memset_chk(local_68,iVar2 + 0x40,cnt,0x10);
        iVar4 = EVP_DigestInit_ex(ctx_00,type_00,(ENGINE *)0x0);
        if (((iVar4 == 0) || (iVar4 = EVP_DigestUpdate(ctx_00,local_68,cnt), iVar4 == 0)) ||
           ((iVar4 = EVP_DigestUpdate(ctx_00,(void *)(*(long *)(param_1 + 0x918) + 0x50),
                                      *(size_t *)(*(long *)(param_1 + 0x918) + 8)), iVar4 == 0 ||
            ((iVar4 = EVP_DigestUpdate(ctx_00,(void *)(param_1 + 0x140),0x20), iVar4 == 0 ||
             (iVar4 = EVP_DigestUpdate(ctx_00,(void *)(param_1 + 0x160),0x20), iVar4 == 0)))))) {
LAB_001280d0:
          ERR_new();
          uVar7 = 0x3d;
          goto LAB_001280e1;
        }
        iVar4 = EVP_DigestFinal_ex(ctx_00,local_58,(uint *)0x0);
        if ((iVar4 == 0) ||
           (((iVar4 = EVP_DigestInit_ex(ctx,type,(ENGINE *)0x0), iVar4 == 0 ||
             (iVar4 = EVP_DigestUpdate(ctx,(void *)(*(long *)(param_1 + 0x918) + 0x50),
                                       *(size_t *)(*(long *)(param_1 + 0x918) + 8)), iVar4 == 0)) ||
            (iVar4 = EVP_DigestUpdate(ctx,local_58,0x14), iVar4 == 0)))) goto LAB_001280d0;
        if (SBORROW4(iVar3,iVar2 * 0x10) == iVar3 + iVar2 * -0x10 < 0) {
          iVar2 = EVP_DigestFinal_ex(ctx,local_b0,(uint *)0x0);
          if (iVar2 == 0) {
            ERR_new();
            uVar7 = 0x48;
            goto LAB_00127ff5;
          }
        }
        else {
          iVar2 = EVP_DigestFinal_ex(ctx,local_58,(uint *)0x0);
          if (iVar2 == 0) {
            ERR_new();
            uVar7 = 0x42;
LAB_00127ff5:
            ERR_set_debug("ssl/s3_enc.c",uVar7,"ssl3_generate_key_block");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            goto LAB_00128056;
          }
          local_a8 = (int)puVar6;
          memcpy(local_b0,local_58,(ulong)(uint)((iVar3 + local_a8) - (int)local_b0));
        }
        local_b0 = local_b0 + 0x10;
        if ((ulong)(iVar3 - 1U >> 4) + 1 == cnt) goto LAB_0012813d;
        cnt = cnt + 1;
      } while (cnt != 0x11);
      ERR_new();
      uVar7 = 0x2c;
LAB_001280e1:
      ERR_set_debug("ssl/s3_enc.c",uVar7,"ssl3_generate_key_block");
      uVar7 = 0xc0103;
      goto LAB_00128044;
    }
LAB_0012813d:
    iVar2 = 1;
    OPENSSL_cleanse(local_58,0x14);
  }
LAB_00128056:
  EVP_MD_CTX_free(ctx);
  EVP_MD_CTX_free(ctx_00);
  ssl_evp_md_free(type);
  ssl_evp_md_free(type_00);
  if ((*(byte *)(param_1 + 0x9e9) & 8) == 0) {
    *(undefined4 *)(param_1 + 0x180) = 1;
    lVar1 = *(long *)(*(long *)(param_1 + 0x918) + 0x2f8);
    if ((lVar1 != 0) && ((iVar3 = *(int *)(lVar1 + 0x24), iVar3 == 0x20 || (iVar3 == 4)))) {
      *(undefined4 *)(param_1 + 0x180) = 0;
    }
  }
LAB_00127c78:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar2;
}