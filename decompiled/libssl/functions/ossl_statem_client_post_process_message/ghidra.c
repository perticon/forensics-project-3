undefined8 ossl_statem_client_post_process_message(long param_1)

{
  undefined8 uVar1;
  
  if ((*(uint *)(param_1 + 0x5c) & 0xffffffdf) == 7) {
    uVar1 = tls_prepare_client_certificate();
    return uVar1;
  }
  if (*(uint *)(param_1 + 0x5c) != 4) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x439,"ossl_statem_client_post_process_message");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  }
  uVar1 = tls_post_process_server_certificate();
  return uVar1;
}