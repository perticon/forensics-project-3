undefined8 SSL_has_pending(long param_1)

{
  int iVar1;
  undefined8 uVar2;
  
  iVar1 = RECORD_LAYER_processed_read_pending(param_1 + 0xc58);
  if (iVar1 != 0) {
    return 1;
  }
  uVar2 = RECORD_LAYER_read_pending(param_1 + 0xc58);
  return uVar2;
}