undefined8 ssl_get_min_max_version(int *param_1,int *param_2,int *param_3,int *param_4)

{
  bool bVar1;
  int iVar2;
  int *piVar3;
  int iVar4;
  int iVar5;
  int iVar6;
  undefined1 *puVar7;
  int local_44;
  
  if (**(int **)(param_1 + 2) == 0x10000) {
    *param_2 = 0;
    puVar7 = tls_version_table;
    if (param_4 != (int *)0x0) {
      *param_4 = 0;
    }
  }
  else {
    if (**(int **)(param_1 + 2) != 0x1ffff) {
      iVar5 = *param_1;
      *param_3 = iVar5;
      *param_2 = iVar5;
      if (param_4 == (int *)0x0) {
        return 0;
      }
      return 0xc0103;
    }
    *param_2 = 0;
    puVar7 = dtls_version_table;
    if (param_4 != (int *)0x0) {
      *param_4 = 0;
      puVar7 = dtls_version_table;
    }
  }
  local_44 = 0;
  bVar1 = true;
  iVar5 = 0;
  do {
    while (*(code **)((long)puVar7 + 8) == (code *)0x0) {
      bVar1 = true;
      iVar5 = 0;
LAB_00176ff3:
      piVar3 = (int *)((long)puVar7 + 0x18);
      puVar7 = (undefined1 *)((long)puVar7 + 0x18);
      if (*piVar3 == 0) goto LAB_001770e1;
    }
    piVar3 = (int *)(**(code **)((long)puVar7 + 8))();
    if ((iVar5 == 0) && (bVar1)) {
      iVar5 = *(int *)puVar7;
    }
    iVar2 = param_1[0x27d];
    iVar6 = *piVar3;
    if ((iVar2 != 0) && (iVar6 != iVar2)) {
      if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
        if (iVar2 <= iVar6) goto LAB_0017702f;
      }
      else {
        if (iVar6 == 0x100) {
          iVar4 = 0xff00;
          if (iVar2 == 0x100) goto LAB_0017702f;
        }
        else {
          iVar4 = iVar6;
          if (iVar2 == 0x100) {
            iVar2 = 0xff00;
          }
        }
        if (iVar4 <= iVar2) goto LAB_0017702f;
      }
LAB_00176fee:
      bVar1 = true;
      goto LAB_00176ff3;
    }
LAB_0017702f:
    iVar2 = ssl_security(param_1,9,0,iVar6,0);
    if (iVar2 == 0) goto LAB_00176fee;
    iVar2 = param_1[0x27e];
    if ((iVar2 != 0) && (iVar6 != iVar2)) {
      if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
        if (iVar6 < iVar2) goto LAB_00177098;
      }
      else {
        if (iVar6 == 0x100) {
          iVar6 = 0xff00;
          if (iVar2 == 0x100) goto LAB_00176fee;
        }
        else if (iVar2 == 0x100) {
          iVar2 = 0xff00;
        }
        if (iVar2 < iVar6) goto LAB_00177098;
      }
      goto LAB_00176fee;
    }
LAB_00177098:
    if (((*(ulong *)(piVar3 + 2) & *(ulong *)(param_1 + 0x27a)) != 0) ||
       (((*(byte *)(piVar3 + 1) & 2) != 0 &&
        ((*(uint *)(*(long *)(param_1 + 0x226) + 0x1c) & 0x30000) != 0)))) goto LAB_00176fee;
    if (bVar1) {
      if ((param_4 != (int *)0x0) && (iVar5 != 0)) {
        *param_4 = iVar5;
      }
      local_44 = *piVar3;
      bVar1 = false;
      *param_2 = local_44;
      goto LAB_00176ff3;
    }
    puVar7 = (undefined1 *)((long)puVar7 + 0x18);
    *param_2 = *piVar3;
  } while (*(int *)puVar7 != 0);
LAB_001770e1:
  *param_3 = local_44;
  if (local_44 != 0) {
    return 0;
  }
  return 0xbf;
}