undefined8 tls12_get_psigalgs(long param_1,int param_2,long *param_3)

{
  long lVar1;
  uint uVar2;
  
  lVar1 = *(long *)(param_1 + 0x898);
  uVar2 = *(uint *)(lVar1 + 0x1c) & 0x30000;
  if (uVar2 == 0x20000) {
    *param_3 = 0x1885f4;
    return 1;
  }
  if (uVar2 == 0x30000) {
    *param_3 = (long)&suiteb_sigalgs;
    return 2;
  }
  if (uVar2 == 0x10000) {
    *param_3 = (long)&suiteb_sigalgs;
    return 1;
  }
  if ((*(int *)(param_1 + 0x38) == param_2) && (*(long *)(lVar1 + 0x1a8) != 0)) {
    *param_3 = *(long *)(lVar1 + 0x1a8);
    return *(undefined8 *)(lVar1 + 0x1b0);
  }
  if (*(long *)(lVar1 + 0x198) != 0) {
    *param_3 = *(long *)(lVar1 + 0x198);
    return *(undefined8 *)(lVar1 + 0x1a0);
  }
  *param_3 = (long)tls12_sigalgs;
  return 0x1f;
}