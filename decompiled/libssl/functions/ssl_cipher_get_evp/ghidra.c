ssl_cipher_get_evp(undefined8 *param_1,int *param_2,long *param_3,long *param_4,int *param_5,
                  undefined8 *param_6,undefined8 *param_7,int param_8)

{
  long lVar1;
  undefined4 uVar2;
  int iVar3;
  undefined8 uVar4;
  ulong uVar5;
  long lVar6;
  long lVar7;
  long in_FS_OFFSET;
  int *local_70;
  int local_58 [6];
  long local_40;
  
  lVar6 = *(long *)(param_2 + 0xbe);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (lVar6 == 0) {
LAB_00131550:
    uVar4 = 0;
  }
  else {
    if (param_7 == (undefined8 *)0x0) {
LAB_0013145e:
      if (((param_3 == (long *)0x0) || (param_4 == (long *)0x0)) ||
         (iVar3 = ssl_cipher_get_evp_cipher(param_1,lVar6,param_3), iVar3 == 0)) goto LAB_00131550;
      iVar3 = *(int *)(lVar6 + 0x28);
      local_70 = param_5;
      if (iVar3 == 1) {
        lVar7 = 0;
LAB_00131580:
        iVar3 = ssl_evp_md_up_ref(param_1[lVar7 + 0xa8]);
        if (iVar3 == 0) {
          ssl_evp_cipher_free(*param_3);
          uVar4 = 0;
          goto LAB_00131552;
        }
        lVar1 = param_1[lVar7 + 0xa8];
        *param_4 = lVar1;
        if (param_5 != (int *)0x0) {
          *param_5 = *(int *)((long)param_1 + (long)(int)lVar7 * 4 + 0x448);
        }
        if (param_6 != (undefined8 *)0x0) {
          *param_6 = param_1[lVar7 + 0xb6];
        }
        if (*param_3 == 0) goto LAB_00131550;
        if (lVar1 == 0) goto LAB_001315f0;
      }
      else {
        if (iVar3 == 2) {
          lVar7 = 1;
          goto LAB_00131580;
        }
        if (iVar3 == 4) {
          lVar7 = 2;
          goto LAB_00131580;
        }
        if (iVar3 == 8) {
          lVar7 = 3;
          goto LAB_00131580;
        }
        if (iVar3 == 0x10) {
          lVar7 = 4;
          goto LAB_00131580;
        }
        if (iVar3 == 0x20) {
          lVar7 = 5;
          goto LAB_00131580;
        }
        if (iVar3 == 0x80) {
          lVar7 = 6;
          goto LAB_00131580;
        }
        if (iVar3 == 0x100) {
          lVar7 = 7;
          goto LAB_00131580;
        }
        if (iVar3 == 0x200) {
          lVar7 = 8;
          goto LAB_00131580;
        }
        if (iVar3 == 0) {
          lVar7 = 9;
          goto LAB_00131580;
        }
        if (iVar3 == 0x400) {
          lVar7 = 0xc;
          goto LAB_00131580;
        }
        if (iVar3 == 0x800) {
          lVar7 = 0xd;
          goto LAB_00131580;
        }
        *param_4 = 0;
        if (param_5 != (int *)0x0) {
          *param_5 = 0;
          iVar3 = *(int *)(lVar6 + 0x28);
        }
        if (param_6 != (undefined8 *)0x0) {
          *param_6 = 0;
        }
        local_70 = (int *)0x0;
        if (iVar3 != 0x40) {
          local_70 = param_5;
        }
        if (*param_3 == 0) goto LAB_00131550;
LAB_001315f0:
        uVar5 = EVP_CIPHER_get_flags();
        if ((uVar5 & 0x200000) == 0) goto LAB_00131550;
      }
      if ((local_70 != (int *)0x0) && (*local_70 == 0)) goto LAB_00131550;
      if ((param_8 == 0) && ((*param_2 >> 8 == 3 && (*param_2 != 0x300)))) {
        iVar3 = *(int *)(lVar6 + 0x24);
        if (iVar3 == 4) {
          if (*(int *)(lVar6 + 0x28) != 1) goto LAB_00131698;
          lVar6 = ssl_evp_cipher_fetch(*param_1,0x393,param_1[0x88]);
        }
        else if (iVar3 == 0x40) {
          if (*(int *)(lVar6 + 0x28) == 2) {
            lVar6 = ssl_evp_cipher_fetch(*param_1,0x394,param_1[0x88]);
          }
          else {
            if (*(int *)(lVar6 + 0x28) != 0x10) goto LAB_00131698;
            lVar6 = ssl_evp_cipher_fetch(*param_1,0x3b4,param_1[0x88]);
          }
        }
        else {
          if (iVar3 != 0x80) goto LAB_00131698;
          if (*(int *)(lVar6 + 0x28) == 2) {
            lVar6 = ssl_evp_cipher_fetch(*param_1,0x396,param_1[0x88]);
          }
          else {
            if (*(int *)(lVar6 + 0x28) != 0x10) goto LAB_00131698;
            lVar6 = ssl_evp_cipher_fetch(*param_1,0x3b6,param_1[0x88]);
          }
        }
        if (lVar6 != 0) {
          ssl_evp_cipher_free(*param_3);
          ssl_evp_md_free(*param_4);
          *param_3 = lVar6;
          *param_4 = 0;
        }
      }
    }
    else {
      CRYPTO_THREAD_run_once(&ssl_load_builtin_comp_once,do_load_builtin_compressions_ossl_);
      lVar7 = ssl_comp_methods;
      local_58[0] = param_2[0xbd];
      *param_7 = 0;
      if (lVar7 != 0) {
        uVar2 = OPENSSL_sk_find(lVar7,local_58);
        uVar4 = OPENSSL_sk_value(ssl_comp_methods,uVar2);
        *param_7 = uVar4;
      }
      if (((ulong)param_3 | (ulong)param_4) != 0) goto LAB_0013145e;
    }
LAB_00131698:
    uVar4 = 1;
  }
LAB_00131552:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}