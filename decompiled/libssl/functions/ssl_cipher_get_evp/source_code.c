int ssl_cipher_get_evp(SSL_CTX *ctx, const SSL_SESSION *s,
                       const EVP_CIPHER **enc, const EVP_MD **md,
                       int *mac_pkey_type, size_t *mac_secret_size,
                       SSL_COMP **comp, int use_etm)
{
    int i;
    const SSL_CIPHER *c;

    c = s->cipher;
    if (c == NULL)
        return 0;
    if (comp != NULL) {
        SSL_COMP ctmp;
#ifndef OPENSSL_NO_COMP
        if (!load_builtin_compressions()) {
            /*
             * Currently don't care, since a failure only means that
             * ssl_comp_methods is NULL, which is perfectly OK
             */
        }
#endif
        *comp = NULL;
        ctmp.id = s->compress_meth;
        if (ssl_comp_methods != NULL) {
            i = sk_SSL_COMP_find(ssl_comp_methods, &ctmp);
            *comp = sk_SSL_COMP_value(ssl_comp_methods, i);
        }
        /* If were only interested in comp then return success */
        if ((enc == NULL) && (md == NULL))
            return 1;
    }

    if ((enc == NULL) || (md == NULL))
        return 0;

    if (!ssl_cipher_get_evp_cipher(ctx, c, enc))
        return 0;

    i = ssl_cipher_info_lookup(ssl_cipher_table_mac, c->algorithm_mac);
    if (i == -1) {
        *md = NULL;
        if (mac_pkey_type != NULL)
            *mac_pkey_type = NID_undef;
        if (mac_secret_size != NULL)
            *mac_secret_size = 0;
        if (c->algorithm_mac == SSL_AEAD)
            mac_pkey_type = NULL;
    } else {
        if (!ssl_evp_md_up_ref(ctx->ssl_digest_methods[i])) {
            ssl_evp_cipher_free(*enc);
            return 0;
        }
        *md = ctx->ssl_digest_methods[i];
        if (mac_pkey_type != NULL)
            *mac_pkey_type = ctx->ssl_mac_pkey_id[i];
        if (mac_secret_size != NULL)
            *mac_secret_size = ctx->ssl_mac_secret_size[i];
    }

    if ((*enc != NULL)
        && (*md != NULL 
            || (EVP_CIPHER_get_flags(*enc) & EVP_CIPH_FLAG_AEAD_CIPHER))
        && (!mac_pkey_type || *mac_pkey_type != NID_undef)) {
        const EVP_CIPHER *evp = NULL;

        if (use_etm
                || s->ssl_version >> 8 != TLS1_VERSION_MAJOR
                || s->ssl_version < TLS1_VERSION)
            return 1;

        if (c->algorithm_enc == SSL_RC4
                && c->algorithm_mac == SSL_MD5)
            evp = ssl_evp_cipher_fetch(ctx->libctx, NID_rc4_hmac_md5,
                                       ctx->propq);
        else if (c->algorithm_enc == SSL_AES128
                    && c->algorithm_mac == SSL_SHA1)
            evp = ssl_evp_cipher_fetch(ctx->libctx,
                                       NID_aes_128_cbc_hmac_sha1,
                                       ctx->propq);
        else if (c->algorithm_enc == SSL_AES256
                    && c->algorithm_mac == SSL_SHA1)
             evp = ssl_evp_cipher_fetch(ctx->libctx,
                                        NID_aes_256_cbc_hmac_sha1,
                                        ctx->propq);
        else if (c->algorithm_enc == SSL_AES128
                    && c->algorithm_mac == SSL_SHA256)
            evp = ssl_evp_cipher_fetch(ctx->libctx,
                                       NID_aes_128_cbc_hmac_sha256,
                                       ctx->propq);
        else if (c->algorithm_enc == SSL_AES256
                    && c->algorithm_mac == SSL_SHA256)
            evp = ssl_evp_cipher_fetch(ctx->libctx,
                                       NID_aes_256_cbc_hmac_sha256,
                                       ctx->propq);

        if (evp != NULL) {
            ssl_evp_cipher_free(*enc);
            ssl_evp_md_free(*md);
            *enc = evp;
            *md = NULL;
        }
        return 1;
    }

    return 0;
}