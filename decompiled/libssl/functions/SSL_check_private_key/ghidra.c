int SSL_check_private_key(SSL *ctx)

{
  X509 *x509;
  X509 *pkey;
  int iVar1;
  
  if (ctx == (SSL *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x673,"SSL_check_private_key");
    ERR_set_error(0x14,0xc0102,0);
  }
  else {
    x509 = **(X509 ***)ctx[3].d1;
    if (x509 == (X509 *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x677,"SSL_check_private_key");
      ERR_set_error(0x14,0xb1,0);
    }
    else {
      pkey = (*(X509 ***)ctx[3].d1)[1];
      if (pkey != (X509 *)0x0) {
        iVar1 = X509_check_private_key(x509,(EVP_PKEY *)pkey);
        return iVar1;
      }
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x67b,"SSL_check_private_key");
      ERR_set_error(0x14,0xbe,0);
    }
  }
  return 0;
}