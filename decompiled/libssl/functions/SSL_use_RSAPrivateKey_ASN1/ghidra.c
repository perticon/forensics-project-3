int SSL_use_RSAPrivateKey_ASN1(SSL *ssl,uchar *d,long len)

{
  int iVar1;
  RSA *rsa;
  long in_FS_OFFSET;
  uchar *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_28 = d;
  rsa = d2i_RSAPrivateKey((RSA **)0x0,&local_28,len);
  if (rsa == (RSA *)0x0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa_legacy.c",0x5a,"SSL_use_RSAPrivateKey_ASN1");
    ERR_set_error(0x14,0x8000d,0);
  }
  else {
    iVar1 = SSL_use_RSAPrivateKey(ssl,rsa);
    RSA_free(rsa);
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}