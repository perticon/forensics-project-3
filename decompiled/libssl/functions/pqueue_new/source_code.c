pqueue *pqueue_new(void)
{
    pqueue *pq = OPENSSL_zalloc(sizeof(*pq));

    if (pq == NULL)
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);

    return pq;
}