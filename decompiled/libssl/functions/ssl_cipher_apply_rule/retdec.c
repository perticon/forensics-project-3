int64_t ssl_cipher_apply_rule(int32_t a1, int32_t a2, int32_t a3, int32_t a4, int32_t a5, int32_t a6, uint64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t result) {
    // 0x2f6f0
    int128_t v1; // 0x2f6f0
    int128_t v2 = v1;
    int32_t v3 = a8; // 0x2f707
    int64_t * v4 = (int64_t *)a10; // 0x2f716
    int64_t v5 = *v4; // 0x2f716
    int64_t * v6 = (int64_t *)result; // 0x2f736
    int64_t v7 = *v6; // 0x2f736
    int64_t v8 = !((v3 == 3 | v3 == 6)) ? v7 : v5;
    int64_t v9 = !((v3 == 3 | v3 == 6)) ? v5 : v7;
    if (v8 == 0 || v9 == 0) {
        // 0x2f8fa
        *v4 = v5;
        *v6 = v7;
        return result;
    }
    int128_t v10 = __asm_pxor(v2, v2); // 0x2f769
    int32_t v11 = a9; // 0x2f79a
    uint32_t v12 = (int32_t)a7;
    int64_t v13 = v9; // 0x2f77f
    int64_t v14 = v7; // 0x2f77f
    int64_t v15 = v5; // 0x2f77f
    int64_t v16; // 0x2f6f0
    int64_t v17; // 0x2f6f0
    int64_t v18; // 0x2f6f0
    int64_t v19; // 0x2f6f0
    int64_t v20; // 0x2f6f0
    int64_t v21; // 0x2f780
    int64_t * v22; // 0x2f780
    int64_t v23; // 0x2f780
    int64_t * v24; // 0x2f784
    int64_t v25; // 0x2f784
    int64_t v26; // 0x2f78a
    while (true) {
      lab_0x2f780:
        // 0x2f780
        v18 = v15;
        v16 = v14;
        v20 = v13;
        v21 = v20 + 16;
        v22 = (int64_t *)v21;
        v23 = *v22;
        v24 = (int64_t *)(v20 + 24);
        v25 = *v24;
        v26 = *(int64_t *)v20;
        if (v11 < 0) {
            if (a1 == 0) {
                goto lab_0x2f80a;
            } else {
                // 0x2f804
                v17 = v16;
                v19 = v18;
                if (*(int32_t *)(v26 + 24) != a1) {
                    goto lab_0x2f7e0;
                } else {
                    goto lab_0x2f80a;
                }
            }
        } else {
            // 0x2f79f
            v17 = v16;
            v19 = v18;
            if (*(int32_t *)(v26 + 68) != v11) {
                goto lab_0x2f7e0;
            } else {
                goto lab_0x2f7a5;
            }
        }
    }
  lab_0x2f8fa_3:;
    // 0x2f8fa
    int64_t v27; // 0x2f6f0
    *v4 = v27;
    int64_t v28; // 0x2f6f0
    *v6 = v28;
    return result;
  lab_0x2f80a:
    if (a2 == 0) {
        goto lab_0x2f81a;
    } else {
        // 0x2f814
        v17 = v16;
        v19 = v18;
        if ((*(int32_t *)(v26 + 28) & a2) == 0) {
            goto lab_0x2f7e0;
        } else {
            goto lab_0x2f81a;
        }
    }
  lab_0x2f7e0:;
    int64_t v29 = v17; // 0x2f7e3
    int64_t v30 = v19; // 0x2f7e3
    v28 = v17;
    v27 = v19;
    if (v8 == v20) {
        // break -> 0x2f8fa
        goto lab_0x2f8fa_3;
    }
    goto lab_0x2f7e9;
  lab_0x2f7a5:
    switch (v3) {
        case 1: {
            goto lab_0x2f8a8;
        }
        case 4: {
            goto lab_0x2f918;
        }
        case 3: {
            goto lab_0x2f9c0;
        }
        default: {
            goto lab_0x2f7c3;
        }
    }
  lab_0x2f81a:
    if (a3 == 0) {
        goto lab_0x2f82a;
    } else {
        // 0x2f824
        v17 = v16;
        v19 = v18;
        if ((*(int32_t *)(v26 + 32) & a3) == 0) {
            goto lab_0x2f7e0;
        } else {
            goto lab_0x2f82a;
        }
    }
  lab_0x2f7e9:
    // 0x2f7e9
    v13 = v3 == 3 | v3 == 6 ? v25 : v23;
    v15 = v30;
    v14 = v29;
    v28 = v14;
    v27 = v15;
    if (v13 == 0) {
        // break -> 0x2f8fa
        goto lab_0x2f8fa_3;
    }
    goto lab_0x2f780;
  lab_0x2f8a8:;
    int32_t * v31 = (int32_t *)(v20 + 8); // 0x2f8a8
    v17 = v16;
    v19 = v18;
    if (*v31 != 0) {
        goto lab_0x2f7e0;
    } else {
        int64_t v32 = v18; // 0x2f8b8
        if (v16 != v20) {
            int64_t v33 = v23; // 0x2f8c4
            if (v25 != 0) {
                // 0x2f8c6
                *(int64_t *)(v25 + 16) = v23;
                v33 = *v22;
            }
            int64_t v34 = v33;
            if (v34 != 0) {
                // 0x2f8d3
                *(int64_t *)(v34 + 24) = v25;
            }
            // 0x2f8d7
            v32 = v20 == v18 ? v23 : v18;
            *(int64_t *)(v16 + 16) = v20;
            *v24 = v16;
            *v22 = 0;
        }
        // 0x2f8e7
        *v31 = 1;
        v29 = v20;
        v30 = v32;
        v28 = v20;
        v27 = v32;
        if (v8 == v20) {
            // break -> 0x2f8fa
            goto lab_0x2f8fa_3;
        }
        goto lab_0x2f7e9;
    }
  lab_0x2f918:
    // 0x2f918
    v17 = v16;
    v19 = v18;
    if (!((v16 == v20 | *(int32_t *)(v20 + 8) == 0))) {
        int64_t v35 = v23; // 0x2f938
        if (v25 != 0) {
            // 0x2f93a
            *(int64_t *)(v25 + 16) = v23;
            v35 = *v22;
        }
        int64_t v36 = v35;
        if (v36 != 0) {
            // 0x2f947
            *(int64_t *)(v36 + 24) = v25;
        }
        // 0x2f94b
        *(int64_t *)(v16 + 16) = v20;
        *v24 = v16;
        *v22 = 0;
        v17 = v20;
        v19 = v20 == v18 ? v23 : v18;
    }
    goto lab_0x2f7e0;
  lab_0x2f9c0:;
    int32_t * v37 = (int32_t *)(v20 + 8); // 0x2f9c0
    v17 = v16;
    v19 = v18;
    if (*v37 != 0) {
        int64_t v38 = v16; // 0x2f9ce
        if (v20 != v18) {
            int64_t v39 = v25; // 0x2f9da
            if (v23 != 0) {
                // 0x2f9dc
                *(int64_t *)(v23 + 24) = v25;
                v39 = *v24;
            }
            int64_t v40 = v39;
            if (v40 != 0) {
                // 0x2f9e9
                *(int64_t *)(v40 + 16) = v23;
            }
            // 0x2f9ed
            v38 = v16 == v20 ? v25 : v16;
            *(int64_t *)(v18 + 24) = v20;
            *v24 = 0;
            *v22 = v18;
        }
        // 0x2fa00
        *v37 = 0;
        v17 = v38;
        v19 = v20;
    }
    goto lab_0x2f7e0;
  lab_0x2f7c3:
    if (v3 == 3 || v3 == 6) {
        // 0x2fa10
        v17 = v16;
        v19 = v18;
        if (!((v20 == v18 | *(int32_t *)(v20 + 8) == 0))) {
            int64_t v41 = v25; // 0x2fa30
            if (v23 != 0) {
                // 0x2fa32
                *(int64_t *)(v23 + 24) = v25;
                v41 = *v24;
            }
            int64_t v42 = v41;
            if (v42 != 0) {
                // 0x2fa3f
                *(int64_t *)(v42 + 16) = v23;
            }
            // 0x2fa44
            *(int64_t *)(v18 + 24) = v20;
            *v22 = v18;
            *v24 = 0;
            v17 = v16 == v20 ? v25 : v16;
            v19 = v20;
        }
    } else {
        // 0x2f7ce
        v17 = v16;
        v19 = v18;
        if (v3 == 2) {
            int64_t v43 = v23; // 0x2f97b
            int64_t v44 = v23; // 0x2f97b
            if (v20 != v18) {
                // 0x2f981
                *(int64_t *)(v25 + 16) = v23;
                v43 = *v22;
                v44 = v18;
            }
            int64_t v45 = v43;
            *(int32_t *)(v20 + 8) = 0;
            int64_t v46 = v25; // 0x2f99a
            if (v45 != 0) {
                // 0x2f99c
                *(int64_t *)(v45 + 24) = v25;
                v46 = *v24;
            }
            int64_t v47 = v46;
            if (v47 != 0) {
                // 0x2f9a9
                *(int64_t *)(v47 + 16) = v45;
            }
            // 0x2f9ae
            __asm_movups(*(int128_t *)v21, v10);
            v17 = v16 == v20 ? v25 : v16;
            v19 = v44;
        }
    }
    goto lab_0x2f7e0;
  lab_0x2f82a:
    if (a4 == 0) {
        goto lab_0x2f83a;
    } else {
        // 0x2f834
        v17 = v16;
        v19 = v18;
        if ((*(int32_t *)(v26 + 36) & a4) == 0) {
            goto lab_0x2f7e0;
        } else {
            goto lab_0x2f83a;
        }
    }
  lab_0x2f83a:
    if (a5 == 0) {
        goto lab_0x2f84a;
    } else {
        // 0x2f844
        v17 = v16;
        v19 = v18;
        if ((*(int32_t *)(v26 + 40) & a5) == 0) {
            goto lab_0x2f7e0;
        } else {
            goto lab_0x2f84a;
        }
    }
  lab_0x2f84a:
    if (a6 == 0) {
        goto lab_0x2f85a;
    } else {
        // 0x2f854
        v17 = v16;
        v19 = v18;
        if (*(int32_t *)(v26 + 44) != a6) {
            goto lab_0x2f7e0;
        } else {
            goto lab_0x2f85a;
        }
    }
  lab_0x2f85a:
    if (a7 % 32 == 0) {
        goto lab_0x2f877;
    } else {
        // 0x2f864
        v17 = v16;
        v19 = v18;
        if ((v12 % 32 & *(int32_t *)(v26 + 60)) == 0) {
            goto lab_0x2f7e0;
        } else {
            goto lab_0x2f877;
        }
    }
  lab_0x2f877:
    if ((a7 & 32) == 0) {
        goto lab_0x2f7a5;
    } else {
        // 0x2f885
        v17 = v16;
        v19 = v18;
        if ((v12 & 32 & *(int32_t *)(v26 + 60)) == 0) {
            goto lab_0x2f7e0;
        } else {
            if (v3 != 1) {
                switch (v3) {
                    case 4: {
                        goto lab_0x2f918;
                    }
                    case 3: {
                        goto lab_0x2f9c0;
                    }
                    default: {
                        goto lab_0x2f7c3;
                    }
                }
            } else {
                goto lab_0x2f8a8;
            }
        }
    }
}