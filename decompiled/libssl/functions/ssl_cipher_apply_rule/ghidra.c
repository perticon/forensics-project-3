void ssl_cipher_apply_rule
               (int param_1,uint param_2,uint param_3,uint param_4,uint param_5,int param_6,
               uint param_7,int param_8,int param_9,long **param_10,long **param_11)

{
  long lVar1;
  long *plVar2;
  long *plVar3;
  long *plVar4;
  long *plVar5;
  long *plVar6;
  long *plVar7;
  long *plVar8;
  long *plVar9;
  bool bVar10;
  bool bVar11;
  
  bVar10 = param_8 == 3;
  bVar11 = param_8 == 6;
  plVar9 = *param_10;
  plVar7 = *param_11;
  plVar2 = plVar9;
  plVar6 = plVar7;
  if (bVar10 || bVar11) {
    plVar2 = plVar7;
    plVar6 = plVar9;
  }
  if ((plVar6 != (long *)0x0) && (plVar2 != (long *)0x0)) {
    do {
      plVar4 = (long *)plVar2[2];
      plVar3 = (long *)plVar2[3];
      lVar1 = *plVar2;
      plVar5 = plVar4;
      if (bVar10 || bVar11) {
        plVar5 = plVar3;
      }
      if (param_9 < 0) {
        if (((((((param_1 == 0) || (*(int *)(lVar1 + 0x18) == param_1)) &&
               ((param_2 == 0 || ((*(uint *)(lVar1 + 0x1c) & param_2) != 0)))) &&
              ((param_3 == 0 || ((*(uint *)(lVar1 + 0x20) & param_3) != 0)))) &&
             ((param_4 == 0 || ((*(uint *)(lVar1 + 0x24) & param_4) != 0)))) &&
            ((((param_5 == 0 || ((*(uint *)(lVar1 + 0x28) & param_5) != 0)) &&
              ((param_6 == 0 || (*(int *)(lVar1 + 0x2c) == param_6)))) &&
             (((param_7 & 0x1f) == 0 || ((param_7 & *(uint *)(lVar1 + 0x3c) & 0x1f) != 0)))))) &&
           (((param_7 & 0x20) == 0 || ((param_7 & *(uint *)(lVar1 + 0x3c) & 0x20) != 0))))
        goto joined_r0x0012f89f;
      }
      else if (*(int *)(lVar1 + 0x44) == param_9) {
joined_r0x0012f89f:
        if (param_8 == 1) {
          if (*(int *)(plVar2 + 1) == 0) {
            if (plVar7 != plVar2) {
              if (plVar2 == plVar9) {
                plVar9 = plVar4;
              }
              if (plVar3 != (long *)0x0) {
                plVar3[2] = (long)plVar4;
                plVar4 = (long *)plVar2[2];
              }
              if (plVar4 != (long *)0x0) {
                plVar4[3] = (long)plVar3;
              }
              plVar7[2] = (long)plVar2;
              plVar2[3] = (long)plVar7;
              plVar2[2] = 0;
            }
            *(undefined4 *)(plVar2 + 1) = 1;
            plVar7 = plVar2;
          }
        }
        else if (param_8 == 4) {
          if ((*(int *)(plVar2 + 1) != 0) && (plVar7 != plVar2)) {
            if (plVar2 == plVar9) {
              plVar9 = plVar4;
            }
            if (plVar3 != (long *)0x0) {
              plVar3[2] = (long)plVar4;
              plVar4 = (long *)plVar2[2];
            }
            if (plVar4 != (long *)0x0) {
              plVar4[3] = (long)plVar3;
            }
            plVar7[2] = (long)plVar2;
            plVar2[3] = (long)plVar7;
            plVar2[2] = 0;
            plVar7 = plVar2;
          }
        }
        else if (param_8 == 3) {
          if (*(int *)(plVar2 + 1) != 0) {
            if (plVar2 != plVar9) {
              if (plVar7 == plVar2) {
                plVar7 = plVar3;
              }
              if (plVar4 != (long *)0x0) {
                plVar4[3] = (long)plVar3;
                plVar3 = (long *)plVar2[3];
              }
              if (plVar3 != (long *)0x0) {
                plVar3[2] = (long)plVar4;
              }
              plVar9[3] = (long)plVar2;
              plVar2[3] = 0;
              plVar2[2] = (long)plVar9;
              plVar9 = plVar2;
            }
            *(undefined4 *)(plVar2 + 1) = 0;
          }
        }
        else if (bVar10 || bVar11) {
          if ((*(int *)(plVar2 + 1) != 0) && (plVar2 != plVar9)) {
            if (plVar7 == plVar2) {
              plVar7 = plVar3;
            }
            if (plVar4 != (long *)0x0) {
              plVar4[3] = (long)plVar3;
              plVar3 = (long *)plVar2[3];
            }
            if (plVar3 != (long *)0x0) {
              plVar3[2] = (long)plVar4;
            }
            plVar9[3] = (long)plVar2;
            plVar2[2] = (long)plVar9;
            plVar2[3] = 0;
            plVar9 = plVar2;
          }
        }
        else if (param_8 == 2) {
          plVar8 = plVar4;
          if (plVar2 != plVar9) {
            plVar3[2] = (long)plVar4;
            plVar4 = (long *)plVar2[2];
            plVar8 = plVar9;
          }
          *(undefined4 *)(plVar2 + 1) = 0;
          if (plVar7 == plVar2) {
            plVar7 = plVar3;
          }
          if (plVar4 != (long *)0x0) {
            plVar4[3] = (long)plVar3;
            plVar3 = (long *)plVar2[3];
          }
          if (plVar3 != (long *)0x0) {
            plVar3[2] = (long)plVar4;
          }
          *(undefined (*) [16])(plVar2 + 2) = (undefined  [16])0x0;
          plVar9 = plVar8;
        }
      }
    } while ((plVar6 != plVar2) && (plVar2 = plVar5, plVar5 != (long *)0x0));
  }
  *param_10 = plVar9;
  *param_11 = plVar7;
  return;
}