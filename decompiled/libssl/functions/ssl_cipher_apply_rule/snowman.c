void ssl_cipher_apply_rule(void** edi, void** esi, void** edx, void** ecx, void** r8d, void** r9d, uint32_t a7, int32_t a8, int32_t a9, struct s0** a10, struct s0** a11, int64_t a12) {
    int32_t r11d13;
    void** v14;
    struct s0* r14_15;
    void** v16;
    unsigned char dl17;
    void** v18;
    int32_t r9d19;
    void** v20;
    struct s0* r13_21;
    unsigned char v22;
    struct s0* r10_23;
    struct s0* rax24;
    int32_t ebp25;
    void** v26;
    uint32_t v27;
    uint32_t v28;
    struct s0* rdx29;
    struct s0* rcx30;
    struct s0* rsi31;
    struct s0* rbx32;
    struct s0* r12_33;

    r11d13 = a8;
    v14 = edx;
    r14_15 = *a10;
    v16 = r9d;
    dl17 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r11d13 == 3)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(r11d13 == 6)));
    v18 = ecx;
    r9d19 = a9;
    v20 = r8d;
    r13_21 = *a11;
    v22 = dl17;
    if (!dl17) {
        r10_23 = r13_21;
        rax24 = r14_15;
        ebp25 = 0;
    } else {
        r10_23 = r14_15;
        rax24 = r13_21;
        ebp25 = 1;
    }
    if (r10_23 && rax24) {
        v26 = esi;
        __asm__("pxor xmm0, xmm0");
        v27 = a7 & 31;
        v28 = a7 & 32;
        while (1) {
            rdx29 = rax24->f10;
            rcx30 = rax24->f18;
            rsi31 = rdx29;
            rbx32 = rdx29;
            r12_33 = rcx30;
            if (ebp25) {
                rsi31 = rcx30;
            }
            if (r9d19 < 0) {
                if (!edi) 
                    goto addr_2f80a_10;
                if (rax24->f0->f18 != edi) 
                    goto addr_2f7e0_12;
                addr_2f80a_10:
                if (!v26) 
                    goto addr_2f81a_13;
                if (!(rax24->f0->f1c & reinterpret_cast<unsigned char>(v26))) 
                    goto addr_2f7e0_12;
                addr_2f81a_13:
                if (!v14) 
                    goto addr_2f82a_15;
                if (!(rax24->f0->f20 & reinterpret_cast<unsigned char>(v14))) 
                    goto addr_2f7e0_12;
                addr_2f82a_15:
                if (!v18) 
                    goto addr_2f83a_17;
                if (!(rax24->f0->f24 & reinterpret_cast<unsigned char>(v18))) 
                    goto addr_2f7e0_12;
                addr_2f83a_17:
                if (!v20) 
                    goto addr_2f84a_19;
                if (!(rax24->f0->f28 & reinterpret_cast<unsigned char>(v20))) 
                    goto addr_2f7e0_12;
                addr_2f84a_19:
                if (!v16) 
                    goto addr_2f85a_21;
                if (rax24->f0->f2c != v16) 
                    goto addr_2f7e0_12;
                addr_2f85a_21:
                if (!v27) 
                    goto addr_2f877_23;
                if (!(a7 & rax24->f0->f3c & 31)) 
                    goto addr_2f7e0_12;
                addr_2f877_23:
                if (v28) 
                    goto addr_2f885_25;
            } else {
                if (rax24->f0->f44 != r9d19) 
                    goto addr_2f7e0_12;
            }
            if (r11d13 == 1) {
                addr_2f8a8_28:
                if (rax24->f8) {
                    addr_2f7e0_12:
                    if (r10_23 == rax24) 
                        break;
                } else {
                    if (r13_21 != rax24) {
                        if (rax24 == r14_15) {
                            r14_15 = rdx29;
                        }
                        if (rcx30) {
                            rcx30->f10 = rdx29;
                            rdx29 = rax24->f10;
                        }
                        if (rdx29) {
                            rdx29->f18 = rcx30;
                        }
                        r13_21->f10 = rax24;
                        rax24->f18 = r13_21;
                        rax24->f10 = reinterpret_cast<struct s0*>(0);
                    }
                    rax24->f8 = 1;
                    r13_21 = rax24;
                    if (r10_23 == rax24) 
                        break;
                }
            } else {
                addr_2f7af_38:
                if (r11d13 == 4) {
                    if (rax24->f8 && r13_21 != rax24) {
                        if (rax24 == r14_15) {
                            r14_15 = rdx29;
                        }
                        if (rcx30) {
                            rcx30->f10 = rdx29;
                            rbx32 = rax24->f10;
                        }
                        if (rbx32) {
                            rbx32->f18 = rcx30;
                        }
                        r13_21->f10 = rax24;
                        rax24->f18 = r13_21;
                        r13_21 = rax24;
                        rax24->f10 = reinterpret_cast<struct s0*>(0);
                        goto addr_2f7e0_12;
                    }
                } else {
                    if (r11d13 == 3) {
                        if (rax24->f8) {
                            if (rax24 != r14_15) {
                                if (r13_21 == rax24) {
                                    r13_21 = rcx30;
                                }
                                if (rdx29) {
                                    rdx29->f18 = rcx30;
                                    rcx30 = rax24->f18;
                                }
                                if (rcx30) {
                                    rcx30->f10 = rdx29;
                                }
                                r14_15->f18 = rax24;
                                rax24->f18 = reinterpret_cast<struct s0*>(0);
                                rax24->f10 = r14_15;
                                r14_15 = rax24;
                            }
                            rax24->f8 = 0;
                            goto addr_2f7e0_12;
                        }
                    } else {
                        if (v22) {
                            if (rax24->f8 && rax24 != r14_15) {
                                if (r13_21 == rax24) {
                                    r13_21 = rcx30;
                                }
                                if (rdx29) {
                                    rdx29->f18 = rcx30;
                                    r12_33 = rax24->f18;
                                }
                                if (r12_33) {
                                    r12_33->f10 = rdx29;
                                }
                                r14_15->f18 = rax24;
                                rax24->f10 = r14_15;
                                r14_15 = rax24;
                                rax24->f18 = reinterpret_cast<struct s0*>(0);
                                goto addr_2f7e0_12;
                            }
                        } else {
                            if (r11d13 == 2) {
                                if (rax24 == r14_15) {
                                    r14_15 = rdx29;
                                } else {
                                    rcx30->f10 = rdx29;
                                    rbx32 = rax24->f10;
                                }
                                rax24->f8 = 0;
                                if (r13_21 == rax24) {
                                    r13_21 = rcx30;
                                }
                                if (rbx32) {
                                    rbx32->f18 = rcx30;
                                    r12_33 = rax24->f18;
                                }
                                if (r12_33) {
                                    r12_33->f10 = rbx32;
                                }
                                __asm__("movups [rax+0x10], xmm0");
                                goto addr_2f7e0_12;
                            } else {
                                goto addr_2f7e0_12;
                            }
                        }
                    }
                }
            }
            if (!rsi31) 
                break;
            rax24 = rsi31;
            continue;
            addr_2f885_25:
            if (!(a7 & rax24->f0->f3c & 32)) 
                goto addr_2f7e0_12;
            if (r11d13 != 1) 
                goto addr_2f7af_38;
            goto addr_2f8a8_28;
        }
    }
    *a10 = r14_15;
    *a11 = r13_21;
    return;
}