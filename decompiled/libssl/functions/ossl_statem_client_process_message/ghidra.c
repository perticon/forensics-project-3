undefined8 ossl_statem_client_process_message(long param_1,ushort **param_2)

{
  ushort uVar1;
  int iVar2;
  ushort *puVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  void *local_30;
  ushort *local_28;
  ushort *local_20;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  switch(*(undefined4 *)(param_1 + 0x5c)) {
  default:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x3ff,"ossl_statem_client_process_message");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    uVar4 = 0;
    break;
  case 2:
    uVar4 = dtls_process_hello_verify();
    break;
  case 3:
    uVar4 = tls_process_server_hello();
    break;
  case 4:
    uVar4 = tls_process_server_certificate();
    break;
  case 5:
    uVar4 = tls_process_cert_status();
    break;
  case 6:
    uVar4 = tls_process_key_exchange();
    break;
  case 7:
    uVar4 = tls_process_certificate_request();
    break;
  case 8:
    uVar4 = tls_process_server_done();
    break;
  case 9:
    uVar4 = tls_process_new_session_ticket();
    break;
  case 10:
    uVar4 = tls_process_change_cipher_spec();
    break;
  case 0xb:
    uVar4 = tls_process_finished();
    break;
  case 0x26:
    local_30 = (void *)0x0;
    if (param_2[1] < (ushort *)0x2) {
LAB_001718ed:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0xe32,"tls_process_encrypted_extensions");
      ossl_statem_fatal(param_1,0x32,0x9f,0);
    }
    else {
      puVar3 = param_2[1] + -1;
      uVar1 = **param_2;
      if (puVar3 != (ushort *)(ulong)(ushort)(uVar1 << 8 | uVar1 >> 8)) goto LAB_001718ed;
      local_28 = *param_2 + 1;
      param_2[1] = (ushort *)0x0;
      *param_2 = (ushort *)((long)local_28 + (long)puVar3);
      local_20 = puVar3;
      iVar2 = tls_collect_extensions(param_1,&local_28,0x400,&local_30,0,1);
      if ((iVar2 != 0) &&
         (iVar2 = tls_parse_all_extensions(param_1,0x400,local_30,0,0,1), iVar2 != 0)) {
        CRYPTO_free(local_30);
        uVar4 = 3;
        break;
      }
    }
    CRYPTO_free(local_30);
    uVar4 = 0;
    break;
  case 0x27:
    uVar4 = tls_process_cert_verify();
    break;
  case 0x29:
    uVar4 = tls_process_hello_req();
    break;
  case 0x2d:
    uVar4 = tls_process_key_update();
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}