ulong ssl3_get_req_cert_type(int *param_1,undefined8 param_2)

{
  uint uVar1;
  long lVar2;
  int iVar3;
  ulong uVar4;
  long in_FS_OFFSET;
  uint local_24;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_24 = 0;
  lVar2 = *(long *)(*(long *)(param_1 + 0x226) + 0x188);
  if (lVar2 != 0) {
    uVar4 = WPACKET_memcpy(param_2,lVar2,*(undefined8 *)(*(long *)(param_1 + 0x226) + 400));
    goto LAB_0012a859;
  }
  ssl_set_sig_mask(&local_24,param_1,0x5000e);
  uVar1 = *(uint *)(*(long *)(param_1 + 0xb8) + 0x1c);
  iVar3 = *param_1;
  if (iVar3 < 0x301) {
LAB_0012a8a9:
    if ((iVar3 == 0x300) && ((uVar1 & 2) != 0)) {
      iVar3 = WPACKET_put_bytes__(param_2,5,1);
      if (iVar3 == 0) goto LAB_0012a940;
      if ((local_24 & 2) == 0) {
        iVar3 = WPACKET_put_bytes__(param_2,6,1);
        if (iVar3 == 0) {
          uVar4 = 0;
          goto LAB_0012a859;
        }
        goto LAB_0012a8ba;
      }
      if ((local_24 & 1) == 0) goto LAB_0012aa30;
    }
    else {
LAB_0012a8ba:
      if ((local_24 & 1) == 0) {
LAB_0012aa30:
        iVar3 = WPACKET_put_bytes__(param_2,1,1);
        if (iVar3 == 0) goto LAB_0012a940;
      }
      if (((local_24 & 2) == 0) && (iVar3 = WPACKET_put_bytes__(param_2,2,1), iVar3 == 0))
      goto LAB_0012a940;
    }
    uVar4 = 1;
    if ((0x300 < *param_1) && ((local_24 & 8) == 0)) {
      iVar3 = WPACKET_put_bytes__(param_2,0x40,1);
      uVar4 = (ulong)(iVar3 != 0);
    }
  }
  else {
    if ((uVar1 & 0x10) == 0) {
      if (0x302 < iVar3) {
LAB_0012a91d:
        if ((uVar1 & 0x200) != 0) {
          iVar3 = WPACKET_put_bytes__(param_2,0x43,1);
          if ((iVar3 != 0) && (iVar3 = WPACKET_put_bytes__(param_2,0x44,1), iVar3 != 0)) {
            iVar3 = *param_1;
            goto LAB_0012a8a9;
          }
          goto LAB_0012a940;
        }
      }
      goto LAB_0012a8ba;
    }
    iVar3 = WPACKET_put_bytes__(param_2,0x16,1);
    if ((((iVar3 != 0) && (iVar3 = WPACKET_put_bytes__(param_2,0x43,1), iVar3 != 0)) &&
        (iVar3 = WPACKET_put_bytes__(param_2,0x44,1), iVar3 != 0)) &&
       ((iVar3 = WPACKET_put_bytes__(param_2,0xee,1), iVar3 != 0 &&
        (iVar3 = WPACKET_put_bytes__(param_2,0xef,1), iVar3 != 0)))) {
      iVar3 = *param_1;
      if (0x302 < iVar3) goto LAB_0012a91d;
      goto LAB_0012a8a9;
    }
LAB_0012a940:
    uVar4 = 0;
  }
LAB_0012a859:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}