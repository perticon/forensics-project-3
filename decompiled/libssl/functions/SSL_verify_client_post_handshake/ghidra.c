int SSL_verify_client_post_handshake(long param_1)

{
  uint uVar1;
  int iVar2;
  
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
      (iVar2 = **(int **)(param_1 + 8), 0x303 < iVar2)) && (iVar2 != 0x10000)) {
    if (*(int *)(param_1 + 0x38) == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x16f4,"SSL_verify_client_post_handshake");
      ERR_set_error(0x14,0x11c,0);
      return 0;
    }
    iVar2 = SSL_is_init_finished();
    if (iVar2 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x16f9,"SSL_verify_client_post_handshake");
      ERR_set_error(0x14,0x79,0);
    }
    else {
      uVar1 = *(uint *)(param_1 + 0xba8);
      if (uVar1 == 3) {
        ERR_new();
        iVar2 = 0;
        ERR_set_debug("ssl/ssl_lib.c",0x1708,"SSL_verify_client_post_handshake");
        ERR_set_error(0x14,0x11d,0);
      }
      else {
        if (uVar1 < 4) {
          if (uVar1 == 0) {
            ERR_new();
            ERR_set_debug("ssl/ssl_lib.c",0x16ff,"SSL_verify_client_post_handshake");
            ERR_set_error(0x14,0x117,0);
            return 0;
          }
          if (uVar1 == 2) {
            *(undefined4 *)(param_1 + 0xba8) = 3;
            iVar2 = send_certificate_request(param_1);
            if (iVar2 != 0) {
              ossl_statem_set_in_init(param_1,1);
              return 1;
            }
            *(undefined4 *)(param_1 + 0xba8) = 2;
            ERR_new();
            ERR_set_debug("ssl/ssl_lib.c",0x1714,"SSL_verify_client_post_handshake");
            ERR_set_error(0x14,0x11b,0);
            return 0;
          }
        }
        else if (uVar1 == 4) {
          ERR_new();
          ERR_set_debug("ssl/ssl_lib.c",0x170b,"SSL_verify_client_post_handshake");
          ERR_set_error(0x14,0x11e,0);
          return 0;
        }
        ERR_new();
        iVar2 = 0;
        ERR_set_debug("ssl/ssl_lib.c",0x1703,"SSL_verify_client_post_handshake");
        ERR_set_error(0x14,0xc0103,0);
      }
    }
  }
  else {
    ERR_new();
    iVar2 = 0;
    ERR_set_debug("ssl/ssl_lib.c",0x16f0,"SSL_verify_client_post_handshake");
    ERR_set_error(0x14,0x10a,0);
  }
  return iVar2;
}