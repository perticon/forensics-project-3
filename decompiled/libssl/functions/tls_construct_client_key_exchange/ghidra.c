undefined8 tls_construct_client_key_exchange(int *param_1,undefined8 param_2)

{
  uint uVar1;
  long lVar2;
  int iVar3;
  int iVar4;
  undefined8 uVar5;
  EVP_PKEY *pkey;
  size_t __n;
  uchar *puVar6;
  undefined8 uVar7;
  EVP_PKEY_CTX *pEVar8;
  long lVar9;
  char *pcVar10;
  long in_FS_OFFSET;
  uchar *local_78;
  uchar *local_70;
  undefined local_68 [40];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = *(uint *)(*(long *)(param_1 + 0xb8) + 0x1c);
  if (((uVar1 & 0x1c8) != 0) && (iVar4 = tls_construct_cke_psk_preamble(), iVar4 == 0))
  goto LAB_0016f200;
  if ((uVar1 & 0x41) == 0) {
    if ((uVar1 & 0x102) == 0) {
      if ((uVar1 & 0x84) == 0) {
        if ((uVar1 & 0x10) == 0) {
          if ((uVar1 & 0x200) == 0) {
            if ((uVar1 & 0x20) == 0) {
              if ((uVar1 & 8) != 0) goto LAB_0016f1e3;
              ERR_new();
              pcVar10 = "tls_construct_client_key_exchange";
              uVar5 = 0xcf0;
            }
            else {
              local_70 = (uchar *)0x0;
              if (*(BIGNUM **)(param_1 + 0x306) != (BIGNUM *)0x0) {
                iVar3 = BN_num_bits(*(BIGNUM **)(param_1 + 0x306));
                iVar4 = iVar3 + 0xe;
                if (-1 < iVar3 + 7) {
                  iVar4 = iVar3 + 7;
                }
                iVar4 = WPACKET_sub_allocate_bytes__(param_2,(long)(iVar4 >> 3),&local_70,2);
                if (iVar4 != 0) {
                  BN_bn2bin(*(BIGNUM **)(param_1 + 0x306),local_70);
                  CRYPTO_free(*(void **)(*(long *)(param_1 + 0x246) + 0x370));
                  lVar9 = *(long *)(param_1 + 0x246);
                  pcVar10 = CRYPTO_strdup(*(char **)(param_1 + 0x2fc),"ssl/statem/statem_clnt.c",
                                          0xcc2);
                  lVar2 = *(long *)(param_1 + 0x246);
                  *(char **)(lVar9 + 0x370) = pcVar10;
                  uVar5 = 1;
                  if (*(long *)(lVar2 + 0x370) != 0) goto LAB_0016f26c;
                  ERR_new();
                  pcVar10 = "tls_construct_cke_srp";
                  uVar5 = 0xcc4;
                  goto LAB_0016fa20;
                }
              }
              ERR_new();
              pcVar10 = "tls_construct_cke_srp";
              uVar5 = 0xcbc;
            }
          }
          else {
            local_78 = (uchar *)0x0;
            iVar4 = ossl_gost18_cke_cipher_nid(param_1);
            if (iVar4 != 0) {
              iVar3 = ossl_gost_ukm(param_1,local_68);
              if (iVar3 < 1) {
                ERR_new();
                uVar5 = 0;
                puVar6 = (uchar *)0x0;
                pEVar8 = (EVP_PKEY_CTX *)0x0;
                ERR_set_debug("ssl/statem/statem_clnt.c",0xc66,"tls_construct_cke_gost18");
                ossl_statem_fatal(param_1,0x50,0xc0103,0);
              }
              else {
                puVar6 = (uchar *)CRYPTO_malloc(0x20,"ssl/statem/statem_clnt.c",0xc6c);
                if (puVar6 == (uchar *)0x0) {
                  ERR_new();
                  pEVar8 = (EVP_PKEY_CTX *)0x0;
                  uVar5 = 0x20;
                  ERR_set_debug("ssl/statem/statem_clnt.c",0xc6e,"tls_construct_cke_gost18");
                  ossl_statem_fatal(param_1,0x50,0xc0100,0);
                }
                else {
                  iVar3 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x26a),puVar6,0x20,0);
                  if (iVar3 < 1) {
                    ERR_new();
                    pEVar8 = (EVP_PKEY_CTX *)0x0;
                    uVar5 = 0x20;
                    ERR_set_debug("ssl/statem/statem_clnt.c",0xc73,"tls_construct_cke_gost18");
                    ossl_statem_fatal(param_1,0x50,0xc0103,0);
                  }
                  else {
                    pEVar8 = *(EVP_PKEY_CTX **)(*(long *)(param_1 + 0x246) + 0x2b8);
                    if (pEVar8 == (EVP_PKEY_CTX *)0x0) {
                      ERR_new();
                      uVar5 = 0x20;
                      ERR_set_debug("ssl/statem/statem_clnt.c",0xc7a,"tls_construct_cke_gost18");
                      ossl_statem_fatal(param_1,0x28,0x14a,0);
                    }
                    else {
                      uVar5 = *(undefined8 *)(*(long *)(param_1 + 0x26a) + 0x440);
                      uVar7 = X509_get0_pubkey(pEVar8);
                      pEVar8 = (EVP_PKEY_CTX *)
                               EVP_PKEY_CTX_new_from_pkey
                                         (**(undefined8 **)(param_1 + 0x26a),uVar7,uVar5);
                      if (pEVar8 == (EVP_PKEY_CTX *)0x0) {
                        ERR_new();
                        ERR_set_debug("ssl/statem/statem_clnt.c",0xc83,"tls_construct_cke_gost18");
                        uVar7 = 0xc0100;
                      }
                      else {
                        iVar3 = EVP_PKEY_encrypt_init(pEVar8);
                        if (iVar3 < 1) {
                          ERR_new();
                          ERR_set_debug("ssl/statem/statem_clnt.c",0xc88,"tls_construct_cke_gost18")
                          ;
                          uVar7 = 0xc0103;
                        }
                        else {
                          iVar3 = EVP_PKEY_CTX_ctrl(pEVar8,-1,0x200,8,0x20,local_68);
                          if (iVar3 < 1) {
                            ERR_new();
                            ERR_set_debug("ssl/statem/statem_clnt.c",0xc8f,
                                          "tls_construct_cke_gost18");
                            uVar7 = 0x112;
                          }
                          else {
                            iVar4 = EVP_PKEY_CTX_ctrl(pEVar8,-1,0x200,0xc,iVar4,(void *)0x0);
                            if (iVar4 < 1) {
                              ERR_new();
                              ERR_set_debug("ssl/statem/statem_clnt.c",0xc95,
                                            "tls_construct_cke_gost18");
                              uVar7 = 0x112;
                            }
                            else {
                              iVar4 = EVP_PKEY_encrypt(pEVar8,(uchar *)0x0,(size_t *)&local_70,
                                                       puVar6,0x20);
                              if (iVar4 < 1) {
                                ERR_new();
                                uVar5 = 0xc9a;
                              }
                              else {
                                iVar4 = WPACKET_allocate_bytes(param_2,local_70,&local_78);
                                if ((iVar4 != 0) &&
                                   (iVar4 = EVP_PKEY_encrypt(pEVar8,local_78,(size_t *)&local_70,
                                                             puVar6,0x20), 0 < iVar4)) {
                                  EVP_PKEY_CTX_free(pEVar8);
                                  *(uchar **)(param_1 + 0xd8) = puVar6;
                                  uVar5 = 1;
                                  *(undefined8 *)(param_1 + 0xda) = 0x20;
                                  goto LAB_0016f26c;
                                }
                                ERR_new();
                                uVar5 = 0xca0;
                              }
                              ERR_set_debug("ssl/statem/statem_clnt.c",uVar5,
                                            "tls_construct_cke_gost18");
                              uVar7 = 0x80006;
                            }
                          }
                        }
                      }
                      uVar5 = 0x20;
                      ossl_statem_fatal(param_1,0x50,uVar7,0);
                    }
                  }
                }
              }
              EVP_PKEY_CTX_free(pEVar8);
              CRYPTO_clear_free(puVar6,uVar5,"ssl/statem/statem_clnt.c",0xcac);
              goto LAB_0016f200;
            }
            ERR_new();
            pcVar10 = "tls_construct_cke_gost18";
            uVar5 = 0xc61;
          }
          goto LAB_0016f4e1;
        }
        iVar4 = tls_construct_cke_gost(param_1,param_2);
        if (iVar4 != 0) {
LAB_0016f1e3:
          uVar5 = 1;
          goto LAB_0016f26c;
        }
        goto LAB_0016f200;
      }
      local_70 = (uchar *)0x0;
      lVar9 = *(long *)(param_1 + 300);
      if (lVar9 == 0) {
        ERR_new();
        pcVar10 = "tls_construct_cke_ecdhe";
        uVar5 = 0xb9e;
        goto LAB_0016f4e1;
      }
      pkey = (EVP_PKEY *)ssl_generate_pkey(param_1,lVar9);
      if (pkey == (EVP_PKEY *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xba4,"tls_construct_cke_ecdhe");
        ossl_statem_fatal(param_1,0x50,0xc0100,0);
      }
      else {
        iVar4 = ssl_derive(param_1,pkey,lVar9,0);
        if (iVar4 != 0) {
          lVar9 = EVP_PKEY_get1_encoded_public_key(pkey,&local_70);
          if (lVar9 == 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0xbb1,"tls_construct_cke_ecdhe");
            ossl_statem_fatal(param_1,0x50,0x80010,0);
          }
          else {
            iVar4 = WPACKET_sub_memcpy__(param_2,local_70,lVar9,1);
            if (iVar4 != 0) goto LAB_0016f888;
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0xbb6,"tls_construct_cke_ecdhe");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
          }
        }
      }
    }
    else {
      local_78 = (uchar *)0x0;
      lVar9 = *(long *)(param_1 + 300);
      local_70 = (uchar *)0x0;
      if (lVar9 == 0) {
        ERR_new();
        pkey = (EVP_PKEY *)0x0;
        ERR_set_debug("ssl/statem/statem_clnt.c",0xb61,"tls_construct_cke_dhe");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
      else {
        pkey = (EVP_PKEY *)ssl_generate_pkey(param_1,lVar9);
        if (pkey == (EVP_PKEY *)0x0) {
          ERR_new();
          uVar5 = 0xb67;
        }
        else {
          iVar4 = ssl_derive(param_1,pkey,lVar9,0);
          if (iVar4 == 0) goto LAB_0016f48e;
          lVar9 = EVP_PKEY_get1_encoded_public_key(pkey,&local_70);
          if (lVar9 == 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0xb75,"tls_construct_cke_dhe");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            EVP_PKEY_free(pkey);
            goto LAB_0016f200;
          }
          iVar4 = EVP_PKEY_get_size(pkey);
          __n = iVar4 - lVar9;
          if (__n != 0) {
            iVar4 = WPACKET_sub_allocate_bytes__(param_2,__n,&local_78,2);
            if (iVar4 == 0) {
              ERR_new();
              uVar5 = 0xb83;
              goto LAB_0016f658;
            }
            memset(local_78,0,__n);
          }
          iVar4 = WPACKET_sub_memcpy__(param_2,local_70,lVar9,2);
          if (iVar4 != 0) {
LAB_0016f888:
            CRYPTO_free(local_70);
            EVP_PKEY_free(pkey);
            goto LAB_0016f1e3;
          }
          ERR_new();
          uVar5 = 0xb8a;
        }
LAB_0016f658:
        ERR_set_debug("ssl/statem/statem_clnt.c",uVar5,"tls_construct_cke_dhe");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
    }
LAB_0016f48e:
    CRYPTO_free(local_70);
    EVP_PKEY_free(pkey);
  }
  else {
    local_78 = (uchar *)0x0;
    if (*(long *)(*(long *)(param_1 + 0x246) + 0x2b8) == 0) {
      ERR_new();
      pcVar10 = "tls_construct_cke_rsa";
      uVar5 = 0xb13;
    }
    else {
      uVar5 = X509_get0_pubkey();
      iVar4 = EVP_PKEY_is_a(uVar5,&DAT_00183541);
      if (iVar4 != 0) {
        puVar6 = (uchar *)CRYPTO_malloc(0x30,"ssl/statem/statem_clnt.c",0xb1e);
        if (puVar6 == (uchar *)0x0) {
          ERR_new();
          pcVar10 = "tls_construct_cke_rsa";
          uVar5 = 0xb20;
LAB_0016fa20:
          ERR_set_debug("ssl/statem/statem_clnt.c",uVar5,pcVar10);
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
          goto LAB_0016f200;
        }
        *puVar6 = (uchar)((uint)param_1[0x283] >> 8);
        puVar6[1] = (uchar)param_1[0x283];
        iVar4 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x26a),puVar6 + 2,0x2e,0);
        if (iVar4 < 1) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_clnt.c",0xb27,"tls_construct_cke_rsa");
          uVar5 = 0xc0100;
LAB_0016f934:
          pEVar8 = (EVP_PKEY_CTX *)0x0;
          ossl_statem_fatal(param_1,0x50,uVar5,0);
        }
        else {
          if ((0x300 < *param_1) && (iVar4 = WPACKET_start_sub_packet_len__(param_2,2), iVar4 == 0))
          {
            ERR_new();
            uVar5 = 0xb2d;
LAB_0016f921:
            ERR_set_debug("ssl/statem/statem_clnt.c",uVar5,"tls_construct_cke_rsa");
            uVar5 = 0xc0103;
            goto LAB_0016f934;
          }
          pEVar8 = (EVP_PKEY_CTX *)
                   EVP_PKEY_CTX_new_from_pkey
                             (**(undefined8 **)(param_1 + 0x26a),uVar5,
                              (*(undefined8 **)(param_1 + 0x26a))[0x88]);
          if ((pEVar8 == (EVP_PKEY_CTX *)0x0) || (iVar4 = EVP_PKEY_encrypt_init(pEVar8), iVar4 < 1))
          {
LAB_0016f510:
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0xb34,"tls_construct_cke_rsa");
            ossl_statem_fatal(param_1,0x50,0x80006,0);
          }
          else {
            iVar4 = EVP_PKEY_encrypt(pEVar8,(uchar *)0x0,(size_t *)&local_70,puVar6,0x30);
            if (iVar4 < 1) goto LAB_0016f510;
            iVar4 = WPACKET_allocate_bytes(param_2,local_70,&local_78);
            if ((iVar4 == 0) ||
               (iVar4 = EVP_PKEY_encrypt(pEVar8,local_78,(size_t *)&local_70,puVar6,0x30), iVar4 < 1
               )) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0xb39,"tls_construct_cke_rsa");
              ossl_statem_fatal(param_1,0x50,0x77,0);
            }
            else {
              EVP_PKEY_CTX_free(pEVar8);
              if ((0x300 < *param_1) && (iVar4 = WPACKET_close(param_2), iVar4 == 0)) {
                ERR_new();
                uVar5 = 0xb41;
                goto LAB_0016f921;
              }
              iVar4 = ssl_log_rsa_client_key_exchange(param_1,local_78,local_70,puVar6,0x30);
              if (iVar4 != 0) {
                *(uchar **)(param_1 + 0xd8) = puVar6;
                uVar5 = 1;
                *(undefined8 *)(param_1 + 0xda) = 0x30;
                goto LAB_0016f26c;
              }
              pEVar8 = (EVP_PKEY_CTX *)0x0;
            }
          }
        }
        CRYPTO_clear_free(puVar6,0x30,"ssl/statem/statem_clnt.c",0xb50);
        EVP_PKEY_CTX_free(pEVar8);
        goto LAB_0016f200;
      }
      ERR_new();
      pcVar10 = "tls_construct_cke_rsa";
      uVar5 = 0xb19;
    }
LAB_0016f4e1:
    ERR_set_debug("ssl/statem/statem_clnt.c",uVar5,pcVar10);
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
LAB_0016f200:
  CRYPTO_clear_free(*(undefined8 *)(param_1 + 0xd8),*(undefined8 *)(param_1 + 0xda),
                    "ssl/statem/statem_clnt.c",0xcf6);
  *(undefined8 *)(param_1 + 0xd8) = 0;
  *(undefined8 *)(param_1 + 0xda) = 0;
  CRYPTO_clear_free(*(undefined8 *)(param_1 + 0xdc),*(undefined8 *)(param_1 + 0xde),
                    "ssl/statem/statem_clnt.c",0xcfa);
  uVar5 = 0;
  *(undefined8 *)(param_1 + 0xdc) = 0;
  *(undefined8 *)(param_1 + 0xde) = 0;
LAB_0016f26c:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
}