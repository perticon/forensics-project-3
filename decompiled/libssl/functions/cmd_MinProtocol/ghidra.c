undefined8 cmd_MinProtocol(long param_1,byte *param_2)

{
  undefined8 uVar1;
  long lVar2;
  long lVar3;
  byte *pbVar4;
  byte *pbVar5;
  bool bVar6;
  bool bVar7;
  bool bVar8;
  
  lVar2 = *(long *)(param_1 + 0x18);
  bVar8 = lVar2 == 0;
  if (bVar8) {
    bVar8 = *(long *)(param_1 + 0x20) == 0;
    if (bVar8) {
      return 0;
    }
    lVar2 = *(long *)(*(long *)(param_1 + 0x20) + 0x9a8);
  }
  bVar6 = false;
  lVar3 = 5;
  pbVar4 = &DAT_0018337f;
  pbVar5 = param_2;
  do {
    if (lVar3 == 0) break;
    lVar3 = lVar3 + -1;
    bVar6 = *pbVar4 < *pbVar5;
    bVar8 = *pbVar4 == *pbVar5;
    pbVar4 = pbVar4 + 1;
    pbVar5 = pbVar5 + 1;
  } while (bVar8);
  bVar7 = false;
  bVar8 = (!bVar6 && !bVar8) == bVar6;
  if (bVar8) {
    lVar3 = 0;
  }
  else {
    lVar3 = 6;
    pbVar4 = (byte *)"SSLv3";
    pbVar5 = param_2;
    do {
      if (lVar3 == 0) break;
      lVar3 = lVar3 + -1;
      bVar7 = *pbVar4 < *pbVar5;
      bVar8 = *pbVar4 == *pbVar5;
      pbVar4 = pbVar4 + 1;
      pbVar5 = pbVar5 + 1;
    } while (bVar8);
    bVar6 = false;
    bVar8 = (!bVar7 && !bVar8) == bVar7;
    if (bVar8) {
      lVar3 = 1;
    }
    else {
      lVar3 = 6;
      pbVar4 = (byte *)"TLSv1";
      pbVar5 = param_2;
      do {
        if (lVar3 == 0) break;
        lVar3 = lVar3 + -1;
        bVar6 = *pbVar4 < *pbVar5;
        bVar8 = *pbVar4 == *pbVar5;
        pbVar4 = pbVar4 + 1;
        pbVar5 = pbVar5 + 1;
      } while (bVar8);
      bVar7 = false;
      bVar8 = (!bVar6 && !bVar8) == bVar6;
      if (bVar8) {
        lVar3 = 2;
      }
      else {
        lVar3 = 8;
        pbVar4 = (byte *)"TLSv1.1";
        pbVar5 = param_2;
        do {
          if (lVar3 == 0) break;
          lVar3 = lVar3 + -1;
          bVar7 = *pbVar4 < *pbVar5;
          bVar8 = *pbVar4 == *pbVar5;
          pbVar4 = pbVar4 + 1;
          pbVar5 = pbVar5 + 1;
        } while (bVar8);
        bVar6 = false;
        bVar8 = (!bVar7 && !bVar8) == bVar7;
        if (bVar8) {
          lVar3 = 3;
        }
        else {
          lVar3 = 8;
          pbVar4 = (byte *)"TLSv1.2";
          pbVar5 = param_2;
          do {
            if (lVar3 == 0) break;
            lVar3 = lVar3 + -1;
            bVar6 = *pbVar4 < *pbVar5;
            bVar8 = *pbVar4 == *pbVar5;
            pbVar4 = pbVar4 + 1;
            pbVar5 = pbVar5 + 1;
          } while (bVar8);
          bVar7 = false;
          bVar8 = (!bVar6 && !bVar8) == bVar6;
          if (bVar8) {
            lVar3 = 4;
          }
          else {
            lVar3 = 8;
            pbVar4 = (byte *)"TLSv1.3";
            pbVar5 = param_2;
            do {
              if (lVar3 == 0) break;
              lVar3 = lVar3 + -1;
              bVar7 = *pbVar4 < *pbVar5;
              bVar8 = *pbVar4 == *pbVar5;
              pbVar4 = pbVar4 + 1;
              pbVar5 = pbVar5 + 1;
            } while (bVar8);
            bVar6 = false;
            bVar8 = (!bVar7 && !bVar8) == bVar7;
            if (bVar8) {
              lVar3 = 5;
            }
            else {
              lVar3 = 7;
              pbVar4 = &DAT_00183ce3;
              pbVar5 = param_2;
              do {
                if (lVar3 == 0) break;
                lVar3 = lVar3 + -1;
                bVar6 = *pbVar4 < *pbVar5;
                bVar8 = *pbVar4 == *pbVar5;
                pbVar4 = pbVar4 + 1;
                pbVar5 = pbVar5 + 1;
              } while (bVar8);
              bVar7 = false;
              bVar8 = (!bVar6 && !bVar8) == bVar6;
              if (bVar8) {
                lVar3 = 6;
              }
              else {
                lVar3 = 9;
                pbVar4 = &DAT_00183cea;
                do {
                  if (lVar3 == 0) break;
                  lVar3 = lVar3 + -1;
                  bVar7 = *pbVar4 < *param_2;
                  bVar8 = *pbVar4 == *param_2;
                  pbVar4 = pbVar4 + 1;
                  param_2 = param_2 + 1;
                } while (bVar8);
                if ((!bVar7 && !bVar8) != bVar7) {
                  return 0;
                }
                lVar3 = 7;
              }
            }
          }
        }
      }
    }
  }
  if (*(int *)(versions_25637 + lVar3 * 0x10 + 8) < 0) {
    return 0;
  }
  uVar1 = ssl_set_version_bound
                    (**(undefined4 **)(lVar2 + 8),*(int *)(versions_25637 + lVar3 * 0x10 + 8),
                     *(undefined8 *)(param_1 + 0x88));
  return uVar1;
}