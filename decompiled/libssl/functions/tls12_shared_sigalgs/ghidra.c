long tls12_shared_sigalgs
               (long param_1,long *param_2,long param_3,long param_4,long param_5,long param_6)

{
  int iVar1;
  long lVar2;
  long lVar3;
  long lVar4;
  long local_48;
  long *local_40;
  
  if (param_4 == 0) {
    return 0;
  }
  local_48 = 0;
  lVar4 = 0;
  local_40 = param_2;
LAB_001496d0:
  lVar3 = *(long *)(*(long *)(param_1 + 0x9a8) + 0x620);
  lVar2 = lVar3 + 0x4d8;
  do {
    if (*(short *)(param_3 + lVar4 * 2) == *(short *)(lVar3 + 8)) {
      if (((*(int *)(lVar3 + 0x24) != 0) &&
          (iVar1 = tls12_sigalg_allowed_part_0(param_1,0x5000c,lVar3), iVar1 != 0)) &&
         (param_6 != 0)) {
        lVar2 = 0;
        goto LAB_00149759;
      }
      break;
    }
    lVar3 = lVar3 + 0x28;
  } while (lVar3 != lVar2);
  goto LAB_0014971a;
  while (lVar2 = lVar2 + 1, param_6 != lVar2) {
LAB_00149759:
    if (*(short *)(param_3 + lVar4 * 2) == *(short *)(param_5 + lVar2 * 2)) {
      local_48 = local_48 + 1;
      if (local_40 != (long *)0x0) {
        *local_40 = lVar3;
        local_40 = local_40 + 1;
      }
      break;
    }
  }
LAB_0014971a:
  lVar4 = lVar4 + 1;
  if (param_4 == lVar4) {
    return local_48;
  }
  goto LAB_001496d0;
}