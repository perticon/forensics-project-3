void** tls12_shared_sigalgs(void** rdi, void** rsi, void** rdx, void* rcx, void** r8, void* r9) {
    void** v7;
    void* v8;
    void** v9;
    void** r13_10;
    void** rbp11;
    void** r14_12;
    void* r12_13;
    void* r15_14;
    uint32_t eax15;
    void** rbx16;
    void** rdx17;
    int32_t eax18;
    uint32_t edx19;
    void* rax20;

    v7 = rsi;
    v8 = rcx;
    if (!rcx) {
        v9 = reinterpret_cast<void**>(0);
    } else {
        r13_10 = rdi;
        rbp11 = rdx;
        r14_12 = r8;
        r12_13 = r9;
        v9 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r15_14) = 0;
        *reinterpret_cast<int32_t*>(&r15_14 + 2) = 0;
        while (1) {
            eax15 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rbp11 + reinterpret_cast<uint16_t>(r15_14) * 2));
            rbx16 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_10 + 0x9a8) + 0x620);
            rdx17 = rbx16 + 0x4d8;
            do {
                if (*reinterpret_cast<void***>(&eax15) == *reinterpret_cast<void***>(rbx16 + 8)) 
                    break;
                rbx16 = rbx16 + 40;
            } while (rbx16 != rdx17);
            goto addr_4971a_7;
            if (!*reinterpret_cast<void***>(rbx16 + 36) || ((eax18 = tls12_sigalg_allowed_part_0(r13_10, 0x5000c, rbx16), !eax18) || !r12_13)) {
                addr_4971a_7:
                r15_14 = reinterpret_cast<void*>(reinterpret_cast<uint16_t>(r15_14) + 1);
                if (v8 != r15_14) 
                    continue; else 
                    break;
            } else {
                edx19 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rbp11 + reinterpret_cast<uint16_t>(r15_14) * 2));
                *reinterpret_cast<int32_t*>(&rax20) = 0;
                *reinterpret_cast<int32_t*>(&rax20 + 2) = 0;
                do {
                    if (*reinterpret_cast<void***>(&edx19) == *reinterpret_cast<void***>(r14_12 + reinterpret_cast<uint16_t>(rax20) * 2)) 
                        break;
                    rax20 = reinterpret_cast<void*>(reinterpret_cast<uint16_t>(rax20) + 1);
                } while (r12_13 != rax20);
                goto addr_4971a_7;
            }
            ++v9;
            if (v7) {
                *reinterpret_cast<void***>(v7) = rbx16;
                v7 = v7 + 8;
                goto addr_4971a_7;
            }
        }
    }
    return v9;
}