int64_t tls12_shared_sigalgs(int64_t a1, int32_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6) {
    // 0x49690
    if (a4 == 0) {
        // 0x49725
        return 0;
    }
    int32_t v1 = a2; // 0x49690
    int64_t v2 = 0;
    int64_t v3 = 0; // 0x4971a
    int64_t result; // 0x49690
    while (true) {
        int64_t v4 = v2;
        int32_t v5 = v1;
        int64_t v6 = *(int64_t *)(a1 + (int64_t)&g364); // 0x496d0
        int16_t * v7 = (int16_t *)(2 * v3 + a3); // 0x496d7
        int64_t v8 = *(int64_t *)(v6 + (int64_t)&g221); // 0x496dd
        int64_t v9 = v8 + (int64_t)&g192; // 0x496e4
        int64_t v10 = v8;
        int64_t v11; // 0x49690
        while (*(int16_t *)(v10 + 8) != *v7) {
            int64_t v12 = v10 + 40; // 0x496f0
            v1 = v5;
            v11 = v4;
            if (v12 == v9) {
                goto lab_0x4971a_2;
            }
            v10 = v12;
        }
        // 0x496ff
        v1 = v5;
        v11 = v4;
        if (*(int32_t *)(v10 + 36) != 0) {
            // 0x49706
            v1 = v5;
            v11 = v4;
            if (!((a6 == 0 | (int32_t)tls12_sigalg_allowed_part_0(a1, 0x5000c, v10) == 0))) {
                int64_t v13 = 0;
                while (*(int16_t *)(2 * v13 + a5) != *v7) {
                    int64_t v14 = v13 + 1; // 0x49750
                    v1 = v5;
                    v11 = v4;
                    if (v14 == a6) {
                        goto lab_0x4971a_2;
                    }
                    v13 = v14;
                }
                int64_t v15 = v4 + 1; // 0x49765
                v1 = 0;
                v11 = v15;
                if (v5 != 0) {
                    // 0x49770
                    *(int64_t *)(int64_t)v5 = v10;
                    v1 = v5 + 8;
                    v11 = v15;
                }
            }
        }
      lab_0x4971a_2:
        // 0x4971a
        v2 = v11;
        v3++;
        result = v2;
        if (v3 == a4) {
            // break -> 0x49725
            break;
        }
    }
    // 0x49725
    return result;
}