undefined8 ssl3_clear(undefined4 *param_1)

{
  undefined8 uVar1;
  ulong uVar2;
  undefined8 *puVar3;
  byte bVar4;
  
  bVar4 = 0;
  ssl3_cleanup_key_block();
  CRYPTO_free(*(void **)(param_1 + 0xbe));
  OPENSSL_sk_pop_free(*(undefined8 *)(param_1 + 0xc2),PTR_X509_NAME_free_001a7fc8);
  CRYPTO_free(*(void **)(param_1 + 0xd4));
  CRYPTO_clear_free(*(undefined8 *)(param_1 + 0xd8),*(undefined8 *)(param_1 + 0xda),"ssl/s3_lib.c");
  CRYPTO_free(*(void **)(param_1 + 0xe4));
  CRYPTO_free(*(void **)(param_1 + 0xe6));
  EVP_PKEY_free(*(EVP_PKEY **)(param_1 + 0xba));
  EVP_PKEY_free(*(EVP_PKEY **)(param_1 + 300));
  ssl3_free_digest_list(param_1);
  CRYPTO_free(*(void **)(param_1 + 0x122));
  CRYPTO_free(*(void **)(param_1 + 0x126));
  *(undefined8 *)(param_1 + 0x2a) = 0;
  *(undefined8 *)(param_1 + 300) = 0;
  puVar3 = (undefined8 *)((ulong)(param_1 + 0x2c) & 0xfffffffffffffff8);
  for (uVar2 = (ulong)(((int)param_1 -
                       (int)(undefined8 *)((ulong)(param_1 + 0x2c) & 0xfffffffffffffff8)) + 0x4b8U
                      >> 3); uVar2 != 0; uVar2 = uVar2 - 1) {
    *puVar3 = 0;
    puVar3 = puVar3 + (ulong)bVar4 * -2 + 1;
  }
  uVar1 = ssl_free_wbio_buffer(param_1);
  if ((int)uVar1 == 0) {
    return uVar1;
  }
  *param_1 = 0x300;
  CRYPTO_free(*(void **)(param_1 + 0x2c6));
  *(undefined8 *)(param_1 + 0x2c6) = 0;
  *(undefined8 *)(param_1 + 0x2c8) = 0;
  return 1;
}