int ssl3_clear(SSL *s)
{
    ssl3_cleanup_key_block(s);
    OPENSSL_free(s->s3.tmp.ctype);
    sk_X509_NAME_pop_free(s->s3.tmp.peer_ca_names, X509_NAME_free);
    OPENSSL_free(s->s3.tmp.ciphers_raw);
    OPENSSL_clear_free(s->s3.tmp.pms, s->s3.tmp.pmslen);
    OPENSSL_free(s->s3.tmp.peer_sigalgs);
    OPENSSL_free(s->s3.tmp.peer_cert_sigalgs);

    EVP_PKEY_free(s->s3.tmp.pkey);
    EVP_PKEY_free(s->s3.peer_tmp);

    ssl3_free_digest_list(s);

    OPENSSL_free(s->s3.alpn_selected);
    OPENSSL_free(s->s3.alpn_proposed);

    /* NULL/zero-out everything in the s3 struct */
    memset(&s->s3, 0, sizeof(s->s3));

    if (!ssl_free_wbio_buffer(s))
        return 0;

    s->version = SSL3_VERSION;

#if !defined(OPENSSL_NO_NEXTPROTONEG)
    OPENSSL_free(s->ext.npn);
    s->ext.npn = NULL;
    s->ext.npn_len = 0;
#endif

    return 1;
}