ulong tls_construct_server_hello(undefined4 *param_1,undefined8 param_2)

{
  long lVar1;
  bool bVar2;
  int iVar3;
  ulong uVar4;
  undefined1 *puVar5;
  undefined8 uVar6;
  undefined4 *puVar7;
  undefined4 uVar8;
  long in_FS_OFFSET;
  undefined auStack56 [8];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
       (iVar3 = **(int **)(param_1 + 2), 0x303 < iVar3)) && (iVar3 != 0x10000)) ||
     (param_1[0x23a] == 1)) {
    uVar8 = 0x303;
    bVar2 = true;
  }
  else {
    uVar8 = *param_1;
    bVar2 = false;
  }
  iVar3 = WPACKET_put_bytes__(param_2,uVar8,2);
  if (iVar3 == 0) {
LAB_00178910:
    ERR_new();
    uVar6 = 0x90f;
  }
  else {
    puVar5 = (undefined1 *)(param_1 + 0x50);
    if (param_1[0x23a] == 1) {
      puVar5 = hrrrandom;
    }
    iVar3 = WPACKET_memcpy(param_2,puVar5,0x20);
    if (iVar3 == 0) goto LAB_00178910;
    lVar1 = *(long *)(param_1 + 0x246);
    if ((*(int *)(lVar1 + 0x2b0) == 0) &&
       (((*(byte *)(*(long *)(param_1 + 0x26a) + 0x50) & 2) != 0 || (param_1[0x134] != 0)))) {
      if (bVar2) goto LAB_00178814;
      uVar4 = *(ulong *)(lVar1 + 0x250);
      if (uVar4 < 0x21) goto LAB_00178939;
LAB_001788e2:
      ERR_new();
      uVar6 = 0x933;
    }
    else {
      *(undefined8 *)(lVar1 + 0x250) = 0;
      if (bVar2) {
LAB_00178814:
        uVar4 = *(ulong *)(param_1 + 600);
        puVar7 = param_1 + 0x250;
        if (0x20 < uVar4) goto LAB_001788e2;
        uVar8 = 0;
      }
      else {
        uVar4 = 0;
LAB_00178939:
        puVar7 = (undefined4 *)(lVar1 + 600);
        uVar8 = 0;
        if (*(undefined4 **)(param_1 + 0xd0) != (undefined4 *)0x0) {
          uVar8 = **(undefined4 **)(param_1 + 0xd0);
        }
      }
      iVar3 = WPACKET_sub_memcpy__(param_2,puVar7,uVar4,1);
      if (((iVar3 != 0) &&
          (iVar3 = (**(code **)(*(long *)(param_1 + 2) + 0x98))
                             (*(undefined8 *)(param_1 + 0xb8),param_2,auStack56), iVar3 != 0)) &&
         (iVar3 = WPACKET_put_bytes__(param_2,uVar8,1), iVar3 != 0)) {
        uVar6 = 0x800;
        if (param_1[0x23a] != 1) {
          uVar6 = 0x100;
          if (((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
             ((iVar3 = **(int **)(param_1 + 2), iVar3 < 0x304 || (uVar6 = 0x200, iVar3 == 0x10000)))
             ) {
            uVar6 = 0x100;
          }
        }
        uVar4 = tls_construct_extensions(param_1,param_2,uVar6,0,0);
        if ((int)uVar4 != 0) {
          if (param_1[0x23a] == 1) {
            SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x246));
            *(undefined8 *)(param_1 + 0x246) = 0;
            param_1[0x134] = 0;
            iVar3 = create_synthetic_message_hash(param_1,0,0,0,0);
            uVar4 = (ulong)(iVar3 != 0);
          }
          else {
            uVar4 = 1;
            if ((*(byte *)(param_1 + 0x25a) & 1) == 0) {
              iVar3 = ssl3_digest_cached_records(param_1,0);
              uVar4 = (ulong)(iVar3 != 0);
            }
          }
        }
        goto LAB_00178894;
      }
      ERR_new();
      uVar6 = 0x944;
    }
  }
  ERR_set_debug("ssl/statem/statem_srvr.c",uVar6,"tls_construct_server_hello");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  uVar4 = 0;
LAB_00178894:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}