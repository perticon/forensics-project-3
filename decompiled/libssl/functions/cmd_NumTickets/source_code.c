static int cmd_NumTickets(SSL_CONF_CTX *cctx, const char *value)
{
    int rv = 0;
    int num_tickets = atoi(value);

    if (num_tickets >= 0) {
        if (cctx->ctx)
            rv = SSL_CTX_set_num_tickets(cctx->ctx, num_tickets);
        if (cctx->ssl)
            rv = SSL_set_num_tickets(cctx->ssl, num_tickets);
    }
    return rv;
}