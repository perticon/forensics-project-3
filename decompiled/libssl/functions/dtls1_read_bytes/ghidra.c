uint dtls1_read_bytes(SSL *param_1,int param_2,undefined4 *param_3,void *param_4,
                     stack_st_SRTP_PROTECTION_PROFILE *param_5,SSL *param_6,
                     stack_st_SRTP_PROTECTION_PROFILE **param_7)

{
  byte bVar1;
  char cVar2;
  ushort uVar3;
  uint uVar4;
  long lVar5;
  long *plVar6;
  SSL_SESSION *c;
  undefined4 uVar7;
  undefined4 uVar8;
  undefined4 uVar9;
  int iVar10;
  int iVar11;
  int iVar12;
  int iVar13;
  pitem *item;
  BIO *b;
  long lVar14;
  ushort *puVar15;
  _func_3153 *p_Var16;
  undefined8 uVar17;
  stack_st_SRTP_PROTECTION_PROFILE *psVar18;
  uchar *psk;
  uint uVar19;
  long in_FS_OFFSET;
  uint local_c8;
  _func_3153 *local_c0;
  char local_98;
  
  iVar13 = (int)param_6;
  lVar5 = *(long *)(in_FS_OFFSET + 0x28);
  if ((param_1[4].mode != 0) || (iVar12 = ssl3_setup_buffers(), iVar12 != 0)) {
    if (((param_2 - 0x16U < 2) || (param_2 == 0)) && ((iVar13 == 0 || (param_2 == 0x17)))) {
      iVar12 = ossl_statem_get_in_handshake(param_1);
      if ((iVar12 == 0) && (iVar12 = SSL_in_init(param_1), iVar12 != 0)) {
        uVar19 = (*param_1->handshake_func)(param_1);
        if ((int)uVar19 < 0) goto LAB_00154dd8;
        if (uVar19 == 0) goto LAB_001551c0;
      }
      local_c0 = (_func_3153 *)0x0;
LAB_00154ce0:
      do {
        do {
          param_1->rwstate = 1;
          iVar12 = SSL_is_init_finished(param_1);
          if (((iVar12 != 0) &&
              (param_1[6].srtp_profiles == (stack_st_SRTP_PROTECTION_PROFILE *)0x0)) &&
             (item = pqueue_pop(*(pqueue *)(param_1[10].tlsext_ellipticcurvelist_length + 0x50)),
             item != (pitem *)0x0)) {
            plVar6 = (long *)item->data;
            SSL3_BUFFER_release(&param_1[4].mode);
            *(long *)&param_1[10].first_packet = *plVar6;
            *(long *)&param_1[10].max_send_fragment = plVar6[1];
            uVar7 = *(undefined4 *)((long)plVar6 + 0x14);
            uVar8 = *(undefined4 *)(plVar6 + 3);
            uVar9 = *(undefined4 *)((long)plVar6 + 0x1c);
            *(undefined4 *)&param_1[4].mode = *(undefined4 *)(plVar6 + 2);
            *(undefined4 *)((long)&param_1[4].mode + 4) = uVar7;
            *(undefined4 *)&param_1[4].max_cert_list = uVar8;
            *(undefined4 *)((long)&param_1[4].max_cert_list + 4) = uVar9;
            iVar12 = *(int *)((long)plVar6 + 0x24);
            uVar19 = *(uint *)(plVar6 + 5);
            uVar7 = *(undefined4 *)((long)plVar6 + 0x2c);
            param_1[4].first_packet = *(int *)(plVar6 + 4);
            param_1[4].client_version = iVar12;
            param_1[4].max_send_fragment = uVar19;
            *(undefined4 *)&param_1[4].field_0x1d4 = uVar7;
            uVar7 = *(undefined4 *)((long)plVar6 + 0x34);
            uVar8 = *(undefined4 *)(plVar6 + 7);
            uVar9 = *(undefined4 *)((long)plVar6 + 0x3c);
            *(undefined4 *)&param_1[4].tlsext_debug_cb = *(undefined4 *)(plVar6 + 6);
            *(undefined4 *)((long)&param_1[4].tlsext_debug_cb + 4) = uVar7;
            *(undefined4 *)&param_1[4].tlsext_debug_arg = uVar8;
            *(undefined4 *)((long)&param_1[4].tlsext_debug_arg + 4) = uVar9;
            uVar7 = *(undefined4 *)((long)plVar6 + 0x44);
            uVar8 = *(undefined4 *)(plVar6 + 9);
            uVar9 = *(undefined4 *)((long)plVar6 + 0x4c);
            *(undefined4 *)&param_1[6].next_proto_negotiated_len = *(undefined4 *)(plVar6 + 8);
            *(undefined4 *)&param_1[6].field_0x28c = uVar7;
            *(undefined4 *)&param_1[6].srtp_profiles = uVar8;
            *(undefined4 *)((long)&param_1[6].srtp_profiles + 4) = uVar9;
            uVar7 = *(undefined4 *)((long)plVar6 + 0x54);
            uVar19 = *(uint *)(plVar6 + 0xb);
            uVar4 = *(uint *)((long)plVar6 + 0x5c);
            *(undefined4 *)&param_1[6].srtp_profile = *(undefined4 *)(plVar6 + 10);
            *(undefined4 *)((long)&param_1[6].srtp_profile + 4) = uVar7;
            param_1[6].tlsext_heartbeat = uVar19;
            param_1[6].tlsext_hb_pending = uVar4;
            iVar12 = *(int *)((long)plVar6 + 100);
            iVar10 = *(int *)(plVar6 + 0xd);
            iVar11 = *(int *)((long)plVar6 + 0x6c);
            param_1[6].tlsext_hb_seq = *(uint *)(plVar6 + 0xc);
            param_1[6].renegotiate = iVar12;
            param_1[7].version = iVar10;
            param_1[7].type = iVar11;
            uVar7 = *(undefined4 *)((long)plVar6 + 0x74);
            uVar8 = *(undefined4 *)(plVar6 + 0xf);
            uVar9 = *(undefined4 *)((long)plVar6 + 0x7c);
            *(undefined4 *)&param_1[7].method = *(undefined4 *)(plVar6 + 0xe);
            *(undefined4 *)((long)&param_1[7].method + 4) = uVar7;
            *(undefined4 *)&param_1[7].rbio = uVar8;
            *(undefined4 *)((long)&param_1[7].rbio + 4) = uVar9;
            uVar7 = *(undefined4 *)((long)plVar6 + 0x84);
            uVar8 = *(undefined4 *)(plVar6 + 0x11);
            uVar9 = *(undefined4 *)((long)plVar6 + 0x8c);
            *(undefined4 *)&param_1[7].wbio = *(undefined4 *)(plVar6 + 0x10);
            *(undefined4 *)((long)&param_1[7].wbio + 4) = uVar7;
            *(undefined4 *)&param_1[7].bbio = uVar8;
            *(undefined4 *)((long)&param_1[7].bbio + 4) = uVar9;
            lVar14 = *plVar6;
            *(undefined4 *)((long)&param_1[10].tlsext_ocsp_resplen + 2) =
                 *(undefined4 *)(lVar14 + 5);
            *(undefined2 *)((long)&param_1[10].tlsext_ticket_expected + 2) =
                 *(undefined2 *)(lVar14 + 9);
            CRYPTO_free(item->data);
            pitem_free(item);
          }
          iVar12 = dtls1_handle_timeout(param_1);
        } while (0 < iVar12);
        iVar12 = ossl_statem_in_error(param_1);
        if (iVar12 != 0) goto LAB_001551c0;
        if ((param_1[6].srtp_profiles != (stack_st_SRTP_PROTECTION_PROFILE *)0x0) &&
           (*(int *)((long)&param_1[4].client_CA + 4) != 0xf1)) {
          iVar12 = *(int *)&param_1[6].field_0x28c;
          if (iVar12 != 0x15) {
LAB_00154d46:
            *(undefined4 *)((long)&param_1[10].tlsext_ecpointformatlist + 4) = 0;
          }
LAB_00154d50:
          uVar19 = (param_1->ex_data).dummy;
          psk = (uchar *)(ulong)uVar19;
          if ((uVar19 == 0) || (iVar12 == 0x16)) {
            uVar4 = param_1->shutdown;
            uVar19 = uVar4 & 2;
            if (uVar19 != 0) {
              param_1[6].srtp_profiles = (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
              uVar19 = 0;
              *(undefined4 *)&param_1[7].rbio = 1;
              param_1->rwstate = 1;
              break;
            }
            if (param_2 == iVar12) {
              iVar12 = SSL_in_init(param_1);
              if (((iVar12 == 0) || (param_2 != 0x17)) || (*(long *)&param_1[3].server != 0)) {
                if (param_3 != (undefined4 *)0x0) goto LAB_001552c5;
LAB_001552cf:
                psVar18 = param_1[6].srtp_profiles;
                if (param_5 == (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
                  if (psVar18 == (stack_st_SRTP_PROTECTION_PROFILE *)0x0) goto LAB_00154f52;
                }
                else {
                  if (psVar18 < param_5) {
                    param_5 = psVar18;
                  }
                  memcpy(param_4,(void *)(*(long *)&param_1[6].tlsext_heartbeat +
                                         *(long *)&param_1[6].tlsext_hb_seq),(size_t)param_5);
                  if (iVar13 == 0) {
                    lVar14 = *(long *)&param_1[6].tlsext_heartbeat;
                    if ((*(byte *)&param_1[3].tlsext_debug_cb & 2) != 0) {
                      OPENSSL_cleanse((void *)(lVar14 + *(long *)&param_1[6].tlsext_hb_seq),
                                      (size_t)param_5);
                      lVar14 = *(long *)&param_1[6].tlsext_heartbeat;
                    }
                    *(long *)&param_1[6].tlsext_heartbeat = (long)&(param_5->stack).num + lVar14;
                    psVar18 = (stack_st_SRTP_PROTECTION_PROFILE *)
                              ((long)param_1[6].srtp_profiles - (long)param_5);
                    param_1[6].srtp_profiles = psVar18;
                    if (psVar18 == (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
                      *(undefined4 *)((long)&param_1[4].client_CA + 4) = 0xf0;
                      *(undefined8 *)&param_1[6].tlsext_heartbeat = 0;
                      goto LAB_001553a7;
                    }
                  }
                  else if (param_1[6].srtp_profiles == (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
LAB_001553a7:
                    *(undefined4 *)&param_1[7].rbio = 1;
                  }
                  uVar19 = 1;
                  *param_7 = param_5;
                }
                break;
              }
              ERR_new();
              ERR_set_debug("ssl/record/rec_layer_d1.c",0x1eb,"dtls1_read_bytes");
              uVar17 = 100;
            }
            else {
              if (iVar12 == 0x14) {
                if (param_2 == 0x16 && param_3 != (undefined4 *)0x0) {
                  SSL_in_init(param_1);
LAB_001552c5:
                  *param_3 = *(undefined4 *)&param_1[6].field_0x28c;
                  goto LAB_001552cf;
                }
                if ((uVar4 & 1) == 0) goto LAB_00154f8a;
LAB_00154f40:
                param_1->rwstate = 1;
                param_1[6].srtp_profiles = (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
LAB_00154f52:
                *(undefined4 *)&param_1[7].rbio = 1;
                break;
              }
              if (iVar12 == 0x15) {
                psVar18 = param_1[6].srtp_profiles;
                if ((0 < (long)psVar18) && (psVar18 != (stack_st_SRTP_PROTECTION_PROFILE *)0x1)) {
                  puVar15 = (ushort *)
                            (*(long *)&param_1[6].tlsext_heartbeat +
                            *(long *)&param_1[6].tlsext_hb_seq);
                  bVar1 = *(byte *)((long)puVar15 + 1);
                  uVar3 = *puVar15;
                  if (psVar18 == (stack_st_SRTP_PROTECTION_PROFILE *)0x2) {
                    cVar2 = *(char *)puVar15;
                    if ((code *)param_1[1].tlsext_ocsp_resp != (code *)0x0) {
                      psk = (uchar *)0x2;
                      param_6 = param_1;
                      (*(code *)param_1[1].tlsext_ocsp_resp)(0,param_1->version,0x15);
                    }
                    p_Var16 = param_1[3].psk_client_callback;
                    if (((param_1[3].psk_client_callback != (_func_3153 *)0x0) ||
                        (p_Var16 = *(_func_3153 **)(*(long *)&param_1[3].ex_data.dummy + 0x120),
                        p_Var16 != (_func_3153 *)0x0)) ||
                       (p_Var16 = local_c0, local_c0 != (_func_3153 *)0x0)) {
                      (*p_Var16)(param_1,(char *)0x4004,
                                 (char *)(ulong)(ushort)(uVar3 << 8 | uVar3 >> 8),(uint)p_Var16,psk,
                                 (uint)param_6);
                      local_c0 = p_Var16;
                    }
                    local_c8 = (uint)bVar1;
                    if (cVar2 != '\x01') {
                      if (cVar2 == '\x02') {
                        param_1->rwstate = 1;
                        *(uint *)&param_1->client_CA = local_c8;
                        ERR_new();
                        ERR_set_debug("ssl/record/rec_layer_d1.c",0x265,"dtls1_read_bytes");
                        ossl_statem_fatal(param_1,0xffffffff,local_c8 + 1000,"SSL alert number %d",
                                          bVar1);
                        param_1->shutdown = param_1->shutdown | 2;
                        c = *(SSL_SESSION **)&param_1[3].sid_ctx_length;
                        *(undefined4 *)&param_1[7].rbio = 1;
                        SSL_CTX_remove_session(*(SSL_CTX **)&param_1[4].mac_flags,c);
                      }
                      else {
                        ERR_new();
                        ERR_set_debug("ssl/record/rec_layer_d1.c",0x26d,"dtls1_read_bytes");
                        uVar19 = 0xffffffff;
                        ossl_statem_fatal(param_1,0x2f,0xf6,0);
                      }
                      break;
                    }
                    *(undefined4 *)&param_1[7].rbio = 1;
                    *(uint *)&(param_1->ex_data).field_0xc = local_c8;
                    iVar12 = *(int *)((long)&param_1[10].tlsext_ecpointformatlist + 4) + 1;
                    *(int *)((long)&param_1[10].tlsext_ecpointformatlist + 4) = iVar12;
                    if (iVar12 == 5) {
                      ERR_new();
                      ERR_set_debug("ssl/record/rec_layer_d1.c",0x24a,"dtls1_read_bytes");
                      uVar17 = 0x199;
                      goto LAB_00154dc3;
                    }
                    if (local_c8 == 0) {
                      param_1->shutdown = param_1->shutdown | 2;
                      break;
                    }
                    goto LAB_00154ce0;
                  }
                }
                ERR_new();
                ERR_set_debug("ssl/record/rec_layer_d1.c",0x232,"dtls1_read_bytes");
                uVar17 = 0xcd;
                goto LAB_00154dc3;
              }
              if ((uVar4 & 1) != 0) goto LAB_00154f40;
              if (iVar12 == 0x16) {
                iVar12 = ossl_statem_get_in_handshake(param_1);
                if (iVar12 == 0) {
                  if ((param_1[7].wbio !=
                       (BIO *)(ulong)*(ushort *)param_1[10].tlsext_ellipticcurvelist_length) ||
                     (param_1[6].srtp_profiles < (stack_st_SRTP_PROTECTION_PROFILE *)0xc))
                  goto LAB_00154f8a;
                  dtls1_get_message_header(*(undefined8 *)&param_1[6].tlsext_hb_seq);
                  if (local_98 == '\x14') {
                    iVar12 = dtls1_check_timeout_num(param_1);
                    if ((iVar12 < 0) ||
                       ((iVar12 = dtls1_retransmit_buffered_messages(param_1), iVar12 < 1 &&
                        (iVar12 = ossl_statem_in_error(param_1), iVar12 != 0)))) goto LAB_001551c0;
                    param_1[6].srtp_profiles = (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
                    *(undefined4 *)&param_1[7].rbio = 1;
                    bVar1 = *(byte *)&param_1[3].tlsext_debug_arg;
                  }
                  else {
                    iVar12 = SSL_is_init_finished();
                    if (iVar12 == 0) {
                      ERR_new();
                      uVar17 = 0x2c1;
                      goto LAB_001553d5;
                    }
                    ossl_statem_set_in_init(param_1);
                    uVar19 = (*param_1->handshake_func)(param_1);
                    if ((int)uVar19 < 0) break;
                    if (uVar19 == 0) goto LAB_001551c0;
                    bVar1 = *(byte *)&param_1[3].tlsext_debug_arg;
                  }
                  if (((bVar1 & 4) == 0) && (param_1[4].tlsext_debug_cb == (_func_3155 *)0x0)) {
                    param_1->rwstate = 3;
                    uVar19 = 0xffffffff;
                    b = SSL_get_rbio(param_1);
                    BIO_clear_flags(b,0xf);
                    BIO_set_flags(b,9);
                    break;
                  }
                  goto LAB_00154ce0;
                }
                iVar12 = *(int *)&param_1[6].field_0x28c;
              }
              if (iVar12 < 0x17) {
                if (0x13 < iVar12) {
                  ERR_new();
                  ERR_set_debug("ssl/record/rec_layer_d1.c",0x2ef,"dtls1_read_bytes");
                  uVar17 = 0xc0103;
                  goto LAB_00154dc3;
                }
LAB_00155344:
                ERR_new();
                uVar17 = 0x2e5;
              }
              else {
                if (iVar12 != 0x17) goto LAB_00155344;
                if (((*(int *)&param_1->mode != 0) && (*(int *)&param_1->options != 0)) &&
                   (iVar13 = ossl_statem_app_data_allowed(param_1), iVar13 != 0)) {
                  *(undefined4 *)&param_1->mode = 2;
                  goto LAB_001551c0;
                }
                ERR_new();
                uVar17 = 0x2ff;
              }
              ERR_set_debug("ssl/record/rec_layer_d1.c",uVar17,"dtls1_read_bytes");
              uVar17 = 0xf5;
            }
LAB_00154dc3:
            uVar19 = 0xffffffff;
            ossl_statem_fatal(param_1,10,uVar17,0);
            break;
          }
          iVar12 = dtls1_buffer_record(param_1,param_1[10].tlsext_ellipticcurvelist_length + 0x48,
                                       &param_1[7].bbio);
          if (iVar12 < 0) goto LAB_001551c0;
LAB_00154f8a:
          param_1[6].srtp_profiles = (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
          *(undefined4 *)&param_1[7].rbio = 1;
          goto LAB_00154ce0;
        }
        *(undefined8 *)&param_1[4].references = 0;
        iVar12 = dtls1_get_record(param_1);
        if (0 < iVar12) {
          iVar12 = *(int *)&param_1[6].field_0x28c;
          *(undefined8 *)&param_1[4].references = 1;
          if ((iVar12 != 0x15) &&
             (param_1[6].srtp_profiles != (stack_st_SRTP_PROTECTION_PROFILE *)0x0))
          goto LAB_00154d46;
          goto LAB_00154d50;
        }
        uVar19 = dtls1_read_failed(param_1);
      } while (0 < (int)uVar19);
      goto LAB_00154dd8;
    }
    ERR_new();
    uVar17 = 0x16a;
LAB_001553d5:
    ERR_set_debug("ssl/record/rec_layer_d1.c",uVar17,"dtls1_read_bytes");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
LAB_001551c0:
  uVar19 = 0xffffffff;
LAB_00154dd8:
  if (lVar5 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar19;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}