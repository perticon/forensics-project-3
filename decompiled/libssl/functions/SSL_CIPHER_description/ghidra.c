char * SSL_CIPHER_description(SSL_CIPHER *param_1,char *buf,int size)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  uint uVar4;
  undefined8 uVar5;
  char *pcVar6;
  char *pcVar7;
  char *pcVar8;
  char *pcVar9;
  size_t n;
  undefined8 uVar10;
  
  if (buf == (char *)0x0) {
    n = 0x80;
    buf = (char *)CRYPTO_malloc(0x80,"ssl/ssl_ciph.c",0x6a6);
    if (buf == (char *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_ciph.c",0x6a7,"SSL_CIPHER_description");
      ERR_set_error(0x14,0xc0100,0);
      return (char *)0x0;
    }
  }
  else {
    if (size < 0x80) {
      return (char *)0x0;
    }
    n = (size_t)size;
  }
  iVar1 = *(int *)&param_1->algorithm_auth;
  uVar2 = *(uint *)((long)&param_1->algorithm_mkey + 4);
  uVar3 = *(uint *)((long)&param_1->algorithm_auth + 4);
  uVar4 = *(uint *)&param_1->algorithm_enc;
  uVar10 = 0x131ada;
  uVar5 = ssl_protocol_to_string(*(undefined4 *)((long)&param_1->algorithm_enc + 4));
  switch(uVar2) {
  case 0:
    pcVar9 = "any";
    break;
  case 1:
    pcVar9 = "RSA";
    break;
  case 2:
    pcVar9 = "DH";
    break;
  case 3:
  case 5:
  case 6:
  case 7:
  case 9:
  case 10:
  case 0xb:
  case 0xc:
  case 0xd:
  case 0xe:
  case 0xf:
  case 0x11:
  case 0x12:
  case 0x13:
  case 0x14:
  case 0x15:
  case 0x16:
  case 0x17:
  case 0x18:
  case 0x19:
  case 0x1a:
  case 0x1b:
  case 0x1c:
  case 0x1d:
  case 0x1e:
  case 0x1f:
    pcVar9 = "unknown";
    break;
  case 4:
    pcVar9 = "ECDH";
    break;
  case 8:
    pcVar9 = "PSK";
    break;
  case 0x10:
    pcVar9 = "GOST";
    break;
  case 0x20:
    pcVar9 = "SRP";
    break;
  default:
    pcVar9 = "DHEPSK";
    if (uVar2 != 0x100) {
      if (uVar2 < 0x101) {
        pcVar9 = "RSAPSK";
        if ((uVar2 != 0x40) && (pcVar9 = "ECDHEPSK", uVar2 != 0x80)) {
          pcVar9 = "unknown";
        }
      }
      else {
        pcVar9 = "GOST18";
        if (uVar2 != 0x200) {
          pcVar9 = "unknown";
        }
      }
    }
  }
  switch(iVar1) {
  case 0:
    pcVar7 = "any";
    break;
  case 1:
    pcVar7 = "RSA";
    break;
  case 2:
    pcVar7 = "DSS";
    break;
  case 3:
  case 5:
  case 6:
  case 7:
  case 9:
  case 10:
  case 0xb:
  case 0xc:
  case 0xd:
  case 0xe:
  case 0xf:
  case 0x11:
  case 0x12:
  case 0x13:
  case 0x14:
  case 0x15:
  case 0x16:
  case 0x17:
  case 0x18:
  case 0x19:
  case 0x1a:
  case 0x1b:
  case 0x1c:
  case 0x1d:
  case 0x1e:
  case 0x1f:
    pcVar7 = "unknown";
    break;
  case 4:
    pcVar7 = "None";
    break;
  case 8:
    pcVar7 = "ECDSA";
    break;
  case 0x10:
    pcVar7 = "PSK";
    break;
  case 0x20:
    pcVar7 = "GOST01";
    break;
  default:
    pcVar7 = "SRP";
    if ((iVar1 != 0x40) && (pcVar7 = "unknown", iVar1 == 0xa0)) {
      pcVar7 = "GOST12";
    }
  }
  pcVar6 = "AESGCM(128)";
  if (uVar3 != 0x1000) {
    if (uVar3 < 0x1001) {
      if (uVar3 < 0x21) {
        pcVar6 = "unknown";
        switch(uVar3) {
        case 1:
          pcVar6 = "DES(56)";
          break;
        case 2:
          pcVar6 = "3DES(168)";
          break;
        case 3:
        case 5:
        case 6:
        case 7:
        case 9:
        case 10:
        case 0xb:
        case 0xc:
        case 0xd:
        case 0xe:
        case 0xf:
        case 0x11:
        case 0x12:
        case 0x13:
        case 0x14:
        case 0x15:
        case 0x16:
        case 0x17:
        case 0x18:
        case 0x19:
        case 0x1a:
        case 0x1b:
        case 0x1c:
        case 0x1d:
        case 0x1e:
        case 0x1f:
          pcVar6 = "unknown";
          break;
        case 4:
          pcVar6 = "RC4(128)";
          break;
        case 8:
          pcVar6 = "RC2(128)";
          break;
        case 0x10:
          pcVar6 = "IDEA(128)";
          break;
        case 0x20:
          pcVar6 = "None";
        }
      }
      else {
        pcVar6 = "Camellia(128)";
        if (uVar3 != 0x100) {
          if (uVar3 < 0x101) {
            pcVar6 = "AES(128)";
            if ((uVar3 != 0x40) && (pcVar6 = "unknown", uVar3 == 0x80)) {
              pcVar6 = "AES(256)";
            }
          }
          else {
            pcVar6 = "GOST89(256)";
            if (uVar3 != 0x400) {
              if (uVar3 == 0x800) {
                pcVar6 = "SEED(128)";
              }
              else {
                pcVar6 = "unknown";
                if (uVar3 == 0x200) {
                  pcVar6 = "Camellia(256)";
                }
              }
            }
          }
        }
      }
    }
    else {
      pcVar6 = "GOST89(256)";
      if (uVar3 != 0x40000) {
        if (uVar3 < 0x40001) {
          pcVar6 = "AESCCM(256)";
          if (uVar3 != 0x8000) {
            if (uVar3 < 0x8001) {
              pcVar6 = "AESGCM(256)";
              if ((uVar3 != 0x2000) && (pcVar6 = "unknown", uVar3 == 0x4000)) {
                pcVar6 = "AESCCM(128)";
              }
            }
            else {
              pcVar6 = "AESCCM8(128)";
              if ((uVar3 != 0x10000) && (pcVar6 = "unknown", uVar3 == 0x20000)) {
                pcVar6 = "AESCCM8(256)";
              }
            }
          }
        }
        else {
          pcVar6 = "ARIAGCM(256)";
          if (uVar3 != 0x200000) {
            if (uVar3 < 0x200001) {
              pcVar6 = "CHACHA20/POLY1305(256)";
              if ((uVar3 != 0x80000) && (pcVar6 = "unknown", uVar3 == 0x100000)) {
                pcVar6 = "ARIAGCM(128)";
              }
            }
            else {
              pcVar6 = "MAGMA";
              if ((uVar3 != 0x400000) && (pcVar6 = "unknown", uVar3 == 0x800000)) {
                pcVar6 = "KUZNYECHIK";
              }
            }
          }
        }
      }
    }
  }
  if (uVar4 < 0x21) {
    pcVar8 = "unknown";
    switch(uVar4) {
    case 1:
      pcVar8 = "MD5";
      break;
    case 2:
      pcVar8 = "SHA1";
      break;
    case 3:
    case 5:
    case 6:
    case 7:
    case 9:
    case 10:
    case 0xb:
    case 0xc:
    case 0xd:
    case 0xe:
    case 0xf:
    case 0x11:
    case 0x12:
    case 0x13:
    case 0x14:
    case 0x15:
    case 0x16:
    case 0x17:
    case 0x18:
    case 0x19:
    case 0x1a:
    case 0x1b:
    case 0x1c:
    case 0x1d:
    case 0x1e:
    case 0x1f:
      pcVar8 = "unknown";
      break;
    case 4:
      pcVar8 = "GOST94";
      break;
    case 8:
      pcVar8 = "GOST89";
      break;
    case 0x10:
      pcVar8 = "SHA256";
      break;
    case 0x20:
      pcVar8 = "SHA384";
    }
  }
  else {
    pcVar8 = "GOST2012";
    if (uVar4 != 0x80) {
      if (uVar4 < 0x81) {
        pcVar8 = "unknown";
        if (uVar4 == 0x40) {
          pcVar8 = "AEAD";
        }
      }
      else {
        pcVar8 = "GOST89";
        if ((uVar4 != 0x100) && (pcVar8 = "unknown", uVar4 == 0x200)) {
          pcVar8 = "GOST2012";
        }
      }
    }
  }
  BIO_snprintf(buf,n,"%-30s %-7s Kx=%-8s Au=%-5s Enc=%-22s Mac=%-4s\n",param_1->name,uVar5,pcVar9,
               pcVar7,pcVar6,pcVar8,uVar10);
  return buf;
}