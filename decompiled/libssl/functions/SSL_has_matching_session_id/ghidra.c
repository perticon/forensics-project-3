int SSL_has_matching_session_id(SSL *ssl,uchar *id,uint id_len)

{
  uint uVar1;
  int iVar2;
  long lVar3;
  long in_FS_OFFSET;
  int local_3c8 [148];
  ulong local_178;
  undefined local_170 [336];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (id_len < 0x21) {
    local_3c8[0] = ssl->version;
    local_178 = (ulong)id_len;
    __memcpy_chk(local_170,id,local_178,0x148);
    iVar2 = CRYPTO_THREAD_read_lock(*(undefined8 *)(*(long *)&ssl[4].mac_flags + 0x3c8));
    if (iVar2 != 0) {
      lVar3 = OPENSSL_LH_retrieve(*(undefined8 *)(*(long *)&ssl[4].mac_flags + 0x30),local_3c8);
      CRYPTO_THREAD_unlock(*(undefined8 *)(*(long *)&ssl[4].mac_flags + 0x3c8));
      uVar1 = (uint)(lVar3 != 0);
      goto LAB_00135f97;
    }
  }
  uVar1 = 0;
LAB_00135f97:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}