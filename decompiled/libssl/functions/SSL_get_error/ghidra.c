int SSL_get_error(SSL *s,int ret_code)

{
  int iVar1;
  int iVar2;
  ulong uVar3;
  BIO *b;
  
  if (0 < ret_code) {
    return 0;
  }
  uVar3 = ERR_peek_error();
  if (uVar3 != 0) {
    if ((uVar3 & 0x80000000) != 0) {
      return 5;
    }
    if ((char)(uVar3 >> 0x17) == '\x02') {
      return 5;
    }
    return 1;
  }
  iVar1 = SSL_want(s);
  if (iVar1 == 3) {
    b = SSL_get_rbio(s);
    iVar1 = BIO_test_flags(b,1);
    if (iVar1 != 0) {
      return 2;
    }
    iVar1 = BIO_test_flags(b,2);
    if (iVar1 == 0) {
      iVar1 = BIO_test_flags(b,4);
      if (iVar1 != 0) goto LAB_0013abba;
      goto LAB_0013aad1;
    }
LAB_0013ac30:
    iVar1 = 3;
  }
  else {
LAB_0013aad1:
    iVar1 = SSL_want(s);
    if (iVar1 == 2) {
      b = s->wbio;
      iVar1 = BIO_test_flags(b,2);
      if (iVar1 != 0) goto LAB_0013ac30;
      iVar1 = BIO_test_flags(b,1);
      if (iVar1 != 0) {
        return 2;
      }
      iVar1 = BIO_test_flags(b,4);
      if (iVar1 != 0) {
LAB_0013abba:
        iVar1 = BIO_get_retry_reason(b);
        if (iVar1 == 2) {
          return 7;
        }
        if (iVar1 != 3) {
          return 5;
        }
        return 8;
      }
    }
    iVar1 = SSL_want(s);
    if (iVar1 != 4) {
      iVar2 = SSL_want(s);
      iVar1 = 0xc;
      if (iVar2 != 8) {
        iVar2 = SSL_want(s);
        iVar1 = 9;
        if (iVar2 != 5) {
          iVar2 = SSL_want(s);
          iVar1 = 10;
          if (iVar2 != 6) {
            iVar1 = SSL_want(s);
            if (iVar1 == 7) {
              iVar1 = 0xb;
            }
            else {
              iVar1 = 5;
              if ((*(byte *)&s->shutdown & 2) != 0) {
                iVar1 = (*(int *)&(s->ex_data).field_0xc == 0) + 5;
              }
            }
          }
        }
      }
    }
  }
  return iVar1;
}