int ssl_get_prev_session(int *param_1,long param_2)

{
  int iVar1;
  size_t __n;
  bool bVar2;
  int iVar3;
  SSL_SESSION *ses;
  time_t tVar4;
  long in_FS_OFFSET;
  SSL_SESSION *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = (SSL_SESSION *)0x0;
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) != 0) ||
      (iVar3 = **(int **)(param_1 + 2), iVar3 < 0x304)) || (iVar3 == 0x10000)) {
    iVar3 = tls_get_ticket_from_client(param_1,param_2,&local_38);
    if (1 < iVar3) {
      if ((1 < iVar3 - 2U) || (*(long *)(param_2 + 0x28) == 0)) goto LAB_00143e48;
      bVar2 = true;
      ses = (SSL_SESSION *)lookup_sess_in_cache(param_1,param_2 + 0x30);
      goto joined_r0x00143e52;
    }
    if (iVar3 < 0) {
LAB_00143e48:
      bVar2 = false;
      ses = local_38;
      goto joined_r0x00143e52;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_sess.c",0x251,"ssl_get_prev_session");
    iVar3 = -1;
    bVar2 = false;
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    goto LAB_00143f3c;
  }
  param_1[0x2a6] = 1;
  iVar3 = tls_parse_extension(param_1,0x12,0x80,*(undefined8 *)(param_2 + 0x288),0,0);
  if ((iVar3 == 0) ||
     (iVar3 = tls_parse_extension(param_1,0x19,0x80,*(undefined8 *)(param_2 + 0x288),0,0),
     iVar3 == 0)) {
    iVar3 = -1;
    goto LAB_00143e19;
  }
  ses = *(SSL_SESSION **)(param_1 + 0x246);
  bVar2 = false;
joined_r0x00143e52:
  local_38 = ses;
  if (ses == (SSL_SESSION *)0x0) {
    iVar3 = 0;
    goto LAB_00143e19;
  }
  if (((ses->ssl_version == *param_1) &&
      (__n = *(size_t *)(ses[1].master_key + 0xc), __n == *(size_t *)(param_1 + 0x23c))) &&
     (iVar3 = memcmp(ses[1].master_key + 0x14,param_1 + 0x23e,__n), iVar3 == 0)) {
    if ((__n == 0) && ((*(byte *)(param_1 + 0x25a) & 1) != 0)) {
      ERR_new();
      ERR_set_debug("ssl/ssl_sess.c",0x27f,"ssl_get_prev_session");
      iVar3 = -1;
      ossl_statem_fatal(param_1,0x50,0x115,0);
    }
    else {
      tVar4 = time((time_t *)0x0);
      if ((tVar4 <= *(long *)ses[1].krb5_client_princ) ||
         (*(int *)(ses[1].krb5_client_princ + 8) != 0)) {
        if ((local_38[1].krb5_client_princ[0xa0] & 1) == 0) {
          ses = local_38;
          if ((*(ulong *)(param_1 + 0x2a) & 0x200) != 0) goto LAB_00143dd0;
        }
        else if ((*(ulong *)(param_1 + 0x2a) & 0x200) == 0) {
          ERR_new();
          ERR_set_debug("ssl/ssl_sess.c",0x292,"ssl_get_prev_session");
          iVar3 = -1;
          ossl_statem_fatal(param_1,0x2f,0x68,0);
          goto LAB_00143f3c;
        }
        if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) != 0) ||
            (iVar3 = **(int **)(param_1 + 2), iVar3 == 0x10000)) || (iVar3 < 0x304)) {
          SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x246));
          *(SSL_SESSION **)(param_1 + 0x246) = local_38;
        }
        LOCK();
        *(int *)(*(long *)(param_1 + 0x2e2) + 0x9c) =
             *(int *)(*(long *)(param_1 + 0x2e2) + 0x9c) + 1;
        iVar3 = 1;
        *(undefined8 *)(param_1 + 0x26e) = *(undefined8 *)(*(long *)(param_1 + 0x246) + 0x2c8);
        goto LAB_00143e19;
      }
      LOCK();
      *(int *)(*(long *)(param_1 + 0x2e2) + 0x94) = *(int *)(*(long *)(param_1 + 0x2e2) + 0x94) + 1;
      if (bVar2) {
        SSL_CTX_remove_session(*(SSL_CTX **)(param_1 + 0x2e2),local_38);
      }
      else {
        iVar3 = 0;
      }
    }
LAB_00143f3c:
    ses = local_38;
    if (local_38 == (SSL_SESSION *)0x0) goto LAB_00143e19;
  }
  else {
LAB_00143dd0:
    iVar3 = 0;
  }
  SSL_SESSION_free(ses);
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
      (iVar1 = **(int **)(param_1 + 2), iVar1 != 0x10000)) && (0x303 < iVar1)) {
    *(undefined8 *)(param_1 + 0x246) = 0;
  }
  if (!bVar2) {
    param_1[0x2a6] = 1;
  }
LAB_00143e19:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}