void SSL_free(SSL *ssl)

{
  uint *puVar1;
  uint uVar2;
  undefined *puVar3;
  _func_3150 *ptr;
  
  if (ssl == (SSL *)0x0) {
    return;
  }
  LOCK();
  puVar1 = &ssl[3].max_send_fragment;
  uVar2 = *puVar1;
  *puVar1 = *puVar1 - 1;
  if ((uVar2 != 1) && (0 < (int)(uVar2 - 1))) {
    return;
  }
  X509_VERIFY_PARAM_free((X509_VERIFY_PARAM *)ssl[1].tlsext_ecpointformatlist);
  OPENSSL_sk_pop_free(ssl[1].tlsext_ellipticcurvelist,tlsa_free);
  ssl[1].tlsext_ellipticcurvelist = (uchar *)0x0;
  OSSL_STACK_OF_X509_free(ssl[1].tlsext_opaque_prf_input);
  ssl[1].tlsext_opaque_prf_input = (void *)0x0;
  X509_free((X509 *)ssl[1].tlsext_session_ticket);
  *(undefined (*) [16])&ssl[1].tlsext_opaque_prf_input_len = (undefined  [16])0x0;
  *(undefined8 *)((long)&ssl[1].tls_session_ticket_ext_cb + 4) = 0xffffffffffffffff;
  CRYPTO_free_ex_data(0,ssl,(CRYPTO_EX_DATA *)&ssl[3].options);
  RECORD_LAYER_release(&ssl[4].ex_data.dummy);
  ssl_free_wbio_buffer(ssl);
  BIO_free_all(ssl->wbio);
  ssl->wbio = (BIO *)0x0;
  BIO_free_all(ssl->rbio);
  ssl->rbio = (BIO *)0x0;
  BUF_MEM_free((BUF_MEM *)ssl->d1);
  OPENSSL_sk_free(ssl[1].initial_ctx);
  OPENSSL_sk_free(ssl[1].next_proto_negotiated);
  OPENSSL_sk_free(*(undefined8 *)&ssl[1].next_proto_negotiated_len);
  OPENSSL_sk_free(ssl[1].tls_session_secret_cb_arg);
  if (*(long *)&ssl[3].sid_ctx_length != 0) {
    ssl_clear_bad_session(ssl);
    SSL_SESSION_free(*(SSL_SESSION **)&ssl[3].sid_ctx_length);
  }
  SSL_SESSION_free(*(SSL_SESSION **)(ssl[3].sid_ctx + 4));
  CRYPTO_free(*(void **)(ssl[3].sid_ctx + 0xc));
  ssl_clear_cipher_ctx(ssl);
  ssl_clear_hash_ctx(&ssl[3].init_buf);
  ssl_clear_hash_ctx(&ssl[3].s3);
  ssl_cert_free(ssl[3].d1);
  CRYPTO_free(ssl[0xb].wbio);
  CRYPTO_free((void *)ssl[3].tlsext_opaque_prf_input_len);
  SSL_CTX_free(*(SSL_CTX **)&ssl[4].mac_flags);
  CRYPTO_free(ssl[3].srtp_profile);
  CRYPTO_free(*(void **)&ssl[3].tlsext_hb_seq);
  CRYPTO_free(ssl[4].method);
  CRYPTO_free(ssl[4].wbio);
  OPENSSL_sk_pop_free(ssl[3].tls_session_secret_cb_arg,PTR_X509_EXTENSION_free_001a7f90);
  OPENSSL_sk_pop_free(ssl[3].tls_session_secret_cb,PTR_OCSP_RESPID_free_001a7fb0);
  SCT_LIST_free(ssl[4].cipher_list);
  CRYPTO_free(ssl[3].tls_session_ticket_ext_cb);
  CRYPTO_free(ssl[3].initial_ctx);
  CRYPTO_free(*(void **)&ssl[4].state);
  CRYPTO_free(ssl[4].s2);
  ptr = ssl[4].msg_callback;
  if (ptr != (_func_3150 *)0x0) {
    CRYPTO_free(*(void **)(ptr + 0x288));
    ptr = ssl[4].msg_callback;
  }
  CRYPTO_free(ptr);
  CRYPTO_free(ssl[4].write_hash);
  EVP_MD_CTX_free(*(undefined8 *)&ssl[4].sid_ctx_length);
  puVar3 = PTR_X509_NAME_free_001a7fc8;
  OPENSSL_sk_pop_free(ssl[3].max_cert_list,PTR_X509_NAME_free_001a7fc8);
  OPENSSL_sk_pop_free(*(undefined8 *)&ssl[3].first_packet,puVar3);
  OSSL_STACK_OF_X509_free(ssl[3].client_CA);
  if (ssl->method != (SSL_METHOD *)0x0) {
    (*ssl->method->ssl_accept)(ssl);
  }
  SSL_CTX_free(*(SSL_CTX **)&ssl[3].ex_data.dummy);
  ASYNC_WAIT_CTX_free(ssl[10].tlsext_session_ticket);
  CRYPTO_free(ssl[4].init_msg);
  OPENSSL_sk_free(ssl[4].enc_read_ctx);
  CRYPTO_THREAD_lock_free(*(undefined8 *)&ssl[10].next_proto_negotiated_len);
  CRYPTO_free(ssl);
  return;
}