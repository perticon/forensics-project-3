long SSL_get0_peer_certificate(long param_1)

{
  long lVar1;
  
  if (param_1 == 0) {
    lVar1 = 0;
  }
  else {
    lVar1 = *(long *)(param_1 + 0x918);
    if (lVar1 != 0) {
      return *(long *)(lVar1 + 0x2b8);
    }
  }
  return lVar1;
}