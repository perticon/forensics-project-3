int construct_ca_names(long param_1,long param_2,undefined8 param_3)

{
  int iVar1;
  int iVar2;
  int iVar3;
  X509_NAME *a;
  undefined8 uVar4;
  long in_FS_OFFSET;
  uchar *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = WPACKET_start_sub_packet_len__(param_3);
  if (iVar1 == 0) {
    ERR_new();
    uVar4 = 0x913;
  }
  else {
    if ((param_2 != 0) && ((*(byte *)(param_1 + 0x9e9) & 2) == 0)) {
      for (iVar1 = 0; iVar3 = OPENSSL_sk_num(param_2), iVar1 < iVar3; iVar1 = iVar1 + 1) {
        a = (X509_NAME *)OPENSSL_sk_value(param_2);
        if ((((a == (X509_NAME *)0x0) || (iVar3 = i2d_X509_NAME(a,(uchar **)0x0), iVar3 < 0)) ||
            (iVar2 = WPACKET_sub_allocate_bytes__(param_3,(long)iVar3,&local_48,2), iVar2 == 0)) ||
           (iVar2 = i2d_X509_NAME(a,&local_48), iVar2 != iVar3)) {
          ERR_new();
          iVar1 = 0;
          ERR_set_debug("ssl/statem/statem_lib.c",0x924,"construct_ca_names");
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
          goto LAB_00178019;
        }
      }
    }
    iVar1 = WPACKET_close(param_3);
    if (iVar1 != 0) {
      iVar1 = 1;
      goto LAB_00178019;
    }
    ERR_new();
    uVar4 = 0x92b;
  }
  ERR_set_debug("ssl/statem/statem_lib.c",uVar4,"construct_ca_names");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
LAB_00178019:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}