uint ssl3_read_bytes(SSL *param_1,int param_2,int *param_3,void *param_4,uchar *param_5,int param_6,
                    uchar **param_7)

{
  uchar uVar1;
  byte bVar2;
  char cVar3;
  ushort uVar4;
  stack_st_SRTP_PROTECTION_PROFILE *psVar5;
  SSL_SESSION *c;
  byte bVar6;
  bool bVar7;
  bool bVar8;
  int iVar9;
  uint uVar10;
  char *pcVar11;
  long lVar12;
  BIO *b;
  void **ppvVar13;
  ushort *puVar14;
  uchar *puVar15;
  uchar *puVar16;
  int iVar17;
  undefined8 uVar18;
  uchar *puVar19;
  SSL *pSVar20;
  void *__dest;
  ulong uVar21;
  ulong uVar22;
  stack_st_SRTP_PROTECTION_PROFILE *psVar23;
  _func_3153 *p_Var24;
  uchar *puVar25;
  bool bVar26;
  _func_3153 *local_48;
  
  if (((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
      (iVar9 = param_1->method->version, iVar9 != 0x10000)) && (0x303 < iVar9)) {
    bVar7 = true;
    bVar6 = 0;
  }
  else {
    bVar7 = false;
    bVar6 = 1;
  }
  puVar19 = param_5;
  if ((param_1[4].mode != 0) || (iVar9 = ssl3_setup_read_buffer(param_1), iVar9 != 0)) {
    if (((param_2 - 0x16U < 2) || (param_2 == 0)) && (param_6 == 0 || param_2 == 0x17)) {
      if ((param_2 == 0x16) && (pcVar11 = param_1[10].tlsext_hostname, pcVar11 != (char *)0x0)) {
        ppvVar13 = &param_1[10].tlsext_debug_arg;
        puVar19 = (uchar *)0x0;
        if (param_5 == (uchar *)0x0) {
LAB_00158b57:
          *(undefined *)&param_1[10].tlsext_debug_arg = *(undefined *)ppvVar13;
          if ((((char *)0x1 < pcVar11) &&
              (*(undefined *)((long)&param_1[10].tlsext_debug_arg + 1) =
                    *(undefined *)((long)ppvVar13 + 1), pcVar11 != (char *)0x2)) &&
             (*(undefined *)((long)&param_1[10].tlsext_debug_arg + 2) =
                   *(undefined *)((long)ppvVar13 + 2), pcVar11 != (char *)0x3)) {
            *(undefined *)((long)&param_1[10].tlsext_debug_arg + 3) =
                 *(undefined *)((long)ppvVar13 + 3);
          }
        }
        else {
          do {
            uVar1 = *(uchar *)ppvVar13;
            ppvVar13 = (void **)((long)ppvVar13 + 1);
            *(uchar *)((long)param_4 + (long)puVar19) = uVar1;
            puVar19 = puVar19 + 1;
            pcVar11 = param_1[10].tlsext_hostname + -1;
            param_1[10].tlsext_hostname = pcVar11;
            if (param_5 == puVar19) {
              if (pcVar11 != (char *)0x0) goto LAB_00158b57;
              break;
            }
          } while (pcVar11 != (char *)0x0);
        }
        if (param_3 != (int *)0x0) {
          *param_3 = 0x16;
        }
        *param_7 = puVar19;
        return 1;
      }
      iVar9 = ossl_statem_get_in_handshake();
      if ((iVar9 == 0) && (iVar9 = SSL_in_init(), iVar9 != 0)) {
        uVar10 = (*param_1->handshake_func)(param_1);
        if ((int)uVar10 < 0) {
          return uVar10;
        }
        if (uVar10 == 0) {
          return 0xffffffff;
        }
      }
      uVar21 = *(ulong *)&param_1[4].references;
      local_48 = (_func_3153 *)0x0;
LAB_0015863c:
      param_1->rwstate = 1;
      if (uVar21 == 0) goto LAB_00158688;
LAB_00158650:
      puVar15 = &param_1[6].next_proto_negotiated_len;
      uVar22 = 0;
      while (*(int *)(puVar15 + 0x38) != 0) {
        uVar22 = uVar22 + 1;
        puVar15 = puVar15 + 0x50;
        if (uVar21 == uVar22) goto code_r0x0015867d;
      }
      pcVar11 = param_1[10].tlsext_hostname;
      iVar9 = *(int *)(puVar15 + 4);
      if (pcVar11 == (char *)0x0) {
LAB_00158703:
        iVar17 = (param_1->ex_data).dummy;
        if (iVar9 != 0x15) goto LAB_00158712;
        if (iVar17 != 0) goto LAB_00158952;
      }
      else {
        if (iVar9 != 0x16) {
          if (((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
              (iVar17 = param_1->method->version, iVar17 != 0x10000)) && (0x303 < iVar17)) {
            ERR_new();
            ERR_set_debug("ssl/record/rec_layer_s3.c",0x55f,"ssl3_read_bytes");
            uVar18 = 0x125;
            goto LAB_00158b34;
          }
          goto LAB_00158703;
        }
        iVar17 = (param_1->ex_data).dummy;
LAB_00158712:
        if (*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) !=
            (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
          *(undefined4 *)((long)&param_1[10].tlsext_ecpointformatlist + 4) = 0;
        }
        if ((iVar17 != 0) && (iVar9 != 0x16)) {
LAB_00158952:
          ERR_new();
          ERR_set_debug("ssl/record/rec_layer_s3.c",0x571,"ssl3_read_bytes");
          uVar18 = 0x91;
          goto LAB_00158b34;
        }
      }
      uVar10 = param_1->shutdown;
      if ((uVar10 & 2) != 0) {
        *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) =
             (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
        param_1->rwstate = 1;
        return 0;
      }
      if (param_2 != iVar9) {
        if ((iVar9 == 0x14) && ((bool)((param_2 == 0x16 && param_3 != (int *)0x0) & bVar6))) {
          SSL_in_init(param_1);
          bVar26 = false;
          if (*(int *)(puVar15 + 4) == 0x14) {
LAB_00158bdb:
            if (param_1[10].tlsext_hostname != (char *)0x0) {
              ERR_new();
              ERR_set_debug("ssl/record/rec_layer_s3.c",0x596,"ssl3_read_bytes");
              uVar18 = 0x85;
              goto LAB_00158b34;
            }
            goto LAB_00158c22;
          }
          goto LAB_00158c2a;
        }
        if (*(int *)puVar15 == 2) {
          ERR_new();
          uVar18 = 0x5e4;
          goto LAB_001586b4;
        }
        pSVar20 = (SSL *)param_1->method;
        if (pSVar20->version == 0x10000) {
          if ((param_1->server != 0) || (iVar9 != 0x15)) {
            param_1->version = *(int *)puVar15;
            ERR_new();
            ERR_set_debug("ssl/record/rec_layer_s3.c",0x5f1,"ssl3_read_bytes");
            uVar18 = 0xf4;
            goto LAB_00158b34;
          }
        }
        else if (iVar9 != 0x15) {
          if ((uVar10 & 1) == 0) {
            if (iVar9 == 0x16) goto LAB_00158fc0;
            if (iVar9 == 0x14) goto LAB_00159022;
            if ((char *)0x3 < pcVar11) goto LAB_00158dcb;
          }
          else {
            if (iVar9 != 0x16) {
              *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) =
                   (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
              *(int *)(puVar15 + 0x38) = 1;
              ERR_new();
              ERR_set_debug("ssl/record/rec_layer_s3.c",0x66d,"ssl3_read_bytes");
              ossl_statem_fatal(param_1,0xffffffff,0x123,0);
              return 0xffffffff;
            }
            if (((*(byte *)&pSVar20->cipher_list_by_id[3].stack.num & 8) != 0) ||
               (pSVar20->version < 0x304)) {
              *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) =
                   (stack_st_SRTP_PROTECTION_PROFILE *)0x0;
              *(int *)(puVar15 + 0x38) = 1;
              if ((*(byte *)&param_1[3].tlsext_debug_arg & 4) == 0) goto LAB_00158f0e;
              goto LAB_0015863c;
            }
LAB_00158fc0:
            psVar23 = (stack_st_SRTP_PROTECTION_PROFILE *)(4 - (long)pcVar11);
            if (*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) < psVar23 ||
                *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) == psVar23) {
              psVar23 = *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8);
            }
            memcpy((char *)((long)&param_1[10].tlsext_debug_arg + (long)pcVar11),
                   (void *)(*(long *)(puVar15 + 0x18) + *(long *)(puVar15 + 0x20)),(size_t)psVar23);
            *(long *)(puVar15 + 0x18) = (long)&(psVar23->stack).num + *(long *)(puVar15 + 0x18);
            *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) =
                 (stack_st_SRTP_PROTECTION_PROFILE *)
                 ((long)*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) - (long)psVar23);
            pcVar11 = param_1[10].tlsext_hostname;
            param_1[10].tlsext_hostname = pcVar11 + (long)&psVar23->stack;
            if (*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) ==
                (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
              *(int *)(puVar15 + 0x38) = 1;
            }
            if (pcVar11 + (long)&psVar23->stack < (char *)0x4) goto LAB_00158d89;
            if (*(int *)(puVar15 + 4) == 0x14) {
LAB_00159022:
              ERR_new();
              ERR_set_debug("ssl/record/rec_layer_s3.c",0x690,"ssl3_read_bytes");
              uVar18 = 0x85;
              goto LAB_00158b34;
            }
LAB_00158dcb:
            iVar9 = ossl_statem_get_in_handshake(param_1);
            if (iVar9 == 0) {
              iVar9 = *(int *)((long)&param_1->s3 + 4);
              ossl_statem_set_in_init(param_1);
              uVar10 = (*param_1->handshake_func)(param_1);
              if ((int)uVar10 < 0) {
                return uVar10;
              }
              if (uVar10 == 0) {
                return 0xffffffff;
              }
              if (iVar9 == 0xb) {
                return 0xffffffff;
              }
              if (((*(byte *)&param_1[3].tlsext_debug_arg & 4) == 0) &&
                 (param_1[4].tlsext_debug_cb == (_func_3155 *)0x0)) {
LAB_00158f0e:
                param_1->rwstate = 3;
                b = SSL_get_rbio(param_1);
                BIO_clear_flags(b,0xf);
                BIO_set_flags(b,9);
                return 0xffffffff;
              }
              goto LAB_00158d89;
            }
            iVar9 = *(int *)(puVar15 + 4);
          }
          if (iVar9 < 0x17) {
            if (0x13 < iVar9) {
              ERR_new();
              ERR_set_debug("ssl/record/rec_layer_s3.c",0x6d6,"ssl3_read_bytes");
              uVar18 = 0xc0103;
              goto LAB_00158b34;
            }
LAB_001587ab:
            ERR_new();
            uVar18 = 0x6cc;
          }
          else {
            if (iVar9 != 0x17) goto LAB_001587ab;
            iVar9 = ossl_statem_app_data_allowed(param_1);
            if (iVar9 != 0) {
              *(undefined4 *)&param_1->mode = 2;
              return 0xffffffff;
            }
            iVar9 = ossl_statem_skip_early_data(param_1);
            if (iVar9 != 0) {
              iVar9 = early_data_count_ok(param_1,*(stack_st_SRTP_PROTECTION_PROFILE **)
                                                   (puVar15 + 8),0x68);
              if (iVar9 == 0) {
                return 0xffffffff;
              }
              *(int *)(puVar15 + 0x38) = 1;
              goto LAB_00158d89;
            }
            ERR_new();
            uVar18 = 0x6f5;
          }
          ERR_set_debug("ssl/record/rec_layer_s3.c",uVar18,"ssl3_read_bytes");
          uVar18 = 0xf5;
          goto LAB_00158b34;
        }
        psVar23 = *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8);
        if ((0 < (long)psVar23) && (psVar23 != (stack_st_SRTP_PROTECTION_PROFILE *)0x1)) {
          puVar14 = (ushort *)(*(long *)(puVar15 + 0x18) + *(long *)(puVar15 + 0x20));
          uVar4 = *puVar14;
          bVar2 = *(byte *)((long)puVar14 + 1);
          if (psVar23 == (stack_st_SRTP_PROTECTION_PROFILE *)0x2) {
            cVar3 = *(char *)puVar14;
            if ((code *)param_1[1].tlsext_ocsp_resp != (code *)0x0) {
              puVar14 = *(ushort **)&param_1[1].tlsext_ocsp_resplen;
              puVar19 = (uchar *)0x2;
              pSVar20 = param_1;
              (*(code *)param_1[1].tlsext_ocsp_resp)(0,param_1->version,0x15);
            }
            p_Var24 = param_1[3].psk_client_callback;
            if (((param_1[3].psk_client_callback != (_func_3153 *)0x0) ||
                (p_Var24 = *(_func_3153 **)(*(long *)&param_1[3].ex_data.dummy + 0x120),
                p_Var24 != (_func_3153 *)0x0)) ||
               (p_Var24 = local_48, local_48 != (_func_3153 *)0x0)) {
              (*p_Var24)(param_1,(char *)0x4004,(char *)(ulong)(ushort)(uVar4 << 8 | uVar4 >> 8),
                         (uint)puVar14,puVar19,(uint)pSVar20);
              local_48 = p_Var24;
            }
            if (cVar3 == '\x01') {
              iVar9 = *(int *)((long)&param_1[10].tlsext_ecpointformatlist + 4);
              *(uint *)&(param_1->ex_data).field_0xc = (uint)bVar2;
              *(int *)(puVar15 + 0x38) = 1;
              iVar9 = iVar9 + 1;
              *(int *)((long)&param_1[10].tlsext_ecpointformatlist + 4) = iVar9;
              if (iVar9 == 5) {
LAB_00159119:
                ERR_new();
                ERR_set_debug("ssl/record/rec_layer_s3.c",0x61d,"ssl3_read_bytes");
                uVar18 = 0x199;
                goto LAB_00158b34;
              }
              if (!bVar7) {
                if (bVar2 == 0) goto LAB_00158b05;
                if (bVar2 == 100) {
LAB_00158e83:
                  ERR_new();
                  ERR_set_debug("ssl/record/rec_layer_s3.c",0x640,"ssl3_read_bytes");
                  ossl_statem_fatal(param_1,0x28,0x153,0);
                  return 0xffffffff;
                }
                goto LAB_00158d89;
              }
              if (bVar2 == 0x5a) goto LAB_00158d89;
            }
            else {
              if (!bVar7) {
                if (bVar2 == 0) {
                  if (cVar3 != '\x02') {
LAB_0015918f:
                    ERR_new();
                    ERR_set_debug("ssl/record/rec_layer_s3.c",0x647,"ssl3_read_bytes");
                    ossl_statem_fatal(param_1,0x2f,0xf6,0);
                    return 0xffffffff;
                  }
                }
                else if (cVar3 != '\x02') {
                  if (bVar2 == 100) goto LAB_00158e83;
                  goto LAB_0015918f;
                }
                goto LAB_00158df9;
              }
              if (bVar2 == 0x5a) {
                *(undefined4 *)&(param_1->ex_data).field_0xc = 0x5a;
                iVar9 = *(int *)((long)&param_1[10].tlsext_ecpointformatlist + 4);
                *(int *)(puVar15 + 0x38) = 1;
                iVar9 = iVar9 + 1;
                *(int *)((long)&param_1[10].tlsext_ecpointformatlist + 4) = iVar9;
                if (iVar9 == 5) goto LAB_00159119;
                goto LAB_00158d89;
              }
            }
            if (bVar2 == 0) {
LAB_00158b05:
              param_1->shutdown = param_1->shutdown | 2;
              return 0;
            }
LAB_00158df9:
            param_1->rwstate = 1;
            *(uint *)&param_1->client_CA = (uint)bVar2;
            ERR_new();
            ERR_set_debug("ssl/record/rec_layer_s3.c",0x630,"ssl3_read_bytes");
            ossl_statem_fatal(param_1,0xffffffff,bVar2 + 1000,"SSL alert number %d");
            param_1->shutdown = param_1->shutdown | 2;
            c = *(SSL_SESSION **)&param_1[3].sid_ctx_length;
            *(int *)(puVar15 + 0x38) = 1;
            SSL_CTX_remove_session(*(SSL_CTX **)&param_1[4].mac_flags,c);
            return 0;
          }
        }
        ERR_new();
        ERR_set_debug("ssl/record/rec_layer_s3.c",0x604,"ssl3_read_bytes");
        uVar18 = 0xcd;
LAB_00158b34:
        ossl_statem_fatal(param_1,10,uVar18,0);
        return 0xffffffff;
      }
      iVar9 = SSL_in_init(param_1);
      bVar26 = param_2 == 0x17;
      bVar8 = iVar9 != 0 && bVar26;
      if (bVar8) {
        bVar26 = bVar8;
        if (*(long *)&param_1[3].server == 0) {
          ERR_new();
          ERR_set_debug("ssl/record/rec_layer_s3.c",0x58f,"ssl3_read_bytes");
          uVar18 = 100;
          goto LAB_00158b34;
        }
      }
      else if ((param_2 == 0x16) && (*(int *)(puVar15 + 4) == 0x14)) goto LAB_00158bdb;
LAB_00158c22:
      if (param_3 != (int *)0x0) {
LAB_00158c2a:
        *param_3 = *(int *)(puVar15 + 4);
      }
      if (param_5 == (uchar *)0x0) {
        if (*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) ==
            (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
          *(int *)(puVar15 + 0x38) = 1;
          return uVar10 & 2;
        }
        return uVar10 & 2;
      }
      puVar16 = (uchar *)0x0;
      __dest = param_4;
      if (param_6 != 0) goto LAB_00158d3e;
      do {
        psVar23 = (stack_st_SRTP_PROTECTION_PROFILE *)(param_5 + -(long)puVar16);
        if (*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) < psVar23 ||
            *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) == psVar23) {
          psVar23 = *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8);
        }
        param_4 = (void *)((long)__dest + (long)psVar23);
        memcpy(__dest,(void *)(*(long *)(puVar15 + 0x18) + *(long *)(puVar15 + 0x20)),
               (size_t)psVar23);
        lVar12 = *(long *)(puVar15 + 0x18);
        if ((*(byte *)&param_1[3].tlsext_debug_cb & 2) != 0) {
          OPENSSL_cleanse((void *)(lVar12 + *(long *)(puVar15 + 0x20)),(size_t)psVar23);
          lVar12 = *(long *)(puVar15 + 0x18);
        }
        psVar5 = *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8);
        *(long *)(puVar15 + 0x18) = (long)&(psVar23->stack).num + lVar12;
        *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) =
             (stack_st_SRTP_PROTECTION_PROFILE *)((long)psVar5 - (long)psVar23);
        puVar25 = puVar15;
        if ((stack_st_SRTP_PROTECTION_PROFILE *)((long)psVar5 - (long)psVar23) ==
            (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
          *(undefined4 *)((long)&param_1[4].client_CA + 4) = 0xf0;
          uVar22 = uVar22 + 1;
          puVar25 = puVar15 + 0x50;
          *(undefined8 *)(puVar15 + 0x18) = 0;
          *(int *)(puVar15 + 0x38) = 1;
        }
        puVar16 = puVar16 + (long)psVar23;
      } while (((bool)(bVar26 & puVar16 < param_5)) &&
              (__dest = param_4, puVar15 = puVar25, uVar22 < uVar21));
      goto LAB_00158d80;
    }
    ERR_new();
    uVar18 = 0x509;
LAB_001586b4:
    ERR_set_debug("ssl/record/rec_layer_s3.c",uVar18,"ssl3_read_bytes");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  return 0xffffffff;
LAB_00158d3e:
  do {
    psVar23 = *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8);
    if ((stack_st_SRTP_PROTECTION_PROFILE *)(param_5 + -(long)puVar16) <=
        *(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8)) {
      psVar23 = (stack_st_SRTP_PROTECTION_PROFILE *)(param_5 + -(long)puVar16);
    }
    param_4 = memcpy(param_4,(void *)(*(long *)(puVar15 + 0x18) + *(long *)(puVar15 + 0x20)),
                     (size_t)psVar23);
    param_4 = (void *)((long)param_4 + (long)psVar23);
    if (*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) ==
        (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
      *(int *)(puVar15 + 0x38) = 1;
LAB_00158d22:
      uVar22 = uVar22 + 1;
      puVar15 = puVar15 + 0x50;
    }
    else if (*(stack_st_SRTP_PROTECTION_PROFILE **)(puVar15 + 8) == psVar23) goto LAB_00158d22;
    puVar16 = puVar16 + (long)psVar23;
  } while (((bool)(bVar26 & puVar16 < param_5)) && (uVar22 < uVar21));
LAB_00158d80:
  if (puVar16 != (uchar *)0x0) {
    if ((((param_6 == 0) && (uVar21 == uVar22)) &&
        ((*(byte *)&param_1[3].tlsext_debug_arg & 0x10) != 0)) &&
       (param_1[4].tlsext_debug_cb == (_func_3155 *)0x0)) {
      ssl3_release_read_buffer(param_1);
    }
    *param_7 = puVar16;
    return 1;
  }
LAB_00158d89:
  uVar21 = *(ulong *)&param_1[4].references;
  goto LAB_0015863c;
code_r0x0015867d:
  *(undefined8 *)&param_1[4].references = 0;
LAB_00158688:
  uVar10 = ssl3_get_record();
  if ((int)uVar10 < 1) {
    return uVar10;
  }
  uVar21 = *(ulong *)&param_1[4].references;
  if (uVar21 == 0) {
    ERR_new();
    uVar18 = 0x54c;
    goto LAB_001586b4;
  }
  goto LAB_00158650;
}