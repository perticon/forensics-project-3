int SSL_set_session_ticket_ext(SSL *s,void *ext_data,int ext_len)

{
  BIO *pBVar1;
  
  if (0x300 < s->version) {
    CRYPTO_free(s[4].bbio);
    s[4].bbio = (BIO *)0x0;
    pBVar1 = (BIO *)CRYPTO_malloc(ext_len + 0x10,"ssl/ssl_sess.c",0x45a);
    s[4].bbio = pBVar1;
    if (pBVar1 != (BIO *)0x0) {
      if (ext_data != (void *)0x0) {
        *(short *)&pBVar1->method = (short)ext_len;
        pBVar1->callback = (_func_603 *)&pBVar1->cb_arg;
        memcpy(&pBVar1->cb_arg,ext_data,(long)ext_len);
        return 1;
      }
      pBVar1->callback = (_func_603 *)0x0;
      *(undefined2 *)&pBVar1->method = 0;
      return 1;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_sess.c",0x45c,"SSL_set_session_ticket_ext");
    ERR_set_error(0x14,0xc0100,0);
  }
  return 0;
}