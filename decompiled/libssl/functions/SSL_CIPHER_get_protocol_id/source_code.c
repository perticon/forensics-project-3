uint16_t SSL_CIPHER_get_protocol_id(const SSL_CIPHER *c)
{
    return c->id & 0xFFFF;
}