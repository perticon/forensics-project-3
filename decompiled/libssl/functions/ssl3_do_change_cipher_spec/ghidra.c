ulong ssl3_do_change_cipher_spec(long param_1)

{
  long lVar1;
  int iVar2;
  ulong uVar3;
  
  iVar2 = *(int *)(param_1 + 0x38);
  if (*(long *)(param_1 + 0x318) == 0) {
    lVar1 = *(long *)(param_1 + 0x918);
    if ((lVar1 == 0) || (*(long *)(lVar1 + 8) == 0)) {
      ERR_new();
      ERR_set_debug("ssl/s3_msg.c",0x18,"ssl3_do_change_cipher_spec");
      ERR_set_error(0x14,0x85,0);
      return 0;
    }
    *(undefined8 *)(lVar1 + 0x2f8) = *(undefined8 *)(param_1 + 0x2e0);
    uVar3 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x10))();
    if ((int)uVar3 == 0) {
      return uVar3;
    }
  }
  iVar2 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x20))
                    (param_1,(-(uint)(iVar2 == 0) & 0xfffffff0) + 0x21);
  return (ulong)(iVar2 != 0);
}