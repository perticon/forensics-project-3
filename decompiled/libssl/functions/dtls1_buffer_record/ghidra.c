undefined4 dtls1_buffer_record(long param_1,long param_2,uchar *param_3)

{
  undefined8 uVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  undefined4 uVar9;
  undefined4 uVar10;
  int iVar11;
  undefined4 uVar12;
  undefined4 extraout_var;
  undefined8 *data;
  pitem *item;
  pitem *ppVar13;
  ulong uVar14;
  undefined8 *puVar15;
  byte bVar16;
  
  bVar16 = 0;
  iVar11 = pqueue_size(*(pqueue *)(param_2 + 8));
  uVar12 = 0;
  if (CONCAT44(extraout_var,iVar11) < 100) {
    data = (undefined8 *)CRYPTO_malloc(0x90,"ssl/record/rec_layer_d1.c",0x9b);
    item = pitem_new(param_3,data);
    if ((data == (undefined8 *)0x0) || (item == (pitem *)0x0)) {
      CRYPTO_free(data);
      pitem_free(item);
      ERR_new();
      ERR_set_debug("ssl/record/rec_layer_d1.c",0xa0,"dtls1_buffer_record");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0xffffffff;
    }
    puVar15 = (undefined8 *)(param_1 + 0x12b0U & 0xfffffffffffffff8);
    *data = *(undefined8 *)(param_1 + 0x1ca8);
    data[1] = *(undefined8 *)(param_1 + 0x1cb0);
    uVar12 = *(undefined4 *)(param_1 + 0xc7c);
    uVar9 = *(undefined4 *)(param_1 + 0xc80);
    uVar10 = *(undefined4 *)(param_1 + 0xc84);
    *(undefined4 *)(data + 2) = *(undefined4 *)(param_1 + 0xc78);
    *(undefined4 *)((long)data + 0x14) = uVar12;
    *(undefined4 *)(data + 3) = uVar9;
    *(undefined4 *)((long)data + 0x1c) = uVar10;
    uVar1 = *(undefined8 *)(param_1 + 0xc88);
    uVar2 = *(undefined8 *)(param_1 + 0xc90);
    data[4] = uVar1;
    data[5] = uVar2;
    uVar2 = *(undefined8 *)(param_1 + 0xc98);
    uVar3 = *(undefined8 *)(param_1 + 0xca0);
    data[6] = uVar2;
    data[7] = uVar3;
    uVar3 = *(undefined8 *)(param_1 + 0x12a8);
    uVar4 = *(undefined8 *)(param_1 + 0x12b0);
    data[8] = uVar3;
    data[9] = uVar4;
    uVar4 = *(undefined8 *)(param_1 + 0x12b8);
    uVar5 = *(undefined8 *)(param_1 + 0x12c0);
    data[10] = uVar4;
    data[0xb] = uVar5;
    uVar5 = *(undefined8 *)(param_1 + 0x12c8);
    uVar6 = *(undefined8 *)(param_1 + 0x12d0);
    data[0xc] = uVar5;
    data[0xd] = uVar6;
    uVar6 = *(undefined8 *)(param_1 + 0x12d8);
    uVar7 = *(undefined8 *)(param_1 + 0x12e0);
    data[0xe] = uVar6;
    data[0xf] = uVar7;
    uVar7 = *(undefined8 *)(param_1 + 0x12e8);
    uVar8 = *(undefined8 *)(param_1 + 0x12f0);
    data[0x10] = uVar7;
    data[0x11] = uVar8;
    item->data = data;
    *(undefined8 *)(param_1 + 0x1ca8) = 0;
    *(undefined8 *)(param_1 + 0x1cb0) = 0;
    *(undefined8 *)(param_1 + 0x12a8) = 0;
    *(undefined8 *)(param_1 + 0x1ca0) = 0;
    *(undefined (*) [16])(param_1 + 0xc78) = (undefined  [16])0x0;
    *(undefined (*) [16])(param_1 + 0xc88) = (undefined  [16])0x0;
    *(undefined (*) [16])(param_1 + 0xc98) = (undefined  [16])0x0;
    uVar14 = (ulong)(((int)param_1 - (int)puVar15) + 0x1ca8U >> 3);
    for (; uVar14 != 0; uVar14 = uVar14 - 1) {
      *puVar15 = 0;
      puVar15 = puVar15 + (ulong)bVar16 * -2 + 1;
    }
    iVar11 = ssl3_setup_buffers(0,uVar7,uVar1,uVar2,uVar3,uVar4,uVar5,uVar6,param_1);
    if (iVar11 == 0) {
      CRYPTO_free((void *)data[2]);
      CRYPTO_free(data);
      pitem_free(item);
      return 0xffffffff;
    }
    ppVar13 = pqueue_insert(*(pqueue *)(param_2 + 8),item);
    uVar12 = 1;
    if (ppVar13 == (pitem *)0x0) {
      CRYPTO_free((void *)data[2]);
      CRYPTO_free(data);
      pitem_free(item);
      uVar12 = 1;
    }
  }
  return uVar12;
}