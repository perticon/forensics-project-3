uint tls1_shared_group(long param_1,uint param_2)

{
  byte bVar1;
  ushort uVar2;
  uint uVar3;
  int iVar4;
  uint uVar5;
  long lVar6;
  uint uVar7;
  ulong uVar8;
  long in_FS_OFFSET;
  long local_50;
  long local_48;
  ulong local_40;
  long local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar5 = 0;
  if (*(int *)(param_1 + 0x38) == 0) goto LAB_00148755;
  if (param_2 == 0xfffffffe) {
    if ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) == 0) {
      param_2 = 0;
      bVar1 = *(byte *)(param_1 + 0x9ea);
      goto joined_r0x00148807;
    }
    iVar4 = *(int *)(*(long *)(param_1 + 0x2e0) + 0x18);
    if (iVar4 == 0x300c02b) {
      uVar5 = 0x17;
      goto LAB_00148755;
    }
    uVar5 = 0x18;
    if (iVar4 == 0x300c02c) goto LAB_00148755;
  }
  else {
    bVar1 = *(byte *)(param_1 + 0x9ea);
joined_r0x00148807:
    if ((bVar1 & 0x40) == 0) {
      local_50 = *(long *)(param_1 + 0xad8);
      local_40 = *(ulong *)(param_1 + 0xad0);
      tls1_get_supported_groups(param_1,&local_48,&local_38);
    }
    else {
      tls1_get_supported_groups(param_1,&local_50,&local_40);
      local_38 = *(long *)(param_1 + 0xad0);
      local_48 = *(long *)(param_1 + 0xad8);
    }
    uVar7 = 0;
    if (local_40 != 0) {
      uVar8 = 0;
      do {
        uVar2 = *(ushort *)(local_50 + uVar8 * 2);
        uVar5 = (uint)uVar2;
        uVar3 = uVar5;
        if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
          iVar4 = **(int **)(param_1 + 8);
          if ((iVar4 == 0x10000) || (iVar4 < 0x304)) {
            uVar3 = (uint)uVar2;
          }
          else if ((*(byte *)(param_1 + 0x9ea) & 0x40) == 0) {
            uVar5 = ssl_group_id_tls13_to_internal(uVar5);
            uVar3 = uVar5;
          }
          else {
            uVar3 = ssl_group_id_internal_to_tls13();
          }
        }
        if (local_38 != 0) {
          lVar6 = 0;
          do {
            if (*(short *)(local_48 + lVar6 * 2) == (short)uVar3) {
              iVar4 = tls_group_allowed(param_1,uVar5 & 0xffff,0x20005);
              if (iVar4 != 0) {
                if (param_2 == uVar7) goto LAB_00148755;
                uVar7 = uVar7 + 1;
              }
              break;
            }
            lVar6 = lVar6 + 1;
          } while (local_38 != lVar6);
        }
        uVar8 = uVar8 + 1;
      } while (uVar8 < local_40);
    }
    uVar5 = uVar7;
    if (param_2 == 0xffffffff) goto LAB_00148755;
  }
  uVar5 = 0;
LAB_00148755:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}