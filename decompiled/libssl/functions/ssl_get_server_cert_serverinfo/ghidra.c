undefined8 ssl_get_server_cert_serverinfo(long param_1,long *param_2,undefined8 *param_3)

{
  long lVar1;
  undefined8 uVar2;
  
  lVar1 = *(long *)(param_1 + 0x388);
  *param_3 = 0;
  if ((lVar1 != 0) && (*(long *)(lVar1 + 0x18) != 0)) {
    uVar2 = *(undefined8 *)(lVar1 + 0x20);
    *param_2 = *(long *)(lVar1 + 0x18);
    *param_3 = uVar2;
    return 1;
  }
  return 0;
}