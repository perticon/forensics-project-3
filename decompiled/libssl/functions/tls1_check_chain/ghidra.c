uint tls1_check_chain(SSL *param_1,X509 *param_2,long param_3,long param_4,int param_5)

{
  dtls1_state_st *pdVar1;
  long lVar2;
  void *pvVar3;
  uint uVar4;
  int iVar5;
  int iVar6;
  int iVar7;
  long lVar8;
  undefined8 uVar9;
  char *pcVar10;
  X509_NAME *pXVar11;
  X509 *a;
  X509_NAME *pXVar12;
  undefined4 uVar13;
  char *pcVar14;
  uchar *puVar15;
  long lVar16;
  char cVar17;
  long lVar18;
  uint uVar19;
  uint uVar20;
  uint uVar21;
  long in_FS_OFFSET;
  long local_70;
  uint local_68;
  uchar *local_60;
  int local_48;
  
  pdVar1 = param_1[3].d1;
  lVar2 = *(long *)(in_FS_OFFSET + 0x28);
  uVar19 = *(uint *)(pdVar1->cookie + 0x18) & 0x30000;
  if (param_5 == -1) {
    if (((param_2 != (X509 *)0x0) && (param_3 != 0)) &&
       (lVar8 = ssl_cert_lookup_by_pkey(param_3), lVar8 != 0)) {
      local_60 = param_1[1].sid_ctx + (long)local_48 * 4 + -4;
      uVar21 = *(uint *)(pdVar1->cookie + 0x18) & 0x30001;
      uVar20 = (-(uint)(uVar21 == 0) & 0xfffff960) + 0xef0;
      uVar21 = (-(uint)(uVar21 == 0) & 0xfffff960) + 0x6f0;
      local_70 = param_3;
      if (uVar19 == 0) {
        local_68 = 1;
LAB_0014c889:
        iVar5 = SSL_version(param_1);
        uVar20 = uVar19;
        if (((iVar5 >> 8 == 3) && (iVar5 = SSL_version(param_1), local_68 != 0)) && (0x302 < iVar5))
        goto LAB_0014cbf1;
        if (uVar21 != 0) {
          uVar19 = uVar19 | 0x30;
LAB_0014ca5e:
          iVar5 = tls1_check_cert_param(param_1,param_2,1);
          if (iVar5 != 0) goto LAB_0014c8c4;
          uVar20 = uVar19;
          if (param_1->server == 0) goto LAB_0014c8d5;
          goto LAB_0014ca83;
        }
        iVar5 = tls1_check_cert_param(param_1,param_2,1);
        if (iVar5 != 0) goto LAB_0014c8c4;
      }
      else {
        iVar5 = X509_chain_check_suiteb(0,param_2,param_4);
        uVar21 = uVar20;
        if (iVar5 == 0) {
          local_68 = 1;
          uVar19 = 0x800;
          goto LAB_0014c889;
        }
        iVar5 = SSL_version(param_1);
        if ((iVar5 >> 8 != 3) || (iVar5 = SSL_version(param_1), iVar5 < 0x303)) {
          local_68 = 1;
          uVar19 = 0x30;
          goto LAB_0014ca5e;
        }
        local_68 = 1;
        uVar20 = 0;
LAB_0014cbf1:
        uVar13 = 0;
        if ((param_1[1].enc_write_ctx == (EVP_CIPHER_CTX *)0x0) &&
           (param_1[1].expand == (COMP_CTX *)0x0)) {
          switch(local_48) {
          case 0:
            iVar5 = 6;
            uVar13 = 0x41;
            break;
          default:
            if (((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
                (iVar5 = param_1->method->version, 0x303 < iVar5)) &&
               ((iVar5 != 0x10000 && (lVar8 = find_sig_alg(param_1,param_2,local_70), lVar8 == 0))))
            {
              uVar19 = uVar20 | 0x20;
            }
            else {
              uVar19 = uVar20 | 0x30;
            }
            for (iVar5 = 0; iVar6 = OPENSSL_sk_num(param_4), iVar5 < iVar6; iVar5 = iVar5 + 1) {
              OPENSSL_sk_value(param_4,iVar5);
            }
            goto LAB_0014cc9f;
          case 2:
            iVar5 = 0x74;
            uVar13 = 0x71;
            break;
          case 3:
            iVar5 = 0x198;
            uVar13 = 0x1a0;
            break;
          case 4:
            iVar5 = 0x32b;
            uVar13 = 0x327;
            break;
          case 5:
            iVar5 = 0x3d3;
            uVar13 = 0x3d9;
            break;
          case 6:
            iVar5 = 0x3d4;
            uVar13 = 0x3da;
          }
          if (*(long *)(pdVar1->rcvd_cookie + 0x94) != 0) {
            if (*(long *)(pdVar1->rcvd_cookie + 0x9c) != 0) {
              lVar18 = 0;
              lVar8 = *(long *)(*(long *)&param_1[3].ex_data.dummy + 0x620);
              do {
                lVar16 = lVar8;
                do {
                  if (*(short *)(*(long *)(pdVar1->rcvd_cookie + 0x94) + lVar18 * 2) ==
                      *(short *)(lVar16 + 8)) {
                    if (((*(int *)(lVar16 + 0x24) != 0) && (*(int *)(lVar16 + 0xc) == 0x40)) &&
                       (*(int *)(lVar16 + 0x14) == iVar5)) goto LAB_0014cc01;
                    break;
                  }
                  lVar16 = lVar16 + 0x28;
                } while (lVar16 != lVar8 + 0x4d8);
                lVar18 = lVar18 + 1;
              } while (lVar18 != *(long *)(pdVar1->rcvd_cookie + 0x9c));
            }
            uVar19 = uVar20;
            if (uVar21 != 0) goto LAB_0014ca5e;
            goto LAB_0014c8f9;
          }
        }
LAB_0014cc01:
        if (((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
            (iVar5 = param_1->method->version, iVar5 != 0x10000)) && (0x303 < iVar5)) {
          lVar8 = find_sig_alg(param_1,param_2,local_70);
          if (lVar8 == 0) goto LAB_0014cc4c;
          uVar19 = uVar20 | 0x30;
          uVar4 = uVar20 | 0x10;
        }
        else {
          iVar5 = tls1_check_sig_alg_part_0(param_1,param_2,uVar13);
          if (iVar5 == 0) {
            if (uVar21 == 0) goto LAB_0014c8f9;
          }
          else {
            uVar20 = uVar20 | 0x10;
          }
LAB_0014cc4c:
          uVar19 = uVar20 | 0x20;
          uVar4 = uVar20;
        }
        for (iVar5 = 0; iVar6 = OPENSSL_sk_num(param_4), iVar5 < iVar6; iVar5 = iVar5 + 1) {
          uVar9 = OPENSSL_sk_value(param_4,iVar5);
          iVar6 = tls1_check_sig_alg_part_0(param_1,uVar9,uVar13);
          if (iVar6 == 0) {
            uVar20 = uVar19;
            uVar19 = uVar4;
            if (uVar21 == 0) goto LAB_0014c8f9;
            goto LAB_0014ca5e;
          }
        }
LAB_0014cc9f:
        iVar5 = tls1_check_cert_param(param_1,param_2,1);
        if (iVar5 == 0) {
          uVar20 = uVar19;
          if (uVar21 == 0) goto LAB_0014c8f9;
          if (param_1->server == 0) {
            uVar20 = uVar19 | 0x80;
            goto LAB_0014ccd8;
          }
LAB_0014ca8f:
          uVar20 = uVar19 | 0x80;
          for (iVar5 = 0; iVar6 = OPENSSL_sk_num(param_4), iVar5 < iVar6; iVar5 = iVar5 + 1) {
            uVar9 = OPENSSL_sk_value(param_4,iVar5);
            iVar6 = tls1_check_cert_param(param_1,uVar9,0);
            if (iVar6 == 0) {
              if (uVar21 == 0) goto LAB_0014c8f9;
              uVar20 = uVar19;
              if (param_1->server == 0) goto LAB_0014ccd8;
              uVar20 = uVar19 | 0x600;
              goto LAB_0014cbb0;
            }
          }
          if (param_1->server != 0) goto LAB_0014c8e5;
LAB_0014ccd8:
          iVar5 = EVP_PKEY_is_a(local_70,&DAT_00183541);
          cVar17 = '\x01';
          if (iVar5 == 0) {
            iVar5 = EVP_PKEY_is_a(local_70,&DAT_00183553);
            cVar17 = '\x02';
            if (iVar5 != 0) goto LAB_0014ccf6;
            iVar5 = EVP_PKEY_is_a(local_70,&DAT_00188294);
            if (iVar5 != 0) {
              cVar17 = '@';
              goto LAB_0014ccf6;
            }
LAB_0014cd2c:
            uVar20 = uVar20 | 0x400;
          }
          else {
LAB_0014ccf6:
            pcVar10 = *(char **)&param_1[1].state;
            if (param_1[1].init_buf != (BUF_MEM *)0x0) {
              pcVar14 = pcVar10 + (long)&(param_1[1].init_buf)->length;
              do {
                if (*pcVar10 == cVar17) goto LAB_0014cd2c;
                pcVar10 = pcVar10 + 1;
              } while (pcVar10 != pcVar14);
            }
            if (uVar21 == 0) {
              uVar21 = 0;
              goto LAB_0014c8f9;
            }
          }
          pvVar3 = param_1[1].init_msg;
          if ((pvVar3 == (void *)0x0) || (iVar5 = OPENSSL_sk_num(pvVar3), iVar5 == 0)) {
LAB_0014cd50:
            uVar20 = uVar20 | 0x200;
            goto LAB_0014c8ec;
          }
          pXVar11 = X509_get_issuer_name(param_2);
          for (iVar5 = 0; iVar6 = OPENSSL_sk_num(pvVar3), iVar5 < iVar6; iVar5 = iVar5 + 1) {
            pXVar12 = (X509_NAME *)OPENSSL_sk_value(pvVar3,iVar5);
            iVar6 = X509_NAME_cmp(pXVar11,pXVar12);
            if (iVar6 == 0) goto LAB_0014cd50;
          }
          for (iVar5 = 0; iVar6 = OPENSSL_sk_num(param_4), iVar5 < iVar6; iVar5 = iVar5 + 1) {
            a = (X509 *)OPENSSL_sk_value(param_4,iVar5);
            pXVar11 = X509_get_issuer_name(a);
            for (iVar6 = 0; iVar7 = OPENSSL_sk_num(pvVar3), iVar6 < iVar7; iVar6 = iVar6 + 1) {
              pXVar12 = (X509_NAME *)OPENSSL_sk_value(pvVar3,iVar6);
              iVar7 = X509_NAME_cmp(pXVar11,pXVar12);
              if (iVar7 == 0) goto LAB_0014cd50;
            }
          }
          if (uVar21 == 0) goto LAB_0014c8f9;
LAB_0014cbb0:
          if ((uVar21 & uVar20) != uVar21) goto LAB_0014c8f9;
        }
        else {
LAB_0014c8c4:
          uVar19 = uVar19 | 0x40;
          uVar20 = uVar19;
          if (param_1->server == 0) {
LAB_0014c8d5:
            uVar20 = uVar19 | 0x80;
            if (local_68 != 0) {
              uVar20 = uVar19 | 0x80;
              goto LAB_0014ccd8;
            }
          }
          else {
LAB_0014ca83:
            uVar19 = uVar20;
            if (local_68 != 0) goto LAB_0014ca8f;
          }
LAB_0014c8e5:
          uVar20 = uVar20 | 0x600;
LAB_0014c8ec:
          if (uVar21 != 0) goto LAB_0014cbb0;
        }
        uVar20 = uVar20 | 1;
      }
LAB_0014c8f9:
      iVar5 = SSL_version(param_1);
      uVar19 = uVar20;
      if (iVar5 >> 8 == 3) goto LAB_0014cb57;
      goto LAB_0014c90d;
    }
  }
  else {
    if (param_5 == -2) {
      puVar15 = *(uchar **)pdVar1;
      local_48 = (int)((long)puVar15 - (long)(pdVar1->cookie + 0x1c) >> 3) * -0x33333333;
      lVar8 = (long)local_48;
    }
    else {
      lVar8 = (long)param_5;
      puVar15 = pdVar1->cookie + 0x1c + lVar8 * 0x28;
      local_48 = param_5;
    }
    local_60 = param_1[1].sid_ctx + lVar8 * 4 + -4;
    param_2 = *(X509 **)puVar15;
    local_70 = *(long *)(puVar15 + 8);
    local_68 = *(uint *)(pdVar1->cookie + 0x18) & 0x30001;
    if ((param_2 != (X509 *)0x0) && (local_70 != 0)) {
      param_4 = *(long *)(puVar15 + 0x10);
      uVar21 = 0;
      if (uVar19 != 0) {
        uVar19 = 0x800;
        uVar21 = X509_chain_check_suiteb(0,param_2,param_4);
        if (uVar21 != 0) goto LAB_0014cb3d;
      }
      goto LAB_0014c889;
    }
LAB_0014cb3d:
    uVar21 = 0;
    uVar20 = 0;
    iVar5 = SSL_version(param_1);
    uVar19 = 0;
    if (iVar5 >> 8 == 3) {
LAB_0014cb57:
      uVar20 = uVar19;
      iVar5 = SSL_version(param_1);
      if (iVar5 < 0x303) goto LAB_0014c90d;
      uVar20 = *(uint *)local_60 & 0x102 | uVar20;
    }
    else {
LAB_0014c90d:
      uVar20 = uVar20 | 0x102;
    }
    if (uVar21 != 0) goto LAB_0014c929;
    if ((uVar20 & 1) != 0) {
      *(uint *)local_60 = uVar20;
      goto LAB_0014c929;
    }
    *(uint *)local_60 = *(uint *)local_60 & 0x102;
  }
  uVar20 = 0;
LAB_0014c929:
  if (lVar2 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar20;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}