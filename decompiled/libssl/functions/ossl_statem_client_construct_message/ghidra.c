undefined8 ossl_statem_client_construct_message(long param_1,code **param_2,undefined4 *param_3)

{
  code *pcVar1;
  
  switch(*(undefined4 *)(param_1 + 0x5c)) {
  case 0xc:
    *param_2 = tls_construct_client_hello;
    *param_3 = 1;
    return 1;
  case 0xd:
    *param_2 = tls_construct_client_certificate;
    *param_3 = 0xb;
    return 1;
  case 0xe:
    *param_2 = tls_construct_client_key_exchange;
    *param_3 = 0x10;
    return 1;
  case 0xf:
    *param_2 = tls_construct_cert_verify;
    *param_3 = 0xf;
    return 1;
  case 0x10:
    break;
  case 0x11:
    *param_2 = tls_construct_next_proto;
    *param_3 = 0x43;
    return 1;
  case 0x12:
    *param_2 = tls_construct_finished;
    *param_3 = 0x14;
    return 1;
  default:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x37a,"ossl_statem_client_construct_message");
    ossl_statem_fatal(param_1,0x50,0xec,0);
    return 0;
  case 0x2b:
    *param_2 = tls_construct_key_update;
    *param_3 = 0x18;
    return 1;
  case 0x2f:
    *param_2 = (code *)0x0;
    *param_3 = 0xffffffff;
    return 1;
  case 0x30:
    *param_2 = tls_construct_end_of_early_data;
    *param_3 = 5;
    return 1;
  }
  if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
    pcVar1 = tls_construct_change_cipher_spec;
  }
  else {
    pcVar1 = dtls_construct_change_cipher_spec;
  }
  *param_2 = pcVar1;
  *param_3 = 0x101;
  return 1;
}