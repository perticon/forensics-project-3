SSL * ssl_ctrl(SSL *param_1,int param_2,SSL *param_3,SSL *param_4)

{
  undefined4 uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  undefined4 uVar8;
  undefined4 uVar9;
  int iVar10;
  SSL **ppSVar11;
  BIO *append;
  long lVar12;
  BIO *b;
  SSL *pSVar13;
  SSL **ppSVar14;
  SSL *pSVar15;
  
  ppSVar11 = (SSL **)BIO_get_data();
  append = BIO_next((BIO *)param_1);
  pSVar13 = *ppSVar11;
  if ((pSVar13 == (SSL *)0x0) && (param_2 != 0x6d)) goto switchD_00123a61_caseD_3;
  switch(param_2) {
  default:
    append = pSVar13->rbio;
    goto LAB_00123bf7;
  case 1:
    SSL_shutdown(pSVar13);
    if ((_func_3058 *)pSVar13->handshake_func == pSVar13->method->ssl_read) {
      SSL_set_connect_state(pSVar13);
    }
    else if (pSVar13->handshake_func == (_func_3149 *)pSVar13->method->ssl_connect) {
      SSL_set_accept_state(pSVar13);
    }
    iVar10 = SSL_clear(pSVar13);
    if (iVar10 != 0) {
      if (append != (BIO *)0x0) {
        param_2 = 1;
        goto LAB_00123bf7;
      }
      append = pSVar13->rbio;
      if (append != (BIO *)0x0) {
        param_2 = 1;
        goto LAB_00123bf7;
      }
      goto LAB_00123ab0;
    }
  case 3:
  case 0xe:
switchD_00123a61_caseD_3:
    pSVar15 = (SSL *)0x0;
    break;
  case 6:
    if ((append != (BIO *)0x0) && (pSVar13->rbio != append)) {
      BIO_up_ref(append);
      SSL_set_bio(pSVar13,append,append);
      return (SSL *)0x1;
    }
    goto LAB_00123ab0;
  case 7:
    if (param_1 == param_4) {
      SSL_set_bio(pSVar13,(BIO *)0x0,(BIO *)0x0);
      return (SSL *)0x1;
    }
    goto LAB_00123ab0;
  case 8:
    iVar10 = BIO_get_shutdown(param_1);
    pSVar15 = (SSL *)(long)iVar10;
    break;
  case 9:
    pSVar15 = (SSL *)0x1;
    BIO_set_shutdown(param_1,(ulong)param_3 & 0xffffffff);
    break;
  case 10:
    iVar10 = SSL_pending(pSVar13);
    pSVar15 = (SSL *)(long)iVar10;
    if (pSVar15 == (SSL *)0x0) {
      lVar12 = BIO_ctrl(pSVar13->rbio,10,0,(void *)0x0);
      pSVar15 = (SSL *)(long)(int)lVar12;
    }
    break;
  case 0xb:
    BIO_clear_flags((BIO *)param_1,0xf);
    pSVar15 = (SSL *)BIO_ctrl(pSVar13->wbio,0xb,(long)param_3,param_4);
    BIO_copy_next_retry((BIO *)param_1);
    break;
  case 0xc:
    ppSVar14 = (SSL **)BIO_get_data(param_4);
    SSL_free(*ppSVar14);
    pSVar13 = SSL_dup(pSVar13);
    uVar1 = *(undefined4 *)(ppSVar11 + 1);
    uVar2 = *(undefined4 *)(ppSVar11 + 2);
    uVar3 = *(undefined4 *)((long)ppSVar11 + 0x14);
    uVar4 = *(undefined4 *)(ppSVar11 + 3);
    uVar5 = *(undefined4 *)((long)ppSVar11 + 0x1c);
    uVar6 = *(undefined4 *)(ppSVar11 + 4);
    uVar7 = *(undefined4 *)((long)ppSVar11 + 0x24);
    uVar8 = *(undefined4 *)(ppSVar11 + 5);
    uVar9 = *(undefined4 *)((long)ppSVar11 + 0x2c);
    *ppSVar14 = pSVar13;
    *(undefined4 *)(ppSVar14 + 1) = uVar1;
    pSVar15 = (SSL *)(ulong)(pSVar13 != (SSL *)0x0);
    *(undefined4 *)(ppSVar14 + 4) = uVar6;
    *(undefined4 *)((long)ppSVar14 + 0x24) = uVar7;
    *(undefined4 *)(ppSVar14 + 5) = uVar8;
    *(undefined4 *)((long)ppSVar14 + 0x2c) = uVar9;
    *(undefined4 *)(ppSVar14 + 2) = uVar2;
    *(undefined4 *)((long)ppSVar14 + 0x14) = uVar3;
    *(undefined4 *)(ppSVar14 + 3) = uVar4;
    *(undefined4 *)((long)ppSVar14 + 0x1c) = uVar5;
    break;
  case 0xd:
    append = pSVar13->wbio;
    param_2 = 0xd;
    goto LAB_00123bf7;
  case 0x65:
    BIO_clear_flags((BIO *)param_1,0xf);
    BIO_set_retry_reason(param_1,0);
    iVar10 = SSL_do_handshake(pSVar13);
    pSVar15 = (SSL *)(long)iVar10;
    iVar10 = SSL_get_error(pSVar13,iVar10);
    if (iVar10 == 4) {
      BIO_set_flags((BIO *)param_1,0xc);
      BIO_set_retry_reason(param_1,1);
    }
    else if (iVar10 < 5) {
      if (iVar10 == 2) {
        BIO_set_flags((BIO *)param_1,9);
      }
      else if (iVar10 == 3) {
        BIO_set_flags((BIO *)param_1,10);
      }
    }
    else if (iVar10 == 7) {
      BIO_set_flags((BIO *)param_1,0xc);
      iVar10 = BIO_get_retry_reason(append);
      BIO_set_retry_reason(param_1,iVar10);
    }
    break;
  case 0x69:
    append = pSVar13->rbio;
    param_2 = 0x69;
LAB_00123bf7:
    pSVar13 = (SSL *)BIO_ctrl(append,param_2,(long)param_3,param_4);
    return pSVar13;
  case 0x6d:
    if (pSVar13 != (SSL *)0x0) {
      if (param_1 != (SSL *)0x0) {
        ppSVar11 = (SSL **)BIO_get_data(param_1);
        iVar10 = BIO_get_shutdown(param_1);
        if (iVar10 != 0) {
          if (*ppSVar11 != (SSL *)0x0) {
            SSL_shutdown(*ppSVar11);
          }
          iVar10 = BIO_get_init(param_1);
          if (iVar10 != 0) {
            SSL_free(*ppSVar11);
          }
          BIO_clear_flags((BIO *)param_1,-1);
          BIO_set_init(param_1,0);
        }
        CRYPTO_free(ppSVar11);
      }
      lVar12 = CRYPTO_zalloc(0x30,"ssl/bio_ssl.c",0x3a);
      if (lVar12 == 0) {
        ERR_new();
        ERR_set_debug("ssl/bio_ssl.c",0x3d,"ssl_new");
        ERR_set_error(0x20,0xc0100,0);
        return (SSL *)0x0;
      }
      BIO_set_init(param_1,0);
      BIO_set_data(param_1,lVar12);
      BIO_clear_flags((BIO *)param_1,-1);
      ppSVar11 = (SSL **)BIO_get_data(param_1);
    }
    BIO_set_shutdown(param_1,(ulong)param_3 & 0xffffffff);
    *ppSVar11 = param_4;
    b = SSL_get_rbio(param_4);
    if (b != (BIO *)0x0) {
      if (append != (BIO *)0x0) {
        BIO_push(b,append);
      }
      BIO_set_next(param_1,b);
      BIO_up_ref(b);
    }
    pSVar15 = (SSL *)0x1;
    BIO_set_init(param_1,1);
    break;
  case 0x6e:
    if (param_4 != (SSL *)0x0) {
      *(SSL **)param_4 = pSVar13;
      return (SSL *)0x1;
    }
    goto switchD_00123a61_caseD_3;
  case 0x77:
    if (param_3 == (SSL *)0x0) {
      SSL_set_accept_state(pSVar13);
      return (SSL *)0x1;
    }
    SSL_set_connect_state(pSVar13);
LAB_00123ab0:
    pSVar15 = (SSL *)0x1;
    break;
  case 0x7d:
    pSVar15 = ppSVar11[2];
    if (0x1ff < (long)param_3) {
      ppSVar11[2] = param_3;
    }
    break;
  case 0x7e:
    pSVar15 = (SSL *)(long)*(int *)(ppSVar11 + 1);
    break;
  case 0x7f:
    pSVar15 = ppSVar11[4];
    if ((long)param_3 < 0x3c) {
      param_3 = (SSL *)0x5;
    }
    ppSVar11[4] = param_3;
    pSVar13 = (SSL *)time((time_t *)0x0);
    ppSVar11[5] = pSVar13;
  }
  return pSVar15;
}