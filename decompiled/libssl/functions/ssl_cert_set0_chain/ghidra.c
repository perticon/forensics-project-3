undefined8 ssl_cert_set0_chain(long param_1,long param_2,undefined8 param_3)

{
  long lVar1;
  int iVar2;
  undefined8 uVar3;
  int iVar4;
  
  if (param_1 == 0) {
    lVar1 = **(long **)(param_2 + 0x158);
  }
  else {
    lVar1 = **(long **)(param_1 + 0x898);
  }
  if (lVar1 != 0) {
    iVar4 = 0;
    while( true ) {
      iVar2 = OPENSSL_sk_num(param_3);
      if (iVar2 <= iVar4) {
        OSSL_STACK_OF_X509_free(*(undefined8 *)(lVar1 + 0x10));
        *(undefined8 *)(lVar1 + 0x10) = param_3;
        return 1;
      }
      uVar3 = OPENSSL_sk_value(param_3,iVar4);
      iVar2 = ssl_security_cert(param_1,param_2,uVar3,0,0);
      if (iVar2 != 1) break;
      iVar4 = iVar4 + 1;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_cert.c",0x107,"ssl_cert_set0_chain");
    ERR_set_error(0x14,iVar2,0);
    return 0;
  }
  return 0;
}