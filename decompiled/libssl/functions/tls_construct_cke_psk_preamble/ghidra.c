int tls_construct_cke_psk_preamble(long param_1,undefined8 param_2)

{
  code *pcVar1;
  undefined8 uVar2;
  uint uVar3;
  uint uVar4;
  int iVar5;
  char *pcVar6;
  long lVar7;
  undefined8 *puVar8;
  undefined8 *puVar9;
  long lVar10;
  size_t len;
  long in_FS_OFFSET;
  bool bVar11;
  undefined8 local_358 [34];
  undefined local_248 [520];
  long local_40;
  
  pcVar1 = *(code **)(param_1 + 0x988);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (pcVar1 == (code *)0x0) {
    ERR_new();
    lVar7 = 0;
    lVar10 = 0;
    len = 0;
    iVar5 = 0;
    ERR_set_debug("ssl/statem/statem_clnt.c",0xaca,"tls_construct_cke_psk_preamble");
    ossl_statem_fatal(param_1,0x50,0xe0,0);
    pcVar6 = (char *)0x0;
  }
  else {
    puVar9 = local_358;
    for (lVar7 = 0x20; lVar7 != 0; lVar7 = lVar7 + -1) {
      *puVar9 = 0;
      puVar9 = puVar9 + 1;
    }
    uVar2 = *(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2a0);
    *(undefined *)puVar9 = 0;
    uVar3 = (*pcVar1)(param_1,uVar2,local_358,0x100);
    len = (size_t)uVar3;
    if (uVar3 < 0x201) {
      puVar9 = local_358;
      if (len == 0) {
        lVar10 = 0;
        lVar7 = 0;
        iVar5 = 0;
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0xad9,"tls_construct_cke_psk_preamble");
        ossl_statem_fatal(param_1,0x28,0xdf,0);
        pcVar6 = (char *)0x0;
      }
      else {
        do {
          puVar8 = puVar9;
          uVar4 = *(uint *)puVar8 + 0xfefefeff & ~*(uint *)puVar8;
          uVar3 = uVar4 & 0x80808080;
          puVar9 = (undefined8 *)((long)puVar8 + 4);
        } while (uVar3 == 0);
        bVar11 = (uVar4 & 0x8080) == 0;
        if (bVar11) {
          uVar3 = uVar3 >> 0x10;
        }
        if (bVar11) {
          puVar9 = (undefined8 *)((long)puVar8 + 6);
        }
        lVar7 = CRYPTO_memdup(local_248,len,"ssl/statem/statem_clnt.c",0xae3);
        lVar10 = (long)puVar9 + ((-3 - (ulong)CARRY1((byte)uVar3,(byte)uVar3)) - (long)local_358);
        pcVar6 = CRYPTO_strdup((char *)local_358,"ssl/statem/statem_clnt.c",0xae4);
        if ((lVar7 == 0) || (pcVar6 == (char *)0x0)) {
          ERR_new();
          iVar5 = 0;
          ERR_set_debug("ssl/statem/statem_clnt.c",0xae6,"tls_construct_cke_psk_preamble");
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
        }
        else {
          CRYPTO_free(*(void **)(param_1 + 0x370));
          *(long *)(param_1 + 0x370) = lVar7;
          *(size_t *)(param_1 + 0x378) = len;
          CRYPTO_free(*(void **)(*(long *)(param_1 + 0x918) + 0x2a8));
          *(char **)(*(long *)(param_1 + 0x918) + 0x2a8) = pcVar6;
          iVar5 = WPACKET_sub_memcpy__(param_2,local_358,lVar10,2);
          if (iVar5 == 0) {
            lVar7 = 0;
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0xaf3,"tls_construct_cke_psk_preamble");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            pcVar6 = (char *)0x0;
          }
          else {
            pcVar6 = (char *)0x0;
            lVar7 = 0;
            iVar5 = 1;
          }
        }
      }
    }
    else {
      ERR_new();
      lVar7 = 0;
      lVar10 = 0;
      len = 0x200;
      iVar5 = 0;
      ERR_set_debug("ssl/statem/statem_clnt.c",0xad5,"tls_construct_cke_psk_preamble");
      ossl_statem_fatal(param_1,0x28,0xc0103,0);
      pcVar6 = (char *)0x0;
    }
  }
  OPENSSL_cleanse(local_248,len);
  OPENSSL_cleanse(local_358,0x101);
  CRYPTO_clear_free(lVar7,len,"ssl/statem/statem_clnt.c",0xafc);
  CRYPTO_clear_free(pcVar6,lVar10,"ssl/statem/statem_clnt.c",0xafd);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}