int32_t tls_construct_cke_psk_preamble(void** rdi, void** rsi) {
    void** r12_3;
    void*** rsp4;
    int64_t r10_5;
    uint64_t rax6;
    uint64_t v7;
    void* rsp8;
    void** rbx9;
    void** rbp10;
    void** r15_11;
    void** r13_12;
    int32_t r14d13;
    void** r9_14;
    void** r8_15;
    int64_t rcx16;
    void** r14_17;
    void** r8_18;
    void** rdx19;
    void** rax20;
    void** rsi21;
    uint32_t eax22;
    void** r8_23;
    uint64_t rax24;
    void** rbp25;
    int64_t rdx26;
    void** v27;
    uint32_t eax28;
    int1_t zf29;
    uint32_t ebx30;
    void** rax31;
    void** rax32;
    int32_t eax33;

    r12_3 = rdi;
    rsp4 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x338);
    r10_5 = *reinterpret_cast<int64_t*>(rdi + 0x988);
    rax6 = g28;
    v7 = rax6;
    if (!r10_5) {
        fun_20230();
        rsp8 = reinterpret_cast<void*>(rsp4 - 8 + 8);
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp10) = 0;
        *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r15_11) = 0;
        *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        r13_12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 16);
        r14d13 = 0;
        fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
        ossl_statem_fatal(r12_3, r12_3);
        *reinterpret_cast<int32_t*>(&r9_14) = 0;
        *reinterpret_cast<int32_t*>(&r9_14 + 4) = 0;
        r8_15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 + 8 + 0x120);
    } else {
        r13_12 = reinterpret_cast<void**>(rsp4 + 16);
        *reinterpret_cast<int32_t*>(&rcx16) = 32;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
        r14_17 = rsi;
        r8_18 = reinterpret_cast<void**>(rsp4 + 0x120);
        rdx19 = r13_12;
        while (rcx16) {
            --rcx16;
        }
        rax20 = *reinterpret_cast<void***>(r12_3 + 0x918);
        rsi21 = *reinterpret_cast<void***>(rax20 + 0x2a0);
        eax22 = reinterpret_cast<uint32_t>(r10_5(r12_3, rsi21, rdx19, 0x100));
        r8_23 = r8_18;
        *reinterpret_cast<uint32_t*>(&r15_11) = eax22;
        *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        if (eax22 > 0x200) 
            goto addr_6bf70_7; else 
            goto addr_6bce0_8;
    }
    addr_6bdff_9:
    fun_20720(r8_15, r15_11, r8_15, r15_11);
    fun_20720(r13_12, 0x101, r13_12, 0x101);
    fun_21a00(rbx9, r15_11, "ssl/statem/statem_clnt.c", 0xafc);
    fun_21a00(r9_14, rbp10, "ssl/statem/statem_clnt.c", 0xafd);
    rax24 = v7 ^ g28;
    if (rax24) {
        fun_20ff0();
    } else {
        return r14d13;
    }
    addr_6bf70_7:
    fun_20230();
    *reinterpret_cast<int32_t*>(&rbx9) = 0;
    *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp10) = 0;
    *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r15_11) = 0x200;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    r14d13 = 0;
    fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
    ossl_statem_fatal(r12_3, r12_3);
    r8_15 = r8_18;
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(&r9_14 + 4) = 0;
    goto addr_6bdff_9;
    addr_6bce0_8:
    rbp25 = r13_12;
    if (!r15_11) {
        *reinterpret_cast<int32_t*>(&rbp10) = 0;
        *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
        r14d13 = 0;
        fun_20230();
        fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
        ossl_statem_fatal(r12_3, r12_3);
        r8_15 = r8_23;
        *reinterpret_cast<int32_t*>(&r9_14) = 0;
        *reinterpret_cast<int32_t*>(&r9_14 + 4) = 0;
        goto addr_6bdff_9;
    } else {
        do {
            *reinterpret_cast<void***>(&rdx26) = v27;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
            rbp25 = rbp25 + 4;
            eax28 = static_cast<uint32_t>(rdx26 - 0x1010101) & ~reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rdx26)) & 0x80808080;
        } while (!eax28);
        zf29 = (eax28 & reinterpret_cast<uint32_t>("X509_STORE_CTX_set0_dane")) == 0;
        if (!zf29) 
            goto addr_6bd20_15;
    }
    eax28 = 0;
    addr_6bd20_15:
    if (zf29) {
        rbp25 = rbp25 + 2;
    }
    ebx30 = eax28;
    rax31 = fun_212a0(r8_23, r15_11, "ssl/statem/statem_clnt.c", r8_23, r15_11, "ssl/statem/statem_clnt.c");
    rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp25) - (3 + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp25) < reinterpret_cast<unsigned char>(3 + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebx30) + *reinterpret_cast<signed char*>(&eax28)) < *reinterpret_cast<unsigned char*>(&ebx30))))))) - reinterpret_cast<unsigned char>(r13_12));
    rbx9 = rax31;
    rax32 = fun_21cf0(r13_12, "ssl/statem/statem_clnt.c", 0xae4, r13_12, "ssl/statem/statem_clnt.c", 0xae4);
    if (!rbx9 || !rax32) {
        fun_20230();
        r14d13 = 0;
        fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
        ossl_statem_fatal(r12_3, r12_3);
        r9_14 = rax32;
        r8_15 = r8_23;
        goto addr_6bdff_9;
    } else {
        fun_208d0();
        *reinterpret_cast<void***>(r12_3 + 0x370) = rbx9;
        *reinterpret_cast<void***>(r12_3 + 0x378) = r15_11;
        fun_208d0();
        *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 0x918) + 0x2a8) = rax32;
        eax33 = WPACKET_sub_memcpy__(r14_17, r13_12, r14_17, r13_12);
        r8_15 = r8_23;
        r14d13 = eax33;
        if (!eax33) {
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
            fun_20230();
            fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
            ossl_statem_fatal(r12_3, r12_3);
            r8_15 = r8_15;
            *reinterpret_cast<int32_t*>(&r9_14) = 0;
            *reinterpret_cast<int32_t*>(&r9_14 + 4) = 0;
            goto addr_6bdff_9;
        } else {
            *reinterpret_cast<int32_t*>(&r9_14) = 0;
            *reinterpret_cast<int32_t*>(&r9_14 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
            r14d13 = 1;
            goto addr_6bdff_9;
        }
    }
}