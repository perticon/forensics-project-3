int64_t tls_construct_cke_psk_preamble(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6) {
    int64_t v1 = __readfsqword(40); // 0x6bc6b
    int64_t result; // 0x6bc50
    if (*(int64_t *)(a1 + (int64_t)&g360) == 0) {
        // 0x6bf18
        function_20230();
        function_207e0();
        ossl_statem_fatal();
        result = 0;
    } else {
        // 0x6bc87
        int64_t v2; // bp-856, 0x6bc50
        int64_t v3 = &v2; // 0x6bc87
        __asm_rep_stosq_memset((char *)&v2, 0, 32);
        int64_t * v4 = (int64_t *)(a1 + (int64_t)&g350); // 0x6bcad
        int64_t v5 = *v4; // 0x6bcad
        bool v6; // 0x6bc50
        *(char *)((v6 ? -256 : 256) + v3) = 0;
        if ((int32_t)v5 < 513) {
            int64_t v7 = v5 & 0xffffffff; // 0x6bcd7
            int64_t v8 = v3; // 0x6bce6
            if (v7 == 0) {
                // 0x6be80
                function_20230();
                function_207e0();
                ossl_statem_fatal();
                result = 0;
            } else {
                int32_t v9 = *(int32_t *)v8; // 0x6bcec
                v8 += 4;
                while (((v9 & -0x7f7f7f80 ^ -0x7f7f7f80) & v9 - 0x1010101) == 0) {
                    // 0x6bcec
                    v9 = *(int32_t *)v8;
                    v8 += 4;
                }
                int64_t v10 = function_212a0(); // 0x6bd37
                int64_t v11 = function_21cf0(); // 0x6bd51
                if (v10 == 0 || v11 == 0) {
                    // 0x6bed0
                    function_20230();
                    function_207e0();
                    ossl_statem_fatal();
                    result = 0;
                } else {
                    // 0x6bd75
                    function_208d0();
                    *(int64_t *)(a1 + (int64_t)&g121) = v10;
                    *(int64_t *)(a1 + (int64_t)&g122) = v7;
                    function_208d0();
                    *(int64_t *)(*v4 + (int64_t)&g72) = 0x100000000 * v11 >> 32;
                    int64_t v12 = WPACKET_sub_memcpy__(); // 0x6bddf
                    result = 1;
                    if ((int32_t)v12 == 0) {
                        // 0x6bfc0
                        function_20230();
                        function_207e0();
                        ossl_statem_fatal();
                        result = v12 & 0xffffffff;
                    }
                }
            }
        } else {
            // 0x6bf70
            function_20230();
            function_207e0();
            ossl_statem_fatal();
            result = 0;
        }
    }
    // 0x6bdff
    function_20720();
    function_20720();
    function_21a00();
    function_21a00();
    if (v1 != __readfsqword(40)) {
        // 0x6c005
        return function_20ff0();
    }
    // 0x6be64
    return result;
}