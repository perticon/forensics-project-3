uint ossl_statem_app_data_allowed(long param_1)

{
  int iVar1;
  uint uVar2;
  
  uVar2 = 0;
  if (((*(int *)(param_1 + 0x48) != 0) && (uVar2 = *(uint *)(param_1 + 0x1b8), uVar2 != 0)) &&
     (uVar2 = *(uint *)(param_1 + 0x1b0), uVar2 != 0)) {
    iVar1 = *(int *)(param_1 + 0x5c);
    if (*(int *)(param_1 + 0x38) != 0) {
      return (uint)(iVar1 == 0 || iVar1 == 0x14);
    }
    uVar2 = (uint)(iVar1 == 0xc);
  }
  return uVar2;
}