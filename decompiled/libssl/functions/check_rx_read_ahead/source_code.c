static int check_rx_read_ahead(SSL *s, unsigned char *rec_seq)
{
    int bit, count_unprocessed;

    count_unprocessed = count_unprocessed_records(s);
    if (count_unprocessed < 0)
        return 0;

    /* increment the crypto_info record sequence */
    while (count_unprocessed) {
        for (bit = 7; bit >= 0; bit--) { /* increment */
            ++rec_seq[bit];
            if (rec_seq[bit] != 0)
                break;
        }
        count_unprocessed--;

    }

    return 1;
}