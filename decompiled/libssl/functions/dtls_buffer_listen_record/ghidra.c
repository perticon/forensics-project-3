bool dtls_buffer_listen_record(long param_1,long param_2,undefined8 *param_3,undefined8 param_4)

{
  undefined8 uVar1;
  int iVar2;
  
  *(undefined (*) [16])(param_1 + 0x12a8) = (undefined  [16])0x0;
  *(undefined (*) [16])(param_1 + 0x12b8) = (undefined  [16])0x0;
  *(long *)(param_1 + 0x12b0) = param_2;
  *(undefined (*) [16])(param_1 + 0x12c8) = (undefined  [16])0x0;
  *(undefined (*) [16])(param_1 + 0x12e8) = (undefined  [16])0x0;
  *(undefined4 *)(param_1 + 0x12ac) = 0x16;
  *(undefined (*) [16])(param_1 + 0x12d8) = (undefined  [16])0x0;
  uVar1 = *param_3;
  *(long *)(param_1 + 0x1cb0) = param_2 + 0xd;
  *(undefined8 *)(param_1 + 0x12f0) = uVar1;
  *(undefined8 *)(param_1 + 0x12c0) = param_4;
  *(long *)(param_1 + 0x1ca8) = *(long *)(param_1 + 0xc78);
  *(long *)(param_1 + 0x12c8) = *(long *)(param_1 + 0xc78) + 0xd;
  iVar2 = dtls1_buffer_record(0,param_1,*(long *)(param_1 + 0x1d10) + 0x38,param_1 + 0x12f0);
  return 0 < iVar2;
}