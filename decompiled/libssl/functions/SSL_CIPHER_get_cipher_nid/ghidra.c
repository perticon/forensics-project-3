undefined4 SSL_CIPHER_get_cipher_nid(long param_1)

{
  long lVar1;
  
  if (param_1 != 0) {
    lVar1 = 0;
    do {
      if (*(int *)(param_1 + 0x24) == *(int *)(ssl_cipher_table_cipher + lVar1 * 8)) {
        return *(undefined4 *)(ssl_cipher_table_cipher + (long)(int)lVar1 * 8 + 4);
      }
      lVar1 = lVar1 + 1;
    } while (lVar1 != 0x18);
  }
  return 0;
}