bool SSL_is_init_finished(long param_1)

{
  bool bVar1;
  
  bVar1 = false;
  if (*(int *)(param_1 + 100) == 0) {
    bVar1 = *(int *)(param_1 + 0x5c) == 1;
  }
  return bVar1;
}