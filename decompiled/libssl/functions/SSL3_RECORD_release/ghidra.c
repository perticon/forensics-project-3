void SSL3_RECORD_release(long param_1,long param_2)

{
  void **ppvVar1;
  void **ppvVar2;
  
  if (param_2 != 0) {
    ppvVar1 = (void **)(param_1 + 0x30);
    do {
      ppvVar2 = ppvVar1 + 10;
      CRYPTO_free(*ppvVar1);
      *ppvVar1 = (void *)0x0;
      ppvVar1 = ppvVar2;
    } while (ppvVar2 != (void **)(param_1 + 0x30) + param_2 * 10);
    return;
  }
  return;
}