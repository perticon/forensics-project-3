ulong ssl3_setup_buffers(undefined8 param_1)

{
  int iVar1;
  ulong uVar2;
  
  uVar2 = ssl3_setup_read_buffer();
  if ((int)uVar2 == 0) {
    return uVar2;
  }
  iVar1 = ssl3_setup_write_buffer(param_1,1,0);
  return (ulong)(iVar1 != 0);
}