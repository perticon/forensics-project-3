void dtls1_start_timer(SSL *param_1)

{
  uint uVar1;
  char **ppcVar2;
  int iVar3;
  ulong uVar4;
  _func_257 *p_Var5;
  BIO *bp;
  long lVar6;
  ulong uVar7;
  X509_EXTENSIONS *pXVar8;
  
  pXVar8 = param_1[1].tlsext_ocsp_exts;
  if ((*(long *)&pXVar8[0xf].stack.sorted == 0) && (pXVar8[0xf].stack.comp == (_func_257 *)0x0)) {
    ppcVar2 = pXVar8[0x10].stack.data;
    if (ppcVar2 == (char **)0x0) {
      pXVar8[0x10].stack.num = 1000000;
    }
    else {
      iVar3 = (*(code *)ppcVar2)(param_1,0);
      pXVar8[0x10].stack.num = iVar3;
      pXVar8 = param_1[1].tlsext_ocsp_exts;
    }
  }
  uVar4 = ossl_time_now();
  uVar7 = (uVar4 >> 9) / 0x1dcd65;
  *(ulong *)&pXVar8[0xf].stack.sorted = uVar7;
  pXVar8[0xf].stack.comp = (_func_257 *)((uVar4 + uVar7 * -1000000000 >> 3) / 0x7d);
  pXVar8 = param_1[1].tlsext_ocsp_exts;
  uVar1 = pXVar8[0x10].stack.num;
  lVar6 = (ulong)uVar1 / 1000000 + *(long *)&pXVar8[0xf].stack.sorted;
  *(long *)&pXVar8[0xf].stack.sorted = lVar6;
  p_Var5 = pXVar8[0xf].stack.comp + uVar1 % 1000000;
  pXVar8[0xf].stack.comp = p_Var5;
  if (999999 < (long)p_Var5) {
    *(long *)&pXVar8[0xf].stack.sorted = lVar6 + 1;
    pXVar8[0xf].stack.comp = p_Var5 + -1000000;
  }
  bp = SSL_get_rbio(param_1);
  BIO_ctrl(bp,0x2d,0,&pXVar8[0xf].stack.sorted);
  return;
}