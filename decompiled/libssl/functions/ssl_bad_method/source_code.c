const SSL_METHOD *ssl_bad_method(int ver)
{
    ERR_raise(ERR_LIB_SSL, ERR_R_SHOULD_NOT_HAVE_BEEN_CALLED);
    return NULL;
}