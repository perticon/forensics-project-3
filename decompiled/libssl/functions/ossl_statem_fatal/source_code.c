void ossl_statem_fatal(SSL *s, int al, int reason, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    ERR_vset_error(ERR_LIB_SSL, reason, fmt, args);
    va_end(args);

    ossl_statem_send_fatal(s, al);
}