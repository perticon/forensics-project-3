undefined4 ssl_x509err2alert(int param_1)

{
  undefined1 *puVar1;
  undefined1 *puVar2;
  
  if (param_1 == 0x32) {
    return 0x28;
  }
  puVar1 = x509table;
  do {
    puVar2 = puVar1;
    if (*(int *)(puVar2 + 8) == param_1) break;
    puVar1 = puVar2 + 8;
  } while (*(int *)(puVar2 + 8) != 0);
  return *(undefined4 *)(puVar2 + 0xc);
}