SSL_set_srp_server_param_pw(long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4)

{
  undefined8 uVar1;
  undefined8 uVar2;
  int iVar3;
  long lVar4;
  BIGNUM *pBVar5;
  BIGNUM *pBVar6;
  undefined8 uVar7;
  undefined auVar8 [16];
  
  auVar8 = SRP_get_default_gN(param_4);
  uVar7 = SUB168(auVar8 >> 0x40,0);
  lVar4 = SUB168(auVar8,0);
  if (lVar4 != 0) {
    pBVar5 = BN_dup(*(BIGNUM **)(lVar4 + 0x10));
    pBVar6 = *(BIGNUM **)(lVar4 + 8);
    *(BIGNUM **)(param_1 + 0xbf8) = pBVar5;
    pBVar6 = BN_dup(pBVar6);
    *(BIGNUM **)(param_1 + 0xc00) = pBVar6;
    BN_clear_free(*(BIGNUM **)(param_1 + 0xc30));
    *(undefined8 *)(param_1 + 0xc30) = 0;
    BN_clear_free(*(BIGNUM **)(param_1 + 0xc08));
    uVar1 = *(undefined8 *)(lVar4 + 0x10);
    uVar2 = *(undefined8 *)(lVar4 + 8);
    *(undefined8 *)(param_1 + 0xc08) = 0;
    uVar7 = (*(undefined8 **)(param_1 + 0x9a8))[0x88];
    iVar3 = SRP_create_verifier_BN_ex
                      (param_2,param_3,param_1 + 0xc08,param_1 + 0xc30,uVar1,uVar2,
                       **(undefined8 **)(param_1 + 0x9a8));
    if (iVar3 != 0) {
      return CONCAT88(uVar7,1);
    }
  }
  return CONCAT88(uVar7,0xffffffff);
}