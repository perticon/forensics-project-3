undefined8 tls_parse_stoc_early_data(long param_1,uint **param_2,int param_3)

{
  uint uVar1;
  uint *puVar2;
  undefined8 uVar3;
  
  puVar2 = param_2[1];
  if (param_3 == 0x2000) {
    if ((uint *)0x3 < puVar2) {
      uVar1 = **param_2;
      *param_2 = *param_2 + 1;
      param_2[1] = puVar2 + -1;
      if (puVar2 + -1 == (uint *)0x0) {
        *(uint *)(*(long *)(param_1 + 0x918) + 0x354) =
             uVar1 >> 0x18 | (uVar1 & 0xff0000) >> 8 | (uVar1 & 0xff00) << 8 | uVar1 << 0x18;
        return 1;
      }
    }
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x777,"tls_parse_stoc_early_data");
    uVar3 = 0xae;
  }
  else {
    if (puVar2 == (uint *)0x0) {
      if ((*(int *)(param_1 + 0xb34) != 0) && (*(int *)(param_1 + 0x4d0) != 0)) {
        *(undefined4 *)(param_1 + 0xb30) = 2;
        return 1;
      }
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x78c,"tls_parse_stoc_early_data");
      ossl_statem_fatal(param_1,0x2f,0x6e,0);
      return 0;
    }
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x781,"tls_parse_stoc_early_data");
    uVar3 = 0x6e;
  }
  ossl_statem_fatal(param_1,0x32,uVar3,0);
  return 0;
}