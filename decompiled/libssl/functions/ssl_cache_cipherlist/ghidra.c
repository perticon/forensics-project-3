undefined8 ssl_cache_cipherlist(long param_1,char **param_2,int param_3)

{
  char *pcVar1;
  char *pcVar2;
  char *pcVar3;
  undefined2 *puVar4;
  long lVar5;
  char *pcVar6;
  ulong uVar7;
  
  uVar7 = (ulong)(param_3 != 0) + 2;
  if (param_2[1] == (char *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x15e8,"ssl_cache_cipherlist");
    ossl_statem_fatal(param_1,0x2f,0xb7,0);
    return 0;
  }
  if ((ulong)param_2[1] % uVar7 != 0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x15ed,"ssl_cache_cipherlist");
    ossl_statem_fatal(param_1,0x32,0x97,0);
    return 0;
  }
  CRYPTO_free(*(void **)(param_1 + 0x350));
  *(undefined8 *)(param_1 + 0x350) = 0;
  *(undefined8 *)(param_1 + 0x358) = 0;
  if (param_3 == 0) {
    CRYPTO_free((void *)0x0);
    *(undefined8 *)(param_1 + 0x358) = 0;
    pcVar1 = param_2[1];
    *(undefined8 *)(param_1 + 0x350) = 0;
    if (pcVar1 != (char *)0x0) {
      lVar5 = CRYPTO_memdup(*param_2,pcVar1,"include/internal/packet.h",0x1c5);
      *(long *)(param_1 + 0x350) = lVar5;
      if (lVar5 == 0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0x161c,"ssl_cache_cipherlist");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        return 0;
      }
      *(char **)(param_1 + 0x358) = pcVar1;
    }
  }
  else {
    pcVar1 = param_2[1];
    pcVar2 = *param_2;
    puVar4 = (undefined2 *)CRYPTO_malloc((int)((ulong)pcVar1 / uVar7) * 2,"ssl/ssl_lib.c",0x1602);
    *(undefined2 **)(param_1 + 0x350) = puVar4;
    if (puVar4 == (undefined2 *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x1605,"ssl_cache_cipherlist");
      ossl_statem_fatal(param_1,0x50,0xc0100,0);
      return 0;
    }
    *(undefined8 *)(param_1 + 0x358) = 0;
    pcVar3 = pcVar2;
    if (pcVar1 != (char *)0x0) {
      do {
        pcVar6 = pcVar3 + 3;
        if (*pcVar3 == '\0') {
          if ((ulong)((long)(pcVar1 + 2 + (long)pcVar2) - (long)pcVar6) < 2) goto LAB_0013cd3a;
          *puVar4 = *(undefined2 *)(pcVar3 + 1);
          *(long *)(param_1 + 0x358) = *(long *)(param_1 + 0x358) + 2;
        }
        else if ((ulong)((long)(pcVar1 + 2 + (long)pcVar2) - (long)pcVar6) < 2) {
LAB_0013cd3a:
          ERR_new();
          ERR_set_debug("ssl/ssl_lib.c",0x1611,"ssl_cache_cipherlist");
          ossl_statem_fatal(param_1,0x32,0xf0,0);
          CRYPTO_free(*(void **)(param_1 + 0x350));
          *(undefined8 *)(param_1 + 0x350) = 0;
          *(undefined8 *)(param_1 + 0x358) = 0;
          return 0;
        }
        puVar4 = puVar4 + 1;
        pcVar3 = pcVar6;
      } while (pcVar2 + (long)pcVar1 != pcVar6);
    }
  }
  return 1;
}