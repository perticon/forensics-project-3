bool ssl_srp_calc_a_param_intern(long param_1)

{
  int iVar1;
  BIGNUM *pBVar2;
  long lVar3;
  long in_FS_OFFSET;
  bool bVar4;
  uchar auStack88 [56];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = RAND_priv_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),auStack88,0x30,0);
  bVar4 = false;
  if (0 < iVar1) {
    pBVar2 = BN_bin2bn(auStack88,0x30,*(BIGNUM **)(param_1 + 0xc20));
    *(BIGNUM **)(param_1 + 0xc20) = pBVar2;
    OPENSSL_cleanse(auStack88,0x30);
    lVar3 = SRP_Calc_A(*(undefined8 *)(param_1 + 0xc20),*(undefined8 *)(param_1 + 0xbf8),
                       *(undefined8 *)(param_1 + 0xc00));
    bVar4 = lVar3 != 0;
    *(long *)(param_1 + 0xc18) = lVar3;
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}