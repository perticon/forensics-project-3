undefined8 ossl_statem_client_read_transition(SSL *param_1,int param_2)

{
  undefined4 uVar1;
  int iVar2;
  uint uVar3;
  BIO *b;
  uint uVar4;
  bool bVar5;
  
  uVar1 = *(undefined4 *)((long)&param_1->init_msg + 4);
  uVar4 = *(uint *)(param_1->method->get_timeout + 0x60) & 8;
  if (uVar4 == 0) {
    iVar2 = param_1->method->version;
    if ((iVar2 == 0x10000) || (iVar2 < 0x304)) {
      switch(uVar1) {
      default:
        goto switchD_0016c27f_caseD_0;
      case 1:
        goto switchD_0016c213_caseD_1;
      case 3:
        goto switchD_0016c213_caseD_3;
      case 4:
        goto switchD_0016c213_caseD_4;
      case 5:
        goto switchD_0016c213_caseD_5;
      case 6:
        goto switchD_0016c213_caseD_6;
      case 7:
        goto switchD_0016c213_caseD_7;
      case 9:
        goto switchD_0016c213_caseD_9;
      case 10:
        goto switchD_0016c213_caseD_a;
      case 0xc:
        goto switchD_0016c213_caseD_c;
      case 0x12:
        goto switchD_0016c213_caseD_12;
      case 0x2e:
        goto switchD_0016c213_caseD_2e;
      }
    }
    switch(uVar1) {
    case 1:
      if (param_2 == 4) goto LAB_0016c317;
      if (param_2 == 0x18) {
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x2d;
        return 1;
      }
      if ((param_2 != 0xd) || (*(int *)&param_1[4].enc_write_ctx != 1)) break;
      *(undefined4 *)&param_1[4].enc_write_ctx = 4;
      uVar4 = tls13_restore_handshake_digest_for_pha(param_1);
      goto joined_r0x0016c2e7;
    case 3:
      if (param_2 == 8) {
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x26;
        return 1;
      }
      break;
    case 4:
      if (param_2 == 0xf) {
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0x27;
        return 1;
      }
      break;
    case 0xc:
      if (param_2 != 2) break;
      goto LAB_0016c419;
    case 0x26:
      if (*(int *)&param_1[1].tlsext_ecpointformatlist_length != 0) goto switchD_0016c27f_caseD_27;
      if (param_2 == 0xd) goto LAB_0016c402;
    case 7:
      if (param_2 == 0xb) {
LAB_0016c3b2:
        *(undefined4 *)((long)&param_1->init_msg + 4) = 4;
        return 1;
      }
      break;
    case 0x27:
switchD_0016c27f_caseD_27:
      if (param_2 == 0x14) {
LAB_0016c38a:
        *(undefined4 *)((long)&param_1->init_msg + 4) = 0xb;
        return 1;
      }
    }
    goto switchD_0016c27f_caseD_0;
  }
  switch(uVar1) {
  default:
    bVar5 = true;
    goto LAB_0016c21b;
  case 1:
switchD_0016c213_caseD_1:
    if (param_2 == 0) {
      *(undefined4 *)((long)&param_1->init_msg + 4) = 0x29;
      return 1;
    }
    break;
  case 3:
switchD_0016c213_caseD_3:
    if (*(int *)&param_1[1].tlsext_ecpointformatlist_length != 0) goto switchD_0016c213_caseD_12;
    bVar5 = uVar4 != 0;
    if ((param_2 == 3) && (bVar5)) {
LAB_0016c344:
      *(undefined4 *)((long)&param_1->init_msg + 4) = 2;
      return 1;
    }
    if ((((0x300 < param_1->version) && (*(long *)&param_1[4].server != 0)) &&
        (*(long *)(*(long *)&param_1[3].sid_ctx_length + 0x338) != 0)) && (param_2 == 0x101)) {
      *(undefined4 *)&param_1[1].tlsext_ecpointformatlist_length = 1;
      *(undefined4 *)((long)&param_1->init_msg + 4) = 10;
      return 1;
    }
    uVar4 = *(uint *)(param_1[1].handshake_func + 0x20);
    if ((uVar4 & 0x54) == 0) {
      if (param_2 == 0xb) goto LAB_0016c3b2;
    }
    else {
      uVar3 = *(uint *)(param_1[1].handshake_func + 0x1c);
      if ((uVar3 & 0x1a6) == 0) {
        if (((uVar3 & 0x1c8) != 0) && (param_2 == 0xc)) goto LAB_0016c4f9;
        if (param_2 == 0xd) {
          if ((0x300 < param_1->version) && ((uVar4 & 4) != 0)) goto switchD_0016c27f_caseD_0;
          uVar4 = uVar4 & 0x50;
          goto joined_r0x0016c2e7;
        }
        if (param_2 == 0xe) goto LAB_0016c536;
      }
      else if (param_2 == 0xc) goto LAB_0016c4f9;
    }
    goto LAB_0016c21b;
  case 4:
switchD_0016c213_caseD_4:
    if ((*(int *)((long)&param_1[3].tls_session_ticket_ext_cb_arg + 4) != 0) && (param_2 == 0x16)) {
      *(undefined4 *)((long)&param_1->init_msg + 4) = 5;
      return 1;
    }
  case 5:
switchD_0016c213_caseD_5:
    if ((*(uint *)(param_1[1].handshake_func + 0x1c) & 0x1a6) == 0) {
      if (((*(uint *)(param_1[1].handshake_func + 0x1c) & 0x1c8) != 0) && (param_2 == 0xc)) {
LAB_0016c4f9:
        *(undefined4 *)((long)&param_1->init_msg + 4) = 6;
        return 1;
      }
switchD_0016c213_caseD_6:
      if (param_2 == 0xd) {
        if ((param_1->version < 0x301) || ((*(uint *)(param_1[1].handshake_func + 0x20) & 4) == 0))
        {
          uVar4 = *(uint *)(param_1[1].handshake_func + 0x20) & 0x50;
joined_r0x0016c2e7:
          if (uVar4 == 0) {
LAB_0016c402:
            *(undefined4 *)((long)&param_1->init_msg + 4) = 7;
            return 1;
          }
        }
        goto switchD_0016c27f_caseD_0;
      }
switchD_0016c213_caseD_7:
      if (param_2 == 0xe) {
LAB_0016c536:
        *(undefined4 *)((long)&param_1->init_msg + 4) = 8;
        return 1;
      }
    }
    else if (param_2 == 0xc) goto LAB_0016c4f9;
    break;
  case 6:
    goto switchD_0016c213_caseD_6;
  case 7:
    goto switchD_0016c213_caseD_7;
  case 9:
switchD_0016c213_caseD_9:
    if (param_2 == 0x101) {
      *(undefined4 *)((long)&param_1->init_msg + 4) = 10;
      return 1;
    }
    goto switchD_0016c27f_caseD_0;
  case 10:
switchD_0016c213_caseD_a:
    if (param_2 == 0x14) goto LAB_0016c38a;
    break;
  case 0xc:
switchD_0016c213_caseD_c:
    if (param_2 == 2) {
LAB_0016c419:
      *(undefined4 *)((long)&param_1->init_msg + 4) = 3;
      return 1;
    }
    bVar5 = uVar4 != 0;
    if ((param_2 == 3) && (bVar5)) goto LAB_0016c344;
    goto LAB_0016c21b;
  case 0x12:
switchD_0016c213_caseD_12:
    if (*(int *)&param_1[3].next_proto_negotiated_len == 0) goto switchD_0016c213_caseD_9;
    if (param_2 == 4) {
LAB_0016c317:
      *(undefined4 *)((long)&param_1->init_msg + 4) = 9;
      return 1;
    }
    break;
  case 0x2e:
switchD_0016c213_caseD_2e:
    if (param_2 == 2) goto LAB_0016c419;
  }
  bVar5 = uVar4 != 0;
LAB_0016c21b:
  if ((param_2 == 0x101) && (bVar5)) {
    param_1->msg_callback = (_func_3150 *)0x0;
    param_1->rwstate = 3;
    b = SSL_get_rbio(param_1);
    BIO_clear_flags(b,0xf);
    BIO_set_flags(b,9);
    return 0;
  }
switchD_0016c27f_caseD_0:
  ERR_new();
  ERR_set_debug("ssl/statem/statem_clnt.c",0x18e,"ossl_statem_client_read_transition");
  ossl_statem_fatal(param_1,10,0xf4,0);
  return 0;
}