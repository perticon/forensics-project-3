void ssl_update_cache(long param_1,uint param_2)

{
  uint uVar1;
  SSL_SESSION *c;
  int iVar2;
  uint *puVar3;
  time_t tm;
  SSL_CTX *s;
  
  c = *(SSL_SESSION **)(param_1 + 0x918);
  if (c->tlsext_tick_lifetime_hint == 0) {
    return;
  }
  if (((*(int *)(param_1 + 0x38) != 0) && (*(long *)(c[1].master_key + 0xc) == 0)) &&
     ((*(byte *)(param_1 + 0x968) & 1) != 0)) {
    return;
  }
  s = *(SSL_CTX **)(param_1 + 0xb88);
  uVar1 = *(uint *)&s->new_session_cb;
  if ((uVar1 & param_2) == 0) goto LAB_00139b08;
  if (*(int *)(param_1 + 0x4d0) == 0) {
    if ((uVar1 & 0x200) == 0) {
      if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
        iVar2 = **(int **)(param_1 + 8);
        goto LAB_00139b44;
      }
LAB_00139b88:
      SSL_CTX_add_session(s,c);
      s = *(SSL_CTX **)(param_1 + 0xb88);
    }
  }
  else {
    if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
        (iVar2 = **(int **)(param_1 + 8), iVar2 == 0x10000)) || (iVar2 < 0x304)) goto LAB_00139b08;
    if ((uVar1 & 0x200) == 0) {
LAB_00139b44:
      if (((iVar2 < 0x304 || iVar2 == 0x10000) || (*(int *)(param_1 + 0x38) == 0)) ||
         (((*(int *)(param_1 + 0x1d40) != 0 && ((*(byte *)(param_1 + 0x9eb) & 1) == 0)) ||
          ((*(long *)&s->stats != 0 || ((*(byte *)(param_1 + 0x9e9) & 0x40) != 0))))))
      goto LAB_00139b88;
    }
  }
  if (s->get_session_cb != (_func_3089 *)0x0) {
    SSL_SESSION_up_ref(*(undefined8 *)(param_1 + 0x918));
    iVar2 = (**(code **)(*(long *)(param_1 + 0xb88) + 0x60))
                      (param_1,*(undefined8 *)(param_1 + 0x918));
    if (iVar2 == 0) {
      SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x918));
    }
  }
LAB_00139b08:
  if (((uVar1 & 0x80) == 0) && ((uVar1 & param_2) == param_2)) {
    puVar3 = (uint *)(*(long *)(param_1 + 0xb88) + 0x8c);
    if ((param_2 & 1) != 0) {
      puVar3 = (uint *)(*(long *)(param_1 + 0xb88) + 0x80);
    }
    if ((*puVar3 & 0xff) == 0xff) {
      tm = time((time_t *)0x0);
      SSL_CTX_flush_sessions(*(SSL_CTX **)(param_1 + 0xb88),tm);
      return;
    }
  }
  return;
}