int32_t tls_process_cke_psk_preamble(void** rdi, void** rsi) {
    uint64_t rax3;
    void** rax4;
    uint32_t ebp5;
    void** rbp6;
    void** r13_7;
    void** rbx8;
    void** rax9;
    void** rbp10;
    void** rsi11;
    uint32_t eax12;
    void** rbx13;
    int32_t eax14;
    void** rax15;
    uint64_t rcx16;

    rax3 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8)) <= reinterpret_cast<unsigned char>(1) || (rax4 = *reinterpret_cast<void***>(rsi + 8) - 2, ebp5 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi))), *reinterpret_cast<uint32_t*>(&rbp6) = *reinterpret_cast<uint16_t*>(&ebp5), *reinterpret_cast<int32_t*>(&rbp6 + 4) = 0, reinterpret_cast<unsigned char>(rax4) < reinterpret_cast<unsigned char>(rbp6))) {
        fun_20230();
        fun_207e0("ssl/statem/statem_srvr.c", "ssl/statem/statem_srvr.c");
    } else {
        r13_7 = *reinterpret_cast<void***>(rsi) + 2;
        *reinterpret_cast<void***>(rsi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(rbp6));
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_7) + reinterpret_cast<unsigned char>(rbp6));
        if (reinterpret_cast<unsigned char>(rbp6) > reinterpret_cast<unsigned char>(0x100)) {
            fun_20230();
            fun_207e0("ssl/statem/statem_srvr.c", "ssl/statem/statem_srvr.c");
        } else {
            if (!*reinterpret_cast<int64_t*>(rdi + 0x990)) {
                fun_20230();
                fun_207e0("ssl/statem/statem_srvr.c", "ssl/statem/statem_srvr.c");
                goto addr_79ec4_7;
            } else {
                rbx8 = *reinterpret_cast<void***>(rdi + 0x918);
                fun_208d0();
                rax9 = fun_21d20(r13_7, rbp6, "include/internal/packet.h", 0x1dc);
                *reinterpret_cast<void***>(rbx8 + 0x2a8) = rax9;
                if (!rax9) {
                    fun_20230();
                    fun_207e0("ssl/statem/statem_srvr.c", "ssl/statem/statem_srvr.c");
                    goto addr_79ec4_7;
                } else {
                    rbp10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x218 - 8 + 8 - 8 + 8);
                    rsi11 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x918) + 0x2a8);
                    eax12 = reinterpret_cast<uint32_t>(*reinterpret_cast<int64_t*>(rdi + 0x990)(rdi, rsi11, rbp10, 0x200));
                    *reinterpret_cast<uint32_t*>(&rbx13) = eax12;
                    *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
                    if (eax12 > 0x200) {
                        fun_20230();
                        fun_207e0("ssl/statem/statem_srvr.c", "ssl/statem/statem_srvr.c");
                        goto addr_79ec4_7;
                    } else {
                        if (!rbx13) {
                            fun_20230();
                            fun_207e0("ssl/statem/statem_srvr.c", "ssl/statem/statem_srvr.c");
                            ossl_statem_fatal(rdi, rdi);
                            eax14 = 0;
                            goto addr_79e05_14;
                        } else {
                            fun_208d0();
                            rax15 = fun_212a0(rbp10, rbx13, "ssl/statem/statem_srvr.c", rbp10, rbx13, "ssl/statem/statem_srvr.c");
                            *reinterpret_cast<void***>(rdi + 0x370) = rax15;
                            fun_20720(rbp10, rbx13, rbp10, rbx13);
                            if (!*reinterpret_cast<void***>(rdi + 0x370)) {
                                *reinterpret_cast<void***>(rdi + 0x378) = reinterpret_cast<void**>(0);
                                fun_20230();
                                fun_207e0("ssl/statem/statem_srvr.c", "ssl/statem/statem_srvr.c");
                                goto addr_79ec4_7;
                            } else {
                                *reinterpret_cast<void***>(rdi + 0x378) = rbx13;
                                eax14 = 1;
                                goto addr_79e05_14;
                            }
                        }
                    }
                }
            }
        }
    }
    ossl_statem_fatal(rdi, rdi);
    eax14 = 0;
    addr_79e05_14:
    rcx16 = rax3 ^ g28;
    if (rcx16) {
        fun_20ff0();
    } else {
        return eax14;
    }
    addr_79ec4_7:
    ossl_statem_fatal(rdi, rdi);
    eax14 = 0;
    goto addr_79e05_14;
}