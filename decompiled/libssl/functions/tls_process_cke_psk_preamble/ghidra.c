undefined8 tls_process_cke_psk_preamble(long param_1,ushort **param_2)

{
  ushort *puVar1;
  ushort uVar2;
  long lVar3;
  uint uVar4;
  ushort *puVar5;
  long lVar6;
  undefined8 uVar7;
  ulong len;
  ushort *puVar8;
  long in_FS_OFFSET;
  undefined auStack568 [520];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_2[1] < (ushort *)0x2) {
LAB_00179dd0:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0xaf7,"tls_process_cke_psk_preamble");
    uVar7 = 0x9f;
  }
  else {
    puVar5 = param_2[1] + -1;
    uVar2 = **param_2;
    puVar8 = (ushort *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8);
    if (puVar5 < puVar8) goto LAB_00179dd0;
    puVar1 = *param_2 + 1;
    param_2[1] = (ushort *)((long)puVar5 - (long)puVar8);
    *param_2 = (ushort *)((long)puVar1 + (long)puVar8);
    if (puVar8 < (ushort *)0x101) {
      if (*(long *)(param_1 + 0x990) == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0xaff,"tls_process_cke_psk_preamble");
        uVar7 = 0xe1;
      }
      else {
        lVar3 = *(long *)(param_1 + 0x918);
        CRYPTO_free(*(void **)(lVar3 + 0x2a8));
        lVar6 = CRYPTO_strndup(puVar1,puVar8,"include/internal/packet.h",0x1dc);
        *(long *)(lVar3 + 0x2a8) = lVar6;
        if (lVar6 == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0xb04,"tls_process_cke_psk_preamble");
          uVar7 = 0xc0103;
        }
        else {
          uVar4 = (**(code **)(param_1 + 0x990))
                            (param_1,*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2a8),auStack568,
                             0x200);
          len = (ulong)uVar4;
          if (uVar4 < 0x201) {
            if (len == 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_srvr.c",0xb12,"tls_process_cke_psk_preamble");
              ossl_statem_fatal(param_1,0x73,0xdf,0);
              uVar7 = 0;
              goto LAB_00179e05;
            }
            CRYPTO_free(*(void **)(param_1 + 0x370));
            uVar7 = CRYPTO_memdup(auStack568,len,"ssl/statem/statem_srvr.c",0xb17);
            *(undefined8 *)(param_1 + 0x370) = uVar7;
            OPENSSL_cleanse(auStack568,len);
            if (*(long *)(param_1 + 0x370) != 0) {
              *(ulong *)(param_1 + 0x378) = len;
              uVar7 = 1;
              goto LAB_00179e05;
            }
            *(undefined8 *)(param_1 + 0x378) = 0;
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xb1c,"tls_process_cke_psk_preamble");
            uVar7 = 0xc0100;
          }
          else {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xb0c,"tls_process_cke_psk_preamble");
            uVar7 = 0xc0103;
          }
        }
      }
      ossl_statem_fatal(param_1,0x50,uVar7,0);
      uVar7 = 0;
      goto LAB_00179e05;
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0xafb,"tls_process_cke_psk_preamble");
    uVar7 = 0x92;
  }
  ossl_statem_fatal(param_1,0x32,uVar7,0);
  uVar7 = 0;
LAB_00179e05:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}