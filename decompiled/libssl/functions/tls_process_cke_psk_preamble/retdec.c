int64_t tls_process_cke_psk_preamble(int64_t a1, int64_t a2, int64_t a3, int64_t a4) {
    int64_t v1 = __readfsqword(40); // 0x79c80
    int64_t * v2 = (int64_t *)(a2 + 8);
    uint64_t v3 = *v2; // 0x79c93
    int64_t result; // 0x79c70
    if (v3 < 2) {
        // 0x79dd0
        function_20230();
        function_207e0();
        // 0x79df4
        ossl_statem_fatal();
        result = 0;
        goto lab_0x79e05;
    } else {
        uint64_t v4 = v3 - 2; // 0x79ca4
        uint16_t v5 = llvm_bswap_i16((int16_t)a2); // 0x79cab
        uint64_t v6 = (int64_t)v5; // 0x79caf
        if (v4 < v6) {
            // 0x79dd0
            function_20230();
            function_207e0();
            // 0x79df4
            ossl_statem_fatal();
            result = 0;
            goto lab_0x79e05;
        } else {
            // 0x79cbb
            *v2 = v4 - v6;
            *(int64_t *)a2 = a2 + 2 + v6;
            if (v5 < 257) {
                // 0x79cdb
                if (*(int64_t *)(a1 + (int64_t)&g361) == 0) {
                    // 0x79ea0
                    function_20230();
                    function_207e0();
                    // 0x79ec4
                    ossl_statem_fatal();
                    result = 0;
                    goto lab_0x79e05;
                } else {
                    int64_t * v7 = (int64_t *)(a1 + (int64_t)&g350); // 0x79ce9
                    function_208d0();
                    int64_t v8 = function_21d20(); // 0x79d1a
                    *(int64_t *)(*v7 + (int64_t)&g72) = v8;
                    if (v8 == 0) {
                        // 0x79ee0
                        function_20230();
                        function_207e0();
                        // 0x79ec4
                        ossl_statem_fatal();
                        result = 0;
                        goto lab_0x79e05;
                    } else {
                        int64_t v9 = *v7; // 0x79d2f
                        if ((int32_t)v9 < 513) {
                            int64_t v10 = v9 & 0xffffffff; // 0x79d54
                            if (v10 == 0) {
                                // 0x79e30
                                function_20230();
                                function_207e0();
                                ossl_statem_fatal();
                                result = 0;
                                goto lab_0x79e05;
                            } else {
                                // 0x79d6a
                                function_208d0();
                                int64_t v11 = function_212a0(); // 0x79d95
                                int64_t * v12 = (int64_t *)(a1 + (int64_t)&g121); // 0x79da0
                                *v12 = v11;
                                function_20720();
                                int64_t * v13 = (int64_t *)(a1 + (int64_t)&g122);
                                if (*v12 == 0) {
                                    // 0x79f40
                                    *v13 = 0;
                                    function_20230();
                                    function_207e0();
                                    // 0x79ec4
                                    ossl_statem_fatal();
                                    result = 0;
                                    goto lab_0x79e05;
                                } else {
                                    // 0x79dbc
                                    *v13 = v10;
                                    result = 1;
                                    goto lab_0x79e05;
                                }
                            }
                        } else {
                            // 0x79f10
                            function_20230();
                            function_207e0();
                            // 0x79ec4
                            ossl_statem_fatal();
                            result = 0;
                            goto lab_0x79e05;
                        }
                    }
                }
            } else {
                // 0x79e70
                function_20230();
                function_207e0();
                // 0x79df4
                ossl_statem_fatal();
                result = 0;
                goto lab_0x79e05;
            }
        }
    }
  lab_0x79e05:
    // 0x79e05
    if (v1 != __readfsqword(40)) {
        // 0x79f75
        return function_20ff0();
    }
    // 0x79e1c
    return result;
}