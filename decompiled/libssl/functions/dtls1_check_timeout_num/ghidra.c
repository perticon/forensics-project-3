undefined8 dtls1_check_timeout_num(SSL *param_1)

{
  int *piVar1;
  X509_EXTENSIONS *pXVar2;
  uint uVar3;
  ulong uVar4;
  BIO *bp;
  undefined8 uVar5;
  
  uVar3 = *(int *)&param_1[1].tlsext_ocsp_exts[0xf].stack.data + 1;
  *(uint *)&param_1[1].tlsext_ocsp_exts[0xf].stack.data = uVar3;
  if (uVar3 < 3) {
    return 0;
  }
  uVar4 = SSL_get_options();
  if ((uVar4 & 0x1000) == 0) {
    bp = SSL_get_wbio(param_1);
    uVar4 = BIO_ctrl(bp,0x2f,0,(void *)0x0);
    pXVar2 = param_1[1].tlsext_ocsp_exts;
    piVar1 = &pXVar2[9].stack.sorted;
    if (uVar4 <= *(ulong *)piVar1 && *(ulong *)piVar1 != uVar4) {
      *(ulong *)&pXVar2[9].stack.sorted = uVar4;
    }
    uVar3 = *(uint *)&pXVar2[0xf].stack.data;
  }
  else {
    uVar3 = *(uint *)&param_1[1].tlsext_ocsp_exts[0xf].stack.data;
  }
  if (uVar3 < 0xd) {
    uVar5 = 0;
  }
  else {
    ERR_new();
    ERR_set_debug("ssl/d1_lib.c",0x17e,"dtls1_check_timeout_num");
    ossl_statem_fatal(param_1,0xffffffff,0x138,0);
    uVar5 = 0xffffffff;
  }
  return uVar5;
}