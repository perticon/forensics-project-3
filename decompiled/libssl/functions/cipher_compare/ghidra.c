uint cipher_compare(long param_1,long param_2)

{
  uint uVar1;
  
  uVar1 = 0;
  if (*(uint *)(param_1 + 0x18) != *(uint *)(param_2 + 0x18)) {
    uVar1 = -(uint)(*(uint *)(param_1 + 0x18) < *(uint *)(param_2 + 0x18)) | 1;
  }
  return uVar1;
}