OSSL_PQUEUE *ossl_pqueue_new(int (*compare)(const void *, const void *))
{
    OSSL_PQUEUE *pq;

    if (compare == NULL)
        return NULL;

    pq = OPENSSL_malloc(sizeof(*pq));
    if (pq == NULL) {
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        return NULL;
    }
    pq->compare = compare;
    pq->hmax = min_nodes;
    pq->htop = 0;
    pq->freelist = 0;
    pq->heap = OPENSSL_malloc(sizeof(*pq->heap) * min_nodes);
    pq->elements = OPENSSL_malloc(sizeof(*pq->elements) * min_nodes);
    if (pq->heap == NULL || pq->elements == NULL) {
        ossl_pqueue_free(pq);
        ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
        return NULL;
    }
    pqueue_add_freelist(pq, 0);
    return pq;
}