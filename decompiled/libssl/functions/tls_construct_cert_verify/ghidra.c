undefined8 tls_construct_cert_verify(int *param_1,undefined8 param_2)

{
  long lVar1;
  long lVar2;
  undefined8 uVar3;
  int iVar4;
  EVP_MD_CTX *ctx;
  long lVar5;
  uchar *sigret;
  undefined8 uVar6;
  long in_FS_OFFSET;
  long local_110;
  undefined8 local_108;
  undefined8 local_100;
  size_t local_f8;
  undefined8 local_f0;
  undefined local_e8 [168];
  long local_40;
  
  lVar1 = *(long *)(param_1 + 0xe0);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_110 = 0;
  local_108 = 0;
  local_100 = 0;
  local_f8 = 0;
  if ((lVar1 == 0) || (*(long *)(param_1 + 0xe2) == 0)) {
    ERR_new();
    uVar6 = 0x11e;
LAB_00174348:
    sigret = (uchar *)0x0;
    ctx = (EVP_MD_CTX *)0x0;
    ERR_set_debug("ssl/statem/statem_lib.c",uVar6,"tls_construct_cert_verify");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    lVar2 = *(long *)(*(long *)(param_1 + 0xe2) + 8);
    if (lVar2 == 0) {
LAB_00174337:
      ERR_new();
      uVar6 = 0x124;
      goto LAB_00174348;
    }
    iVar4 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x26a),lVar1,&local_110);
    if (iVar4 == 0) goto LAB_00174337;
    ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
    if (ctx == (EVP_MD_CTX *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x12a,"tls_construct_cert_verify");
      uVar6 = 0xc0100;
LAB_001745a4:
      sigret = (uchar *)0x0;
      ossl_statem_fatal(param_1,0x50,uVar6,0);
    }
    else {
      iVar4 = get_cert_verify_tbs_data(param_1,local_e8,&local_f0,&local_100);
      if (iVar4 != 0) {
        if ((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 2) != 0) {
          iVar4 = WPACKET_put_bytes__(param_2,*(undefined2 *)(lVar1 + 8),2);
          if (iVar4 == 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x135,"tls_construct_cert_verify");
            uVar6 = 0xc0103;
            goto LAB_001745a4;
          }
        }
        uVar6 = (*(undefined8 **)(param_1 + 0x26a))[0x88];
        uVar3 = **(undefined8 **)(param_1 + 0x26a);
        lVar5 = local_110;
        if (local_110 != 0) {
          lVar5 = EVP_MD_get0_name(local_110);
        }
        iVar4 = EVP_DigestSignInit_ex(ctx,&local_108,lVar5,uVar3,uVar6,lVar2);
        if (iVar4 < 1) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_lib.c",0x13d,"tls_construct_cert_verify");
          uVar6 = 0x80006;
        }
        else if (*(int *)(lVar1 + 0x14) == 0x390) {
          iVar4 = EVP_PKEY_CTX_set_rsa_padding(local_108,6);
          if (0 < iVar4) {
            iVar4 = EVP_PKEY_CTX_set_rsa_pss_saltlen(local_108,0xffffffff);
            if (0 < iVar4) goto LAB_00174465;
          }
          ERR_new();
          ERR_set_debug("ssl/statem/statem_lib.c",0x145,"tls_construct_cert_verify");
          uVar6 = 0x80006;
        }
        else {
LAB_00174465:
          if (*param_1 == 0x300) {
            iVar4 = EVP_DigestSignUpdate(ctx,local_f0,local_100);
            if (0 < iVar4) {
              iVar4 = EVP_MD_CTX_ctrl(ctx,0x1d,*(undefined8 *)(*(long *)(param_1 + 0x246) + 8));
              if (0 < iVar4) {
                iVar4 = EVP_DigestSignFinal(ctx,(uchar *)0x0,&local_f8);
                if (0 < iVar4) {
                  sigret = (uchar *)CRYPTO_malloc((int)local_f8,"ssl/statem/statem_lib.c",0x157);
                  if (sigret != (uchar *)0x0) {
                    iVar4 = EVP_DigestSignFinal(ctx,sigret,&local_f8);
                    if (0 < iVar4) goto LAB_001744d8;
                  }
                  ERR_new();
                  uVar6 = 0x15a;
                  goto LAB_00174611;
                }
              }
            }
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x154,"tls_construct_cert_verify");
            uVar6 = 0x80006;
          }
          else {
            iVar4 = EVP_DigestSign(ctx,0,&local_f8);
            if (0 < iVar4) {
              sigret = (uchar *)CRYPTO_malloc((int)local_f8,"ssl/statem/statem_lib.c",0x166);
              if (sigret != (uchar *)0x0) {
                iVar4 = EVP_DigestSign(ctx,sigret,&local_f8,local_f0,local_100);
                if (0 < iVar4) {
LAB_001744d8:
                  if ((*(int *)(lVar1 + 0x14) - 0x3d3U < 2) || (*(int *)(lVar1 + 0x14) == 0x32b)) {
                    BUF_reverse(sigret,(uchar *)0x0,local_f8);
                  }
                  iVar4 = WPACKET_sub_memcpy__(param_2,sigret,local_f8,2);
                  if (iVar4 == 0) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_lib.c",0x17a,"tls_construct_cert_verify");
                    ossl_statem_fatal(param_1,0x50,0xc0103,0);
                  }
                  else {
                    iVar4 = ssl3_digest_cached_records(param_1,0);
                    if (iVar4 != 0) {
                      CRYPTO_free(sigret);
                      EVP_MD_CTX_free(ctx);
                      uVar6 = 1;
                      goto LAB_0017438e;
                    }
                  }
                  goto LAB_00174370;
                }
              }
              ERR_new();
              uVar6 = 0x169;
LAB_00174611:
              ERR_set_debug("ssl/statem/statem_lib.c",uVar6,"tls_construct_cert_verify");
              ossl_statem_fatal(param_1,0x50,0x80006,0);
              goto LAB_00174370;
            }
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x163,"tls_construct_cert_verify");
            uVar6 = 0x80006;
          }
        }
        goto LAB_001745a4;
      }
      sigret = (uchar *)0x0;
    }
  }
LAB_00174370:
  CRYPTO_free(sigret);
  EVP_MD_CTX_free(ctx);
  uVar6 = 0;
LAB_0017438e:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar6;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}