undefined8 ssl3_release_read_buffer(long param_1)

{
  void *ptr;
  
  ptr = *(void **)(param_1 + 0xc78);
  if ((*(byte *)(param_1 + 0x9e8) & 2) != 0) {
    OPENSSL_cleanse(ptr,*(size_t *)(param_1 + 0xc88));
    ptr = *(void **)(param_1 + 0xc78);
  }
  CRYPTO_free(ptr);
  *(undefined8 *)(param_1 + 0xc78) = 0;
  return 1;
}