int SSL_add_client_CA(SSL *ssl,X509 *x)

{
  int iVar1;
  X509_NAME *pXVar2;
  long lVar3;
  
  if (x == (X509 *)0x0) {
    return 0;
  }
  if (*(long *)&ssl[3].first_packet == 0) {
    lVar3 = OPENSSL_sk_new_null();
    *(long *)&ssl[3].first_packet = lVar3;
    if (lVar3 == 0) {
      return 0;
    }
  }
  pXVar2 = X509_get_subject_name(x);
  pXVar2 = X509_NAME_dup(pXVar2);
  if (pXVar2 == (X509_NAME *)0x0) {
    return 0;
  }
  iVar1 = OPENSSL_sk_push(*(undefined8 *)&ssl[3].first_packet,pXVar2);
  if (iVar1 == 0) {
    X509_NAME_free(pXVar2);
    return 0;
  }
  return 1;
}