undefined4 ssl_get_security_level_bits(undefined8 param_1,long param_2,int *param_3)

{
  int iVar1;
  
  if (param_2 == 0) {
    iVar1 = SSL_get_security_level();
  }
  else {
    iVar1 = SSL_CTX_get_security_level(param_2);
  }
  if (iVar1 < 0) {
    iVar1 = 0;
  }
  if (5 < iVar1) {
    iVar1 = 5;
  }
  if (param_3 != (int *)0x0) {
    *param_3 = iVar1;
  }
  return *(undefined4 *)(minbits_table_27751 + (long)iVar1 * 4);
}