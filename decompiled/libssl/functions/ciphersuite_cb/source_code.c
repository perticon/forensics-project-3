static int ciphersuite_cb(const char *elem, int len, void *arg)
{
    STACK_OF(SSL_CIPHER) *ciphersuites = (STACK_OF(SSL_CIPHER) *)arg;
    const SSL_CIPHER *cipher;
    /* Arbitrary sized temp buffer for the cipher name. Should be big enough */
    char name[80];

    if (len > (int)(sizeof(name) - 1))
        /* Anyway return 1 so we can parse rest of the list */
        return 1;

    memcpy(name, elem, len);
    name[len] = '\0';

    cipher = ssl3_get_cipher_by_std_name(name);
    if (cipher == NULL)
        /* Ciphersuite not found but return 1 to parse rest of the list */
        return 1;

    if (!sk_SSL_CIPHER_push(ciphersuites, cipher)) {
        ERR_raise(ERR_LIB_SSL, ERR_R_INTERNAL_ERROR);
        return 0;
    }

    return 1;
}