int ciphersuite_cb(undefined8 param_1,int param_2,undefined8 param_3)

{
  int iVar1;
  undefined8 uVar2;
  long lVar3;
  long in_FS_OFFSET;
  undefined auStack120 [88];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_2 < 0x50) {
    uVar2 = __memcpy_chk(auStack120,param_1,(long)param_2,0x50);
    auStack120[param_2] = 0;
    lVar3 = ssl3_get_cipher_by_std_name(uVar2);
    if (lVar3 != 0) {
      iVar1 = OPENSSL_sk_push(param_3,lVar3);
      if (iVar1 == 0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_ciph.c",0x52f,"ciphersuite_cb");
        ERR_set_error(0x14,0xc0103,0);
        goto LAB_0012fb57;
      }
    }
  }
  iVar1 = 1;
LAB_0012fb57:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}