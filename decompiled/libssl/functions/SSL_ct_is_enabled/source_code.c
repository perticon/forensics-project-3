int SSL_ct_is_enabled(const SSL *s)
{
    return s->ct_validation_callback != NULL;
}