static int ssl_print_certificates(BIO *bio, const SSL *ssl, int server,
                                  int indent, const unsigned char *msg,
                                  size_t msglen)
{
    size_t clen;

    if (SSL_IS_TLS13(ssl)
            && !ssl_print_hexbuf(bio, indent, "context", 1, &msg, &msglen))
        return 0;

    if (msglen < 3)
        return 0;
    clen = (msg[0] << 16) | (msg[1] << 8) | msg[2];
    if (msglen != clen + 3)
        return 0;
    msg += 3;
    BIO_indent(bio, indent, 80);
    BIO_printf(bio, "certificate_list, length=%d\n", (int)clen);
    while (clen > 0) {
        if (!ssl_print_certificate(bio, indent + 2, &msg, &clen))
            return 0;
        if (SSL_IS_TLS13(ssl)
            && !ssl_print_extensions(bio, indent + 2, server,
                                     SSL3_MT_CERTIFICATE, &msg, &clen))
            return 0;

    }
    return 1;
}