static int cmd_VerifyMode(SSL_CONF_CTX *cctx, const char *value)
{
    static const ssl_flag_tbl ssl_vfy_list[] = {
        SSL_FLAG_VFY_CLI("Peer", SSL_VERIFY_PEER),
        SSL_FLAG_VFY_SRV("Request", SSL_VERIFY_PEER),
        SSL_FLAG_VFY_SRV("Require",
                         SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT),
        SSL_FLAG_VFY_SRV("Once", SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE),
        SSL_FLAG_VFY_SRV("RequestPostHandshake",
                         SSL_VERIFY_PEER | SSL_VERIFY_POST_HANDSHAKE),
        SSL_FLAG_VFY_SRV("RequirePostHandshake",
                         SSL_VERIFY_PEER | SSL_VERIFY_POST_HANDSHAKE |
                         SSL_VERIFY_FAIL_IF_NO_PEER_CERT),
    };
    if (value == NULL)
        return -3;
    cctx->tbl = ssl_vfy_list;
    cctx->ntbl = OSSL_NELEM(ssl_vfy_list);
    return CONF_parse_list(value, ',', 1, ssl_set_option_list, cctx);
}