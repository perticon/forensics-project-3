int SSL_set_session(SSL *to,SSL_SESSION *session)

{
  SSL_METHOD *method;
  int iVar1;
  
  ssl_clear_bad_session();
  method = *(SSL_METHOD **)(*(long *)&to[3].ex_data.dummy + 8);
  if ((method != to->method) && (iVar1 = SSL_set_ssl_method(to,method), iVar1 == 0)) {
    return 0;
  }
  if (session != (SSL_SESSION *)0x0) {
    SSL_SESSION_up_ref(session);
    *(undefined8 *)&to[3].references = *(undefined8 *)(session[1].sid_ctx + 4);
  }
  SSL_SESSION_free(*(SSL_SESSION **)&to[3].sid_ctx_length);
  *(SSL_SESSION **)&to[3].sid_ctx_length = session;
  return 1;
}