int SSL_CTX_load_verify_locations(SSL_CTX *ctx,char *CAfile,char *CApath)

{
  int iVar1;
  uint uVar2;
  
  if (((ulong)CAfile | (ulong)CApath) == 0) {
    return 0;
  }
  if (CAfile != (char *)0x0) {
    iVar1 = SSL_CTX_load_verify_file();
    if (iVar1 == 0) {
      return 0;
    }
  }
  uVar2 = 1;
  if (CApath != (char *)0x0) {
    iVar1 = SSL_CTX_load_verify_dir(ctx,CApath);
    uVar2 = (uint)(iVar1 != 0);
  }
  return uVar2;
}