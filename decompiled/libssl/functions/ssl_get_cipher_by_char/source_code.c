const SSL_CIPHER *ssl_get_cipher_by_char(SSL *ssl, const unsigned char *ptr,
                                         int all)
{
    const SSL_CIPHER *c = ssl->method->get_cipher_by_char(ptr);

    if (c == NULL || (!all && c->valid == 0))
        return NULL;
    return c;
}