int SSL_renegotiate_abbreviated(SSL *s)
{
    if (!can_renegotiate(s))
        return 0;

    s->renegotiate = 1;
    s->new_session = 0;
    return s->method->ssl_renegotiate(s);
}