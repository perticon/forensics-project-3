int SSL_renegotiate_abbreviated(SSL *s)

{
  SSL_METHOD *pSVar1;
  _func_3063 *UNRECOVERED_JUMPTABLE;
  int iVar2;
  
  pSVar1 = s->method;
  if (((byte)pSVar1->get_timeout[0x60] & 8) == 0) {
    if ((0x303 < pSVar1->version) && (pSVar1->version != 0x10000)) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x8f2,"can_renegotiate");
      ERR_set_error(0x14,0x10a,0);
      return 0;
    }
  }
  if ((*(byte *)((long)&s[3].tlsext_debug_cb + 3) & 0x40) == 0) {
    *(undefined4 *)&s[4].expand = 1;
    UNRECOVERED_JUMPTABLE = pSVar1->ssl_renegotiate_check;
    s->new_session = 0;
                    /* WARNING: Could not recover jumptable at 0x0013856d. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    iVar2 = (*UNRECOVERED_JUMPTABLE)();
    return iVar2;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x8f7,"can_renegotiate");
  ERR_set_error(0x14,0x153,0);
  return 0;
}