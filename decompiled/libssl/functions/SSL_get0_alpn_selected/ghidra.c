void SSL_get0_alpn_selected(long param_1,long *param_2,undefined4 *param_3)

{
  long lVar1;
  undefined4 uVar2;
  
  lVar1 = *(long *)(param_1 + 0x488);
  uVar2 = 0;
  *param_2 = lVar1;
  if (lVar1 != 0) {
    uVar2 = *(undefined4 *)(param_1 + 0x490);
  }
  *param_3 = uVar2;
  return;
}