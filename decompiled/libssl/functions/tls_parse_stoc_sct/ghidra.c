bool tls_parse_stoc_sct(long param_1,void **param_2,uint param_3,undefined8 param_4,
                       undefined8 param_5)

{
  void *__n;
  bool bVar1;
  int iVar2;
  long lVar3;
  void *__dest;
  undefined8 uVar4;
  
  if (param_3 == 0x4000) {
    return true;
  }
  if (*(long *)(param_1 + 0xb68) == 0) {
    uVar4 = 0x16338e;
    lVar3 = custom_ext_find(*(long *)(param_1 + 0x898) + 0x1d8,((param_3 & 0x100) == 0) * '\x02',
                            0x12,0);
    if (lVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x5cf,"tls_parse_stoc_sct");
      ossl_statem_fatal(param_1,0x6e,0x6e,0);
      return false;
    }
    iVar2 = custom_ext_parse(param_1,param_3,0x12,*param_2,param_2[1],param_4,param_5,uVar4);
    return iVar2 != 0;
  }
  __n = param_2[1];
  CRYPTO_free(*(void **)(param_1 + 0xa68));
  *(short *)(param_1 + 0xa70) = (short)__n;
  *(undefined8 *)(param_1 + 0xa68) = 0;
  if (__n == (void *)0x0) {
LAB_0016334f:
    bVar1 = true;
  }
  else {
    __dest = CRYPTO_malloc((int)__n,"ssl/statem/extensions_clnt.c",0x5b9);
    *(void **)(param_1 + 0xa68) = __dest;
    if (__dest == (void *)0x0) {
      *(undefined2 *)(param_1 + 0xa70) = 0;
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x5bc,"tls_parse_stoc_sct");
      uVar4 = 0xc0100;
    }
    else {
      if (__n <= param_2[1]) {
        memcpy(__dest,*param_2,(size_t)__n);
        *param_2 = (void *)((long)*param_2 + (long)__n);
        param_2[1] = (void *)((long)param_2[1] - (long)__n);
        goto LAB_0016334f;
      }
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x5c0,"tls_parse_stoc_sct");
      uVar4 = 0xc0103;
    }
    ossl_statem_fatal(param_1,0x50,uVar4,0);
    bVar1 = false;
  }
  return bVar1;
}