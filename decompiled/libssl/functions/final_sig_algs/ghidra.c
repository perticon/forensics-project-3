int final_sig_algs(long param_1,undefined8 param_2,int param_3)

{
  int iVar1;
  
  iVar1 = 1;
  if (param_3 == 0) {
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
      iVar1 = **(int **)(param_1 + 8);
      if (((iVar1 == 0x10000) || (iVar1 < 0x304)) || (iVar1 = *(int *)(param_1 + 0x4d0), iVar1 != 0)
         ) {
        iVar1 = 1;
      }
      else {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions.c",0x4f5,"final_sig_algs");
        ossl_statem_fatal(param_1,0x6d,0x70,0);
      }
    }
  }
  return iVar1;
}