undefined4 tls_parse_ctos_psk(long param_1,ushort **param_2)

{
  byte *pbVar1;
  int iVar2;
  uint uVar3;
  undefined4 uVar4;
  SSL_SESSION *pSVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  time_t tVar8;
  void *ptr;
  long lVar9;
  byte *pbVar10;
  ulong len;
  ushort *puVar11;
  ulong uVar12;
  ushort *puVar13;
  ushort *puVar14;
  uint uVar15;
  ushort uVar16;
  uint *puVar17;
  ushort *puVar18;
  long in_FS_OFFSET;
  uint local_288;
  undefined4 local_284;
  SSL_SESSION *local_258;
  undefined2 local_24a;
  undefined local_248 [520];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_258 = (SSL_SESSION *)0x0;
  if ((*(byte *)(param_1 + 0xb28) & 3) == 0) goto LAB_00167850;
  if ((ushort *)0x1 < param_2[1]) {
    puVar18 = param_2[1] + -1;
    uVar16 = **param_2;
    puVar13 = (ushort *)(ulong)(ushort)(uVar16 << 8 | uVar16 >> 8);
    if (puVar13 <= puVar18) {
      puVar17 = (uint *)(*param_2 + 1);
      param_2[1] = (ushort *)((long)puVar18 - (long)puVar13);
      *param_2 = (ushort *)((long)puVar17 + (long)puVar13);
      *(undefined4 *)(param_1 + 0xa98) = 0;
      if (puVar13 != (ushort *)0x0) {
        uVar6 = 0;
        local_284 = 0;
        local_288 = 0;
        do {
          if (puVar13 == (ushort *)0x1) {
LAB_00167810:
            ERR_new();
            uVar6 = 0x3f1;
LAB_00167821:
            ERR_set_debug("ssl/statem/extensions_srvr.c",uVar6,"tls_parse_ctos_psk");
            ossl_statem_fatal(param_1,0x32,0x6e,0);
            uVar4 = 0;
            goto LAB_001677e5;
          }
          uVar16 = *(ushort *)puVar17 << 8 | *(ushort *)puVar17 >> 8;
          puVar18 = (ushort *)(ulong)uVar16;
          if ((puVar13 + -1 < puVar18) ||
             (uVar12 = (long)(puVar13 + -1) - (long)puVar18, uVar12 < 4)) goto LAB_00167810;
          puVar13 = (ushort *)((long)puVar17 + 2);
          uVar15 = *(uint *)((long)puVar13 + (long)puVar18);
          puVar17 = (uint *)((long)puVar13 + (long)puVar18) + 1;
          if ((*(code **)(param_1 + 0x998) != (code *)0x0) &&
             (iVar2 = (**(code **)(param_1 + 0x998))(param_1,puVar13,puVar18), iVar2 == 0)) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x3f9,"tls_parse_ctos_psk");
            ossl_statem_fatal(param_1,0x50,0x6e,0);
            uVar4 = 0;
            goto LAB_001677e5;
          }
          if (local_258 == (SSL_SESSION *)0x0) {
            if ((*(long *)(param_1 + 0x990) == 0) || ((ushort *)0x100 < puVar18)) {
LAB_001678cf:
              if (((*(ulong *)(param_1 + 0x9e8) & 0x4000) == 0) &&
                 ((*(int *)(param_1 + 0x1d40) == 0 ||
                  ((*(ulong *)(param_1 + 0x9e8) & 0x1000000) != 0)))) {
                uVar3 = tls_decrypt_ticket(param_1,puVar13,puVar18,0,0,&local_258);
                if (uVar3 != 3) {
                  if (1 < uVar3) {
                    pSVar5 = local_258;
                    if ((uVar3 - 2 & 0xfffffffd) != 0) goto LAB_00167928;
                    goto LAB_001679f0;
                  }
                  ERR_new();
                  uVar6 = 0x45c;
                  goto LAB_00167c82;
                }
              }
              else {
                *(undefined4 *)(param_1 + 0xa98) = 1;
                if (uVar16 != 0) {
                  if ((uVar16 == 0x20) &&
                     (pSVar5 = (SSL_SESSION *)lookup_sess_in_cache(param_1,puVar13,0x20),
                     pSVar5 != (SSL_SESSION *)0x0)) {
LAB_00167928:
                    local_258 = pSVar5;
                    if (((*(int *)(param_1 + 0x1d40) == 0) ||
                        ((*(byte *)(param_1 + 0x9eb) & 1) != 0)) ||
                       (iVar2 = SSL_CTX_remove_session(*(SSL_CTX **)(param_1 + 0xb88),local_258),
                       iVar2 != 0)) {
                      tVar8 = time((time_t *)0x0);
                      pSVar5 = local_258;
                      if (((local_288 == 0) &&
                          (uVar3 = (int)tVar8 - *(int *)(local_258[1].sid_ctx + 0x1c),
                          (long)(ulong)uVar3 <= *(long *)(local_258[1].sid_ctx + 0x14))) &&
                         ((uVar3 * 1000) / 1000 == uVar3)) {
                        uVar3 = uVar3 * 1000 + 1000;
                        uVar15 = (uVar15 >> 0x18 | (uVar15 & 0xff0000) >> 8 | (uVar15 & 0xff00) << 8
                                 | uVar15 << 0x18) - *(int *)(local_258[1].krb5_client_princ + 0x68)
                        ;
                        if ((uVar15 <= uVar3) && (uVar3 <= uVar15 + 10000)) {
                          *(undefined4 *)(param_1 + 0xb34) = 1;
                        }
                      }
                      goto LAB_001676a6;
                    }
                    SSL_SESSION_free(local_258);
                    local_258 = (SSL_SESSION *)0x0;
                  }
                  goto LAB_001679f0;
                }
              }
              ERR_new();
              uVar6 = 0x456;
              goto LAB_00167821;
            }
            CRYPTO_free((void *)0x0);
            ptr = (void *)CRYPTO_strndup(puVar13,puVar18,"include/internal/packet.h",0x1dc);
            if (ptr == (void *)0x0) {
              ERR_new();
              uVar6 = 0x406;
            }
            else {
              uVar3 = (**(code **)(param_1 + 0x990))(param_1,ptr,local_248);
              CRYPTO_free(ptr);
              if (uVar3 < 0x201) {
                if (uVar3 != 0) {
                  local_24a = 0x113;
                  lVar9 = SSL_CIPHER_find(param_1,&local_24a);
                  len = (ulong)uVar3;
                  if (lVar9 == 0) {
                    OPENSSL_cleanse(local_248,len);
                    ERR_new();
                    uVar6 = 0x41a;
                    goto LAB_00167c82;
                  }
                  local_258 = SSL_SESSION_new();
                  if ((((local_258 == (SSL_SESSION *)0x0) ||
                       (iVar2 = SSL_SESSION_set1_master_key(local_258,local_248,len), iVar2 == 0))
                      || (iVar2 = SSL_SESSION_set_cipher(local_258,lVar9), iVar2 == 0)) ||
                     (iVar2 = SSL_SESSION_set_protocol_version(local_258,0x304), iVar2 == 0)) {
                    OPENSSL_cleanse(local_248,len);
                    ERR_new();
                    ERR_set_debug("ssl/statem/extensions_srvr.c",0x426,"tls_parse_ctos_psk");
                    ossl_statem_fatal(param_1,0x50,0xc0103,0);
                    goto LAB_001678a3;
                  }
                  OPENSSL_cleanse(local_248,len);
                }
                if (local_258 != (SSL_SESSION *)0x0) goto LAB_0016763e;
                goto LAB_001678cf;
              }
              ERR_new();
              uVar6 = 0x40d;
            }
LAB_00167c82:
            ERR_set_debug("ssl/statem/extensions_srvr.c",uVar6,"tls_parse_ctos_psk");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            uVar4 = 0;
            goto LAB_001677e5;
          }
LAB_0016763e:
          pSVar5 = (SSL_SESSION *)ssl_session_dup(local_258,0);
          if (pSVar5 == (SSL_SESSION *)0x0) {
            ERR_new();
            uVar6 = 0x433;
            goto LAB_00167c82;
          }
          SSL_SESSION_free(local_258);
          local_258 = pSVar5;
          memcpy(pSVar5[1].master_key + 0x14,(void *)(param_1 + 0x8f8),*(size_t *)(param_1 + 0x8f0))
          ;
          *(undefined8 *)(pSVar5[1].master_key + 0xc) = *(undefined8 *)(param_1 + 0x8f0);
          if (local_288 == 0) {
            *(undefined4 *)(param_1 + 0xb34) = 1;
          }
          *(undefined4 *)(param_1 + 0xa98) = 1;
          local_284 = 1;
LAB_001676a6:
          uVar6 = ssl_md(*(undefined8 *)(param_1 + 0x9a8),
                         *(undefined4 *)(*(long *)(pSVar5[1].krb5_client_princ + 0x10) + 0x40));
          uVar7 = ssl_md(*(undefined8 *)(param_1 + 0x9a8),
                         *(undefined4 *)(*(long *)(param_1 + 0x2e0) + 0x40));
          EVP_MD_get0_name(uVar7);
          iVar2 = EVP_MD_is_a(uVar6);
          if (iVar2 != 0) break;
          SSL_SESSION_free(local_258);
          local_258 = (SSL_SESSION *)0x0;
          *(undefined4 *)(param_1 + 0xb34) = 0;
          *(undefined4 *)(param_1 + 0xa98) = 0;
LAB_001679f0:
          local_288 = local_288 + 1;
          puVar13 = (ushort *)(uVar12 - 4);
        } while (puVar13 != (ushort *)0x0);
        if (local_258 != (SSL_SESSION *)0x0) {
          puVar18 = *param_2;
          lVar9 = *(long *)(*(long *)(param_1 + 0x88) + 8);
          iVar2 = EVP_MD_get_size(uVar6);
          if (param_2[1] < (ushort *)0x2) {
LAB_00167870:
            ERR_new();
            uVar6 = 0x49b;
LAB_00167881:
            ERR_set_debug("ssl/statem/extensions_srvr.c",uVar6,"tls_parse_ctos_psk");
            ossl_statem_fatal(param_1,0x32,0x6e,0);
          }
          else {
            puVar13 = param_2[1] + -1;
            uVar16 = **param_2;
            puVar11 = (ushort *)(ulong)(ushort)(uVar16 << 8 | uVar16 >> 8);
            if (puVar13 < puVar11) goto LAB_00167870;
            puVar14 = *param_2 + 1;
            param_2[1] = (ushort *)((long)puVar13 - (long)puVar11);
            *param_2 = (ushort *)((long)puVar14 + (long)puVar11);
            uVar15 = 0;
            do {
              if (puVar11 == (ushort *)0x0) {
LAB_00167a48:
                ERR_new();
                uVar6 = 0x4a1;
                goto LAB_00167881;
              }
              pbVar10 = (byte *)(ulong)*(byte *)puVar14;
              if ((byte *)((long)puVar11 + -1) < pbVar10) goto LAB_00167a48;
              pbVar1 = (byte *)((long)puVar14 + 1);
              puVar11 = (ushort *)((byte *)((long)puVar11 + -1) + -(long)pbVar10);
              uVar15 = uVar15 + 1;
              puVar14 = (ushort *)(pbVar1 + (long)pbVar10);
            } while (uVar15 <= local_288);
            if ((byte *)(long)iVar2 != pbVar10) {
              ERR_new();
              uVar6 = 0x4a7;
              goto LAB_00167881;
            }
            iVar2 = tls_psk_do_binder(param_1,uVar6,*(undefined8 *)(*(long *)(param_1 + 0x88) + 8),
                                      (long)puVar18 - lVar9,pbVar1,0,local_258,0,local_284,
                                      *(long *)(param_1 + 0x88));
            if (iVar2 == 1) {
              *(uint *)(param_1 + 0xb50) = local_288;
              SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x918));
              uVar4 = 1;
              *(SSL_SESSION **)(param_1 + 0x918) = local_258;
              goto LAB_001677e5;
            }
          }
LAB_001678a3:
          SSL_SESSION_free(local_258);
          uVar4 = 0;
          goto LAB_001677e5;
        }
      }
LAB_00167850:
      uVar4 = 1;
      goto LAB_001677e5;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x3e5,"tls_parse_ctos_psk");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
  uVar4 = 0;
LAB_001677e5:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}