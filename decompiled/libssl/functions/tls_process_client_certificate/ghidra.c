uint tls_process_client_certificate(int *param_1,byte **param_2)

{
  byte *a;
  void *b;
  int iVar1;
  undefined4 uVar2;
  long lVar3;
  X509 *pXVar4;
  undefined8 uVar5;
  long lVar6;
  byte *pbVar7;
  ushort *puVar8;
  undefined8 uVar9;
  byte *pbVar10;
  uint uVar11;
  byte *pbVar12;
  long in_FS_OFFSET;
  X509 *local_70;
  ushort *local_68;
  void *local_60;
  ushort *local_58;
  byte *local_50;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  param_1[0x20] = 0;
  local_70 = (X509 *)0x0;
  lVar3 = OPENSSL_sk_new_null();
  if (lVar3 == 0) {
    ERR_new();
    uVar9 = 0xd68;
LAB_0017f621:
    uVar11 = 0;
    ERR_set_debug("ssl/statem/statem_srvr.c",uVar9,"tls_process_client_certificate");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
    goto LAB_0017f526;
  }
  pbVar7 = *param_2;
  pbVar10 = param_2[1];
  uVar11 = *(uint *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8;
  if (((uVar11 != 0) || (iVar1 = **(int **)(param_1 + 2), iVar1 < 0x304)) || (iVar1 == 0x10000))
  goto LAB_0017f1d0;
  if (pbVar10 != (byte *)0x0) {
    pbVar12 = (byte *)(ulong)*pbVar7;
    if (pbVar12 <= pbVar10 + -1) {
      a = pbVar7 + 1;
      b = *(void **)(param_1 + 0x2ec);
      pbVar10 = pbVar10 + -1 + -(long)pbVar12;
      pbVar7 = a + (long)pbVar12;
      param_2[1] = pbVar10;
      *param_2 = pbVar7;
      if (b == (void *)0x0) {
        if (pbVar12 == (byte *)0x0) goto LAB_0017f1d0;
      }
      else if ((*(byte **)(param_1 + 0x2ee) == pbVar12) &&
              (iVar1 = CRYPTO_memcmp(a,b,(size_t)pbVar12), iVar1 == 0)) {
        pbVar7 = *param_2;
        pbVar10 = param_2[1];
LAB_0017f1d0:
        if ((byte *)0x2 < pbVar10) {
          puVar8 = (ushort *)(pbVar7 + 3);
          pbVar7 = (byte *)((ulong)*pbVar7 << 0x10 | (ulong)pbVar7[1] << 8 | (ulong)pbVar7[2]);
          if (pbVar7 <= pbVar10 + -3) {
            pbVar10 = pbVar10 + -3 + -(long)pbVar7;
            *param_2 = (byte *)((long)puVar8 + (long)pbVar7);
            param_2[1] = pbVar10;
            if (pbVar10 == (byte *)0x0) {
              if (pbVar7 != (byte *)0x0) {
                do {
                  if (pbVar7 < (byte *)0x3) {
LAB_0017f560:
                    ERR_new();
                    uVar9 = 0xd7d;
LAB_0017f571:
                    ERR_set_debug("ssl/statem/statem_srvr.c",uVar9,"tls_process_client_certificate")
                    ;
                    uVar9 = 0x87;
                    goto LAB_0017f514;
                  }
                  pbVar12 = (byte *)((ulong)*(byte *)puVar8 << 0x10 |
                                     (ulong)*(byte *)((long)puVar8 + 1) << 8 |
                                    (ulong)*(byte *)(puVar8 + 1));
                  if (pbVar7 + -3 < pbVar12) goto LAB_0017f560;
                  local_68 = (ushort *)((long)puVar8 + 3);
                  pbVar7 = pbVar7 + -3 + -(long)pbVar12;
                  puVar8 = (ushort *)((long)local_68 + (long)pbVar12);
                  local_70 = (X509 *)X509_new_ex(**(undefined8 **)(param_1 + 0x26a),
                                                 (*(undefined8 **)(param_1 + 0x26a))[0x88]);
                  if (local_70 == (X509 *)0x0) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_srvr.c",0xd84,"tls_process_client_certificate")
                    ;
                    uVar9 = 0xc0100;
                    goto LAB_0017f514;
                  }
                  pXVar4 = d2i_X509(&local_70,(uchar **)&local_68,(long)pbVar12);
                  if (pXVar4 == (X509 *)0x0) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_srvr.c",0xd88,"tls_process_client_certificate")
                    ;
                    uVar9 = 0x8000d;
                    goto LAB_0017f514;
                  }
                  if (local_68 != puVar8) {
                    ERR_new();
                    uVar9 = 0xd8d;
                    goto LAB_0017f571;
                  }
                  uVar11 = *(uint *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8;
                  if (((uVar11 == 0) && (iVar1 = **(int **)(param_1 + 2), 0x303 < iVar1)) &&
                     (iVar1 != 0x10000)) {
                    local_60 = (void *)0x0;
                    if ((byte *)0x1 < pbVar7) {
                      pbVar12 = (byte *)(ulong)(ushort)(*puVar8 << 8 | *puVar8 >> 8);
                      if (pbVar12 <= pbVar7 + -2) {
                        pbVar7 = pbVar7 + -2 + -(long)pbVar12;
                        local_58 = puVar8 + 1;
                        puVar8 = (ushort *)((long)local_58 + (long)pbVar12);
                        local_50 = pbVar12;
                        iVar1 = tls_collect_extensions
                                          (param_1,&local_58,0x1000,&local_60,0,
                                           pbVar10 == (byte *)0x0);
                        if ((iVar1 != 0) &&
                           (iVar1 = tls_parse_all_extensions(param_1,0x1000,local_60), iVar1 != 0))
                        {
                          CRYPTO_free(local_60);
                          goto LAB_0017f3c0;
                        }
                        CRYPTO_free(local_60);
                        goto LAB_0017f526;
                      }
                    }
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_srvr.c",0xd96,"tls_process_client_certificate")
                    ;
                    ossl_statem_fatal(param_1,0x32,0x10f,0);
                    goto LAB_0017f526;
                  }
LAB_0017f3c0:
                  iVar1 = OPENSSL_sk_push(lVar3,local_70);
                  if (iVar1 == 0) {
                    ERR_new();
                    uVar9 = 0xda6;
                    goto LAB_0017f621;
                  }
                  local_70 = (X509 *)0x0;
                  pbVar10 = pbVar10 + 1;
                } while (pbVar7 != (byte *)0x0);
              }
              iVar1 = OPENSSL_sk_num(lVar3);
              if (iVar1 < 1) {
                if (*param_1 == 0x300) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0xdaf,"tls_process_client_certificate");
                  uVar9 = 0xb0;
LAB_0017f710:
                  uVar11 = 0;
                  ossl_statem_fatal(param_1,0x28,uVar9,0);
                  goto LAB_0017f526;
                }
                if ((param_1[0x25a] & 3U) == 3) {
                  ERR_new();
                  uVar11 = 0;
                  ERR_set_debug("ssl/statem/statem_srvr.c",0xdb6,"tls_process_client_certificate");
                  ossl_statem_fatal(param_1,0x74,199,0);
                  goto LAB_0017f526;
                }
                if (*(long *)(param_1 + 0x62) != 0) {
                  uVar11 = 0;
                  iVar1 = ssl3_digest_cached_records(param_1,0);
                  if (iVar1 == 0) goto LAB_0017f526;
                }
              }
              else {
                iVar1 = ssl_verify_cert_chain(param_1,lVar3);
                if (iVar1 < 1) {
                  ERR_new();
                  uVar11 = 0;
                  ERR_set_debug("ssl/statem/statem_srvr.c",0xdc3,"tls_process_client_certificate");
                  uVar2 = ssl_x509err2alert(param_1[0x26e]);
                  ossl_statem_fatal(param_1,uVar2,0x86,0);
                  goto LAB_0017f526;
                }
                uVar9 = OPENSSL_sk_value(lVar3,0);
                lVar6 = X509_get0_pubkey(uVar9);
                if (lVar6 == 0) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0xdc9,"tls_process_client_certificate");
                  uVar9 = 0xf7;
                  goto LAB_0017f710;
                }
              }
              lVar6 = *(long *)(param_1 + 0x246);
              if (param_1[0x2ea] == 4) {
                lVar6 = ssl_session_dup(lVar6,0);
                if (lVar6 == 0) {
                  ERR_new();
                  uVar9 = 0xdd9;
                  goto LAB_0017f621;
                }
                SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x246));
                *(long *)(param_1 + 0x246) = lVar6;
              }
              X509_free(*(X509 **)(lVar6 + 0x2b8));
              lVar6 = *(long *)(param_1 + 0x246);
              uVar5 = OPENSSL_sk_shift(lVar3);
              uVar9 = *(undefined8 *)(param_1 + 0x26e);
              *(undefined8 *)(lVar6 + 0x2b8) = uVar5;
              lVar6 = *(long *)(param_1 + 0x246);
              *(undefined8 *)(lVar6 + 0x2c8) = uVar9;
              OSSL_STACK_OF_X509_free(*(undefined8 *)(lVar6 + 0x2c0));
              *(long *)(*(long *)(param_1 + 0x246) + 0x2c0) = lVar3;
              uVar11 = *(uint *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8;
              if (((uVar11 == 0) && (iVar1 = **(int **)(param_1 + 2), 0x303 < iVar1)) &&
                 (iVar1 != 0x10000)) {
                iVar1 = ssl3_digest_cached_records(param_1,1);
                if (iVar1 == 0) {
LAB_0017f8a6:
                  lVar3 = 0;
                  goto LAB_0017f526;
                }
                if ((((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0) &&
                    (iVar1 = **(int **)(param_1 + 2), 0x303 < iVar1)) && (iVar1 != 0x10000)) {
                  iVar1 = ssl_handshake_hash(param_1,param_1 + 0x228,0x40,param_1 + 0x238);
                  if (iVar1 == 0) goto LAB_0017f8a6;
                  *(undefined8 *)(param_1 + 0x75e) = 0;
                }
              }
              lVar3 = 0;
              uVar11 = 3;
              goto LAB_0017f526;
            }
          }
        }
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0xd76,"tls_process_client_certificate");
        uVar9 = 0x9f;
LAB_0017f514:
        uVar11 = 0;
        ossl_statem_fatal(param_1,0x32,uVar9,0);
        goto LAB_0017f526;
      }
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_srvr.c",0xd70,"tls_process_client_certificate");
  ossl_statem_fatal(param_1,0x32,0x11a,0);
LAB_0017f526:
  X509_free(local_70);
  OSSL_STACK_OF_X509_free(lVar3);
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar11;
}