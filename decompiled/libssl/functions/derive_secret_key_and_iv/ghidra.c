derive_secret_key_and_iv
          (long param_1,int param_2,undefined8 param_3,EVP_CIPHER *param_4,undefined8 param_5,
          undefined8 param_6,undefined8 param_7,undefined8 param_8,undefined8 param_9,
          uchar *param_10,undefined8 param_11,EVP_CIPHER_CTX *param_12)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  long lVar4;
  undefined8 uVar5;
  char cVar6;
  int local_44;
  
  iVar1 = EVP_MD_get_size(param_3);
  if (iVar1 < 0) {
    ERR_new();
    uVar5 = 0x158;
  }
  else {
    iVar1 = tls13_hkdf_expand(param_1,param_3,param_5,param_7,param_8,param_6,(long)iVar1,param_9,
                              (long)iVar1,1);
    if (iVar1 == 0) {
      return 0;
    }
    iVar1 = EVP_CIPHER_get_key_length(param_4);
    iVar2 = EVP_CIPHER_get_mode(param_4);
    if (iVar2 == 7) {
      lVar4 = *(long *)(param_1 + 0x2e0);
      if (((lVar4 == 0) && (lVar4 = *(long *)(*(long *)(param_1 + 0x918) + 0x2f8), lVar4 == 0)) &&
         ((*(long *)(param_1 + 0x920) == 0 ||
          (lVar4 = *(long *)(*(long *)(param_1 + 0x920) + 0x2f8), lVar4 == 0)))) {
        ERR_new();
        uVar5 = 0x171;
        goto LAB_001515c9;
      }
      local_44 = 0xc;
      uVar3 = *(uint *)(lVar4 + 0x24) & 0x30000;
      iVar2 = (-(uint)(uVar3 == 0) & 8) + 8;
      cVar6 = (-(uVar3 == 0) & 8U) + 8;
    }
    else {
      iVar2 = 0;
      cVar6 = '\0';
      local_44 = EVP_CIPHER_get_iv_length(param_4);
    }
    iVar1 = tls13_derive_key(param_1,param_3,param_9,param_10,(long)iVar1);
    if (iVar1 == 0) {
      return 0;
    }
    iVar1 = tls13_derive_iv(param_1,param_3,param_9);
    if (iVar1 == 0) {
      return 0;
    }
    iVar1 = EVP_CipherInit_ex(param_12,param_4,(ENGINE *)0x0,(uchar *)0x0,(uchar *)0x0,param_2);
    if (((0 < iVar1) && (iVar1 = EVP_CIPHER_CTX_ctrl(param_12,9,local_44,(void *)0x0), 0 < iVar1))
       && (((cVar6 == '\0' ||
            (iVar1 = EVP_CIPHER_CTX_ctrl(param_12,0x11,iVar2,(void *)0x0), 0 < iVar1)) &&
           (iVar1 = EVP_CipherInit_ex(param_12,(EVP_CIPHER *)0x0,(ENGINE *)0x0,param_10,(uchar *)0x0
                                      ,-1), 0 < iVar1)))) {
      return 1;
    }
    ERR_new();
    uVar5 = 0x188;
  }
LAB_001515c9:
  ERR_set_debug("ssl/tls13_enc.c",uVar5,"derive_secret_key_and_iv");
  ossl_statem_fatal(param_1,0x50,0x80006,0);
  return 0;
}