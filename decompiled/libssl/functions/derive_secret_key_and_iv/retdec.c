int64_t derive_secret_key_and_iv(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12) {
    // 0x513f0
    if ((int32_t)function_204a0() < 0) {
        // 0x515b8
        function_20230();
        // 0x515c9
        function_207e0();
        ossl_statem_fatal();
        // 0x51457
        return 0;
    }
    // 0x5141f
    if ((int32_t)tls13_hkdf_expand() == 0) {
        // 0x51457
        return 0;
    }
    // 0x51470
    function_211a0();
    int64_t v1; // 0x513f0
    if ((int32_t)function_21b00() != 7) {
        // 0x515f0
        function_21be0();
        v1 = 0;
        goto lab_0x514dc;
    } else {
        // 0x51496
        v1 = 1;
        if (*(int64_t *)(a1 + (int64_t)&g80) == 0) {
            int64_t v2 = *(int64_t *)(a1 + (int64_t)&g350); // 0x51638
            v1 = 1;
            if (*(int64_t *)(v2 + (int64_t)&g84) != 0) {
                goto lab_0x514dc;
            } else {
                int64_t v3 = *(int64_t *)(a1 + (int64_t)&g351); // 0x51650
                if (v3 == 0) {
                    goto lab_0x5166d;
                } else {
                    // 0x5165d
                    if (*(int64_t *)(v3 + (int64_t)&g84) != 0) {
                        goto lab_0x514dc;
                    } else {
                        goto lab_0x5166d;
                    }
                }
            }
        } else {
            goto lab_0x514dc;
        }
    }
  lab_0x514dc:
    // 0x514dc
    if ((int32_t)tls13_derive_key() == 0 || (int32_t)tls13_derive_iv() == 0) {
        // 0x51457
        return 0;
    }
    // 0x5151e
    if ((int32_t)function_20e40() < 1) {
        goto lab_0x51620;
    } else {
        // 0x51540
        if ((int32_t)function_21650() < 1) {
            goto lab_0x51620;
        } else {
            // 0x51560
            if (v1 == 0) {
                goto lab_0x51584;
            } else {
                // 0x51565
                if ((int32_t)function_21650() < 1) {
                    goto lab_0x51620;
                } else {
                    goto lab_0x51584;
                }
            }
        }
    }
  lab_0x51620:
    // 0x51620
    function_20230();
    // 0x515c9
    function_207e0();
    ossl_statem_fatal();
    // 0x51457
    return 0;
  lab_0x5166d:
    // 0x5166d
    function_20230();
    // 0x515c9
    function_207e0();
    ossl_statem_fatal();
    // 0x51457
    return 0;
  lab_0x51584:
    // 0x51584
    if ((int32_t)function_20e40() >= 1) {
        // 0x51457
        return 1;
    }
    goto lab_0x51620;
}