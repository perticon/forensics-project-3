int32_t derive_secret_key_and_iv(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12, void** a13) {
    int64_t rax14;
    int32_t eax15;
    int32_t eax16;
    int32_t eax17;
    int64_t r8_18;
    int64_t r14_19;
    void** eax20;
    int64_t v21;
    int32_t eax22;
    int32_t eax23;
    int32_t eax24;
    uint32_t eax25;
    int32_t eax26;
    int32_t eax27;
    uint32_t eax28;
    void** rax29;
    void** eax30;
    uint64_t r14_31;
    int64_t r14_32;

    rax14 = fun_204a0(rdx, rsi);
    if (*reinterpret_cast<int32_t*>(&rax14) < 0) {
        fun_20230();
    } else {
        eax15 = tls13_hkdf_expand(rdi, rdx, r8, a7, a8, rdi, rdx, r8, a7, a8);
        if (!eax15) 
            goto addr_51455_4;
        eax16 = fun_211a0(rcx, rcx);
        eax17 = fun_21b00(rcx, rcx);
        r8_18 = eax16;
        if (eax17 != 7) 
            goto addr_515f0_6; else 
            goto addr_51496_7;
    }
    addr_515c9_8:
    fun_207e0("ssl/tls13_enc.c", "ssl/tls13_enc.c");
    ossl_statem_fatal(rdi, rdi);
    goto addr_51455_4;
    addr_515f0_6:
    *reinterpret_cast<int32_t*>(&r14_19) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_19) + 4) = 0;
    eax20 = fun_21be0(rcx, rcx);
    r8_18 = r8_18;
    v21 = reinterpret_cast<int32_t>(eax20);
    addr_514dc_9:
    eax22 = tls13_derive_key(rdi, rdx, a9, a10, r8_18, r9);
    if (!eax22 || (eax23 = tls13_derive_iv(rdi, rdx, a9, a11, v21, r9), eax23 == 0)) {
        addr_51455_4:
        eax24 = 0;
    } else {
        eax25 = fun_20e40(a12, a12);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax25 == 0) || ((eax26 = fun_21650(a12, 9, a12, 9), reinterpret_cast<uint1_t>(eax26 < 0) | reinterpret_cast<uint1_t>(eax26 == 0)) || (r14_19 && (eax27 = fun_21650(a12, 17, a12, 17), reinterpret_cast<uint1_t>(eax27 < 0) | reinterpret_cast<uint1_t>(eax27 == 0)) || (eax28 = fun_20e40(a12, a12), reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax28) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax28 == 0))))) {
            fun_20230();
            goto addr_515c9_8;
        } else {
            eax24 = 1;
        }
    }
    return eax24;
    addr_51496_7:
    rax29 = *reinterpret_cast<void***>(rdi + 0x2e0);
    if (rax29 || ((rax29 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 0x918)) + reinterpret_cast<uint64_t>("@")), !!rax29) || *reinterpret_cast<void***>(rdi + 0x920) && (rax29 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 0x920)) + reinterpret_cast<uint64_t>("@")), !!rax29))) {
        eax30 = *reinterpret_cast<void***>(rax29 + 36);
        v21 = 12;
        r14_31 = reinterpret_cast<unsigned char>(r8) - (reinterpret_cast<unsigned char>(r8) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r8) + static_cast<uint64_t>(reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(eax30) & 0x30000) < 1))))));
        *reinterpret_cast<uint32_t*>(&r14_32) = *reinterpret_cast<uint32_t*>(&r14_31) & 8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_32) + 4) = 0;
        r14_19 = r14_32 + 8;
        goto addr_514dc_9;
    } else {
        fun_20230();
        goto addr_515c9_8;
    }
}