pitem * pitem_new(uchar *prio64be,void *data)

{
  undefined8 uVar1;
  pitem *ppVar2;
  
  ppVar2 = (pitem *)CRYPTO_malloc(0x18,"ssl/pqueue.c",0x14);
  if (ppVar2 == (pitem *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/pqueue.c",0x17,"pitem_new");
    ERR_set_error(0x14,0xc0100,0);
  }
  else {
    uVar1 = *(undefined8 *)prio64be;
    ppVar2->data = data;
    ppVar2->next = (_pitem *)0x0;
    *(undefined8 *)ppVar2->priority = uVar1;
  }
  return ppVar2;
}