const char *OPENSSL_cipher_name(const char *stdname)
{
    const SSL_CIPHER *c;

    if (stdname == NULL)
        return "(NONE)";
    c = ssl3_get_cipher_by_std_name(stdname);
    return SSL_CIPHER_get_name(c);
}