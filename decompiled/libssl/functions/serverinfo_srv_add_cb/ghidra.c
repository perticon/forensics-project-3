serverinfo_srv_add_cb
          (undefined8 param_1,uint param_2,long *param_3,ulong *param_4,undefined4 *param_5)

{
  ushort *puVar1;
  long lVar2;
  undefined8 uVar3;
  ulong uVar4;
  long lVar5;
  ulong uVar6;
  long in_FS_OFFSET;
  long local_40;
  ulong local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_40 = 0;
  local_38 = 0;
  uVar3 = ssl_get_server_cert_serverinfo(param_1,&local_40,&local_38);
  if ((int)uVar3 != 0) {
    *param_3 = 0;
    *param_4 = 0;
    if ((local_40 == 0) || (uVar4 = local_38, lVar5 = local_40, (long)local_38 < 1)) {
LAB_00140248:
      *param_5 = 0x50;
      uVar3 = 0xffffffff;
    }
    else {
      do {
        if (uVar4 < 8) goto LAB_00140248;
        uVar6 = (ulong)(ushort)(*(ushort *)(lVar5 + 6) << 8 | *(ushort *)(lVar5 + 6) >> 8);
        if (uVar4 - 8 < uVar6) goto LAB_00140248;
        puVar1 = (ushort *)(lVar5 + 4);
        lVar2 = lVar5 + 8;
        uVar4 = (uVar4 - 8) - uVar6;
        lVar5 = lVar2 + uVar6;
        if (param_2 == (ushort)(*puVar1 << 8 | *puVar1 >> 8)) {
          *param_3 = lVar2;
          uVar3 = 1;
          *param_4 = uVar6;
          goto LAB_001401bf;
        }
      } while (uVar4 != 0);
      uVar3 = 0;
    }
  }
LAB_001401bf:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}