int ssl3_shutdown(long param_1)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  long in_FS_OFFSET;
  undefined8 uVar4;
  undefined local_28 [8];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(int *)(param_1 + 0x40) == 0) {
    uVar4 = 0x12aafd;
    iVar1 = SSL_in_before();
    if (iVar1 == 0) {
      uVar3 = *(uint *)(param_1 + 0x44);
      if ((uVar3 & 1) == 0) {
        *(uint *)(param_1 + 0x44) = uVar3 | 1;
        ssl3_send_alert(param_1,1,0);
        if (*(int *)(param_1 + 0x1a4) != 0) {
LAB_0012ab6d:
          iVar1 = -1;
          goto LAB_0012aad4;
        }
        uVar3 = *(uint *)(param_1 + 0x44);
LAB_0012aba2:
        if (uVar3 != 3) goto LAB_0012aad4;
      }
      else {
        if (*(int *)(param_1 + 0x1a4) == 0) {
          if ((uVar3 & 2) != 0) goto LAB_0012aba2;
          (**(code **)(*(long *)(param_1 + 8) + 0x68))(param_1,0,0,0,0,0,local_28,uVar4);
          uVar3 = *(uint *)(param_1 + 0x44);
          if ((uVar3 & 2) == 0) goto LAB_0012ab6d;
        }
        else {
          iVar2 = (**(code **)(*(long *)(param_1 + 8) + 0x78))(param_1);
          if (iVar2 == -1) goto LAB_0012ab6d;
          uVar3 = *(uint *)(param_1 + 0x44);
        }
        if ((uVar3 != 3) || (*(int *)(param_1 + 0x1a4) != 0)) goto LAB_0012aad4;
      }
      iVar1 = 1;
      goto LAB_0012aad4;
    }
  }
  *(undefined4 *)(param_1 + 0x44) = 3;
  iVar1 = 1;
LAB_0012aad4:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}