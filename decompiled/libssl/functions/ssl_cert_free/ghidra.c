void ssl_cert_free(void *param_1)

{
  int *piVar1;
  int iVar2;
  
  if (param_1 == (void *)0x0) {
    return;
  }
  LOCK();
  piVar1 = (int *)((long)param_1 + 0x208);
  iVar2 = *piVar1;
  *piVar1 = *piVar1 + -1;
  if ((iVar2 != 1) && (0 < iVar2 + -1)) {
    return;
  }
  EVP_PKEY_free(*(EVP_PKEY **)((long)param_1 + 8));
  ssl_cert_clear_certs(param_1);
  CRYPTO_free(*(void **)((long)param_1 + 0x198));
  CRYPTO_free(*(void **)((long)param_1 + 0x1a8));
  CRYPTO_free(*(void **)((long)param_1 + 0x188));
  X509_STORE_free(*(X509_STORE **)((long)param_1 + 0x1d0));
  X509_STORE_free(*(X509_STORE **)((long)param_1 + 0x1c8));
  custom_exts_free((long)param_1 + 0x1d8);
  CRYPTO_free(*(void **)((long)param_1 + 0x200));
  CRYPTO_THREAD_lock_free(*(undefined8 *)((long)param_1 + 0x210));
  CRYPTO_free(param_1);
  return;
}