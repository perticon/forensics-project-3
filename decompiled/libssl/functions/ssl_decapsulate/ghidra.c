ssl_decapsulate(long param_1,long param_2,undefined8 param_3,undefined8 param_4,int param_5)

{
  int iVar1;
  undefined4 uVar2;
  EVP_PKEY_CTX *ctx;
  void *pvVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_48 = 0;
  if (param_2 == 0) {
    ERR_new();
    ERR_set_debug("ssl/s3_lib.c",0x12fd,"ssl_decapsulate");
    uVar2 = 0;
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    goto LAB_0012bad2;
  }
  ctx = (EVP_PKEY_CTX *)
        EVP_PKEY_CTX_new_from_pkey
                  (**(undefined8 **)(param_1 + 0x9a8),param_2,
                   (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
  iVar1 = EVP_PKEY_decapsulate_init(ctx,0);
  if (iVar1 < 1) {
LAB_0012ba78:
    ERR_new();
    pvVar3 = (void *)0x0;
    ERR_set_debug("ssl/s3_lib.c",0x1305,"ssl_decapsulate");
    uVar2 = 0;
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    iVar1 = EVP_PKEY_decapsulate(ctx,0,&local_48,param_3,param_4);
    if (iVar1 < 1) goto LAB_0012ba78;
    pvVar3 = CRYPTO_malloc((int)local_48,"ssl/s3_lib.c",0x1309);
    if (pvVar3 == (void *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/s3_lib.c",0x130b,"ssl_decapsulate");
      uVar4 = 0xc0100;
LAB_0012bb84:
      uVar2 = 0;
      ossl_statem_fatal(param_1,0x50,uVar4,0);
    }
    else {
      iVar1 = EVP_PKEY_decapsulate(ctx,pvVar3,&local_48,param_3,param_4);
      if (iVar1 < 1) {
        ERR_new();
        ERR_set_debug("ssl/s3_lib.c",0x1310,"ssl_decapsulate");
        uVar4 = 0xc0103;
        goto LAB_0012bb84;
      }
      if (param_5 == 0) {
        *(void **)(param_1 + 0x360) = pvVar3;
        pvVar3 = (void *)0x0;
        *(undefined8 *)(param_1 + 0x368) = local_48;
        uVar2 = 1;
      }
      else {
        uVar2 = ssl_gensecret(param_1,pvVar3,local_48);
      }
    }
  }
  CRYPTO_clear_free(pvVar3,local_48,"ssl/s3_lib.c",0x1320);
  EVP_PKEY_CTX_free(ctx);
LAB_0012bad2:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}