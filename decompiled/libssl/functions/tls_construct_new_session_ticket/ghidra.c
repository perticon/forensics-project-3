int tls_construct_new_session_ticket(long param_1,undefined8 param_2)

{
  uint uVar1;
  ulong uVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  code *pcVar6;
  uchar *in;
  EVP_CIPHER_CTX *ctx;
  long lVar7;
  SSL_SESSION *in_00;
  time_t tVar8;
  EVP_CIPHER *cipher;
  char *pcVar9;
  undefined8 uVar10;
  long in_FS_OFFSET;
  long lVar11;
  int local_f0;
  uchar *local_d8;
  int local_c8;
  int local_c4;
  uchar *local_c0;
  uchar *local_b8;
  uchar *local_b0;
  long local_a8;
  long local_a0;
  uchar *local_98;
  ulong local_90;
  long local_88;
  long local_80;
  uint local_74;
  ulong local_70;
  uchar local_68 [16];
  undefined4 local_58;
  undefined4 uStack84;
  undefined4 uStack80;
  undefined4 uStack76;
  long local_40;
  
  lVar11 = *(long *)(param_1 + 0xb88);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_74 = 0;
  uVar1 = *(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60);
  if ((uVar1 & 8) == 0) {
    iVar3 = **(int **)(param_1 + 8);
    if ((iVar3 == 0x10000) || (iVar3 < 0x304)) {
      pcVar6 = *(code **)(lVar11 + 0x3f8);
      uVar1 = uVar1 & 8;
      if (pcVar6 != (code *)0x0) goto LAB_0017a101;
      goto LAB_0017a1fa;
    }
    uVar10 = ssl_handshake_md();
    iVar3 = EVP_MD_get_size(uVar10);
    if (iVar3 < 0) {
      ERR_new();
      pcVar9 = "tls_construct_new_session_ticket";
      uVar10 = 0xf2f;
LAB_0017a8ac:
      ERR_set_debug("ssl/statem/statem_srvr.c",uVar10,pcVar9);
      uVar10 = 0xc0103;
LAB_0017a8bf:
      ossl_statem_fatal(param_1,0x50,uVar10,0);
    }
    else {
      lVar7 = *(long *)(param_1 + 0x918);
      if ((*(long *)(param_1 + 0x1d78) == 0) && (*(int *)(param_1 + 0x4d0) == 0)) {
LAB_0017a5c0:
        iVar4 = ssl_generate_session_id(param_1,lVar7);
        if (iVar4 != 0) {
          iVar4 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),&local_74,4,0);
          if (iVar4 < 1) {
            ERR_new();
            pcVar9 = "tls_construct_new_session_ticket";
            uVar10 = 0xf4b;
            goto LAB_0017a8ac;
          }
          lVar7 = *(long *)(param_1 + 0x918);
          *(uint *)(lVar7 + 0x350) = local_74;
          uVar2 = *(ulong *)(param_1 + 0x1d80);
          local_70 = uVar2 >> 0x38 | (uVar2 & 0xff000000000000) >> 0x28 |
                     (uVar2 & 0xff0000000000) >> 0x18 | (uVar2 & 0xff00000000) >> 8 |
                     (uVar2 & 0xff000000) << 8 | (uVar2 & 0xff0000) << 0x18 |
                     (uVar2 & 0xff00) << 0x28 | uVar2 << 0x38;
          iVar4 = tls13_hkdf_expand(param_1,uVar10,param_1 + 0x604,"resumption",10,&local_70,8,
                                    lVar7 + 0x50,(long)iVar3,1);
          if (iVar4 == 0) goto LAB_0017a1a0;
          lVar7 = *(long *)(param_1 + 0x918);
          *(long *)(lVar7 + 8) = (long)iVar3;
          tVar8 = time((time_t *)0x0);
          uVar10 = *(undefined8 *)(param_1 + 0x918);
          *(time_t *)(lVar7 + 0x2e0) = tVar8;
          ssl_session_calculate_timeout(uVar10);
          lVar7 = *(long *)(param_1 + 0x918);
          if (*(long *)(param_1 + 0x488) == 0) {
LAB_0017a6fc:
            *(undefined4 *)(lVar7 + 0x354) = *(undefined4 *)(param_1 + 0x1d40);
            pcVar6 = *(code **)(lVar11 + 0x3f8);
            if (pcVar6 != (code *)0x0) goto LAB_0017a101;
            goto LAB_0017a118;
          }
          CRYPTO_free(*(void **)(lVar7 + 0x358));
          lVar7 = *(long *)(param_1 + 0x918);
          uVar10 = CRYPTO_memdup(*(undefined8 *)(param_1 + 0x488),*(undefined8 *)(param_1 + 0x490),
                                 "ssl/statem/statem_srvr.c");
          *(undefined8 *)(lVar7 + 0x358) = uVar10;
          lVar7 = *(long *)(param_1 + 0x918);
          if (*(long *)(lVar7 + 0x358) != 0) {
            *(undefined8 *)(lVar7 + 0x360) = *(undefined8 *)(param_1 + 0x490);
            goto LAB_0017a6fc;
          }
          *(undefined8 *)(lVar7 + 0x360) = 0;
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0xf6a,"tls_construct_new_session_ticket");
          uVar10 = 0xc0100;
          goto LAB_0017a8bf;
        }
      }
      else {
        lVar7 = ssl_session_dup(lVar7,0);
        if (lVar7 != 0) {
          SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x918));
          *(long *)(param_1 + 0x918) = lVar7;
          goto LAB_0017a5c0;
        }
      }
    }
  }
  else {
    pcVar6 = *(code **)(lVar11 + 0x3f8);
    if (pcVar6 == (code *)0x0) {
      uVar1 = 0;
      goto LAB_0017a1fa;
    }
LAB_0017a101:
    iVar3 = (*pcVar6)(param_1);
    if (iVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0xf74,"tls_construct_new_session_ticket");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      goto LAB_0017a1a3;
    }
LAB_0017a118:
    uVar1 = local_74;
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
      iVar3 = **(int **)(param_1 + 8);
      if ((iVar3 == 0x10000) || (iVar3 < 0x304)) {
        lVar11 = *(long *)(param_1 + 0xb88);
        goto LAB_0017a1fa;
      }
      if (((*(ulong *)(param_1 + 0x9e8) & 0x4000) == 0) &&
         ((*(int *)(param_1 + 0x1d40) == 0 || ((*(ulong *)(param_1 + 0x9e8) & 0x1000000) != 0))))
      goto LAB_0017a720;
      iVar3 = create_ticket_prequel(param_1,param_2,local_74,&local_70);
      if (iVar3 == 0) goto LAB_0017a1a0;
      iVar3 = WPACKET_memcpy(param_2,*(long *)(param_1 + 0x918) + 600,
                             *(undefined8 *)(*(long *)(param_1 + 0x918) + 0x250));
      if ((iVar3 == 0) || (iVar3 = WPACKET_close(param_2), iVar3 == 0)) {
        ERR_new();
        pcVar9 = "construct_stateful_ticket";
        uVar10 = 0xf14;
        goto LAB_0017a8ac;
      }
    }
    else {
LAB_0017a720:
      lVar11 = *(long *)(param_1 + 0xb88);
LAB_0017a1fa:
      iVar3 = i2d_SSL_SESSION(*(SSL_SESSION **)(param_1 + 0x918),(uchar **)0x0);
      if ((iVar3 == 0) || (0xff00 < iVar3)) {
        ERR_new();
        lVar7 = 0;
        ctx = (EVP_CIPHER_CTX *)0x0;
        in = (uchar *)0x0;
        ERR_set_debug("ssl/statem/statem_srvr.c",0xe67,"construct_stateless_ticket");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        local_f0 = 0;
      }
      else {
        in = (uchar *)CRYPTO_malloc(iVar3,"ssl/statem/statem_srvr.c",0xe6a);
        if (in == (uchar *)0x0) {
          ERR_new();
          lVar7 = 0;
          ctx = (EVP_CIPHER_CTX *)0x0;
          ERR_set_debug("ssl/statem/statem_srvr.c",0xe6c,"construct_stateless_ticket");
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
          local_f0 = 0;
        }
        else {
          ctx = EVP_CIPHER_CTX_new();
          lVar7 = ssl_hmac_new(lVar11);
          if ((ctx == (EVP_CIPHER_CTX *)0x0) || (lVar7 == 0)) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xe73,"construct_stateless_ticket");
            uVar10 = 0xc0100;
LAB_0017a79c:
            ossl_statem_fatal(param_1,0x50,uVar10,0);
            local_f0 = 0;
          }
          else {
            local_c0 = in;
            iVar4 = i2d_SSL_SESSION(*(SSL_SESSION **)(param_1 + 0x918),&local_c0);
            if (iVar4 == 0) {
              local_f0 = 0;
              ERR_new();
              uVar10 = 0xe79;
LAB_0017a745:
              ERR_set_debug("ssl/statem/statem_srvr.c",uVar10,"construct_stateless_ticket");
              ossl_statem_fatal(param_1,0x50,0xc0103,0);
            }
            else {
              local_98 = in;
              in_00 = d2i_SSL_SESSION((SSL_SESSION **)0x0,&local_98,(long)iVar3);
              if (in_00 == (SSL_SESSION *)0x0) {
                ERR_new();
                uVar10 = 0xe83;
LAB_0017a450:
                ERR_set_debug("ssl/statem/statem_srvr.c",uVar10,"construct_stateless_ticket");
                uVar10 = 0xc0103;
                goto LAB_0017a79c;
              }
              iVar4 = i2d_SSL_SESSION(in_00,(uchar **)0x0);
              if ((iVar4 == 0) || (iVar3 < iVar4)) {
                ERR_new();
                ERR_set_debug("ssl/statem/statem_srvr.c",0xe8a,"construct_stateless_ticket");
                ossl_statem_fatal(param_1,0x50,0xc0103,0);
                SSL_SESSION_free(in_00);
                local_f0 = 0;
              }
              else {
                local_c0 = in;
                local_f0 = i2d_SSL_SESSION(in_00,&local_c0);
                if (local_f0 == 0) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0xe90,"construct_stateless_ticket");
                  ossl_statem_fatal(param_1,0x50,0xc0103,0);
                  SSL_SESSION_free(in_00);
                }
                else {
                  SSL_SESSION_free(in_00);
                  pcVar6 = *(code **)(lVar11 + 0x248);
                  if (pcVar6 == (code *)0x0) {
                    pcVar6 = *(code **)(lVar11 + 0x240);
                    if (pcVar6 != (code *)0x0) {
                      uVar10 = ssl_hmac_get0_HMAC_CTX(lVar7);
                      goto LAB_0017a31a;
                    }
                    cipher = (EVP_CIPHER *)
                             EVP_CIPHER_fetch(**(undefined8 **)(param_1 + 0x9a8),&DAT_00188288,
                                              (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
                    if (cipher == (EVP_CIPHER *)0x0) {
                      ossl_statem_send_fatal(param_1,0x50);
                      local_f0 = 0;
                      goto LAB_0017a4af;
                    }
                    iVar3 = EVP_CIPHER_get_iv_length(cipher);
                    if (iVar3 < 0) {
LAB_0017ab67:
                      EVP_CIPHER_free(cipher);
                      ERR_new();
                      uVar10 = 0xed2;
                      goto LAB_0017a450;
                    }
                    iVar5 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),local_68,(long)iVar3,0)
                    ;
                    if (((iVar5 < 1) ||
                        (iVar5 = EVP_EncryptInit_ex(ctx,cipher,(ENGINE *)0x0,
                                                    (uchar *)(*(long *)(lVar11 + 0x238) + 0x20),
                                                    local_68), iVar5 == 0)) ||
                       (iVar5 = ssl_hmac_init(lVar7,*(undefined8 *)(lVar11 + 0x238),0x20,"SHA256"),
                       iVar5 == 0)) goto LAB_0017ab67;
                    EVP_CIPHER_free(cipher);
                    local_58 = *(undefined4 *)(lVar11 + 0x228);
                    uStack84 = *(undefined4 *)(lVar11 + 0x22c);
                    uStack80 = *(undefined4 *)(lVar11 + 0x230);
                    uStack76 = *(undefined4 *)(lVar11 + 0x234);
                  }
                  else {
                    uVar10 = ssl_hmac_get0_EVP_MAC_CTX(lVar7);
LAB_0017a31a:
                    local_f0 = (*pcVar6)(param_1,&local_58,local_68,ctx,uVar10,1);
                    if (local_f0 == 0) {
                      iVar3 = WPACKET_put_bytes__(param_2,0,4);
                      if ((iVar3 != 0) && (iVar3 = WPACKET_put_bytes__(param_2,0,2), iVar3 != 0)) {
                        CRYPTO_free(in);
                        EVP_CIPHER_CTX_free(ctx);
                        ssl_hmac_free(lVar7);
                        goto LAB_0017a4e6;
                      }
                      ERR_new();
                      uVar10 = 0xeb2;
                      goto LAB_0017a745;
                    }
                    if (local_f0 < 0) {
                      ERR_new();
                      ERR_set_debug("ssl/statem/statem_srvr.c",0xebb,"construct_stateless_ticket");
                      uVar10 = 0xea;
                      goto LAB_0017a79c;
                    }
                    iVar3 = EVP_CIPHER_CTX_get_iv_length(ctx);
                  }
                  local_d8 = local_68;
                  local_f0 = create_ticket_prequel(param_1,param_2,uVar1,&local_70);
                  if (local_f0 != 0) {
                    iVar5 = WPACKET_get_total_written(param_2,&local_88);
                    if (((iVar5 != 0) &&
                        (iVar5 = WPACKET_memcpy(param_2,&local_58,0x10), iVar5 != 0)) &&
                       (iVar3 = WPACKET_memcpy(param_2,local_d8,(long)iVar3), iVar3 != 0)) {
                      iVar3 = WPACKET_reserve_bytes(param_2,(long)(iVar4 + 0x20),&local_b8);
                      if ((iVar3 != 0) &&
                         (iVar3 = EVP_EncryptUpdate(ctx,local_b8,&local_c8,in,iVar4), iVar3 != 0)) {
                        iVar3 = WPACKET_allocate_bytes(param_2,(long)local_c8,&local_b0);
                        if (((((iVar3 != 0) &&
                              ((((local_b8 == local_b0 &&
                                 (iVar3 = EVP_EncryptFinal(ctx,local_b8 + local_c8,&local_c4),
                                 iVar3 != 0)) &&
                                (iVar3 = WPACKET_allocate_bytes(param_2,(long)local_c4,&local_b0),
                                iVar3 != 0)) &&
                               ((local_b0 == local_b8 + local_c8 &&
                                (local_c8 + local_c4 <= iVar4 + 0x20)))))) &&
                             (iVar3 = WPACKET_get_total_written(param_2,&local_80), iVar3 != 0)) &&
                            ((iVar3 = ssl_hmac_update(lVar7,local_88 +
                                                            *(long *)(*(long *)(param_1 + 0x88) + 8)
                                                      ,local_80 - local_88), iVar3 != 0 &&
                             (iVar3 = WPACKET_reserve_bytes(param_2,0x40,&local_a8), iVar3 != 0))))
                           && ((iVar3 = ssl_hmac_final(lVar7,local_a8,&local_90,0x40), iVar3 != 0 &&
                               (((local_90 < 0x41 &&
                                 (iVar3 = WPACKET_allocate_bytes(param_2,local_90,&local_a0),
                                 iVar3 != 0)) && (local_a8 == local_a0)))))) {
                          iVar3 = WPACKET_close(param_2);
                          if (iVar3 == 0) {
                            local_f0 = 0;
                            ERR_new();
                            uVar10 = 0xefd;
                            goto LAB_0017a745;
                          }
                          local_f0 = 1;
                          goto LAB_0017a4af;
                        }
                      }
                    }
                    ERR_new();
                    uVar10 = 0xef7;
                    goto LAB_0017a450;
                  }
                }
              }
            }
          }
        }
      }
LAB_0017a4af:
      CRYPTO_free(in);
      EVP_CIPHER_CTX_free(ctx);
      ssl_hmac_free(lVar7);
      if (local_f0 == 0) goto LAB_0017a1a0;
    }
LAB_0017a4e6:
    iVar3 = 1;
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) goto LAB_0017a1a3;
    iVar3 = **(int **)(param_1 + 8);
    if ((iVar3 < 0x304) || (iVar3 == 0x10000)) {
      iVar3 = 1;
      goto LAB_0017a1a3;
    }
    iVar3 = tls_construct_extensions(param_1,param_2,0x2000,0,0);
    if (iVar3 != 0) {
      lVar11 = *(long *)(param_1 + 0x1d80) + _UNK_0018e938;
      *(long *)(param_1 + 0x1d78) = *(long *)(param_1 + 0x1d78) + _DAT_0018e930;
      *(long *)(param_1 + 0x1d80) = lVar11;
      if (0 < *(int *)(param_1 + 0xa9c)) {
        *(int *)(param_1 + 0xa9c) = *(int *)(param_1 + 0xa9c) + -1;
      }
      iVar3 = 1;
      ssl_update_cache(param_1,2);
      goto LAB_0017a1a3;
    }
  }
LAB_0017a1a0:
  iVar3 = 0;
LAB_0017a1a3:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar3;
}