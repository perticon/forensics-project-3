int64_t ssl_add_cert_to_wpacket(int64_t a1, int64_t a2, int64_t a3, int32_t a4) {
    int64_t v1 = __readfsqword(40); // 0x73d9f
    int32_t v2 = function_209e0(); // 0x73db4
    int64_t result; // 0x73d80
    if (v2 < 0) {
        // 0x73e78
        function_20230();
        function_207e0();
        // 0x73e44
        ossl_statem_fatal();
        result = 0;
        goto lab_0x73e55;
    } else {
        // 0x73dbc
        if ((int32_t)WPACKET_sub_allocate_bytes__() == 0) {
            goto lab_0x73e20;
        } else {
            // 0x73dd8
            if ((int32_t)function_209e0() != v2) {
                goto lab_0x73e20;
            } else {
                int64_t v3 = *(int64_t *)(a1 + 8); // 0x73de7
                result = 1;
                if ((*(char *)(*(int64_t *)(v3 + 192) + 96) & 8) == 0) {
                    int32_t v4 = *(int32_t *)v3; // 0x73dfd
                    result = 1;
                    if (v4 > (int32_t)&g90 == (v4 != (int32_t)&g2)) {
                        // 0x73ea0
                        result = (int32_t)tls_construct_extensions() != 0;
                    }
                }
                goto lab_0x73e55;
            }
        }
    }
  lab_0x73e20:
    // 0x73e20
    function_20230();
    function_207e0();
    // 0x73e44
    ossl_statem_fatal();
    result = 0;
    goto lab_0x73e55;
  lab_0x73e55:
    // 0x73e55
    if (v1 != __readfsqword(40)) {
        // 0x73ec0
        return function_20ff0();
    }
    // 0x73e65
    return result;
}