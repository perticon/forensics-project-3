bool ssl_add_cert_to_wpacket(long param_1,undefined8 param_2,X509 *param_3,int param_4)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  bool bVar4;
  uchar *puStack72;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = i2d_X509(param_3,(uchar **)0x0);
  if (iVar1 < 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x387,"ssl_add_cert_to_wpacket");
    uVar3 = 0x80007;
  }
  else {
    iVar2 = WPACKET_sub_allocate_bytes__(param_2,(long)iVar1,&puStack72,3);
    if (iVar2 != 0) {
      iVar2 = i2d_X509(param_3,&puStack72);
      if (iVar2 == iVar1) {
        bVar4 = true;
        if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) {
          iVar1 = **(int **)(param_1 + 8);
          if ((iVar1 < 0x304) || (iVar1 == 0x10000)) {
            bVar4 = true;
          }
          else {
            iVar1 = tls_construct_extensions(param_1,param_2,0x1000,param_3,(long)param_4);
            bVar4 = iVar1 != 0;
          }
        }
        goto LAB_00173e55;
      }
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x38c,"ssl_add_cert_to_wpacket");
    uVar3 = 0xc0103;
  }
  ossl_statem_fatal(param_1,0x50,uVar3,0);
  bVar4 = false;
LAB_00173e55:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}