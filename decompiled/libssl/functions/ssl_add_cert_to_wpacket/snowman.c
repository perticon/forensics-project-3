uint32_t ssl_add_cert_to_wpacket(void** rdi, void** rsi, void** rdx, int32_t ecx, int64_t r8, void** r9) {
    uint64_t rax7;
    int32_t eax8;
    int32_t eax9;
    int32_t eax10;
    uint32_t eax11;
    int64_t rax12;
    uint64_t rbx13;

    rax7 = g28;
    eax8 = fun_209e0(rdx);
    if (eax8 < 0) {
        fun_20230();
        fun_207e0("ssl/statem/statem_lib.c", "ssl/statem/statem_lib.c");
    } else {
        eax9 = WPACKET_sub_allocate_bytes__(rsi, static_cast<int64_t>(eax8), reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 24 - 8 + 8, 3, r8, r9);
        if (!eax9 || (eax10 = fun_209e0(rdx, rdx), eax10 != eax8)) {
            fun_20230();
            fun_207e0("ssl/statem/statem_lib.c", "ssl/statem/statem_lib.c");
        } else {
            eax11 = 1;
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 8) + 0xc0) + 96)) & 8)) {
                if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 8))) <= reinterpret_cast<signed char>(0x303) || reinterpret_cast<int1_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 8)) == 0x10000)) {
                    eax11 = 1;
                    goto addr_73e55_8;
                } else {
                    rax12 = tls_construct_extensions(rdi, rsi, 0x1000, rdi, rsi, 0x1000);
                    eax11 = reinterpret_cast<uint1_t>(!!*reinterpret_cast<int32_t*>(&rax12));
                    goto addr_73e55_8;
                }
            }
        }
    }
    ossl_statem_fatal(rdi, rdi);
    eax11 = 0;
    addr_73e55_8:
    rbx13 = rax7 ^ g28;
    if (rbx13) {
        fun_20ff0();
    } else {
        return eax11;
    }
}