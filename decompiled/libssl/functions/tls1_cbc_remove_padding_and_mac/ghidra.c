bool tls1_cbc_remove_padding_and_mac
               (ulong *param_1,ulong param_2,long param_3,void **param_4,undefined4 *param_5,
               long param_6,ulong param_7,int param_8,undefined8 param_9)

{
  byte *pbVar1;
  int iVar2;
  ulong uVar3;
  ulong uVar4;
  void *pvVar5;
  void *pvVar6;
  ulong uVar7;
  byte bVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  ulong uVar12;
  byte bVar13;
  ulong uVar14;
  ulong uVar15;
  ulong uVar16;
  long in_FS_OFFSET;
  bool bVar17;
  byte local_108 [64];
  undefined local_c8 [136];
  long local_40;
  
  uVar11 = *param_1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  bVar17 = false;
  uVar3 = (param_6 != 1) + param_7;
  if (uVar11 < uVar3) goto LAB_0015d99f;
  if (param_6 == 1) {
    uVar3 = 0x40;
    if (param_2 < 0x41) {
      uVar3 = param_2;
    }
    if (param_7 <= uVar3) {
      if (param_7 != 0) {
        *param_1 = uVar11 - param_7;
        if (param_4 != (void **)0x0) {
          *param_4 = (void *)((uVar11 - param_7) + param_3);
        }
        if (param_5 != (undefined4 *)0x0) {
          *param_5 = 0;
          bVar17 = true;
          goto LAB_0015d99f;
        }
      }
      goto LAB_0015d999;
    }
  }
  else {
    bVar13 = *(byte *)(param_3 + -1 + uVar11);
    uVar15 = (ulong)bVar13;
    if (param_8 != 0) {
      *param_1 = ((uVar11 - 1) - param_7) - uVar15;
LAB_0015d999:
      bVar17 = true;
      goto LAB_0015d99f;
    }
    uVar3 = uVar3 + uVar15;
    uVar3 = (long)((uVar3 ^ uVar11 | uVar11 - uVar3 ^ uVar3) ^ uVar11) >> 0x3f;
    uVar14 = ~uVar3;
    if (uVar11 < 0x100) {
      uVar16 = uVar11;
      if (uVar11 != 0) goto LAB_0015d6fd;
    }
    else {
      uVar16 = 0x100;
LAB_0015d6fd:
      uVar7 = 0;
      uVar3 = uVar15;
      do {
        uVar9 = uVar7 ^ uVar3;
        uVar4 = uVar15 ^ uVar7;
        uVar7 = uVar7 + 1;
        pbVar1 = (byte *)((uVar11 - uVar15) + param_3 + -1 + uVar3);
        uVar3 = uVar3 - 1;
        uVar14 = uVar14 & ((long)((uVar4 | uVar9) ^ uVar15) >> 0x3f | ~(ulong)(*pbVar1 ^ bVar13));
      } while (uVar7 < uVar16);
      uVar3 = ~uVar14;
    }
    uVar14 = (uVar14 | 0xffffffffffffff00) & (uVar3 & 0xff) - 1;
    uVar15 = uVar15 + 1 & (long)uVar14 >> 0x3f;
    uVar16 = uVar11 - uVar15;
    uVar3 = 0x40;
    if (param_2 < 0x41) {
      uVar3 = param_2;
    }
    *param_1 = uVar16;
    if (param_7 <= uVar3) {
      if (param_7 == 0) {
        bVar17 = (long)uVar14 < 0;
        goto LAB_0015d99f;
      }
      *param_1 = uVar16 - param_7;
      iVar2 = RAND_bytes_ex(param_9,local_108,param_7,0);
      if (((0 < iVar2) && (param_4 != (void **)0x0)) && (param_5 != (undefined4 *)0x0)) {
        pvVar5 = CRYPTO_malloc((int)param_7,"ssl/record/tls_pad.c",0x104);
        *param_4 = pvVar5;
        if (pvVar5 != (void *)0x0) {
          uVar3 = (param_2 - 0x100) - param_7;
          *param_5 = 1;
          if (param_2 <= param_7 + 0x100) {
            uVar3 = 0;
          }
          pvVar6 = memset(local_c8 + (-(int)local_c8 & 0x3f),0,param_7);
          if (uVar3 < param_2) {
            uVar7 = 0;
            uVar4 = 0;
            uVar9 = 0;
            do {
              uVar10 = uVar16 - param_7 ^ uVar3;
              uVar12 = (long)(~uVar10 & uVar10 - 1) >> 0x3f;
              uVar10 = (uVar3 + (uVar15 - uVar11) ^ uVar16 | uVar16 ^ uVar3) ^ uVar3;
              uVar7 = uVar7 | uVar12 & uVar4;
              pbVar1 = (byte *)(param_3 + uVar3);
              uVar3 = uVar3 + 1;
              uVar9 = (uVar9 | uVar12) & (long)uVar10 >> 0x3f;
              uVar10 = uVar4 + 1;
              *(byte *)((long)pvVar6 + uVar4) =
                   *(byte *)((long)pvVar6 + uVar4) | *pbVar1 & (byte)uVar9;
              uVar4 = (long)((uVar10 - param_7 ^ param_7 | param_7 ^ uVar10) ^ uVar10) >> 0x3f &
                      uVar10;
            } while (param_2 != uVar3);
          }
          else {
            uVar7 = 0;
          }
          bVar13 = (byte)((long)uVar14 >> 0x3f);
          uVar11 = 0;
          do {
            uVar15 = uVar11 + 1;
            uVar3 = uVar7 + 1;
            bVar8 = (byte)((int)(((uint)uVar7 & 0x20) - 1) >> 0x1f);
            *(byte *)((long)pvVar5 + uVar11) =
                 (~bVar8 & *(byte *)((long)pvVar6 + (uVar7 | 0x20)) |
                 *(byte *)((long)pvVar6 + (uVar7 & 0xffffffffffffffdf)) & bVar8) & bVar13 |
                 local_108[uVar11] & ~bVar13;
            uVar7 = (long)((uVar3 - param_7 ^ param_7 | param_7 ^ uVar3) ^ uVar3) >> 0x3f & uVar3;
            uVar11 = uVar15;
          } while (param_7 != uVar15);
          goto LAB_0015d999;
        }
      }
    }
  }
  bVar17 = false;
LAB_0015d99f:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return bVar17;
}