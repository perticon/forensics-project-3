int SSL_pending(SSL *s)

{
  int iVar1;
  undefined4 extraout_var;
  ulong uVar2;
  
  iVar1 = (*s->method->ssl_pending)(s);
  uVar2 = CONCAT44(extraout_var,iVar1);
  if (0x7ffffffe < uVar2) {
    uVar2 = 0x7fffffff;
  }
  return (int)uVar2;
}