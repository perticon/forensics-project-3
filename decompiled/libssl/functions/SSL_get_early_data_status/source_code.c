int SSL_get_early_data_status(const SSL *s)
{
    return s->ext.early_data;
}