undefined8 tls_construct_certificate_authorities(undefined8 param_1,undefined8 param_2)

{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  
  lVar2 = get_ca_names();
  if (lVar2 == 0) {
    return 2;
  }
  iVar1 = OPENSSL_sk_num(lVar2);
  if (iVar1 == 0) {
    return 2;
  }
  iVar1 = WPACKET_put_bytes__(param_2,0x2f,2);
  if ((iVar1 == 0) || (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 == 0)) {
    ERR_new();
    uVar3 = 0x4ca;
  }
  else {
    iVar1 = construct_ca_names(param_1,lVar2,param_2);
    if (iVar1 == 0) {
      return 0;
    }
    iVar1 = WPACKET_close(param_2);
    if (iVar1 != 0) {
      return 1;
    }
    ERR_new();
    uVar3 = 0x4d4;
  }
  ERR_set_debug("ssl/statem/extensions.c",uVar3,"tls_construct_certificate_authorities");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}