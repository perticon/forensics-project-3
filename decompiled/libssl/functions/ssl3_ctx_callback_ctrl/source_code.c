long ssl3_ctx_callback_ctrl(SSL_CTX *ctx, int cmd, void (*fp) (void))
{
    switch (cmd) {
#if !defined(OPENSSL_NO_DEPRECATED_3_0)
    case SSL_CTRL_SET_TMP_DH_CB:
        {
            ctx->cert->dh_tmp_cb = (DH *(*)(SSL *, int, int))fp;
        }
        break;
#endif
    case SSL_CTRL_SET_TLSEXT_SERVERNAME_CB:
        ctx->ext.servername_cb = (int (*)(SSL *, int *, void *))fp;
        break;

    case SSL_CTRL_SET_TLSEXT_STATUS_REQ_CB:
        ctx->ext.status_cb = (int (*)(SSL *, void *))fp;
        break;

# ifndef OPENSSL_NO_DEPRECATED_3_0
    case SSL_CTRL_SET_TLSEXT_TICKET_KEY_CB:
        ctx->ext.ticket_key_cb = (int (*)(SSL *, unsigned char *,
                                             unsigned char *,
                                             EVP_CIPHER_CTX *,
                                             HMAC_CTX *, int))fp;
        break;
#endif

#ifndef OPENSSL_NO_SRP
    case SSL_CTRL_SET_SRP_VERIFY_PARAM_CB:
        ctx->srp_ctx.srp_Mask |= SSL_kSRP;
        ctx->srp_ctx.SRP_verify_param_callback = (int (*)(SSL *, void *))fp;
        break;
    case SSL_CTRL_SET_TLS_EXT_SRP_USERNAME_CB:
        ctx->srp_ctx.srp_Mask |= SSL_kSRP;
        ctx->srp_ctx.TLS_ext_srp_username_callback =
            (int (*)(SSL *, int *, void *))fp;
        break;
    case SSL_CTRL_SET_SRP_GIVE_CLIENT_PWD_CB:
        ctx->srp_ctx.srp_Mask |= SSL_kSRP;
        ctx->srp_ctx.SRP_give_srp_client_pwd_callback =
            (char *(*)(SSL *, void *))fp;
        break;
#endif
    case SSL_CTRL_SET_NOT_RESUMABLE_SESS_CB:
        {
            ctx->not_resumable_session_cb = (int (*)(SSL *, int))fp;
        }
        break;
    default:
        return 0;
    }
    return 1;
}