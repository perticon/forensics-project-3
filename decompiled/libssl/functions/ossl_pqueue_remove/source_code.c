void *ossl_pqueue_remove(OSSL_PQUEUE *pq, size_t elem)
{
    size_t n;

    if (pq == NULL || elem >= pq->hmax || pq->htop == 0)
        return 0;

    ASSERT_ELEM_USED(pq, elem);
    n = pq->elements[elem].posn;

    ASSERT_USED(pq, n);

    if (n == pq->htop - 1)
        return pq->heap[--pq->htop].data;
    if (n > 0)
        pqueue_force_bottom(pq, n);
    return ossl_pqueue_pop(pq);
}