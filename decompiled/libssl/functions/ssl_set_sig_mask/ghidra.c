void ssl_set_sig_mask(uint *param_1,long param_2,undefined4 param_3)

{
  int iVar1;
  long lVar2;
  long lVar3;
  uint uVar4;
  long lVar5;
  long lVar6;
  long in_FS_OFFSET;
  short *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  lVar2 = tls12_get_psigalgs(param_2,1,&local_48);
  if (lVar2 == 0) {
    uVar4 = 0xb;
  }
  else {
    uVar4 = 0xb;
    lVar6 = 0;
    do {
      lVar5 = *(long *)(*(long *)(param_2 + 0x9a8) + 0x620);
      lVar3 = lVar5 + 0x4d8;
      do {
        if (*local_48 == *(short *)(lVar5 + 8)) {
          if ((((*(int *)(lVar5 + 0x24) != 0) &&
               (lVar3 = ssl_cert_lookup_by_idx((long)*(int *)(lVar5 + 0x18)), lVar3 != 0)) &&
              ((*(uint *)(lVar3 + 4) & uVar4) != 0)) &&
             ((*(int *)(lVar5 + 0x24) != 0 &&
              (iVar1 = tls12_sigalg_allowed_part_0(param_2,param_3,lVar5), iVar1 != 0)))) {
            uVar4 = uVar4 & ~*(uint *)(lVar3 + 4);
          }
          break;
        }
        lVar5 = lVar5 + 0x28;
      } while (lVar5 != lVar3);
      local_48 = local_48 + 1;
      lVar6 = lVar6 + 1;
    } while (lVar2 != lVar6);
  }
  *param_1 = *param_1 | uVar4;
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}