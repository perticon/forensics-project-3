bool tls13_generate_secret
               (long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,
               undefined8 param_6)

{
  bool bVar1;
  int iVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long lVar5;
  undefined8 *puVar6;
  undefined8 *puVar7;
  long in_FS_OFFSET;
  undefined4 local_198;
  undefined4 uStack404;
  undefined4 uStack400;
  undefined4 uStack396;
  undefined4 local_188;
  undefined4 uStack388;
  undefined4 uStack384;
  undefined4 uStack380;
  undefined8 local_178;
  undefined4 local_15c;
  undefined local_158 [32];
  undefined8 local_138;
  undefined8 local_110;
  undefined8 local_108 [4];
  undefined8 local_e8;
  undefined8 local_e0 [20];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_15c = 1;
  uVar3 = EVP_MD_get0_name(param_2);
  uVar4 = EVP_KDF_fetch(**(undefined8 **)(param_1 + 0x9a8),&DAT_0018b146,
                        (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
  lVar5 = EVP_KDF_CTX_new(uVar4);
  EVP_KDF_free(uVar4);
  if (lVar5 == 0) {
    ERR_new();
    ERR_set_debug("ssl/tls13_enc.c",0xb7,"tls13_generate_secret");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    bVar1 = false;
  }
  else {
    iVar2 = EVP_MD_get_size(param_2);
    if (iVar2 < 0) {
      ERR_new();
      ERR_set_debug("ssl/tls13_enc.c",0xbe,"tls13_generate_secret");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      EVP_KDF_CTX_free(lVar5);
      bVar1 = false;
    }
    else {
      OSSL_PARAM_construct_int(&local_198,"mode",&local_15c);
      local_138 = local_178;
      puVar6 = local_108;
      OSSL_PARAM_construct_utf8_string
                (CONCAT44(uStack404,local_198),CONCAT44(uStack388,local_188),&local_198,"digest",
                 uVar3,0);
      local_110 = local_178;
      if (param_4 != 0) {
        puVar6 = local_e0;
        OSSL_PARAM_construct_octet_string(&local_198,"key",param_4,param_5);
        local_e8 = local_178;
      }
      puVar7 = puVar6 + 5;
      if (param_3 != 0) {
        OSSL_PARAM_construct_octet_string(&local_198,&DAT_0018b160,param_3,(long)iVar2);
        *(undefined4 *)puVar6 = local_198;
        *(undefined4 *)((long)puVar6 + 4) = uStack404;
        *(undefined4 *)(puVar6 + 1) = uStack400;
        *(undefined4 *)((long)puVar6 + 0xc) = uStack396;
        *(undefined4 *)(puVar6 + 2) = local_188;
        *(undefined4 *)((long)puVar6 + 0x14) = uStack388;
        *(undefined4 *)(puVar6 + 3) = uStack384;
        *(undefined4 *)((long)puVar6 + 0x1c) = uStack380;
        puVar6[4] = local_178;
        puVar7 = puVar6 + 10;
        puVar6 = puVar6 + 5;
      }
      OSSL_PARAM_construct_octet_string(&local_198,"prefix","tls13 ",6);
      *puVar6 = CONCAT44(uStack404,local_198);
      puVar6[1] = CONCAT44(uStack396,uStack400);
      puVar6[2] = CONCAT44(uStack388,local_188);
      puVar6[3] = CONCAT44(uStack380,uStack384);
      puVar6[4] = local_178;
      OSSL_PARAM_construct_octet_string(&local_198,"label","derived",7);
      *puVar7 = CONCAT44(uStack404,local_198);
      puVar7[1] = CONCAT44(uStack396,uStack400);
      puVar7[2] = CONCAT44(uStack388,local_188);
      puVar7[3] = CONCAT44(uStack380,uStack384);
      puVar7[4] = local_178;
      OSSL_PARAM_construct_end(&local_198);
      puVar6[10] = CONCAT44(uStack404,local_198);
      puVar6[0xb] = CONCAT44(uStack396,uStack400);
      puVar6[0xc] = CONCAT44(uStack388,local_188);
      puVar6[0xd] = CONCAT44(uStack380,uStack384);
      puVar6[0xe] = local_178;
      iVar2 = EVP_KDF_derive(CONCAT44(uStack404,local_198),CONCAT44(uStack388,local_188),lVar5,
                             param_6,(long)iVar2,local_158);
      if (iVar2 < 1) {
        ERR_new();
        ERR_set_debug("ssl/tls13_enc.c",0xd9,"tls13_generate_secret");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
      EVP_KDF_CTX_free(lVar5);
      bVar1 = 0 < iVar2;
    }
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}