ulong tls13_alert_code(uint param_1)

{
  ulong uVar1;
  
  if ((param_1 != 0x6d) && (param_1 != 0x74)) {
    uVar1 = tls1_alert_code();
    return uVar1;
  }
  return (ulong)param_1;
}