void ssl_evp_md_free(long param_1)

{
  long lVar1;
  
  if (param_1 == 0) {
    return;
  }
  lVar1 = EVP_MD_get0_provider();
  if (lVar1 != 0) {
    EVP_MD_free(param_1);
    return;
  }
  return;
}