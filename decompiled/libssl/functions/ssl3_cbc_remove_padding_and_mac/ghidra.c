bool ssl3_cbc_remove_padding_and_mac
               (ulong *param_1,ulong param_2,long param_3,void **param_4,undefined4 *param_5,
               ulong param_6,ulong param_7,undefined8 param_8)

{
  byte *pbVar1;
  int iVar2;
  void *pvVar3;
  void *pvVar4;
  ulong uVar5;
  ulong uVar6;
  ulong uVar7;
  byte bVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  ulong uVar12;
  ulong uVar13;
  ulong uVar14;
  ulong uVar15;
  ulong uVar16;
  long in_FS_OFFSET;
  bool bVar17;
  byte local_108 [64];
  undefined local_c8 [136];
  long local_40;
  
  bVar17 = false;
  uVar7 = *param_1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (uVar7 < param_7 + 1) goto LAB_0015d353;
  uVar16 = (ulong)*(byte *)(param_3 + -1 + uVar7);
  uVar11 = param_7 + 1 + uVar16;
  uVar16 = uVar16 + 1;
  uVar10 = ~((long)((uVar7 - uVar11 ^ uVar11 | uVar11 ^ uVar7) ^ uVar7 |
                   (param_6 - uVar16 ^ uVar16 | uVar16 ^ param_6) ^ param_6) >> 0x3f);
  uVar15 = uVar7 - (uVar16 & uVar10);
  uVar11 = 0x40;
  if (param_2 < 0x41) {
    uVar11 = param_2;
  }
  *param_1 = uVar15;
  if (uVar11 < param_7) goto LAB_0015d5f8;
  if (param_7 == 0) {
    bVar17 = uVar10 != 0;
    goto LAB_0015d353;
  }
  uVar11 = uVar15 - param_7;
  *param_1 = uVar11;
  if (param_6 == 1) {
    if (param_4 != (void **)0x0) {
      *param_4 = (void *)(param_3 + uVar11);
    }
    if (param_5 != (undefined4 *)0x0) {
      *param_5 = 0;
    }
LAB_0015d5e7:
    bVar17 = true;
  }
  else {
    iVar2 = RAND_bytes_ex(param_8,local_108,param_7,0);
    if (((0 < iVar2) && (param_4 != (void **)0x0)) && (param_5 != (undefined4 *)0x0)) {
      pvVar3 = CRYPTO_malloc((int)param_7,"ssl/record/tls_pad.c",0x104);
      *param_4 = pvVar3;
      if (pvVar3 != (void *)0x0) {
        uVar6 = (param_2 - 0x100) - param_7;
        *param_5 = 1;
        if (param_2 <= param_7 + 0x100) {
          uVar6 = 0;
        }
        pvVar4 = memset(local_c8 + (-(int)local_c8 & 0x3f),0,param_7);
        if (uVar6 < param_2) {
          uVar5 = 0;
          uVar9 = 0;
          uVar14 = 0;
          do {
            uVar12 = (long)((uVar11 ^ uVar6) - 1 & ~(uVar11 ^ uVar6)) >> 0x3f;
            uVar5 = uVar5 | uVar12 & uVar9;
            pbVar1 = (byte *)(param_3 + uVar6);
            uVar13 = (uVar6 + ((uVar16 & uVar10) - uVar7) ^ uVar15 | uVar15 ^ uVar6) ^ uVar6;
            uVar6 = uVar6 + 1;
            uVar14 = (uVar14 | uVar12) & (long)uVar13 >> 0x3f;
            uVar12 = uVar9 + 1;
            *(byte *)((long)pvVar4 + uVar9) =
                 *(byte *)((long)pvVar4 + uVar9) | *pbVar1 & (byte)uVar14;
            uVar9 = (long)((uVar12 - param_7 ^ param_7 | param_7 ^ uVar12) ^ uVar12) >> 0x3f &
                    uVar12;
          } while (param_2 != uVar6);
        }
        else {
          uVar5 = 0;
        }
        uVar7 = 0;
        do {
          uVar15 = uVar7 + 1;
          uVar11 = uVar5 + 1;
          bVar8 = (byte)((int)(((uint)uVar5 & 0x20) - 1) >> 0x1f);
          *(byte *)((long)pvVar3 + uVar7) =
               (~bVar8 & *(byte *)((long)pvVar4 + (uVar5 | 0x20)) |
               *(byte *)((long)pvVar4 + (uVar5 & 0xffffffffffffffdf)) & bVar8) & (byte)uVar10 |
               local_108[uVar7] & ~(byte)uVar10;
          uVar5 = (long)((uVar11 - param_7 ^ param_7 | param_7 ^ uVar11) ^ uVar11) >> 0x3f & uVar11;
          uVar7 = uVar15;
        } while (param_7 != uVar15);
        goto LAB_0015d5e7;
      }
    }
LAB_0015d5f8:
    bVar17 = false;
  }
LAB_0015d353:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar17;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}