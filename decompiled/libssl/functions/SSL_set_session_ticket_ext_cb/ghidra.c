int SSL_set_session_ticket_ext_cb(SSL *s,tls_session_ticket_ext_cb_fn cb,void *arg)

{
  if (s != (SSL *)0x0) {
    *(tls_session_ticket_ext_cb_fn *)&s[4].rwstate = cb;
    s[4].handshake_func = (_func_3149 *)arg;
    return 1;
  }
  return 0;
}