static int init_post_handshake_auth(SSL *s, ossl_unused unsigned int context)
{
    s->post_handshake_auth = SSL_PHA_NONE;

    return 1;
}