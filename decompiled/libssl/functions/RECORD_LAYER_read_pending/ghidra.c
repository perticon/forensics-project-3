bool RECORD_LAYER_read_pending(long param_1)

{
  return *(long *)(param_1 + 0x40) != 0;
}