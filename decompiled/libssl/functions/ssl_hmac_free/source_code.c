void ssl_hmac_free(SSL_HMAC *ctx)
{
    if (ctx != NULL) {
        EVP_MAC_CTX_free(ctx->ctx);
#ifndef OPENSSL_NO_DEPRECATED_3_0
        ssl_hmac_old_free(ctx);
#endif
        OPENSSL_free(ctx);
    }
}