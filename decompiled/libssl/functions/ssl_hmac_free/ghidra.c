void ssl_hmac_free(undefined8 *param_1)

{
  if (param_1 != (undefined8 *)0x0) {
    EVP_MAC_CTX_free(*param_1);
    ssl_hmac_old_free(param_1);
    CRYPTO_free(param_1);
    return;
  }
  return;
}