bool tls_valid_group(long param_1,undefined8 param_2,int param_3,int param_4,int param_5,
                    uint *param_6)

{
  int iVar1;
  long lVar2;
  int iVar3;
  byte *pbVar4;
  byte *pbVar5;
  byte *pbVar6;
  bool bVar7;
  bool bVar8;
  bool bVar9;
  bool bVar10;
  byte bVar11;
  
  bVar11 = 0;
  lVar2 = tls1_group_id_lookup();
  if (param_6 != (uint *)0x0) {
    *param_6 = 0;
  }
  if (lVar2 == 0) {
    return false;
  }
  if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
    iVar3 = *(int *)(lVar2 + 0x20);
    if (iVar3 < 0) {
      return false;
    }
    iVar1 = *(int *)(lVar2 + 0x24);
    if (iVar1 < 0) {
      return false;
    }
    bVar7 = iVar1 == 0 || param_3 <= iVar1;
    bVar8 = iVar1 == 0 || param_3 <= iVar1;
    if (iVar3 != 0) {
      bVar7 = bVar7 && iVar3 <= param_4;
      bVar8 = bVar7;
    }
    if ((param_6 != (uint *)0x0 && param_4 == 0x304) && (bVar8)) {
      *param_6 = (uint)(iVar1 == 0 || 0x303 < iVar1);
    }
  }
  else {
    iVar3 = *(int *)(lVar2 + 0x28);
    if (iVar3 < 0) {
      return false;
    }
    iVar1 = *(int *)(lVar2 + 0x2c);
    if (iVar1 < 0) {
      return false;
    }
    bVar7 = true;
    if (iVar1 != 0) {
      if (param_3 == 0x100) {
        param_3 = 0xff00;
      }
      if (iVar1 == 0x100) {
        iVar1 = 0xff00;
      }
      bVar7 = iVar1 <= param_3;
    }
    if (iVar3 != 0) {
      if (param_4 == 0x100) {
        param_4 = 0xff00;
      }
      if (iVar3 == 0x100) {
        iVar3 = 0xff00;
      }
      bVar7 = (bool)(bVar7 & param_4 <= iVar3);
    }
  }
  if (param_5 != 0) {
    pbVar5 = *(byte **)(lVar2 + 0x10);
    bVar8 = *pbVar5 < 0x45;
    bVar10 = *pbVar5 == 0x45;
    if (bVar10) {
      bVar8 = pbVar5[1] < 0x43;
      bVar10 = pbVar5[1] == 0x43;
      if (bVar10) {
        bVar8 = false;
        bVar10 = pbVar5[2] == 0;
        if (bVar10) {
          return bVar7;
        }
      }
    }
    lVar2 = 7;
    pbVar4 = pbVar5;
    pbVar6 = (byte *)"X25519";
    do {
      if (lVar2 == 0) break;
      lVar2 = lVar2 + -1;
      bVar8 = *pbVar4 < *pbVar6;
      bVar10 = *pbVar4 == *pbVar6;
      pbVar4 = pbVar4 + (ulong)bVar11 * -2 + 1;
      pbVar6 = pbVar6 + (ulong)bVar11 * -2 + 1;
    } while (bVar10);
    bVar9 = false;
    bVar8 = (!bVar8 && !bVar10) == bVar8;
    if (!bVar8) {
      lVar2 = 5;
      pbVar4 = &DAT_00188283;
      do {
        if (lVar2 == 0) break;
        lVar2 = lVar2 + -1;
        bVar9 = *pbVar5 < *pbVar4;
        bVar8 = *pbVar5 == *pbVar4;
        pbVar5 = pbVar5 + (ulong)bVar11 * -2 + 1;
        pbVar4 = pbVar4 + (ulong)bVar11 * -2 + 1;
      } while (bVar8);
      bVar7 = (bool)(bVar7 & (!bVar9 && !bVar8) == bVar9);
    }
  }
  return bVar7;
}