int tls_valid_group(SSL *s, uint16_t group_id, int minversion, int maxversion,
                    int isec, int *okfortls13)
{
    const TLS_GROUP_INFO *ginfo = tls1_group_id_lookup(s->ctx, group_id);
    int ret;

    if (okfortls13 != NULL)
        *okfortls13 = 0;

    if (ginfo == NULL)
        return 0;

    if (SSL_IS_DTLS(s)) {
        if (ginfo->mindtls < 0 || ginfo->maxdtls < 0)
            return 0;
        if (ginfo->maxdtls == 0)
            ret = 1;
        else
            ret = DTLS_VERSION_LE(minversion, ginfo->maxdtls);
        if (ginfo->mindtls > 0)
            ret &= DTLS_VERSION_GE(maxversion, ginfo->mindtls);
    } else {
        if (ginfo->mintls < 0 || ginfo->maxtls < 0)
            return 0;
        if (ginfo->maxtls == 0)
            ret = 1;
        else
            ret = (minversion <= ginfo->maxtls);
        if (ginfo->mintls > 0)
            ret &= (maxversion >= ginfo->mintls);
        if (ret && okfortls13 != NULL && maxversion == TLS1_3_VERSION)
            *okfortls13 = (ginfo->maxtls == 0)
                          || (ginfo->maxtls >= TLS1_3_VERSION);
    }
    ret &= !isec
           || strcmp(ginfo->algorithm, "EC") == 0
           || strcmp(ginfo->algorithm, "X25519") == 0
           || strcmp(ginfo->algorithm, "X448") == 0;

    return ret;
}