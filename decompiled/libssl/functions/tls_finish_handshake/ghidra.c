undefined8 tls_finish_handshake(long param_1,undefined8 param_2,int param_3,int param_4)

{
  int iVar1;
  long lVar2;
  uint uVar3;
  int iVar4;
  int *piVar5;
  SSL_CTX *pSVar6;
  code *pcVar7;
  
  piVar5 = *(int **)(param_1 + 8);
  iVar1 = *(int *)(param_1 + 0x70);
  uVar3 = *(uint *)(*(long *)(piVar5 + 0x30) + 0x60) & 8;
  if (param_3 != 0) {
    if (uVar3 == 0) {
      BUF_MEM_free(*(BUF_MEM **)(param_1 + 0x88));
      *(undefined8 *)(param_1 + 0x88) = 0;
      iVar4 = ssl_free_wbio_buffer();
    }
    else {
      iVar4 = ssl_free_wbio_buffer();
    }
    if (iVar4 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x42c,"tls_finish_handshake");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    }
    *(undefined8 *)(param_1 + 0x98) = 0;
    piVar5 = *(int **)(param_1 + 8);
    uVar3 = *(uint *)(*(long *)(piVar5 + 0x30) + 0x60) & 8;
  }
  if ((((uVar3 == 0) && (0x303 < *piVar5)) && (*piVar5 != 0x10000)) &&
     ((*(int *)(param_1 + 0x38) == 0 && (*(int *)(param_1 + 0xba8) == 4)))) {
    *(undefined4 *)(param_1 + 0xba8) = 1;
  }
  if (iVar1 == 0) {
    pcVar7 = *(code **)(param_1 + 0x978);
    if ((pcVar7 != (code *)0x0) ||
       (pcVar7 = *(code **)(*(long *)(param_1 + 0x9a8) + 0x120), pcVar7 != (code *)0x0)) {
      ossl_statem_set_in_init(param_1,0);
      if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
           (iVar1 = **(int **)(param_1 + 8), iVar1 == 0x10000)) || (iVar1 < 0x304)) ||
         ((*(long *)(param_1 + 0x240) == 0 || (*(long *)(param_1 + 0x2c8) == 0)))) {
        (*pcVar7)(param_1,0x20,1);
      }
      goto joined_r0x00175a38;
    }
  }
  else {
    *(undefined4 *)(param_1 + 0xba0) = 0;
    *(undefined4 *)(param_1 + 0x3c) = 0;
    *(undefined4 *)(param_1 + 0x70) = 0;
    *(undefined4 *)(param_1 + 0xa98) = 0;
    ssl3_cleanup_key_block(param_1);
    piVar5 = *(int **)(param_1 + 8);
    if (*(int *)(param_1 + 0x38) == 0) {
      if ((((*(byte *)(*(long *)(piVar5 + 0x30) + 0x60) & 8) == 0) && (*piVar5 != 0x10000)) &&
         (0x303 < *piVar5)) {
        pSVar6 = *(SSL_CTX **)(param_1 + 0xb88);
        if ((*(byte *)&pSVar6->new_session_cb & 1) != 0) {
          SSL_CTX_remove_session(pSVar6,*(SSL_SESSION **)(param_1 + 0x918));
          pSVar6 = *(SSL_CTX **)(param_1 + 0xb88);
        }
      }
      else {
        ssl_update_cache(param_1,1);
        pSVar6 = *(SSL_CTX **)(param_1 + 0xb88);
      }
      if (*(int *)(param_1 + 0x4d0) != 0) {
        LOCK();
        piVar5 = (int *)((long)&pSVar6->app_verify_callback + 4);
        *piVar5 = *piVar5 + 1;
        pSVar6 = *(SSL_CTX **)(param_1 + 0xb88);
      }
      *(code **)(param_1 + 0x30) = ossl_statem_connect;
      LOCK();
      piVar5 = &(pSVar6->stats).sess_miss;
      *piVar5 = *piVar5 + 1;
    }
    else {
      if ((((*(byte *)(*(long *)(piVar5 + 0x30) + 0x60) & 8) != 0) || (*piVar5 == 0x10000)) ||
         (*piVar5 < 0x304)) {
        ssl_update_cache(param_1,2);
      }
      LOCK();
      piVar5 = (int *)(*(long *)(param_1 + 0x9a8) + 0x8c);
      *piVar5 = *piVar5 + 1;
      *(code **)(param_1 + 0x30) = ossl_statem_accept;
    }
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0) {
      lVar2 = *(long *)(param_1 + 0x4b8);
      *(undefined4 *)(lVar2 + 0x10c) = 0;
      *(undefined2 *)(lVar2 + 0x110) = 0;
      dtls1_clear_received_buffer(param_1);
    }
    pcVar7 = *(code **)(param_1 + 0x978);
    if ((pcVar7 != (code *)0x0) ||
       (pcVar7 = *(code **)(*(long *)(param_1 + 0x9a8) + 0x120), pcVar7 != (code *)0x0)) {
      ossl_statem_set_in_init(param_1,0);
      (*pcVar7)(param_1,0x20,1);
      goto joined_r0x00175a38;
    }
  }
  ossl_statem_set_in_init(param_1,0);
joined_r0x00175a38:
  if (param_4 != 0) {
    return 1;
  }
  ossl_statem_set_in_init(param_1,1);
  return 2;
}