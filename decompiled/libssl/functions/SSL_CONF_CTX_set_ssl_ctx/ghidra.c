void SSL_CONF_CTX_set_ssl_ctx(long param_1,long param_2)

{
  *(long *)(param_1 + 0x18) = param_2;
  *(undefined8 *)(param_1 + 0x20) = 0;
  if (param_2 != 0) {
    *(long *)(param_1 + 0x28) = param_2 + 0x138;
    *(long *)(param_1 + 0x78) = *(long *)(param_2 + 0x158) + 0x1c;
    *(long *)(param_1 + 0x80) = param_2 + 0x178;
    *(long *)(param_1 + 0x88) = param_2 + 0x144;
    *(long *)(param_1 + 0x90) = param_2 + 0x148;
    return;
  }
  *(undefined8 *)(param_1 + 0x28) = 0;
  *(undefined (*) [16])(param_1 + 0x78) = (undefined  [16])0x0;
  *(undefined (*) [16])(param_1 + 0x88) = (undefined  [16])0x0;
  return;
}