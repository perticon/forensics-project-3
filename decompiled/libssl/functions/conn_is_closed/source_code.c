static ossl_inline int conn_is_closed(void)
{
    switch (get_last_sys_error()) {
#if defined(EPIPE)
    case EPIPE:
        return 1;
#endif
#if defined(ECONNRESET)
    case ECONNRESET:
        return 1;
#endif
#if defined(WSAECONNRESET)
    case WSAECONNRESET:
        return 1;
#endif
    default:
        return 0;
    }
}