undefined8 SSL_sendfile(long param_1)

{
  undefined8 uVar1;
  
  if (*(long *)(param_1 + 0x30) == 0) {
    ERR_new();
    uVar1 = 0x808;
  }
  else {
    if ((*(byte *)(param_1 + 0x44) & 1) != 0) {
      *(undefined4 *)(param_1 + 0x28) = 1;
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x80e,"SSL_sendfile");
      ERR_set_error(0x14,0xcf,0);
      return 0xffffffffffffffff;
    }
    ERR_new();
    uVar1 = 0x813;
  }
  ERR_set_debug("ssl/ssl_lib.c",uVar1,"SSL_sendfile");
  ERR_set_error(0x14,0x114,0);
  return 0xffffffffffffffff;
}