void tls13_generate_master_secret
               (undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
               long *param_5)

{
  int iVar1;
  undefined8 uVar2;
  
  uVar2 = ssl_handshake_md();
  iVar1 = EVP_MD_get_size(uVar2);
  *param_5 = (long)iVar1;
  tls13_generate_secret(param_1,uVar2,param_3,0,0,param_2);
  return;
}