undefined8 tls_construct_ctos_supported_versions(undefined8 param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  int local_28;
  int local_24;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = ssl_get_min_max_version(param_1,&local_28,&local_24,0);
  if (iVar1 == 0) {
    uVar2 = 2;
    if (local_24 < 0x304) goto LAB_001614d9;
    iVar1 = WPACKET_put_bytes__(param_2,0x2b,2);
    if (((iVar1 == 0) || (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 == 0)) ||
       (iVar1 = WPACKET_start_sub_packet_len__(param_2,1), iVar1 == 0)) {
      ERR_new();
      uVar2 = 0x230;
    }
    else {
      if (local_28 <= local_24) {
        do {
          iVar1 = WPACKET_put_bytes__(param_2,local_24,2);
          if (iVar1 == 0) {
            ERR_new();
            uVar2 = 0x236;
            goto LAB_001614b5;
          }
          local_24 = local_24 + -1;
        } while (local_28 <= local_24);
      }
      iVar1 = WPACKET_close(param_2);
      if ((iVar1 != 0) && (iVar1 = WPACKET_close(param_2), iVar1 != 0)) {
        uVar2 = 1;
        goto LAB_001614d9;
      }
      ERR_new();
      uVar2 = 0x23b;
    }
LAB_001614b5:
    ERR_set_debug("ssl/statem/extensions_clnt.c",uVar2,"tls_construct_ctos_supported_versions");
    iVar1 = 0xc0103;
  }
  else {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x222,"tls_construct_ctos_supported_versions");
  }
  ossl_statem_fatal(param_1,0x50,iVar1,0);
  uVar2 = 0;
LAB_001614d9:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar2;
}