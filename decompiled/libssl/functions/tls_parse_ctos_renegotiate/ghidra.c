undefined8 tls_parse_ctos_renegotiate(long param_1,byte **param_2)

{
  int iVar1;
  byte *pbVar2;
  byte *__n;
  undefined8 uVar3;
  byte *__s1;
  
  if (param_2[1] != (byte *)0x0) {
    pbVar2 = param_2[1] + -1;
    __n = (byte *)(ulong)**param_2;
    __s1 = *param_2 + 1;
    param_2[1] = pbVar2;
    *param_2 = __s1;
    if (__n <= pbVar2) {
      *param_2 = __s1 + (long)__n;
      param_2[1] = pbVar2 + -(long)__n;
      if (__n == *(byte **)(param_1 + 0x430)) {
        iVar1 = memcmp(__s1,(void *)(param_1 + 0x3f0),(size_t)__n);
        if (iVar1 == 0) {
          *(undefined4 *)(param_1 + 0x480) = 1;
          return 1;
        }
        ERR_new();
        uVar3 = 0x3e;
      }
      else {
        ERR_new();
        uVar3 = 0x38;
      }
      ERR_set_debug("ssl/statem/extensions_srvr.c",uVar3,"tls_parse_ctos_renegotiate");
      ossl_statem_fatal(param_1,0x28,0x151,0);
      return 0;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x32,"tls_parse_ctos_renegotiate");
  ossl_statem_fatal(param_1,0x32,0x150,0);
  return 0;
}