undefined8 tls_construct_key_update(long param_1,undefined8 param_2)

{
  int iVar1;
  
  iVar1 = WPACKET_put_bytes__(param_2,*(undefined4 *)(param_1 + 0xba4),1);
  if (iVar1 != 0) {
    *(undefined4 *)(param_1 + 0xba4) = 0xffffffff;
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_lib.c",0x284,"tls_construct_key_update");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}