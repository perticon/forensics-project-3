ulong ssl_get_algorithm2(long param_1)

{
  long lVar1;
  ulong uVar2;
  
  lVar1 = *(long *)(param_1 + 0x2e0);
  if (lVar1 == 0) {
    uVar2 = 0xffffffffffffffff;
  }
  else {
    uVar2 = (ulong)*(uint *)(lVar1 + 0x40);
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 4) != 0) {
      if (uVar2 == 0x909) {
        uVar2 = 0x404;
      }
      return uVar2;
    }
    if ((*(uint *)(lVar1 + 0x1c) & 0x1c8) != 0) {
      if (uVar2 == 0x505) {
        uVar2 = 0x909;
      }
      return uVar2;
    }
  }
  return uVar2;
}