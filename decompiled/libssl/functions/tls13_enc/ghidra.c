undefined8 tls13_enc(long param_1,undefined4 *param_2,long param_3,int param_4)

{
  byte *pbVar1;
  byte bVar2;
  byte bVar3;
  int iVar4;
  uint uVar5;
  uint uVar6;
  uchar *iv;
  long lVar7;
  ulong uVar8;
  undefined8 uVar9;
  EVP_CIPHER_CTX *ctx;
  long lVar10;
  ulong uVar11;
  byte *pbVar12;
  long in_FS_OFFSET;
  long local_c8;
  int local_ac;
  int local_a8;
  int local_a4;
  long local_a0;
  undefined local_98 [56];
  byte abStack96 [32];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 != 1) {
    ERR_new();
    uVar9 = 0x26;
    goto LAB_0015ced1;
  }
  if (param_4 == 0) {
    ctx = *(EVP_CIPHER_CTX **)(param_1 + 0x848);
    lVar10 = param_1 + 0x850;
    pbVar12 = (byte *)(param_1 + 0x1cf8);
  }
  else {
    ctx = *(EVP_CIPHER_CTX **)(param_1 + 0x878);
    lVar10 = param_1 + 0x880;
    pbVar12 = (byte *)(param_1 + 0x1d00);
  }
  if ((ctx == (EVP_CIPHER_CTX *)0x0) || (param_2[1] == 0x15)) {
    uVar9 = 1;
    memmove(*(void **)(param_2 + 8),*(void **)(param_2 + 10),*(size_t *)(param_2 + 2));
    *(undefined8 *)(param_2 + 10) = *(undefined8 *)(param_2 + 8);
    goto LAB_0015cef6;
  }
  iVar4 = EVP_CIPHER_CTX_get_iv_length(ctx);
  uVar11 = (ulong)iVar4;
  if (*(int *)(param_1 + 0x84) - 3U < 2) {
    lVar7 = *(long *)(param_1 + 0x918);
    if (((lVar7 != 0) && (*(int *)(lVar7 + 0x354) != 0)) ||
       ((lVar7 = *(long *)(param_1 + 0x920), lVar7 != 0 && (*(int *)(lVar7 + 0x354) != 0)))) {
      uVar5 = *(uint *)(*(long *)(lVar7 + 0x2f8) + 0x24);
      goto LAB_0015cce3;
    }
    ERR_new();
    uVar9 = 0x49;
LAB_0015ced1:
    ERR_set_debug("ssl/record/ssl3_record_tls13.c",uVar9,__func___25783);
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    if (*(long *)(param_1 + 0x2e0) == 0) {
      ERR_new();
      uVar9 = 0x54;
      goto LAB_0015ced1;
    }
    uVar5 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x24);
LAB_0015cce3:
    if ((uVar5 & 0x3c000) != 0) {
      uVar6 = uVar5 & 0x30000;
      local_ac = (-(uint)(uVar6 == 0) & 8) + 8;
      uVar8 = (ulong)(-(uint)(uVar6 == 0) & 8) + 9;
      local_c8 = (ulong)(-(uint)(uVar6 == 0) & 8) + 8;
      if (param_4 == 0) {
LAB_0015cf8e:
        if (uVar8 <= *(ulong *)(param_2 + 2)) {
          *(ulong *)(param_2 + 2) = *(ulong *)(param_2 + 2) - local_c8;
          goto LAB_0015cd1f;
        }
        goto LAB_0015cef3;
      }
      iVar4 = EVP_CIPHER_CTX_ctrl(ctx,0x11,local_ac,(void *)0x0);
      if (0 < iVar4) goto LAB_0015cd1f;
      ERR_new();
      uVar9 = 0x61;
      goto LAB_0015ced1;
    }
    if ((uVar5 & 0x83000) == 0) {
      ERR_new();
      uVar9 = 0x69;
      goto LAB_0015ced1;
    }
    local_ac = 0x10;
    if (param_4 == 0) {
      local_c8 = 0x10;
      uVar8 = 0x11;
      goto LAB_0015cf8e;
    }
    local_c8 = 0x10;
LAB_0015cd1f:
    if (uVar11 < 8) {
      ERR_new();
      uVar9 = 0x7a;
      goto LAB_0015ced1;
    }
    iv = (uchar *)__memcpy_chk(abStack96 + 8);
    abStack96[uVar11] = *(byte *)(lVar10 + -8 + uVar11) ^ *pbVar12;
    bVar2 = *(byte *)(lVar10 + -1 + uVar11);
    abStack96[uVar11 + 1] = *(byte *)(lVar10 + -7 + uVar11) ^ pbVar12[1];
    abStack96[uVar11 + 2] = *(byte *)(lVar10 + -6 + uVar11) ^ pbVar12[2];
    abStack96[uVar11 + 3] = *(byte *)(lVar10 + -5 + uVar11) ^ pbVar12[3];
    abStack96[uVar11 + 4] = *(byte *)(lVar10 + -4 + uVar11) ^ pbVar12[4];
    abStack96[uVar11 + 5] = *(byte *)(lVar10 + -3 + uVar11) ^ pbVar12[5];
    abStack96[uVar11 + 6] = *(byte *)(lVar10 + -2 + uVar11) ^ pbVar12[6];
    bVar3 = pbVar12[7] + 1;
    abStack96[uVar11 + 7] = bVar2 ^ pbVar12[7];
    pbVar12[7] = bVar3;
    if (bVar3 == 0) {
      pbVar1 = pbVar12 + 6;
      *pbVar1 = *pbVar1 + 1;
      if (*pbVar1 == 0) {
        pbVar1 = pbVar12 + 5;
        *pbVar1 = *pbVar1 + 1;
        if (*pbVar1 == 0) {
          pbVar1 = pbVar12 + 4;
          *pbVar1 = *pbVar1 + 1;
          if (*pbVar1 == 0) {
            pbVar1 = pbVar12 + 3;
            *pbVar1 = *pbVar1 + 1;
            if (*pbVar1 == 0) {
              pbVar1 = pbVar12 + 2;
              *pbVar1 = *pbVar1 + 1;
              if (*pbVar1 == 0) {
                pbVar1 = pbVar12 + 1;
                *pbVar1 = *pbVar1 + 1;
                if ((*pbVar1 == 0) && (*pbVar12 = *pbVar12 + 1, *pbVar12 == 0)) goto LAB_0015cef3;
              }
            }
          }
        }
      }
    }
    iVar4 = EVP_CipherInit_ex(ctx,(EVP_CIPHER *)0x0,(ENGINE *)0x0,(uchar *)0x0,iv,param_4);
    if ((iVar4 < 1) ||
       ((param_4 == 0 &&
        (iVar4 = EVP_CIPHER_CTX_ctrl(ctx,0x11,local_ac,
                                     (void *)(*(long *)(param_2 + 2) + *(long *)(param_2 + 8))),
        iVar4 < 1)))) {
      ERR_new();
      uVar9 = 0x91;
      goto LAB_0015ced1;
    }
    iVar4 = WPACKET_init_static_len(local_98,abStack96 + 3,5);
    if ((((iVar4 == 0) || (iVar4 = WPACKET_put_bytes__(local_98,param_2[1],1), iVar4 == 0)) ||
        (iVar4 = WPACKET_put_bytes__(local_98,*param_2,2), iVar4 == 0)) ||
       (((iVar4 = WPACKET_put_bytes__(local_98,local_ac + param_2[2],2), iVar4 == 0 ||
         (iVar4 = WPACKET_get_total_written(local_98), iVar4 == 0)) ||
        ((local_a0 != 5 || (iVar4 = WPACKET_finish(local_98), iVar4 == 0)))))) {
      ERR_new();
      uVar9 = 0;
      ERR_set_debug("ssl/record/ssl3_record_tls13.c",0x9d,__func___25783);
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      WPACKET_cleanup(local_98);
      goto LAB_0015cef6;
    }
    if (((uVar5 & 0x3c000) == 0) ||
       (iVar4 = EVP_CipherUpdate(ctx,(uchar *)0x0,&local_a8,(uchar *)0x0,param_2[2]), 0 < iVar4)) {
      iVar4 = EVP_CipherUpdate(ctx,(uchar *)0x0,&local_a8,abStack96 + 3,5);
      if ((0 < iVar4) &&
         (((iVar4 = EVP_CipherUpdate(ctx,*(uchar **)(param_2 + 8),&local_a8,
                                     *(uchar **)(param_2 + 10),param_2[2]), 0 < iVar4 &&
           (iVar4 = EVP_CipherFinal_ex(ctx,(uchar *)((long)local_a8 + *(long *)(param_2 + 8)),
                                       &local_a4), 0 < iVar4)) &&
          ((long)(local_a4 + local_a8) == *(long *)(param_2 + 2))))) {
        uVar9 = 1;
        if (param_4 == 0) goto LAB_0015cef6;
        iVar4 = EVP_CIPHER_CTX_ctrl(ctx,0x10,local_ac,
                                    (void *)((long)(local_a4 + local_a8) + *(long *)(param_2 + 8)));
        if (0 < iVar4) {
          *(long *)(param_2 + 2) = *(long *)(param_2 + 2) + local_c8;
          goto LAB_0015cef6;
        }
        ERR_new();
        uVar9 = 0xb5;
        goto LAB_0015ced1;
      }
    }
  }
LAB_0015cef3:
  uVar9 = 0;
LAB_0015cef6:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar9;
}