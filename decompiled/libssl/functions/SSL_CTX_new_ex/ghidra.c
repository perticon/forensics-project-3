SSL_CTX * SSL_CTX_new_ex(SSL_METHOD *param_1,char *param_2,stack_st_SSL_CIPHER *param_3)

{
  _func_3099 *p_Var1;
  int iVar2;
  SSL_CTX *ptr;
  long lVar3;
  _func_3098 *p_Var4;
  undefined4 extraout_var;
  ssl_session_st *psVar5;
  X509_STORE *pXVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  X509_VERIFY_PARAM *pXVar9;
  stack_st_SSL_COMP *psVar10;
  _func_3094 *p_Var11;
  cert_st *pcVar12;
  _func_3103 *p_Var13;
  char *pcVar14;
  char *pcVar15;
  
  if (param_3 == (stack_st_SSL_CIPHER *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0xc79,"SSL_CTX_new_ex");
    ERR_set_error(0x14,0xc4,0);
    return (SSL_CTX *)0x0;
  }
  iVar2 = OPENSSL_init_ssl(0x200000,0);
  if (iVar2 == 0) {
    return (SSL_CTX *)0x0;
  }
  iVar2 = SSL_get_ex_data_X509_STORE_CTX_idx();
  if (iVar2 < 0) {
    ERR_new();
    ptr = (SSL_CTX *)0x0;
    ERR_set_debug("ssl/ssl_lib.c",0xc81,"SSL_CTX_new_ex");
    ERR_set_error(0x14,0x10d,0);
  }
  else {
    pcVar14 = "ssl/ssl_lib.c";
    pcVar15 = (char *)0x650;
    ptr = (SSL_CTX *)CRYPTO_zalloc(0x650,"ssl/ssl_lib.c",0xc84);
    if (ptr != (SSL_CTX *)0x0) {
      *(undefined4 *)((long)&ptr->app_verify_arg + 4) = 1;
      lVar3 = CRYPTO_THREAD_lock_new();
      *(long *)(ptr[1].sid_ctx + 0x10) = lVar3;
      if (lVar3 == 0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0xc8c,"SSL_CTX_new_ex");
        ERR_set_error(0x14,0xc0100,0);
        CRYPTO_free(ptr);
        return (SSL_CTX *)0x0;
      }
      ptr->method = param_1;
      if (param_2 != (char *)0x0) {
        pcVar14 = "ssl/ssl_lib.c";
        pcVar15 = param_2;
        p_Var4 = (_func_3098 *)CRYPTO_strdup(param_2,"ssl/ssl_lib.c",0xc9b);
        ptr[1].tlsext_ticket_key_cb = p_Var4;
        if (p_Var4 == (_func_3098 *)0x0) goto LAB_0013f080;
      }
      ptr->cipher_list = param_3;
      ptr->msg_callback = (_func_3095 *)0x4;
      *(undefined4 *)&ptr->msg_callback_arg = 0;
      *(undefined4 *)&ptr->new_session_cb = 2;
      ptr->session_cache_tail = (ssl_session_st *)0x5000;
      iVar2 = (*param_3[5].stack.comp)(pcVar15,pcVar14);
      *(undefined8 *)&ptr->verify_mode = 0x19000;
      ptr->remove_session_cb = (_func_3088 *)CONCAT44(extraout_var,iVar2);
      *(undefined4 *)&ptr->default_verify_callback = 0;
      lVar3 = ssl_cert_new();
      *(long *)ptr->sid_ctx = lVar3;
      if (lVar3 != 0) {
        psVar5 = (ssl_session_st *)OPENSSL_LH_new(ssl_session_hash,ssl_session_cmp);
        ptr->session_cache_head = psVar5;
        if (psVar5 != (ssl_session_st *)0x0) {
          pXVar6 = X509_STORE_new();
          ptr->session_cache_size = (ulong)pXVar6;
          if (pXVar6 != (X509_STORE *)0x0) {
            lVar3 = CTLOG_STORE_new_ex(param_1,param_2);
            *(long *)(ptr->tlsext_tick_hmac_key + 8) = lVar3;
            if (lVar3 != 0) {
              iVar2 = ssl_load_ciphers(ptr);
              if (((iVar2 == 0) || (iVar2 = ssl_setup_sig_algs(ptr), iVar2 == 0)) ||
                 (iVar2 = ssl_load_groups(ptr), iVar2 == 0)) goto LAB_0013f0b0;
              uVar7 = OSSL_default_ciphersuites();
              iVar2 = SSL_CTX_set_ciphersuites(ptr,uVar7);
              if (iVar2 != 0) {
                uVar7 = *(undefined8 *)ptr->sid_ctx;
                uVar8 = OSSL_default_cipher_list();
                lVar3 = ssl_create_cipher_list
                                  (ptr,ptr->sessions,&ptr->cipher_list_by_id,&ptr->cert_store,uVar8,
                                   uVar7);
                if ((lVar3 == 0) || (iVar2 = OPENSSL_sk_num(ptr->cipher_list_by_id), iVar2 < 1)) {
                  ERR_new();
                  ERR_set_debug("ssl/ssl_lib.c",0xccc,"SSL_CTX_new_ex");
                  ERR_set_error(0x14,0xa1,0);
                  goto LAB_0013f0b0;
                }
                pXVar9 = X509_VERIFY_PARAM_new();
                *(X509_VERIFY_PARAM **)(ptr->tlsext_tick_key_name + 8) = pXVar9;
                if (pXVar9 != (X509_VERIFY_PARAM *)0x0) {
                  psVar10 = (stack_st_SSL_COMP *)ssl_evp_md_fetch(param_1,4,param_2);
                  ptr->comp_methods = psVar10;
                  p_Var11 = (_func_3094 *)ssl_evp_md_fetch(param_1,0x40,param_2);
                  ptr->info_callback = p_Var11;
                  lVar3 = OPENSSL_sk_new_null();
                  ptr->max_cert_list = lVar3;
                  if (lVar3 != 0) {
                    pcVar12 = (cert_st *)OPENSSL_sk_new_null();
                    ptr->cert = pcVar12;
                    if ((pcVar12 != (cert_st *)0x0) &&
                       (iVar2 = CRYPTO_new_ex_data(1,ptr,(CRYPTO_EX_DATA *)&ptr->sha1), iVar2 != 0))
                    {
                      p_Var13 = (_func_3103 *)CRYPTO_secure_zalloc(0x40,"ssl/ssl_lib.c",0xce4);
                      ptr->next_protos_advertised_cb = p_Var13;
                      if (p_Var13 != (_func_3103 *)0x0) {
                        if ((*(byte *)(*(long *)&param_3[6].stack + 0x60) & 8) == 0) {
                          psVar10 = SSL_COMP_get_compression_methods();
                          ptr->options = (ulong)psVar10;
                        }
                        p_Var1 = _UNK_00186b78;
                        p_Var4 = _DAT_00186b70;
                        ptr->tlsext_ticket_key_cb = _DAT_00186b70;
                        ptr->tlsext_status_cb = p_Var1;
                        iVar2 = RAND_bytes_ex(p_Var4,param_1,&ptr->wbuf_freelist,0x10,0);
                        if (((iVar2 < 1) ||
                            (iVar2 = RAND_priv_bytes_ex(param_1,ptr->next_protos_advertised_cb,0x20,
                                                        0), iVar2 < 1)) ||
                           (iVar2 = RAND_priv_bytes_ex(param_1,ptr->next_protos_advertised_cb + 0x20
                                                       ,0x20,0), iVar2 < 1)) {
                          *(ulong *)&ptr->read_ahead = *(ulong *)&ptr->read_ahead | 0x4000;
                        }
                        iVar2 = RAND_priv_bytes_ex(param_1,&ptr[1].stats.sess_accept_renegotiate,
                                                   0x20,0);
                        if ((0 < iVar2) && (iVar2 = ssl_ctx_srp_ctx_init_intern(ptr), iVar2 != 0)) {
                          *(ulong *)&ptr->read_ahead = *(ulong *)&ptr->read_ahead | 0x120000;
                          *(undefined4 *)&ptr[1].method = 0xffffffff;
                          ptr[1].default_verify_callback = (_func_3096 *)0x400000000000;
                          *(undefined8 *)ptr[1].tlsext_tick_key_name = 2;
                          ssl_ctx_system_config(ptr);
                          return ptr;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
LAB_0013f080:
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0xd40,"SSL_CTX_new_ex");
  ERR_set_error(0x14,0xc0100,0);
LAB_0013f0b0:
  SSL_CTX_free(ptr);
  return (SSL_CTX *)0x0;
}