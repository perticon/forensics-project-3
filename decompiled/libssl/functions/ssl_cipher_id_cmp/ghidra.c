int ssl_cipher_id_cmp(long param_1,long param_2)

{
  uint uVar1;
  int iVar2;
  bool bVar3;
  
  uVar1 = *(uint *)(param_2 + 0x18);
  iVar2 = 1;
  bVar3 = *(uint *)(param_1 + 0x18) < uVar1;
  if (bVar3 || *(uint *)(param_1 + 0x18) == uVar1) {
    iVar2 = -(uint)bVar3;
  }
  return iVar2;
}