undefined8 ssl_srp_ctx_free_intern(long param_1)

{
  ulong uVar1;
  undefined8 *puVar2;
  byte bVar3;
  
  bVar3 = 0;
  if (param_1 != 0) {
    CRYPTO_free(*(void **)(param_1 + 0xbf0));
    CRYPTO_free(*(void **)(param_1 + 0xc38));
    BN_free(*(BIGNUM **)(param_1 + 0xbf8));
    BN_free(*(BIGNUM **)(param_1 + 0xc00));
    BN_free(*(BIGNUM **)(param_1 + 0xc08));
    BN_free(*(BIGNUM **)(param_1 + 0xc10));
    BN_free(*(BIGNUM **)(param_1 + 0xc18));
    BN_free(*(BIGNUM **)(param_1 + 0xc20));
    BN_free(*(BIGNUM **)(param_1 + 0xc28));
    BN_free(*(BIGNUM **)(param_1 + 0xc30));
    *(undefined8 *)(param_1 + 0xbd0) = 0;
    *(undefined8 *)(param_1 + 0xc48) = 0;
    puVar2 = (undefined8 *)(param_1 + 0xbd8U & 0xfffffffffffffff8);
    uVar1 = (ulong)(((int)param_1 - (int)puVar2) + 0xc50U >> 3);
    for (; uVar1 != 0; uVar1 = uVar1 - 1) {
      *puVar2 = 0;
      puVar2 = puVar2 + (ulong)bVar3 * -2 + 1;
    }
    *(undefined4 *)(param_1 + 0xc40) = 0x400;
    return 1;
  }
  return 0;
}