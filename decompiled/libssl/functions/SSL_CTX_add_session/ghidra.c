int SSL_CTX_add_session(SSL_CTX *s,SSL_SESSION *c)

{
  undefined *puVar1;
  undefined *puVar2;
  int iVar3;
  SSL_SESSION *pSVar4;
  long lVar5;
  long lVar6;
  time_t tVar7;
  
  SSL_SESSION_up_ref(c);
  iVar3 = CRYPTO_THREAD_write_lock(*(undefined8 *)(s[1].sid_ctx + 0x10));
  if (iVar3 == 0) {
    SSL_SESSION_free(c);
    return 0;
  }
  pSVar4 = (SSL_SESSION *)OPENSSL_LH_insert(s->session_cache_head,c);
  if (pSVar4 == (SSL_SESSION *)0x0) {
LAB_00143ac0:
    lVar5 = OPENSSL_LH_retrieve(s->session_cache_head,c);
    if (lVar5 == 0) {
      pSVar4 = c;
      if ((*(byte *)((long)&s->new_session_cb + 1) & 4) != 0) {
LAB_00143a93:
        tVar7 = time((time_t *)0x0);
        *(time_t *)(c[1].sid_ctx + 0x1c) = tVar7;
        ssl_session_calculate_timeout(c);
      }
      SSL_SESSION_list_add(s,c);
      if (pSVar4 != (SSL_SESSION *)0x0) goto LAB_00143a66;
    }
    else {
LAB_00143903:
      if ((*(byte *)((long)&s->new_session_cb + 1) & 4) != 0) {
        pSVar4 = (SSL_SESSION *)0x0;
        goto LAB_00143a93;
      }
      SSL_SESSION_list_add(s,c);
    }
    lVar5 = SSL_CTX_ctrl(s,0x2b,0,(void *)0x0);
    if (0 < lVar5) {
      while( true ) {
        lVar5 = SSL_CTX_ctrl(s,0x14,0,(void *)0x0);
        lVar6 = SSL_CTX_ctrl(s,0x2b,0,(void *)0x0);
        if (((lVar5 <= lVar6) || (lVar5 = s->session_timeout, lVar5 == 0)) ||
           (*(long *)(lVar5 + 0x250) == 0)) goto LAB_00143a23;
        lVar6 = OPENSSL_LH_retrieve(s->session_cache_head,lVar5);
        if (lVar6 == 0) break;
        pSVar4 = (SSL_SESSION *)OPENSSL_LH_delete(s->session_cache_head,lVar6);
        puVar1 = *(undefined **)(pSVar4[1].krb5_client_princ + 0x40);
        if ((puVar1 != (undefined *)0x0) &&
           (puVar2 = *(undefined **)(pSVar4[1].krb5_client_princ + 0x38), puVar2 != (undefined *)0x0
           )) {
          if ((long *)puVar1 == &s->session_timeout) {
            if ((int *)puVar2 == &s->session_cache_mode) {
              *(undefined (*) [16])&s->session_cache_mode = (undefined  [16])0x0;
            }
            else {
              s->session_timeout = (long)puVar2;
              *(long **)(puVar2 + 0x328) = &s->session_timeout;
            }
          }
          else if ((int *)puVar2 == &s->session_cache_mode) {
            *(undefined **)&s->session_cache_mode = puVar1;
            *(undefined **)(puVar1 + 800) = puVar2;
          }
          else {
            *(undefined **)(puVar1 + 800) = puVar2;
            *(undefined **)(puVar2 + 0x328) = puVar1;
          }
          *(undefined8 *)(pSVar4[1].krb5_client_princ + 0xa8) = 0;
          *(undefined (*) [16])(pSVar4[1].krb5_client_princ + 0x38) = (undefined  [16])0x0;
        }
        *(undefined4 *)(lVar5 + 0x2b0) = 1;
        if (*(code **)&s->stats != (code *)0x0) {
          (**(code **)&s->stats)(s,lVar5);
        }
        SSL_SESSION_free(pSVar4);
        LOCK();
        *(int *)&s->app_verify_callback = *(int *)&s->app_verify_callback + 1;
      }
      *(undefined4 *)(lVar5 + 0x2b0) = 1;
      if (*(code **)&s->stats != (code *)0x0) {
        iVar3 = 1;
        (**(code **)&s->stats)(s,lVar5);
        goto LAB_00143a71;
      }
    }
LAB_00143a23:
    iVar3 = 1;
  }
  else {
    if (c != pSVar4) {
      puVar1 = *(undefined **)(pSVar4[1].krb5_client_princ + 0x40);
      if ((puVar1 != (undefined *)0x0) &&
         (puVar2 = *(undefined **)(pSVar4[1].krb5_client_princ + 0x38), puVar2 != (undefined *)0x0))
      {
        if ((long *)puVar1 == &s->session_timeout) {
          if ((int *)puVar2 == &s->session_cache_mode) {
            *(undefined (*) [16])&s->session_cache_mode = (undefined  [16])0x0;
          }
          else {
            s->session_timeout = (long)puVar2;
            *(undefined **)(puVar2 + 0x328) = puVar1;
          }
        }
        else if ((int *)puVar2 == &s->session_cache_mode) {
          *(undefined **)&s->session_cache_mode = puVar1;
          *(undefined **)(puVar1 + 800) = puVar2;
        }
        else {
          *(undefined **)(puVar1 + 800) = puVar2;
          *(undefined **)(puVar2 + 0x328) = puVar1;
        }
        *(undefined8 *)(pSVar4[1].krb5_client_princ + 0xa8) = 0;
        *(undefined (*) [16])(pSVar4[1].krb5_client_princ + 0x38) = (undefined  [16])0x0;
      }
      SSL_SESSION_free(pSVar4);
      goto LAB_00143903;
    }
    if (pSVar4 == (SSL_SESSION *)0x0) goto LAB_00143ac0;
    if ((*(byte *)((long)&s->new_session_cb + 1) & 4) != 0) goto LAB_00143a93;
    SSL_SESSION_list_add(s,c);
LAB_00143a66:
    iVar3 = 0;
    SSL_SESSION_free(pSVar4);
  }
LAB_00143a71:
  CRYPTO_THREAD_unlock(*(undefined8 *)(s[1].sid_ctx + 0x10));
  return iVar3;
}