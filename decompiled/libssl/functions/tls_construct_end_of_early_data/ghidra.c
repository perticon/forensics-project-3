undefined8 tls_construct_end_of_early_data(long param_1)

{
  if ((*(uint *)(param_1 + 0x84) & 0xfffffffb) == 3) {
    *(undefined4 *)(param_1 + 0x84) = 7;
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/statem_clnt.c",0xec0,"tls_construct_end_of_early_data");
  ossl_statem_fatal(param_1,0x50,0xc0101,0);
  return 0;
}