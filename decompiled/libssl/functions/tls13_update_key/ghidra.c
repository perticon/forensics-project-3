int tls13_update_key(long param_1,int param_2)

{
  undefined8 uVar1;
  int iVar2;
  int iVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  long lVar6;
  void *__dest;
  long in_FS_OFFSET;
  undefined local_c8 [64];
  undefined local_88 [72];
  long local_40;
  
  __dest = (void *)(param_1 + 0x784);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar4 = ssl_handshake_md();
  iVar2 = EVP_MD_get_size(uVar4);
  if (*(int *)(param_1 + 0x38) != param_2) {
    __dest = (void *)(param_1 + 0x744);
  }
  if (param_2 == 0) {
    uVar4 = *(undefined8 *)(param_1 + 0x848);
    lVar6 = param_1 + 0x850;
    RECORD_LAYER_reset_read_sequence(param_1 + 0xc58);
  }
  else {
    *(undefined4 *)(param_1 + 0x7c) = 1;
    uVar4 = *(undefined8 *)(param_1 + 0x878);
    lVar6 = param_1 + 0x880;
    RECORD_LAYER_reset_write_sequence();
  }
  uVar1 = *(undefined8 *)(param_1 + 800);
  uVar5 = ssl_handshake_md(param_1);
  iVar3 = derive_secret_key_and_iv
                    (param_1,param_2,uVar5,uVar1,__dest,0,"traffic upd",0xb,local_88,local_c8,lVar6,
                     uVar4);
  if (iVar3 != 0) {
    iVar3 = 1;
    memcpy(__dest,local_88,(long)iVar2);
    *(undefined4 *)(param_1 + 0x7c) = 0;
  }
  OPENSSL_cleanse(local_c8,0x40);
  OPENSSL_cleanse(local_88,0x40);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}