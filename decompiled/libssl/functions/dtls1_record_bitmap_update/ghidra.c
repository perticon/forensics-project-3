void dtls1_record_bitmap_update(long param_1,ulong *param_2)

{
  ulong uVar1;
  ulong uVar2;
  int iVar3;
  long lVar4;
  ulong uVar5;
  
  uVar2 = *(ulong *)(param_1 + 0x1cf8);
  uVar1 = param_2[1];
  uVar5 = uVar2 >> 0x38 | (uVar2 & 0xff000000000000) >> 0x28 | (uVar2 & 0xff0000000000) >> 0x18 |
          (uVar2 & 0xff00000000) >> 8 | (uVar2 & 0xff000000) << 8 | (uVar2 & 0xff0000) << 0x18 |
          (uVar2 & 0xff00) << 0x28 | uVar2 << 0x38;
  uVar2 = uVar1 >> 0x38 | (uVar1 & 0xff000000000000) >> 0x28 | (uVar1 & 0xff0000000000) >> 0x18 |
          (uVar1 & 0xff00000000) >> 8 | (uVar1 & 0xff000000) << 8 | (uVar1 & 0xff0000) << 0x18 |
          (uVar1 & 0xff00) << 0x28 | uVar1 << 0x38;
  lVar4 = uVar5 - uVar2;
  if ((uVar2 < uVar5) && (lVar4 < 0)) {
LAB_001544a8:
    *param_2 = 1;
    param_2[1] = *(ulong *)(param_1 + 0x1cf8);
    return;
  }
  if ((uVar2 <= uVar5) || (lVar4 < 1)) {
    if (0x80 < lVar4) goto LAB_001544a8;
    if (-0x81 < lVar4) {
      iVar3 = (int)lVar4;
      if (0 < iVar3) {
        if (iVar3 < 0x40) {
          *param_2 = *param_2 << ((byte)lVar4 & 0x3f) | 1;
          param_2[1] = *(ulong *)(param_1 + 0x1cf8);
          return;
        }
        goto LAB_001544a8;
      }
      if (-0x40 < iVar3) {
        *param_2 = *param_2 | 1 << (-(byte)lVar4 & 0x3f);
      }
    }
  }
  return;
}