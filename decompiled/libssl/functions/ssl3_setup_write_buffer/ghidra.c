undefined8 ssl3_setup_write_buffer(long param_1,long param_2,void *param_3)

{
  uint uVar1;
  int iVar2;
  int iVar3;
  void *pvVar4;
  void **ppvVar5;
  long lVar6;
  
  *(long *)(param_1 + 0xc70) = param_2;
  if (param_3 == (void *)0x0) {
    uVar1 = *(uint *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8;
    iVar2 = ssl_get_max_send_fragment();
    iVar3 = ssl_allow_compression(param_1);
    lVar6 = (ulong)(iVar2 + 0x50) + (-(ulong)(uVar1 == 0) & 0xfffffffffffffff7) + 0xe;
    param_3 = (void *)(lVar6 + 7);
    if (iVar3 != 0) {
      param_3 = (void *)(lVar6 + 0x407);
    }
    if ((*(byte *)(param_1 + 0x9e9) & 8) == 0) {
      param_3 = (void *)((-(ulong)(uVar1 == 0) & 0xfffffffffffffff7) + 0x65 + (long)param_3);
    }
  }
  lVar6 = 0;
  ppvVar5 = (void **)(param_1 + 0xca8);
  if (param_2 == 0) {
    return 1;
  }
  do {
    while (ppvVar5[2] == param_3) {
      if (*ppvVar5 == (void *)0x0) goto LAB_00159504;
      lVar6 = lVar6 + 1;
      ppvVar5 = ppvVar5 + 6;
      if (param_2 == lVar6) {
        return 1;
      }
    }
    CRYPTO_free(*ppvVar5);
    *ppvVar5 = (void *)0x0;
LAB_00159504:
    pvVar4 = CRYPTO_malloc((int)param_3,"ssl/record/ssl3_buffer.c",0x77);
    if (pvVar4 == (void *)0x0) {
      *(long *)(param_1 + 0xc70) = lVar6;
      ERR_new();
      ERR_set_debug("ssl/record/ssl3_buffer.c",0x7f,"ssl3_setup_write_buffer");
      ossl_statem_fatal(param_1,0xffffffff,0xc0100,0);
      return 0;
    }
    lVar6 = lVar6 + 1;
    *ppvVar5 = pvVar4;
    *(undefined (*) [16])(ppvVar5 + 1) = (undefined  [16])0x0;
    *(undefined (*) [16])(ppvVar5 + 3) = (undefined  [16])0x0;
    ppvVar5[5] = (void *)0x0;
    ppvVar5[2] = param_3;
    ppvVar5 = ppvVar5 + 6;
    if (param_2 == lVar6) {
      return 1;
    }
  } while( true );
}