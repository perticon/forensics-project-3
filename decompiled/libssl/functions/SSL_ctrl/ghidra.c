long SSL_ctrl(SSL *ssl,int cmd,long larg,void *parg)

{
  uint uVar1;
  int iVar2;
  long lVar3;
  char **ppcVar4;
  undefined4 in_register_00000034;
  
  uVar1 = (uint)larg;
  switch(cmd) {
  case 0x10:
    *(void **)&ssl[1].tlsext_ocsp_resplen = parg;
    return 1;
  default:
                    /* WARNING: Could not recover jumptable at 0x001386fa. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    lVar3 = (*ssl->method->ssl_ctrl)(ssl,(int)CONCAT44(in_register_00000034,cmd),larg);
    return lVar3;
  case 0x21:
    uVar1 = uVar1 | *(uint *)&ssl[3].tlsext_debug_arg;
    *(uint *)&ssl[3].tlsext_debug_arg = uVar1;
    return (ulong)uVar1;
  case 0x28:
    return (long)*(int *)&ssl[4].client_CA;
  case 0x29:
    iVar2 = *(int *)&ssl[4].client_CA;
    *(uint *)&ssl[4].client_CA = uVar1;
    return (long)iVar2;
  case 0x32:
    return *(long *)&ssl[3].servername_done;
  case 0x33:
    lVar3 = 0;
    if (-1 < larg) {
      lVar3 = *(long *)&ssl[3].servername_done;
      *(long *)&ssl[3].servername_done = larg;
      return lVar3;
    }
    break;
  case 0x34:
    if (0x3e00 < larg - 0x200U) {
      return 0;
    }
    ssl[3].tlsext_ocsp_exts = (X509_EXTENSIONS *)larg;
    if (ssl[3].tlsext_ocsp_ids <= (ulong)larg) {
      return 1;
    }
    goto LAB_0013875c;
  case 0x4c:
    return (long)(int)ssl[1].max_send_fragment;
  case 0x4e:
    uVar1 = ~uVar1 & *(uint *)&ssl[3].tlsext_debug_arg;
    *(uint *)&ssl[3].tlsext_debug_arg = uVar1;
    return (ulong)uVar1;
  case 99:
    uVar1 = uVar1 | *(uint *)((ssl[3].d1)->cookie + 0x18);
    *(uint *)((ssl[3].d1)->cookie + 0x18) = uVar1;
    return (ulong)uVar1;
  case 100:
    uVar1 = ~uVar1 & *(uint *)((ssl[3].d1)->cookie + 0x18);
    *(uint *)((ssl[3].d1)->cookie + 0x18) = uVar1;
    return (ulong)uVar1;
  case 0x6e:
    if (parg == (void *)0x0) {
      return 2;
    }
    lVar3 = 0;
    if (ssl[1].msg_callback_arg != (void *)0x0) {
      iVar2 = ssl[1].hit;
      *(void **)parg = ssl[1].msg_callback_arg;
      return (long)iVar2;
    }
    break;
  case 0x7a:
    if (((*(long *)&ssl[3].sid_ctx_length != 0) && (iVar2 = SSL_in_init(ssl), iVar2 == 0)) &&
       (iVar2 = ossl_statem_get_in_handshake(ssl), iVar2 == 0)) {
      return (ulong)(*(uint *)(*(long *)&ssl[3].sid_ctx_length + 0x388) & 1);
    }
    return -1;
  case 0x7b:
    iVar2 = ssl_check_allowed_versions(larg & 0xffffffff,*(undefined4 *)&ssl[3].tlsext_hostname);
    ppcVar4 = (char **)((long)&ssl[3].tlsext_debug_arg + 4);
    lVar3 = 0;
    if (iVar2 != 0) {
LAB_00138851:
      iVar2 = ssl_set_version_bound
                        (**(undefined4 **)(*(long *)&ssl[3].ex_data.dummy + 8),larg & 0xffffffff,
                         ppcVar4);
      return (ulong)(iVar2 != 0);
    }
    break;
  case 0x7c:
    iVar2 = ssl_check_allowed_versions
                      (*(undefined4 *)((long)&ssl[3].tlsext_debug_arg + 4),larg & 0xffffffff);
    lVar3 = 0;
    if (iVar2 != 0) {
      ppcVar4 = &ssl[3].tlsext_hostname;
      goto LAB_00138851;
    }
    break;
  case 0x7d:
    if ((ssl[3].tlsext_ocsp_exts < (ulong)larg) || (larg == 0)) {
      return 0;
    }
LAB_0013875c:
    ssl[3].tlsext_ocsp_ids = (stack_st_OCSP_RESPID *)larg;
    return 1;
  case 0x7e:
    if (0x1f < larg - 1U) {
      return 0;
    }
    ssl[3].tlsext_ocsp_resp = (uchar *)larg;
    if (larg == 1) {
      return 1;
    }
    *(undefined4 *)&ssl[4].client_CA = 1;
    return 1;
  case 0x82:
    return (long)*(int *)((long)&ssl[3].tlsext_debug_arg + 4);
  case 0x83:
    return (long)*(int *)&ssl[3].tlsext_hostname;
  case 0x88:
    ssl->rwstate = 8;
    lVar3 = 1;
  }
  return lVar3;
}