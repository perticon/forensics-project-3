undefined8 ssl_hmac_final(long *param_1)

{
  undefined8 uVar1;
  
  if (*param_1 != 0) {
    uVar1 = (*(code *)PTR_EVP_MAC_final_001a85e8)(*param_1);
    return uVar1;
  }
  if (param_1[1] == 0) {
    return 0;
  }
  uVar1 = ssl_hmac_old_final();
  return uVar1;
}