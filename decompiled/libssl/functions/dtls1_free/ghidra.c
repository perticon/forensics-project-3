void dtls1_free(long param_1)

{
  void *ptr;
  
  DTLS_RECORD_LAYER_free(param_1 + 0xc58);
  ssl3_free(param_1);
  ptr = *(void **)(param_1 + 0x4b8);
  if (ptr != (void *)0x0) {
    dtls1_clear_received_buffer(param_1);
    dtls1_clear_sent_buffer(param_1);
    pqueue_free(*(pqueue *)(*(long *)(param_1 + 0x4b8) + 0x118));
    pqueue_free(*(pqueue *)(*(long *)(param_1 + 0x4b8) + 0x120));
    ptr = *(void **)(param_1 + 0x4b8);
  }
  CRYPTO_free(ptr);
  *(undefined8 *)(param_1 + 0x4b8) = 0;
  return;
}