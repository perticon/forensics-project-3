void SSL_get0_next_proto_negotiated(SSL *s,uchar **data,uint *len)

{
  uchar *puVar1;
  uint uVar2;
  
  puVar1 = (uchar *)s[4].init_msg;
  uVar2 = 0;
  *data = puVar1;
  if (puVar1 != (uchar *)0x0) {
    uVar2 = s[4].init_num;
  }
  *len = uVar2;
  return;
}