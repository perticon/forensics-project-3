long tls1_final_finish_mac(long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4)

{
  int iVar1;
  long lVar2;
  long in_FS_OFFSET;
  size_t local_90;
  undefined local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  lVar2 = (-(ulong)((*(uint *)(*(long *)(param_1 + 0x2e0) + 0x1c) & 0x200) == 0) &
          0xffffffffffffffec) + 0x20;
  iVar1 = ssl3_digest_cached_records(param_1,0);
  if (iVar1 != 0) {
    iVar1 = ssl_handshake_hash(param_1,local_88,0x40,&local_90);
    if (iVar1 != 0) {
      iVar1 = tls1_PRF_constprop_0
                        (param_1,param_2,param_3,local_88,local_90,0,0,0,0,
                         *(long *)(param_1 + 0x918) + 0x50,
                         *(undefined8 *)(*(long *)(param_1 + 0x918) + 8),param_4,lVar2,1);
      if (iVar1 != 0) {
        OPENSSL_cleanse(local_88,local_90);
        goto LAB_00146cf9;
      }
    }
  }
  lVar2 = 0;
LAB_00146cf9:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return lVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}