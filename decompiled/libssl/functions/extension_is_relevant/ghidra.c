uint extension_is_relevant(int *param_1,ulong param_2,ulong param_3)

{
  int iVar1;
  bool bVar2;
  uint uVar3;
  
  uVar3 = *(uint *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8;
  if ((param_3 & 0x800) == 0) {
    if (uVar3 == 0) {
      iVar1 = **(int **)(param_1 + 2);
      if ((0x303 < iVar1) && (iVar1 != 0x10000)) {
        if ((*param_1 == 0x300) && ((param_2 & 8) == 0)) {
          return 0;
        }
        goto LAB_0015ee61;
      }
    }
    else if ((param_2 & 4) != 0) {
      return 0;
    }
    if (*param_1 == 0x300) {
      bVar2 = false;
LAB_0015eec2:
      if ((param_2 & 8) == 0) {
        return 0;
      }
      if (bVar2) goto LAB_0015ee61;
    }
    if ((param_2 & 0x20) != 0) {
      if ((param_3 & 0x80) == 0) {
        return 0;
      }
      if (param_1[0xe] != 0) {
        return 0;
      }
    }
  }
  else {
    if ((uVar3 != 0) && ((param_2 & 4) != 0)) {
      return 0;
    }
    bVar2 = true;
    if (*param_1 == 0x300) goto LAB_0015eec2;
LAB_0015ee61:
    if ((param_2 & 0x10) != 0) {
      return 0;
    }
  }
  if (param_1[0x134] == 0) {
    return 1;
  }
  return ((uint)(param_2 >> 6) & 0x3ffffff ^ 1) & 1;
}