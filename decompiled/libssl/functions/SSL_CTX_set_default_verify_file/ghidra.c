bool SSL_CTX_set_default_verify_file(undefined8 *param_1)

{
  X509_LOOKUP_METHOD *m;
  X509_LOOKUP *pXVar1;
  undefined8 uVar2;
  
  m = X509_LOOKUP_file();
  pXVar1 = X509_STORE_add_lookup((X509_STORE *)param_1[5],m);
  if (pXVar1 != (X509_LOOKUP *)0x0) {
    uVar2 = 0x13a62d;
    ERR_set_mark();
    X509_LOOKUP_ctrl_ex(pXVar1,1,0,3,0,*param_1,param_1[0x88],uVar2);
    ERR_pop_to_mark();
  }
  return pXVar1 != (X509_LOOKUP *)0x0;
}