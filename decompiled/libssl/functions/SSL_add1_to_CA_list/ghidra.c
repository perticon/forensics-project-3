undefined4 SSL_add1_to_CA_list(long param_1,X509 *param_2)

{
  int iVar1;
  X509_NAME *pXVar2;
  long lVar3;
  
  if (param_2 == (X509 *)0x0) {
    return 0;
  }
  if (*(long *)(param_1 + 0x9d0) == 0) {
    lVar3 = OPENSSL_sk_new_null();
    *(long *)(param_1 + 0x9d0) = lVar3;
    if (lVar3 == 0) {
      return 0;
    }
  }
  pXVar2 = X509_get_subject_name(param_2);
  pXVar2 = X509_NAME_dup(pXVar2);
  if (pXVar2 == (X509_NAME *)0x0) {
    return 0;
  }
  iVar1 = OPENSSL_sk_push(*(undefined8 *)(param_1 + 0x9d0),pXVar2);
  if (iVar1 == 0) {
    X509_NAME_free(pXVar2);
    return 0;
  }
  return 1;
}