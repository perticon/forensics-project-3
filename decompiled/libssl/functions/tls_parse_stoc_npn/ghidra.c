int tls_parse_stoc_npn(long param_1,byte **param_2)

{
  code *pcVar1;
  int iVar2;
  byte *pbVar3;
  undefined8 *puVar4;
  long lVar5;
  byte *pbVar6;
  ulong uVar7;
  byte *pbVar8;
  undefined8 *puVar9;
  long in_FS_OFFSET;
  byte bVar10;
  byte local_29;
  undefined8 *local_28;
  long local_20;
  
  bVar10 = 0;
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*(long *)(param_1 + 0x240) == 0) || (iVar2 = 1, *(long *)(param_1 + 0x2c8) == 0)) {
    pcVar1 = *(code **)(*(long *)(param_1 + 0x9a8) + 0x2c8);
    if (pcVar1 == (code *)0x0) {
      ERR_new();
      iVar2 = 0;
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x603,"tls_parse_stoc_npn");
      ossl_statem_fatal(param_1,0x6e,0x6e,0);
    }
    else {
      pbVar3 = param_2[1];
      pbVar6 = *param_2;
      do {
        if (pbVar3 == (byte *)0x0) {
          iVar2 = (*pcVar1)(param_1,&local_28,&local_29,*param_2,param_2[1],
                            *(undefined8 *)(*(long *)(param_1 + 0x9a8) + 0x2d0));
          if (iVar2 == 0) {
            CRYPTO_free(*(void **)(param_1 + 0xb18));
            puVar4 = (undefined8 *)
                     CRYPTO_malloc((uint)local_29,"ssl/statem/extensions_clnt.c",0x61b);
            *(undefined8 **)(param_1 + 0xb18) = puVar4;
            if (puVar4 == (undefined8 *)0x0) {
              *(undefined8 *)(param_1 + 0xb20) = 0;
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_clnt.c",0x61e,"tls_parse_stoc_npn");
              ossl_statem_fatal(param_1,0x50,0xc0103,0);
            }
            else {
              uVar7 = (ulong)local_29;
              if (local_29 < 8) {
                if ((local_29 & 4) == 0) {
                  if ((local_29 != 0) &&
                     (*(undefined *)puVar4 = *(undefined *)local_28, (local_29 & 2) != 0)) {
                    *(undefined2 *)((long)puVar4 + (uVar7 - 2)) =
                         *(undefined2 *)((long)local_28 + (uVar7 - 2));
                  }
                }
                else {
                  *(undefined4 *)puVar4 = *(undefined4 *)local_28;
                  *(undefined4 *)((long)puVar4 + (uVar7 - 4)) =
                       *(undefined4 *)((long)local_28 + (uVar7 - 4));
                }
              }
              else {
                *puVar4 = *local_28;
                *(undefined8 *)((long)puVar4 + (uVar7 - 8)) =
                     *(undefined8 *)((long)local_28 + (uVar7 - 8));
                lVar5 = (long)puVar4 -
                        (long)(undefined8 *)((ulong)(puVar4 + 1) & 0xfffffffffffffff8);
                puVar9 = (undefined8 *)((long)local_28 - lVar5);
                puVar4 = (undefined8 *)((ulong)(puVar4 + 1) & 0xfffffffffffffff8);
                for (uVar7 = (ulong)((uint)local_29 + (int)lVar5 >> 3); uVar7 != 0;
                    uVar7 = uVar7 - 1) {
                  *puVar4 = *puVar9;
                  puVar9 = puVar9 + (ulong)bVar10 * -2 + 1;
                  puVar4 = puVar4 + (ulong)bVar10 * -2 + 1;
                }
              }
              *(ulong *)(param_1 + 0xb20) = (ulong)local_29;
              iVar2 = 1;
              *(undefined4 *)(param_1 + 0x484) = 1;
            }
          }
          else {
            ERR_new();
            iVar2 = 0;
            ERR_set_debug("ssl/statem/extensions_clnt.c",0x612,"tls_parse_stoc_npn");
            ossl_statem_fatal(param_1,0x28,0x6e,0);
          }
          goto LAB_00163596;
        }
        pbVar8 = (byte *)(ulong)*pbVar6;
        if (pbVar3 + -1 < pbVar8) break;
        pbVar6 = pbVar6 + 1 + (long)pbVar8;
        pbVar3 = pbVar3 + -1 + -(long)pbVar8;
      } while (pbVar8 != (byte *)0x0);
      ERR_new();
      iVar2 = 0;
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x5ee,"ssl_next_proto_validate");
      ossl_statem_fatal(param_1,0x32,0x6e,0);
    }
  }
LAB_00163596:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar2;
}