ulong ssl3_cbc_digest_record
                (EVP_MD *param_1,uchar *param_2,ulong *param_3,undefined8 *param_4,
                undefined8 *param_5,long param_6,ulong param_7,void *param_8,ulong param_9,
                char param_10)

{
  byte *pbVar1;
  byte bVar2;
  byte bVar3;
  bool bVar4;
  int iVar5;
  uint uVar6;
  long lVar7;
  ulong uVar8;
  EVP_MD_CTX *ctx;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  ulong extraout_RDX;
  ulong extraout_RDX_00;
  ulong uVar12;
  byte bVar13;
  ulong uVar14;
  undefined8 *puVar15;
  undefined8 *puVar16;
  long lVar17;
  ulong uVar18;
  byte bVar19;
  byte bVar20;
  size_t __n;
  long lVar21;
  ulong uVar22;
  long in_FS_OFFSET;
  byte bVar23;
  ushort uVar24;
  ulong local_3e8;
  ulong local_3e0;
  ulong local_3d0;
  ulong local_3c8;
  ulong local_3c0;
  long local_3b8;
  ulong local_398;
  SHA512_CTX *local_388;
  code *local_380;
  ulong local_378;
  code *local_370;
  ulong local_368;
  ulong local_360;
  ulong local_310;
  uint local_2fc;
  SHA512_CTX local_2f8;
  byte abStack544 [8];
  undefined8 local_218 [2];
  undefined local_208 [5];
  byte abStack515 [3];
  undefined4 uStack512;
  undefined4 uStack508;
  undefined local_1f8 [8];
  uint uStack496;
  uint uStack492;
  undefined local_1e8 [8];
  uint uStack480;
  uint uStack476;
  undefined local_1d8 [8];
  uint uStack464;
  uint uStack460;
  undefined8 local_1c8;
  undefined8 uStack448;
  undefined8 local_1b8;
  undefined8 uStack432;
  undefined8 local_1a8;
  undefined8 uStack416;
  undefined8 local_198;
  undefined8 uStack400;
  undefined8 local_188;
  undefined8 uStack384;
  undefined8 local_178;
  undefined8 uStack368;
  undefined8 local_168;
  undefined8 uStack352;
  undefined8 local_158;
  undefined8 uStack336;
  undefined local_148 [8];
  undefined4 local_140;
  undefined local_13c;
  undefined local_13b [115];
  undefined4 local_c8;
  undefined4 uStack196;
  undefined4 uStack192;
  undefined4 uStack188;
  uint local_b8;
  uint uStack180;
  uint uStack176;
  uint uStack172;
  uint local_a8;
  uint uStack164;
  uint uStack160;
  uint uStack156;
  uint local_98;
  uint uStack148;
  uint uStack144;
  uint uStack140;
  long local_40;
  
  bVar23 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_7 < 0x100000) {
    iVar5 = EVP_MD_is_a(param_1,"MD5");
    if (iVar5 == 0) {
      iVar5 = EVP_MD_is_a(param_1,&DAT_00180502);
      if (iVar5 == 0) {
        iVar5 = EVP_MD_is_a(param_1,"SHA2-224");
        if (iVar5 == 0) {
          iVar5 = EVP_MD_is_a(param_1,"SHA2-256");
          if (iVar5 == 0) {
            iVar5 = EVP_MD_is_a(param_1,"SHA2-384");
            if (iVar5 == 0) {
              uVar12 = EVP_MD_is_a(param_1,"SHA2-512");
              if ((int)uVar12 == 0) {
                if (param_3 != (ulong *)0x0) {
                  *param_3 = 0;
                  goto LAB_0012669b;
                }
              }
              else {
                iVar5 = SHA512_Init(&local_2f8);
                if (0 < iVar5) {
                  local_3e8 = 0x80;
                  lVar7 = 0x4f;
                  lVar17 = 0x73;
                  local_398 = 4;
                  iVar5 = 0x400;
                  bVar4 = true;
                  lVar21 = 0x10;
                  local_380 = (code *)PTR_SHA512_Transform_001a7f98;
                  local_310 = 0x28;
                  local_378 = 0x40;
                  local_370 = tls1_sha512_final_raw;
                  goto LAB_00126750;
                }
              }
            }
            else {
              iVar5 = SHA384_Init(&local_2f8);
              if (0 < iVar5) {
                local_3e8 = 0x80;
                lVar7 = 0x5f;
                lVar17 = 0x73;
                local_398 = 4;
                iVar5 = 0x400;
                bVar4 = true;
                lVar21 = 0x10;
                local_380 = (code *)PTR_SHA512_Transform_001a7f98;
                local_310 = 0x28;
                local_378 = 0x30;
                local_370 = tls1_sha512_final_raw;
                goto LAB_00126750;
              }
            }
          }
          else {
            iVar5 = SHA256_Init((SHA256_CTX *)&local_2f8);
            if (0 < iVar5) {
              local_3e8 = 0x40;
              lVar7 = 0x27;
              lVar17 = 0x33;
              local_398 = 6;
              iVar5 = 0x200;
              bVar4 = true;
              lVar21 = 8;
              local_380 = (code *)PTR_SHA256_Transform_001a7fd0;
              local_310 = 0x28;
              local_378 = 0x20;
              local_370 = tls1_sha256_final_raw;
              goto LAB_00126750;
            }
          }
        }
        else {
          iVar5 = SHA224_Init((SHA256_CTX *)&local_2f8);
          if (0 < iVar5) {
            local_3e8 = 0x40;
            lVar7 = 0x2b;
            lVar17 = 0x33;
            local_398 = 6;
            iVar5 = 0x200;
            bVar4 = true;
            lVar21 = 8;
            local_380 = (code *)PTR_SHA256_Transform_001a7fd0;
            local_310 = 0x28;
            local_378 = 0x1c;
            local_370 = tls1_sha256_final_raw;
            goto LAB_00126750;
          }
        }
      }
      else {
        iVar5 = SHA1_Init((SHA_CTX *)&local_2f8);
        if (0 < iVar5) {
          local_380 = (code *)PTR_SHA1_Transform_001a7fa8;
          local_3e8 = 0x40;
          lVar7 = 0x33;
          lVar17 = 0x33;
          local_398 = 6;
          iVar5 = 0x200;
          bVar4 = true;
          lVar21 = 8;
          local_310 = 0x28;
          local_378 = 0x14;
          local_370 = tls1_sha1_final_raw;
          goto LAB_00126750;
        }
      }
    }
    else {
      iVar5 = MD5_Init((MD5_CTX *)&local_2f8);
      if (0 < iVar5) {
        lVar7 = 0x37;
        local_398 = 6;
        lVar17 = 0x33;
        iVar5 = 0x200;
        bVar4 = false;
        local_310 = 0x30;
        lVar21 = 8;
        local_380 = (code *)PTR_MD5_Transform_001a7fc0;
        local_3e8 = 0x40;
        local_378 = 0x10;
        local_370 = tls1_md5_final_raw;
LAB_00126750:
        local_388 = &local_2f8;
        if (param_10 == '\0') {
          uVar12 = ((local_3e8 + 0xc + param_7 + lVar21) - local_378) / local_3e8;
          if (local_398 < uVar12) {
            local_3c0 = uVar12 - local_398;
            local_3e0 = local_3e8 * local_3c0;
            local_398 = uVar12;
          }
          else {
            local_3e0 = 0;
            local_3c0 = 0;
          }
          puVar15 = &local_1c8;
          for (uVar12 = local_3e8 >> 3; uVar12 != 0; uVar12 = uVar12 - 1) {
            *puVar15 = 0;
            puVar15 = puVar15 + (ulong)bVar23 * -2 + 1;
          }
          if (0x80 < param_9) goto LAB_00126699;
          __memcpy_chk(&local_1c8,param_8,param_9);
          uVar10 = local_3e8 >> 4;
          local_1c8 = local_1c8 ^ _DAT_00180530;
          uStack448 = uStack448 ^ _UNK_00180538;
          local_1b8 = local_1b8 ^ _DAT_00180530;
          uStack432 = uStack432 ^ _UNK_00180538;
          local_1a8 = local_1a8 ^ _DAT_00180530;
          uStack416 = uStack416 ^ _UNK_00180538;
          local_198 = local_198 ^ _DAT_00180530;
          uStack400 = uStack400 ^ _UNK_00180538;
          uVar12 = _DAT_00180530;
          uVar11 = local_198;
          if (local_3e8 != 0x40) {
            local_188 = local_188 ^ _DAT_00180530;
            uStack384 = uStack384 ^ _UNK_00180538;
            uVar11 = local_188;
            if (uVar10 != 5) {
              local_178 = local_178 ^ _DAT_00180530;
              uStack368 = uStack368 ^ _UNK_00180538;
              uVar11 = local_178;
              if (uVar10 != 6) {
                uVar11 = local_168 ^ _DAT_00180530;
                uStack352 = uStack352 ^ _UNK_00180538;
                local_168 = uVar11;
                if (uVar10 == 8) {
                  uVar12 = _DAT_00180530 ^ local_158;
                  uStack336 = _UNK_00180538 ^ uStack336;
                  local_158 = uVar12;
                }
              }
            }
          }
          uVar22 = param_6 + 0xd;
          local_3d0 = param_7 + 0xd;
          local_368 = uVar22 / local_3e8;
          uVar10 = uVar22 % local_3e8;
          local_360 = (lVar21 + uVar22) / local_3e8;
          iVar5 = iVar5 + (int)uVar22 * 8;
          (*local_380)(uVar12,uVar11,local_388,&local_1c8,(lVar21 + uVar22) % local_3e8);
          local_3c8 = 0xd;
        }
        else {
          local_3c8 = local_310 + 0xb + param_9;
          local_3d0 = param_7 + local_3c8;
          uVar12 = param_6 + local_3c8;
          local_398 = (lVar7 + local_3d0) / local_3e8;
          local_368 = uVar12 / local_3e8;
          uVar10 = uVar12 % local_3e8;
          local_360 = (lVar21 + uVar12) / local_3e8;
          iVar5 = (int)uVar12 * 8;
          if (local_398 < 4) {
            local_398 = 2;
            local_3e0 = 0;
            local_3c0 = 0;
          }
          else {
            local_3c0 = local_398 - 2;
            local_3e0 = local_3e8 * local_3c0;
          }
        }
        bVar13 = (byte)((uint)iVar5 >> 0x18);
        bVar20 = (byte)((uint)iVar5 >> 0x10);
        bVar19 = (byte)((uint)iVar5 >> 8);
        if (bVar4) {
          __memset_chk(local_218,0);
          abStack544[lVar21 + 4] = bVar13;
          abStack544[lVar21 + 5] = bVar20;
          abStack544[lVar21 + 6] = bVar19;
          abStack544[lVar21 + 7] = (byte)iVar5;
        }
        else {
          uVar6 = 0;
          do {
            uVar12 = (ulong)uVar6;
            uVar6 = uVar6 + 8;
            *(undefined8 *)((long)local_218 + uVar12) = 0;
          } while (uVar6 < (uint)lVar21);
          abStack544[lVar21 + 3] = bVar13;
          abStack544[lVar21 + 2] = bVar20;
          abStack544[lVar21 + 1] = bVar19;
          abStack544[lVar21] = (byte)iVar5;
        }
        if (local_3e0 != 0) {
          if (param_10 == '\0') {
            local_148 = *param_4;
            local_140 = *(undefined4 *)(param_4 + 1);
            local_13c = *(undefined *)((long)param_4 + 0xc);
            local_13b._0_8_ = *param_5;
            *(undefined8 *)(local_148 + lVar17 + 5) = *(undefined8 *)((long)param_5 + lVar17 + -8);
            puVar15 = (undefined8 *)((long)param_5 + 3);
            puVar16 = (undefined8 *)(local_13b + 3);
            for (uVar12 = (ulong)((int)lVar17 - 3U >> 3); uVar12 != 0; uVar12 = uVar12 - 1) {
              *puVar16 = *puVar15;
              puVar15 = puVar15 + (ulong)bVar23 * -2 + 1;
              puVar16 = puVar16 + (ulong)bVar23 * -2 + 1;
            }
            (*local_380)(local_388,local_148);
            uVar12 = local_3e0 % local_3e8;
            if (1 < local_3e0 / local_3e8) {
              lVar7 = (long)param_5 + -0xd;
              uVar11 = 1;
              do {
                lVar7 = lVar7 + local_3e8;
                uVar11 = uVar11 + 1;
                (*local_380)(local_388,lVar7,uVar12);
                uVar12 = extraout_RDX_00;
              } while (uVar11 != local_3e0 / local_3e8);
            }
          }
          else {
            if (local_3c8 <= local_3e8) goto LAB_00126699;
            (*local_380)(local_388,param_4);
            __memcpy_chk(local_148,(long)param_4 + local_3e8,local_3c8 - local_3e8,0x80);
            __n = local_3e8 * 2 - local_3c8;
            memcpy(local_148 + (local_3c8 - local_3e8),param_5,__n);
            (*local_380)(local_388,local_148);
            uVar11 = local_3e0 % local_3e8;
            uVar12 = local_3e0 / local_3e8 - 1;
            if (1 < uVar12) {
              uVar22 = 1;
              lVar7 = (long)param_5 + __n;
              do {
                uVar22 = uVar22 + 1;
                (*local_380)(local_388,lVar7,uVar11);
                uVar11 = extraout_RDX;
                lVar7 = lVar7 + local_3e8;
              } while (uVar22 != uVar12);
            }
          }
        }
        _local_208 = (undefined  [16])0x0;
        _local_1f8 = (undefined  [16])0x0;
        _local_1e8 = (undefined  [16])0x0;
        _local_1d8 = (undefined  [16])0x0;
        if (local_3c0 <= local_398) {
          local_3b8 = (local_3e0 - local_3c8) + (long)param_5;
          uVar12 = local_378 & 0xfffffffffffffff0;
          uVar11 = local_378 >> 4;
          do {
            uVar14 = (local_3c8 - (long)param_5) + local_3b8;
            uVar8 = (local_360 ^ local_3c0) - 1 & ~(local_360 ^ local_3c0);
            bVar19 = (byte)((long)uVar8 >> 0x3f);
            bVar13 = (char)(~(byte)((local_368 ^ local_3c0) >> 0x38) &
                           (byte)((local_368 ^ local_3c0) - 1 >> 0x38)) >> 7;
            uVar22 = local_3e8 - lVar21;
            if (local_3e8 <= local_3e8 - lVar21) {
              uVar22 = local_3e8;
            }
            uVar9 = 0;
            uVar18 = ~uVar10;
            do {
              if (uVar14 < local_3c8) {
                bVar20 = *(byte *)((long)param_4 + uVar14);
              }
              else {
                bVar20 = 0;
                if (uVar14 < local_3d0) {
                  bVar20 = *(byte *)(local_3b8 + uVar9);
                }
              }
              uVar14 = uVar14 + 1;
              bVar3 = (byte)(uVar9 >> 0x38);
              bVar2 = ~((char)((bVar3 | (byte)(uVar18 + 1 >> 0x38)) ^ bVar3) >> 7) & bVar13;
              *(byte *)((long)&local_c8 + uVar9) =
                   ~(~((char)((bVar3 | (byte)(uVar18 >> 0x38)) ^ bVar3) >> 7) & bVar13) &
                   (bVar13 | ~bVar19) & (bVar2 & 0x80 | ~bVar2 & bVar20);
              uVar9 = uVar9 + 1;
              uVar18 = uVar18 + 1;
            } while (uVar9 < uVar22);
            if (uVar9 < local_3e8) {
              lVar7 = ~uVar10 + uVar9;
              do {
                if (uVar14 < local_3c8) {
                  bVar20 = *(byte *)((long)param_4 + uVar14);
                }
                else {
                  bVar20 = 0;
                  if (uVar14 < local_3d0) {
                    bVar20 = *(byte *)((long)param_5 + (uVar14 - local_3c8));
                  }
                }
                bVar3 = (byte)(uVar9 >> 0x38);
                uVar14 = uVar14 + 1;
                bVar2 = ~((char)(((byte)((ulong)(lVar7 + 1) >> 0x38) | bVar3) ^ bVar3) >> 7) &
                        bVar13;
                *(byte *)((long)&local_c8 + uVar9) =
                     (~bVar2 & bVar20 | bVar2 & 0x80) &
                     ~(~((char)(((byte)((ulong)lVar7 >> 0x38) | bVar3) ^ bVar3) >> 7) & bVar13) &
                     (bVar13 | ~bVar19) & ~bVar19 |
                     *(byte *)((long)local_218 + uVar9 + (lVar21 - local_3e8)) & bVar19;
                uVar9 = uVar9 + 1;
                lVar7 = lVar7 + 1;
              } while (uVar9 < local_3e8);
            }
            (*local_380)(local_388,&local_c8);
            (*local_370)(local_388,&local_c8);
            uVar24 = (ushort)((long)uVar8 >> 0x3f);
            uVar6 = SUB164(CONCAT142(ZEXT614(0) &
                                     SUB1614((undefined  [16])0xffffffffff00ffff >> 0x10,0),
                                     uVar24 & 0xff | uVar24 << 8),0);
            uVar6 = uVar6 | uVar6 << 0x10;
            _local_208 = CONCAT412(uStack188 & uVar6 | uStack508,
                                   CONCAT48(uStack192 & uVar6 | uStack512,
                                            CONCAT44(uStack196 & uVar6 | stack0xfffffffffffffdfc,
                                                     local_c8 & uVar6 | local_208._0_4_)));
            if (uVar11 != 1) {
              _local_1f8 = CONCAT412(uStack172 & uVar6 | uStack492,
                                     CONCAT48(uStack176 & uVar6 | uStack496,
                                              CONCAT44(uStack180 & uVar6 | local_1f8._4_4_,
                                                       local_b8 & uVar6 | local_1f8._0_4_)));
              if (uVar11 != 2) {
                _local_1e8 = CONCAT412(uStack156 & uVar6 | uStack476,
                                       CONCAT48(uStack160 & uVar6 | uStack480,
                                                CONCAT44(uStack164 & uVar6 | local_1e8._4_4_,
                                                         local_a8 & uVar6 | local_1e8._0_4_)));
                if (uVar11 == 4) {
                  _local_1d8 = CONCAT412(uVar6 & uStack140 | uStack460,
                                         CONCAT48(uVar6 & uStack144 | uStack464,
                                                  CONCAT44(uVar6 & uStack148 | local_1d8._4_4_,
                                                           uVar6 & local_98 | local_1d8._0_4_)));
                }
              }
            }
            if (local_378 != uVar12) {
              local_208[uVar12] = local_208[uVar12] | *(byte *)((long)&local_c8 + uVar12) & bVar19;
              if (uVar12 + 1 < local_378) {
                local_208[uVar12 + 1] =
                     local_208[uVar12 + 1] | *(byte *)((long)&local_c8 + uVar12 + 1) & bVar19;
                if (uVar12 + 2 < local_378) {
                  local_208[uVar12 + 2] =
                       local_208[uVar12 + 2] | *(byte *)((long)&local_c8 + uVar12 + 2) & bVar19;
                  if (uVar12 + 3 < local_378) {
                    local_208[uVar12 + 3] =
                         local_208[uVar12 + 3] | *(byte *)((long)&local_c8 + uVar12 + 3) & bVar19;
                    if (uVar12 + 4 < local_378) {
                      local_208[uVar12 + 4] =
                           local_208[uVar12 + 4] | *(byte *)((long)&uStack196 + uVar12) & bVar19;
                      if (uVar12 + 5 < local_378) {
                        abStack515[uVar12] =
                             abStack515[uVar12] | *(byte *)((long)&uStack196 + uVar12 + 1) & bVar19;
                        if (uVar12 + 6 < local_378) {
                          abStack515[uVar12 + 1] =
                               abStack515[uVar12 + 1] |
                               *(byte *)((long)&uStack196 + uVar12 + 2) & bVar19;
                          if (uVar12 + 7 < local_378) {
                            abStack515[uVar12 + 2] =
                                 abStack515[uVar12 + 2] |
                                 *(byte *)((long)&uStack196 + uVar12 + 3) & bVar19;
                            if (uVar12 + 8 < local_378) {
                              *(byte *)((long)&uStack512 + uVar12) =
                                   *(byte *)((long)&uStack512 + uVar12) |
                                   *(byte *)((long)&uStack192 + uVar12) & bVar19;
                              if (uVar12 + 9 < local_378) {
                                pbVar1 = (byte *)((long)&uStack512 + uVar12 + 1);
                                *pbVar1 = *pbVar1 | *(byte *)((long)&uStack192 + uVar12 + 1) &
                                                    bVar19;
                                if (uVar12 + 10 < local_378) {
                                  pbVar1 = (byte *)((long)&uStack512 + uVar12 + 2);
                                  *pbVar1 = *pbVar1 | *(byte *)((long)&uStack192 + uVar12 + 2) &
                                                      bVar19;
                                  if (uVar12 + 0xb < local_378) {
                                    pbVar1 = (byte *)((long)&uStack512 + uVar12 + 3);
                                    *pbVar1 = *pbVar1 | *(byte *)((long)&uStack192 + uVar12 + 3) &
                                                        bVar19;
                                    if (uVar12 + 0xc < local_378) {
                                      local_1f8[uVar12 - 4] =
                                           local_1f8[uVar12 - 4] |
                                           *(byte *)((long)&uStack188 + uVar12) & bVar19;
                                      if (uVar12 + 0xd < local_378) {
                                        local_1f8[uVar12 - 3] =
                                             local_1f8[uVar12 - 3] |
                                             *(byte *)((long)&uStack188 + uVar12 + 1) & bVar19;
                                        if (uVar12 + 0xe < local_378) {
                                          local_1f8[uVar12 - 2] =
                                               local_1f8[uVar12 - 2] |
                                               *(byte *)((long)&uStack188 + uVar12 + 2) & bVar19;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            local_3c0 = local_3c0 + 1;
            local_3b8 = local_3b8 + local_3e8;
          } while (local_3c0 <= local_398);
        }
        ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
        if ((ctx == (EVP_MD_CTX *)0x0) ||
           (iVar5 = EVP_DigestInit_ex(ctx,param_1,(ENGINE *)0x0), iVar5 < 1)) {
LAB_001271d8:
          uVar6 = 0;
        }
        else {
          if (param_10 != '\0') {
            puVar15 = &local_1c8;
            for (uVar12 = local_310 >> 3; uVar12 != 0; uVar12 = uVar12 - 1) {
              *puVar15 = 0x5c5c5c5c5c5c5c5c;
              puVar15 = puVar15 + (ulong)bVar23 * -2 + 1;
            }
            iVar5 = EVP_DigestUpdate(ctx,param_8,param_9);
            if ((0 < iVar5) && (iVar5 = EVP_DigestUpdate(ctx,&local_1c8,local_310), 0 < iVar5))
            goto LAB_0012711b;
            goto LAB_001271d8;
          }
          uVar12 = local_3e8 >> 4;
          local_1c8 = CONCAT44(local_1c8._4_4_ ^ _UNK_00180544,(uint)local_1c8 ^ _DAT_00180540);
          uStack448 = CONCAT44(uStack448._4_4_ ^ _UNK_0018054c,(uint)uStack448 ^ _UNK_00180548);
          local_1b8 = CONCAT44(local_1b8._4_4_ ^ _UNK_00180544,(uint)local_1b8 ^ _DAT_00180540);
          uStack432 = CONCAT44(uStack432._4_4_ ^ _UNK_0018054c,(uint)uStack432 ^ _UNK_00180548);
          local_1a8 = CONCAT44(local_1a8._4_4_ ^ _UNK_00180544,(uint)local_1a8 ^ _DAT_00180540);
          uStack416 = CONCAT44(uStack416._4_4_ ^ _UNK_0018054c,(uint)uStack416 ^ _UNK_00180548);
          local_198 = CONCAT44(local_198._4_4_ ^ _UNK_00180544,(uint)local_198 ^ _DAT_00180540);
          uStack400 = CONCAT44(uStack400._4_4_ ^ _UNK_0018054c,(uint)uStack400 ^ _UNK_00180548);
          if (local_3e8 != 0x40) {
            local_188 = CONCAT44(local_188._4_4_ ^ _UNK_00180544,(uint)local_188 ^ _DAT_00180540);
            uStack384 = CONCAT44(uStack384._4_4_ ^ _UNK_0018054c,(uint)uStack384 ^ _UNK_00180548);
            if (uVar12 != 5) {
              local_178 = CONCAT44(local_178._4_4_ ^ _UNK_00180544,(uint)local_178 ^ _DAT_00180540);
              uStack368 = CONCAT44(uStack368._4_4_ ^ _UNK_0018054c,(uint)uStack368 ^ _UNK_00180548);
              if (uVar12 != 6) {
                local_168 = CONCAT44(local_168._4_4_ ^ _UNK_00180544,(uint)local_168 ^ _DAT_00180540
                                    );
                uStack352 = CONCAT44(uStack352._4_4_ ^ _UNK_0018054c,(uint)uStack352 ^ _UNK_00180548
                                    );
                if (uVar12 == 8) {
                  local_158 = CONCAT44(_UNK_00180544 ^ local_158._4_4_,
                                       _DAT_00180540 ^ (uint)local_158);
                  uStack336 = CONCAT44(_UNK_0018054c ^ uStack336._4_4_,
                                       _UNK_00180548 ^ (uint)uStack336);
                }
              }
            }
          }
          iVar5 = EVP_DigestUpdate(ctx,&local_1c8,local_3e8);
          if (iVar5 < 1) goto LAB_001271d8;
LAB_0012711b:
          iVar5 = EVP_DigestUpdate(ctx,local_208,local_378);
          if (iVar5 < 1) goto LAB_001271d8;
          iVar5 = EVP_DigestFinal(ctx,param_2,&local_2fc);
          if ((iVar5 != 0) && (param_3 != (ulong *)0x0)) {
            *param_3 = (ulong)local_2fc;
          }
          uVar6 = 1;
        }
        uVar12 = (ulong)uVar6;
        EVP_MD_CTX_free(ctx);
        goto LAB_0012669b;
      }
    }
  }
LAB_00126699:
  uVar12 = 0;
LAB_0012669b:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar12;
}