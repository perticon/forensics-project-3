undefined8 ssl_fill_hello_random(long param_1,int param_2,uint *param_3,ulong param_4,int param_5)

{
  uint uVar1;
  time_t tVar2;
  undefined8 uVar3;
  
  if (param_4 < 4) {
    return 0;
  }
  if (param_2 == 0) {
    uVar1 = *(uint *)(param_1 + 0x9f0) >> 5;
  }
  else {
    uVar1 = *(uint *)(param_1 + 0x9f0) >> 6;
  }
  if ((uVar1 & 1) == 0) {
    uVar3 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),param_3,param_4,0);
  }
  else {
    tVar2 = time((time_t *)0x0);
    uVar1 = (uint)tVar2;
    *param_3 = uVar1 >> 0x18 | (uVar1 & 0xff0000) >> 8 | (uVar1 & 0xff00) << 8 | uVar1 << 0x18;
    uVar3 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),param_3 + 1,param_4 - 4,0);
  }
  if (0 < (int)uVar3) {
    if (param_4 < 9) {
      return 0;
    }
    if (param_5 == 1) {
      *(undefined8 *)((long)param_3 + (param_4 - 8)) = 0x14452474e574f44;
    }
    else if (param_5 == 2) {
      *(undefined8 *)((long)param_3 + (param_4 - 8)) = 0x4452474e574f44;
    }
  }
  return uVar3;
}