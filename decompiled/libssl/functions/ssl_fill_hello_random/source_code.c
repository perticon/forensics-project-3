int ssl_fill_hello_random(SSL *s, int server, unsigned char *result, size_t len,
                          DOWNGRADE dgrd)
{
    int send_time = 0, ret;

    if (len < 4)
        return 0;
    if (server)
        send_time = (s->mode & SSL_MODE_SEND_SERVERHELLO_TIME) != 0;
    else
        send_time = (s->mode & SSL_MODE_SEND_CLIENTHELLO_TIME) != 0;
    if (send_time) {
        unsigned long Time = (unsigned long)time(NULL);
        unsigned char *p = result;

        l2n(Time, p);
        ret = RAND_bytes_ex(s->ctx->libctx, p, len - 4, 0);
    } else {
        ret = RAND_bytes_ex(s->ctx->libctx, result, len, 0);
    }

    if (ret > 0) {
        if (!ossl_assert(sizeof(tls11downgrade) < len)
                || !ossl_assert(sizeof(tls12downgrade) < len))
             return 0;
        if (dgrd == DOWNGRADE_TO_1_2)
            memcpy(result + len - sizeof(tls12downgrade), tls12downgrade,
                   sizeof(tls12downgrade));
        else if (dgrd == DOWNGRADE_TO_1_1)
            memcpy(result + len - sizeof(tls11downgrade), tls11downgrade,
                   sizeof(tls11downgrade));
    }

    return ret;
}