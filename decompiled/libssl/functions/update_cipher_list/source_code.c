static int update_cipher_list(SSL_CTX *ctx,
                              STACK_OF(SSL_CIPHER) **cipher_list,
                              STACK_OF(SSL_CIPHER) **cipher_list_by_id,
                              STACK_OF(SSL_CIPHER) *tls13_ciphersuites)
{
    int i;
    STACK_OF(SSL_CIPHER) *tmp_cipher_list = sk_SSL_CIPHER_dup(*cipher_list);

    if (tmp_cipher_list == NULL)
        return 0;

    /*
     * Delete any existing TLSv1.3 ciphersuites. These are always first in the
     * list.
     */
    while (sk_SSL_CIPHER_num(tmp_cipher_list) > 0
           && sk_SSL_CIPHER_value(tmp_cipher_list, 0)->min_tls
              == TLS1_3_VERSION)
        (void)sk_SSL_CIPHER_delete(tmp_cipher_list, 0);

    /* Insert the new TLSv1.3 ciphersuites */
    for (i = sk_SSL_CIPHER_num(tls13_ciphersuites) - 1; i >= 0; i--) {
        const SSL_CIPHER *sslc = sk_SSL_CIPHER_value(tls13_ciphersuites, i);

        /* Don't include any TLSv1.3 ciphersuites that are disabled */
        if ((sslc->algorithm_enc & ctx->disabled_enc_mask) == 0
                && (ssl_cipher_table_mac[sslc->algorithm2
                                         & SSL_HANDSHAKE_MAC_MASK].mask
                    & ctx->disabled_mac_mask) == 0) {
            sk_SSL_CIPHER_unshift(tmp_cipher_list, sslc);
        }
    }

    if (!update_cipher_list_by_id(cipher_list_by_id, tmp_cipher_list)) {
        sk_SSL_CIPHER_free(tmp_cipher_list);
        return 0;
    }

    sk_SSL_CIPHER_free(*cipher_list);
    *cipher_list = tmp_cipher_list;

    return 1;
}