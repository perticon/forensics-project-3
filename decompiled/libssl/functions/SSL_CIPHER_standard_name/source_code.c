const char *SSL_CIPHER_standard_name(const SSL_CIPHER *c)
{
    if (c != NULL)
        return c->stdname;
    return "(NONE)";
}