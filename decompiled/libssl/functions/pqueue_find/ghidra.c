pitem * pqueue_find(pqueue pq,uchar *prio64be)

{
  _pitem *p_Var1;
  int iVar2;
  pitem *__s1;
  
  p_Var1 = *(pitem **)pq;
  if (*(pitem **)pq == (pitem *)0x0) {
    __s1 = (pitem *)0x0;
  }
  else {
    do {
      __s1 = p_Var1;
      p_Var1 = __s1->next;
      iVar2 = memcmp(__s1,prio64be,8);
      if (p_Var1 == (_pitem *)0x0) {
        if (iVar2 == 0) {
          return __s1;
        }
        return (pitem *)0x0;
      }
    } while (iVar2 != 0);
  }
  return __s1;
}