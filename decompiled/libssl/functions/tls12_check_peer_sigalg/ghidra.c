int tls12_check_peer_sigalg(long param_1,ushort param_2,undefined8 param_3)

{
  bool bVar1;
  ushort uVar2;
  int iVar3;
  uint uVar4;
  undefined4 uVar5;
  int iVar6;
  long lVar7;
  ushort *puVar8;
  char *pcVar9;
  int *piVar10;
  char cVar11;
  long lVar12;
  char *pcVar13;
  long lVar14;
  undefined8 uVar15;
  long in_FS_OFFSET;
  ushort *local_68;
  long local_60;
  int local_58 [2];
  long local_50;
  ushort local_42;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = 0;
  iVar3 = EVP_PKEY_get_id(param_3);
  if (iVar3 == -1) goto LAB_0014b886;
  piVar10 = *(int **)(param_1 + 8);
  uVar4 = *(uint *)(*(long *)(piVar10 + 0x30) + 0x60) & 8;
  if (((uVar4 == 0) && (*piVar10 != 0x10000)) && (0x303 < *piVar10)) {
    if (iVar3 != 0x74) {
      if (iVar3 == 6) {
        iVar3 = 0x390;
      }
      goto LAB_0014b7d3;
    }
    ERR_new();
    uVar15 = 0x5ff;
  }
  else {
LAB_0014b7d3:
    lVar14 = *(long *)(*(long *)(param_1 + 0x9a8) + 0x620);
    lVar7 = lVar14 + 0x4d8;
    do {
      if (param_2 == *(ushort *)(lVar14 + 8)) {
        if (((*(int *)(lVar14 + 0x24) != 0) &&
            (((uVar4 != 0 || (*piVar10 == 0x10000)) ||
             ((*piVar10 < 0x304 ||
              ((*(int *)(lVar14 + 0xc) != 0x40 && (*(int *)(lVar14 + 0xc) != 0x2a3)))))))) &&
           ((*(int *)(lVar14 + 0x14) == iVar3 ||
            ((iVar3 == 6 && (*(int *)(lVar14 + 0x14) == 0x390)))))) {
          uVar5 = EVP_PKEY_get_id(param_3);
          iVar6 = ssl_cert_lookup_by_nid(uVar5,local_58);
          if ((iVar6 == 0) || (*(int *)(lVar14 + 0x18) != local_58[0])) {
            ERR_new();
            uVar15 = 0x615;
            goto LAB_0014b861;
          }
          if (iVar3 != 0x198) {
            if ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) == 0) goto LAB_0014b932;
            ERR_new();
            uVar15 = 0x63c;
            goto LAB_0014ba87;
          }
          iVar3 = EVP_PKEY_is_a(param_3,&DAT_00188294);
          if (iVar3 == 0) goto LAB_0014bb8c;
          iVar3 = EVP_PKEY_get_ec_point_conv_form(param_3);
          if (iVar3 == 0) goto LAB_0014bd07;
          if (iVar3 == 4) {
            cVar11 = '\0';
          }
          else {
            if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
                (iVar3 = **(int **)(param_1 + 8), iVar3 != 0x10000)) && (0x303 < iVar3))
            goto LAB_0014bbc3;
            iVar3 = EVP_PKEY_get_field_type(param_3);
            if (iVar3 == 0x196) {
              cVar11 = '\x01';
            }
            else {
              cVar11 = '\x02';
              if (iVar3 != 0x197) goto LAB_0014bd07;
            }
          }
          pcVar9 = *(char **)(param_1 + 0xab8);
          if (pcVar9 == (char *)0x0) goto LAB_0014bb8c;
          if (*(long *)(param_1 + 0xab0) == 0) goto LAB_0014bd07;
          pcVar13 = pcVar9 + *(long *)(param_1 + 0xab0);
          goto LAB_0014bb88;
        }
        break;
      }
      lVar14 = lVar14 + 0x28;
    } while (lVar14 != lVar7);
    ERR_new();
    uVar15 = 0x60f;
  }
LAB_0014b861:
  ERR_set_debug("ssl/t1_lib.c",uVar15,"tls12_check_peer_sigalg");
  uVar15 = 0x172;
  goto LAB_0014b874;
LAB_0014bb8c:
  piVar10 = *(int **)(param_1 + 8);
  if ((*(byte *)(*(long *)(piVar10 + 0x30) + 0x60) & 8) == 0) {
    if (((0x303 < *piVar10) && (*piVar10 != 0x10000)) ||
       ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) != 0)) {
LAB_0014bbc3:
      iVar3 = ssl_get_EC_curve_nid(param_3);
      if ((*(int *)(lVar14 + 0x20) != 0) && (*(int *)(lVar14 + 0x20) != iVar3)) {
        ERR_new();
        ERR_set_debug("ssl/t1_lib.c",0x627,"tls12_check_peer_sigalg");
        uVar15 = 0x17a;
        goto LAB_0014b874;
      }
      piVar10 = *(int **)(param_1 + 8);
      if ((*(byte *)(*(long *)(piVar10 + 0x30) + 0x60) & 8) != 0) goto LAB_0014bbff;
    }
    if ((*piVar10 < 0x304) || (*piVar10 == 0x10000)) goto LAB_0014bbff;
LAB_0014b932:
    lVar7 = tls12_get_psigalgs(param_1,1,&local_68);
    bVar1 = false;
    lVar12 = 0;
    puVar8 = local_68;
    if (lVar7 != 0) {
      do {
        if (*puVar8 == param_2) {
          if (bVar1) {
            local_68 = puVar8;
          }
          goto LAB_0014b984;
        }
        lVar12 = lVar12 + 1;
        puVar8 = puVar8 + 1;
        bVar1 = true;
      } while (lVar7 != lVar12);
    }
    local_68 = puVar8;
    if ((*(int *)(lVar14 + 0xc) == 0x40) &&
       ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30001) == 0)) {
LAB_0014b984:
      iVar3 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x9a8),lVar14,&local_60);
      if (iVar3 == 0) {
        ERR_new();
        ERR_set_debug("ssl/t1_lib.c",0x64d,"tls12_check_peer_sigalg");
        ossl_statem_fatal(param_1,0x28,0x170,0);
        goto LAB_0014b886;
      }
      local_42 = param_2 << 8 | param_2 >> 8;
      local_50 = 0;
      iVar3 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x9a8),lVar14,&local_50);
      if (iVar3 != 0) {
        if (local_50 == 0) {
          if (*(short *)(lVar14 + 8) == 0x807) {
            iVar3 = 0x80;
          }
          else {
            iVar3 = 0xe0;
            if (*(short *)(lVar14 + 8) != 0x808) goto LAB_0014bcd8;
          }
        }
        else {
          iVar3 = EVP_MD_get_type();
          iVar6 = EVP_MD_get_size(local_50);
          if (iVar3 != 0x40) {
            if (iVar3 == 0x72) {
              iVar3 = 0x43;
            }
            else if (iVar3 == 4) {
              iVar3 = 0x27;
            }
            else {
              iVar3 = iVar6 << 2;
              if (iVar3 == 0) goto LAB_0014bcd8;
            }
          }
        }
        uVar5 = 0;
        if (local_60 != 0) {
          uVar5 = EVP_MD_get_type();
        }
        iVar3 = ssl_security(param_1,0x5000d,iVar3,uVar5,&local_42);
        if (iVar3 != 0) {
          *(long *)(param_1 + 0x3b0) = lVar14;
          iVar3 = 1;
          goto LAB_0014b886;
        }
      }
LAB_0014bcd8:
      ERR_new();
      uVar15 = 0x65b;
    }
    else {
      ERR_new();
      uVar15 = 0x649;
    }
  }
  else {
    if ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) != 0) goto LAB_0014bbc3;
LAB_0014bbff:
    uVar4 = ssl_get_EC_curve_nid(param_3);
    if (uVar4 != 0) {
      uVar2 = tls1_nid2group_id(uVar4,uVar4);
      uVar4 = (uint)uVar2;
    }
    iVar3 = tls1_check_group_id(param_1,uVar4,1);
    if (iVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/t1_lib.c",0x62e,"tls12_check_peer_sigalg");
      ossl_statem_fatal(param_1,0x2f,0x17a,0);
      goto LAB_0014b886;
    }
    if (((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) == 0) ||
       ((param_2 & 0xfeff) == 0x403)) goto LAB_0014b932;
    ERR_new();
    uVar15 = 0x635;
  }
LAB_0014ba87:
  iVar3 = 0;
  ERR_set_debug("ssl/t1_lib.c",uVar15,"tls12_check_peer_sigalg");
  ossl_statem_fatal(param_1,0x28,0x172,0);
  goto LAB_0014b886;
  while (pcVar9 = pcVar9 + 1, pcVar9 != pcVar13) {
LAB_0014bb88:
    if (*pcVar9 == cVar11) goto LAB_0014bb8c;
  }
LAB_0014bd07:
  ERR_new();
  ERR_set_debug("ssl/t1_lib.c",0x61d,"tls12_check_peer_sigalg");
  uVar15 = 0xa2;
LAB_0014b874:
  iVar3 = 0;
  ossl_statem_fatal(param_1,0x2f,uVar15,0);
LAB_0014b886:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar3;
}