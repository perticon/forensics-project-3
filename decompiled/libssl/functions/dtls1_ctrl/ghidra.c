long dtls1_ctrl(long param_1,int param_2,long param_3,undefined8 param_4)

{
  int iVar1;
  long lVar2;
  
  if (param_2 == 0x4a) {
    iVar1 = dtls1_handle_timeout();
  }
  else {
    if (param_2 < 0x4b) {
      if (param_2 == 0x11) {
        if (param_3 < 0xd0) {
          return 0;
        }
        *(long *)(*(long *)(param_1 + 0x4b8) + 0x130) = param_3;
        return param_3;
      }
      if (param_2 == 0x49) {
        lVar2 = dtls1_get_timeout(param_1,param_4);
        return (long)(int)(uint)(lVar2 != 0);
      }
    }
    else {
      if (param_2 == 0x78) {
        if (param_3 < 0x100) {
          return 0;
        }
        *(long *)(*(long *)(param_1 + 0x4b8) + 0x128) = param_3;
        return 1;
      }
      if (param_2 == 0x79) {
        return 0x100;
      }
    }
    iVar1 = ssl3_ctrl();
  }
  return (long)iVar1;
}