uint64_t SSL_clear_options(SSL *s, uint64_t op)
{
    return s->options &= ~op;
}