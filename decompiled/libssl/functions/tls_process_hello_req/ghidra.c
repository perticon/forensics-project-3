undefined8 tls_process_hello_req(SSL *param_1,long param_2)

{
  if (*(long *)(param_2 + 8) != 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0xe13,"tls_process_hello_req");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
    return 0;
  }
  if ((*(byte *)((long)&param_1[3].tlsext_debug_cb + 3) & 0x40) == 0) {
    if (((byte)param_1->method->get_timeout[0x60] & 8) != 0) {
      SSL_renegotiate(param_1);
      return 1;
    }
    SSL_renegotiate_abbreviated(param_1);
    return 1;
  }
  ssl3_send_alert(param_1,1,100);
  return 1;
}