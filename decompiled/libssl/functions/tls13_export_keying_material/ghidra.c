bool tls13_export_keying_material
               (long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
               undefined8 param_5,void *param_6,size_t param_7,int param_8)

{
  int iVar1;
  EVP_MD *type;
  EVP_MD_CTX *ctx;
  size_t cnt;
  long in_FS_OFFSET;
  bool bVar2;
  uint local_110;
  uint local_10c;
  undefined local_108 [64];
  uchar local_c8 [64];
  uchar local_88 [72];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  type = (EVP_MD *)ssl_handshake_md();
  ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
  if (ctx != (EVP_MD_CTX *)0x0) {
    iVar1 = ossl_statem_export_allowed(param_1);
    if (iVar1 != 0) {
      cnt = 0;
      if (param_8 != 0) {
        cnt = param_7;
      }
      iVar1 = EVP_DigestInit_ex(ctx,type,(ENGINE *)0x0);
      if (0 < iVar1) {
        iVar1 = EVP_DigestUpdate(ctx,param_6,cnt);
        if (0 < iVar1) {
          iVar1 = EVP_DigestFinal_ex(ctx,local_c8,&local_110);
          if (0 < iVar1) {
            iVar1 = EVP_DigestInit_ex(ctx,type,(ENGINE *)0x0);
            if (0 < iVar1) {
              iVar1 = EVP_DigestFinal_ex(ctx,local_88,&local_10c);
              if (0 < iVar1) {
                iVar1 = tls13_hkdf_expand(param_1,type,param_1 + 0x7c4,param_4,param_5,local_88,
                                          local_10c,local_108,local_110,0);
                if (iVar1 != 0) {
                  iVar1 = tls13_hkdf_expand(param_1,type,local_108,"exporter",8,local_c8,local_110,
                                            param_2,param_3,0);
                  bVar2 = iVar1 != 0;
                  goto LAB_00152b43;
                }
              }
            }
          }
        }
      }
    }
  }
  bVar2 = false;
LAB_00152b43:
  EVP_MD_CTX_free(ctx);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}