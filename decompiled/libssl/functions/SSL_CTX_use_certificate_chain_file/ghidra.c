int SSL_CTX_use_certificate_chain_file(SSL_CTX *ctx,char *file)

{
  _func_3091 *cb;
  _func_3092 *u;
  int iVar1;
  BIO_METHOD *type;
  BIO *bp;
  long lVar2;
  X509 *pXVar3;
  ulong uVar4;
  int iVar5;
  long in_FS_OFFSET;
  X509 *local_50;
  X509 *local_48;
  long local_40;
  
  iVar5 = 0;
  iVar1 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = (X509 *)0x0;
  if (ctx != (SSL_CTX *)0x0) {
    ERR_clear_error();
    cb = ctx->client_cert_cb;
    u = ctx->app_gen_cookie_cb;
    type = BIO_s_file();
    bp = BIO_new(type);
    iVar5 = iVar1;
    if (bp == (BIO *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0x1bf,"use_certificate_chain_file");
      ERR_set_error(0x14,0x80007,0);
    }
    else {
      lVar2 = BIO_ctrl(bp,0x6c,3,file);
      if ((int)lVar2 < 1) {
        ERR_new();
        ERR_set_debug("ssl/ssl_rsa.c",0x1c4,"use_certificate_chain_file");
        ERR_set_error(0x14,0x80002,0);
      }
      else {
        local_50 = (X509 *)X509_new_ex(ctx->method,ctx[1].tlsext_ticket_key_cb);
        if (local_50 == (X509 *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/ssl_rsa.c",0x1ca,"use_certificate_chain_file");
          ERR_set_error(0x14,0xc0100,0);
        }
        else {
          pXVar3 = PEM_read_bio_X509_AUX(bp,&local_50,cb,u);
          if (pXVar3 == (X509 *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/ssl_rsa.c",0x1cf,"use_certificate_chain_file");
            ERR_set_error(0x14,0x80009,0);
          }
          else {
            iVar1 = SSL_CTX_use_certificate(ctx,local_50);
            uVar4 = ERR_peek_error();
            if ((iVar1 == 0) || (uVar4 != 0)) {
              iVar5 = 0;
            }
            else {
              lVar2 = SSL_CTX_ctrl(ctx,0x58,0,(void *)0x0);
              iVar5 = (int)lVar2;
              if ((int)lVar2 != 0) {
                do {
                  local_48 = (X509 *)X509_new_ex(ctx->method,ctx[1].tlsext_ticket_key_cb);
                  if (local_48 == (X509 *)0x0) {
                    ERR_new();
                    ERR_set_debug("ssl/ssl_rsa.c",0x1f1,"use_certificate_chain_file");
                    ERR_set_error(0x14,0xc0100,0);
                    iVar5 = iVar1;
                    goto LAB_0014138e;
                  }
                  pXVar3 = PEM_read_bio_X509(bp,&local_48,cb,u);
                  if (pXVar3 == (X509 *)0x0) {
                    iVar5 = 0;
                    X509_free(local_48);
                    uVar4 = ERR_peek_last_error();
                    if ((((uVar4 & 0x80000000) == 0) && ((char)(uVar4 >> 0x17) == '\t')) &&
                       (((uint)uVar4 & 0x7fffff) == 0x6c)) {
                      ERR_clear_error();
                      iVar5 = iVar1;
                    }
                    goto LAB_0014138e;
                  }
                  lVar2 = SSL_CTX_ctrl(ctx,0x59,0,local_48);
                } while ((int)lVar2 != 0);
                X509_free(local_48);
                iVar5 = (int)lVar2;
              }
            }
          }
        }
      }
    }
LAB_0014138e:
    X509_free(local_50);
    BIO_free(bp);
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}