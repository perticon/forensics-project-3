undefined8 SSL_dane_enable(SSL *param_1,void *param_2)

{
  int iVar1;
  uchar *puVar2;
  undefined8 uVar3;
  long lVar4;
  
  if (*(char *)(*(long *)&param_1[3].ex_data.dummy + 0x3a8) == '\0') {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x40f,"SSL_dane_enable");
    ERR_set_error(0x14,0xa7,0);
    return 0;
  }
  if (param_1[1].tlsext_ellipticcurvelist != (uchar *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x413,"SSL_dane_enable");
    ERR_set_error(0x14,0xac,0);
    return 0;
  }
  if (param_1[3].tlsext_opaque_prf_input_len == 0) {
    lVar4 = SSL_ctrl(param_1,0x37,0,param_2);
    if (lVar4 == 0) {
      ERR_new();
      uVar3 = 0x41e;
      goto LAB_00138a71;
    }
  }
  iVar1 = X509_VERIFY_PARAM_set1_host(param_1[1].tlsext_ecpointformatlist,param_2,0);
  if (iVar1 != 0) {
    lVar4 = *(long *)&param_1[3].ex_data.dummy;
    *(undefined8 *)((long)&param_1[1].tls_session_ticket_ext_cb + 4) = 0xffffffffffffffff;
    param_1[1].tlsext_ellipticcurvelist_length = lVar4 + 0x398;
    puVar2 = (uchar *)OPENSSL_sk_new_null();
    param_1[1].tlsext_ellipticcurvelist = puVar2;
    uVar3 = 1;
    if (puVar2 == (uchar *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x42f,"SSL_dane_enable");
      ERR_set_error(0x14,0xc0100,0);
      uVar3 = 0xffffffff;
    }
    return uVar3;
  }
  ERR_new();
  uVar3 = 0x425;
LAB_00138a71:
  ERR_set_debug("ssl/ssl_lib.c",uVar3,"SSL_dane_enable");
  ERR_set_error(0x14,0xcc,0);
  return 0xffffffff;
}