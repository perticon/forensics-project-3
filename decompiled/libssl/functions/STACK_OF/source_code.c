const STACK_OF(X509_NAME) *get_ca_names(SSL *s)
{
    const STACK_OF(X509_NAME) *ca_sk = NULL;

    if (s->server) {
        ca_sk = SSL_get_client_CA_list(s);
        if (ca_sk != NULL && sk_X509_NAME_num(ca_sk) == 0)
            ca_sk = NULL;
    }

    if (ca_sk == NULL)
        ca_sk = SSL_get0_CA_list(s);

    return ca_sk;
}