uint ssl_generate_master_secret(long param_1,void *param_2,size_t param_3,int param_4)

{
  ushort uVar1;
  size_t __n;
  long lVar2;
  uint uVar3;
  int iVar4;
  ushort *puVar5;
  void *pvVar6;
  undefined *puVar7;
  long lVar8;
  
  uVar3 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x1c);
  if ((uVar3 & 0x1c8) == 0) {
    iVar4 = (**(code **)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x18))
                      (param_1,*(long *)(param_1 + 0x918) + 0x50,param_2,param_3,
                       *(long *)(param_1 + 0x918) + 8);
    uVar3 = (uint)(iVar4 != 0);
  }
  else {
    uVar1 = *(ushort *)(param_1 + 0x378);
    __n = *(size_t *)(param_1 + 0x378);
    if ((uVar3 & 8) == 0) {
      lVar8 = __n + 4 + param_3;
      puVar5 = (ushort *)CRYPTO_malloc((int)lVar8,"ssl/s3_lib.c",0x1211);
      if (puVar5 == (ushort *)0x0) {
        uVar3 = 0;
        goto LAB_0012b125;
      }
      *puVar5 = (ushort)param_3 << 8 | (ushort)param_3 >> 8;
      pvVar6 = memcpy(puVar5 + 1,param_2,param_3);
    }
    else {
      lVar8 = __n * 2 + 4;
      puVar5 = (ushort *)CRYPTO_malloc((int)lVar8,"ssl/s3_lib.c",0x1211);
      param_3 = __n;
      if (puVar5 == (ushort *)0x0) {
        uVar3 = 0;
        goto LAB_0012b125;
      }
      *puVar5 = uVar1 << 8 | uVar1 >> 8;
      pvVar6 = memset(puVar5 + 1,0,__n);
    }
    puVar7 = (undefined *)((long)pvVar6 + param_3);
    *puVar7 = (char)(__n >> 8);
    puVar7[1] = (char)__n;
    memcpy(puVar7 + 2,*(void **)(param_1 + 0x370),__n);
    CRYPTO_clear_free(*(undefined8 *)(param_1 + 0x370),__n,"ssl/s3_lib.c",0x121e);
    *(undefined8 *)(param_1 + 0x370) = 0;
    lVar2 = *(long *)(*(long *)(param_1 + 8) + 0xc0);
    *(undefined8 *)(param_1 + 0x378) = 0;
    uVar3 = (**(code **)(lVar2 + 0x18))
                      (param_1,*(long *)(param_1 + 0x918) + 0x50,puVar5,lVar8,
                       *(long *)(param_1 + 0x918) + 8);
    if (uVar3 == 0) {
      CRYPTO_clear_free(puVar5,lVar8,"ssl/s3_lib.c");
    }
    else {
      CRYPTO_clear_free(puVar5,lVar8,"ssl/s3_lib.c");
      uVar3 = 1;
    }
  }
LAB_0012b125:
  if (param_2 != (void *)0x0) {
    if (param_4 == 0) {
      OPENSSL_cleanse(param_2,param_3);
    }
    else {
      CRYPTO_clear_free(param_2,param_3,"ssl/s3_lib.c",0x123a);
    }
  }
  if (*(int *)(param_1 + 0x38) == 0) {
    *(undefined8 *)(param_1 + 0x360) = 0;
    *(undefined8 *)(param_1 + 0x368) = 0;
  }
  return uVar3;
}