tls1_export_keying_material
          (long param_1,undefined8 param_2,undefined8 param_3,void *param_4,size_t param_5,
          void *param_6,size_t param_7,int param_8)

{
  undefined4 *puVar1;
  ulong uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  ulong *__dest;
  long lVar6;
  
  lVar6 = param_5 + 0x40;
  if (param_8 == 0) {
    __dest = (ulong *)CRYPTO_malloc((int)lVar6,"ssl/t1_enc.c",0x293);
    if (__dest != (ulong *)0x0) {
      memcpy(__dest,param_4,param_5);
      puVar1 = (undefined4 *)((long)__dest + param_5);
      uVar5 = *(undefined4 *)(param_1 + 0x164);
      uVar3 = *(undefined4 *)(param_1 + 0x168);
      uVar4 = *(undefined4 *)(param_1 + 0x16c);
      *puVar1 = *(undefined4 *)(param_1 + 0x160);
      puVar1[1] = uVar5;
      puVar1[2] = uVar3;
      puVar1[3] = uVar4;
      uVar5 = *(undefined4 *)(param_1 + 0x174);
      uVar3 = *(undefined4 *)(param_1 + 0x178);
      uVar4 = *(undefined4 *)(param_1 + 0x17c);
      puVar1[4] = *(undefined4 *)(param_1 + 0x170);
      puVar1[5] = uVar5;
      puVar1[6] = uVar3;
      puVar1[7] = uVar4;
      puVar1 = (undefined4 *)((long)__dest + param_5 + 0x20);
      uVar5 = *(undefined4 *)(param_1 + 0x144);
      uVar3 = *(undefined4 *)(param_1 + 0x148);
      uVar4 = *(undefined4 *)(param_1 + 0x14c);
      *puVar1 = *(undefined4 *)(param_1 + 0x140);
      puVar1[1] = uVar5;
      puVar1[2] = uVar3;
      puVar1[3] = uVar4;
      uVar5 = *(undefined4 *)(param_1 + 0x154);
      uVar3 = *(undefined4 *)(param_1 + 0x158);
      uVar4 = *(undefined4 *)(param_1 + 0x15c);
      puVar1[4] = *(undefined4 *)(param_1 + 0x150);
      puVar1[5] = uVar5;
      puVar1[6] = uVar3;
      puVar1[7] = uVar4;
LAB_0014706a:
      uVar2 = *__dest;
      goto joined_r0x00147078;
    }
  }
  else {
    lVar6 = param_5 + 0x42 + param_7;
    __dest = (ulong *)CRYPTO_malloc((int)lVar6,"ssl/t1_enc.c",0x293);
    if (__dest != (ulong *)0x0) {
      memcpy(__dest,param_4,param_5);
      puVar1 = (undefined4 *)((long)__dest + param_5);
      uVar5 = *(undefined4 *)(param_1 + 0x164);
      uVar3 = *(undefined4 *)(param_1 + 0x168);
      uVar4 = *(undefined4 *)(param_1 + 0x16c);
      *puVar1 = *(undefined4 *)(param_1 + 0x160);
      puVar1[1] = uVar5;
      puVar1[2] = uVar3;
      puVar1[3] = uVar4;
      uVar5 = *(undefined4 *)(param_1 + 0x174);
      uVar3 = *(undefined4 *)(param_1 + 0x178);
      uVar4 = *(undefined4 *)(param_1 + 0x17c);
      puVar1[4] = *(undefined4 *)(param_1 + 0x170);
      puVar1[5] = uVar5;
      puVar1[6] = uVar3;
      puVar1[7] = uVar4;
      puVar1 = (undefined4 *)((long)__dest + param_5 + 0x20);
      uVar5 = *(undefined4 *)(param_1 + 0x144);
      uVar3 = *(undefined4 *)(param_1 + 0x148);
      uVar4 = *(undefined4 *)(param_1 + 0x14c);
      *puVar1 = *(undefined4 *)(param_1 + 0x140);
      puVar1[1] = uVar5;
      puVar1[2] = uVar3;
      puVar1[3] = uVar4;
      uVar5 = *(undefined4 *)(param_1 + 0x154);
      uVar3 = *(undefined4 *)(param_1 + 0x158);
      uVar4 = *(undefined4 *)(param_1 + 0x15c);
      puVar1[4] = *(undefined4 *)(param_1 + 0x150);
      puVar1[5] = uVar5;
      puVar1[6] = uVar3;
      puVar1[7] = uVar4;
      *(char *)((long)__dest + param_5 + 0x40) = (char)(param_7 >> 8);
      *(undefined *)((long)__dest + param_5 + 0x41) = (undefined)param_7;
      if (((ulong)param_6 | param_7) == 0) goto LAB_0014706a;
      memcpy((void *)((long)__dest + param_5 + 0x42),param_6,param_7);
      uVar2 = *__dest;
joined_r0x00147078:
      if ((((((uVar2 == 0x6620746e65696c63) && (*(int *)(__dest + 1) == 0x73696e69)) &&
            (*(short *)((long)__dest + 0xc) == 0x6568)) && (*(char *)((long)__dest + 0xe) == 'd'))
          || (((((*__dest == 0x6620726576726573 && (*(int *)(__dest + 1) == 0x73696e69)) &&
                ((*(short *)((long)__dest + 0xc) == 0x6568 && (*(char *)((long)__dest + 0xe) == 'd')
                 ))) || (((*__dest == 0x732072657473616d && (*(int *)(__dest + 1) == 0x65726365)) &&
                         (*(char *)((long)__dest + 0xc) == 't')))) ||
              ((((__dest[1] ^ 0x2072657473616d20 | *__dest ^ 0x6465646e65747865) == 0 &&
                (*(int *)(__dest + 2) == 0x72636573)) && (*(short *)((long)__dest + 0x14) == 0x7465)
               ))))) ||
         (((*__dest == 0x617078652079656b && (*(int *)(__dest + 1) == 0x6f69736e)) &&
          (*(char *)((long)__dest + 0xc) == 'n')))) {
        ERR_new();
        uVar5 = 0;
        ERR_set_debug("ssl/t1_enc.c",0x2c8,"tls1_export_keying_material");
        ERR_set_error(0x14,0x16f,0);
      }
      else {
        uVar5 = tls1_PRF_constprop_0
                          (param_1,__dest,lVar6,0,0,0,0,0,0,*(long *)(param_1 + 0x918) + 0x50,
                           *(undefined8 *)(*(long *)(param_1 + 0x918) + 8),param_2,param_3,0);
      }
      goto LAB_00147117;
    }
  }
  ERR_new();
  __dest = (ulong *)0x0;
  uVar5 = 0;
  ERR_set_debug("ssl/t1_enc.c",0x2cc,"tls1_export_keying_material");
  ERR_set_error(0x14,0xc0100,0);
LAB_00147117:
  CRYPTO_clear_free(__dest,lVar6,"ssl/t1_enc.c",0x2cf);
  return uVar5;
}