static ossl_inline void pqueue_force_bottom(OSSL_PQUEUE *pq, size_t n)
{
    ASSERT_USED(pq, n);
    while (n > 0) {
        const size_t p = (n - 1) / 2;

        ASSERT_USED(pq, p);
        pqueue_swap_elem(pq, n, p);
        n = p;
    }
}