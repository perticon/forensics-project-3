void ssl_cert_clear_certs(long param_1)

{
  X509 **ppXVar1;
  X509 **ppXVar2;
  
  if (param_1 != 0) {
    ppXVar1 = (X509 **)(param_1 + 0x20);
    do {
      ppXVar2 = ppXVar1 + 5;
      X509_free(*ppXVar1);
      *ppXVar1 = (X509 *)0x0;
      EVP_PKEY_free((EVP_PKEY *)ppXVar1[1]);
      ppXVar1[1] = (X509 *)0x0;
      OSSL_STACK_OF_X509_free(ppXVar1[2]);
      ppXVar1[2] = (X509 *)0x0;
      CRYPTO_free(ppXVar1[3]);
      ppXVar1[3] = (X509 *)0x0;
      ppXVar1[4] = (X509 *)0x0;
      ppXVar1 = ppXVar2;
    } while (ppXVar2 != (X509 **)(param_1 + 0x188));
    return;
  }
  return;
}