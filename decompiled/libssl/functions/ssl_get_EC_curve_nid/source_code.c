int ssl_get_EC_curve_nid(const EVP_PKEY *pkey)
{
    char gname[OSSL_MAX_NAME_SIZE];

    if (EVP_PKEY_get_group_name(pkey, gname, sizeof(gname), NULL) > 0)
        return OBJ_txt2nid(gname);

    return NID_undef;
}