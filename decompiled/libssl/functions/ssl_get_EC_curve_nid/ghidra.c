int ssl_get_EC_curve_nid(undefined8 param_1)

{
  int iVar1;
  int iVar2;
  long in_FS_OFFSET;
  char acStack72 [56];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = EVP_PKEY_get_group_name(param_1,acStack72,0x32,0);
  iVar2 = 0;
  if (0 < iVar1) {
    iVar2 = OBJ_txt2nid(acStack72);
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}