int SSL_CTX_use_certificate_ASN1(SSL_CTX *ctx,int len,uchar *d)

{
  int iVar1;
  X509 *pXVar2;
  long in_FS_OFFSET;
  uchar *local_30;
  X509 *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_30 = d;
  local_28 = (X509 *)X509_new_ex(ctx->method,ctx[1].tlsext_ticket_key_cb);
  if (local_28 == (X509 *)0x0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x14e,"SSL_CTX_use_certificate_ASN1");
    ERR_set_error(0x14,0xc0100,0);
  }
  else {
    pXVar2 = d2i_X509(&local_28,&local_30,(long)len);
    if (pXVar2 == (X509 *)0x0) {
      iVar1 = 0;
      X509_free(local_28);
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0x154,"SSL_CTX_use_certificate_ASN1");
      ERR_set_error(0x14,0x8000d,0);
    }
    else {
      iVar1 = SSL_CTX_use_certificate(ctx,local_28);
      X509_free(local_28);
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}