static int ssl3_set_req_cert_type(CERT *c, const unsigned char *p, size_t len)
{
    OPENSSL_free(c->ctype);
    c->ctype = NULL;
    c->ctype_len = 0;
    if (p == NULL || len == 0)
        return 1;
    if (len > 0xff)
        return 0;
    c->ctype = OPENSSL_memdup(p, len);
    if (c->ctype == NULL)
        return 0;
    c->ctype_len = len;
    return 1;
}