int64_t tls_construct_cke_gost(int64_t a1, int64_t a2, int64_t a3, int64_t a4) {
    int64_t v1 = __readfsqword(40); // 0x6b624
    int64_t v2 = *(int64_t *)(a1 + (int64_t)&g350); // 0x6b649
    int64_t result; // 0x6b610
    if (*(int64_t *)(v2 + (int64_t)&g74) == 0) {
        // 0x6b930
        function_20230();
        function_207e0();
        ossl_statem_fatal();
        result = 0;
        goto lab_0x6b8c0;
    } else {
        // 0x6b66e
        function_20e60();
        if (function_20340() == 0) {
            // 0x6b970
            function_20230();
            function_207e0();
            ossl_statem_fatal();
            result = 0;
            goto lab_0x6b8c0;
        } else {
            int64_t v3 = function_21b90(); // 0x6b6b6
            if (v3 == 0) {
                // 0x6b9b0
                function_20230();
                function_207e0();
                // 0x6b884
                ossl_statem_fatal();
                goto lab_0x6b895;
            } else {
                // 0x6b6c7
                if ((int32_t)function_206d0() < 1) {
                    // 0x6b860
                    function_20230();
                    // 0x6b871
                    function_207e0();
                    // 0x6b884
                    ossl_statem_fatal();
                    goto lab_0x6b895;
                } else {
                    // 0x6b6d7
                    if ((int32_t)function_20ca0() < 1) {
                        // 0x6b860
                        function_20230();
                        // 0x6b871
                        function_207e0();
                        // 0x6b884
                        ossl_statem_fatal();
                        goto lab_0x6b895;
                    } else {
                        // 0x6b6f8
                        if (function_21050() == 0) {
                            goto lab_0x6b8f0;
                        } else {
                            // 0x6b709
                            function_20f40();
                            function_20120();
                            if ((int32_t)function_21cd0() < 1) {
                                goto lab_0x6b8f0;
                            } else {
                                // 0x6b72c
                                if ((int32_t)function_21ce0() < 1) {
                                    goto lab_0x6b8f0;
                                } else {
                                    // 0x6b748
                                    if ((int32_t)function_21ce0() < 1) {
                                        goto lab_0x6b8f0;
                                    } else {
                                        // 0x6b764
                                        if ((int32_t)function_20870() < 1) {
                                            goto lab_0x6b8f0;
                                        } else {
                                            // 0x6b781
                                            function_21a90();
                                            if ((int32_t)function_20270() < 1) {
                                                // 0x6b9ef
                                                function_20230();
                                                function_207e0();
                                                // 0x6b884
                                                ossl_statem_fatal();
                                                goto lab_0x6b895;
                                            } else {
                                                // 0x6b7b1
                                                if ((int32_t)function_20f80() < 1) {
                                                    // 0x6ba18
                                                    function_20230();
                                                    function_207e0();
                                                    // 0x6b884
                                                    ossl_statem_fatal();
                                                    goto lab_0x6b895;
                                                } else {
                                                    // 0x6b7e0
                                                    if ((int32_t)WPACKET_put_bytes__() == 0) {
                                                        goto lab_0x6b9d9;
                                                    } else {
                                                        // 0x6b805
                                                        if ((int32_t)WPACKET_put_bytes__() == 0) {
                                                            goto lab_0x6b9d9;
                                                        } else {
                                                            // 0x6b824
                                                            if ((int32_t)WPACKET_sub_memcpy__() == 0) {
                                                                goto lab_0x6b9d9;
                                                            } else {
                                                                // 0x6b83c
                                                                function_217f0();
                                                                *(int64_t *)(a1 + (int64_t)&g119) = v3;
                                                                *(int64_t *)(a1 + (int64_t)&g120) = 32;
                                                                result = 1;
                                                                goto lab_0x6b8c0;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
  lab_0x6b8c0:
    // 0x6b8c0
    if (v1 != __readfsqword(40)) {
        // 0x6ba41
        return function_20ff0();
    }
    // 0x6b8d7
    return result;
  lab_0x6b895:
    // 0x6b895
    function_217f0();
    function_21a00();
    function_21a90();
    result = 0;
    goto lab_0x6b8c0;
  lab_0x6b8f0:
    // 0x6b8f0
    function_20230();
    function_207e0();
    ossl_statem_fatal();
    goto lab_0x6b895;
  lab_0x6b9d9:
    // 0x6b9d9
    function_20230();
    // 0x6b871
    function_207e0();
    // 0x6b884
    ossl_statem_fatal();
    goto lab_0x6b895;
}