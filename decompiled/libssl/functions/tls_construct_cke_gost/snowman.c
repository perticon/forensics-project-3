int32_t tls_construct_cke_gost(void** rdi, void** rsi) {
    uint64_t rax3;
    uint32_t r12d4;
    int32_t r12d5;
    int32_t r12d6;
    uint32_t r12d7;
    int32_t r12d8;
    void** rdi9;
    int32_t eax10;
    void** r13_11;
    void** rax12;
    void** rdi13;
    void** rax14;
    void** rax15;
    int32_t eax16;
    void** rdi17;
    int64_t rax18;
    void** rax19;
    void** rbx20;
    int64_t rdi21;
    void** rax22;
    void** rax23;
    int32_t eax24;
    int32_t eax25;
    int32_t eax26;
    void* rsp27;
    void** rsi28;
    int32_t eax29;
    int32_t eax30;
    int32_t eax31;
    int64_t rax32;
    uint64_t rbx33;
    void** rsi34;
    int32_t eax35;
    int64_t rax36;

    rax3 = g28;
    r12d4 = r12d5 - (r12d6 + reinterpret_cast<uint1_t>(r12d7 < r12d8 + reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x2e0) + 32)) & 0x80) < 1)));
    rdi9 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x918) + 0x2b8);
    *reinterpret_cast<unsigned char*>(&r12d4) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d4) & 83);
    if (!rdi9) {
        fun_20230();
        fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
        ossl_statem_fatal(rdi, rdi);
        eax10 = 0;
    } else {
        r13_11 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x9a8) + 0x440);
        rax12 = fun_20e60(rdi9);
        rdi13 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x9a8));
        rax14 = fun_20340(rdi13, rax12, r13_11);
        if (!rax14) {
            fun_20230();
            fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
            ossl_statem_fatal(rdi, rdi);
            eax10 = 0;
        } else {
            rax15 = fun_21b90(32, "ssl/statem/statem_clnt.c", 0xbeb);
            if (!rax15) {
                fun_20230();
                fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
                goto addr_6b884_7;
            } else {
                eax16 = fun_206d0(rax14, "ssl/statem/statem_clnt.c", 0xbeb);
                if (reinterpret_cast<uint1_t>(eax16 < 0) | reinterpret_cast<uint1_t>(eax16 == 0) || (rdi17 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x9a8)), rax18 = fun_20ca0(rdi17, rax15, 32), reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax18) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax18) == 0))) {
                    fun_20230();
                    goto addr_6b871_10;
                } else {
                    rax19 = fun_21050(rdi17, rdi17);
                    rbx20 = rax19;
                    if (!rax19 || ((*reinterpret_cast<uint32_t*>(&rdi21) = r12d4 + 0x3d6, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0, rax22 = fun_20f40(rdi21, rax15, rdi21, rax15), rax23 = fun_20120(rax22, rax15, 32), eax24 = fun_21cd0(rbx20, rax23, 32), reinterpret_cast<uint1_t>(eax24 < 0) | reinterpret_cast<uint1_t>(eax24 == 0)) || ((eax25 = fun_21ce0(rbx20, rdi + 0x160, 32), reinterpret_cast<uint1_t>(eax25 < 0) | reinterpret_cast<uint1_t>(eax25 == 0)) || ((eax26 = fun_21ce0(rbx20, rdi + 0x140, 32), rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8), reinterpret_cast<uint1_t>(eax26 < 0) | reinterpret_cast<uint1_t>(eax26 == 0)) || (rsi28 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp27) + 16), eax29 = fun_20870(rbx20, rsi28, rbx20, rsi28), rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8), reinterpret_cast<uint1_t>(eax29 < 0) | reinterpret_cast<uint1_t>(eax29 == 0)))))) {
                        fun_20230();
                        fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
                        ossl_statem_fatal(rdi, rdi);
                        goto addr_6b895_13;
                    } else {
                        fun_21a90(rbx20, rsi28, rbx20, rsi28);
                        eax30 = fun_20270(rax14, rax14);
                        if (reinterpret_cast<uint1_t>(eax30 < 0) | reinterpret_cast<uint1_t>(eax30 == 0)) {
                            fun_20230();
                            fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
                            goto addr_6b884_7;
                        } else {
                            eax31 = fun_20f80(rax14, rax14);
                            if (reinterpret_cast<uint1_t>(eax31 < 0) | reinterpret_cast<uint1_t>(eax31 == 0)) {
                                fun_20230();
                                fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
                                goto addr_6b884_7;
                            } else {
                                rax32 = WPACKET_put_bytes__(rsi, rsi);
                                if (!*reinterpret_cast<int32_t*>(&rax32)) 
                                    goto addr_6b9d9_19;
                                if (0) 
                                    goto addr_6b824_21; else 
                                    goto addr_6b805_22;
                            }
                        }
                    }
                }
            }
        }
    }
    addr_6b8c0_23:
    rbx33 = rax3 ^ g28;
    if (rbx33) {
        fun_20ff0();
    } else {
        return eax10;
    }
    addr_6b884_7:
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
    ossl_statem_fatal(rdi, rdi);
    addr_6b895_13:
    fun_217f0();
    fun_21a00(rax15, 32, "ssl/statem/statem_clnt.c", 0xc26, rax15, 32, "ssl/statem/statem_clnt.c", 0xc26);
    fun_21a90(rbx20, 32, rbx20, 32);
    eax10 = 0;
    goto addr_6b8c0_23;
    addr_6b871_10:
    fun_207e0("ssl/statem/statem_clnt.c", "ssl/statem/statem_clnt.c");
    goto addr_6b884_7;
    addr_6b824_21:
    rsi34 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8 + 48);
    eax35 = WPACKET_sub_memcpy__(rsi, rsi34, rsi, rsi34);
    if (!eax35) {
        addr_6b9d9_19:
        fun_20230();
        goto addr_6b871_10;
    } else {
        fun_217f0();
        *reinterpret_cast<void***>(rdi + 0x360) = rax15;
        eax10 = 1;
        *reinterpret_cast<void***>(rdi + 0x368) = reinterpret_cast<void**>(32);
        goto addr_6b8c0_23;
    }
    addr_6b805_22:
    rax36 = WPACKET_put_bytes__(rsi, rsi);
    if (!*reinterpret_cast<int32_t*>(&rax36)) 
        goto addr_6b9d9_19;
    goto addr_6b824_21;
}