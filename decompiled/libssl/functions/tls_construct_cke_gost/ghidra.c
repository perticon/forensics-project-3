undefined8 tls_construct_cke_gost(long param_1,undefined8 param_2)

{
  uint uVar1;
  int iVar2;
  undefined8 uVar3;
  EVP_PKEY_CTX *ctx;
  uchar *in;
  EVP_MD_CTX *ctx_00;
  char *name;
  EVP_MD *type;
  undefined8 uVar4;
  long in_FS_OFFSET;
  uint local_174;
  ulong local_170;
  uchar local_168 [32];
  uchar local_148 [264];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20);
  if (*(long *)(*(long *)(param_1 + 0x918) + 0x2b8) == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0xbd7,"tls_construct_cke_gost");
    ossl_statem_fatal(param_1,0x28,0x14a,0);
    uVar4 = 0;
    goto LAB_0016b8c0;
  }
  uVar4 = *(undefined8 *)(*(long *)(param_1 + 0x9a8) + 0x440);
  uVar3 = X509_get0_pubkey();
  ctx = (EVP_PKEY_CTX *)EVP_PKEY_CTX_new_from_pkey(**(undefined8 **)(param_1 + 0x9a8),uVar3,uVar4);
  if (ctx == (EVP_PKEY_CTX *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0xbe0,"tls_construct_cke_gost");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
    uVar4 = 0;
    goto LAB_0016b8c0;
  }
  in = (uchar *)CRYPTO_malloc(0x20,"ssl/statem/statem_clnt.c",0xbeb);
  if (in == (uchar *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0xbed,"tls_construct_cke_gost");
    uVar4 = 0xc0100;
LAB_0016b884:
    ctx_00 = (EVP_MD_CTX *)0x0;
    ossl_statem_fatal(param_1,0x50,uVar4,0);
  }
  else {
    iVar2 = EVP_PKEY_encrypt_init(ctx);
    if ((iVar2 < 1) ||
       (iVar2 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),in,0x20,0), iVar2 < 1)) {
      ERR_new();
      uVar4 = 0xbf5;
LAB_0016b871:
      ERR_set_debug("ssl/statem/statem_clnt.c",uVar4,"tls_construct_cke_gost");
      uVar4 = 0xc0103;
      goto LAB_0016b884;
    }
    ctx_00 = (EVP_MD_CTX *)EVP_MD_CTX_new();
    if (ctx_00 != (EVP_MD_CTX *)0x0) {
      name = OBJ_nid2sn((-(uint)((uVar1 & 0x80) == 0) & 0xffffff53) + 0x3d6);
      type = EVP_get_digestbyname(name);
      iVar2 = EVP_DigestInit(ctx_00,type);
      if (((0 < iVar2) &&
          (iVar2 = EVP_DigestUpdate(ctx_00,(void *)(param_1 + 0x160),0x20), 0 < iVar2)) &&
         (iVar2 = EVP_DigestUpdate(ctx_00,(void *)(param_1 + 0x140),0x20), 0 < iVar2)) {
        iVar2 = EVP_DigestFinal_ex(ctx_00,local_168,&local_174);
        if (0 < iVar2) {
          EVP_MD_CTX_free(ctx_00);
          iVar2 = EVP_PKEY_CTX_ctrl(ctx,-1,0x200,8,8,local_168);
          if (iVar2 < 1) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0xc0b,"tls_construct_cke_gost");
            uVar4 = 0x112;
          }
          else {
            local_170 = 0xff;
            iVar2 = EVP_PKEY_encrypt(ctx,local_148,&local_170,in,0x20);
            if (0 < iVar2) {
              iVar2 = WPACKET_put_bytes__(param_2,0x30,1);
              if (((iVar2 != 0) &&
                  ((local_170 < 0x80 || (iVar2 = WPACKET_put_bytes__(param_2,0x81,1), iVar2 != 0))))
                 && (iVar2 = WPACKET_sub_memcpy__(param_2,local_148,local_170,1), iVar2 != 0)) {
                EVP_PKEY_CTX_free(ctx);
                *(uchar **)(param_1 + 0x360) = in;
                uVar4 = 1;
                *(undefined8 *)(param_1 + 0x368) = 0x20;
                goto LAB_0016b8c0;
              }
              ERR_new();
              uVar4 = 0xc1b;
              goto LAB_0016b871;
            }
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0xc14,"tls_construct_cke_gost");
            uVar4 = 0x112;
          }
          goto LAB_0016b884;
        }
      }
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0xc04,"tls_construct_cke_gost");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  EVP_PKEY_CTX_free(ctx);
  CRYPTO_clear_free(in,0x20,"ssl/statem/statem_clnt.c",0xc26);
  EVP_MD_CTX_free(ctx_00);
  uVar4 = 0;
LAB_0016b8c0:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}