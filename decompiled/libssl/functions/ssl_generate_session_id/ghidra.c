int ssl_generate_session_id(SSL *param_1,long param_2)

{
  uchar *id;
  size_t __n;
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  ulong uVar4;
  code *pcVar5;
  long in_FS_OFFSET;
  uint local_34;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = param_1->version;
  if (iVar1 == 0xfefd) {
LAB_00143257:
    iVar1 = *(int *)&param_1[3].next_proto_negotiated_len;
    *(undefined8 *)(param_2 + 0x250) = 0x20;
    if (iVar1 != 0) {
      *(undefined8 *)(param_2 + 0x250) = 0;
      iVar1 = 1;
      goto LAB_0014327d;
    }
    iVar1 = CRYPTO_THREAD_read_lock(*(undefined8 *)&param_1[10].next_proto_negotiated_len);
    if (iVar1 == 0) goto LAB_0014327d;
    iVar1 = CRYPTO_THREAD_read_lock(*(undefined8 *)(*(long *)&param_1[4].mac_flags + 0x3c8));
    if (iVar1 == 0) {
      CRYPTO_THREAD_unlock(*(undefined8 *)&param_1[10].next_proto_negotiated_len);
      ERR_new();
      ERR_set_debug("ssl/ssl_sess.c",0x180,"ssl_generate_session_id");
      ossl_statem_fatal(param_1,0x50,0x115,0);
      goto LAB_0014327d;
    }
    pcVar5 = *(code **)(param_1[3].sid_ctx + 0x1c);
    if ((pcVar5 == (code *)0x0) &&
       (pcVar5 = *(code **)(*(long *)&param_1[4].mac_flags + 0x1b0), pcVar5 == (code *)0x0)) {
      pcVar5 = def_generate_session_id;
    }
    id = (uchar *)(param_2 + 600);
    CRYPTO_THREAD_unlock(*(undefined8 *)(*(long *)&param_1[4].mac_flags + 0x3c8));
    CRYPTO_THREAD_unlock(*(undefined8 *)&param_1[10].next_proto_negotiated_len);
    __n = *(size_t *)(param_2 + 0x250);
    memset(id,0,__n);
    local_34 = (uint)__n;
    iVar1 = (*pcVar5)(param_1,id,&local_34);
    if (iVar1 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_sess.c",399,"ssl_generate_session_id");
      ossl_statem_fatal(param_1,0x50,0x12d,0);
      goto LAB_0014327d;
    }
    uVar4 = (ulong)local_34;
    if ((local_34 == 0) ||
       (*(ulong *)(param_2 + 0x250) <= uVar4 && uVar4 != *(ulong *)(param_2 + 0x250))) {
      ERR_new();
      ERR_set_debug("ssl/ssl_sess.c",0x199,"ssl_generate_session_id");
      uVar3 = 0x12f;
    }
    else {
      *(ulong *)(param_2 + 0x250) = uVar4;
      iVar1 = 1;
      iVar2 = SSL_has_matching_session_id(param_1,id,local_34);
      if (iVar2 == 0) goto LAB_0014327d;
      ERR_new();
      ERR_set_debug("ssl/ssl_sess.c",0x1a1,"ssl_generate_session_id");
      uVar3 = 0x12e;
    }
  }
  else {
    if (iVar1 < 0xfefe) {
      if ((iVar1 == 0x100) || (iVar1 - 0x300U < 5)) goto LAB_00143257;
    }
    else if (iVar1 == 0xfeff) goto LAB_00143257;
    ERR_new();
    ERR_set_debug("ssl/ssl_sess.c",0x163,"ssl_generate_session_id");
    uVar3 = 0x103;
  }
  iVar1 = 0;
  ossl_statem_fatal(param_1,0x50,uVar3,0);
LAB_0014327d:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}