const SSL_CIPHER *ossl_quic_get_cipher(unsigned int u)
{
    /*
     * TODO(QUIC): This is needed so the SSL_CTX_set_cipher_list("DEFAULT");
     * produces at least one valid TLS-1.2 cipher.
     * Later we should allow that there are none with QUIC protocol as
     * SSL_CTX_set_cipher_list should still allow setting a SECLEVEL.
     */
    static const SSL_CIPHER ciph = {
        1,
        TLS1_TXT_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
        TLS1_RFC_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
        TLS1_CK_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
        SSL_kECDHE,
        SSL_aRSA,
        SSL_AES256GCM,
        SSL_AEAD,
        TLS1_2_VERSION, TLS1_2_VERSION,
        DTLS1_2_VERSION, DTLS1_2_VERSION,
        SSL_HIGH | SSL_FIPS,
        SSL_HANDSHAKE_MAC_SHA384 | TLS1_PRF_SHA384,
        256,
        256
    };

    return &ciph;
}