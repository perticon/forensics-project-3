undefined4 SSL_get0_dane_authority(long param_1,undefined8 *param_2,undefined8 *param_3)

{
  long lVar1;
  int iVar2;
  undefined4 uVar3;
  undefined8 uVar4;
  
  iVar2 = OPENSSL_sk_num(*(undefined8 *)(param_1 + 0x4e8));
  if ((iVar2 < 1) || (*(long *)(param_1 + 0x9b8) != 0)) {
    uVar3 = 0xffffffff;
  }
  else {
    lVar1 = *(long *)(param_1 + 0x4f8);
    if (lVar1 != 0) {
      if (param_2 != (undefined8 *)0x0) {
        *param_2 = *(undefined8 *)(param_1 + 0x500);
      }
      if (param_3 != (undefined8 *)0x0) {
        uVar4 = 0;
        if (*(long *)(param_1 + 0x500) == 0) {
          uVar4 = *(undefined8 *)(lVar1 + 0x18);
        }
        *param_3 = uVar4;
      }
    }
    uVar3 = *(undefined4 *)(param_1 + 0x50c);
  }
  return uVar3;
}