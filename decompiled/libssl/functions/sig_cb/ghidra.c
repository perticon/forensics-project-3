undefined8 sig_cb(long param_1,int param_2,long *param_3)

{
  int iVar1;
  char *pcVar2;
  undefined1 *puVar3;
  long lVar4;
  undefined8 uVar5;
  short sVar6;
  long lVar7;
  long in_FS_OFFSET;
  int local_60;
  int local_5c;
  char local_58 [40];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = 0;
  local_5c = 0;
  if (((param_1 != 0) && (lVar7 = *param_3, param_2 < 0x28)) && (lVar7 != 0x3e)) {
    __memcpy_chk(local_58,param_1,(long)param_2,0x28);
    local_58[param_2] = '\0';
    pcVar2 = strchr(local_58,0x2b);
    if (pcVar2 == (char *)0x0) {
      puVar3 = sigalg_lookup_tbl;
      do {
        if ((*(char **)puVar3 != (char *)0x0) &&
           (iVar1 = strcmp(local_58,*(char **)puVar3), iVar1 == 0)) {
          sVar6 = *(short *)((long)puVar3 + 8);
          *param_3 = lVar7 + 1;
          *(short *)((long)param_3 + lVar7 * 2 + 8) = sVar6;
          goto LAB_00147cd4;
        }
        puVar3 = (undefined1 *)((long)puVar3 + 0x28);
      } while (puVar3 != &DAT_001a3978);
    }
    else {
      *pcVar2 = '\0';
      if (pcVar2[1] != '\0') {
        get_sigorhash(&local_60,&local_5c,local_58);
        get_sigorhash(&local_60,&local_5c,pcVar2 + 1);
        if ((local_60 != 0) && (local_5c != 0)) {
          puVar3 = sigalg_lookup_tbl;
LAB_00147c6d:
          if ((local_5c != *(int *)(puVar3 + 0xc)) || (local_60 != *(int *)(puVar3 + 0x14)))
          goto LAB_00147c60;
          lVar7 = *param_3;
          *param_3 = lVar7 + 1;
          sVar6 = *(short *)(puVar3 + 8);
          *(short *)((long)param_3 + lVar7 * 2 + 8) = sVar6;
LAB_00147cd4:
          if (lVar7 == 0) {
LAB_00147d00:
            uVar5 = 1;
            goto LAB_00147bf2;
          }
          lVar4 = 0;
          while (*(short *)((long)param_3 + lVar4 * 2 + 8) != sVar6) {
            lVar4 = lVar4 + 1;
            if (lVar7 == lVar4) goto LAB_00147d00;
          }
          *param_3 = lVar7;
        }
      }
    }
  }
LAB_00147bf0:
  uVar5 = 0;
LAB_00147bf2:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
LAB_00147c60:
  puVar3 = puVar3 + 0x28;
  if (puVar3 == &DAT_001a3978) goto LAB_00147bf0;
  goto LAB_00147c6d;
}