undefined8 ssl3_send_alert(int *param_1,int param_2,undefined4 param_3)

{
  long lVar1;
  int iVar2;
  undefined8 uVar3;
  undefined uVar4;
  
  lVar1 = *(long *)(*(int **)(param_1 + 2) + 0x30);
  if (((((*(byte *)(lVar1 + 0x60) & 8) == 0) && (iVar2 = **(int **)(param_1 + 2), iVar2 != 0x10000))
      && (0x303 < iVar2)) || ((param_1[0x21] - 1U < 4 || (param_1[0x23a] == 1)))) {
    iVar2 = tls13_alert_code();
  }
  else {
    iVar2 = (**(code **)(lVar1 + 0x50))(param_3);
  }
  if ((*param_1 == 0x300) && (iVar2 == 0x46)) {
    if ((*(byte *)(param_1 + 0x11) & 1) != 0) {
      return 0xffffffff;
    }
    uVar4 = 0x28;
  }
  else {
    if (iVar2 < 0) {
      return 0xffffffff;
    }
    uVar4 = (undefined)iVar2;
    if ((*(byte *)(param_1 + 0x11) & 1) != 0) {
      if (iVar2 != 0) {
        return 0xffffffff;
      }
      uVar4 = 0;
    }
  }
  if ((param_2 == 2) && (*(SSL_SESSION **)(param_1 + 0x246) != (SSL_SESSION *)0x0)) {
    SSL_CTX_remove_session(*(SSL_CTX **)(param_1 + 0x2e2),*(SSL_SESSION **)(param_1 + 0x246));
  }
  *(char *)(param_1 + 0x6a) = (char)param_2;
  param_1[0x69] = 1;
  *(undefined *)((long)param_1 + 0x1a9) = uVar4;
  iVar2 = RECORD_LAYER_write_pending(param_1 + 0x316);
  if (iVar2 != 0) {
    return 0xffffffff;
  }
                    /* WARNING: Could not recover jumptable at 0x0012c03e. Too many branches */
                    /* WARNING: Treating indirect jump as call */
  uVar3 = (**(code **)(*(long *)(param_1 + 2) + 0x78))(param_1);
  return uVar3;
}