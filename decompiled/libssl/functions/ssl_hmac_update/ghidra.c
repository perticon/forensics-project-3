undefined8 ssl_hmac_update(long *param_1)

{
  undefined8 uVar1;
  
  if (*param_1 != 0) {
    uVar1 = (*(code *)PTR_EVP_MAC_update_001a89d8)(*param_1);
    return uVar1;
  }
  if (param_1[1] == 0) {
    return 0;
  }
  uVar1 = ssl_hmac_old_update();
  return uVar1;
}