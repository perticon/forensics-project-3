uint tls_construct_client_hello(SSL *param_1,uchar *param_2)

{
  long lVar1;
  int *piVar2;
  bool bVar3;
  uint uVar4;
  int iVar5;
  int iVar6;
  int iVar7;
  KSSL_CTX **ppKVar8;
  stack_st_SSL_CIPHER *psVar9;
  ulong uVar10;
  SSL_CIPHER *cipher;
  undefined4 *puVar11;
  int iVar12;
  char *pcVar13;
  undefined8 uVar14;
  _func_3152 *__n;
  SSL_SESSION **__src;
  long in_FS_OFFSET;
  ulong local_70;
  long local_48;
  
  puVar11 = *(undefined4 **)&param_1[3].sid_ctx_length;
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  uVar4 = ssl_set_client_hello_version();
  if (uVar4 == 0) {
    if (((puVar11 != (undefined4 *)0x0) &&
        (iVar5 = ssl_version_supported(param_1,*puVar11,0), iVar5 != 0)) &&
       (iVar5 = SSL_SESSION_is_resumable(), iVar5 != 0)) {
LAB_0016ae42:
      if (((byte)param_1->method->get_timeout[0x60] & 8) == 0) {
        if (*(int *)&param_1[3].read_hash == 0) goto LAB_0016ae6d;
LAB_0016ac02:
        iVar5 = WPACKET_put_bytes__(param_2,*(undefined4 *)&param_1[3].field_0x1fc,2);
        if ((iVar5 == 0) || (iVar5 = WPACKET_memcpy(param_2,&param_1->kssl_ctx,0x20), iVar5 == 0)) {
          ERR_new();
          pcVar13 = "tls_construct_client_hello";
          uVar14 = 0x49e;
        }
        else {
          if ((param_1->new_session == 0) &&
             (piVar2 = *(int **)&param_1[3].sid_ctx_length, *piVar2 != 0x304)) {
            __n = *(_func_3152 **)(piVar2 + 0x94);
            __src = (SSL_SESSION **)(piVar2 + 0x96);
            if (param_1->version == 0x304) {
              param_1[3].info_callback = __n;
              memcpy(&param_1[3].session,__src,(size_t)__n);
            }
            iVar5 = WPACKET_start_sub_packet_len__(param_2,1);
            if (iVar5 != 0) {
              if (__n != (_func_3152 *)0x0) {
LAB_0016af7e:
                iVar5 = WPACKET_memcpy(param_2,__src,__n);
                goto joined_r0x0016af8e;
              }
LAB_0016ac65:
              iVar5 = WPACKET_close(param_2);
              if (iVar5 != 0) {
                if (((byte)param_1->method->get_timeout[0x60] & 8) != 0) {
                  uVar10 = *(ulong *)&param_1[1].tlsext_ocsp_exts[8].stack;
                  if ((0xff < uVar10) ||
                     (iVar5 = WPACKET_sub_memcpy__(param_2,param_1[1].tlsext_ocsp_exts,uVar10,1),
                     iVar5 == 0)) {
                    ERR_new();
                    pcVar13 = "tls_construct_client_hello";
                    uVar14 = 0x4c8;
                    goto LAB_0016af11;
                  }
                }
                iVar5 = WPACKET_start_sub_packet_len__(param_2,2);
                if (iVar5 == 0) {
                  ERR_new();
                  pcVar13 = "tls_construct_client_hello";
                  uVar14 = 0x4cf;
                }
                else {
                  psVar9 = SSL_get_ciphers(param_1);
                  iVar5 = *(int *)&param_1[4].expand;
                  iVar6 = ssl_set_client_disabled(param_1);
                  if (iVar6 == 0) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_clnt.c",0xe5e,"ssl_cipher_list_to_bytes");
                    uVar4 = 0xbf;
                    goto LAB_0016aec2;
                  }
                  if (psVar9 == (stack_st_SSL_CIPHER *)0x0) {
                    ERR_new();
                    pcVar13 = "ssl_cipher_list_to_bytes";
                    uVar14 = 0xe63;
                    goto LAB_0016af11;
                  }
                  bVar3 = false;
                  local_70 = 0;
                  uVar10 = (-(ulong)(iVar5 == 0) & 0xfffffffffffffffe) + 0xfffc;
                  if ((*(byte *)&param_1[3].tlsext_debug_arg & 0x80) == 0) {
                    uVar10 = (-(ulong)(iVar5 == 0) & 0xfffffffffffffffe) + 0xfffe;
                  }
                  iVar6 = 0;
                  while ((iVar7 = OPENSSL_sk_num(psVar9), iVar6 < iVar7 && (local_70 < uVar10))) {
                    cipher = (SSL_CIPHER *)OPENSSL_sk_value(psVar9,iVar6);
                    iVar7 = ssl_cipher_disabled(param_1,cipher,0x10001);
                    if (iVar7 == 0) {
                      iVar7 = (*param_1->method->put_cipher_by_char)(cipher,param_2);
                      if (iVar7 == 0) {
                        ERR_new();
                        pcVar13 = "ssl_cipher_list_to_bytes";
                        uVar14 = 0xe85;
                        goto LAB_0016b104;
                      }
                      if (!bVar3) {
                        iVar7 = *(int *)&param_1[1].generate_session_id;
                        if (((byte)param_1->method->get_timeout[0x60] & 8) == 0) {
                          if (iVar7 <= *(int *)&cipher->algorithm_mac) {
                            piVar2 = (int *)((long)&cipher->algorithm_enc + 4);
                            bVar3 = *piVar2 == iVar7 || *piVar2 < iVar7;
                          }
                        }
                        else {
                          iVar12 = *(int *)&cipher->algorithm_ssl;
                          if (iVar12 == 0x100) {
                            iVar12 = 0xff00;
                            if (iVar7 != 0x100) goto LAB_0016ad6d;
                            iVar7 = 0xff00;
                          }
                          else {
                            if (iVar7 == 0x100) {
                              iVar7 = 0xff00;
                            }
LAB_0016ad6d:
                            if (iVar7 < iVar12) goto LAB_0016ad78;
                          }
                          iVar12 = *(int *)((long)&cipher->algorithm_mac + 4);
                          if (iVar12 == 0x100) {
                            iVar12 = 0xff00;
                          }
                          bVar3 = iVar7 <= iVar12;
                        }
                      }
LAB_0016ad78:
                      local_70 = local_70 + local_48;
                    }
                    iVar6 = iVar6 + 1;
                  }
                  if ((local_70 == 0) || (!bVar3)) {
                    pcVar13 = "No ciphers enabled for max supported SSL/TLS version";
                    if (bVar3) {
                      pcVar13 = (char *)0x0;
                    }
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_clnt.c",0xe9f,"ssl_cipher_list_to_bytes");
                    ossl_statem_fatal(param_1,0x50,0xb5,pcVar13);
                    goto LAB_0016aed4;
                  }
                  if ((iVar5 == 0) &&
                     (iVar5 = (*param_1->method->put_cipher_by_char)
                                        ((SSL_CIPHER *)scsv_27850,param_2), iVar5 == 0)) {
                    ERR_new();
                    pcVar13 = "ssl_cipher_list_to_bytes";
                    uVar14 = 0xeaa;
                  }
                  else if (((*(byte *)&param_1[3].tlsext_debug_arg & 0x80) == 0) ||
                          (iVar5 = (*param_1->method->put_cipher_by_char)
                                             ((SSL_CIPHER *)scsv_27851,param_2), iVar5 != 0)) {
                    iVar5 = WPACKET_close(param_2);
                    if (iVar5 == 0) {
                      ERR_new();
                      pcVar13 = "tls_construct_client_hello";
                      uVar14 = 0x4d8;
                    }
                    else {
                      iVar5 = WPACKET_start_sub_packet_len__(param_2,1);
                      if (iVar5 != 0) {
                        iVar5 = ssl_allow_compression(param_1);
                        if ((((iVar5 != 0) &&
                             (*(long *)(*(long *)&param_1[3].ex_data.dummy + 0x118) != 0)) &&
                            ((((byte)param_1->method->get_timeout[0x60] & 8) != 0 ||
                             (*(int *)&param_1[1].generate_session_id < 0x304)))) &&
                           (iVar5 = OPENSSL_sk_num(), 0 < iVar5)) {
                          iVar6 = 0;
                          do {
                            puVar11 = (undefined4 *)
                                      OPENSSL_sk_value(*(undefined8 *)
                                                        (*(long *)&param_1[3].ex_data.dummy + 0x118)
                                                       ,iVar6);
                            iVar7 = WPACKET_put_bytes__(param_2,*puVar11,1);
                            if (iVar7 == 0) {
                              ERR_new();
                              pcVar13 = "tls_construct_client_hello";
                              uVar14 = 0x4e9;
                              goto LAB_0016b104;
                            }
                            iVar6 = iVar6 + 1;
                          } while (iVar5 != iVar6);
                        }
                        iVar5 = WPACKET_put_bytes__(param_2,0,1);
                        if ((iVar5 != 0) && (iVar5 = WPACKET_close(param_2), iVar5 != 0)) {
                          iVar5 = tls_construct_extensions(param_1,param_2,0x80,0,0);
                          uVar4 = (uint)(iVar5 != 0);
                          goto LAB_0016aed4;
                        }
                        ERR_new();
                        pcVar13 = "tls_construct_client_hello";
                        uVar14 = 0x4f1;
                        goto LAB_0016af11;
                      }
                      ERR_new();
                      pcVar13 = "tls_construct_client_hello";
                      uVar14 = 0x4de;
                    }
                  }
                  else {
                    ERR_new();
                    pcVar13 = "ssl_cipher_list_to_bytes";
                    uVar14 = 0xeb3;
                  }
                }
LAB_0016b104:
                ERR_set_debug("ssl/statem/statem_clnt.c",uVar14,pcVar13);
                uVar4 = 0xc0103;
                goto LAB_0016aec2;
              }
            }
          }
          else if ((param_1->version == 0x304) &&
                  ((*(byte *)((long)&param_1[3].tlsext_debug_cb + 2) & 0x10) != 0)) {
            __src = &param_1[3].session;
            param_1[3].info_callback = (_func_3152 *)0x20;
            if ((*(int *)&param_1[3].read_hash == 0) &&
               (iVar5 = RAND_bytes_ex(**(undefined8 **)&param_1[3].ex_data.dummy,__src,0x20,0),
               iVar5 < 1)) {
              ERR_new();
              pcVar13 = "tls_construct_client_hello";
              uVar14 = 0x4ad;
              goto LAB_0016af11;
            }
            iVar5 = WPACKET_start_sub_packet_len__(param_2,1);
            if (iVar5 != 0) {
              __n = (_func_3152 *)0x20;
              goto LAB_0016af7e;
            }
          }
          else {
            iVar5 = WPACKET_start_sub_packet_len__(param_2,1);
joined_r0x0016af8e:
            if (iVar5 != 0) goto LAB_0016ac65;
          }
          ERR_new();
          pcVar13 = "tls_construct_client_hello";
          uVar14 = 0x4bf;
        }
      }
      else {
LAB_0016abe1:
        ppKVar8 = &param_1->kssl_ctx;
        do {
          if (*(char *)ppKVar8 != '\0') goto LAB_0016ac02;
          ppKVar8 = (KSSL_CTX **)((long)ppKVar8 + 1);
        } while ((KSSL_CTX **)&param_1->debug != ppKVar8);
LAB_0016ae6d:
        iVar5 = ssl_fill_hello_random(param_1,0,&param_1->kssl_ctx,0x20,0);
        if (0 < iVar5) goto LAB_0016ac02;
        ERR_new();
        pcVar13 = "tls_construct_client_hello";
        uVar14 = 0x477;
      }
LAB_0016af11:
      ERR_set_debug("ssl/statem/statem_clnt.c",uVar14,pcVar13);
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      goto LAB_0016aed4;
    }
    if (*(int *)&param_1[3].read_hash != 0) {
      if (((byte)param_1->method->get_timeout[0x60] & 8) == 0) goto LAB_0016ac02;
      goto LAB_0016abe1;
    }
    iVar5 = ssl_get_new_session(param_1);
    if (iVar5 != 0) goto LAB_0016ae42;
  }
  else {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x453,"tls_construct_client_hello");
LAB_0016aec2:
    ossl_statem_fatal(param_1,0x50,uVar4,0);
  }
  uVar4 = 0;
LAB_0016aed4:
  if (lVar1 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}