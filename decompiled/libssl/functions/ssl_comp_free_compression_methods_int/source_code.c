void ssl_comp_free_compression_methods_int(void)
{
    STACK_OF(SSL_COMP) *old_meths = ssl_comp_methods;
    ssl_comp_methods = NULL;
    sk_SSL_COMP_pop_free(old_meths, cmeth_free);
}