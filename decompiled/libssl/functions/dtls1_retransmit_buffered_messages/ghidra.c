undefined8 dtls1_retransmit_buffered_messages(long param_1)

{
  undefined2 uVar1;
  int iVar2;
  pitem *ppVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  undefined4 local_2c;
  pitem *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_2c = 0;
  local_28 = pqueue_iterator(*(pqueue *)(*(long *)(param_1 + 0x4b8) + 0x120));
  ppVar3 = pqueue_next(&local_28);
  if (ppVar3 != (pitem *)0x0) {
    do {
      uVar1 = dtls1_get_queue_priority
                        (*(undefined2 *)((long)ppVar3->data + 0x10),
                         *(undefined4 *)((long)ppVar3->data + 0x28));
      iVar2 = dtls1_retransmit_message(param_1,uVar1,&local_2c);
      if (iVar2 < 1) {
        uVar4 = 0xffffffff;
        goto LAB_00173195;
      }
      ppVar3 = pqueue_next(&local_28);
    } while (ppVar3 != (pitem *)0x0);
  }
  uVar4 = 1;
LAB_00173195:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}