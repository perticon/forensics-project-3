uint tls_post_process_server_certificate(long param_1)

{
  undefined8 uVar1;
  int iVar2;
  undefined4 uVar3;
  undefined8 uVar4;
  EVP_PKEY *pkey;
  long lVar5;
  uint uVar6;
  long in_FS_OFFSET;
  undefined auStack40 [8];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(int *)(param_1 + 0x28) == 8) {
    *(undefined4 *)(param_1 + 0x28) = 1;
  }
  iVar2 = ssl_verify_cert_chain(param_1,*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2c0));
  if (iVar2 < 1) {
    if (*(int *)(param_1 + 0x968) != 0) {
      ERR_new();
      uVar6 = 0;
      ERR_set_debug("ssl/statem/statem_clnt.c",0x75f,"tls_post_process_server_certificate");
      uVar3 = ssl_x509err2alert(*(undefined4 *)(param_1 + 0x9b8));
      ossl_statem_fatal(param_1,uVar3,0x86,0);
      goto LAB_0016dff3;
    }
  }
  else if (*(int *)(param_1 + 0x28) == 8) {
    uVar6 = 3;
    goto LAB_0016dff3;
  }
  ERR_clear_error();
  uVar4 = OPENSSL_sk_value(*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2c0),0);
  pkey = (EVP_PKEY *)X509_get0_pubkey(uVar4);
  if ((pkey == (EVP_PKEY *)0x0) || (iVar2 = EVP_PKEY_missing_parameters(pkey), iVar2 != 0)) {
    ERR_new();
    uVar6 = 0;
    ERR_set_debug("ssl/statem/statem_clnt.c",0x76e,"tls_post_process_server_certificate");
    ossl_statem_fatal(param_1,0x50,0xef,0);
  }
  else {
    lVar5 = ssl_cert_lookup_by_pkey(pkey,auStack40);
    if (lVar5 == 0) {
      ERR_new();
      uVar6 = 0;
      ERR_set_debug("ssl/statem/statem_clnt.c",0x774,"tls_post_process_server_certificate");
      ossl_statem_fatal(param_1,0x2f,0xf7,0);
    }
    else if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
              (iVar2 = **(int **)(param_1 + 8), 0x303 < iVar2)) && (iVar2 != 0x10000)) ||
            (uVar6 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20) & *(uint *)(lVar5 + 4), uVar6 != 0
            )) {
      X509_free(*(X509 **)(*(long *)(param_1 + 0x918) + 0x2b8));
      X509_up_ref(uVar4);
      lVar5 = *(long *)(param_1 + 0x918);
      uVar1 = *(undefined8 *)(param_1 + 0x9b8);
      *(undefined8 *)(lVar5 + 0x2b8) = uVar4;
      *(undefined8 *)(lVar5 + 0x2c8) = uVar1;
      uVar6 = *(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8;
      if (((uVar6 != 0) || (iVar2 = **(int **)(param_1 + 8), iVar2 < 0x304)) ||
         ((iVar2 == 0x10000 ||
          (iVar2 = ssl_handshake_hash(param_1,param_1 + 0x8a0,0x40,param_1 + 0x8e0), iVar2 != 0))))
      {
        uVar6 = 2;
      }
    }
    else {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x77e,"tls_post_process_server_certificate");
      ossl_statem_fatal(param_1,0x2f,0x17f,0);
    }
  }
LAB_0016dff3:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar6;
}