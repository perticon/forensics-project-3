int SSL_SESSION_print(BIO *fp,SSL_SESSION *ses)

{
  int iVar1;
  uint uVar2;
  int iVar3;
  undefined8 uVar4;
  char *pcVar5;
  undefined1 *puVar6;
  undefined *puVar7;
  char *format;
  ulong uVar8;
  long in_FS_OFFSET;
  uint *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (ses != (SSL_SESSION *)0x0) {
    iVar3 = ses->ssl_version;
    iVar1 = BIO_puts(fp,"SSL-Session:\n");
    if (0 < iVar1) {
      uVar4 = ssl_protocol_to_string(ses->ssl_version);
      iVar1 = BIO_printf(fp,"    Protocol  : %s\n",uVar4);
      if (0 < iVar1) {
        if (*(long *)(ses[1].krb5_client_princ + 0x10) == 0) {
          uVar2 = (uint)*(ulong *)(ses[1].krb5_client_princ + 0x18);
          if ((uVar2 & 0xff000000) == 0x2000000) {
            pcVar5 = (char *)(ulong)(uVar2 & 0xffffff);
            format = "    Cipher    : %06lX\n";
          }
          else {
            pcVar5 = (char *)(*(ulong *)(ses[1].krb5_client_princ + 0x18) & 0xffff);
            format = "    Cipher    : %04lX\n";
          }
        }
        else {
          pcVar5 = *(char **)(*(long *)(ses[1].krb5_client_princ + 0x10) + 8);
          format = "    Cipher    : %s\n";
          if (pcVar5 == (char *)0x0) {
            pcVar5 = "unknown";
          }
        }
        iVar1 = BIO_printf(fp,format,pcVar5);
        if ((0 < iVar1) && (iVar1 = BIO_puts(fp,"    Session-ID: "), 0 < iVar1)) {
          if (ses->tlsext_tick_lifetime_hint != 0) {
            uVar8 = 0;
            do {
              iVar1 = BIO_printf(fp,"%02X",(ulong)ses[1].key_arg[uVar8 - 8]);
              if (iVar1 < 1) goto LAB_001455b0;
              uVar8 = uVar8 + 1;
            } while (uVar8 < (ulong)ses->tlsext_tick_lifetime_hint);
          }
          iVar1 = BIO_puts(fp,"\n    Session-ID-ctx: ");
          if (0 < iVar1) {
            if (*(long *)(ses[1].master_key + 0xc) != 0) {
              uVar8 = 0;
              do {
                iVar1 = BIO_printf(fp,"%02X",(ulong)ses[1].master_key[uVar8 + 0x14]);
                if (iVar1 < 1) goto LAB_001455b0;
                uVar8 = uVar8 + 1;
              } while (uVar8 < *(ulong *)(ses[1].master_key + 0xc));
            }
            if (iVar3 == 0x304) {
              iVar1 = BIO_puts(fp,"\n    Resumption PSK: ");
            }
            else {
              iVar1 = BIO_puts(fp,"\n    Master-Key: ");
            }
            if (0 < iVar1) {
              uVar8 = 0;
              if (*(long *)ses->key_arg != 0) {
                do {
                  iVar1 = BIO_printf(fp,"%02X",(ulong)ses->session_id[uVar8 + 8]);
                  if (iVar1 < 1) goto LAB_001455b0;
                  uVar8 = uVar8 + 1;
                } while (uVar8 < *(ulong *)ses->key_arg);
              }
              iVar1 = BIO_puts(fp,"\n    PSK identity: ");
              if (0 < iVar1) {
                puVar6 = *(undefined1 **)(ses[1].session_id + 8);
                if (puVar6 == (undefined1 *)0x0) {
                  puVar6 = &DAT_0018337f;
                }
                iVar1 = BIO_printf(fp,"%s",puVar6);
                if ((0 < iVar1) && (iVar1 = BIO_puts(fp,"\n    PSK identity hint: "), 0 < iVar1)) {
                  puVar6 = *(undefined1 **)ses[1].session_id;
                  if (puVar6 == (undefined1 *)0x0) {
                    puVar6 = &DAT_0018337f;
                  }
                  iVar1 = BIO_printf(fp,"%s",puVar6);
                  if ((0 < iVar1) && (iVar1 = BIO_puts(fp,"\n    SRP username: "), 0 < iVar1)) {
                    puVar6 = *(undefined1 **)(ses[1].krb5_client_princ + 0x88);
                    if (puVar6 == (undefined1 *)0x0) {
                      puVar6 = &DAT_0018337f;
                    }
                    iVar1 = BIO_printf(fp,"%s",puVar6);
                    if (((0 < iVar1) &&
                        ((*(long *)(ses[1].krb5_client_princ + 0x60) == 0 ||
                         (iVar1 = BIO_printf(fp,
                                             "\n    TLS session ticket lifetime hint: %ld (seconds)"
                                            ), 0 < iVar1)))) &&
                       ((*(long *)(ses[1].krb5_client_princ + 0x50) == 0 ||
                        ((iVar1 = BIO_puts(fp,"\n    TLS session ticket:\n"), 0 < iVar1 &&
                         (iVar1 = BIO_dump_indent(fp,*(char **)(ses[1].krb5_client_princ + 0x50),
                                                  *(int *)(ses[1].krb5_client_princ + 0x58),4),
                         0 < iVar1)))))) {
                      if (*(int *)(ses[1].krb5_client_princ + 0xc) == 0) {
LAB_00145803:
                        if ((((*(long *)(ses[1].sid_ctx + 0x1c) == 0) ||
                             (iVar1 = BIO_printf(fp,"\n    Start Time: %lld"), 0 < iVar1)) &&
                            ((*(long *)(ses[1].sid_ctx + 0x14) == 0 ||
                             (iVar1 = BIO_printf(fp,"\n    Timeout   : %lld (sec)"), 0 < iVar1))))
                           && ((iVar1 = BIO_puts(fp,"\n"), 0 < iVar1 &&
                               (iVar1 = BIO_puts(fp,"    Verify return code: "), 0 < iVar1)))) {
                          pcVar5 = X509_verify_cert_error_string(*(long *)(ses[1].sid_ctx + 4));
                          iVar1 = BIO_printf(fp,"%ld (%s)\n",*(undefined8 *)(ses[1].sid_ctx + 4),
                                             pcVar5);
                          if (0 < iVar1) {
                            puVar7 = &DAT_00187de4;
                            if ((ses[1].krb5_client_princ[0xa0] & 1) == 0) {
                              puVar7 = &DAT_00187de8;
                            }
                            iVar1 = BIO_printf(fp,"    Extended master secret: %s\n",puVar7);
                            if (0 < iVar1) {
                              uVar2 = 1;
                              if (iVar3 == 0x304) {
                                iVar3 = BIO_printf(fp,"    Max Early Data: %u\n",
                                                   (ulong)*(uint *)(ses[1].krb5_client_princ + 0x6c)
                                                  );
                                uVar2 = (uint)(0 < iVar3);
                              }
                              goto LAB_001455b2;
                            }
                          }
                        }
                      }
                      else {
                        local_38 = (uint *)0x0;
                        iVar1 = ssl_cipher_get_evp(0,ses,0,0,0,0,&local_38);
                        if (iVar1 != 0) {
                          if (local_38 == (uint *)0x0) {
                            iVar1 = BIO_printf(fp,"\n    Compression: %d",
                                               (ulong)*(uint *)(ses[1].krb5_client_princ + 0xc),0);
                          }
                          else {
                            iVar1 = BIO_printf(fp,"\n    Compression: %d (%s)",(ulong)*local_38,
                                               *(undefined8 *)(local_38 + 2));
                          }
                          if (0 < iVar1) goto LAB_00145803;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
LAB_001455b0:
  uVar2 = 0;
LAB_001455b2:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}