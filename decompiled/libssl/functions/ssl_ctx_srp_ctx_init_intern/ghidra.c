undefined8 ssl_ctx_srp_ctx_init_intern(long param_1)

{
  undefined8 uVar1;
  ulong uVar2;
  undefined8 *puVar3;
  
  uVar1 = 0;
  if (param_1 != 0) {
    *(undefined8 *)(param_1 + 0x318) = 0;
    *(undefined8 *)(param_1 + 0x390) = 0;
    puVar3 = (undefined8 *)(param_1 + 800U & 0xfffffffffffffff8);
    uVar2 = (ulong)(((int)param_1 - (int)puVar3) + 0x398U >> 3);
    for (; uVar2 != 0; uVar2 = uVar2 - 1) {
      *puVar3 = 0;
      puVar3 = puVar3 + 1;
    }
    uVar1 = 1;
    *(undefined4 *)(param_1 + 0x388) = 0x400;
  }
  return uVar1;
}