uint SSL_read_early_data(SSL *param_1,undefined8 param_2,undefined8 param_3,undefined8 *param_4)

{
  uint uVar1;
  int iVar2;
  undefined8 uVar3;
  
  uVar1 = param_1->server;
  if (uVar1 == 0) {
    ERR_new();
    uVar3 = 0x769;
LAB_0013b5a1:
    ERR_set_debug("ssl/ssl_lib.c",uVar3,"SSL_read_early_data");
    ERR_set_error(0x14,0xc0101,0);
    return uVar1;
  }
  iVar2 = *(int *)((long)&param_1->s3 + 4);
  if (iVar2 != 8) {
    if (iVar2 == 10) goto LAB_0013b556;
    if (iVar2 != 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x795,"SSL_read_early_data");
      ERR_set_error(0x14,0xc0101,0);
      return 0;
    }
    uVar1 = SSL_in_before();
    if (uVar1 == 0) {
      ERR_new();
      uVar3 = 0x770;
      goto LAB_0013b5a1;
    }
  }
  *(undefined4 *)((long)&param_1->s3 + 4) = 9;
  iVar2 = SSL_accept(param_1);
  if (iVar2 < 1) {
    *(undefined4 *)((long)&param_1->s3 + 4) = 8;
    return 0;
  }
LAB_0013b556:
  if (param_1[4].packet_length == 2) {
    *(undefined4 *)((long)&param_1->s3 + 4) = 0xb;
    iVar2 = SSL_read_ex(param_1,param_2,param_3,param_4);
    if ((0 < iVar2) || (*(int *)((long)&param_1->s3 + 4) != 0xc)) {
      *(undefined4 *)((long)&param_1->s3 + 4) = 10;
      return (uint)(0 < iVar2);
    }
  }
  else {
    *(undefined4 *)((long)&param_1->s3 + 4) = 0xc;
  }
  *param_4 = 0;
  return 2;
}