void ossl_pqueue_free(OSSL_PQUEUE *pq)
{
    if (pq != NULL) {
        OPENSSL_free(pq->heap);
        OPENSSL_free(pq->elements);
        OPENSSL_free(pq);
    }    
}