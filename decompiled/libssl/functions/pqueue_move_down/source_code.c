static ossl_inline void pqueue_move_down(OSSL_PQUEUE *pq, size_t n)
{
    struct pq_heap_st *h = pq->heap;

    ASSERT_USED(pq, n);
    while (n > 0) {
        const size_t p = (n - 1) / 2;

        ASSERT_USED(pq, p);
        if (pq->compare(h[n].data, h[p].data) >= 0)
            break;
        pqueue_swap_elem(pq, n, p);
        n = p;
    }
}