int ssl_set_cert_and_key
              (long param_1,long param_2,X509 *param_3,EVP_PKEY *param_4,long param_5,int param_6)

{
  int iVar1;
  int iVar2;
  int iVar3;
  EVP_PKEY *pkey;
  long lVar4;
  undefined8 uVar5;
  long in_FS_OFFSET;
  EVP_PKEY *local_60;
  long *local_50;
  long local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == 0) {
    local_50 = *(long **)(param_2 + 0x158);
  }
  else {
    local_50 = *(long **)(param_1 + 0x898);
  }
  iVar3 = 0;
  iVar1 = ssl_security_cert(param_1,param_2,param_3,0,1);
  if (iVar1 != 1) {
    ERR_new();
    ERR_set_debug("ssl/ssl_rsa.c",900,"ssl_set_cert_and_key");
    iVar2 = iVar1;
LAB_0013fbe2:
    pkey = (EVP_PKEY *)0x0;
    iVar1 = 0;
    ERR_set_error(0x14,iVar2,0);
    goto LAB_0013fbf4;
  }
  for (; iVar2 = OPENSSL_sk_num(param_5), iVar3 < iVar2; iVar3 = iVar3 + 1) {
    uVar5 = OPENSSL_sk_value(param_5,iVar3);
    iVar2 = ssl_security_cert(param_1,param_2,uVar5,0,0);
    if (iVar2 != 1) {
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0x38a,"ssl_set_cert_and_key");
      goto LAB_0013fbe2;
    }
  }
  pkey = X509_get_pubkey(param_3);
  if (pkey == (EVP_PKEY *)0x0) {
    iVar1 = 0;
    goto LAB_0013fbf4;
  }
  local_60 = pkey;
  if (param_4 != (EVP_PKEY *)0x0) {
    iVar3 = EVP_PKEY_missing_parameters(param_4);
    if (iVar3 == 0) {
      iVar3 = EVP_PKEY_missing_parameters(pkey);
      if ((iVar3 != 0) && (iVar3 = EVP_PKEY_copy_parameters(pkey,param_4), iVar3 == 0)) {
        ERR_new();
        uVar5 = 0x3a5;
LAB_0013fcb3:
        iVar1 = 0;
        ERR_set_debug("ssl/ssl_rsa.c",uVar5,"ssl_set_cert_and_key");
        ERR_set_error(0x14,0x128,0);
        goto LAB_0013fbf4;
      }
    }
    else {
      iVar3 = EVP_PKEY_missing_parameters(pkey);
      if (iVar3 != 0) {
        ERR_new();
        iVar1 = 0;
        ERR_set_debug("ssl/ssl_rsa.c",0x399,"ssl_set_cert_and_key");
        ERR_set_error(0x14,0x122,0);
        goto LAB_0013fbf4;
      }
      iVar3 = EVP_PKEY_copy_parameters(param_4,pkey);
      if (iVar3 == 0) {
        ERR_new();
        uVar5 = 0x39e;
        goto LAB_0013fcb3;
      }
    }
    iVar3 = EVP_PKEY_eq(pkey,param_4);
    local_60 = param_4;
    if (iVar3 != 1) {
      ERR_new();
      iVar1 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",0x3ac,"ssl_set_cert_and_key");
      ERR_set_error(0x14,0x120,0);
      goto LAB_0013fbf4;
    }
  }
  lVar4 = ssl_cert_lookup_by_pkey(pkey,&local_48);
  if (lVar4 == 0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x3b1,"ssl_set_cert_and_key");
    ERR_set_error(0x14,0xf7,0);
  }
  else if ((param_6 == 0) &&
          (((local_50[local_48 * 5 + 4] != 0 || (local_50[local_48 * 5 + 5] != 0)) ||
           (local_50[local_48 * 5 + 6] != 0)))) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x3b9,"ssl_set_cert_and_key");
    ERR_set_error(0x14,0x121,0);
  }
  else if ((param_5 == 0) || (param_5 = X509_chain_up_ref(param_5), param_5 != 0)) {
    OSSL_STACK_OF_X509_free(local_50[local_48 * 5 + 6]);
    local_50[local_48 * 5 + 6] = param_5;
    X509_free((X509 *)local_50[local_48 * 5 + 4]);
    X509_up_ref(param_3);
    local_50[local_48 * 5 + 4] = (long)param_3;
    EVP_PKEY_free((EVP_PKEY *)local_50[local_48 * 5 + 5]);
    EVP_PKEY_up_ref(local_60);
    local_50[local_48 * 5 + 5] = (long)local_60;
    *local_50 = (long)(local_50 + local_48 * 5 + 4);
  }
  else {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x3c0,"ssl_set_cert_and_key");
    ERR_set_error(0x14,0xc0100,0);
  }
LAB_0013fbf4:
  EVP_PKEY_free(pkey);
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}