int64_t ssl_set_cert_and_key(int64_t a1, int32_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6) {
    int64_t v1 = __readfsqword(40); // 0x3fa54
    int64_t v2 = a1 == 0 ? (int64_t)a2 + 344 : a1 + (int64_t)&g297;
    int64_t v3 = *(int64_t *)v2;
    int64_t v4 = ssl_security_cert(); // 0x3fa8d
    int64_t result; // 0x3fa30
    if ((int32_t)v4 == 1) {
        int64_t v5 = 0; // 0x3fad6
        if (function_20630() > 0) {
            function_212d0();
            while ((int32_t)ssl_security_cert() == 1) {
                int64_t v6 = v5 + 1 & 0xffffffff; // 0x3fac7
                v5 = v6;
                if (function_20630() <= v6) {
                    goto lab_0x3fad8;
                }
                function_212d0();
            }
            // 0x3fc28
            function_20230();
            function_207e0();
            // 0x3fbe2
            function_20680();
            result = 0;
            goto lab_0x3fbf4;
        } else {
            goto lab_0x3fad8;
        }
    } else {
        // 0x3fbc0
        function_20230();
        function_207e0();
        // 0x3fbe2
        function_20680();
        result = 0;
        goto lab_0x3fbf4;
    }
  lab_0x3fad8:;
    int64_t v7 = function_21870(); // 0x3fadb
    result = 0;
    int64_t v8; // 0x3fa30
    if (v7 == 0) {
        goto lab_0x3fbf4;
    } else {
        // 0x3faec
        v8 = v7;
        if (a4 == 0) {
            goto lab_0x3fb42;
        } else {
            int64_t v9 = function_21860(); // 0x3fafa
            int64_t v10 = function_21860();
            if ((int32_t)v9 == 0) {
                if ((int32_t)v10 == 0) {
                    goto lab_0x3fb2c;
                } else {
                    // 0x3fc8d
                    if ((int32_t)function_21630() != 0) {
                        goto lab_0x3fb2c;
                    } else {
                        // 0x3fca2
                        function_20230();
                        goto lab_0x3fcb3;
                    }
                }
            } else {
                if ((int32_t)v10 != 0) {
                    // 0x3fdc1
                    function_20230();
                    function_207e0();
                    function_20680();
                    result = 0;
                    goto lab_0x3fbf4;
                } else {
                    // 0x3fb17
                    if ((int32_t)function_21630() == 0) {
                        // 0x3fe31
                        function_20230();
                        goto lab_0x3fcb3;
                    } else {
                        goto lab_0x3fb2c;
                    }
                }
            }
        }
    }
  lab_0x3fbf4:
    // 0x3fbf4
    function_20810();
    if (v1 != __readfsqword(40)) {
        // 0x3fe7f
        return function_20ff0();
    }
    // 0x3fc10
    return result;
  lab_0x3fb42:;
    // 0x3fb42
    int64_t v11; // 0x3fa30
    if (ssl_cert_lookup_by_pkey() == 0) {
        // 0x3fdf9
        function_20230();
        function_207e0();
        function_20680();
        result = 0;
        goto lab_0x3fbf4;
    } else {
        // 0x3fb58
        if ((int32_t)a6 != 0) {
            goto lab_0x3fce8;
        } else {
            int64_t v12 = v3 + 40 * v11; // 0x3fb72
            if (*(int64_t *)(v12 + 32) != 0) {
                goto lab_0x3fb88;
            } else {
                // 0x3fb7d
                if (*(int64_t *)(v12 + 40) == 0) {
                    int64_t v13 = *(int64_t *)(v12 + 48); // 0x3fcda
                    if (v13 != 0) {
                        goto lab_0x3fb88;
                    } else {
                        goto lab_0x3fce8;
                    }
                } else {
                    goto lab_0x3fb88;
                }
            }
        }
    }
  lab_0x3fce8:;
    int64_t v14 = 0; // 0x3fceb
    if (a5 == 0) {
        goto lab_0x3fd01;
    } else {
        int64_t v15 = function_21c00(); // 0x3fcf0
        v14 = v15;
        if (v15 == 0) {
            // 0x3fe47
            function_20230();
            function_207e0();
            function_20680();
            result = 0;
            goto lab_0x3fbf4;
        } else {
            goto lab_0x3fd01;
        }
    }
  lab_0x3fb2c:
    // 0x3fb2c
    v8 = a4;
    if ((int32_t)function_20b60() != 1) {
        // 0x3fd89
        function_20230();
        function_207e0();
        function_20680();
        result = 0;
        goto lab_0x3fbf4;
    } else {
        goto lab_0x3fb42;
    }
  lab_0x3fd01:
    // 0x3fd01
    function_21a50();
    int64_t v16 = v3 + 40 * v11;
    *(int64_t *)(v16 + 48) = v14;
    function_20690();
    function_20dc0();
    int64_t v17 = v16 + 32;
    *(int64_t *)v17 = a3;
    function_20810();
    function_21ba0();
    *(int64_t *)(v16 + 40) = v8;
    *(int64_t *)v3 = v17;
    result = v4 & 0xffffffff;
    goto lab_0x3fbf4;
  lab_0x3fb88:
    // 0x3fb88
    function_20230();
    function_207e0();
    function_20680();
    result = 0;
    goto lab_0x3fbf4;
  lab_0x3fcb3:
    // 0x3fcb3
    function_207e0();
    function_20680();
    result = 0;
    goto lab_0x3fbf4;
}