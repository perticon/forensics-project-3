void BIO_ssl_shutdown(BIO *ssl_bio)

{
  int iVar1;
  SSL **ppSVar2;
  
  if (ssl_bio == (BIO *)0x0) {
    return;
  }
  do {
    iVar1 = BIO_method_type(ssl_bio);
    if (((iVar1 == 0x207) && (ppSVar2 = (SSL **)BIO_get_data(ssl_bio), ppSVar2 != (SSL **)0x0)) &&
       (*ppSVar2 != (SSL *)0x0)) {
      SSL_shutdown(*ppSVar2);
      ssl_bio = BIO_next(ssl_bio);
    }
    else {
      ssl_bio = BIO_next(ssl_bio);
    }
  } while (ssl_bio != (BIO *)0x0);
  return;
}