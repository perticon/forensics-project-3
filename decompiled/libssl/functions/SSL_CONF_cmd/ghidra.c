undefined8 SSL_CONF_cmd(uint *param_1,char *param_2,long param_3)

{
  char *__s2;
  ulong *puVar1;
  int iVar2;
  uint uVar3;
  size_t sVar4;
  code **ppcVar5;
  uint *puVar6;
  long lVar7;
  ulong uVar8;
  
  if (param_2 == (char *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_conf.c",0x37a,"SSL_CONF_cmd");
    ERR_set_error(0x14,0x181,0);
    return 0;
  }
  __s2 = *(char **)(param_1 + 2);
  if (__s2 == (char *)0x0) {
    if ((*(byte *)param_1 & 1) != 0) {
      if (*param_2 != '-') {
        return 0xfffffffe;
      }
      if (param_2[1] == '\0') {
        return 0xfffffffe;
      }
      param_2 = param_2 + 1;
    }
  }
  else {
    sVar4 = strlen(param_2);
    uVar8 = *(ulong *)(param_1 + 4);
    if (sVar4 <= uVar8) {
      return 0xfffffffe;
    }
    uVar3 = *param_1;
    if (((uVar3 & 1) != 0) && (iVar2 = strncmp(param_2,__s2,uVar8), iVar2 != 0)) {
      return 0xfffffffe;
    }
    if ((uVar3 & 2) != 0) {
      iVar2 = OPENSSL_strncasecmp(param_2,__s2,uVar8);
      if (iVar2 != 0) {
        return 0xfffffffe;
      }
      uVar8 = *(ulong *)(param_1 + 4);
    }
    param_2 = param_2 + uVar8;
  }
  ppcVar5 = (code **)ssl_conf_cmd_lookup_part_0(param_1,param_2);
  if (ppcVar5 == (code **)0x0) {
    if ((*(byte *)param_1 & 0x10) != 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_conf.c",0x396,"SSL_CONF_cmd");
      ERR_set_error(0x14,0x182,"cmd=%s",param_2);
    }
    return 0xfffffffe;
  }
  if (*(short *)((long)ppcVar5 + 0x1a) == 4) {
    if (ppcVar5 + -0x34330 < (code **)0x320) {
      puVar1 = *(ulong **)(param_1 + 10);
      lVar7 = ((long)(ppcVar5 + -0x34330) >> 5) * 0x10;
      if (puVar1 == (ulong *)0x0) {
        return 1;
      }
      uVar8 = *(ulong *)(ssl_cmd_switches + lVar7);
      uVar3 = *(uint *)(ssl_cmd_switches + lVar7 + 8) & 0xf00;
      if ((*(uint *)(ssl_cmd_switches + lVar7 + 8) & 1) != 0) {
        if (uVar3 == 0x100) {
          puVar6 = *(uint **)(param_1 + 0x1e);
        }
        else {
          if (uVar3 != 0x200) {
            if (uVar3 != 0) {
              return 1;
            }
            *puVar1 = *puVar1 & ~uVar8;
            return 1;
          }
          puVar6 = *(uint **)(param_1 + 0x20);
        }
        *puVar6 = *puVar6 & ~(uint)uVar8;
        return 1;
      }
      if (uVar3 == 0x100) {
        puVar6 = *(uint **)(param_1 + 0x1e);
      }
      else {
        if (uVar3 != 0x200) {
          if (uVar3 != 0) {
            return 1;
          }
          *puVar1 = *puVar1 | uVar8;
          return 1;
        }
        puVar6 = *(uint **)(param_1 + 0x20);
      }
      *puVar6 = *puVar6 | (uint)uVar8;
      return 1;
    }
  }
  else {
    if (param_3 == 0) {
      return 0xfffffffd;
    }
    iVar2 = (**ppcVar5)(param_1,param_3);
    if (0 < iVar2) {
      return 2;
    }
    if (iVar2 == -2) {
      return 0xfffffffe;
    }
    if ((*(byte *)param_1 & 0x10) != 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_conf.c",0x390,"SSL_CONF_cmd");
      ERR_set_error(0x14,0x180,"cmd=%s, value=%s",param_2,param_3);
    }
  }
  return 0;
}