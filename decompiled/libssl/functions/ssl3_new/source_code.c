int ssl3_new(SSL *s)
{
#ifndef OPENSSL_NO_SRP
    if (!ssl_srp_ctx_init_intern(s))
        return 0;
#endif

    if (!s->method->ssl_clear(s))
        return 0;

    return 1;
}