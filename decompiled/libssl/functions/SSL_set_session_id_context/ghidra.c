int SSL_set_session_id_context(SSL *ssl,uchar *sid_ctx,uint sid_ctx_len)

{
  COMP_CTX *pCVar1;
  uint uVar2;
  long lVar3;
  ulong uVar4;
  uint uVar5;
  ulong uVar6;
  
  if (sid_ctx_len < 0x21) {
    pCVar1 = (COMP_CTX *)(ulong)sid_ctx_len;
    ssl[3].expand = pCVar1;
    if (sid_ctx_len < 8) {
      if ((sid_ctx_len & 4) == 0) {
        if ((sid_ctx_len != 0) &&
           (*(uchar *)&ssl[3].enc_write_ctx = *sid_ctx, (sid_ctx_len & 2) != 0)) {
          *(undefined2 *)((long)&ssl[3].expand + (long)((long)&pCVar1->meth + 6)) =
               *(undefined2 *)(sid_ctx + -2 + (long)pCVar1);
        }
      }
      else {
        *(undefined4 *)&ssl[3].enc_write_ctx = *(undefined4 *)sid_ctx;
        *(undefined4 *)((long)&ssl[3].expand + (long)((long)&pCVar1->meth + 4)) =
             *(undefined4 *)(sid_ctx + -4 + (long)pCVar1);
      }
    }
    else {
      ssl[3].enc_write_ctx = *(EVP_CIPHER_CTX **)sid_ctx;
      uVar6 = (ulong)&ssl[3].write_hash & 0xfffffffffffffff8;
      *(undefined8 *)((long)&ssl[3].expand + (long)pCVar1) =
           *(undefined8 *)(sid_ctx + -8 + (long)pCVar1);
      lVar3 = (long)ssl + (0x8f8 - uVar6);
      uVar5 = sid_ctx_len + (int)lVar3 & 0xfffffff8;
      if (7 < uVar5) {
        uVar2 = 0;
        do {
          uVar4 = (ulong)uVar2;
          uVar2 = uVar2 + 8;
          *(undefined8 *)(uVar6 + uVar4) = *(undefined8 *)(sid_ctx + (uVar4 - lVar3));
        } while (uVar2 < uVar5);
      }
    }
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x37b,"SSL_set_session_id_context");
  ERR_set_error(0x14,0x111,0);
  return 0;
}