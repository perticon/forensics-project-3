undefined8 ossl_statem_server_pre_work(long param_1,undefined8 param_2)

{
  long lVar1;
  long lVar2;
  int iVar3;
  undefined8 uVar4;
  
  if (0x2e < *(uint *)(param_1 + 0x5c)) {
    return 2;
  }
  switch(*(undefined4 *)(param_1 + 0x5c)) {
  case 1:
switchD_0017b67f_caseD_1:
    uVar4 = tls_finish_handshake(param_1,param_2,1,1);
    return uVar4;
  case 0x13:
    *(undefined4 *)(param_1 + 0x44) = 0;
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0) {
      dtls1_clear_sent_buffer();
      return 2;
    }
    break;
  case 0x15:
    *(undefined4 *)(param_1 + 0x44) = 0;
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0) {
      dtls1_clear_sent_buffer();
      *(undefined4 *)(param_1 + 0x78) = 0;
      return 2;
    }
    break;
  case 0x16:
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0) {
      *(undefined4 *)(param_1 + 0x78) = 1;
      return 2;
    }
    break;
  case 0x21:
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) {
LAB_0017b803:
      *(undefined4 *)(param_1 + 0x78) = 0;
      return 2;
    }
    iVar3 = **(int **)(param_1 + 8);
    if ((((0x303 < iVar3) && (iVar3 != 0x10000)) && (*(long *)(param_1 + 0x1d78) == 0)) &&
       (*(int *)(param_1 + 0xa9c) == 0)) {
      uVar4 = tls_finish_handshake(param_1,param_2,0,0);
      return uVar4;
    }
    break;
  case 0x23:
    lVar1 = *(long *)(*(int **)(param_1 + 8) + 0x30);
    if (((*(byte *)(lVar1 + 0x60) & 8) != 0) ||
       ((iVar3 = **(int **)(param_1 + 8), iVar3 < 0x304 || (iVar3 == 0x10000)))) {
      lVar2 = *(long *)(*(long *)(param_1 + 0x918) + 0x2f8);
      if (lVar2 == 0) {
        *(long *)(*(long *)(param_1 + 0x918) + 0x2f8) = *(long *)(param_1 + 0x2e0);
      }
      else if (lVar2 != *(long *)(param_1 + 0x2e0)) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0x2f1,"ossl_statem_server_pre_work");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        return 0;
      }
      iVar3 = (**(code **)(lVar1 + 0x10))(param_1);
      if (iVar3 == 0) {
        return 0;
      }
      if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) != 0) goto LAB_0017b803;
    }
    break;
  case 0x2e:
    if ((*(int *)(param_1 + 0x84) == 9) || ((*(byte *)(param_1 + 0xa9) & 8) != 0))
    goto switchD_0017b67f_caseD_1;
  }
  return 2;
}