int SSL_SESSION_print_fp(FILE *fp,SSL_SESSION *ses)

{
  int iVar1;
  BIO_METHOD *type;
  BIO *bp;
  
  type = BIO_s_file();
  bp = BIO_new(type);
  if (bp != (BIO *)0x0) {
    BIO_ctrl(bp,0x6a,0,fp);
    iVar1 = SSL_SESSION_print(bp,ses);
    BIO_free(bp);
    return iVar1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_txt.c",0x16,"SSL_SESSION_print_fp");
  ERR_set_error(0x14,0x80007,0);
  return 0;
}