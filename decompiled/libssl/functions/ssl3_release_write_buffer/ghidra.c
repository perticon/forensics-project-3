undefined8 ssl3_release_write_buffer(long param_1)

{
  void **ppvVar1;
  void **ppvVar2;
  
  if (*(long *)(param_1 + 0xc70) != 0) {
    ppvVar1 = (void **)(param_1 + 0xc78 + *(long *)(param_1 + 0xc70) * 0x30);
    do {
      while (*(int *)(ppvVar1 + 5) == 0) {
        ppvVar2 = ppvVar1 + -6;
        CRYPTO_free(*ppvVar1);
        *ppvVar1 = (void *)0x0;
        ppvVar1 = ppvVar2;
        if (ppvVar2 == (void **)(param_1 + 0xc78)) goto LAB_0015966f;
      }
      *(undefined4 *)(ppvVar1 + 5) = 0;
      ppvVar2 = ppvVar1 + -6;
      *ppvVar1 = (void *)0x0;
      ppvVar1 = ppvVar2;
    } while ((void **)(param_1 + 0xc78) != ppvVar2);
  }
LAB_0015966f:
  *(undefined8 *)(param_1 + 0xc70) = 0;
  return 1;
}