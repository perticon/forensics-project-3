EVP_PKEY * ssl_generate_param_group(long param_1,undefined2 param_2)

{
  int iVar1;
  long lVar2;
  EVP_PKEY_CTX *ctx;
  long in_FS_OFFSET;
  EVP_PKEY *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_28 = (EVP_PKEY *)0x0;
  lVar2 = tls1_group_id_lookup(*(undefined8 *)(param_1 + 0x9a8),param_2);
  if (lVar2 != 0) {
    ctx = (EVP_PKEY_CTX *)
          EVP_PKEY_CTX_new_from_name
                    (**(undefined8 **)(param_1 + 0x9a8),*(undefined8 *)(lVar2 + 0x10),
                     (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
    if (ctx != (EVP_PKEY_CTX *)0x0) {
      iVar1 = EVP_PKEY_paramgen_init(ctx);
      if (0 < iVar1) {
        iVar1 = EVP_PKEY_CTX_set_group_name(ctx,*(undefined8 *)(lVar2 + 8));
        if (iVar1 < 1) {
          ERR_new();
          ERR_set_debug("ssl/s3_lib.c",0x1296,"ssl_generate_param_group");
          ossl_statem_fatal(param_1,0x50,0x80006,0);
        }
        else {
          iVar1 = EVP_PKEY_paramgen(ctx,&local_28);
          if (iVar1 < 1) {
            EVP_PKEY_free(local_28);
            local_28 = (EVP_PKEY *)0x0;
          }
        }
      }
      goto LAB_0012b5c2;
    }
  }
  ctx = (EVP_PKEY_CTX *)0x0;
LAB_0012b5c2:
  EVP_PKEY_CTX_free(ctx);
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return local_28;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}