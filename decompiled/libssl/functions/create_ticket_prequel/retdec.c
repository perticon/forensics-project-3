int64_t create_ticket_prequel(int64_t a1, int64_t a2, int32_t a3, int64_t a4) {
    int64_t v1 = WPACKET_put_bytes__(); // 0x78ad2
    if ((int32_t)v1 == 0) {
        // 0x78b30
        function_20230();
        // 0x78b41
        function_207e0();
        ossl_statem_fatal();
        return v1 & 0xffffffff;
    }
    int64_t v2 = *(int64_t *)(a1 + 8); // 0x78ade
    if ((*(char *)(*(int64_t *)(v2 + 192) + 96) & 8) != 0) {
        goto lab_0x78b03;
    } else {
        int32_t v3 = *(int32_t *)v2; // 0x78aef
        if (v3 > (int32_t)&g90 == (v3 != (int32_t)&g2)) {
            // 0x78b88
            if ((int32_t)WPACKET_put_bytes__() == 0) {
                goto lab_0x78bb9;
            } else {
                // 0x78b9c
                if ((int32_t)WPACKET_sub_memcpy__() != 0) {
                    goto lab_0x78b03;
                } else {
                    goto lab_0x78bb9;
                }
            }
        } else {
            goto lab_0x78b03;
        }
    }
  lab_0x78b03:;
    int64_t v4 = WPACKET_start_sub_packet_len__(); // 0x78b0b
    if ((int32_t)v4 != 0) {
        // 0x78b21
        return 1;
    }
    // 0x78bf8
    function_20230();
    // 0x78b41
    function_207e0();
    ossl_statem_fatal();
    return v4 & 0xffffffff;
  lab_0x78bb9:
    // 0x78bb9
    function_20230();
    function_207e0();
    ossl_statem_fatal();
    // 0x78b21
    return 0;
}