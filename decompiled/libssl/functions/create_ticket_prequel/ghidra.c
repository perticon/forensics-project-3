int create_ticket_prequel(long param_1,undefined8 param_2,undefined4 param_3,undefined8 param_4)

{
  ulong uVar1;
  int iVar2;
  ulong uVar3;
  undefined8 uVar4;
  
  uVar1 = *(ulong *)(*(long *)(param_1 + 0x918) + 0x2d8);
  uVar3 = uVar1 & 0xffffffff;
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
      (iVar2 = **(int **)(param_1 + 8), 0x303 < iVar2)) && (iVar2 != 0x10000)) {
    if (0x93a80 < (long)uVar1) {
      uVar3 = 0x93a80;
    }
  }
  else if (*(int *)(param_1 + 0x4d0) != 0) {
    uVar3 = 0;
  }
  iVar2 = WPACKET_put_bytes__(param_2,uVar3,4);
  if (iVar2 != 0) {
    if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
        (iVar2 = **(int **)(param_1 + 8), 0x303 < iVar2)) &&
       ((iVar2 != 0x10000 &&
        ((iVar2 = WPACKET_put_bytes__(param_2,param_3,4), iVar2 == 0 ||
         (iVar2 = WPACKET_sub_memcpy__(param_2,param_4,8,1), iVar2 == 0)))))) {
      ERR_new();
      iVar2 = 0;
      ERR_set_debug("ssl/statem/statem_srvr.c",0xe41,"create_ticket_prequel");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
    else {
      iVar2 = WPACKET_start_sub_packet_len__(param_2,2);
      if (iVar2 == 0) {
        ERR_new();
        uVar4 = 0xe48;
        goto LAB_00178b41;
      }
      iVar2 = 1;
    }
    return iVar2;
  }
  ERR_new();
  uVar4 = 0xe3a;
LAB_00178b41:
  ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,"create_ticket_prequel");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return iVar2;
}