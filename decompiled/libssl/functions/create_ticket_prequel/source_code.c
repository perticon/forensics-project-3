static int create_ticket_prequel(SSL *s, WPACKET *pkt, uint32_t age_add,
                                 unsigned char *tick_nonce)
{
    uint32_t timeout = (uint32_t)s->session->timeout;

    /*
     * Ticket lifetime hint:
     * In TLSv1.3 we reset the "time" field above, and always specify the
     * timeout, limited to a 1 week period per RFC8446.
     * For TLSv1.2 this is advisory only and we leave this unspecified for
     * resumed session (for simplicity).
     */
#define ONE_WEEK_SEC (7 * 24 * 60 * 60)

    if (SSL_IS_TLS13(s)) {
        if (s->session->timeout > ONE_WEEK_SEC)
            timeout = ONE_WEEK_SEC;
    } else if (s->hit)
        timeout = 0;

    if (!WPACKET_put_bytes_u32(pkt, timeout)) {
        SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
        return 0;
    }

    if (SSL_IS_TLS13(s)) {
        if (!WPACKET_put_bytes_u32(pkt, age_add)
                || !WPACKET_sub_memcpy_u8(pkt, tick_nonce, TICKET_NONCE_SIZE)) {
            SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
            return 0;
        }
    }

    /* Start the sub-packet for the actual ticket data */
    if (!WPACKET_start_sub_packet_u16(pkt)) {
        SSLfatal(s, SSL_AD_INTERNAL_ERROR, ERR_R_INTERNAL_ERROR);
        return 0;
    }

    return 1;
}