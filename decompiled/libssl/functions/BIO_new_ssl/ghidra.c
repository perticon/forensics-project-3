BIO * BIO_new_ssl(SSL_CTX *ctx,int client)

{
  BIO_METHOD *type;
  BIO *a;
  SSL *s;
  BIO *pBVar1;
  
  type = BIO_f_ssl();
  a = BIO_new(type);
  pBVar1 = a;
  if (a != (BIO *)0x0) {
    s = SSL_new(ctx);
    if (s == (SSL *)0x0) {
      pBVar1 = (BIO *)0x0;
      BIO_free(a);
    }
    else {
      if (client == 0) {
        SSL_set_accept_state(s);
      }
      else {
        SSL_set_connect_state(s);
      }
      BIO_ctrl(a,0x6d,1,s);
    }
  }
  return pBVar1;
}