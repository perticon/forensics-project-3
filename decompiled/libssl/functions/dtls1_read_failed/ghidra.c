ulong dtls1_read_failed(SSL *param_1,uint param_2)

{
  int iVar1;
  ulong uVar2;
  BIO *b;
  
  if (0 < (int)param_2) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_dtls.c",0x3d3,"dtls1_read_failed");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  }
  iVar1 = dtls1_is_timer_expired();
  if (iVar1 != 0) {
    iVar1 = ossl_statem_in_error(param_1);
    if (iVar1 == 0) {
      iVar1 = SSL_in_init(param_1);
      if (iVar1 != 0) {
        uVar2 = dtls1_handle_timeout();
        return uVar2;
      }
      b = SSL_get_rbio(param_1);
      BIO_set_flags(b,1);
    }
  }
  return (ulong)param_2;
}