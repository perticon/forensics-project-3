undefined8 ssl_hmac_size(long *param_1)

{
  undefined8 uVar1;
  
  if (*param_1 != 0) {
    uVar1 = (*(code *)PTR_EVP_MAC_CTX_get_mac_size_001a8e28)(*param_1);
    return uVar1;
  }
  if (param_1[1] == 0) {
    return 0;
  }
  uVar1 = ssl_hmac_old_size();
  return uVar1;
}