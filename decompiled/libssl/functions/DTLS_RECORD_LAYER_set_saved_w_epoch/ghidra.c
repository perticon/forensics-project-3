void DTLS_RECORD_LAYER_set_saved_w_epoch(long param_1,ushort param_2)

{
  long lVar1;
  
  lVar1 = *(long *)(param_1 + 0x10b8);
  if ((uint)param_2 == *(ushort *)(lVar1 + 2) - 1) {
    *(undefined8 *)(lVar1 + 0x60) = *(undefined8 *)(param_1 + 0x10a8);
    *(undefined8 *)(param_1 + 0x10a8) = *(undefined8 *)(*(long *)(param_1 + 0x10b8) + 0x58);
    *(ushort *)(*(long *)(param_1 + 0x10b8) + 2) = param_2;
    return;
  }
  if ((uint)param_2 != *(ushort *)(lVar1 + 2) + 1) {
    *(ushort *)(lVar1 + 2) = param_2;
    return;
  }
  *(undefined8 *)(lVar1 + 0x58) = *(undefined8 *)(param_1 + 0x10a8);
  *(undefined8 *)(param_1 + 0x10a8) = *(undefined8 *)(*(long *)(param_1 + 0x10b8) + 0x60);
  *(ushort *)(*(long *)(param_1 + 0x10b8) + 2) = param_2;
  return;
}