SSL_get0_dane_tlsa(long param_1,undefined *param_2,undefined *param_3,undefined *param_4,
                  undefined8 *param_5,undefined8 *param_6)

{
  int iVar1;
  undefined4 uVar2;
  
  iVar1 = OPENSSL_sk_num(*(undefined8 *)(param_1 + 0x4e8));
  if ((iVar1 < 1) || (*(long *)(param_1 + 0x9b8) != 0)) {
    uVar2 = 0xffffffff;
  }
  else {
    if (*(undefined **)(param_1 + 0x4f8) != (undefined *)0x0) {
      if (param_2 != (undefined *)0x0) {
        *param_2 = **(undefined **)(param_1 + 0x4f8);
      }
      if (param_3 != (undefined *)0x0) {
        *param_3 = *(undefined *)(*(long *)(param_1 + 0x4f8) + 1);
      }
      if (param_4 != (undefined *)0x0) {
        *param_4 = *(undefined *)(*(long *)(param_1 + 0x4f8) + 2);
      }
      if (param_5 != (undefined8 *)0x0) {
        *param_5 = *(undefined8 *)(*(long *)(param_1 + 0x4f8) + 8);
      }
      if (param_6 != (undefined8 *)0x0) {
        *param_6 = *(undefined8 *)(*(long *)(param_1 + 0x4f8) + 0x10);
      }
    }
    uVar2 = *(undefined4 *)(param_1 + 0x50c);
  }
  return uVar2;
}