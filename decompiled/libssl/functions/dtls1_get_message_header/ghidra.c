void dtls1_get_message_header(undefined *param_1,undefined8 *param_2)

{
  ulong uVar1;
  undefined8 *puVar2;
  
  *param_2 = 0;
  param_2[10] = 0;
  puVar2 = (undefined8 *)((ulong)(param_2 + 1) & 0xfffffffffffffff8);
  for (uVar1 = (ulong)(((int)param_2 -
                       (int)(undefined8 *)((ulong)(param_2 + 1) & 0xfffffffffffffff8)) + 0x58U >> 3)
      ; uVar1 != 0; uVar1 = uVar1 - 1) {
    *puVar2 = 0;
    puVar2 = puVar2 + 1;
  }
  *(undefined *)param_2 = *param_1;
  param_2[1] = (ulong)(byte)param_1[1] << 0x10 | (ulong)(byte)param_1[2] << 8 |
               (ulong)(byte)param_1[3];
  *(ushort *)(param_2 + 2) = *(ushort *)(param_1 + 4) << 8 | *(ushort *)(param_1 + 4) >> 8;
  param_2[3] = (ulong)(byte)param_1[6] << 0x10 | (ulong)(byte)param_1[7] << 8 |
               (ulong)(byte)param_1[8];
  param_2[4] = (ulong)(byte)param_1[9] << 0x10 | (ulong)(byte)param_1[10] << 8 |
               (ulong)(byte)param_1[0xb];
  return;
}