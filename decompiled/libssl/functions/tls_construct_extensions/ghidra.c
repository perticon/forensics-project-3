int tls_construct_extensions
              (long param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5)

{
  byte *pbVar1;
  int iVar2;
  code *pcVar3;
  long lVar4;
  undefined1 *puVar5;
  long in_FS_OFFSET;
  undefined local_48 [4];
  undefined4 local_44;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_44 = 0;
  iVar2 = WPACKET_start_sub_packet_len__(param_2,2);
  if ((iVar2 == 0) ||
     (((param_3 & 0x180) != 0 && (iVar2 = WPACKET_set_flags(param_2,2), iVar2 == 0)))) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions.c",0x324,"tls_construct_extensions");
    iVar2 = 0xc0103;
LAB_0015f761:
    ossl_statem_fatal(param_1,0x50,iVar2,0);
  }
  else {
    if ((param_3 & 0x80) != 0) {
      iVar2 = ssl_get_min_max_version(param_1,local_48,&local_44,0);
      if (iVar2 != 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions.c",0x32b,"tls_construct_extensions");
        goto LAB_0015f761;
      }
      custom_ext_init(*(long *)(param_1 + 0x898) + 0x1d8);
    }
    iVar2 = custom_ext_add(param_1,param_3,param_2,param_4,param_5,local_44);
    if (iVar2 != 0) {
      puVar5 = ext_defs;
      if ((param_3 & 0x6080) == 0) {
        do {
          iVar2 = should_add_extension(param_1,*(undefined4 *)(puVar5 + 4),param_3);
          if (iVar2 != 0) {
            pcVar3 = *(code **)(puVar5 + 0x28);
            if (*(int *)(param_1 + 0x38) != 0) {
              pcVar3 = *(code **)(puVar5 + 0x20);
            }
            if ((pcVar3 != (code *)0x0) &&
               (iVar2 = (*pcVar3)(param_1,param_2,param_3,param_4,param_5), iVar2 == 0))
            goto LAB_0015f770;
          }
          puVar5 = puVar5 + 0x38;
        } while (puVar5 != &DAT_001a7bf0);
      }
      else {
        lVar4 = 0;
        do {
          iVar2 = should_add_extension(param_1,*(undefined4 *)(puVar5 + 4),param_3,local_44);
          if (iVar2 != 0) {
            pcVar3 = *(code **)(puVar5 + 0x28);
            if (*(int *)(param_1 + 0x38) != 0) {
              pcVar3 = *(code **)(puVar5 + 0x20);
            }
            if (pcVar3 != (code *)0x0) {
              iVar2 = (*pcVar3)(param_1,param_2,param_3,param_4,param_5);
              if (iVar2 == 0) goto LAB_0015f770;
              if (iVar2 == 1) {
                pbVar1 = (byte *)(param_1 + 0xa28 + lVar4);
                *pbVar1 = *pbVar1 | 2;
              }
            }
          }
          puVar5 = puVar5 + 0x38;
          lVar4 = lVar4 + 1;
        } while (puVar5 != &DAT_001a7bf0);
      }
      iVar2 = WPACKET_close(param_2);
      if (iVar2 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions.c",0x356,"tls_construct_extensions");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
      }
      else {
        iVar2 = 1;
      }
      goto LAB_0015f773;
    }
  }
LAB_0015f770:
  iVar2 = 0;
LAB_0015f773:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar2;
}