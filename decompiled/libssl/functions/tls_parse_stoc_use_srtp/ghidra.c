undefined8 tls_parse_stoc_use_srtp(SSL *param_1,ushort **param_2)

{
  char cVar1;
  ushort uVar2;
  ushort *puVar3;
  ushort *puVar4;
  int iVar5;
  stack_st_SRTP_PROTECTION_PROFILE *psVar6;
  EVP_MD_CTX *pEVar7;
  int iVar8;
  undefined8 uVar9;
  
  puVar3 = param_2[1];
  if (puVar3 < (ushort *)0x2) {
LAB_00163bc0:
    ERR_new();
    uVar9 = 0x677;
  }
  else {
    puVar4 = *param_2;
    uVar2 = *puVar4;
    *param_2 = puVar4 + 1;
    param_2[1] = puVar3 + -1;
    if ((puVar3 + -1 < (ushort *)0x2) || ((ushort)(uVar2 << 8 | uVar2 >> 8) != 2))
    goto LAB_00163bc0;
    uVar2 = puVar4[1];
    *param_2 = puVar4 + 2;
    param_2[1] = puVar3 + -2;
    if (puVar3 + -2 == (ushort *)0x0) goto LAB_00163bc0;
    cVar1 = *(char *)(puVar4 + 2);
    *param_2 = (ushort *)((long)puVar4 + 5);
    param_2[1] = (ushort *)((long)puVar3 + -5);
    if ((ushort *)((long)puVar3 + -5) != (ushort *)0x0) goto LAB_00163bc0;
    if (cVar1 != '\0') {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x67e,"tls_parse_stoc_use_srtp");
      ossl_statem_fatal(param_1,0x2f,0x160,0);
      return 0;
    }
    psVar6 = SSL_get_srtp_profiles(param_1);
    if (psVar6 == (stack_st_SRTP_PROTECTION_PROFILE *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x685,"tls_parse_stoc_use_srtp");
      uVar9 = 0x167;
      goto LAB_00163be4;
    }
    for (iVar8 = 0; iVar5 = OPENSSL_sk_num(psVar6), iVar8 < iVar5; iVar8 = iVar8 + 1) {
      pEVar7 = (EVP_MD_CTX *)OPENSSL_sk_value(psVar6,iVar8);
      if (pEVar7->engine == (ENGINE *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8)) {
        param_1[4].read_hash = pEVar7;
        return 1;
      }
    }
    ERR_new();
    uVar9 = 0x696;
  }
  ERR_set_debug("ssl/statem/extensions_clnt.c",uVar9,"tls_parse_stoc_use_srtp");
  uVar9 = 0x161;
LAB_00163be4:
  ossl_statem_fatal(param_1,0x32,uVar9,0);
  return 0;
}