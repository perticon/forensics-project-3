void ssl3_digest_master_key_set_params(long param_1,undefined8 *param_2)

{
  long in_FS_OFFSET;
  undefined8 local_58;
  undefined8 uStack80;
  undefined8 local_48;
  undefined8 uStack64;
  undefined8 local_38;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  OSSL_PARAM_construct_octet_string(&local_58,"ssl3-ms",param_1 + 0x50,*(undefined8 *)(param_1 + 8))
  ;
  param_2[4] = local_38;
  *param_2 = local_58;
  param_2[1] = uStack80;
  param_2[2] = local_48;
  param_2[3] = uStack64;
  OSSL_PARAM_construct_end(local_58,local_48,&local_58);
  param_2[9] = local_38;
  param_2[5] = local_58;
  param_2[6] = uStack80;
  param_2[7] = local_48;
  param_2[8] = uStack64;
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}