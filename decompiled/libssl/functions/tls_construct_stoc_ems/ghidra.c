undefined8 tls_construct_stoc_ems(long param_1,undefined8 param_2)

{
  int iVar1;
  
  if ((*(byte *)(param_1 + 0xa9) & 2) == 0) {
    return 2;
  }
  iVar1 = WPACKET_put_bytes__(param_2,0x17,2);
  if (iVar1 != 0) {
    iVar1 = WPACKET_put_bytes__(param_2,0,2);
    if (iVar1 != 0) {
      return 1;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x610,"tls_construct_stoc_ems");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}