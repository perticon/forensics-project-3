uint tls_process_cert_verify(int *param_1,ushort **param_2)

{
  ushort uVar1;
  int iVar2;
  EVP_MD_CTX *ctx;
  ushort *out;
  long lVar3;
  ushort *puVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  uint uVar7;
  ushort *siz;
  ushort *in;
  long in_FS_OFFSET;
  long local_108;
  undefined8 local_100;
  undefined8 local_f8;
  undefined8 local_f0;
  undefined local_e8 [168];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_108 = 0;
  local_100 = 0;
  ctx = (EVP_MD_CTX *)EVP_MD_CTX_new();
  local_f0 = 0;
  if (ctx == (EVP_MD_CTX *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x1a0,"tls_process_cert_verify");
    uVar5 = 0xc0100;
    uVar6 = 0x50;
LAB_00174c69:
    out = (ushort *)0x0;
    uVar7 = 0;
    ossl_statem_fatal(param_1,uVar6,uVar5,0);
    goto LAB_00174ac8;
  }
  out = (ushort *)X509_get0_pubkey(*(undefined8 *)(*(long *)(param_1 + 0x246) + 0x2b8));
  if (out == (ushort *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x1a7,"tls_process_cert_verify");
    uVar5 = 0xc0103;
LAB_00174ca4:
    uVar7 = 0;
    ossl_statem_fatal(param_1,0x50,uVar5,0);
    goto LAB_00174ac8;
  }
  lVar3 = ssl_cert_lookup_by_pkey(out,0);
  if (lVar3 == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_lib.c",0x1ac,"tls_process_cert_verify");
    uVar5 = 0xdc;
    uVar6 = 0x2f;
    goto LAB_00174c69;
  }
  uVar7 = *(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 2;
  if (uVar7 == 0) {
    iVar2 = tls1_set_peer_legacy_sigalg(param_1,out);
    if (iVar2 == 0) {
      ERR_new();
      out = (ushort *)0x0;
      ERR_set_debug("ssl/statem/statem_lib.c",0x1bd,"tls_process_cert_verify");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      goto LAB_00174ac8;
    }
LAB_00174906:
    iVar2 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x26a),*(undefined8 *)(param_1 + 0xec),
                           &local_108);
    if (iVar2 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x1c2,"tls_process_cert_verify");
      uVar5 = 0xc0103;
LAB_00174d14:
      uVar6 = 0x50;
    }
    else {
      puVar4 = param_2[1];
      if ((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 2) == 0) {
        if (puVar4 != (ushort *)0x40) {
LAB_00174b8a:
          if (puVar4 == (ushort *)0x80) {
            iVar2 = EVP_PKEY_get_id(out);
            if (iVar2 == 0x3d4) goto LAB_00174e70;
            puVar4 = param_2[1];
          }
          goto LAB_00174940;
        }
        iVar2 = EVP_PKEY_get_id(out);
        if (iVar2 != 0x32b) {
          iVar2 = EVP_PKEY_get_id(out);
          if (iVar2 != 0x3d3) {
            puVar4 = param_2[1];
            goto LAB_00174b8a;
          }
        }
LAB_00174e70:
        puVar4 = param_2[1];
        siz = (ushort *)((ulong)puVar4 & 0xffffffff);
LAB_0017496c:
        if (siz <= puVar4) {
          in = *param_2;
          param_2[1] = (ushort *)((long)puVar4 - (long)siz);
          *param_2 = (ushort *)((long)in + (long)siz);
          iVar2 = get_cert_verify_tbs_data(param_1,local_e8,&local_f8,&local_100);
          if (iVar2 == 0) goto LAB_00174ac3;
          uVar5 = (*(undefined8 **)(param_1 + 0x26a))[0x88];
          uVar6 = **(undefined8 **)(param_1 + 0x26a);
          lVar3 = local_108;
          if (local_108 != 0) {
            lVar3 = EVP_MD_get0_name(local_108);
          }
          iVar2 = EVP_DigestVerifyInit_ex(ctx,&local_f0,lVar3,uVar6,uVar5,out);
          if (iVar2 < 1) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x1ef,"tls_process_cert_verify");
            uVar5 = 0x80006;
            goto LAB_00174d14;
          }
          iVar2 = EVP_PKEY_get_id(out);
          if ((iVar2 - 0x3d3U < 2) || (out = (ushort *)0x0, iVar2 == 0x32b)) {
            out = (ushort *)CRYPTO_malloc((int)siz,"ssl/statem/statem_lib.c",0x1f8);
            if (out == (ushort *)0x0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_lib.c",0x1f9,"tls_process_cert_verify");
              uVar5 = 0xc0100;
              goto LAB_00174ca4;
            }
            BUF_reverse((uchar *)out,(uchar *)in,(size_t)siz);
            in = out;
          }
          if ((*(long *)(param_1 + 0xec) != 0) &&
             (*(int *)(*(long *)(param_1 + 0xec) + 0x14) == 0x390)) {
            iVar2 = EVP_PKEY_CTX_set_rsa_padding(local_f0,6);
            if (0 < iVar2) {
              iVar2 = EVP_PKEY_CTX_set_rsa_pss_saltlen(local_f0,0xffffffff);
              if (0 < iVar2) goto LAB_00174bb8;
            }
            ERR_new();
            ERR_set_debug("ssl/statem/statem_lib.c",0x206,"tls_process_cert_verify");
            uVar5 = 0x80006;
            goto LAB_00174ca4;
          }
LAB_00174bb8:
          if (*param_1 != 0x300) {
            iVar2 = EVP_DigestVerify(ctx,in,siz);
            if (iVar2 < 1) {
              ERR_new();
              uVar5 = 0x219;
LAB_00174dd1:
              uVar7 = 0;
              ERR_set_debug("ssl/statem/statem_lib.c",uVar5,"tls_process_cert_verify");
              ossl_statem_fatal(param_1,0x33,0x7b,0);
              goto LAB_00174ac8;
            }
LAB_00174be4:
            uVar7 = 3;
            if ((param_1[0xe] == 0) &&
               ((*(byte *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60) & 8) == 0)) {
              iVar2 = **(int **)(param_1 + 2);
              if ((iVar2 == 0x10000) || (iVar2 < 0x304)) {
                uVar7 = 3;
              }
              else {
                uVar7 = (param_1[0xbc] != 1) + 2;
              }
            }
            goto LAB_00174ac8;
          }
          iVar2 = EVP_DigestVerifyUpdate(ctx,local_f8,local_100);
          if (0 < iVar2) {
            iVar2 = EVP_MD_CTX_ctrl(ctx,0x1d,*(undefined8 *)(*(long *)(param_1 + 0x246) + 8),
                                    *(long *)(param_1 + 0x246) + 0x50);
            if (0 < iVar2) {
              iVar2 = EVP_DigestVerifyFinal(ctx,(uchar *)in,(size_t)siz);
              if (iVar2 < 1) {
                ERR_new();
                uVar5 = 0x213;
                goto LAB_00174dd1;
              }
              goto LAB_00174be4;
            }
          }
          ERR_new();
          ERR_set_debug("ssl/statem/statem_lib.c",0x20f,"tls_process_cert_verify");
          uVar5 = 0x80006;
          goto LAB_00174ca4;
        }
        ERR_new();
        uVar5 = 0x1df;
      }
      else {
LAB_00174940:
        if ((ushort *)0x1 < puVar4) {
          puVar4 = puVar4 + -1;
          uVar1 = **param_2;
          param_2[1] = puVar4;
          *param_2 = *param_2 + 1;
          siz = (ushort *)(ulong)(ushort)(uVar1 << 8 | uVar1 >> 8);
          goto LAB_0017496c;
        }
        ERR_new();
        uVar5 = 0x1da;
      }
      ERR_set_debug("ssl/statem/statem_lib.c",uVar5,"tls_process_cert_verify");
      uVar5 = 0x9f;
      uVar6 = 0x32;
    }
LAB_00174ab9:
    ossl_statem_fatal(param_1,uVar6,uVar5,0);
  }
  else {
    if (param_2[1] < (ushort *)0x2) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x1b5,"tls_process_cert_verify");
      uVar5 = 0xf0;
      uVar6 = 0x32;
      goto LAB_00174ab9;
    }
    uVar1 = **param_2;
    param_2[1] = param_2[1] + -1;
    *param_2 = *param_2 + 1;
    iVar2 = tls12_check_peer_sigalg(param_1,uVar1 << 8 | uVar1 >> 8,out);
    if (0 < iVar2) goto LAB_00174906;
  }
LAB_00174ac3:
  uVar7 = 0;
  out = (ushort *)0x0;
LAB_00174ac8:
  BIO_free(*(BIO **)(param_1 + 0x62));
  *(undefined8 *)(param_1 + 0x62) = 0;
  EVP_MD_CTX_free(ctx);
  CRYPTO_free(out);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}