int SSL_CTX_use_RSAPrivateKey_file(SSL_CTX *ctx,char *file,int type)

{
  int iVar1;
  BIO_METHOD *type_00;
  BIO *bp;
  long lVar2;
  void *u;
  undefined1 *cb;
  RSA *rsa;
  undefined8 uVar3;
  
  type_00 = BIO_s_file();
  bp = BIO_new(type_00);
  if (bp == (BIO *)0x0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/ssl_rsa_legacy.c",0x85,"SSL_CTX_use_RSAPrivateKey_file");
    ERR_set_error(0x14,0x80007,0);
  }
  else {
    lVar2 = BIO_ctrl(bp,0x6c,3,file);
    if ((int)lVar2 < 1) {
      ERR_new();
      iVar1 = 0;
      ERR_set_debug("ssl/ssl_rsa_legacy.c",0x8a,"SSL_CTX_use_RSAPrivateKey_file");
      ERR_set_error(0x14,0x80002,0);
    }
    else {
      if (type == 2) {
        uVar3 = 0x8000d;
        rsa = d2i_RSAPrivateKey_bio(bp,(RSA **)0x0);
      }
      else {
        if (type != 1) {
          ERR_new();
          iVar1 = 0;
          ERR_set_debug("ssl/ssl_rsa_legacy.c",0x96,"SSL_CTX_use_RSAPrivateKey_file");
          ERR_set_error(0x14,0x7c,0);
          goto LAB_0014266b;
        }
        uVar3 = 0x80009;
        u = (void *)SSL_CTX_get_default_passwd_cb_userdata(ctx);
        cb = (undefined1 *)SSL_CTX_get_default_passwd_cb(ctx);
        rsa = PEM_read_bio_RSAPrivateKey(bp,(RSA **)0x0,cb,u);
      }
      if (rsa == (RSA *)0x0) {
        ERR_new();
        iVar1 = 0;
        ERR_set_debug("ssl/ssl_rsa_legacy.c",0x9a,"SSL_CTX_use_RSAPrivateKey_file");
        ERR_set_error(0x14,uVar3,0);
      }
      else {
        iVar1 = SSL_CTX_use_RSAPrivateKey(ctx,rsa);
        RSA_free(rsa);
      }
    }
  }
LAB_0014266b:
  BIO_free(bp);
  return iVar1;
}