int BIO_ssl_copy_session_id(BIO *to,BIO *from)

{
  int extraout_EAX;
  BIO *pBVar1;
  BIO *pBVar2;
  SSL **ppSVar3;
  SSL **ppSVar4;
  
  pBVar1 = BIO_find_type(to,0x207);
  pBVar2 = BIO_find_type(from,0x207);
  if ((pBVar1 != (BIO *)0x0) && (pBVar2 != (BIO *)0x0)) {
    ppSVar3 = (SSL **)BIO_get_data(pBVar1);
    ppSVar4 = (SSL **)BIO_get_data(pBVar2);
    if ((*ppSVar3 != (SSL *)0x0) && (*ppSVar4 != (SSL *)0x0)) {
      SSL_copy_session_id(*ppSVar3,*ppSVar4);
      return (int)(extraout_EAX != 0);
    }
  }
  return 0;
}