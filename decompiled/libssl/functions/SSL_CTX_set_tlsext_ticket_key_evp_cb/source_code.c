int SSL_CTX_set_tlsext_ticket_key_evp_cb
    (SSL_CTX *ctx, int (*fp)(SSL *, unsigned char *, unsigned char *,
                             EVP_CIPHER_CTX *, EVP_MAC_CTX *, int))
{
    ctx->ext.ticket_key_evp_cb = fp;
    return 1;
}