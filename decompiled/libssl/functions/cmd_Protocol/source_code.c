static int cmd_Protocol(SSL_CONF_CTX *cctx, const char *value)
{
    static const ssl_flag_tbl ssl_protocol_list[] = {
        SSL_FLAG_TBL_INV("ALL", SSL_OP_NO_SSL_MASK),
        SSL_FLAG_TBL_INV("SSLv2", SSL_OP_NO_SSLv2),
        SSL_FLAG_TBL_INV("SSLv3", SSL_OP_NO_SSLv3),
        SSL_FLAG_TBL_INV("TLSv1", SSL_OP_NO_TLSv1),
        SSL_FLAG_TBL_INV("TLSv1.1", SSL_OP_NO_TLSv1_1),
        SSL_FLAG_TBL_INV("TLSv1.2", SSL_OP_NO_TLSv1_2),
        SSL_FLAG_TBL_INV("TLSv1.3", SSL_OP_NO_TLSv1_3),
        SSL_FLAG_TBL_INV("DTLSv1", SSL_OP_NO_DTLSv1),
        SSL_FLAG_TBL_INV("DTLSv1.2", SSL_OP_NO_DTLSv1_2)
    };
    cctx->tbl = ssl_protocol_list;
    cctx->ntbl = OSSL_NELEM(ssl_protocol_list);
    return CONF_parse_list(value, ',', 1, ssl_set_option_list, cctx);
}