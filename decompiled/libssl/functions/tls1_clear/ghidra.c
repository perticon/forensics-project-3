undefined8 tls1_clear(int *param_1)

{
  undefined8 uVar1;
  
  uVar1 = ssl3_clear();
  if ((int)uVar1 != 0) {
    if (**(int **)(param_1 + 2) == 0x10000) {
      *param_1 = 0x304;
      return 1;
    }
    *param_1 = **(int **)(param_1 + 2);
    uVar1 = 1;
  }
  return uVar1;
}