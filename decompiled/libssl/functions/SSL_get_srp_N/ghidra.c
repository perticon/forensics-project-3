long SSL_get_srp_N(long param_1)

{
  if (*(long *)(param_1 + 0xbf8) != 0) {
    return *(long *)(param_1 + 0xbf8);
  }
  return *(long *)(*(long *)(param_1 + 0x9a8) + 0x340);
}