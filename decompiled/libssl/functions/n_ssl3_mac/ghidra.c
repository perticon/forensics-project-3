undefined8 n_ssl3_mac(long param_1,long param_2,uchar *param_3,int param_4)

{
  void *d;
  undefined4 uVar1;
  EVP_MD_CTX *in;
  undefined8 uVar2;
  undefined8 uVar3;
  undefined auVar4 [16];
  ulong uVar5;
  char cVar6;
  int iVar7;
  undefined8 uVar8;
  size_t __n;
  EVP_MD_CTX *out;
  long lVar9;
  undefined8 uVar10;
  long lVar11;
  long in_FS_OFFSET;
  void *local_c0;
  undefined local_a5;
  uint local_a4;
  ulong local_a0;
  undefined8 local_98;
  undefined auStack144 [80];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_4 == 0) {
    in = *(EVP_MD_CTX **)(param_1 + 0x860);
    uVar8 = EVP_MD_CTX_get0_md(in);
    iVar7 = EVP_MD_get_size(uVar8);
    if (-1 < iVar7) {
      local_a0 = (ulong)iVar7;
      auVar4 = ZEXT816(0) << 0x40 | ZEXT816(0x30);
      local_c0 = (void *)(param_1 + 0x1cf8);
      d = (void *)(param_1 + 0xb8);
      __n = SUB168(auVar4 / ZEXT816(local_a0),0) * local_a0;
      uVar8 = EVP_CIPHER_CTX_get0_cipher
                        (*(undefined8 *)(param_1 + 0x848),local_c0,
                         SUB168(auVar4 % ZEXT816(local_a0),0));
      iVar7 = EVP_CIPHER_get_mode(uVar8);
      if ((iVar7 != 2) ||
         (cVar6 = ssl3_cbc_record_digest_supported(in), uVar5 = local_a0, cVar6 == '\0')) {
        out = (EVP_MD_CTX *)EVP_MD_CTX_new();
        goto joined_r0x0015bd37;
      }
      lVar9 = __memcpy_chk(&local_98,d,local_a0,0x4b);
      memcpy((void *)(lVar9 + uVar5),ssl3_pad_1,__n);
      uVar8 = *(undefined8 *)(param_2 + 8);
      lVar11 = __n + uVar5;
      *(undefined8 *)((long)&local_98 + lVar11) = *(undefined8 *)(param_1 + 0x1cf8);
      uVar1 = *(undefined4 *)(param_2 + 4);
      auStack144[lVar11 + 2] = (char)uVar8;
      auStack144[lVar11 + 1] = (char)((ulong)uVar8 >> 8);
      auStack144[lVar11] = (char)uVar1;
      uVar2 = *(undefined8 *)(param_2 + 0x10);
      uVar3 = *(undefined8 *)(param_2 + 0x28);
      uVar10 = EVP_MD_CTX_get0_md(in);
      iVar7 = ssl3_cbc_digest_record(uVar10,param_3,&local_a0,lVar9,uVar3,uVar8,uVar2,d,uVar5,1);
      if (0 < iVar7) goto LAB_0015be80;
    }
  }
  else {
    in = *(EVP_MD_CTX **)(param_1 + 0x890);
    uVar8 = EVP_MD_CTX_get0_md(in);
    iVar7 = EVP_MD_get_size(uVar8);
    if (-1 < iVar7) {
      local_c0 = (void *)(param_1 + 0x1d00);
      local_a0 = (ulong)iVar7;
      d = (void *)(param_1 + 0x100);
      __n = SUB168((ZEXT816(0) << 0x40 | ZEXT816(0x30)) / ZEXT816(local_a0),0) * local_a0;
      out = (EVP_MD_CTX *)EVP_MD_CTX_new();
joined_r0x0015bd37:
      if (out != (EVP_MD_CTX *)0x0) {
        local_a5 = (undefined)*(undefined4 *)(param_2 + 4);
        *param_3 = (uchar)((ulong)*(undefined8 *)(param_2 + 8) >> 8);
        param_3[1] = (uchar)*(undefined8 *)(param_2 + 8);
        iVar7 = EVP_MD_CTX_copy_ex(out,in);
        if (((((0 < iVar7) && (iVar7 = EVP_DigestUpdate(out,d,local_a0), 0 < iVar7)) &&
             (iVar7 = EVP_DigestUpdate(out,ssl3_pad_1,__n), 0 < iVar7)) &&
            (((iVar7 = EVP_DigestUpdate(out,local_c0,8), 0 < iVar7 &&
              (iVar7 = EVP_DigestUpdate(out,&local_a5,1), 0 < iVar7)) &&
             ((iVar7 = EVP_DigestUpdate(out,param_3,2), 0 < iVar7 &&
              ((iVar7 = EVP_DigestUpdate(out,*(void **)(param_2 + 0x28),*(size_t *)(param_2 + 8)),
               0 < iVar7 && (iVar7 = EVP_DigestFinal_ex(out,param_3,(uint *)0x0), 0 < iVar7))))))))
           && ((iVar7 = EVP_MD_CTX_copy_ex(out,in), 0 < iVar7 &&
               ((((iVar7 = EVP_DigestUpdate(out,d,local_a0), 0 < iVar7 &&
                  (iVar7 = EVP_DigestUpdate(out,
                                            "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
                                            ,__n), 0 < iVar7)) &&
                 (iVar7 = EVP_DigestUpdate(out,param_3,local_a0), 0 < iVar7)) &&
                (iVar7 = EVP_DigestFinal_ex(out,param_3,&local_a4), 0 < iVar7)))))) {
          EVP_MD_CTX_free(out);
LAB_0015be80:
          ssl3_record_sequence_update(local_c0);
          uVar8 = 1;
          goto LAB_0015bc9a;
        }
        EVP_MD_CTX_free(out);
      }
    }
  }
  uVar8 = 0;
LAB_0015bc9a:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar8;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}