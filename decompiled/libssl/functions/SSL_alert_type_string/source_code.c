const char *SSL_alert_type_string(int value)
{
    switch (value >> 8) {
    case SSL3_AL_WARNING:
        return "W";
    case SSL3_AL_FATAL:
        return "F";
    default:
        return "U";
    }
}