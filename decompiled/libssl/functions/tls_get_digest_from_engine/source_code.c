const EVP_MD *tls_get_digest_from_engine(int nid)
{
    const EVP_MD *ret = NULL;
#ifndef OPENSSL_NO_ENGINE
    ENGINE *eng;

    /*
     * If there is an Engine available for this digest we use the "implicit"
     * form to ensure we use that engine later.
     */
    eng = ENGINE_get_digest_engine(nid);
    if (eng != NULL) {
        ret = ENGINE_get_digest(eng, nid);
        ENGINE_finish(eng);
    }
#endif
    return ret;
}