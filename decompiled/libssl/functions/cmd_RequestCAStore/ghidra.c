undefined8 cmd_RequestCAStore(long param_1,undefined8 param_2)

{
  undefined8 uVar1;
  long lVar2;
  
  lVar2 = *(long *)(param_1 + 0xa8);
  if (lVar2 == 0) {
    lVar2 = OPENSSL_sk_new_null();
    *(long *)(param_1 + 0xa8) = lVar2;
    if (lVar2 == 0) {
      return 0;
    }
  }
  uVar1 = SSL_add_store_cert_subjects_to_stack(lVar2,param_2);
  return uVar1;
}