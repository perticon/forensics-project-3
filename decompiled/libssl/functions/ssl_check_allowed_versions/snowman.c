uint32_t ssl_check_allowed_versions(int32_t edi, int32_t esi) {
    int32_t eax3;
    uint32_t edx4;
    uint32_t eax5;
    uint32_t eax6;
    uint32_t eax7;
    uint32_t eax8;

    if (edi == 0x100) {
        if (esi == 0x100) {
            addr_35854_3:
            return 1;
        } else {
            if (esi >> 8 == 0xfe) {
                return 1;
            }
        }
    } else {
        eax3 = edi >> 8;
        if (esi == 0x100 || (edx4 = reinterpret_cast<uint32_t>(esi >> 8), edx4 == 0xfe)) {
            if (eax3 != 0xfe) {
                eax5 = 0;
                *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<uint1_t>(edi == 0);
                return eax5;
            }
        } else {
            if (eax3 != 0xfe) {
                if (edi) {
                    if (!esi) {
                        esi = 0x304;
                    }
                    eax6 = 1;
                    if (edi != 0x300) {
                        *reinterpret_cast<unsigned char*>(&eax6) = reinterpret_cast<uint1_t>(edi <= 0x300);
                        *reinterpret_cast<unsigned char*>(&edx4) = reinterpret_cast<uint1_t>(esi > 0x2ff);
                        eax7 = eax6 & edx4 ^ 1;
                        return static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&eax7));
                    }
                }
            } else {
                eax8 = 0;
                *reinterpret_cast<unsigned char*>(&eax8) = reinterpret_cast<uint1_t>(esi == 0);
                return eax8;
            }
        }
        goto addr_35854_3;
    }
}