bool ssl_check_allowed_versions(int param_1,int param_2)

{
  if (param_1 == 0x100) {
    if (param_2 != 0x100) {
      if (param_2 >> 8 == 0xfe) {
        return true;
      }
LAB_00135809:
      return param_2 == 0;
    }
  }
  else if ((param_2 == 0x100) || (param_2 >> 8 == 0xfe)) {
    if (param_1 >> 8 != 0xfe) {
      return param_1 == 0;
    }
  }
  else {
    if (param_1 >> 8 == 0xfe) goto LAB_00135809;
    if (param_1 != 0) {
      if (param_2 == 0) {
        param_2 = 0x304;
      }
      if (param_1 != 0x300) {
        return 0x300 < param_1 || param_2 < 0x300;
      }
    }
  }
  return true;
}