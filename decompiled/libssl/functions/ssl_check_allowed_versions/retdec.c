int64_t ssl_check_allowed_versions(int64_t a1, int32_t a2) {
    int64_t v1 = a2;
    int32_t v2 = a1; // 0x357e0
    if (v2 == 256) {
        if (a2 == 256) {
            // 0x3584a
            return 1;
        }
        if ((v1 & 0xffffff00) != 0xfe00) {
            // 0x35809
            return a2 == 0;
        }
        // 0x3584a
        return 1;
    }
    if (a2 == 256 || (v1 & 0xffffff00) == 0xfe00) {
        if ((v2 & -256) != 0xfe00) {
            // 0x3581f
            return v2 == 0;
        }
        // 0x3584a
        return 1;
    }
    if ((v2 & -256) == 0xfe00) {
        // 0x35809
        return a2 == 0;
    }
    if (v2 == 0) {
        // 0x3584a
        return 1;
    }
    int32_t v3 = v2 - (int32_t)&g86; // 0x35867
    if (v3 == 0) {
        // 0x3584a
        return 1;
    }
    int32_t v4 = a2 == 0 ? (int32_t)&g91 : a2; // 0x35872
    int32_t v5 = v4 - (int32_t)&g85; // 0x35872
    return v3 < 0 == ((v3 ^ v2) & (v2 ^ (int32_t)&g86)) < 0 | v5 < 0 == ((v5 ^ v4) & (v4 ^ (int32_t)&g85)) < 0 != v5 != 0;
}