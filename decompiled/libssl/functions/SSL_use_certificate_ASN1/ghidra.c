int SSL_use_certificate_ASN1(SSL *ssl,uchar *d,int len)

{
  undefined8 *puVar1;
  int iVar2;
  X509 *pXVar3;
  long in_FS_OFFSET;
  uchar *local_30;
  X509 *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  puVar1 = *(undefined8 **)&ssl[3].ex_data.dummy;
  local_30 = d;
  local_28 = (X509 *)X509_new_ex(*puVar1,puVar1[0x88]);
  if (local_28 == (X509 *)0x0) {
    ERR_new();
    iVar2 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0x6a,"SSL_use_certificate_ASN1");
    ERR_set_error(0x14,0xc0100,0);
  }
  else {
    pXVar3 = d2i_X509(&local_28,&local_30,(long)len);
    if (pXVar3 == (X509 *)0x0) {
      iVar2 = 0;
      X509_free(local_28);
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0x70,"SSL_use_certificate_ASN1");
      ERR_set_error(0x14,0x8000d,0);
    }
    else {
      iVar2 = SSL_use_certificate(ssl,local_28);
      X509_free(local_28);
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}