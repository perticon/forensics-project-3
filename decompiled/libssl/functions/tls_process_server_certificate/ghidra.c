undefined8 tls_process_server_certificate(long param_1,ushort **param_2)

{
  byte bVar1;
  byte bVar2;
  byte bVar3;
  ushort uVar4;
  undefined8 *puVar5;
  int iVar6;
  long lVar7;
  ushort *puVar8;
  X509 *pXVar9;
  undefined8 uVar10;
  long lVar11;
  ushort *puVar12;
  ushort *puVar13;
  long in_FS_OFFSET;
  X509 *local_70;
  ushort *local_68;
  void *local_60;
  ushort *local_58;
  ushort *local_50;
  long local_40;
  
  lVar11 = *(long *)(param_1 + 0x918);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_70 = (X509 *)0x0;
  lVar7 = OPENSSL_sk_new_null();
  *(long *)(lVar11 + 0x2c0) = lVar7;
  if (lVar7 == 0) {
    ERR_new();
    uVar10 = 0x6f4;
LAB_0016dde1:
    ERR_set_debug("ssl/statem/statem_clnt.c",uVar10,"tls_process_server_certificate");
    ossl_statem_fatal(param_1,0x50,0xc0100,0);
  }
  else {
    puVar8 = param_2[1];
    if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
        (iVar6 = **(int **)(param_1 + 8), iVar6 != 0x10000)) && (0x303 < iVar6)) {
      if (puVar8 != (ushort *)0x0) {
        puVar8 = (ushort *)((long)puVar8 + -1);
        bVar1 = *(byte *)*param_2;
        param_2[1] = puVar8;
        *param_2 = (ushort *)((long)*param_2 + 1);
        if (bVar1 == 0) goto LAB_0016dac0;
      }
    }
    else {
LAB_0016dac0:
      if ((ushort *)0x2 < puVar8) {
        puVar13 = *param_2;
        bVar1 = *(byte *)puVar13;
        bVar2 = *(byte *)((long)puVar13 + 1);
        bVar3 = *(byte *)(puVar13 + 1);
        *param_2 = (ushort *)((long)puVar13 + 3);
        puVar8 = (ushort *)((long)puVar8 + -3);
        param_2[1] = puVar8;
        if ((puVar8 != (ushort *)0x0) &&
           ((ushort *)((ulong)bVar1 << 0x10 | (ulong)bVar2 << 8 | (ulong)bVar3) == puVar8)) {
          lVar11 = 0;
          do {
            if (puVar8 < (ushort *)0x3) {
LAB_0016dd98:
              ERR_new();
              uVar10 = 0x703;
LAB_0016dda9:
              ERR_set_debug("ssl/statem/statem_clnt.c",uVar10,"tls_process_server_certificate");
              ossl_statem_fatal(param_1,0x32,0x87,0);
              goto LAB_0016db31;
            }
            puVar13 = *param_2;
            puVar8 = (ushort *)((long)puVar8 + -3);
            bVar1 = *(byte *)puVar13;
            bVar2 = *(byte *)((long)puVar13 + 1);
            puVar12 = (ushort *)((long)puVar13 + 3);
            bVar3 = *(byte *)(puVar13 + 1);
            *param_2 = puVar12;
            param_2[1] = puVar8;
            puVar13 = (ushort *)((ulong)bVar1 << 0x10 | (ulong)bVar2 << 8 | (ulong)bVar3);
            if (puVar8 < puVar13) goto LAB_0016dd98;
            param_2[1] = (ushort *)((long)puVar8 - (long)puVar13);
            puVar5 = *(undefined8 **)(param_1 + 0x9a8);
            *param_2 = (ushort *)((long)puVar12 + (long)puVar13);
            local_68 = puVar12;
            local_70 = (X509 *)X509_new_ex(*puVar5,puVar5[0x88]);
            if (local_70 == (X509 *)0x0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x70a,"tls_process_server_certificate");
              ossl_statem_fatal(param_1,0x32,0xc0100,0);
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x70b,"tls_process_server_certificate");
              ERR_set_error(0x14,0xc0100,0);
              goto LAB_0016db31;
            }
            pXVar9 = d2i_X509(&local_70,(uchar **)&local_68,(long)puVar13);
            if (pXVar9 == (X509 *)0x0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x710,"tls_process_server_certificate");
              ossl_statem_fatal(param_1,0x2a,0x8000d,0);
              goto LAB_0016db31;
            }
            if (local_68 != (ushort *)((long)puVar12 + (long)puVar13)) {
              ERR_new();
              uVar10 = 0x715;
              goto LAB_0016dda9;
            }
            if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
                (iVar6 = **(int **)(param_1 + 8), 0x303 < iVar6)) && (iVar6 != 0x10000)) {
              local_60 = (void *)0x0;
              if ((ushort *)0x1 < param_2[1]) {
                puVar8 = param_2[1] + -1;
                uVar4 = **param_2;
                puVar13 = (ushort *)(ulong)(ushort)(uVar4 << 8 | uVar4 >> 8);
                if (puVar13 <= puVar8) {
                  local_58 = *param_2 + 1;
                  param_2[1] = (ushort *)((long)puVar8 - (long)puVar13);
                  *param_2 = (ushort *)((long)local_58 + (long)puVar13);
                  local_50 = puVar13;
                  iVar6 = tls_collect_extensions(param_1,&local_58,0x1000,&local_60,0,lVar11 == 0);
                  if ((iVar6 != 0) &&
                     (iVar6 = tls_parse_all_extensions(param_1,0x1000,local_60), iVar6 != 0)) {
                    CRYPTO_free(local_60);
                    goto LAB_0016dd20;
                  }
                  CRYPTO_free(local_60);
                  goto LAB_0016db31;
                }
              }
              ERR_new();
              ERR_set_debug("ssl/statem/statem_clnt.c",0x71e,"tls_process_server_certificate");
              ossl_statem_fatal(param_1,0x32,0x10f,0);
              goto LAB_0016db31;
            }
LAB_0016dd20:
            iVar6 = OPENSSL_sk_push(*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2c0),local_70);
            if (iVar6 == 0) {
              ERR_new();
              uVar10 = 0x72f;
              goto LAB_0016dde1;
            }
            puVar8 = param_2[1];
            lVar11 = lVar11 + 1;
            local_70 = (X509 *)0x0;
          } while (puVar8 != (ushort *)0x0);
          uVar10 = 2;
          goto LAB_0016db62;
        }
      }
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x6fd,"tls_process_server_certificate");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
  }
LAB_0016db31:
  X509_free(local_70);
  OSSL_STACK_OF_X509_free(*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2c0));
  *(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2c0) = 0;
  uVar10 = 0;
LAB_0016db62:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar10;
}