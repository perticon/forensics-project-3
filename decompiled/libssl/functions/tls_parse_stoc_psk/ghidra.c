undefined8 tls_parse_stoc_psk(long param_1,ushort **param_2)

{
  SSL_SESSION *ses;
  undefined8 uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  uint uVar5;
  ushort *puVar6;
  ushort uVar7;
  
  if ((ushort *)0x1 < param_2[1]) {
    uVar7 = **param_2;
    puVar6 = param_2[1] + -1;
    *param_2 = *param_2 + 1;
    param_2[1] = puVar6;
    if (puVar6 == (ushort *)0x0) {
      uVar7 = uVar7 << 8 | uVar7 >> 8;
      if (*(uint *)(param_1 + 0xb50) <= (uint)uVar7) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x7a1,"tls_parse_stoc_psk");
        ossl_statem_fatal(param_1,0x2f,0x72,0);
        return 0;
      }
      ses = *(SSL_SESSION **)(param_1 + 0x920);
      if (uVar7 == 0) {
        if ((*(uint *)(param_1 + 0xb50) == 2) || (ses == (SSL_SESSION *)0x0)) {
          *(undefined4 *)(param_1 + 0x4d0) = 1;
          SSL_SESSION_free(ses);
          *(undefined8 *)(param_1 + 0x920) = 0;
          return 1;
        }
      }
      else if (ses == (SSL_SESSION *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_clnt.c",0x7b3,"tls_parse_stoc_psk");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        return 0;
      }
      if ((((*(uint *)(param_1 + 0x84) & 0xfffffffb) != 3) ||
          (*(int *)((*(SSL_SESSION **)(param_1 + 0x918))[1].krb5_client_princ + 0x6c) != 0)) ||
         (*(int *)(ses[1].krb5_client_princ + 0x6c) == 0)) {
        uVar2 = *(undefined4 *)ses->master_key;
        uVar3 = *(undefined4 *)(ses->master_key + 4);
        uVar4 = *(undefined4 *)(ses->master_key + 8);
        *(int *)(param_1 + 0x544) = ses->master_key_length;
        *(undefined4 *)(param_1 + 0x548) = uVar2;
        *(undefined4 *)(param_1 + 0x54c) = uVar3;
        *(undefined4 *)(param_1 + 0x550) = uVar4;
        uVar2 = *(undefined4 *)(ses->master_key + 0x10);
        uVar3 = *(undefined4 *)(ses->master_key + 0x14);
        uVar4 = *(undefined4 *)(ses->master_key + 0x18);
        *(undefined4 *)(param_1 + 0x554) = *(undefined4 *)(ses->master_key + 0xc);
        *(undefined4 *)(param_1 + 0x558) = uVar2;
        *(undefined4 *)(param_1 + 0x55c) = uVar3;
        *(undefined4 *)(param_1 + 0x560) = uVar4;
        uVar2 = *(undefined4 *)(ses->master_key + 0x20);
        uVar3 = *(undefined4 *)(ses->master_key + 0x24);
        uVar4 = *(undefined4 *)(ses->master_key + 0x28);
        *(undefined4 *)(param_1 + 0x564) = *(undefined4 *)(ses->master_key + 0x1c);
        *(undefined4 *)(param_1 + 0x568) = uVar2;
        *(undefined4 *)(param_1 + 0x56c) = uVar3;
        *(undefined4 *)(param_1 + 0x570) = uVar4;
        uVar5 = ses->session_id_length;
        uVar2 = *(undefined4 *)ses->session_id;
        uVar3 = *(undefined4 *)(ses->session_id + 4);
        *(undefined4 *)(param_1 + 0x574) = *(undefined4 *)(ses->master_key + 0x2c);
        *(uint *)(param_1 + 0x578) = uVar5;
        *(undefined4 *)(param_1 + 0x57c) = uVar2;
        *(undefined4 *)(param_1 + 0x580) = uVar3;
      }
      SSL_SESSION_free(*(SSL_SESSION **)(param_1 + 0x918));
      uVar1 = *(undefined8 *)(param_1 + 0x920);
      *(undefined4 *)(param_1 + 0x4d0) = 1;
      *(undefined8 *)(param_1 + 0x920) = 0;
      *(undefined8 *)(param_1 + 0x918) = uVar1;
      if (uVar7 == 0) {
        return 1;
      }
      *(undefined4 *)(param_1 + 0xb34) = 0;
      return 1;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x79c,"tls_parse_stoc_psk");
  ossl_statem_fatal(param_1,0x32,0x9f,0);
  return 0;
}