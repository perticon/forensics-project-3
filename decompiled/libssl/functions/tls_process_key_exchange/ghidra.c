undefined8 tls_process_key_exchange(long param_1,ushort **param_2)

{
  byte *pbVar1;
  byte bVar2;
  uint uVar3;
  ushort *puVar4;
  void *ptr;
  undefined8 uVar5;
  EVP_PKEY *pEVar6;
  uint uVar7;
  int iVar8;
  undefined4 uVar9;
  ushort *puVar10;
  BIGNUM *pBVar11;
  BIGNUM *a;
  BIGNUM *a_00;
  EVP_PKEY_CTX *ctx;
  ushort *puVar12;
  ushort *puVar13;
  long lVar14;
  long lVar15;
  byte *pbVar16;
  long lVar17;
  ushort uVar18;
  ushort uVar19;
  ushort *puVar20;
  ushort *puVar21;
  undefined8 uVar22;
  long lVar23;
  ushort *puVar24;
  ushort *puVar25;
  byte *pbVar26;
  ushort uVar27;
  long in_FS_OFFSET;
  long local_80;
  uint local_78;
  undefined8 local_58;
  long local_50;
  EVP_PKEY *local_48;
  long local_40;
  
  puVar13 = param_2[1];
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_58 = 0;
  uVar3 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x1c);
  puVar4 = *param_2;
  EVP_PKEY_free(*(EVP_PKEY **)(param_1 + 0x4b0));
  *(undefined8 *)(param_1 + 0x4b0) = 0;
  if ((uVar3 & 0x1c8) == 0) goto LAB_0017046e;
  if (param_2[1] < (ushort *)0x2) {
LAB_001708e0:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x79b,"tls_process_ske_psk_preamble");
    uVar22 = 0x9f;
LAB_00170908:
    ossl_statem_fatal(param_1,0x32,uVar22,0);
LAB_001704a0:
    lVar15 = 0;
  }
  else {
    puVar21 = param_2[1] + -1;
    uVar19 = **param_2;
    puVar10 = (ushort *)(ulong)(ushort)(uVar19 << 8 | uVar19 >> 8);
    if (puVar21 < puVar10) goto LAB_001708e0;
    puVar25 = *param_2 + 1;
    param_2[1] = (ushort *)((long)puVar21 - (long)puVar10);
    *param_2 = (ushort *)((long)puVar25 + (long)puVar10);
    if ((ushort *)0x100 < puVar10) {
      ERR_new();
      lVar15 = 0;
      ERR_set_debug("ssl/statem/statem_clnt.c",0x7a6,"tls_process_ske_psk_preamble");
      ossl_statem_fatal(param_1,0x28,0x92,0);
      goto LAB_001704a3;
    }
    lVar17 = *(long *)(param_1 + 0x918);
    ptr = *(void **)(lVar17 + 0x2a0);
    if (puVar10 == (ushort *)0x0) {
      CRYPTO_free(ptr);
      *(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2a0) = 0;
    }
    else {
      CRYPTO_free(ptr);
      lVar15 = CRYPTO_strndup(puVar25,puVar10,"include/internal/packet.h");
      *(long *)(lVar17 + 0x2a0) = lVar15;
      if (lVar15 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0x7af,"tls_process_ske_psk_preamble");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        goto LAB_001704a3;
      }
    }
LAB_0017046e:
    if ((uVar3 & 0x48) != 0) {
LAB_00170474:
      uVar7 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20);
LAB_0017047f:
      if ((uVar7 & 0x44 | uVar3 & 0x1c8) != 0) {
LAB_00170506:
        uVar22 = 3;
        if (param_2[1] == (ushort *)0x0) goto LAB_001704ad;
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0x911,"tls_process_key_exchange");
        uVar22 = 0x99;
        goto LAB_00170908;
      }
LAB_00170487:
      iVar8 = ssl3_check_cert_and_algorithm(param_1);
      if (iVar8 != 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0x90a,"tls_process_key_exchange");
        uVar22 = 0x186;
        goto LAB_00170908;
      }
      goto LAB_001704a0;
    }
    if ((uVar3 & 0x20) != 0) {
      if ((ushort *)0x1 < param_2[1]) {
        puVar10 = param_2[1] + -1;
        uVar19 = **param_2;
        uVar19 = uVar19 << 8 | uVar19 >> 8;
        puVar21 = (ushort *)(ulong)uVar19;
        if (puVar21 <= puVar10) {
          puVar25 = *param_2 + 1;
          puVar10 = (ushort *)((long)puVar10 - (long)puVar21);
          puVar21 = (ushort *)((long)puVar25 + (long)puVar21);
          param_2[1] = puVar10;
          *param_2 = puVar21;
          if ((ushort *)0x1 < puVar10) {
            uVar27 = *puVar21 << 8 | *puVar21 >> 8;
            puVar20 = (ushort *)(ulong)uVar27;
            if (puVar20 <= puVar10 + -1) {
              puVar10 = (ushort *)((long)(puVar10 + -1) - (long)puVar20);
              puVar20 = (ushort *)((long)(puVar21 + 1) + (long)puVar20);
              param_2[1] = puVar10;
              *param_2 = puVar20;
              if (puVar10 != (ushort *)0x0) {
                bVar2 = *(byte *)puVar20;
                pbVar16 = (byte *)(ulong)bVar2;
                if (pbVar16 <= (byte *)((long)puVar10 + -1)) {
                  puVar12 = (ushort *)((byte *)((long)puVar10 + -1) + -(long)pbVar16);
                  puVar10 = (ushort *)((byte *)((long)puVar20 + 1) + (long)pbVar16);
                  param_2[1] = puVar12;
                  *param_2 = puVar10;
                  if ((ushort *)0x1 < puVar12) {
                    uVar18 = *puVar10 << 8 | *puVar10 >> 8;
                    puVar24 = (ushort *)(ulong)uVar18;
                    if (puVar24 <= puVar12 + -1) {
                      param_2[1] = (ushort *)((long)(puVar12 + -1) - (long)puVar24);
                      *param_2 = (ushort *)((long)(puVar10 + 1) + (long)puVar24);
                      pBVar11 = BN_bin2bn((uchar *)puVar25,(uint)uVar19,(BIGNUM *)0x0);
                      local_78 = (uint)uVar27;
                      *(BIGNUM **)(param_1 + 0xbf8) = pBVar11;
                      if (pBVar11 != (BIGNUM *)0x0) {
                        pBVar11 = BN_bin2bn((uchar *)(puVar21 + 1),local_78,(BIGNUM *)0x0);
                        *(BIGNUM **)(param_1 + 0xc00) = pBVar11;
                        if (pBVar11 != (BIGNUM *)0x0) {
                          pBVar11 = BN_bin2bn((byte *)((long)puVar20 + 1),(uint)bVar2,(BIGNUM *)0x0)
                          ;
                          *(BIGNUM **)(param_1 + 0xc08) = pBVar11;
                          if (pBVar11 != (BIGNUM *)0x0) {
                            pBVar11 = BN_bin2bn((uchar *)(puVar10 + 1),(uint)uVar18,(BIGNUM *)0x0);
                            *(BIGNUM **)(param_1 + 0xc10) = pBVar11;
                            if (pBVar11 != (BIGNUM *)0x0) {
                              iVar8 = srp_verify_server_param(param_1);
                              if (iVar8 != 0) {
                                uVar7 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20);
                                if ((uVar7 & 3) != 0) {
                                  lVar17 = X509_get0_pubkey(*(undefined8 *)
                                                             (*(long *)(param_1 + 0x918) + 0x2b8));
                                  goto LAB_00170b0f;
                                }
                                goto LAB_0017047f;
                              }
                              goto LAB_001704a0;
                            }
                          }
                        }
                      }
                      ERR_new();
                      ERR_set_debug("ssl/statem/statem_clnt.c",0x7d3,"tls_process_ske_srp");
                      uVar22 = 0x80003;
LAB_00170f97:
                      lVar15 = 0;
                      ossl_statem_fatal(param_1,0x50,uVar22,0);
                      goto LAB_001704a3;
                    }
                  }
                }
              }
            }
          }
        }
      }
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x7c3,"tls_process_ske_srp");
      uVar22 = 0x9f;
      goto LAB_00170908;
    }
    if ((uVar3 & 0x102) != 0) {
      local_48 = (EVP_PKEY *)0x0;
      if ((ushort *)0x1 < param_2[1]) {
        puVar10 = param_2[1] + -1;
        uVar19 = **param_2;
        uVar19 = uVar19 << 8 | uVar19 >> 8;
        puVar21 = (ushort *)(ulong)uVar19;
        if (puVar21 <= puVar10) {
          puVar25 = *param_2 + 1;
          puVar10 = (ushort *)((long)puVar10 - (long)puVar21);
          puVar21 = (ushort *)((long)puVar25 + (long)puVar21);
          param_2[1] = puVar10;
          *param_2 = puVar21;
          if ((ushort *)0x1 < puVar10) {
            uVar27 = *puVar21 << 8 | *puVar21 >> 8;
            puVar20 = (ushort *)(ulong)uVar27;
            if (puVar20 <= puVar10 + -1) {
              puVar10 = (ushort *)((long)(puVar10 + -1) - (long)puVar20);
              puVar20 = (ushort *)((long)(puVar21 + 1) + (long)puVar20);
              param_2[1] = puVar10;
              *param_2 = puVar20;
              if (puVar10 < (ushort *)0x2) goto LAB_00170920;
              uVar18 = *puVar20 << 8 | *puVar20 >> 8;
              puVar12 = (ushort *)(ulong)uVar18;
              if (puVar10 + -1 < puVar12) goto LAB_00170920;
              param_2[1] = (ushort *)((long)(puVar10 + -1) - (long)puVar12);
              *param_2 = (ushort *)((long)(puVar20 + 1) + (long)puVar12);
              pBVar11 = BN_bin2bn((uchar *)puVar25,(uint)uVar19,(BIGNUM *)0x0);
              a = BN_bin2bn((uchar *)(puVar21 + 1),(uint)uVar27,(BIGNUM *)0x0);
              a_00 = BN_bin2bn((uchar *)(puVar20 + 1),(uint)uVar18,(BIGNUM *)0x0);
              if ((pBVar11 == (BIGNUM *)0x0 || a == (BIGNUM *)0x0) || (a_00 == (BIGNUM *)0x0)) {
                ERR_new();
                lVar15 = 0;
                ERR_set_debug("ssl/statem/statem_clnt.c",0x7fe,"tls_process_ske_dhe");
                ossl_statem_fatal(param_1,0x50,0x80003,0);
                ctx = (EVP_PKEY_CTX *)0x0;
                local_80 = 0;
              }
              else {
                lVar15 = OSSL_PARAM_BLD_new();
                if ((((lVar15 == 0) ||
                     (iVar8 = OSSL_PARAM_BLD_push_BN(lVar15,&DAT_00188a4e,pBVar11), iVar8 == 0)) ||
                    (iVar8 = OSSL_PARAM_BLD_push_BN(lVar15,&DAT_00188a53,a), iVar8 == 0)) ||
                   ((iVar8 = OSSL_PARAM_BLD_push_BN(lVar15,&DAT_0018c916,a_00), iVar8 == 0 ||
                    (local_80 = OSSL_PARAM_BLD_to_param(lVar15), local_80 == 0)))) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_clnt.c",0x809,"tls_process_ske_dhe");
                  ossl_statem_fatal(param_1,0x50,0xc0103,0);
                  ctx = (EVP_PKEY_CTX *)0x0;
                  local_80 = 0;
                }
                else {
                  ctx = (EVP_PKEY_CTX *)
                        EVP_PKEY_CTX_new_from_name
                                  (**(undefined8 **)(param_1 + 0x9a8),&DAT_00183578,
                                   (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
                  if (ctx == (EVP_PKEY_CTX *)0x0) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_clnt.c",0x80f,"tls_process_ske_dhe");
                    uVar22 = 0xc0103;
                  }
                  else {
                    iVar8 = EVP_PKEY_fromdata_init(ctx);
                    if ((0 < iVar8) &&
                       (iVar8 = EVP_PKEY_fromdata(ctx,&local_48,0x87,local_80), 0 < iVar8)) {
                      EVP_PKEY_CTX_free(ctx);
                      ctx = (EVP_PKEY_CTX *)
                            EVP_PKEY_CTX_new_from_pkey
                                      (**(undefined8 **)(param_1 + 0x9a8),local_48,
                                       (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
                      if ((ctx == (EVP_PKEY_CTX *)0x0) ||
                         ((iVar8 = EVP_PKEY_param_check_quick(ctx), iVar8 != 1 ||
                          (iVar8 = EVP_PKEY_public_check(ctx), pEVar6 = local_48, iVar8 != 1)))) {
                        ERR_new();
                        ERR_set_debug("ssl/statem/statem_clnt.c",0x823,"tls_process_ske_dhe");
                        ossl_statem_fatal(param_1,0x2f,0x66,0);
                      }
                      else {
                        uVar9 = EVP_PKEY_get_security_bits(local_48);
                        iVar8 = ssl_security(param_1,0x40007,uVar9,0,pEVar6);
                        pEVar6 = local_48;
                        if (iVar8 != 0) {
                          lVar17 = 0;
                          local_48 = (EVP_PKEY *)0x0;
                          *(EVP_PKEY **)(param_1 + 0x4b0) = pEVar6;
                          if ((*(byte *)(*(long *)(param_1 + 0x2e0) + 0x20) & 3) != 0) {
                            lVar17 = X509_get0_pubkey(*(undefined8 *)
                                                       (*(long *)(param_1 + 0x918) + 0x2b8));
                          }
                          OSSL_PARAM_BLD_free(lVar15);
                          OSSL_PARAM_free(local_80);
                          EVP_PKEY_free(local_48);
                          EVP_PKEY_CTX_free(ctx);
                          BN_free(pBVar11);
                          BN_free(a);
                          BN_free(a_00);
                          goto LAB_00170b0f;
                        }
                        ERR_new();
                        ERR_set_debug("ssl/statem/statem_clnt.c",0x82a,"tls_process_ske_dhe");
                        ossl_statem_fatal(param_1,0x28,0x18a,0);
                      }
                      goto LAB_00170890;
                    }
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_clnt.c",0x814,"tls_process_ske_dhe");
                    uVar22 = 0x66;
                  }
                  ossl_statem_fatal(param_1,0x50,uVar22,0);
                }
              }
LAB_00170890:
              OSSL_PARAM_BLD_free(lVar15);
              OSSL_PARAM_free(local_80);
              EVP_PKEY_free(local_48);
              EVP_PKEY_CTX_free(ctx);
              lVar15 = 0;
              BN_free(pBVar11);
              BN_free(a);
              BN_free(a_00);
              goto LAB_001704a3;
            }
          }
        }
      }
LAB_00170920:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x7f4,"tls_process_ske_dhe");
      uVar22 = 0x9f;
      goto LAB_00170908;
    }
    if ((uVar3 & 0x84) == 0) {
      if (uVar3 != 0) {
        ERR_new();
        lVar15 = 0;
        ERR_set_debug("ssl/statem/statem_clnt.c",0x8a9,"tls_process_key_exchange");
        ossl_statem_fatal(param_1,10,0xf4,0);
        goto LAB_001704a3;
      }
      if ((*(byte *)(*(long *)(param_1 + 0x2e0) + 0x20) & 0x44) != 0) goto LAB_00170506;
      goto LAB_00170487;
    }
    puVar21 = param_2[1];
    if (puVar21 == (ushort *)0x0) {
LAB_00170ed0:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x852,"tls_process_ske_ecdhe");
      uVar22 = 0xa0;
      goto LAB_00170908;
    }
    puVar10 = *param_2;
    bVar2 = *(byte *)puVar10;
    *param_2 = (ushort *)((long)puVar10 + 1);
    param_2[1] = (ushort *)((long)puVar21 + -1);
    if ((ushort *)((long)puVar21 + -1) < (ushort *)0x2) goto LAB_00170ed0;
    uVar19 = *(ushort *)((long)puVar10 + 1);
    *param_2 = (ushort *)((long)puVar10 + 3);
    param_2[1] = (ushort *)((long)puVar21 + -3);
    if (bVar2 == 3) {
      uVar19 = uVar19 << 8 | uVar19 >> 8;
      iVar8 = tls1_check_group_id(param_1,uVar19,1);
      if (iVar8 == 0) goto LAB_00171090;
      lVar15 = ssl_generate_param_group(param_1,uVar19);
      *(long *)(param_1 + 0x4b0) = lVar15;
      if (lVar15 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0x860,"tls_process_ske_ecdhe");
        ossl_statem_fatal(param_1,0x50,0x13a,0);
        goto LAB_001704a3;
      }
      if (param_2[1] == (ushort *)0x0) {
LAB_0017121d:
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0x866,"tls_process_ske_ecdhe");
        uVar22 = 0x9f;
        goto LAB_00170908;
      }
      pbVar16 = (byte *)((long)param_2[1] + -1);
      pbVar26 = (byte *)(ulong)*(byte *)*param_2;
      if (pbVar16 < pbVar26) goto LAB_0017121d;
      pbVar1 = (byte *)((long)*param_2 + 1);
      param_2[1] = (ushort *)(pbVar16 + -(long)pbVar26);
      *param_2 = (ushort *)(pbVar1 + (long)pbVar26);
      iVar8 = EVP_PKEY_set1_encoded_public_key(lVar15,pbVar1,pbVar26);
      if (0 < iVar8) {
        uVar7 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20);
        if (((uVar7 & 8) == 0) && ((uVar7 & 1) == 0)) {
          *(uint *)(*(long *)(param_1 + 0x918) + 0x308) = (uint)uVar19;
          goto LAB_0017047f;
        }
        lVar17 = X509_get0_pubkey(*(undefined8 *)(*(long *)(param_1 + 0x918) + 0x2b8));
        *(uint *)(*(long *)(param_1 + 0x918) + 0x308) = (uint)uVar19;
LAB_00170b0f:
        if (lVar17 == 0) goto LAB_00170474;
        local_50 = 0;
        puVar21 = param_2[1];
        lVar23 = (long)puVar13 - (long)puVar21;
        if ((puVar13 < puVar21) || (lVar23 < 0)) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_clnt.c",0x8bc,"tls_process_key_exchange");
          uVar22 = 0xc0103;
          goto LAB_00170908;
        }
        if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 2) == 0) {
          iVar8 = tls1_set_peer_legacy_sigalg(param_1,lVar17);
          if (iVar8 == 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0x8cc,"tls_process_key_exchange");
            uVar22 = 0xc0103;
            goto LAB_00170f97;
          }
        }
        else {
          if (puVar21 < (ushort *)0x2) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_clnt.c",0x8c4,"tls_process_key_exchange");
            uVar22 = 0xa0;
            goto LAB_00170908;
          }
          uVar19 = **param_2;
          param_2[1] = puVar21 + -1;
          *param_2 = *param_2 + 1;
          iVar8 = tls12_check_peer_sigalg(param_1,uVar19 << 8 | uVar19 >> 8,lVar17);
          if (iVar8 < 1) goto LAB_001704a0;
        }
        iVar8 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x9a8),*(undefined8 *)(param_1 + 0x3b0),
                               &local_50);
        if (iVar8 == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_clnt.c",0x8d1,"tls_process_key_exchange");
          uVar22 = 0x129;
          goto LAB_00170f97;
        }
        if ((ushort *)0x1 < param_2[1]) {
          puVar13 = param_2[1] + -1;
          uVar19 = **param_2;
          puVar21 = (ushort *)(ulong)(ushort)(uVar19 << 8 | uVar19 >> 8);
          if (puVar21 <= puVar13) {
            puVar10 = *param_2 + 1;
            puVar13 = (ushort *)((long)puVar13 - (long)puVar21);
            param_2[1] = puVar13;
            *param_2 = (ushort *)((long)puVar10 + (long)puVar21);
            if (puVar13 == (ushort *)0x0) {
              lVar15 = EVP_MD_CTX_new();
              if (lVar15 == 0) {
                ERR_new();
                ERR_set_debug("ssl/statem/statem_clnt.c",0x8e1,"tls_process_key_exchange");
                ossl_statem_fatal(param_1,0x50,0xc0100,0);
              }
              else {
                uVar22 = (*(undefined8 **)(param_1 + 0x9a8))[0x88];
                uVar5 = **(undefined8 **)(param_1 + 0x9a8);
                lVar14 = local_50;
                if (local_50 != 0) {
                  lVar14 = EVP_MD_get0_name(local_50);
                }
                iVar8 = EVP_DigestVerifyInit_ex(lVar15,&local_58,lVar14,uVar5,uVar22,lVar17);
                if (iVar8 < 1) {
                  ERR_new();
                  uVar22 = 0x8e9;
                }
                else {
                  if (((*(long *)(param_1 + 0x3b0) == 0) ||
                      (*(int *)(*(long *)(param_1 + 0x3b0) + 0x14) != 0x390)) ||
                     ((iVar8 = EVP_PKEY_CTX_set_rsa_padding(local_58,6), 0 < iVar8 &&
                      (iVar8 = EVP_PKEY_CTX_set_rsa_pss_saltlen(local_58,0xffffffff), 0 < iVar8))))
                  {
                    lVar17 = construct_key_exchange_tbs(param_1,&local_48,puVar4,lVar23);
                    if (lVar17 != 0) {
                      iVar8 = EVP_DigestVerify(lVar15,puVar10,puVar21,local_48,lVar17);
                      CRYPTO_free(local_48);
                      if (0 < iVar8) {
                        EVP_MD_CTX_free(lVar15);
                        uVar22 = 3;
                        goto LAB_001704ad;
                      }
                      ERR_new();
                      ERR_set_debug("ssl/statem/statem_clnt.c",0x8ff,"tls_process_key_exchange");
                      ossl_statem_fatal(param_1,0x33,0x7b,0);
                    }
                    goto LAB_001704a3;
                  }
                  ERR_new();
                  uVar22 = 0x8f0;
                }
                ERR_set_debug("ssl/statem/statem_clnt.c",uVar22,"tls_process_key_exchange");
                ossl_statem_fatal(param_1,0x50,0x80006,0);
              }
              goto LAB_001704a3;
            }
          }
        }
        ERR_new();
        ERR_set_debug("ssl/statem/statem_clnt.c",0x8db,"tls_process_key_exchange");
        uVar22 = 0x9f;
        goto LAB_00170908;
      }
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x86d,"tls_process_ske_ecdhe");
      uVar22 = 0x132;
    }
    else {
LAB_00171090:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0x85b,"tls_process_ske_ecdhe");
      uVar22 = 0x17a;
    }
    lVar15 = 0;
    ossl_statem_fatal(param_1,0x2f,uVar22,0);
  }
LAB_001704a3:
  EVP_MD_CTX_free(lVar15);
  uVar22 = 0;
LAB_001704ad:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar22;
}