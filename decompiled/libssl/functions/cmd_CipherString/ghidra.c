bool cmd_CipherString(long param_1,char *param_2)

{
  int iVar1;
  
  iVar1 = 1;
  if (*(SSL_CTX **)(param_1 + 0x18) != (SSL_CTX *)0x0) {
    iVar1 = SSL_CTX_set_cipher_list(*(SSL_CTX **)(param_1 + 0x18),param_2);
  }
  if (*(SSL **)(param_1 + 0x20) != (SSL *)0x0) {
    iVar1 = SSL_set_cipher_list(*(SSL **)(param_1 + 0x20),param_2);
  }
  return 0 < iVar1;
}