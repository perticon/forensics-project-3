undefined1 * tls1_get_legacy_sigalg(long param_1,uint param_2)

{
  undefined1 *puVar1;
  uint uVar2;
  short sVar3;
  int iVar4;
  long lVar5;
  undefined1 *puVar6;
  ulong uVar7;
  
  uVar7 = (ulong)param_2;
  if (param_2 == 0xffffffff) {
    if (*(int *)(param_1 + 0x38) == 0) {
      uVar7 = (**(long **)(param_1 + 0x898) - (long)(*(long **)(param_1 + 0x898) + 4) >> 3) *
              -0x3333333333333333;
      param_2 = (uint)uVar7;
      goto LAB_00149456;
    }
    uVar7 = 0;
    while ((lVar5 = ssl_cert_lookup_by_idx(uVar7), lVar5 == 0 ||
           (uVar2 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x20),
           (*(uint *)(lVar5 + 4) & uVar2) == 0))) {
      uVar7 = uVar7 + 1;
      if (uVar7 == 9) {
        return (undefined1 *)0x0;
      }
    }
    param_2 = (uint)uVar7;
    if (uVar7 != 4) {
      if (param_2 == 5) {
        sVar3 = (*(long *)(*(long *)(param_1 + 0x898) + 0x118) != 0) + 0x840;
        goto LAB_00149488;
      }
      goto LAB_00149460;
    }
    if (uVar2 == 0x20) {
LAB_001495da:
      param_2 = 4;
    }
    else {
      lVar5 = *(long *)(param_1 + 0x898);
      if (*(long *)(lVar5 + 0x118) == 0) {
        if (*(long *)(lVar5 + 0xf0) == 0) {
          sVar3 = -0x1213;
          if (*(long *)(lVar5 + 200) == 0) goto LAB_00149488;
          goto LAB_001495da;
        }
        param_2 = 5;
      }
      else {
        param_2 = 6;
      }
    }
  }
  else {
LAB_00149456:
    if (8 < (uint)uVar7) {
      return (undefined1 *)0x0;
    }
LAB_00149460:
    if ((*(uint *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 2 | (uint)uVar7) == 0) {
      iVar4 = tls12_sigalg_allowed_part_0(param_1,0x5000b,legacy_rsa_sigalg);
      if (iVar4 == 0) {
        return (undefined1 *)0x0;
      }
      return legacy_rsa_sigalg;
    }
  }
  sVar3 = *(short *)(tls_default_sigalg + (long)(int)param_2 * 2);
LAB_00149488:
  puVar6 = *(undefined1 **)(*(long *)(param_1 + 0x9a8) + 0x620);
  puVar1 = puVar6 + 0x4d8;
  while (sVar3 != *(short *)(puVar6 + 8)) {
    puVar6 = puVar6 + 0x28;
    if (puVar6 == puVar1) {
      return (undefined1 *)0x0;
    }
  }
  if (*(int *)(puVar6 + 0x24) == 0) {
    return (undefined1 *)0x0;
  }
  iVar4 = tls1_lookup_md(*(long *)(param_1 + 0x9a8),puVar6,0);
  if (iVar4 != 0) {
    if (*(int *)(puVar6 + 0x24) == 0) {
      return (undefined1 *)0x0;
    }
    iVar4 = tls12_sigalg_allowed_part_0(param_1,0x5000b,puVar6);
    if (iVar4 != 0) {
      return puVar6;
    }
    return (undefined1 *)0x0;
  }
  return (undefined1 *)0x0;
}