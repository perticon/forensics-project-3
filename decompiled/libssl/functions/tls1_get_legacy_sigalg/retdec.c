int64_t tls1_get_legacy_sigalg(int64_t a1, int64_t a2) {
    int64_t v1 = a2; // 0x49450
    int64_t v2 = a2 & 0xffffffff; // 0x49450
    int16_t v3; // 0x49440
    int64_t v4; // 0x49440
    int64_t v5; // 0x49440
    int32_t v6; // 0x49440
    if ((int32_t)a2 == -1) {
        // 0x49500
        if (*(int32_t *)(a1 + 56) != 0) {
            int64_t v7 = 0;
            int64_t v8 = ssl_cert_lookup_by_idx(); // 0x49513
            int32_t v9; // 0x49524
            int64_t v10; // 0x49609
            int16_t v11; // 0x49617
            int64_t v12; // 0x495ac
            if (v8 != 0) {
                // 0x4951d
                v9 = *(int32_t *)(*(int64_t *)(a1 + (int64_t)&g80) + 32);
                if ((*(int32_t *)(v8 + 4) & v9) != 0) {
                    if (v7 != 4) {
                        // 0x495f8
                        v6 = v7;
                        v4 = v7 & 0xffffffff;
                        if (v6 != 5) {
                            goto lab_0x49460;
                        } else {
                            // 0x49602
                            v10 = *(int64_t *)(*(int64_t *)(a1 + (int64_t)&g297) + 280);
                            v11 = v10 != 0 ? (int16_t)&g264 + 1 : (int16_t)&g264;
                            v3 = v11;
                            goto lab_0x49488;
                        }
                    } else {
                        // 0x495a7
                        v5 = 4;
                        if (v9 == 32) {
                            goto lab_0x4947a;
                        } else {
                            // 0x495ac
                            v12 = *(int64_t *)(a1 + (int64_t)&g297);
                            v5 = 6;
                            if (*(int64_t *)(v12 + 280) != 0) {
                                goto lab_0x4947a;
                            } else {
                                // 0x495bd
                                v5 = 5;
                                if (*(int64_t *)(v12 + 240) != 0) {
                                    goto lab_0x4947a;
                                } else {
                                    // 0x495c7
                                    v5 = 4;
                                    v3 = -0x1213;
                                    if (*(int64_t *)(v12 + 200) == 0) {
                                        goto lab_0x49488;
                                    } else {
                                        goto lab_0x4947a;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            int64_t v13 = v7 + 1; // 0x49534
            while (v7 != 8) {
                // 0x49510
                v7 = v13;
                v8 = ssl_cert_lookup_by_idx();
                if (v8 != 0) {
                    // 0x4951d
                    v9 = *(int32_t *)(*(int64_t *)(a1 + (int64_t)&g80) + 32);
                    if ((*(int32_t *)(v8 + 4) & v9) != 0) {
                        if (v7 != 4) {
                            // 0x495f8
                            v6 = v7;
                            v4 = v7 & 0xffffffff;
                            if (v6 != 5) {
                                goto lab_0x49460;
                            } else {
                                // 0x49602
                                v10 = *(int64_t *)(*(int64_t *)(a1 + (int64_t)&g297) + 280);
                                v11 = v10 != 0 ? (int16_t)&g264 + 1 : (int16_t)&g264;
                                v3 = v11;
                                goto lab_0x49488;
                            }
                        } else {
                            // 0x495a7
                            v5 = 4;
                            if (v9 == 32) {
                                goto lab_0x4947a;
                            } else {
                                // 0x495ac
                                v12 = *(int64_t *)(a1 + (int64_t)&g297);
                                v5 = 6;
                                if (*(int64_t *)(v12 + 280) != 0) {
                                    goto lab_0x4947a;
                                } else {
                                    // 0x495bd
                                    v5 = 5;
                                    if (*(int64_t *)(v12 + 240) != 0) {
                                        goto lab_0x4947a;
                                    } else {
                                        // 0x495c7
                                        v5 = 4;
                                        v3 = -0x1213;
                                        if (*(int64_t *)(v12 + 200) == 0) {
                                            goto lab_0x49488;
                                        } else {
                                            goto lab_0x4947a;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // 0x4952c
                v13 = v7 + 1;
            }
            // 0x494eb
            return 0;
        }
        int64_t v14 = *(int64_t *)(a1 + (int64_t)&g297); // 0x49540
        v2 = -0x3333333333333333 * (-32 - v14 + *(int64_t *)v14 >> 3);
        v1 = v2 & 0xffffffff;
    }
    int32_t v15 = v2;
    v6 = v15;
    v4 = v1;
    if (v15 >= 9) {
        // 0x494eb
        return 0;
    }
    goto lab_0x49460;
  lab_0x49460:;
    int64_t v16 = *(int64_t *)(*(int64_t *)(a1 + 8) + 192); // 0x49464
    v5 = v4;
    if ((*(int32_t *)(v16 + 96) & 2 || v6) == 0) {
        // 0x49570
        if ((int32_t)tls12_sigalg_allowed_part_0(a1, 0x5000b, (int64_t)&g399) == 0) {
            // 0x494eb
            return 0;
        }
        // 0x494eb
        return (int64_t)&g399;
    }
    goto lab_0x4947a;
  lab_0x4947a:;
    int16_t v17 = *(int16_t *)((0x100000000 * v5 >> 31) + (int64_t)&g283); // 0x49484
    v3 = v17;
    goto lab_0x49488;
  lab_0x49488:;
    int64_t v18 = *(int64_t *)(a1 + (int64_t)&g364); // 0x49488
    int64_t v19 = *(int64_t *)(v18 + (int64_t)&g221); // 0x4948f
    int64_t result = v19;
    while (*(int16_t *)(result + 8) != v3) {
        int64_t v20 = result + 40; // 0x494a0
        if (v20 == v19 + (int64_t)&g192) {
            // 0x494eb
            return 0;
        }
        result = v20;
    }
    int32_t * v21 = (int32_t *)(result + 36); // 0x494b1
    if (*v21 == 0 || (int32_t)tls1_lookup_md() == 0 || *v21 == 0) {
        // 0x494eb
        return 0;
    }
    // 0x494d1
    if ((int32_t)tls12_sigalg_allowed_part_0(a1, 0x5000b, result) != 0) {
        // 0x494eb
        return result;
    }
    // 0x494eb
    return 0;
}