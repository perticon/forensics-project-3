void** tls1_get_legacy_sigalg(void** rdi, uint32_t esi, ...) {
    int64_t r12_3;
    void** rbp4;
    struct s6* rax5;
    void** r12_6;
    int32_t eax7;
    uint32_t eax8;
    uint32_t eax9;
    void** rdi10;
    void** rdx11;
    void** eax12;
    int32_t eax13;

    *reinterpret_cast<uint32_t*>(&r12_3) = esi;
    rbp4 = rdi;
    if (esi == 0xffffffff) {
        if (!*reinterpret_cast<void***>(rdi + 56)) {
            r12_3 = (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 0x898))) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 0x898) + 32)) >> 3) * 0xcccccccccccccccd;
            esi = *reinterpret_cast<uint32_t*>(&r12_3);
            goto addr_49456_4;
        } else {
            *reinterpret_cast<uint32_t*>(&r12_3) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_3) + 4) = 0;
            do {
                rax5 = ssl_cert_lookup_by_idx(r12_3);
                if (!rax5) 
                    continue;
                if (rax5->f4 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 0x2e0) + 32))) 
                    break;
                ++r12_3;
            } while (r12_3 != 9);
            goto addr_49536_9;
        }
    } else {
        addr_49456_4:
        if (*reinterpret_cast<uint32_t*>(&r12_3) > 8) {
            addr_494e8_10:
            *reinterpret_cast<int32_t*>(&r12_6) = 0;
            *reinterpret_cast<int32_t*>(&r12_6 + 4) = 0;
            goto addr_494eb_11;
        } else {
            addr_49460_12:
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 8) + 0xc0) + 96)) & 2 | *reinterpret_cast<uint32_t*>(&r12_3))) {
                eax7 = tls12_sigalg_allowed_part_0(rbp4, 0x5000b, 0xa3460);
                if (eax7) {
                    return 0xa3460;
                }
            } else {
                addr_4947a_15:
                eax8 = *reinterpret_cast<uint16_t*>(0x885e0 + esi * 2);
                goto addr_49488_16;
            }
        }
    }
    esi = *reinterpret_cast<uint32_t*>(&r12_3);
    if (r12_3 != 4) {
        if (*reinterpret_cast<uint32_t*>(&r12_3) != 5) 
            goto addr_49460_12;
        eax9 = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 0x898) + 0x118));
        *reinterpret_cast<void***>(&eax8) = reinterpret_cast<void**>(&(*reinterpret_cast<struct s8**>(&eax9))->f840);
    } else {
        if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 0x2e0) + 32) == 32) {
            addr_495da_21:
            esi = 4;
            goto addr_4947a_15;
        } else {
            if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 0x898) + 0x118)) {
                esi = 6;
                goto addr_4947a_15;
            }
            if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 0x898) + 0xf0)) {
                esi = 5;
                goto addr_4947a_15;
            }
            eax8 = 0xffffeded;
            if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 0x898) + 0xc8)) 
                goto addr_495da_21;
        }
    }
    addr_49488_16:
    rdi10 = *reinterpret_cast<void***>(rbp4 + 0x9a8);
    r12_6 = *reinterpret_cast<void***>(rdi10 + 0x620);
    rdx11 = r12_6 + 0x4d8;
    do {
        if (*reinterpret_cast<void***>(&eax8) == *reinterpret_cast<void***>(r12_6 + 8)) 
            break;
        r12_6 = r12_6 + 40;
    } while (r12_6 != rdx11);
    goto addr_494e8_10;
    if (*reinterpret_cast<void***>(r12_6 + 36) && ((eax12 = tls1_lookup_md(rdi10, r12_6), !!eax12) && *reinterpret_cast<void***>(r12_6 + 36))) {
        eax13 = tls12_sigalg_allowed_part_0(rbp4, 0x5000b, r12_6);
        if (eax13) {
            addr_494eb_11:
            return r12_6;
        } else {
            goto addr_494e8_10;
        }
    }
    addr_49536_9:
    goto addr_494e8_10;
}