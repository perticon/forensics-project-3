undefined8 ssl_setup_sig_algs(undefined8 *param_1)

{
  undefined4 uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  int iVar4;
  undefined4 *ptr;
  EVP_PKEY *pkey;
  EVP_PKEY_CTX *ctx;
  undefined1 *puVar5;
  undefined4 *puVar6;
  undefined8 uVar7;
  
  ptr = (undefined4 *)CRYPTO_malloc(0x4d8,"ssl/t1_lib.c",0x4b5);
  pkey = EVP_PKEY_new();
  if ((ptr == (undefined4 *)0x0) || (pkey == (EVP_PKEY *)0x0)) {
    uVar7 = 0;
  }
  else {
    ERR_set_mark();
    puVar5 = sigalg_lookup_tbl;
    puVar6 = ptr;
    do {
      while( true ) {
        uVar1 = *(undefined4 *)((long)puVar5 + 4);
        uVar2 = *(undefined4 *)((long)puVar5 + 8);
        uVar3 = *(undefined4 *)((long)puVar5 + 0xc);
        *puVar6 = *(undefined4 *)puVar5;
        puVar6[1] = uVar1;
        puVar6[2] = uVar2;
        puVar6[3] = uVar3;
        uVar1 = *(undefined4 *)((long)puVar5 + 0x14);
        uVar2 = *(undefined4 *)((long)puVar5 + 0x18);
        uVar3 = *(undefined4 *)((long)puVar5 + 0x1c);
        puVar6[4] = *(undefined4 *)((long)puVar5 + 0x10);
        puVar6[5] = uVar1;
        puVar6[6] = uVar2;
        puVar6[7] = uVar3;
        *(undefined8 *)(puVar6 + 8) = *(undefined8 *)((long)puVar5 + 0x20);
        if ((*(int *)((long)puVar5 + 0xc) == 0) ||
           (param_1[(long)*(int *)((long)puVar5 + 0x10) + 0xa8] != 0)) break;
LAB_00148de0:
        puVar5 = (undefined1 *)((long)puVar5 + 0x28);
        puVar6[9] = 0;
        puVar6 = puVar6 + 10;
        if (puVar5 == &DAT_001a3978) goto LAB_00148df4;
      }
      iVar4 = EVP_PKEY_set_type(pkey,*(int *)((long)puVar5 + 0x14));
      if (iVar4 == 0) goto LAB_00148de0;
      ctx = (EVP_PKEY_CTX *)EVP_PKEY_CTX_new_from_pkey(*param_1,pkey,param_1[0x88]);
      if (ctx == (EVP_PKEY_CTX *)0x0) {
        puVar6[9] = 0;
      }
      EVP_PKEY_CTX_free(ctx);
      puVar5 = (undefined1 *)((long)puVar5 + 0x28);
      puVar6 = puVar6 + 10;
    } while (puVar5 != &DAT_001a3978);
LAB_00148df4:
    ERR_pop_to_mark();
    param_1[0xc4] = ptr;
    ptr = (undefined4 *)0x0;
    uVar7 = 1;
  }
  CRYPTO_free(ptr);
  EVP_PKEY_free(pkey);
  return uVar7;
}