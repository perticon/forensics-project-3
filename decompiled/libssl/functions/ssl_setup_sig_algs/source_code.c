int ssl_setup_sig_algs(SSL_CTX *ctx)
{
    size_t i;
    const SIGALG_LOOKUP *lu;
    SIGALG_LOOKUP *cache
        = OPENSSL_malloc(sizeof(*lu) * OSSL_NELEM(sigalg_lookup_tbl));
    EVP_PKEY *tmpkey = EVP_PKEY_new();
    int ret = 0;

    if (cache == NULL || tmpkey == NULL)
        goto err;

    ERR_set_mark();
    for (i = 0, lu = sigalg_lookup_tbl;
         i < OSSL_NELEM(sigalg_lookup_tbl); lu++, i++) {
        EVP_PKEY_CTX *pctx;

        cache[i] = *lu;

        /*
         * Check hash is available.
         * This test is not perfect. A provider could have support
         * for a signature scheme, but not a particular hash. However the hash
         * could be available from some other loaded provider. In that case it
         * could be that the signature is available, and the hash is available
         * independently - but not as a combination. We ignore this for now.
         */
        if (lu->hash != NID_undef
                && ctx->ssl_digest_methods[lu->hash_idx] == NULL) {
            cache[i].enabled = 0;
            continue;
        }

        if (!EVP_PKEY_set_type(tmpkey, lu->sig)) {
            cache[i].enabled = 0;
            continue;
        }
        pctx = EVP_PKEY_CTX_new_from_pkey(ctx->libctx, tmpkey, ctx->propq);
        /* If unable to create pctx we assume the sig algorithm is unavailable */
        if (pctx == NULL)
            cache[i].enabled = 0;
        EVP_PKEY_CTX_free(pctx);
    }
    ERR_pop_to_mark();
    ctx->sigalg_lookup_cache = cache;
    cache = NULL;

    ret = 1;
 err:
    OPENSSL_free(cache);
    EVP_PKEY_free(tmpkey);
    return ret;
}