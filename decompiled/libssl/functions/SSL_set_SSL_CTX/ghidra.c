SSL_CTX * SSL_set_SSL_CTX(SSL *ssl,SSL_CTX *ctx)

{
  COMP_CTX *__n;
  long lVar1;
  undefined4 uVar2;
  uint uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  int iVar6;
  dtls1_state_st *pdVar7;
  
  if (*(SSL_CTX **)&ssl[3].ex_data.dummy == ctx) {
    return ctx;
  }
  if (ctx == (SSL_CTX *)0x0) {
    ctx = *(SSL_CTX **)&ssl[4].mac_flags;
  }
  pdVar7 = (dtls1_state_st *)ssl_cert_dup(*(undefined8 *)ctx->sid_ctx);
  if (pdVar7 != (dtls1_state_st *)0x0) {
    iVar6 = custom_exts_copy_flags(pdVar7->rcvd_cookie + 0xd4,(ssl[3].d1)->rcvd_cookie + 0xd4);
    if (iVar6 == 0) {
      ssl_cert_free(pdVar7);
    }
    else {
      ssl_cert_free(ssl[3].d1);
      __n = ssl[3].expand;
      ssl[3].d1 = pdVar7;
      if (__n < (COMP_CTX *)0x21) {
        lVar1 = *(long *)&ssl[3].ex_data.dummy;
        if ((lVar1 != 0) && (__n == *(COMP_CTX **)(lVar1 + 0x180))) {
          iVar6 = memcmp(&ssl[3].enc_write_ctx,(void *)(lVar1 + 0x188),(size_t)__n);
          if (iVar6 == 0) {
            ssl[3].expand = (COMP_CTX *)ctx->generate_session_id;
            uVar2 = *(undefined4 *)((long)&ctx->param + 4);
            iVar6 = ctx->quiet_shutdown;
            uVar3 = ctx->max_send_fragment;
            *(undefined4 *)&ssl[3].enc_write_ctx = *(undefined4 *)&ctx->param;
            *(undefined4 *)((long)&ssl[3].enc_write_ctx + 4) = uVar2;
            *(int *)&ssl[3].write_hash = iVar6;
            *(uint *)((long)&ssl[3].write_hash + 4) = uVar3;
            uVar2 = *(undefined4 *)((long)&ctx->client_cert_engine + 4);
            uVar4 = *(undefined4 *)&ctx->tlsext_servername_callback;
            uVar5 = *(undefined4 *)((long)&ctx->tlsext_servername_callback + 4);
            *(undefined4 *)&ssl[3].compress = *(undefined4 *)&ctx->client_cert_engine;
            *(undefined4 *)((long)&ssl[3].compress + 4) = uVar2;
            *(undefined4 *)&ssl[3].cert = uVar4;
            *(undefined4 *)((long)&ssl[3].cert + 4) = uVar5;
          }
        }
        SSL_CTX_up_ref(ctx);
        SSL_CTX_free(*(SSL_CTX **)&ssl[3].ex_data.dummy);
        *(SSL_CTX **)&ssl[3].ex_data.dummy = ctx;
        return ctx;
      }
    }
  }
  return (SSL_CTX *)0x0;
}