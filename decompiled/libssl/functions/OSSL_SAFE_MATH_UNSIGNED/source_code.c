OSSL_SAFE_MATH_UNSIGNED(size_t, size_t)

/*
 * Fundamental operations:
 *                        Binary Heap   Fibonacci Heap
 *  Get smallest            O(1)          O(1)
 *  Delete any              O(log n)      O(log n) average but worst O(n)
 *  Insert                  O(log n)      O(1)
 *
 * Not supported:
 *  Merge two structures    O(log n)      O(1)
 *  Decrease key            O(log n)      O(1)
 *  Increase key            O(log n)      ?
 *
 * The Fibonacci heap is quite a bit more complicated to implement and has
 * larger overhead in practice.  We favour the binary heap here.  A multi-way
 * (ternary or quaternary) heap might elicit a performance advantage via better
 * cache access patterns.
 */

struct pq_heap_st {
    void *data;     /* User supplied data pointer */
    size_t index;   /* Constant index in elements[] */
}