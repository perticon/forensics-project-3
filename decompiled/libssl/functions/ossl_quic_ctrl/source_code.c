long ossl_quic_ctrl(SSL *s, int cmd, long larg, void *parg)
{
    switch(cmd) {
    case SSL_CTRL_CHAIN:
        if (larg)
            return ssl_cert_set1_chain(s, NULL, (STACK_OF(X509) *)parg);
        else
            return ssl_cert_set0_chain(s, NULL, (STACK_OF(X509) *)parg);
    }
    return 0;
}