int SSL_read_ex(void)

{
  int iVar1;
  
  iVar1 = ssl_read_internal();
  if (iVar1 < 0) {
    iVar1 = 0;
  }
  return iVar1;
}