SSL_get_shared_sigalgs
          (long param_1,int param_2,undefined4 *param_3,undefined4 *param_4,undefined4 *param_5,
          undefined *param_6,undefined *param_7)

{
  long lVar1;
  
  if ((((*(long *)(param_1 + 0x1da8) != 0) && (-1 < param_2)) &&
      (*(ulong *)(param_1 + 0x1db0) < 0x80000000)) && (param_2 < (int)*(ulong *)(param_1 + 0x1db0)))
  {
    lVar1 = *(long *)(*(long *)(param_1 + 0x1da8) + (long)param_2 * 8);
    if (param_4 != (undefined4 *)0x0) {
      *param_4 = *(undefined4 *)(lVar1 + 0xc);
    }
    if (param_3 != (undefined4 *)0x0) {
      *param_3 = *(undefined4 *)(lVar1 + 0x14);
    }
    if (param_5 != (undefined4 *)0x0) {
      *param_5 = *(undefined4 *)(lVar1 + 0x1c);
    }
    if (param_6 != (undefined *)0x0) {
      *param_6 = (char)*(undefined2 *)(lVar1 + 8);
    }
    if (param_7 != (undefined *)0x0) {
      *param_7 = *(undefined *)(lVar1 + 9);
    }
    return *(undefined4 *)(param_1 + 0x1db0);
  }
  return 0;
}