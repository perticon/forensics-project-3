int SSL_get_peer_signature_type_nid(const SSL *s, int *pnid)
{
    if (s->s3.tmp.peer_sigalg == NULL)
        return 0;
    *pnid = s->s3.tmp.peer_sigalg->sig;
    return 1;
}