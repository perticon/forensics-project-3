int tls13_setup_key_block(long param_1)

{
  long lVar1;
  undefined8 uVar2;
  int iVar3;
  long in_FS_OFFSET;
  undefined8 local_20;
  undefined8 local_18;
  long local_10;
  
  lVar1 = *(long *)(param_1 + 0x918);
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = *(undefined8 *)(param_1 + 0x9a8);
  *(undefined8 *)(lVar1 + 0x2f8) = *(undefined8 *)(param_1 + 0x2e0);
  iVar3 = ssl_cipher_get_evp(uVar2,lVar1,&local_20,&local_18,0,0);
  if (iVar3 == 0) {
    ossl_statem_send_fatal(param_1,0x50,0,0);
  }
  else {
    ssl_evp_cipher_free(*(undefined8 *)(param_1 + 800));
    *(undefined8 *)(param_1 + 800) = local_20;
    ssl_evp_md_free(*(undefined8 *)(param_1 + 0x328));
    *(undefined8 *)(param_1 + 0x328) = local_18;
    iVar3 = 1;
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}