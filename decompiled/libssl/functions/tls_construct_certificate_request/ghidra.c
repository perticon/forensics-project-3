int tls_construct_certificate_request(long param_1,undefined8 param_2)

{
  int iVar1;
  void *pvVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  undefined8 local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
      (iVar1 = **(int **)(param_1 + 8), 0x303 < iVar1)) && (iVar1 != 0x10000)) {
    if (*(int *)(param_1 + 0xba8) != 3) {
      iVar1 = WPACKET_put_bytes__(param_2,0,1);
      if (iVar1 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0xac2,"tls_construct_certificate_request");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        goto LAB_00179a56;
      }
LAB_001799cb:
      iVar1 = tls_construct_extensions(param_1,param_2,0x4000,0,0);
      goto joined_r0x00179bd8;
    }
    CRYPTO_free(*(void **)(param_1 + 0xbb0));
    *(undefined8 *)(param_1 + 3000) = 0x20;
    pvVar2 = CRYPTO_malloc(0x20,"ssl/statem/statem_srvr.c",0xaaf);
    *(void **)(param_1 + 0xbb0) = pvVar2;
    if (pvVar2 == (void *)0x0) {
      *(undefined8 *)(param_1 + 3000) = 0;
      ERR_new();
      uVar3 = 0xab1;
LAB_00179a31:
      ERR_set_debug("ssl/statem/statem_srvr.c",uVar3,"tls_construct_certificate_request");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
    else {
      iVar1 = RAND_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),pvVar2,
                            *(undefined8 *)(param_1 + 3000),0);
      if ((iVar1 < 1) ||
         (iVar1 = WPACKET_sub_memcpy__
                            (param_2,*(undefined8 *)(param_1 + 0xbb0),
                             *(undefined8 *)(param_1 + 3000),1), iVar1 == 0)) {
        ERR_new();
        uVar3 = 0xab8;
        goto LAB_00179a31;
      }
      iVar1 = tls13_restore_handshake_digest_for_pha(param_1);
      if (iVar1 != 0) goto LAB_001799cb;
    }
  }
  else {
    iVar1 = WPACKET_start_sub_packet_len__(param_2,1);
    if (((iVar1 == 0) || (iVar1 = ssl3_get_req_cert_type(param_1,param_2), iVar1 == 0)) ||
       (iVar1 = WPACKET_close(param_2), iVar1 == 0)) {
      ERR_new();
      uVar3 = 0xad3;
      goto LAB_00179a31;
    }
    if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 2) != 0) {
      uVar3 = tls12_get_psigalgs(param_1,1,&local_28);
      iVar1 = WPACKET_start_sub_packet_len__(param_2,2);
      if (((iVar1 == 0) || (iVar1 = WPACKET_set_flags(param_2,1), iVar1 == 0)) ||
         ((iVar1 = tls12_copy_sigalgs(param_1,param_2,local_28,uVar3), iVar1 == 0 ||
          (iVar1 = WPACKET_close(param_2), iVar1 == 0)))) {
        ERR_new();
        uVar3 = 0xadf;
        goto LAB_00179a31;
      }
    }
    uVar3 = get_ca_names(param_1);
    iVar1 = construct_ca_names(param_1,uVar3,param_2);
joined_r0x00179bd8:
    if (iVar1 != 0) {
      *(int *)(param_1 + 0xbc0) = *(int *)(param_1 + 0xbc0) + 1;
      iVar1 = 1;
      *(undefined4 *)(param_1 + 0x348) = 1;
      goto LAB_00179a56;
    }
  }
  iVar1 = 0;
LAB_00179a56:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}