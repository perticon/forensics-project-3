uint tls_construct_stoc_status_request
               (long param_1,undefined8 param_2,int param_3,undefined8 param_4,long param_5)

{
  int iVar1;
  undefined8 uVar2;
  uint uVar3;
  
  if (param_3 == 0x4000) {
    return 2;
  }
  uVar3 = 2;
  if ((*(int *)(param_1 + 0xa74) != 0) &&
     ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0 ||
       (iVar1 = **(int **)(param_1 + 8), iVar1 < 0x304 || iVar1 == 0x10000)) || (param_5 == 0)))) {
    iVar1 = WPACKET_put_bytes__(param_2,5,2);
    if ((iVar1 == 0) || (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 == 0)) {
      ERR_new();
      uVar2 = 0x58d;
LAB_00168601:
      ERR_set_debug("ssl/statem/extensions_srvr.c",uVar2,"tls_construct_stoc_status_request");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    }
    uVar3 = *(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8;
    if ((uVar3 != 0) ||
       (((iVar1 = **(int **)(param_1 + 8), iVar1 == 0x10000 || (iVar1 < 0x304)) ||
        (iVar1 = tls_construct_cert_status_body(param_1,param_2), iVar1 != 0)))) {
      iVar1 = WPACKET_close(param_2);
      if (iVar1 == 0) {
        ERR_new();
        uVar2 = 0x59b;
        goto LAB_00168601;
      }
      uVar3 = 1;
    }
  }
  return uVar3;
}