SSL * SSL_dup(SSL *ssl)

{
  tls_session_secret_cb_fn ptVar1;
  X509_VERIFY_PARAM *to;
  X509_VERIFY_PARAM *from;
  _func_3149 *p_Var2;
  int iVar3;
  int extraout_EAX;
  int iVar4;
  uint yes;
  int iVar5;
  SSL_CTX *pSVar6;
  SSL *s;
  uchar *puVar7;
  undefined *puVar8;
  long larg;
  undefined4 extraout_var;
  cb *cb;
  dtls1_state_st *pdVar9;
  int val;
  X509_STORE_CTX *pXVar10;
  
  iVar3 = SSL_in_init();
  if ((iVar3 == 0) || (iVar3 = SSL_in_before(ssl), iVar3 == 0)) {
    LOCK();
    ssl[3].max_send_fragment = ssl[3].max_send_fragment + 1;
    return ssl;
  }
  pSVar6 = SSL_get_SSL_CTX(ssl);
  s = SSL_new(pSVar6);
  if (s == (SSL *)0x0) {
    return (SSL *)0x0;
  }
  if (*(long *)&ssl[3].sid_ctx_length == 0) {
    iVar3 = SSL_set_ssl_method(s,ssl->method);
    if (iVar3 == 0) goto LAB_0013e9dd;
    if (ssl[3].d1 != (dtls1_state_st *)0x0) {
      ssl_cert_free(s[3].d1);
      pdVar9 = (dtls1_state_st *)ssl_cert_dup(ssl[3].d1);
      s[3].d1 = pdVar9;
      if (pdVar9 == (dtls1_state_st *)0x0) goto LAB_0013e9dd;
    }
    iVar3 = SSL_set_session_id_context(s,(uchar *)&ssl[3].enc_write_ctx,*(uint *)&ssl[3].expand);
  }
  else {
    SSL_copy_session_id(s,ssl);
    iVar3 = extraout_EAX;
  }
  if (iVar3 != 0) {
    iVar3 = OPENSSL_sk_num(ssl[1].tlsext_ellipticcurvelist);
    if (0 < iVar3) {
      iVar3 = OPENSSL_sk_num(ssl[1].tlsext_ellipticcurvelist);
      OPENSSL_sk_pop_free(s[1].tlsext_ellipticcurvelist,tlsa_free);
      s[1].tlsext_ellipticcurvelist = (uchar *)0x0;
      OSSL_STACK_OF_X509_free(s[1].tlsext_opaque_prf_input);
      s[1].tlsext_opaque_prf_input = (void *)0x0;
      X509_free((X509 *)s[1].tlsext_session_ticket);
      *(undefined (*) [16])&s[1].tlsext_opaque_prf_input_len = (undefined  [16])0x0;
      ptVar1 = ssl[1].tls_session_secret_cb;
      *(undefined8 *)((long)&s[1].tls_session_ticket_ext_cb + 4) = 0xffffffffffffffff;
      s[1].tls_session_secret_cb = ptVar1;
      s[1].tlsext_ellipticcurvelist_length = *(long *)&s[3].ex_data.dummy + 0x398;
      puVar7 = (uchar *)OPENSSL_sk_new_reserve(0,0,iVar3);
      s[1].tlsext_ellipticcurvelist = puVar7;
      if (puVar7 == (uchar *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_lib.c",0xcb,"ssl_dane_dup");
        ERR_set_error(0x14,0xc0100,0);
        goto LAB_0013e9dd;
      }
      iVar5 = 0;
      if (0 < iVar3) {
        do {
          puVar8 = (undefined *)OPENSSL_sk_value(ssl[1].tlsext_ellipticcurvelist,iVar5);
          iVar4 = SSL_dane_tlsa_add(s,*puVar8,puVar8[1],puVar8[2],*(undefined8 *)(puVar8 + 8),
                                    *(undefined8 *)(puVar8 + 0x10));
          if (iVar4 < 1) goto LAB_0013e9dd;
          iVar5 = iVar5 + 1;
        } while (iVar3 != iVar5);
      }
    }
    s->version = ssl->version;
    s[3].tlsext_debug_cb = ssl[3].tlsext_debug_cb;
    *(undefined4 *)((long)&s[3].tlsext_debug_arg + 4) =
         *(undefined4 *)((long)&ssl[3].tlsext_debug_arg + 4);
    *(undefined4 *)&s[3].tlsext_hostname = *(undefined4 *)&ssl[3].tlsext_hostname;
    *(undefined4 *)&s[3].tlsext_debug_arg = *(undefined4 *)&ssl[3].tlsext_debug_arg;
    larg = SSL_ctrl(ssl,0x32,0,(void *)0x0);
    SSL_ctrl(s,0x33,larg,(void *)0x0);
    yes = SSL_get_read_ahead(ssl);
    pXVar10 = (X509_STORE_CTX *)(ulong)yes;
    SSL_set_read_ahead(s,yes);
    s[1].tlsext_ocsp_resp = ssl[1].tlsext_ocsp_resp;
    *(undefined8 *)&s[1].tlsext_ocsp_resplen = *(undefined8 *)&ssl[1].tlsext_ocsp_resplen;
    iVar3 = SSL_get_verify_callback((int)ssl,pXVar10);
    iVar5 = SSL_get_verify_mode(ssl);
    SSL_set_verify(s,iVar5,(callback *)CONCAT44(extraout_var,iVar3));
    iVar3 = SSL_get_verify_depth(ssl);
    SSL_set_verify_depth(s,iVar3);
    *(undefined8 *)(s[3].sid_ctx + 0x1c) = *(undefined8 *)(ssl[3].sid_ctx + 0x1c);
    SSL_get_info_callback(ssl,iVar3,val);
    SSL_set_info_callback(s,cb);
    iVar3 = CRYPTO_dup_ex_data(0,(CRYPTO_EX_DATA *)&s[3].options,(CRYPTO_EX_DATA *)&ssl[3].options);
    if (iVar3 != 0) {
      iVar3 = ssl->server;
      p_Var2 = ssl->handshake_func;
      s->server = iVar3;
      if (p_Var2 != (_func_3149 *)0x0) {
        if (iVar3 == 0) {
          SSL_set_connect_state(s);
        }
        else {
          SSL_set_accept_state(s);
        }
      }
      to = (X509_VERIFY_PARAM *)s[1].tlsext_ecpointformatlist;
      from = (X509_VERIFY_PARAM *)ssl[1].tlsext_ecpointformatlist;
      s->shutdown = ssl->shutdown;
      *(undefined4 *)&s[1].tlsext_ecpointformatlist_length =
           *(undefined4 *)&ssl[1].tlsext_ecpointformatlist_length;
      s[10].tlsext_ellipticcurvelist = ssl[10].tlsext_ellipticcurvelist;
      s[10].tlsext_opaque_prf_input = ssl[10].tlsext_opaque_prf_input;
      X509_VERIFY_PARAM_inherit(to,from);
      if (ssl[1].initial_ctx != (SSL_CTX *)0x0) {
        pSVar6 = (SSL_CTX *)OPENSSL_sk_dup();
        s[1].initial_ctx = pSVar6;
        if (pSVar6 == (SSL_CTX *)0x0) goto LAB_0013e9dd;
      }
      if (ssl[1].next_proto_negotiated != (uchar *)0x0) {
        puVar7 = (uchar *)OPENSSL_sk_dup();
        s[1].next_proto_negotiated = puVar7;
        if (puVar7 == (uchar *)0x0) goto LAB_0013e9dd;
      }
      if (ssl[3].max_cert_list == 0) {
        s[3].max_cert_list = 0;
      }
      else {
        iVar3 = dup_ca_names_part_0(&s[3].max_cert_list);
        if (iVar3 == 0) goto LAB_0013e9dd;
      }
      if (*(long *)&ssl[3].first_packet == 0) {
        *(undefined8 *)&s[3].first_packet = 0;
      }
      else {
        iVar3 = dup_ca_names_part_0(&s[3].first_packet);
        if (iVar3 == 0) goto LAB_0013e9dd;
      }
      return s;
    }
  }
LAB_0013e9dd:
  SSL_free(s);
  return (SSL *)0x0;
}