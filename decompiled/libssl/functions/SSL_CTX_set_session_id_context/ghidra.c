int SSL_CTX_set_session_id_context(SSL_CTX *ctx,uchar *sid_ctx,uint sid_ctx_len)

{
  GEN_SESSION_CB pGVar1;
  uint uVar2;
  long lVar3;
  ulong uVar4;
  uint uVar5;
  ulong uVar6;
  
  if (sid_ctx_len < 0x21) {
    pGVar1 = (GEN_SESSION_CB)(ulong)sid_ctx_len;
    ctx->generate_session_id = pGVar1;
    if (sid_ctx_len < 8) {
      if ((sid_ctx_len & 4) == 0) {
        if ((sid_ctx_len != 0) && (*(uchar *)&ctx->param = *sid_ctx, (sid_ctx_len & 2) != 0)) {
          *(undefined2 *)(pGVar1 + (long)((long)&ctx->generate_session_id + 6)) =
               *(undefined2 *)(sid_ctx + -2 + (long)pGVar1);
        }
      }
      else {
        *(undefined4 *)&ctx->param = *(undefined4 *)sid_ctx;
        *(undefined4 *)(pGVar1 + (long)((long)&ctx->generate_session_id + 4)) =
             *(undefined4 *)(sid_ctx + -4 + (long)pGVar1);
      }
    }
    else {
      ctx->param = *(X509_VERIFY_PARAM **)sid_ctx;
      uVar6 = (ulong)&ctx->quiet_shutdown & 0xfffffffffffffff8;
      *(undefined8 *)((long)&ctx->generate_session_id + (long)pGVar1) =
           *(undefined8 *)(sid_ctx + -8 + (long)pGVar1);
      lVar3 = (long)ctx + (0x188 - uVar6);
      uVar5 = sid_ctx_len + (int)lVar3 & 0xfffffff8;
      if (7 < uVar5) {
        uVar2 = 0;
        do {
          uVar4 = (ulong)uVar2;
          uVar2 = uVar2 + 8;
          *(undefined8 *)(uVar6 + uVar4) = *(undefined8 *)(sid_ctx + (uVar4 - lVar3));
        } while (uVar2 < uVar5);
      }
    }
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x36e,"SSL_CTX_set_session_id_context");
  ERR_set_error(0x14,0x111,0);
  return 0;
}