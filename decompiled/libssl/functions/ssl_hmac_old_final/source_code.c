int ssl_hmac_old_final(SSL_HMAC *ctx, unsigned char *md, size_t *len)
{
    unsigned int l;

    if (HMAC_Final(ctx->old_ctx, md, &l) > 0) {
        if (len != NULL)
            *len = l;
        return 1;
    }

    return 0;
}