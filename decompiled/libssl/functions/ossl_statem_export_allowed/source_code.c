int ossl_statem_export_allowed(SSL *s)
{
    return s->s3.previous_server_finished_len != 0
           && s->statem.hand_state != TLS_ST_SW_FINISHED;
}