bool ossl_statem_export_allowed(long param_1)

{
  bool bVar1;
  
  bVar1 = false;
  if (*(long *)(param_1 + 0x478) != 0) {
    bVar1 = *(int *)(param_1 + 0x5c) != 0x24;
  }
  return bVar1;
}