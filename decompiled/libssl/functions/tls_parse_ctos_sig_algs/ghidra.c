int tls_parse_ctos_sig_algs(long param_1,ushort **param_2)

{
  ushort uVar1;
  int iVar2;
  ushort *puVar3;
  long in_FS_OFFSET;
  ushort *local_38;
  ushort *local_30;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if ((ushort *)0x1 < param_2[1]) {
    puVar3 = param_2[1] + -1;
    uVar1 = **param_2;
    if (puVar3 == (ushort *)(ulong)(ushort)(uVar1 << 8 | uVar1 >> 8)) {
      local_38 = *param_2 + 1;
      param_2[1] = (ushort *)0x0;
      *param_2 = (ushort *)((long)local_38 + (long)puVar3);
      local_30 = puVar3;
      if (puVar3 != (ushort *)0x0) {
        if (*(int *)(param_1 + 0x4d0) == 0) {
          iVar2 = tls1_save_sigalgs(param_1,&local_38,0);
          if (iVar2 == 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x125,"tls_parse_ctos_sig_algs");
            ossl_statem_fatal(param_1,0x32,0x6e,0);
            goto LAB_00165d84;
          }
        }
        iVar2 = 1;
        goto LAB_00165d84;
      }
    }
  }
  ERR_new();
  iVar2 = 0;
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x120,"tls_parse_ctos_sig_algs");
  ossl_statem_fatal(param_1,0x32,0x6e,0);
LAB_00165d84:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}