int SSL_stateless(SSL *param_1)

{
  int iVar1;
  
  iVar1 = SSL_clear(param_1);
  if (iVar1 != 0) {
    ERR_clear_error();
    *(ulong *)&param_1->hit = *(ulong *)&param_1->hit | 0x800;
    iVar1 = SSL_accept(param_1);
    *(ulong *)&param_1->hit = *(ulong *)&param_1->hit & 0xfffffffffffff7ff;
    if (((iVar1 < 1) || (iVar1 = 1, *(int *)&param_1[4].d1 == 0)) &&
       (iVar1 = -1, *(int *)&param_1[3].read_hash == 1)) {
      iVar1 = ossl_statem_in_error(param_1);
      return -(uint)(iVar1 != 0);
    }
  }
  return iVar1;
}