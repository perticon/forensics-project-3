custom_ext_add(long param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5)

{
  undefined2 *puVar1;
  long lVar2;
  code *pcVar3;
  int iVar4;
  ulong uVar5;
  long in_FS_OFFSET;
  undefined8 uVar6;
  undefined4 local_54;
  undefined8 local_50;
  long local_48;
  long local_40;
  
  lVar2 = *(long *)(param_1 + 0x898);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(long *)(lVar2 + 0x1e0) != 0) {
    uVar5 = 0;
    do {
      local_50 = 0;
      local_48 = 0;
      puVar1 = (undefined2 *)(*(long *)(lVar2 + 0x1d8) + uVar5 * 0x38);
      uVar6 = 0x164afa;
      iVar4 = should_add_extension(param_1,*(undefined4 *)(puVar1 + 4),param_2);
      if ((iVar4 != 0) && (((param_2 & 0x1f00) == 0 || ((*(byte *)(puVar1 + 6) & 1) != 0)))) {
        pcVar3 = *(code **)(puVar1 + 8);
        if ((param_2 & 0x80) == 0) {
          if (pcVar3 != (code *)0x0) {
LAB_001649d9:
            iVar4 = (*pcVar3)(param_1,*puVar1,param_2,&local_50,&local_48,param_4,param_5,&local_54,
                              *(undefined8 *)(puVar1 + 0x10),uVar6);
            if (iVar4 < 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_cust.c",0xd4,"custom_ext_add");
              ossl_statem_fatal(param_1,local_54,0xea,0);
              goto LAB_00164b63;
            }
            if (iVar4 != 0) goto LAB_00164a18;
          }
        }
        else {
          if (pcVar3 != (code *)0x0) goto LAB_001649d9;
LAB_00164a18:
          iVar4 = WPACKET_put_bytes__(param_3,*puVar1,2);
          if ((((iVar4 == 0) || (iVar4 = WPACKET_start_sub_packet_len__(param_3), iVar4 == 0)) ||
              ((local_48 != 0 && (iVar4 = WPACKET_memcpy(param_3), iVar4 == 0)))) ||
             (iVar4 = WPACKET_close(param_3), iVar4 == 0)) {
            ERR_new();
            uVar6 = 0xdf;
LAB_00164b41:
            ERR_set_debug("ssl/statem/extensions_cust.c",uVar6,"custom_ext_add");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
LAB_00164b63:
            uVar6 = 0;
            goto LAB_00164b65;
          }
          if ((param_2 & 0x80) != 0) {
            if ((*(uint *)(puVar1 + 6) & 2) != 0) {
              ERR_new();
              uVar6 = 0xe7;
              goto LAB_00164b41;
            }
            *(uint *)(puVar1 + 6) = *(uint *)(puVar1 + 6) | 2;
          }
          if (*(code **)(puVar1 + 0xc) != (code *)0x0) {
            (**(code **)(puVar1 + 0xc))
                      (param_1,*puVar1,param_2,local_50,*(undefined8 *)(puVar1 + 0x10));
          }
        }
      }
      uVar5 = uVar5 + 1;
    } while (uVar5 <= *(ulong *)(lVar2 + 0x1e0) && *(ulong *)(lVar2 + 0x1e0) != uVar5);
  }
  uVar6 = 1;
LAB_00164b65:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar6;
}