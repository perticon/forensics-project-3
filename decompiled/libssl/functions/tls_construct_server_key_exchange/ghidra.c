undefined8 tls_construct_server_key_exchange(long param_1,undefined8 param_2)

{
  uint uVar1;
  char *__s;
  BIGNUM *a;
  bool bVar2;
  ushort uVar3;
  int iVar4;
  int iVar5;
  int iVar6;
  undefined4 uVar7;
  int iVar8;
  long lVar9;
  size_t sVar10;
  EVP_PKEY *pEVar11;
  long lVar12;
  BIGNUM **ppBVar13;
  undefined8 uVar14;
  undefined8 uVar15;
  EVP_PKEY *pkey;
  long in_FS_OFFSET;
  long local_e8;
  uint local_dc;
  long local_c8;
  void *local_b0;
  undefined8 local_a8;
  undefined8 local_a0;
  long local_98;
  long local_90;
  long local_88;
  long local_80;
  void *local_78;
  uchar *local_70;
  undefined local_68 [16];
  undefined local_58 [24];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_e8 = *(long *)(param_1 + 0x380);
  local_b0 = (void *)0x0;
  lVar9 = EVP_MD_CTX_new();
  local_a8 = 0;
  iVar4 = WPACKET_get_total_written(param_2,&local_98);
  if (iVar4 == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0x985,"tls_construct_server_key_exchange");
    uVar15 = 0xc0103;
LAB_00179044:
    pkey = (EVP_PKEY *)0x0;
    ossl_statem_fatal(param_1,0x50,uVar15,0);
  }
  else {
    if (lVar9 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0x98a,"tls_construct_server_key_exchange");
      uVar15 = 0xc0100;
      goto LAB_00179044;
    }
    lVar12 = *(long *)(param_1 + 0x2e0);
    local_68 = (undefined  [16])0x0;
    uVar1 = *(uint *)(lVar12 + 0x1c);
    local_58._0_16_ = (undefined  [16])0x0;
    local_dc = uVar1 & 0x48;
    if (local_dc != 0) {
      local_dc = 0;
      bVar2 = false;
      local_c8 = 0;
LAB_00178cc9:
      if (((*(byte *)(lVar12 + 0x20) & 0x44) == 0) && ((*(uint *)(lVar12 + 0x1c) & 0x1c8) == 0)) {
        if (local_e8 != 0) goto LAB_00178ce4;
        ERR_new();
        uVar15 = 0;
        ERR_set_debug("ssl/statem/statem_srvr.c",0xa0f,"tls_construct_server_key_exchange");
        ossl_statem_fatal(param_1,0x32,0xc0103,0);
      }
      else {
        local_e8 = 0;
LAB_00178ce4:
        if ((uVar1 & 0x1c8) == 0) {
LAB_00178d33:
          ppBVar13 = (BIGNUM **)local_68;
          iVar4 = 0;
          do {
            if (*ppBVar13 == (BIGNUM *)0x0) break;
            if (iVar4 == 2) {
              if ((uVar1 & 0x20) == 0) {
                iVar5 = WPACKET_start_sub_packet_len__(param_2);
              }
              else {
                iVar5 = WPACKET_start_sub_packet_len__(param_2);
              }
              if (iVar5 == 0) {
LAB_001791e8:
                ERR_new();
                uVar15 = 0xa31;
                goto LAB_00178e41;
              }
              if ((uVar1 & 0x102) != 0) {
                iVar6 = BN_num_bits(local_68._0_8_);
                iVar8 = BN_num_bits(local_58._0_8_);
                iVar5 = iVar6 + 0xe;
                if (-1 < iVar6 + 7) {
                  iVar5 = iVar6 + 7;
                }
                iVar6 = iVar8 + 7;
                if (iVar8 + 7 < 0) {
                  iVar6 = iVar8 + 0xe;
                }
                sVar10 = (size_t)((iVar5 >> 3) - (iVar6 >> 3));
                if (sVar10 != 0) {
                  iVar5 = WPACKET_allocate_bytes(param_2,sVar10,&local_70);
                  if (iVar5 == 0) {
                    ERR_new();
                    uVar15 = 0xa3f;
                    goto LAB_00178e41;
                  }
                  memset(local_70,0,sVar10);
                }
              }
            }
            else {
              iVar5 = WPACKET_start_sub_packet_len__(param_2,2);
              if (iVar5 == 0) goto LAB_001791e8;
            }
            iVar6 = BN_num_bits(*ppBVar13);
            iVar5 = iVar6 + 0xe;
            if (-1 < iVar6 + 7) {
              iVar5 = iVar6 + 7;
            }
            iVar5 = WPACKET_allocate_bytes(param_2,(long)(iVar5 >> 3),&local_70);
            if ((iVar5 == 0) || (iVar5 = WPACKET_close(param_2), iVar5 == 0)) {
              ERR_new();
              uVar15 = 0xa48;
              goto LAB_00178e41;
            }
            a = *ppBVar13;
            iVar4 = iVar4 + 1;
            ppBVar13 = ppBVar13 + 1;
            BN_bn2bin(a,local_70);
          } while (iVar4 != 4);
          if ((uVar1 & 0x84) != 0) {
            iVar4 = WPACKET_put_bytes__(param_2,3,1);
            if ((((iVar4 == 0) || (iVar4 = WPACKET_put_bytes__(param_2,0,1), iVar4 == 0)) ||
                (iVar4 = WPACKET_put_bytes__(param_2,local_dc,1), iVar4 == 0)) ||
               (iVar4 = WPACKET_sub_memcpy__(param_2,local_b0,local_c8,1), iVar4 == 0)) {
              ERR_new();
              uVar14 = 0xa5a;
              goto LAB_001791a1;
            }
            CRYPTO_free(local_b0);
            local_b0 = (void *)0x0;
          }
          uVar15 = 1;
          if (local_e8 != 0) {
            lVar12 = *(long *)(*(long *)(param_1 + 0x388) + 8);
            local_70 = (uchar *)0x0;
            if ((lVar12 == 0) ||
               (iVar4 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x9a8),local_e8,&local_90),
               iVar4 == 0)) {
              ERR_new();
              uVar15 = 0xa6a;
LAB_00178e41:
              ERR_set_debug("ssl/statem/statem_srvr.c",uVar15,"tls_construct_server_key_exchange");
              ossl_statem_fatal(param_1,0x50,0xc0103,0);
            }
            else {
              iVar4 = WPACKET_get_length(param_2,&local_a0);
              if (iVar4 == 0) {
                ERR_new();
                uVar15 = 0xa6f;
                goto LAB_00178e41;
              }
              if (((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 2) != 0) &&
                 (iVar4 = WPACKET_put_bytes__(param_2,*(undefined2 *)(local_e8 + 8),2), iVar4 == 0))
              {
                ERR_new();
                uVar15 = 0xa74;
                goto LAB_00178e41;
              }
              uVar15 = (*(undefined8 **)(param_1 + 0x9a8))[0x88];
              uVar14 = **(undefined8 **)(param_1 + 0x9a8);
              if (local_90 != 0) {
                local_90 = EVP_MD_get0_name(local_90);
              }
              iVar4 = EVP_DigestSignInit_ex(lVar9,&local_a8,local_90,uVar14,uVar15,lVar12);
              if (iVar4 < 1) {
                ERR_new();
                uVar15 = 0xa7c;
                goto LAB_00178e41;
              }
              if ((*(int *)(local_e8 + 0x14) == 0x390) &&
                 ((iVar4 = EVP_PKEY_CTX_set_rsa_padding(local_a8,6), iVar4 < 1 ||
                  (iVar4 = EVP_PKEY_CTX_set_rsa_pss_saltlen(local_a8,0xffffffff), iVar4 < 1)))) {
                ERR_new();
                ERR_set_debug("ssl/statem/statem_srvr.c",0xa82,"tls_construct_server_key_exchange");
                ossl_statem_fatal(param_1,0x50,0x80006,0);
              }
              else {
                lVar12 = construct_key_exchange_tbs
                                   (param_1,&local_78,
                                    local_98 + *(long *)(*(long *)(param_1 + 0x88) + 8),local_a0);
                if (lVar12 != 0) {
                  iVar4 = EVP_DigestSign(lVar9,0,&local_70,local_78,lVar12);
                  if ((((0 < iVar4) &&
                       (iVar4 = WPACKET_sub_reserve_bytes__(param_2,local_70,&local_88,2),
                       iVar4 != 0)) &&
                      (iVar4 = EVP_DigestSign(lVar9,local_88,&local_70,local_78,lVar12), 0 < iVar4))
                     && ((iVar4 = WPACKET_sub_allocate_bytes__(param_2,local_70,&local_80,2),
                         iVar4 != 0 && (local_88 == local_80)))) {
                    uVar15 = 1;
                    CRYPTO_free(local_78);
                    goto LAB_00178e66;
                  }
                  CRYPTO_free(local_78);
                  ERR_new();
                  uVar15 = 0xa94;
                  goto LAB_00178e41;
                }
              }
            }
            uVar15 = 0;
          }
        }
        else {
          __s = *(char **)(*(long *)(param_1 + 0x898) + 0x200);
          if (__s == (char *)0x0) {
            sVar10 = 0;
LAB_00178d1b:
            iVar4 = WPACKET_sub_memcpy__(param_2,__s,sVar10,2);
            if (iVar4 != 0) goto LAB_00178d33;
          }
          else {
            sVar10 = strlen(__s);
            if (sVar10 < 0x101) goto LAB_00178d1b;
          }
          ERR_new();
          uVar14 = 0xa1f;
LAB_001791a1:
          uVar15 = 0;
          ERR_set_debug("ssl/statem/statem_srvr.c",uVar14,"tls_construct_server_key_exchange");
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
        }
      }
LAB_00178e66:
      EVP_PKEY_free((EVP_PKEY *)0x0);
      CRYPTO_free(local_b0);
      EVP_MD_CTX_free(lVar9);
      if (!bVar2) goto LAB_0017907f;
LAB_00178e97:
      BN_free(local_68._0_8_);
      BN_free(local_68._8_8_);
      BN_free(local_58._0_8_);
      BN_free(local_58._8_8_);
      goto LAB_0017907f;
    }
    if ((uVar1 & 0x102) != 0) {
      lVar12 = *(long *)(param_1 + 0x898);
      if (*(int *)(lVar12 + 0x18) != 0) {
        pEVar11 = (EVP_PKEY *)ssl_get_auto_dh(0,param_1);
        pkey = pEVar11;
        if (pEVar11 != (EVP_PKEY *)0x0) goto LAB_00178f01;
        ERR_new();
        uVar15 = 0x99d;
        goto LAB_00178f44;
      }
      pEVar11 = *(EVP_PKEY **)(lVar12 + 8);
      pkey = (EVP_PKEY *)0x0;
      if (pEVar11 == (EVP_PKEY *)0x0) {
        if (*(code **)(lVar12 + 0x10) == (code *)0x0) {
          ERR_new(0);
          ERR_set_debug("ssl/statem/statem_srvr.c",0x9af,"tls_construct_server_key_exchange");
          ossl_statem_fatal(param_1,0x50,0xab,0);
          goto LAB_00179056;
        }
        uVar15 = (**(code **)(lVar12 + 0x10))(0,param_1,0,0x400);
        pEVar11 = (EVP_PKEY *)ssl_dh_to_pkey(uVar15);
        pkey = pEVar11;
        if (pEVar11 == (EVP_PKEY *)0x0) {
          ERR_new();
          uVar15 = 0x9a8;
          goto LAB_00178f44;
        }
      }
LAB_00178f01:
      uVar7 = EVP_PKEY_get_security_bits(pEVar11);
      iVar4 = ssl_security(param_1,0x40007,uVar7,0);
      if (iVar4 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0x9b4,"tls_construct_server_key_exchange");
        ossl_statem_fatal(param_1,0x28,0x18a,0);
        goto LAB_00179056;
      }
      if (*(long *)(param_1 + 0x2e8) != 0) {
        ERR_new();
        uVar15 = 0x9b8;
        pEVar11 = pkey;
LAB_00178f44:
        ERR_set_debug("ssl/statem/statem_srvr.c",uVar15,"tls_construct_server_key_exchange");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        pkey = pEVar11;
        goto LAB_00179056;
      }
      lVar12 = ssl_generate_pkey(param_1,pEVar11);
      *(long *)(param_1 + 0x2e8) = lVar12;
      if (lVar12 == 0) {
        ERR_new();
        uVar15 = 0x9be;
        pEVar11 = pkey;
        goto LAB_00178f44;
      }
      EVP_PKEY_free(pkey);
      iVar4 = EVP_PKEY_get_bn_param(*(undefined8 *)(param_1 + 0x2e8),&DAT_00188a4e,local_68);
      if (((iVar4 != 0) &&
          (iVar4 = EVP_PKEY_get_bn_param
                             (*(undefined8 *)(param_1 + 0x2e8),&DAT_00188a53,local_68 + 8),
          iVar4 != 0)) &&
         (iVar4 = EVP_PKEY_get_bn_param(*(undefined8 *)(param_1 + 0x2e8),&DAT_0018c916,local_58),
         iVar4 != 0)) {
        bVar2 = true;
        lVar12 = *(long *)(param_1 + 0x2e0);
        local_c8 = 0;
        goto LAB_00178cc9;
      }
      ERR_new();
      uVar15 = 0;
      ERR_set_debug("ssl/statem/statem_srvr.c",0x9cd,"tls_construct_server_key_exchange");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      EVP_PKEY_free((EVP_PKEY *)0x0);
      CRYPTO_free(local_b0);
      EVP_MD_CTX_free(lVar9);
      goto LAB_00178e97;
    }
    local_dc = uVar1 & 0x84;
    if (local_dc == 0) {
      if ((uVar1 & 0x20) == 0) {
        ERR_new(0);
        ERR_set_debug("ssl/statem/statem_srvr.c",0xa07,"tls_construct_server_key_exchange");
        uVar15 = 0xfa;
      }
      else {
        if (((*(long *)(param_1 + 0xbf8) != 0) && (*(long *)(param_1 + 0xc00) != 0)) &&
           ((*(long *)(param_1 + 0xc08) != 0 && (*(long *)(param_1 + 0xc10) != 0)))) {
          bVar2 = false;
          local_c8 = 0;
          local_68 = CONCAT88(*(long *)(param_1 + 0xc00),*(long *)(param_1 + 0xbf8));
          local_58._0_16_ = CONCAT88(*(long *)(param_1 + 0xc10),*(long *)(param_1 + 0xc08));
          goto LAB_00178cc9;
        }
        ERR_new(0);
        ERR_set_debug("ssl/statem/statem_srvr.c",0x9fd,"tls_construct_server_key_exchange");
        uVar15 = 0x166;
      }
      goto LAB_00179044;
    }
    pkey = *(EVP_PKEY **)(param_1 + 0x2e8);
    if (pkey != (EVP_PKEY *)0x0) {
      ERR_new(0);
      ERR_set_debug("ssl/statem/statem_srvr.c",0x9d3,"tls_construct_server_key_exchange");
      uVar15 = 0xc0103;
      goto LAB_00179044;
    }
    uVar3 = tls1_shared_group(0,param_1,0xfffffffe);
    if (uVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0x9da,"tls_construct_server_key_exchange");
      ossl_statem_fatal(param_1,0x28,0x13b,0);
    }
    else {
      local_dc = (uint)uVar3;
      *(uint *)(*(long *)(param_1 + 0x918) + 0x308) = local_dc;
      lVar12 = ssl_generate_pkey_group(param_1,local_dc);
      *(long *)(param_1 + 0x2e8) = lVar12;
      if (lVar12 != 0) {
        local_c8 = EVP_PKEY_get1_encoded_public_key(lVar12,&local_b0);
        if (local_c8 != 0) {
          bVar2 = false;
          lVar12 = *(long *)(param_1 + 0x2e0);
          local_68 = (undefined  [16])0x0;
          local_58._0_16_ = (undefined  [16])0x0;
          goto LAB_00178cc9;
        }
        ERR_new(0);
        ERR_set_debug("ssl/statem/statem_srvr.c",0x9eb,"tls_construct_server_key_exchange");
        ossl_statem_fatal(param_1,0x50,0x80010,0);
      }
    }
  }
LAB_00179056:
  uVar15 = 0;
  EVP_PKEY_free(pkey);
  CRYPTO_free(local_b0);
  EVP_MD_CTX_free(lVar9);
LAB_0017907f:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar15;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}