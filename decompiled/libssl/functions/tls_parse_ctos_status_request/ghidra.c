tls_parse_ctos_status_request(long param_1,ushort **param_2,undefined8 param_3,long param_4)

{
  byte bVar1;
  ushort uVar2;
  ushort *puVar3;
  undefined *puVar4;
  int iVar5;
  undefined8 uVar6;
  long lVar7;
  OCSP_RESPID *a;
  ushort *puVar8;
  X509_EXTENSIONS *pXVar9;
  byte *len;
  ushort *puVar10;
  byte *pbVar11;
  long in_FS_OFFSET;
  ushort *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*(int *)(param_1 + 0x4d0) == 0) && (param_4 == 0)) {
    puVar10 = param_2[1];
    if (puVar10 == (ushort *)0x0) {
      ERR_new();
      uVar6 = 0x13b;
    }
    else {
      puVar3 = *param_2;
      bVar1 = *(byte *)puVar3;
      *(uint *)(param_1 + 0xa60) = (uint)bVar1;
      *param_2 = (ushort *)((long)puVar3 + 1);
      param_2[1] = (ushort *)((long)puVar10 - 1U);
      puVar4 = PTR_OCSP_RESPID_free_001a7fb0;
      if (bVar1 != 1) {
        *(undefined4 *)(param_1 + 0xa60) = 0xffffffff;
        goto LAB_00165e61;
      }
      if ((ushort *)0x1 < (ushort *)((long)puVar10 - 1U)) {
        pbVar11 = (byte *)(ulong)(ushort)(*(ushort *)((long)puVar3 + 1) << 8 |
                                         *(ushort *)((long)puVar3 + 1) >> 8);
        if (pbVar11 <= (byte *)((long)puVar10 + -3)) {
          puVar3 = (ushort *)((long)puVar3 + 3);
          uVar6 = *(undefined8 *)(param_1 + 0xa78);
          param_2[1] = (ushort *)((byte *)((long)puVar10 + -3) + -(long)pbVar11);
          *param_2 = (ushort *)((long)puVar3 + (long)pbVar11);
          OPENSSL_sk_pop_free(uVar6,puVar4);
          if (pbVar11 == (byte *)0x0) {
            *(undefined8 *)(param_1 + 0xa78) = 0;
          }
          else {
            lVar7 = OPENSSL_sk_new_null();
            *(long *)(param_1 + 0xa78) = lVar7;
            puVar10 = local_48;
            if (lVar7 == 0) {
              ERR_new();
              ERR_set_debug("ssl/statem/extensions_srvr.c",0x154,"tls_parse_ctos_status_request");
              ossl_statem_fatal(param_1,0x50,0xc0100,0);
              uVar6 = 0;
              goto LAB_00165e66;
            }
            do {
              if (pbVar11 == (byte *)0x1) {
LAB_00166050:
                local_48 = puVar10;
                ERR_new();
                uVar6 = 0x162;
                goto LAB_00165ee1;
              }
              len = (byte *)(ulong)(ushort)(*puVar3 << 8 | *puVar3 >> 8);
              if (pbVar11 + -2 < len) goto LAB_00166050;
              local_48 = puVar3 + 1;
              pbVar11 = pbVar11 + -2 + -(long)len;
              puVar3 = (ushort *)((long)local_48 + (long)len);
              if (len == (byte *)0x0) goto LAB_00166050;
              a = d2i_OCSP_RESPID((OCSP_RESPID **)0x0,(uchar **)&local_48,(long)len);
              if (a == (OCSP_RESPID *)0x0) {
                ERR_new();
                uVar6 = 0x16a;
                goto LAB_00165ee1;
              }
              if (local_48 != puVar3) {
                OCSP_RESPID_free(a);
                ERR_new();
                uVar6 = 0x170;
                goto LAB_00165ee1;
              }
              iVar5 = OPENSSL_sk_push(*(undefined8 *)(param_1 + 0xa78),a);
              if (iVar5 == 0) {
                OCSP_RESPID_free(a);
                ERR_new();
                ERR_set_debug("ssl/statem/extensions_srvr.c",0x177,"tls_parse_ctos_status_request");
                ossl_statem_fatal(param_1,0x50,0xc0103,0);
                uVar6 = 0;
                goto LAB_00165e66;
              }
              puVar10 = local_48;
            } while (pbVar11 != (byte *)0x0);
          }
          if ((ushort *)0x1 < param_2[1]) {
            puVar10 = param_2[1] + -1;
            uVar2 = **param_2;
            if (puVar10 == (ushort *)(ulong)(ushort)(uVar2 << 8 | uVar2 >> 8)) {
              puVar8 = *param_2 + 1;
              param_2[1] = (ushort *)0x0;
              puVar3 = (ushort *)((long)puVar8 + (long)puVar10);
              *param_2 = puVar3;
              if (puVar10 != (ushort *)0x0) {
                local_48 = puVar8;
                OPENSSL_sk_pop_free(*(undefined8 *)(param_1 + 0xa80),
                                    PTR_X509_EXTENSION_free_001a7f90);
                pXVar9 = d2i_X509_EXTENSIONS((X509_EXTENSIONS **)0x0,(uchar **)&local_48,
                                             (long)puVar10);
                *(X509_EXTENSIONS **)(param_1 + 0xa80) = pXVar9;
                if ((pXVar9 != (X509_EXTENSIONS *)0x0) && (uVar6 = 1, local_48 == puVar3))
                goto LAB_00165e66;
                ERR_new();
                uVar6 = 0x18b;
                goto LAB_00165ee1;
              }
              goto LAB_00165e61;
            }
          }
          ERR_new();
          uVar6 = 0x17f;
          goto LAB_00165ee1;
        }
      }
      ERR_new();
      uVar6 = 0x148;
    }
LAB_00165ee1:
    ERR_set_debug("ssl/statem/extensions_srvr.c",uVar6,"tls_parse_ctos_status_request");
    ossl_statem_fatal(param_1,0x32,0x6e,0);
    uVar6 = 0;
  }
  else {
LAB_00165e61:
    uVar6 = 1;
  }
LAB_00165e66:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar6;
}