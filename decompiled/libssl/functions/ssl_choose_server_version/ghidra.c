undefined8 ssl_choose_server_version(uint *param_1,long param_2,undefined4 *param_3)

{
  ulong uVar1;
  int *piVar2;
  long lVar3;
  bool bVar4;
  uint uVar5;
  int iVar6;
  undefined8 uVar7;
  uint *puVar8;
  uint uVar9;
  uint uVar10;
  uint uVar11;
  ushort uVar12;
  undefined1 *puVar13;
  long lVar14;
  byte *pbVar15;
  uint uVar16;
  byte *pbVar17;
  long in_FS_OFFSET;
  undefined8 local_48;
  long local_40;
  
  piVar2 = *(int **)(param_1 + 2);
  uVar16 = *(uint *)(param_2 + 4);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar6 = *piVar2;
  param_1[0x283] = uVar16;
  if (iVar6 == 0x10000) {
    uVar9 = 0x304;
    puVar13 = tls_version_table;
LAB_00176954:
    lVar14 = *(long *)(param_2 + 0x288);
    if (*(int *)(lVar14 + 0x2b8) == 0) goto LAB_00176969;
    uVar10 = *(uint *)(*(long *)(piVar2 + 0x30) + 0x60);
    if ((uVar10 & 8) == 0) {
LAB_00176ae3:
      local_48 = 0;
      lVar3 = *(long *)(lVar14 + 0x2b0);
      *(undefined4 *)(lVar14 + 700) = 1;
      if (lVar3 != 0) {
        pbVar17 = *(byte **)(lVar14 + 0x2a8);
        if (lVar3 - 1U == (ulong)*pbVar17) {
          *(undefined8 *)(lVar14 + 0x2b0) = 0;
          *(byte **)(lVar14 + 0x2a8) = pbVar17 + lVar3;
          uVar7 = 0x124;
          if ((int)uVar16 < 0x301) goto LAB_00176aa8;
          if (1 < lVar3 - 1U) {
            uVar1 = lVar3 - 3;
            uVar16 = 0;
            pbVar15 = pbVar17 + (uVar1 & 0xfffffffffffffffe);
            do {
              uVar12 = *(ushort *)(pbVar17 + 1) << 8 | *(ushort *)(pbVar17 + 1) >> 8;
              if (uVar16 != uVar12) {
                uVar9 = (uint)uVar12;
                if ((uVar10 & 8) == 0) {
                  if (uVar9 < uVar16) goto LAB_00176ddc;
                }
                else {
                  if (uVar9 == 0x100) {
                    uVar10 = 0xff00;
                    if (uVar16 == 0x100) goto LAB_00176e21;
LAB_00176dd5:
                    uVar5 = uVar16;
                  }
                  else {
                    uVar10 = uVar9;
                    uVar5 = 0xff00;
                    if (uVar16 != 0x100) goto LAB_00176dd5;
                  }
                  if (uVar5 < uVar10) goto LAB_00176ddc;
                }
LAB_00176e21:
                iVar6 = ssl_version_supported(param_1,uVar9,&local_48);
                if (iVar6 != 0) {
                  uVar16 = uVar9;
                }
              }
LAB_00176ddc:
              if (pbVar15 == pbVar17) goto code_r0x00176de1;
              pbVar17 = pbVar17 + 2;
              uVar10 = *(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60);
            } while( true );
          }
          if (lVar3 == 1) goto LAB_00176aa3;
        }
      }
      goto LAB_00176b1a;
    }
  }
  else {
    if (iVar6 == 0x1ffff) {
      uVar9 = 0xfefd;
      puVar13 = dtls_version_table;
      goto LAB_00176954;
    }
    uVar10 = *(uint *)(*(long *)(piVar2 + 0x30) + 0x60);
    if (((uVar10 & 8) != 0) || (iVar6 < 0x304)) {
      uVar9 = *param_1;
      if (uVar9 != uVar16) {
        if ((uVar10 & 8) == 0) {
          uVar7 = 0x10a;
          if ((int)uVar16 < (int)uVar9) goto LAB_00176aa8;
        }
        else {
          if (uVar16 == 0x100) {
            uVar16 = 0xff00;
            if (uVar9 == 0x100) goto LAB_00176932;
          }
          else if (uVar9 == 0x100) {
            uVar9 = 0xff00;
          }
          uVar7 = 0x10a;
          if ((int)uVar9 < (int)uVar16) goto LAB_00176aa8;
        }
      }
LAB_00176932:
      *param_3 = 0;
      uVar7 = 0;
      goto LAB_00176aa8;
    }
    lVar14 = *(long *)(param_2 + 0x288);
    if (*(int *)(lVar14 + 0x2b8) != 0) goto LAB_00176ae3;
    uVar9 = 0x304;
    puVar13 = tls_version_table;
LAB_00176969:
    if (param_1[0x23a] != 0) goto LAB_00176aa3;
    uVar10 = *(uint *)(*(long *)(piVar2 + 0x30) + 0x60);
  }
  if (uVar16 == 0x304) {
    uVar16 = 0x303;
  }
  else if ((uVar10 & 8) == 0) {
    if (0x303 < (int)uVar16) {
      uVar16 = 0x303;
    }
  }
  else if ((uVar16 != 0x100) && ((int)uVar16 < 0x305)) {
    uVar16 = 0x303;
  }
  uVar10 = 0xff00;
  if (uVar16 != 0x100) {
    uVar10 = uVar16;
  }
  bVar4 = false;
  do {
    while( true ) {
      if (*(code **)((long)puVar13 + 0x10) == (code *)0x0) goto LAB_001769f3;
      if (uVar16 != uVar9) break;
LAB_00176a2a:
      puVar8 = (uint *)(**(code **)((long)puVar13 + 0x10))();
      uVar9 = *puVar8;
      uVar5 = param_1[0x27d];
      if ((uVar5 == 0) || (uVar9 == uVar5)) {
LAB_00176b38:
        iVar6 = ssl_security(param_1,9,0,uVar9,0);
        if (iVar6 != 0) {
          uVar5 = param_1[0x27e];
          if ((uVar5 == 0) || (uVar9 == uVar5)) {
LAB_00176ba0:
            if (((*(ulong *)(puVar8 + 2) & *(ulong *)(param_1 + 0x27a)) == 0) &&
               (((*(byte *)(puVar8 + 1) & 2) == 0 ||
                ((*(uint *)(*(long *)(param_1 + 0x226) + 0x1c) & 0x30000) == 0)))) {
              if (*(uint *)puVar13 == 0x303) {
                iVar6 = ssl_version_supported(param_1,0x304,0);
                if (iVar6 == 0) goto LAB_00176be2;
                *param_3 = 1;
              }
              else if ((((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) &&
                       ((int)*(uint *)puVar13 < 0x303)) &&
                      (iVar6 = ssl_version_supported(param_1,0x303,0), iVar6 != 0)) {
                *param_3 = 2;
              }
              else {
LAB_00176be2:
                *param_3 = 0;
              }
              uVar16 = *(uint *)puVar13;
              *(uint **)(param_1 + 2) = puVar8;
              *param_1 = uVar16;
              uVar7 = 0;
              goto LAB_00176aa8;
            }
          }
          else if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
            if ((int)uVar9 < (int)uVar5) goto LAB_00176ba0;
          }
          else {
            if (uVar9 == 0x100) {
              uVar9 = 0xff00;
              if (uVar5 == 0x100) goto LAB_00176a86;
            }
            else if (uVar5 == 0x100) {
              uVar5 = 0xff00;
            }
            if ((int)uVar5 < (int)uVar9) goto LAB_00176ba0;
          }
        }
      }
      else if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
        if ((int)uVar5 <= (int)uVar9) goto LAB_00176b38;
      }
      else {
        if (uVar9 == 0x100) {
          uVar11 = 0xff00;
          if (uVar5 == 0x100) goto LAB_00176b38;
        }
        else {
          uVar11 = uVar9;
          if (uVar5 == 0x100) {
            uVar5 = 0xff00;
          }
        }
        if ((int)uVar11 <= (int)uVar5) goto LAB_00176b38;
      }
LAB_00176a86:
      uVar9 = *(uint *)((long)puVar13 + 0x18);
      puVar13 = (undefined1 *)((long)puVar13 + 0x18);
      bVar4 = true;
      if (uVar9 == 0) goto LAB_00176a9a;
    }
    if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) != 0) {
      if (uVar16 == 0x100) {
        uVar5 = 0xff00;
        if (uVar9 == 0x100) goto LAB_00176a2a;
      }
      else {
        uVar5 = uVar10;
        if (uVar9 == 0x100) {
          uVar9 = 0xff00;
          uVar5 = uVar16;
        }
      }
      if ((int)uVar9 < (int)uVar5) goto LAB_001769f3;
      goto LAB_00176a2a;
    }
    if ((int)uVar9 <= (int)uVar16) goto LAB_00176a2a;
LAB_001769f3:
    uVar9 = *(uint *)((long)puVar13 + 0x18);
    puVar13 = (undefined1 *)((long)puVar13 + 0x18);
  } while (uVar9 != 0);
LAB_00176a9a:
  uVar7 = 0x18c;
  if (!bVar4) goto LAB_00176aa8;
  goto LAB_00176aa3;
code_r0x00176de1:
  if ((uVar1 & 0xfffffffffffffffe) != uVar1) {
LAB_00176b1a:
    uVar7 = 0x9f;
    goto LAB_00176aa8;
  }
  if (uVar16 != 0) {
    if (param_1[0x23a] == 0) {
      if (uVar16 == 0x303) {
        iVar6 = ssl_version_supported(param_1,0x304,0);
        if (iVar6 == 0) goto LAB_00176eba;
        *param_3 = 1;
      }
      else if ((((*(byte *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) &&
               (uVar16 < 0x303)) && (iVar6 = ssl_version_supported(param_1,0x303,0), iVar6 != 0)) {
        *param_3 = 2;
      }
      else {
LAB_00176eba:
        *param_3 = 0;
      }
      *param_1 = uVar16;
      *(undefined8 *)(param_1 + 2) = local_48;
      uVar7 = 0;
      goto LAB_00176aa8;
    }
    uVar7 = 0;
    if (uVar16 == 0x304) goto LAB_00176aa8;
  }
LAB_00176aa3:
  uVar7 = 0x102;
LAB_00176aa8:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar7;
}