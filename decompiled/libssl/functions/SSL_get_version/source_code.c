const char *SSL_get_version(const SSL *s)
{
    return ssl_protocol_to_string(s->version);
}