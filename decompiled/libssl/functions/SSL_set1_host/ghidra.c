undefined8 SSL_set1_host(long param_1,long param_2)

{
  int iVar1;
  undefined8 uVar2;
  
  uVar2 = *(undefined8 *)(param_1 + 0x4d8);
  if (param_2 != 0) {
    iVar1 = X509_VERIFY_PARAM_set1_ip_asc(uVar2);
    if (iVar1 == 1) {
      return 1;
    }
    uVar2 = *(undefined8 *)(param_1 + 0x4d8);
  }
  uVar2 = X509_VERIFY_PARAM_set1_host(uVar2,param_2,0);
  return uVar2;
}