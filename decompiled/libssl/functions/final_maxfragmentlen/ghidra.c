bool final_maxfragmentlen(long param_1,undefined8 param_2,int param_3)

{
  long lVar1;
  int iVar2;
  char cVar3;
  byte bVar4;
  
  lVar1 = *(long *)(param_1 + 0x918);
  if ((*(int *)(param_1 + 0x38) == 0) || (*(int *)(param_1 + 0x4d0) == 0)) {
    if (lVar1 == 0) {
      return true;
    }
    cVar3 = *(char *)(lVar1 + 0x368);
    bVar4 = cVar3 - 1;
  }
  else {
    cVar3 = *(char *)(lVar1 + 0x368);
    bVar4 = cVar3 - 1;
    if ((param_3 == 0) && (bVar4 < 4)) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions.c",0x68a,"final_maxfragmentlen");
      ossl_statem_fatal(param_1,0x6d,0x6e,0);
      return false;
    }
  }
  if ((bVar4 < 4) && (*(ulong *)(param_1 + 0xa18) < (ulong)(uint)(0x200 << (cVar3 - 1U & 0x1f)))) {
    iVar2 = ssl3_setup_buffers(param_1);
    return iVar2 != 0;
  }
  return true;
}