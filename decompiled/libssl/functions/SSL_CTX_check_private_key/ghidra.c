int SSL_CTX_check_private_key(SSL_CTX *ctx)

{
  X509 *x509;
  X509 *pkey;
  int iVar1;
  
  if (ctx != (SSL_CTX *)0x0) {
    x509 = *(X509 **)**(long **)ctx->sid_ctx;
    if (x509 != (X509 *)0x0) {
      pkey = ((X509 **)**(long **)ctx->sid_ctx)[1];
      if (pkey != (X509 *)0x0) {
        iVar1 = X509_check_private_key(x509,(EVP_PKEY *)pkey);
        return iVar1;
      }
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0x668,"SSL_CTX_check_private_key");
      ERR_set_error(0x14,0xbe,0);
      return 0;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x664,"SSL_CTX_check_private_key");
  ERR_set_error(0x14,0xb1,0);
  return 0;
}