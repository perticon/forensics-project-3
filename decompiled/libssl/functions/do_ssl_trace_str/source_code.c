static const char *do_ssl_trace_str(int val, const ssl_trace_tbl *tbl,
                                    size_t ntbl)
{
    size_t i;

    for (i = 0; i < ntbl; i++, tbl++) {
        if (tbl->num == val)
            return tbl->name;
    }
    return "UNKNOWN";
}