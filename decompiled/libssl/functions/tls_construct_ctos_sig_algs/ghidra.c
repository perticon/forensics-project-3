undefined8 tls_construct_ctos_sig_algs(long param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined8 local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = *(int *)(param_1 + 0xa0c);
  if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
    uVar2 = 2;
    if (iVar1 < 0x303) goto LAB_00160b67;
  }
  else if ((iVar1 == 0x100) || (0xfefd < iVar1)) {
    uVar2 = 2;
    goto LAB_00160b67;
  }
  uVar2 = tls12_get_psigalgs(param_1,1,&local_28);
  iVar1 = WPACKET_put_bytes__(param_2,0xd,2);
  if ((((iVar1 != 0) && (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 != 0)) &&
      (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 != 0)) &&
     ((iVar1 = tls12_copy_sigalgs(param_1,param_2,local_28,uVar2), iVar1 != 0 &&
      (iVar1 = WPACKET_close(param_2), iVar1 != 0)))) {
    iVar1 = WPACKET_close(param_2);
    uVar2 = 1;
    if (iVar1 != 0) goto LAB_00160b67;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x149,"tls_construct_ctos_sig_algs");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  uVar2 = 0;
LAB_00160b67:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar2;
}