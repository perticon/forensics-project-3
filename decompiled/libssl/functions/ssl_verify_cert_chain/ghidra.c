int ssl_verify_cert_chain(void *param_1,stack_st_X509 *param_2)

{
  undefined8 *puVar1;
  code *pcVar2;
  int iVar3;
  int iVar4;
  X509_STORE_CTX *ctx;
  X509 *x509;
  X509_VERIFY_PARAM *to;
  long lVar5;
  stack_st_X509 *psVar6;
  char *name;
  X509_STORE *store;
  
  if ((param_2 != (stack_st_X509 *)0x0) && (iVar3 = OPENSSL_sk_num(param_2), iVar3 != 0)) {
    store = *(X509_STORE **)(*(long *)((long)param_1 + 0x898) + 0x1d0);
    puVar1 = *(undefined8 **)((long)param_1 + 0x9a8);
    if (store == (X509_STORE *)0x0) {
      store = (X509_STORE *)puVar1[5];
    }
    ctx = (X509_STORE_CTX *)X509_STORE_CTX_new_ex(*puVar1,puVar1[0x88]);
    if (ctx != (X509_STORE_CTX *)0x0) {
      x509 = (X509 *)OPENSSL_sk_value(param_2,0);
      iVar3 = X509_STORE_CTX_init(ctx,store,x509,param_2);
      if (iVar3 == 0) {
        ERR_new();
        ERR_set_debug("ssl/ssl_cert.c",0x18c,"ssl_verify_cert_chain");
        ERR_set_error(0x14,0x8000b,0);
        X509_STORE_CTX_free(ctx);
      }
      else {
        to = X509_STORE_CTX_get0_param(ctx);
        SSL_get_security_level(param_1);
        X509_VERIFY_PARAM_set_auth_level(to);
        X509_STORE_CTX_set_flags
                  (ctx,(ulong)(*(uint *)(*(long *)((long)param_1 + 0x898) + 0x1c) & 0x30000));
        iVar3 = SSL_get_ex_data_X509_STORE_CTX_idx();
        iVar3 = X509_STORE_CTX_set_ex_data(ctx,iVar3,param_1);
        if (iVar3 == 0) {
          X509_STORE_CTX_free(ctx);
        }
        else {
          iVar3 = OPENSSL_sk_num(*(undefined8 *)((long)param_1 + 0x4e8));
          if (0 < iVar3) {
            X509_STORE_CTX_set0_dane(ctx,(long)param_1 + 0x4e0);
          }
          name = "ssl_client";
          if (*(int *)((long)param_1 + 0x38) == 0) {
            name = "ssl_server";
          }
          X509_STORE_CTX_set_default(ctx,name);
          X509_VERIFY_PARAM_set1(to,*(X509_VERIFY_PARAM **)((long)param_1 + 0x4d8));
          if (*(verify_cb **)((long)param_1 + 0x970) != (verify_cb *)0x0) {
            X509_STORE_CTX_set_verify_cb(ctx,*(verify_cb **)((long)param_1 + 0x970));
          }
          pcVar2 = *(code **)(*(long *)((long)param_1 + 0x9a8) + 0xa8);
          if (pcVar2 == (code *)0x0) {
            iVar4 = X509_verify_cert(ctx);
            iVar3 = 0;
            if (-1 < iVar4) {
              iVar3 = iVar4;
            }
          }
          else {
            iVar3 = (*pcVar2)(ctx,*(undefined8 *)(*(long *)((long)param_1 + 0x9a8) + 0xb0));
          }
          iVar4 = X509_STORE_CTX_get_error(ctx);
          *(long *)((long)param_1 + 0x9b8) = (long)iVar4;
          OSSL_STACK_OF_X509_free(*(undefined8 *)((long)param_1 + 0x9b0));
          *(undefined8 *)((long)param_1 + 0x9b0) = 0;
          lVar5 = X509_STORE_CTX_get0_chain(ctx);
          if (lVar5 != 0) {
            psVar6 = X509_STORE_CTX_get1_chain(ctx);
            *(stack_st_X509 **)((long)param_1 + 0x9b0) = psVar6;
            if (psVar6 == (stack_st_X509 *)0x0) {
              ERR_new();
              iVar3 = 0;
              ERR_set_debug("ssl/ssl_cert.c",0x1c0,"ssl_verify_cert_chain");
              ERR_set_error(0x14,0xc0100,0);
            }
          }
          X509_VERIFY_PARAM_move_peername(*(undefined8 *)((long)param_1 + 0x4d8),to);
          X509_STORE_CTX_free(ctx);
        }
      }
      return iVar3;
    }
    ERR_new();
    ERR_set_debug("ssl/ssl_cert.c",0x186,"ssl_verify_cert_chain");
    ERR_set_error(0x14,0xc0100,0);
  }
  return 0;
}