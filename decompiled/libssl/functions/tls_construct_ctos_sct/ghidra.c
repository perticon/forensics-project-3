undefined8 tls_construct_ctos_sct(long param_1,undefined8 param_2,undefined8 param_3,long param_4)

{
  int iVar1;
  
  if ((*(long *)(param_1 + 0xb68) != 0) && (param_4 == 0)) {
    iVar1 = WPACKET_put_bytes__(param_2,0x12,2);
    if (iVar1 != 0) {
      iVar1 = WPACKET_put_bytes__(param_2,0,2);
      if (iVar1 != 0) {
        return 1;
      }
    }
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x203,"tls_construct_ctos_sct");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  }
  return 2;
}