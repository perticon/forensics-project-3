ulong ssl3_ctrl(long param_1,int param_2,ulong param_3,long *param_4)

{
  ushort *puVar1;
  undefined2 uVar2;
  long lVar3;
  int iVar4;
  uint uVar5;
  undefined4 uVar6;
  ulong uVar7;
  long lVar8;
  size_t sVar9;
  char *pcVar10;
  EVP_PKEY *pkey;
  undefined8 uVar11;
  ushort *puVar12;
  
  if (0x87 < param_2 - 3U) {
    return 0;
  }
  switch(param_2) {
  case 3:
    if (param_4 != (long *)0x0) {
      pkey = (EVP_PKEY *)ssl_dh_to_pkey(param_4);
      if (pkey == (EVP_PKEY *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/s3_lib.c",0xd78,__func___27614);
        ERR_set_error(0x14,0xc0100,0);
        return 0;
      }
      iVar4 = SSL_set0_tmp_dh_pkey(param_1,pkey);
      if (iVar4 != 0) {
        return 1;
      }
      EVP_PKEY_free(pkey);
      return 0;
    }
    ERR_new();
    uVar11 = 0xd73;
    goto LAB_0012975e;
  case 4:
    if (param_4 != (long *)0x0) {
      iVar4 = ssl_set_tmp_ecdh_groups(param_1 + 0xac8,param_1 + 0xac0,param_4);
      return (long)iVar4;
    }
    ERR_new();
    uVar11 = 0xd8f;
LAB_0012975e:
    ERR_set_debug("ssl/s3_lib.c",uVar11,__func___27614);
    ERR_set_error(0x14,0xc0102,0);
    uVar7 = 0;
    break;
  default:
    goto switchD_00128f9e_caseD_5;
  case 6:
    ERR_new();
    ERR_set_debug("ssl/s3_lib.c",0xd84,__func___27614);
    ERR_set_error(0x14,0xc0101,0);
    uVar7 = 0;
    break;
  case 10:
    uVar7 = (ulong)*(int *)(param_1 + 0x1b4);
    break;
  case 0xb:
    uVar7 = (ulong)*(int *)(param_1 + 0x1b4);
    *(undefined4 *)(param_1 + 0x1b4) = 0;
    break;
  case 0xc:
    uVar7 = (ulong)*(int *)(param_1 + 0x1b0);
    break;
  case 0xd:
    uVar7 = (ulong)*(int *)(param_1 + 0xa8);
    break;
  case 0x37:
    if (param_3 != 0) {
      ERR_new();
      ERR_set_debug("ssl/s3_lib.c",0xdb3,__func___27614);
      ERR_set_error(0x14,0x140,0);
      return 0;
    }
    CRYPTO_free(*(void **)(param_1 + 0xa58));
    *(undefined8 *)(param_1 + 0xa58) = 0;
    if (param_4 != (long *)0x0) {
      sVar9 = strlen((char *)param_4);
      if (0xfe < sVar9 - 1) {
        ERR_new();
        ERR_set_debug("ssl/s3_lib.c",0xdab,__func___27614);
        ERR_set_error(0x14,0x13f,0);
        return 0;
      }
      pcVar10 = CRYPTO_strdup((char *)param_4,"ssl/s3_lib.c",0xdae);
      *(char **)(param_1 + 0xa58) = pcVar10;
      if (pcVar10 == (char *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/s3_lib.c",0xdaf,__func___27614);
        ERR_set_error(0x14,0xc0103,0);
        return 0;
      }
    }
    goto LAB_00129548;
  case 0x39:
    *(long **)(param_1 + 0xa50) = param_4;
LAB_00129548:
    uVar7 = 1;
    break;
  case 0x41:
    *(int *)(param_1 + 0xa60) = (int)param_3;
    uVar7 = 1;
    break;
  case 0x42:
    *param_4 = *(long *)(param_1 + 0xa80);
    uVar7 = 1;
    break;
  case 0x43:
    *(long **)(param_1 + 0xa80) = param_4;
    uVar7 = 1;
    break;
  case 0x44:
    *param_4 = *(long *)(param_1 + 0xa78);
    uVar7 = 1;
    break;
  case 0x45:
    *(long **)(param_1 + 0xa78) = param_4;
    uVar7 = 1;
    break;
  case 0x46:
    *param_4 = *(long *)(param_1 + 0xa88);
    uVar7 = *(ulong *)(param_1 + 0xa90);
    if ((long)uVar7 < 1) {
      uVar7 = 0xffffffffffffffff;
    }
    break;
  case 0x47:
    CRYPTO_free(*(void **)(param_1 + 0xa88));
    *(long **)(param_1 + 0xa88) = param_4;
    uVar7 = 1;
    *(ulong *)(param_1 + 0xa90) = param_3;
    break;
  case 0x58:
    if (param_3 == 0) {
      iVar4 = ssl_cert_set0_chain(param_1,0,param_4);
      uVar7 = (ulong)iVar4;
    }
    else {
      iVar4 = ssl_cert_set1_chain();
      uVar7 = (ulong)iVar4;
    }
    break;
  case 0x59:
    if (param_3 == 0) {
      iVar4 = ssl_cert_add0_chain_cert(param_1,0,param_4);
      uVar7 = (ulong)iVar4;
    }
    else {
      iVar4 = ssl_cert_add1_chain_cert();
      uVar7 = (ulong)iVar4;
    }
    break;
  case 0x5a:
    if (*(long *)(param_1 + 0x918) != 0) {
      lVar3 = *(long *)(param_1 + 0xad0);
      if ((param_4 != (long *)0x0) && (lVar3 != 0)) {
        puVar12 = *(ushort **)(param_1 + 0xad8);
        puVar1 = puVar12 + lVar3;
        do {
          if ((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
              (iVar4 = **(int **)(param_1 + 8), iVar4 != 0x10000)) && (0x303 < iVar4)) {
            ssl_group_id_tls13_to_internal(*puVar12);
          }
          lVar8 = tls1_group_id_lookup();
          if (lVar8 == 0) {
            *(uint *)param_4 = *puVar12 | 0x1000000;
          }
          else {
            uVar6 = tls1_group_id2nid();
            *(undefined4 *)param_4 = uVar6;
          }
          puVar12 = puVar12 + 1;
          param_4 = (long *)((long)param_4 + 4);
        } while (puVar12 != puVar1);
      }
      return (long)(int)lVar3;
    }
    goto switchD_00128f9e_caseD_5;
  case 0x5b:
    iVar4 = tls1_set_groups(param_1 + 0xac8,param_1 + 0xac0,param_4,param_3);
    uVar7 = (ulong)iVar4;
    break;
  case 0x5c:
    iVar4 = tls1_set_groups_list(*(undefined8 *)(param_1 + 0x9a8),param_1 + 0xac8,param_1 + 0xac0);
    uVar7 = (ulong)iVar4;
    break;
  case 0x5d:
    uVar5 = tls1_shared_group(param_1,param_3 & 0xffffffff);
    uVar7 = (ulong)(uVar5 & 0xffff);
    if (param_3 != 0xffffffffffffffff) {
      iVar4 = tls1_group_id2nid((short)uVar5,1,uVar5);
      uVar7 = (ulong)iVar4;
    }
    break;
  case 0x61:
    iVar4 = tls1_set_sigalgs(*(undefined8 *)(param_1 + 0x898),param_4,param_3,0);
    uVar7 = (ulong)iVar4;
    break;
  case 0x62:
    iVar4 = tls1_set_sigalgs_list(*(undefined8 *)(param_1 + 0x898),param_4,0);
    uVar7 = (ulong)iVar4;
    break;
  case 0x65:
    iVar4 = tls1_set_sigalgs(*(undefined8 *)(param_1 + 0x898),param_4,param_3,1);
    uVar7 = (ulong)iVar4;
    break;
  case 0x66:
    iVar4 = tls1_set_sigalgs_list(*(undefined8 *)(param_1 + 0x898),param_4,1);
    uVar7 = (ulong)iVar4;
    break;
  case 0x67:
    uVar7 = 0;
    if ((*(int *)(param_1 + 0x38) == 0) && (*(int *)(param_1 + 0x2f0) != 0)) {
      if (param_4 != (long *)0x0) {
        *param_4 = *(long *)(param_1 + 0x2f8);
      }
      uVar7 = *(ulong *)(param_1 + 0x300);
    }
    break;
  case 0x68:
    if (*(int *)(param_1 + 0x38) == 0) {
      return 0;
    }
    lVar3 = *(long *)(param_1 + 0x898);
    CRYPTO_free(*(void **)(lVar3 + 0x188));
    *(undefined8 *)(lVar3 + 0x188) = 0;
    *(undefined8 *)(lVar3 + 400) = 0;
    if ((param_4 == (long *)0x0) || (param_3 == 0)) goto LAB_00129548;
    if (param_3 < 0x100) {
      lVar8 = CRYPTO_memdup(param_4,param_3,"ssl/s3_lib.c",0x112b);
      *(long *)(lVar3 + 0x188) = lVar8;
      if (lVar8 != 0) {
        *(ulong *)(lVar3 + 400) = param_3;
        return 1;
      }
    }
    goto switchD_00128f9e_caseD_5;
  case 0x69:
    iVar4 = ssl_build_cert_chain(param_1,0,param_3 & 0xffffffff);
    uVar7 = (ulong)iVar4;
    break;
  case 0x6a:
    iVar4 = ssl_cert_set_cert_store(*(undefined8 *)(param_1 + 0x898),param_4,0,param_3 & 0xffffffff)
    ;
    uVar7 = (ulong)iVar4;
    break;
  case 0x6b:
    iVar4 = ssl_cert_set_cert_store(*(undefined8 *)(param_1 + 0x898),param_4,1,param_3 & 0xffffffff)
    ;
    uVar7 = (ulong)iVar4;
    break;
  case 0x6c:
    lVar3 = *(long *)(param_1 + 0x3b0);
    goto joined_r0x001290ad;
  case 0x6d:
    if (*(long *)(param_1 + 0x918) != 0) {
      if (*(long *)(param_1 + 0x4b0) == 0) {
        return 0;
      }
      EVP_PKEY_up_ref();
      *param_4 = *(long *)(param_1 + 0x4b0);
      return 1;
    }
    goto switchD_00128f9e_caseD_5;
  case 0x6f:
    if (*(long *)(param_1 + 0xab8) != 0) {
      *param_4 = *(long *)(param_1 + 0xab8);
      return (long)*(int *)(param_1 + 0xab0);
    }
    goto switchD_00128f9e_caseD_5;
  case 0x73:
    *param_4 = *(long *)(**(long **)(param_1 + 0x898) + 0x10);
    uVar7 = 1;
    break;
  case 0x74:
    iVar4 = ssl_cert_select_current(*(undefined8 *)(param_1 + 0x898),param_4);
    uVar7 = (ulong)iVar4;
    break;
  case 0x75:
    if (param_3 == 3) {
      uVar7 = 0;
      if (((*(int *)(param_1 + 0x38) != 0) && (*(long *)(param_1 + 0x2e0) != 0)) &&
         (uVar7 = 2, (*(byte *)(*(long *)(param_1 + 0x2e0) + 0x20) & 0x44) == 0)) {
        uVar7 = 0;
        if (*(long *)(param_1 + 0x388) != 0) {
          **(long **)(param_1 + 0x898) = *(long *)(param_1 + 0x388);
          uVar7 = 1;
        }
      }
    }
    else {
      iVar4 = ssl_cert_set_current(*(undefined8 *)(param_1 + 0x898),param_3);
      uVar7 = (ulong)iVar4;
    }
    break;
  case 0x76:
    *(int *)(*(long *)(param_1 + 0x898) + 0x18) = (int)param_3;
    uVar7 = 1;
    break;
  case 0x7f:
    uVar7 = (ulong)*(int *)(param_1 + 0xa60);
    break;
  case 0x84:
    lVar3 = *(long *)(param_1 + 0x380);
joined_r0x001290ad:
    if (lVar3 != 0) {
      *(undefined4 *)param_4 = *(undefined4 *)(lVar3 + 0xc);
      return 1;
    }
    goto switchD_00128f9e_caseD_5;
  case 0x85:
    if (*(long *)(param_1 + 0x918) != 0) {
      if (*(long *)(param_1 + 0x2e8) == 0) {
        return 0;
      }
      EVP_PKEY_up_ref();
      *param_4 = *(long *)(param_1 + 0x2e8);
      return 1;
    }
switchD_00128f9e_caseD_5:
    uVar7 = 0;
    break;
  case 0x86:
    if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
         (iVar4 = **(int **)(param_1 + 8), 0x303 < iVar4)) && (iVar4 != 0x10000)) &&
       (*(char *)(param_1 + 0x4ad) != '\0')) {
      uVar2 = *(undefined2 *)(param_1 + 0x4ae);
    }
    else {
      uVar2 = *(undefined2 *)(*(long *)(param_1 + 0x918) + 0x308);
    }
    iVar4 = tls1_group_id2nid(uVar2,1);
    uVar7 = (ulong)iVar4;
    break;
  case 0x87:
    if (param_4 != (long *)0x0) {
      *param_4 = *(long *)(param_1 + 0xad8);
    }
    uVar7 = (ulong)*(int *)(param_1 + 0xad0);
    break;
  case 0x89:
    iVar4 = ssl_cert_get_cert_store(*(undefined8 *)(param_1 + 0x898),param_4,0);
    uVar7 = (ulong)iVar4;
    break;
  case 0x8a:
    iVar4 = ssl_cert_get_cert_store(*(undefined8 *)(param_1 + 0x898),param_4,1);
    uVar7 = (ulong)iVar4;
  }
  return uVar7;
}