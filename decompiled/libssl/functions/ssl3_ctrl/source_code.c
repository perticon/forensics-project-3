long ssl3_ctrl(SSL *s, int cmd, long larg, void *parg)
{
    int ret = 0;

    switch (cmd) {
    case SSL_CTRL_GET_CLIENT_CERT_REQUEST:
        break;
    case SSL_CTRL_GET_NUM_RENEGOTIATIONS:
        ret = s->s3.num_renegotiations;
        break;
    case SSL_CTRL_CLEAR_NUM_RENEGOTIATIONS:
        ret = s->s3.num_renegotiations;
        s->s3.num_renegotiations = 0;
        break;
    case SSL_CTRL_GET_TOTAL_RENEGOTIATIONS:
        ret = s->s3.total_renegotiations;
        break;
    case SSL_CTRL_GET_FLAGS:
        ret = (int)(s->s3.flags);
        break;
#if !defined(OPENSSL_NO_DEPRECATED_3_0)
    case SSL_CTRL_SET_TMP_DH:
        {
            EVP_PKEY *pkdh = NULL;
            if (parg == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_PASSED_NULL_PARAMETER);
                return 0;
            }
            pkdh = ssl_dh_to_pkey(parg);
            if (pkdh == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_MALLOC_FAILURE);
                return 0;
            }
            if (!SSL_set0_tmp_dh_pkey(s, pkdh)) {
                EVP_PKEY_free(pkdh);
                return 0;
            }
            return 1;
        }
        break;
    case SSL_CTRL_SET_TMP_DH_CB:
        {
            ERR_raise(ERR_LIB_SSL, ERR_R_SHOULD_NOT_HAVE_BEEN_CALLED);
            return ret;
        }
#endif
    case SSL_CTRL_SET_DH_AUTO:
        s->cert->dh_tmp_auto = larg;
        return 1;
#if !defined(OPENSSL_NO_DEPRECATED_3_0)
    case SSL_CTRL_SET_TMP_ECDH:
        {
            if (parg == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_PASSED_NULL_PARAMETER);
                return 0;
            }
            return ssl_set_tmp_ecdh_groups(&s->ext.supportedgroups,
                                           &s->ext.supportedgroups_len,
                                           parg);
        }
#endif                          /* !OPENSSL_NO_DEPRECATED_3_0 */
    case SSL_CTRL_SET_TLSEXT_HOSTNAME:
        /*
         * This API is only used for a client to set what SNI it will request
         * from the server, but we currently allow it to be used on servers
         * as well, which is a programming error.  Currently we just clear
         * the field in SSL_do_handshake() for server SSLs, but when we can
         * make ABI-breaking changes, we may want to make use of this API
         * an error on server SSLs.
         */
        if (larg == TLSEXT_NAMETYPE_host_name) {
            size_t len;

            OPENSSL_free(s->ext.hostname);
            s->ext.hostname = NULL;

            ret = 1;
            if (parg == NULL)
                break;
            len = strlen((char *)parg);
            if (len == 0 || len > TLSEXT_MAXLEN_host_name) {
                ERR_raise(ERR_LIB_SSL, SSL_R_SSL3_EXT_INVALID_SERVERNAME);
                return 0;
            }
            if ((s->ext.hostname = OPENSSL_strdup((char *)parg)) == NULL) {
                ERR_raise(ERR_LIB_SSL, ERR_R_INTERNAL_ERROR);
                return 0;
            }
        } else {
            ERR_raise(ERR_LIB_SSL, SSL_R_SSL3_EXT_INVALID_SERVERNAME_TYPE);
            return 0;
        }
        break;
    case SSL_CTRL_SET_TLSEXT_DEBUG_ARG:
        s->ext.debug_arg = parg;
        ret = 1;
        break;

    case SSL_CTRL_GET_TLSEXT_STATUS_REQ_TYPE:
        ret = s->ext.status_type;
        break;

    case SSL_CTRL_SET_TLSEXT_STATUS_REQ_TYPE:
        s->ext.status_type = larg;
        ret = 1;
        break;

    case SSL_CTRL_GET_TLSEXT_STATUS_REQ_EXTS:
        *(STACK_OF(X509_EXTENSION) **)parg = s->ext.ocsp.exts;
        ret = 1;
        break;

    case SSL_CTRL_SET_TLSEXT_STATUS_REQ_EXTS:
        s->ext.ocsp.exts = parg;
        ret = 1;
        break;

    case SSL_CTRL_GET_TLSEXT_STATUS_REQ_IDS:
        *(STACK_OF(OCSP_RESPID) **)parg = s->ext.ocsp.ids;
        ret = 1;
        break;

    case SSL_CTRL_SET_TLSEXT_STATUS_REQ_IDS:
        s->ext.ocsp.ids = parg;
        ret = 1;
        break;

    case SSL_CTRL_GET_TLSEXT_STATUS_REQ_OCSP_RESP:
        *(unsigned char **)parg = s->ext.ocsp.resp;
        if (s->ext.ocsp.resp_len == 0
                || s->ext.ocsp.resp_len > LONG_MAX)
            return -1;
        return (long)s->ext.ocsp.resp_len;

    case SSL_CTRL_SET_TLSEXT_STATUS_REQ_OCSP_RESP:
        OPENSSL_free(s->ext.ocsp.resp);
        s->ext.ocsp.resp = parg;
        s->ext.ocsp.resp_len = larg;
        ret = 1;
        break;

    case SSL_CTRL_CHAIN:
        if (larg)
            return ssl_cert_set1_chain(s, NULL, (STACK_OF(X509) *)parg);
        else
            return ssl_cert_set0_chain(s, NULL, (STACK_OF(X509) *)parg);

    case SSL_CTRL_CHAIN_CERT:
        if (larg)
            return ssl_cert_add1_chain_cert(s, NULL, (X509 *)parg);
        else
            return ssl_cert_add0_chain_cert(s, NULL, (X509 *)parg);

    case SSL_CTRL_GET_CHAIN_CERTS:
        *(STACK_OF(X509) **)parg = s->cert->key->chain;
        ret = 1;
        break;

    case SSL_CTRL_SELECT_CURRENT_CERT:
        return ssl_cert_select_current(s->cert, (X509 *)parg);

    case SSL_CTRL_SET_CURRENT_CERT:
        if (larg == SSL_CERT_SET_SERVER) {
            const SSL_CIPHER *cipher;
            if (!s->server)
                return 0;
            cipher = s->s3.tmp.new_cipher;
            if (cipher == NULL)
                return 0;
            /*
             * No certificate for unauthenticated ciphersuites or using SRP
             * authentication
             */
            if (cipher->algorithm_auth & (SSL_aNULL | SSL_aSRP))
                return 2;
            if (s->s3.tmp.cert == NULL)
                return 0;
            s->cert->key = s->s3.tmp.cert;
            return 1;
        }
        return ssl_cert_set_current(s->cert, larg);

    case SSL_CTRL_GET_GROUPS:
        {
            uint16_t *clist;
            size_t clistlen;

            if (!s->session)
                return 0;
            clist = s->ext.peer_supportedgroups;
            clistlen = s->ext.peer_supportedgroups_len;
            if (parg) {
                size_t i;
                int *cptr = parg;

                for (i = 0; i < clistlen; i++) {
                    uint16_t cid = SSL_IS_TLS13(s)
                                   ? ssl_group_id_tls13_to_internal(clist[i])
                                   : clist[i];
                    const TLS_GROUP_INFO *cinf
                        = tls1_group_id_lookup(s->ctx, cid);

                    if (cinf != NULL)
                        cptr[i] = tls1_group_id2nid(cinf->group_id, 1);
                    else
                        cptr[i] = TLSEXT_nid_unknown | clist[i];
                }
            }
            return (int)clistlen;
        }

    case SSL_CTRL_SET_GROUPS:
        return tls1_set_groups(&s->ext.supportedgroups,
                               &s->ext.supportedgroups_len, parg, larg);

    case SSL_CTRL_SET_GROUPS_LIST:
        return tls1_set_groups_list(s->ctx, &s->ext.supportedgroups,
                                    &s->ext.supportedgroups_len, parg);

    case SSL_CTRL_GET_SHARED_GROUP:
        {
            uint16_t id = tls1_shared_group(s, larg);

            if (larg != -1)
                return tls1_group_id2nid(id, 1);
            return id;
        }
    case SSL_CTRL_GET_NEGOTIATED_GROUP:
        {
            unsigned int id;

            if (SSL_IS_TLS13(s) && s->s3.did_kex)
                id = s->s3.group_id;
            else
                id = s->session->kex_group;
            ret = tls1_group_id2nid(id, 1);
            break;
        }
    case SSL_CTRL_SET_SIGALGS:
        return tls1_set_sigalgs(s->cert, parg, larg, 0);

    case SSL_CTRL_SET_SIGALGS_LIST:
        return tls1_set_sigalgs_list(s->cert, parg, 0);

    case SSL_CTRL_SET_CLIENT_SIGALGS:
        return tls1_set_sigalgs(s->cert, parg, larg, 1);

    case SSL_CTRL_SET_CLIENT_SIGALGS_LIST:
        return tls1_set_sigalgs_list(s->cert, parg, 1);

    case SSL_CTRL_GET_CLIENT_CERT_TYPES:
        {
            const unsigned char **pctype = parg;
            if (s->server || !s->s3.tmp.cert_req)
                return 0;
            if (pctype)
                *pctype = s->s3.tmp.ctype;
            return s->s3.tmp.ctype_len;
        }

    case SSL_CTRL_SET_CLIENT_CERT_TYPES:
        if (!s->server)
            return 0;
        return ssl3_set_req_cert_type(s->cert, parg, larg);

    case SSL_CTRL_BUILD_CERT_CHAIN:
        return ssl_build_cert_chain(s, NULL, larg);

    case SSL_CTRL_SET_VERIFY_CERT_STORE:
        return ssl_cert_set_cert_store(s->cert, parg, 0, larg);

    case SSL_CTRL_SET_CHAIN_CERT_STORE:
        return ssl_cert_set_cert_store(s->cert, parg, 1, larg);

    case SSL_CTRL_GET_VERIFY_CERT_STORE:
        return ssl_cert_get_cert_store(s->cert, parg, 0);

    case SSL_CTRL_GET_CHAIN_CERT_STORE:
        return ssl_cert_get_cert_store(s->cert, parg, 1);

    case SSL_CTRL_GET_PEER_SIGNATURE_NID:
        if (s->s3.tmp.peer_sigalg == NULL)
            return 0;
        *(int *)parg = s->s3.tmp.peer_sigalg->hash;
        return 1;

    case SSL_CTRL_GET_SIGNATURE_NID:
        if (s->s3.tmp.sigalg == NULL)
            return 0;
        *(int *)parg = s->s3.tmp.sigalg->hash;
        return 1;

    case SSL_CTRL_GET_PEER_TMP_KEY:
        if (s->session == NULL || s->s3.peer_tmp == NULL) {
            return 0;
        } else {
            EVP_PKEY_up_ref(s->s3.peer_tmp);
            *(EVP_PKEY **)parg = s->s3.peer_tmp;
            return 1;
        }

    case SSL_CTRL_GET_TMP_KEY:
        if (s->session == NULL || s->s3.tmp.pkey == NULL) {
            return 0;
        } else {
            EVP_PKEY_up_ref(s->s3.tmp.pkey);
            *(EVP_PKEY **)parg = s->s3.tmp.pkey;
            return 1;
        }

    case SSL_CTRL_GET_EC_POINT_FORMATS:
        {
            const unsigned char **pformat = parg;

            if (s->ext.peer_ecpointformats == NULL)
                return 0;
            *pformat = s->ext.peer_ecpointformats;
            return (int)s->ext.peer_ecpointformats_len;
        }

    case SSL_CTRL_GET_IANA_GROUPS:
        {
            if (parg != NULL) {
                *(uint16_t **)parg = (uint16_t *)s->ext.peer_supportedgroups;
            }
            return (int)s->ext.peer_supportedgroups_len;
        }

    default:
        break;
    }
    return ret;
}