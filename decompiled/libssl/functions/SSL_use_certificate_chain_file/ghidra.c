int SSL_use_certificate_chain_file(SSL *param_1,void *param_2)

{
  undefined8 *puVar1;
  uchar *cb;
  void *u;
  int iVar2;
  BIO_METHOD *type;
  BIO *bp;
  long lVar3;
  X509 *pXVar4;
  ulong uVar5;
  int iVar6;
  long in_FS_OFFSET;
  X509 *local_50;
  X509 *local_48;
  long local_40;
  
  iVar6 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = (X509 *)0x0;
  iVar2 = 0;
  if (param_1 != (SSL *)0x0) {
    puVar1 = *(undefined8 **)&param_1[3].ex_data.dummy;
    ERR_clear_error();
    cb = param_1[10].tlsext_ellipticcurvelist;
    u = param_1[10].tlsext_opaque_prf_input;
    type = BIO_s_file();
    bp = BIO_new(type);
    if (bp == (BIO *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_rsa.c",0x1bf,"use_certificate_chain_file");
      ERR_set_error(0x14,0x80007,0);
    }
    else {
      lVar3 = BIO_ctrl(bp,0x6c,3,param_2);
      if ((int)lVar3 < 1) {
        ERR_new();
        ERR_set_debug("ssl/ssl_rsa.c",0x1c4,"use_certificate_chain_file");
        ERR_set_error(0x14,0x80002,0);
      }
      else {
        local_50 = (X509 *)X509_new_ex(*puVar1,puVar1[0x88]);
        if (local_50 == (X509 *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/ssl_rsa.c",0x1ca,"use_certificate_chain_file");
          ERR_set_error(0x14,0xc0100,0);
        }
        else {
          pXVar4 = PEM_read_bio_X509_AUX(bp,&local_50,cb,u);
          if (pXVar4 == (X509 *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/ssl_rsa.c",0x1cf,"use_certificate_chain_file");
            ERR_set_error(0x14,0x80009,0);
          }
          else {
            iVar2 = SSL_use_certificate(param_1,local_50);
            uVar5 = ERR_peek_error();
            if ((iVar2 == 0) || (uVar5 != 0)) {
              iVar6 = 0;
            }
            else {
              lVar3 = SSL_ctrl(param_1,0x58,0,(void *)0x0);
              iVar6 = (int)lVar3;
              if ((int)lVar3 != 0) {
                do {
                  local_48 = (X509 *)X509_new_ex(*puVar1,puVar1[0x88]);
                  if (local_48 == (X509 *)0x0) {
                    ERR_new();
                    ERR_set_debug("ssl/ssl_rsa.c",0x1f1,"use_certificate_chain_file");
                    ERR_set_error(0x14,0xc0100,0);
                    iVar6 = iVar2;
                    goto LAB_001416a3;
                  }
                  pXVar4 = PEM_read_bio_X509(bp,&local_48,cb,u);
                  if (pXVar4 == (X509 *)0x0) {
                    iVar6 = 0;
                    X509_free(local_48);
                    uVar5 = ERR_peek_last_error();
                    if ((((uVar5 & 0x80000000) == 0) && ((char)(uVar5 >> 0x17) == '\t')) &&
                       (((uint)uVar5 & 0x7fffff) == 0x6c)) {
                      ERR_clear_error();
                      iVar6 = iVar2;
                    }
                    goto LAB_001416a3;
                  }
                  lVar3 = SSL_ctrl(param_1,0x59,0,local_48);
                } while ((int)lVar3 != 0);
                X509_free(local_48);
                iVar6 = (int)lVar3;
              }
            }
          }
        }
      }
    }
LAB_001416a3:
    X509_free(local_50);
    BIO_free(bp);
    iVar2 = iVar6;
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}