undefined8 tls_choose_sigalg(long param_1,int param_2)

{
  long lVar1;
  long *plVar2;
  long lVar3;
  int iVar4;
  uint uVar5;
  int iVar6;
  int *piVar7;
  long lVar8;
  undefined8 uVar9;
  long lVar10;
  ulong uVar11;
  uint uVar12;
  long lVar13;
  ulong uVar14;
  long in_FS_OFFSET;
  bool bVar15;
  int local_50;
  short *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  *(undefined (*) [16])(param_1 + 0x380) = (undefined  [16])0x0;
  uVar12 = *(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60);
  if ((((uVar12 & 8) == 0) && (iVar4 = **(int **)(param_1 + 8), 0x303 < iVar4)) &&
     (iVar4 != 0x10000)) {
    lVar10 = find_sig_alg(0,param_1,0,0);
    if (lVar10 == 0) {
      if (param_2 == 0) goto LAB_0014c240;
      ERR_new();
      ERR_set_debug("ssl/t1_lib.c",0xcd0,"tls_choose_sigalg");
      uVar9 = 0x76;
      goto LAB_0014c447;
    }
LAB_0014c37d:
    lVar13 = (long)*(int *)(lVar10 + 0x18);
LAB_0014c381:
    plVar2 = *(long **)(param_1 + 0x898) + lVar13 * 5 + 4;
    *(long **)(param_1 + 0x388) = plVar2;
    **(long **)(param_1 + 0x898) = (long)plVar2;
    *(long *)(param_1 + 0x380) = lVar10;
  }
  else if ((*(byte *)(*(long *)(param_1 + 0x2e0) + 0x20) & 0xab) != 0) {
    if (*(int *)(param_1 + 0x38) == 0) {
      plVar2 = *(long **)(param_1 + 0x898);
      uVar5 = (int)(*plVar2 - (long)(plVar2 + 4) >> 3) * -0x33333333;
      if (((8 < uVar5) || (plVar2[(long)(int)uVar5 * 5 + 4] == 0)) ||
         (plVar2[(long)(int)uVar5 * 5 + 5] == 0)) goto LAB_0014c240;
    }
    if ((uVar12 & 2) == 0) {
      lVar10 = tls1_get_legacy_sigalg(0,param_1,0xffffffff);
      if (lVar10 != 0) goto LAB_0014c37d;
      if (param_2 != 0) {
        ERR_new();
        ERR_set_debug("ssl/t1_lib.c",0xd3f,"tls_choose_sigalg");
        ossl_statem_fatal(param_1,0x50,0x76,0);
        uVar9 = 0;
        goto LAB_0014c245;
      }
    }
    else if (*(long *)(param_1 + 0x390) == 0) {
      lVar10 = tls1_get_legacy_sigalg(0,param_1,0xffffffff);
      if (lVar10 == 0) {
        if (param_2 != 0) {
          ERR_new();
          ERR_set_debug("ssl/t1_lib.c",0xd27,"tls_choose_sigalg");
          uVar9 = 0x76;
          goto LAB_0014c447;
        }
      }
      else {
        lVar13 = 0;
        lVar8 = tls12_get_psigalgs(param_1,1,&local_48);
        if (lVar8 != 0) {
          do {
            if ((*(short *)(lVar10 + 8) == *local_48) && (*(uint *)(lVar10 + 0x18) < 9)) {
              lVar1 = *(long *)(param_1 + 0x898) + (long)(int)*(uint *)(lVar10 + 0x18) * 0x28;
              lVar3 = *(long *)(lVar1 + 0x20);
              if ((lVar3 != 0) &&
                 ((*(long *)(lVar1 + 0x28) != 0 &&
                  (iVar4 = check_cert_usable_isra_0(param_1,*(undefined4 *)(lVar10 + 0xc),lVar3),
                  iVar4 != 0)))) goto LAB_0014c37d;
            }
            local_48 = local_48 + 1;
            lVar13 = lVar13 + 1;
          } while (lVar8 != lVar13);
        }
        if (param_2 != 0) {
          ERR_new();
          ERR_set_debug("ssl/t1_lib.c",0xd36,"tls_choose_sigalg");
          uVar9 = 0x172;
          goto LAB_0014c447;
        }
      }
    }
    else {
      local_50 = -1;
      if ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) != 0) {
        local_50 = ssl_get_EC_curve_nid(0,*(undefined8 *)(*(long *)(param_1 + 0x898) + 0xa0));
      }
      uVar11 = *(ulong *)(param_1 + 0x1db0);
      uVar14 = 0;
      if (uVar11 != 0) {
        do {
          while( true ) {
            lVar10 = *(long *)(*(long *)(param_1 + 0x1da8) + uVar14 * 8);
            uVar12 = *(uint *)(lVar10 + 0x18);
            if (*(int *)(param_1 + 0x38) != 0) break;
            if ((uVar12 == (int)(**(long **)(param_1 + 0x898) -
                                 (long)(*(long **)(param_1 + 0x898) + 4) >> 3) * -0x33333333) &&
               (uVar12 != 0xffffffff)) goto LAB_0014c19f;
LAB_0014c12e:
            uVar14 = uVar14 + 1;
            bVar15 = uVar14 == uVar11;
            if (uVar11 <= uVar14) goto LAB_0014c32c;
          }
          piVar7 = (int *)ssl_cert_lookup_by_idx((long)(int)uVar12);
          if (((piVar7 == (int *)0x0) ||
              ((*(uint *)(*(long *)(param_1 + 0x2e0) + 0x20) & piVar7[1]) == 0)) ||
             ((((*piVar7 == 0x390 && ((*(byte *)(*(long *)(param_1 + 0x2e0) + 0x1c) & 1) != 0)) ||
               ((*(byte *)(param_1 + 0x3b8 + (long)(int)uVar12 * 4) & 1) == 0)) ||
              (uVar12 == 0xffffffff)))) {
            uVar11 = *(ulong *)(param_1 + 0x1db0);
            uVar12 = 0xffffffff;
            goto LAB_0014c12e;
          }
LAB_0014c19f:
          if (uVar12 < 9) {
            lVar13 = (long)(int)uVar12;
            lVar8 = *(long *)(param_1 + 0x898) + lVar13 * 0x28;
            if (((*(long *)(lVar8 + 0x20) != 0) && (*(long *)(lVar8 + 0x28) != 0)) &&
               (iVar4 = check_cert_usable_isra_0(param_1,*(undefined4 *)(lVar10 + 0xc)), iVar4 != 0)
               ) {
              if (*(int *)(lVar10 + 0x14) != 0x390) {
LAB_0014c1f9:
                uVar11 = *(ulong *)(param_1 + 0x1db0);
                if ((local_50 != -1) && (*(int *)(lVar10 + 0x20) != local_50)) goto LAB_0014c12e;
                if (uVar14 != uVar11) goto LAB_0014c381;
                goto LAB_0014c21b;
              }
              lVar8 = *(long *)(lVar13 * 0x28 + *(long *)(param_1 + 0x898) + 0x28);
              if (((lVar8 != 0) &&
                  (iVar4 = tls1_lookup_md(*(undefined8 *)(param_1 + 0x9a8),lVar10,&local_48),
                  iVar4 != 0)) && (local_48 != (short *)0x0)) {
                iVar4 = EVP_PKEY_get_size(lVar8);
                iVar6 = EVP_MD_get_size(local_48);
                if (iVar6 * 2 + 2 <= iVar4) goto LAB_0014c1f9;
              }
            }
          }
          uVar11 = *(ulong *)(param_1 + 0x1db0);
          uVar14 = uVar14 + 1;
          bVar15 = uVar14 == uVar11;
        } while (uVar14 < uVar11);
LAB_0014c32c:
        if (bVar15) goto LAB_0014c21b;
LAB_0014c332:
        lVar13 = (long)(int)uVar12;
        if (uVar12 == 0xffffffff) goto LAB_0014c37d;
        goto LAB_0014c381;
      }
LAB_0014c21b:
      if ((*(byte *)(*(long *)(param_1 + 0x2e0) + 0x20) & 0xa0) != 0) {
        lVar10 = tls1_get_legacy_sigalg(param_1,0xffffffff);
        if (lVar10 == 0) {
          if (param_2 != 0) {
            ERR_new();
            ERR_set_debug("ssl/t1_lib.c",0xd0d,"tls_choose_sigalg");
            uVar9 = 0x76;
            goto LAB_0014c447;
          }
          goto LAB_0014c240;
        }
        if (*(long *)(param_1 + 0x1db0) != 0) {
          uVar12 = *(uint *)(lVar10 + 0x18);
          goto LAB_0014c332;
        }
      }
      if (param_2 != 0) {
        ERR_new();
        ERR_set_debug("ssl/t1_lib.c",0xd19,"tls_choose_sigalg");
        uVar9 = 0x76;
LAB_0014c447:
        ossl_statem_fatal(param_1,0x28,uVar9,0);
        uVar9 = 0;
        goto LAB_0014c245;
      }
    }
  }
LAB_0014c240:
  uVar9 = 1;
LAB_0014c245:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar9;
}