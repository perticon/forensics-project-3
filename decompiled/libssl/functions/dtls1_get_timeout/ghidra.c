undefined (*) [16] dtls1_get_timeout(long param_1,undefined (*param_2) [16])

{
  undefined4 uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  ulong uVar4;
  long lVar5;
  long lVar6;
  ulong uVar7;
  
  if ((*(long *)(*(long *)(param_1 + 0x4b8) + 0x1f0) == 0) &&
     (*(long *)(*(long *)(param_1 + 0x4b8) + 0x1f8) == 0)) {
    return (undefined (*) [16])0x0;
  }
  uVar4 = ossl_time_now();
  uVar7 = (uVar4 >> 9) / 0x1dcd65;
  lVar5 = *(long *)(param_1 + 0x4b8);
  uVar4 = (uVar4 + uVar7 * -1000000000 >> 3) / 0x7d;
  if (((long)uVar7 <= (long)*(ulong *)(lVar5 + 0x1f0)) &&
     ((*(ulong *)(lVar5 + 0x1f0) != uVar7 ||
      (*(ulong *)(lVar5 + 0x1f8) != uVar4 && (long)uVar4 <= (long)*(ulong *)(lVar5 + 0x1f8))))) {
    uVar1 = *(undefined4 *)(lVar5 + 500);
    uVar2 = *(undefined4 *)(lVar5 + 0x1f8);
    uVar3 = *(undefined4 *)(lVar5 + 0x1fc);
    *(undefined4 *)*param_2 = *(undefined4 *)(lVar5 + 0x1f0);
    *(undefined4 *)(*param_2 + 4) = uVar1;
    *(undefined4 *)(*param_2 + 8) = uVar2;
    *(undefined4 *)(*param_2 + 0xc) = uVar3;
    lVar6 = *(long *)*param_2 - uVar7;
    lVar5 = *(long *)(*param_2 + 8) - uVar4;
    *(long *)*param_2 = lVar6;
    *(long *)(*param_2 + 8) = lVar5;
    if (lVar5 < 0) {
      lVar6 = lVar6 + -1;
      lVar5 = lVar5 + 1000000;
      *(long *)*param_2 = lVar6;
      *(long *)(*param_2 + 8) = lVar5;
    }
    if ((lVar6 != 0) || (14999 < lVar5)) {
      return param_2;
    }
  }
  *param_2 = (undefined  [16])0x0;
  return param_2;
}