int ossl_quic_read(SSL *s, void *buf, size_t len, size_t *readbytes)
{
    int ret;
    BIO *rbio = SSL_get_rbio(s);

    if (rbio == NULL)
        return 0;

    s->rwstate = SSL_READING;
    ret = BIO_read_ex(rbio, buf, len, readbytes);
    if (ret > 0 || !BIO_should_retry(rbio))
        s->rwstate = SSL_NOTHING;
    return ret <= 0 ? -1 : ret;
}