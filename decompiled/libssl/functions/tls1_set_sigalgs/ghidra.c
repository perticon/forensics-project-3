undefined4 tls1_set_sigalgs(long param_1,long param_2,ulong param_3,int param_4)

{
  void *ptr;
  undefined1 *puVar1;
  ulong uVar2;
  undefined4 uVar3;
  
  uVar3 = 0;
  uVar2 = (ulong)((uint)param_3 & 1);
  if ((param_3 & 1) == 0) {
    ptr = CRYPTO_malloc((uint)param_3 & 0xfffffffe,"ssl/t1_lib.c",0xa10);
    if (ptr == (void *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/t1_lib.c",0xa11,"tls1_set_sigalgs");
      ERR_set_error(0x14,0xc0100,0);
      uVar3 = 0;
    }
    else {
      if (param_3 != 0) {
        do {
          puVar1 = sigalg_lookup_tbl;
          while ((*(int *)(puVar1 + 0xc) != *(int *)(param_2 + uVar2 * 4) ||
                 (*(int *)(puVar1 + 0x14) != *(int *)(param_2 + 4 + uVar2 * 4)))) {
            puVar1 = puVar1 + 0x28;
            if (puVar1 == &DAT_001a3978) {
              CRYPTO_free(ptr);
              return 0;
            }
          }
          *(undefined2 *)((long)ptr + uVar2) = *(undefined2 *)(puVar1 + 8);
          uVar2 = uVar2 + 2;
        } while (uVar2 < param_3);
      }
      if (param_4 == 0) {
        CRYPTO_free(*(void **)(param_1 + 0x198));
        *(void **)(param_1 + 0x198) = ptr;
        uVar3 = 1;
        *(ulong *)(param_1 + 0x1a0) = param_3 >> 1;
      }
      else {
        CRYPTO_free(*(void **)(param_1 + 0x1a8));
        *(void **)(param_1 + 0x1a8) = ptr;
        uVar3 = 1;
        *(ulong *)(param_1 + 0x1b0) = param_3 >> 1;
      }
    }
  }
  return uVar3;
}