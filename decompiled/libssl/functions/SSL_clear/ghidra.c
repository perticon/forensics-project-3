int SSL_clear(SSL *s)

{
  void *ptr;
  dtls1_state_st *a;
  undefined8 uVar1;
  X509 *a_00;
  long lVar2;
  SSL_METHOD *pSVar3;
  int iVar4;
  int iVar5;
  
  if (s->method == (SSL_METHOD *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0x23c,"SSL_clear");
    ERR_set_error(0x14,0xbc,0);
    return 0;
  }
  iVar4 = ssl_clear_bad_session();
  if (iVar4 != 0) {
    SSL_SESSION_free(*(SSL_SESSION **)&s[3].sid_ctx_length);
    *(undefined8 *)&s[3].sid_ctx_length = 0;
  }
  SSL_SESSION_free(*(SSL_SESSION **)(s[3].sid_ctx + 4));
  ptr = *(void **)(s[3].sid_ctx + 0xc);
  *(undefined8 *)(s[3].sid_ctx + 4) = 0;
  CRYPTO_free(ptr);
  iVar4 = *(int *)&s[4].expand;
  *(undefined8 *)(s[3].sid_ctx + 0xc) = 0;
  *(undefined8 *)(s[3].sid_ctx + 0x14) = 0;
  *(undefined4 *)&s[3].read_hash = 0;
  s[10].srtp_profile = (SRTP_PROTECTION_PROFILE *)0x0;
  *(undefined4 *)&s[3].psk_server_callback = 0;
  *(undefined4 *)&s[1].tlsext_ecpointformatlist_length = 0;
  s->shutdown = 0;
  if (iVar4 == 0) {
    ossl_statem_clear(s);
    a = s->d1;
    iVar5 = s->method->version;
    s->rwstate = 1;
    s->version = iVar5;
    *(int *)&s[3].field_0x1fc = iVar5;
    BUF_MEM_free((BUF_MEM *)a);
    s->d1 = (dtls1_state_st *)0x0;
    ssl_clear_cipher_ctx(s);
    ssl_clear_hash_ctx(&s[3].init_buf);
    ssl_clear_hash_ctx(&s[3].s3);
    uVar1 = *(undefined8 *)&s[4].sid_ctx_length;
    s[3].tlsext_status_expected = 0;
    *(undefined4 *)((long)&s[4].expand + 4) = 0xffffffff;
    EVP_MD_CTX_free(uVar1);
    a_00 = (X509 *)s[1].tlsext_session_ticket;
    *(undefined8 *)&s[4].sid_ctx_length = 0;
    *(undefined8 *)((long)&s[1].tls_session_ticket_ext_cb + 4) = 0xffffffffffffffff;
    X509_free(a_00);
    *(undefined (*) [16])&s[1].tlsext_opaque_prf_input_len = (undefined  [16])0x0;
    X509_VERIFY_PARAM_move_peername(0,s[1].tlsext_ecpointformatlist,0);
    CRYPTO_free(s[0xb].wbio);
    lVar2 = *(long *)&s[3].ex_data.dummy;
    pSVar3 = s->method;
    s[0xb].wbio = (BIO *)0x0;
    s[0xb].bbio = (BIO *)0x0;
    if (pSVar3 == *(SSL_METHOD **)(lVar2 + 8)) {
      iVar5 = (*pSVar3->ssl_free)(s);
    }
    else {
      (*pSVar3->ssl_accept)(s);
      pSVar3 = *(SSL_METHOD **)(*(long *)&s[3].ex_data.dummy + 8);
      s->method = pSVar3;
      iVar5 = (*pSVar3->ssl_clear)(s);
    }
    if (iVar5 != 0) {
      iVar4 = 1;
      RECORD_LAYER_clear(&s[4].ex_data.dummy);
    }
    return iVar4;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x251,"SSL_clear");
  ERR_set_error(0x14,0xc0103,0);
  return 0;
}