static int do_store(SSL_CONF_CTX *cctx,
                    const char *CAfile, const char *CApath, const char *CAstore,
                    int verify_store)
{
    CERT *cert;
    X509_STORE **st;
    SSL_CTX *ctx;
    OSSL_LIB_CTX *libctx = NULL;
    const char *propq = NULL;

    if (cctx->ctx != NULL) {
        cert = cctx->ctx->cert;
        ctx = cctx->ctx;
    } else if (cctx->ssl != NULL) {
        cert = cctx->ssl->cert;
        ctx = cctx->ssl->ctx;
    } else {
        return 1;
    }
    if (ctx != NULL) {
        libctx = ctx->libctx;
        propq = ctx->propq;
    }
    st = verify_store ? &cert->verify_store : &cert->chain_store;
    if (*st == NULL) {
        *st = X509_STORE_new();
        if (*st == NULL)
            return 0;
    }

    if (CAfile != NULL && !X509_STORE_load_file_ex(*st, CAfile, libctx, propq))
        return 0;
    if (CApath != NULL && !X509_STORE_load_path(*st, CApath))
        return 0;
    if (CAstore != NULL && !X509_STORE_load_store_ex(*st, CAstore, libctx,
                                                     propq))
        return 0;
    return 1;
}