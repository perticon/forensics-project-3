int tls_parse_ctos_cookie(undefined4 *param_1,ushort **param_2)

{
  byte *pbVar1;
  ushort *puVar2;
  char cVar3;
  uint uVar4;
  ushort *puVar5;
  ushort *puVar6;
  int iVar7;
  int iVar8;
  long lVar9;
  EVP_PKEY *pkey;
  long lVar10;
  ulong uVar11;
  ulong uVar12;
  ushort *puVar13;
  undefined8 uVar14;
  ulong uVar15;
  ulong uVar16;
  long in_FS_OFFSET;
  long local_1180;
  undefined8 local_1178;
  undefined local_1170 [8];
  undefined local_1168 [64];
  undefined local_1128 [32];
  undefined local_1108 [4296];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*(long *)(*(long *)(param_1 + 0x26a) + 0xe8) == 0) ||
     ((*(byte *)((long)param_1 + 0xa9) & 8) == 0)) goto LAB_00166e50;
  puVar5 = param_2[1];
  if (puVar5 < (ushort *)0x2) {
LAB_00166b01:
    ERR_new();
    uVar14 = 0x2bf;
  }
  else {
    puVar6 = *param_2;
    puVar13 = (ushort *)(ulong)(ushort)(*puVar6 << 8 | *puVar6 >> 8);
    if (puVar5 + -1 != puVar13) goto LAB_00166b01;
    puVar2 = puVar6 + 1;
    param_2[1] = (ushort *)0x0;
    *param_2 = (ushort *)((long)puVar2 + (long)puVar13);
    if ((ushort *)0x1f < puVar13) {
      lVar9 = EVP_MD_CTX_new();
      uVar14 = 0x166eba;
      pkey = (EVP_PKEY *)
             EVP_PKEY_new_raw_private_key_ex
                       (**(undefined8 **)(param_1 + 0x26a),&DAT_00188069,
                        (*(undefined8 **)(param_1 + 0x26a))[0x88],*(long *)(param_1 + 0x2e2) + 0x2d8
                        ,0x20);
      if ((lVar9 == 0) || (pkey == (EVP_PKEY *)0x0)) {
        EVP_MD_CTX_free(lVar9);
        EVP_PKEY_free(pkey);
        ERR_new();
        ERR_set_debug("ssl/statem/extensions_srvr.c",0x2d6,"tls_parse_ctos_cookie");
        uVar14 = 0xc0100;
      }
      else {
        local_1180 = 0x20;
        iVar8 = EVP_DigestSignInit_ex
                          (lVar9,0,"SHA2-256",**(undefined8 **)(param_1 + 0x26a),
                           (*(undefined8 **)(param_1 + 0x26a))[0x88],pkey,0,uVar14);
        if (iVar8 < 1) {
LAB_00167008:
          EVP_MD_CTX_free(lVar9);
          EVP_PKEY_free(pkey);
          ERR_new();
          uVar14 = 0x2e2;
        }
        else {
          iVar8 = EVP_DigestSign(lVar9,local_1128,&local_1180,puVar2,puVar5 + -0x11);
          if ((iVar8 < 1) || (local_1180 != 0x20)) goto LAB_00167008;
          EVP_MD_CTX_free(lVar9);
          EVP_PKEY_free(pkey);
          iVar8 = CRYPTO_memcmp(local_1128,(void *)((long)puVar2 + (long)(puVar5 + -0x11)),0x20);
          if (iVar8 != 0) {
            ERR_new();
            uVar14 = 0x2ea;
LAB_001670b7:
            iVar8 = 0;
            ERR_set_debug("ssl/statem/extensions_srvr.c",uVar14,"tls_parse_ctos_cookie");
            ossl_statem_fatal(param_1,0x2f,0x134,0);
            goto LAB_00166b37;
          }
          if (puVar6[1] != 0) {
LAB_00166e50:
            iVar8 = 1;
            goto LAB_00166b37;
          }
          if (puVar6[2] != 0x403) {
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x301,"tls_parse_ctos_cookie");
            ossl_statem_fatal(param_1,0x2f,0x74,0);
            goto LAB_00166b37;
          }
          if (*(ushort *)((long)param_1 + 0x4ae) != (ushort)(puVar6[3] << 8 | puVar6[3] >> 8)) {
LAB_00166fcc:
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",0x317,"tls_parse_ctos_cookie");
            ossl_statem_fatal(param_1,0x2f,0xba,0);
            goto LAB_00166b37;
          }
          lVar9 = *(long *)(param_1 + 0xb8);
          lVar10 = ssl_get_cipher_by_char(param_1,puVar6 + 4,0);
          if (lVar9 != lVar10) goto LAB_00166fcc;
          uVar16 = (ulong)(ushort)(*(ushort *)((long)puVar6 + 0xf) << 8 |
                                  *(ushort *)((long)puVar6 + 0xf) >> 8);
          if (((long)puVar13 - 0xfU < uVar16) ||
             (lVar9 = ((long)puVar13 - 0xfU) - uVar16, lVar9 == 0)) {
LAB_00167162:
            ERR_new();
            ERR_set_debug("ssl/statem/extensions_srvr.c",800,"tls_parse_ctos_cookie");
            ossl_statem_fatal(param_1,0x32,0x9f,0);
            goto LAB_00166b37;
          }
          uVar11 = lVar9 - 1;
          pbVar1 = (byte *)((long)puVar6 + 0x11 + uVar16);
          uVar12 = (ulong)*pbVar1;
          if ((uVar11 < uVar12) || (uVar11 - uVar12 != 0x20)) goto LAB_00167162;
          cVar3 = *(char *)(puVar6 + 5);
          uVar4 = *(uint *)((long)puVar6 + 0xb);
          uVar15 = (ulong)(uVar4 >> 0x18 | (uVar4 & 0xff0000) >> 8 | (uVar4 & 0xff00) << 8 |
                          uVar4 << 0x18);
          uVar11 = time((time_t *)0x0);
          if ((uVar11 < uVar15) || (600 < uVar11 - uVar15)) goto LAB_00166e50;
          iVar7 = (**(code **)(*(long *)(param_1 + 0x26a) + 0xe8))(param_1,pbVar1 + 1,uVar12);
          if (iVar7 == 0) {
            ERR_new();
            uVar14 = 0x32e;
            goto LAB_001670b7;
          }
          iVar7 = WPACKET_init_static_len(local_1168,local_1108,0x10c8,0);
          if (iVar7 != 0) {
            iVar7 = WPACKET_put_bytes__(local_1168,2,1);
            if (iVar7 == 0) {
LAB_001671f5:
              WPACKET_cleanup(local_1168);
              ERR_new();
              uVar14 = 0x346;
            }
            else {
              iVar7 = WPACKET_start_sub_packet_len__(local_1168,3);
              if (iVar7 == 0) goto LAB_001671f5;
              iVar7 = WPACKET_put_bytes__(local_1168,0x303,2);
              if (iVar7 == 0) goto LAB_001671f5;
              iVar7 = WPACKET_memcpy(local_1168,hrrrandom,0x20);
              if (iVar7 == 0) goto LAB_001671f5;
              iVar7 = WPACKET_sub_memcpy__
                                (local_1168,param_1 + 0x250,*(undefined8 *)(param_1 + 600),1);
              if (iVar7 == 0) goto LAB_001671f5;
              iVar7 = (**(code **)(*(long *)(param_1 + 2) + 0x98))
                                (*(undefined8 *)(param_1 + 0xb8),local_1168,local_1170);
              if (iVar7 == 0) goto LAB_001671f5;
              iVar7 = WPACKET_put_bytes__(local_1168,0,1);
              if (iVar7 == 0) goto LAB_001671f5;
              iVar7 = WPACKET_start_sub_packet_len__(local_1168,2);
              if (iVar7 == 0) goto LAB_001671f5;
              iVar7 = WPACKET_put_bytes__(local_1168,0x2b,2);
              if (iVar7 == 0) {
LAB_0016719a:
                WPACKET_cleanup(local_1168);
                ERR_new();
                uVar14 = 0x34e;
              }
              else {
                iVar7 = WPACKET_start_sub_packet_len__(local_1168,2);
                if (iVar7 == 0) goto LAB_0016719a;
                iVar7 = WPACKET_put_bytes__(local_1168,*param_1,2);
                if (iVar7 == 0) goto LAB_0016719a;
                iVar7 = WPACKET_close(local_1168);
                if (iVar7 == 0) goto LAB_0016719a;
                if (cVar3 == '\0') {
LAB_00166d76:
                  iVar7 = WPACKET_put_bytes__(local_1168,0x2c,2);
                  if (iVar7 != 0) {
                    iVar7 = WPACKET_start_sub_packet_len__(local_1168,2);
                    if (iVar7 != 0) {
                      iVar7 = WPACKET_sub_memcpy__(local_1168,puVar2,puVar13,2);
                      if (iVar7 != 0) {
                        iVar7 = WPACKET_close(local_1168);
                        if (iVar7 != 0) {
                          iVar7 = WPACKET_close(local_1168);
                          if (iVar7 != 0) {
                            iVar7 = WPACKET_close(local_1168);
                            if (iVar7 != 0) {
                              iVar7 = WPACKET_get_total_written(local_1168,&local_1178);
                              if (iVar7 != 0) {
                                iVar7 = WPACKET_finish(local_1168);
                                if (iVar7 != 0) {
                                  iVar8 = create_synthetic_message_hash
                                                    (param_1,(long)puVar6 + 0x11,uVar16,local_1108,
                                                     local_1178);
                                  if (iVar8 == 0) goto LAB_00166b37;
                                  param_1[0x23a] = 1;
                                  param_1[0x2d2] = 1;
                                  goto LAB_00166e50;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                  WPACKET_cleanup(local_1168);
                  ERR_new();
                  uVar14 = 0x364;
                }
                else {
                  iVar7 = WPACKET_put_bytes__(local_1168,0x33,2);
                  if (iVar7 != 0) {
                    iVar7 = WPACKET_start_sub_packet_len__(local_1168,2);
                    if (iVar7 != 0) {
                      iVar7 = WPACKET_put_bytes__(local_1168,*(undefined2 *)((long)param_1 + 0x4ae),
                                                  2);
                      if (iVar7 != 0) {
                        iVar7 = WPACKET_close(local_1168);
                        if (iVar7 != 0) goto LAB_00166d76;
                      }
                    }
                  }
                  WPACKET_cleanup(local_1168);
                  ERR_new();
                  uVar14 = 0x357;
                }
              }
            }
            ERR_set_debug("ssl/statem/extensions_srvr.c",uVar14,"tls_parse_ctos_cookie");
            ossl_statem_fatal(param_1,0x50,0xc0103,0);
            goto LAB_00166b37;
          }
          ERR_new();
          uVar14 = 0x338;
        }
        ERR_set_debug("ssl/statem/extensions_srvr.c",uVar14,"tls_parse_ctos_cookie");
        uVar14 = 0xc0103;
      }
      iVar8 = 0;
      ossl_statem_fatal(param_1,0x50,uVar14,0);
      goto LAB_00166b37;
    }
    ERR_new();
    uVar14 = 0x2c8;
  }
  iVar8 = 0;
  ERR_set_debug("ssl/statem/extensions_srvr.c",uVar14,"tls_parse_ctos_cookie");
  ossl_statem_fatal(param_1,0x32,0x9f,0);
LAB_00166b37:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar8;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}