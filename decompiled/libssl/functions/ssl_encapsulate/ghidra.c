int ssl_encapsulate(long param_1,long param_2,void **param_3,long *param_4,int param_5)

{
  int iVar1;
  EVP_PKEY_CTX *ctx;
  void *pvVar2;
  void *ptr;
  undefined8 uVar3;
  long in_FS_OFFSET;
  long local_50;
  long local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = 0;
  local_48 = 0;
  if (param_2 == 0) {
    ERR_new();
    iVar1 = 0;
    ERR_set_debug("ssl/s3_lib.c",0x132f,"ssl_encapsulate");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    goto LAB_0012bcec;
  }
  ctx = (EVP_PKEY_CTX *)
        EVP_PKEY_CTX_new_from_pkey
                  (**(undefined8 **)(param_1 + 0x9a8),param_2,
                   (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
  iVar1 = EVP_PKEY_encapsulate_init(ctx,0);
  if (iVar1 < 1) {
LAB_0012bc7c:
    ERR_new();
    ptr = (void *)0x0;
    pvVar2 = (void *)0x0;
    iVar1 = 0;
    ERR_set_debug("ssl/s3_lib.c",0x1338,"ssl_encapsulate");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
  }
  else {
    iVar1 = EVP_PKEY_encapsulate(ctx,0,&local_48,0,&local_50);
    if (((iVar1 < 1) || (local_50 == 0)) || (local_48 == 0)) goto LAB_0012bc7c;
    pvVar2 = CRYPTO_malloc((int)local_50,"ssl/s3_lib.c",0x133c);
    ptr = CRYPTO_malloc((int)local_48,"ssl/s3_lib.c",0x133d);
    if ((pvVar2 == (void *)0x0) || (ptr == (void *)0x0)) {
      ERR_new();
      ERR_set_debug("ssl/s3_lib.c",0x133f,"ssl_encapsulate");
      uVar3 = 0xc0100;
LAB_0012be24:
      iVar1 = 0;
      ossl_statem_fatal(param_1,0x50,uVar3,0);
    }
    else {
      iVar1 = EVP_PKEY_encapsulate(ctx,ptr,&local_48,pvVar2,&local_50);
      if (iVar1 < 1) {
        ERR_new();
        ERR_set_debug("ssl/s3_lib.c",0x1344,"ssl_encapsulate");
        uVar3 = 0xc0103;
        goto LAB_0012be24;
      }
      if (param_5 == 0) {
        *(long *)(param_1 + 0x368) = local_50;
        iVar1 = 1;
        *(void **)(param_1 + 0x360) = pvVar2;
        pvVar2 = (void *)0x0;
LAB_0012bd9a:
        *param_3 = ptr;
        ptr = (void *)0x0;
        *param_4 = local_48;
      }
      else {
        iVar1 = ssl_gensecret(param_1,pvVar2,local_50);
        if (0 < iVar1) goto LAB_0012bd9a;
      }
    }
  }
  CRYPTO_clear_free(pvVar2,local_50,"ssl/s3_lib.c",0x135b);
  CRYPTO_free(ptr);
  EVP_PKEY_CTX_free(ctx);
LAB_0012bcec:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}