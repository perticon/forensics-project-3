undefined8 check_in_list(undefined8 param_1,short param_2,long param_3,ulong param_4,int param_5)

{
  short sVar1;
  int iVar2;
  ulong uVar3;
  
  if (param_3 == 0) {
    return 0;
  }
  if (param_4 == 0) {
    return 0;
  }
  if (param_5 == 1) {
    param_2 = ssl_group_id_tls13_to_internal(param_2);
  }
  else {
    if (param_5 == 2) {
      uVar3 = 0;
      while ((sVar1 = ssl_group_id_tls13_to_internal(*(undefined2 *)(param_3 + uVar3 * 2)),
             param_2 != sVar1 || (iVar2 = tls_group_allowed(param_1,param_2,0x20006), iVar2 == 0)))
      {
        uVar3 = uVar3 + 1;
        if (param_4 <= uVar3) {
          return 0;
        }
      }
      return 1;
    }
    if (param_5 == 0) {
      uVar3 = 0;
      do {
        if (*(short *)(param_3 + uVar3 * 2) == param_2) {
          return 1;
        }
        uVar3 = uVar3 + 1;
      } while (uVar3 < param_4);
      return 0;
    }
  }
  uVar3 = 0;
  while ((param_2 != *(short *)(param_3 + uVar3 * 2) ||
         (iVar2 = tls_group_allowed(param_1,param_2,0x20006), iVar2 == 0))) {
    uVar3 = uVar3 + 1;
    if (param_4 <= uVar3) {
      return 0;
    }
  }
  return 1;
}