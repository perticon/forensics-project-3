undefined8 tls_parse_stoc_maxfragmentlen(long param_1,byte **param_2)

{
  byte bVar1;
  undefined8 uVar2;
  
  if (param_2[1] != (byte *)0x1) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x4fd,"tls_parse_stoc_maxfragmentlen");
    ossl_statem_fatal(param_1,0x32,0x6e,0);
    return 0;
  }
  bVar1 = **param_2;
  param_2[1] = (byte *)0x0;
  *param_2 = *param_2 + 1;
  if (bVar1 - 1 < 4) {
    if (*(byte *)(param_1 + 0xb4c) == bVar1) {
      *(byte *)(*(long *)(param_1 + 0x918) + 0x368) = bVar1;
      return 1;
    }
    ERR_new();
    uVar2 = 0x50f;
  }
  else {
    ERR_new();
    uVar2 = 0x503;
  }
  ERR_set_debug("ssl/statem/extensions_clnt.c",uVar2,"tls_parse_stoc_maxfragmentlen");
  ossl_statem_fatal(param_1,0x2f,0xe8,0);
  return 0;
}