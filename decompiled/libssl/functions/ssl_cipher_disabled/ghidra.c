bool ssl_cipher_disabled(long param_1,long param_2,undefined4 param_3,int param_4)

{
  int iVar1;
  int iVar2;
  
  if ((*(uint *)(param_1 + 0x3dc) & *(uint *)(param_2 + 0x1c)) != 0) {
    return true;
  }
  if ((*(uint *)(param_1 + 0x3e0) & *(uint *)(param_2 + 0x20)) != 0) {
    return true;
  }
  iVar1 = *(int *)(param_1 + 1000);
  if (iVar1 == 0) {
    return true;
  }
  if ((*(byte *)(*(long *)(*(long *)(param_1 + 8) + 0xc0) + 0x60) & 8) == 0) {
    iVar2 = *(int *)(param_2 + 0x2c);
    if ((iVar2 == 0x301) && (param_4 != 0)) {
      iVar2 = ((*(uint *)(param_2 + 0x1c) & 0x84) == 0) + 0x300;
    }
    if (iVar1 < iVar2) {
      return true;
    }
    if (*(int *)(param_2 + 0x30) < *(int *)(param_1 + 0x3e4)) {
      return true;
    }
    goto LAB_00149103;
  }
  iVar2 = *(int *)(param_2 + 0x34);
  if (iVar2 == 0x100) {
    iVar2 = 0xff00;
    if (iVar1 != 0x100) goto LAB_0014913c;
  }
  else {
    if (iVar1 == 0x100) {
      iVar1 = 0xff00;
    }
LAB_0014913c:
    if (iVar2 < iVar1) {
      return true;
    }
  }
  iVar1 = *(int *)(param_2 + 0x38);
  iVar2 = *(int *)(param_1 + 0x3e4);
  if (iVar1 == 0x100) {
    iVar1 = 0xff00;
    if (iVar2 == 0x100) goto LAB_00149103;
  }
  else if (iVar2 == 0x100) {
    iVar2 = 0xff00;
  }
  if (iVar2 < iVar1) {
    return true;
  }
LAB_00149103:
  iVar1 = ssl_security(param_1,param_3,*(undefined4 *)(param_2 + 0x44),0);
  return iVar1 == 0;
}