undefined4 dtls1_retransmit_message(long param_1,ushort param_2,undefined4 *param_3)

{
  undefined uVar1;
  undefined2 uVar2;
  undefined2 uVar3;
  undefined *puVar4;
  long lVar5;
  undefined8 uVar6;
  long lVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  BIO *bp;
  undefined4 uVar11;
  pitem *ppVar12;
  long lVar13;
  long in_FS_OFFSET;
  undefined4 local_48;
  undefined2 local_44;
  ushort local_42;
  long local_40;
  
  local_42 = param_2 << 8 | param_2 >> 8;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_44 = 0;
  local_48 = 0;
  ppVar12 = pqueue_find(*(pqueue *)(*(long *)(param_1 + 0x4b8) + 0x120),(uchar *)&local_48);
  if (ppVar12 == (pitem *)0x0) {
    ERR_new();
    uVar11 = 0;
    ERR_set_debug("ssl/statem/statem_dtls.c",0x465,"dtls1_retransmit_message");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    *param_3 = 0;
  }
  else {
    *param_3 = 1;
    puVar4 = (undefined *)ppVar12->data;
    lVar13 = (ulong)(-(uint)(*(int *)(puVar4 + 0x28) == 0) & 0xb) + 1;
    memcpy(*(void **)(*(long *)(param_1 + 0x88) + 8),*(void **)(puVar4 + 0x58),
           *(long *)(puVar4 + 8) + lVar13);
    lVar5 = *(long *)(puVar4 + 8);
    uVar6 = *(undefined8 *)(puVar4 + 0x20);
    uVar2 = *(undefined2 *)(puVar4 + 0x10);
    lVar7 = *(long *)(param_1 + 0x4b8);
    *(long *)(param_1 + 0x98) = lVar13 + lVar5;
    uVar1 = *puVar4;
    *(undefined2 *)(lVar7 + 0x148) = uVar2;
    *(undefined *)(lVar7 + 0x138) = uVar1;
    lVar13 = *(long *)(param_1 + 0x1d10);
    *(undefined8 *)(lVar7 + 0x158) = uVar6;
    uVar6 = *(undefined8 *)(param_1 + 0x890);
    *(long *)(lVar7 + 0x140) = lVar5;
    uVar8 = *(undefined8 *)(param_1 + 0x878);
    uVar9 = *(undefined8 *)(param_1 + 0x868);
    uVar10 = *(undefined8 *)(param_1 + 0x918);
    *(undefined8 *)(lVar7 + 0x150) = 0;
    uVar2 = *(undefined2 *)(lVar13 + 2);
    *(undefined4 *)(lVar7 + 0x204) = 1;
    uVar3 = *(undefined2 *)(puVar4 + 0x50);
    *(undefined8 *)(param_1 + 0x878) = *(undefined8 *)(puVar4 + 0x30);
    *(undefined8 *)(param_1 + 0x890) = *(undefined8 *)(puVar4 + 0x38);
    *(undefined8 *)(param_1 + 0x868) = *(undefined8 *)(puVar4 + 0x40);
    *(undefined8 *)(param_1 + 0x918) = *(undefined8 *)(puVar4 + 0x48);
    DTLS_RECORD_LAYER_set_saved_w_epoch(param_1 + 0xc58,uVar3);
    uVar11 = dtls1_do_write(param_1,(-(*(int *)(puVar4 + 0x28) == 0) & 2U) + 0x14);
    *(undefined8 *)(param_1 + 0x868) = uVar9;
    *(undefined8 *)(param_1 + 0x890) = uVar6;
    *(undefined8 *)(param_1 + 0x878) = uVar8;
    *(undefined8 *)(param_1 + 0x918) = uVar10;
    DTLS_RECORD_LAYER_set_saved_w_epoch(param_1 + 0xc58,uVar2);
    bp = *(BIO **)(param_1 + 0x18);
    *(undefined4 *)(*(long *)(param_1 + 0x4b8) + 0x204) = 0;
    BIO_ctrl(bp,0xb,0,(void *)0x0);
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar11;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}