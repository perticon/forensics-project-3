undefined8 dtls1_handle_timeout(long param_1)

{
  long lVar1;
  undefined4 uVar2;
  int iVar3;
  undefined8 uVar4;
  uint uVar5;
  
  uVar4 = dtls1_is_timer_expired();
  if ((int)uVar4 != 0) {
    lVar1 = *(long *)(param_1 + 0x4b8);
    if (*(code **)(lVar1 + 0x208) == (code *)0x0) {
      uVar5 = *(int *)(lVar1 + 0x200) * 2;
      if (60000000 < uVar5) {
        uVar5 = 60000000;
      }
      *(uint *)(lVar1 + 0x200) = uVar5;
    }
    else {
      uVar2 = (**(code **)(lVar1 + 0x208))(param_1);
      *(undefined4 *)(lVar1 + 0x200) = uVar2;
    }
    iVar3 = dtls1_check_timeout_num(param_1);
    if (-1 < iVar3) {
      dtls1_start_timer(param_1);
      uVar4 = dtls1_retransmit_buffered_messages(param_1);
      return uVar4;
    }
    uVar4 = 0xffffffff;
  }
  return uVar4;
}