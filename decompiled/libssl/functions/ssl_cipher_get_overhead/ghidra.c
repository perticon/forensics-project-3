ssl_cipher_get_overhead(long param_1,long *param_2,undefined8 *param_3,long *param_4,long *param_5)

{
  int iVar1;
  char *pcVar2;
  EVP_MD *pEVar3;
  EVP_CIPHER *pEVar4;
  long lVar5;
  long lVar6;
  long lVar7;
  undefined8 uVar8;
  
  if ((*(uint *)(param_1 + 0x24) & 0x30f000) == 0) {
    if ((*(uint *)(param_1 + 0x24) & 0xb0000) == 0) {
      if ((*(byte *)(param_1 + 0x28) & 0x40) == 0) {
        iVar1 = SSL_CIPHER_get_digest_nid();
        pcVar2 = OBJ_nid2sn(iVar1);
        pEVar3 = EVP_get_digestbyname(pcVar2);
        if (pEVar3 != (EVP_MD *)0x0) {
          iVar1 = EVP_MD_get_size();
          lVar6 = (long)iVar1;
          if (*(int *)(param_1 + 0x24) == 0x20) {
            lVar7 = 0;
            lVar5 = 0;
            uVar8 = 0;
          }
          else {
            iVar1 = SSL_CIPHER_get_cipher_nid();
            pcVar2 = OBJ_nid2sn(iVar1);
            pEVar4 = EVP_get_cipherbyname(pcVar2);
            if (pEVar4 == (EVP_CIPHER *)0x0) {
              return 0;
            }
            iVar1 = EVP_CIPHER_get_mode(pEVar4);
            if (iVar1 != 2) {
              return 0;
            }
            iVar1 = EVP_CIPHER_get_iv_length(pEVar4);
            lVar7 = (long)iVar1;
            iVar1 = EVP_CIPHER_get_block_size(pEVar4);
            uVar8 = 1;
            lVar5 = (long)iVar1;
          }
          goto LAB_001326e1;
        }
      }
      return 0;
    }
    lVar7 = 0x10;
    lVar5 = 0;
    uVar8 = 0;
    lVar6 = 0;
  }
  else {
    lVar7 = 0x18;
    lVar5 = 0;
    uVar8 = 0;
    lVar6 = 0;
  }
LAB_001326e1:
  *param_2 = lVar6;
  *param_3 = uVar8;
  *param_4 = lVar5;
  *param_5 = lVar7;
  return 1;
}