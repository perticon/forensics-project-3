int SSL_get_security_level(const SSL *s)
{
    return s->cert->sec_level;
}