void ssl3_free(long param_1)

{
  ulong uVar1;
  undefined8 *puVar2;
  byte bVar3;
  
  bVar3 = 0;
  if (param_1 != 0) {
    ssl3_cleanup_key_block();
    EVP_PKEY_free(*(EVP_PKEY **)(param_1 + 0x4b0));
    *(undefined8 *)(param_1 + 0x4b0) = 0;
    EVP_PKEY_free(*(EVP_PKEY **)(param_1 + 0x2e8));
    *(undefined8 *)(param_1 + 0x2e8) = 0;
    ssl_evp_cipher_free(*(undefined8 *)(param_1 + 800));
    ssl_evp_md_free(*(undefined8 *)(param_1 + 0x328));
    CRYPTO_free(*(void **)(param_1 + 0x2f8));
    OPENSSL_sk_pop_free(*(undefined8 *)(param_1 + 0x308),PTR_X509_NAME_free_001a7fc8);
    CRYPTO_free(*(void **)(param_1 + 0x350));
    CRYPTO_clear_free(*(undefined8 *)(param_1 + 0x360),*(undefined8 *)(param_1 + 0x368),
                      "ssl/s3_lib.c");
    CRYPTO_free(*(void **)(param_1 + 0x390));
    CRYPTO_free(*(void **)(param_1 + 0x398));
    ssl3_free_digest_list(param_1);
    CRYPTO_free(*(void **)(param_1 + 0x488));
    CRYPTO_free(*(void **)(param_1 + 0x498));
    ssl_srp_ctx_free_intern(param_1);
    *(undefined8 *)(param_1 + 0xa8) = 0;
    *(undefined8 *)(param_1 + 0x4b0) = 0;
    puVar2 = (undefined8 *)(param_1 + 0xb0U & 0xfffffffffffffff8);
    uVar1 = (ulong)(((int)param_1 - (int)puVar2) + 0x4b8U >> 3);
    for (; uVar1 != 0; uVar1 = uVar1 - 1) {
      *puVar2 = 0;
      puVar2 = puVar2 + (ulong)bVar3 * -2 + 1;
    }
    return;
  }
  return;
}