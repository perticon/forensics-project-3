void ssl3_free(SSL *s)
{
    if (s == NULL)
        return;

    ssl3_cleanup_key_block(s);

    EVP_PKEY_free(s->s3.peer_tmp);
    s->s3.peer_tmp = NULL;
    EVP_PKEY_free(s->s3.tmp.pkey);
    s->s3.tmp.pkey = NULL;

    ssl_evp_cipher_free(s->s3.tmp.new_sym_enc);
    ssl_evp_md_free(s->s3.tmp.new_hash);

    OPENSSL_free(s->s3.tmp.ctype);
    sk_X509_NAME_pop_free(s->s3.tmp.peer_ca_names, X509_NAME_free);
    OPENSSL_free(s->s3.tmp.ciphers_raw);
    OPENSSL_clear_free(s->s3.tmp.pms, s->s3.tmp.pmslen);
    OPENSSL_free(s->s3.tmp.peer_sigalgs);
    OPENSSL_free(s->s3.tmp.peer_cert_sigalgs);
    ssl3_free_digest_list(s);
    OPENSSL_free(s->s3.alpn_selected);
    OPENSSL_free(s->s3.alpn_proposed);

#ifndef OPENSSL_NO_SRP
    ssl_srp_ctx_free_intern(s);
#endif
    memset(&s->s3, 0, sizeof(s->s3));
}