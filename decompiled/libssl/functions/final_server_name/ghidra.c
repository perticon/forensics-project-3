int final_server_name(SSL *param_1,undefined8 param_2,int param_3)

{
  int *piVar1;
  int iVar2;
  long lVar3;
  int iVar4;
  ulong uVar5;
  long lVar6;
  SSL_SESSION *pSVar7;
  char *pcVar8;
  undefined8 uVar9;
  long in_FS_OFFSET;
  undefined4 local_34;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_34 = 0x70;
  uVar5 = SSL_get_options();
  lVar6 = *(long *)&param_1[3].ex_data.dummy;
  if ((lVar6 == 0) || (lVar3 = *(long *)&param_1[4].mac_flags, lVar3 == 0)) {
    ERR_new();
    iVar4 = 0;
    ERR_set_debug("ssl/statem/extensions.c",0x39f,"final_server_name");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    goto LAB_0015deae;
  }
  if (*(code **)(lVar6 + 0x218) == (code *)0x0) {
    iVar4 = 3;
    if (*(code **)(lVar3 + 0x218) != (code *)0x0) {
      iVar4 = (**(code **)(lVar3 + 0x218))(param_1,&local_34,*(undefined8 *)(lVar3 + 0x220));
      goto LAB_0015de37;
    }
  }
  else {
    iVar4 = (**(code **)(lVar6 + 0x218))(param_1,&local_34,*(undefined8 *)(lVar6 + 0x220));
LAB_0015de37:
    if (((param_1->server != 0) && (param_3 != 0)) && (iVar4 == 0)) {
      iVar4 = *(int *)&param_1[1].tlsext_ecpointformatlist_length;
      if (iVar4 == 0) {
        CRYPTO_free(*(void **)(*(long *)&param_1[3].sid_ctx_length + 0x330));
        lVar6 = *(long *)&param_1[3].sid_ctx_length;
        pcVar8 = CRYPTO_strdup((char *)param_1[3].tlsext_opaque_prf_input_len,
                               "ssl/statem/extensions.c",0x3b6);
        *(char **)(lVar6 + 0x330) = pcVar8;
        if ((*(long *)(*(long *)&param_1[3].sid_ctx_length + 0x330) == 0) &&
           (param_1[3].tlsext_opaque_prf_input_len != 0)) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions.c",0x3b8,"final_server_name");
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
          goto LAB_0015de5a;
        }
      }
      iVar4 = 0;
    }
  }
LAB_0015de5a:
  if (param_1->tlsext_opaque_prf_input == (void *)0x0) {
    lVar6 = *(long *)&param_1[3].ex_data.dummy;
    if (lVar6 == *(long *)&param_1[4].mac_flags) goto LAB_0015de86;
LAB_0015de78:
    if (*(int *)&param_1[3].read_hash != 0) goto LAB_0015de86;
    LOCK();
    *(int *)(lVar6 + 0x84) = *(int *)(lVar6 + 0x84) + 1;
    LOCK();
    piVar1 = (int *)(*(long *)&param_1[4].mac_flags + 0x84);
    *piVar1 = *piVar1 + -1;
    if (iVar4 != 0) goto LAB_0015ded0;
LAB_0015de8b:
    if ((((uVar5 & 0x4000) == 0) && (*(int *)&param_1[3].next_proto_negotiated_len != 0)) &&
       (uVar5 = SSL_get_options(param_1), (uVar5 & 0x4000) != 0)) {
      iVar4 = *(int *)&param_1[1].tlsext_ecpointformatlist_length;
      *(undefined4 *)&param_1[3].next_proto_negotiated_len = 0;
      if (iVar4 == 0) {
        pSVar7 = SSL_get_session(param_1);
        if (pSVar7 == (SSL_SESSION *)0x0) {
          ERR_new();
          uVar9 = 0x3df;
        }
        else {
          CRYPTO_free(*(void **)(pSVar7[1].krb5_client_princ + 0x50));
          *(undefined8 *)(pSVar7[1].krb5_client_princ + 0x50) = 0;
          *(undefined4 *)(pSVar7[1].krb5_client_princ + 0x68) = 0;
          *(undefined (*) [16])(pSVar7[1].krb5_client_princ + 0x58) = (undefined  [16])0x0;
          iVar4 = ssl_generate_session_id(0,param_1,pSVar7);
          if (iVar4 != 0) goto LAB_0015dea8;
          ERR_new();
          uVar9 = 0x3db;
        }
        ERR_set_debug("ssl/statem/extensions.c",uVar9,"final_server_name");
        ossl_statem_fatal(param_1,0x50,0xc0103,0);
        goto LAB_0015deae;
      }
    }
  }
  else {
    if ((param_1[1].wbio == (BIO *)0x0) &&
       (lVar6 = *(long *)&param_1[3].ex_data.dummy, lVar6 != *(long *)&param_1[4].mac_flags))
    goto LAB_0015de78;
LAB_0015de86:
    if (iVar4 == 0) goto LAB_0015de8b;
LAB_0015ded0:
    if (iVar4 == 2) {
      ERR_new();
      iVar4 = 0;
      ERR_set_debug("ssl/statem/extensions.c",999,"final_server_name");
      ossl_statem_fatal(param_1,local_34,0xea,0);
      goto LAB_0015deae;
    }
    if (iVar4 == 3) {
      *(undefined4 *)&param_1[4].msg_callback_arg = 0;
    }
    else if (iVar4 == 1) {
      if (((((byte)param_1->method->get_timeout[0x60] & 8) != 0) ||
          (iVar2 = param_1->method->version, iVar2 == 0x10000)) || (iVar2 < 0x304)) {
        ssl3_send_alert(param_1,1,local_34);
      }
      *(undefined4 *)&param_1[4].msg_callback_arg = 0;
      goto LAB_0015deae;
    }
  }
LAB_0015dea8:
  iVar4 = 1;
LAB_0015deae:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar4;
}