undefined8 OPENSSL_init_ssl(ulong param_1)

{
  int iVar1;
  ulong uVar2;
  
  if (stopped == 0) {
    uVar2 = param_1 | 0x4c;
    if ((param_1 & 0x80) != 0) {
      uVar2 = param_1 | 0xc;
    }
    iVar1 = OPENSSL_init_crypto(uVar2);
    if (((((iVar1 != 0) &&
          (iVar1 = CRYPTO_THREAD_run_once(&ssl_base,ossl_init_ssl_base_ossl_), iVar1 != 0)) &&
         (ossl_init_ssl_base_ossl_ret_ != 0)) &&
        (((uVar2 & 0x100000) == 0 ||
         ((iVar1 = CRYPTO_THREAD_run_once(&ssl_strings,ossl_init_no_load_ssl_strings_ossl_),
          iVar1 != 0 && (ossl_init_load_ssl_strings_ossl_ret_ != 0)))))) &&
       (((uVar2 & 0x200000) == 0 ||
        ((iVar1 = CRYPTO_THREAD_run_once(&ssl_strings,ossl_init_load_ssl_strings_ossl_), iVar1 != 0
         && (ossl_init_load_ssl_strings_ossl_ret_ != 0)))))) {
      return 1;
    }
  }
  else if (stoperrset_25445 == 0) {
    stoperrset_25445 = 1;
    ERR_new();
    ERR_set_debug("ssl/ssl_init.c",0x67,"OPENSSL_init_ssl");
    ERR_set_error(0x14,0xc0105,0);
    return 0;
  }
  return 0;
}