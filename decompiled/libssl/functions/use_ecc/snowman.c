int32_t use_ecc(void** rdi, int32_t esi, int32_t edx) {
    uint64_t rax4;
    uint64_t v5;
    int1_t zf6;
    int32_t eax7;
    void** rbx8;
    int32_t r12d9;
    int32_t r13d10;
    void** rax11;
    void** r15_12;
    int64_t rax13;
    void* rsp14;
    int32_t ebp15;
    int32_t r14d16;
    void** rax17;
    uint64_t rbx18;
    uint64_t rbp19;
    void* rsp20;
    int64_t v21;
    void** rcx22;
    int64_t rdx23;
    uint32_t r14d24;
    int64_t rsi25;
    int32_t eax26;
    void** rsi27;
    int32_t eax28;
    uint64_t v29;

    rax4 = g28;
    v5 = rax4;
    zf6 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 0x300);
    if (zf6) {
        addr_600a8_2:
        eax7 = 0;
    } else {
        rbx8 = rdi;
        r12d9 = esi;
        r13d10 = edx;
        rax11 = SSL_get1_supported_ciphers();
        r15_12 = rax11;
        rax13 = fun_20630(rax11);
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8);
        ebp15 = *reinterpret_cast<int32_t*>(&rax13);
        if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax13) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax13) == 0)) {
            addr_600a0_4:
            fun_20d50(r15_12, r15_12);
            goto addr_600a8_2;
        } else {
            r14d16 = 0;
            do {
                rax17 = fun_212d0(r15_12, r15_12);
                rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax17 + 28)) & 0x84 | reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax17 + 32)) & 8) 
                    goto addr_60024_7;
                if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax17 + 44)) > reinterpret_cast<signed char>(0x303)) 
                    goto addr_60024_7;
                ++r14d16;
            } while (ebp15 != r14d16);
            goto addr_600a0_4;
        }
    }
    addr_600aa_10:
    rbx18 = v5 ^ g28;
    if (rbx18) {
        fun_20ff0();
    } else {
        return eax7;
    }
    addr_60024_7:
    *reinterpret_cast<int32_t*>(&rbp19) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp19) + 4) = 0;
    fun_20d50(r15_12, r15_12);
    rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    tls1_get_supported_groups(rbx8, reinterpret_cast<int64_t>(rsp20) + 8, reinterpret_cast<int64_t>(rsp20) + 16);
    if (v21) {
        do {
            *reinterpret_cast<int32_t*>(&rcx22) = r13d10;
            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx23) = r12d9;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
            r14d24 = *reinterpret_cast<uint16_t*>(rbp19 * 2);
            *reinterpret_cast<uint32_t*>(&rsi25) = r14d24;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0;
            eax26 = tls_valid_group(rbx8, rsi25, rdx23, rcx22, rbx8, rsi25, rdx23, rcx22);
            if (!eax26) 
                continue;
            *reinterpret_cast<uint32_t*>(&rsi27) = r14d24;
            *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
            eax28 = tls_group_allowed(rbx8, rsi27, 0x20004, rcx22, rbx8, rsi27, 0x20004, rcx22);
            if (eax28) 
                break;
            ++rbp19;
        } while (v29 > rbp19);
        goto addr_600a8_2;
    } else {
        goto addr_600a8_2;
    }
    eax7 = 1;
    goto addr_600aa_10;
}