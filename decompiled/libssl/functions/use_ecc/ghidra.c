undefined8 use_ecc(int *param_1,undefined4 param_2,undefined4 param_3)

{
  undefined2 uVar1;
  int iVar2;
  undefined8 uVar3;
  long lVar4;
  ulong uVar5;
  int iVar6;
  long in_FS_OFFSET;
  long local_50;
  ulong local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = 0;
  if (*param_1 != 0x300) {
    uVar3 = SSL_get1_supported_ciphers();
    iVar2 = OPENSSL_sk_num(uVar3);
    if (0 < iVar2) {
      iVar6 = 0;
      do {
        lVar4 = OPENSSL_sk_value(uVar3,iVar6);
        if (((*(uint *)(lVar4 + 0x1c) & 0x84 | *(uint *)(lVar4 + 0x20) & 8) != 0) ||
           (0x303 < *(int *)(lVar4 + 0x2c))) {
          uVar5 = 0;
          OPENSSL_sk_free(uVar3);
          tls1_get_supported_groups(param_1,&local_50,&local_48);
          if (local_48 == 0) goto LAB_001600a8;
          goto LAB_0016005b;
        }
        iVar6 = iVar6 + 1;
      } while (iVar2 != iVar6);
    }
    OPENSSL_sk_free(uVar3);
  }
  goto LAB_001600a8;
  while (uVar5 = uVar5 + 1, uVar5 < local_48) {
LAB_0016005b:
    uVar1 = *(undefined2 *)(local_50 + uVar5 * 2);
    iVar2 = tls_valid_group(param_1,uVar1,param_2,param_3,1,0);
    if ((iVar2 != 0) && (iVar2 = tls_group_allowed(param_1,uVar1,0x20004), iVar2 != 0)) {
      uVar3 = 1;
      goto LAB_001600aa;
    }
  }
LAB_001600a8:
  uVar3 = 0;
LAB_001600aa:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar3;
}