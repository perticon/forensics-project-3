int64_t use_ecc(int64_t a1, int64_t a2, int64_t a3) {
    // 0x5ff90
    int64_t v1; // 0x5ff90
    uint64_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x5ff9e
    int64_t result = 0; // 0x5ffbd
    if ((int32_t)a1 != (int32_t)&g86) {
        // 0x5ffc3
        SSL_get1_supported_ciphers();
        int32_t v4 = function_20630(); // 0x5ffde
        int64_t v5 = 0; // 0x5ffe0
        if (v4 < 1) {
          lab_0x600a0:
            // 0x600a0
            function_20d50();
            result = 0;
        } else {
            int64_t v6 = function_212d0(); // 0x6000c
            while ((*(int32_t *)(v6 + 32) & 8 | *(int32_t *)(v6 + 28) & 132) == 0) {
                // 0x5fff0
                if (*(int32_t *)(v6 + 44) > (int32_t)&g90) {
                    // break -> 0x60024
                    break;
                }
                int64_t v7 = v5 + 1; // 0x5fff9
                v5 = v7 & 0xffffffff;
                if (v4 == (int32_t)v7) {
                    goto lab_0x600a0;
                }
                v6 = function_212d0();
            }
            // 0x60024
            function_20d50();
            tls1_get_supported_groups();
            result = 0;
            if (v2 != 0) {
                if ((int32_t)tls_valid_group() != 0) {
                    // 0x60083
                    if ((int32_t)tls_group_allowed() != 0) {
                        // break -> 0x600aa
                        break;
                    }
                }
                int64_t v8 = 1; // 0x60050
                int64_t v9 = v8; // 0x60059
                result = 0;
                while (v2 > v8) {
                    // 0x6005b
                    if ((int32_t)tls_valid_group() != 0) {
                        // 0x60083
                        result = 1;
                        if ((int32_t)tls_group_allowed() != 0) {
                            // break -> 0x600aa
                            break;
                        }
                    }
                    // 0x60050
                    v8 = v9 + 1;
                    v9 = v8;
                    result = 0;
                }
            }
        }
    }
    // 0x600aa
    if (v3 != __readfsqword(40)) {
        // 0x600c9
        return function_20ff0();
    }
    // 0x600ba
    return result;
}