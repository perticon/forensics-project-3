undefined8 ssl3_init_finished_mac(long param_1)

{
  BIO_METHOD *type;
  BIO *bp;
  
  type = BIO_s_mem();
  bp = BIO_new(type);
  if (bp != (BIO *)0x0) {
    ssl3_free_digest_list(param_1);
    *(BIO **)(param_1 + 0x188) = bp;
    BIO_ctrl(bp,9,1,(void *)0x0);
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/s3_enc.c",0x140,"ssl3_init_finished_mac");
  ossl_statem_fatal(param_1,0x50,0xc0100,0);
  return 0;
}