SSL_CIPHER * SSL_get_current_cipher(SSL *s)

{
  SSL_CIPHER *pSVar1;
  
  pSVar1 = *(SSL_CIPHER **)&s[3].sid_ctx_length;
  if (pSVar1 != (SSL_CIPHER *)0x0) {
    pSVar1 = (SSL_CIPHER *)pSVar1[8].algorithm_ssl;
  }
  return pSVar1;
}