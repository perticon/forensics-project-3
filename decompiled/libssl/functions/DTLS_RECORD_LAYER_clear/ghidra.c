void DTLS_RECORD_LAYER_clear(long *param_1)

{
  undefined8 *puVar1;
  void *pvVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  pitem *ppVar6;
  ulong uVar7;
  void *pvVar8;
  undefined8 *puVar9;
  byte bVar10;
  
  bVar10 = 0;
  puVar1 = (undefined8 *)param_1[0x217];
  while (ppVar6 = pqueue_pop((pqueue)puVar1[6]), ppVar6 != (pitem *)0x0) {
    CRYPTO_free(*(void **)((long)ppVar6->data + 0x10));
    CRYPTO_free(ppVar6->data);
    pitem_free(ppVar6);
  }
  while (ppVar6 = pqueue_pop((pqueue)puVar1[8]), ppVar6 != (pitem *)0x0) {
    pvVar2 = ppVar6->data;
    pvVar8 = *(void **)((long)pvVar2 + 0x10);
    if ((*(byte *)(*param_1 + 0x9e8) & 2) != 0) {
      OPENSSL_cleanse(pvVar8,*(size_t *)((long)pvVar2 + 0x20));
      pvVar8 = *(void **)((long)pvVar2 + 0x10);
    }
    CRYPTO_free(pvVar8);
    CRYPTO_free(ppVar6->data);
    pitem_free(ppVar6);
  }
  while (ppVar6 = pqueue_pop((pqueue)puVar1[10]), ppVar6 != (pitem *)0x0) {
    pvVar2 = ppVar6->data;
    pvVar8 = *(void **)((long)pvVar2 + 0x10);
    if ((*(byte *)(*param_1 + 0x9e8) & 2) != 0) {
      OPENSSL_cleanse(pvVar8,*(size_t *)((long)pvVar2 + 0x20));
      pvVar8 = *(void **)((long)pvVar2 + 0x10);
    }
    CRYPTO_free(pvVar8);
    CRYPTO_free(ppVar6->data);
    pitem_free(ppVar6);
  }
  uVar3 = puVar1[6];
  uVar4 = puVar1[8];
  *puVar1 = 0;
  uVar5 = puVar1[10];
  puVar1[0xc] = 0;
  puVar9 = (undefined8 *)((ulong)(puVar1 + 1) & 0xfffffffffffffff8);
  for (uVar7 = (ulong)(((int)puVar1 - (int)(undefined8 *)((ulong)(puVar1 + 1) & 0xfffffffffffffff8))
                       + 0x68U >> 3); uVar7 != 0; uVar7 = uVar7 - 1) {
    *puVar9 = 0;
    puVar9 = puVar9 + (ulong)bVar10 * -2 + 1;
  }
  puVar1[6] = uVar3;
  puVar1[8] = uVar4;
  puVar1[10] = uVar5;
  return;
}