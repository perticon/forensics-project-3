static int cmd_RecordPadding(SSL_CONF_CTX *cctx, const char *value)
{
    int rv = 0;
    int block_size = atoi(value);

    /*
     * All we care about is a non-negative value,
     * the setters check the range
     */
    if (block_size >= 0) {
        if (cctx->ctx)
            rv = SSL_CTX_set_block_padding(cctx->ctx, block_size);
        if (cctx->ssl)
            rv = SSL_set_block_padding(cctx->ssl, block_size);
    }
    return rv;
}