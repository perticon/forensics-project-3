undefined8 cmd_RecordPadding(long param_1,char *param_2)

{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  
  lVar2 = strtol(param_2,(char **)0x0,10);
  iVar1 = (int)lVar2;
  if (iVar1 < 0) {
    uVar3 = 0;
  }
  else {
    uVar3 = 0;
    if (*(long *)(param_1 + 0x18) != 0) {
      uVar3 = SSL_CTX_set_block_padding(*(long *)(param_1 + 0x18),(long)iVar1);
    }
    if (*(long *)(param_1 + 0x20) != 0) {
      uVar3 = SSL_set_block_padding(*(long *)(param_1 + 0x20),(long)iVar1);
      return uVar3;
    }
  }
  return uVar3;
}