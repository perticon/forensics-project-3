ulong tls1_check_ec_tmp_key(long param_1,long param_2)

{
  short sVar1;
  ulong uVar2;
  
  if ((*(uint *)(*(long *)(param_1 + 0x898) + 0x1c) & 0x30000) == 0) {
    sVar1 = tls1_shared_group(param_1,0);
    return (ulong)(sVar1 != 0);
  }
  if (param_2 != 0x300c02b) {
    if (param_2 != 0x300c02c) {
      return 0;
    }
    uVar2 = tls1_check_group_id(param_1,0x18,1);
    return uVar2;
  }
  uVar2 = tls1_check_group_id(param_1,0x17,1);
  return uVar2;
}