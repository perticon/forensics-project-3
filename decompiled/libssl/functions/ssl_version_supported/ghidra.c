bool ssl_version_supported(int *param_1,int param_2,undefined8 *param_3)

{
  long lVar1;
  int iVar2;
  undefined4 uVar3;
  int *piVar4;
  undefined8 uVar5;
  uint uVar6;
  int iVar7;
  int iVar8;
  int iVar9;
  undefined1 *puVar10;
  
  iVar9 = **(int **)(param_1 + 2);
  uVar6 = *(uint *)(*(long *)(*(int **)(param_1 + 2) + 0x30) + 0x60);
  if (iVar9 == 0x10000) {
    puVar10 = tls_version_table;
    iVar9 = 0x304;
  }
  else {
    if (iVar9 != 0x1ffff) {
      return *param_1 == param_2;
    }
    puVar10 = dtls_version_table;
    iVar9 = 0xfefd;
  }
  iVar8 = 0xff00;
  if (param_2 != 0x100) {
    iVar8 = param_2;
  }
  do {
    if (param_2 == iVar9) {
      if (*(code **)(puVar10 + 8) != (code *)0x0) {
        piVar4 = (int *)(**(code **)(puVar10 + 8))();
        iVar9 = *piVar4;
        iVar2 = param_1[0x27d];
        if ((iVar2 == 0) || (iVar9 == iVar2)) {
LAB_001763d8:
          iVar2 = ssl_security(param_1,9,0,iVar9,0);
          if (iVar2 != 0) {
            iVar2 = param_1[0x27e];
            if ((iVar2 == 0) || (iVar9 == iVar2)) {
LAB_00176440:
              if (((*(ulong *)(piVar4 + 2) & *(ulong *)(param_1 + 0x27a)) == 0) &&
                 (((*(byte *)(piVar4 + 1) & 2) == 0 ||
                  ((*(uint *)(*(long *)(param_1 + 0x226) + 0x1c) & 0x30000) == 0)))) {
                if ((param_1[0xe] == 0) || (param_2 != 0x304)) {
LAB_001764a9:
                  if (param_3 != (undefined8 *)0x0) {
                    uVar5 = (**(code **)(puVar10 + 8))();
                    *param_3 = uVar5;
                    return true;
                  }
                  return true;
                }
                if ((*(long *)(param_1 + 0x26a) != 0) && (*(long *)(param_1 + 0x2e2) != 0)) {
                  if (((*(long *)(*(long *)(param_1 + 0x26a) + 0x218) == 0) &&
                      (((*(long *)(*(long *)(param_1 + 0x2e2) + 0x218) == 0 &&
                        (*(long *)(param_1 + 0x264) == 0)) && (*(long *)(param_1 + 0x266) == 0))))
                     && (*(long *)(*(long *)(param_1 + 0x226) + 0x1b8) == 0)) {
                    iVar9 = 0;
                    do {
                      if (iVar9 - 4U < 3) {
LAB_00176536:
                        if (iVar9 == 8) goto LAB_00176342;
                      }
                      else {
                        lVar1 = *(long *)(param_1 + 0x226) + (long)iVar9 * 0x28;
                        if ((*(long *)(lVar1 + 0x20) == 0) || (*(long *)(lVar1 + 0x28) == 0))
                        goto LAB_00176536;
                        if (iVar9 != 3) break;
                        uVar3 = ssl_get_EC_curve_nid
                                          (*(undefined8 *)(*(long *)(param_1 + 0x226) + 0xa0));
                        iVar2 = tls_check_sigalg_curve(param_1,uVar3);
                        if (iVar2 != 0) break;
                      }
                      do {
                        iVar9 = iVar9 + 1;
                      } while (iVar9 == 2);
                    } while( true );
                  }
                  goto LAB_001764a9;
                }
              }
            }
            else if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
              if (iVar9 < iVar2) goto LAB_00176440;
            }
            else {
              if (iVar9 == 0x100) {
                iVar9 = 0xff00;
                if (iVar2 == 0x100) goto LAB_00176342;
              }
              else if (iVar2 == 0x100) {
                iVar2 = 0xff00;
              }
              if (iVar2 < iVar9) goto LAB_00176440;
            }
          }
        }
        else if ((*(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60) & 8) == 0) {
          if (iVar2 <= iVar9) goto LAB_001763d8;
        }
        else {
          if (iVar9 == 0x100) {
            iVar7 = 0xff00;
            if (iVar2 == 0x100) goto LAB_001763d8;
          }
          else {
            iVar7 = iVar9;
            if (iVar2 == 0x100) {
              iVar2 = 0xff00;
            }
          }
          if (iVar7 <= iVar2) goto LAB_001763d8;
        }
      }
    }
    else if ((uVar6 & 8) == 0) {
      if (iVar9 <= param_2) {
        return false;
      }
    }
    else {
      if (param_2 == 0x100) {
        if (iVar9 == 0x100) {
          return false;
        }
        iVar2 = 0xff00;
      }
      else {
        iVar2 = iVar8;
        if (iVar9 == 0x100) {
          iVar9 = 0xff00;
          iVar2 = param_2;
        }
      }
      if (iVar2 <= iVar9) {
        return false;
      }
    }
LAB_00176342:
    iVar9 = *(int *)(puVar10 + 0x18);
    puVar10 = puVar10 + 0x18;
    if (iVar9 == 0) {
      return false;
    }
    uVar6 = *(uint *)(*(long *)(*(long *)(param_1 + 2) + 0xc0) + 0x60);
  } while( true );
}