undefined8 tls1_process_sigalgs(long param_1)

{
  int *piVar1;
  byte bVar2;
  int iVar3;
  long lVar4;
  undefined8 uVar5;
  void *pvVar6;
  ulong uVar7;
  uint uVar8;
  long lVar9;
  ulong uVar10;
  long lVar11;
  long in_FS_OFFSET;
  long local_48;
  long local_40;
  
  lVar9 = *(long *)(param_1 + 0x898);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar8 = *(uint *)(lVar9 + 0x1c);
  CRYPTO_free(*(void **)(param_1 + 0x1da8));
  *(undefined8 *)(param_1 + 0x1da8) = 0;
  *(undefined8 *)(param_1 + 0x1db0) = 0;
  uVar8 = uVar8 & 0x30000;
  if ((*(int *)(param_1 + 0x38) == 0) && (*(long *)(lVar9 + 0x1a8) != 0)) {
    if (uVar8 == 0) {
      uVar5 = *(undefined8 *)(lVar9 + 0x1b0);
      bVar2 = *(byte *)(param_1 + 0x9ea);
      local_48 = *(long *)(lVar9 + 0x1a8);
joined_r0x00149d9b:
      if ((bVar2 & 0x40) == 0) goto LAB_00149d9d;
LAB_00149d5b:
      lVar9 = *(long *)(param_1 + 0x390);
      lVar11 = local_48;
    }
    else {
      uVar5 = tls12_get_psigalgs(param_1,0,&local_48);
      lVar9 = *(long *)(param_1 + 0x390);
      lVar11 = local_48;
    }
  }
  else {
    if ((*(long *)(lVar9 + 0x198) != 0) && (uVar8 == 0)) {
      uVar5 = *(undefined8 *)(lVar9 + 0x1a0);
      bVar2 = *(byte *)(param_1 + 0x9ea);
      local_48 = *(long *)(lVar9 + 0x198);
      goto joined_r0x00149d9b;
    }
    uVar5 = tls12_get_psigalgs(param_1,0,&local_48);
    if ((*(byte *)(param_1 + 0x9ea) & 0x40) != 0) goto LAB_00149d5b;
    if (uVar8 != 0) {
      lVar9 = *(long *)(param_1 + 0x390);
      lVar11 = local_48;
      goto LAB_00149db1;
    }
LAB_00149d9d:
    uVar5 = *(undefined8 *)(param_1 + 0x3a0);
    lVar9 = local_48;
    lVar11 = *(long *)(param_1 + 0x390);
  }
LAB_00149db1:
  lVar4 = tls12_shared_sigalgs(param_1,0,lVar11,uVar5,lVar9);
  if (lVar4 == 0) {
    *(undefined8 *)(param_1 + 0x1da8) = 0;
    *(undefined8 *)(param_1 + 0x1db0) = 0;
    *(undefined (*) [16])(param_1 + 0x3b8) = (undefined  [16])0x0;
    *(undefined4 *)(param_1 + 0x3d8) = 0;
    *(undefined (*) [16])(param_1 + 0x3c8) = (undefined  [16])0x0;
  }
  else {
    pvVar6 = CRYPTO_malloc((int)lVar4 * 8,"ssl/t1_lib.c",0x8f5);
    if (pvVar6 == (void *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/t1_lib.c",0x8f6,"tls1_set_shared_sigalgs");
      ERR_set_error(0x14,0xc0100,0);
      uVar5 = 0;
      goto LAB_00149e0c;
    }
    uVar7 = tls12_shared_sigalgs(param_1,pvVar6,lVar11,uVar5,lVar9);
    *(void **)(param_1 + 0x1da8) = pvVar6;
    *(ulong *)(param_1 + 0x1db0) = uVar7;
    *(undefined (*) [16])(param_1 + 0x3b8) = (undefined  [16])0x0;
    *(undefined4 *)(param_1 + 0x3d8) = 0;
    *(undefined (*) [16])(param_1 + 0x3c8) = (undefined  [16])0x0;
    if (uVar7 != 0) {
      uVar10 = 0;
      while( true ) {
        lVar9 = *(long *)((long)pvVar6 + uVar10 * 8);
        if ((((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) ||
              (iVar3 = **(int **)(param_1 + 8), iVar3 < 0x304)) || (iVar3 == 0x10000)) ||
            (*(int *)(lVar9 + 0x14) != 6)) &&
           (piVar1 = (int *)(param_1 + 0x3b8 + (long)*(int *)(lVar9 + 0x18) * 4), *piVar1 == 0)) {
          iVar3 = ssl_cert_is_disabled(*(undefined8 *)(param_1 + 0x9a8));
          if (iVar3 == 0) {
            *piVar1 = 0x102;
          }
          uVar7 = *(ulong *)(param_1 + 0x1db0);
        }
        uVar10 = uVar10 + 1;
        if (uVar7 <= uVar10) break;
        pvVar6 = *(void **)(param_1 + 0x1da8);
      }
    }
  }
  uVar5 = 1;
LAB_00149e0c:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}