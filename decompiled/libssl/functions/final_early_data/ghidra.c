uint final_early_data(long param_1,int param_2,int param_3)

{
  int iVar1;
  uint uVar2;
  
  uVar2 = 1;
  if (param_3 != 0) {
    if (*(int *)(param_1 + 0x38) == 0) {
      if (param_2 == 0x400) {
        uVar2 = *(uint *)(param_1 + 0xb34);
        if (uVar2 == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/extensions.c",0x665,"final_early_data");
          ossl_statem_fatal(param_1,0x2f,0xe9,0);
        }
        else {
          uVar2 = 1;
        }
      }
    }
    else {
      if ((((*(int *)(param_1 + 0x1d40) == 0) || (*(int *)(param_1 + 0x4d0) == 0)) ||
          (*(int *)(param_1 + 0x84) != 9)) ||
         ((*(int *)(param_1 + 0xb34) == 0 || (*(int *)(param_1 + 0x8e8) != 0)))) {
LAB_0015e695:
        *(undefined4 *)(param_1 + 0xb30) = 1;
        return 1;
      }
      if (*(code **)(param_1 + 0x1d88) != (code *)0x0) {
        iVar1 = (**(code **)(param_1 + 0x1d88))(param_1,*(undefined8 *)(param_1 + 0x1d90));
        if (iVar1 == 0) goto LAB_0015e695;
      }
      *(undefined4 *)(param_1 + 0xb30) = 2;
      iVar1 = tls13_change_cipher_state(param_1,0x61);
      uVar2 = (uint)(iVar1 != 0);
    }
  }
  return uVar2;
}