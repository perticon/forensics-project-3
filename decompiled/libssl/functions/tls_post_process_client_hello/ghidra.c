undefined8 tls_post_process_client_hello(SSL *param_1,int param_2)

{
  _func_3076 *p_Var1;
  code *pcVar2;
  int *piVar3;
  stack_st_void *psVar4;
  KSSL_CTX *pKVar5;
  _func_3153 *p_Var6;
  _func_3154 *p_Var7;
  uint uVar8;
  int iVar9;
  undefined4 uVar10;
  int iVar11;
  int iVar12;
  undefined8 uVar13;
  SSL_CTX *pSVar14;
  uchar *puVar15;
  stack_st_SSL_CIPHER *psVar16;
  _func_3149 *p_Var17;
  ulong uVar18;
  SSL_CIPHER *c;
  long lVar19;
  SSL_METHOD *pSVar20;
  uint *puVar21;
  int *piVar22;
  size_t len;
  X509_EXTENSIONS *__s2;
  int *piVar23;
  ulong uVar24;
  long in_FS_OFFSET;
  bool bVar25;
  undefined4 local_64;
  undefined4 local_60;
  int local_5c;
  void *local_58;
  long local_50;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_2 == 3) {
    lVar19 = *(long *)&param_1[3].ex_data.dummy;
    piVar3 = (int *)param_1[4].msg_callback;
    local_64 = 0x50;
    local_58 = (void *)0x0;
    pcVar2 = *(code **)(lVar19 + 0x208);
    local_60 = 0;
    local_50 = 0;
    if (pcVar2 != (code *)0x0) {
      iVar9 = (*pcVar2)(param_1,&local_64,*(undefined8 *)(lVar19 + 0x210));
      if (iVar9 == -1) {
        param_1->rwstate = 7;
        uVar13 = 3;
        goto LAB_0017cde5;
      }
      if (iVar9 != 1) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0x64f,"tls_early_post_process_client_hello");
        ossl_statem_fatal(param_1,local_64,0xea,0);
        goto LAB_0017d402;
      }
    }
    pKVar5 = *(KSSL_CTX **)(piVar3 + 2);
    p_Var6 = *(_func_3153 **)(piVar3 + 4);
    param_1->kssl_ctx = pKVar5;
    param_1->psk_client_callback = p_Var6;
    p_Var7 = *(_func_3154 **)(piVar3 + 6);
    pSVar14 = *(SSL_CTX **)(piVar3 + 8);
    param_1->psk_server_callback = p_Var7;
    param_1->ctx = pSVar14;
    if (*piVar3 != 0) {
      uVar8 = piVar3[1];
      if ((uVar8 == 2) || ((uVar8 & 0xff00) != 0x300)) {
        ERR_new(pKVar5,p_Var7);
        ERR_set_debug("ssl/statem/statem_srvr.c",0x661,"tls_early_post_process_client_hello");
        ossl_statem_fatal(param_1,0x46,0xfc,0);
        goto LAB_0017d402;
      }
      *(uint *)&param_1[3].field_0x1fc = uVar8;
    }
    if (((byte)param_1->method->get_timeout[0x60] & 8) == 0) {
      iVar9 = ssl_choose_server_version(pKVar5,p_Var7,param_1,piVar3,&local_60);
      if (iVar9 == 0) {
        if (((byte)param_1->method->get_timeout[0x60] & 8) != 0) goto LAB_0017d572;
        iVar9 = param_1->method->version;
        if ((iVar9 != 0x10000) && (0x303 < iVar9)) {
          iVar9 = RECORD_LAYER_processed_read_pending(&param_1[4].ex_data.dummy);
          if (iVar9 != 0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0x67f,"tls_early_post_process_client_hello");
            ossl_statem_fatal(param_1,10,0xb6,0);
            goto LAB_0017d402;
          }
          if (((byte)param_1->method->get_timeout[0x60] & 8) != 0) goto LAB_0017d572;
        }
        goto LAB_0017d5ce;
      }
LAB_0017d88e:
      if ((param_1->tlsext_opaque_prf_input == (void *)0x0) || (param_1[1].wbio == (BIO *)0x0)) {
        iVar12 = piVar3[1];
        *(int *)&param_1[3].field_0x1fc = iVar12;
        param_1->version = iVar12;
      }
      ERR_new();
      uVar13 = 0x679;
LAB_0017d8bf:
      ERR_set_debug("ssl/statem/statem_srvr.c",uVar13,"tls_early_post_process_client_hello");
      ossl_statem_fatal(param_1,0x46,iVar9,0);
    }
    else {
      if (param_1->method->version != 0x1ffff) {
        iVar9 = piVar3[1];
        iVar12 = param_1->version;
        if (iVar9 == 0x100) {
          iVar9 = 0xff00;
          if (iVar12 == 0x100) goto LAB_0017d572;
        }
        else if (iVar12 == 0x100) {
          iVar12 = 0xff00;
        }
        if (iVar12 < iVar9) {
          iVar9 = 0x18c;
          goto LAB_0017d88e;
        }
      }
LAB_0017d572:
      uVar18 = SSL_get_options(param_1);
      if ((uVar18 & 0x2000) == 0) {
LAB_0017d5be:
        if ((param_1->method->version == 0x1ffff) &&
           (iVar9 = ssl_choose_server_version(param_1,piVar3,&local_60), iVar9 != 0)) {
          param_1->version = *(int *)&param_1[3].field_0x1fc;
          ERR_new();
          uVar13 = 0x69b;
          goto LAB_0017d8bf;
        }
LAB_0017d5ce:
        *(undefined4 *)&param_1[1].tlsext_ecpointformatlist_length = 0;
        iVar9 = ssl_cache_cipherlist(param_1,piVar3 + 0x56,*piVar3);
        if ((iVar9 != 0) &&
           (iVar9 = bytes_to_cipher_list(param_1,piVar3 + 0x56,&local_58), iVar9 != 0)) {
          iVar9 = 0;
          param_1[1].max_send_fragment = 0;
          if (local_50 != 0) {
            for (; iVar12 = OPENSSL_sk_num(local_50), iVar9 < iVar12; iVar9 = iVar9 + 1) {
              c = (SSL_CIPHER *)OPENSSL_sk_value(local_50,iVar9);
              uVar18 = SSL_CIPHER_get_id(c);
              if ((int)uVar18 == 0x30000ff) {
                if (*(int *)&param_1[4].expand != 0) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0x6b3,
                                "tls_early_post_process_client_hello");
                  ossl_statem_fatal(param_1,0x28,0x159,0);
                  goto LAB_0017d402;
                }
                param_1[1].max_send_fragment = 1;
              }
              else {
                uVar18 = SSL_CIPHER_get_id(c);
                if (((int)uVar18 == 0x3005600) &&
                   (iVar12 = ssl_check_version_downgrade(param_1), iVar12 == 0)) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0x6c1,
                                "tls_early_post_process_client_hello");
                  ossl_statem_fatal(param_1,0x56,0x175,0);
                  goto LAB_0017d402;
                }
              }
            }
          }
          if (((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
              (iVar9 = param_1->method->version, iVar9 != 0x10000)) && (0x303 < iVar9)) {
            psVar16 = SSL_get_ciphers(param_1);
            p_Var17 = (_func_3149 *)ssl3_choose_cipher(param_1,local_58,psVar16);
            if (p_Var17 == (_func_3149 *)0x0) {
              ERR_new();
              uVar13 = 0x6ce;
LAB_0017deaf:
              ERR_set_debug("ssl/statem/statem_srvr.c",uVar13,"tls_early_post_process_client_hello")
              ;
              ossl_statem_fatal(param_1,0x28,0xc1,0);
            }
            else {
              if ((*(int *)&param_1[3].read_hash != 1) ||
                 ((param_1[1].handshake_func != (_func_3149 *)0x0 &&
                  (*(int *)(param_1[1].handshake_func + 0x18) == *(int *)(p_Var17 + 0x18))))) {
                param_1[1].handshake_func = p_Var17;
                goto LAB_0017ce35;
              }
              ERR_new();
              ERR_set_debug("ssl/statem/statem_srvr.c",0x6d8,"tls_early_post_process_client_hello");
              ossl_statem_fatal(param_1,0x2f,0xba,0);
            }
          }
          else {
LAB_0017ce35:
            iVar9 = tls_parse_extension(param_1,0xd,0x80,*(undefined8 *)(piVar3 + 0xa2));
            if (iVar9 != 0) {
              if ((*piVar3 == 0) &&
                 ((param_1->new_session == 0 ||
                  ((*(byte *)((long)&param_1[3].tlsext_debug_cb + 2) & 1) == 0)))) {
                iVar9 = ssl_get_prev_session(param_1,piVar3);
                if (iVar9 != 1) {
                  if (iVar9 == -1) goto LAB_0017d402;
                  goto LAB_0017ce93;
                }
                *(undefined4 *)&param_1[1].tlsext_ecpointformatlist_length = 1;
                pSVar20 = param_1->method;
                if (((byte)pSVar20->get_timeout[0x60] & 8) == 0) {
LAB_0017ceba:
                  iVar9 = pSVar20->version;
                  if (iVar9 == 0x10000) {
LAB_0017db7f:
                    if ((iVar9 < 0x304) || (iVar9 == 0x10000)) goto LAB_0017ced4;
                  }
                  else {
                    if (0x303 < iVar9) {
                      memcpy(&param_1[3].session,param_1[4].msg_callback + 0x30,
                             *(size_t *)(param_1[4].msg_callback + 0x28));
                      param_1[3].info_callback = *(_func_3152 **)(param_1[4].msg_callback + 0x28);
                      if (((byte)pSVar20->get_timeout[0x60] & 8) == 0) {
                        iVar9 = pSVar20->version;
                        goto LAB_0017db7f;
                      }
                    }
LAB_0017ced4:
                    if (*(int *)&param_1[1].tlsext_ecpointformatlist_length != 0) goto LAB_0017da87;
                  }
LAB_0017cee2:
                  if (*(long *)(piVar3 + 0x5a) != 0) {
                    piVar23 = piVar3 + 0x5c;
                    piVar22 = piVar23;
LAB_0017cf1d:
                    if (*(byte *)piVar22 != 0) goto LAB_0017cf10;
                    if (((*(byte *)&param_1[3].tlsext_debug_cb & 0x40) != 0) &&
                       (5 < *(ulong *)(piVar3 + 0x9e))) {
                      lVar19 = *(long *)(piVar3 + 0x9c);
                      uVar24 = *(ulong *)(piVar3 + 0x9e) - 6;
                      uVar18 = (ulong)(ushort)(*(ushort *)(lVar19 + 4) << 8 |
                                              *(ushort *)(lVar19 + 4) >> 8);
                      if ((uVar18 <= uVar24) && (*(short *)(lVar19 + 2) == 0)) {
                        iVar9 = SSL_client_version(param_1);
                        if ((iVar9 >> 8 == 3) &&
                           (iVar9 = SSL_client_version(param_1), 0x302 < iVar9)) {
                          len = 0x22;
                        }
                        else {
                          len = 0x12;
                        }
                        bVar25 = false;
                        if (uVar24 - uVar18 == len) {
                          iVar9 = CRYPTO_memcmp((void *)(lVar19 + 6 + uVar18),
                                                kSafariExtensionsBlock_27256,len);
                          bVar25 = iVar9 == 0;
                        }
                        param_1[1].field_0x1fc = bVar25;
                      }
                    }
                    iVar9 = tls_parse_all_extensions
                                      (param_1,0x80,*(undefined8 *)(piVar3 + 0xa2),0,0,1);
                    if (iVar9 == 0) goto LAB_0017d402;
                    iVar9 = ssl_fill_hello_random(param_1,1,&param_1->verify_mode,0x20,local_60);
                    if (iVar9 < 1) {
                      ERR_new();
                      uVar13 = 0x756;
                      goto LAB_0017de70;
                    }
                    if (*(int *)&param_1[1].tlsext_ecpointformatlist_length == 0) {
                      pSVar20 = param_1->method;
                      if ((((param_1->version < 0x301) ||
                           (((byte)pSVar20->get_timeout[0x60] & 8) != 0)) ||
                          ((0x303 < pSVar20->version && (pSVar20->version != 0x10000)))) ||
                         (*(code **)&param_1[4].server == (code *)0x0)) goto LAB_0017d0ab;
                      local_48 = 0;
                      local_5c = 0x200;
                      iVar9 = (**(code **)&param_1[4].server)
                                        (param_1,*(long *)&param_1[3].sid_ctx_length + 0x50,
                                         &local_5c,local_58,&local_48,
                                         *(undefined8 *)&param_1[4].quiet_shutdown);
                      if ((iVar9 != 0) && (0 < local_5c)) {
                        lVar19 = *(long *)&param_1[3].sid_ctx_length;
                        *(long *)(lVar19 + 8) = (long)local_5c;
                        *(undefined4 *)&param_1[1].tlsext_ecpointformatlist_length = 1;
                        param_1[1].tls_session_secret_cb_arg = local_58;
                        *(undefined8 *)(lVar19 + 0x2c8) = 0;
                        local_58 = (void *)0x0;
                        if (local_48 == 0) {
                          psVar16 = SSL_get_ciphers(param_1);
                          local_48 = ssl3_choose_cipher(param_1,param_1[1].tls_session_secret_cb_arg
                                                        ,psVar16);
                          if (local_48 == 0) {
                            ERR_new();
                            uVar13 = 0x779;
                            goto LAB_0017deaf;
                          }
                          lVar19 = *(long *)&param_1[3].sid_ctx_length;
                        }
                        *(ulong *)(lVar19 + 0x2f8) = local_48;
                        OPENSSL_sk_free(param_1[1].initial_ctx);
                        pSVar14 = (SSL_CTX *)OPENSSL_sk_dup(param_1[1].tls_session_secret_cb_arg);
                        param_1[1].initial_ctx = pSVar14;
                        OPENSSL_sk_free(param_1[1].next_proto_negotiated);
                        puVar15 = (uchar *)OPENSSL_sk_dup(param_1[1].tls_session_secret_cb_arg);
                        param_1[1].next_proto_negotiated = puVar15;
                      }
                    }
                    pSVar20 = param_1->method;
LAB_0017d0ab:
                    p_Var1 = pSVar20->get_timeout;
                    *(undefined8 *)&param_1[1].read_ahead = 0;
                    if (((((byte)p_Var1[0x60] & 8) == 0) && (pSVar20->version != 0x10000)) &&
                       (0x303 < pSVar20->version)) {
                      puVar21 = (uint *)0x0;
                      if (*(long *)(piVar3 + 0x5a) == 1) goto LAB_0017d0f2;
                      ERR_new();
                      ERR_set_debug("ssl/statem/statem_srvr.c",0x792,
                                    "tls_early_post_process_client_hello");
                      ossl_statem_fatal(param_1,0x2f,0x155,0);
                      goto LAB_0017d402;
                    }
                    uVar8 = *(uint *)(*(long *)&param_1[3].sid_ctx_length + 0x2f4);
                    if (uVar8 == 0) {
                      iVar9 = *(int *)&param_1[1].tlsext_ecpointformatlist_length;
                      puVar21 = (uint *)0x0;
                      if (iVar9 == 0) {
                        iVar12 = ssl_allow_compression(param_1);
                        if ((iVar12 != 0) &&
                           (*(long *)(*(long *)&param_1[3].ex_data.dummy + 0x118) != 0)) {
                          iVar12 = OPENSSL_sk_num();
                          for (; iVar9 < iVar12; iVar9 = iVar9 + 1) {
                            puVar21 = (uint *)OPENSSL_sk_value(*(undefined8 *)
                                                                (*(long *)&param_1[3].ex_data.dummy
                                                                + 0x118));
                            if (*(long *)(piVar3 + 0x5a) != 0) {
                              piVar22 = piVar23;
                              do {
                                if (*puVar21 == (uint)*(byte *)piVar22) {
                                  *(uint **)&param_1[1].read_ahead = puVar21;
                                  goto LAB_0017ddbf;
                                }
                                piVar22 = (int *)((long)piVar22 + 1);
                              } while ((int *)((long)piVar3 + *(long *)(piVar3 + 0x5a) + 0x170) !=
                                       piVar22);
                            }
                          }
                        }
                        puVar21 = (uint *)0x0;
                        goto LAB_0017ddbf;
                      }
                      goto LAB_0017dbfc;
                    }
                    iVar9 = ssl_allow_compression(param_1);
                    if (iVar9 == 0) {
                      ERR_new();
                      ERR_set_debug("ssl/statem/statem_srvr.c",0x79f,
                                    "tls_early_post_process_client_hello");
                      ossl_statem_fatal(param_1,0x28,0x154,0);
                      goto LAB_0017d402;
                    }
                    iVar9 = 0;
                    puVar21 = (uint *)0x0;
                    goto LAB_0017dd66;
                  }
LAB_0017dc9b:
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0x73e,
                                "tls_early_post_process_client_hello");
                  ossl_statem_fatal(param_1,0x32,0xbb,0);
                }
                else {
LAB_0017da87:
                  iVar9 = *(int *)(*(long *)(*(long *)&param_1[3].sid_ctx_length + 0x2f8) + 0x18);
                  for (iVar12 = 0; iVar11 = OPENSSL_sk_num(local_58), iVar12 < iVar11;
                      iVar12 = iVar12 + 1) {
                    lVar19 = OPENSSL_sk_value(local_58,iVar12);
                    if (iVar9 == *(int *)(lVar19 + 0x18)) goto LAB_0017cee2;
                  }
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0x72f,
                                "tls_early_post_process_client_hello");
                  ossl_statem_fatal(param_1,0x2f,0xd7,0);
                }
              }
              else {
LAB_0017ce93:
                iVar9 = ssl_get_new_session(param_1,1);
                if (iVar9 != 0) {
                  pSVar20 = param_1->method;
                  if (((byte)pSVar20->get_timeout[0x60] & 8) == 0) goto LAB_0017ceba;
                  goto LAB_0017ced4;
                }
              }
            }
          }
        }
      }
      else {
        pcVar2 = *(code **)(*(long *)&param_1[3].ex_data.dummy + 0xd8);
        if (pcVar2 == (code *)0x0) {
          __s2 = param_1[1].tlsext_ocsp_exts;
          if ((*(size_t *)&__s2[8].stack == *(size_t *)(piVar3 + 0x14)) &&
             (iVar9 = memcmp(piVar3 + 0x16,__s2,*(size_t *)&__s2[8].stack), iVar9 == 0))
          goto LAB_0017d5b2;
          ERR_new();
          uVar13 = 0x692;
        }
        else {
          iVar9 = (*pcVar2)(param_1,piVar3 + 0x16);
          if (iVar9 != 0) {
            __s2 = param_1[1].tlsext_ocsp_exts;
LAB_0017d5b2:
            *(undefined4 *)&__s2[8].stack.data = 1;
            goto LAB_0017d5be;
          }
          ERR_new();
          uVar13 = 0x689;
        }
        ERR_set_debug("ssl/statem/statem_srvr.c",uVar13,"tls_early_post_process_client_hello");
        ossl_statem_fatal(param_1,0x28,0x134,0);
      }
    }
    goto LAB_0017d402;
  }
  if (param_2 == 4) goto LAB_0017d1a0;
  if (param_2 == 5) {
LAB_0017d2d9:
    local_48 = CONCAT44(local_48._4_4_,0x70);
    if ((((byte)param_1[1].handshake_func[0x1c] & 0x20) != 0) &&
       (*(long *)(param_1[4].sid_ctx + 0xc) != 0)) {
      if (param_1[4].session == (SSL_SESSION *)0x0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0x4e9,"ssl_check_srp_ext_ClientHello");
        ossl_statem_fatal(param_1,0x73,0xdf,0);
        goto LAB_0017d71b;
      }
      iVar9 = ssl_srp_server_param_with_username_intern(param_1,&local_48);
      if (iVar9 < 0) {
        param_1->rwstate = 4;
        uVar13 = 5;
        goto LAB_0017cde5;
      }
      if (iVar9 == 2) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0x4f1,"ssl_check_srp_ext_ClientHello");
        ossl_statem_fatal(param_1,local_48 & 0xffffffff,(ulong)((int)local_48 != 0x73) * 3 + 0xdf,0)
        ;
        uVar13 = 0;
        goto LAB_0017cde5;
      }
    }
  }
  uVar13 = 1;
  goto LAB_0017cde5;
LAB_0017cf10:
  piVar22 = (int *)((long)piVar22 + 1);
  if (piVar22 == (int *)((long)piVar3 + *(long *)(piVar3 + 0x5a) + 0x170)) goto LAB_0017dc9b;
  goto LAB_0017cf1d;
LAB_0017dd66:
  iVar12 = OPENSSL_sk_num(*(undefined8 *)(*(long *)&param_1[3].ex_data.dummy + 0x118));
  if (iVar12 <= iVar9) goto code_r0x0017dd7e;
  puVar21 = (uint *)OPENSSL_sk_value(*(undefined8 *)(*(long *)&param_1[3].ex_data.dummy + 0x118),
                                     iVar9);
  if (uVar8 == *puVar21) {
    *(uint **)&param_1[1].read_ahead = puVar21;
    goto LAB_0017dd8c;
  }
  iVar9 = iVar9 + 1;
  goto LAB_0017dd66;
code_r0x0017dd7e:
  if (*(long *)&param_1[1].read_ahead == 0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0x7ac,"tls_early_post_process_client_hello");
    ossl_statem_fatal(param_1,0x28,0x155,0);
  }
  else {
LAB_0017dd8c:
    if (*(long *)(piVar3 + 0x5a) != 0) {
      do {
        if (uVar8 == *(byte *)piVar23) goto LAB_0017ddbf;
        piVar23 = (int *)((long)piVar23 + 1);
      } while (piVar23 != (int *)((long)piVar3 + *(long *)(piVar3 + 0x5a) + 0x170));
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0x7b6,"tls_early_post_process_client_hello");
    ossl_statem_fatal(param_1,0x2f,0x156,0);
  }
  goto LAB_0017d402;
LAB_0017ddbf:
  if (*(int *)&param_1[1].tlsext_ecpointformatlist_length == 0) {
LAB_0017d0f2:
    OPENSSL_sk_free(param_1[1].tls_session_secret_cb_arg);
    param_1[1].tls_session_secret_cb_arg = local_58;
    if (local_58 == (void *)0x0) {
      ERR_new();
      uVar13 = 0x7e6;
LAB_0017de70:
      ERR_set_debug("ssl/statem/statem_srvr.c",uVar13,"tls_early_post_process_client_hello");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
LAB_0017d402:
      OPENSSL_sk_free(local_58);
      OPENSSL_sk_free(local_50);
      CRYPTO_free(*(void **)(piVar3 + 0xa2));
      CRYPTO_free(param_1[4].msg_callback);
      uVar13 = 0;
      param_1[4].msg_callback = (_func_3150 *)0x0;
      goto LAB_0017cde5;
    }
    local_58 = (void *)0x0;
    if (*(int *)&param_1[1].tlsext_ecpointformatlist_length == 0) {
      if (puVar21 == (uint *)0x0) {
        uVar8 = 0;
      }
      else {
        uVar8 = *puVar21;
      }
      *(uint *)(*(long *)&param_1[3].sid_ctx_length + 0x2f4) = uVar8;
      iVar9 = tls1_set_server_sigalgs(param_1);
      if (iVar9 == 0) goto LAB_0017d402;
    }
  }
  else {
    pSVar20 = param_1->method;
LAB_0017dbfc:
    if (((((byte)pSVar20->get_timeout[0x60] & 8) == 0) && (0x303 < pSVar20->version)) &&
       (pSVar20->version != 0x10000)) goto LAB_0017d0f2;
  }
  OPENSSL_sk_free(local_58);
  OPENSSL_sk_free(local_50);
  CRYPTO_free(*(void **)(piVar3 + 0xa2));
  CRYPTO_free(param_1[4].msg_callback);
  param_1[4].msg_callback = (_func_3150 *)0x0;
LAB_0017d1a0:
  if (*(int *)&param_1[1].tlsext_ecpointformatlist_length != 0) {
    if (((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
        (iVar9 = param_1->method->version, iVar9 != 0x10000)) && (0x303 < iVar9)) goto LAB_0017d22d;
    param_1[1].handshake_func = *(_func_3149 **)(*(long *)&param_1[3].sid_ctx_length + 0x2f8);
    goto LAB_0017d23b;
  }
  pcVar2 = *(code **)((param_1[3].d1)->rcvd_cookie + 0xb4);
  if (pcVar2 != (code *)0x0) {
    iVar9 = (*pcVar2)(param_1,*(undefined8 *)((param_1[3].d1)->rcvd_cookie + 0xbc));
    if (iVar9 != 0) {
      if (iVar9 < 0) {
        param_1->rwstate = 4;
        uVar13 = 4;
        goto LAB_0017cde5;
      }
      param_1->rwstate = 1;
      goto LAB_0017d1e2;
    }
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0x89c,"tls_post_process_client_hello");
    uVar13 = 0x179;
LAB_0017d4cd:
    ossl_statem_fatal(param_1,0x50,uVar13,0);
    uVar13 = 0;
    goto LAB_0017cde5;
  }
LAB_0017d1e2:
  if (((((byte)param_1->method->get_timeout[0x60] & 8) != 0) ||
      (iVar9 = param_1->method->version, iVar9 == 0x10000)) || (iVar9 < 0x304)) {
    psVar16 = SSL_get_ciphers(param_1);
    p_Var17 = (_func_3149 *)ssl3_choose_cipher(param_1,param_1[1].tls_session_secret_cb_arg,psVar16)
    ;
    if (p_Var17 == (_func_3149 *)0x0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0x8ac,"tls_post_process_client_hello");
      ossl_statem_fatal(param_1,0x28,0xc1,0);
      uVar13 = 0;
      goto LAB_0017cde5;
    }
    param_1[1].handshake_func = p_Var17;
  }
LAB_0017d22d:
  if (*(int *)&param_1[1].tlsext_ecpointformatlist_length == 0) {
    iVar9 = tls_choose_sigalg(param_1,1);
    if (iVar9 != 0) {
      psVar4 = param_1[4].ex_data.sk;
      lVar19 = *(long *)&param_1[3].sid_ctx_length;
      if (psVar4 != (stack_st_void *)0x0) {
        uVar10 = (*(code *)psVar4)(param_1,((byte)param_1[1].handshake_func[0x1c] & 6) != 0);
        *(undefined4 *)(lVar19 + 0x2b0) = uVar10;
        lVar19 = *(long *)&param_1[3].sid_ctx_length;
      }
      if (*(int *)(lVar19 + 0x2b0) != 0) {
        *(undefined4 *)&param_1[3].next_proto_negotiated_len = 0;
      }
      goto LAB_0017d23b;
    }
  }
  else {
LAB_0017d23b:
    iVar9 = *(int *)&param_1[3].tlsext_session_ticket;
    *(undefined4 *)((long)&param_1[3].tls_session_ticket_ext_cb_arg + 4) = 0;
    if (((iVar9 != -1) && (lVar19 = *(long *)&param_1[3].ex_data.dummy, lVar19 != 0)) &&
       ((pcVar2 = *(code **)(lVar19 + 0x250), pcVar2 != (code *)0x0 &&
        (param_1[1].read_hash != (EVP_MD_CTX *)0x0)))) {
      *(EVP_MD_CTX **)param_1[3].d1 = param_1[1].read_hash;
      iVar9 = (*pcVar2)(param_1,*(undefined8 *)(lVar19 + 600));
      if (iVar9 == 0) {
        if (param_1[3].initial_ctx != (SSL_CTX *)0x0) {
          *(undefined4 *)((long)&param_1[3].tls_session_ticket_ext_cb_arg + 4) = 1;
        }
      }
      else {
        if (iVar9 != 3) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0x82f,"tls_handle_status_request");
          uVar13 = 0xe2;
          goto LAB_0017d4cd;
        }
        *(undefined4 *)((long)&param_1[3].tls_session_ticket_ext_cb_arg + 4) = 0;
      }
    }
    if ((((((byte)param_1->method->get_timeout[0x60] & 8) == 0) &&
         (iVar9 = param_1->method->version, iVar9 != 0x10000)) && (0x303 < iVar9)) ||
       (iVar9 = tls_handle_alpn(param_1), iVar9 != 0)) goto LAB_0017d2d9;
  }
LAB_0017d71b:
  uVar13 = 0;
LAB_0017cde5:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar13;
}