tls_parse_extension(long param_1,uint param_2,undefined4 param_3,long param_4,undefined8 param_5,
                   undefined8 param_6)

{
  long lVar1;
  undefined8 *puVar2;
  int iVar3;
  ulong uVar4;
  code *UNRECOVERED_JUMPTABLE;
  undefined8 uVar5;
  long extraout_RDX;
  long lVar6;
  undefined auVar7 [16];
  
  uVar4 = (ulong)param_2;
  lVar6 = uVar4 * 5;
  puVar2 = (undefined8 *)(param_4 + uVar4 * 0x28);
  if ((*(int *)(puVar2 + 2) != 0) && (*(int *)((long)puVar2 + 0x14) == 0)) {
    *(undefined4 *)((long)puVar2 + 0x14) = 1;
    if (param_2 < 0x1a) {
      lVar1 = uVar4 * 0x38;
      iVar3 = extension_is_relevant(param_1,*(undefined4 *)(ext_defs + lVar1 + 4),param_3);
      lVar6 = extraout_RDX;
      if (iVar3 == 0) goto LAB_0015f478;
      UNRECOVERED_JUMPTABLE = *(code **)(ext_defs + lVar1 + 0x18);
      if (*(int *)(param_1 + 0x38) != 0) {
        UNRECOVERED_JUMPTABLE = *(code **)(ext_defs + lVar1 + 0x10);
      }
      if (UNRECOVERED_JUMPTABLE != (code *)0x0) {
                    /* WARNING: Could not recover jumptable at 0x0015f473. Too many branches */
                    /* WARNING: Treating indirect jump as call */
        auVar7 = (*UNRECOVERED_JUMPTABLE)(param_1,puVar2,param_3,param_5,param_6);
        return auVar7;
      }
    }
    uVar5 = custom_ext_parse(param_1,param_3,*(undefined4 *)(puVar2 + 3),*puVar2,puVar2[1],param_5);
    return CONCAT88(param_6,uVar5);
  }
LAB_0015f478:
  return CONCAT88(lVar6,1);
}