undefined8 ssl_srp_ctx_init_intern(long param_1)

{
  long lVar1;
  int iVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  BIGNUM *pBVar6;
  char *pcVar7;
  ulong uVar8;
  undefined8 uVar9;
  undefined8 *puVar10;
  byte bVar11;
  
  bVar11 = 0;
  if (param_1 == 0) {
    return 0;
  }
  lVar1 = *(long *)(param_1 + 0x9a8);
  if (lVar1 == 0) {
    return 0;
  }
  *(undefined8 *)(param_1 + 0xbd0) = 0;
  iVar2 = (int)param_1 + 0xbd0;
  *(undefined8 *)(param_1 + 0xc48) = 0;
  puVar10 = (undefined8 *)(param_1 + 0xbd8U & 0xfffffffffffffff8);
  uVar8 = (ulong)((iVar2 - (int)puVar10) + 0x80U >> 3);
  for (; uVar8 != 0; uVar8 = uVar8 - 1) {
    *puVar10 = 0;
    puVar10 = puVar10 + 1;
  }
  *(undefined8 *)(param_1 + 0xbd0) = *(undefined8 *)(lVar1 + 0x318);
  uVar3 = *(undefined4 *)(lVar1 + 0x324);
  uVar4 = *(undefined4 *)(lVar1 + 0x328);
  uVar5 = *(undefined4 *)(lVar1 + 0x32c);
  *(undefined4 *)(param_1 + 0xbd8) = *(undefined4 *)(lVar1 + 800);
  *(undefined4 *)(param_1 + 0xbdc) = uVar3;
  *(undefined4 *)(param_1 + 0xbe0) = uVar4;
  *(undefined4 *)(param_1 + 0xbe4) = uVar5;
  pBVar6 = *(BIGNUM **)(lVar1 + 0x340);
  *(undefined8 *)(param_1 + 0xbe8) = *(undefined8 *)(lVar1 + 0x330);
  *(undefined4 *)(param_1 + 0xc40) = *(undefined4 *)(lVar1 + 0x388);
  if (pBVar6 != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(pBVar6);
    *(BIGNUM **)(param_1 + 0xbf8) = pBVar6;
    if (pBVar6 != (BIGNUM *)0x0) goto LAB_0015344b;
LAB_001535b0:
    ERR_new();
    ERR_set_debug("ssl/tls_srp.c",0x7e,"ssl_srp_ctx_init_intern");
    ERR_set_error(0x14,0x80003,0);
    goto LAB_001535e0;
  }
LAB_0015344b:
  if (*(BIGNUM **)(lVar1 + 0x348) != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(*(BIGNUM **)(lVar1 + 0x348));
    *(BIGNUM **)(param_1 + 0xc00) = pBVar6;
    if (pBVar6 == (BIGNUM *)0x0) goto LAB_001535b0;
  }
  if (*(BIGNUM **)(lVar1 + 0x350) != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(*(BIGNUM **)(lVar1 + 0x350));
    *(BIGNUM **)(param_1 + 0xc08) = pBVar6;
    if (pBVar6 == (BIGNUM *)0x0) goto LAB_001535b0;
  }
  if (*(BIGNUM **)(lVar1 + 0x358) != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(*(BIGNUM **)(lVar1 + 0x358));
    *(BIGNUM **)(param_1 + 0xc10) = pBVar6;
    if (pBVar6 == (BIGNUM *)0x0) goto LAB_001535b0;
  }
  if (*(BIGNUM **)(lVar1 + 0x360) != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(*(BIGNUM **)(lVar1 + 0x360));
    *(BIGNUM **)(param_1 + 0xc18) = pBVar6;
    if (pBVar6 == (BIGNUM *)0x0) goto LAB_001535b0;
  }
  if (*(BIGNUM **)(lVar1 + 0x368) != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(*(BIGNUM **)(lVar1 + 0x368));
    *(BIGNUM **)(param_1 + 0xc20) = pBVar6;
    if (pBVar6 == (BIGNUM *)0x0) goto LAB_001535b0;
  }
  if (*(BIGNUM **)(lVar1 + 0x378) != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(*(BIGNUM **)(lVar1 + 0x378));
    *(BIGNUM **)(param_1 + 0xc30) = pBVar6;
    if (pBVar6 == (BIGNUM *)0x0) goto LAB_001535b0;
  }
  if (*(BIGNUM **)(lVar1 + 0x370) != (BIGNUM *)0x0) {
    pBVar6 = BN_dup(*(BIGNUM **)(lVar1 + 0x370));
    *(BIGNUM **)(param_1 + 0xc28) = pBVar6;
    if (pBVar6 == (BIGNUM *)0x0) goto LAB_001535b0;
  }
  if (*(char **)(lVar1 + 0x338) == (char *)0x0) {
LAB_00153563:
    if (*(char **)(lVar1 + 0x380) == (char *)0x0) {
LAB_00153591:
      *(undefined8 *)(param_1 + 0xc48) = *(undefined8 *)(lVar1 + 0x390);
      return 1;
    }
    pcVar7 = CRYPTO_strdup(*(char **)(lVar1 + 0x380),"ssl/tls_srp.c",0x87);
    *(char **)(param_1 + 0xc38) = pcVar7;
    if (pcVar7 != (char *)0x0) goto LAB_00153591;
    ERR_new();
    uVar9 = 0x88;
  }
  else {
    pcVar7 = CRYPTO_strdup(*(char **)(lVar1 + 0x338),"ssl/tls_srp.c",0x82);
    *(char **)(param_1 + 0xbf0) = pcVar7;
    if (pcVar7 != (char *)0x0) goto LAB_00153563;
    ERR_new();
    uVar9 = 0x83;
  }
  ERR_set_debug("ssl/tls_srp.c",uVar9,"ssl_srp_ctx_init_intern");
  ERR_set_error(0x14,0xc0103,0);
LAB_001535e0:
  CRYPTO_free(*(void **)(param_1 + 0xbf0));
  CRYPTO_free(*(void **)(param_1 + 0xc38));
  BN_free(*(BIGNUM **)(param_1 + 0xbf8));
  BN_free(*(BIGNUM **)(param_1 + 0xc00));
  BN_free(*(BIGNUM **)(param_1 + 0xc08));
  BN_free(*(BIGNUM **)(param_1 + 0xc10));
  BN_free(*(BIGNUM **)(param_1 + 0xc18));
  BN_free(*(BIGNUM **)(param_1 + 0xc20));
  BN_free(*(BIGNUM **)(param_1 + 0xc28));
  BN_free(*(BIGNUM **)(param_1 + 0xc30));
  *(undefined8 *)(param_1 + 0xbd0) = 0;
  puVar10 = (undefined8 *)(param_1 + 0xbd8U & 0xfffffffffffffff8);
  *(undefined8 *)(param_1 + 0xc48) = 0;
  uVar8 = (ulong)((iVar2 - (int)puVar10) + 0x80U >> 3);
  for (; uVar8 != 0; uVar8 = uVar8 - 1) {
    *puVar10 = 0;
    puVar10 = puVar10 + (ulong)bVar11 * -2 + 1;
  }
  return 0;
}