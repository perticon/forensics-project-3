int SSL_use_PrivateKey_file(SSL *ssl,char *file,int type)

{
  undefined8 *puVar1;
  int iVar2;
  BIO_METHOD *type_00;
  BIO *bp;
  long lVar3;
  EVP_PKEY *pkey;
  undefined8 uVar4;
  
  type_00 = BIO_s_file();
  bp = BIO_new(type_00);
  if (bp == (BIO *)0x0) {
    ERR_new();
    iVar2 = 0;
    ERR_set_debug("ssl/ssl_rsa.c",0xa1,"SSL_use_PrivateKey_file");
    ERR_set_error(0x14,0x80007,0);
  }
  else {
    lVar3 = BIO_ctrl(bp,0x6c,3,file);
    if ((int)lVar3 < 1) {
      ERR_new();
      iVar2 = 0;
      ERR_set_debug("ssl/ssl_rsa.c",0xa6,"SSL_use_PrivateKey_file");
      ERR_set_error(0x14,0x80002,0);
    }
    else {
      if (type == 1) {
        puVar1 = *(undefined8 **)&ssl[3].ex_data.dummy;
        uVar4 = 0x80009;
        pkey = (EVP_PKEY *)
               PEM_read_bio_PrivateKey_ex
                         (bp,0,ssl[10].tlsext_ellipticcurvelist,ssl[10].tlsext_opaque_prf_input,
                          *puVar1,puVar1[0x88]);
      }
      else {
        if (type != 2) {
          ERR_new();
          iVar2 = 0;
          ERR_set_debug("ssl/ssl_rsa.c",0xb5,"SSL_use_PrivateKey_file");
          ERR_set_error(0x14,0x7c,0);
          goto LAB_001408b3;
        }
        puVar1 = *(undefined8 **)&ssl[3].ex_data.dummy;
        uVar4 = 0x8000d;
        pkey = (EVP_PKEY *)d2i_PrivateKey_ex_bio(bp,0,*puVar1,puVar1[0x88]);
      }
      if (pkey == (EVP_PKEY *)0x0) {
        ERR_new();
        iVar2 = 0;
        ERR_set_debug("ssl/ssl_rsa.c",0xb9,"SSL_use_PrivateKey_file");
        ERR_set_error(0x14,uVar4,0);
      }
      else {
        iVar2 = SSL_use_PrivateKey(ssl,pkey);
        EVP_PKEY_free(pkey);
      }
    }
  }
LAB_001408b3:
  BIO_free(bp);
  return iVar2;
}