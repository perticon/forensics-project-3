int ssl_set_option_list(char *param_1,int param_2,uint *param_3)

{
  uint uVar1;
  ulong *puVar2;
  char *pcVar3;
  uint *puVar4;
  int iVar5;
  char **ppcVar6;
  uint uVar7;
  ulong uVar8;
  ulong uVar9;
  int iVar10;
  int local_40;
  
  if (param_1 == (char *)0x0) {
    return 0;
  }
  if (param_2 == -1) {
    uVar9 = *(ulong *)(param_3 + 0x28);
    local_40 = 1;
    iVar10 = 0;
    ppcVar6 = *(char ***)(param_3 + 0x26);
    if (uVar9 == 0) {
      return 0;
    }
LAB_0013410e:
    uVar1 = *param_3;
    uVar8 = 0;
    do {
      uVar7 = *(uint *)((long)ppcVar6 + 0xc);
      if (((uVar1 & uVar7 & 0xc) != 0) && (iVar5 = strcmp(*ppcVar6,param_1), iVar5 == 0))
      goto LAB_001340aa;
      uVar8 = uVar8 + 1;
      ppcVar6 = ppcVar6 + 3;
    } while (uVar8 != uVar9);
  }
  else {
    if (*param_1 == '+') {
      local_40 = 1;
      iVar10 = 0;
LAB_001341e3:
      param_2 = param_2 + -1;
      param_1 = param_1 + 1;
      uVar9 = *(ulong *)(param_3 + 0x28);
      ppcVar6 = *(char ***)(param_3 + 0x26);
      if (uVar9 == 0) {
        return 0;
      }
      if (param_2 == -1) goto LAB_0013410e;
    }
    else {
      if (*param_1 == '-') {
        local_40 = 0;
        iVar10 = 1;
        goto LAB_001341e3;
      }
      uVar9 = *(ulong *)(param_3 + 0x28);
      local_40 = 1;
      iVar10 = 0;
      ppcVar6 = *(char ***)(param_3 + 0x26);
      if (uVar9 == 0) {
        return 0;
      }
    }
    uVar8 = 0;
    do {
      if (((*(uint *)((long)ppcVar6 + 0xc) & *param_3 & 0xc) != 0) &&
         (*(int *)(ppcVar6 + 1) == param_2)) {
        iVar5 = OPENSSL_strncasecmp(*ppcVar6,param_1);
        if (iVar5 == 0) {
          uVar7 = *(uint *)((long)ppcVar6 + 0xc);
LAB_001340aa:
          puVar2 = *(ulong **)(param_3 + 10);
          if (puVar2 != (ulong *)0x0) {
            if ((uVar7 & 1) == 0) {
              iVar10 = local_40;
            }
            uVar7 = uVar7 & 0xf00;
            pcVar3 = ppcVar6[2];
            if (uVar7 == 0x100) {
              puVar4 = *(uint **)(param_3 + 0x1e);
              uVar1 = *puVar4;
            }
            else {
              if (uVar7 != 0x200) {
                if (uVar7 != 0) {
                  return 1;
                }
                if (iVar10 != 0) {
                  *puVar2 = (ulong)pcVar3 | *puVar2;
                  return iVar10;
                }
                *puVar2 = ~(ulong)pcVar3 & *puVar2;
                return 1;
              }
              puVar4 = *(uint **)(param_3 + 0x20);
              uVar1 = *puVar4;
            }
            if (iVar10 != 0) {
              *puVar4 = (uint)pcVar3 | uVar1;
              return iVar10;
            }
            *puVar4 = ~(uint)pcVar3 & uVar1;
          }
          return 1;
        }
        uVar9 = *(ulong *)(param_3 + 0x28);
      }
      uVar8 = uVar8 + 1;
      ppcVar6 = ppcVar6 + 3;
    } while (uVar8 < uVar9);
  }
  return 0;
}