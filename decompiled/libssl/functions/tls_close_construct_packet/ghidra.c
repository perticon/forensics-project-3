undefined8 tls_close_construct_packet(long param_1,undefined8 param_2,int param_3)

{
  int iVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  ulong local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 == 0x101) {
LAB_00174244:
    iVar1 = WPACKET_get_length(param_2,&local_28);
    if ((iVar1 != 0) && (local_28 < 0x80000000)) {
      *(ulong *)(param_1 + 0x98) = local_28;
      uVar2 = 1;
      *(undefined8 *)(param_1 + 0xa0) = 0;
      goto LAB_00174282;
    }
  }
  else {
    iVar1 = WPACKET_close(param_2);
    if (iVar1 != 0) goto LAB_00174244;
  }
  uVar2 = 0;
LAB_00174282:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}