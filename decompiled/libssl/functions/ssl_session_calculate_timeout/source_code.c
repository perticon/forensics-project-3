void ssl_session_calculate_timeout(SSL_SESSION *ss)
{
    /* Force positive timeout */
    if (ss->timeout < 0)
        ss->timeout = 0;
    ss->calc_timeout = ss->time + ss->timeout;
    /*
     * |timeout| is always zero or positive, so the check for
     * overflow only needs to consider if |time| is positive
     */
    ss->timeout_ovf = ss->time > 0 && ss->calc_timeout < ss->time;
    /*
     * N.B. Realistic overflow can only occur in our lifetimes on a
     *      32-bit machine in January 2038.
     *      However, There are no controls to limit the |timeout|
     *      value, except to keep it positive.
     */
}