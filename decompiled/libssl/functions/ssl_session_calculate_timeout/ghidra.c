void ssl_session_calculate_timeout(long param_1)

{
  long lVar1;
  
  lVar1 = *(long *)(param_1 + 0x2d8);
  if (lVar1 < 0) {
    *(undefined8 *)(param_1 + 0x2d8) = 0;
    lVar1 = 0;
  }
  lVar1 = lVar1 + *(long *)(param_1 + 0x2e0);
  *(long *)(param_1 + 0x2e8) = lVar1;
  if (lVar1 < 0) {
    lVar1 = 0;
  }
  *(uint *)(param_1 + 0x2f0) = (uint)(lVar1 < *(long *)(param_1 + 0x2e0));
  return;
}