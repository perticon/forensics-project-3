int SSL_CIPHER_get_kx_nid(const SSL_CIPHER *c)
{
    int i = ssl_cipher_info_lookup(ssl_cipher_table_kx, c->algorithm_mkey);

    if (i == -1)
        return NID_undef;
    return ssl_cipher_table_kx[i].nid;
}