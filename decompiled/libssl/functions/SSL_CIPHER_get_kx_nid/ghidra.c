undefined4 SSL_CIPHER_get_kx_nid(long param_1)

{
  int iVar1;
  long lVar2;
  
  iVar1 = *(int *)(param_1 + 0x1c);
  if (iVar1 == 1) {
    lVar2 = 0;
  }
  else if (iVar1 == 4) {
    lVar2 = 1;
  }
  else if (iVar1 == 2) {
    lVar2 = 2;
  }
  else if (iVar1 == 0x80) {
    lVar2 = 3;
  }
  else if (iVar1 == 0x100) {
    lVar2 = 4;
  }
  else if (iVar1 == 0x40) {
    lVar2 = 5;
  }
  else if (iVar1 == 8) {
    lVar2 = 6;
  }
  else if (iVar1 == 0x20) {
    lVar2 = 7;
  }
  else if (iVar1 == 0x10) {
    lVar2 = 8;
  }
  else if (iVar1 == 0x200) {
    lVar2 = 9;
  }
  else {
    lVar2 = 10;
    if (iVar1 != 0) {
      return 0;
    }
  }
  return *(undefined4 *)(ssl_cipher_table_kx + lVar2 * 8 + 4);
}