void ssl_evp_cipher_free(long param_1)

{
  long lVar1;
  
  if (param_1 == 0) {
    return;
  }
  lVar1 = EVP_CIPHER_get0_provider();
  if (lVar1 != 0) {
    EVP_CIPHER_free(param_1);
    return;
  }
  return;
}