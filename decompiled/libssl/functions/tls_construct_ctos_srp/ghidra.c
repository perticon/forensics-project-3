undefined8 tls_construct_ctos_srp(long param_1,undefined8 param_2)

{
  char *__s;
  int iVar1;
  size_t sVar2;
  
  if (*(long *)(param_1 + 0xbf0) == 0) {
    return 2;
  }
  iVar1 = WPACKET_put_bytes__(param_2,0xc,2);
  if ((((iVar1 != 0) && (iVar1 = WPACKET_start_sub_packet_len__(param_2,2), iVar1 != 0)) &&
      (iVar1 = WPACKET_start_sub_packet_len__(param_2,1), iVar1 != 0)) &&
     (iVar1 = WPACKET_set_flags(param_2,1), iVar1 != 0)) {
    __s = *(char **)(param_1 + 0xbf0);
    sVar2 = strlen(__s);
    iVar1 = WPACKET_memcpy(param_2,__s,sVar2);
    if (((iVar1 != 0) && (iVar1 = WPACKET_close(param_2), iVar1 != 0)) &&
       (iVar1 = WPACKET_close(param_2), iVar1 != 0)) {
      return 1;
    }
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x67,"tls_construct_ctos_srp");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}