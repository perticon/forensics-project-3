long SSL_SESSION_get_timeout(SSL_SESSION *s)

{
  if (s != (SSL_SESSION *)0x0) {
    return *(long *)(s[1].sid_ctx + 0x14);
  }
  return 0;
}