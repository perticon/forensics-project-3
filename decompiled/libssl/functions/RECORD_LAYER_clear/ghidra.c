void RECORD_LAYER_clear(undefined8 *param_1)

{
  *(undefined4 *)((long)param_1 + 0xc) = 0xf0;
  param_1[0x20a] = 0;
  *(undefined4 *)(param_1 + 0x20d) = 0;
  param_1[0x20e] = 0;
  param_1[0x210] = 0;
  *(undefined4 *)(param_1 + 0x211) = 0;
  param_1[0x212] = 0;
  param_1[0x213] = 0;
  *(undefined (*) [16])(param_1 + 0x20b) = (undefined  [16])0x0;
  SSL3_BUFFER_clear(0);
  ssl3_release_write_buffer(*param_1);
  param_1[2] = 0;
  SSL3_RECORD_clear(param_1 + 0xca,0x20);
  RECORD_LAYER_reset_read_sequence(param_1);
  RECORD_LAYER_reset_write_sequence(param_1);
  if (param_1[0x217] != 0) {
    DTLS_RECORD_LAYER_clear(param_1);
    return;
  }
  return;
}