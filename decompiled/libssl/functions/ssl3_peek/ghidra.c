ulong ssl3_peek(long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4)

{
  uint uVar1;
  int *piVar2;
  ulong uVar3;
  undefined auVar4 [16];
  undefined8 uVar5;
  undefined8 uVar6;
  
  uVar6 = 0x12addf;
  piVar2 = __errno_location();
  *piVar2 = 0;
  if (*(int *)(param_1 + 0x1ac) != 0) {
    uVar6 = 0x12ae9a;
    ssl3_renegotiate_check(param_1,0);
  }
  *(undefined4 *)(param_1 + 0x1b8) = 1;
  uVar5 = param_4;
  auVar4 = (**(code **)(*(long *)(param_1 + 8) + 0x68))
                     (param_1,0x17,0,param_2,param_3,1,param_4,uVar6);
  uVar3 = SUB168(auVar4,0);
  if ((SUB164(auVar4,0) == -1) && (*(int *)(param_1 + 0x1b8) == 2)) {
    uVar6 = 0x12ae56;
    ossl_statem_set_in_handshake(param_1,1,SUB168(auVar4 >> 0x40,0),uVar5);
    uVar1 = (**(code **)(*(long *)(param_1 + 8) + 0x68))
                      (param_1,0x17,0,param_2,param_3,1,param_4,uVar6);
    ossl_statem_set_in_handshake(param_1,0);
    uVar3 = (ulong)uVar1;
  }
  else {
    *(undefined4 *)(param_1 + 0x1b8) = 0;
  }
  return uVar3;
}