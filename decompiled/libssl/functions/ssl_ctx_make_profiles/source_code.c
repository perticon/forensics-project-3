static int ssl_ctx_make_profiles(const char *profiles_string,
                                 STACK_OF(SRTP_PROTECTION_PROFILE) **out)
{
    STACK_OF(SRTP_PROTECTION_PROFILE) *profiles;

    char *col;
    char *ptr = (char *)profiles_string;
    SRTP_PROTECTION_PROFILE *p;

    if ((profiles = sk_SRTP_PROTECTION_PROFILE_new_null()) == NULL) {
        ERR_raise(ERR_LIB_SSL, SSL_R_SRTP_COULD_NOT_ALLOCATE_PROFILES);
        return 1;
    }

    do {
        col = strchr(ptr, ':');

        if (!find_profile_by_name(ptr, &p, col ? (size_t)(col - ptr)
                                               : strlen(ptr))) {
            if (sk_SRTP_PROTECTION_PROFILE_find(profiles, p) >= 0) {
                ERR_raise(ERR_LIB_SSL, SSL_R_BAD_SRTP_PROTECTION_PROFILE_LIST);
                goto err;
            }

            if (!sk_SRTP_PROTECTION_PROFILE_push(profiles, p)) {
                ERR_raise(ERR_LIB_SSL, SSL_R_SRTP_COULD_NOT_ALLOCATE_PROFILES);
                goto err;
            }
        } else {
            ERR_raise(ERR_LIB_SSL, SSL_R_SRTP_UNKNOWN_PROTECTION_PROFILE);
            goto err;
        }

        if (col)
            ptr = col + 1;
    } while (col);

    sk_SRTP_PROTECTION_PROFILE_free(*out);

    *out = profiles;

    return 0;
 err:
    sk_SRTP_PROTECTION_PROFILE_free(profiles);
    return 1;
}