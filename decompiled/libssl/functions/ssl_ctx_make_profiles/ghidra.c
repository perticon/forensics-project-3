undefined4 ssl_ctx_make_profiles(char *param_1,long *param_2)

{
  int iVar1;
  long lVar2;
  char *pcVar3;
  size_t sVar4;
  char *__s;
  size_t __n;
  undefined1 *puVar5;
  
  lVar2 = OPENSSL_sk_new_null();
  if (lVar2 == 0) {
    ERR_new();
    ERR_set_debug("ssl/d1_srtp.c",0x67,"ssl_ctx_make_profiles");
    ERR_set_error(0x14,0x16a,0);
    return 1;
  }
  do {
    pcVar3 = strchr(param_1,0x3a);
    if (pcVar3 == (char *)0x0) {
      __n = strlen(param_1);
      __s = srtp_known_profiles._0_8_;
    }
    else {
      __n = (long)pcVar3 - (long)param_1;
      __s = srtp_known_profiles._0_8_;
    }
    srtp_known_profiles._0_8_ = __s;
    if (__s == (char *)0x0) {
LAB_00125ad0:
      ERR_new();
      ERR_set_debug("ssl/d1_srtp.c",0x7a,"ssl_ctx_make_profiles");
      ERR_set_error(0x14,0x16c,0);
LAB_00125b00:
      OPENSSL_sk_free(lVar2);
      return 1;
    }
    puVar5 = srtp_known_profiles;
    while ((sVar4 = strlen(__s), __n != sVar4 || (iVar1 = strncmp(__s,param_1,__n), iVar1 != 0))) {
      __s = *(char **)(puVar5 + 0x10);
      puVar5 = puVar5 + 0x10;
      if (__s == (char *)0x0) goto LAB_00125ad0;
    }
    iVar1 = OPENSSL_sk_find(lVar2,puVar5);
    if (-1 < iVar1) {
      ERR_new();
      ERR_set_debug("ssl/d1_srtp.c",0x71,"ssl_ctx_make_profiles");
      ERR_set_error(0x14,0x161,0);
      goto LAB_00125b00;
    }
    iVar1 = OPENSSL_sk_push(lVar2,puVar5);
    if (iVar1 == 0) {
      ERR_new();
      ERR_set_debug("ssl/d1_srtp.c",0x76,"ssl_ctx_make_profiles");
      ERR_set_error(0x14,0x16a,0);
      goto LAB_00125b00;
    }
    if (pcVar3 == (char *)0x0) {
      OPENSSL_sk_free(*param_2);
      *param_2 = lVar2;
      return 0;
    }
    param_1 = pcVar3 + 1;
  } while( true );
}