int SSL_CTX_use_psk_identity_hint(SSL_CTX *ctx,char *identity_hint)

{
  long lVar1;
  size_t sVar2;
  char *pcVar3;
  
  if (identity_hint == (char *)0x0) {
    CRYPTO_free(*(void **)(*(long *)ctx->sid_ctx + 0x200));
    *(undefined8 *)(*(long *)ctx->sid_ctx + 0x200) = 0;
    return 1;
  }
  sVar2 = strlen(identity_hint);
  if (sVar2 < 0x101) {
    CRYPTO_free(*(void **)(*(long *)ctx->sid_ctx + 0x200));
    lVar1 = *(long *)ctx->sid_ctx;
    pcVar3 = CRYPTO_strdup(identity_hint,"ssl/ssl_lib.c",0x11cf);
    *(char **)(lVar1 + 0x200) = pcVar3;
    return (int)(*(long *)(*(long *)ctx->sid_ctx + 0x200) != 0);
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x11ca,"SSL_CTX_use_psk_identity_hint");
  ERR_set_error(0x14,0x92,0);
  return 0;
}