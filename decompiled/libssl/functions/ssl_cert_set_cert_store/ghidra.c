undefined8 ssl_cert_set_cert_store(long param_1,long param_2,int param_3,int param_4)

{
  X509_STORE *v;
  long *plVar1;
  
  if (param_3 == 0) {
    plVar1 = (long *)(param_1 + 0x1d0);
    v = *(X509_STORE **)(param_1 + 0x1d0);
  }
  else {
    plVar1 = (long *)(param_1 + 0x1c8);
    v = *(X509_STORE **)(param_1 + 0x1c8);
  }
  X509_STORE_free(v);
  *plVar1 = param_2;
  if ((param_4 != 0) && (param_2 != 0)) {
    X509_STORE_up_ref(param_2);
    return 1;
  }
  return 1;
}