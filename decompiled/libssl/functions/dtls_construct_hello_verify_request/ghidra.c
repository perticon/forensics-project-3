int dtls_construct_hello_verify_request(long param_1,undefined8 param_2)

{
  code *pcVar1;
  int iVar2;
  long in_FS_OFFSET;
  uint local_24;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  pcVar1 = *(code **)(*(long *)(param_1 + 0x9a8) + 0xd0);
  if (pcVar1 != (code *)0x0) {
    iVar2 = (*pcVar1)(param_1,*(undefined8 *)(param_1 + 0x4b8),&local_24);
    if ((iVar2 != 0) && (local_24 < 0x100)) {
      *(ulong *)(*(long *)(param_1 + 0x4b8) + 0x100) = (ulong)local_24;
      iVar2 = dtls_raw_hello_verify_request(param_2);
      if (iVar2 == 0) {
        ERR_new();
        ERR_set_debug("ssl/statem/statem_srvr.c",0x516,"dtls_construct_hello_verify_request");
        ossl_statem_fatal(param_1,0xffffffff,0xc0103,0);
      }
      else {
        iVar2 = 1;
      }
      goto LAB_0017bff8;
    }
  }
  ERR_new();
  iVar2 = 0;
  ERR_set_debug("ssl/statem/statem_srvr.c",0x50f,"dtls_construct_hello_verify_request");
  ossl_statem_fatal(param_1,0xffffffff,400,0);
LAB_0017bff8:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}