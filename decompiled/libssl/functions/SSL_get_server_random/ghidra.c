void SSL_get_server_random(long param_1,undefined8 *param_2,ulong param_3)

{
  ulong uVar1;
  uint uVar2;
  uint uVar3;
  ulong uVar4;
  long lVar5;
  
  if (param_3 != 0) {
    uVar1 = 0x20;
    if (param_3 < 0x21) {
      uVar1 = param_3;
    }
    uVar4 = uVar1 & 0xffffffff;
    if ((uint)uVar1 < 8) {
      if ((uVar1 & 4) != 0) {
        *(undefined4 *)param_2 = *(undefined4 *)(param_1 + 0x140);
        *(undefined4 *)((long)param_2 + (uVar4 - 4)) = *(undefined4 *)(param_1 + 0x13c + uVar4);
        return;
      }
      if (((int)uVar4 != 0) &&
         (*(undefined *)param_2 = *(undefined *)(param_1 + 0x140), (uVar1 & 2) != 0)) {
        *(undefined2 *)((long)param_2 + (uVar4 - 2)) = *(undefined2 *)(param_1 + 0x13e + uVar4);
        return;
      }
    }
    else {
      *param_2 = *(undefined8 *)(param_1 + 0x140);
      *(undefined8 *)((long)param_2 + ((uVar1 & 0xffffffff) - 8)) =
           *(undefined8 *)(param_1 + 0x138 + (uVar1 & 0xffffffff));
      lVar5 = (long)param_2 - ((ulong)(param_2 + 1) & 0xfffffffffffffff8);
      uVar3 = (uint)uVar1 + (int)lVar5 & 0xfffffff8;
      if (7 < uVar3) {
        uVar2 = 0;
        do {
          uVar1 = (ulong)uVar2;
          uVar2 = uVar2 + 8;
          *(undefined8 *)(((ulong)(param_2 + 1) & 0xfffffffffffffff8) + uVar1) =
               *(undefined8 *)(((param_1 + 0x140) - lVar5) + uVar1);
        } while (uVar2 < uVar3);
      }
    }
  }
  return;
}