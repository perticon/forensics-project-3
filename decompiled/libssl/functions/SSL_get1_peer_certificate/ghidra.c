long SSL_get1_peer_certificate(void)

{
  long lVar1;
  
  lVar1 = SSL_get0_peer_certificate();
  if (lVar1 != 0) {
    X509_up_ref(lVar1);
  }
  return lVar1;
}