COMP_METHOD * SSL_get_current_expansion(SSL *s)

{
  COMP_METHOD *pCVar1;
  
  if (*(long *)&s[3].init_num != 0) {
    pCVar1 = (COMP_METHOD *)(*(code *)PTR_COMP_CTX_get_method_001a8238)();
    return pCVar1;
  }
  return (COMP_METHOD *)0x0;
}