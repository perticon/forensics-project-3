undefined8 ssl_srp_server_param_with_username_intern(long param_1,undefined4 *param_2)

{
  code *pcVar1;
  int iVar2;
  undefined8 uVar3;
  BIGNUM *pBVar4;
  long lVar5;
  long in_FS_OFFSET;
  uchar auStack88 [56];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  pcVar1 = *(code **)(param_1 + 0xbd8);
  *param_2 = 0x73;
  if (pcVar1 != (code *)0x0) {
    uVar3 = (*pcVar1)(param_1,param_2,*(undefined8 *)(param_1 + 0xbd0));
    if ((int)uVar3 != 0) goto LAB_00153878;
  }
  lVar5 = *(long *)(param_1 + 0xbf8);
  *param_2 = 0x50;
  if ((((lVar5 != 0) && (*(long *)(param_1 + 0xc00) != 0)) && (*(long *)(param_1 + 0xc08) != 0)) &&
     (*(long *)(param_1 + 0xc30) != 0)) {
    iVar2 = RAND_priv_bytes_ex(**(undefined8 **)(param_1 + 0x9a8),auStack88,0x30,0);
    if (0 < iVar2) {
      pBVar4 = BN_bin2bn(auStack88,0x30,(BIGNUM *)0x0);
      *(BIGNUM **)(param_1 + 0xc28) = pBVar4;
      OPENSSL_cleanse(auStack88,0x30);
      lVar5 = SRP_Calc_B_ex(*(undefined8 *)(param_1 + 0xc28),*(undefined8 *)(param_1 + 0xbf8),
                            *(undefined8 *)(param_1 + 0xc00),*(undefined8 *)(param_1 + 0xc30),
                            **(undefined8 **)(param_1 + 0x9a8),
                            (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
      *(long *)(param_1 + 0xc10) = lVar5;
      uVar3 = 0;
      if (lVar5 != 0) goto LAB_00153878;
    }
  }
  uVar3 = 2;
LAB_00153878:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}