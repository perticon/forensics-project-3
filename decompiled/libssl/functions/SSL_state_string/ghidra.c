char * SSL_state_string(SSL *s)

{
  int iVar1;
  undefined4 uVar2;
  
  iVar1 = ossl_statem_in_error();
  if (iVar1 == 0) {
    uVar2 = SSL_get_state(s);
    switch(uVar2) {
    case 0:
      return "PINIT";
    case 1:
      return "SSLOK";
    case 2:
      return "DRCHV";
    case 3:
      return "TRSH";
    case 4:
      return "TRSC";
    case 5:
      return "TRCS";
    case 6:
      return "TRSKE";
    case 7:
      return "TRCR";
    case 8:
      return "TRSD";
    case 9:
      return "TRST";
    case 10:
    case 0x1f:
      return "TRCCS";
    case 0xb:
    case 0x20:
      return "TRFIN";
    case 0xc:
      return "TWCH";
    case 0xd:
      return "TWCC";
    case 0xe:
      return "TWCKE";
    case 0xf:
      return "TWCV";
    case 0x10:
    case 0x23:
      return "TWCCS";
    case 0x11:
      return "TWNP";
    case 0x12:
    case 0x24:
      return "TWFIN";
    case 0x13:
      return "TWHR";
    case 0x14:
      return "TRCH";
    case 0x15:
      return "DWCHV";
    case 0x16:
      return "TWSH";
    case 0x17:
      return "TWSC";
    case 0x18:
      return "TWSKE";
    case 0x19:
      return "TWCR";
    case 0x1a:
      return "TWSD";
    case 0x1b:
      return "TRCC";
    case 0x1c:
      return "TRCKE";
    case 0x1d:
      return "TRCV";
    case 0x1e:
      return "TRNP";
    case 0x21:
      return "TWST";
    case 0x22:
      return "TWCS";
    case 0x25:
      return "TWEE";
    case 0x26:
      return "TREE";
    case 0x27:
      return "TRSCV";
    case 0x28:
      return "TWSCV";
    case 0x29:
      return "TRHR";
    case 0x2a:
      return "TWSKU";
    case 0x2b:
      return "TWCKU";
    case 0x2c:
      return "TRCKU";
    case 0x2d:
      return "TRSKU";
    case 0x2e:
      return "TED";
    case 0x2f:
      return "TPEDE";
    case 0x30:
    case 0x31:
      return "TWEOED";
    default:
      return "UNKWN";
    }
  }
  return "SSLERR";
}