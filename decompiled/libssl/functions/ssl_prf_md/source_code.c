const EVP_MD *ssl_prf_md(SSL *s)
{
    return ssl_md(s->ctx, ssl_get_algorithm2(s) >> TLS1_PRF_DGST_SHIFT);
}