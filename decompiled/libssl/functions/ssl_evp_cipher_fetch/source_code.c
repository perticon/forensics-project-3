const EVP_CIPHER *ssl_evp_cipher_fetch(OSSL_LIB_CTX *libctx,
                                       int nid,
                                       const char *properties)
{
    const EVP_CIPHER *ciph;

    ciph = tls_get_cipher_from_engine(nid);
    if (ciph != NULL)
        return ciph;

    /*
     * If there is no engine cipher then we do an explicit fetch. This may fail
     * and that could be ok
     */
    ERR_set_mark();
    ciph = EVP_CIPHER_fetch(libctx, OBJ_nid2sn(nid), properties);
    ERR_pop_to_mark();
    return ciph;
}