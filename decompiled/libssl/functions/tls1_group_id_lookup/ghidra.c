long tls1_group_id_lookup(long param_1,short param_2)

{
  long lVar1;
  long lVar2;
  
  if (*(long *)(param_1 + 0x630) != 0) {
    lVar1 = *(long *)(param_1 + 0x628);
    lVar2 = 0;
    do {
      if (*(short *)(lVar1 + 0x1c) == param_2) {
        return lVar1;
      }
      lVar2 = lVar2 + 1;
      lVar1 = lVar1 + 0x38;
    } while (*(long *)(param_1 + 0x630) != lVar2);
  }
  return 0;
}