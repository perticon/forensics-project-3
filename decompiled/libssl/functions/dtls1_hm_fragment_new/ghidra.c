void * dtls1_hm_fragment_new(long param_1,int param_2)

{
  void *ptr;
  void *ptr_00;
  long lVar1;
  
  ptr = CRYPTO_malloc(0x68,"ssl/statem/statem_dtls.c",0x3e);
  if (ptr == (void *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/statem/statem_dtls.c",0x3f,"dtls1_hm_fragment_new");
    ERR_set_error(0x14,0xc0100,0);
  }
  else {
    ptr_00 = (void *)0x0;
    if ((param_1 != 0) &&
       (ptr_00 = CRYPTO_malloc((int)param_1,"ssl/statem/statem_dtls.c",0x44), ptr_00 == (void *)0x0)
       ) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_dtls.c",0x45,"dtls1_hm_fragment_new");
      ERR_set_error(0x14,0xc0100,0);
      CRYPTO_free(ptr);
      return (void *)0x0;
    }
    *(void **)((long)ptr + 0x58) = ptr_00;
    lVar1 = 0;
    if ((param_2 != 0) &&
       (lVar1 = CRYPTO_zalloc(param_1 + 7U >> 3,"ssl/statem/statem_dtls.c",0x50), lVar1 == 0)) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_dtls.c",0x52,"dtls1_hm_fragment_new");
      ERR_set_error(0x14,0xc0100,0);
      CRYPTO_free(ptr_00);
      CRYPTO_free(ptr);
      return (void *)0x0;
    }
    *(long *)((long)ptr + 0x60) = lVar1;
  }
  return ptr;
}