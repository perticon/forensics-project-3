void** dtls1_hm_fragment_new(void** rdi, int32_t esi, ...) {
    void** rax3;
    void** r12_4;
    void** r13_5;
    void** rax6;
    void** rax7;

    rax3 = fun_21b90(0x68, "ssl/statem/statem_dtls.c", 62);
    r12_4 = rax3;
    if (!rax3) {
        fun_20230();
        fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
        fun_20680();
    } else {
        *reinterpret_cast<int32_t*>(&r13_5) = 0;
        *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
        if (!rdi || (rax6 = fun_21b90(rdi, "ssl/statem/statem_dtls.c", 68), r13_5 = rax6, !!rax6)) {
            *reinterpret_cast<void***>(r12_4 + 88) = r13_5;
            *reinterpret_cast<int32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
            if (!esi || (rax7 = fun_219a0(reinterpret_cast<uint64_t>(rdi + 7) >> 3, "ssl/statem/statem_dtls.c", 80), !!rax7)) {
                *reinterpret_cast<void***>(r12_4 + 96) = rax7;
            } else {
                fun_20230();
                fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
                fun_20680();
                fun_208d0();
                *reinterpret_cast<int32_t*>(&r12_4) = 0;
                *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
                fun_208d0();
            }
        } else {
            fun_20230();
            fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
            fun_20680();
            *reinterpret_cast<int32_t*>(&r12_4) = 0;
            *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
            fun_208d0();
        }
    }
    return r12_4;
}