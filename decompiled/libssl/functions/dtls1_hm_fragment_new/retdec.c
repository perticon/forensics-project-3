int64_t dtls1_hm_fragment_new(int64_t a1, int64_t a2) {
    int64_t result = function_21b90(); // 0x71e40
    if (result == 0) {
        // 0x71f70
        function_20230();
        function_207e0();
        function_20680();
        // 0x71e6d
        return 0;
    }
    int64_t v1 = 0; // 0x71e57
    if (a1 != 0) {
        // 0x71e80
        v1 = function_21b90();
        if (v1 == 0) {
            // 0x71e9c
            function_20230();
            function_207e0();
            function_20680();
            function_208d0();
            // 0x71e6d
            return 0;
        }
    }
    // 0x71e59
    *(int64_t *)(result + 88) = v1;
    if ((int32_t)a2 == 0) {
        // 0x71e68
        *(int64_t *)(result + 96) = 0;
        // 0x71e6d
        return result;
    }
    int64_t v2 = function_219a0(); // 0x71efc
    if (v2 != 0) {
        // 0x71e68
        *(int64_t *)(result + 96) = v2;
        // 0x71e6d
        return result;
    }
    // 0x71f0a
    function_20230();
    function_207e0();
    function_20680();
    function_208d0();
    function_208d0();
    // 0x71e6d
    return 0;
}