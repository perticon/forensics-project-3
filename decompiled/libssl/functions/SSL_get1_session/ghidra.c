SSL_SESSION * SSL_get1_session(SSL *ssl)

{
  SSL_SESSION *pSVar1;
  int iVar2;
  
  iVar2 = CRYPTO_THREAD_read_lock(*(undefined8 *)&ssl[10].next_proto_negotiated_len);
  if (iVar2 != 0) {
    pSVar1 = *(SSL_SESSION **)&ssl[3].sid_ctx_length;
    if (pSVar1 != (SSL_SESSION *)0x0) {
      SSL_SESSION_up_ref(pSVar1);
    }
    CRYPTO_THREAD_unlock(*(undefined8 *)&ssl[10].next_proto_negotiated_len);
    return pSVar1;
  }
  return (SSL_SESSION *)0x0;
}