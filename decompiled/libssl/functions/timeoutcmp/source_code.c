__owur static int timeoutcmp(SSL_SESSION *a, SSL_SESSION *b)
{
    /* if only one overflowed, then it is greater */
    if (a->timeout_ovf && !b->timeout_ovf)
        return 1;
    if (!a->timeout_ovf && b->timeout_ovf)
        return -1;
    /* No overflow, or both overflowed, so straight compare is safe */
    if (a->calc_timeout < b->calc_timeout)
        return -1;
    if (a->calc_timeout > b->calc_timeout)
        return 1;
    return 0;
}