undefined4 srp_generate_server_master_secret(long param_1)

{
  int iVar1;
  int iVar2;
  undefined4 uVar3;
  BIGNUM *a;
  BIGNUM *a_00;
  uchar *to;
  
  iVar1 = SRP_Verify_A_mod_N(*(undefined8 *)(param_1 + 0xc18),*(undefined8 *)(param_1 + 0xbf8));
  if (iVar1 != 0) {
    a = (BIGNUM *)
        SRP_Calc_u_ex(*(undefined8 *)(param_1 + 0xc18),*(undefined8 *)(param_1 + 0xc10),
                      *(undefined8 *)(param_1 + 0xbf8),**(undefined8 **)(param_1 + 0x9a8),
                      (*(undefined8 **)(param_1 + 0x9a8))[0x88]);
    if (a != (BIGNUM *)0x0) {
      uVar3 = 0;
      a_00 = (BIGNUM *)
             SRP_Calc_server_key(*(undefined8 *)(param_1 + 0xc18),*(undefined8 *)(param_1 + 0xc30),a
                                 ,*(undefined8 *)(param_1 + 0xc28),*(undefined8 *)(param_1 + 0xbf8))
      ;
      if (a_00 != (BIGNUM *)0x0) {
        iVar2 = BN_num_bits(a_00);
        iVar1 = iVar2 + 0xe;
        if (-1 < iVar2 + 7) {
          iVar1 = iVar2 + 7;
        }
        to = (uchar *)CRYPTO_malloc(iVar1 >> 3,"ssl/tls_srp.c",0x13d);
        if (to == (uchar *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/tls_srp.c",0x13e,"srp_generate_server_master_secret");
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
        }
        else {
          BN_bn2bin(a_00,to);
          uVar3 = ssl_generate_master_secret(param_1,to,(long)(iVar1 >> 3),1);
        }
      }
      goto LAB_00153bd5;
    }
  }
  uVar3 = 0;
  a = (BIGNUM *)0x0;
  a_00 = (BIGNUM *)0x0;
LAB_00153bd5:
  BN_clear_free(a_00);
  BN_clear_free(a);
  return uVar3;
}