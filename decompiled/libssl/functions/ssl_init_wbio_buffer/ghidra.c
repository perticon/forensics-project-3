undefined8 ssl_init_wbio_buffer(long param_1)

{
  BIO_METHOD *type;
  BIO *pBVar1;
  long lVar2;
  
  if (*(long *)(param_1 + 0x20) != 0) {
    return 1;
  }
  type = BIO_f_buffer();
  pBVar1 = BIO_new(type);
  if (pBVar1 != (BIO *)0x0) {
    lVar2 = BIO_int_ctrl(pBVar1,0x75,1,0);
    if (lVar2 != 0) {
      *(BIO **)(param_1 + 0x20) = pBVar1;
      pBVar1 = BIO_push(pBVar1,*(BIO **)(param_1 + 0x18));
      *(BIO **)(param_1 + 0x18) = pBVar1;
      return 1;
    }
  }
  BIO_free(pBVar1);
  ERR_new();
  ERR_set_debug("ssl/ssl_lib.c",0x108a,"ssl_init_wbio_buffer");
  ERR_set_error(0x14,0x80007,0);
  return 0;
}