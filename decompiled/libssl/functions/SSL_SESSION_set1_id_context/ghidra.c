int SSL_SESSION_set1_id_context(SSL_SESSION *s,uchar *sid_ctx,uint sid_ctx_len)

{
  uint uVar1;
  long lVar2;
  ulong uVar3;
  uint uVar4;
  ulong uVar5;
  
  if (sid_ctx_len < 0x21) {
    uVar3 = (ulong)sid_ctx_len;
    *(ulong *)(s[1].master_key + 0xc) = uVar3;
    if (s[1].master_key + 0x14 != sid_ctx) {
      if (sid_ctx_len < 8) {
        if ((sid_ctx_len & 4) == 0) {
          if ((sid_ctx_len != 0) && (s[1].master_key[0x14] = *sid_ctx, (sid_ctx_len & 2) != 0)) {
            *(undefined2 *)(s[1].master_key + uVar3 + 0x12) = *(undefined2 *)(sid_ctx + (uVar3 - 2))
            ;
          }
        }
        else {
          *(undefined4 *)(s[1].master_key + 0x14) = *(undefined4 *)sid_ctx;
          *(undefined4 *)(s[1].master_key + uVar3 + 0x10) = *(undefined4 *)(sid_ctx + (uVar3 - 4));
        }
      }
      else {
        *(undefined8 *)(s[1].master_key + 0x14) = *(undefined8 *)sid_ctx;
        uVar5 = (ulong)(s[1].master_key + 0x1c) & 0xfffffffffffffff8;
        *(undefined8 *)(s[1].master_key + uVar3 + 0xc) = *(undefined8 *)(sid_ctx + (uVar3 - 8));
        lVar2 = (long)(s[1].master_key + 0x14) - uVar5;
        uVar4 = sid_ctx_len + (int)lVar2 & 0xfffffff8;
        if (7 < uVar4) {
          uVar1 = 0;
          do {
            uVar3 = (ulong)uVar1;
            uVar1 = uVar1 + 8;
            *(undefined8 *)(uVar5 + uVar3) = *(undefined8 *)(sid_ctx + (uVar3 - lVar2));
          } while (uVar1 < uVar4);
        }
      }
    }
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/ssl_sess.c",0x41a,"SSL_SESSION_set1_id_context");
  ERR_set_error(0x14,0x111,0);
  return 0;
}