int xname_cmp(X509_NAME *param_1,X509_NAME *param_2)

{
  uchar *ptr;
  int iVar1;
  int iVar2;
  long in_FS_OFFSET;
  uchar *local_30;
  uchar *local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_30 = (uchar *)0x0;
  local_28 = (uchar *)0x0;
  iVar1 = i2d_X509_NAME(param_1,&local_30);
  iVar2 = i2d_X509_NAME(param_2,&local_28);
  ptr = local_30;
  if ((iVar1 < 0) || (iVar2 < 0)) {
    iVar1 = -2;
  }
  else if (iVar1 == iVar2) {
    iVar1 = memcmp(local_30,local_28,(long)iVar1);
  }
  else {
    iVar1 = iVar1 - iVar2;
  }
  CRYPTO_free(ptr);
  CRYPTO_free(local_28);
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}