undefined4 tls_construct_finished(long param_1,undefined8 param_2)

{
  void *__src;
  uint uVar1;
  int iVar2;
  ulong __n;
  long lVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  
  lVar3 = *(long *)(*(int **)(param_1 + 8) + 0x30);
  if (*(int *)(param_1 + 0x38) == 0) {
    uVar1 = *(uint *)(lVar3 + 0x60);
    if (*(int *)(param_1 + 0xba8) != 4) {
      *(undefined4 *)(param_1 + 0x70) = 1;
    }
    if (((((uVar1 & 8) == 0) && (iVar2 = **(int **)(param_1 + 8), 0x303 < iVar2)) &&
        (iVar2 != 0x10000)) && (*(int *)(param_1 + 0x2f0) == 0)) {
      iVar2 = (**(code **)(lVar3 + 0x20))(param_1,0x92);
      if (iVar2 == 0) {
        return 0;
      }
      lVar3 = *(long *)(*(long *)(param_1 + 8) + 0xc0);
      if (*(int *)(param_1 + 0x38) != 0) goto LAB_00175098;
    }
    uVar5 = *(undefined8 *)(lVar3 + 0x30);
    uVar4 = *(undefined8 *)(lVar3 + 0x38);
  }
  else {
LAB_00175098:
    uVar5 = *(undefined8 *)(lVar3 + 0x40);
    uVar4 = *(undefined8 *)(lVar3 + 0x48);
  }
  __src = (void *)(param_1 + 0x1c0);
  __n = (**(code **)(lVar3 + 0x28))(param_1,uVar5,uVar4,__src);
  if (__n != 0) {
    *(ulong *)(param_1 + 0x240) = __n;
    iVar2 = WPACKET_memcpy(param_2,__src,__n);
    if (iVar2 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x25e,"tls_construct_finished");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    }
    if (((((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) == 0) &&
         (iVar2 = **(int **)(param_1 + 8), 0x303 < iVar2)) && (iVar2 != 0x10000)) ||
       (iVar2 = ssl_log_secret(param_1,"CLIENT_RANDOM",*(long *)(param_1 + 0x918) + 0x50,
                               *(undefined8 *)(*(long *)(param_1 + 0x918) + 8)), iVar2 != 0)) {
      if (__n < 0x41) {
        if (*(int *)(param_1 + 0x38) == 0) {
          memcpy((void *)(param_1 + 0x3f0),__src,__n);
          *(ulong *)(param_1 + 0x430) = __n;
          return 1;
        }
        memcpy((void *)(param_1 + 0x438),__src,__n);
        *(ulong *)(param_1 + 0x478) = __n;
        return 1;
      }
      ERR_new();
      ERR_set_debug("ssl/statem/statem_lib.c",0x271,"tls_construct_finished");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
    }
  }
  return 0;
}