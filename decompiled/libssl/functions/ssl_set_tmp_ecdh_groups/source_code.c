int ssl_set_tmp_ecdh_groups(uint16_t **pext, size_t *pextlen,
                            void *key)
{
#  ifndef OPENSSL_NO_EC
    const EC_GROUP *group = EC_KEY_get0_group((const EC_KEY *)key);
    int nid;

    if (group == NULL) {
        ERR_raise(ERR_LIB_SSL, SSL_R_MISSING_PARAMETERS);
        return 0;
    }
    nid = EC_GROUP_get_curve_name(group);
    if (nid == NID_undef)
        return 0;
    return tls1_set_groups(pext, pextlen, &nid, 1);
#  else
    return 0;
#  endif
}