undefined8 tls_client_key_exchange_post_work(long param_1)

{
  uint uVar1;
  int iVar2;
  long lVar3;
  undefined8 uVar4;
  
  lVar3 = *(long *)(param_1 + 0x360);
  uVar4 = *(undefined8 *)(param_1 + 0x368);
  uVar1 = *(uint *)(*(long *)(param_1 + 0x2e0) + 0x1c);
  if ((uVar1 & 0x20) == 0) {
    if ((lVar3 == 0) && ((uVar1 & 8) == 0)) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_clnt.c",0xd15,"tls_client_key_exchange_post_work");
      ossl_statem_fatal(param_1,0x50,0xc0100,0);
    }
    else {
      iVar2 = ssl_generate_master_secret(param_1,lVar3,uVar4,1);
      if (iVar2 != 0) {
        return 1;
      }
      uVar4 = 0;
      lVar3 = 0;
    }
  }
  else {
    iVar2 = srp_generate_client_master_secret();
    if (iVar2 != 0) {
      return 1;
    }
  }
  CRYPTO_clear_free(lVar3,uVar4,"ssl/statem/statem_clnt.c",0xd42);
  *(undefined8 *)(param_1 + 0x360) = 0;
  *(undefined8 *)(param_1 + 0x368) = 0;
  return 0;
}