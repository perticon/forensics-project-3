tls1_generate_master_secret
          (long param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 *param_5
          )

{
  int iVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  size_t local_d0;
  undefined local_c8 [136];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*(byte *)(*(long *)(param_1 + 0x918) + 0x388) & 1) == 0) {
    uVar2 = tls1_PRF_constprop_0
                      (param_1,"master secret",0xd,param_1 + 0x160,0x20,0,0,param_1 + 0x140,0x20,
                       param_3,param_4,param_2,0x30,1);
    if ((int)uVar2 == 0) goto LAB_00146df7;
LAB_00146e5c:
    *param_5 = 0x30;
    uVar2 = 1;
  }
  else {
    iVar1 = ssl3_digest_cached_records(param_1,1);
    if (iVar1 != 0) {
      iVar1 = ssl_handshake_hash(param_1,local_c8,0x80,&local_d0);
      if (iVar1 != 0) {
        iVar1 = tls1_PRF_constprop_0
                          (param_1,"extended master secret",0x16,local_c8,local_d0,0,0,0,0,param_3,
                           param_4,param_2,0x30,1);
        if (iVar1 != 0) {
          OPENSSL_cleanse(local_c8,local_d0);
          goto LAB_00146e5c;
        }
      }
    }
    uVar2 = 0;
  }
LAB_00146df7:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}