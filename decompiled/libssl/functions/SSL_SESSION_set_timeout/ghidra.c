long SSL_SESSION_set_timeout(SSL_SESSION *s,long t)

{
  int iVar1;
  
  if (s == (SSL_SESSION *)0x0) {
    return 0;
  }
  if (-1 < t) {
    if (*(long *)(s[1].krb5_client_princ + 0xa8) == 0) {
      *(long *)(s[1].sid_ctx + 0x14) = t;
      ssl_session_calculate_timeout();
      return 1;
    }
    iVar1 = CRYPTO_THREAD_write_lock
                      (*(undefined8 *)(*(long *)(s[1].krb5_client_princ + 0xa8) + 0x3c8));
    if (iVar1 != 0) {
      *(long *)(s[1].sid_ctx + 0x14) = t;
      ssl_session_calculate_timeout(s);
      SSL_SESSION_list_add(*(undefined8 *)(s[1].krb5_client_princ + 0xa8),s);
      CRYPTO_THREAD_unlock(*(undefined8 *)(*(long *)(s[1].krb5_client_princ + 0xa8) + 0x3c8));
      return 1;
    }
  }
  return 0;
}