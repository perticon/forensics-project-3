long SSL_SESSION_set_timeout(SSL_SESSION *s, long t)
{
    time_t new_timeout = (time_t)t;

    if (s == NULL || t < 0)
        return 0;
    if (s->owner != NULL) {
        if (!CRYPTO_THREAD_write_lock(s->owner->lock))
            return 0;
        s->timeout = new_timeout;
        ssl_session_calculate_timeout(s);
        SSL_SESSION_list_add(s->owner, s);
        CRYPTO_THREAD_unlock(s->owner->lock);
    } else {
        s->timeout = new_timeout;
        ssl_session_calculate_timeout(s);
    }
    return 1;
}