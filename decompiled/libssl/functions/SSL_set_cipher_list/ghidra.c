int SSL_set_cipher_list(SSL *s,char *str)

{
  int iVar1;
  long lVar2;
  long lVar3;
  int iVar4;
  int iVar5;
  
  iVar5 = 0;
  lVar2 = ssl_create_cipher_list
                    (*(undefined8 *)&s[3].ex_data.dummy,
                     *(undefined8 *)&s[1].next_proto_negotiated_len,&s[1].initial_ctx,
                     &s[1].next_proto_negotiated,str,s[3].d1);
  if (lVar2 != 0) {
    iVar4 = 0;
    while( true ) {
      iVar1 = OPENSSL_sk_num(lVar2);
      if (iVar1 <= iVar4) break;
      lVar3 = OPENSSL_sk_value(lVar2,iVar4);
      iVar4 = iVar4 + 1;
      iVar5 = iVar5 + (uint)(*(int *)(lVar3 + 0x2c) < 0x304);
    }
    if (iVar5 == 0) {
      ERR_new();
      ERR_set_debug("ssl/ssl_lib.c",0xacc,"SSL_set_cipher_list");
      ERR_set_error(0x14,0xb9,0);
      return 0;
    }
    iVar5 = 1;
  }
  return iVar5;
}