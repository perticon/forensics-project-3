undefined8 tls_construct_stoc_session_ticket(long param_1,undefined8 param_2)

{
  int iVar1;
  
  if (*(int *)(param_1 + 0xa98) != 0) {
    iVar1 = tls_use_ticket();
    if (iVar1 != 0) {
      iVar1 = WPACKET_put_bytes__(param_2,0x23,2);
      if (iVar1 != 0) {
        iVar1 = WPACKET_put_bytes__(param_2,0,2);
        if (iVar1 != 0) {
          return 1;
        }
      }
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_srvr.c",0x575,"tls_construct_stoc_session_ticket");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    }
  }
  *(undefined4 *)(param_1 + 0xa98) = 0;
  return 2;
}