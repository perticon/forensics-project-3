int SSL_do_handshake(SSL *s)

{
  int iVar1;
  long lVar2;
  int in_ECX;
  int stn;
  long in_R8;
  int *in_R9;
  long in_FS_OFFSET;
  SSL *local_38;
  undefined local_30 [16];
  undefined local_20 [16];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  if (s->handshake_func == (_func_3149 *)0x0) {
    ERR_new();
    ERR_set_debug("ssl/ssl_lib.c",0xf42,"SSL_do_handshake");
    ERR_set_error(0x14,0x90,0);
    iVar1 = -1;
  }
  else {
    ossl_statem_check_finish_init();
    (*s->method->ssl_get_message)(s,0,stn,in_ECX,in_R8,in_R9);
    iVar1 = SSL_in_init(s);
    if (iVar1 == 0) {
      iVar1 = SSL_in_before(s);
      if (iVar1 == 0) {
        iVar1 = 1;
        goto LAB_00139d68;
      }
    }
    if ((*(byte *)((long)&s[3].tlsext_debug_arg + 1) & 1) != 0) {
      lVar2 = ASYNC_get_current_job();
      if (lVar2 == 0) {
        local_30 = (undefined  [16])0x0;
        local_20 = (undefined  [16])0x0;
        local_38 = s;
        iVar1 = ssl_start_async_job(0,s,&local_38,ssl_do_handshake_intern);
        goto LAB_00139d68;
      }
    }
    iVar1 = (*s->handshake_func)(s);
  }
LAB_00139d68:
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}