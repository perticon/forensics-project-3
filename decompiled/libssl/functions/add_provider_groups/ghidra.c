undefined8 add_provider_groups(undefined8 param_1,long *param_2)

{
  undefined (*pauVar1) [16];
  undefined8 *puVar2;
  long lVar3;
  int iVar4;
  void *pvVar5;
  long lVar6;
  long lVar7;
  char *pcVar8;
  ulong uVar9;
  undefined8 uVar10;
  undefined8 *puVar11;
  undefined8 uVar12;
  long in_FS_OFFSET;
  byte bVar13;
  uint local_38;
  uint local_34;
  long local_30;
  
  bVar13 = 0;
  puVar2 = (undefined8 *)*param_2;
  lVar3 = param_2[1];
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_34 = 0;
  lVar6 = puVar2[199];
  lVar7 = puVar2[0xc6];
  if (lVar6 == lVar7) {
    if (lVar6 == 0) {
      pvVar5 = CRYPTO_malloc(0x230,"ssl/t1_lib.c",0xf3);
    }
    else {
      pvVar5 = CRYPTO_realloc((void *)puVar2[0xc5],(int)lVar6 * 0x38 + 0x230,"ssl/t1_lib.c",0xf6);
    }
    if (pvVar5 == (void *)0x0) {
      ERR_new();
      uVar12 = 0;
      ERR_set_debug("ssl/t1_lib.c",0xfb,"add_provider_groups");
      ERR_set_error(0x14,0xc0100,0);
      goto LAB_00147416;
    }
    puVar2[0xc5] = pvVar5;
    puVar11 = (undefined8 *)((long)pvVar5 + puVar2[199] * 0x38);
    *puVar11 = 0;
    puVar11[0x45] = 0;
    uVar9 = (ulong)(((int)puVar11 - (int)(undefined8 *)((ulong)(puVar11 + 1) & 0xfffffffffffffff8))
                    + 0x230U >> 3);
    puVar11 = (undefined8 *)((ulong)(puVar11 + 1) & 0xfffffffffffffff8);
    for (; uVar9 != 0; uVar9 = uVar9 - 1) {
      *puVar11 = 0;
      puVar11 = puVar11 + (ulong)bVar13 * -2 + 1;
    }
    puVar2[199] = puVar2[199] + 10;
    lVar7 = puVar2[0xc6];
  }
  pauVar1 = (undefined (*) [16])(puVar2[0xc5] + lVar7 * 0x38);
  lVar6 = OSSL_PARAM_locate_const(param_1,"tls-group-name");
  if ((lVar6 == 0) || (*(int *)(lVar6 + 8) != 4)) {
    ERR_new();
    uVar12 = 0;
    ERR_set_debug("ssl/t1_lib.c",0x109,"add_provider_groups");
    ERR_set_error(0x14,0x80106,0);
    if (pauVar1 == (undefined (*) [16])0x0) goto LAB_00147416;
  }
  else {
    pcVar8 = CRYPTO_strdup(*(char **)(lVar6 + 0x10),"ssl/t1_lib.c",0x10c);
    *(char **)*pauVar1 = pcVar8;
    if (pcVar8 == (char *)0x0) {
      ERR_new();
      uVar10 = 0x10e;
LAB_001477b1:
      uVar12 = 0;
      ERR_set_debug("ssl/t1_lib.c",uVar10,"add_provider_groups");
      ERR_set_error(0x14,0xc0100,0);
    }
    else {
      lVar6 = OSSL_PARAM_locate_const(param_1,"tls-group-name-internal");
      if ((lVar6 == 0) || (*(int *)(lVar6 + 8) != 4)) {
        ERR_new();
        uVar10 = 0x114;
        goto LAB_00147685;
      }
      pcVar8 = CRYPTO_strdup(*(char **)(lVar6 + 0x10),"ssl/t1_lib.c",0x117);
      *(char **)(*pauVar1 + 8) = pcVar8;
      if (pcVar8 == (char *)0x0) {
        ERR_new();
        uVar10 = 0x119;
        goto LAB_001477b1;
      }
      lVar6 = OSSL_PARAM_locate_const(param_1,"tls-group-id");
      if (lVar6 == 0) {
LAB_00147780:
        ERR_new();
        uVar10 = 0x11f;
      }
      else {
        iVar4 = OSSL_PARAM_get_uint(lVar6,&local_38);
        if ((iVar4 == 0) || (0xffff < local_38)) goto LAB_00147780;
        *(short *)(pauVar1[1] + 0xc) = (short)local_38;
        lVar6 = OSSL_PARAM_locate_const(param_1,"tls-group-alg");
        if ((lVar6 == 0) || (*(int *)(lVar6 + 8) != 4)) {
          ERR_new();
          uVar10 = 0x126;
        }
        else {
          pcVar8 = CRYPTO_strdup(*(char **)(lVar6 + 0x10),"ssl/t1_lib.c",0x129);
          *(char **)pauVar1[1] = pcVar8;
          if (pcVar8 == (char *)0x0) {
            ERR_new();
            uVar10 = 299;
            goto LAB_001477b1;
          }
          lVar6 = OSSL_PARAM_locate_const(param_1,"tls-group-sec-bits");
          if (lVar6 != 0) {
            iVar4 = OSSL_PARAM_get_uint(lVar6,pauVar1[1] + 8);
            if (iVar4 != 0) {
              lVar6 = OSSL_PARAM_locate_const(param_1,"tls-group-is-kem");
              if (lVar6 != 0) {
                iVar4 = OSSL_PARAM_get_uint(lVar6,&local_34);
                if ((iVar4 == 0) || (1 < local_34)) {
                  ERR_new();
                  uVar10 = 0x137;
                  goto LAB_00147685;
                }
              }
              pauVar1[3][0] = (byte)local_34 & 1;
              lVar6 = OSSL_PARAM_locate_const(param_1,"tls-min-tls");
              if (lVar6 != 0) {
                iVar4 = OSSL_PARAM_get_int(lVar6,pauVar1[2]);
                if (iVar4 != 0) {
                  lVar6 = OSSL_PARAM_locate_const(param_1,"tls-max-tls");
                  if (lVar6 != 0) {
                    iVar4 = OSSL_PARAM_get_int(lVar6,pauVar1[2] + 4);
                    if (iVar4 != 0) {
                      lVar6 = OSSL_PARAM_locate_const(param_1,"tls-min-dtls");
                      if (lVar6 != 0) {
                        iVar4 = OSSL_PARAM_get_int(lVar6,pauVar1[2] + 8);
                        if (iVar4 != 0) {
                          lVar6 = OSSL_PARAM_locate_const(param_1,"tls-max-dtls");
                          if (lVar6 != 0) {
                            iVar4 = OSSL_PARAM_get_int(lVar6,pauVar1[2] + 0xc);
                            if (iVar4 != 0) {
                              ERR_set_mark();
                              lVar6 = EVP_KEYMGMT_fetch(*puVar2,*(undefined8 *)pauVar1[1],
                                                        puVar2[0x88]);
                              if (lVar6 != 0) {
                                lVar7 = EVP_KEYMGMT_get0_provider(lVar6);
                                if (lVar3 == lVar7) {
                                  puVar2[0xc6] = puVar2[0xc6] + 1;
                                  uVar12 = 1;
                                  EVP_KEYMGMT_free(lVar6);
                                  ERR_pop_to_mark();
                                  goto LAB_00147416;
                                }
                                EVP_KEYMGMT_free(lVar6);
                              }
                              ERR_pop_to_mark();
                              uVar12 = 1;
                              goto LAB_001475e0;
                            }
                          }
                          ERR_new();
                          uVar10 = 0x150;
                          goto LAB_00147685;
                        }
                      }
                      ERR_new();
                      uVar10 = 0x14a;
                      goto LAB_00147685;
                    }
                  }
                  ERR_new();
                  uVar10 = 0x144;
                  goto LAB_00147685;
                }
              }
              ERR_new();
              uVar10 = 0x13e;
              goto LAB_00147685;
            }
          }
          ERR_new();
          uVar10 = 0x131;
        }
      }
LAB_00147685:
      uVar12 = 0;
      ERR_set_debug("ssl/t1_lib.c",uVar10,"add_provider_groups");
      ERR_set_error(0x14,0x80106,0);
    }
  }
LAB_001475e0:
  CRYPTO_free(*(void **)*pauVar1);
  CRYPTO_free(*(void **)(*pauVar1 + 8));
  CRYPTO_free(*(void **)pauVar1[1]);
  *(undefined8 *)pauVar1[1] = 0;
  *pauVar1 = (undefined  [16])0x0;
LAB_00147416:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar12;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}