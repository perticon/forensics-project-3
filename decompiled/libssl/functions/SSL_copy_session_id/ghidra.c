void SSL_copy_session_id(SSL *to,SSL *from)

{
  ushort *puVar1;
  SSL_METHOD *pSVar2;
  COMP_CTX *pCVar3;
  int iVar4;
  SSL_SESSION *session;
  
  session = SSL_get_session(from);
  iVar4 = SSL_set_session(to,session);
  if (iVar4 == 0) {
    return;
  }
  if (to->method != from->method) {
    (*to->method->ssl_accept)(to);
    pSVar2 = from->method;
    to->method = pSVar2;
    iVar4 = (*pSVar2->ssl_clear)(to);
    if (iVar4 == 0) {
      return;
    }
  }
  LOCK();
  puVar1 = &(from[3].d1)->r_epoch;
  *(int *)puVar1 = *(int *)puVar1 + 1;
  ssl_cert_free(to[3].d1);
  pCVar3 = from[3].expand;
  to[3].d1 = from[3].d1;
  SSL_set_session_id_context(to,(uchar *)&from[3].enc_write_ctx,(uint)pCVar3);
  return;
}