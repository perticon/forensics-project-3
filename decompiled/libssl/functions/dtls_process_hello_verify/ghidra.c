undefined8 dtls_process_hello_verify(long param_1,long *param_2)

{
  byte bVar1;
  long lVar2;
  undefined8 *puVar3;
  long lVar4;
  ulong uVar5;
  ulong uVar6;
  undefined8 *puVar7;
  
  uVar5 = param_2[1];
  if (uVar5 < 2) {
LAB_0016ccf8:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x505,"dtls_process_hello_verify");
    ossl_statem_fatal(param_1,0x32,0x9f,0);
    return 0;
  }
  lVar2 = *param_2;
  *param_2 = lVar2 + 2;
  param_2[1] = uVar5 - 2;
  if (uVar5 - 2 == 0) goto LAB_0016ccf8;
  bVar1 = *(byte *)(lVar2 + 2);
  uVar6 = (ulong)bVar1;
  if (uVar5 - 3 < uVar6) goto LAB_0016ccf8;
  param_2[1] = (uVar5 - 3) - uVar6;
  puVar3 = *(undefined8 **)(param_1 + 0x4b8);
  *param_2 = lVar2 + 3 + uVar6;
  if (bVar1 < 8) {
    if ((bVar1 & 4) != 0) {
      *(undefined4 *)puVar3 = *(undefined4 *)(lVar2 + 3);
      *(undefined4 *)((long)puVar3 + (uVar6 - 4)) = *(undefined4 *)(lVar2 + -1 + uVar6);
      puVar3 = *(undefined8 **)(param_1 + 0x4b8);
      goto LAB_0016cce7;
    }
    if (bVar1 == 0) goto LAB_0016cce7;
    *(undefined *)puVar3 = *(undefined *)(lVar2 + 3);
    if ((bVar1 & 2) != 0) {
      *(undefined2 *)((long)puVar3 + (uVar6 - 2)) = *(undefined2 *)(lVar2 + 1 + uVar6);
      puVar3 = *(undefined8 **)(param_1 + 0x4b8);
      goto LAB_0016cce7;
    }
  }
  else {
    *puVar3 = *(undefined8 *)(lVar2 + 3);
    *(undefined8 *)((long)puVar3 + (uVar6 - 8)) = *(undefined8 *)(lVar2 + -5 + uVar6);
    lVar4 = (long)puVar3 - (long)(undefined8 *)((ulong)(puVar3 + 1) & 0xfffffffffffffff8);
    puVar7 = (undefined8 *)((lVar2 + 3) - lVar4);
    puVar3 = (undefined8 *)((ulong)(puVar3 + 1) & 0xfffffffffffffff8);
    for (uVar5 = (ulong)((int)lVar4 + (uint)bVar1 >> 3); uVar5 != 0; uVar5 = uVar5 - 1) {
      *puVar3 = *puVar7;
      puVar7 = puVar7 + 1;
      puVar3 = puVar3 + 1;
    }
  }
  puVar3 = *(undefined8 **)(param_1 + 0x4b8);
LAB_0016cce7:
  puVar3[0x20] = uVar6;
  return 1;
}