undefined4 dtls_construct_change_cipher_spec(int *param_1,undefined8 param_2)

{
  short *psVar1;
  long lVar2;
  int iVar3;
  
  if (*param_1 == 0x100) {
    lVar2 = *(long *)(param_1 + 0x12e);
    psVar1 = (short *)(lVar2 + 0x10e);
    *psVar1 = *psVar1 + 1;
    iVar3 = WPACKET_put_bytes__(param_2,*(undefined2 *)(lVar2 + 0x10c),2);
    if (iVar3 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_dtls.c",0x3a0,"dtls_construct_change_cipher_spec");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    }
  }
  return 1;
}