__owur int tls13_set_encoded_pub_key(EVP_PKEY *pkey,
                                     const unsigned char *enckey,
                                     size_t enckeylen)
{
    if (EVP_PKEY_is_a(pkey, "DH")) {
        int bits = EVP_PKEY_get_bits(pkey);

        if (bits <= 0 || enckeylen != (size_t)bits / 8)
            /* the encoded key must be padded to the length of the p */
            return 0;
    } else if (EVP_PKEY_is_a(pkey, "EC")) {
        if (enckeylen < 3 /* point format and at least 1 byte for x and y */
            || enckey[0] != 0x04)
            return 0;
    }

    return EVP_PKEY_set1_encoded_public_key(pkey, enckey, enckeylen);
}