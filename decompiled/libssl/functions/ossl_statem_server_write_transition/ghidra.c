undefined4 ossl_statem_server_write_transition(long param_1)

{
  undefined4 uVar1;
  uint uVar2;
  int iVar3;
  long lVar4;
  ulong uVar5;
  
  uVar1 = *(undefined4 *)(param_1 + 0x5c);
  if ((*(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) {
    switch(uVar1) {
    case 0:
    case 0x15:
    case 0x1a:
      goto LAB_0017b231;
    case 1:
      goto switchD_0017b257_caseD_1;
    default:
switchD_0017b257_caseD_2:
      ERR_new();
      ERR_set_debug("ssl/statem/statem_srvr.c",0x22f,"ossl_statem_server_write_transition");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    case 0x13:
      goto switchD_0017b257_caseD_13;
    case 0x14:
      if ((*(int *)(*(long *)(param_1 + 0x4b8) + 0x108) == 0) &&
         (uVar5 = SSL_get_options(param_1), (uVar5 & 0x2000) != 0)) {
        *(undefined4 *)(param_1 + 0x5c) = 0x15;
        return 1;
      }
switchD_0017b20f_caseD_14:
      if (((*(int *)(param_1 + 0xba0) != 0) || (*(long *)(param_1 + 0x240) == 0)) ||
         (*(long *)(param_1 + 0x2c8) == 0)) {
switchD_0017b279_caseD_14:
        *(undefined4 *)(param_1 + 0x5c) = 0x16;
        return 1;
      }
      goto switchD_0017b257_caseD_13;
    case 0x16:
switchD_0017b257_caseD_16:
      if (*(int *)(param_1 + 0x4d0) != 0) goto LAB_0017b303;
      lVar4 = *(long *)(param_1 + 0x2e0);
      if ((*(byte *)(lVar4 + 0x20) & 0x54) == 0) {
LAB_0017b4a0:
        *(undefined4 *)(param_1 + 0x5c) = 0x17;
        return 1;
      }
      goto LAB_0017b2b3;
    case 0x17:
switchD_0017b257_caseD_17:
      if (*(int *)(param_1 + 0xa74) != 0) {
        *(undefined4 *)(param_1 + 0x5c) = 0x22;
        return 1;
      }
    case 0x22:
switchD_0017b257_caseD_22:
      lVar4 = *(long *)(param_1 + 0x2e0);
LAB_0017b2b3:
      uVar2 = *(uint *)(lVar4 + 0x1c);
      if (((uVar2 & 6) != 0) ||
         ((((uVar2 & 0x48) != 0 && (*(long *)(*(long *)(param_1 + 0x898) + 0x200) != 0)) ||
          ((uVar2 & 0x1a0) != 0)))) {
        *(undefined4 *)(param_1 + 0x5c) = 0x18;
        return 1;
      }
switchD_0017b257_caseD_18:
      iVar3 = send_certificate_request(param_1);
      if (iVar3 == 0) {
switchD_0017b257_caseD_19:
        *(undefined4 *)(param_1 + 0x5c) = 0x1a;
        return 1;
      }
      break;
    case 0x18:
      goto switchD_0017b257_caseD_18;
    case 0x19:
      goto switchD_0017b257_caseD_19;
    case 0x20:
switchD_0017b257_caseD_20:
      if (*(int *)(param_1 + 0x4d0) != 0) goto switchD_0017b257_caseD_13;
LAB_0017b303:
      if (*(int *)(param_1 + 0xa98) != 0) {
LAB_0017b4e8:
        *(undefined4 *)(param_1 + 0x5c) = 0x21;
        return 1;
      }
switchD_0017b257_caseD_21:
      *(undefined4 *)(param_1 + 0x5c) = 0x23;
      return 1;
    case 0x21:
      goto switchD_0017b257_caseD_21;
    case 0x23:
      goto switchD_0017b257_caseD_23;
    case 0x24:
switchD_0017b257_caseD_24:
      if (*(int *)(param_1 + 0x4d0) != 0) {
        return 2;
      }
      goto switchD_0017b257_caseD_13;
    }
    goto LAB_0017b538;
  }
  iVar3 = **(int **)(param_1 + 8);
  if ((iVar3 == 0x10000) || (iVar3 < 0x304)) {
    switch(uVar1) {
    case 0:
    case 0x15:
    case 0x1a:
      goto LAB_0017b231;
    case 1:
      goto switchD_0017b257_caseD_1;
    default:
      goto switchD_0017b257_caseD_2;
    case 0x13:
      goto switchD_0017b257_caseD_13;
    case 0x14:
      goto switchD_0017b20f_caseD_14;
    case 0x16:
      goto switchD_0017b257_caseD_16;
    case 0x17:
      goto switchD_0017b257_caseD_17;
    case 0x18:
      goto switchD_0017b257_caseD_18;
    case 0x19:
      goto switchD_0017b257_caseD_19;
    case 0x20:
      goto switchD_0017b257_caseD_20;
    case 0x21:
      goto switchD_0017b257_caseD_21;
    case 0x22:
      goto switchD_0017b257_caseD_22;
    case 0x23:
      goto switchD_0017b257_caseD_23;
    case 0x24:
      goto switchD_0017b257_caseD_24;
    }
  }
  switch(uVar1) {
  default:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_srvr.c",0x1a8,"ossl_statem_server13_write_transition");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  case 1:
    if (*(int *)(param_1 + 0xba4) != -1) {
      *(undefined4 *)(param_1 + 0x5c) = 0x2a;
      return uVar1;
    }
    if (*(int *)(param_1 + 0xba8) != 3) {
      if (*(int *)(param_1 + 0xa9c) < 1) {
        return 2;
      }
      *(undefined4 *)(param_1 + 0x5c) = 0x21;
      return uVar1;
    }
    goto LAB_0017b538;
  case 0x14:
    goto switchD_0017b279_caseD_14;
  case 0x16:
    iVar3 = *(int *)(param_1 + 0x8e8);
    if ((*(byte *)(param_1 + 0x9ea) & 0x10) != 0) {
      if (iVar3 != 2) goto switchD_0017b257_caseD_21;
      goto LAB_0017b45c;
    }
    break;
  case 0x17:
    *(undefined4 *)(param_1 + 0x5c) = 0x28;
    return 1;
  case 0x19:
    if (*(int *)(param_1 + 0xba8) == 3) {
      *(undefined4 *)(param_1 + 0xba8) = 4;
      *(undefined4 *)(param_1 + 0x5c) = 1;
      return 1;
    }
    goto LAB_0017b4a0;
  case 0x20:
    if (*(int *)(param_1 + 0xba8) == 4) {
      *(undefined4 *)(param_1 + 0xba8) = 2;
    }
    else if (*(int *)(param_1 + 0xa98) == 0) goto switchD_0017b257_caseD_13;
    if (*(ulong *)(param_1 + 0x1d70) < *(ulong *)(param_1 + 0x1d78) ||
        *(ulong *)(param_1 + 0x1d70) == *(ulong *)(param_1 + 0x1d78))
    goto switchD_0017b257_caseD_13;
    goto LAB_0017b4e8;
  case 0x21:
    if (((*(long *)(param_1 + 0x240) != 0) && (*(long *)(param_1 + 0x2c8) != 0)) &&
       (0 < *(int *)(param_1 + 0xa9c))) {
      return 1;
    }
    if ((*(int *)(param_1 + 0x4d0) == 0) &&
       (*(ulong *)(param_1 + 0x1d78) <= *(ulong *)(param_1 + 0x1d70) &&
        *(ulong *)(param_1 + 0x1d70) != *(ulong *)(param_1 + 0x1d78))) {
      return 1;
    }
  case 0x2a:
  case 0x2c:
switchD_0017b257_caseD_13:
    *(undefined4 *)(param_1 + 0x5c) = 1;
    return 1;
  case 0x23:
    iVar3 = *(int *)(param_1 + 0x8e8);
    break;
  case 0x24:
    goto switchD_0017b279_caseD_24;
  case 0x25:
    if (*(int *)(param_1 + 0x4d0) != 0) goto switchD_0017b257_caseD_23;
    iVar3 = send_certificate_request();
    if (iVar3 == 0) goto LAB_0017b4a0;
LAB_0017b538:
    *(undefined4 *)(param_1 + 0x5c) = 0x19;
    return 1;
  case 0x28:
switchD_0017b257_caseD_23:
    *(undefined4 *)(param_1 + 0x5c) = 0x24;
    return 1;
  case 0x2e:
    goto LAB_0017b231;
  }
  if (iVar3 != 1) {
LAB_0017b45c:
    *(undefined4 *)(param_1 + 0x5c) = 0x25;
    return 1;
  }
switchD_0017b279_caseD_24:
  *(undefined4 *)(param_1 + 0x5c) = 0x2e;
  return 1;
switchD_0017b257_caseD_1:
  if (*(int *)(param_1 + 0x60) == 0x13) {
    *(undefined8 *)(param_1 + 0x5c) = 0x13;
    return 1;
  }
  iVar3 = tls_setup_handshake(param_1);
  if (iVar3 == 0) {
    return 0;
  }
LAB_0017b231:
  return 2;
}