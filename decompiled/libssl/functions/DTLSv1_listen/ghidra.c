undefined4 DTLSv1_listen(SSL *param_1,void *param_2)

{
  byte bVar1;
  uint uVar2;
  char *data;
  code *pcVar3;
  X509_EXTENSIONS *pXVar4;
  int iVar5;
  int iVar6;
  undefined4 uVar7;
  BIO *b;
  BIO *bp;
  ulong uVar8;
  int *piVar9;
  ulong uVar10;
  long lVar11;
  void *parg;
  uint uVar12;
  ushort uVar13;
  ulong uVar14;
  undefined8 extraout_RDX;
  char *data_00;
  ulong uVar15;
  uint uVar16;
  BIO *pBVar17;
  long in_FS_OFFSET;
  undefined8 uVar18;
  uint local_194;
  int local_190 [2];
  undefined local_188 [56];
  undefined8 local_150;
  undefined local_148 [264];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1->handshake_func == (_func_3149 *)0x0) {
    SSL_set_accept_state(param_1);
  }
  iVar5 = SSL_clear(param_1);
  if (iVar5 != 0) {
    ERR_clear_error();
    b = SSL_get_rbio(param_1);
    bp = SSL_get_wbio(param_1);
    if ((b == (BIO *)0x0) || (bp == (BIO *)0x0)) {
      ERR_new();
      ERR_set_debug("ssl/d1_lib.c",0x1c0,"DTLSv1_listen");
      ERR_set_error(0x14,0x80,0);
      uVar7 = 0xffffffff;
      goto LAB_00124b5a;
    }
    if ((param_1->version & 0xff00U) != 0xfe00) {
      ERR_new();
      ERR_set_debug("ssl/d1_lib.c",0x1cc,"DTLSv1_listen");
      ERR_set_error(0x14,0x103,0);
      uVar7 = 0xffffffff;
      goto LAB_00124b5a;
    }
    iVar5 = ssl3_setup_buffers(param_1);
    if (iVar5 != 0) {
      data = param_1[4].tlsext_hostname;
      uVar8 = (ulong)(-(int)param_1[4].mode - 5U & 7);
      data_00 = (char *)(param_1[4].mode + uVar8);
      piVar9 = __errno_location();
      do {
        *piVar9 = 0;
        uVar18 = 0x124c33;
        pBVar17 = b;
        iVar5 = BIO_read(b,data_00,0x400d);
        if (iVar5 < 1) {
          iVar5 = BIO_test_flags(b,8);
          if (iVar5 == 0) goto LAB_00124b55;
          uVar7 = 0;
          goto LAB_00124f62;
        }
        if (iVar5 < 0xd) {
          ERR_new();
          ERR_set_debug("ssl/d1_lib.c",0x202,"DTLSv1_listen");
          ERR_set_error(0x14,0x12a,0);
          uVar7 = 0;
          goto LAB_00124f62;
        }
        if ((code *)param_1[1].tlsext_ocsp_resp != (code *)0x0) {
          pBVar17 = (BIO *)0x0;
          (*(code *)param_1[1].tlsext_ocsp_resp)
                    (0,0,0x100,data_00,0xd,param_1,*(undefined8 *)&param_1[1].tlsext_ocsp_resplen,
                     uVar18);
        }
        if (iVar5 == 1) {
          ERR_new();
          uVar18 = 0x20d;
LAB_00124f41:
          ERR_set_debug("ssl/d1_lib.c",uVar18,"DTLSv1_listen");
          ERR_set_error(0x14,0x9f,0);
          uVar7 = 0;
          goto LAB_00124f62;
        }
        if (*data_00 != '\x16') {
          ERR_new();
          uVar18 = 0x212;
          goto LAB_0012508f;
        }
        if (data_00[1] != -2) {
          ERR_new();
          ERR_set_debug("ssl/d1_lib.c",0x21b,"DTLSv1_listen");
          ERR_set_error(0x14,0x74,0);
          uVar7 = 0;
          goto LAB_00124f62;
        }
        local_150 = *(undefined8 *)(data_00 + 3);
        if ((long)iVar5 - 0xbU < 2) {
LAB_00124f30:
          ERR_new();
          uVar18 = 0x223;
          goto LAB_00124f41;
        }
        uVar13 = *(ushort *)(data_00 + 0xb) << 8 | *(ushort *)(data_00 + 0xb) >> 8;
        uVar15 = (ulong)uVar13;
        if ((long)iVar5 - 0xdU < uVar15) goto LAB_00124f30;
        if ((byte)((byte)((ulong)local_150 >> 8) | (byte)local_150) != 0) {
          ERR_new();
          uVar18 = 0x22e;
LAB_0012508f:
          ERR_set_debug("ssl/d1_lib.c",uVar18,"DTLSv1_listen");
          ERR_set_error(0x14,0xf4,0);
          uVar7 = 0;
          goto LAB_00124f62;
        }
        if (uVar13 < 0xc) {
LAB_00124fde:
          ERR_new();
          uVar18 = 0x23d;
          goto LAB_00124f41;
        }
        uVar10 = (ulong)(byte)data_00[0x16] << 0x10 | (ulong)(byte)data_00[0x17] << 8 |
                 (ulong)(byte)data_00[0x18];
        if (uVar10 != uVar15 - 0xc) goto LAB_00124fde;
        if (data_00[0xd] != '\x01') {
          ERR_new();
          uVar18 = 0x242;
          goto LAB_0012508f;
        }
        if (2 < (ushort)(*(ushort *)(data_00 + 0x11) << 8 | *(ushort *)(data_00 + 0x11) >> 8)) {
          ERR_new();
          ERR_set_debug("ssl/d1_lib.c",0x248,"DTLSv1_listen");
          ERR_set_error(0x14,0x192,0);
          uVar7 = 0;
          goto LAB_00124f62;
        }
        if (((ulong)(byte)data_00[0x13] << 0x10 | (ulong)(byte)data_00[0x14] << 8 |
            (ulong)(byte)data_00[0x15]) != 0) {
LAB_001254f7:
          ERR_new();
          ERR_set_debug("ssl/d1_lib.c",0x255,"DTLSv1_listen");
          ERR_set_error(0x14,0x191,0);
          uVar7 = 0;
          goto LAB_00124f62;
        }
        if (((ulong)(byte)data_00[0xe] << 0x10 | (ulong)(byte)data_00[0xf] << 8 |
            (ulong)(byte)data_00[0x10]) < uVar10) goto LAB_001254f7;
        if ((code *)param_1[1].tlsext_ocsp_resp != (code *)0x0) {
          (*(code *)param_1[1].tlsext_ocsp_resp)
                    (0,param_1->version,0x16,data_00 + 0xd,uVar10 + 0xc,param_1,
                     *(undefined8 *)&param_1[1].tlsext_ocsp_resplen,pBVar17);
        }
        if (uVar10 < 2) {
          ERR_new();
          uVar18 = 0x25f;
          goto LAB_00124f41;
        }
        uVar13 = *(ushort *)(data_00 + 0x19) << 8 | *(ushort *)(data_00 + 0x19) >> 8;
        uVar2 = param_1->method->version;
        uVar16 = (uint)uVar13;
        if (uVar13 == 0x100) {
          if (uVar2 != 0x100) {
            uVar16 = 0xff00;
            uVar12 = uVar2;
            goto LAB_00124dd9;
          }
        }
        else {
          uVar12 = 0xff00;
          if (uVar2 != 0x100) {
            uVar12 = uVar2;
          }
LAB_00124dd9:
          if ((uVar12 < uVar16) && (uVar2 != 0x1ffff)) {
            ERR_new();
            ERR_set_debug("ssl/d1_lib.c",0x268,"DTLSv1_listen");
            ERR_set_error(0x14,0x10b,0);
            uVar7 = 0;
            goto LAB_00124f62;
          }
        }
        if (uVar10 - 2 < 0x21) {
LAB_001250b5:
          ERR_new();
          uVar18 = 0x273;
          goto LAB_00124f41;
        }
        uVar14 = (ulong)(byte)data_00[0x3b];
        if ((uVar10 - 0x23 < uVar14) || (lVar11 = (uVar10 - 0x23) - uVar14, lVar11 == 0))
        goto LAB_001250b5;
        bVar1 = data_00[uVar14 + 0x3c];
        if (lVar11 - 1U < (ulong)bVar1) goto LAB_001250b5;
        if ((ulong)bVar1 != 0) {
          pcVar3 = *(code **)(*(long *)&param_1[3].ex_data.dummy + 0xd8);
          if (pcVar3 == (code *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/d1_lib.c",0x282,"DTLSv1_listen");
            ERR_set_error(0x14,0x193,0);
            uVar7 = 0xffffffff;
            goto LAB_00124b5a;
          }
          iVar5 = (*pcVar3)(param_1,data_00 + uVar14 + 0x3c + 1,bVar1);
          if (iVar5 != 0) {
            pXVar4 = param_1[1].tlsext_ocsp_exts;
            *(undefined4 *)((long)&pXVar4[8].stack.data + 4) = 0x10001;
            *(undefined2 *)&pXVar4[8].stack.sorted = 1;
            DTLS_RECORD_LAYER_set_write_sequence(&param_1[4].ex_data.dummy,&local_150);
            SSL_set_options(param_1,0x2000);
            ossl_statem_set_hello_verify_done(param_1);
            lVar11 = BIO_ctrl(b,0x2e,0,param_2);
            if ((int)lVar11 < 1) {
              BIO_ADDR_clear(param_2);
            }
            iVar5 = dtls_buffer_listen_record(param_1,uVar15,&local_150,uVar8);
            if (iVar5 == 0) goto LAB_00124b55;
            uVar7 = 1;
            goto LAB_00124f62;
          }
        }
        pcVar3 = *(code **)(*(long *)&param_1[3].ex_data.dummy + 0xd0);
        if (pcVar3 == (code *)0x0) {
LAB_0012542c:
          ERR_new();
          ERR_set_debug("ssl/d1_lib.c",0x2a2,"DTLSv1_listen");
          ERR_set_error(0x14,400,0);
          goto LAB_00124b55;
        }
        iVar5 = (*pcVar3)(param_1,local_148,&local_194);
        if ((iVar5 == 0) || (0xff < local_194)) goto LAB_0012542c;
        if (param_1->method->version == 0x1ffff) {
          iVar5 = 0xfeff;
        }
        else {
          iVar5 = param_1->version;
        }
        iVar6 = ssl_get_max_send_fragment(param_1);
        iVar6 = WPACKET_init_static_len(local_188,data,iVar6 + 0xd,0);
        if (iVar6 == 0) {
LAB_001253dc:
          ERR_new();
          ERR_set_debug("ssl/d1_lib.c",0x2e1,"DTLSv1_listen");
          ERR_set_error(0x14,0xc0103,0);
          WPACKET_cleanup(local_188);
          goto LAB_00124b55;
        }
        iVar6 = WPACKET_put_bytes__(local_188,0x16,1);
        if (iVar6 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_put_bytes__(local_188,iVar5,2);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_memcpy(local_188,&local_150,8);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_start_sub_packet_len__(local_188,2);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_put_bytes__(local_188,3,1);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_put_bytes__(local_188,0,3);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_put_bytes__(local_188,0,2);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_put_bytes__(local_188,0,3);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_start_sub_packet_len__(local_188,3);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = dtls_raw_hello_verify_request(local_188,local_148,local_194);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_close(local_188);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_close(local_188);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_get_total_written(local_188,local_190);
        if (iVar5 == 0) goto LAB_001253dc;
        iVar5 = WPACKET_finish(local_188);
        if (iVar5 == 0) goto LAB_001253dc;
        *(undefined2 *)(data + 0xe) = *(undefined2 *)(data + 0x16);
        data[0x10] = data[0x18];
        if ((code *)param_1[1].tlsext_ocsp_resp != (code *)0x0) {
          (*(code *)param_1[1].tlsext_ocsp_resp)
                    (1,0,0x100,data_00,0xd,param_1,*(undefined8 *)&param_1[1].tlsext_ocsp_resplen,
                     extraout_RDX);
        }
        parg = (void *)BIO_ADDR_new();
        if (parg == (void *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/d1_lib.c",0x2f7,"DTLSv1_listen");
          ERR_set_error(0x14,0xc0100,0);
          uVar7 = 0;
          goto LAB_00124f62;
        }
        lVar11 = BIO_ctrl(b,0x2e,0,parg);
        if (0 < (int)lVar11) {
          BIO_ctrl(bp,0x2c,0,parg);
        }
        BIO_ADDR_free(parg);
        iVar5 = BIO_write(bp,data,local_190[0]);
        if (iVar5 < local_190[0]) break;
        lVar11 = BIO_ctrl(bp,0xb,0,(void *)0x0);
      } while (0 < (int)lVar11);
      iVar5 = BIO_test_flags(bp,8);
      if (iVar5 != 0) {
        uVar7 = 0;
LAB_00124f62:
        BIO_ADDR_free(0);
        goto LAB_00124b5a;
      }
    }
  }
LAB_00124b55:
  uVar7 = 0xffffffff;
LAB_00124b5a:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}