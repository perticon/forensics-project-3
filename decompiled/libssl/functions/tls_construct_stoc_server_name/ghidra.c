undefined8 tls_construct_stoc_server_name(long param_1,undefined8 param_2)

{
  int iVar1;
  
  if (*(int *)(param_1 + 0xb60) != 1) {
    return 2;
  }
  if (*(int *)(param_1 + 0x4d0) != 0) {
    if ((*(byte *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8) != 0) {
      return 2;
    }
    iVar1 = **(int **)(param_1 + 8);
    if ((iVar1 < 0x304) || (iVar1 == 0x10000)) {
      return 2;
    }
  }
  iVar1 = WPACKET_put_bytes__(param_2,0,2);
  if ((iVar1 != 0) && (iVar1 = WPACKET_put_bytes__(param_2,0,2), iVar1 != 0)) {
    return 1;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_srvr.c",0x4f6,"tls_construct_stoc_server_name");
  ossl_statem_fatal(param_1,0x50,0xc0103,0);
  return 0;
}