undefined4 tls_parse_stoc_session_ticket(long param_1,undefined8 *param_2)

{
  int iVar1;
  
  if (*(code **)(param_1 + 0xae8) != (code *)0x0) {
    iVar1 = (**(code **)(param_1 + 0xae8))
                      (param_1,*param_2,*(undefined4 *)(param_2 + 1),
                       *(undefined8 *)(param_1 + 0xaf0));
    if (iVar1 == 0) {
      ERR_new();
      ERR_set_debug("ssl/statem/extensions_clnt.c",0x567,"tls_parse_stoc_session_ticket");
      ossl_statem_fatal(param_1,0x28,0x6e,0);
      return 0;
    }
  }
  iVar1 = tls_use_ticket(param_1);
  if (iVar1 != 0) {
    if (param_2[1] == 0) {
      *(undefined4 *)(param_1 + 0xa98) = 1;
      return 1;
    }
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_clnt.c",0x570,"tls_parse_stoc_session_ticket");
    ossl_statem_fatal(param_1,0x32,0x6e,0);
    return 0;
  }
  ERR_new();
  ERR_set_debug("ssl/statem/extensions_clnt.c",0x56c,"tls_parse_stoc_session_ticket");
  ossl_statem_fatal(param_1,0x6e,0x6e,0);
  return 0;
}