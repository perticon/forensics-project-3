char * SSL_get_shared_ciphers(SSL *s,char *buf,int len)

{
  void *pvVar1;
  int iVar2;
  int iVar3;
  stack_st_SSL_CIPHER *psVar4;
  long lVar5;
  size_t sVar6;
  char *pcVar7;
  char *__dest;
  
  if ((((s->server != 0) && (pvVar1 = s[1].tls_session_secret_cb_arg, 1 < len)) &&
      (pvVar1 != (void *)0x0)) &&
     (((psVar4 = SSL_get_ciphers(s), psVar4 != (stack_st_SSL_CIPHER *)0x0 &&
       (iVar2 = OPENSSL_sk_num(pvVar1), iVar2 != 0)) && (iVar2 = OPENSSL_sk_num(psVar4), iVar2 != 0)
      ))) {
    iVar2 = 0;
    __dest = buf;
    do {
      iVar3 = OPENSSL_sk_num(pvVar1);
      if (iVar3 <= iVar2) {
        __dest[-1] = '\0';
        return buf;
      }
      lVar5 = OPENSSL_sk_value(pvVar1,iVar2);
      iVar3 = OPENSSL_sk_find(psVar4);
      if (-1 < iVar3) {
        pcVar7 = *(char **)(lVar5 + 8);
        sVar6 = strlen(pcVar7);
        iVar3 = (int)sVar6;
        if (len <= iVar3) {
          pcVar7 = __dest + -1;
          if (__dest == buf) {
            pcVar7 = buf;
          }
          *pcVar7 = '\0';
          return buf;
        }
        memcpy(__dest,pcVar7,sVar6 + 1);
        len = len - (iVar3 + 1);
        __dest[iVar3] = ':';
        __dest = __dest + iVar3 + 1;
      }
      iVar2 = iVar2 + 1;
    } while( true );
  }
  return (char *)0x0;
}