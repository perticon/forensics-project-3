undefined8 tls_construct_stoc_supported_groups(SSL *param_1,undefined8 param_2)

{
  short sVar1;
  bool bVar2;
  int iVar3;
  int iVar4;
  undefined8 uVar5;
  ulong uVar6;
  long in_FS_OFFSET;
  long local_50;
  ulong local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(short *)&param_1[1].field_0x1fe == 0) {
LAB_00168408:
    uVar5 = 2;
  }
  else {
    tls1_get_supported_groups(param_1,&local_50,&local_48);
    if (local_48 == 0) {
      ERR_new();
      uVar5 = 0x53d;
    }
    else {
      iVar3 = SSL_version(param_1);
      if (local_48 != 0) {
        bVar2 = true;
        uVar6 = 0;
        do {
          while( true ) {
            sVar1 = *(short *)(local_50 + uVar6 * 2);
            iVar4 = tls_valid_group(param_1,sVar1,iVar3,iVar3,0,0);
            if ((iVar4 != 0) && (iVar4 = tls_group_allowed(param_1,sVar1,0x20004), iVar4 != 0))
            break;
            uVar6 = uVar6 + 1;
            if (local_48 <= uVar6) goto LAB_00168390;
          }
          if (bVar2) {
            if (*(short *)&param_1[1].field_0x1fe == sVar1) goto LAB_00168408;
            iVar4 = WPACKET_put_bytes__(param_2,10,2);
            if (((iVar4 == 0) || (iVar4 = WPACKET_start_sub_packet_len__(param_2,2), iVar4 == 0)) ||
               (iVar4 = WPACKET_start_sub_packet_len__(param_2), iVar4 == 0)) {
              ERR_new();
              uVar5 = 0x555;
              goto LAB_001683c1;
            }
          }
          iVar4 = WPACKET_put_bytes__(param_2,sVar1,2);
          if (iVar4 == 0) {
            ERR_new();
            uVar5 = 0x55c;
            goto LAB_001683c1;
          }
          bVar2 = false;
          uVar6 = uVar6 + 1;
        } while (uVar6 < local_48);
      }
LAB_00168390:
      iVar3 = WPACKET_close(param_2);
      if ((iVar3 != 0) && (iVar3 = WPACKET_close(param_2), iVar3 != 0)) {
        uVar5 = 1;
        goto LAB_001683e5;
      }
      ERR_new();
      uVar5 = 0x563;
    }
LAB_001683c1:
    ERR_set_debug("ssl/statem/extensions_srvr.c",uVar5,"tls_construct_stoc_supported_groups");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    uVar5 = 0;
  }
LAB_001683e5:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
}