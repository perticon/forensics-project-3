EVP_PKEY * ssl_dh_to_pkey(dh_st *param_1)

{
  int iVar1;
  EVP_PKEY *pkey;
  EVP_PKEY *pEVar2;
  
  if (param_1 != (dh_st *)0x0) {
    pkey = EVP_PKEY_new();
    iVar1 = EVP_PKEY_set1_DH(pkey,param_1);
    pEVar2 = pkey;
    if (iVar1 < 1) {
      pEVar2 = (EVP_PKEY *)0x0;
      EVP_PKEY_free(pkey);
    }
    return pEVar2;
  }
  return (EVP_PKEY *)0x0;
}