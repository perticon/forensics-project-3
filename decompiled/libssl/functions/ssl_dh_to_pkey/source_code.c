EVP_PKEY *ssl_dh_to_pkey(DH *dh)
{
# ifndef OPENSSL_NO_DH
    EVP_PKEY *ret;

    if (dh == NULL)
        return NULL;
    ret = EVP_PKEY_new();
    if (EVP_PKEY_set1_DH(ret, dh) <= 0) {
        EVP_PKEY_free(ret);
        return NULL;
    }
    return ret;
# else
    return NULL;
# endif
}