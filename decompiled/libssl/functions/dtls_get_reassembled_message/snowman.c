int64_t dtls_get_reassembled_message(void** rdi, void** rsi, void*** rdx) {
    void** r13_4;
    void* rsp5;
    void*** v6;
    void** rbp7;
    void** rdx8;
    void** v9;
    uint64_t rax10;
    uint64_t v11;
    void** rdi12;
    void** rax13;
    void* rsp14;
    void** r15_15;
    uint32_t ecx16;
    uint32_t edx17;
    void** rdi18;
    void** rax19;
    int32_t eax20;
    void* rsp21;
    int64_t rax22;
    int32_t v23;
    signed char v24;
    void** rdi25;
    void** rax26;
    void* v27;
    void** rdx28;
    int64_t v29;
    void** r15_30;
    void** rsi31;
    struct s14* rdi32;
    void** rax33;
    void** v34;
    uint32_t eax35;
    uint16_t v36;
    void** r12_37;
    void** v38;
    int64_t v39;
    uint64_t v40;
    void** rdi41;
    void** rbp42;
    struct s15* rax43;
    void** rdx44;
    struct s15* v45;
    uint32_t eax46;
    uint16_t v47;
    uint32_t edx48;
    void** v49;
    signed char v50;
    void** v51;
    void** eax52;
    void** rax53;
    void** rax54;
    void** r14_55;
    void** rax56;
    int32_t eax57;
    uint64_t v58;
    void** v59;
    signed char v60;
    void** rdi61;
    void** rbx62;
    void** rsi63;
    int32_t eax64;
    void** rdi65;
    uint64_t rbx66;
    int32_t ecx67;
    void** rdi68;
    void** v69;
    void** rax70;
    int32_t eax71;
    void** v72;
    void** rax73;
    void** rdi74;
    int64_t rax75;
    int32_t eax76;
    void** rax77;
    int32_t eax78;
    void** v79;
    uint32_t eax80;
    unsigned char v81;
    unsigned char v82;
    unsigned char v83;
    void** rax84;

    r13_4 = rdi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1c8);
    v6 = rdx;
    rbp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 0xa4);
    rdx8 = *reinterpret_cast<void***>(rdi + 0x4b8);
    v9 = rsi;
    rax10 = g28;
    v11 = rax10;
    *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
    while (rdi12 = *reinterpret_cast<void***>(rdx8 + 0x118), rax13 = pqueue_peek(rdi12, rsi, rdx8), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8), !!rax13) {
        r15_15 = *reinterpret_cast<void***>(rax13 + 8);
        ecx16 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(r15_15 + 16));
        edx17 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x4b8) + 0x110));
        if (*reinterpret_cast<uint16_t*>(&ecx16) >= *reinterpret_cast<uint16_t*>(&edx17)) 
            goto addr_734d0_4;
        rdi18 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x4b8) + 0x118);
        pqueue_pop(rdi18, rsi);
        dtls1_hm_fragment_free(r15_15, rsi);
        pitem_free(rax13, rsi);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8 - 8 + 8);
        rdx8 = *reinterpret_cast<void***>(r13_4 + 0x4b8);
    }
    goto addr_73377_6;
    addr_734d0_4:
    if (*reinterpret_cast<void***>(r15_15 + 96) || *reinterpret_cast<uint16_t*>(&ecx16) != *reinterpret_cast<uint16_t*>(&edx17)) {
        addr_73377_6:
        rax19 = *reinterpret_cast<void***>(r13_4 + 8);
        eax20 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(rax19 + 0x68)(r13_4, 22, reinterpret_cast<int64_t>(rsp14) + 44, rbp7, 12));
        rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 - 8 - 8 + 8 + 8 + 8);
        if (reinterpret_cast<uint1_t>(eax20 < 0) | reinterpret_cast<uint1_t>(eax20 == 0)) {
            addr_736b0_7:
            *reinterpret_cast<void***>(r13_4 + 40) = reinterpret_cast<void**>(3);
            *v6 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax22) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
        } else {
            if (v23 == 20) {
                if (v24 == 1) {
                    rdi25 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x88) + 8);
                    fun_20b20(rdi25, rbp7, rdi25, rbp7);
                    rax26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v27) - 1);
                    rdx28 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x88) + 8);
                    *reinterpret_cast<void***>(r13_4 + 0x98) = rax26;
                    *reinterpret_cast<void***>(r13_4 + 0x2d0) = rax26;
                    *reinterpret_cast<void***>(r13_4 + 0x2d8) = reinterpret_cast<void**>(0x101);
                    *reinterpret_cast<void***>(r13_4 + 0x90) = rdx28 + 1;
                    *v6 = rax26;
                    *reinterpret_cast<int32_t*>(&rax22) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
                } else {
                    fun_20230();
                    fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
                    ossl_statem_fatal(r13_4, r13_4);
                    goto addr_735eb_12;
                }
            } else {
                if (v29 != 12) {
                    fun_20230();
                    goto addr_73681_15;
                } else {
                    r15_30 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 64);
                    rsi31 = r15_30;
                    dtls1_get_message_header(rbp7, rsi31, rbp7, rsi31);
                    rdi32 = reinterpret_cast<struct s14*>(r13_4 + 0xc58);
                    rax33 = RECORD_LAYER_get_rrec_length(rdi32, rdi32);
                    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 - 8 + 8);
                    if (reinterpret_cast<unsigned char>(v34) > reinterpret_cast<unsigned char>(rax33)) {
                        fun_20230();
                        goto addr_735c9_18;
                    } else {
                        eax35 = v36;
                        __asm__("rol cx, 0x8");
                        if (*reinterpret_cast<void***>(&eax35) != *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x4b8) + 0x110)) {
                            r12_37 = v38;
                            if (reinterpret_cast<uint64_t>(v39 + reinterpret_cast<unsigned char>(r12_37)) <= v40) {
                                rdi41 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x4b8) + 0x118);
                                rbp42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 0x9c);
                                rsi31 = rbp42;
                                rax43 = pqueue_find(rdi41, rsi31, rdi41, rsi31);
                                rdx44 = *reinterpret_cast<void***>(r13_4 + 0x4b8);
                                v45 = rax43;
                                eax46 = v47;
                                edx48 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rdx44 + 0x110));
                                if (!rax43 || r12_37 != v49) {
                                    if (*reinterpret_cast<uint16_t*>(&eax46) <= *reinterpret_cast<uint16_t*>(&edx48) || (reinterpret_cast<int32_t>(eax46) > reinterpret_cast<int32_t>(*reinterpret_cast<uint16_t*>(&edx48) + 10) || !*reinterpret_cast<uint16_t*>(&edx48) && v50 == 20)) {
                                        v45 = reinterpret_cast<struct s15*>(0);
                                        goto addr_737e3_24;
                                    } else {
                                        if (r12_37 != v51) {
                                            addr_738d2_26:
                                            eax52 = dtls1_reassemble_fragment(r13_4, r15_30);
                                            goto addr_73761_27;
                                        } else {
                                            *reinterpret_cast<int32_t*>(&rax53) = 0x454c;
                                            *reinterpret_cast<int32_t*>(&rax53 + 4) = 0;
                                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_4 + 0xa00)) >= reinterpret_cast<unsigned char>(0x454c)) {
                                                rax53 = *reinterpret_cast<void***>(r13_4 + 0xa00);
                                            }
                                            if (reinterpret_cast<unsigned char>(r12_37) > reinterpret_cast<unsigned char>(rax53)) 
                                                goto addr_73754_31;
                                            *reinterpret_cast<int32_t*>(&rsi31) = 0;
                                            *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
                                            rax54 = dtls1_hm_fragment_new(r12_37, 0, r12_37, 0);
                                            r14_55 = rax54;
                                            if (!rax54) 
                                                goto addr_73754_31; else 
                                                goto addr_73930_33;
                                        }
                                    }
                                } else {
                                    addr_737e3_24:
                                    if (r12_37) {
                                        do {
                                            rax56 = *reinterpret_cast<void***>(r13_4 + 8);
                                            if (reinterpret_cast<unsigned char>(r12_37) <= reinterpret_cast<unsigned char>(0x100)) {
                                            }
                                            *reinterpret_cast<int32_t*>(&rsi31) = 22;
                                            *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
                                            eax57 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(rax56 + 0x68)(r13_4, 22));
                                            if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax57 < 0) | reinterpret_cast<uint1_t>(eax57 == 0))) 
                                                goto addr_7383b_37;
                                            r12_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_37) - v58);
                                        } while (r12_37);
                                        goto addr_739ae_39;
                                    } else {
                                        goto addr_739ae_39;
                                    }
                                }
                            } else {
                                addr_73754_31:
                                *reinterpret_cast<int32_t*>(&r14_55) = 0;
                                *reinterpret_cast<int32_t*>(&r14_55 + 4) = 0;
                                goto addr_73757_41;
                            }
                        } else {
                            if (reinterpret_cast<unsigned char>(v34) >= reinterpret_cast<unsigned char>(v59)) 
                                goto addr_73428_43;
                            if (v34) 
                                goto addr_738d2_26;
                            addr_73428_43:
                            if (*reinterpret_cast<void***>(r13_4 + 56)) 
                                goto addr_73540_45;
                            if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x4b8) + 0x1a8)) 
                                goto addr_73540_45;
                            if (*reinterpret_cast<uint32_t*>(r13_4 + 92) == 1) 
                                goto addr_73540_45;
                            if (v60) 
                                goto addr_73540_45; else 
                                goto addr_7345c_49;
                        }
                    }
                }
            }
        }
    } else {
        rdi61 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x4b8) + 0x118);
        rbx62 = *reinterpret_cast<void***>(r15_15 + 32);
        pqueue_pop(rdi61, rsi);
        rsi63 = r15_15;
        eax64 = dtls1_preprocess_fragment(r13_4, rsi63);
        if (!eax64) {
            dtls1_hm_fragment_free(r15_15, rsi63);
            pitem_free(rax13, rsi63);
            *reinterpret_cast<int32_t*>(&rax22) = eax64;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
            *reinterpret_cast<void***>(r13_4 + 0x98) = reinterpret_cast<void**>(0);
        } else {
            if (*reinterpret_cast<void***>(r15_15 + 32)) {
                rsi63 = *reinterpret_cast<void***>(r15_15 + 88);
                rdi65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x88) + 8)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_15 + 24)) + 12);
                fun_20b20(rdi65, rsi63);
            }
            dtls1_hm_fragment_free(r15_15, rsi63);
            pitem_free(rax13, rsi63);
            *reinterpret_cast<void***>(r13_4 + 0x98) = rbx62;
            *v6 = rbx62;
            *reinterpret_cast<int32_t*>(&rax22) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
        }
    }
    addr_73604_55:
    rbx66 = v11 ^ g28;
    if (rbx66) {
        fun_20ff0();
    } else {
        return rax22;
    }
    addr_73681_15:
    fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
    ossl_statem_fatal(r13_4, r13_4);
    goto addr_735eb_12;
    addr_735c9_18:
    fun_207e0("ssl/statem/statem_dtls.c", "ssl/statem/statem_dtls.c");
    ossl_statem_fatal(r13_4, r13_4);
    goto addr_735eb_12;
    addr_73761_27:
    *reinterpret_cast<void***>(v9) = eax52;
    *reinterpret_cast<int32_t*>(&rax22) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
    goto addr_73604_55;
    addr_73930_33:
    ecx67 = 22;
    rdi68 = rax54;
    while (ecx67) {
        --ecx67;
        *reinterpret_cast<void***>(rdi68) = v69;
        rdi68 = rdi68 + 4;
    }
    if (!r12_37) 
        goto addr_7397b_62;
    rax70 = *reinterpret_cast<void***>(r13_4 + 8);
    *reinterpret_cast<int32_t*>(&rsi31) = 22;
    *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
    eax71 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(rax70 + 0x68)(r13_4, 22));
    if (reinterpret_cast<uint1_t>(eax71 < 0) | reinterpret_cast<uint1_t>(eax71 == 0)) 
        goto addr_73757_41;
    if (r12_37 != v72) 
        goto addr_73757_41;
    addr_7397b_62:
    rax73 = pitem_new(rbp42, r14_55, rbp42, r14_55);
    rsi31 = rax73;
    if (!rax73) 
        goto addr_73757_41;
    rdi74 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r13_4 + 0x4b8) + 0x118);
    rax75 = pqueue_insert(rdi74, rsi31, rdi74, rsi31);
    if (rax75) 
        goto addr_739ae_39;
    addr_73757_41:
    dtls1_hm_fragment_free(r14_55, rsi31, r14_55, rsi31);
    goto addr_7375f_66;
    addr_739ae_39:
    eax52 = reinterpret_cast<void**>(0xfffffffd);
    goto addr_73761_27;
    addr_7383b_37:
    if (v45) {
        addr_7375f_66:
        eax52 = reinterpret_cast<void**>(0);
        goto addr_73761_27;
    } else {
        goto addr_73754_31;
    }
    addr_73540_45:
    eax76 = dtls1_preprocess_fragment(r13_4, r15_30);
    if (!eax76) {
        addr_735eb_12:
        *reinterpret_cast<void***>(r13_4 + 0x98) = reinterpret_cast<void**>(0);
        *v6 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rax22) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
        goto addr_73604_55;
    } else {
        if (!v34) 
            goto addr_73658_69;
        rax77 = *reinterpret_cast<void***>(r13_4 + 8);
        eax78 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(rax77 + 0x68)(r13_4, 22));
        if (reinterpret_cast<uint1_t>(eax78 < 0) | reinterpret_cast<uint1_t>(eax78 == 0)) 
            goto addr_736b0_7;
    }
    if (v34 == v79) {
        addr_73658_69:
        *reinterpret_cast<void***>(r13_4 + 0x98) = v34;
        *v6 = v34;
        *reinterpret_cast<int32_t*>(&rax22) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
        goto addr_73604_55;
    } else {
        fun_20230();
        goto addr_735c9_18;
    }
    addr_7345c_49:
    eax80 = v81;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax80) | v82) | v83) {
        fun_20230();
        goto addr_73681_15;
    } else {
        rax84 = *reinterpret_cast<void***>(r13_4 + 0x4c0);
        if (rax84) {
            rax84();
        }
    }
}