undefined8 dtls_get_reassembled_message(undefined4 *param_1,undefined4 *param_2,ulong *param_3)

{
  ushort uVar1;
  void *pvVar2;
  long lVar3;
  ulong uVar4;
  int iVar5;
  undefined4 uVar6;
  pitem *ppVar7;
  ulong uVar8;
  ulong uVar9;
  undefined4 *data;
  long lVar10;
  undefined4 *puVar11;
  undefined4 *puVar12;
  undefined8 in_R11;
  long in_FS_OFFSET;
  byte bVar13;
  size_t *psVar14;
  undefined8 uVar15;
  pitem *local_1f0;
  int local_1cc;
  size_t local_1c8;
  ulong local_1c0;
  char local_1b8;
  ulong local_1b0;
  ushort local_1a8;
  long local_1a0;
  ulong local_198;
  undefined4 local_15c;
  undefined2 local_158;
  ushort local_156;
  char local_154;
  byte local_153;
  byte local_152;
  byte local_151;
  undefined local_148 [264];
  long local_40;
  
  bVar13 = 0;
  lVar10 = *(long *)(param_1 + 0x12e);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  *param_2 = 0;
LAB_00173363:
  uVar15 = 0x17336f;
  ppVar7 = pqueue_peek(*(pqueue *)(lVar10 + 0x118));
  if (ppVar7 != (pitem *)0x0) {
    pvVar2 = ppVar7->data;
    lVar10 = *(long *)(param_1 + 0x12e);
    if (*(ushort *)((long)pvVar2 + 0x10) < *(ushort *)(lVar10 + 0x110)) goto code_r0x00173340;
    if ((*(long *)((long)pvVar2 + 0x60) != 0) ||
       (*(ushort *)((long)pvVar2 + 0x10) != *(ushort *)(lVar10 + 0x110))) goto LAB_00173377;
    uVar9 = *(ulong *)((long)pvVar2 + 0x20);
    pqueue_pop(*(pqueue *)(lVar10 + 0x118));
    iVar5 = dtls1_preprocess_fragment(param_1,pvVar2);
    if (iVar5 == 0) {
      dtls1_hm_fragment_free(pvVar2);
      pitem_free(ppVar7);
      uVar15 = 0;
      *(undefined8 *)(param_1 + 0x26) = 0;
    }
    else {
      if (*(size_t *)((long)pvVar2 + 0x20) != 0) {
        memcpy((void *)(*(long *)(*(long *)(param_1 + 0x22) + 8) + 0xc +
                       *(long *)((long)pvVar2 + 0x18)),*(void **)((long)pvVar2 + 0x58),
               *(size_t *)((long)pvVar2 + 0x20));
      }
      dtls1_hm_fragment_free(pvVar2);
      pitem_free(ppVar7);
      *(ulong *)(param_1 + 0x26) = uVar9;
      *param_3 = uVar9;
      uVar15 = 1;
    }
    goto LAB_00173604;
  }
LAB_00173377:
  psVar14 = &local_1c8;
  iVar5 = (**(code **)(*(long *)(param_1 + 2) + 0x68))
                    (param_1,0x16,&local_1cc,&local_154,0xc,0,psVar14,uVar15);
  if (iVar5 < 1) {
LAB_001736b0:
    param_1[10] = 3;
    *param_3 = 0;
    uVar15 = 0;
    goto LAB_00173604;
  }
  if (local_1cc == 0x14) {
    if (local_154 != '\x01') {
      ERR_new();
      ERR_set_debug("ssl/statem/statem_dtls.c",0x314,"dtls_get_reassembled_message");
      ossl_statem_fatal(param_1,10,0x67,0);
      goto LAB_001735eb;
    }
    memcpy(*(void **)(*(long *)(param_1 + 0x22) + 8),&local_154,local_1c8);
    uVar9 = local_1c8 - 1;
    lVar10 = *(long *)(*(long *)(param_1 + 0x22) + 8);
    *(ulong *)(param_1 + 0x26) = uVar9;
    *(ulong *)(param_1 + 0xb4) = uVar9;
    param_1[0xb6] = 0x101;
    *(long *)(param_1 + 0x24) = lVar10 + 1;
    *param_3 = uVar9;
    uVar15 = 1;
    goto LAB_00173604;
  }
  if (local_1c8 != 0xc) {
    ERR_new();
    uVar15 = 0x324;
    goto LAB_00173681;
  }
  dtls1_get_message_header(&local_154);
  uVar4 = local_198;
  lVar3 = local_1a0;
  uVar9 = local_1b0;
  uVar15 = 0x1733f2;
  uVar8 = RECORD_LAYER_get_rrec_length(param_1 + 0x316);
  if (uVar8 < uVar4) {
    ERR_new();
    uVar15 = 0x334;
LAB_001735c9:
    ERR_set_debug("ssl/statem/statem_dtls.c",uVar15,"dtls_get_reassembled_message");
    ossl_statem_fatal(param_1,0x2f,0x10f,0);
    goto LAB_001735eb;
  }
  lVar10 = *(long *)(param_1 + 0x12e);
  if (local_1a8 != *(ushort *)(lVar10 + 0x110)) {
    if (local_1b0 < local_1a0 + local_198) goto LAB_00173754;
    local_15c = 0;
    local_158 = 0;
    uVar15 = 0x1737a3;
    local_156 = local_1a8 << 8 | local_1a8 >> 8;
    local_1f0 = pqueue_find(*(pqueue *)(lVar10 + 0x118),(uchar *)&local_15c);
    uVar1 = *(ushort *)(*(long *)(param_1 + 0x12e) + 0x110);
    if ((local_1f0 != (pitem *)0x0) && (local_198 == local_1b0)) {
LAB_001737e3:
      goto joined_r0x001737f8;
    }
    if (((local_1a8 <= uVar1) || (uVar1 + 10 < (uint)local_1a8)) ||
       ((uVar1 == 0 && (local_1b8 == '\x14')))) {
      local_1f0 = (pitem *)0x0;
      goto LAB_001737e3;
    }
    if (local_198 != local_1b0) goto LAB_001738d2;
    uVar9 = 0x454c;
    if (0x454b < *(ulong *)(param_1 + 0x280)) {
      uVar9 = *(ulong *)(param_1 + 0x280);
    }
    if ((local_198 <= uVar9) &&
       (data = (undefined4 *)dtls1_hm_fragment_new(local_198,0), data != (undefined4 *)0x0)) {
      puVar11 = (undefined4 *)&local_1b8;
      puVar12 = data;
      for (lVar10 = 0x16; lVar10 != 0; lVar10 = lVar10 + -1) {
        *puVar12 = *puVar11;
        puVar11 = puVar11 + (ulong)bVar13 * -2 + 1;
        puVar12 = puVar12 + (ulong)bVar13 * -2 + 1;
      }
      if (((local_198 == 0) ||
          ((iVar5 = (**(code **)(*(long *)(param_1 + 2) + 0x68))
                              (param_1,0x16,0,*(undefined8 *)(data + 0x16),local_198,0,&local_1c0,
                               in_R11), 0 < iVar5 && (local_198 == local_1c0)))) &&
         ((ppVar7 = pitem_new((uchar *)&local_15c,data), ppVar7 != (pitem *)0x0 &&
          (ppVar7 = pqueue_insert(*(pqueue *)(*(long *)(param_1 + 0x12e) + 0x118),ppVar7),
          ppVar7 != (pitem *)0x0)))) goto LAB_001739ae;
      goto LAB_00173757;
    }
    goto LAB_00173754;
  }
  if ((uVar4 < uVar9) && (uVar4 != 0)) {
LAB_001738d2:
    uVar6 = dtls1_reassemble_fragment(param_1,&local_1b8);
    goto LAB_00173761;
  }
  if ((((param_1[0xe] != 0) || (*(long *)(lVar10 + 0x1a8) != 0)) || (param_1[0x17] == 1)) ||
     (local_154 != '\0')) {
    uVar15 = 0x17354b;
    iVar5 = dtls1_preprocess_fragment(param_1,&local_1b8);
    if (iVar5 == 0) goto LAB_001735eb;
    if (uVar4 != 0) {
      iVar5 = (**(code **)(*(long *)(param_1 + 2) + 0x68))
                        (param_1,0x16,0,*(long *)(*(long *)(param_1 + 0x22) + 8) + 0xc + lVar3,uVar4
                         ,0);
      if (iVar5 < 1) goto LAB_001736b0;
      if (uVar4 != local_1c8) {
        ERR_new(uVar15,psVar14);
        uVar15 = 0x37e;
        goto LAB_001735c9;
      }
    }
    *(ulong *)(param_1 + 0x26) = uVar4;
    *param_3 = uVar4;
    uVar15 = 1;
    goto LAB_00173604;
  }
  if ((byte)(local_153 | local_152 | local_151) == 0) {
    if (*(code **)(param_1 + 0x130) != (code *)0x0) {
      (**(code **)(param_1 + 0x130))
                (0,*param_1,0x16,&local_154,0xc,param_1,*(undefined8 *)(param_1 + 0x132),uVar15);
      lVar10 = *(long *)(param_1 + 0x12e);
    }
    *(undefined8 *)(param_1 + 0x26) = 0;
    goto LAB_00173363;
  }
  ERR_new();
  uVar15 = 0x35b;
LAB_00173681:
  ERR_set_debug("ssl/statem/statem_dtls.c",uVar15,"dtls_get_reassembled_message");
  ossl_statem_fatal(param_1,10,0xf4,0);
LAB_001735eb:
  *(undefined8 *)(param_1 + 0x26) = 0;
  *param_3 = 0;
  uVar15 = 0;
LAB_00173604:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar15;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
code_r0x00173340:
  pqueue_pop(*(pqueue *)(lVar10 + 0x118));
  dtls1_hm_fragment_free(pvVar2);
  pitem_free(ppVar7);
  lVar10 = *(long *)(param_1 + 0x12e);
  goto LAB_00173363;
joined_r0x001737f8:
  if (local_198 == 0) {
LAB_001739ae:
    uVar6 = 0xfffffffd;
  }
  else {
    uVar9 = 0x100;
    if (local_198 < 0x101) {
      uVar9 = local_198;
    }
    iVar5 = (**(code **)(*(long *)(param_1 + 2) + 0x68))
                      (param_1,0x16,0,local_148,uVar9,0,&local_1c0,uVar15);
    if (0 < iVar5) goto LAB_00173800;
    if (local_1f0 == (pitem *)0x0) {
LAB_00173754:
      data = (undefined4 *)0x0;
LAB_00173757:
      dtls1_hm_fragment_free(data);
    }
    uVar6 = 0;
  }
  goto LAB_00173761;
LAB_00173800:
  local_198 = local_198 - local_1c0;
  goto joined_r0x001737f8;
LAB_00173761:
  *param_2 = uVar6;
  uVar15 = 0;
  goto LAB_00173604;
}