int64_t dtls_get_reassembled_message(int64_t a1, int64_t * a2, int64_t * a3, int64_t a4, int64_t a5, int64_t a6) {
    // 0x732d0
    int64_t v1; // bp-504, 0x732d0
    int64_t v2 = &v1; // 0x732dd
    __readfsqword(40);
    *(int32_t *)a2 = 0;
    int64_t v3 = pqueue_peek(); // 0x7336a
    int64_t result; // 0x732d0
    if (v3 != 0) {
        int64_t v4 = *(int64_t *)(v3 + 8); // 0x73320
        uint16_t v5 = *(int16_t *)(v4 + 16); // 0x7332b
        uint16_t v6 = *(int16_t *)(*(int64_t *)(a1 + (int64_t)&g188) + 272); // 0x73330
        while (v5 < v6) {
            // 0x73340
            pqueue_pop();
            dtls1_hm_fragment_free();
            pitem_free();
            int64_t v7 = pqueue_peek(); // 0x7336a
            if (v7 == 0) {
                goto lab_0x73377;
            }
            v4 = *(int64_t *)(v7 + 8);
            v5 = *(int16_t *)(v4 + 16);
            v6 = *(int16_t *)(*(int64_t *)(a1 + (int64_t)&g188) + 272);
        }
        // 0x734d0
        if (v5 == v6 == *(int64_t *)(v4 + 96) == 0) {
            int64_t v8 = v5;
            int64_t * v9 = (int64_t *)(v4 + 32); // 0x734eb
            int64_t v10 = *v9; // 0x734eb
            pqueue_pop();
            int64_t v11 = dtls1_preprocess_fragment(a1, v4, v8, v8); // 0x734fa
            if ((int32_t)v11 == 0) {
                // 0x73630
                dtls1_hm_fragment_free();
                pitem_free();
                *(int64_t *)(a1 + 152) = 0;
                result = v11 & 0xffffffff;
            } else {
                // 0x73507
                if (*v9 != 0) {
                    // 0x738b0
                    function_20b20();
                }
                // 0x73514
                dtls1_hm_fragment_free();
                pitem_free();
                *(int64_t *)(a1 + 152) = v10;
                *a3 = v10;
                result = 1;
            }
            goto lab_0x73604;
        } else {
            goto lab_0x73377;
        }
    } else {
        goto lab_0x73377;
    }
  lab_0x73377:;
    int64_t * v12 = (int64_t *)(a1 + 8); // 0x7337c
    int32_t v13; // 0x732d0
    int64_t v14; // 0x732d0
    int64_t v15; // 0x732d0
    char v16; // bp-440, 0x732d0
    int64_t v17; // bp-448, 0x732d0
    uint64_t v18; // 0x733d7
    int64_t v19; // 0x73400
    int64_t v20; // 0x73409
    int32_t v21; // 0x732d0
    int64_t v22; // 0x732d0
    if ((int32_t)*v12 < 1) {
        goto lab_0x736b0;
    } else {
        if (v21 == 20) {
            char v23; // 0x732d0
            if (v23 == 1) {
                // 0x7384c
                function_20b20();
                int64_t v24 = *(int64_t *)(a1 + 136); // 0x7386e
                int64_t v25; // 0x732d0
                int64_t v26 = v25 - 1; // 0x73875
                int64_t v27 = *(int64_t *)(v24 + 8); // 0x73879
                *(int64_t *)(a1 + 152) = v26;
                *(int64_t *)(a1 + (int64_t)&g78) = v26;
                *(int32_t *)(a1 + (int64_t)&g79) = 257;
                *(int64_t *)(a1 + 144) = v27 + 1;
                *a3 = v26;
                result = 1;
                goto lab_0x73604;
            } else {
                // 0x736de
                function_20230();
                function_207e0();
                ossl_statem_fatal();
                goto lab_0x735eb;
            }
        } else {
            if (v22 != 12) {
                // 0x73720
                function_20230();
                // 0x73681
                function_207e0();
                ossl_statem_fatal();
                goto lab_0x735eb;
            } else {
                // 0x733c2
                dtls1_get_message_header();
                v18 = (int64_t)v21;
                if (RECORD_LAYER_get_rrec_length() < v18) {
                    // 0x735b8
                    function_20230();
                    // 0x735c9
                    function_207e0();
                    ossl_statem_fatal();
                    goto lab_0x735eb;
                } else {
                    int64_t * v28 = (int64_t *)(a1 + (int64_t)&g188); // 0x73400
                    v19 = *v28;
                    uint16_t v29 = (int16_t)v21; // 0x73409
                    if (*(int16_t *)(v19 + 272) != v29) {
                        if (v22 + v18 > v22) {
                            // 0x73757
                            dtls1_hm_fragment_free();
                            v13 = 0;
                            goto lab_0x73761;
                        } else {
                            int64_t v30 = pqueue_find(); // 0x7379e
                            uint16_t v31 = *(int16_t *)(*v28 + 272); // 0x737b7
                            if (v31 < v29 == (v22 == v18 != v30 != 0)) {
                                int64_t v32 = v31; // 0x737b7
                                int64_t v33 = v32 + 10; // 0x737cf
                                if (v33 < (int64_t)(v21 % 0x10000)) {
                                    goto lab_0x737e3;
                                } else {
                                    // 0x738e2
                                    if (v31 == 0 == v16 == 20) {
                                        goto lab_0x737e3;
                                    } else {
                                        // 0x738f2
                                        v14 = v33;
                                        v15 = v32;
                                        if (v22 != v18) {
                                            goto lab_0x738d2;
                                        } else {
                                            uint64_t v34 = *(int64_t *)(a1 + (int64_t)&g382); // 0x738f9
                                            uint64_t v35 = v34 >= (int64_t)&g169 ? v34 : (int64_t)&g169; // 0x73909
                                            if (v35 < v18) {
                                                // 0x73757
                                                dtls1_hm_fragment_free();
                                                v13 = 0;
                                                goto lab_0x73761;
                                            } else {
                                                int64_t v36 = dtls1_hm_fragment_new(v18, 0); // 0x7391f
                                                if (v36 == 0) {
                                                    // 0x73757
                                                    dtls1_hm_fragment_free();
                                                    v13 = 0;
                                                    goto lab_0x73761;
                                                } else {
                                                    // 0x73930
                                                    __asm_rep_movsd_memcpy((char *)v36, &v16, 22);
                                                    if (v21 == 0) {
                                                        goto lab_0x7397b;
                                                    } else {
                                                        // 0x73942
                                                        if ((int32_t)*v12 < 1) {
                                                            // 0x73757
                                                            dtls1_hm_fragment_free();
                                                            v13 = 0;
                                                            goto lab_0x73761;
                                                        } else {
                                                            // 0x73970
                                                            if (v17 != v18) {
                                                                // 0x73757
                                                                dtls1_hm_fragment_free();
                                                                v13 = 0;
                                                                goto lab_0x73761;
                                                            } else {
                                                                goto lab_0x7397b;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                goto lab_0x737e3;
                            }
                        }
                    } else {
                        // 0x7341a
                        v20 = llvm_bswap_i16(v29);
                        v14 = v20;
                        v15 = v19;
                        if (v22 > v18 == (v21 != 0)) {
                            goto lab_0x738d2;
                        } else {
                            // 0x73428
                            if (*(int32_t *)(a1 + 56) != 0) {
                                goto lab_0x73540;
                            } else {
                                // 0x73435
                                if (*(int64_t *)(v19 + 424) != 0) {
                                    goto lab_0x73540;
                                } else {
                                    // 0x73443
                                    char v37; // 0x732d0
                                    if (v37 != 0 | *(int32_t *)(a1 + 92) == 1) {
                                        goto lab_0x73540;
                                    } else {
                                        if ((v37 || v37 || v37) == 0) {
                                            // 0x73478
                                            return *(int64_t *)(a1 + (int64_t)&g189);
                                        }
                                        // 0x73670
                                        function_20230();
                                        // 0x73681
                                        function_207e0();
                                        ossl_statem_fatal();
                                        goto lab_0x735eb;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
  lab_0x73761:
    // 0x73761
    *(int32_t *)*(int64_t *)(v2 + 24) = v13;
    result = 0;
    goto lab_0x73604;
  lab_0x736b0:
    // 0x736b0
    *(int32_t *)(a1 + 40) = 3;
    *a3 = 0;
    result = 0;
    goto lab_0x73604;
  lab_0x73604:
    // 0x73604
    if (*(int64_t *)(v2 + 440) != __readfsqword(40)) {
        // 0x739b8
        return function_20ff0();
    }
    // 0x7361b
    return result;
  lab_0x735eb:
    // 0x735eb
    *(int64_t *)(a1 + 152) = 0;
    *a3 = 0;
    result = 0;
    goto lab_0x73604;
  lab_0x738d2:;
    int64_t v38 = dtls1_reassemble_fragment(a1, (int64_t *)&v16, (int16_t)v15, v14, 12, 0); // 0x738d8
    v13 = v38;
    goto lab_0x73761;
  lab_0x737e3:
    // 0x737e3
    v13 = -3;
    if (v21 != 0) {
        int64_t v39 = v18; // 0x73800
        *(int64_t *)(v2 - 16) = (int64_t)&v17;
        int32_t v40 = (int32_t)*v12; // 0x73837
        while (v40 >= 0 == (v40 != 0)) {
            // 0x73800
            v39 -= *(int64_t *)(v2 + 56);
            v13 = -3;
            if (v39 == 0) {
                goto lab_0x73761;
            }
            *(int64_t *)(v2 - 16) = (int64_t)&v17;
            v40 = (int32_t)*v12;
        }
        // 0x7383b
        v13 = 0;
        if (*(int64_t *)(v2 + 8) != 0) {
            goto lab_0x73761;
        } else {
            // 0x73757
            dtls1_hm_fragment_free();
            v13 = 0;
            goto lab_0x73761;
        }
    } else {
        goto lab_0x73761;
    }
  lab_0x73540:;
    int64_t v41 = dtls1_preprocess_fragment(a1, (int64_t)&v16, v19, v20); // 0x73546
    if ((int32_t)v41 == 0) {
        goto lab_0x735eb;
    } else {
        if (v21 == 0) {
            goto lab_0x73658;
        } else {
            // 0x7355c
            if ((int32_t)*v12 < 1) {
                goto lab_0x736b0;
            } else {
                if (v22 == v18) {
                    goto lab_0x73658;
                } else {
                    // 0x735a2
                    function_20230();
                    // 0x735c9
                    function_207e0();
                    ossl_statem_fatal();
                    goto lab_0x735eb;
                }
            }
        }
    }
  lab_0x73658:
    // 0x73658
    *(int64_t *)(a1 + 152) = v18;
    *a3 = v18;
    result = 1;
    goto lab_0x73604;
  lab_0x7397b:
    // 0x7397b
    if (pitem_new() == 0) {
        // 0x73757
        dtls1_hm_fragment_free();
        v13 = 0;
        goto lab_0x73761;
    } else {
        // 0x73992
        v13 = -3;
        if (pqueue_insert() == 0) {
            // 0x73757
            dtls1_hm_fragment_free();
            v13 = 0;
            goto lab_0x73761;
        } else {
            goto lab_0x73761;
        }
    }
}