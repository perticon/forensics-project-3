uchar * SSL_SESSION_get_id(SSL_SESSION *s,uint *len)

{
  if (len != (uint *)0x0) {
    *len = (uint)s->tlsext_tick_lifetime_hint;
  }
  return (uchar *)(s + 1);
}