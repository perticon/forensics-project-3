undefined8 tls_parse_ctos_maxfragmentlen(long param_1,byte **param_2)

{
  byte bVar1;
  undefined8 uVar2;
  
  if (param_2[1] != (byte *)0x1) {
    ERR_new();
    ERR_set_debug("ssl/statem/extensions_srvr.c",0xac,"tls_parse_ctos_maxfragmentlen");
    ossl_statem_fatal(param_1,0x32,0x6e,0);
    return 0;
  }
  bVar1 = **param_2;
  param_2[1] = (byte *)0x0;
  *param_2 = *param_2 + 1;
  if (bVar1 - 1 < 4) {
    if ((*(int *)(param_1 + 0x4d0) == 0) || (*(byte *)(*(long *)(param_1 + 0x918) + 0x368) == bVar1)
       ) {
      *(byte *)(*(long *)(param_1 + 0x918) + 0x368) = bVar1;
      return 1;
    }
    ERR_new();
    uVar2 = 0xbd;
  }
  else {
    ERR_new();
    uVar2 = 0xb2;
  }
  ERR_set_debug("ssl/statem/extensions_srvr.c",uVar2,"tls_parse_ctos_maxfragmentlen");
  ossl_statem_fatal(param_1,0x2f,0xe8,0);
  return 0;
}