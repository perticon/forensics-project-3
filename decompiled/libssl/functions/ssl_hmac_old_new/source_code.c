int ssl_hmac_old_new(SSL_HMAC *ret)
{
    ret->old_ctx = HMAC_CTX_new();
    if (ret->old_ctx == NULL)
        return 0;

    return 1;
}