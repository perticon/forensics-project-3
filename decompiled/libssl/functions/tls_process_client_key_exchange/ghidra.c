undefined8 tls_process_client_key_exchange(uint *param_1,ushort **param_2)

{
  uint uVar1;
  int iVar2;
  int iVar3;
  undefined8 uVar4;
  EVP_PKEY_CTX *pEVar5;
  uchar *out;
  ushort *puVar6;
  ushort *puVar7;
  EVP_PKEY *to;
  BIGNUM *a;
  EVP_PKEY *pEVar8;
  ASN1_TYPE **ppAVar9;
  ushort *puVar10;
  long lVar11;
  char *pcVar12;
  ushort uVar13;
  long lVar14;
  undefined8 uVar15;
  undefined8 *puVar16;
  long in_FS_OFFSET;
  undefined4 local_148;
  undefined4 uStack324;
  undefined4 uStack320;
  undefined4 uStack316;
  undefined4 local_138;
  undefined4 uStack308;
  undefined4 uStack304;
  undefined4 uStack300;
  undefined8 local_128;
  size_t local_118;
  ushort *local_110;
  undefined local_108 [32];
  undefined8 local_e8;
  undefined8 local_e0 [5];
  undefined8 local_b8 [6];
  undefined local_88 [32];
  uchar local_68 [40];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = *(uint *)(*(long *)(param_1 + 0xb8) + 0x1c);
  if (((uVar1 & 0x1c8) == 0) || (iVar2 = tls_process_cke_psk_preamble(), iVar2 != 0)) {
    if ((uVar1 & 8) == 0) {
      if ((uVar1 & 0x41) == 0) {
        if ((uVar1 & 0x102) != 0) {
          if (param_2[1] < (ushort *)0x2) {
LAB_0017e530:
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xb94,"tls_process_cke_dhe");
            uVar4 = 0x94;
            uVar15 = 0x32;
          }
          else {
            puVar7 = param_2[1] + -1;
            uVar13 = **param_2;
            puVar10 = *param_2 + 1;
            param_2[1] = puVar7;
            *param_2 = puVar10;
            if (puVar7 != (ushort *)(ulong)(ushort)(uVar13 << 8 | uVar13 >> 8)) goto LAB_0017e530;
            pEVar8 = *(EVP_PKEY **)(param_1 + 0xba);
            if (pEVar8 == (EVP_PKEY *)0x0) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_srvr.c",0xb99,"tls_process_cke_dhe");
              uVar4 = 0xab;
LAB_0017ee24:
              uVar15 = 0x50;
            }
            else {
              if (puVar7 != (ushort *)0x0) {
                param_2[1] = (ushort *)0x0;
                *param_2 = (ushort *)((long)puVar10 + (long)puVar7);
                to = EVP_PKEY_new();
                if ((to == (EVP_PKEY *)0x0) ||
                   (iVar2 = EVP_PKEY_copy_parameters(to,pEVar8), iVar2 == 0)) {
                  ERR_new();
                  ERR_set_debug("ssl/statem/statem_srvr.c",0xba8,"tls_process_cke_dhe");
                  uVar4 = 0x128;
LAB_0017e6e8:
                  ossl_statem_fatal(param_1,0x50,uVar4,0);
                }
                else {
                  iVar2 = EVP_PKEY_set1_encoded_public_key(to,puVar10,puVar7);
                  if (iVar2 == 0) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_srvr.c",0xbad,"tls_process_cke_dhe");
                    uVar4 = 0xc0103;
                    goto LAB_0017e6e8;
                  }
LAB_0017e4e9:
                  iVar2 = ssl_derive(param_1,pEVar8,to,1);
                  if (iVar2 != 0) {
                    EVP_PKEY_free(*(EVP_PKEY **)(param_1 + 0xba));
                    *(undefined8 *)(param_1 + 0xba) = 0;
                    EVP_PKEY_free(to);
                    uVar4 = 2;
                    goto LAB_0017e417;
                  }
                }
                EVP_PKEY_free(to);
                goto LAB_0017e3e0;
              }
              ERR_new();
              ERR_set_debug("ssl/statem/statem_srvr.c",0xb9e,"tls_process_cke_dhe");
              uVar4 = 0xab;
              uVar15 = 0x32;
            }
          }
LAB_0017e559:
          ossl_statem_fatal(param_1,uVar15,uVar4,0);
          EVP_PKEY_free((EVP_PKEY *)0x0);
          goto LAB_0017e3e0;
        }
        if ((uVar1 & 0x84) != 0) {
          if (param_2[1] == (ushort *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xbc6,"tls_process_cke_ecdhe");
            uVar4 = 0x137;
            uVar15 = 0x28;
          }
          else {
            puVar7 = (ushort *)((long)param_2[1] + -1);
            pEVar8 = *(EVP_PKEY **)(param_1 + 0xba);
            puVar6 = (ushort *)(ulong)*(byte *)*param_2;
            puVar10 = (ushort *)((long)*param_2 + 1);
            param_2[1] = puVar7;
            *param_2 = puVar10;
            if (puVar6 <= puVar7) {
              *param_2 = (ushort *)((long)puVar10 + (long)puVar6);
              param_2[1] = (ushort *)((long)puVar7 - (long)puVar6);
              if ((ushort *)((long)puVar7 - (long)puVar6) == (ushort *)0x0) {
                if (pEVar8 != (EVP_PKEY *)0x0) {
                  to = EVP_PKEY_new();
                  if ((to == (EVP_PKEY *)0x0) ||
                     (iVar2 = EVP_PKEY_copy_parameters(to,pEVar8), iVar2 < 1)) {
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_srvr.c",0xbde,"tls_process_cke_ecdhe");
                    uVar4 = 0x128;
                  }
                  else {
                    iVar2 = EVP_PKEY_set1_encoded_public_key(to,puVar10,puVar6);
                    if (0 < iVar2) goto LAB_0017e4e9;
                    ERR_new();
                    ERR_set_debug("ssl/statem/statem_srvr.c",0xbe3,"tls_process_cke_ecdhe");
                    uVar4 = 0x80010;
                  }
                  goto LAB_0017e6e8;
                }
                ERR_new();
                ERR_set_debug("ssl/statem/statem_srvr.c",0xbd8,"tls_process_cke_ecdhe");
                uVar4 = 0x137;
                goto LAB_0017ee24;
              }
            }
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xbd4,"tls_process_cke_ecdhe");
            uVar4 = 0x9f;
            uVar15 = 0x32;
          }
          goto LAB_0017e559;
        }
        if ((uVar1 & 0x20) != 0) {
          if ((ushort *)0x1 < param_2[1]) {
            puVar10 = param_2[1] + -1;
            uVar13 = **param_2;
            puVar6 = *param_2 + 1;
            param_2[1] = puVar10;
            *param_2 = puVar6;
            uVar13 = uVar13 << 8 | uVar13 >> 8;
            puVar7 = (ushort *)(ulong)uVar13;
            if (puVar7 <= puVar10) {
              *param_2 = (ushort *)((long)puVar6 + (long)puVar7);
              param_2[1] = (ushort *)((long)puVar10 - (long)puVar7);
              a = BN_bin2bn((uchar *)puVar6,(uint)uVar13,(BIGNUM *)0x0);
              *(BIGNUM **)(param_1 + 0x306) = a;
              if (a == (BIGNUM *)0x0) {
                ERR_new();
                ERR_set_debug("ssl/statem/statem_srvr.c",0xc02,"tls_process_cke_srp");
                ossl_statem_fatal(param_1,0x50,0x80003,0);
              }
              else {
                iVar2 = BN_ucmp(a,*(BIGNUM **)(param_1 + 0x2fe));
                if ((iVar2 < 0) &&
                   (iVar2 = BN_is_zero(*(undefined8 *)(param_1 + 0x306)), iVar2 == 0)) {
                  CRYPTO_free(*(void **)(*(long *)(param_1 + 0x246) + 0x370));
                  lVar14 = *(long *)(param_1 + 0x246);
                  pcVar12 = CRYPTO_strdup(*(char **)(param_1 + 0x2fc),"ssl/statem/statem_srvr.c",
                                          0xc0a);
                  *(char **)(lVar14 + 0x370) = pcVar12;
                  if (*(long *)(*(long *)(param_1 + 0x246) + 0x370) != 0) {
                    iVar2 = srp_generate_server_master_secret(param_1);
                    goto joined_r0x0017e87d;
                  }
                  ERR_new();
                  pcVar12 = "tls_process_cke_srp";
                  uVar4 = 0xc0c;
                  goto LAB_0017eab1;
                }
                ERR_new();
                ERR_set_debug("ssl/statem/statem_srvr.c",0xc06,"tls_process_cke_srp");
                ossl_statem_fatal(param_1,0x2f,0x173,0);
              }
              goto LAB_0017e3e0;
            }
          }
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0xbfe,"tls_process_cke_srp");
          ossl_statem_fatal(param_1,0x32,0x15b,0);
          goto LAB_0017e3e0;
        }
        if ((uVar1 & 0x10) != 0) {
          local_118 = 0x20;
          if ((*(uint *)(*(long *)(param_1 + 0xb8) + 0x20) & 0x80) == 0) {
            lVar14 = 0;
            if ((*(uint *)(*(long *)(param_1 + 0xb8) + 0x20) & 0x20) != 0) {
              lVar11 = *(long *)(param_1 + 0x226);
              goto LAB_0017eaf1;
            }
          }
          else {
            lVar11 = *(long *)(param_1 + 0x226);
            lVar14 = *(long *)(lVar11 + 0x118);
            if ((lVar14 == 0) && (lVar14 = *(long *)(lVar11 + 0xf0), lVar14 == 0)) {
LAB_0017eaf1:
              lVar14 = *(long *)(lVar11 + 200);
            }
          }
          pEVar5 = (EVP_PKEY_CTX *)
                   EVP_PKEY_CTX_new_from_pkey
                             (**(undefined8 **)(param_1 + 0x26a),lVar14,
                              (*(undefined8 **)(param_1 + 0x26a))[0x88]);
          if (pEVar5 == (EVP_PKEY_CTX *)0x0) {
            ERR_new();
            pcVar12 = "tls_process_cke_gost";
            uVar4 = 0xc3d;
            goto LAB_0017eab1;
          }
          iVar2 = EVP_PKEY_decrypt_init(pEVar5);
          if (iVar2 < 1) {
            ERR_new();
            pcVar12 = "tls_process_cke_gost";
            uVar4 = 0xc41;
            goto LAB_0017eb51;
          }
          pEVar8 = (EVP_PKEY *)X509_get0_pubkey(*(undefined8 *)(*(long *)(param_1 + 0x246) + 0x2b8))
          ;
          if ((pEVar8 != (EVP_PKEY *)0x0) &&
             (iVar2 = EVP_PKEY_derive_set_peer(pEVar5,pEVar8), iVar2 < 1)) {
            ERR_clear_error();
          }
          local_110 = *param_2;
          ppAVar9 = (ASN1_TYPE **)d2i_GOST_KX_MESSAGE(0,&local_110,param_2[1]);
          if (((ppAVar9 == (ASN1_TYPE **)0x0) || (*ppAVar9 == (ASN1_TYPE *)0x0)) ||
             (iVar2 = ASN1_TYPE_get(*ppAVar9), iVar2 != 0x10)) {
            ERR_new();
            uVar4 = 0xc57;
LAB_0017ea61:
            ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,"tls_process_cke_gost");
            uVar4 = 0x32;
LAB_0017ea79:
            ossl_statem_fatal(param_1,uVar4,0x93,0);
          }
          else {
            if (param_2[1] < (ushort *)((long)local_110 - (long)*param_2)) {
              ERR_new();
              uVar4 = 0xc5c;
LAB_0017efa1:
              ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,"tls_process_cke_gost");
              uVar4 = 0x50;
              goto LAB_0017ea79;
            }
            puVar10 = (ushort *)
                      ((long)param_2[1] - (long)(ushort *)((long)local_110 - (long)*param_2));
            *param_2 = local_110;
            param_2[1] = puVar10;
            if (puVar10 != (ushort *)0x0) {
              ERR_new();
              uVar4 = 0xc61;
              goto LAB_0017efa1;
            }
            iVar2 = EVP_PKEY_decrypt(pEVar5,local_68,&local_118,
                                     *(uchar **)((int *)(*ppAVar9)->value + 2),
                                     (long)*(int *)(*ppAVar9)->value);
            if (iVar2 < 1) {
              ERR_new();
              uVar4 = 0xc6a;
              goto LAB_0017ea61;
            }
            iVar2 = ssl_generate_master_secret(param_1,local_68,0x20,0);
            if (iVar2 != 0) {
              iVar2 = EVP_PKEY_CTX_ctrl(pEVar5,-1,-1,2,2,(void *)0x0);
              if (0 < iVar2) {
                param_1[0x1d] = 1;
              }
              EVP_PKEY_CTX_free(pEVar5);
              GOST_KX_MESSAGE_free(ppAVar9);
              goto LAB_0017e0cd;
            }
          }
          EVP_PKEY_CTX_free(pEVar5);
          GOST_KX_MESSAGE_free(ppAVar9);
          goto LAB_0017e3e0;
        }
        if ((uVar1 & 0x200) == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0xd08,"tls_process_client_key_exchange");
          ossl_statem_fatal(param_1,0x50,0xf9,0);
          goto LAB_0017e3e0;
        }
        local_110 = (ushort *)0x20;
        iVar2 = ossl_gost18_cke_cipher_nid(param_1);
        if (iVar2 == 0) {
          ERR_new();
          pcVar12 = "tls_process_cke_gost18";
          uVar4 = 0xc91;
LAB_0017eb51:
          ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,pcVar12);
          ossl_statem_fatal(param_1,0x50,0xc0103,0);
          goto LAB_0017e3e0;
        }
        iVar3 = ossl_gost_ukm(param_1,local_88);
        if (iVar3 < 1) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0xc96,"tls_process_cke_gost18");
          uVar4 = 0xc0103;
LAB_0017ed62:
          ossl_statem_fatal(param_1,0x50,uVar4,0);
          pEVar5 = (EVP_PKEY_CTX *)0x0;
        }
        else {
          lVar14 = *(long *)(*(long *)(param_1 + 0x226) + 0x118);
          if ((lVar14 == 0) && (lVar14 = *(long *)(*(long *)(param_1 + 0x226) + 0xf0), lVar14 == 0))
          {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xc9f,"tls_process_cke_gost18");
            uVar4 = 0xec;
            goto LAB_0017ed62;
          }
          pEVar5 = (EVP_PKEY_CTX *)
                   EVP_PKEY_CTX_new_from_pkey
                             (**(undefined8 **)(param_1 + 0x26a),lVar14,
                              (*(undefined8 **)(param_1 + 0x26a))[0x88]);
          if (pEVar5 == (EVP_PKEY_CTX *)0x0) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xca5,"tls_process_cke_gost18");
            uVar4 = 0xc0100;
            goto LAB_0017ed62;
          }
          iVar3 = EVP_PKEY_decrypt_init(pEVar5);
          if (iVar3 < 1) {
            ERR_new();
            ERR_set_debug("ssl/statem/statem_srvr.c",0xca9,"tls_process_cke_gost18");
            uVar4 = 0xc0103;
            uVar15 = 0x50;
LAB_0017ee57:
            ossl_statem_fatal(param_1,uVar15,uVar4,0);
          }
          else {
            iVar3 = EVP_PKEY_CTX_ctrl(pEVar5,-1,0x400,8,0x20,local_88);
            if (iVar3 < 1) {
              ERR_new();
              uVar4 = 0xcb0;
LAB_0017ef56:
              ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,"tls_process_cke_gost18");
              uVar4 = 0x112;
              uVar15 = 0x50;
              goto LAB_0017ee57;
            }
            iVar2 = EVP_PKEY_CTX_ctrl(pEVar5,-1,0x400,0xc,iVar2,(void *)0x0);
            if (iVar2 < 1) {
              ERR_new();
              uVar4 = 0xcb6;
              goto LAB_0017ef56;
            }
            iVar2 = EVP_PKEY_decrypt(pEVar5,local_68,(size_t *)&local_110,(uchar *)*param_2,
                                     (size_t)param_2[1]);
            if (iVar2 < 1) {
              ERR_new();
              ERR_set_debug("ssl/statem/statem_srvr.c",0xcbd,"tls_process_cke_gost18");
              uVar4 = 0x93;
              uVar15 = 0x32;
              goto LAB_0017ee57;
            }
            iVar2 = ssl_generate_master_secret(param_1,local_68,0x20,0);
            if (iVar2 != 0) {
              EVP_PKEY_CTX_free(pEVar5);
              uVar4 = 2;
              goto LAB_0017e417;
            }
          }
        }
      }
      else {
        lVar14 = *(long *)(*(long *)(param_1 + 0x226) + 0x28);
        if (lVar14 == 0) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0xb36,"tls_process_cke_rsa");
          ossl_statem_fatal(param_1,0x50,0xa8,0);
          goto LAB_0017e3e0;
        }
        puVar10 = param_2[1];
        puVar7 = *param_2;
        if ((*param_1 & 0xfffffdff) != 0x100) {
          if ((ushort *)0x1 < puVar10) {
            puVar6 = puVar10 + -1;
            puVar10 = (ushort *)(ulong)(ushort)(*puVar7 << 8 | *puVar7 >> 8);
            if (puVar10 <= puVar6) {
              puVar7 = puVar7 + 1;
              puVar6 = (ushort *)((long)puVar6 - (long)puVar10);
              param_2[1] = puVar6;
              *param_2 = (ushort *)((long)puVar7 + (long)puVar10);
              if (puVar6 == (ushort *)0x0) goto LAB_0017e248;
            }
          }
          ERR_new();
          pcVar12 = "tls_process_cke_rsa";
          uVar4 = 0xb40;
          goto LAB_0017e581;
        }
LAB_0017e248:
        local_110 = (ushort *)0x30;
        out = (uchar *)CRYPTO_malloc(0x30,"ssl/statem/statem_srvr.c",0xb46);
        if (out == (uchar *)0x0) {
          ERR_new();
          pcVar12 = "tls_process_cke_rsa";
          uVar4 = 0xb48;
LAB_0017eab1:
          ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,pcVar12);
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
          goto LAB_0017e3e0;
        }
        pEVar5 = (EVP_PKEY_CTX *)
                 EVP_PKEY_CTX_new_from_pkey
                           (**(undefined8 **)(param_1 + 0x26a),lVar14,
                            (*(undefined8 **)(param_1 + 0x26a))[0x88]);
        if (pEVar5 == (EVP_PKEY_CTX *)0x0) {
          ERR_new();
          ERR_set_debug("ssl/statem/statem_srvr.c",0xb4e,"tls_process_cke_rsa");
          ossl_statem_fatal(param_1,0x50,0xc0100,0);
          CRYPTO_free(out);
          EVP_PKEY_CTX_free((EVP_PKEY_CTX *)0x0);
          goto LAB_0017e3e0;
        }
        iVar2 = EVP_PKEY_decrypt_init(pEVar5);
        if ((iVar2 < 1) || (iVar2 = EVP_PKEY_CTX_set_rsa_padding(pEVar5,7), iVar2 < 1)) {
          ERR_new();
          uVar4 = 0xb5f;
LAB_0017e398:
          ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,"tls_process_cke_rsa");
          ossl_statem_fatal(param_1,0x33,0x93,0);
        }
        else {
          puVar16 = local_e0;
          OSSL_PARAM_construct_uint(&local_148,"tls-client-version",param_1 + 0x283);
          local_e8 = local_128;
          if ((*(byte *)((long)param_1 + 0x9ea) & 0x80) != 0) {
            puVar16 = local_b8;
            OSSL_PARAM_construct_uint
                      (CONCAT44(uStack324,local_148),CONCAT44(uStack308,local_138),&local_148,
                       "tls-negotiated-version",param_1);
          }
          OSSL_PARAM_construct_end(&local_148);
          *puVar16 = CONCAT44(uStack324,local_148);
          puVar16[1] = CONCAT44(uStack316,uStack320);
          puVar16[2] = CONCAT44(uStack308,local_138);
          puVar16[3] = CONCAT44(uStack300,uStack304);
          puVar16[4] = local_128;
          iVar2 = EVP_PKEY_CTX_set_params(pEVar5,local_108);
          if ((iVar2 == 0) ||
             (iVar2 = EVP_PKEY_decrypt(pEVar5,out,(size_t *)&local_110,(uchar *)puVar7,
                                       (size_t)puVar10), iVar2 < 1)) {
            ERR_new();
            uVar4 = 0xb6f;
            goto LAB_0017e398;
          }
          if (local_110 != (ushort *)0x30) {
            OPENSSL_cleanse(out,0x30);
            ERR_new();
            uVar4 = 0xb79;
            goto LAB_0017e398;
          }
          iVar2 = ssl_generate_master_secret(param_1,out,0x30,0);
          if (iVar2 != 0) {
            CRYPTO_free(out);
            EVP_PKEY_CTX_free(pEVar5);
            uVar4 = 2;
            goto LAB_0017e417;
          }
        }
        CRYPTO_free(out);
      }
      EVP_PKEY_CTX_free(pEVar5);
    }
    else if (param_2[1] == (ushort *)0x0) {
      iVar2 = ssl_generate_master_secret(param_1,0,0,0);
joined_r0x0017e87d:
      if (iVar2 != 0) {
LAB_0017e0cd:
        uVar4 = 2;
        goto LAB_0017e417;
      }
    }
    else {
      ERR_new();
      pcVar12 = "tls_process_client_key_exchange";
      uVar4 = 0xce1;
LAB_0017e581:
      ERR_set_debug("ssl/statem/statem_srvr.c",uVar4,pcVar12);
      ossl_statem_fatal(param_1,0x32,0x9f,0);
    }
  }
LAB_0017e3e0:
  CRYPTO_clear_free(*(undefined8 *)(param_1 + 0xdc),*(undefined8 *)(param_1 + 0xde),
                    "ssl/statem/statem_srvr.c",0xd0f);
  uVar4 = 0;
  *(undefined8 *)(param_1 + 0xdc) = 0;
  *(undefined8 *)(param_1 + 0xde) = 0;
LAB_0017e417:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}