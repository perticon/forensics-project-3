undefined8 custom_exts_copy_flags(undefined8 param_1,undefined8 *param_2)

{
  undefined4 *puVar1;
  long lVar2;
  undefined2 *puVar3;
  ulong uVar4;
  
  puVar3 = (undefined2 *)*param_2;
  if (param_2[1] != 0) {
    uVar4 = 0;
    do {
      while( true ) {
        lVar2 = custom_ext_find(param_1,*(undefined4 *)(puVar3 + 2),*puVar3,0);
        if (lVar2 != 0) break;
        uVar4 = uVar4 + 1;
        puVar3 = puVar3 + 0x1c;
        if ((ulong)param_2[1] < uVar4 || param_2[1] == uVar4) {
          return 1;
        }
      }
      puVar1 = (undefined4 *)(puVar3 + 6);
      uVar4 = uVar4 + 1;
      puVar3 = puVar3 + 0x1c;
      *(undefined4 *)(lVar2 + 0xc) = *puVar1;
    } while (uVar4 <= (ulong)param_2[1] && param_2[1] != uVar4);
  }
  return 1;
}