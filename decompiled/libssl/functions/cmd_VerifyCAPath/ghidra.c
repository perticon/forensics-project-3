bool cmd_VerifyCAPath(long param_1,long param_2)

{
  long lVar1;
  int iVar2;
  X509_STORE *pXVar3;
  
  if (*(long *)(param_1 + 0x18) == 0) {
    if (*(long *)(param_1 + 0x20) == 0) {
      return true;
    }
    lVar1 = *(long *)(*(long *)(param_1 + 0x20) + 0x898);
    pXVar3 = *(X509_STORE **)(lVar1 + 0x1d0);
  }
  else {
    lVar1 = *(long *)(*(long *)(param_1 + 0x18) + 0x158);
    pXVar3 = *(X509_STORE **)(lVar1 + 0x1d0);
  }
  if (pXVar3 == (X509_STORE *)0x0) {
    pXVar3 = X509_STORE_new();
    *(X509_STORE **)(lVar1 + 0x1d0) = pXVar3;
    if (pXVar3 == (X509_STORE *)0x0) {
      return false;
    }
  }
  if (param_2 != 0) {
    iVar2 = X509_STORE_load_path(pXVar3);
    return iVar2 != 0;
  }
  return true;
}