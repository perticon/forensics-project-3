undefined4 ossl_statem_client_write_transition(long param_1)

{
  int iVar1;
  undefined8 uVar2;
  uint uVar3;
  undefined4 uVar4;
  
  uVar4 = *(undefined4 *)(param_1 + 0x5c);
  uVar3 = *(uint *)(*(long *)(*(int **)(param_1 + 8) + 0x30) + 0x60) & 8;
  if (((uVar3 == 0) && (iVar1 = **(int **)(param_1 + 8), iVar1 != 0x10000)) && (0x303 < iVar1)) {
    switch(uVar4) {
    default:
      ERR_new();
      uVar2 = 0x1a3;
      goto LAB_0016c82b;
    case 1:
      if (*(int *)(param_1 + 0xba4) == -1) {
        return 2;
      }
      *(undefined4 *)(param_1 + 0x5c) = 0x2b;
      return uVar4;
    case 7:
      if (*(int *)(param_1 + 0xba8) == 4) {
LAB_0016c8e0:
        *(undefined4 *)(param_1 + 0x5c) = 0xd;
        return 1;
      }
      if ((*(byte *)(param_1 + 0x44) & 1) != 0) goto switchD_0016c649_caseD_9;
      ERR_new();
      uVar2 = 0x1b1;
LAB_0016c82b:
      ERR_set_debug("ssl/statem/statem_clnt.c",uVar2,"ossl_statem_client13_write_transition");
      ossl_statem_fatal(param_1,0x50,0xc0103,0);
      return 0;
    case 9:
    case 0x12:
    case 0x2b:
    case 0x2d:
      goto switchD_0016c649_caseD_9;
    case 0xb:
      if ((*(uint *)(param_1 + 0x84) & 0xfffffffb) == 3) {
        *(undefined4 *)(param_1 + 0x5c) = 0x2f;
        return 1;
      }
      if (((*(byte *)(param_1 + 0x9ea) & 0x10) != 0) && (*(int *)(param_1 + 0x8e8) == 0))
      goto switchD_0016c5ef_caseD_f;
      break;
    case 0xd:
      *(uint *)(param_1 + 0x5c) = (uint)(*(int *)(param_1 + 0x2f0) != 1) * 3 + 0xf;
      return 1;
    case 0xf:
      goto switchD_0016c649_caseD_f;
    case 0x10:
    case 0x30:
      break;
    case 0x2f:
      if (*(int *)(param_1 + 0xb30) == 2) {
        *(undefined4 *)(param_1 + 0x5c) = 0x30;
        return 1;
      }
    }
    *(uint *)(param_1 + 0x5c) = (-(uint)(*(int *)(param_1 + 0x2f0) == 0) & 5) + 0xd;
    return 1;
  }
  switch(uVar4) {
  case 0:
  case 2:
    goto switchD_0016c5ef_caseD_0;
  case 1:
    iVar1 = *(int *)(param_1 + 0xba0);
    uVar4 = 2;
    goto joined_r0x0016c905;
  case 3:
    if (((*(byte *)(param_1 + 0x9ea) & 0x10) == 0) || (*(int *)(param_1 + 0x84) == 7))
    goto switchD_0016c5ef_caseD_0;
    break;
  default:
    ERR_new();
    ERR_set_debug("ssl/statem/statem_clnt.c",0x1ff,"ossl_statem_client_write_transition");
    ossl_statem_fatal(param_1,0x50,0xc0103,0);
    return 0;
  case 8:
    if (*(int *)(param_1 + 0x2f0) != 0) goto LAB_0016c8e0;
  case 0xd:
    *(undefined4 *)(param_1 + 0x5c) = 0xe;
    return 1;
  case 0xb:
    if (*(int *)(param_1 + 0x4d0) != 0) break;
    goto switchD_0016c649_caseD_9;
  case 0xc:
    if (*(int *)(param_1 + 0x84) != 2) {
      return 2;
    }
    if ((*(byte *)(param_1 + 0x9ea) & 0x10) == 0) {
LAB_0016c767:
      *(undefined4 *)(param_1 + 0x5c) = 0x2e;
      return 1;
    }
    break;
  case 0xe:
    *(uint *)(param_1 + 0x5c) = (*(int *)(param_1 + 0x2f0) != 1) + 0xf;
    if ((*(byte *)(param_1 + 0xa8) & 0x10) == 0) {
      return 1;
    }
    break;
  case 0xf:
    break;
  case 0x10:
    if (*(int *)(param_1 + 0x8e8) == 1) goto switchD_0016c5ef_caseD_0;
    if (*(int *)(param_1 + 0x84) == 2) goto LAB_0016c767;
    if ((uVar3 == 0) && (*(int *)(param_1 + 0x484) != 0)) {
      *(undefined4 *)(param_1 + 0x5c) = 0x11;
      return 1;
    }
  case 0x11:
switchD_0016c649_caseD_f:
    *(undefined4 *)(param_1 + 0x5c) = 0x12;
    return 1;
  case 0x12:
    if (*(int *)(param_1 + 0x4d0) == 0) {
      return 2;
    }
switchD_0016c649_caseD_9:
    *(undefined4 *)(param_1 + 0x5c) = 1;
    return 1;
  case 0x29:
    iVar1 = ssl3_renegotiate_check(param_1,1);
    if (iVar1 == 0) goto switchD_0016c649_caseD_9;
    uVar4 = 0;
    iVar1 = tls_setup_handshake(param_1);
joined_r0x0016c905:
    if (iVar1 == 0) {
      return uVar4;
    }
switchD_0016c5ef_caseD_0:
    *(undefined4 *)(param_1 + 0x5c) = 0xc;
    return 1;
  case 0x2e:
    return 2;
  }
switchD_0016c5ef_caseD_f:
  *(undefined4 *)(param_1 + 0x5c) = 0x10;
  return 1;
}