undefined8 tls1_set_peer_legacy_sigalg(long param_1,undefined8 param_2)

{
  long lVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined4 local_18 [2];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  lVar1 = ssl_cert_lookup_by_pkey(param_2,local_18);
  if (lVar1 != 0) {
    lVar1 = tls1_get_legacy_sigalg(param_1,local_18[0]);
    if (lVar1 != 0) {
      *(long *)(param_1 + 0x3b0) = lVar1;
      uVar2 = 1;
      goto LAB_00149668;
    }
  }
  uVar2 = 0;
LAB_00149668:
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}