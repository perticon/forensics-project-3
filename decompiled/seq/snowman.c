
void** fun_24b0(void** rdi, ...);

void** fun_2540();

signed char all_digits_p(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    int32_t r8d5;
    int32_t eax6;

    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 48) > 9) {
        return 0;
    } else {
        rax3 = fun_24b0(rdi);
        rax4 = fun_2540();
        *reinterpret_cast<unsigned char*>(&r8d5) = reinterpret_cast<uint1_t>(rax4 == rax3);
        eax6 = r8d5;
        return *reinterpret_cast<signed char*>(&eax6);
    }
}

void** g28;

void* xstrtold(void** rdi);

void** quote(void** rdi, ...);

void** fun_2490();

void fun_26a0();

int32_t usage();

int64_t quote_n();

void** fun_2720(void** rdi, ...);

void** fun_24f0(void** rdi, void** rsi);

void** fun_2550(void** rdi, int64_t rsi);

void fun_24c0();

void** fun_25b0(void** rdi);

void** scan_arg(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void** rax5;
    void** v6;
    void* eax7;
    void** rax8;
    void* rcx9;
    int64_t rdx10;
    int32_t v11;
    void** rax12;
    void** rax13;
    void** rax14;
    int1_t zf15;
    void** r14_16;
    void** rax17;
    void** rax18;
    void** r13_19;
    void** rax20;
    void* rax21;
    void** rdi22;
    void** rax23;
    void** rax24;
    void** rcx25;

    r12_3 = rdi;
    rbp4 = rsi;
    rax5 = g28;
    v6 = rax5;
    eax7 = xstrtold(rbp4);
    if (!*reinterpret_cast<signed char*>(&eax7)) {
        addr_42c4_2:
        quote(rbp4, rbp4);
        fun_2490();
        fun_26a0();
        usage();
    } else {
        __asm__("fld tword [rsp+0x10]");
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fucomip st0, st0");
        if (__intrinsic()) {
            addr_4272_4:
            quote_n();
            quote_n();
            fun_2490();
            fun_26a0();
            usage();
            goto addr_42c4_2;
        } else {
            rax8 = fun_2720(rbp4);
            rcx9 = *rax8;
            while ((*reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rcx9) + rdx10 * 2 + 1) & 32)) || *reinterpret_cast<signed char*>(&rdx10) == 43) {
                ++rbp4;
            }
            v11 = 0x7fffffff;
            rax12 = fun_24f0(rbp4, 46);
            if (rax12) 
                goto addr_400e_9;
            rax13 = fun_24f0(rbp4, 0x70);
            if (!rax13) 
                goto addr_4193_11;
        }
    }
    addr_400e_9:
    rax14 = fun_2550(rbp4, "xX");
    zf15 = *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp4) + reinterpret_cast<unsigned char>(rax14)) == 0;
    if (!zf15) 
        goto addr_4140_12;
    __asm__("fldz ");
    __asm__("fld tword [rsp]");
    __asm__("fmul st0, st1");
    __asm__("fucomip st0, st1");
    __asm__("fstp st0");
    if (__intrinsic()) 
        goto addr_4140_12;
    if (!zf15) 
        goto addr_4140_12;
    fun_24b0(rbp4, rbp4);
    if (!rax12) {
        *reinterpret_cast<int32_t*>(&r14_16) = 0;
        *reinterpret_cast<int32_t*>(&r14_16 + 4) = 0;
    } else {
        rax17 = fun_2550(rax12 + 1, "eE");
        r14_16 = rax17;
        if (reinterpret_cast<unsigned char>(rax17) > reinterpret_cast<unsigned char>(0x7fffffff) || (v11 = *reinterpret_cast<int32_t*>(&rax17), !!rax17)) {
            if (rbp4 == rax12) {
            }
        }
    }
    rax18 = fun_24f0(rbp4, 0x65);
    r13_19 = rax18;
    if (rax18) 
        goto addr_40b1_24;
    rax20 = fun_24f0(rbp4, 69);
    r13_19 = rax20;
    if (!rax20) {
        addr_4140_12:
        __asm__("movdqa xmm0, [rsp+0x10]");
        __asm__("movdqa xmm1, [rsp+0x20]");
        __asm__("movaps [r12], xmm0");
        __asm__("movaps [r12+0x10], xmm1");
        rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
        if (rax21) {
            fun_24c0();
            goto addr_4272_4;
        } else {
            return r12_3;
        }
    }
    addr_40b1_24:
    rdi22 = r13_19 + 1;
    rax23 = fun_25b0(rdi22);
    if (reinterpret_cast<signed char>(rax23) < reinterpret_cast<signed char>(0x8000000000000002)) {
        goto addr_40f3_30;
    }
    rax24 = fun_25b0(rdi22);
    if (reinterpret_cast<signed char>(rax24) < reinterpret_cast<signed char>(0)) {
        addr_40f3_30:
        fun_24b0(rbp4);
        if (rax12) {
            if (r13_19 == rax12 + 1) {
            }
        }
    } else {
        rcx25 = reinterpret_cast<void**>(static_cast<int64_t>(v11));
        if (reinterpret_cast<signed char>(rcx25) > reinterpret_cast<signed char>(rax24)) {
            rcx25 = rax24;
        }
        fun_24b0(rbp4);
        if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!r14_16)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax12)) && !(v11 - *reinterpret_cast<int32_t*>(&rcx25))) {
        }
        if (reinterpret_cast<unsigned char>(rax24) <= reinterpret_cast<unsigned char>(r14_16)) {
        }
        goto addr_4131_43;
    }
    addr_4131_43:
    goto addr_4140_12;
    addr_4193_11:
    v11 = 0;
    goto addr_400e_9;
}

void** stdout = reinterpret_cast<void**>(0);

void fun_2450(void** rdi);

int32_t* fun_23f0();

int32_t fun_2590(void** rdi, void** rsi, void** rdx, ...);

void** g1;

void** xmalloc(void** rdi, void** rsi, void** rdx);

void** fun_25c0(void** rdi, void** rsi, void** rdx, ...);

void** fun_2670(void** rdi, void** rsi, void** rdx);

uint32_t fun_2560(void** rdi, void** rsi, void** rdx, void** rcx);

void** separator = reinterpret_cast<void**>(0);

void** xrealloc(void** rdi, void** rsi, void** rdx);

void** fun_2680(void* rdi, void** rsi, void** rdx);

int64_t fun_2630(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_26d0();

void fun_23d0(void** rdi, ...);

void io_error(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rdi7;
    void** rax8;
    int32_t* rax9;
    void** rdx10;
    void** rsi11;
    void** rbp12;
    void** rbx13;
    void** rsi14;
    void** v15;
    int32_t eax16;
    int1_t zf17;
    void** v18;
    void** rax19;
    void** r14_20;
    void** r10_21;
    void** rax22;
    void** rax23;
    void** r15_24;
    void** r12_25;
    void** rcx26;
    void** v27;
    void** rax28;
    void** rdx29;
    void** rsi30;
    void** v31;
    void** rax32;
    void** v33;
    void** r15_34;
    void** rax35;
    void** rcx36;
    void** rax37;
    void** rax38;
    void** rax39;
    void** rax40;
    void** rcx41;
    void** rcx42;
    void** rax43;
    void** v44;
    void** rax45;
    void** r14_46;
    void* v47;
    void** rax48;
    void** rcx49;
    unsigned char r13b50;
    void** rsi51;
    void** rax52;
    uint32_t eax53;
    void** rax54;
    void** r10_55;
    uint32_t eax56;
    void** rcx57;
    void** rax58;
    void** rdx59;
    void** rax60;
    void** rcx61;
    void** rax62;
    void** rax63;
    void** rdi64;
    void** rcx65;
    int64_t rax66;
    int64_t rax67;
    uint32_t eax68;

    rdi7 = stdout;
    fun_2450(rdi7);
    rax8 = fun_2490();
    rax9 = fun_23f0();
    rdx10 = rax8;
    *reinterpret_cast<int32_t*>(&rsi11) = *rax9;
    *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
    fun_26a0();
    rbp12 = reinterpret_cast<void**>(1);
    rbx13 = rsi11;
    rsi14 = reinterpret_cast<void**>("inf");
    v15 = rdx10;
    eax16 = fun_2590(rbx13, "inf", rdx10);
    zf17 = reinterpret_cast<int1_t>(g1 == 48);
    *reinterpret_cast<int32_t*>(&v18) = eax16;
    if (zf17) {
        rax19 = reinterpret_cast<void**>(1);
        do {
            r14_20 = rax19;
            *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax19 + 1));
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            ++rax19;
        } while (rdx10 == 48);
        if (1 == rax19) 
            goto addr_393c_6;
        if (!rdx10) 
            goto addr_3821_8;
        addr_393c_6:
        r14_20 = rax19;
        goto addr_3821_8;
    }
    while (1) {
        r14_20 = rbp12;
        addr_3821_8:
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx13) == 48)) {
            r10_21 = rbx13;
        } else {
            rax22 = rbx13;
            do {
                r10_21 = rax22;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ++rax22;
            } while (rdx10 == 48);
            if (rbx13 == rax22) 
                goto addr_3934_14;
            if (rdx10) 
                goto addr_3934_14;
        }
        addr_3851_16:
        *reinterpret_cast<int32_t*>(&rbx13) = 31;
        *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
        rax23 = fun_24b0(r14_20, r14_20);
        r15_24 = rax23 + 1;
        r12_25 = rax23;
        if (reinterpret_cast<unsigned char>(r15_24) >= reinterpret_cast<unsigned char>(31)) {
            rbx13 = r15_24;
        }
        if (!*reinterpret_cast<int32_t*>(&v18)) {
            rcx26 = rbx13 + 1;
            *reinterpret_cast<int32_t*>(&rbp12) = 0;
            *reinterpret_cast<int32_t*>(&rbp12 + 4) = 0;
            v27 = rcx26;
            rax28 = xmalloc(rcx26, rsi14, rdx10);
            rdx29 = r15_24;
            rsi30 = r14_20;
            v31 = rax28;
            rax32 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(rax23)) + reinterpret_cast<unsigned char>(rax28), rsi30, rdx29);
            v33 = reinterpret_cast<void**>(0);
            r15_34 = rax32;
        } else {
            rax35 = fun_24b0(r10_21, r10_21);
            rbp12 = rax35;
            if (reinterpret_cast<unsigned char>(rbx13) < reinterpret_cast<unsigned char>(rax35)) {
                rbx13 = rax35;
            }
            rcx36 = rbx13 + 1;
            v27 = rcx36;
            rax37 = xmalloc(rcx36, rsi14, rdx10);
            v31 = rax37;
            rax38 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(rax23)) + reinterpret_cast<unsigned char>(rax37), r14_20, r15_24);
            r15_34 = rax38;
            rax39 = xmalloc(v27, r14_20, r15_24);
            rsi30 = r10_21;
            rdx29 = rbp12 + 1;
            rax40 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(rbp12)) + reinterpret_cast<unsigned char>(rax39), rsi30, rdx29);
            rcx41 = v27;
            v33 = rax40;
            if (reinterpret_cast<unsigned char>(rax23) < reinterpret_cast<unsigned char>(rbp12)) 
                goto addr_3981_23; else 
                goto addr_3900_24;
        }
        addr_397c_25:
        rcx41 = v27;
        addr_3981_23:
        rcx42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx41) + reinterpret_cast<unsigned char>(rcx41));
        *reinterpret_cast<int32_t*>(&rax43) = 0x2000;
        *reinterpret_cast<int32_t*>(&rax43 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rcx42) >= reinterpret_cast<unsigned char>(0x2000)) {
            rax43 = rcx42;
        }
        v44 = rax43;
        rax45 = xmalloc(rax43, rsi30, rdx29);
        rdx10 = rax23;
        r14_46 = rax45;
        v47 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v44) + reinterpret_cast<unsigned char>(r14_46));
        rax48 = fun_2670(r14_46, r15_34, rdx10);
        rcx49 = rax48;
        r13b50 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&v18) == 0);
        while (1) {
            if (v15) {
                rsi51 = v15;
                do {
                    rax52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_34) + reinterpret_cast<unsigned char>(r12_25) + 0xffffffffffffffff);
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax52));
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        if (reinterpret_cast<signed char>(rdx10) <= reinterpret_cast<signed char>(56)) 
                            break;
                        --rax52;
                        *reinterpret_cast<void***>(rax52 + 1) = reinterpret_cast<void**>(48);
                    } while (reinterpret_cast<unsigned char>(rax52) >= reinterpret_cast<unsigned char>(r15_34));
                    goto addr_3ab0_33;
                    *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rdx10) + 1;
                    *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                    *reinterpret_cast<void***>(rax52) = rdx10;
                    continue;
                    addr_3ab0_33:
                    *reinterpret_cast<signed char*>(r15_34 + 0xffffffffffffffff) = 49;
                    ++r12_25;
                    --r15_34;
                    --rsi51;
                } while (rsi51);
            }
            if (reinterpret_cast<unsigned char>(r12_25) < reinterpret_cast<unsigned char>(rbp12)) 
                goto addr_3a49_37;
            if (r13b50) 
                goto addr_3a49_37;
            if (reinterpret_cast<unsigned char>(r12_25) > reinterpret_cast<unsigned char>(rbp12)) 
                break;
            rdx10 = r12_25;
            v18 = rcx49;
            eax53 = fun_2560(r15_34, v33, rdx10, rcx49);
            rcx49 = v18;
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax53) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax53 == 0))) 
                break;
            addr_3a49_37:
            rax54 = separator;
            r10_55 = rcx49 + 1;
            eax56 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax54));
            *reinterpret_cast<void***>(rcx49) = *reinterpret_cast<void***>(&eax56);
            if (rbx13 == r12_25 && (rbx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<unsigned char>(rbx13)), rcx57 = rbx13 + 1, v18 = rcx57, rax58 = xrealloc(v31, rcx57, rdx10), rdx59 = r12_25 + 1, v31 = rax58, rax60 = fun_2680(reinterpret_cast<unsigned char>(rax58) + reinterpret_cast<unsigned char>(r12_25), rax58, rdx59), r10_55 = r10_55, r15_34 = rax60, rcx61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(v18)), reinterpret_cast<unsigned char>(rcx61) > reinterpret_cast<unsigned char>(v44))) {
                v18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_55) - reinterpret_cast<unsigned char>(r14_46));
                rax62 = xrealloc(r14_46, rcx61, rdx59);
                r14_46 = rax62;
                v44 = rcx61;
                r10_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r14_46));
                v47 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax62) + reinterpret_cast<unsigned char>(rcx61));
            }
            rdx10 = r12_25;
            rax63 = fun_2670(r10_55, r15_34, rdx10);
            rcx49 = rax63;
            if (reinterpret_cast<unsigned char>(rcx49) <= reinterpret_cast<unsigned char>(~reinterpret_cast<unsigned char>(r12_25) + reinterpret_cast<uint64_t>(v47))) 
                continue;
            *reinterpret_cast<uint32_t*>(&rdx10) = 1;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rdi64 = r14_46;
            rsi14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx49) - reinterpret_cast<unsigned char>(r14_46));
            rcx65 = stdout;
            rax66 = fun_2630(rdi64, rsi14, 1, rcx65);
            if (rax66 != 1) 
                goto addr_3b8a_44;
            rcx49 = r14_46;
        }
        *reinterpret_cast<void***>(rcx49) = reinterpret_cast<void**>(10);
        *reinterpret_cast<uint32_t*>(&rdx10) = 1;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rdi64 = r14_46;
        rsi14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx49 + 1) - reinterpret_cast<unsigned char>(r14_46));
        rcx65 = stdout;
        rax67 = fun_2630(rdi64, rsi14, 1, rcx65);
        if (rax67 - 1) 
            break;
        fun_26d0();
        continue;
        addr_3900_24:
        if (reinterpret_cast<unsigned char>(rax23) > reinterpret_cast<unsigned char>(rbp12)) 
            goto addr_3914_48;
        rdx29 = rax23;
        rsi30 = rax40;
        eax68 = fun_2560(r15_34, rsi30, rdx29, rcx41);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax68) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax68 == 0)) 
            goto addr_397c_25; else 
            goto addr_3914_48;
        addr_3934_14:
        r10_21 = rax22;
        goto addr_3851_16;
    }
    addr_3b8a_44:
    io_error(rdi64, rsi14, 1, rcx65, r8, r9);
    addr_3914_48:
    fun_23d0(v31, v31);
}

void seq_fast(void** rdi, void** rsi, void** rdx) {
    void** rbp4;
    void** rbx5;
    void** rsi6;
    void** v7;
    int32_t eax8;
    int1_t zf9;
    void** v10;
    void** rax11;
    void** r14_12;
    void** r10_13;
    void** rax14;
    void** rax15;
    void** r15_16;
    void** r12_17;
    void** rcx18;
    void** v19;
    void** rax20;
    void** rdx21;
    void** rsi22;
    void** v23;
    void** rax24;
    void** v25;
    void** r15_26;
    void** rax27;
    void** rcx28;
    void** rax29;
    void** rax30;
    void** rax31;
    void** rax32;
    void** rcx33;
    void** rcx34;
    void** rax35;
    void** v36;
    void** rax37;
    void** r14_38;
    void* v39;
    void** rax40;
    void** rcx41;
    unsigned char r13b42;
    void** rsi43;
    void** rax44;
    uint32_t eax45;
    void** rax46;
    void** r10_47;
    uint32_t eax48;
    void** rcx49;
    void** rax50;
    void** rdx51;
    void** rax52;
    void** rcx53;
    void** rax54;
    void** rax55;
    void** rdi56;
    void** rcx57;
    int64_t rax58;
    int64_t rax59;
    uint32_t eax60;
    void** r8_61;
    void** r9_62;

    rbp4 = rdi;
    rbx5 = rsi;
    rsi6 = reinterpret_cast<void**>("inf");
    v7 = rdx;
    eax8 = fun_2590(rbx5, "inf", rdx);
    zf9 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp4) == 48);
    *reinterpret_cast<int32_t*>(&v10) = eax8;
    if (zf9) {
        rax11 = rbp4;
        do {
            r14_12 = rax11;
            *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax11 + 1));
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            ++rax11;
        } while (rdx == 48);
        if (rbp4 == rax11) 
            goto addr_393c_5;
        if (!rdx) 
            goto addr_3821_7;
        addr_393c_5:
        r14_12 = rax11;
        goto addr_3821_7;
    }
    while (1) {
        r14_12 = rbp4;
        addr_3821_7:
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx5) == 48)) {
            r10_13 = rbx5;
        } else {
            rax14 = rbx5;
            do {
                r10_13 = rax14;
                *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax14 + 1));
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                ++rax14;
            } while (rdx == 48);
            if (rbx5 == rax14) 
                goto addr_3934_13;
            if (rdx) 
                goto addr_3934_13;
        }
        addr_3851_15:
        *reinterpret_cast<int32_t*>(&rbx5) = 31;
        *reinterpret_cast<int32_t*>(&rbx5 + 4) = 0;
        rax15 = fun_24b0(r14_12, r14_12);
        r15_16 = rax15 + 1;
        r12_17 = rax15;
        if (reinterpret_cast<unsigned char>(r15_16) >= reinterpret_cast<unsigned char>(31)) {
            rbx5 = r15_16;
        }
        if (!*reinterpret_cast<int32_t*>(&v10)) {
            rcx18 = rbx5 + 1;
            *reinterpret_cast<int32_t*>(&rbp4) = 0;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            v19 = rcx18;
            rax20 = xmalloc(rcx18, rsi6, rdx);
            rdx21 = r15_16;
            rsi22 = r14_12;
            v23 = rax20;
            rax24 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx5) - reinterpret_cast<unsigned char>(rax15)) + reinterpret_cast<unsigned char>(rax20), rsi22, rdx21);
            v25 = reinterpret_cast<void**>(0);
            r15_26 = rax24;
        } else {
            rax27 = fun_24b0(r10_13, r10_13);
            rbp4 = rax27;
            if (reinterpret_cast<unsigned char>(rbx5) < reinterpret_cast<unsigned char>(rax27)) {
                rbx5 = rax27;
            }
            rcx28 = rbx5 + 1;
            v19 = rcx28;
            rax29 = xmalloc(rcx28, rsi6, rdx);
            v23 = rax29;
            rax30 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx5) - reinterpret_cast<unsigned char>(rax15)) + reinterpret_cast<unsigned char>(rax29), r14_12, r15_16);
            r15_26 = rax30;
            rax31 = xmalloc(v19, r14_12, r15_16);
            rsi22 = r10_13;
            rdx21 = rbp4 + 1;
            rax32 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx5) - reinterpret_cast<unsigned char>(rbp4)) + reinterpret_cast<unsigned char>(rax31), rsi22, rdx21);
            rcx33 = v19;
            v25 = rax32;
            if (reinterpret_cast<unsigned char>(rax15) < reinterpret_cast<unsigned char>(rbp4)) 
                goto addr_3981_22; else 
                goto addr_3900_23;
        }
        addr_397c_24:
        rcx33 = v19;
        addr_3981_22:
        rcx34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx33) + reinterpret_cast<unsigned char>(rcx33));
        *reinterpret_cast<int32_t*>(&rax35) = 0x2000;
        *reinterpret_cast<int32_t*>(&rax35 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rcx34) >= reinterpret_cast<unsigned char>(0x2000)) {
            rax35 = rcx34;
        }
        v36 = rax35;
        rax37 = xmalloc(rax35, rsi22, rdx21);
        rdx = rax15;
        r14_38 = rax37;
        v39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v36) + reinterpret_cast<unsigned char>(r14_38));
        rax40 = fun_2670(r14_38, r15_26, rdx);
        rcx41 = rax40;
        r13b42 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&v10) == 0);
        while (1) {
            if (v7) {
                rsi43 = v7;
                do {
                    rax44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_26) + reinterpret_cast<unsigned char>(r12_17) + 0xffffffffffffffff);
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax44));
                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                        if (reinterpret_cast<signed char>(rdx) <= reinterpret_cast<signed char>(56)) 
                            break;
                        --rax44;
                        *reinterpret_cast<void***>(rax44 + 1) = reinterpret_cast<void**>(48);
                    } while (reinterpret_cast<unsigned char>(rax44) >= reinterpret_cast<unsigned char>(r15_26));
                    goto addr_3ab0_32;
                    *reinterpret_cast<uint32_t*>(&rdx) = *reinterpret_cast<uint32_t*>(&rdx) + 1;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    *reinterpret_cast<void***>(rax44) = rdx;
                    continue;
                    addr_3ab0_32:
                    *reinterpret_cast<signed char*>(r15_26 + 0xffffffffffffffff) = 49;
                    ++r12_17;
                    --r15_26;
                    --rsi43;
                } while (rsi43);
            }
            if (reinterpret_cast<unsigned char>(r12_17) < reinterpret_cast<unsigned char>(rbp4)) 
                goto addr_3a49_36;
            if (r13b42) 
                goto addr_3a49_36;
            if (reinterpret_cast<unsigned char>(r12_17) > reinterpret_cast<unsigned char>(rbp4)) 
                break;
            rdx = r12_17;
            v10 = rcx41;
            eax45 = fun_2560(r15_26, v25, rdx, rcx41);
            rcx41 = v10;
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax45) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax45 == 0))) 
                break;
            addr_3a49_36:
            rax46 = separator;
            r10_47 = rcx41 + 1;
            eax48 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax46));
            *reinterpret_cast<void***>(rcx41) = *reinterpret_cast<void***>(&eax48);
            if (rbx5 == r12_17 && (rbx5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rbx5)), rcx49 = rbx5 + 1, v10 = rcx49, rax50 = xrealloc(v23, rcx49, rdx), rdx51 = r12_17 + 1, v23 = rax50, rax52 = fun_2680(reinterpret_cast<unsigned char>(rax50) + reinterpret_cast<unsigned char>(r12_17), rax50, rdx51), r10_47 = r10_47, r15_26 = rax52, rcx53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(v10)), reinterpret_cast<unsigned char>(rcx53) > reinterpret_cast<unsigned char>(v36))) {
                v10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_47) - reinterpret_cast<unsigned char>(r14_38));
                rax54 = xrealloc(r14_38, rcx53, rdx51);
                r14_38 = rax54;
                v36 = rcx53;
                r10_47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(r14_38));
                v39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax54) + reinterpret_cast<unsigned char>(rcx53));
            }
            rdx = r12_17;
            rax55 = fun_2670(r10_47, r15_26, rdx);
            rcx41 = rax55;
            if (reinterpret_cast<unsigned char>(rcx41) <= reinterpret_cast<unsigned char>(~reinterpret_cast<unsigned char>(r12_17) + reinterpret_cast<uint64_t>(v39))) 
                continue;
            *reinterpret_cast<uint32_t*>(&rdx) = 1;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rdi56 = r14_38;
            rsi6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx41) - reinterpret_cast<unsigned char>(r14_38));
            rcx57 = stdout;
            rax58 = fun_2630(rdi56, rsi6, 1, rcx57);
            if (rax58 != 1) 
                goto addr_3b8a_43;
            rcx41 = r14_38;
        }
        *reinterpret_cast<void***>(rcx41) = reinterpret_cast<void**>(10);
        *reinterpret_cast<uint32_t*>(&rdx) = 1;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rdi56 = r14_38;
        rsi6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx41 + 1) - reinterpret_cast<unsigned char>(r14_38));
        rcx57 = stdout;
        rax59 = fun_2630(rdi56, rsi6, 1, rcx57);
        if (rax59 - 1) 
            break;
        fun_26d0();
        continue;
        addr_3900_23:
        if (reinterpret_cast<unsigned char>(rax15) > reinterpret_cast<unsigned char>(rbp4)) 
            goto addr_3914_47;
        rdx21 = rax15;
        rsi22 = rax32;
        eax60 = fun_2560(r15_26, rsi22, rdx21, rcx33);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax60) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax60 == 0)) 
            goto addr_397c_24; else 
            goto addr_3914_47;
        addr_3934_13:
        r10_13 = rax14;
        goto addr_3851_15;
    }
    addr_3b8a_43:
    io_error(rdi56, rsi6, 1, rcx57, r8_61, r9_62);
    addr_3914_47:
    fun_23d0(v23, v23);
}

int64_t fun_24a0();

int64_t fun_23e0(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24a0();
    if (r8d > 10) {
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa960 + rax11 * 4) + 0xa960;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_2530();

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s1* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x5a5f;
    rax8 = fun_23f0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xe070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x8471]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5aeb;
            fun_2530();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xe120) {
                fun_23d0(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5b7a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_24c0();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xe080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s2 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s2* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, int64_t rdx) {
    struct s2* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xa901);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xa8fc);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xa905);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xa8f8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gddf8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gddf8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23b3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __snprintf_chk = 0x2030;

void fun_23c3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2040;

void fun_23d3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_23e3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_23f3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2403() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2413() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x2090;

void fun_2423() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x20a0;

void fun_2433() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20b0;

void fun_2443() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clearerr_unlocked = 0x20c0;

void fun_2453() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x20d0;

void fun_2463() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_2473() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20f0;

void fun_2483() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_2493() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_24a3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_24b3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2130;

void fun_24c3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2140;

void fun_24d3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2150;

void fun_24e3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2160;

void fun_24f3() {
    __asm__("cli ");
    goto strchr;
}

int64_t newlocale = 0x2170;

void fun_2503() {
    __asm__("cli ");
    goto newlocale;
}

int64_t strrchr = 0x2180;

void fun_2513() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2190;

void fun_2523() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x21a0;

void fun_2533() {
    __asm__("cli ");
    goto memset;
}

int64_t strspn = 0x21b0;

void fun_2543() {
    __asm__("cli ");
    goto strspn;
}

int64_t strcspn = 0x21c0;

void fun_2553() {
    __asm__("cli ");
    goto strcspn;
}

int64_t memcmp = 0x21d0;

void fun_2563() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2573() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2583() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2593() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_25a3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t strtol = 0x2220;

void fun_25b3() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x2230;

void fun_25c3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2240;

void fun_25d3() {
    __asm__("cli ");
    goto fileno;
}

int64_t uselocale = 0x2250;

void fun_25e3() {
    __asm__("cli ");
    goto uselocale;
}

int64_t malloc = 0x2260;

void fun_25f3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2270;

void fun_2603() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2280;

void fun_2613() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2290;

void fun_2623() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x22a0;

void fun_2633() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x22b0;

void fun_2643() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22c0;

void fun_2653() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22d0;

void fun_2663() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x22e0;

void fun_2673() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x22f0;

void fun_2683() {
    __asm__("cli ");
    goto memmove;
}

int64_t strtold = 0x2300;

void fun_2693() {
    __asm__("cli ");
    goto strtold;
}

int64_t error = 0x2310;

void fun_26a3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2320;

void fun_26b3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x2330;

void fun_26c3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2340;

void fun_26d3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2350;

void fun_26e3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2360;

void fun_26f3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2370;

void fun_2703() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2380;

void fun_2713() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2390;

void fun_2723() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x23a0;

void fun_2733() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_2650(void** rdi, ...);

unsigned char locale_ok = 0;

void fun_2480(int64_t rdi, void** rsi);

void fun_2460(int64_t rdi, void** rsi);

void atexit(int64_t rdi, void** rsi);

void* optind = reinterpret_cast<void*>(0);

signed char equal_width = 0;

int32_t fun_2660(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, int64_t a10, void** a11, int64_t a12, void** a13, int64_t a14, void** a15, int64_t a16, void** a17, int64_t a18, int64_t a19, int64_t a20);

uint32_t fun_2570(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, int64_t a10, void** a11, int64_t a12, void** a13, int64_t a14, void** a15, int64_t a16, void** a17, int64_t a18, int64_t a19, ...);

int32_t rpl_asprintf(void** rdi, void** rsi, void** rdx, ...);

struct s3 {
    signed char[2] pad2;
    void** f2;
};

void fun_2420(uint64_t rdi, void** rsi);

int64_t Version = 0xa8a4;

void version_etc(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8);

void** optarg = reinterpret_cast<void**>(0);

int32_t fun_24d0(int64_t rdi, void** rsi, void* rdx, void* rcx);

void** xstrdup(int64_t rdi, void** rsi, void** rdx);

void xalloc_die();

void fun_2730(int64_t rdi, int64_t rsi, void** rdx);

void fun_2793(void* edi, void** rsi) {
    void** r15_3;
    void** r12_4;
    int64_t rbx5;
    void** rdi6;
    void** rax7;
    void** rsi8;
    void** rbp9;
    void*** rsp10;
    int64_t rdx11;
    void* r14_12;
    void* r13_13;
    uint1_t below_or_equal14;
    void*** rsp15;
    void** rsi16;
    void** rdi17;
    void** rdx18;
    void** rcx19;
    void** r8_20;
    void** r9_21;
    void** v22;
    void** v23;
    void** v24;
    int64_t v25;
    void** v26;
    int64_t v27;
    int64_t v28;
    void** v29;
    int64_t v30;
    void** v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int32_t eax35;
    uint1_t zf36;
    uint1_t below_or_equal37;
    void** v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    void** v43;
    int64_t v44;
    int64_t v45;
    uint32_t eax46;
    uint32_t tmp32_47;
    uint1_t zf48;
    int1_t zf49;
    void** rdi50;
    int32_t eax51;
    int1_t zf52;
    void** rsi53;
    void** rdi54;
    void*** rsp55;
    int64_t v56;
    void* v57;
    void* eax58;
    void* rsp59;
    int1_t zf60;
    int32_t eax61;
    void** v62;
    int32_t eax63;
    void** v64;
    void** rax65;
    void** rax66;
    void* rsp67;
    void** rdx68;
    void** rax69;
    void* rdi70;
    void* r14_71;
    void** rax72;
    int32_t r8d73;
    void** rsi74;
    void** rax75;
    void* rsp76;
    void** rdx77;
    struct s3* rdi78;
    void* rax79;
    uint32_t ecx80;
    void* rcx81;
    void** rax82;
    void** rax83;
    int1_t zf84;
    void** rcx85;
    void** r8_86;
    signed char al87;
    void*** rsp88;
    void** r8_89;
    void** rdi90;
    signed char al91;
    void** rdi92;
    signed char al93;
    void* rsp94;
    void* edx95;
    void* eax96;
    uint1_t zf97;
    void** rcx98;
    void** r8_99;
    signed char al100;
    void** rcx101;
    void** rdi102;
    signed char al103;
    void** rdi104;
    signed char al105;
    void** rsi106;
    void** rdi107;
    int64_t rax108;
    uint32_t v109;
    void** rdi110;
    int1_t zf111;
    void** rdi112;
    void** rax113;
    void** r8_114;
    void** r10_115;
    int64_t rax116;
    int64_t rax117;
    void** rsi118;
    void** rsi119;
    void** v120;
    void* edx121;
    void** r10_122;
    void** v123;
    uint32_t eax124;
    uint32_t v125;
    int64_t rax126;
    void** rdi127;
    void** rax128;
    void* rsp129;
    void** rdi130;
    int64_t rcx131;
    void** rax132;
    int32_t eax133;
    int64_t rdi134;
    int32_t eax135;
    void** eax136;
    void** rdi137;
    void** rax138;
    void* rdx139;
    void* rax140;
    uint32_t ecx141;
    void* rcx142;
    void** rax143;
    uint1_t zf144;
    uint1_t below_or_equal145;
    int1_t zf146;
    uint1_t zf147;
    void** rdi148;
    void** rax149;
    int32_t eax150;
    int64_t v151;
    void** rax152;
    void** v153;
    int32_t eax154;
    void** v155;
    void** v156;
    int1_t zf157;
    int1_t zf158;
    unsigned char sil159;
    uint1_t zf160;
    uint1_t zf161;
    uint1_t zf162;
    void** rsi163;
    void** v164;
    uint32_t v165;
    void** rax166;
    void** v167;
    int64_t v168;
    int64_t v169;
    int64_t v170;
    int64_t v171;
    void** v172;
    int64_t v173;
    int64_t v174;
    uint32_t eax175;
    uint32_t tmp32_176;
    uint1_t zf177;
    void** v178;

    __asm__("cli ");
    __asm__("fld1 ");
    r15_3 = rsi;
    r12_4 = reinterpret_cast<void**>(0xad21);
    *reinterpret_cast<void**>(&rbx5) = edi;
    rdi6 = *reinterpret_cast<void***>(rsi);
    __asm__("fstp tword [rsp+0xa0]");
    set_program_name(rdi6);
    rax7 = fun_2650(6, 6);
    rsi8 = reinterpret_cast<void**>("/usr/local/share/locale");
    locale_ok = reinterpret_cast<uint1_t>(!!rax7);
    fun_2480("coreutils", "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&rbp9) = 0;
    *reinterpret_cast<int32_t*>(&rbp9 + 4) = 0;
    fun_2460("coreutils", "/usr/local/share/locale");
    atexit(0x43d0, "/usr/local/share/locale");
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xf8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    rdx11 = reinterpret_cast<int32_t>(optind);
    equal_width = 0;
    separator = reinterpret_cast<void**>("\n");
    if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)) <= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx11))) 
        goto addr_28f0_2;
    r14_12 = reinterpret_cast<void*>(0xdae0);
    r13_13 = reinterpret_cast<void*>("+f:s:w");
    goto addr_2861_4;
    addr_35a3_5:
    quote(rbp9, rbp9);
    fun_2490();
    fun_26a0();
    addr_35d5_6:
    quote(rbp9, rbp9);
    fun_2490();
    fun_26a0();
    addr_3607_7:
    quote(rbp9, rbp9);
    fun_2490();
    fun_26a0();
    addr_2d75_8:
    goto addr_3607_7;
    while (1) {
        addr_2e66_9:
        __asm__("fld tword [rsp+0x40]");
        __asm__("fld tword [rsp+0x30]");
        __asm__("fcomip st0, st1");
        __asm__("fstp st0");
        *reinterpret_cast<unsigned char*>(&rbx5) = reinterpret_cast<uint1_t>(!below_or_equal14);
        while (1) {
            if (*reinterpret_cast<unsigned char*>(&rbx5)) {
                addr_2cd0_11:
                fun_26d0();
                rsp10 = rsp15 - 8 + 8;
            } else {
                __asm__("fld tword [rsp+0x40]");
                __asm__("fstp tword [rsp+0x20]");
                __asm__("fld1 ");
                __asm__("fstp tword [rsp+0x10]");
                while (rsi16 = rbp9, *reinterpret_cast<int32_t*>(&rdi17) = 1, *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0, eax35 = fun_2660(1, rsi16, rdx18, rcx19, r8_20, r9_21, v22, v23, v24, v25, v26, v27, v23, v28, v29, v30, v31, v32, v33, v34), r8_20 = v22, r9_21 = v23, rsp15 = rsp15 - 8 - 8 - 8 + 8 + 8 + 8, below_or_equal14 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(eax35 == 0))), eax35 >= 0) {
                    zf36 = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rbx5) == 0);
                    below_or_equal37 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf36));
                    if (!zf36) 
                        goto addr_2e45_15;
                    __asm__("fld tword [rsp+0x50]");
                    __asm__("fld st0");
                    __asm__("fld tword [rsp+0x10]");
                    __asm__("fmulp st1, st0");
                    __asm__("fld tword [rsp+0x40]");
                    __asm__("faddp st1, st0");
                    __asm__("fld st0");
                    __asm__("fstp tword [rsp]");
                    __asm__("fldz ");
                    __asm__("fcomip st0, st2");
                    __asm__("fstp st1");
                    if (!below_or_equal37) {
                        __asm__("fld tword [rsp+0x30]");
                        __asm__("fcomip st0, st1");
                        __asm__("fstp st0");
                        if (!below_or_equal37) 
                            goto addr_2af8_18;
                        goto addr_2c0c_20;
                    }
                    __asm__("fstp st0");
                    __asm__("fld tword [rsp+0x30]");
                    __asm__("fld tword [rsp]");
                    __asm__("fcomip st0, st1");
                    __asm__("fstp st0");
                    if (below_or_equal37) {
                        addr_2c0c_20:
                        rsi16 = stdout;
                        rdi17 = separator;
                        eax46 = fun_2570(rdi17, rsi16, rdx18, rcx19, r8_20, r9_21, v24, v38, v26, v39, v23, v40, v29, v41, v31, v42, v43, v44, v45);
                        rsp15 = rsp15 - 8 + 8;
                        tmp32_47 = eax46 + 1;
                        zf48 = reinterpret_cast<uint1_t>(tmp32_47 == 0);
                        below_or_equal14 = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(tmp32_47 < eax46) | zf48);
                        if (zf48) 
                            break;
                    } else {
                        addr_2af8_18:
                        zf49 = locale_ok == 0;
                        if (!zf49) {
                            fun_2650(1, 1);
                            rsp15 = rsp15 - 8 + 8;
                            goto addr_2b05_23;
                        }
                    }
                    __asm__("fld tword [rsp+0x10]");
                    __asm__("fld1 ");
                    __asm__("faddp st1, st0");
                    __asm__("fstp tword [rsp+0x10]");
                    __asm__("fld tword [rsp]");
                    __asm__("fstp tword [rsp+0x20]");
                    continue;
                    addr_2b05_23:
                    rdi50 = reinterpret_cast<void**>(rsp15 + 0x80);
                    eax51 = rpl_asprintf(rdi50, rbp9, rdx18, rdi50, rbp9, rdx18);
                    zf52 = locale_ok == 0;
                    rsi53 = v24;
                    rbx5 = eax51;
                    rdi54 = v24;
                    rsp55 = rsp15 - 8 - 8 - 8 + 8 + 8 + 8;
                    if (!zf52) {
                        rsi53 = r12_4;
                        *reinterpret_cast<int32_t*>(&rdi54) = 1;
                        *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
                        fun_2650(1, 1);
                        rsp55 = rsp55 - 8 + 8;
                    }
                    if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5)) < reinterpret_cast<int32_t>(0)) 
                        goto addr_3572_27;
                    rcx19 = reinterpret_cast<void**>(0x4300);
                    rdx18 = reinterpret_cast<void**>(rsp55 + 0x90);
                    *reinterpret_cast<signed char*>(v56 + (rbx5 - reinterpret_cast<uint64_t>(r14_12))) = 0;
                    eax58 = xstrtold(reinterpret_cast<int64_t>(v57) + reinterpret_cast<uint64_t>(r13_13));
                    rsp59 = reinterpret_cast<void*>(rsp55 - 8 + 8);
                    *reinterpret_cast<void**>(&rbx5) = eax58;
                    zf60 = *reinterpret_cast<signed char*>(&eax58) == 0;
                    if (zf60) 
                        goto addr_2e38_29;
                    __asm__("fld tword [rsp+0x30]");
                    __asm__("fld tword [rsp+0x90]");
                    __asm__("fucomip st0, st1");
                    __asm__("fstp st0");
                    if (__intrinsic()) 
                        goto addr_2e38_29;
                    if (!zf60) 
                        goto addr_2e38_29;
                    rdi54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp59) + 0x88);
                    rsi53 = rbp9;
                    eax61 = rpl_asprintf(rdi54, rsi53, rdx18, rdi54, rsi53, rdx18);
                    rcx19 = v23;
                    rsp55 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp59) - 8 - 8 - 8 + 8 + 8 + 8);
                    if (eax61 < 0) 
                        goto addr_3572_27;
                    rdx18 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<signed char*>(eax61 - reinterpret_cast<uint64_t>(r14_12)) = 0;
                    v23 = reinterpret_cast<void**>(0);
                    eax63 = fun_2590(0, v62, 0);
                    *reinterpret_cast<int32_t*>(&r15_3) = eax63;
                    *reinterpret_cast<int32_t*>(&r15_3 + 4) = 0;
                    fun_23d0(0, 0);
                    fun_23d0(v64, v64);
                    rsp15 = rsp55 - 8 + 8 - 8 + 8 - 8 + 8;
                    if (!*reinterpret_cast<int32_t*>(&r15_3)) 
                        goto addr_2e45_15; else 
                        goto addr_2c0c_20;
                }
                break;
            }
            while (1) {
                rax65 = fun_2540();
                rax66 = fun_2540();
                rsp67 = reinterpret_cast<void*>(rsp10 - 8 + 8 - 8 + 8);
                rdx68 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_12) + reinterpret_cast<unsigned char>(rax65) + reinterpret_cast<unsigned char>(rax66));
                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(rdx68)) == 46) {
                    rax69 = fun_2540();
                    rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                    rdx68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx68 + 1) + reinterpret_cast<unsigned char>(rax69));
                }
                *reinterpret_cast<int32_t*>(&rdi70) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi70) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdi70) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(rdx68)) == 76);
                r14_71 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx68) + reinterpret_cast<uint64_t>(rdi70));
                rax72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r14_71));
                r8d73 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax72));
                v23 = rax72;
                if (!*reinterpret_cast<signed char*>(&r8d73)) 
                    goto addr_35a3_5;
                *reinterpret_cast<int32_t*>(&rsi74) = r8d73;
                *reinterpret_cast<int32_t*>(&rsi74 + 4) = 0;
                v29 = rdx68;
                rax75 = fun_24f0("efgaEFGA", rsi74);
                rsp76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                rdx77 = v29;
                rdi78 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(r14_71) + 1);
                *reinterpret_cast<int32_t*>(&rax79) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax79) + 4) = 0;
                if (!rax75) 
                    goto addr_2d75_8;
                while (1) {
                    ecx80 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rdi78));
                    r14_12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax79) + 0xffffffffffffffff);
                    if (*reinterpret_cast<signed char*>(&ecx80) != 37) {
                        if (!*reinterpret_cast<signed char*>(&ecx80)) 
                            break;
                        *reinterpret_cast<int32_t*>(&rcx81) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx81) + 4) = 0;
                    } else {
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rdi78) + 1) != 37) 
                            goto addr_35d5_6;
                        *reinterpret_cast<int32_t*>(&rcx81) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx81) + 4) = 0;
                    }
                    rdi78 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<uint64_t>(rcx81));
                    rax79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax79) + 1);
                }
                rax82 = xmalloc(&rdi78->f2, rsi74, rdx77);
                rax83 = fun_25c0(rax82, rbp9, rdx77);
                rsi8 = v23;
                v26 = rax83;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax83) + reinterpret_cast<unsigned char>(rdx77)) = 76;
                fun_2420(reinterpret_cast<unsigned char>(rax83) + reinterpret_cast<unsigned char>(rdx77) + 1, rsi8);
                rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp76) - 8 + 8 - 8 + 8 - 8 + 8);
                zf84 = equal_width == 0;
                if (!zf84) 
                    break;
                rbp9 = v26;
                *reinterpret_cast<void**>(&rdx11) = optind;
                while (1) {
                    rcx85 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx11))));
                    if (v24 != 3) {
                        r8_86 = *reinterpret_cast<void***>(r15_3 + reinterpret_cast<unsigned char>(rcx85) * 8);
                        *reinterpret_cast<void**>(&v29) = *reinterpret_cast<void**>(&rdx11);
                        v23 = rcx85;
                        v26 = r8_86;
                        al87 = all_digits_p(r8_86, rsi8);
                        rsp88 = rsp10 - 8 + 8;
                        r8_89 = v26;
                        rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&v29))));
                        if (al87) {
                            __asm__("pxor xmm2, xmm2");
                            __asm__("movss [rsp+0x68], xmm2");
                            if (v24 == 1) 
                                goto addr_2f4b_49;
                            rdi90 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_3 + reinterpret_cast<unsigned char>(v23) * 8) + 8);
                            *reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&rdx18);
                            v26 = r8_89;
                            al91 = all_digits_p(rdi90, rsi8);
                            rsp88 = rsp88 - 8 + 8;
                            r8_89 = v26;
                            rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v23)));
                            if (!al91) 
                                goto addr_2a1f_51;
                            goto addr_2f4b_49;
                        } else {
                            __asm__("pxor xmm1, xmm1");
                            __asm__("movss [rsp+0x68], xmm1");
                            goto addr_2a1f_51;
                        }
                    }
                    rdi92 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_3 + reinterpret_cast<unsigned char>(rcx85) * 8) + 8);
                    al93 = all_digits_p(rdi92, rsi8);
                    rsp94 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                    __asm__("pxor xmm0, xmm0");
                    edx95 = *reinterpret_cast<void**>(&rdx11);
                    __asm__("movss [rsp+0x68], xmm0");
                    if (al93) {
                        *reinterpret_cast<int32_t*>(&rsi8) = 0;
                        *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                        eax96 = xstrtold(rdi92);
                        rsp94 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp94) - 8 + 8);
                        zf97 = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax96) == 0);
                        if (zf97) {
                            addr_33ae_56:
                            edx95 = optind;
                            goto addr_2ea8_57;
                        } else {
                            __asm__("fldz ");
                            __asm__("fld tword [rsp+0xa0]");
                            __asm__("fcomi st0, st1");
                            __asm__("fstp st1");
                            if (zf97) {
                                __asm__("fstp st0");
                                goto addr_33ae_56;
                            } else {
                                __asm__("fld dword [rip+0x751b]");
                                edx95 = optind;
                                __asm__("fcomip st0, st1");
                                __asm__("fstp st0");
                                *reinterpret_cast<unsigned char*>(&v31) = 1;
                            }
                        }
                    } else {
                        addr_2ea8_57:
                        *reinterpret_cast<unsigned char*>(&v31) = 0;
                    }
                    rcx98 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(edx95)));
                    *reinterpret_cast<void**>(&v29) = edx95;
                    r8_99 = *reinterpret_cast<void***>(r15_3 + reinterpret_cast<unsigned char>(rcx98) * 8);
                    v23 = rcx98;
                    v26 = r8_99;
                    al100 = all_digits_p(r8_99, rsi8);
                    rsp88 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp94) - 8 + 8);
                    rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&v29))));
                    if (!al100 || ((rcx101 = v23 + 1, *reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&rdx18), rdi102 = *reinterpret_cast<void***>(r15_3 + reinterpret_cast<unsigned char>(rcx101) * 8), v26 = v26, v29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx101) * 8), al103 = all_digits_p(rdi102, rsi8), rsp88 = rsp88 - 8 + 8, rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v23))), al103 == 0) || ((v26 = v26, *reinterpret_cast<unsigned char*>(&v31) == 0) || (*reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&rdx18), rdi104 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r15_3) + reinterpret_cast<unsigned char>(v29) + 8), al105 = all_digits_p(rdi104, rsi8), rsp88 = rsp88 - 8 + 8, rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v23))), r8_89 = v26, al105 == 0)))) {
                        addr_2a1f_51:
                        rsi106 = *reinterpret_cast<void***>(r15_3 + reinterpret_cast<unsigned char>(rdx18) * 8);
                        rdi107 = reinterpret_cast<void**>(rsp88 + 0xc0);
                        optind = reinterpret_cast<void*>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdx18 + 1)));
                        v24 = rdi107;
                        scan_arg(rdi107, rsi106);
                        rsp15 = rsp88 - 8 + 8;
                        rax108 = reinterpret_cast<int32_t>(optind);
                        __asm__("fld tword [rsp+0xc0]");
                        *reinterpret_cast<uint32_t*>(&r8_20) = v109;
                        *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
                        rdi110 = v24;
                        __asm__("fstp tword [rsp+0x40]");
                        if (reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rax108)) >= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5))) 
                            goto addr_2a6b_62;
                    } else {
                        addr_2f4b_49:
                        zf111 = equal_width == 1;
                        if (!zf111 && (!rbp9 && (rdi112 = separator, *reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&rdx18), v26 = r8_89, rax113 = fun_24b0(rdi112, rdi112), rsp88 = rsp88 - 8 + 8, r8_114 = v26, rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v23))), !(rax113 - 1)))) {
                            __asm__("fld dword [rip+0x7910]");
                            __asm__("fld tword [rsp+0xa0]");
                            if (v24 == 1) {
                                r8_114 = reinterpret_cast<void**>("1");
                            }
                            __asm__("fcomi st0, st1");
                            if (reinterpret_cast<unsigned char>(v24) >= reinterpret_cast<unsigned char>(1)) {
                                __asm__("fnstcw word [rsp+0x6e]");
                                __asm__("fsubrp st1, st0");
                                __asm__("fldcw word [rsp+0x6c]");
                                __asm__("fistp qword [rsp+0x10]");
                                __asm__("fldcw word [rsp+0x6e]");
                                r10_115 = v26;
                                __asm__("btc r10, 0x3f");
                            } else {
                                __asm__("fstp st1");
                                __asm__("fnstcw word [rsp+0x6e]");
                                __asm__("fldcw word [rsp+0x6c]");
                                __asm__("fistp qword [rsp+0x10]");
                                __asm__("fldcw word [rsp+0x6e]");
                                r10_115 = v26;
                            }
                            *reinterpret_cast<void***>(&rax116) = v24;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax116) + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rax117) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rax116 + reinterpret_cast<unsigned char>(rdx18)) - 1);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax117) + 4) = 0;
                            rsi118 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r15_3) + reinterpret_cast<uint64_t>(rax117 * 8));
                            seq_fast(r8_114, rsi118, r10_115);
                            rsp88 = rsp88 - 8 + 8;
                            rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(optind)));
                            goto addr_2a1f_51;
                        }
                    }
                    rsi119 = *reinterpret_cast<void***>(r15_3 + rax108 * 8);
                    *reinterpret_cast<uint32_t*>(&v26) = *reinterpret_cast<uint32_t*>(&r8_20);
                    v24 = v120;
                    optind = reinterpret_cast<void*>(static_cast<uint32_t>(rax108 + 1));
                    v23 = rdi110;
                    scan_arg(rdi110, rsi119);
                    rsp15 = rsp15 - 8 + 8;
                    edx121 = optind;
                    rcx19 = v24;
                    __asm__("fld tword [rsp+0xc0]");
                    r10_122 = v123;
                    eax124 = v125;
                    *reinterpret_cast<uint32_t*>(&r8_20) = *reinterpret_cast<uint32_t*>(&v26);
                    *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
                    __asm__("fld st0");
                    __asm__("fstp tword [rsp+0x30]");
                    if (reinterpret_cast<int32_t>(edx121) >= reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5))) 
                        goto addr_30e3_70;
                    __asm__("movdqa xmm3, [rsp+0xc0]");
                    __asm__("fldz ");
                    __asm__("fxch st0, st1");
                    rax126 = reinterpret_cast<int32_t>(edx121);
                    __asm__("movdqa xmm4, [rsp+0xd0]");
                    __asm__("fucomip st0, st1");
                    __asm__("fstp st0");
                    __asm__("movaps [rsp+0xa0], xmm3");
                    __asm__("movaps [rsp+0xb0], xmm4");
                    if (__intrinsic()) 
                        goto addr_30a3_72;
                    if (edx121 != *reinterpret_cast<void**>(&rbx5)) 
                        goto addr_30a3_72;
                    rdi127 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_3 + rax126 * 8) - 8);
                    rax128 = quote(rdi127, rdi127);
                    rsp129 = reinterpret_cast<void*>(rsp15 - 8 + 8);
                    r12_4 = rax128;
                    while (1) {
                        fun_2490();
                        fun_26a0();
                        rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp129) - 8 + 8 - 8 + 8);
                        while (1) {
                            while (1) {
                                addr_2974_76:
                                usage();
                                rsp10 = rsp10 - 8 + 8;
                                while (1) {
                                    addr_297e_77:
                                    rdi130 = stdout;
                                    rcx131 = Version;
                                    *reinterpret_cast<uint32_t*>(&r9_21) = 0;
                                    *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
                                    rsi8 = reinterpret_cast<void**>("seq");
                                    version_etc(rdi130, "seq", "GNU coreutils", rcx131, "Ulrich Drepper");
                                    fun_26d0();
                                    rsp10 = rsp10 - 8 + 8 - 8 + 8;
                                    while (1) {
                                        addr_29b2_78:
                                        rax132 = optarg;
                                        separator = rax132;
                                        while (rdx11 = reinterpret_cast<int32_t>(optind), reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx11)) < reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbx5))) {
                                            addr_2861_4:
                                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_3 + reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx11)) * 8)) == 45)) 
                                                goto addr_287d_80;
                                            eax133 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_3 + reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rdx11)) * 8) + 1));
                                            if (*reinterpret_cast<signed char*>(&eax133) == 46) 
                                                break;
                                            if (eax133 - 48 <= 9) 
                                                break;
                                            addr_287d_80:
                                            rsi8 = r15_3;
                                            *reinterpret_cast<void**>(&rdi134) = *reinterpret_cast<void**>(&rbx5);
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi134) + 4) = 0;
                                            eax135 = fun_24d0(rdi134, rsi8, r13_13, r14_12);
                                            rsp10 = rsp10 - 8 + 8;
                                            if (eax135 == -1) 
                                                goto addr_2cc4_83;
                                            if (eax135 == 0x66) {
                                                rbp9 = optarg;
                                            } else {
                                                if (eax135 <= 0x66) {
                                                    if (eax135 == 0xffffff7d) 
                                                        goto addr_297e_77;
                                                    if (eax135 != 0xffffff7e) 
                                                        goto addr_2974_76;
                                                    eax135 = usage();
                                                    rsp10 = rsp10 - 8 + 8;
                                                }
                                                if (eax135 == 0x73) 
                                                    goto addr_29b2_78;
                                                if (eax135 != 0x77) 
                                                    goto addr_2974_76;
                                                equal_width = 1;
                                            }
                                        }
                                        goto addr_28f0_2;
                                    }
                                }
                            }
                            addr_28f0_2:
                            eax136 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rbx5)) - reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&rdx11)));
                            v24 = eax136;
                            if (eax136) 
                                break;
                            fun_2490();
                            fun_26a0();
                            rsp10 = rsp10 - 8 + 8 - 8 + 8;
                            continue;
                            addr_2cc4_83:
                            rdx11 = reinterpret_cast<int32_t>(optind);
                            goto addr_28f0_2;
                        }
                        if (reinterpret_cast<unsigned char>(v24) <= reinterpret_cast<unsigned char>(3)) 
                            break;
                        rdi137 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_3 + rdx11 * 8) + 24);
                        rax138 = quote(rdi137, rdi137);
                        rsp129 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                        r12_4 = rax138;
                    }
                    if (rbp9) 
                        break;
                    *reinterpret_cast<int32_t*>(&r14_12) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_12) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r13_13) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_13) + 4) = 0;
                }
                *reinterpret_cast<int32_t*>(&rdx139) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx139) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax140) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax140) + 4) = 0;
                while (1) {
                    ecx141 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax140));
                    r13_13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx139) + 0xffffffffffffffff);
                    if (*reinterpret_cast<signed char*>(&ecx141) != 37) {
                        if (!*reinterpret_cast<signed char*>(&ecx141)) 
                            goto addr_34c9_101;
                        *reinterpret_cast<int32_t*>(&rcx142) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx142) + 4) = 0;
                    } else {
                        r14_12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax140) + 1);
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r14_12)) != 37) 
                            break;
                        *reinterpret_cast<int32_t*>(&rcx142) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx142) + 4) = 0;
                    }
                    rax140 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax140) + reinterpret_cast<uint64_t>(rcx142));
                    rdx139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx139) + 1);
                }
            }
            rax143 = fun_2490();
            rdx18 = rax143;
            fun_26a0();
            usage();
            rsp15 = rsp10 - 8 + 8 - 8 + 8 - 8 + 8;
            goto addr_3197_107;
            addr_2a6b_62:
            zf144 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r8_20) == 0);
            below_or_equal145 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf144));
            if (zf144) {
                __asm__("fld tword [rsp+0x40]");
                r10_122 = v120;
                *reinterpret_cast<int32_t*>(&rcx19) = 1;
                *reinterpret_cast<int32_t*>(&rcx19 + 4) = 0;
                __asm__("fstp tword [rsp+0x30]");
                __asm__("fld1 ");
                __asm__("fstp tword [rsp+0x40]");
            } else {
                __asm__("fld tword [rsp+0x40]");
                r10_122 = v120;
                eax124 = *reinterpret_cast<uint32_t*>(&r8_20);
                *reinterpret_cast<int32_t*>(&rcx19) = 1;
                *reinterpret_cast<int32_t*>(&rcx19 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r8_20) = 0;
                *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
                __asm__("fstp tword [rsp+0x30]");
                __asm__("fld1 ");
                __asm__("fstp tword [rsp+0x40]");
                goto addr_2a97_110;
            }
            addr_33cf_111:
            __asm__("fldz ");
            __asm__("fld tword [rsp+0x30]");
            __asm__("fcomip st0, st1");
            if (0) {
                __asm__("fstp st0");
                goto addr_3496_113;
            }
            __asm__("fld tword [rsp+0xa0]");
            __asm__("fld st0");
            __asm__("fstp tword [rsp+0x50]");
            __asm__("fcomi st0, st1");
            __asm__("fstp st1");
            if (below_or_equal145) {
                __asm__("fstp st0");
                goto addr_3496_113;
            }
            __asm__("fld dword [rip+0x74a2]");
            __asm__("fcomip st0, st1");
            __asm__("fstp st0");
            if (static_cast<int1_t>(zf146 = equal_width == 0, !zf146)) {
                addr_3496_113:
                eax124 = 0;
                *reinterpret_cast<uint32_t*>(&r8_20) = 0;
                *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
                goto addr_2a97_110;
            } else {
                zf147 = reinterpret_cast<uint1_t>(rbp9 == 0);
                below_or_equal14 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf147));
                if (!zf147) {
                    addr_2aab_118:
                    __asm__("fld tword [rsp+0x50]");
                    __asm__("fldz ");
                    __asm__("fcomip st0, st1");
                    __asm__("fstp st0");
                    if (!below_or_equal14) 
                        goto addr_2e66_9;
                } else {
                    rdi148 = separator;
                    v26 = r10_122;
                    v24 = rcx19;
                    rax149 = fun_24b0(rdi148, rdi148);
                    rsp15 = rsp15 - 8 + 8;
                    if (!(rax149 - 1)) {
                        addr_3197_107:
                        rdi54 = reinterpret_cast<void**>(rsp15 + 0x70);
                        rsi53 = reinterpret_cast<void**>("%0.Lf");
                        eax150 = rpl_asprintf(rdi54, "%0.Lf", rdx18);
                        rbx5 = v151;
                        r15_3 = v31;
                        rsp55 = rsp15 - 8 - 8 - 8 + 8 + 8 + 8;
                        rcx19 = v24;
                        r10_122 = v26;
                        __asm__("fldz ");
                        if (eax150 < 0) {
                            __asm__("fstp st0");
                            goto addr_3572_27;
                        } else {
                            __asm__("fld tword [rsp+0x30]");
                            v24 = rcx19;
                            __asm__("fmul st0, st1");
                            __asm__("fucomip st0, st1");
                            __asm__("fstp st0");
                            if (__intrinsic() || eax150) {
                                rax152 = xstrdup("inf", "%0.Lf", rdx18);
                                rsp55 = rsp55 - 8 + 8;
                                rcx19 = v24;
                                r10_122 = r10_122;
                                v153 = rax152;
                                goto addr_3200_123;
                            }
                            rdi54 = reinterpret_cast<void**>(rsp55 + 0x78);
                            rsi53 = reinterpret_cast<void**>("%0.Lf");
                            eax154 = rpl_asprintf(rdi54, "%0.Lf", rdx18);
                            rsp55 = rsp55 - 8 - 8 - 8 + 8 + 8 + 8;
                            rcx19 = v24;
                            r10_122 = r10_122;
                            if (eax154 >= 0) {
                                addr_3200_123:
                                rdi54 = v155;
                                if (*reinterpret_cast<void***>(rdi54) != 45 && (rsi53 = v153, *reinterpret_cast<void***>(rsi53) != 45)) {
                                    __asm__("fld dword [rip+0x7686]");
                                    __asm__("fld tword [rsp+0xa0]");
                                    __asm__("fcomi st0, st1");
                                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi53)) >= 45) {
                                        addr_3577_126:
                                        __asm__("fnstcw word [rsp+0x6e]");
                                        __asm__("fsubrp st1, st0");
                                        __asm__("fldcw word [rsp+0x6c]");
                                        __asm__("fistp qword [rsp]");
                                        __asm__("fldcw word [rsp+0x6e]");
                                        rdx18 = v24;
                                        __asm__("btc rdx, 0x3f");
                                    } else {
                                        __asm__("fstp st1");
                                        __asm__("fnstcw word [rsp+0x6e]");
                                        __asm__("fldcw word [rsp+0x6c]");
                                        __asm__("fistp qword [rsp]");
                                        __asm__("fldcw word [rsp+0x6e]");
                                        rdx18 = v24;
                                    }
                                    seq_fast(rdi54, rsi53, rdx18);
                                    rsp55 = rsp55 - 8 + 8;
                                    rdi54 = v156;
                                    r10_122 = r10_122;
                                    rcx19 = rcx19;
                                    goto addr_3267_129;
                                }
                            } else {
                                goto addr_3572_27;
                            }
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&r9_21) = 0;
                        *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
                        rbp9 = reinterpret_cast<void**>("%Lg");
                        if (1) {
                            *reinterpret_cast<uint32_t*>(&r9_21) = 0;
                            *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
                        }
                        below_or_equal14 = 1;
                        if (0) 
                            goto addr_2aab_118; else 
                            goto addr_345b_134;
                    }
                }
            }
            __asm__("fld tword [rsp+0x30]");
            __asm__("fld tword [rsp+0x40]");
            __asm__("fcomip st0, st1");
            __asm__("fstp st0");
            *reinterpret_cast<unsigned char*>(&rbx5) = reinterpret_cast<uint1_t>(!below_or_equal14);
            continue;
            addr_3572_27:
            xalloc_die();
            rsp55 = rsp55 - 8 + 8;
            goto addr_3577_126;
            addr_3267_129:
            v26 = r10_122;
            v24 = rcx19;
            fun_23d0(rdi54, rdi54);
            fun_23d0(v153, v153);
            rsp15 = rsp55 - 8 + 8 - 8 + 8;
            rcx19 = v24;
            r10_122 = v26;
            eax124 = 0;
            *reinterpret_cast<uint32_t*>(&r8_20) = 0;
            *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
            addr_328d_136:
            *reinterpret_cast<uint32_t*>(&r9_21) = 0;
            *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r8_20) >= reinterpret_cast<int32_t>(0)) {
                *reinterpret_cast<uint32_t*>(&r9_21) = *reinterpret_cast<uint32_t*>(&r8_20);
                *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
            }
            below_or_equal14 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_21) <= 0x7fffffff);
            if (*reinterpret_cast<uint32_t*>(&r9_21) == 0x7fffffff || (below_or_equal14 = reinterpret_cast<uint1_t>(eax124 <= 0x7fffffff), eax124 == 0x7fffffff)) {
                __asm__("fld tword [rsp+0xa0]");
                rbp9 = reinterpret_cast<void**>("%Lg");
                __asm__("fstp tword [rsp+0x50]");
                goto addr_2aab_118;
            } else {
                addr_32b4_140:
                zf157 = equal_width == 0;
                if (zf157) {
                    addr_345b_134:
                    rbp9 = reinterpret_cast<void**>(0xe0d0);
                    *reinterpret_cast<uint32_t*>(&r8_20) = *reinterpret_cast<uint32_t*>(&r9_21);
                    *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rdx18) = 28;
                    *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
                    rcx19 = reinterpret_cast<void**>("%%.%dLf");
                    below_or_equal14 = 1;
                    fun_2730(0xe0d0, 1, 28);
                    rsp15 = rsp15 - 8 + 8;
                    __asm__("fld tword [rsp+0xa0]");
                    __asm__("fstp tword [rsp+0x50]");
                    goto addr_2aab_118;
                } else {
                    rdx18 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&r9_21) - *reinterpret_cast<uint32_t*>(&r8_20))) + reinterpret_cast<unsigned char>(rcx19));
                    rcx19 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&r9_21) - eax124)) + reinterpret_cast<unsigned char>(r10_122));
                    zf158 = *reinterpret_cast<uint32_t*>(&r9_21) == 0;
                    sil159 = reinterpret_cast<uint1_t>(!zf158);
                    if (!zf158) {
                        if (eax124) {
                            addr_32f5_143:
                            if (!*reinterpret_cast<uint32_t*>(&r8_20)) {
                                rdx18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx18) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx18) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(sil159 < 1)))))));
                            }
                        } else {
                            goto addr_32ed_146;
                        }
                    } else {
                        if (eax124) {
                            --rcx19;
                        } else {
                            addr_32ed_146:
                            rcx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx19) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(sil159 < 1)))))));
                            goto addr_32f5_143;
                        }
                    }
                    rbp9 = reinterpret_cast<void**>("%Lg");
                    if (reinterpret_cast<unsigned char>(rdx18) < reinterpret_cast<unsigned char>(rcx19)) {
                        rdx18 = rcx19;
                    }
                    below_or_equal14 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx18) <= reinterpret_cast<unsigned char>(0x7fffffff));
                    if (!below_or_equal14) {
                        addr_2aa0_152:
                        __asm__("fld tword [rsp+0xa0]");
                        __asm__("fstp tword [rsp+0x50]");
                        goto addr_2aab_118;
                    } else {
                        rbp9 = reinterpret_cast<void**>(0xe0d0);
                        *reinterpret_cast<uint32_t*>(&r8_20) = *reinterpret_cast<uint32_t*>(&rdx18);
                        *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
                        below_or_equal14 = 1;
                        rcx19 = reinterpret_cast<void**>("%%0%d.%dLf");
                        *reinterpret_cast<uint32_t*>(&rdx18) = 28;
                        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
                        fun_2730(0xe0d0, 1, 28);
                        rsp15 = rsp15 - 8 + 8;
                        __asm__("fld tword [rsp+0xa0]");
                        __asm__("fstp tword [rsp+0x50]");
                        goto addr_2aab_118;
                    }
                }
            }
            addr_30e3_70:
            __asm__("fstp st0");
            addr_30e5_154:
            *reinterpret_cast<uint32_t*>(&rdx18) = *reinterpret_cast<uint32_t*>(&r8_20) | eax124;
            *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
            zf160 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx18) == 0);
            below_or_equal145 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf160));
            if (!zf160) {
                addr_2a97_110:
                zf161 = reinterpret_cast<uint1_t>(rbp9 == 0);
                below_or_equal14 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf161));
                if (zf161) 
                    goto addr_328d_136; else 
                    goto addr_2aa0_152;
            } else {
                __asm__("fld dword [rsp+0x68]");
                __asm__("fld st0");
                __asm__("fld tword [rsp+0x40]");
                __asm__("fmulp st2, st0");
                __asm__("fxch st0, st1");
                __asm__("fucomip st0, st1");
                if (__intrinsic()) {
                    __asm__("fstp st0");
                    goto addr_3115_157;
                }
                if (!zf160) {
                    __asm__("fstp st0");
                    goto addr_3115_157;
                }
                __asm__("fld tword [rsp+0x40]");
                __asm__("fcomip st0, st1");
                __asm__("fstp st0");
                if (0) {
                    addr_3115_157:
                    zf162 = reinterpret_cast<uint1_t>(rbp9 == 0);
                    below_or_equal14 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf162));
                    if (!zf162) {
                        __asm__("fld tword [rsp+0xa0]");
                        __asm__("fstp tword [rsp+0x50]");
                        goto addr_2aab_118;
                    }
                } else {
                    goto addr_33cf_111;
                }
            }
            addr_34fb_163:
            *reinterpret_cast<uint32_t*>(&r9_21) = 0;
            *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
            eax124 = 0;
            if (0) {
                *reinterpret_cast<uint32_t*>(&r9_21) = 0;
                *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
            }
            *reinterpret_cast<uint32_t*>(&r8_20) = 0;
            *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
            goto addr_32b4_140;
            addr_30a3_72:
            rsi163 = *reinterpret_cast<void***>(r15_3 + rax126 * 8);
            *reinterpret_cast<uint32_t*>(&v26) = *reinterpret_cast<uint32_t*>(&r8_20);
            v24 = rcx19;
            optind = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(edx121) + 1);
            scan_arg(v23, rsi163);
            rsp15 = rsp15 - 8 + 8;
            __asm__("fld tword [rsp+0xc0]");
            *reinterpret_cast<uint32_t*>(&r8_20) = *reinterpret_cast<uint32_t*>(&v26);
            *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0;
            r10_122 = v164;
            eax124 = v165;
            rcx19 = v24;
            __asm__("fstp tword [rsp+0x30]");
            goto addr_30e5_154;
            addr_34c9_101:
            rax166 = quote(rbp9, rbp9);
            r12_4 = rax166;
            fun_2490();
            rcx19 = r12_4;
            fun_26a0();
            rsp15 = rsp10 - 8 + 8 - 8 + 8 - 8 + 8;
            goto addr_34fb_163;
            addr_2e45_15:
            rsi16 = stdout;
            rdi17 = reinterpret_cast<void**>("\n");
            eax175 = fun_2570("\n", rsi16, rdx18, rcx19, r8_20, r9_21, v24, v167, v26, v168, v23, v169, v29, v170, v31, v171, v172, v173, v174);
            rsp15 = rsp15 - 8 + 8;
            tmp32_176 = eax175 + 1;
            zf177 = reinterpret_cast<uint1_t>(tmp32_176 == 0);
            below_or_equal14 = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(tmp32_176 < eax175) | zf177);
            if (!zf177) 
                goto addr_2cd0_11; else 
                break;
            addr_2e38_29:
            fun_23d0(v178, v178);
            rsp15 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
            goto addr_2e45_15;
        }
        io_error(rdi17, rsi16, rdx18, rcx19, r8_20, r9_21);
        rsp15 = rsp15 - 8 + 8;
    }
}

int64_t __libc_start_main = 0;

void fun_3653() {
    __asm__("cli ");
    __libc_start_main(0x2790, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xe008;

void fun_23b0(int64_t rdi);

int64_t fun_36f3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23b0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3733() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

int32_t fun_2400(void** rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void** stderr = reinterpret_cast<void**>(0);

void fun_26f0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3b93(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r8_6;
    void** r9_7;
    void** r12_8;
    void** rax9;
    void** r9_10;
    int64_t r9_11;
    void** r12_12;
    void** rax13;
    void** r9_14;
    int64_t r9_15;
    void** r12_16;
    void** rax17;
    void** r9_18;
    int64_t r9_19;
    void** r12_20;
    void** rax21;
    void** r9_22;
    int64_t r9_23;
    void** r12_24;
    void** rax25;
    void** r9_26;
    int64_t r9_27;
    void** r12_28;
    void** rax29;
    void** r9_30;
    int64_t r9_31;
    void** r12_32;
    void** rax33;
    void** r9_34;
    int64_t r9_35;
    int32_t eax36;
    void** r13_37;
    void** rax38;
    void** r9_39;
    void** rax40;
    int32_t eax41;
    void** rax42;
    void** r9_43;
    void** rax44;
    void** r9_45;
    void** rax46;
    int32_t eax47;
    void** rax48;
    void** r9_49;
    void** r15_50;
    void** rax51;
    void** r9_52;
    int64_t r9_53;
    void** rax54;
    void** r9_55;
    void** rax56;
    void** rdi57;
    void** r8_58;
    void** r9_59;
    int64_t v60;
    int64_t v61;
    int64_t v62;
    int64_t v63;
    int64_t v64;
    int64_t v65;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2490();
            r8_6 = r12_2;
            fun_2660(1, rax5, r12_2, r12_2, r8_6, r9_7, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_8 = stdout;
            rax9 = fun_2490();
            fun_2570(rax9, r12_8, 5, r12_2, r8_6, r9_10, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax9, r12_8, 5, r12_2, r8_6, r9_11, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
            r12_12 = stdout;
            rax13 = fun_2490();
            fun_2570(rax13, r12_12, 5, r12_2, r8_6, r9_14, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax13, r12_12, 5, r12_2, r8_6, r9_15, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
            r12_16 = stdout;
            rax17 = fun_2490();
            fun_2570(rax17, r12_16, 5, r12_2, r8_6, r9_18, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax17, r12_16, 5, r12_2, r8_6, r9_19, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
            r12_20 = stdout;
            rax21 = fun_2490();
            fun_2570(rax21, r12_20, 5, r12_2, r8_6, r9_22, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax21, r12_20, 5, r12_2, r8_6, r9_23, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
            r12_24 = stdout;
            rax25 = fun_2490();
            fun_2570(rax25, r12_24, 5, r12_2, r8_6, r9_26, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax25, r12_24, 5, r12_2, r8_6, r9_27, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
            r12_28 = stdout;
            rax29 = fun_2490();
            fun_2570(rax29, r12_28, 5, r12_2, r8_6, r9_30, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax29, r12_28, 5, r12_2, r8_6, r9_31, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
            r12_32 = stdout;
            rax33 = fun_2490();
            fun_2570(rax33, r12_32, 5, r12_2, r8_6, r9_34, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax33, r12_32, 5, r12_2, r8_6, r9_35, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
            do {
                if (1) 
                    break;
                eax36 = fun_2590("seq", 0, 5, "seq", 0, 5);
            } while (eax36);
            r13_37 = v4;
            if (!r13_37) {
                rax38 = fun_2490();
                fun_2660(1, rax38, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_6, r9_39, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax40 = fun_2650(5, 5);
                if (!rax40 || (eax41 = fun_2400(rax40, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax41)) {
                    rax42 = fun_2490();
                    r13_37 = reinterpret_cast<void**>("seq");
                    fun_2660(1, rax42, "https://www.gnu.org/software/coreutils/", "seq", r8_6, r9_43, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_37 = reinterpret_cast<void**>("seq");
                    goto addr_3f38_9;
                }
            } else {
                rax44 = fun_2490();
                fun_2660(1, rax44, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_6, r9_45, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax46 = fun_2650(5, 5);
                if (!rax46 || (eax47 = fun_2400(rax46, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax47)) {
                    addr_3e3e_11:
                    rax48 = fun_2490();
                    fun_2660(1, rax48, "https://www.gnu.org/software/coreutils/", "seq", r8_6, r9_49, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_37 == "seq")) {
                        r12_2 = reinterpret_cast<void**>(0xad21);
                    }
                } else {
                    addr_3f38_9:
                    r15_50 = stdout;
                    rax51 = fun_2490();
                    fun_2570(rax51, r15_50, 5, "https://www.gnu.org/software/coreutils/", r8_6, r9_52, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, rax51, r15_50, 5, "https://www.gnu.org/software/coreutils/", r8_6, r9_53, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0);
                    goto addr_3e3e_11;
                }
            }
            rax54 = fun_2490();
            fun_2660(1, rax54, r13_37, r12_2, r8_6, r9_55, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            addr_3bee_14:
            fun_26d0();
        }
    } else {
        rax56 = fun_2490();
        rdi57 = stderr;
        fun_26f0(rdi57, 1, rax56, r12_2, r8_58, r9_59, v60, v61, v62, v63, v64, v65);
        goto addr_3bee_14;
    }
}

void fun_2690(int64_t rdi, int64_t* rsi);

void c_strtold(int64_t rdi, void* rsi);

void fun_4303(int64_t rdi, uint64_t* rsi) {
    void* rsp3;
    void** rax4;
    signed char* v5;
    int32_t* rax6;
    int32_t r13d7;
    uint64_t v8;
    uint64_t v9;
    uint64_t v10;
    void* rax11;

    __asm__("cli ");
    rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 56);
    rax4 = g28;
    fun_2690(rdi, reinterpret_cast<int64_t>(rsp3) + 24);
    __asm__("fld st0");
    if (*v5) {
        __asm__("fstp st0");
        __asm__("fstp tword [rsp]");
        rax6 = fun_23f0();
        r13d7 = *rax6;
        c_strtold(rdi, reinterpret_cast<int64_t>(rsp3) - 8 + 8 - 8 + 8 + 32);
        __asm__("fld tword [rsp]");
        if (v8 >= v9) {
            __asm__("fstp st1");
            *rax6 = r13d7;
        } else {
            __asm__("fstp st0");
            v10 = v9;
        }
    } else {
        __asm__("fstp st1");
    }
    if (rsi) {
        *rsi = v10;
    }
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax11) {
        __asm__("fstp st0");
        fun_24c0();
    } else {
        return;
    }
}

int64_t file_name = 0;

void fun_43b3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_43c3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2410(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_43d3() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23f0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2490();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4463_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_26a0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2410(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4463_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_26a0();
    }
}

void fun_26e0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s4 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s4* fun_2510();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4483(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s4* rax4;
    void** r12_5;
    void** rcx6;
    void** r8_7;
    int32_t eax8;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_26e0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23e0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2510();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax8 = fun_2400(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6, r8_7), !eax8))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5c23(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23f0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xe220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5c63(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5c83(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe220);
    }
    *rdi = esi;
    return 0xe220;
}

int64_t fun_5ca3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xe220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s5 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5ce3(struct s5* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s5*>(0xe220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s6 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s6* fun_5d03(struct s6* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xe220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x274a;
    if (!rdx) 
        goto 0x274a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xe220;
}

struct s7 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5d43(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s7* r8) {
    struct s7* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s7*>(0xe220);
    }
    rax7 = fun_23f0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5d76);
    *rax7 = r15d8;
    return rax13;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5dc3(int64_t rdi, int64_t rsi, void*** rdx, struct s8* rcx) {
    struct s8* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s8*>(0xe220);
    }
    rax6 = fun_23f0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5df1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5e4c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5eb3() {
    __asm__("cli ");
}

void** ge078 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_5ec3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23d0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xe120) {
        fun_23d0(rdi7);
        ge078 = reinterpret_cast<void**>(0xe120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xe070) {
        fun_23d0(r12_2);
        slotvec = reinterpret_cast<void**>(0xe070);
    }
    nslots = 1;
    return;
}

void fun_5f63() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f83() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f93(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5fb3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5fd3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2750;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_6063(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2755;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void** fun_60f3(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x275a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_24c0();
    } else {
        return rax5;
    }
}

void** fun_6183(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x275f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_6213(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8000]");
    __asm__("movdqa xmm1, [rip+0x8008]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x7ff1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_24c0();
    } else {
        return rax10;
    }
}

void** fun_62b3(int64_t rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7f60]");
    __asm__("movdqa xmm1, [rip+0x7f68]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x7f51]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

void** fun_6353(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7ec0]");
    __asm__("movdqa xmm1, [rip+0x7ec8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x7ea9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_24c0();
    } else {
        return rax3;
    }
}

void** fun_63e3(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7e30]");
    __asm__("movdqa xmm1, [rip+0x7e38]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x7e26]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_24c0();
    } else {
        return rax4;
    }
}

void** fun_6473(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2764;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_6513(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7cfa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x7cf2]");
    __asm__("movdqa xmm2, [rip+0x7cfa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2769;
    if (!rdx) 
        goto 0x2769;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void** fun_65b3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7c5a]");
    __asm__("movdqa xmm1, [rip+0x7c62]");
    __asm__("movdqa xmm2, [rip+0x7c6a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x276e;
    if (!rdx) 
        goto 0x276e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

void** fun_6663(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7baa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x7ba2]");
    __asm__("movdqa xmm2, [rip+0x7baa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2773;
    if (!rsi) 
        goto 0x2773;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_6703(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7b0a]");
    __asm__("movdqa xmm1, [rip+0x7b12]");
    __asm__("movdqa xmm2, [rip+0x7b1a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2778;
    if (!rsi) 
        goto 0x2778;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void fun_67a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_67b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_67d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_67f3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s9 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_25a0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_6813(void** rdi, void** rsi, void** rdx, void** rcx, struct s9* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26f0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_26f0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2490();
    fun_26f0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_25a0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2490();
    fun_26f0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_25a0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2490();
        fun_26f0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xafc8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xafc8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6c83() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s10 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6ca3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s10* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s10* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_24c0();
    } else {
        return;
    }
}

void fun_6d43(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6de6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6df0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_24c0();
    } else {
        return;
    }
    addr_6de6_5:
    goto addr_6df0_7;
}

void fun_6e23() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** v10;
    void** v11;
    int64_t v12;
    void** v13;
    int64_t v14;
    void** v15;
    int64_t v16;
    void** v17;
    int64_t v18;
    void** v19;
    int64_t v20;
    int64_t v21;
    int64_t v22;
    void** rax23;
    void** r8_24;
    void** r9_25;
    void** v26;
    void** v27;
    int64_t v28;
    void** v29;
    int64_t v30;
    void** v31;
    int64_t v32;
    void** v33;
    int64_t v34;
    void** v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25a0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2490();
    fun_2660(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22);
    rax23 = fun_2490();
    fun_2660(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_24, r9_25, v26, __return_address(), v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    fun_2490();
    goto fun_2660;
}

int64_t fun_2440();

void fun_6ec3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2440();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_25f0(void** rdi, void** rsi, ...);

void fun_6f03(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6f23(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6f43(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2640(void** rdi, void** rsi, ...);

void fun_6f63(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6f93(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2640(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6fc3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2440();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7003() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2440();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7043(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2440();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7073(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2440();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70c3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2440();
            if (rax5) 
                break;
            addr_710d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_710d_5;
        rax8 = fun_2440();
        if (rax8) 
            goto addr_70f6_9;
        if (rbx4) 
            goto addr_710d_5;
        addr_70f6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7153(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2440();
            if (rax8) 
                break;
            addr_719a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_719a_5;
        rax11 = fun_2440();
        if (rax11) 
            goto addr_7182_9;
        if (!rbx6) 
            goto addr_7182_9;
        if (r12_4) 
            goto addr_719a_5;
        addr_7182_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_71e3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_728d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_72a0_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_7240_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_7266_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_72b4_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_725d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_72b4_14;
            addr_725d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_72b4_14;
            addr_7266_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2640(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_72b4_14;
            if (!rbp13) 
                break;
            addr_72b4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_728d_9;
        } else {
            if (!r13_6) 
                goto addr_72a0_10;
            goto addr_7240_11;
        }
    }
}

int64_t fun_2580();

void fun_72e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2580();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7313() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2580();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7343() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2580();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7363() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2580();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7383(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25c0;
    }
}

void fun_73c3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25c0;
    }
}

void fun_7403(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_25c0;
    }
}

void fun_7443(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_24b0(rdi);
    rax4 = fun_25f0(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_25c0;
    }
}

void fun_7483() {
    void** rdi1;

    __asm__("cli ");
    fun_2490();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_26a0();
    fun_23e0(rdi1);
}

int64_t fun_74c3(signed char* rdi, signed char** rsi, int64_t rdx, int64_t rcx) {
    void** rax5;
    int32_t* rax6;
    signed char* v7;
    int32_t r8d8;
    int1_t zf9;
    int1_t zf10;
    void* rax11;
    int64_t rax12;

    __asm__("cli ");
    rax5 = g28;
    rax6 = fun_23f0();
    *rax6 = 0;
    rcx(rdi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16 - 8 + 8);
    if (v7 == rdi) {
        r8d8 = 0;
    } else {
        zf9 = rsi == 0;
        if (zf9) {
            r8d8 = 0;
            zf10 = *v7 == 0;
            if (!zf10) 
                goto addr_7535_5;
            __asm__("fldz ");
            __asm__("fxch st0, st1");
            __asm__("fucomi st0, st1");
            __asm__("fstp st1");
            if (__intrinsic()) 
                goto addr_7523_7; else 
                goto addr_7572_8;
        } else {
            __asm__("fldz ");
            __asm__("fxch st0, st1");
            __asm__("fucomi st0, st1");
            __asm__("fstp st1");
            if (__intrinsic()) 
                goto addr_7523_7;
            r8d8 = 1;
            if (zf9) 
                goto addr_7531_11; else 
                goto addr_7523_7;
        }
    }
    addr_752c_12:
    if (!rsi) {
        addr_7535_5:
        __asm__("fstp tword [r13+0x0]");
        rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
        if (rax11) {
            fun_24c0();
        } else {
            *reinterpret_cast<int32_t*>(&rax12) = r8d8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
            return rax12;
        }
    } else {
        addr_7531_11:
        *rsi = v7;
        goto addr_7535_5;
    }
    addr_7523_7:
    *reinterpret_cast<unsigned char*>(&r8d8) = reinterpret_cast<uint1_t>(*rax6 != 34);
    goto addr_752c_12;
    addr_7572_8:
    r8d8 = 1;
    if (zf10) 
        goto addr_7535_5;
    goto addr_7523_7;
}

void rpl_vasprintf();

void fun_7593() {
    signed char al1;
    void** rax2;
    void* rdx3;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    rpl_vasprintf();
    rdx3 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx3) {
        fun_24c0();
    } else {
        return;
    }
}

void** vasnprintf();

uint64_t fun_7653(void*** rdi, int64_t rsi, int64_t rdx) {
    void** rax4;
    void** rax5;
    uint64_t rax6;
    uint64_t v7;
    int32_t* rax8;
    void* rdx9;

    __asm__("cli ");
    rax4 = g28;
    rax5 = vasnprintf();
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rax6 = v7;
        if (rax6 > 0x7fffffff) {
            fun_23d0(rax5, rax5);
            rax8 = fun_23f0();
            *rax8 = 75;
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            *rdi = rax5;
        }
    }
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_24c0();
    } else {
        return rax6;
    }
}

int64_t c_locale_cache = 0;

int64_t fun_2500(int64_t rdi, int64_t* rsi);

int64_t fun_25e0(int64_t rdi, int64_t* rsi);

void fun_76d3(int64_t rdi, int64_t* rsi) {
    int64_t* rbp3;
    int64_t rax4;
    int64_t rax5;
    int64_t rdi6;
    int64_t rax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    rbp3 = rsi;
    rax4 = c_locale_cache;
    if (!rax4) {
        rsi = reinterpret_cast<int64_t*>("C");
        rax5 = fun_2500(0x1fbf, "C");
        c_locale_cache = rax5;
    }
    rdi6 = c_locale_cache;
    if (!rdi6 || (rax7 = fun_25e0(rdi6, rsi), rax7 == 0)) {
        if (!rbp3) {
            __asm__("fldz ");
            return;
        } else {
            *rbp3 = rdi;
            __asm__("fldz ");
            return;
        }
    } else {
        fun_2690(rdi, rbp3);
        __asm__("fstp tword [rsp]");
        rax8 = fun_23f0();
        r12d9 = *rax8;
        rax10 = fun_25e0(rax7, rbp3);
        if (!rax10) 
            goto 0x277d;
        *rax8 = r12d9;
        __asm__("fld tword [rsp]");
        return;
    }
}

int64_t fun_2430();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_77a3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_2430();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_77fe_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23f0();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_77fe_3;
            rax6 = fun_23f0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_25d0(struct s11* rdi);

int32_t fun_2620(struct s11* rdi);

int64_t fun_2520(int64_t rdi, ...);

int32_t rpl_fflush(struct s11* rdi);

int64_t fun_2470(struct s11* rdi);

int64_t fun_7813(struct s11* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_25d0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2620(rdi);
        if (!(eax3 && (eax4 = fun_25d0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2520(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23f0();
            r12d9 = *rax8;
            rax10 = fun_2470(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2470;
}

void rpl_fseeko(struct s11* rdi);

void fun_78a3(struct s11* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2620(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_78f3(struct s11* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_25d0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2520(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2610(int64_t rdi);

signed char* fun_7973() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2610(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24e0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_79b3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24e0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_24c0();
    } else {
        return r12_7;
    }
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s12 {
    signed char[7] pad7;
    void** f7;
};

struct s13 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s14 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_7a43(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    void** rax12;
    void** v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    void* rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    int32_t* rax30;
    void** r15_31;
    int32_t* v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    int32_t* rax41;
    void** rax42;
    struct s12* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    int32_t* rax55;
    struct s13* r14_56;
    struct s13* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s14* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    int32_t* rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    int32_t* rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    int32_t* rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x897a;
                fun_24c0();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_897a_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_8984_6;
                addr_886e_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x8893, rax21 = fun_2640(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x88b9;
                    fun_23d0(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x88df;
                    fun_23d0(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x8905;
                    fun_23d0(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_8984_6:
            addr_8568_16:
            v28 = r10_16;
            addr_856f_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0x8574;
            rax30 = fun_23f0();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_8582_18:
            *v32 = 12;
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0x8121;
                fun_23d0(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0x8139;
                fun_23d0(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_7d78_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0x7d90;
                fun_23d0(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0x7da8;
            fun_23d0(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_23f0();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *rax41 = 22;
        goto addr_7d78_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_7d6d_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_7d6d_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 0x1000);
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_25f0(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_7d6d_33:
            rax55 = fun_23f0();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *rax55 = 12;
            goto addr_7d78_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_7b6c_46;
    while (1) {
        addr_84c4_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_7c2f_50;
            if (r14_56->f50 != -1) 
                goto 0x2782;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_849f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_8568_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_8568_16;
                if (r10_16 == v10) 
                    goto addr_87b4_64; else 
                    goto addr_8473_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s13*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_84c4_47;
            addr_7b6c_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_8620_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_8620_73;
                if (r15_31 == v10) 
                    goto addr_85b0_79; else 
                    goto addr_7bc7_80;
            }
            addr_7bec_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0x7c02;
            fun_25c0(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_85b0_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0x85b8;
            rax67 = fun_25f0(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_8620_73;
            if (!r9_58) 
                goto addr_7bec_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0x85f7;
            rax69 = fun_25c0(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_7bec_81;
            addr_7bc7_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0x7bd2;
            rax71 = fun_2640(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_8620_73; else 
                goto addr_7bec_81;
            addr_87b4_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x87ca;
            rax73 = fun_25f0(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_8989_84;
            if (r12_9) 
                goto addr_87ea_86;
            r10_16 = rax73;
            goto addr_849f_55;
            addr_87ea_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x87ff;
            rax75 = fun_25c0(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_849f_55;
            addr_8473_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0x848c;
            rax77 = fun_2640(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_856f_17;
            r10_16 = rax77;
            goto addr_849f_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_897a_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_886e_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_8568_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_892d_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_892d_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_8568_16; else 
                goto addr_8937_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_8843_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x894e;
        rax80 = fun_25f0(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_886e_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x896d;
                rax82 = fun_25c0(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_886e_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x8862;
        rax84 = fun_2640(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_856f_17; else 
            goto addr_886e_7;
    }
    addr_8937_96:
    rbx19 = r13_8;
    goto addr_8843_98;
    addr_7c2f_50:
    if (r14_56->f50 == -1) 
        goto 0x2782;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x2787;
        goto *reinterpret_cast<int32_t*>(0xb0b8 + r13_87 * 4) + 0xb0b8;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0x7cff;
        fun_25c0(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0x7d39;
        fun_25c0(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0xb048 + rax95 * 4) + 0xb048;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x2782;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x2782;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_7e72_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_8568_16;
    }
    addr_7e72_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_8568_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_7e93_142; else 
                goto addr_8722_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_8722_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_8568_16; else 
                    goto addr_872c_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_7e93_142;
            }
        }
    }
    addr_7ec5_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0x7ecf;
    rax102 = fun_23f0();
    *rax102 = 0;
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x2787;
    goto *reinterpret_cast<int32_t*>(0xb070 + rax103 * 4) + 0xb070;
    addr_7e93_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x8788;
        rax105 = fun_25f0(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_8989_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x898e;
            rax107 = fun_23f0();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_8582_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x87af;
                fun_25c0(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_7ec5_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0x7eb2;
        rax110 = fun_2640(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_8568_16; else 
            goto addr_7ec5_147;
    }
    addr_872c_145:
    rbx19 = tmp64_100;
    goto addr_7e93_142;
    addr_8620_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0x8625;
    rax112 = fun_23f0();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_8582_18;
}

int32_t setlocale_null_r();

int64_t fun_89c3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax3;
    }
}

int64_t fun_8a43(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2650(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_24b0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_25c0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_25c0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_8af3() {
    __asm__("cli ");
    goto fun_2650;
}

struct s15 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_8b03(int64_t rdi, struct s15* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_8b39_5;
    return 0xffffffff;
    addr_8b39_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb0e0 + rdx3 * 4) + 0xb0e0;
}

struct s16 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s17 {
    uint64_t f0;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s18 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

struct s19 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_8d33(void** rdi, struct s16* rsi, struct s17* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s17* r15_7;
    struct s16* r14_8;
    void** rcx9;
    uint64_t r9_10;
    void** v11;
    uint64_t v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    struct s18* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s18* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    uint64_t rbp52;
    void** rdx53;
    void** r8_54;
    uint64_t rdx55;
    void*** rax56;
    void*** rcx57;
    uint64_t r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    uint64_t rdx63;
    void** rax64;
    struct s18* rbx65;
    uint64_t rsi66;
    int32_t eax67;
    struct s18* rdx68;
    uint64_t rax69;
    uint64_t rcx70;
    uint64_t rcx71;
    uint64_t tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    uint64_t rbx77;
    uint64_t r9_78;
    uint64_t rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    uint64_t rdx87;
    void** rax88;
    void** rax89;
    int32_t* rax90;
    void*** rax91;
    void** rdi92;
    int32_t* rax93;
    struct s19* rbx94;
    uint64_t rdi95;
    int32_t eax96;
    struct s19* rcx97;
    uint64_t rax98;
    uint64_t rdx99;
    uint64_t rdx100;
    uint64_t tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_10) + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = 0;
    rdx->f8 = rdi6;
    v12 = 0;
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_8de8_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_8dd5_7:
    return rax15;
    addr_8de8_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<uint32_t*>(r12_16 + 16) = 0;
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint64_t*>(r12_16 + 40) = 0xffffffffffffffff;
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint64_t*>(r12_16 + 64) = 0xffffffffffffffff;
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_8e59_11;
    } else {
        addr_8e59_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<uint32_t*>(r12_16 + 16) = *reinterpret_cast<uint32_t*>(r12_16 + 16) | 1;
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_8e70_14;
        } else {
            goto addr_8e70_14;
        }
    }
    rax23 = reinterpret_cast<struct s18*>(rax5 + 2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s18*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_9338_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s18*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s18*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_9338_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<void**>(&rcx26->f2);
    goto addr_8e59_11;
    addr_8e70_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb15c + rax33 * 4) + 0xb15c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_8fb7_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_96b9_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_96be_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_96b4_42;
            }
        } else {
            addr_8e9d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_90b8_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_8ea7_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<unsigned char*>(rbx14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_98a4_56; else 
                        goto addr_90f5_57;
                }
            } else {
                addr_8ea7_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_8ec8_60;
                } else {
                    goto addr_8ec8_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_943b_65;
    addr_8fb7_33:
    *reinterpret_cast<uint64_t*>(r12_16 + 40) = 0;
    if (0) 
        goto addr_9338_22;
    rbp52 = 0;
    v12 = 1;
    rbx14 = rdx22;
    addr_8fdc_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (7 > rbp52) {
        addr_905a_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>((rdx55 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx55 <= rbp52) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (rdx55 <= rbp52);
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = 14;
        if (14 <= rbp52) {
            r9_58 = rbp52 + 1;
        }
        if (r9_58 >> 59) 
            goto addr_995b_75; else 
            goto addr_9003_76;
    }
    rbp59 = reinterpret_cast<void***>((rbp52 << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_933c_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_8ea7_52;
        goto addr_90b8_44;
    }
    addr_96be_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_8e9d_43;
    addr_995b_75:
    if (v11 != rdx53) {
        fun_23d0(rdx53);
        r10_4 = r10_4;
        goto addr_976a_84;
    } else {
        goto addr_976a_84;
    }
    addr_9003_76:
    rsi60 = reinterpret_cast<void**>(r9_58 << 5);
    if (v11 == rdx53) {
        rax61 = fun_25f0(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2640(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_995b_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_25c0(r8_54, v11, rdx63 << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_905a_68;
    addr_943b_65:
    rbx65 = reinterpret_cast<struct s18*>(rbx14 + 2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s18*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (rsi66 > 0x1999999999999999) {
            rcx70 = 0xffffffffffffffff;
        } else {
            rcx71 = rsi66 + rsi66 * 4;
            rcx70 = rcx71 + rcx71;
        }
        while (tmp64_72 = rcx70 + rax69, rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), tmp64_72 < rcx70) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_9338_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s18*>(&rbx65->pad2);
            rcx70 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s18*>(&rbx65->pad2);
    }
    rbp52 = tmp64_72 - 1;
    if (rbp52 > 0xfffffffffffffffd) 
        goto addr_9338_22;
    *reinterpret_cast<uint64_t*>(r12_16 + 40) = rbp52;
    rbx14 = reinterpret_cast<void**>(&rdx68->f2);
    goto addr_8fdc_67;
    label_41:
    addr_96b4_42:
    goto addr_96b9_36;
    addr_98a4_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_98ca_104;
    addr_90f5_57:
    rbx77 = *reinterpret_cast<uint64_t*>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<uint64_t*>(r12_16 + 64) = v12;
        if (0) {
            addr_9338_22:
            r8_54 = r15_7->f8;
            goto addr_933c_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_9104_107:
    r8_54 = r15_7->f8;
    if (r9_10 <= rbx77) {
        r9_78 = r9_10 + r9_10;
        if (r9_78 <= rbx77) {
            r9_78 = rbx77 + 1;
        }
        if (!(r9_78 >> 59)) 
            goto addr_97b2_111;
    } else {
        addr_9111_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>((rdx79 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx79 <= rbx77) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (rdx79 <= rbx77);
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_9147_116;
        }
    }
    rdx53 = r8_54;
    goto addr_995b_75;
    addr_97b2_111:
    rsi82 = reinterpret_cast<void**>(r9_78 << 5);
    if (v11 == r8_54) {
        rax83 = fun_25f0(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_976a_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_23d0(rdi86);
            }
        } else {
            addr_99f0_120:
            rdx87 = r15_7->f0;
            rax88 = fun_25c0(rdi84, r8_85, rdx87 << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_980f_121;
        }
    } else {
        rax89 = fun_2640(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_995b_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_99f0_120;
            }
        }
    }
    rax90 = fun_23f0();
    *rax90 = 12;
    return 0xffffffff;
    addr_980f_121:
    r15_7->f8 = r8_54;
    goto addr_9111_112;
    addr_9147_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + (rbx77 << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_933c_80:
            if (v11 != r8_54) {
                fun_23d0(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_8ea7_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_8ea7_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_23d0(rdi92, rdi92);
    }
    rax93 = fun_23f0();
    *rax93 = 22;
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_8dd5_7;
    addr_98ca_104:
    rbx94 = reinterpret_cast<struct s19*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s19*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (rdi95 > 0x1999999999999999) {
            rdx99 = 0xffffffffffffffff;
        } else {
            rdx100 = rdi95 + rdi95 * 4;
            rdx99 = rdx100 + rdx100;
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = rdx99 + rax98, rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), tmp64_101 < rdx99) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_9338_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s19*>(&rbx94->pad2);
            rdx99 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s19*>(&rbx94->pad2);
    }
    rbx77 = tmp64_101 + 0xffffffffffffffff;
    if (rbx77 > 0xfffffffffffffffd) 
        goto addr_9338_22;
    *reinterpret_cast<uint64_t*>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx97->f2);
    goto addr_9104_107;
    addr_8ec8_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xb2c0 + rax105 * 4) + 0xb2c0;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb204 + rax106 * 4) + 0xb204;
    }
}

void fun_9ac3() {
    __asm__("cli ");
}

void fun_9ad7() {
    __asm__("cli ");
    return;
}

void** rpl_mbrtowc(void* rdi, unsigned char* rsi);

int32_t fun_2710(int64_t rdi, unsigned char* rsi);

uint32_t fun_2700(void** rdi, unsigned char* rsi);

void fun_46b5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rax10;
    void** r11_11;
    void** v12;
    int32_t ebp13;
    void** rax14;
    void* r15_15;
    int32_t ebx16;
    void** rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void* rsi30;
    void* v31;
    void** v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rdx44;
    void** rcx45;
    uint32_t edx46;
    unsigned char al47;
    void** v48;
    int64_t v49;
    void** v50;
    void** v51;
    void** rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    void** v61;
    unsigned char v62;
    void* v63;
    void* v64;
    void** v65;
    signed char* v66;
    void** r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    void** r13_72;
    unsigned char* rsi73;
    void* v74;
    void** r15_75;
    unsigned char* rdx76;
    void* v77;
    int64_t rax78;
    int64_t rdi79;
    int32_t v80;
    int32_t eax81;
    void* rdi82;
    uint32_t edx83;
    unsigned char v84;
    void* rdi85;
    void* v86;
    uint32_t esi87;
    uint32_t ebp88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    void* rdx95;
    void* rcx96;
    void* v97;
    void** rax98;
    uint1_t zf99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    int64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    int64_t rax111;
    int64_t rax112;
    uint32_t r12d113;
    void* v114;
    void* rdx115;
    void* rax116;
    void* v117;
    uint64_t rax118;
    int64_t v119;
    int64_t rax120;
    int64_t rax121;
    int64_t rax122;
    int64_t v123;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2490();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_2490();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx17)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24b0(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_49b3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_49b3_22; else 
                            goto addr_4dad_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4e6d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_51c0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_49b0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_49b0_30; else 
                                goto addr_51d9_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24b0(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_51c0_28;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2560(rdi39, v26, v28, rcx45);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_51c0_28; else 
                            goto addr_485c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5320_39:
                    *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_51a0_40:
                        if (r11_27 == 1) {
                            addr_4d2d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_52e8_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx45);
                                goto addr_4967_44;
                            }
                        } else {
                            goto addr_51b0_46;
                        }
                    } else {
                        addr_532f_47:
                        rax24 = v48;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_4d2d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_49b3_22:
                                if (v49 != 1) {
                                    addr_4f09_52:
                                    v50 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_24b0(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_4f54_54;
                                    }
                                } else {
                                    goto addr_49c0_56;
                                }
                            } else {
                                addr_4965_57:
                                ebp36 = 0;
                                goto addr_4967_44;
                            }
                        } else {
                            addr_5194_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_532f_47; else 
                                goto addr_519e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4d2d_41;
                        if (v49 == 1) 
                            goto addr_49c0_56; else 
                            goto addr_4f09_52;
                    }
                }
                addr_4a21_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<void**>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_48b8_63:
                    if (!1 && (edx54 = *reinterpret_cast<uint32_t*>(&rcx45), *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<unsigned char*>(&rcx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_48dd_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_4be0_65;
                    } else {
                        addr_4a49_66:
                        ++r9_37;
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5298_67;
                    }
                } else {
                    goto addr_4a40_69;
                }
                addr_48f1_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                ++r9_37;
                addr_493c_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5298_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_493c_81;
                }
                addr_4a40_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_48dd_64; else 
                    goto addr_4a49_66;
                addr_4967_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_4a1f_91;
                if (v22) 
                    goto addr_497f_93;
                addr_4a1f_91:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4a21_62;
                addr_4f54_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx45 = r12_67;
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_56db_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_574b_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72)) + 1);
                        rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = *rdx76 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_554f_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    *reinterpret_cast<int32_t*>(&rdi79) = v80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
                    eax81 = fun_2710(rdi79, rsi73);
                    if (!eax81) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2700(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx83 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx83) & reinterpret_cast<unsigned char>(v32));
                addr_504e_109:
                if (reinterpret_cast<uint64_t>(rdi82) <= 1) {
                    addr_4a0c_110:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        edx83 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_5058_112;
                    }
                } else {
                    addr_5058_112:
                    v84 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi85 = v86;
                    esi87 = 0;
                    ebp88 = reinterpret_cast<unsigned char>(v22);
                    rcx45 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi82) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_5129_114;
                }
                addr_4a18_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4a1f_91;
                while (1) {
                    addr_5129_114:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        *reinterpret_cast<unsigned char*>(&esi87) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax89 = esi87;
                        if (*reinterpret_cast<signed char*>(&ebp88)) 
                            goto addr_5637_117;
                        eax90 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) & *reinterpret_cast<unsigned char*>(&esi87));
                        if (*reinterpret_cast<unsigned char*>(&eax90)) 
                            goto addr_5096_119;
                    } else {
                        eax57 = (esi87 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                            goto addr_5645_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_5117_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_5117_128;
                        }
                    }
                    addr_50c5_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax91 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax91) >> 6);
                        eax92 = eax91 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax92);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax93 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax93) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax93) >> 3);
                        eax94 = (eax93 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax94);
                    }
                    ++r9_37;
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                        break;
                    esi87 = edx83;
                    addr_5117_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi85) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_5096_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax90;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_50c5_134;
                }
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_493c_81;
                addr_5645_125:
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_5298_67;
                addr_56db_96:
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx83 = reinterpret_cast<unsigned char>(v32);
                goto addr_504e_109;
                addr_574b_98:
                r11_27 = v65;
                rdi82 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx95 = rdi82;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx96 = v97;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx96) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx95 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx95) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx95));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi82 = rdx95;
                }
                edx83 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_504e_109;
                addr_49c0_56:
                rax98 = fun_2720(rdi39, rdi39);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi82) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf99 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax98) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf99);
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf99) & reinterpret_cast<unsigned char>(v32));
                goto addr_4a0c_110;
                addr_519e_59:
                goto addr_51a0_40;
                addr_4e6d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_49b3_22;
                *reinterpret_cast<uint32_t*>(&rcx45) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx45));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_4a18_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4965_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_49b3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4eb2_160;
                if (!v22) 
                    goto addr_5287_162; else 
                    goto addr_5493_163;
                addr_4eb2_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_5287_162:
                    ++r9_37;
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    goto addr_5298_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!v32) 
                        goto addr_4d5b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                addr_4bc3_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_48f1_70; else 
                    goto addr_4bd7_169;
                addr_4d5b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_48b8_63;
                goto addr_4a40_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5194_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_52cf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_49b0_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_48a8_178; else 
                        goto addr_5252_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5194_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_49b3_22;
                }
                addr_52cf_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_49b0_30:
                    r8d42 = 0;
                    goto addr_49b3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4a21_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        goto addr_52e8_42;
                    }
                }
                addr_48a8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_48b8_63;
                addr_5252_179:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_51b0_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4a21_62;
                } else {
                    addr_5262_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_49b3_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_5a12_188;
                if (v28) 
                    goto addr_5287_162;
                addr_5a12_188:
                *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                goto addr_4bc3_168;
                addr_485c_37:
                if (v22) 
                    goto addr_5853_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4873_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5320_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_53ab_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_49b3_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_48a8_178; else 
                        goto addr_5387_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5194_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_49b3_22;
                }
                addr_53ab_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_49b3_22;
                }
                addr_5387_199:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_51b0_46;
                goto addr_5262_186;
                addr_4873_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_49b3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_49b3_22; else 
                    goto addr_4884_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx45) = reinterpret_cast<uint1_t>(r15_15 == 0);
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (*reinterpret_cast<unsigned char*>(&rcx45) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_595e_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_57e4_210;
            if (1) 
                goto addr_57e2_212;
            if (!v29) 
                goto addr_541e_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v49 = rax108;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_5951_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4be0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_499b_219; else 
            goto addr_4bfa_220;
        addr_497f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax89 = reinterpret_cast<unsigned char>(v32);
        addr_4993_221:
        if (*reinterpret_cast<signed char*>(&eax89)) 
            goto addr_4bfa_220; else 
            goto addr_499b_219;
        addr_554f_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_4bfa_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_556d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_59e0_225:
        v31 = r13_34;
        addr_5446_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_5637_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4993_221;
        addr_5493_163:
        eax89 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4993_221;
        addr_4bd7_169:
        goto addr_4be0_65;
        addr_595e_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_4bfa_220;
        goto addr_556d_222;
        addr_57e4_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (*reinterpret_cast<uint32_t*>(&rcx45) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx45)))) {
            rsi30 = v114;
            rdx115 = r15_15;
            rax116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx115)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi30) + reinterpret_cast<uint64_t>(rdx115)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                rdx115 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx115) + 1);
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax116) + reinterpret_cast<uint64_t>(rdx115));
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx45));
            r15_15 = rdx115;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v117) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax118 = reinterpret_cast<uint64_t>(v119 - reinterpret_cast<unsigned char>(g28));
        if (!rax118) 
            goto addr_583e_236;
        fun_24c0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_59e0_225;
        addr_57e2_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_57e4_210;
        addr_541e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_57e4_210;
        } else {
            goto addr_5446_226;
        }
        addr_5951_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4dad_24:
    *reinterpret_cast<uint32_t*>(&rax120) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax120) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xaa8c + rax120 * 4) + 0xaa8c;
    addr_51d9_32:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xab8c + rax121 * 4) + 0xab8c;
    addr_5853_190:
    addr_499b_219:
    goto 0x4680;
    addr_4884_206:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa98c + rax122 * 4) + 0xa98c;
    addr_583e_236:
    goto v123;
}

void fun_48a0() {
}

void fun_4a58() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4752;
}

void fun_4ab1() {
    goto 0x4752;
}

void fun_4b9e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4a21;
    }
    if (v2) 
        goto 0x5493;
    if (!r10_3) 
        goto addr_55fe_5;
    if (!v4) 
        goto addr_54ce_7;
    addr_55fe_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_54ce_7:
    goto 0x48d4;
}

void fun_4bbc() {
}

void fun_4c67() {
    signed char v1;

    if (v1) {
        goto 0x4bef;
    } else {
        goto 0x492a;
    }
}

void fun_4c81() {
    signed char v1;

    if (!v1) 
        goto 0x4c7a; else 
        goto "???";
}

void fun_4ca8() {
    goto 0x4bc3;
}

void fun_4d28() {
}

void fun_4d40() {
}

void fun_4d6f() {
    goto 0x4bc3;
}

void fun_4dc1() {
    goto 0x4d50;
}

void fun_4df0() {
    goto 0x4d50;
}

void fun_4e23() {
    goto 0x4d50;
}

void fun_51f0() {
    goto 0x48a8;
}

void fun_54ee() {
    signed char v1;

    if (v1) 
        goto 0x5493;
    goto 0x48d4;
}

void fun_5595() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x48d4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x48b8;
        goto 0x48d4;
    }
}

void fun_59b2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4c20;
    } else {
        goto 0x4752;
    }
}

void fun_68e8() {
    fun_2490();
}

int32_t fun_23c0(int64_t rdi);

struct s20 {
    signed char[1] pad1;
    signed char f1;
};

struct s21 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_7f38() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    void** rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    void** rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    void** rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    void** rax30;
    void** rcx31;
    void* rbx32;
    void* rbx33;
    void** tmp64_34;
    void* r12_35;
    void** rax36;
    void** rbx37;
    int64_t r15_38;
    int64_t rbp39;
    void** r15_40;
    void** rax41;
    void** rax42;
    int64_t r12_43;
    void** r15_44;
    void** r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s21* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_23c0(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<void***>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_80c3_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_23c0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_23c0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_80c3_5;
    }
    rcx19 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x2782;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_7fcd_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0x84a7;
        }
    } else {
        addr_7fc5_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_7fcd_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0x8110;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0x7ee8;
        goto 0x8582;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0x8582;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_8006_26;
    rax36 = rcx31;
    addr_8006_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0x7ee8;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0x8582;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_2640(r15_40, rax36);
        if (!rax41) 
            goto 0x8582;
        goto 0x7ee8;
    }
    rax42 = fun_25f0(rax36, rsi10);
    if (!rax42) 
        goto 0x8582;
    if (r12_43) 
        goto addr_83ca_36;
    goto 0x7ee8;
    addr_83ca_36:
    fun_25c0(rax42, r15_44, r12_45, rax42, r15_44, r12_45);
    goto 0x7ee8;
    addr_80c3_5:
    if ((*reinterpret_cast<struct s20**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s20**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0x7ee8;
    }
    if (eax7 >= 0) 
        goto addr_7fc5_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0x8110;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_8107_42;
    eax50 = 84;
    addr_8107_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_8050() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_8140() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x8285;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_23c0(r15_4 + r12_5);
            goto 0x7f9d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x7f73;
        }
    }
}

void fun_8188() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_23c0(rdi5);
            goto 0x7f9d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_23c0(rdi5);
    goto 0x7f9d;
}

void fun_81f0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8076;
}

void fun_8240() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x8220;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x807f;
}

void fun_82f0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8076;
    goto 0x8220;
}

void fun_8383() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x7df2;
}

void fun_8520() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x84a7;
}

struct s22 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s23 {
    signed char[8] pad8;
    int64_t f8;
};

struct s24 {
    signed char[16] pad16;
    int64_t f10;
};

struct s25 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8b48() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s22* rcx3;
    struct s23* rcx4;
    int64_t r11_5;
    struct s24* rcx6;
    uint32_t* rcx7;
    struct s25* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x8b30; else 
        goto "???";
}

struct s26 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s27 {
    signed char[8] pad8;
    int64_t f8;
};

struct s28 {
    signed char[16] pad16;
    int64_t f10;
};

struct s29 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8b80() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s26* rcx3;
    struct s27* rcx4;
    int64_t r11_5;
    struct s28* rcx6;
    uint32_t* rcx7;
    struct s29* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x8b66;
}

struct s30 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s31 {
    signed char[8] pad8;
    int64_t f8;
};

struct s32 {
    signed char[16] pad16;
    int64_t f10;
};

struct s33 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_8ba0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s30* rcx3;
    struct s31* rcx4;
    int64_t r11_5;
    struct s32* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s33* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x8b66;
}

struct s34 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s35 {
    signed char[8] pad8;
    int64_t f8;
};

struct s36 {
    signed char[16] pad16;
    int64_t f10;
};

struct s37 {
    signed char[16] pad16;
    signed char f10;
};

void fun_8bc0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s34* rcx3;
    struct s35* rcx4;
    int64_t r11_5;
    struct s36* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s37* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x8b66;
}

struct s38 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s39 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_8c40() {
    struct s38* rcx1;
    struct s39* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x8b66;
}

struct s40 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s41 {
    signed char[8] pad8;
    int64_t f8;
};

struct s42 {
    signed char[16] pad16;
    int64_t f10;
};

struct s43 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8c90() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s40* rcx3;
    struct s41* rcx4;
    int64_t r11_5;
    struct s42* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s43* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x8b66;
}

void fun_8edc() {
}

void fun_8f0b() {
    goto 0x8ee8;
}

void fun_8f60() {
}

void fun_9170() {
    goto 0x8f63;
}

struct s44 {
    signed char[8] pad8;
    void** f8;
};

struct s45 {
    signed char[8] pad8;
    int64_t f8;
};

struct s46 {
    signed char[8] pad8;
    void** f8;
};

struct s47 {
    signed char[8] pad8;
    void** f8;
};

void fun_9218() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s44* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s45* r15_14;
    void** rax15;
    struct s46* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s47* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x9957;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x9957;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_25f0(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x9778; else 
                goto "???";
        }
    } else {
        rax15 = fun_2640(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x9957;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_9aaa_9; else 
            goto addr_928e_10;
    }
    addr_93e4_11:
    rax19 = fun_25c0(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_928e_10:
    r14_20->f8 = rcx12;
    goto 0x8da9;
    addr_9aaa_9:
    r13_18 = *r14_21;
    goto addr_93e4_11;
}

struct s48 {
    signed char[11] pad11;
    void** fb;
};

struct s49 {
    signed char[80] pad80;
    int64_t f50;
};

struct s50 {
    signed char[80] pad80;
    int64_t f50;
};

struct s51 {
    signed char[8] pad8;
    void** f8;
};

struct s52 {
    signed char[8] pad8;
    void** f8;
};

struct s53 {
    signed char[8] pad8;
    void** f8;
};

struct s54 {
    signed char[72] pad72;
    signed char f48;
};

struct s55 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_94a1() {
    void** ecx1;
    int32_t edx2;
    struct s48* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s49* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s50* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s51* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s52* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s53* r15_37;
    void*** r13_38;
    struct s54* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s55* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s48*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x9338;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x9a8c;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_95fd_12;
    } else {
        addr_91a4_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_91df_17;
        }
    }
    rax25 = fun_25f0(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x976a;
    addr_9710_19:
    rdx30 = *r15_31;
    rax32 = fun_25c0(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_9646_20:
    r15_33->f8 = r8_12;
    goto addr_91a4_13;
    addr_95fd_12:
    rax34 = fun_2640(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x9957;
    if (v36 != r15_37->f8) 
        goto addr_9646_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_9710_19;
    addr_91df_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x933c;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x9220;
    goto 0x8da9;
}

void fun_94bf() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x9188;
    if (dl2 & 4) 
        goto 0x9188;
    if (edx3 > 7) 
        goto 0x9188;
    if (dl4 & 2) 
        goto 0x9188;
    goto 0x9188;
}

void fun_9508() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x9188;
    if (dl2 & 4) 
        goto 0x9188;
    if (edx3 > 7) 
        goto 0x9188;
    if (dl4 & 2) 
        goto 0x9188;
    goto 0x9188;
}

void fun_9550() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x9188;
    if (dl2 & 4) 
        goto 0x9188;
    if (edx3 > 7) 
        goto 0x9188;
    if (dl4 & 2) 
        goto 0x9188;
    goto 0x9188;
}

void fun_9598() {
    goto 0x9188;
}

void fun_4ade() {
    goto 0x4752;
}

void fun_4cb4() {
    goto 0x4c6c;
}

void fun_4d7b() {
    goto 0x48a8;
}

void fun_4dcd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4d50;
    goto 0x497f;
}

void fun_4dff() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4d5b;
        goto 0x4780;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4bfa;
        goto 0x499b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5598;
    if (r10_8 > r15_9) 
        goto addr_4ce5_9;
    addr_4cea_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x55a3;
    goto 0x48d4;
    addr_4ce5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4cea_10;
}

void fun_4e32() {
    goto 0x4967;
}

void fun_5200() {
    goto 0x4967;
}

void fun_599f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4abc;
    } else {
        goto 0x4c20;
    }
}

void fun_69a0() {
}

struct s56 {
    signed char[1] pad1;
    signed char f1;
};

void fun_7de0() {
    signed char* r13_1;
    struct s56* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_852e() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x84a7;
}

struct s57 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s58 {
    signed char[8] pad8;
    int64_t f8;
};

struct s59 {
    signed char[16] pad16;
    int64_t f10;
};

struct s60 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8c60() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s57* rcx3;
    struct s58* rcx4;
    int64_t r11_5;
    struct s59* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s60* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x8b66;
}

void fun_8f15() {
    goto 0x8ee8;
}

void fun_9864() {
    goto 0x9188;
}

void fun_9178() {
}

void fun_95a8() {
    goto 0x9188;
}

void fun_4e3c() {
    goto 0x4dd7;
}

void fun_520a() {
    goto 0x4d2d;
}

void fun_6a00() {
    fun_2490();
    goto fun_26f0;
}

void fun_82c0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8076;
    goto 0x8220;
}

void fun_853c() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x84a7;
}

struct s61 {
    int32_t f0;
    int32_t f4;
};

struct s62 {
    int32_t f0;
    int32_t f4;
};

struct s63 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s64 {
    signed char[8] pad8;
    int64_t f8;
};

struct s65 {
    signed char[8] pad8;
    int64_t f8;
};

struct s66 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_8c10(struct s61* rdi, struct s62* rsi) {
    struct s63* rcx3;
    struct s64* rcx4;
    struct s65* rcx5;
    struct s66* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x8b66;
}

void fun_8f1f() {
    goto 0x8ee8;
}

void fun_986e() {
    goto 0x9188;
}

void fun_4b0d() {
    goto 0x4752;
}

void fun_4e48() {
    goto 0x4dd7;
}

void fun_5217() {
    goto 0x4d7e;
}

void fun_6a40() {
    fun_2490();
    goto fun_26f0;
}

void fun_854b() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x84a7;
}

void fun_8f29() {
    goto 0x8ee8;
}

void fun_4b3a() {
    goto 0x4752;
}

void fun_4e54() {
    goto 0x4d50;
}

void fun_6a80() {
    fun_2490();
    goto fun_26f0;
}

void fun_8f33() {
    goto 0x8ee8;
}

void fun_4b5c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x54f0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4a21;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4a21;
    }
    if (v11) 
        goto 0x5853;
    if (r10_12 > r15_13) 
        goto addr_58a3_8;
    addr_58a8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x55e1;
    addr_58a3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_58a8_9;
}

struct s67 {
    signed char[24] pad24;
    int64_t f18;
};

struct s68 {
    signed char[16] pad16;
    void** f10;
};

struct s69 {
    signed char[8] pad8;
    void** f8;
};

void fun_6ad0() {
    int64_t r15_1;
    struct s67* rbx2;
    void** r14_3;
    struct s68* rbx4;
    void** r13_5;
    struct s69* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2490();
    fun_26f0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6af2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6b28() {
    fun_2490();
    goto 0x6af9;
}

struct s70 {
    signed char[32] pad32;
    int64_t f20;
};

struct s71 {
    signed char[24] pad24;
    int64_t f18;
};

struct s72 {
    signed char[16] pad16;
    void** f10;
};

struct s73 {
    signed char[8] pad8;
    void** f8;
};

struct s74 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6b60() {
    int64_t rcx1;
    struct s70* rbx2;
    int64_t r15_3;
    struct s71* rbx4;
    void** r14_5;
    struct s72* rbx6;
    void** r13_7;
    struct s73* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s74* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2490();
    fun_26f0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6b94, __return_address(), rcx1);
    goto v15;
}

void fun_6bd8() {
    fun_2490();
    goto 0x6b9b;
}
