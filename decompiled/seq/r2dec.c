#include <stdint.h>

/* /tmp/tmpuw2etcww @ 0x3650 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpuw2etcww @ 0x4530 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000a905;
        rdx = 0x0000a8f8;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000a8ff;
        rdx = 0x0000a169;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000a901;
    rdx = 0x0000a8fc;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x7970 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x2610 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpuw2etcww @ 0x4610 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2740)() ();
    }
    rdx = 0x0000a960;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xa960 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000a909;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000a169;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000a98c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xa98c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000a8ff;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000a169;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000a8ff;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000a169;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000aa8c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xaa8c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000ab8c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xab8c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000a169;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000a8ff;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000a8ff;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000a169;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpuw2etcww @ 0x2740 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 29586 named .text */
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x5a30 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2745)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x2745 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x274a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x23e0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpuw2etcww @ 0x2750 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2755 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x275a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x275f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2764 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2769 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x276e */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2773 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2778 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x277d */
 
void c_strtold_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2782 */
 
void vasnprintf_cold (void) {
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x3740 */
 
uint64_t dbg_all_digits_p (int64_t arg1) {
    rdi = arg1;
    /* _Bool all_digits_p(char const * s); */
    eax = *(rdi);
    eax -= 0x30;
    if (eax <= 9) {
        rax = strlen (rdi);
        rbx = rax;
        rax = strspn (rbp, "0123456789");
        r8b = (rax == rbx) ? 1 : 0;
        eax = r8d;
        return rax;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x24b0 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpuw2etcww @ 0x2540 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmpuw2etcww @ 0x3780 */
 
uint64_t dbg_io_error (int64_t arg_1h, int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_78h;
    /* void io_error(); */
    rdi = stdout;
    clearerr_unlocked ();
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
}

/* /tmp/tmpuw2etcww @ 0x37c0 */
 
uint64_t dbg_seq_fast (size_t n, int64_t arg_8h, size_t arg_10h, char * ptr, size_t size, void * s2, void ** arg_30h, uint32_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg1, int64_t arg3, char * s1) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_78h;
    rdi = arg1;
    rdx = arg3;
    rsi = s1;
    /* void seq_fast(char const * a,char const * b,uintmax_t step); */
    rbx = rsi;
    *((rsp + 8)) = rdx;
    eax = strcmp (rbx, 0x0000a01b);
    *((rsp + 0x10)) = eax;
    if (*(rbp) != 0x30) {
        goto label_6;
    }
    rax = rbp;
    do {
        r14 = rax;
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x30);
    if (rbp == rax) {
        goto label_7;
    }
    if (dl != 0) {
        goto label_7;
    }
label_0:
    if (*(rbx) != 0x30) {
        goto label_8;
    }
    rax = rbx;
    do {
        r10 = rax;
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x30);
    if (rbx == rax) {
        goto label_9;
    }
    while (1) {
label_5:
        *((rsp + 0x18)) = r10;
        ebx = 0x1f;
        rax = strlen (r14);
        r15 = rax + 1;
        r13 = rax;
        r12 = rax;
        eax = *((rsp + 0x10));
        if (r15 >= rbx) {
            rbx = r15;
        }
        if (eax == 0) {
            goto label_10;
        }
        r10 = *((rsp + 0x18));
        *((rsp + 0x28)) = r10;
        rax = strlen (*((rsp + 0x18)));
        if (rbx < rax) {
            rbx = rax;
        }
        rcx = rbx + 1;
        rdi = rcx;
        *((rsp + 0x20)) = rcx;
        rax = xmalloc (rdi);
        rdi -= r13;
        *((rsp + 0x18)) = rax;
        rdi += rax;
        rax = memcpy (rbx, r14, r15);
        r15 = rax;
        rax = xmalloc (*((rsp + 0x20)));
        rdi -= rbp;
        r14 = rax;
        rdi += rax;
        rax = memcpy (rbx, *((rsp + 0x28)), rbp + 1);
        rcx = *((rsp + 0x20));
        *((rsp + 0x28)) = rax;
        if (r13 < rbp) {
            goto label_11;
        }
        if (r13 <= rbp) {
            eax = memcmp (r15, rax, r13);
            if (eax <= 0) {
                goto label_12;
            }
        }
        rax = free (*((rsp + 0x18)));
        rdi = r14;
        void (*0x23d0)() ();
label_9:
        r10 = rax;
    }
label_7:
    r14 = rax;
    goto label_0;
label_10:
    rcx = rbx + 1;
    ebp = 0;
    rdi = rcx;
    *((rsp + 0x20)) = rcx;
    rax = xmalloc (rdi);
    rdi -= r13;
    *((rsp + 0x18)) = rax;
    rdi += rax;
    rax = memcpy (rbx, r14, r15);
    *((rsp + 0x28)) = 0;
    r15 = rax;
label_12:
    rcx = *((rsp + 0x20));
label_11:
    rcx += rcx;
    eax = _init;
    if (rcx >= rax) {
        rax = rcx;
    }
    rdi = rax;
    *((rsp + 0x30)) = rax;
    rax = xmalloc (rdi);
    rdx = r13;
    rsi = r15;
    r14 = rax;
    rax = *((rsp + 0x30));
    rdi = r14;
    rax += r14;
    *((rsp + 0x20)) = rax;
    rax = mempcpy ();
    rcx = rax;
    r13b = (*((rsp + 0x10)) == 0) ? 1 : 0;
label_2:
    rax = *((rsp + 8));
    if (rax == 0) {
        goto label_13;
    }
    rsi = rax;
label_1:
    rax = r15 + r12 - 1;
    while (dl > 0x38) {
        rax--;
        *((rax + 1)) = 0x30;
        if (rax < r15) {
            goto label_14;
        }
        edx = *(rax);
    }
    edx++;
    *(rax) = dl;
label_3:
    rsi--;
    if (rsi != 0) {
        goto label_1;
    }
label_13:
    if (r12 >= rbp) {
        if (r13b != 0) {
            goto label_15;
        }
        if (r12 > rbp) {
            goto label_16;
        }
        *((rsp + 0x10)) = rcx;
        eax = memcmp (r15, *((rsp + 0x28)), r12);
        rcx = *((rsp + 0x10));
        if (eax > 0) {
            goto label_16;
        }
    }
label_15:
    rax = separator;
    r10 = rcx + 1;
    eax = *(rax);
    *(rcx) = al;
    if (rbx == r12) {
        goto label_17;
    }
label_4:
    rdx = r12;
    rsi = r15;
    rdi = r10;
    rax = mempcpy ();
    rcx = rax;
    rax = r12;
    rax = ~rax;
    rax += *((rsp + 0x20));
    if (rcx <= rax) {
        goto label_2;
    }
    rcx -= r14;
    edx = 1;
    rdi = r14;
    rsi = rcx;
    rcx = stdout;
    rax = fwrite_unlocked ();
    if (rax != 1) {
        goto label_18;
    }
    rcx = r14;
    goto label_2;
label_14:
    *((r15 - 1)) = 0x31;
    r12++;
    r15--;
    goto label_3;
label_17:
    rbx += rbx;
    rdi = *((rsp + 0x18));
    *((rsp + 0x38)) = r10;
    rcx = rbx + 1;
    rsi = rcx;
    *((rsp + 0x10)) = rcx;
    rax = xrealloc ();
    rsi = rax;
    *((rsp + 0x18)) = rax;
    rax = memmove (rax + r12, rsi, r12 + 1);
    rcx = *((rsp + 0x10));
    r10 = *((rsp + 0x38));
    r15 = rax;
    rcx += rcx;
    if (rcx <= *((rsp + 0x30))) {
        goto label_4;
    }
    r10 -= r14;
    rsi = rcx;
    rdi = r14;
    *((rsp + 0x20)) = rcx;
    *((rsp + 0x10)) = r10;
    rax = xrealloc ();
    rcx = *((rsp + 0x20));
    r10 = *((rsp + 0x10));
    r14 = rax;
    rax = rax + rcx;
    *((rsp + 0x30)) = rcx;
    r10 += r14;
    *((rsp + 0x20)) = rax;
    goto label_4;
label_16:
    *(rcx) = 0xa;
    rcx++;
    edx = 1;
    rdi = r14;
    rcx -= r14;
    rsi = rcx;
    rcx = stdout;
    rax = fwrite_unlocked ();
    rax--;
    if (rax == 0) {
        exit (0);
label_6:
        r14 = rbp;
        goto label_0;
label_8:
        r10 = rbx;
        goto label_5;
    }
label_18:
    io_error (rdi, rsi, rdx, rcx, r8, r9);
}

/* /tmp/tmpuw2etcww @ 0x3f70 */
 
int64_t dbg_scan_arg (int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2) {
    operand ret;
    int64_t var_10h;
    size_t var_20h;
    int64_t var_28h_2;
    int64_t var_38h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* operand scan_arg(char const * arg); */
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    al = xstrtold (rbp, 0, rsp + 0x10, dbg.cl_strtold);
    if (al == 0) {
        goto label_10;
    }
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    __asm ("fucompi st(0)");
    if (al == 0) {
        goto label_11;
    }
    rax = ctype_b_loc ();
    rcx = *(rax);
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
label_0:
        rbp++;
        edx = *(rbp);
    }
    if (dl == 0x2b) {
        goto label_0;
    }
    esi = 0x2e;
    rdi = rbp;
    rax = void (*0xffffffffff0124f0)(uint64_t) (0);
    rbx = rax;
    while (rax != 0) {
label_1:
        strcspn (rbp, 0x0000a0eb);
        if (*((rbp + rax)) == 0) {
            *(fp_stack--) = 0.0;
            *(fp_stack--) = fp_stack[?];
            fp_stack[0] *= fp_stack[1];
            __asm ("fucompi st(1)");
            fp_stack++;
            if (*((rbp + rax)) == 0) {
                goto label_3;
            }
            if (*((rbp + rax)) != 0) {
                goto label_3;
            }
            rax = strlen (rbp);
            *((rsp + 0x20)) = rax;
            r13 = rax;
            if (rbx == 0) {
                goto label_12;
            }
            rax = strcspn (rbx + 1, 0x0000a0ee);
            r14 = rax;
            if (rax <= 0x7fffffff) {
                goto label_13;
            }
label_5:
            if (rbp == rbx) {
                goto label_14;
            }
            eax = *((rbx - 1));
            eax -= 0x30;
            al = (eax > 9) ? 1 : 0;
            eax = (int32_t) al;
label_6:
            r13 += rax;
            *((rsp + 0x20)) = r13;
label_2:
            rax = strchr (rbp, 0x65);
            r13 = rax;
            if (rax == 0) {
                goto label_15;
            }
label_4:
            r15 = r13;
            *(rsp) = rdi;
            r15 -= rbp;
            rax = strtol (r13 + 1, 0, 0xa);
            rdi = *(rsp);
            r8 = rax;
            rax = 0x8000000000000002;
            if (r8 >= rax) {
                goto label_16;
            }
            rdx = 0x8000000000000001;
            ecx = *((rsp + 0x28));
label_7:
            ecx -= edx;
            *(rsp) = rdx;
            *((rsp + 0x28)) = ecx;
            rax = strlen (rbp);
            r15 += *((rsp + 0x20));
            rdx = *(rsp);
            r15 -= rax;
            if (rbx == 0) {
                goto label_17;
            }
            rbx++;
            rax = r15 + 1;
            if (r13 == rbx) {
                r15 = rax;
            }
            *((rsp + 0x20)) = r15;
label_9:
            rdx = -rdx;
label_8:
        }
label_3:
        __asm ("movdqa xmm0, xmmword [rsp + 0x10]");
        __asm ("movdqa xmm1, xmmword [rsp + 0x20]");
        *(r12) = xmm0;
        *((r12 + 0x10)) = xmm1;
        rax = *((rsp + 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_18;
        }
        rax = r12;
        return rax;
        rax = strchr (rbp, 0x70);
    }
    *((rsp + 0x28)) = 0;
    goto label_1;
label_12:
    r14d = 0;
    goto label_2;
label_15:
    rax = strchr (rbp, 0x45);
    r13 = rax;
    if (rax == 0) {
        goto label_3;
    }
    goto label_4;
label_13:
    *((rsp + 0x28)) = eax;
    if (rax != 0) {
        goto label_5;
    }
    rax |= 0xffffffffffffffff;
    goto label_6;
label_16:
    rax = strtol (rdi, 0, 0xa);
    rdx = rax;
    eax = *((rsp + 0x28));
    ecx = *((rsp + 0x28));
    if (rdx < 0) {
        goto label_7;
    }
    rcx = (int64_t) eax;
    rdi = rbp;
    *(rsp) = rdx;
    if (rcx > rdx) {
        rcx = rdx;
    }
    eax -= ecx;
    *((rsp + 0x28)) = eax;
    r13d = eax;
    rax = strlen (rdi);
    r15 += *((rsp + 0x20));
    rdx = *(rsp);
    r15 -= rax;
    cl = (r14 != 0) ? 1 : 0;
    *((rsp + 0x20)) = r15;
    al = (rbx != 0) ? 1 : 0;
    if ((cl & al) == 0) {
        goto label_19;
    }
    while (1) {
label_19:
        if (rdx <= r14) {
            r14 = rdx;
        }
        rdx -= r14;
        goto label_8;
label_14:
        eax = 1;
        goto label_6;
label_17:
        r15++;
        *((rsp + 0x20)) = r15;
        goto label_9;
        r15--;
        *((rsp + 0x20)) = r15;
    }
label_18:
    stack_chk_fail ();
label_11:
    rsi = rbp;
    edi = 1;
    rax = quote_n ();
    rsi = "not-a-number";
    edi = 0;
    rbx = rax;
    rax = quote_n ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid %s argument: %s");
    r8 = rbx;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    usage (1, rsi, rdx, rcx, r8, r9);
label_10:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid floating point argument: %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    rax = usage (1, rsi, rdx, rcx, r8, r9);
}

/* /tmp/tmpuw2etcww @ 0x3680 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x36b0 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x36f0 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_000023b0 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpuw2etcww @ 0x23b0 */
 
void fcn_000023b0 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpuw2etcww @ 0x3730 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpuw2etcww @ 0x9ac0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpuw2etcww @ 0x5f80 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x62b0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x24c0 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpuw2etcww @ 0x6d40 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x76d0 */
 
int64_t c_strtold (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rax = c_locale_cache;
    if (rax == 0) {
        goto label_1;
    }
label_0:
    rdi = c_locale_cache;
    if (rdi == 0) {
        goto label_2;
    }
    rax = uselocale ();
    r13 = rax;
    if (rax == 0) {
        goto label_2;
    }
    strtold (r12, rbp);
    ? = fp_stack[0];
    fp_stack--;
    rax = errno_location ();
    rdi = r13;
    r12d = *(rax);
    rax = uselocale ();
    if (rax == 0) {
        void (*0x277d)() ();
    }
    *(rbp) = r12d;
    *(fp_stack--) = fp_stack[?];
    return rax;
label_2:
    if (rbp != 0) {
        *(rbp) = r12;
        *(fp_stack--) = 0.0;
        return rax;
    }
    *(fp_stack--) = 0.0;
    return rax;
label_1:
    edx = 0;
    rsi = 0x0000a1a2;
    edi = 0x1fbf;
    rax = newlocale ();
    *(obj.c_locale_cache) = rax;
    goto label_0;
}

/* /tmp/tmpuw2etcww @ 0x74c0 */
 
int64_t dbg_xstrtold (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    char * terminator;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool xstrtold(char const * str,char const ** ptr,long double * result,long double (*)() convert); */
    r14 = rcx;
    r13 = rdx;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    errno_location ();
    rsi = rsp;
    rdi = rbx;
    *(rax) = 0;
    r12 = rax;
    void (*r14)() ();
    rax = *(rsp);
    if (rax == rbx) {
        goto label_3;
    }
    if (rbp == 0) {
        goto label_4;
    }
    *(fp_stack--) = 0.0;
    fp_tmp_0 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_0;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (fp_stack[0] != fp_stack[1]) {
        r8d = 1;
        if (fp_stack[0] == fp_stack[1]) {
            goto label_5;
        }
    }
label_0:
    r8b = (*(r12) != 0x22) ? 1 : 0;
label_2:
    if (rbp == 0) {
        goto label_1;
    }
label_5:
    *(rbp) = rax;
    do {
label_1:
        ? = fp_stack[0];
        fp_stack--;
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        eax = r8d;
        return rax;
label_4:
        r8d = 0;
    } while (*(rax) != 0);
    *(fp_stack--) = 0.0;
    fp_tmp_1 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_1;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (fp_stack[0] == fp_stack[1]) {
        goto label_0;
    }
    r8d = 1;
    if (fp_stack[0] == fp_stack[1]) {
        goto label_1;
    }
    goto label_0;
label_3:
    r8d = 0;
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x5dc0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x23f0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpuw2etcww @ 0x6f40 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x25f0 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpuw2etcww @ 0x7480 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000a8b5);
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2490 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpuw2etcww @ 0x26a0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpuw2etcww @ 0x2640 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpuw2etcww @ 0x2440 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpuw2etcww @ 0x5ce0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x79b0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x5fd0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2750)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x5d40 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x4480 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpuw2etcww @ 0x2510 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpuw2etcww @ 0x2400 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpuw2etcww @ 0x26e0 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpuw2etcww @ 0x6510 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x2769)() ();
    }
    if (rdx == 0) {
        void (*0x2769)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x65b0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x276e)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x276e)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x60f0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x275a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x6470 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2764)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x9ad4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpuw2etcww @ 0x6fc0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x70c0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x6f60 */
 
uint64_t xrealloc (char * arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x78a0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2600)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpuw2etcww @ 0x6f00 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x5fb0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x7440 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x6f20 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x6c80 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x6810)() ();
}

/* /tmp/tmpuw2etcww @ 0x43d0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000a8b5);
    } while (1);
}

/* /tmp/tmpuw2etcww @ 0x8a40 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpuw2etcww @ 0x6ec0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x89c0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x6660 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2773)() ();
    }
    if (rax == 0) {
        void (*0x2773)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x63e0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x7310 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x2580 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpuw2etcww @ 0x5c80 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x67a0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x5c20 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x7380 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x5ec0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x43c0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpuw2etcww @ 0x6f90 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x7150 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x7810 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2470)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpuw2etcww @ 0x7070 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x67b0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x6700 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2778)() ();
    }
    if (rax == 0) {
        void (*0x2778)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x73c0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x5eb0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x5dc0)() ();
}

/* /tmp/tmpuw2etcww @ 0x7a40 */
 
int64_t vasnprintf (void * arg1, void ** arg2, int64_t arg3, int64_t arg4) {
    int64_t var_418h;
    int64_t var_40ch;
    int64_t var_408h;
    void ** var_400h;
    int64_t var_3f8h;
    int64_t var_3f0h;
    void * s2;
    void * var_3e0h;
    int64_t var_3d8h;
    void ** var_3d0h;
    void ** var_3c8h;
    int64_t var_3bch;
    int64_t var_3b8h;
    int64_t var_3b4h;
    int64_t var_3b0h;
    void ** var_3a8h;
    int64_t var_3a0h;
    uint32_t var_2c0h;
    void ** var_2b8h;
    int64_t var_2b0h;
    int64_t var_2a8h;
    int64_t var_2a0h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    int64_t var_fh;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
label_0:
    abort ();
    r14 = section__gnu_hash;
    r13 = rdx;
    r12 = rcx;
    *((rbp - 0x3e8)) = rdi;
    *((rbp - 0x400)) = rsi;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = printf_parse (r13, rbp - 0x2c0, r14);
    if (eax < 0) {
        goto label_34;
    }
    eax = printf_fetchargs (r12, r14);
    if (eax < 0) {
        goto label_35;
    }
    rdx = *((rbp - 0x2b0));
    rax = rdx + 7;
    rdx = 0xffffffffffffffff;
    if (rdx >= 0xfffffffffffffff9) {
        rax = rdx;
    }
    rax += *((rbp - 0x2a8));
    if (rax < 0) {
        goto label_36;
    }
    rdi = rax;
    r8d = 0;
    rdi += 6;
    r8b = (rdi < 0) ? 1 : 0;
    if (rdi < 0) {
        goto label_36;
    }
    if (rdi <= 0xf9f) {
        goto label_37;
    }
    *((rbp - 0x3c8)) = r8;
    if (rdi == -1) {
        goto label_36;
    }
    rax = malloc (rdi);
    *((rbp - 0x3e0)) = rax;
    if (rax == 0) {
        goto label_36;
    }
    *((rbp - 0x408)) = rax;
    r8 = *((rbp - 0x3c8));
label_11:
    ebx = 0;
    if (*((rbp - 0x3e8)) != 0) {
        rax = *((rbp - 0x400));
        rbx = *(rax);
    }
    r14 = *((rbp - 0x2b8));
    r9 = r8;
    r8 = r13;
    *((rbp - section..dynsym)) = 0;
    r15 = *((rbp - 0x3e8));
    r13 = *(r14);
    if (r13 == r8) {
        goto label_38;
    }
label_15:
    r13 -= r8;
    r12 = r9;
    rax = 0xffffffffffffffff;
    r12 += r13;
    if (r12 < 0) {
        r12 = rax;
    }
    if (rbx >= r12) {
        goto label_39;
    }
    if (rbx == 0) {
        goto label_40;
    }
    if (rbx < 0) {
        goto label_41;
    }
    rbx += rbx;
label_17:
    if (rbx < r12) {
        rbx = r12;
    }
    if (rbx == -1) {
        goto label_41;
    }
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    if (r15 == *((rbp - 0x3e8))) {
        goto label_42;
    }
    rax = realloc (r15, rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
label_18:
    *((rbp - 0x3c8)) = r10;
    memcpy (r10 + r9, r8, r13);
    r10 = *((rbp - 0x3c8));
label_16:
    rax = *((rbp - section..dynsym));
    if (*((rbp - 0x2c0)) == rax) {
        goto label_43;
    }
    edx = *((r14 + 0x48));
    rax = *((r14 + 0x50));
    if (dl == 0x25) {
        goto label_44;
    }
    if (rax == -1) {
        void (*0x2782)() ();
    }
    r15 = *((rbp - 0x3a8));
    rax <<= 5;
    rax += r15;
    ecx = *(rax);
    *((rbp - 0x3c8)) = ecx;
    if (dl == 0x6e) {
        goto label_45;
    }
    rcx = *((rbp - 0x3e0));
    eax = *((r14 + 0x10));
    *((rbp - 0x3b8)) = 0;
    *(rcx) = 0x25;
    r13 = rcx + 1;
    if ((al & 1) != 0) {
        rdx = *((rbp - 0x3e0));
        *((rdx + 1)) = 0x27;
        r13 = rdx + 2;
    }
    if ((al & 2) != 0) {
        *(r13) = 0x2d;
        r13++;
    }
    if ((al & 4) != 0) {
        *(r13) = 0x2b;
        r13++;
    }
    if ((al & 8) != 0) {
        *(r13) = 0x20;
        r13++;
    }
    if ((al & 0x10) != 0) {
        *(r13) = 0x23;
        r13++;
    }
    if ((al & 0x40) != 0) {
        *(r13) = 0x49;
        r13++;
    }
    if ((al & 0x20) != 0) {
        *(r13) = 0x30;
        r13++;
    }
    rsi = *((r14 + 0x18));
    rax = *((r14 + 0x20));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    rsi = *((r14 + 0x30));
    rax = *((r14 + 0x38));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    eax = *((rbp - 0x3c8));
    eax -= 7;
    if (eax > 9) {
        goto label_12;
    }
    rcx = 0x0000b048;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (10 cases) at 0xb048 */
    void (*rax)() ();
label_36:
    errno_location ();
    *(rax) = 0xc;
    do {
label_4:
        rdi = *((rbp - 0x2b8));
        rax = rbp - 0x2a0;
        if (rdi != rax) {
            free (rdi);
        }
        rdi = *((rbp - 0x3a8));
        rax = rbp - 0x3a0;
        if (rdi != rax) {
            free (rdi);
        }
label_34:
        r10d = 0;
label_31:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_46;
        }
        rsp = rbp - 0x28;
        rax = r10;
        return rax;
label_35:
        errno_location ();
        *(rax) = 0x16;
    } while (1);
    *(r13) = 0x6c;
    r13++;
    *(r13) = 0x6c;
    r13++;
label_12:
    eax = *((r14 + 0x48));
    *((r13 + 1)) = 0;
    *(r13) = al;
    rax = *((r14 + 0x28));
    if (rax == -1) {
        goto label_47;
    }
    rax <<= 5;
    rax += r15;
    if (*(rax) != 5) {
        void (*0x2782)() ();
    }
    *((rbp - 0x3d8)) = 1;
    eax = *((rax + 0x10));
    *((rbp - 0x3b8)) = eax;
label_21:
    rax = *((r14 + 0x40));
    if (rax == -1) {
        goto label_48;
    }
    rax <<= 5;
    rcx = r15 + rax;
    if (*(rcx) != 5) {
        void (*0x2782)() ();
    }
    eax = *((rbp - 0x3d8));
    edx = *((rcx + 0x10));
    *((rbp + rax*4 - 0x3b8)) = edx;
    eax = rax + 1;
    *((rbp - 0x3d8)) = eax;
label_48:
    rax = r12;
    rax += 2;
    if (rax < 0) {
        goto label_49;
    }
    if (rbx >= rax) {
        goto label_50;
    }
    if (rbx != 0) {
        goto label_51;
    }
    if (rax > 0xc) {
        goto label_52;
    }
    ebx = 0xc;
label_26:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_53;
    }
    rdi = r10;
    *((rbp - 0x3d0)) = r10;
    rax = realloc (rdi, rbx);
    r10 = *((rbp - 0x3d0));
    r15 = rax;
    if (rax == 0) {
        goto label_25;
    }
label_22:
    *((r15 + r12)) = 0;
    rax = errno_location ();
    *((rbp - 0x3f0)) = r13;
    *((rbp - 0x3d0)) = rax;
    eax = *(rax);
    *((rbp - 0x40c)) = eax;
label_1:
    rax = *((rbp - 0x3d0));
    r13 = rbx;
    esi = 0x7fffffff;
    *((rbp - 0x3bc)) = 0xffffffff;
    r13 -= r12;
    *(rax) = 0;
    eax = *((rbp - 0x3c8));
    if (r13 <= rsi) {
        rsi = r13;
    }
    if (eax > 0x11) {
        goto label_0;
    }
    rdi = 0x0000b070;
    rax = *((rdi + rax*4));
    rax += rdi;
    /* switch table (18 cases) at 0xb070 */
    void (*rax)() ();
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_54;
    }
    if (eax == 2) {
        goto label_55;
    }
    rax = rbp - 0x3bc;
label_5:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    rsi = *((rbp - 0x418));
label_6:
    edx = *((rbp - 0x3bc));
    if (edx < 0) {
        goto label_56;
    }
label_2:
    rcx = (int64_t) edx;
    if (rcx >= rsi) {
        goto label_57;
    }
    rcx += r15;
    if (*((rcx + r12)) != 0) {
        void (*0x2782)() ();
    }
label_57:
    if (edx < eax) {
label_3:
        *((rbp - 0x3bc)) = eax;
        edx = eax;
    }
    eax = rdx + 1;
    if (rax < rsi) {
        goto label_58;
    }
    if (r13 > 0x7ffffffe) {
        goto label_59;
    }
    if (rbx < 0) {
        goto label_60;
    }
    eax = rdx + 2;
    rcx = rbx + rbx;
    rax += r12;
    if (rax < 0) {
        goto label_23;
    }
    if (rax < rcx) {
        rax = rcx;
    }
    if (rbx >= rax) {
        goto label_1;
    }
    if (rcx >= rax) {
        rax = rcx;
    }
    rbx = rax;
    if (rax == -1) {
        goto label_23;
    }
    if (r15 == *((rbp - 0x3e8))) {
        goto label_61;
    }
    rax = realloc (r15, rax);
    if (rax == 0) {
        goto label_23;
    }
    r15 = rax;
    goto label_1;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_10;
    }
label_7:
    if (eax == 2) {
        goto label_62;
    }
label_9:
    rax = rbp - 0x3bc;
label_8:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    edx = *((rbp - 0x3bc));
    rsi = *((rbp - 0x418));
    if (edx >= 0) {
        goto label_2;
    }
label_56:
    rcx = *((rbp - 0x3f0));
    if (*((rcx + 1)) != 0) {
        goto label_63;
    }
    if (eax >= 0) {
        goto label_3;
    }
    rax = *((rbp - 0x3d0));
    eax = *(rax);
    if (eax == 0) {
        eax = *((r14 + 0x48));
        edx = 0x54;
        rbx = *((rbp - 0x3d0));
        eax &= 0xffffffef;
        eax = 0x16;
        if (al == 0x63) {
            eax = edx;
        }
        *(rbx) = eax;
    }
label_20:
    if (r15 != *((rbp - 0x3e8))) {
        free (r15);
    }
    rax = *((rbp - 0x408));
    if (rax == 0) {
        goto label_4;
    }
    free (rax);
    goto label_4;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    *(fp_stack--) = fp_stack[?];
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_64;
    }
    if (eax == 2) {
        goto label_65;
    }
    r9 = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_5;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    xmm0 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_66;
    }
    if (eax == 2) {
        goto label_67;
    }
    r8 = *((rbp - 0x3e0));
    edx = 1;
    eax = 1;
    r9 = rbp - 0x3bc;
    rcx = 0xffffffffffffffff;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    do {
label_10:
        rax = rbp - 0x3bc;
label_14:
        r9d = *((rbp - 0x3b8));
        goto label_8;
        rax = *((r14 + 0x50));
        rdi = r15 + r12;
        rax <<= 5;
        rax += *((rbp - 0x3a8));
        r9d = *((rax + 0x10));
        eax = *((rbp - 0x3d8));
    } while (eax == 1);
    if (eax != 2) {
        goto label_9;
    }
label_62:
    rax = rbp - 0x3bc;
label_13:
    eax = *((rbp - 0x3b4));
label_24:
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    eax = 0;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
label_37:
    rax += 0x1d;
    rcx = rsp;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_68;
    }
    do {
    } while (rsp != rcx);
label_68:
    edx &= 0xfff;
    if (rdx != 0) {
        goto label_69;
    }
label_29:
    *((rbp - 0x408)) = 0;
    rax = rsp + 0xf;
    rax &= 0xfffffffffffffff0;
    *((rbp - 0x3e0)) = rax;
    goto label_11;
    *(r13) = 0x4c;
    r13++;
    goto label_12;
label_60:
    if (rbx == -1) {
        goto label_1;
    }
    goto label_23;
label_61:
    rax = malloc (rax);
    if (rax == 0) {
        goto label_23;
    }
    if (r12 == 0) {
        goto label_70;
    }
    rax = memcpy (rax, r15, r12);
    r15 = rax;
    goto label_1;
label_55:
    rax = rbp - 0x3bc;
    goto label_13;
label_54:
    rax = rbp - 0x3bc;
    goto label_14;
label_63:
    *((rcx + 1)) = 0;
    goto label_1;
label_44:
    if (rax != -1) {
        void (*0x2782)() ();
    }
    r15 = r12 + 1;
    r9 = 0xffffffffffffffff;
    if (r12 < -1) {
        r9 = r15;
    }
    if (rbx < r9) {
        if (rbx == 0) {
            goto label_71;
        }
        if (rbx < 0) {
            goto label_25;
        }
        rbx += rbx;
label_27:
        if (rbx < r9) {
            rbx = r9;
        }
        if (rbx == -1) {
            goto label_25;
        }
        if (r10 == *((rbp - 0x3e8))) {
            goto label_72;
        }
        rdi = r10;
        *((rbp - 0x3d0)) = r9;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, rbx);
        r9 = *((rbp - 0x3d0));
        if (rax == 0) {
            goto label_30;
        }
        r10 = rax;
    }
label_28:
    *((r10 + r12)) = 0x25;
    r15 = r10;
label_19:
    r8 = *((r14 + 8));
    r13 = *((r14 + 0x58));
    r14 += 0x58;
    *((rbp - section..dynsym))++;
    if (r13 != r8) {
        goto label_15;
    }
label_38:
    r12 = r9;
    r10 = r15;
    goto label_16;
label_40:
    ebx = 0xc;
    goto label_17;
label_39:
    r10 = r15;
    goto label_18;
label_45:
    r13d = ecx;
    r13d -= 0x12;
    if (r13d > 4) {
        goto label_0;
    }
    rcx = 0x0000b0b8;
    rax = *((rax + 0x10));
    rdx = *((rcx + r13*4));
    rdx += rcx;
    /* switch table (5 cases) at 0xb0b8 */
    void (*rdx)() ();
    *(rax) = r12;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12d;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12w;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12b;
    r9 = r12;
    r15 = r10;
    goto label_19;
label_49:
    if (rbx == -1) {
        goto label_50;
    }
label_25:
    *((rbp - 0x3c8)) = r10;
label_30:
    rax = errno_location ();
    r15 = *((rbp - 0x3c8));
    *((rbp - 0x3d0)) = rax;
label_23:
    rax = *((rbp - 0x3d0));
    *(rax) = 0xc;
    goto label_20;
label_47:
    *((rbp - 0x3d8)) = 0;
    goto label_21;
label_42:
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
    if (r9 == 0) {
        goto label_18;
    }
    rdx = r9;
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, *((rbp - 0x3e8)), rdx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    goto label_18;
label_50:
    r15 = r10;
    goto label_22;
label_41:
    rax = errno_location ();
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_65:
    rax = rbp - 0x3bc;
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    eax = *((rbp - 0x3b4));
    rcx = 0xffffffffffffffff;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
label_64:
    rax = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_24;
label_67:
    rax = rbp - 0x3bc;
    eax = *((rbp - 0x3b4));
    do {
        r9d = *((rbp - 0x3b8));
        edx = 1;
        eax = 1;
        r8 = *((rbp - 0x3e0));
        rcx = 0xffffffffffffffff;
        *((rbp - 0x418)) = rsi;
        snprintf_chk ();
        rsi = *((rbp - 0x418));
        goto label_6;
label_66:
        rax = rbp - 0x3bc;
    } while (1);
    if (r9 < 0) {
label_51:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= rax) {
        goto label_26;
    }
label_52:
    if (rax == -1) {
        goto label_25;
    }
    rbx = rax;
    goto label_26;
label_58:
    rax = (int64_t) edx;
    edx = *((rbp - 0x40c));
    r9 = rax + r12;
    rax = *((rbp - 0x3d0));
    *(rax) = edx;
    goto label_19;
label_59:
    rax = *((rbp - 0x3d0));
    *(rax) = 0x4b;
    goto label_20;
label_71:
    ebx = 0xc;
    goto label_27;
label_53:
    rax = malloc (rbx);
    r15 = rax;
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_22;
    }
    memcpy (rax, *((rbp - 0x3e8)), r12);
    goto label_22;
label_72:
    *((rbp - 0x3d0)) = r10;
    *((rbp - 0x3c8)) = r9;
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r10 = *((rbp - 0x3d0));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_74;
    }
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, r10, r12);
    r9 = *((rbp - 0x3c8));
    r10 = rax;
    goto label_28;
label_69:
    goto label_29;
label_43:
    r13 = r12;
    r13++;
    if (r13 < 0) {
        goto label_75;
    }
    if (rbx >= r13) {
        goto label_33;
    }
    if (rbx != 0) {
        goto label_76;
    }
    if (r13 > 0xc) {
        goto label_77;
    }
    ebx = 0xc;
label_32:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_78;
    }
    rdi = r10;
    *((rbp - 0x3c8)) = r10;
    rax = realloc (rdi, rbx);
    r10 = rax;
    if (rax == 0) {
        goto label_30;
    }
label_33:
    *((r10 + r12)) = 0;
    if (rbx > r13) {
        if (r10 == *((rbp - 0x3e8))) {
            goto label_79;
        }
        rdi = r10;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, r13);
        r10 = *((rbp - 0x3c8));
        if (rax == 0) {
            r10 = rax;
            goto label_79;
        }
    }
label_79:
    rdi = *((rbp - 0x408));
    if (rdi != 0) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x2b8));
    rax = rbp - 0x2a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x3a8));
    rax = rbp - 0x3a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rax = *((rbp - 0x400));
    *(rax) = r12;
    goto label_31;
    if (rdi < rax) {
label_76:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= r13) {
        goto label_32;
    }
label_77:
    if (r13 == -1) {
        goto label_25;
    }
    rbx = r13;
    goto label_32;
label_78:
    *((rbp - 0x3c8)) = r10;
    rax = malloc (rbx);
    r10 = *((rbp - 0x3c8));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_80;
    }
    rax = memcpy (rax, r10, r12);
    r10 = rax;
    goto label_33;
label_46:
    stack_chk_fail ();
label_75:
    if (rbx == -1) {
        goto label_33;
    }
    goto label_25;
label_73:
    rax = errno_location ();
    r15 = *((rbp - 0x3e8));
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_70:
    r15 = rax;
    goto label_1;
label_74:
    r10 = rax;
    goto label_28;
label_80:
    r10 = rax;
    goto label_33;
}

/* /tmp/tmpuw2etcww @ 0x8b00 */
 
int64_t dbg_printf_fetchargs (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int printf_fetchargs(__va_list_tag * args,arguments * a); */
    r8 = *(rsi);
    rax = *((rsi + 8));
    rcx = rdi;
    if (r8 == 0) {
        goto label_6;
    }
    esi = 0;
    rdi = 0x0000b0e0;
    r10 = "(NULL)";
    r9 = "(NULL)";
    do {
        if (*(rax) > 0x16) {
            goto label_7;
        }
        edx = *(rax);
        rdx = *((rdi + rdx*4));
        rdx += rdi;
        /* switch table (23 cases) at 0xb0e0 */
        rax = void (*rdx)() ();
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_8;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_1:
        rdx = *(r11);
        *((rax + 0x10)) = rdx;
label_0:
        rsi++;
        rax += 0x20;
    } while (rsi != r8);
label_6:
    eax = 0;
    return rax;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_9;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        edx = *(r11);
        *((rax + 0x10)) = edx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_10;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_2:
        edx = *(r11);
        *((rax + 0x10)) = dx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_11;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_3:
        edx = *(r11);
        *((rax + 0x10)) = dl;
        goto label_0;
label_8:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_1;
label_9:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
    edx = *((rcx + 4));
    if (edx > 0xaf) {
        goto label_12;
    }
    r11d = edx;
    edx += 0x10;
    r11 += *((rcx + 0x10));
    *((rcx + 4)) = edx;
label_4:
    xmm0 = *(r11);
    *((rax + 0x10)) = xmm0;
    goto label_0;
    rdx = *((rcx + 8));
    rdx += 0xf;
    rdx &= 0xfffffffffffffff0;
    r11 = rdx + 0x10;
    *((rcx + 8)) = r11;
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_13;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
label_5:
    rdx = *(r11);
    if (rdx == 0) {
        rdx = r10;
    }
    *((rax + 0x10)) = rdx;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_14;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        rdx = *(r11);
        if (rdx == 0) {
            rdx = r9;
        }
        *((rax + 0x10)) = rdx;
        goto label_0;
label_10:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_2;
label_11:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_3;
label_14:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
label_12:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_4;
label_13:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_5;
label_7:
    eax |= 0xffffffff;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x5f90 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x2790 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    char * s2;
    char * x_str;
    char * x0_str;
    long double x_val;
    operand step;
    int64_t var_8h_2;
    int64_t var_8h;
    size_t n;
    int64_t var_28h;
    size_t src;
    int64_t var_38h;
    int64_t var_30h;
    int64_t var_48h;
    uint32_t var_40h;
    int64_t var_68h;
    int64_t var_6ch;
    int64_t var_6eh;
    int64_t var_70h;
    void * var_78h;
    char * ptr;
    char * s1;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_b8h;
    int64_t var_c0h;
    size_t var_d0h;
    size_t var_d8h;
    int64_t var_e8h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    *(fp_stack--) = 1.0;
    r15 = rsi;
    r12 = 0x0000ad21;
    rbp = 0x0000a09d;
    ebx = edi;
    rax = *(fs:0x28);
    *((rsp + 0xe8)) = rax;
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    *((rsp + 0xb0)) = 1;
    *((rsp + 0xb8)) = 0;
    set_program_name (*(rsi), rsi, rdx);
    rax = setlocale (6, r12);
    obj.locale_ok = (rax != 0) ? 1 : 0;
    bindtextdomain (rbp, "/usr/local/share/locale");
    ebp = 0;
    textdomain (rbp, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    rdx = *(obj.optind);
    rax = 0x0000ad20;
    *(obj.equal_width) = 0;
    *(obj.separator) = rax;
    if (ebx <= edx) {
        goto label_6;
    }
    r14 = obj_long_options;
    r13 = "+f:s:w";
    do {
        rax = (int64_t) edx;
        rax = *((r15 + rax*8));
        if (*(rax) == 0x2d) {
            eax = *((rax + 1));
            if (al == 0x2e) {
                goto label_6;
            }
            eax -= 0x30;
            if (eax <= 9) {
                goto label_6;
            }
        }
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = r15;
        edi = ebx;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_30;
        }
        if (eax == 0x66) {
            goto label_31;
        }
        if (eax <= 0x66) {
            if (eax == 0xffffff7d) {
                goto label_32;
            }
            if (eax != 0xffffff7e) {
                goto label_9;
            }
            rax = usage (0, rsi, rdx, rcx, r8, r9);
        }
        if (eax == 0x73) {
            goto label_33;
        }
        if (eax != 0x77) {
            goto label_9;
        }
        *(obj.equal_width) = 1;
label_1:
        rdx = *(obj.optind);
    } while (edx < ebx);
label_6:
    eax = ebx;
    eax -= edx;
    *(rsp) = eax;
    if (eax == 0) {
        goto label_34;
    }
    if (*(rsp) > 3) {
        goto label_35;
    }
    if (rbp == 0) {
        goto label_36;
    }
    edx = 1;
    eax = 0;
    while (cl != 0x25) {
        if (cl == 0) {
            goto label_37;
        }
        ecx = 1;
label_0:
        rax += rcx;
        rdx++;
        ecx = *((rbp + rax));
        r13 = rdx - 1;
    }
    r14 = rax + 1;
    rdi = rbp + r14;
    if (*(rdi) != 0x25) {
        goto label_38;
    }
    ecx = 2;
    goto label_0;
label_34:
    edx = 5;
    rax = dcgettext (0, "missing operand");
    eax = 0;
    error (0, 0, rax);
label_9:
    eax = usage (1, rsi, rdx, rcx, r8, r9);
label_32:
    eax = 0;
    version_etc (*(obj.stdout), 0x0000a01f, "GNU coreutils", *(obj.Version), "Ulrich Drepper", 0);
    exit (0);
label_33:
    rax = optarg;
    *(obj.separator) = rax;
    goto label_1;
label_31:
    rbp = optarg;
    goto label_1;
label_36:
    r14d = 0;
    r13d = 0;
label_8:
    rcx = (int64_t) edx;
    if (*(rsp) == 3) {
        goto label_39;
    }
    r8 = *((r15 + rcx*8));
    *((rsp + 0x30)) = edx;
    *((rsp + 0x20)) = rcx;
    rdi = r8;
    *((rsp + 0x10)) = r8;
    al = all_digits_p (rdi);
    r8 = *((rsp + 0x10));
    rcx = *((rsp + 0x20));
    rdx = *((rsp + 0x30));
    if (al != 0) {
        goto label_40;
    }
    xmm1 = 0;
    *((rsp + 0x68)) = xmm1;
label_12:
    eax = rdx + 1;
    *(obj.optind) = eax;
    *(rsp) = rdi;
    scan_arg (rsp + 0xc0, *((r15 + rdx*8)), rdx, rcx, r8);
    rax = *(obj.optind);
    *(fp_stack--) = fp_stack[?];
    rcx = *((rsp + 0xd0));
    r8d = *((rsp + 0xd8));
    rdi = *(rsp);
    ? = fp_stack[0];
    fp_stack--;
    if (eax < ebx) {
        goto label_41;
    }
    eax = r8d;
    eax |= *((rsp + 0xb8));
    if (eax == 0) {
        goto label_42;
    }
    *(fp_stack--) = fp_stack[?];
    r10 = rcx;
    eax = r8d;
    ecx = 1;
    r8d = 0;
    ? = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = 1.0;
    ? = fp_stack[0];
    fp_stack--;
label_14:
    if (rbp == 0) {
        goto label_43;
    }
label_17:
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
label_15:
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(1)");
    fp_stack++;
    if (rbp > 0) {
        goto label_44;
    }
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    fp_stack++;
    bl = (rbp > 0) ? 1 : 0;
label_11:
    if (bl != 0) {
        goto label_10;
    }
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = 1.0;
    ? = fp_stack[0];
    fp_stack--;
    while (bl <= 0) {
        fp_stack++;
        *(fp_stack--) = fp_stack[?];
        *(fp_stack--) = fp_stack[?];
        __asm ("fcompi st(1)");
        fp_stack++;
        if (bl > 0) {
label_2:
            if (*(obj.locale_ok) != 0) {
                goto label_45;
            }
label_5:
            eax = 0;
            eax = rpl_asprintf (rsp + 0x80, rbp, rdx, rcx, r8, r9);
            rbx = (int64_t) eax;
            if (*(obj.locale_ok) != 0) {
                goto label_46;
            }
label_4:
            if (ebx < 0) {
                goto label_47;
            }
            rax = *((rsp + 0x80));
            rbx -= r14;
            *((rax + rbx)) = 0;
            rdi += r13;
            eax = xstrtold (*((rsp + 0x80)), 0, rsp + 0x90, dbg.cl_strtold);
            ebx = eax;
            if (al == 0) {
                goto label_48;
            }
            *(fp_stack--) = fp_stack[?];
            *(fp_stack--) = fp_stack[?];
            __asm ("fucompi st(1)");
            fp_stack++;
            if (al == 0) {
                goto label_48;
            }
            if (al != 0) {
                goto label_48;
            }
            eax = 0;
            *((rsp + 0x88)) = 0;
            eax = rpl_asprintf (rsp + 0x88, rbp, rdx, rcx, r8, r9);
            if (eax < 0) {
                goto label_47;
            }
            rdx = *((rsp + 0x88));
            rax = (int64_t) eax;
            rax -= r14;
            *((rdx + rax)) = 0;
            *((rsp + 0x20)) = rdi;
            eax = strcmp (*((rsp + 0x88)), *((rsp + 0x80)));
            r15d = eax;
            free (*((rsp + 0x20)));
            free (*((rsp + 0x80)));
            if (r15d == 0) {
                goto label_49;
            }
        }
label_3:
        rsi = stdout;
        rdi = separator;
        eax = fputs_unlocked ();
        eax++;
        if (eax == 0) {
            goto label_50;
        }
        *(fp_stack--) = fp_stack[?];
        *(fp_stack--) = 1.0;
        fp_stack[0] += fp_stack[1];
        fp_stack++;
        ? = fp_stack[0];
        fp_stack--;
        *(fp_stack--) = fp_stack[?];
        ? = fp_stack[0];
        fp_stack--;
        rsi = rbp;
        edi = 1;
        eax = 0;
        eax = printf_chk ();
        if (eax < 0) {
            goto label_50;
        }
        if (bl != 0) {
            goto label_49;
        }
        *(fp_stack--) = fp_stack[?];
        *(fp_stack--) = fp_stack[0];
        *(fp_stack--) = fp_stack[?];
        fp_stack[0] *= fp_stack[1];
        fp_stack++;
        *(fp_stack--) = fp_stack[?];
        fp_stack[0] += fp_stack[1];
        fp_stack++;
        *(fp_stack--) = fp_stack[0];
        ? = fp_stack[0];
        fp_stack--;
        *(fp_stack--) = 0.0;
        __asm ("fcompi st(2)");
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    fp_stack++;
    if (bl > 0) {
        goto label_2;
    }
    goto label_3;
label_46:
    setlocale (1, r12);
    goto label_4;
label_45:
    setlocale (1, 0x0000a1a2);
    goto label_5;
label_30:
    rdx = *(obj.optind);
    goto label_6;
label_10:
    exit (0);
label_38:
    rax = strspn (rdi, "-+#0 ');
    r14 += rax;
    strspn (rbp + r14, "0123456789");
    rsi = "0123456789";
    rdx = r14 + rax;
    if (*((rbp + rdx)) == 0x2e) {
        r14 = rdx + 1;
        strspn (rbp + r14, rsi);
        rdx = r14 + rax;
    }
    edi = 0;
    dil = (*((rbp + rdx)) == 0x4c) ? 1 : 0;
    r14 = rdx + rdi;
    rax = rbp + r14;
    r8d = *(rax);
    *((rsp + 0x20)) = rax;
    if (r8b == 0) {
        goto label_51;
    }
    esi = r8d;
    *((rsp + 0x30)) = rdx;
    *((rsp + 0x10)) = r8d;
    rax = strchr ("efgaEFGA", esi);
    r8d = *((rsp + 0x10));
    rdx = *((rsp + 0x30));
    rdi = r14 + 1;
    r10 = rax;
    eax = 1;
    if (r10 != 0) {
        goto label_52;
    }
    goto label_53;
    do {
        if (cl == 0) {
            goto label_54;
        }
        ecx = 1;
label_7:
        rdi += rcx;
        rax++;
label_52:
        ecx = *((rbp + rdi));
        r14 = rax - 1;
    } while (cl != 0x25);
    if (*((rbp + rdi + 1)) != 0x25) {
        goto label_55;
    }
    ecx = 2;
    goto label_7;
label_54:
    rdi += 2;
    *((rsp + 0x10)) = rdx;
    rax = xmalloc (rdi);
    rax = memcpy (rax, rbp, *((rsp + 0x10)));
    rdx = *((rsp + 0x10));
    *((rsp + 0x10)) = rax;
    *((rax + rdx)) = 0x4c;
    strcpy (rax + rdx + 1, *((rsp + 0x20)));
    if (*(obj.equal_width) != 0) {
        goto label_56;
    }
    rbp = *((rsp + 0x10));
    edx = optind;
    goto label_8;
label_35:
    rax = quote (*((r15 + rdx*8 + 0x18)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
label_13:
    rax = dcgettext (0, "extra operand %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_9;
label_48:
    free (*((rsp + 0x80)));
label_49:
    rsi = stdout;
    rdi = obj_terminator;
    eax = fputs_unlocked ();
    eax++;
    if (eax != 0) {
        goto label_10;
    }
label_50:
    io_error (rdi, rsi, rdx, rcx, r8, r9);
label_44:
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    fp_stack++;
    bl = (eax > 0) ? 1 : 0;
    goto label_11;
label_39:
    *((rsp + 0x20)) = edx;
    *((rsp + 0x10)) = rdi;
    al = all_digits_p (*((r15 + rcx*8 + 8)));
    xmm0 = 0;
    rdi = *((rsp + 0x10));
    edx = *((rsp + 0x20));
    *((rsp + 0x68)) = xmm0;
    if (al != 0) {
        goto label_57;
    }
label_19:
    *((rsp + 0x40)) = 0;
label_18:
    rcx = (int64_t) edx;
    *((rsp + 0x30)) = edx;
    r8 = *((r15 + rcx*8));
    *((rsp + 0x20)) = rcx;
    rdi = r8;
    *((rsp + 0x10)) = r8;
    al = all_digits_p (rdi);
    r8 = *((rsp + 0x10));
    rcx = *((rsp + 0x20));
    rdx = *((rsp + 0x30));
    if (al == 0) {
        goto label_12;
    }
    rcx++;
    *((rsp + 0x20)) = edx;
    rax = rcx*8;
    *((rsp + 0x10)) = r8;
    *((rsp + 0x30)) = rax;
    al = all_digits_p (*((r15 + rcx*8)));
    r8 = *((rsp + 0x10));
    rdx = *((rsp + 0x20));
    if (al == 0) {
        goto label_12;
    }
    *((rsp + 0x10)) = r8;
    if (*((rsp + 0x40)) == 0) {
        goto label_12;
    }
    rax = *((rsp + 0x30));
    *((rsp + 0x20)) = edx;
    al = all_digits_p (*((r15 + rax + 8)));
    rdx = *((rsp + 0x20));
    r8 = *((rsp + 0x10));
    if (al == 0) {
        goto label_12;
    }
label_16:
    if (*(obj.equal_width) == 1) {
        goto label_12;
    }
    if (rbp != 0) {
        goto label_12;
    }
    *((rsp + 0x20)) = edx;
    *((rsp + 0x10)) = r8;
    rax = strlen (*(obj.separator));
    r8 = *((rsp + 0x10));
    rdx = *((rsp + 0x20));
    rax--;
    if (rax != 0) {
        goto label_12;
    }
    *(fp_stack--) = *(0x0000a8a0);
    *(fp_stack--) = fp_stack[?];
    rax = 0x0000a0f1;
    if (*(rsp) == 1) {
        r8 = rax;
    }
    if (fp_stack[0] >= fp_stack[1]) {
        goto label_58;
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    eax = *((rsp + 0x6e));
    ah |= 0xc;
    *((rsp + 0x6c)) = ax;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
label_27:
    eax = *(rsp);
    eax = rax + rdx - 1;
    seq_fast (r8, *((r15 + rax*8)), r10, *((rsp + 0x10)), r8, r9);
    rdx = *(obj.optind);
    goto label_12;
label_41:
    *((rsp + 0x10)) = r8d;
    *(rsp) = rcx;
    *(obj.optind) = edx;
    *((rsp + 0x20)) = rdi;
    scan_arg (rdi, *((r15 + rax*8)), rax + 1, rcx, r8);
    edx = optind;
    rcx = *(rsp);
    *(fp_stack--) = fp_stack[?];
    r10 = *((rsp + 0xd0));
    eax = *((rsp + 0xd8));
    r8d = *((rsp + 0x10));
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    if (edx < ebx) {
        __asm ("movdqa xmm3, xmmword [rsp + 0xc0]");
        *(fp_stack--) = 0.0;
        fp_tmp_0 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_0;
        rax = (int64_t) edx;
        rdi = *((rsp + 0x20));
        __asm ("movdqa xmm4, xmmword [rsp + 0xd0]");
        __asm ("fucompi st(1)");
        fp_stack++;
        rsi = rax*8;
        *((rsp + 0xa0)) = xmm3;
        *((rsp + 0xb0)) = xmm4;
        if (edx != ebx) {
            if (edx != ebx) {
                goto label_59;
            }
            rax = quote (*((r15 + rsi - 8)), rsi, rdx, rcx, r8);
            edx = 5;
            rsi = "invalid Zero increment value: %s";
            r12 = rax;
            goto label_13;
        }
label_59:
        edx++;
        *((rsp + 0x10)) = r8d;
        *(rsp) = rcx;
        *(obj.optind) = edx;
        scan_arg (rdi, *((r15 + rax*8)), rdx, rcx, r8);
        *(fp_stack--) = fp_stack[?];
        r8d = *((rsp + 0x10));
        r10 = *((rsp + 0xd0));
        eax = *((rsp + 0xd8));
        rcx = *(rsp);
        ? = fp_stack[0];
        fp_stack--;
    } else {
        fp_stack++;
    }
    edx = *((rsp + 0xb8));
    edx |= r8d;
    edx |= eax;
    if (edx != 0) {
        goto label_14;
    }
    *(fp_stack--) = *((rsp + 0x68));
    *(fp_stack--) = fp_stack[0];
    *(fp_stack--) = fp_stack[?];
    fp_stack[0] *= fp_stack[2];
    fp_stack++;
    fp_tmp_1 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_1;
    __asm ("fucompi st(1)");
    if (edx != 0) {
        if (edx == 0) {
            goto label_60;
        }
        fp_stack++;
    } else {
        fp_stack++;
    }
label_21:
    if (rbp == 0) {
        goto label_61;
    }
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_15;
label_40:
    xmm2 = 0;
    *((rsp + 0x68)) = xmm2;
    if (*(rsp) == 1) {
        goto label_16;
    }
    *((rsp + 0x20)) = edx;
    *((rsp + 0x10)) = r8;
    al = all_digits_p (*((r15 + rcx*8 + 8)));
    r8 = *((rsp + 0x10));
    rdx = *((rsp + 0x20));
    if (al == 0) {
        goto label_12;
    }
    goto label_16;
label_56:
    edx = 5;
    rax = dcgettext (0, "format string may not be specified when printing equal width strings");
    eax = 0;
    error (0, 0, rax);
    eax = usage (1, rsi, rdx, rcx, r8, r9);
label_20:
    rbp = "%0.Lf";
    eax = 0;
    eax = rpl_asprintf (rsp + 0x70, rbp, rdx, rcx, r8, r9);
    rcx = *(rsp);
    r10 = *((rsp + 0x10));
    *(fp_stack--) = 0.0;
    if (eax < 0) {
        goto label_62;
    }
    *(fp_stack--) = fp_stack[?];
    *((rsp + 0x10)) = r10;
    *(rsp) = rcx;
    fp_stack[0] *= fp_stack[1];
    __asm ("fucompi st(1)");
    fp_stack++;
    if (eax != 0) {
        if (eax == 0) {
            goto label_63;
        }
    }
    rax = xstrdup (0x0000a01b);
    rcx = *(rsp);
    r10 = *((rsp + 0x10));
    *((rsp + 0x78)) = rax;
label_28:
    rdi = *((rsp + 0x70));
    if (*(rdi) != 0x2d) {
        rsi = *((rsp + 0x78));
        if (*(rsi) == 0x2d) {
            goto label_64;
        }
        *(fp_stack--) = *(0x0000a8a0);
        *(fp_stack--) = fp_stack[?];
        if (fp_stack[0] >= fp_stack[1]) {
            goto label_65;
        }
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        eax = *((rsp + 0x6e));
        ah |= 0xc;
        *((rsp + 0x6c)) = ax;
        *(rsp) = fp_stack[0];
        fp_stack--;
label_29:
        *((rsp + 0x10)) = r10;
        *(rsp) = rcx;
        seq_fast (rdi, rsi, *(rsp), rcx, r8, r9);
        r10 = *((rsp + 0x10));
        rcx = *(rsp);
    }
label_64:
    *((rsp + 0x10)) = r10;
    *(rsp) = rcx;
    free (*((rsp + 0x70)));
    eax = free (*((rsp + 0x78)));
    rcx = *(rsp);
    r10 = *((rsp + 0x10));
    eax = 0;
    r8d = 0;
label_43:
    r9d = *((rsp + 0xb8));
    if (r8d >= r9d) {
        r9d = r8d;
    }
    if (r9d == 0x7fffffff) {
        goto label_66;
    }
    if (eax == 0x7fffffff) {
        goto label_66;
    }
label_26:
    if (*(obj.equal_width) == 0) {
        goto label_67;
    }
    edx = r9d;
    edx -= r8d;
    rdx = (int64_t) edx;
    rdx += rcx;
    ecx = r9d;
    ecx -= eax;
    rcx = (int64_t) ecx;
    rcx += r10;
    sil = (r9d != 0) ? 1 : 0;
    if (r9d != 0) {
        goto label_68;
    }
    if (eax != 0) {
        goto label_69;
    }
label_24:
    rcx -= 0xffffffffffffffff;
label_23:
    if (r8d == 0) {
        rdx -= 0xffffffffffffffff;
    }
label_25:
    rbp = 0x0000a0f3;
    if (rdx < rcx) {
        rdx = rcx;
    }
    if (rdx > 0x7fffffff) {
        goto label_17;
    }
    rbp = obj_format_buf_0;
    r8d = edx;
    esi = 1;
    eax = 0;
    rcx = "%%0%d.%dLf";
    edx = 0x1c;
    rdi = rbp;
    sprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_15;
label_57:
    al = xstrtold (rdi, 0, rsp + 0xa0, dbg.cl_strtold);
    if (al != 0) {
        *(fp_stack--) = 0.0;
        *(fp_stack--) = fp_stack[?];
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        if (fp_stack[0] > fp_stack[1]) {
            *(fp_stack--) = *(0x0000a89c);
            edx = optind;
            __asm ("fcompi st(1)");
            fp_stack++;
            rsp + 0x40 = (fp_stack[0] >= fp_stack[1]) ? 1 : 0;
            goto label_18;
label_66:
            *(fp_stack--) = fp_stack[?];
            rbp = 0x0000a0f3;
            ? = fp_stack[0];
            fp_stack--;
            goto label_15;
        }
        fp_stack++;
    }
    edx = optind;
    goto label_19;
label_42:
    *(fp_stack--) = fp_stack[?];
    r10 = rcx;
    ecx = 1;
    ? = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = 1.0;
    ? = fp_stack[0];
    fp_stack--;
label_22:
    *(fp_stack--) = 0.0;
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (fp_stack[0] < fp_stack[1]) {
        goto label_70;
    }
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (fp_stack[0] <= fp_stack[1]) {
        goto label_71;
    }
    *(fp_stack--) = *(0x0000a89c);
    __asm ("fcompi st(1)");
    fp_stack++;
    if (fp_stack[0] < fp_stack[1]) {
        goto label_72;
    }
    if (*(obj.equal_width) != 0) {
        goto label_72;
    }
    if (rbp != 0) {
        goto label_15;
    }
    *((rsp + 0x10)) = r10;
    *(rsp) = rcx;
    rax = strlen (*(obj.separator));
    rax--;
    if (rax == 0) {
        goto label_20;
    }
    eax = *((rsp + 0xb8));
    r9d = 0;
    rbp = 0x0000a0f3;
    __asm ("cmovns r9d, eax");
    if (eax == 0x7fffffff) {
        goto label_15;
    }
label_67:
    rbp = obj_format_buf_0;
    r8d = r9d;
    edx = 0x1c;
    rcx = "%%.%dLf";
    esi = 1;
    rdi = rbp;
    eax = 0;
    eax = sprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_15;
label_70:
    fp_stack++;
    goto label_72;
label_71:
    fp_stack++;
label_72:
    eax = 0;
    r8d = 0;
    goto label_14;
label_60:
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    fp_stack++;
    if (eax < 0x7fffffff) {
        goto label_21;
    }
    goto label_22;
label_68:
    if (eax != 0) {
        goto label_23;
    }
    goto label_24;
label_69:
    rcx--;
    goto label_25;
label_37:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "format %s has no %% directive");
    rcx = r12;
    eax = 0;
    eax = error (1, 0, rax);
label_61:
    r9d = *((rsp + 0xb8));
    eax = 0;
    __asm ("cmovs r9d, eax");
    r8d = 0;
    goto label_26;
label_58:
    fp_stack[1] -= fp_stack[0];
    fp_stack++;
    eax = *((rsp + 0x6e));
    ah |= 0xc;
    *((rsp + 0x6c)) = ax;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    r10 = *((rsp + 0x10));
    __asm ("btc r10, 0x3f");
    goto label_27;
label_63:
    eax = 0;
    eax = rpl_asprintf (rsp + 0x78, rbp, rdx, rcx, r8, r9);
    rcx = *(rsp);
    r10 = *((rsp + 0x10));
    if (eax >= 0) {
        goto label_28;
    }
    goto label_47;
label_62:
    fp_stack++;
label_47:
    xalloc_die ();
label_65:
    fp_stack[1] -= fp_stack[0];
    fp_stack++;
    eax = *((rsp + 0x6e));
    ah |= 0xc;
    *((rsp + 0x6c)) = ax;
    *(rsp) = fp_stack[0];
    fp_stack--;
    rdx = *(rsp);
    __asm ("btc rdx, 0x3f");
    goto label_29;
label_51:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "format %s ends in %%");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_55:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "format %s has too many %% directives");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_53:
    *(rsp) = r8d;
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "format %s has unknown %%%c directive");
    r8d = *(rsp);
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
}

/* /tmp/tmpuw2etcww @ 0x3b90 */
 
int64_t dbg_usage (char * arg_8h, int64_t arg_10h, char * arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, char * arg_60h, int64_t arg_68h, int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... LAST\n  or:  %s [OPTION]... FIRST LAST\n  or:  %s [OPTION]... FIRST INCREMENT LAST\n");
    rcx = r12;
    r8 = r12;
    rdx = r12;
    rsi = rax;
    edi = 1;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Print numbers from FIRST to LAST, in steps of INCREMENT.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -f, --format=FORMAT      use printf style floating-point FORMAT\n  -s, --separator=STRING   use STRING to separate numbers (default: \\n)\n  -w, --equal-width        equalize width by padding with leading zeroes\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nIf FIRST or INCREMENT is omitted, it defaults to 1.  That is, an\nomitted INCREMENT defaults to 1 even when LAST is smaller than FIRST.\nThe sequence of numbers ends when the sum of the current number and\nINCREMENT would become greater than LAST.\nFIRST, INCREMENT, and LAST are interpreted as floating point values.\nINCREMENT is usually positive if FIRST is smaller than LAST, and\nINCREMENT is usually negative if FIRST is greater than LAST.\nINCREMENT must not be 0; none of FIRST, INCREMENT and LAST may be NaN.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "FORMAT must be suitable for printing one argument of type 'double';\nit defaults to %.PRECf if FIRST, INCREMENT, and LAST are all fixed point\ndecimal numbers with maximum precision PREC, and to %g otherwise.\n");
    rsi = r12;
    r12 = 0x0000a01f;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000a023;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x0000a09d;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000a0a7, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000ad21;
    r12 = 0x0000a03f;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000a0a7, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = 0x0000a01f;
        printf_chk ();
        r12 = 0x0000a03f;
    }
label_5:
    r13 = 0x0000a01f;
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpuw2etcww @ 0x5c60 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x6350 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x6180 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x275f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x6060 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2755)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x7650 */
 
int64_t dbg_rpl_vasprintf (int64_t arg1, int64_t arg2, int64_t arg3) {
    size_t length;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_vasprintf(char ** resultp,char const * format,__va_list_tag * args); */
    r8 = rsi;
    rbx = rdi;
    rcx = rdx;
    edi = 0;
    rdx = r8;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rsi = rsp;
    rax = vasnprintf ();
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = *(rsp);
    if (rax > 0x7fffffff) {
        goto label_2;
    }
    *(rbx) = rdi;
    do {
label_0:
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_3;
        }
        return rax;
label_2:
        free (rdi);
        errno_location ();
        *(rax) = 0x4b;
        eax = 0xffffffff;
    } while (1);
label_1:
    eax = 0xffffffff;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x7590 */
 
int64_t dbg_rpl_asprintf (int64_t arg_e0h, void * arg1, int64_t arg10, int64_t arg11, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list args;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* int rpl_asprintf(char ** resultp,char const * format,va_args ...); */
    *((rsp + 0x30)) = rdx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x40)) = r8;
    *((rsp + 0x48)) = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = rsp + 0xe0;
    *(rsp) = 0x10;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    rpl_vasprintf (rdi, rsi, rsp);
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x67f0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x5d00 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x274a)() ();
    }
    if (rdx == 0) {
        void (*0x274a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x6210 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x72e0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x7360 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x5f60 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x4300 */
 
int64_t dbg_cl_strtold (uint32_t canary, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2) {
    char * end;
    char * c_end;
    char * * endptr;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* long double cl_strtold(char const * nptr,char ** restrict endptr); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    strtold (rdi, rsp + 0x18);
    rax = *((rsp + 0x18));
    *(fp_stack--) = fp_stack[0];
    if (*(rax) != 0) {
        goto label_1;
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    do {
label_0:
        if (rbx != 0) {
            rax = *((rsp + 0x18));
            *(rbx) = rax;
        }
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        return rax;
label_1:
        fp_stack++;
        ? = fp_stack[0];
        fp_stack--;
        rax = errno_location ();
        rsi = rsp + 0x20;
        rdi = rbp;
        r13d = *(rax);
        r12 = rax;
        c_strtold ();
        rax = *((rsp + 0x20));
        *(fp_stack--) = fp_stack[?];
        if (*((rsp + 0x18)) >= rax) {
            goto label_3;
        }
        fp_stack++;
        *((rsp + 0x18)) = rax;
    } while (1);
label_3:
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *(r12) = r13d;
    goto label_0;
label_2:
    fp_stack++;
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x7000 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x67d0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpuw2etcww @ 0x78f0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x26b0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpuw2etcww @ 0x5ca0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x71e0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpuw2etcww @ 0x6e20 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpuw2etcww @ 0x25a0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpuw2etcww @ 0x2660 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpuw2etcww @ 0x43b0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpuw2etcww @ 0x77a0 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpuw2etcww @ 0x2430 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpuw2etcww @ 0x6810 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000acc8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000acdb);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000afc8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xafc8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x26f0)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x26f0)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x26f0)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpuw2etcww @ 0x8d30 */
 
int64_t dbg_printf_parse (int64_t arg1, int64_t arg2, int64_t arg3, size_t sum) {
    int64_t var_1h;
    int64_t var_4ch;
    int64_t var_30h;
    int64_t var_25h;
    int64_t var_bp_20h;
    int64_t var_8h;
    int64_t var_10h;
    void * s2;
    int64_t var_20h;
    void ** var_28h;
    void ** var_sp_30h;
    void ** var_38h;
    void ** var_40h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r9 = sum;
    /* int printf_parse(char const * format,char_directives * d,arguments * a); */
    r10 = rsi + 0x20;
    rax = rdi;
    rdi = rdx + 0x10;
    r15 = rdx;
    r14 = rsi;
    rcx = r10;
    r9d = 7;
    r13d = 0;
    r11d = 7;
    *(rsi) = 0;
    *((rsi + 8)) = r10;
    *((rsp + 0x18)) = rdi;
    *(rdx) = 0;
    *((rdx + 8)) = rdi;
    *((rsp + 0x10)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x20)) = 0;
    while (dl != 0) {
        rbx = rax + 1;
        if (dl == 0x25) {
            goto label_34;
        }
label_4:
        rax = rbx;
        edx = *(rax);
    }
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    *((rcx + rdx*8)) = rax;
    rax = *((rsp + 8));
    *((r14 + 0x10)) = rax;
    rax = *((rsp + 0x10));
    *((r14 + 0x18)) = rax;
    eax = 0;
label_7:
    return rax;
label_34:
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    r13 = 0xffffffffffffffff;
    r12 = rcx + rdx*8;
    *(r12) = rax;
    *((r12 + 0x10)) = 0;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x28)) = 0xffffffffffffffff;
    *((r12 + 0x30)) = 0;
    *((r12 + 0x38)) = 0;
    *((r12 + 0x40)) = 0xffffffffffffffff;
    *((r12 + 0x50)) = 0xffffffffffffffff;
    ebp = *((rax + 1));
    edx = rbp - 0x30;
    if (dl <= 9) {
        goto label_35;
    }
label_5:
    rcx = 0x0000b15c;
    rdx = rbx + 1;
    if (bpl == 0x27) {
        goto label_36;
    }
    do {
        eax = rbp - 0x20;
        if (al <= 0x29) {
            eax = (int32_t) al;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (42 cases) at 0xb15c */
            void (*rax)() ();
        }
        if (bpl == 0x2a) {
            goto label_37;
        }
        eax = rbp - 0x30;
        if (al <= 9) {
            goto label_38;
        }
label_17:
        if (bpl == 0x2e) {
            goto label_39;
        }
label_3:
        edx = 0;
        rsi = 0x0000b204;
        edi = 1;
        rbx++;
        if (bpl == 0x68) {
            goto label_40;
        }
label_1:
        eax = rbp - 0x4c;
        if (al > 0x2e) {
            goto label_41;
        }
        eax = (int32_t) al;
        rax = *((rsi + rax*4));
        rax += rsi;
        /* switch table (47 cases) at 0xb204 */
        void (*rax)() ();
        eax = *((r12 + 0x10));
        eax |= 0x40;
label_0:
        *((r12 + 0x10)) = eax;
        ebp = *(rdx);
        rbx = rdx;
        rdx = rbx + 1;
    } while (bpl != 0x27);
label_36:
    eax = *((r12 + 0x10));
    eax |= 1;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x20;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 2;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 4;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x10;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 8;
    goto label_0;
label_41:
    eax = rbp - 0x25;
    if (al > 0x53) {
        goto label_10;
    }
    rcx = 0x0000b2c0;
    eax = (int32_t) al;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (84 cases) at 0xb2c0 */
    void (*rax)() ();
    edx += 8;
label_2:
    ebp = *(rbx);
    rbx++;
    if (bpl != 0x68) {
        goto label_1;
    }
label_40:
    ecx = edx;
    eax = edi;
    ecx &= 1;
    eax <<= cl;
    edx |= eax;
    goto label_2;
label_37:
    rdi = *((rsp + 8));
    eax = 1;
    *((r12 + 0x18)) = rbx;
    *((r12 + 0x20)) = rdx;
    ecx = *((rbx + 1));
    if (rdi != 0) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
    eax = rcx - 0x30;
    if (al <= 9) {
        goto label_42;
    }
label_9:
    rdi = *((rsp + 0x20));
    *((r12 + 0x28)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbp = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    rbx = rdx;
label_29:
    rdx = *((r15 + 8));
    r8 = *((r15 + 8));
    if (r9 <= rbp) {
        r9 += r9;
        rax = rbp + 1;
        if (r9 <= rbp) {
            r9 = rax;
        }
        rax = r9;
        rax >>= 0x3b;
        if (rax != 0) {
            goto label_30;
        }
        rsi = r9;
        rsi <<= 5;
        if (*((rsp + 0x18)) == rdx) {
            goto label_43;
        }
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r11;
        *((rsp + 0x28)) = r10;
        rax = realloc (rdx, rsi);
        rdx = *((r15 + 8));
        r10 = *((rsp + 0x28));
        r11 = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r8 = rax;
label_20:
        if (r8 == 0) {
            goto label_30;
        }
        if (*((rsp + 0x18)) == rdx) {
            goto label_44;
        }
label_26:
        *((r15 + 8)) = r8;
    }
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbp) {
        goto label_45;
    }
    do {
        rdx++;
        *(rax) = 0;
        rcx = rax;
        rax += 0x20;
    } while (rdx <= rbp);
    *(r15) = rdx;
    *(rcx) = 0;
label_45:
    rbp <<= 5;
    rbp += r8;
    eax = *(rbp);
    if (eax != 0) {
        goto label_46;
    }
    *(rbp) = 5;
    ebp = *(rbx);
    if (bpl != 0x2e) {
        goto label_3;
    }
label_39:
    if (*((rbx + 1)) != 0x2a) {
        goto label_47;
    }
    rdi = *((rsp + 0x10));
    eax = 2;
    rcx = rbx + 2;
    *((r12 + 0x30)) = rbx;
    *((r12 + 0x38)) = rcx;
    esi = *((rbx + 2));
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    eax = rsi - 0x30;
    if (al <= 9) {
        goto label_48;
    }
label_22:
    rbx = *((r12 + 0x40));
    if (rbx == -1) {
        goto label_49;
    }
label_21:
    r8 = *((r15 + 8));
    if (r9 <= rbx) {
        goto label_50;
    }
label_19:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbx) {
        goto label_51;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= rbx);
    *(r15) = rdx;
    *(rsi) = 0;
label_51:
    rbx <<= 5;
    rax = r8 + rbx;
    edx = *(rax);
    if (edx != 0) {
        goto label_52;
    }
    *(rax) = 5;
    rbx = rcx;
    ebp = *(rcx);
    goto label_3;
    edx |= 4;
    goto label_2;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xf;
label_12:
    if (r13 == -1) {
        goto label_53;
    }
    *((r12 + 0x50)) = r13;
label_16:
    r8 = *((r15 + 8));
    if (r9 <= r13) {
        goto label_54;
    }
label_15:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > r13) {
        goto label_55;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= r13);
    *(r15) = rdx;
    *(rsi) = 0;
label_55:
    r13 <<= 5;
    r13 += r8;
    eax = *(r13);
    if (eax != 0) {
        goto label_56;
    }
    *(r13) = ecx;
label_13:
    *((r12 + 0x48)) = bpl;
    rax = *(r14);
    *((r12 + 8)) = rbx;
    r13 = rax + 1;
    *(r14) = r13;
    if (r11 > r13) {
        rcx = *((r14 + 8));
        goto label_4;
    }
    if (r11 < 0) {
        goto label_57;
    }
    rax = 0x2e8ba2e8ba2e8ba;
    r12 = r11 + r11;
    if (r12 > rax) {
        goto label_57;
    }
    rax = r11 * 5;
    rbp = *((r14 + 8));
    *((rsp + 0x30)) = r9;
    rsi = r11 + rax*2;
    *((rsp + 0x28)) = r10;
    rsi <<= 4;
    if (r10 == rbp) {
        goto label_58;
    }
    rax = realloc (rbp, rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_57;
    }
    rbp = *((r14 + 8));
    if (r10 == rbp) {
        goto label_59;
    }
label_8:
    *((r14 + 8)) = rcx;
    r13 = *(r14);
    r11 = r12;
    goto label_4;
label_35:
    rdx = rbx;
    do {
        ecx = *((rdx + 1));
        rdx++;
        esi = rcx - 0x30;
    } while (sil <= 9);
    r13 = 0xffffffffffffffff;
    if (cl != 0x24) {
        goto label_5;
    }
    rax += 2;
    edi = 0;
    while (rsi >= 0) {
        if (dl > 9) {
            goto label_60;
        }
        rax++;
        edx = rbp - 0x30;
        rcx = rax - 1;
        rsi = 0x1999999999999999;
        rdx = (int64_t) dl;
        if (rdi > rsi) {
            goto label_61;
        }
        rsi = rdi * 5;
        rsi += rsi;
label_6:
        ebp = *(rax);
        rsi += rdx;
        rdi = rsi;
        edx = rbp - 0x30;
    }
    if (dl <= 9) {
        rcx = rax;
        rdx = (int64_t) dl;
        rax++;
        rsi = 0xffffffffffffffff;
        goto label_6;
    }
label_10:
    r8 = *((r15 + 8));
label_14:
    if (*((rsp + 0x18)) != r8) {
        *((rsp + 8)) = r10;
        free (r8);
        r10 = *((rsp + 8));
    }
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
    errno_location ();
    *(rax) = 0x16;
    eax = 0xffffffff;
    goto label_7;
label_47:
    *((r12 + 0x30)) = rbx;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    eax -= 0x30;
    if (al > 9) {
        goto label_62;
    }
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
label_28:
    rdi = *((rsp + 0x10));
    *((r12 + 0x38)) = rdx;
    ebp = *(rdx);
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    goto label_3;
label_58:
    rax = malloc (rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_63;
    }
label_33:
    rax = r13 * 5;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r10;
    rdx <<= 3;
    rax = memcpy (rcx, rbp, r13 + rax*2);
    r9 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    rcx = rax;
    goto label_8;
label_42:
    rax = rdx;
    do {
        esi = *((rax + 1));
        rax++;
        edi = rsi - 0x30;
    } while (dil <= 9);
    if (sil != 0x24) {
        goto label_9;
    }
    rbx += 2;
    esi = 0;
    while (rcx >= 0) {
        if (al > 9) {
            goto label_64;
        }
        rbx++;
        eax = rcx - 0x30;
        rdx = rbx - 1;
        rdi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rsi > rdi) {
            goto label_65;
        }
        rcx = rsi * 5;
        rcx += rcx;
label_11:
        rcx += rax;
        rsi = rcx;
        ecx = *(rbx);
        eax = rcx - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rdx = rbx;
    rax = (int64_t) al;
    rbx++;
    rcx = 0xffffffffffffffff;
    goto label_11;
    ecx = 0xc;
    if (edx > 0xf) {
        goto label_12;
    }
    ecx = 0;
    edx &= 4;
    cl = (edx != 0) ? 1 : 0;
    ecx += 0xb;
    goto label_12;
    ecx = 0xa;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 8;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 2;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 4;
    goto label_12;
    ecx = 9;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 7;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 1;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 3;
    goto label_12;
    ecx = 0x16;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 0x15;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 0x12;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx = 0x14;
    ecx -= edx;
    goto label_12;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xd;
    goto label_12;
    ecx = 0xe;
    goto label_12;
label_56:
    if (eax == ecx) {
        goto label_13;
    }
    goto label_14;
label_54:
    r9 += r9;
    rax = r13 + 1;
    if (r9 <= r13) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_67;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = realloc (r8, rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_68;
    }
label_18:
    *((r15 + 8)) = r8;
    goto label_15;
label_53:
    rdi = *((rsp + 0x20));
    *((r12 + 0x50)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    r13 = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_16;
label_38:
    *((r12 + 0x18)) = rbx;
    eax = *(rbx);
    eax -= 0x30;
    if (al > 9) {
        goto label_69;
    }
    rdx = rbx;
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rdi = *((rsp + 8));
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
label_69:
    *((r12 + 0x20)) = rbx;
    do {
        ebp = *(rbx);
        goto label_17;
label_46:
    } while (eax == 5);
    goto label_14;
label_67:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = malloc (rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax != 0) {
label_31:
        *((rsp + 0x40)) = r9;
        *((rsp + 0x38)) = r11;
        rdx <<= 5;
        *((rsp + 0x30)) = r10;
        *((rsp + 0x28)) = ecx;
        rax = memcpy (rdi, r8, *(r15));
        r9 = *((rsp + 0x40));
        r11 = *((rsp + 0x38));
        r10 = *((rsp + 0x30));
        ecx = *((rsp + 0x28));
        r8 = rax;
        goto label_18;
label_63:
        rdx = *((r15 + 8));
        if (*((rsp + 0x18)) == rdx) {
            goto label_70;
        }
label_24:
        *((rsp + 8)) = r10;
        free (rdx);
        r10 = *((rsp + 8));
    }
label_25:
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
label_70:
    errno_location ();
    *(rax) = 0xc;
    eax = 0xffffffff;
    return rax;
label_50:
    r9 += r9;
    rax = rbx + 1;
    if (r9 <= rbx) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_71;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = realloc (r8, rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_72;
    }
label_27:
    *((r15 + 8)) = r8;
    goto label_19;
label_52:
    if (edx != 5) {
        goto label_14;
    }
    ebp = *(rcx);
    rbx = rcx;
    goto label_3;
label_43:
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x28)) = r10;
    rax = malloc (rsi);
    rdx = *((rsp + 0x40));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_20;
    ecx = 0x11;
    goto label_12;
    ecx = 0x10;
    goto label_12;
label_49:
    rdi = *((rsp + 0x20));
    *((r12 + 0x40)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbx = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_21;
label_48:
    rax = rcx;
    do {
        edx = *((rax + 1));
        rax++;
        edi = rdx - 0x30;
    } while (dil <= 9);
    if (dl != 0x24) {
        goto label_22;
    }
    rbx += 3;
    edi = 0;
    while (rdx >= 0) {
        if (al > 9) {
            goto label_73;
        }
        rbx++;
        eax = rsi - 0x30;
        rcx = rbx - 1;
        rsi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rdi > rsi) {
            goto label_74;
        }
        rdx = rdi * 5;
        rdx += rdx;
label_23:
        esi = *(rbx);
        rdx += rax;
        rdi = rdx;
        eax = rsi - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rcx = rbx;
    rax = (int64_t) al;
    rbx++;
    rdx = 0xffffffffffffffff;
    goto label_23;
label_61:
    rsi = 0xffffffffffffffff;
    goto label_6;
label_60:
    r13 = rsi;
    r13--;
    if (r13 > 0xfffffffffffffffd) {
        goto label_10;
    }
    ebp = *((rcx + 2));
    rbx = rcx + 2;
    goto label_5;
label_57:
    rdx = *((r15 + 8));
label_30:
    if (*((rsp + 0x18)) != rdx) {
        goto label_24;
    }
    goto label_25;
label_44:
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    rdx <<= 5;
    *((rsp + 0x28)) = r10;
    rax = memcpy (r8, *((rsp + 0x18)), *(r15));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_26;
label_71:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = malloc (rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax == 0) {
        goto label_25;
    }
label_32:
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    rdx <<= 5;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = memcpy (rdi, r8, *(r15));
    r9 = *((rsp + 0x40));
    r11 = *((rsp + 0x38));
    r10 = *((rsp + 0x30));
    rcx = *((rsp + 0x28));
    r8 = rax;
    goto label_27;
label_65:
    rcx = 0xffffffffffffffff;
    goto label_11;
label_62:
    rbx = rdx;
    eax = 1;
    goto label_28;
label_64:
    rbp--;
    if (rbp > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x28)) = rbp;
    rbx = rdx + 2;
    goto label_29;
label_74:
    rdx = 0xffffffffffffffff;
    goto label_23;
label_73:
    rbx = rdx - 1;
    if (rbx > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x40)) = rbx;
    rcx += 2;
    goto label_21;
label_66:
    rdx = r8;
    goto label_30;
label_68:
    rdi = r8;
    r8 = rax;
    goto label_31;
label_72:
    rdi = r8;
    r8 = rax;
    goto label_32;
label_59:
    r13 = *(r14);
    goto label_33;
}

/* /tmp/tmpuw2etcww @ 0x6ca0 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpuw2etcww @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpuw2etcww @ 0x7340 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x8af0 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpuw2etcww @ 0x7040 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x7400 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpuw2etcww @ 0x23c0 */
 
void snprintf_chk (void) {
    /* [15] -r-x section size 896 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpuw2etcww @ 0x23d0 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpuw2etcww @ 0x0 */
 
void libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) += cl;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += bh;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(0xfffffffffffffe43) += cl;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += cl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(0x0000005d) += cl;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    *(rax)++;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    *(rbx)++;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) &= dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) &= dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    cl += ah;
    if (cl != 0) {
    }
    *(rax) += al;
    *(rax) += al;
    cl += ah;
    if (cl != 0) {
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) -= al;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x28)) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmpuw2etcww @ 0x2410 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpuw2etcww @ 0x2420 */
 
void strcpy (void) {
    __asm ("bnd jmp qword [reloc.strcpy]");
}

/* /tmp/tmpuw2etcww @ 0x2450 */
 
void clearerr_unlocked (void) {
    __asm ("bnd jmp qword [reloc.clearerr_unlocked]");
}

/* /tmp/tmpuw2etcww @ 0x2460 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpuw2etcww @ 0x2470 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpuw2etcww @ 0x2480 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpuw2etcww @ 0x24a0 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpuw2etcww @ 0x24d0 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpuw2etcww @ 0x24e0 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpuw2etcww @ 0x24f0 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpuw2etcww @ 0x2500 */
 
void newlocale (void) {
    __asm ("bnd jmp qword [reloc.newlocale]");
}

/* /tmp/tmpuw2etcww @ 0x2520 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpuw2etcww @ 0x2530 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpuw2etcww @ 0x2550 */
 
void strcspn (void) {
    __asm ("bnd jmp qword [reloc.strcspn]");
}

/* /tmp/tmpuw2etcww @ 0x2560 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpuw2etcww @ 0x2570 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpuw2etcww @ 0x2590 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpuw2etcww @ 0x25b0 */
 
void strtol (void) {
    __asm ("bnd jmp qword [reloc.strtol]");
}

/* /tmp/tmpuw2etcww @ 0x25c0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpuw2etcww @ 0x25d0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpuw2etcww @ 0x25e0 */
 
void uselocale (void) {
    __asm ("bnd jmp qword [reloc.uselocale]");
}

/* /tmp/tmpuw2etcww @ 0x2600 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpuw2etcww @ 0x2620 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpuw2etcww @ 0x2630 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmpuw2etcww @ 0x2650 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpuw2etcww @ 0x2670 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmpuw2etcww @ 0x2680 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmpuw2etcww @ 0x2690 */
 
void strtold (void) {
    __asm ("bnd jmp qword [reloc.strtold]");
}

/* /tmp/tmpuw2etcww @ 0x26b0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpuw2etcww @ 0x26c0 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpuw2etcww @ 0x26d0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpuw2etcww @ 0x26f0 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpuw2etcww @ 0x2700 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpuw2etcww @ 0x2710 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpuw2etcww @ 0x2720 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpuw2etcww @ 0x2730 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmpuw2etcww @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 912 named .plt */
    __asm ("bnd jmp qword [0x0000ddf8]");
}

/* /tmp/tmpuw2etcww @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpuw2etcww @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}
