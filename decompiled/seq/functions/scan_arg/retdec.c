int64_t scan_arg(char * arg) {
    int64_t v1 = __readfsqword(40); // 0x3f90
    int64_t v2; // 0x3f70
    char * v3 = (char *)v2;
    float80_t v4; // bp-104, 0x3f70
    bool v5 = xstrtold(v3, NULL, (float128_t *)&v4, (float128_t (*)(char *, char **))0x4300); // 0x3fa5
    char * v6 = v3; // 0x3fac
    int64_t v7; // 0x3f70
    int64_t v8; // 0x3f70
    int64_t v9; // 0x3f70
    int64_t v10; // 0x3f70
    int128_t v11; // 0x3f70
    int64_t result; // 0x3f70
    char * v12; // 0x3f70
    int64_t v13; // 0x3ffd
    int64_t v14; // 0x4042
    int64_t v15; // 0x4063
    if (!v5) {
        goto lab_0x42c4;
    } else {
        // 0x3fb2
        int3_t v16; // 0x3f70
        int3_t v17 = v16 - 1; // 0x3fb2
        __frontend_reg_store_fpr(v17, v4);
        int3_t v18 = v16 - 2; // 0x3fb6
        __frontend_reg_store_fpr(v18, __frontend_reg_load_fpr(v17));
        float80_t v19 = __frontend_reg_load_fpr(v18); // 0x3fb8
        float80_t v20 = __frontend_reg_load_fpr(v17); // 0x3fbb
        float80_t v21 = __frontend_reg_load_fpr(v17); // 0x3fbb
        v6 = v3;
        if (v20 != v20 || v21 != v21) {
            goto lab_0x4272;
        } else {
            // 0x3fc3
            result = (int64_t)arg;
            int64_t v22 = *(int64_t *)function_2720(); // 0x3fc8
            int64_t v23; // 0x3f70
            v9 = v23;
            v12 = (char *)v9;
            unsigned char v24 = *v12; // 0x3fd4
            v23 = v9 + 1;
            while (v24 == 43 || (*(char *)(v22 + 1 + 2 * (int64_t)v24) & 32) != 0) {
                // 0x3fd4
                v9 = v23;
                v12 = (char *)v9;
                v24 = *v12;
                v23 = v9 + 1;
            }
            // 0x3fe4
            v13 = function_24f0();
            if (v13 == 0) {
                // 0x417d
                function_24f0();
            }
            char v25 = *(char *)(function_2550() + v9); // 0x401d
            v11 = 0;
            if (v25 != 0) {
                goto lab_0x4140;
            } else {
                // 0x4028
                __frontend_reg_store_fpr(v17, 0.0L);
                __frontend_reg_store_fpr(v18, v19);
                __frontend_reg_store_fpr(v18, __frontend_reg_load_fpr(v18) * __frontend_reg_load_fpr(v17));
                float80_t v26 = __frontend_reg_load_fpr(v18); // 0x402f
                float80_t v27 = __frontend_reg_load_fpr(v17); // 0x402f
                __frontend_reg_store_fpr(v17, __frontend_reg_load_fpr(v17));
                v11 = 0;
                if (v26 != v27) {
                    goto lab_0x4140;
                } else {
                    // 0x403f
                    v14 = function_24b0();
                    v10 = v14;
                    v7 = 0;
                    if (v13 == 0) {
                        goto lab_0x4098;
                    } else {
                        // 0x4058
                        v15 = function_2550();
                        if (v15 < 0x80000000) {
                            // 0x41c2
                            v8 = -1;
                            if (v15 != 0) {
                                goto lab_0x4077;
                            } else {
                                goto lab_0x4090;
                            }
                        } else {
                            goto lab_0x4077;
                        }
                    }
                }
            }
        }
    }
  lab_0x42c4:
    // 0x42c4
    quote(v6);
    function_2490();
    function_26a0();
    usage(1);
    return &g40;
  lab_0x4272:
    // 0x4272
    quote_n();
    quote_n();
    function_2490();
    function_26a0();
    usage(1);
    goto lab_0x42c4;
  lab_0x4140:;
    int128_t v28 = __asm_movdqa((int128_t)(int64_t)(float64_t)v4); // 0x4140
    int128_t v29 = __asm_movdqa(v11); // 0x4146
    *(int128_t *)arg = (int128_t)__asm_movaps(v28);
    *(int128_t *)(result + 16) = (int128_t)__asm_movaps(v29);
    if (v1 == __readfsqword(40)) {
        // 0x416b
        return result;
    }
    // 0x426d
    function_24c0();
    v6 = v12;
    goto lab_0x4272;
  lab_0x4098:;
    uint64_t v30 = v7;
    int64_t v31 = v10;
    int64_t v32 = function_24f0(); // 0x40a0
    int64_t v33 = v32; // 0x40ab
    if (v32 == 0) {
        int64_t v34 = function_24f0(); // 0x41b0
        v33 = v34;
        v11 = v31;
        if (v34 == 0) {
            goto lab_0x4140;
        } else {
            goto lab_0x40b1;
        }
    } else {
        goto lab_0x40b1;
    }
  lab_0x40b1:;
    int64_t v35 = v33;
    int64_t v36 = v35 - v9; // 0x40c3
    int64_t v37 = -0x7fffffffffffffff; // 0x40df
    int64_t v38; // 0x3f70
    int64_t v39; // 0x3f70
    if (function_25b0() > -0x7fffffffffffffff) {
        int64_t v40 = function_25b0(); // 0x41df
        v37 = v40;
        if (v40 < 0) {
            goto lab_0x40f3;
        } else {
            int64_t v41 = v36 + v31 - function_24b0(); // 0x421e
            if (v13 != 0 && v30 != 0) {
                // branch -> 0x423b
            }
            int64_t v42 = v30 < (int64_t)(float64_t)(float80_t)(int80_t)v40 ? v30 : (int64_t)(float64_t)(float80_t)(int80_t)v40; // 0x423e
            v38 = v41;
            v39 = (int64_t)(float64_t)(float80_t)(int80_t)v40 - v42;
            goto lab_0x4131;
        }
    } else {
        goto lab_0x40f3;
    }
  lab_0x4077:
    // 0x4077
    v8 = 1;
    if (v9 != v13) {
        // 0x4080
        v8 = (int32_t)*(char *)(v13 - 1) >= 58;
    }
    goto lab_0x4090;
  lab_0x40f3:;
    int64_t v43 = v36 + v31 - function_24b0(); // 0x410e
    int64_t v44; // 0x3f70
    if (v13 == 0) {
        // 0x4254
        v44 = v43 + 1;
    } else {
        // 0x411a
        v44 = v43 + (int64_t)(v35 == v13 + 1);
    }
    // 0x412e
    v38 = v44;
    v39 = -(int64_t)(float64_t)(float80_t)(int80_t)v37;
    goto lab_0x4131;
  lab_0x4090:
    // 0x4090
    v10 = v8 + v14;
    v7 = v15;
    goto lab_0x4098;
  lab_0x4131:
    // 0x4131
    v11 = v39 + v38;
    goto lab_0x4140;
}