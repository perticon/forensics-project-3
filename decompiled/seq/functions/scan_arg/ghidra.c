undefined8 * scan_arg(undefined8 *param_1,byte *param_2)

{
  float10 fVar1;
  char cVar2;
  int iVar3;
  ushort **ppuVar4;
  byte *pbVar5;
  size_t sVar6;
  byte *pbVar7;
  ulong uVar8;
  long lVar9;
  char *pcVar10;
  undefined8 uVar11;
  undefined8 uVar12;
  ulong uVar13;
  long in_FS_OFFSET;
  undefined8 local_68;
  undefined2 uStack96;
  undefined6 uStack94;
  byte *local_58;
  int iStack80;
  undefined4 uStack76;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  cVar2 = xstrtold(param_2,0,&local_68,cl_strtold);
  if (cVar2 == '\0') {
    uVar11 = quote(param_2);
    uVar12 = dcgettext(0,"invalid floating point argument: %s",5);
    error(0,0,uVar12,uVar11);
                    /* WARNING: Subroutine does not return */
    usage(1);
  }
  fVar1 = (float10)CONCAT28(uStack96,local_68);
  ppuVar4 = __ctype_b_loc();
  for (; ((*(byte *)((long)*ppuVar4 + (ulong)*param_2 * 2 + 1) & 0x20) != 0 || (*param_2 == 0x2b));
      param_2 = param_2 + 1) {
  }
  local_58 = (byte *)0x0;
  iStack80 = 0x7fffffff;
  pbVar5 = (byte *)strchr((char *)param_2,0x2e);
  if ((pbVar5 == (byte *)0x0) && (pcVar10 = strchr((char *)param_2,0x70), pcVar10 == (char *)0x0)) {
    iStack80 = 0;
  }
  sVar6 = strcspn((char *)param_2,"xX");
  if ((param_2[sVar6] != 0) || (fVar1 * (float10)0 != (float10)0)) goto LAB_00104140;
  pbVar7 = (byte *)strlen((char *)param_2);
  local_58 = pbVar7;
  if (pbVar5 == (byte *)0x0) {
    uVar8 = 0;
  }
  else {
    uVar8 = strcspn((char *)(pbVar5 + 1),"eE");
    if ((uVar8 < 0x80000000) && (iStack80 = (int)uVar8, uVar8 == 0)) {
      uVar13 = 0xffffffffffffffff;
    }
    else if (param_2 == pbVar5) {
      uVar13 = 1;
    }
    else {
      uVar13 = (ulong)(9 < (int)(char)pbVar5[-1] - 0x30U);
    }
    local_58 = pbVar7 + uVar13;
  }
  pbVar7 = (byte *)strchr((char *)param_2,0x65);
  if ((pbVar7 == (byte *)0x0) &&
     (pbVar7 = (byte *)strchr((char *)param_2,0x45), pbVar7 == (byte *)0x0)) goto LAB_00104140;
  lVar9 = strtol((char *)(pbVar7 + 1),(char **)0x0,10);
  if (lVar9 < -0x7ffffffffffffffe) {
    uVar13 = 0x8000000000000001;
LAB_001040f3:
    iStack80 = iStack80 - (int)uVar13;
    sVar6 = strlen((char *)param_2);
    local_58 = pbVar7 + (long)(local_58 + (-sVar6 - (long)param_2));
    if (pbVar5 == (byte *)0x0) {
      local_58 = local_58 + 1;
    }
    else if (pbVar7 == pbVar5 + 1) {
      local_58 = local_58 + 1;
    }
    lVar9 = -uVar13;
  }
  else {
    uVar13 = strtol((char *)(pbVar7 + 1),(char **)0x0,10);
    if ((long)uVar13 < 0) goto LAB_001040f3;
    iVar3 = iStack80;
    if ((long)uVar13 < (long)iStack80) {
      iVar3 = (int)uVar13;
    }
    iVar3 = iStack80 - iVar3;
    iStack80 = iVar3;
    sVar6 = strlen((char *)param_2);
    local_58 = pbVar7 + (long)(local_58 + (-sVar6 - (long)param_2));
    if ((uVar8 != 0 && pbVar5 != (byte *)0x0) && (iVar3 == 0)) {
      local_58 = local_58 + -1;
    }
    if (uVar13 <= uVar8) {
      uVar8 = uVar13;
    }
    lVar9 = uVar13 - uVar8;
  }
  local_58 = local_58 + lVar9;
LAB_00104140:
  *param_1 = local_68;
  param_1[1] = CONCAT62(uStack94,uStack96);
  param_1[2] = local_58;
  param_1[3] = CONCAT44(uStack76,iStack80);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return param_1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail(local_68,local_58);
}