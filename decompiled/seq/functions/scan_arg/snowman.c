void** scan_arg(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void** rax5;
    void** v6;
    void* eax7;
    void** rax8;
    void* rcx9;
    int64_t rdx10;
    int32_t v11;
    void** rax12;
    void** rax13;
    void** rax14;
    int1_t zf15;
    void** r14_16;
    void** rax17;
    void** rax18;
    void** r13_19;
    void** rax20;
    void* rax21;
    void** rdi22;
    void** rax23;
    void** rax24;
    void** rcx25;

    r12_3 = rdi;
    rbp4 = rsi;
    rax5 = g28;
    v6 = rax5;
    eax7 = xstrtold(rbp4);
    if (!*reinterpret_cast<signed char*>(&eax7)) {
        addr_42c4_2:
        quote(rbp4, rbp4);
        fun_2490();
        fun_26a0();
        usage();
    } else {
        __asm__("fld tword [rsp+0x10]");
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fucomip st0, st0");
        if (__intrinsic()) {
            addr_4272_4:
            quote_n();
            quote_n();
            fun_2490();
            fun_26a0();
            usage();
            goto addr_42c4_2;
        } else {
            rax8 = fun_2720(rbp4);
            rcx9 = *rax8;
            while ((*reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rcx9) + rdx10 * 2 + 1) & 32)) || *reinterpret_cast<signed char*>(&rdx10) == 43) {
                ++rbp4;
            }
            v11 = 0x7fffffff;
            rax12 = fun_24f0(rbp4, 46);
            if (rax12) 
                goto addr_400e_9;
            rax13 = fun_24f0(rbp4, 0x70);
            if (!rax13) 
                goto addr_4193_11;
        }
    }
    addr_400e_9:
    rax14 = fun_2550(rbp4, "xX");
    zf15 = *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp4) + reinterpret_cast<unsigned char>(rax14)) == 0;
    if (!zf15) 
        goto addr_4140_12;
    __asm__("fldz ");
    __asm__("fld tword [rsp]");
    __asm__("fmul st0, st1");
    __asm__("fucomip st0, st1");
    __asm__("fstp st0");
    if (__intrinsic()) 
        goto addr_4140_12;
    if (!zf15) 
        goto addr_4140_12;
    fun_24b0(rbp4, rbp4);
    if (!rax12) {
        *reinterpret_cast<int32_t*>(&r14_16) = 0;
        *reinterpret_cast<int32_t*>(&r14_16 + 4) = 0;
    } else {
        rax17 = fun_2550(rax12 + 1, "eE");
        r14_16 = rax17;
        if (reinterpret_cast<unsigned char>(rax17) > reinterpret_cast<unsigned char>(0x7fffffff) || (v11 = *reinterpret_cast<int32_t*>(&rax17), !!rax17)) {
            if (rbp4 == rax12) {
            }
        }
    }
    rax18 = fun_24f0(rbp4, 0x65);
    r13_19 = rax18;
    if (rax18) 
        goto addr_40b1_24;
    rax20 = fun_24f0(rbp4, 69);
    r13_19 = rax20;
    if (!rax20) {
        addr_4140_12:
        __asm__("movdqa xmm0, [rsp+0x10]");
        __asm__("movdqa xmm1, [rsp+0x20]");
        __asm__("movaps [r12], xmm0");
        __asm__("movaps [r12+0x10], xmm1");
        rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
        if (rax21) {
            fun_24c0();
            goto addr_4272_4;
        } else {
            return r12_3;
        }
    }
    addr_40b1_24:
    rdi22 = r13_19 + 1;
    rax23 = fun_25b0(rdi22);
    if (reinterpret_cast<signed char>(rax23) < reinterpret_cast<signed char>(0x8000000000000002)) {
        goto addr_40f3_30;
    }
    rax24 = fun_25b0(rdi22);
    if (reinterpret_cast<signed char>(rax24) < reinterpret_cast<signed char>(0)) {
        addr_40f3_30:
        fun_24b0(rbp4);
        if (rax12) {
            if (r13_19 == rax12 + 1) {
            }
        }
    } else {
        rcx25 = reinterpret_cast<void**>(static_cast<int64_t>(v11));
        if (reinterpret_cast<signed char>(rcx25) > reinterpret_cast<signed char>(rax24)) {
            rcx25 = rax24;
        }
        fun_24b0(rbp4);
        if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!r14_16)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax12)) && !(v11 - *reinterpret_cast<int32_t*>(&rcx25))) {
        }
        if (reinterpret_cast<unsigned char>(rax24) <= reinterpret_cast<unsigned char>(r14_16)) {
        }
        goto addr_4131_43;
    }
    addr_4131_43:
    goto addr_4140_12;
    addr_4193_11:
    v11 = 0;
    goto addr_400e_9;
}