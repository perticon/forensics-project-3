all_digits_p(char *param_1,undefined8 param_2,ulong param_3,undefined8 param_4,uint param_5)

{
  char cVar1;
  size_t sVar2;
  size_t sVar3;
  
  cVar1 = *param_1;
  if ((int)cVar1 - 0x30U < 10) {
    sVar2 = strlen(param_1);
    sVar3 = strspn(param_1,"0123456789");
    return ZEXT1216(CONCAT48((int)cVar1 - 0x30U,
                             (ulong)(param_5 & 0xffffff00 | (uint)(sVar3 == sVar2))));
  }
  return ZEXT816(param_3) << 0x40;
}