signed char all_digits_p(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    int32_t r8d5;
    int32_t eax6;

    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 48) > 9) {
        return 0;
    } else {
        rax3 = fun_24b0(rdi);
        rax4 = fun_2540();
        *reinterpret_cast<unsigned char*>(&r8d5) = reinterpret_cast<uint1_t>(rax4 == rax3);
        eax6 = r8d5;
        return *reinterpret_cast<signed char*>(&eax6);
    }
}