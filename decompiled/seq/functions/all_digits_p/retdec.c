bool all_digits_p(char * s) {
    if (0x1000000 * (int32_t)(int64_t)s >> 24 >= 58) {
        // 0x3776
        return false;
    }
    int64_t v1 = function_24b0(); // 0x3751
    return function_2540() == v1;
}