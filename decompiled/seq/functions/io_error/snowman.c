void io_error(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rdi7;
    void** rax8;
    int32_t* rax9;
    void** rdx10;
    void** rsi11;
    void** rbp12;
    void** rbx13;
    void** rsi14;
    void** v15;
    int32_t eax16;
    int1_t zf17;
    void** v18;
    void** rax19;
    void** r14_20;
    void** r10_21;
    void** rax22;
    void** rax23;
    void** r15_24;
    void** r12_25;
    void** rcx26;
    void** v27;
    void** rax28;
    void** rdx29;
    void** rsi30;
    void** v31;
    void** rax32;
    void** v33;
    void** r15_34;
    void** rax35;
    void** rcx36;
    void** rax37;
    void** rax38;
    void** rax39;
    void** rax40;
    void** rcx41;
    void** rcx42;
    void** rax43;
    void** v44;
    void** rax45;
    void** r14_46;
    void* v47;
    void** rax48;
    void** rcx49;
    unsigned char r13b50;
    void** rsi51;
    void** rax52;
    uint32_t eax53;
    void** rax54;
    void** r10_55;
    uint32_t eax56;
    void** rcx57;
    void** rax58;
    void** rdx59;
    void** rax60;
    void** rcx61;
    void** rax62;
    void** rax63;
    void** rdi64;
    void** rcx65;
    int64_t rax66;
    int64_t rax67;
    uint32_t eax68;

    rdi7 = stdout;
    fun_2450(rdi7);
    rax8 = fun_2490();
    rax9 = fun_23f0();
    rdx10 = rax8;
    *reinterpret_cast<int32_t*>(&rsi11) = *rax9;
    *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
    fun_26a0();
    rbp12 = reinterpret_cast<void**>(1);
    rbx13 = rsi11;
    rsi14 = reinterpret_cast<void**>("inf");
    v15 = rdx10;
    eax16 = fun_2590(rbx13, "inf", rdx10);
    zf17 = reinterpret_cast<int1_t>(g1 == 48);
    *reinterpret_cast<int32_t*>(&v18) = eax16;
    if (zf17) {
        rax19 = reinterpret_cast<void**>(1);
        do {
            r14_20 = rax19;
            *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax19 + 1));
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            ++rax19;
        } while (rdx10 == 48);
        if (1 == rax19) 
            goto addr_393c_6;
        if (!rdx10) 
            goto addr_3821_8;
        addr_393c_6:
        r14_20 = rax19;
        goto addr_3821_8;
    }
    while (1) {
        r14_20 = rbp12;
        addr_3821_8:
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx13) == 48)) {
            r10_21 = rbx13;
        } else {
            rax22 = rbx13;
            do {
                r10_21 = rax22;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ++rax22;
            } while (rdx10 == 48);
            if (rbx13 == rax22) 
                goto addr_3934_14;
            if (rdx10) 
                goto addr_3934_14;
        }
        addr_3851_16:
        *reinterpret_cast<int32_t*>(&rbx13) = 31;
        *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
        rax23 = fun_24b0(r14_20, r14_20);
        r15_24 = rax23 + 1;
        r12_25 = rax23;
        if (reinterpret_cast<unsigned char>(r15_24) >= reinterpret_cast<unsigned char>(31)) {
            rbx13 = r15_24;
        }
        if (!*reinterpret_cast<int32_t*>(&v18)) {
            rcx26 = rbx13 + 1;
            *reinterpret_cast<int32_t*>(&rbp12) = 0;
            *reinterpret_cast<int32_t*>(&rbp12 + 4) = 0;
            v27 = rcx26;
            rax28 = xmalloc(rcx26, rsi14, rdx10);
            rdx29 = r15_24;
            rsi30 = r14_20;
            v31 = rax28;
            rax32 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(rax23)) + reinterpret_cast<unsigned char>(rax28), rsi30, rdx29);
            v33 = reinterpret_cast<void**>(0);
            r15_34 = rax32;
        } else {
            rax35 = fun_24b0(r10_21, r10_21);
            rbp12 = rax35;
            if (reinterpret_cast<unsigned char>(rbx13) < reinterpret_cast<unsigned char>(rax35)) {
                rbx13 = rax35;
            }
            rcx36 = rbx13 + 1;
            v27 = rcx36;
            rax37 = xmalloc(rcx36, rsi14, rdx10);
            v31 = rax37;
            rax38 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(rax23)) + reinterpret_cast<unsigned char>(rax37), r14_20, r15_24);
            r15_34 = rax38;
            rax39 = xmalloc(v27, r14_20, r15_24);
            rsi30 = r10_21;
            rdx29 = rbp12 + 1;
            rax40 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(rbp12)) + reinterpret_cast<unsigned char>(rax39), rsi30, rdx29);
            rcx41 = v27;
            v33 = rax40;
            if (reinterpret_cast<unsigned char>(rax23) < reinterpret_cast<unsigned char>(rbp12)) 
                goto addr_3981_23; else 
                goto addr_3900_24;
        }
        addr_397c_25:
        rcx41 = v27;
        addr_3981_23:
        rcx42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx41) + reinterpret_cast<unsigned char>(rcx41));
        *reinterpret_cast<int32_t*>(&rax43) = 0x2000;
        *reinterpret_cast<int32_t*>(&rax43 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rcx42) >= reinterpret_cast<unsigned char>(0x2000)) {
            rax43 = rcx42;
        }
        v44 = rax43;
        rax45 = xmalloc(rax43, rsi30, rdx29);
        rdx10 = rax23;
        r14_46 = rax45;
        v47 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v44) + reinterpret_cast<unsigned char>(r14_46));
        rax48 = fun_2670(r14_46, r15_34, rdx10);
        rcx49 = rax48;
        r13b50 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&v18) == 0);
        while (1) {
            if (v15) {
                rsi51 = v15;
                do {
                    rax52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_34) + reinterpret_cast<unsigned char>(r12_25) + 0xffffffffffffffff);
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax52));
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        if (reinterpret_cast<signed char>(rdx10) <= reinterpret_cast<signed char>(56)) 
                            break;
                        --rax52;
                        *reinterpret_cast<void***>(rax52 + 1) = reinterpret_cast<void**>(48);
                    } while (reinterpret_cast<unsigned char>(rax52) >= reinterpret_cast<unsigned char>(r15_34));
                    goto addr_3ab0_33;
                    *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rdx10) + 1;
                    *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                    *reinterpret_cast<void***>(rax52) = rdx10;
                    continue;
                    addr_3ab0_33:
                    *reinterpret_cast<signed char*>(r15_34 + 0xffffffffffffffff) = 49;
                    ++r12_25;
                    --r15_34;
                    --rsi51;
                } while (rsi51);
            }
            if (reinterpret_cast<unsigned char>(r12_25) < reinterpret_cast<unsigned char>(rbp12)) 
                goto addr_3a49_37;
            if (r13b50) 
                goto addr_3a49_37;
            if (reinterpret_cast<unsigned char>(r12_25) > reinterpret_cast<unsigned char>(rbp12)) 
                break;
            rdx10 = r12_25;
            v18 = rcx49;
            eax53 = fun_2560(r15_34, v33, rdx10, rcx49);
            rcx49 = v18;
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax53) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax53 == 0))) 
                break;
            addr_3a49_37:
            rax54 = separator;
            r10_55 = rcx49 + 1;
            eax56 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax54));
            *reinterpret_cast<void***>(rcx49) = *reinterpret_cast<void***>(&eax56);
            if (rbx13 == r12_25 && (rbx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<unsigned char>(rbx13)), rcx57 = rbx13 + 1, v18 = rcx57, rax58 = xrealloc(v31, rcx57, rdx10), rdx59 = r12_25 + 1, v31 = rax58, rax60 = fun_2680(reinterpret_cast<unsigned char>(rax58) + reinterpret_cast<unsigned char>(r12_25), rax58, rdx59), r10_55 = r10_55, r15_34 = rax60, rcx61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(v18)), reinterpret_cast<unsigned char>(rcx61) > reinterpret_cast<unsigned char>(v44))) {
                v18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_55) - reinterpret_cast<unsigned char>(r14_46));
                rax62 = xrealloc(r14_46, rcx61, rdx59);
                r14_46 = rax62;
                v44 = rcx61;
                r10_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r14_46));
                v47 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax62) + reinterpret_cast<unsigned char>(rcx61));
            }
            rdx10 = r12_25;
            rax63 = fun_2670(r10_55, r15_34, rdx10);
            rcx49 = rax63;
            if (reinterpret_cast<unsigned char>(rcx49) <= reinterpret_cast<unsigned char>(~reinterpret_cast<unsigned char>(r12_25) + reinterpret_cast<uint64_t>(v47))) 
                continue;
            *reinterpret_cast<uint32_t*>(&rdx10) = 1;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rdi64 = r14_46;
            rsi14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx49) - reinterpret_cast<unsigned char>(r14_46));
            rcx65 = stdout;
            rax66 = fun_2630(rdi64, rsi14, 1, rcx65);
            if (rax66 != 1) 
                goto addr_3b8a_44;
            rcx49 = r14_46;
        }
        *reinterpret_cast<void***>(rcx49) = reinterpret_cast<void**>(10);
        *reinterpret_cast<uint32_t*>(&rdx10) = 1;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rdi64 = r14_46;
        rsi14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx49 + 1) - reinterpret_cast<unsigned char>(r14_46));
        rcx65 = stdout;
        rax67 = fun_2630(rdi64, rsi14, 1, rcx65);
        if (rax67 - 1) 
            break;
        fun_26d0();
        continue;
        addr_3900_24:
        if (reinterpret_cast<unsigned char>(rax23) > reinterpret_cast<unsigned char>(rbp12)) 
            goto addr_3914_48;
        rdx29 = rax23;
        rsi30 = rax40;
        eax68 = fun_2560(r15_34, rsi30, rdx29, rcx41);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax68) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax68 == 0)) 
            goto addr_397c_25; else 
            goto addr_3914_48;
        addr_3934_14:
        r10_21 = rax22;
        goto addr_3851_16;
    }
    addr_3b8a_44:
    io_error(rdi64, rsi14, 1, rcx65, r8, r9);
    addr_3914_48:
    fun_23d0(v31, v31);
}