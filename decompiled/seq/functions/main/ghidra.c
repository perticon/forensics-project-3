void main(int param_1,undefined8 *param_2)

{
  ulong uVar1;
  char cVar2;
  int iVar3;
  char *pcVar4;
  long lVar5;
  size_t sVar6;
  size_t sVar7;
  void *pvVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  long lVar11;
  uint uVar12;
  ulong uVar13;
  long lVar14;
  undefined1 *__src;
  undefined1 *puVar15;
  long in_R10;
  char *__locale;
  undefined1 *unaff_R14;
  long in_FS_OFFSET;
  bool bVar16;
  ushort in_FPUControlWord;
  float10 fVar17;
  undefined auVar18 [16];
  uint local_128;
  int iStack292;
  undefined2 auStack288 [4];
  undefined local_118 [8];
  undefined2 uStack272;
  uint local_108;
  int iStack260;
  undefined2 uStack256;
  uint local_f8;
  undefined4 uStack244;
  undefined2 uStack240;
  char local_e8;
  undefined7 uStack231;
  undefined2 uStack224;
  float10 local_d8;
  float local_c0;
  ushort local_bc;
  char *local_b8;
  char *local_b0;
  char *local_a8;
  char *local_a0;
  float10 local_98;
  undefined8 local_88;
  undefined2 uStack128;
  long local_78;
  ulong uStack112;
  undefined8 local_68;
  undefined2 uStack96;
  long local_58;
  uint uStack80;
  undefined4 uStack76;
  undefined8 local_40;
  
  __locale = "";
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_88 = SUB108((float10)1,0);
  uStack128 = (undefined2)((unkuint10)(float10)1 >> 0x40);
  local_78 = 1;
  uStack112 = uStack112 & 0xffffffff00000000;
  set_program_name(*param_2);
  pcVar4 = setlocale(6,"");
  locale_ok = pcVar4 != (char *)0x0;
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  uVar13 = (ulong)(int)optind;
  equal_width = '\0';
  separator = "\n";
  __src = (char *)0x0;
  local_118 = (char *)in_R10;
  if ((int)optind < param_1) {
    unaff_R14 = long_options;
    do {
      if ((*(char *)param_2[(int)uVar13] == '-') &&
         ((cVar2 = ((char *)param_2[(int)uVar13])[1], cVar2 == '.' || ((int)cVar2 - 0x30U < 10))))
      break;
      iVar3 = getopt_long(param_1,param_2,"+f:s:w");
      if (iVar3 == -1) {
        uVar13 = (ulong)(int)optind;
        break;
      }
      pcVar4 = optarg;
      if (iVar3 != 0x66) {
        if (iVar3 < 0x67) {
          if (iVar3 == -0x83) {
            version_etc(stdout,&DAT_0010a01f,"GNU coreutils",Version,"Ulrich Drepper",0);
                    /* WARNING: Subroutine does not return */
            exit(0);
          }
          if (iVar3 == -0x82) {
                    /* WARNING: Subroutine does not return */
            usage(0);
          }
          goto LAB_00102974;
        }
        pcVar4 = __src;
        if (iVar3 == 0x73) {
          separator = optarg;
        }
        else {
          if (iVar3 != 0x77) goto LAB_00102974;
          equal_width = '\x01';
        }
      }
      uVar13 = (ulong)(int)optind;
      __src = pcVar4;
    } while ((int)optind < param_1);
  }
  local_128 = param_1 - (int)uVar13;
  if (local_128 == 0) {
    uVar9 = dcgettext(0,"missing operand",5);
    error(0,0,uVar9);
    goto LAB_00102974;
  }
  if (3 < local_128) {
    uVar9 = quote(param_2[uVar13 + 3]);
    pcVar4 = "extra operand %s";
LAB_00102e1b:
    uVar10 = dcgettext(0,pcVar4,5);
    error(0,0,uVar10,uVar9);
LAB_00102974:
                    /* WARNING: Subroutine does not return */
    usage(1);
  }
  if (__src != (char *)0x0) {
    lVar14 = 1;
    lVar5 = 0;
    do {
      lVar11 = lVar14 + -1;
      if (__src[lVar5] == '%') {
        unaff_R14 = (undefined1 *)(lVar5 + 1);
        if (__src[(long)unaff_R14] != '%') goto LAB_00102cd7;
        lVar11 = 2;
      }
      else {
        if (__src[lVar5] == '\0') {
          __locale = (char *)quote(__src);
          uVar9 = dcgettext(0,"format %s has no %% directive",5);
          pcVar4 = __locale;
          error(1,0,uVar9);
          goto LAB_001034fb;
        }
        lVar11 = 1;
      }
      lVar5 = lVar5 + lVar11;
      lVar14 = lVar14 + 1;
    } while( true );
  }
  unaff_R14 = (undefined1 *)0x0;
  lVar11 = 0;
LAB_001029d7:
  local_108 = (uint)uVar13;
  if (local_128 == 3) {
    _local_118 = (float10)CONCAT28(uStack272,param_2[(long)(int)local_108 + 1]);
    cVar2 = all_digits_p();
    local_c0 = 0.0;
    if (((cVar2 == '\0') ||
        (cVar2 = xstrtold(0,local_118,0,&local_88,cl_strtold), local_108 = optind, cVar2 == '\0'))
       || ((float10)CONCAT28(uStack128,local_88) <= (float10)0)) {
      local_e8 = false;
    }
    else {
      local_e8 = (float10)CONCAT28(uStack128,local_88) <= (float10)_DAT_0010a89c;
    }
    iStack260 = (int)local_108 >> 0x1f;
    _local_118 = (float10)CONCAT28(uStack272,param_2[(int)local_108]);
    local_f8 = local_108;
    cVar2 = all_digits_p(param_2[(int)local_108]);
    uVar12 = local_f8;
    if (cVar2 != '\0') {
      lVar14 = CONCAT44(iStack260,local_108) + 1;
      local_108 = local_f8;
      lVar5 = lVar14 * 8;
      local_f8 = (uint)lVar5;
      uStack244 = (undefined4)((ulong)lVar5 >> 0x20);
      cVar2 = all_digits_p(param_2[lVar14]);
      uVar12 = local_108;
      if ((cVar2 != '\0') && (local_e8 != '\0')) {
        cVar2 = all_digits_p(*(undefined8 *)((long)param_2 + CONCAT44(uStack244,local_f8) + 8));
        uVar12 = local_108;
        if (cVar2 != '\0') goto LAB_00102f4b;
      }
    }
  }
  else {
    iStack260 = (int)local_108 >> 0x1f;
    _local_118 = (float10)CONCAT28(uStack272,param_2[(int)local_108]);
    local_f8 = local_108;
    cVar2 = all_digits_p(param_2[(int)local_108]);
    lVar5 = CONCAT44(iStack260,local_108);
    uVar12 = local_f8;
    if (cVar2 == '\0') {
      local_c0 = 0.0;
    }
    else {
      local_c0 = 0.0;
      if (local_128 != 1) {
        local_108 = local_f8;
        cVar2 = all_digits_p(param_2[lVar5 + 1]);
        uVar12 = local_108;
        if (cVar2 == '\0') goto LAB_00102a1f;
      }
LAB_00102f4b:
      if ((equal_width != '\x01') && (__src == (char *)0x0)) {
        local_108 = uVar12;
        sVar6 = strlen(separator);
        uVar12 = local_108;
        if (sVar6 == 1) {
          fVar17 = (float10)CONCAT28(uStack128,local_88);
          if (local_128 == 1) {
            local_118 = &DAT_0010a0f1;
          }
          if ((float10)_DAT_0010a8a0 <= fVar17) {
            local_bc = in_FPUControlWord & 0xff | (ushort)(byte)((ulong)in_FPUControlWord >> 8) << 8
            ;
            uVar1 = (ulong)ROUND(fVar17 - (float10)_DAT_0010a8a0);
            uVar13 = uVar1 ^ 0x8000000000000000;
          }
          else {
            local_bc = in_FPUControlWord & 0xff | (ushort)(byte)((ulong)in_FPUControlWord >> 8) << 8
            ;
            uVar13 = (ulong)ROUND(fVar17);
            uVar1 = uVar13;
          }
          local_bc = local_bc | 0xc00;
          _local_118 = (float10)CONCAT28(uStack272,uVar1);
          seq_fast(local_118,param_2[(local_128 - 1) + local_108],uVar13);
          uVar12 = optind;
        }
      }
    }
  }
LAB_00102a1f:
  optind = uVar12 + 1;
  local_128 = (uint)&local_68;
  iStack292 = (int)((ulong)&local_68 >> 0x20);
  scan_arg();
  iVar3 = iStack292;
  uVar12 = local_128;
  local_e8 = (char)local_68;
  uStack231 = (undefined7)((ulong)local_68 >> 8);
  uStack224 = uStack96;
  if ((int)optind < param_1) {
    optind = optind + 1;
    _local_118 = (float10)CONCAT64(stack0xfffffffffffffeec,uStack80);
    local_128 = (uint)local_58;
    iStack292 = (int)((ulong)local_58 >> 0x20);
    local_108 = uVar12;
    iStack260 = iVar3;
    scan_arg();
    pcVar4 = (char *)CONCAT44(iStack292,local_128);
    local_f8 = (uint)local_68;
    uStack244 = (undefined4)((ulong)local_68 >> 0x20);
    uStack240 = uStack96;
    if ((int)optind < param_1) {
      uStack112 = CONCAT44(uStack76,uStack80);
      local_88 = local_68;
      uStack128 = uStack96;
      local_78 = local_58;
      if ((float10)CONCAT28(uStack96,local_68) == (float10)0) {
        uVar9 = quote(param_2[(long)(int)optind + -1]);
        pcVar4 = "invalid Zero increment value: %s";
        goto LAB_00102e1b;
      }
      optind = optind + 1;
      scan_arg();
      pcVar4 = (char *)CONCAT44(iStack292,local_128);
      local_f8 = (uint)local_68;
      uStack244 = (undefined4)((ulong)local_68 >> 0x20);
      uStack240 = uStack96;
    }
    local_118 = (char *)local_58;
    if (((uint)uStack112 | local_118._0_4_ | uStack80) != 0) goto LAB_00102a97;
    fVar17 = (float10)local_c0;
    if (((float10)CONCAT28(uStack224,CONCAT71(uStack231,local_e8)) * fVar17 == fVar17) &&
       (fVar17 <= (float10)CONCAT28(uStack224,CONCAT71(uStack231,local_e8)))) goto LAB_001033cf;
    if (__src != (char *)0x0) {
      local_d8 = (float10)CONCAT28(uStack128,local_88);
      goto LAB_00102aab;
    }
LAB_001034fb:
    uStack80 = 0;
    uVar12 = (uint)uStack112;
    if ((int)(uint)uStack112 < 0) {
      uVar12 = 0;
    }
    local_118._0_4_ = 0;
LAB_001032b4:
    if (equal_width == '\0') {
LAB_0010345b:
      __src = format_buf_0;
      __sprintf_chk(format_buf_0,1,0x1c,"%%.%dLf",uVar12);
      local_d8 = (float10)CONCAT28(uStack128,local_88);
      goto LAB_00102aab;
    }
    pcVar4 = pcVar4 + (int)(uVar12 - local_118._0_4_);
    local_118 = (char *)((long)(int)(uVar12 - uStack80) + (long)local_118);
    bVar16 = uVar12 != 0;
    if (bVar16) {
      if (uStack80 == 0) goto LAB_001032ed;
LAB_001032f5:
      if (local_118._0_4_ == 0) {
        pcVar4 = pcVar4 + bVar16;
      }
    }
    else {
      if (uStack80 == 0) {
LAB_001032ed:
        local_118 = local_118 + bVar16;
        goto LAB_001032f5;
      }
      local_118 = local_118 + -1;
    }
    __src = "%Lg";
    if (pcVar4 < local_118) {
      pcVar4 = local_118;
    }
    if (pcVar4 < (char *)0x80000000) {
      __src = format_buf_0;
      __sprintf_chk(format_buf_0,1,0x1c,"%%0%d.%dLf",(ulong)pcVar4 & 0xffffffff);
      local_d8 = (float10)CONCAT28(uStack128,local_88);
      goto LAB_00102aab;
    }
  }
  else {
    local_f8 = (uint)local_68;
    uStack244 = (undefined4)((ulong)local_68 >> 0x20);
    local_118 = (char *)local_58;
    if ((uStack80 | (uint)uStack112) == 0) {
      pcVar4 = (char *)0x1;
      uStack240 = uStack96;
      fVar17 = (float10)1;
      local_e8 = SUB101(fVar17,0);
      uStack231 = (undefined7)((unkuint10)fVar17 >> 8);
      uStack224 = (undefined2)((unkuint10)fVar17 >> 0x40);
LAB_001033cf:
      if (((((float10)CONCAT28(uStack240,CONCAT44(uStack244,local_f8)) < (float10)0) ||
           (local_d8 = (float10)CONCAT28(uStack128,local_88), local_d8 <= (float10)0)) ||
          ((float10)_DAT_0010a89c < local_d8)) || (equal_width != '\0')) {
        uStack80 = 0;
        local_118._0_4_ = 0;
        goto LAB_00102a97;
      }
      if (__src != (char *)0x0) goto LAB_00102aab;
      local_128 = (uint)pcVar4;
      iStack292 = (int)((ulong)pcVar4 >> 0x20);
      sVar6 = strlen(separator);
      if (sVar6 == 1) {
        iVar3 = rpl_asprintf(&local_b8);
        if (iVar3 < 0) {
LAB_00103572:
                    /* WARNING: Subroutine does not return */
          xalloc_die();
        }
        if ((float10)CONCAT28(uStack240,CONCAT44(uStack244,local_f8)) * (float10)0 == (float10)0) {
          iVar3 = rpl_asprintf(&local_b0);
          if (iVar3 < 0) goto LAB_00103572;
        }
        else {
          local_b0 = (char *)xstrdup(&DAT_0010a01b);
        }
        if ((*local_b8 != '-') && (*local_b0 != '-')) {
          fVar17 = (float10)CONCAT28(uStack128,local_88);
          if ((float10)_DAT_0010a8a0 <= fVar17) {
            local_bc = in_FPUControlWord & 0xff | (ushort)(byte)((ulong)in_FPUControlWord >> 8) << 8
            ;
            uVar13 = (long)ROUND(fVar17 - (float10)_DAT_0010a8a0) ^ 0x8000000000000000;
          }
          else {
            local_bc = in_FPUControlWord & 0xff | (ushort)(byte)((ulong)in_FPUControlWord >> 8) << 8
            ;
            uVar13 = (ulong)ROUND(fVar17);
          }
          local_bc = local_bc | 0xc00;
          seq_fast(local_b8,local_b0,uVar13);
        }
        free(local_b8);
        free(local_b0);
        pcVar4 = (char *)CONCAT44(iStack292,local_128);
        uStack80 = 0;
        local_118._0_4_ = 0;
        goto LAB_0010328d;
      }
      __src = "%Lg";
      uVar12 = 0;
      if (-1 < (int)(uint)uStack112) {
        uVar12 = (uint)uStack112;
      }
      if ((uint)uStack112 == 0x7fffffff) goto LAB_00102aab;
      goto LAB_0010345b;
    }
    pcVar4 = (char *)0x1;
    local_118._0_4_ = 0;
    uStack240 = uStack96;
    fVar17 = (float10)1;
    local_e8 = SUB101(fVar17,0);
    uStack231 = (undefined7)((unkuint10)fVar17 >> 8);
    uStack224 = (undefined2)((unkuint10)fVar17 >> 0x40);
LAB_00102a97:
    if (__src == (char *)0x0) {
LAB_0010328d:
      uVar12 = (uint)uStack112;
      if ((int)(uint)uStack112 <= (int)local_118._0_4_) {
        uVar12 = local_118._0_4_;
      }
      if ((uVar12 == 0x7fffffff) || (uStack80 == 0x7fffffff)) {
        local_d8 = (float10)CONCAT28(uStack128,local_88);
        __src = "%Lg";
        goto LAB_00102aab;
      }
      goto LAB_001032b4;
    }
  }
  local_d8 = (float10)CONCAT28(uStack128,local_88);
LAB_00102aab:
  if (local_d8 < (float10)0) goto LAB_00102e66;
  cVar2 = (float10)CONCAT28(uStack240,CONCAT44(uStack244,local_f8)) <
          (float10)CONCAT28(uStack224,CONCAT71(uStack231,local_e8));
  do {
    if ((bool)cVar2 != false) {
LAB_00102cd0:
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    local_108 = (uint)CONCAT71(uStack231,local_e8);
    iStack260 = (int)((uint7)uStack231 >> 0x18);
    uStack256 = uStack224;
    _local_118 = (float10)1;
    while (iVar3 = __printf_chk(1,__src), -1 < iVar3) {
      if (cVar2 != '\0') goto LAB_00102e45;
      fVar17 = (float10)CONCAT28(uStack224,CONCAT71(uStack231,local_e8)) + _local_118 * local_d8;
      local_128 = SUB104(fVar17,0);
      iStack292 = (int)((unkuint10)fVar17 >> 0x20);
      auStack288[0] = (undefined2)((unkuint10)fVar17 >> 0x40);
      if ((float10)0 <= local_d8) {
        if ((float10)CONCAT28(uStack240,CONCAT44(uStack244,local_f8)) < fVar17) goto LAB_00102af8;
      }
      else if (fVar17 < (float10)CONCAT28(uStack240,CONCAT44(uStack244,local_f8))) {
LAB_00102af8:
        if (locale_ok != '\0') {
          setlocale(1,"C");
        }
        iVar3 = rpl_asprintf(&local_a8,__src);
        if (locale_ok != '\0') {
          setlocale(1,__locale);
        }
        if (iVar3 < 0) goto LAB_00103572;
        local_a8[(long)iVar3 - (long)unaff_R14] = '\0';
        cVar2 = xstrtold(local_a8 + lVar11,0,&local_98,cl_strtold);
        if ((cVar2 == '\0') ||
           (local_98 != (float10)CONCAT28(uStack240,CONCAT44(uStack244,local_f8)))) {
          free(local_a8);
        }
        else {
          local_a0 = (char *)0x0;
          iVar3 = rpl_asprintf(&local_a0,__src);
          if (iVar3 < 0) goto LAB_00103572;
          local_a0[(long)iVar3 - (long)unaff_R14] = '\0';
          local_108 = (uint)local_a0;
          iStack260 = (int)((ulong)local_a0 >> 0x20);
          iVar3 = strcmp(local_a0,local_a8);
          free((void *)CONCAT44(iStack260,local_108));
          free(local_a8);
          if (iVar3 != 0) goto LAB_00102c0c;
        }
LAB_00102e45:
        iVar3 = fputs_unlocked((char *)&terminator,stdout);
        if (iVar3 != -1) goto LAB_00102cd0;
        break;
      }
LAB_00102c0c:
      iVar3 = fputs_unlocked(separator,stdout);
      if (iVar3 == -1) break;
      _local_118 = (float10)1 + _local_118;
      local_108 = local_128;
      iStack260 = iStack292;
      uStack256 = auStack288[0];
    }
    io_error();
LAB_00102e66:
    cVar2 = (float10)CONCAT28(uStack224,CONCAT71(uStack231,local_e8)) <
            (float10)CONCAT28(uStack240,CONCAT44(uStack244,local_f8));
  } while( true );
LAB_00102cd7:
  sVar6 = strspn(__src + (long)unaff_R14,"-+#0 \'");
  sVar7 = strspn(__src + (long)(unaff_R14 + sVar6),"0123456789");
  puVar15 = unaff_R14 + sVar6 + sVar7;
  if (__src[(long)puVar15] == '.') {
    sVar6 = strspn(__src + (long)(puVar15 + 1),"0123456789");
    puVar15 = puVar15 + 1 + sVar6;
  }
  cVar2 = __src[(long)puVar15];
  pcVar4 = __src + (long)(puVar15 + (cVar2 == 'L'));
  local_118._0_4_ = (uint)*pcVar4;
  local_108 = (uint)pcVar4;
  iStack260 = (int)((ulong)pcVar4 >> 0x20);
  if (*pcVar4 == '\0') {
    uVar9 = quote(__src);
    uVar10 = dcgettext(0,"format %s ends in %%",5);
    error(1,0,uVar10,uVar9);
LAB_001035d5:
    uVar9 = quote(__src);
    uVar10 = dcgettext(0,"format %s has too many %% directives",5);
    error(1,0,uVar10,uVar9);
  }
  else {
    local_f8 = (uint)puVar15;
    uStack244 = (undefined4)((ulong)puVar15 >> 0x20);
    pcVar4 = strchr("efgaEFGA",local_118._0_4_);
    local_118 = (char *)CONCAT44(uStack244,local_f8);
    puVar15 = puVar15 + (cVar2 == 'L') + 1;
    lVar5 = 1;
    if (pcVar4 != (char *)0x0) {
      do {
        unaff_R14 = (undefined1 *)(lVar5 + -1);
        if (__src[(long)puVar15] == '%') {
          if (__src[(long)(puVar15 + 1)] != '%') goto LAB_001035d5;
          lVar14 = 2;
        }
        else {
          if (__src[(long)puVar15] == '\0') {
            pvVar8 = (void *)xmalloc(puVar15 + 2);
            pvVar8 = memcpy(pvVar8,__src,(size_t)local_118);
            _local_118 = (float10)CONCAT28(uStack272,pvVar8);
            *(undefined *)((long)pvVar8 + (long)local_118) = 0x4c;
            strcpy((char *)((long)pvVar8 + (long)local_118 + 1U),
                   (char *)CONCAT44(iStack260,local_108));
            if (equal_width != '\0') {
              uVar9 = dcgettext(0,
                                "format string may not be specified when printing equal width strings"
                                ,5);
              error(0,0,uVar9);
                    /* WARNING: Subroutine does not return */
              usage(1);
            }
            uVar13 = (ulong)optind;
            __src = local_118;
            goto LAB_001029d7;
          }
          lVar14 = 1;
        }
        puVar15 = puVar15 + lVar14;
        lVar5 = lVar5 + 1;
      } while( true );
    }
  }
  local_128 = local_118._0_4_;
  uVar9 = quote(__src);
  uVar10 = dcgettext(0,"format %s has unknown %%%c directive",5);
  auVar18 = error(1,0,uVar10,uVar9,local_128);
  uVar9 = CONCAT44(iStack292,local_128);
  local_128 = SUB164(auVar18,0);
  iStack292 = SUB164(auVar18 >> 0x20,0);
  (*(code *)PTR___libc_start_main_0010dfc0)
            (main,uVar9,auStack288,0,0,SUB168(auVar18 >> 0x40,0),&local_128);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}