void seq_fast(void** rdi, void** rsi, void** rdx) {
    void** rbp4;
    void** rbx5;
    void** rsi6;
    void** v7;
    int32_t eax8;
    int1_t zf9;
    void** v10;
    void** rax11;
    void** r14_12;
    void** r10_13;
    void** rax14;
    void** rax15;
    void** r15_16;
    void** r12_17;
    void** rcx18;
    void** v19;
    void** rax20;
    void** rdx21;
    void** rsi22;
    void** v23;
    void** rax24;
    void** v25;
    void** r15_26;
    void** rax27;
    void** rcx28;
    void** rax29;
    void** rax30;
    void** rax31;
    void** rax32;
    void** rcx33;
    void** rcx34;
    void** rax35;
    void** v36;
    void** rax37;
    void** r14_38;
    void* v39;
    void** rax40;
    void** rcx41;
    unsigned char r13b42;
    void** rsi43;
    void** rax44;
    uint32_t eax45;
    void** rax46;
    void** r10_47;
    uint32_t eax48;
    void** rcx49;
    void** rax50;
    void** rdx51;
    void** rax52;
    void** rcx53;
    void** rax54;
    void** rax55;
    void** rdi56;
    void** rcx57;
    int64_t rax58;
    int64_t rax59;
    uint32_t eax60;
    void** r8_61;
    void** r9_62;

    rbp4 = rdi;
    rbx5 = rsi;
    rsi6 = reinterpret_cast<void**>("inf");
    v7 = rdx;
    eax8 = fun_2590(rbx5, "inf", rdx);
    zf9 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp4) == 48);
    *reinterpret_cast<int32_t*>(&v10) = eax8;
    if (zf9) {
        rax11 = rbp4;
        do {
            r14_12 = rax11;
            *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax11 + 1));
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            ++rax11;
        } while (rdx == 48);
        if (rbp4 == rax11) 
            goto addr_393c_5;
        if (!rdx) 
            goto addr_3821_7;
        addr_393c_5:
        r14_12 = rax11;
        goto addr_3821_7;
    }
    while (1) {
        r14_12 = rbp4;
        addr_3821_7:
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx5) == 48)) {
            r10_13 = rbx5;
        } else {
            rax14 = rbx5;
            do {
                r10_13 = rax14;
                *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax14 + 1));
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                ++rax14;
            } while (rdx == 48);
            if (rbx5 == rax14) 
                goto addr_3934_13;
            if (rdx) 
                goto addr_3934_13;
        }
        addr_3851_15:
        *reinterpret_cast<int32_t*>(&rbx5) = 31;
        *reinterpret_cast<int32_t*>(&rbx5 + 4) = 0;
        rax15 = fun_24b0(r14_12, r14_12);
        r15_16 = rax15 + 1;
        r12_17 = rax15;
        if (reinterpret_cast<unsigned char>(r15_16) >= reinterpret_cast<unsigned char>(31)) {
            rbx5 = r15_16;
        }
        if (!*reinterpret_cast<int32_t*>(&v10)) {
            rcx18 = rbx5 + 1;
            *reinterpret_cast<int32_t*>(&rbp4) = 0;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            v19 = rcx18;
            rax20 = xmalloc(rcx18, rsi6, rdx);
            rdx21 = r15_16;
            rsi22 = r14_12;
            v23 = rax20;
            rax24 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx5) - reinterpret_cast<unsigned char>(rax15)) + reinterpret_cast<unsigned char>(rax20), rsi22, rdx21);
            v25 = reinterpret_cast<void**>(0);
            r15_26 = rax24;
        } else {
            rax27 = fun_24b0(r10_13, r10_13);
            rbp4 = rax27;
            if (reinterpret_cast<unsigned char>(rbx5) < reinterpret_cast<unsigned char>(rax27)) {
                rbx5 = rax27;
            }
            rcx28 = rbx5 + 1;
            v19 = rcx28;
            rax29 = xmalloc(rcx28, rsi6, rdx);
            v23 = rax29;
            rax30 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx5) - reinterpret_cast<unsigned char>(rax15)) + reinterpret_cast<unsigned char>(rax29), r14_12, r15_16);
            r15_26 = rax30;
            rax31 = xmalloc(v19, r14_12, r15_16);
            rsi22 = r10_13;
            rdx21 = rbp4 + 1;
            rax32 = fun_25c0(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx5) - reinterpret_cast<unsigned char>(rbp4)) + reinterpret_cast<unsigned char>(rax31), rsi22, rdx21);
            rcx33 = v19;
            v25 = rax32;
            if (reinterpret_cast<unsigned char>(rax15) < reinterpret_cast<unsigned char>(rbp4)) 
                goto addr_3981_22; else 
                goto addr_3900_23;
        }
        addr_397c_24:
        rcx33 = v19;
        addr_3981_22:
        rcx34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx33) + reinterpret_cast<unsigned char>(rcx33));
        *reinterpret_cast<int32_t*>(&rax35) = 0x2000;
        *reinterpret_cast<int32_t*>(&rax35 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rcx34) >= reinterpret_cast<unsigned char>(0x2000)) {
            rax35 = rcx34;
        }
        v36 = rax35;
        rax37 = xmalloc(rax35, rsi22, rdx21);
        rdx = rax15;
        r14_38 = rax37;
        v39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v36) + reinterpret_cast<unsigned char>(r14_38));
        rax40 = fun_2670(r14_38, r15_26, rdx);
        rcx41 = rax40;
        r13b42 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&v10) == 0);
        while (1) {
            if (v7) {
                rsi43 = v7;
                do {
                    rax44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_26) + reinterpret_cast<unsigned char>(r12_17) + 0xffffffffffffffff);
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax44));
                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                        if (reinterpret_cast<signed char>(rdx) <= reinterpret_cast<signed char>(56)) 
                            break;
                        --rax44;
                        *reinterpret_cast<void***>(rax44 + 1) = reinterpret_cast<void**>(48);
                    } while (reinterpret_cast<unsigned char>(rax44) >= reinterpret_cast<unsigned char>(r15_26));
                    goto addr_3ab0_32;
                    *reinterpret_cast<uint32_t*>(&rdx) = *reinterpret_cast<uint32_t*>(&rdx) + 1;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    *reinterpret_cast<void***>(rax44) = rdx;
                    continue;
                    addr_3ab0_32:
                    *reinterpret_cast<signed char*>(r15_26 + 0xffffffffffffffff) = 49;
                    ++r12_17;
                    --r15_26;
                    --rsi43;
                } while (rsi43);
            }
            if (reinterpret_cast<unsigned char>(r12_17) < reinterpret_cast<unsigned char>(rbp4)) 
                goto addr_3a49_36;
            if (r13b42) 
                goto addr_3a49_36;
            if (reinterpret_cast<unsigned char>(r12_17) > reinterpret_cast<unsigned char>(rbp4)) 
                break;
            rdx = r12_17;
            v10 = rcx41;
            eax45 = fun_2560(r15_26, v25, rdx, rcx41);
            rcx41 = v10;
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax45) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax45 == 0))) 
                break;
            addr_3a49_36:
            rax46 = separator;
            r10_47 = rcx41 + 1;
            eax48 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax46));
            *reinterpret_cast<void***>(rcx41) = *reinterpret_cast<void***>(&eax48);
            if (rbx5 == r12_17 && (rbx5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rbx5)), rcx49 = rbx5 + 1, v10 = rcx49, rax50 = xrealloc(v23, rcx49, rdx), rdx51 = r12_17 + 1, v23 = rax50, rax52 = fun_2680(reinterpret_cast<unsigned char>(rax50) + reinterpret_cast<unsigned char>(r12_17), rax50, rdx51), r10_47 = r10_47, r15_26 = rax52, rcx53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(v10)), reinterpret_cast<unsigned char>(rcx53) > reinterpret_cast<unsigned char>(v36))) {
                v10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_47) - reinterpret_cast<unsigned char>(r14_38));
                rax54 = xrealloc(r14_38, rcx53, rdx51);
                r14_38 = rax54;
                v36 = rcx53;
                r10_47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(r14_38));
                v39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax54) + reinterpret_cast<unsigned char>(rcx53));
            }
            rdx = r12_17;
            rax55 = fun_2670(r10_47, r15_26, rdx);
            rcx41 = rax55;
            if (reinterpret_cast<unsigned char>(rcx41) <= reinterpret_cast<unsigned char>(~reinterpret_cast<unsigned char>(r12_17) + reinterpret_cast<uint64_t>(v39))) 
                continue;
            *reinterpret_cast<uint32_t*>(&rdx) = 1;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rdi56 = r14_38;
            rsi6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx41) - reinterpret_cast<unsigned char>(r14_38));
            rcx57 = stdout;
            rax58 = fun_2630(rdi56, rsi6, 1, rcx57);
            if (rax58 != 1) 
                goto addr_3b8a_43;
            rcx41 = r14_38;
        }
        *reinterpret_cast<void***>(rcx41) = reinterpret_cast<void**>(10);
        *reinterpret_cast<uint32_t*>(&rdx) = 1;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rdi56 = r14_38;
        rsi6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx41 + 1) - reinterpret_cast<unsigned char>(r14_38));
        rcx57 = stdout;
        rax59 = fun_2630(rdi56, rsi6, 1, rcx57);
        if (rax59 - 1) 
            break;
        fun_26d0();
        continue;
        addr_3900_23:
        if (reinterpret_cast<unsigned char>(rax15) > reinterpret_cast<unsigned char>(rbp4)) 
            goto addr_3914_47;
        rdx21 = rax15;
        rsi22 = rax32;
        eax60 = fun_2560(r15_26, rsi22, rdx21, rcx33);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax60) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax60 == 0)) 
            goto addr_397c_24; else 
            goto addr_3914_47;
        addr_3934_13:
        r10_13 = rax14;
        goto addr_3851_15;
    }
    addr_3b8a_43:
    io_error(rdi56, rsi6, 1, rcx57, r8_61, r9_62);
    addr_3914_47:
    fun_23d0(v23, v23);
}