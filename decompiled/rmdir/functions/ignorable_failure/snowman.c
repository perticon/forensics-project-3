unsigned char ignorable_failure(uint32_t edi, void** rsi, void** rdx, void** rcx, int64_t r8) {
    int1_t zf6;
    uint32_t eax7;
    uint32_t ecx8;
    uint64_t rax9;
    int32_t eax10;
    uint32_t* rax11;
    uint32_t* rbx12;
    uint32_t r12d13;
    int64_t rdi14;
    int64_t rax15;
    int64_t rbp16;
    int64_t rdi17;
    struct s0* rax18;
    int64_t rdx19;
    uint32_t eax20;
    int32_t eax21;

    zf6 = ignore_fail_on_non_empty == 0;
    if (zf6) {
        return 0;
    }
    *reinterpret_cast<unsigned char*>(&eax7) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(edi == 17)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(edi == 39)));
    if (*reinterpret_cast<unsigned char*>(&eax7) || (edi > 30 || (ecx8 = edi, rax9 = 0x40012002 >> *reinterpret_cast<signed char*>(&ecx8), eax7 = *reinterpret_cast<uint32_t*>(&rax9) & 1, !eax7))) {
        return *reinterpret_cast<unsigned char*>(&eax7);
    }
    eax10 = fun_2480(0xffffff9c);
    rax11 = fun_23c0(0xffffff9c, 0xffffff9c);
    rbx12 = rax11;
    if (eax10 >= 0) 
        goto addr_2d0d_6;
    r12d13 = *rax11;
    goto addr_2d53_8;
    addr_2d0d_6:
    *reinterpret_cast<int32_t*>(&rdi14) = eax10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    rax15 = fun_2660(rdi14);
    rbp16 = rax15;
    if (!rax15) {
        *reinterpret_cast<int32_t*>(&rdi17) = eax10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
        fun_2500(rdi17);
        r12d13 = *rbx12;
        goto addr_2d53_8;
    }
    *rbx12 = 0;
    do {
        rax18 = fun_25b0(rbp16);
        if (!rax18) 
            break;
        if (rax18->f13 != 46) 
            goto addr_2d36_13;
        *reinterpret_cast<int32_t*>(&rdx19) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx19) = reinterpret_cast<uint1_t>(rax18->f14 == 46);
        eax20 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax18) + rdx19 + 20);
    } while (!*reinterpret_cast<signed char*>(&eax20) || *reinterpret_cast<signed char*>(&eax20) == 47);
    goto addr_2d76_15;
    r12d13 = *rbx12;
    fun_2510(rbp16);
    *rbx12 = r12d13;
    if (r12d13) {
        addr_2d53_8:
        *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(r12d13 == 0);
    } else {
        eax21 = 0;
    }
    return *reinterpret_cast<unsigned char*>(&eax21);
    addr_2d36_13:
    r12d13 = *rbx12;
    fun_2510(rbp16);
    *rbx12 = r12d13;
    goto addr_2d53_8;
    addr_2d76_15:
    goto addr_2d36_13;
}