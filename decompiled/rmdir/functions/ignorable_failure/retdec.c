bool ignorable_failure(uint32_t error_number, char * dir) {
    // 0x2cb0
    if (*(char *)&ignore_fail_on_non_empty == 0) {
        // 0x2ce0
        return false;
    }
    bool v1 = error_number == 17 | error_number == 39;
    if (error_number < 31 != !v1) {
        // 0x2cdd
        return (int64_t)v1 != 0;
    }
    uint32_t v2 = error_number % 64;
    if (v2 == 0) {
        // 0x2cdd
        return false;
    }
    uint32_t v3 = (int32_t)(0x40012002 >> (int64_t)v2) % 2;
    if (v3 == 0) {
        // 0x2cdd
        return (int64_t)v3 != 0;
    }
    int64_t v4 = function_2480(); // 0x2cf8
    int64_t v5 = function_23c0(); // 0x2d00
    if ((int32_t)v4 < 0) {
        // 0x2ce0
        return *(int32_t *)v5 == 0;
    }
    // 0x2d0d
    if (function_2660() == 0) {
        // 0x2d97
        function_2500();
        // 0x2ce0
        return *(int32_t *)v5 == 0;
    }
    int32_t * v6 = (int32_t *)v5; // 0x2d1d
    *v6 = 0;
    int64_t v7 = function_25b0(); // 0x2d26
    int64_t v8 = v7; // 0x2d2e
    if (v7 == 0) {
      lab_0x2d80:;
        int32_t v9 = *v6; // 0x2d80
        function_2510();
        *v6 = v9;
        if (v9 == 0) {
            // 0x2ce0
            return false;
        }
        // 0x2ce0
        return v9 == 0;
    }
    while (true) {
      lab_0x2d30:
        // 0x2d30
        if (*(char *)(v8 + 19) != 46) {
            // break -> 0x2d36
            break;
        }
        int64_t v10 = v8 + 20; // 0x2d62
        char v11 = *(char *)(v10 + (int64_t)(*(char *)v10 == 46)); // 0x2d69
        switch (v11) {
            case 0: {
                goto lab_0x2d23;
            }
            case 47: {
                goto lab_0x2d23;
            }
            default: {
                goto lab_0x2d36;
            }
        }
    }
  lab_0x2d36:;
    int32_t v12 = *v6; // 0x2d36
    function_2510();
    *v6 = v12;
    // 0x2ce0
    return v12 == 0;
  lab_0x2d23:
    // 0x2d23
    v8 = function_25b0();
    if (v8 == 0) {
        goto lab_0x2d80;
    }
    goto lab_0x2d30;
}