ulong ignorable_failure(uint param_1,char *param_2)

{
  int iVar1;
  int iVar2;
  ulong in_RAX;
  ulong uVar3;
  ulong uVar4;
  int *piVar5;
  DIR *__dirp;
  dirent *pdVar6;
  undefined4 extraout_var;
  undefined4 extraout_var_00;
  undefined4 extraout_var_01;
  
  if (ignore_fail_on_non_empty == '\0') {
    return 0;
  }
  uVar3 = in_RAX & 0xffffffffffffff00 | (ulong)(param_1 == 0x11 || param_1 == 0x27);
  if (((param_1 == 0x11 || param_1 == 0x27) || (0x1e < param_1)) ||
     (uVar4 = 0x40012002 >> ((byte)param_1 & 0x3f), uVar3 = (ulong)((uint)uVar4 & 1),
     (uVar4 & 1) == 0)) {
    return uVar3;
  }
  iVar1 = openat(-100,param_2,0x30900);
  piVar5 = __errno_location();
  if (iVar1 < 0) {
    iVar1 = *piVar5;
  }
  else {
    __dirp = fdopendir(iVar1);
    if (__dirp == (DIR *)0x0) {
      iVar2 = close(iVar1);
      iVar1 = *piVar5;
      piVar5 = (int *)CONCAT44(extraout_var_01,iVar2);
    }
    else {
      *piVar5 = 0;
      do {
        pdVar6 = readdir(__dirp);
        if (pdVar6 == (dirent *)0x0) {
          iVar1 = *piVar5;
          iVar2 = closedir(__dirp);
          *piVar5 = iVar1;
          piVar5 = (int *)CONCAT44(extraout_var_00,iVar2);
          if (iVar1 == 0) {
            return 0;
          }
          goto LAB_00102d53;
        }
      } while ((pdVar6->d_name[0] == '.') &&
              ((pdVar6->d_name[(ulong)(pdVar6->d_name[1] == '.') + 1] == '\0' ||
               (pdVar6->d_name[(ulong)(pdVar6->d_name[1] == '.') + 1] == '/'))));
      iVar1 = *piVar5;
      iVar2 = closedir(__dirp);
      *piVar5 = iVar1;
      piVar5 = (int *)CONCAT44(extraout_var,iVar2);
    }
  }
LAB_00102d53:
  return (ulong)piVar5 & 0xffffffffffffff00 | (ulong)(iVar1 == 0);
}