usage (int status)
{
  if (status != EXIT_SUCCESS)
    emit_try_help ();
  else
    {
      printf (_("Usage: %s [OPTION]... DIRECTORY...\n"), program_name);
      fputs (_("\
Remove the DIRECTORY(ies), if they are empty.\n\
\n\
"), stdout);
      fputs (_("\
      --ignore-fail-on-non-empty\n\
                    ignore each failure to remove a non-empty directory\n\
"), stdout);
      fputs (_("\
  -p, --parents     remove DIRECTORY and its ancestors;\n\
                    e.g., 'rmdir -p a/b' is similar to 'rmdir a/b a'\n\
\n\
"), stdout);
      fputs (_("\
  -v, --verbose     output a diagnostic for every directory processed\n\
"), stdout);
      fputs (HELP_OPTION_DESCRIPTION, stdout);
      fputs (VERSION_OPTION_DESCRIPTION, stdout);
      emit_ancillary_info (PROGRAM_NAME);
    }
  exit (status);
}