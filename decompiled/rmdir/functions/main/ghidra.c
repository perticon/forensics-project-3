byte main(int param_1,undefined8 *param_2)

{
  char *__path;
  byte bVar1;
  char cVar2;
  int iVar3;
  int iVar4;
  undefined8 uVar5;
  char *pcVar6;
  int *piVar7;
  undefined8 uVar8;
  undefined *__ptr;
  char *unaff_R14;
  long in_FS_OFFSET;
  bool bVar9;
  byte local_d9;
  stat local_d8;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  __ptr = &DAT_00109107;
  textdomain("coreutils");
  atexit(close_stdout);
  remove_empty_parents = 0;
LAB_001027b5:
  do {
    iVar3 = getopt_long(param_1,param_2,&DAT_00109107,longopts,0);
    if (iVar3 == -1) {
      if (optind != param_1) {
        local_d9 = 1;
        unaff_R14 = "removing directory, %s";
        if (optind < param_1) goto LAB_001028c5;
LAB_00102a34:
        if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
          __stack_chk_fail();
        }
        return local_d9 ^ 1;
      }
      uVar8 = dcgettext(0,"missing operand",5);
      error(0,0,uVar8);
LAB_00102b5e:
      usage(1);
LAB_00102b68:
      quotearg_style(4,__ptr);
      uVar8 = dcgettext(0,"failed to remove %s: Symbolic link not followed",5);
      error(0,0,uVar8);
      free(__ptr);
      local_d9 = 0;
LAB_001028ae:
      do {
        while( true ) {
          optind = optind + 1;
          if (param_1 <= optind) goto LAB_00102a34;
LAB_001028c5:
          __path = (char *)param_2[optind];
          if (verbose != '\0') {
            uVar8 = quotearg_style(4,__path);
            uVar5 = dcgettext(0,unaff_R14,5);
            prog_fprintf(stdout,uVar5,uVar8);
          }
          iVar3 = rmdir(__path);
          bVar1 = remove_empty_parents;
          if (iVar3 != 0) break;
          if (remove_empty_parents != 0) {
            strip_trailing_slashes(__path);
            do {
              pcVar6 = strrchr(__path,0x2f);
              if (pcVar6 == (char *)0x0) goto LAB_001029f2;
              if (__path < pcVar6) {
                do {
                  if (*pcVar6 != '/') break;
                  pcVar6 = pcVar6 + -1;
                } while (__path != pcVar6);
              }
              bVar9 = verbose != '\0';
              pcVar6[1] = '\0';
              if (bVar9) {
                uVar8 = quotearg_style(4,__path);
                uVar5 = dcgettext(0,unaff_R14,5);
                prog_fprintf(stdout,uVar5,uVar8);
              }
              iVar4 = rmdir(__path);
              piVar7 = __errno_location();
              iVar3 = *piVar7;
            } while (iVar4 == 0);
            cVar2 = ignorable_failure(iVar3,__path);
            if (cVar2 == '\0') {
              pcVar6 = "failed to remove %s";
              if (iVar3 != 0x14) {
                pcVar6 = "failed to remove directory %s";
              }
              quotearg_style(4,__path);
              uVar8 = dcgettext(0,pcVar6,5);
              error(0,iVar3,uVar8);
              bVar1 = 0;
            }
LAB_001029f2:
            local_d9 = local_d9 & bVar1;
          }
        }
        piVar7 = __errno_location();
        iVar3 = *piVar7;
        cVar2 = ignorable_failure(iVar3,__path);
      } while (cVar2 != '\0');
      if (((iVar3 == 0x14) && (pcVar6 = strrchr(__path,0x2f), pcVar6 != (char *)0x0)) &&
         (pcVar6[1] == '\0')) {
        iVar4 = stat(__path,&local_d8);
        if (iVar4 == 0) {
          if ((local_d8.st_mode & 0xf000) == 0x4000) {
LAB_00102aed:
            __ptr = (undefined *)xstrdup(__path);
            strip_trailing_slashes(__path);
            iVar4 = lstat(__path,&local_d8);
            if ((iVar4 == 0) && ((local_d8.st_mode & 0xf000) == 0xa000)) goto LAB_00102b68;
            free(__ptr);
          }
        }
        else if (*piVar7 != 0x14) goto LAB_00102aed;
      }
      quotearg_style(4,__path);
      uVar8 = dcgettext(0,"failed to remove %s",5);
      error(0,iVar3,uVar8);
      local_d9 = 0;
      goto LAB_001028ae;
    }
    if (iVar3 == 0x70) {
LAB_00102860:
      remove_empty_parents = 1;
      goto LAB_001027b5;
    }
    if (iVar3 < 0x71) {
      if (iVar3 == -0x83) {
        version_etc(stdout,"rmdir","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 == -0x82) {
        usage();
        goto LAB_00102860;
      }
      goto LAB_00102b5e;
    }
    if (iVar3 == 0x76) {
      verbose = '\x01';
    }
    else {
      if (iVar3 != 0x80) goto LAB_00102b5e;
      ignore_fail_on_non_empty = 1;
    }
  } while( true );
}