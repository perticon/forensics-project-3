errno_may_be_non_empty (int error_number)
{
  switch (error_number)
    {
    case EACCES:
    case EPERM:
    case EROFS:
    case EBUSY:
      return true;
    default:
      return false;
    }
}