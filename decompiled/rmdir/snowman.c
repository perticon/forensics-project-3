
signed char ignore_fail_on_non_empty = 0;

int32_t fun_2480(void** rdi);

uint32_t* fun_23c0(void** rdi, ...);

int64_t fun_2660(int64_t rdi);

void fun_2500(int64_t rdi);

struct s0 {
    signed char[19] pad19;
    signed char f13;
    signed char f14;
};

struct s0* fun_25b0(int64_t rdi);

void fun_2510(int64_t rdi);

unsigned char ignorable_failure(uint32_t edi, void** rsi, void** rdx, void** rcx, int64_t r8) {
    int1_t zf6;
    uint32_t eax7;
    uint32_t ecx8;
    uint64_t rax9;
    int32_t eax10;
    uint32_t* rax11;
    uint32_t* rbx12;
    uint32_t r12d13;
    int64_t rdi14;
    int64_t rax15;
    int64_t rbp16;
    int64_t rdi17;
    struct s0* rax18;
    int64_t rdx19;
    uint32_t eax20;
    int32_t eax21;

    zf6 = ignore_fail_on_non_empty == 0;
    if (zf6) {
        return 0;
    }
    *reinterpret_cast<unsigned char*>(&eax7) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(edi == 17)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(edi == 39)));
    if (*reinterpret_cast<unsigned char*>(&eax7) || (edi > 30 || (ecx8 = edi, rax9 = 0x40012002 >> *reinterpret_cast<signed char*>(&ecx8), eax7 = *reinterpret_cast<uint32_t*>(&rax9) & 1, !eax7))) {
        return *reinterpret_cast<unsigned char*>(&eax7);
    }
    eax10 = fun_2480(0xffffff9c);
    rax11 = fun_23c0(0xffffff9c, 0xffffff9c);
    rbx12 = rax11;
    if (eax10 >= 0) 
        goto addr_2d0d_6;
    r12d13 = *rax11;
    goto addr_2d53_8;
    addr_2d0d_6:
    *reinterpret_cast<int32_t*>(&rdi14) = eax10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    rax15 = fun_2660(rdi14);
    rbp16 = rax15;
    if (!rax15) {
        *reinterpret_cast<int32_t*>(&rdi17) = eax10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
        fun_2500(rdi17);
        r12d13 = *rbx12;
        goto addr_2d53_8;
    }
    *rbx12 = 0;
    do {
        rax18 = fun_25b0(rbp16);
        if (!rax18) 
            break;
        if (rax18->f13 != 46) 
            goto addr_2d36_13;
        *reinterpret_cast<int32_t*>(&rdx19) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx19) = reinterpret_cast<uint1_t>(rax18->f14 == 46);
        eax20 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax18) + rdx19 + 20);
    } while (!*reinterpret_cast<signed char*>(&eax20) || *reinterpret_cast<signed char*>(&eax20) == 47);
    goto addr_2d76_15;
    r12d13 = *rbx12;
    fun_2510(rbp16);
    *rbx12 = r12d13;
    if (r12d13) {
        addr_2d53_8:
        *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(r12d13 == 0);
    } else {
        eax21 = 0;
    }
    return *reinterpret_cast<unsigned char*>(&eax21);
    addr_2d36_13:
    r12d13 = *rbx12;
    fun_2510(rbp16);
    *rbx12 = r12d13;
    goto addr_2d53_8;
    addr_2d76_15:
    goto addr_2d36_13;
}

int64_t fun_2460();

int64_t fun_23b0(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2460();
    if (r8d > 10) {
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9540 + rax11 * 4) + 0x9540;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_24f0();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_23a0(void** rdi, ...);

void** xcharalloc(void** rdi, ...);

void fun_2490();

void** quotearg_n_options(void** rdi, void** rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    uint32_t* rax8;
    void** r15_9;
    uint32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x496f;
    rax8 = fun_23c0(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xd070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x8561]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x49fb;
            fun_24f0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xd0e0) {
                fun_23a0(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x4a8a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2490();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xd080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x94e3);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x94dc);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x94e7);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x94d8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gce18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gce18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2383() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __snprintf_chk = 0x2030;

void fun_2393() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2040;

void fun_23a3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_23b3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_23c3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23d3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23e3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23f3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_2403() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20b0;

void fun_2413() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_2423() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_2433() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t rmdir = 0x20e0;

void fun_2443() {
    __asm__("cli ");
    goto rmdir;
}

int64_t dcgettext = 0x20f0;

void fun_2453() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2463() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2473() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x2120;

void fun_2483() {
    __asm__("cli ");
    goto openat;
}

int64_t __stack_chk_fail = 0x2130;

void fun_2493() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2140;

void fun_24a3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2150;

void fun_24b3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2160;

void fun_24c3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2170;

void fun_24d3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_24e3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2190;

void fun_24f3() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x21a0;

void fun_2503() {
    __asm__("cli ");
    goto close;
}

int64_t closedir = 0x21b0;

void fun_2513() {
    __asm__("cli ");
    goto closedir;
}

int64_t lstat = 0x21c0;

void fun_2523() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x21d0;

void fun_2533() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2543() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2553() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2563() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_2573() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x2220;

void fun_2583() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2230;

void fun_2593() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2240;

void fun_25a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x2250;

void fun_25b3() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x2260;

void fun_25c3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2270;

void fun_25d3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2280;

void fun_25e3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2290;

void fun_25f3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x22a0;

void fun_2603() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x22b0;

void fun_2613() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22c0;

void fun_2623() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22d0;

void fun_2633() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22e0;

void fun_2643() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22f0;

void fun_2653() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fdopendir = 0x2300;

void fun_2663() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t __cxa_atexit = 0x2310;

void fun_2673() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2320;

void fun_2683() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2330;

void fun_2693() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2340;

void fun_26a3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2350;

void fun_26b3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2360;

void fun_26c3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2370;

void fun_26d3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2620(int64_t rdi, ...);

void fun_2430(int64_t rdi, int64_t rsi);

void fun_2410(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

unsigned char remove_empty_parents = 0;

int32_t fun_24a0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void usage();

void** stdout = reinterpret_cast<void**>(0);

void** Version = reinterpret_cast<void**>(0x73);

void version_etc(void** rdi, int64_t rsi, void** rdx, void** rcx, int64_t r8);

int32_t fun_2680();

signed char verbose = 0;

int32_t optind = 0;

void** fun_2450();

void fun_2640();

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void prog_fprintf(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

int32_t fun_2440(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void** fun_24d0(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void strip_trailing_slashes(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

int32_t fun_2580(void** rdi);

void** xstrdup(void** rdi);

int32_t fun_2520(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

int64_t fun_2733(int32_t edi, void** rsi) {
    int32_t ebp3;
    void** rbx4;
    void** rdi5;
    void** rax6;
    void** v7;
    void** r12_8;
    void*** rsp9;
    int64_t r8_10;
    void** rcx11;
    void** rdx12;
    void** rsi13;
    int64_t rdi14;
    int32_t eax15;
    void** rdi16;
    int32_t eax17;
    void** rax18;
    unsigned char v19;
    void* rsp20;
    void** rax21;
    void** rax22;
    int32_t eax23;
    int1_t zf24;
    void** r15_25;
    void** rax26;
    void** rax27;
    void** rdi28;
    int32_t eax29;
    uint32_t* rax30;
    uint32_t r13d31;
    unsigned char al32;
    void** rax33;
    uint32_t eax34;
    void** v35;
    void* rsp36;
    void** rax37;
    int1_t zf38;
    void** rax39;
    void** rax40;
    void** rdi41;
    int32_t eax42;
    uint32_t* rax43;
    uint32_t r13d44;
    void** rax45;
    void** rax46;
    void** v47;
    int32_t eax48;
    void** rax49;
    int32_t eax50;
    uint32_t v51;
    uint32_t v52;
    unsigned char al53;
    void** rax54;
    uint32_t r13d55;
    int64_t rax56;
    void* rdx57;

    __asm__("cli ");
    ebp3 = edi;
    rbx4 = rsi;
    rdi5 = *reinterpret_cast<void***>(rsi);
    rax6 = g28;
    v7 = rax6;
    set_program_name(rdi5);
    fun_2620(6, 6);
    fun_2430("coreutils", "/usr/local/share/locale");
    r12_8 = reinterpret_cast<void**>("pv");
    fun_2410("coreutils", "/usr/local/share/locale");
    atexit(0x32a0, "/usr/local/share/locale");
    rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    remove_empty_parents = 0;
    while (*reinterpret_cast<int32_t*>(&r8_10) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0, rcx11 = reinterpret_cast<void**>(0xcae0), rdx12 = reinterpret_cast<void**>("pv"), rsi13 = rbx4, *reinterpret_cast<int32_t*>(&rdi14) = ebp3, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0, eax15 = fun_24a0(rdi14, rsi13, "pv", 0xcae0), rsp9 = rsp9 - 8 + 8, eax15 != -1) {
        if (eax15 == 0x70) {
            addr_2860_4:
            remove_empty_parents = 1;
            continue;
        } else {
            if (eax15 <= 0x70) {
                if (eax15 != 0xffffff7d) {
                    if (eax15 != 0xffffff7e) 
                        goto addr_2b5e_8;
                    usage();
                    rsp9 = rsp9 - 8 + 8;
                    goto addr_2860_4;
                } else {
                    rdi16 = stdout;
                    rcx11 = Version;
                    r8_10 = reinterpret_cast<int64_t>("David MacKenzie");
                    rdx12 = reinterpret_cast<void**>("GNU coreutils");
                    version_etc(rdi16, "rmdir", "GNU coreutils", rcx11, "David MacKenzie");
                    eax15 = fun_2680();
                    rsp9 = rsp9 - 8 + 8 - 8 + 8;
                }
            } else {
                if (eax15 == 0x76) {
                    verbose = 1;
                    continue;
                }
            }
        }
        if (eax15 != 0x80) 
            goto addr_2b5e_8;
        ignore_fail_on_non_empty = 1;
    }
    eax17 = optind;
    if (eax17 == ebp3) {
        rax18 = fun_2450();
        rdx12 = rax18;
        fun_2640();
        rsp9 = rsp9 - 8 + 8 - 8 + 8;
    } else {
        v19 = 1;
        if (eax17 < ebp3) 
            goto addr_28c5_18;
        goto addr_2a34_20;
    }
    addr_2b5e_8:
    usage();
    rsp20 = reinterpret_cast<void*>(rsp9 - 8 + 8);
    while (1) {
        addr_2b68_21:
        rax21 = quotearg_style(4, r12_8, rdx12, rcx11, r8_10);
        rax22 = fun_2450();
        rcx11 = rax21;
        *reinterpret_cast<uint32_t*>(&rsi13) = 0;
        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
        rdx12 = rax22;
        fun_2640();
        fun_23a0(r12_8, r12_8);
        rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp20) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        v19 = 0;
        while (eax23 = optind, eax17 = eax23 + 1, optind = eax17, eax17 < ebp3) {
            addr_28c5_18:
            zf24 = verbose == 0;
            r15_25 = *reinterpret_cast<void***>(rbx4 + eax17 * 8);
            if (!zf24) {
                rax26 = quotearg_style(4, r15_25, rdx12, rcx11, r8_10);
                rax27 = fun_2450();
                rdi28 = stdout;
                rdx12 = rax26;
                rsi13 = rax27;
                prog_fprintf(rdi28, rsi13, rdx12, rcx11, r8_10);
                rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
            }
            eax29 = fun_2440(r15_25, rsi13, rdx12, rcx11, r8_10);
            rsp9 = rsp9 - 8 + 8;
            if (eax29) {
                rax30 = fun_23c0(r15_25, r15_25);
                rsi13 = r15_25;
                r13d31 = *rax30;
                al32 = ignorable_failure(r13d31, rsi13, rdx12, rcx11, r8_10);
                rsp9 = rsp9 - 8 + 8 - 8 + 8;
                if (al32) 
                    continue;
                if (r13d31 != 20) 
                    goto addr_2945_27;
                rax33 = fun_24d0(r15_25, 47, rdx12, rcx11, r8_10);
                rsp9 = rsp9 - 8 + 8;
                if (!rax33) 
                    goto addr_2945_27;
                if (!*reinterpret_cast<void***>(rax33 + 1)) 
                    goto addr_2ac0_30;
            } else {
                eax34 = remove_empty_parents;
                v35 = *reinterpret_cast<void***>(&eax34);
                if (!*reinterpret_cast<void***>(&eax34)) 
                    continue;
                strip_trailing_slashes(r15_25, rsi13, rdx12, rcx11, r8_10);
                rsp36 = reinterpret_cast<void*>(rsp9 - 8 + 8);
                do {
                    *reinterpret_cast<uint32_t*>(&rsi13) = 47;
                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                    rax37 = fun_24d0(r15_25, 47, rdx12, rcx11, r8_10);
                    rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
                    if (!rax37) 
                        goto addr_29f2_34;
                    if (reinterpret_cast<unsigned char>(r15_25) < reinterpret_cast<unsigned char>(rax37)) {
                        do {
                            if (*reinterpret_cast<void***>(rax37) != 47) 
                                break;
                            --rax37;
                        } while (r15_25 != rax37);
                    }
                    zf38 = verbose == 0;
                    *reinterpret_cast<void***>(rax37 + 1) = reinterpret_cast<void**>(0);
                    if (!zf38) {
                        rax39 = quotearg_style(4, r15_25, rdx12, rcx11, r8_10);
                        rax40 = fun_2450();
                        rdi41 = stdout;
                        rdx12 = rax39;
                        rsi13 = rax40;
                        prog_fprintf(rdi41, rsi13, rdx12, rcx11, r8_10);
                        rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
                    }
                    eax42 = fun_2440(r15_25, rsi13, rdx12, rcx11, r8_10);
                    rax43 = fun_23c0(r15_25, r15_25);
                    rsp36 = reinterpret_cast<void*>(rsp9 - 8 + 8 - 8 + 8);
                    r13d44 = *rax43;
                } while (!eax42);
                goto addr_29e3_42;
            }
            addr_2945_27:
            rax45 = quotearg_style(4, r15_25, rdx12, rcx11, r8_10);
            rax46 = fun_2450();
            rcx11 = rax45;
            *reinterpret_cast<uint32_t*>(&rsi13) = r13d31;
            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
            rdx12 = rax46;
            fun_2640();
            rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
            v19 = 0;
            continue;
            addr_2ac0_30:
            v47 = reinterpret_cast<void**>(rsp9 + 16);
            eax48 = fun_2580(r15_25);
            rsp9 = rsp9 - 8 + 8;
            if (eax48) {
                if (*rax30 != 20) {
                    addr_2aed_44:
                    rax49 = xstrdup(r15_25);
                    r12_8 = rax49;
                    strip_trailing_slashes(r15_25, v47, rdx12, rcx11, r8_10);
                    eax50 = fun_2520(r15_25, v47, rdx12, rcx11, r8_10);
                    rsp20 = reinterpret_cast<void*>(rsp9 - 8 + 8 - 8 + 8 - 8 + 8);
                    if (eax50) 
                        goto addr_2b24_45;
                    if ((v51 & 0xf000) == 0xa000) 
                        goto addr_2b68_21;
                } else {
                    goto addr_2945_27;
                }
            } else {
                if ((v52 & 0xf000) != 0x4000) 
                    goto addr_2945_27; else 
                    goto addr_2aed_44;
            }
            addr_2b24_45:
            fun_23a0(r12_8, r12_8);
            rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
            goto addr_2945_27;
            addr_29f2_34:
            *reinterpret_cast<uint32_t*>(&rcx11) = reinterpret_cast<unsigned char>(v35);
            *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
            v19 = reinterpret_cast<unsigned char>(v19 & *reinterpret_cast<unsigned char*>(&rcx11));
            continue;
            addr_29e3_42:
            rsi13 = r15_25;
            al53 = ignorable_failure(r13d44, rsi13, rdx12, rcx11, r8_10);
            rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
            if (!al53) {
                if (r13d44 != 20) {
                }
                quotearg_style(4, r15_25, rdx12, rcx11, r8_10);
                rax54 = fun_2450();
                *reinterpret_cast<uint32_t*>(&rsi13) = r13d44;
                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                rdx12 = rax54;
                fun_2640();
                rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
                v35 = reinterpret_cast<void**>(0);
                goto addr_29f2_34;
            }
        }
        break;
    }
    addr_2a34_20:
    r13d55 = static_cast<uint32_t>(v19) ^ 1;
    *reinterpret_cast<uint32_t*>(&rax56) = *reinterpret_cast<unsigned char*>(&r13d55);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax56) + 4) = 0;
    rdx57 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rdx57) {
        fun_2490();
    } else {
        return rax56;
    }
}

int64_t __libc_start_main = 0;

void fun_2bc3() {
    __asm__("cli ");
    __libc_start_main(0x2730, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xd008;

void fun_2380(int64_t rdi);

int64_t fun_2c63() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2380(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2ca3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2630(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2540(void** rdi, void** rsi, int64_t rdx, void** rcx);

int32_t fun_2560(int64_t rdi);

int32_t fun_23d0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_26a0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2db3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    int32_t eax19;
    void** r13_20;
    void** rax21;
    void** rax22;
    int32_t eax23;
    void** rax24;
    void** rax25;
    void** rax26;
    int32_t eax27;
    void** rax28;
    void** r15_29;
    void** rax30;
    void** rax31;
    void** rax32;
    void** rdi33;
    void** r8_34;
    void** r9_35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2450();
            fun_2630(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2450();
            fun_2540(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2450();
            fun_2540(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2450();
            fun_2540(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2450();
            fun_2540(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2450();
            fun_2540(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2450();
            fun_2540(rax18, r12_17, 5, rcx6);
            do {
                if (1) 
                    break;
                eax19 = fun_2560("rmdir");
            } while (eax19);
            r13_20 = v4;
            if (!r13_20) {
                rax21 = fun_2450();
                fun_2630(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax22 = fun_2620(5);
                if (!rax22 || (eax23 = fun_23d0(rax22, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax23)) {
                    rax24 = fun_2450();
                    r13_20 = reinterpret_cast<void**>("rmdir");
                    fun_2630(1, rax24, "https://www.gnu.org/software/coreutils/", "rmdir");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_20 = reinterpret_cast<void**>("rmdir");
                    goto addr_3130_9;
                }
            } else {
                rax25 = fun_2450();
                fun_2630(1, rax25, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax26 = fun_2620(5);
                if (!rax26 || (eax27 = fun_23d0(rax26, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax27)) {
                    addr_3036_11:
                    rax28 = fun_2450();
                    fun_2630(1, rax28, "https://www.gnu.org/software/coreutils/", "rmdir");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_20 == "rmdir")) {
                        r12_2 = reinterpret_cast<void**>(0x9901);
                    }
                } else {
                    addr_3130_9:
                    r15_29 = stdout;
                    rax30 = fun_2450();
                    fun_2540(rax30, r15_29, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3036_11;
                }
            }
            rax31 = fun_2450();
            rcx6 = r12_2;
            fun_2630(1, rax31, r13_20, rcx6);
            addr_2e0e_14:
            fun_2680();
        }
    } else {
        rax32 = fun_2450();
        rdi33 = stderr;
        rcx6 = r12_2;
        fun_26a0(rdi33, 1, rax32, rcx6, r8_34, r9_35, v36, v37, v38, v39, v40, v41);
        goto addr_2e0e_14;
    }
}

void fun_2600(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

void rpl_vfprintf(void** rdi, int64_t rsi, void* rdx, void** rcx);

void fun_24c0(void** rdi, int64_t rsi, void* rdx, void** rcx);

void fun_3163(void** rdi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8, int64_t r9) {
    signed char al7;
    void** rax8;
    void** rdi9;
    void* rdx10;
    void** rax11;
    void* rax12;

    __asm__("cli ");
    if (al7) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax8 = g28;
    rdi9 = program_name;
    fun_2540(rdi9, rdi, rdx, rcx);
    fun_2600(": ", 1, 2, rdi);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0xd8 - 8 + 8 - 8 + 8);
    rpl_vfprintf(rdi, rsi, rdx10, rdi);
    rax11 = *reinterpret_cast<void***>(rdi + 40);
    if (reinterpret_cast<unsigned char>(rax11) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48))) {
        fun_24c0(rdi, 10, rdx10, rdi);
    } else {
        *reinterpret_cast<void***>(rdi + 40) = rax11 + 1;
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(10);
    }
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2490();
    } else {
        return;
    }
}

int64_t file_name = 0;

void fun_3283(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3293(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_23e0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_32a3() {
    void** rdi1;
    int32_t eax2;
    uint32_t* rax3;
    int1_t zf4;
    uint32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23c0(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2450();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3333_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2640();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23e0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3333_5:
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2640();
    }
}

signed char* last_component();

int64_t base_len(signed char* rdi);

unsigned char fun_3353(signed char* rdi) {
    signed char* rax2;
    signed char* rbx3;
    int64_t rax4;
    signed char* rbx5;
    int1_t zf6;

    __asm__("cli ");
    rax2 = last_component();
    rbx3 = rax2;
    if (!*rax2) {
        rbx3 = rdi;
    }
    rax4 = base_len(rbx3);
    rbx5 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rbx3) + rax4);
    zf6 = *rbx5 == 0;
    *rbx5 = 0;
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!zf6));
}

uint64_t fun_2690(void** rdi, void* rsi, uint64_t rdx, void** rcx);

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_3393(void** rdi) {
    void** rcx2;
    void** rbx3;
    void** rdx4;
    void** rcx5;
    int64_t r8_6;
    void** rax7;
    void** r12_8;
    void** rcx9;
    int32_t eax10;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2690("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23b0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax7 = fun_24d0(rdi, 47, rdx4, rcx5, r8_6);
        if (rax7 && ((r12_8 = rax7 + 1, reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_8) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax10 = fun_23d0(rax7 + 0xfffffffffffffffa, "/.libs/", 7, rcx9), !eax10))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax7 + 1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_8 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_8 + 2) != 45)) {
                rbx3 = r12_8;
            } else {
                rbx3 = rax7 + 4;
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(void** rdi, int64_t rsi);

void fun_4b33(void** rdi) {
    void** rbp2;
    uint32_t* rax3;
    uint32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23c0(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<void**>(0xd1e0);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_4b73(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4b93(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd1e0);
    }
    *rdi = esi;
    return 0xd1e0;
}

int64_t fun_4bb3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xd1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s4 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_4bf3(struct s4* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s4*>(0xd1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s5 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s5* fun_4c13(struct s5* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s5*>(0xd1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26ea;
    if (!rdx) 
        goto 0x26ea;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xd1e0;
}

struct s6 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_4c53(void** rdi, void** rsi, void** rdx, int64_t rcx, struct s6* r8) {
    struct s6* rbx6;
    uint32_t* rax7;
    uint32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s6*>(0xd1e0);
    }
    rax7 = fun_23c0(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x4c86);
    *rax7 = r15d8;
    return rax13;
}

struct s7 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_4cd3(void** rdi, int64_t rsi, void*** rdx, struct s7* rcx) {
    struct s7* rbx5;
    uint32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    uint32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s7*>(0xd1e0);
    }
    rax6 = fun_23c0(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x4d01);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x4d5c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_4dc3() {
    __asm__("cli ");
}

void** gd078 = reinterpret_cast<void**>(0xe0);

int64_t slotvec0 = 0x100;

void fun_4dd3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23a0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xd0e0) {
        fun_23a0(rdi7);
        gd078 = reinterpret_cast<void**>(0xd0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xd070) {
        fun_23a0(r12_2);
        slotvec = reinterpret_cast<void**>(0xd070);
    }
    nslots = 1;
    return;
}

void fun_4e73() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4e93() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4ea3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4ec3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_4ee3(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26f0;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

void** fun_4f73(void** rdi, int32_t esi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26f5;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2490();
    } else {
        return rax7;
    }
}

void** fun_5003(int32_t edi, void** rsi) {
    void** rax3;
    struct s1* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26fa;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2490();
    } else {
        return rax5;
    }
}

void** fun_5093(int32_t edi, void** rsi, int64_t rdx) {
    void** rax4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26ff;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

void** fun_5123(void** rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x80b0]");
    __asm__("movdqa xmm1, [rip+0x80b8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x80a1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2490();
    } else {
        return rax10;
    }
}

void** fun_51c3(void** rdi, uint32_t esi) {
    struct s1* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8010]");
    __asm__("movdqa xmm1, [rip+0x8018]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8001]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2490();
    } else {
        return rax9;
    }
}

void** fun_5263(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7f70]");
    __asm__("movdqa xmm1, [rip+0x7f78]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x7f59]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2490();
    } else {
        return rax3;
    }
}

void** fun_52f3(void** rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7ee0]");
    __asm__("movdqa xmm1, [rip+0x7ee8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x7ed6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2490();
    } else {
        return rax4;
    }
}

void** fun_5383(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2704;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

void** fun_5423(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7daa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x7da2]");
    __asm__("movdqa xmm2, [rip+0x7daa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2709;
    if (!rdx) 
        goto 0x2709;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2490();
    } else {
        return rax7;
    }
}

void** fun_54c3(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8) {
    void** rcx6;
    struct s1* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7d0a]");
    __asm__("movdqa xmm1, [rip+0x7d12]");
    __asm__("movdqa xmm2, [rip+0x7d1a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x270e;
    if (!rdx) 
        goto 0x270e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2490();
    } else {
        return rax9;
    }
}

void** fun_5573(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7c5a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x7c52]");
    __asm__("movdqa xmm2, [rip+0x7c5a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2713;
    if (!rsi) 
        goto 0x2713;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

void** fun_5613(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7bba]");
    __asm__("movdqa xmm1, [rip+0x7bc2]");
    __asm__("movdqa xmm2, [rip+0x7bca]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2718;
    if (!rsi) 
        goto 0x2718;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2490();
    } else {
        return rax7;
    }
}

void fun_56b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_56c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_56e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5703(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s8 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2570(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_5723(void** rdi, void** rsi, void** rdx, void** rcx, struct s8* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26a0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_26a0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2450();
    fun_26a0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2570(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2450();
    fun_26a0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2570(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2450();
        fun_26a0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x9ba8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x9ba8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5b93() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s9 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5bb3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s9* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s9* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2490();
    } else {
        return;
    }
}

void fun_5c53(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_5cf6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_5d00_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2490();
    } else {
        return;
    }
    addr_5cf6_5:
    goto addr_5d00_7;
}

void fun_5d33() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2570(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2450();
    fun_2630(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2450();
    fun_2630(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2450();
    goto fun_2630;
}

int64_t fun_2400();

void xalloc_die();

void fun_5dd3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_25c0(void** rdi, void** rsi, ...);

void fun_5e13(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e33(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e53(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2610(void** rdi, void** rsi, ...);

void fun_5e73(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2610(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5ea3(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2610(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5ed3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5f13() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5f53(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5f83(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5fd3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2400();
            if (rax5) 
                break;
            addr_601d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_601d_5;
        rax8 = fun_2400();
        if (rax8) 
            goto addr_6006_9;
        if (rbx4) 
            goto addr_601d_5;
        addr_6006_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6063(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2400();
            if (rax8) 
                break;
            addr_60aa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_60aa_5;
        rax11 = fun_2400();
        if (rax11) 
            goto addr_6092_9;
        if (!rbx6) 
            goto addr_6092_9;
        if (r12_4) 
            goto addr_60aa_5;
        addr_6092_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_60f3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_619d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_61b0_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_6150_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_6176_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_61c4_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_616d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_61c4_14;
            addr_616d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_61c4_14;
            addr_6176_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2610(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_61c4_14;
            if (!rbp13) 
                break;
            addr_61c4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_619d_9;
        } else {
            if (!r13_6) 
                goto addr_61b0_10;
            goto addr_6150_11;
        }
    }
}

int64_t fun_2550();

void fun_61f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2550();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6223() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2550();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6253() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2550();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6273() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2550();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2590(void** rdi, void** rsi, void** rdx);

void fun_6293(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_62d3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_6313(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2590;
    }
}

void** fun_2470(void** rdi, ...);

void fun_6353(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_2470(rdi);
    rax4 = fun_25c0(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_6393() {
    void** rdi1;

    __asm__("cli ");
    fun_2450();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2640();
    fun_23b0(rdi1);
}

void** vasnprintf(void** rdi, void* rsi, uint64_t rdx, void** rcx);

void fseterr(void** rdi, void* rsi, uint64_t rdx, void** rcx);

int64_t fun_63d3(void** rdi, uint64_t rsi, void** rdx) {
    void** rcx4;
    uint64_t rdx5;
    void* rsp6;
    void** rax7;
    void** r13_8;
    void* rsi9;
    void** rax10;
    int64_t rax11;
    void** rdi12;
    uint64_t rax13;
    void* rdx14;
    uint32_t* rax15;

    __asm__("cli ");
    rcx4 = rdx;
    rdx5 = rsi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x808);
    rax7 = g28;
    r13_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    rsi9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 24);
    rax10 = vasnprintf(r13_8, rsi9, rdx5, rcx4);
    if (!rax10) {
        addr_64a7_2:
        fseterr(rdi, rsi9, rdx5, rcx4);
        *reinterpret_cast<int32_t*>(&rax11) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    } else {
        rcx4 = rdi;
        rdx5 = 0x7d0;
        *reinterpret_cast<int32_t*>(&rsi9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rdi12 = rax10;
        rax13 = fun_2690(rdi12, 1, 0x7d0, rcx4);
        if (rax13 < 0x7d0) {
            *reinterpret_cast<int32_t*>(&rax11) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (rax10 != r13_8) {
                fun_23a0(rax10, rax10);
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
        } else {
            if (rax10 != r13_8) {
                rdi12 = rax10;
                fun_23a0(rdi12, rdi12);
            }
            if (0) 
                goto addr_649c_9; else 
                goto addr_645a_10;
        }
    }
    addr_645c_11:
    rdx14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx14) {
        fun_2490();
    } else {
        return rax11;
    }
    addr_649c_9:
    rax15 = fun_23c0(rdi12, rdi12);
    *rax15 = 75;
    goto addr_64a7_2;
    addr_645a_10:
    *reinterpret_cast<int32_t*>(&rax11) = 0x7d0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    goto addr_645c_11;
}

struct s10 {
    unsigned char f0;
    unsigned char f1;
};

struct s10* fun_64c3(struct s10* rdi) {
    uint32_t edx2;
    struct s10* rax3;
    struct s10* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s10*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s10*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s10*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_6523(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2470(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

int64_t fun_23f0();

int64_t rpl_fclose(void** rdi);

int64_t fun_6553(void** rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    uint32_t* rax5;
    uint32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23f0();
    ebx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_65ae_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23c0(rdi);
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_65ae_3;
            rax6 = fun_23c0(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_25a0(void** rdi);

int32_t fun_25f0(void** rdi);

int64_t fun_24e0(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_2420(void** rdi);

int64_t fun_65c3(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    uint32_t* rax8;
    uint32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_25a0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25f0(rdi);
        if (!(eax3 && (eax4 = fun_25a0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24e0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23c0(rdi, rdi);
            r12d9 = *rax8;
            rax10 = fun_2420(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2420;
}

void rpl_fseeko(void** rdi);

void fun_6653(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25f0(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_66a3(void** rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax4 = fun_25a0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24e0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_6723(uint32_t* rdi) {
    __asm__("cli ");
    *rdi = *rdi | 32;
    return;
}

signed char* fun_25e0(int64_t rdi);

signed char* fun_6733() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25e0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24b0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6773(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24b0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2490();
    } else {
        return r12_7;
    }
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s11 {
    signed char[7] pad7;
    void** f7;
};

struct s12 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s13 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_6803(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void** rdi11;
    void*** v12;
    void** rax13;
    void** v14;
    int32_t eax15;
    void* rsp16;
    void** r10_17;
    void* rax18;
    int64_t* rsp19;
    void** rbx20;
    int64_t* rsp21;
    void** rax22;
    void** v23;
    int64_t* rsp24;
    void** v25;
    int64_t* rsp26;
    void** v27;
    int64_t* rsp28;
    void** v29;
    int64_t* rsp30;
    uint32_t* rax31;
    void** r15_32;
    uint32_t* v33;
    int64_t* rsp34;
    int64_t* rsp35;
    void** v36;
    int64_t* rsp37;
    void** v38;
    int64_t* rsp39;
    void** rsi40;
    int32_t eax41;
    uint32_t* rax42;
    void** rax43;
    struct s11* v44;
    void** tmp64_45;
    void* v46;
    void** r8_47;
    void** tmp64_48;
    uint1_t cf49;
    void* rax50;
    void* rcx51;
    uint64_t rdx52;
    void* rdx53;
    void** v54;
    void** rax55;
    uint32_t* rax56;
    struct s12* r14_57;
    struct s12* v58;
    void** r9_59;
    void** r8_60;
    int64_t v61;
    int64_t v62;
    uint32_t edx63;
    void** tmp64_64;
    void** r10_65;
    int64_t* rsp66;
    int64_t* rsp67;
    void** rax68;
    int64_t* rsp69;
    void** rax70;
    int64_t* rsp71;
    void** rax72;
    int64_t* rsp73;
    void** rax74;
    int64_t* rsp75;
    void** rax76;
    int64_t* rsp77;
    void** rax78;
    void** tmp64_79;
    int64_t* rsp80;
    void** rax81;
    int64_t* rsp82;
    void** rax83;
    int64_t* rsp84;
    void** rax85;
    uint32_t ecx86;
    uint32_t* v87;
    int64_t r13_88;
    int32_t eax89;
    void** rsi90;
    void** rax91;
    int64_t* rsp92;
    void** rsi93;
    void** rax94;
    int64_t* rsp95;
    int64_t rax96;
    uint32_t eax97;
    int32_t v98;
    struct s13* rcx99;
    int64_t rax100;
    void** tmp64_101;
    void** r15_102;
    uint32_t* rax103;
    int64_t rax104;
    int64_t* rsp105;
    void** rax106;
    int64_t* rsp107;
    uint32_t* rax108;
    int64_t* rsp109;
    int64_t* rsp110;
    void** rax111;
    int64_t* rsp112;
    uint32_t* rax113;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    rdi11 = r13_8;
    v12 = rsi;
    rax13 = g28;
    v14 = rax13;
    eax15 = printf_parse(rdi11, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax15 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_17) = 0;
            *reinterpret_cast<int32_t*>(&r10_17 + 4) = 0;
            while (rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28)), !!rax18) {
                rsp19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp19 = 0x773a;
                fun_2490();
                rsp16 = reinterpret_cast<void*>(rsp19 + 1);
                addr_773a_5:
                if (rbx20 != 0xffffffffffffffff) 
                    goto addr_7744_6;
                addr_762e_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_17) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx20) > reinterpret_cast<unsigned char>(r13_8) && (r10_17 != v10 && (rsp21 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp21 = 0x7653, rax22 = fun_2610(r10_17, r13_8, r10_17, r13_8), rsp16 = reinterpret_cast<void*>(rsp21 + 1), r10_17 = r10_17, !!rax22))) {
                    r10_17 = rax22;
                }
                if (v23) {
                    rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp24 = 0x7679;
                    fun_23a0(v23, v23);
                    rsp16 = reinterpret_cast<void*>(rsp24 + 1);
                    r10_17 = r10_17;
                }
                if (v25 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp26 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp26 = 0x769f;
                    fun_23a0(v25, v25);
                    rsp16 = reinterpret_cast<void*>(rsp26 + 1);
                    r10_17 = r10_17;
                }
                rdi11 = v27;
                if (rdi11 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp28 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp28 = 0x76c5;
                    fun_23a0(rdi11, rdi11);
                    rsp16 = reinterpret_cast<void*>(rsp28 + 1);
                    r10_17 = r10_17;
                }
                *v12 = r12_9;
            }
            break;
            addr_7744_6:
            addr_7328_16:
            v29 = r10_17;
            addr_732f_17:
            rsp30 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp30 = 0x7334;
            rax31 = fun_23c0(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(rsp30 + 1);
            r15_32 = v29;
            v33 = rax31;
            addr_7342_18:
            *v33 = 12;
            if (r15_32 != v10) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp34 = 0x6ee1;
                fun_23a0(r15_32, r15_32);
                rsp16 = reinterpret_cast<void*>(rsp34 + 1);
            }
            if (v23) {
                rsp35 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp35 = 0x6ef9;
                fun_23a0(v23, v23);
                rsp16 = reinterpret_cast<void*>(rsp35 + 1);
            }
            addr_6b38_23:
            if (v36 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp37 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp37 = 0x6b50;
                fun_23a0(v36, v36);
                rsp16 = reinterpret_cast<void*>(rsp37 + 1);
            }
            rdi11 = v38;
            if (rdi11 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp39 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp39 = 0x6b68;
            fun_23a0(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(rsp39 + 1);
        }
        return r10_17;
    }
    rsi40 = r14_7;
    rdi11 = r12_9;
    eax41 = printf_fetchargs(rdi11, rsi40, r14_7);
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax41 < 0) {
        rax42 = fun_23c0(rdi11, rdi11);
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        *rax42 = 22;
        goto addr_6b38_23;
    }
    rax43 = reinterpret_cast<void**>(&v44->f7);
    if (reinterpret_cast<uint64_t>(v44) >= 0xfffffffffffffff9) {
        rax43 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax43) + reinterpret_cast<uint64_t>(v46));
    if (reinterpret_cast<unsigned char>(tmp64_45) < reinterpret_cast<unsigned char>(rax43)) 
        goto addr_6b2d_33;
    *reinterpret_cast<int32_t*>(&r8_47) = 0;
    *reinterpret_cast<int32_t*>(&r8_47 + 4) = 0;
    tmp64_48 = tmp64_45 + 6;
    cf49 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_48) < reinterpret_cast<unsigned char>(tmp64_45));
    rdi11 = tmp64_48;
    *reinterpret_cast<unsigned char*>(&r8_47) = cf49;
    if (cf49) 
        goto addr_6b2d_33;
    if (reinterpret_cast<unsigned char>(rdi11) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax50 = reinterpret_cast<void*>(tmp64_45 + 29);
        rcx51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - (reinterpret_cast<uint64_t>(rax50) & 0xfffffffffffff000));
        rdx52 = reinterpret_cast<uint64_t>(rax50) & 0xfffffffffffffff0;
        if (rsp16 != rcx51) {
            do {
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 0x1000);
            } while (rsp16 != rcx51);
        }
        *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(&rdx52) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx53) + 4) = 0;
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - reinterpret_cast<int64_t>(rdx53));
        if (rdx53) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp16) + reinterpret_cast<int64_t>(rdx53) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp16) + reinterpret_cast<int64_t>(rdx53) - 8);
        }
        v23 = reinterpret_cast<void**>(0);
        v54 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp16) + 15 & 0xfffffffffffffff0);
    } else {
        if (rdi11 == 0xffffffffffffffff || (rax55 = fun_25c0(rdi11, rsi40), rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8), v54 = rax55, rax55 == 0)) {
            addr_6b2d_33:
            rax56 = fun_23c0(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
            *rax56 = 12;
            goto addr_6b38_23;
        } else {
            v23 = rax55;
            r8_47 = r8_47;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
    if (v10) {
        rbx20 = *v12;
    }
    r14_57 = v58;
    r9_59 = r8_47;
    r8_60 = r13_8;
    v61 = 0;
    r15_32 = v10;
    r13_8 = r14_57->f0;
    if (r13_8 != r8_60) 
        goto addr_692c_46;
    while (1) {
        addr_7284_47:
        r12_9 = r9_59;
        r10_17 = r15_32;
        while (v62 != v61) {
            edx63 = r14_57->f48;
            if (*reinterpret_cast<signed char*>(&edx63) != 37) 
                goto addr_69ef_50;
            if (r14_57->f50 != -1) 
                goto 0x271d;
            r9_59 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_59 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(r9_59)) {
                addr_725f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_17) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_32 = r10_17;
            } else {
                if (!rbx20) {
                    *reinterpret_cast<int32_t*>(&rbx20) = 12;
                    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
                        goto addr_7328_16;
                    rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
                }
                if (reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(r9_59)) {
                    rbx20 = r9_59;
                }
                if (rbx20 == 0xffffffffffffffff) 
                    goto addr_7328_16;
                if (r10_17 == v10) 
                    goto addr_7574_64; else 
                    goto addr_7233_65;
            }
            r8_60 = r14_57->f8;
            r13_8 = r14_57->f58;
            r14_57 = reinterpret_cast<struct s12*>(&r14_57->f58);
            ++v61;
            if (r13_8 == r8_60) 
                goto addr_7284_47;
            addr_692c_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_60));
            tmp64_64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_59) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_64;
            if (reinterpret_cast<unsigned char>(tmp64_64) < reinterpret_cast<unsigned char>(r9_59)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_65 = r15_32;
            } else {
                if (!rbx20) {
                    *reinterpret_cast<int32_t*>(&rbx20) = 12;
                    *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
                        goto addr_73e0_73;
                    rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
                }
                if (reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx20 = r12_9;
                }
                if (rbx20 == 0xffffffffffffffff) 
                    goto addr_73e0_73;
                if (r15_32 == v10) 
                    goto addr_7370_79; else 
                    goto addr_6987_80;
            }
            addr_69ac_81:
            rdi11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_65) + reinterpret_cast<unsigned char>(r9_59));
            rsi40 = r8_60;
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp66 = 0x69c2;
            fun_2590(rdi11, rsi40, r13_8);
            rsp16 = reinterpret_cast<void*>(rsp66 + 1);
            r10_17 = r10_65;
            continue;
            addr_7370_79:
            rdi11 = rbx20;
            rsp67 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp67 = 0x7378;
            rax68 = fun_25c0(rdi11, rsi40, rdi11, rsi40);
            rsp16 = reinterpret_cast<void*>(rsp67 + 1);
            r9_59 = r9_59;
            r8_60 = r8_60;
            r10_65 = rax68;
            if (!rax68) 
                goto addr_73e0_73;
            if (!r9_59) 
                goto addr_69ac_81;
            rsp69 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp69 = 0x73b7;
            rax70 = fun_2590(rax68, v10, r9_59);
            rsp16 = reinterpret_cast<void*>(rsp69 + 1);
            r9_59 = r9_59;
            r8_60 = r8_60;
            r10_65 = rax70;
            goto addr_69ac_81;
            addr_6987_80:
            rdi11 = r15_32;
            rsp71 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp71 = 0x6992;
            rax72 = fun_2610(rdi11, rbx20, rdi11, rbx20);
            rsp16 = reinterpret_cast<void*>(rsp71 + 1);
            r9_59 = r9_59;
            r8_60 = r8_60;
            r10_65 = rax72;
            if (!rax72) 
                goto addr_73e0_73; else 
                goto addr_69ac_81;
            addr_7574_64:
            rdi11 = rbx20;
            rsp73 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp73 = 0x758a;
            rax74 = fun_25c0(rdi11, rsi40);
            rsp16 = reinterpret_cast<void*>(rsp73 + 1);
            r9_59 = r9_59;
            if (!rax74) 
                goto addr_7749_84;
            if (r12_9) 
                goto addr_75aa_86;
            r10_17 = rax74;
            goto addr_725f_55;
            addr_75aa_86:
            rsi40 = r10_17;
            rdi11 = rax74;
            rsp75 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp75 = 0x75bf;
            rax76 = fun_2590(rdi11, rsi40, r12_9);
            rsp16 = reinterpret_cast<void*>(rsp75 + 1);
            r9_59 = r9_59;
            r10_17 = rax76;
            goto addr_725f_55;
            addr_7233_65:
            rsi40 = rbx20;
            rdi11 = r10_17;
            v29 = r10_17;
            rsp77 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp77 = 0x724c;
            rax78 = fun_2610(rdi11, rsi40);
            rsp16 = reinterpret_cast<void*>(rsp77 + 1);
            r9_59 = r9_59;
            if (!rax78) 
                goto addr_732f_17;
            r10_17 = rax78;
            goto addr_725f_55;
        }
        break;
    }
    tmp64_79 = r12_9 + 1;
    r13_8 = tmp64_79;
    if (reinterpret_cast<unsigned char>(tmp64_79) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_773a_5;
    if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_762e_7;
    if (rbx20) {
        if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
            goto addr_7328_16;
        rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
        if (reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_76ed_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_76ed_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_7328_16; else 
                goto addr_76f7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx20) = 12;
            *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
        }
    }
    addr_7603_98:
    if (r10_17 == v10) {
        rdi11 = rbx20;
        rsp80 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp80 = 0x770e;
        rax81 = fun_25c0(rdi11, rsi40, rdi11, rsi40);
        rsp16 = reinterpret_cast<void*>(rsp80 + 1);
        if (rax81) {
            if (!r12_9) {
                r10_17 = rax81;
                goto addr_762e_7;
            } else {
                rsp82 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp82 = 0x772d;
                rax83 = fun_2590(rax81, r10_17, r12_9);
                rsp16 = reinterpret_cast<void*>(rsp82 + 1);
                r10_17 = rax83;
                goto addr_762e_7;
            }
        }
    } else {
        rdi11 = r10_17;
        v29 = r10_17;
        rsp84 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp84 = 0x7622;
        rax85 = fun_2610(rdi11, rbx20, rdi11, rbx20);
        rsp16 = reinterpret_cast<void*>(rsp84 + 1);
        r10_17 = rax85;
        if (!rax85) 
            goto addr_732f_17; else 
            goto addr_762e_7;
    }
    addr_76f7_96:
    rbx20 = r13_8;
    goto addr_7603_98;
    addr_69ef_50:
    if (r14_57->f50 == -1) 
        goto 0x271d;
    ecx86 = *reinterpret_cast<uint32_t*>((r14_57->f50 << 5) + reinterpret_cast<int64_t>(v87));
    if (*reinterpret_cast<signed char*>(&edx63) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_88) = ecx86 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_88) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_88) > 4) 
            goto 0x2722;
        goto *reinterpret_cast<int32_t*>(0x9c98 + r13_88 * 4) + 0x9c98;
    }
    eax89 = r14_57->f10;
    *reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(37);
    r13_8 = v54 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax89) & 1) {
        *reinterpret_cast<void***>(v54 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v54 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax89) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi90 = r14_57->f18;
    if (rsi90 != r14_57->f20) {
        rax91 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_57->f20) - reinterpret_cast<unsigned char>(rsi90));
        rdi11 = r13_8;
        rsp92 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp92 = 0x6abf;
        fun_2590(rdi11, rsi90, rax91);
        rsp16 = reinterpret_cast<void*>(rsp92 + 1);
        r10_17 = r10_17;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax91));
    }
    rsi93 = r14_57->f30;
    if (rsi93 != r14_57->f38) {
        rax94 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_57->f38) - reinterpret_cast<unsigned char>(rsi93));
        rdi11 = r13_8;
        rsp95 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp95 = 0x6af9;
        fun_2590(rdi11, rsi93, rax94);
        rsp16 = reinterpret_cast<void*>(rsp95 + 1);
        r10_17 = r10_17;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax94));
    }
    *reinterpret_cast<uint32_t*>(&rax96) = ecx86 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax96) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax96) <= 9) {
        goto *reinterpret_cast<int32_t*>(0x9c28 + rax96 * 4) + 0x9c28;
    }
    eax97 = r14_57->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax97);
    if (r14_57->f28 == -1) {
        v98 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_57->f28 << 5) + reinterpret_cast<int64_t>(v87)) != 5) 
            goto 0x271d;
        v98 = 1;
    }
    if (r14_57->f40 != -1) {
        rcx99 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(v87) + (r14_57->f40 << 5));
        if (rcx99->f0 != 5) 
            goto 0x271d;
        *reinterpret_cast<int32_t*>(&rax100) = v98;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax100) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax100 * 4 - 0x3b8) = rcx99->f10;
    }
    tmp64_101 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_101) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_6c32_135;
    if (rbx20 != 0xffffffffffffffff) {
        goto addr_7328_16;
    }
    addr_6c32_135:
    if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(tmp64_101)) {
        r15_102 = r10_17;
    } else {
        if (rbx20) {
            if (reinterpret_cast<signed char>(rbx20) < reinterpret_cast<signed char>(0)) 
                goto addr_7328_16;
            rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<unsigned char>(rbx20));
            if (reinterpret_cast<unsigned char>(rbx20) >= reinterpret_cast<unsigned char>(tmp64_101)) 
                goto addr_6c53_142; else 
                goto addr_74e2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_101) > reinterpret_cast<unsigned char>(12)) {
                addr_74e2_143:
                if (tmp64_101 == 0xffffffffffffffff) 
                    goto addr_7328_16; else 
                    goto addr_74ec_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx20) = 12;
                *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
                goto addr_6c53_142;
            }
        }
    }
    addr_6c85_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_102) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8) = 0x6c8f;
    rax103 = fun_23c0(rdi11, rdi11);
    *rax103 = 0;
    *reinterpret_cast<uint32_t*>(&rax104) = ecx86;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx20) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax104) > 17) 
        goto 0x2722;
    goto *reinterpret_cast<int32_t*>(0x9c50 + rax104 * 4) + 0x9c50;
    addr_6c53_142:
    if (r10_17 == v10) {
        rdi11 = rbx20;
        rsp105 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp105 = 0x7548;
        rax106 = fun_25c0(rdi11, rsi93);
        rsp16 = reinterpret_cast<void*>(rsp105 + 1);
        r15_102 = rax106;
        if (!rax106) {
            addr_7749_84:
            rsp107 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
            *rsp107 = 0x774e;
            rax108 = fun_23c0(rdi11, rdi11);
            rsp16 = reinterpret_cast<void*>(rsp107 + 1);
            r15_32 = v10;
            v33 = rax108;
            goto addr_7342_18;
        } else {
            if (r12_9) {
                rdi11 = rax106;
                rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp109 = 0x756f;
                fun_2590(rdi11, v10, r12_9);
                rsp16 = reinterpret_cast<void*>(rsp109 + 1);
                goto addr_6c85_147;
            }
        }
    } else {
        rdi11 = r10_17;
        rsp110 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp110 = 0x6c72;
        rax111 = fun_2610(rdi11, rbx20);
        rsp16 = reinterpret_cast<void*>(rsp110 + 1);
        r10_17 = r10_17;
        r15_102 = rax111;
        if (!rax111) 
            goto addr_7328_16; else 
            goto addr_6c85_147;
    }
    addr_74ec_145:
    rbx20 = tmp64_101;
    goto addr_6c53_142;
    addr_73e0_73:
    rsp112 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp16) - 8);
    *rsp112 = 0x73e5;
    rax113 = fun_23c0(rdi11, rdi11);
    rsp16 = reinterpret_cast<void*>(rsp112 + 1);
    v33 = rax113;
    goto addr_7342_18;
}

int32_t setlocale_null_r();

int64_t fun_7783() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax3;
    }
}

int64_t fun_7803(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2620(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2470(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2590(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2590(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_78b3() {
    __asm__("cli ");
    goto fun_2620;
}

struct s14 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_78c3(int64_t rdi, struct s14* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_78f9_5;
    return 0xffffffff;
    addr_78f9_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9cc0 + rdx3 * 4) + 0x9cc0;
}

struct s15 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s16 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s17 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

struct s18 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_7af3(void** rdi, struct s15* rsi, struct s16* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s16* r15_7;
    struct s15* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    struct s17* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s17* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    struct s17* rbx65;
    void* rsi66;
    int32_t eax67;
    struct s17* rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rax88;
    void** rax89;
    uint32_t* rax90;
    void*** rax91;
    void** rdi92;
    uint32_t* rax93;
    struct s18* rbx94;
    void* rdi95;
    int32_t eax96;
    struct s18* rcx97;
    void* rax98;
    void* rdx99;
    void* rdx100;
    void* tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_7ba8_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_7b95_7:
    return rax15;
    addr_7ba8_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_7c19_11;
    } else {
        addr_7c19_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_16 + 16)) | 1);
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_7c30_14;
        } else {
            goto addr_7c30_14;
        }
    }
    rax23 = reinterpret_cast<struct s17*>(rax5 + 2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s17*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_80f8_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s17*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s17*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_80f8_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<void**>(&rcx26->f2);
    goto addr_7c19_11;
    addr_7c30_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9d3c + rax33 * 4) + 0x9d3c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_7d77_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_8479_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_847e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_8474_42;
            }
        } else {
            addr_7c5d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_7e78_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_7c67_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<unsigned char*>(rbx14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_8664_56; else 
                        goto addr_7eb5_57;
                }
            } else {
                addr_7c67_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_7c88_60;
                } else {
                    goto addr_7c88_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_81fb_65;
    addr_7d77_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_80f8_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_7d9c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_7e1a_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_871b_75; else 
            goto addr_7dc3_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_80fc_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_7c67_52;
        goto addr_7e78_44;
    }
    addr_847e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_7c5d_43;
    addr_871b_75:
    if (v11 != rdx53) {
        fun_23a0(rdx53);
        r10_4 = r10_4;
        goto addr_852a_84;
    } else {
        goto addr_852a_84;
    }
    addr_7dc3_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_25c0(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2610(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_871b_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_2590(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_7e1a_68;
    addr_81fb_65:
    rbx65 = reinterpret_cast<struct s17*>(rbx14 + 2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s17*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_80f8_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s17*>(&rbx65->pad2);
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s17*>(&rbx65->pad2);
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_80f8_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = reinterpret_cast<void**>(&rdx68->f2);
    goto addr_7d9c_67;
    label_41:
    addr_8474_42:
    goto addr_8479_36;
    addr_8664_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_868a_104;
    addr_7eb5_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_80f8_22:
            r8_54 = r15_7->f8;
            goto addr_80fc_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_7ec4_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_8572_111;
    } else {
        addr_7ed1_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_7f07_116;
        }
    }
    rdx53 = r8_54;
    goto addr_871b_75;
    addr_8572_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_25c0(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_852a_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_23a0(rdi86);
            }
        } else {
            addr_87b0_120:
            rdx87 = r15_7->f0;
            rax88 = fun_2590(rdi84, r8_85, reinterpret_cast<unsigned char>(rdx87) << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_85cf_121;
        }
    } else {
        rax89 = fun_2610(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_871b_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_87b0_120;
            }
        }
    }
    rax90 = fun_23c0(rdi86);
    *rax90 = 12;
    return 0xffffffff;
    addr_85cf_121:
    r15_7->f8 = r8_54;
    goto addr_7ed1_112;
    addr_7f07_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_80fc_80:
            if (v11 != r8_54) {
                fun_23a0(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_7c67_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_7c67_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_23a0(rdi92, rdi92);
    }
    rax93 = fun_23c0(rdi92, rdi92);
    *rax93 = 22;
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_7b95_7;
    addr_868a_104:
    rbx94 = reinterpret_cast<struct s18*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s18*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (reinterpret_cast<uint64_t>(rdi95) > 0x1999999999999999) {
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi95) + reinterpret_cast<uint64_t>(rdi95) * 4);
            rdx99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx99) + reinterpret_cast<uint64_t>(rax98)), rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_101) < reinterpret_cast<uint64_t>(rdx99)) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_80f8_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s18*>(&rbx94->pad2);
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s18*>(&rbx94->pad2);
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_101) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_80f8_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx97->f2);
    goto addr_7ec4_107;
    addr_7c88_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x9ea0 + rax105 * 4) + 0x9ea0;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9de4 + rax106 * 4) + 0x9de4;
    }
}

void fun_8883() {
    __asm__("cli ");
}

void fun_8897() {
    __asm__("cli ");
    return;
}

uint32_t fun_2530(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_26c0(int64_t rdi, void** rsi);

uint32_t fun_26b0(void** rdi, void** rsi);

void** fun_26d0(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_35c5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2450();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2450();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2470(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_38c3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_38c3_22; else 
                            goto addr_3cbd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_3d7d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_40d0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_38c0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_38c0_30; else 
                                goto addr_40e9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2470(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_40d0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2530(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_40d0_28; else 
                            goto addr_376c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4230_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_40b0_40:
                        if (r11_27 == 1) {
                            addr_3c3d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_41f8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3877_44;
                            }
                        } else {
                            goto addr_40c0_46;
                        }
                    } else {
                        addr_423f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_3c3d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_38c3_22:
                                if (v47 != 1) {
                                    addr_3e19_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2470(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_3e64_54;
                                    }
                                } else {
                                    goto addr_38d0_56;
                                }
                            } else {
                                addr_3875_57:
                                ebp36 = 0;
                                goto addr_3877_44;
                            }
                        } else {
                            addr_40a4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_423f_47; else 
                                goto addr_40ae_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_3c3d_41;
                        if (v47 == 1) 
                            goto addr_38d0_56; else 
                            goto addr_3e19_52;
                    }
                }
                addr_3931_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_37c8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_37ed_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_3af0_65;
                    } else {
                        addr_3959_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_41a8_67;
                    }
                } else {
                    goto addr_3950_69;
                }
                addr_3801_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_384c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_41a8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_384c_81;
                }
                addr_3950_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_37ed_64; else 
                    goto addr_3959_66;
                addr_3877_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_392f_91;
                if (v22) 
                    goto addr_388f_93;
                addr_392f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3931_62;
                addr_3e64_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_45eb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_465b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_445f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26c0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_26b0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_3f5e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_391c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_3f68_112;
                    }
                } else {
                    addr_3f68_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4039_114;
                }
                addr_3928_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_392f_91;
                while (1) {
                    addr_4039_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4547_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_3fa6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4555_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4027_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4027_128;
                        }
                    }
                    addr_3fd5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4027_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_3fa6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3fd5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_384c_81;
                addr_4555_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_41a8_67;
                addr_45eb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_3f5e_109;
                addr_465b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_3f5e_109;
                addr_38d0_56:
                rax93 = fun_26d0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_391c_110;
                addr_40ae_59:
                goto addr_40b0_40;
                addr_3d7d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_38c3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3928_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3875_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_38c3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_3dc2_160;
                if (!v22) 
                    goto addr_4197_162; else 
                    goto addr_43a3_163;
                addr_3dc2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4197_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_41a8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_3c6b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_3ad3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3801_70; else 
                    goto addr_3ae7_169;
                addr_3c6b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_37c8_63;
                goto addr_3950_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_40a4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_41df_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_38c0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_37b8_178; else 
                        goto addr_4162_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_40a4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_38c3_22;
                }
                addr_41df_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_38c0_30:
                    r8d42 = 0;
                    goto addr_38c3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3931_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_41f8_42;
                    }
                }
                addr_37b8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_37c8_63;
                addr_4162_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_40c0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3931_62;
                } else {
                    addr_4172_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_38c3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4922_188;
                if (v28) 
                    goto addr_4197_162;
                addr_4922_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_3ad3_168;
                addr_376c_37:
                if (v22) 
                    goto addr_4763_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3783_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4230_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_42bb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_38c3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_37b8_178; else 
                        goto addr_4297_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_40a4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_38c3_22;
                }
                addr_42bb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_38c3_22;
                }
                addr_4297_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_40c0_46;
                goto addr_4172_186;
                addr_3783_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_38c3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_38c3_22; else 
                    goto addr_3794_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_486e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_46f4_210;
            if (1) 
                goto addr_46f2_212;
            if (!v29) 
                goto addr_432e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_4861_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3af0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_38ab_219; else 
            goto addr_3b0a_220;
        addr_388f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_38a3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_3b0a_220; else 
            goto addr_38ab_219;
        addr_445f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_3b0a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_447d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_48f0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4356_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_4547_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_38a3_221;
        addr_43a3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_38a3_221;
        addr_3ae7_169:
        goto addr_3af0_65;
        addr_486e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_3b0a_220;
        goto addr_447d_222;
        addr_46f4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_474e_236;
        fun_2490();
        rsp25 = rsp25 - 8 + 8;
        goto addr_48f0_225;
        addr_46f2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_46f4_210;
        addr_432e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_46f4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_4356_226;
        }
        addr_4861_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_3cbd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x966c + rax113 * 4) + 0x966c;
    addr_40e9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x976c + rax114 * 4) + 0x976c;
    addr_4763_190:
    addr_38ab_219:
    goto 0x3590;
    addr_3794_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x956c + rax115 * 4) + 0x956c;
    addr_474e_236:
    goto v116;
}

void fun_37b0() {
}

void fun_3968() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3662;
}

void fun_39c1() {
    goto 0x3662;
}

void fun_3aae() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3931;
    }
    if (v2) 
        goto 0x43a3;
    if (!r10_3) 
        goto addr_450e_5;
    if (!v4) 
        goto addr_43de_7;
    addr_450e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_43de_7:
    goto 0x37e4;
}

void fun_3acc() {
}

void fun_3b77() {
    signed char v1;

    if (v1) {
        goto 0x3aff;
    } else {
        goto 0x383a;
    }
}

void fun_3b91() {
    signed char v1;

    if (!v1) 
        goto 0x3b8a; else 
        goto "???";
}

void fun_3bb8() {
    goto 0x3ad3;
}

void fun_3c38() {
}

void fun_3c50() {
}

void fun_3c7f() {
    goto 0x3ad3;
}

void fun_3cd1() {
    goto 0x3c60;
}

void fun_3d00() {
    goto 0x3c60;
}

void fun_3d33() {
    goto 0x3c60;
}

void fun_4100() {
    goto 0x37b8;
}

void fun_43fe() {
    signed char v1;

    if (v1) 
        goto 0x43a3;
    goto 0x37e4;
}

void fun_44a5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x37e4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x37c8;
        goto 0x37e4;
    }
}

void fun_48c2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x3b30;
    } else {
        goto 0x3662;
    }
}

void fun_57f8() {
    fun_2450();
}

int32_t fun_2390(int64_t rdi);

struct s19 {
    signed char[1] pad1;
    signed char f1;
};

struct s20 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_6cf8() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    void** rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    void** rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    void** rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    void** rax30;
    void** rcx31;
    void* rbx32;
    void* rbx33;
    void** tmp64_34;
    void* r12_35;
    void** rax36;
    void** rbx37;
    int64_t r15_38;
    int64_t rbp39;
    void** r15_40;
    void** rax41;
    void** rax42;
    int64_t r12_43;
    void** r15_44;
    void** r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s20* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_2390(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<void***>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_6e83_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_2390(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_2390(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_6e83_5;
    }
    rcx19 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x271d;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_6d8d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0x7267;
        }
    } else {
        addr_6d85_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_6d8d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0x6ed0;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0x6ca8;
        goto 0x7342;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0x7342;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_6dc6_26;
    rax36 = rcx31;
    addr_6dc6_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0x6ca8;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0x7342;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_2610(r15_40, rax36);
        if (!rax41) 
            goto 0x7342;
        goto 0x6ca8;
    }
    rax42 = fun_25c0(rax36, rsi10);
    if (!rax42) 
        goto 0x7342;
    if (r12_43) 
        goto addr_718a_36;
    goto 0x6ca8;
    addr_718a_36:
    fun_2590(rax42, r15_44, r12_45);
    goto 0x6ca8;
    addr_6e83_5:
    if ((*reinterpret_cast<struct s19**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s19**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0x6ca8;
    }
    if (eax7 >= 0) 
        goto addr_6d85_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0x6ed0;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_6ec7_42;
    eax50 = 84;
    addr_6ec7_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_6e10() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_6f00() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x7045;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_2390(r15_4 + r12_5);
            goto 0x6d5d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x6d33;
        }
    }
}

void fun_6f48() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_2390(rdi5);
            goto 0x6d5d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_2390(rdi5);
    goto 0x6d5d;
}

void fun_6fb0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x6e36;
}

void fun_7000() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x6fe0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x6e3f;
}

void fun_70b0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x6e36;
    goto 0x6fe0;
}

void fun_7143() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x6bb2;
}

void fun_72e0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x7267;
}

struct s21 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s22 {
    signed char[8] pad8;
    int64_t f8;
};

struct s23 {
    signed char[16] pad16;
    int64_t f10;
};

struct s24 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_7908() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s21* rcx3;
    struct s22* rcx4;
    int64_t r11_5;
    struct s23* rcx6;
    uint32_t* rcx7;
    struct s24* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x78f0; else 
        goto "???";
}

struct s25 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s26 {
    signed char[8] pad8;
    int64_t f8;
};

struct s27 {
    signed char[16] pad16;
    int64_t f10;
};

struct s28 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_7940() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s25* rcx3;
    struct s26* rcx4;
    int64_t r11_5;
    struct s27* rcx6;
    uint32_t* rcx7;
    struct s28* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x7926;
}

struct s29 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s30 {
    signed char[8] pad8;
    int64_t f8;
};

struct s31 {
    signed char[16] pad16;
    int64_t f10;
};

struct s32 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_7960() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s29* rcx3;
    struct s30* rcx4;
    int64_t r11_5;
    struct s31* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s32* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x7926;
}

struct s33 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s34 {
    signed char[8] pad8;
    int64_t f8;
};

struct s35 {
    signed char[16] pad16;
    int64_t f10;
};

struct s36 {
    signed char[16] pad16;
    signed char f10;
};

void fun_7980() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s33* rcx3;
    struct s34* rcx4;
    int64_t r11_5;
    struct s35* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s36* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x7926;
}

struct s37 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s38 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_7a00() {
    struct s37* rcx1;
    struct s38* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x7926;
}

struct s39 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s40 {
    signed char[8] pad8;
    int64_t f8;
};

struct s41 {
    signed char[16] pad16;
    int64_t f10;
};

struct s42 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_7a50() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s39* rcx3;
    struct s40* rcx4;
    int64_t r11_5;
    struct s41* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s42* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x7926;
}

void fun_7c9c() {
}

void fun_7ccb() {
    goto 0x7ca8;
}

void fun_7d20() {
}

void fun_7f30() {
    goto 0x7d23;
}

struct s43 {
    signed char[8] pad8;
    void** f8;
};

struct s44 {
    signed char[8] pad8;
    int64_t f8;
};

struct s45 {
    signed char[8] pad8;
    void** f8;
};

struct s46 {
    signed char[8] pad8;
    void** f8;
};

void fun_7fd8() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s43* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s44* r15_14;
    void** rax15;
    struct s45* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s46* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x8717;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x8717;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_25c0(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x8538; else 
                goto "???";
        }
    } else {
        rax15 = fun_2610(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x8717;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_886a_9; else 
            goto addr_804e_10;
    }
    addr_81a4_11:
    rax19 = fun_2590(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_804e_10:
    r14_20->f8 = rcx12;
    goto 0x7b69;
    addr_886a_9:
    r13_18 = *r14_21;
    goto addr_81a4_11;
}

struct s47 {
    signed char[11] pad11;
    void** fb;
};

struct s48 {
    signed char[80] pad80;
    int64_t f50;
};

struct s49 {
    signed char[80] pad80;
    int64_t f50;
};

struct s50 {
    signed char[8] pad8;
    void** f8;
};

struct s51 {
    signed char[8] pad8;
    void** f8;
};

struct s52 {
    signed char[8] pad8;
    void** f8;
};

struct s53 {
    signed char[72] pad72;
    signed char f48;
};

struct s54 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_8261() {
    void** ecx1;
    int32_t edx2;
    struct s47* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s48* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s49* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s50* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s51* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s52* r15_37;
    void*** r13_38;
    struct s53* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s54* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s47*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x80f8;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x884c;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_83bd_12;
    } else {
        addr_7f64_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_7f9f_17;
        }
    }
    rax25 = fun_25c0(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x852a;
    addr_84d0_19:
    rdx30 = *r15_31;
    rax32 = fun_2590(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_8406_20:
    r15_33->f8 = r8_12;
    goto addr_7f64_13;
    addr_83bd_12:
    rax34 = fun_2610(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x8717;
    if (v36 != r15_37->f8) 
        goto addr_8406_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_84d0_19;
    addr_7f9f_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x80fc;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x7fe0;
    goto 0x7b69;
}

void fun_827f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x7f48;
    if (dl2 & 4) 
        goto 0x7f48;
    if (edx3 > 7) 
        goto 0x7f48;
    if (dl4 & 2) 
        goto 0x7f48;
    goto 0x7f48;
}

void fun_82c8() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x7f48;
    if (dl2 & 4) 
        goto 0x7f48;
    if (edx3 > 7) 
        goto 0x7f48;
    if (dl4 & 2) 
        goto 0x7f48;
    goto 0x7f48;
}

void fun_8310() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x7f48;
    if (dl2 & 4) 
        goto 0x7f48;
    if (edx3 > 7) 
        goto 0x7f48;
    if (dl4 & 2) 
        goto 0x7f48;
    goto 0x7f48;
}

void fun_8358() {
    goto 0x7f48;
}

void fun_39ee() {
    goto 0x3662;
}

void fun_3bc4() {
    goto 0x3b7c;
}

void fun_3c8b() {
    goto 0x37b8;
}

void fun_3cdd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3c60;
    goto 0x388f;
}

void fun_3d0f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x3c6b;
        goto 0x3690;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x3b0a;
        goto 0x38ab;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x44a8;
    if (r10_8 > r15_9) 
        goto addr_3bf5_9;
    addr_3bfa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x44b3;
    goto 0x37e4;
    addr_3bf5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_3bfa_10;
}

void fun_3d42() {
    goto 0x3877;
}

void fun_4110() {
    goto 0x3877;
}

void fun_48af() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x39cc;
    } else {
        goto 0x3b30;
    }
}

void fun_58b0() {
}

struct s55 {
    signed char[1] pad1;
    signed char f1;
};

void fun_6ba0() {
    signed char* r13_1;
    struct s55* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_72ee() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x7267;
}

struct s56 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s57 {
    signed char[8] pad8;
    int64_t f8;
};

struct s58 {
    signed char[16] pad16;
    int64_t f10;
};

struct s59 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_7a20() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s56* rcx3;
    struct s57* rcx4;
    int64_t r11_5;
    struct s58* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s59* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x7926;
}

void fun_7cd5() {
    goto 0x7ca8;
}

void fun_8624() {
    goto 0x7f48;
}

void fun_7f38() {
}

void fun_8368() {
    goto 0x7f48;
}

void fun_3d4c() {
    goto 0x3ce7;
}

void fun_411a() {
    goto 0x3c3d;
}

void fun_5910() {
    fun_2450();
    goto fun_26a0;
}

void fun_7080() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x6e36;
    goto 0x6fe0;
}

void fun_72fc() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x7267;
}

struct s60 {
    int32_t f0;
    int32_t f4;
};

struct s61 {
    int32_t f0;
    int32_t f4;
};

struct s62 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s63 {
    signed char[8] pad8;
    int64_t f8;
};

struct s64 {
    signed char[8] pad8;
    int64_t f8;
};

struct s65 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_79d0(struct s60* rdi, struct s61* rsi) {
    struct s62* rcx3;
    struct s63* rcx4;
    struct s64* rcx5;
    struct s65* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x7926;
}

void fun_7cdf() {
    goto 0x7ca8;
}

void fun_862e() {
    goto 0x7f48;
}

void fun_3a1d() {
    goto 0x3662;
}

void fun_3d58() {
    goto 0x3ce7;
}

void fun_4127() {
    goto 0x3c8e;
}

void fun_5950() {
    fun_2450();
    goto fun_26a0;
}

void fun_730b() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x7267;
}

void fun_7ce9() {
    goto 0x7ca8;
}

void fun_3a4a() {
    goto 0x3662;
}

void fun_3d64() {
    goto 0x3c60;
}

void fun_5990() {
    fun_2450();
    goto fun_26a0;
}

void fun_7cf3() {
    goto 0x7ca8;
}

void fun_3a6c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4400;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3931;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3931;
    }
    if (v11) 
        goto 0x4763;
    if (r10_12 > r15_13) 
        goto addr_47b3_8;
    addr_47b8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x44f1;
    addr_47b3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_47b8_9;
}

struct s66 {
    signed char[24] pad24;
    int64_t f18;
};

struct s67 {
    signed char[16] pad16;
    void** f10;
};

struct s68 {
    signed char[8] pad8;
    void** f8;
};

void fun_59e0() {
    int64_t r15_1;
    struct s66* rbx2;
    void** r14_3;
    struct s67* rbx4;
    void** r13_5;
    struct s68* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2450();
    fun_26a0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5a02, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5a38() {
    fun_2450();
    goto 0x5a09;
}

struct s69 {
    signed char[32] pad32;
    int64_t f20;
};

struct s70 {
    signed char[24] pad24;
    int64_t f18;
};

struct s71 {
    signed char[16] pad16;
    void** f10;
};

struct s72 {
    signed char[8] pad8;
    void** f8;
};

struct s73 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_5a70() {
    int64_t rcx1;
    struct s69* rbx2;
    int64_t r15_3;
    struct s70* rbx4;
    void** r14_5;
    struct s71* rbx6;
    void** r13_7;
    struct s72* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s73* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2450();
    fun_26a0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x5aa4, __return_address(), rcx1);
    goto v15;
}

void fun_5ae8() {
    fun_2450();
    goto 0x5aab;
}
